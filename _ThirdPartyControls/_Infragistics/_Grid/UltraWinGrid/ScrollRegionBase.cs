#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Collections;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Security.Permissions;
	using Infragistics.Win.UltraWinScrollBar;
	using Infragistics.Shared.Serialization;

    /// <summary>
    ///    Summary description for ScrollRegionBase.
    /// </summary>
	

    public abstract class ScrollRegionBase : SubObjectBase
    {
		
		//Following two constants are defined in ColScrollRegion.h
		//file (in c++ version of UltraGRID) and are being used in
		//InternalScroll function
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//internal static readonly int SCROLL_ORIGIN_BASE = 0x40000000;
		internal const int SCROLL_ORIGIN_BASE = 0x40000000;

		internal ScrollEventType  lastScrollType;
	

		//bool regeneratedDuringScrolling;
    	//bool scrolling;
		//bool scrolling;
		//bool regeneratedDuringScrolling; 


		private Scrollbar			scrollbar;
		private SizingMode			sizingMode;

		/// <summary>
		/// collections of scroll regions
		/// </summary>
		// AS 1/8/03 - fxcop
		// Cannot have 2 public/protected members that differ only by case
        //protected ScrollRegionsCollectionBase collection = null;
		protected ScrollRegionsCollectionBase collectionValue = null;

        private bool    hidden = false;
        private int     origin = 0;
        private int     extent = 0;

		/// <summary>
		/// scroll bar rectangle
		/// </summary>
		// AS 1/8/03 - fxcop
		// Cannot have 2 public/protected members that differ only by case
		//protected Rectangle scrollbarRect;
		protected Rectangle scrollbarRectValue;

		private HeadersCollection exclusiveItems = null;

		/// <summary>
		/// indicates whether the scrollbarRect member var is set
		/// </summary>
        protected bool    scrollbarRectSet = false;

		/// <summary>
		/// internal flag used to indicate if the scrollbar position
		/// is dirty either resulting from rows being added or bands
		/// expanded
		/// </summary>
        protected bool    scrollbarPositionDirty = false;

		/// <summary>
		/// internal flag indicating if rows/headers are being regenerated
		/// </summary>
        protected bool    regenerating = false;

		/// <summary>
		/// internal flag indicating if origin is currently cached
		/// </summary>
        protected bool    originCached = false;

		/// <summary>
		/// internal state flag indicating if scrolling is taking place
		/// </summary>
        protected bool    scrolling = false;

		/// <summary>
		/// internal flag indicating whether rows are being regenerated
		/// </summary>
        protected bool    regeneratedDuringScrolling = false;

		/// <summary>
		/// internal flag indicating whether a event has be interrupted
		/// </summary>
	    protected bool	  eventInterrupted = false;

		/// <summary>
		/// internal flag used for optimization purposes
		/// </summary>
	    protected bool	  dirtyGridElement = false;

		/// <summary>
		/// internally used by scrolling code to cache scroll origin
		/// </summary>
		// AS 1/8/03 - fxcop
		// Cannot have 2 public/protected members that differ only by case
		//protected int     cachedScrollOrigin;
		protected int     cachedScrollOriginValue;

		/// <summary>
		/// cached value for width of last scroll region resize
		/// </summary>
        protected int     extentLastResize;

		/// <summary>
		/// scroll bar control for the scroll region
		/// </summary>
		// AS - 12/14/01
		// Changed to use the internal class instead of a separate scrollbar control
		//
		//	// AS - 11/26/01
		//	// Changed to use UltraScrollBar
		//	//protected  System.Windows.Forms.ScrollBar scrollBarInfo = null;
		//	protected UltraScrollBar scrollBarInfo = null;
		// AS 1/8/03 - fxcop
		// Cannot have 2 public/protected members that differ only by case
		//protected int     cachedScrollOrigin;
		//protected ScrollBarInfo		scrollBarInfo = null;
		protected ScrollBarInfo		scrollBarInfoValue = null;

		// JJD 3/8/02
		// Change member name from 'clonedFrom' to 'clonedFromRegion' since it is not CLSCompliant
		// to have a member and a property whose name differs by case only
		//
		/// <summary>
		/// If this instance is a clone, then clonedFromRegion references the instance
		/// that it was cloned from
		/// </summary>
        protected  ScrollRegionBase clonedFromRegion = null;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private  bool inOnScroll = false;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG	
		//        /// <summary>
		//        /// Returns true if we are in a scroll notification from the control.
		//        /// </summary>
		//#endif
		//        internal bool InOnScroll { get { return this.inOnScroll; } }

		#endregion Not Used

		internal ScrollRegionBase()
		{

		}
        internal ScrollRegionBase( ScrollRegionsCollectionBase collection )
        {
			// AS 1/8/03 fxcop
			// Cannot have 2 public/protected members that differ only by case
            //this.collection = collection;
			this.collectionValue = collection;

            this.Reset();

			// AS 1/8/03 fxcop
			// Cannot have 2 public/protected members that differ only by case
			//this.scrollbarRect = new Rectangle(0,0,0,0);
			this.scrollbarRectValue = new Rectangle(0,0,0,0);

			// AS 1/8/03 fxcop
			// Cannot have 2 public/protected members that differ only by case
			//this.cachedScrollOrigin = SCROLL_ORIGIN_BASE;
			this.cachedScrollOriginValue = SCROLL_ORIGIN_BASE;
        }

		/// <summary>
		/// Returns the Layout object that determines the layout of the object. This property is read-only at run-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The Layout property of an object is used to access the Layout object that determines the settings of various properties related to the appearance and behavior of the object. The Layout object provides a simple way to maintain multiple layouts for the grid and apply them as needed. You can also save grid layouts to disk, the registry or a storage stream and restore them later.</p>
		/// <p class="body">The Layout object has properties such as <b>Appearance</b> and <b>Override</b>, so the Layout object has sub-objects of these types, and their settings are included as part of the layout. However, the information that is actually persisted depends on how the settings of these properties were assigned. If the properties were set using the Layout object's intrinsic objects, the property settings will be included as part of the layout. However, if a named object was assigned to the property from a collection, the layout will only include the reference into the collection, not the actual settings of the named object. (For an overview of the difference between named and intrinsic objects, please see the <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.Appearance"/>property.</p>
		/// <p class="body">For example, if the Layout object's <b>Appearance</b> property is used to set values for the intrinsic Appearance object like this:</p>
		/// <p class="code">UltraGrid1.DisplayLayout.Appearance.ForeColor = vbBlue</p>
		/// <p class="body">Then the setting (in this case, <b>ForeColor</b>) will be included as part of the layout, and will be saved, loaded and applied along with the other layout data. However, suppose you apply the settings of a named object to the Layout's <b>Appearance</b> property in this manner:</p>
		/// <p class="code">UltraWinGrid1.Appearances.Add "New1"</p>
		/// <p class="code">UltraWinGrid1.Appearances("New1").ForeColor = vbBlue</p>
		/// <p class="code">UltraWinGrid1.Layout.Appearance = UltraWinGrid1.Appearances("New1")</p>
		/// <p class="body">In this case, the ForeColor setting will not be persisted as part of the layout. Instead, the layout will include a reference to the "New1" Appearance object and use whatever setting is present in that object when the layout is applied.</p>
		/// <p class="body">By default, the layout includes a copy of the entire Appearances collection, so if the layout is saved and restored using the default settings, the object should always be present in the collection when it is referred to. However, it is possible to use the <b>Load</b> and <b>Save</b> methods of the Layout object in such a way that the collection will not be re-created when the layout is applied. If this is the case, and the layout contains a reference to a nonexistent object, the default settings for that object's properties will be used.</p>
		/// </remarks>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Browsable(false)   ]
        public UltraGridLayout Layout
        {
            get 
            {
				// AS 2/28/02 UWG1033
				// If the collection is null, we should return null.
				//
				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return (this.collection == null ? null : this.collection.Layout);
				return (this.collectionValue == null ? null : this.collectionValue.Layout);
            }
        }

		

        /// <summary>
        /// If this is a cloned region then ClonedFrom is the
        /// region it was cloned from
        /// </summary>
        protected ScrollRegionBase ClonedFrom
        {
            get 
            {
                return this.clonedFromRegion;
            }
        }

        /// <summary>
        /// True if this is a temporary clone of the region passed
        /// into one of the 'Before' scroll events 
        /// </summary>
        public bool IsClone
        {
            get
            {
                return (null != this.clonedFromRegion);
            }
        }


		

        

		
 
        /// <summary>
        /// Returns true for regions with vertical scrollbars and
        /// false for regions with horizontal scrollbars
        /// </summary>
        protected abstract bool IsVerticalScrollbar
        {
            get;
        }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        /* AS - 12/14/01
		//         * 
		//        // AS - 11/26/01
		////		internal bool OnScroll( System.Windows.Forms.ScrollBar scrollBar,
		////			ScrollEventType scrollType, bool exitEditMode )
		//        internal bool OnScroll( UltraScrollBar scrollBar,
		//            ScrollEventType scrollType, bool exitEditMode )
		//        */
		//        internal bool OnScroll( ScrollBarInfo scrollBar,
		//            ScrollEventType scrollType, bool exitEditMode )
		//        {
		//#if DEBUG 
		////			Debug.WriteLine( "ScrollRegion OnScroll with exit: " + this.OnScrollCount.ToString() );
		//#endif
		//            return this.OnScroll( scrollBar, scrollType, exitEditMode, false );
		//        }

		//#if DEBUG 
		//        static private int onScrollCount = 0;

		//        private int OnScrollCount { get { return ++onScrollCount; } }
		//#endif

		#endregion Not Used

		
		internal bool OnScroll( ScrollBarInfo scrollBar,
			ScrollEventType scrollType )
		{


			return this.OnScroll( scrollBar, scrollType, true, false );
		}

		
		internal abstract bool OnScroll( ScrollBarInfo scrollBar,
			ScrollEventType scrollType, bool exitEditMode, bool invalidateRegion );


		internal void OnScroll( object sender, ScrollEventArgs e )
		{			

			// SSP 10/15/03 UWG2709
			// Before setting the Value of the ScrollBarInfo to the e.NewValue below, try
			// to exit the edit mode and if that doesn't succeed (because the user cancels
			// it), then don't scroll and set the NewValue to the original value. This
			// fixes the problem where if a cell's in edit mode and the user scrolls
			// the scrollbar's value changes even though no scrolling occured as a
			// result of developer canceling the BeforeExitEditMode.
			//
			// --------------------------------------------------------------------------
			if ( null != this.Layout.ActiveCell && this.Layout.ActiveCell.IsInEditMode )
			{				
				this.Layout.ActiveCell.ExitEditMode();

				if ( null != this.Layout.ActiveCell && this.Layout.ActiveCell.IsInEditMode )
				{
					if ( sender is ScrollBarInfo )
					{
						e.NewValue = ((ScrollBarInfo )sender).Value;

						// Cancel the scroll. Pass in true to prevent any scroll notifications from
						// coming in.
						//
						((ScrollBarInfo )sender).CancelScroll( true );
					}

					return;
				}
			}
			// --------------------------------------------------------------------------

			//in c#, new postion of the scrollbar is passed in as e.NewValue
			//However, scrollBarInfo.Value still has the old postion
			// AS 1/8/03 - fxcop
			// Cannot have 2 public/protected members that differ only by case
			//this.scrollBarInfo.Value = e.NewValue;
			this.scrollBarInfoValue.Value = e.NewValue;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.inOnScroll = true; 
			//
			//try 
			//{
				// AS - 12/14/01
				//this.OnScroll((System.Windows.Forms.ScrollBar)sender, e.Type );
				//this.OnScroll((UltraScrollBar)sender, e.Type );
				this.OnScroll( (ScrollBarInfo)sender, e.Type );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//}
			//finally
			//{
			//    this.inOnScroll = false; 
			//}
			
			// AS 1/8/03 - fxcop
			// Cannot have 2 public/protected members that differ only by case
			//e.NewValue = this.scrollBarInfo.Value;			
			e.NewValue = this.scrollBarInfoValue.Value;			
		}

		
		internal Infragistics.Win.UltraWinScrollBar.ScrollBarInfo ScrollBarInfo
        {
            get
            {
				// AS 1/8/03 - fxcop
				// Cannot have 2 public/protected members that differ only by case
				//if ( null == this.scrollBarInfo )
				if ( null == this.scrollBarInfoValue )
                {
					// AS - 11/26/01
					UltraControlBase grid = (this.Layout == null ? null : this.Layout.Grid );

					if ( this.IsVerticalScrollbar )
					{
						
						// AS 1/8/03 - fxcop
						// Cannot have 2 public/protected members that differ only by case
						//this.scrollBarInfo = new ScrollBarInfo( grid, Orientation.Vertical, null );
						this.scrollBarInfoValue = new ScrollBarInfo( grid, Orientation.Vertical, null );
					}
					else
					{
						
						// AS 1/8/03 - fxcop
						// Cannot have 2 public/protected members that differ only by case
						//this.scrollBarInfo = new ScrollBarInfo( grid, Orientation.Horizontal, null );
						this.scrollBarInfoValue = new ScrollBarInfo( grid, Orientation.Horizontal, null );
					}

					// AS - 12/19/01
					if (this.Layout != null)
						// AS 1/8/03 - fxcop
						// Cannot have 2 public/protected members that differ only by case
						//this.scrollBarInfo.ScrollBarLook = this.Layout.ScrollBarLook;
						this.scrollBarInfoValue.ScrollBarLook = this.Layout.ScrollBarLook;

					// AS - 12/14/01
					//this.scrollBarInfo.Bounds = this.ScrollbarRect;

					//Register the event handler for scrolling
					// AS 1/8/03 - fxcop
					// Cannot have 2 public/protected members that differ only by case
					//this.scrollBarInfo.Scroll += new ScrollEventHandler(OnScroll);
					this.scrollBarInfoValue.Scroll += new ScrollEventHandler(OnScroll);

					// JJD 1/16/02
					// Set autodisable to true which will disable the scrollbar when
					// the thumb would take up the entire thumb area
					//
					// AS 1/8/03 - fxcop
					// Cannot have 2 public/protected members that differ only by case
					//this.scrollBarInfo.AutoDisable = true;
					this.scrollBarInfoValue.AutoDisable = true;

					

					// SSP 6/11/02
					// Added PriorityScrolling property to grid.
					//
					if ( null != this.Layout )
						// AS 1/8/03 - fxcop
						// Cannot have 2 public/protected members that differ only by case
						//this.scrollBarInfo.PriorityScrolling = this.Layout.PriorityScrolling;
						this.scrollBarInfoValue.PriorityScrolling = this.Layout.PriorityScrolling;
				}
                
				// AS 1/8/03 - fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return this.scrollBarInfo;
				return this.scrollBarInfoValue;
            }
        }

		// SSP 6/11/02
		//
		internal bool HasScrollBarInfo
		{
			get
			{
				// AS 1/8/03 - fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return null != this.scrollBarInfo;				
				return null != this.scrollBarInfoValue;				
			}
		}

        /// <summary>
        /// Called when the object is disposed of
        /// </summary>
        protected override void OnDispose()
        {
            // unhook notifications and call dispose on the bands collection
            //
			// AS 1/8/03 - fxcop
			// Cannot have 2 public/protected members that differ only by case
			//if ( null != this.scrollBarInfo )
 			if ( null != this.scrollBarInfoValue )
           {
				// AS 1/8/03 - fxcop
				// Cannot have 2 public/protected members that differ only by case
				//this.scrollBarInfo.Scroll -= new ScrollEventHandler(OnScroll);
                //this.scrollBarInfo.Dispose();
				this.scrollBarInfoValue.Scroll -= new ScrollEventHandler(OnScroll);
                this.scrollBarInfoValue.Dispose();

				// AS - 12/14/01
                //this.Layout.Grid.Controls.Remove( this.scrollBarInfo );

				// AS 1/8/03 - fxcop
				// Cannot have 2 public/protected members that differ only by case
				//this.scrollBarInfo = null;
                this.scrollBarInfoValue = null;
            }
		}

        /// <summary>
        /// The origin of this ScrollRegionBase in client coordinates
        /// </summary>
        [ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
        public int Origin
        {
            get
            {
                return this.origin;
            }
			set
			{
				this.origin = value;
			}
        }
		internal void GetObjectData( SerializationInfo info, StreamingContext context )
		{
			//set the assembly name because of version number conflicts
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			if ( this.ShouldSerializeHidden() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Hidden", this.hidden  );
				//info.AddValue("Hidden", this.hidden );
			}

			if ( this.ShouldSerializeScrollbar() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Scrollbar", this.Scrollbar );
				//info.AddValue("Scrollbar", (int)this.Scrollbar );
			}

			if ( this.ShouldSerializeSizingMode() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SizingMode", this.SizingMode );
				//info.AddValue("SizingMode", (int)this.SizingMode );
			}

			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Extent", this.extent  );
			//info.AddValue("Extent", this.extent );

			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal ScrollRegionBase( SerializationInfo info, StreamingContext context )
		protected ScrollRegionBase( SerializationInfo info, StreamingContext context )
		{	
			//since we're not saving properties with default values, we must set the default here
			this.Reset();

			// AS 1/8/03 fxcop
			// Cannot have 2 public/protected members that differ only by case
			//this.scrollbarRect = new Rectangle(0,0,0,0);
			this.scrollbarRectValue = new Rectangle(0,0,0,0);

			// AS 1/8/03 fxcop
			// Cannot have 2 public/protected members that differ only by case
			//this.cachedScrollOrigin = SCROLL_ORIGIN_BASE;
			this.cachedScrollOriginValue = SCROLL_ORIGIN_BASE;

			//set in call to setcollection
			//this.collection = null;
			this.InitCollection ( null );

			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "Hidden":
						//this.Hidden = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Hidden = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.hidden );
						//this.Hidden = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
						
					case "Scrollbar":
						//this.Scrollbar = (Infragistics.Win.UltraWinGrid.Scrollbar)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Scrollbar = (Scrollbar)Utils.DeserializeProperty( entry, typeof(Scrollbar), this.scrollbar );
						//this.Scrollbar = (Scrollbar)Utils.ConvertEnum(entry.Value, this.Scrollbar);
						break;

					case "SizingMode":
						//this.SizingMode = (Infragistics.Win.UltraWinGrid.SizingMode)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.SizingMode = (SizingMode)Utils.DeserializeProperty( entry, typeof(SizingMode), this.sizingMode );
						//this.SizingMode = (SizingMode)Utils.ConvertEnum(entry.Value, this.SizingMode);
						break;

					case "Tag":
						// AS 8/15/02
						// Use our tag deserializer.
						//
						//this.tagValue = entry.Value;
						this.DeserializeTag(entry);
						break;
                        						
					case "Extent":
						//this.Extent = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Extent = (int)Utils.DeserializeProperty( entry, typeof(int), this.extent );
						//this.Extent = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;
				}
			}
		}
		internal void InitCollection( Infragistics.Win.UltraWinGrid.ScrollRegionsCollectionBase collection )			
		{
			// AS 1/8/03 fxcop
			// Cannot have 2 public/protected members that differ only by case
			//this.collection = collection;		
			this.collectionValue = collection;		
		}

		internal Infragistics.Win.UltraWinGrid.ScrollRegionsCollectionBase Collection
		{
			get
			{
				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return this.collection;
				return this.collectionValue;
			}
		}

		internal int Extent
        {
            get
            {
				//RobA 10/16/01 if the extent has not been initialized then
				//just returns the control's width
				//
				//if ( this.extent == 0 )
				//	return this.Layout.Grid.Width;	
				
                return this.extent;
            }

			set
			{
				this.extent = value;
			}
        }
 
        /// <summary>
        /// Returns true if this is the first visible region (read-only)
        /// </summary>
        public bool IsFirstVisibleRegion
        {
            get
            {
				if ( this.clonedFromRegion != null )
					return this.clonedFromRegion.IsFirstVisibleRegion;

				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return ( this == this.collection.FirstVisibleRegion );
 				return ( this == this.collectionValue.FirstVisibleRegion );
           }
        }
 
        /// <summary>
        /// Returns true if this is the last visible region (read-only)
        /// </summary>
        public bool IsLastVisibleRegion
        {
            get
            {
				if ( this.clonedFromRegion != null )
					return this.clonedFromRegion.IsLastVisibleRegion;

				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return (this == this.collection.LastVisibleRegion );
				return (this == this.collectionValue.LastVisibleRegion );
            }
        }

		// SSP 12/1/04 - Merged Cell Feature
		// Added IsActiveScrollRegion  method.
		//
		/// <summary>
		/// Returns true if this scroll region is active. In the case of the row scroll region returns 
		/// true if this instance is the same as the <see cref="UltraGridBase.ActiveRowScrollRegion"/>. 
		/// Likewise for the column scroll region.
		/// </summary>
		public abstract bool IsActiveScrollRegion { get; }

		internal Rectangle ScrollbarRect
        {
            get
            {
				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return this.scrollbarRect;
				return this.scrollbarRectValue;
            }

            set
            {

				if ( this.scrollbarRectSet &&  this.ScrollbarRect.Equals(value) )
				{
					return;
				}

				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//this.scrollbarRect = value;
				this.scrollbarRectValue = value;

                this.scrollbarRectSet = true;


				// SSP 8/7/01 UWG17
				// When scrollBarRect is set to empty rect, we still need to
				// call PositionScrollbar so that it hides it
				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//if (this.scrollbarRect.IsEmpty)
				if (this.scrollbarRectValue.IsEmpty)
				{
					// Instead of calling PositionScrollBar when the rect is empty
					// which causes the DirtyGridElement to be called in an infinite
					// loop somehow, just hide the scrollbar.
					//ROBA 8/17/01 UWG176
					// AS 1/8/03 - fxcop
					// Cannot have 2 public/protected members that differ only by case
					//if ( null != this.scrollBarInfo && 
					//	 this.scrollBarInfo.Visible )
					//	this.scrollBarInfo.Hide();
					if ( null != this.scrollBarInfoValue && 
						 this.scrollBarInfoValue.Visible )
						this.scrollBarInfoValue.Hide();
					
					return;
				}

				this.PositionScrollbar( true );
				
                //Look in C++ version of ColScrollRegion::SetScrollbarRect
            }
        }

		// SSP 11/2/01
		// Made this method virtual and overrode it in Row and Col scroll region
		// derived classes
		//
		internal virtual void SetOriginAndExtent( int origin, int extent ) 
        {
			if ( this.origin == origin  &&
				this.extent == extent )
			{
				// JJD 10/30/01
				// if the origin and extent haven't changed we 
				// still need to call PositionScrollbar
				//
				this.PositionScrollbar( false );
				return;
			}

            this.origin = origin;
            this.extent = extent;

			// JJD 10/30/01
			// We need to position the scrollbar here
			//
			this.PositionScrollbar( true );

			// JJD 10/30/01
			// The following code was removed. It made no sense.
			//
			// SSP 8/29/01 UWG259

		}

		// SSP 10/22/03 UWG2442
		// Added AreAnyOtherScrollRegionsVisible method.
		//
		private bool AreAnyOtherScrollRegionsVisible( )
		{
			// Look in IsFirstVisibleRegion and IsLastVisibleRegion for more info.
			//
			if ( this.clonedFromRegion != null )
				return this.clonedFromRegion.AreAnyOtherScrollRegionsVisible( );

			Debug.Assert( null != this.Collection, "No collection !" );

			ScrollRegionsCollectionBase collection = this.Collection;
			if ( null != collection )
			{
				for ( int i = 0; i < collection.Count; i++ )
				{
					ScrollRegionBase scrollRegion = collection.GetItem( i ) as ScrollRegionBase;

					if ( null != scrollRegion && scrollRegion != this && ! scrollRegion.Hidden )
						return true;
				}
			}

			return false;
		}

        /// <summary>
        /// Determines whether the object will be displayed. This property is not available at design-time.
        /// </summary>
		/// <remarks>
		/// <p class="body">The <b>Hidden</b> property determines whether an object is visible. Hiding an object may have have effects that go beyond simply removing it from view. For example, hiding a band also hides all the rows in that band. Also, changing the <b>Hidden</b> property of an object affects all instances of that object. For example, a hidden column or row is hidden in all scrolling regions.</p>
		/// <p class="body">There may be instances where the <b>Hidden</b> property cannot be changed. For example, you cannot hide the currently active rowscrollregion or colscrollregion. If you attempt to set the <b>Hidden</b> property of the active rowscrollregion to True, an error will occur.</p>
		/// </remarks>
        public bool Hidden
        {
            get
            {
                return this.hidden;
            }
            set
            {
                if ( value != this.hidden )
                {
					// SSP 10/22/03 UWG2442
					// Instead of checking if this is the last visible region among possibly
					// many visible scroll regions, check if there are any other visible
					// scroll regions besides this one. (IsLastVisibleRegion name is misleading).
					//
					//if ( this.IsLastVisibleRegion )
					if ( value && ! this.AreAnyOtherScrollRegionsVisible( ) )
					{
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_258"));
					}
					else
					{
						// SSP 10/22/03 UWG2442
						// If this scroll region is either ActiveColScrollRegion or ActiveRowScrollRegion then
						// reset it to null so it's get will reassign itself to the first visible scroll 
						// region.
						//
						// ------------------------------------------------------------------------------------
						if ( value && ! this.Disposed && null != this.Layout && ! this.Layout.Disposed 
							&& null != this.Layout.Grid && ! this.Layout.Grid.IsDisposed )
						{
							if ( this == this.Layout.ActiveColScrollRegion  )
								this.Layout.Grid.InternalResetActiveColScrollRegion( );
							else if ( this == this.Layout.ActiveRowScrollRegion )
								this.Layout.Grid.InternalResetActiveRowScrollRegion( );
						}
						// ------------------------------------------------------------------------------------

						this.hidden = value;

						// SSP 10/22/03 UWG2442
						// Also dirty the grid element so the grid gets refreshed.
						//
						if ( null != this.Layout )
							this.Layout.DirtyGridElement( );
					}
                }
            }
        }

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeHidden() 
        {
           return this.hidden;		
        }
 
		/// <summary>
		/// Resets Hidden to its default value (False.)
		/// </summary>
        public void ResetHidden() 
        {
            this.Hidden = false;
        }

		/// <summary>
		/// overall rect of this scroll region base
		/// </summary>
        public abstract Rectangle Rect { get; }

        internal void Invalidate( Rectangle intersectRect )
        {
            if ( !this.Layout.IsDisplayLayout )
                return;

            if ( intersectRect.IntersectsWith ( this.Rect ) )
            {
                intersectRect.Intersect( this.Rect );

				// SSP 9/28/01 UWG 342
				// We were constructing a region without disposing of it
				// and we didn't even need to create a region. Just
				// pass in the rect
				//
                //this.Layout.Grid.Invalidate( new Region( intersectRect ) );

				// JJD 10/02/01
				// Invalidate the ControlForGridDisplay instead to support
				// the UltraCombo
				//
				//this.Layout.Grid.Invalidate( intersectRect );
				this.Layout.Grid.ControlForGridDisplay.Invalidate( intersectRect );
            }
        }

        internal void Invalidate()
        {
            if ( !this.Layout.IsDisplayLayout )
                return;

			// SSP 9/28/01 UWG 342
			// We were constructing a region without disposing of it
			// and we didn't even need to create a region. Just
			// pass in the rect
			//
            //this.Layout.Grid.Invalidate( new Region( this.Rect ) );

			// JJD 10/02/01
			// Invalidate the ControlForGridDisplay instead to support
			// the UltraCombo
			//
			// this.Layout.Grid.Invalidate( this.Rect );
			this.Layout.Grid.ControlForGridDisplay.Invalidate( this.Rect );
        }

        /// <summary>
        /// Returns true is any of the properties have been
        /// set to non-default values
        /// </summary>
        public virtual bool ShouldSerialize() 
        {
			return	this.ShouldSerializeHidden() ||
					this.ShouldSerializeScrollbar() ||
					this.ShouldSerializeSizingMode() ||
					this.ShouldSerializeTag();            
        }
 
        /// <summary>
        /// Resets all properties back to their default values
        /// </summary>
        public virtual void Reset() 
        {
			this.ResetHidden();
			this.ResetScrollbar();
			this.ResetSizingMode();
			this.ResetTag();
			
        }
    
		/// <summary>
		/// Returns or sets a value that indicates whether a scroll bar will be displayed for a scrolling region.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property determines whether a scroll bar should be displayed for a scrolling region.</p>
		/// <p class="body">When a colscrollregion is scrolled, the <b>BeforeColRegionScroll</b> event is generated. When a rowscrollregion is scrolled, the <b>BeforeRowRegionScroll</b> event is generated.</p>
		/// <p class="body">A scrolling region can be scrolled programmatically, even if no scroll bars are displayed, by invoking its <b>Scroll</b> method.</p>
		/// <p class="body">The user can be prevented from scrolling a colscrollregion or rowscrollregion, even if its scroll bars are displayed, by setting the <i>cancel</i> argument of the <b>BeforeColRegionScroll</b> or <b>BeforeRowRegionScroll</b> event, respectively, to True.</p>
		/// <p class="body">The current, as well as maximum, position of a colscrollregion's scroll bar can be determined by its <b>Range</b> and <b>Position</b> properties, respectively.</p>
		/// <p class="body">The <b>ScrollBars</b> property can be used to set the value of the <b>ScrollBar</b> property for all colscrollregions and rowscrollregions that have their <b>ScrollBar</b> property set to 0 (ScrollBarDefault).</p>
		/// </remarks>
    	public Infragistics.Win.UltraWinGrid.Scrollbar Scrollbar
        {
            get
            {
                return this.scrollbar;
            }

            set
            {
                if ( value != this.scrollbar )
                {
                    // test that the value is in range
                    //
					// SSP 11/6/01 UWG650
					// Use the fully qualified ScrollBar type name
					//
                    //if ( !Enum.IsDefined( typeof(ScrollBar), value ) )
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.Scrollbar ), value ) )
                        throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_259"));

                    this.scrollbar = value;
    		    	this.NotifyPropChange( PropertyIds.Scrollbar, null );
                }
            }
        }

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeScrollbar() 
        {
            return ( this.scrollbar != Scrollbar.Default );			
        }
 
		/// <summary>
		/// Resets Scrollbar to its default value.
		/// </summary>
        public void ResetScrollbar() 
        {
            this.Scrollbar = Scrollbar.Default;
        }

		/// <summary>
		/// Returns or sets a value that indicates whether the user can resize two adjacent scrolling regions with the splitter bar. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">When this property is set for a colscrollregion, it either frees or restricts the splitter bar between that colscrollregion and the one to its right, unless the current colscrollregion is the rightmost region, in which case the splitter bar between that colscrollregion and the one to its right is affected.</p>
		/// <p class="body">When a colscrollregion is sized, the <b>BeforeColRegionSize</b> event is generated.</p>
		/// <p class="body">When this property is set for a rowscrollregion, it either frees or restricts the splitter bar between that rowscrollregion and the one beneath it, unless the current rowscrollregion is the bottommost region, in which case the splitter bar between that rowscrollregion and the one above it is affected.</p>
		/// <p class="body">When a rowscrollregion is sized, the <b>BeforeRowRegionSize</b> event is generated.</p>
		/// </remarks>
    	public SizingMode SizingMode
        {
            get
            {
                return this.sizingMode;
            }

            set
            {
                if ( value != this.sizingMode )
                {
                    // test that the value is in range
                    //
                    if ( !Enum.IsDefined( typeof(SizingMode), value ) )
                        throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_260"));

                    this.sizingMode = value;
    		    	this.NotifyPropChange( PropertyIds.SizingMode, null );
                }
            }
        }

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeSizingMode() 
        {
            return ( this.sizingMode != SizingMode.Free );			
        }
 
		/// <summary>
		/// Resets SizingMode to its default value (Free).
		/// </summary>
        public void ResetSizingMode() 
        {
            this.SizingMode = SizingMode.Free;
        }

		/// <summary>
		/// Abstract function that is overridden in ColScrollRegion 
		/// and RowScrollRegion to provide proper code for resetting
		/// the scroll information and scrollbar state
		/// </summary>
        protected abstract void ResetScrollInfo();

        /// <summary>
        /// Virtual method that is called from PositionScrollbar to 
        /// show/hide the scrollbar
        /// </summary>
        /// <param name="resetScrollInfo">true to reset the scroll info.</param>
        /// <param name="show">true to show the scrollbar. false to hide it.</param>        
        protected void ShowScrollbar( bool show, bool resetScrollInfo )
        {
            if ( show )
				// JM 01-17-02
                //this.scrollBarInfo.Show();
				this.ScrollBarInfo.Show();
            else
				// JM 01-17-02
				//this.scrollBarInfo.Hide();
				this.ScrollBarInfo.Hide();

            // Moved reset after the show window above since the scrollbar would
            // not appear disabled (appropriately if scrolling wasn't required)
            // when it first came up otherwise. 
            //
            if ( resetScrollInfo )
                this.ResetScrollInfo();
        }

		/// <summary>
		/// Returns the resolved scrollbar enumerator
		/// </summary>
		protected Infragistics.Win.UltraWinGrid.Scrollbar ScrollbarResolved
		{
			get 
			{
				if ( this.IsClone )				
				{
					return this.ClonedFrom.ScrollbarResolved;
				}
				Infragistics.Win.UltraWinGrid.Scrollbar enumScrollbar;
				
				// If this.Scrollbar is set to default then get the
				// value from the layout's scrollbars property
				//
				if (Infragistics.Win.UltraWinGrid.Scrollbar.Default == this.Scrollbar )
				{
					// JJD 1/23/02 - UWG497
					// Use the new ScrollbarsResolved property which has special logic
					// for dropdowns.
					//
				    switch ( this.Layout.ScrollbarsResolved )
				    {
						case Scrollbars.None:
							enumScrollbar = Infragistics.Win.UltraWinGrid.Scrollbar.Hide;
							break;
					    
						case Scrollbars.Vertical:

							// JJD 10/02/01 - UWG416
							// Resolve to show/hide based on whether this is a 
							// vertical or horizontal scroll region
							//
							if ( this.IsVerticalScrollbar )
								enumScrollbar = Infragistics.Win.UltraWinGrid.Scrollbar.Show;
							else
								enumScrollbar = Infragistics.Win.UltraWinGrid.Scrollbar.Hide;
							break;
					    
						case Scrollbars.Horizontal:

							// JJD 10/02/01 - UWG416
							// Resolve to show/hide based on whether this is a 
							// vertical or horizontal scroll region
							//
							if ( this.IsVerticalScrollbar )
								enumScrollbar = Infragistics.Win.UltraWinGrid.Scrollbar.Hide;
							else
								enumScrollbar = Infragistics.Win.UltraWinGrid.Scrollbar.Show;
							break;
					    
						case Scrollbars.Both:
							enumScrollbar =  Infragistics.Win.UltraWinGrid.Scrollbar.Show;
							break;
					    
						case Scrollbars.Automatic:
							enumScrollbar = Infragistics.Win.UltraWinGrid.Scrollbar.ShowIfNeeded;
							break;				
					    
						default:
							Debug.Assert(false, "Invalid Layout m_enumScrollbars in ColScrollRegion.ResolvedScrollbar");						
							enumScrollbar = Infragistics.Win.UltraWinGrid.Scrollbar.Show;
							break;
				    }
				}
				else
				{
				    enumScrollbar = this.Scrollbar;
				}

				return enumScrollbar;
			}		
		}

		internal Infragistics.Win.UltraWinGrid.HeadersCollection ExclusiveItems
		{			
			get
			{
				if ( null == this.exclusiveItems )
				{
					this.exclusiveItems = new HeadersCollection(0);
				}
				return this.exclusiveItems;
			}
		}

		internal abstract void CheckIfSizeChanged();

		/// <summary>
		/// Returns true if this is an exclusive region
		/// </summary>
		internal bool HasExclusiveItems
		{
			get
			{
				return ( null != this.exclusiveItems &&
						 this.exclusiveItems.Count > 0 );
			}
		}

		internal bool WillScrollbarBeShown( )
		{
			return this.WillScrollbarBeShown( ScrollbarVisibility.Check );
		}

		// SSP 11/4/04 UWG3593
		// Changed the parameter for assumeColScrollbarsVisible from boolean to an enum
		// that specifies whether to assume and if so to whether to assume the scrollbar
		// is hidden or visible. This is to fix UWG3593 where the scrollbars are displayed 
		// even when they are not needed because we assume the horizontal scrollbar is 
		// visible to find out if vertical scrollbar should be displayed or not.
		//
		//internal abstract bool WillScrollbarBeShown ( bool assumeRowScrollbarsVisible );
		internal abstract bool WillScrollbarBeShown( ScrollbarVisibility assumeRowScrollbarsVisible );

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// returns true if the scrollbar is to be shown in this region
		//        /// determined from Scrollbar property settings
		//        /// </summary>
		//#endif
		//        internal bool CanScrollbarBeShown 
		//        {
		//            get
		//            {								

		//                Infragistics.Win.UltraWinGrid.Scrollbar enumScrollbar = this.ScrollbarResolved;

		//                bool fShow;

		//                switch( enumScrollbar )
		//                {
		//                case Infragistics.Win.UltraWinGrid.Scrollbar.Show:
		//                case Infragistics.Win.UltraWinGrid.Scrollbar.ShowIfNeeded:
		//                    fShow = true;
		//                    break;

		//                case Infragistics.Win.UltraWinGrid.Scrollbar.Hide:
		//                    fShow = false;
		//                    break;

		//                default:
		//                    Debug.Assert(false, "Invalid m_enumScrollbar in ScrollRegionBase::CanScrollbarBeShown");
		//                    fShow = true;
		//                    break;
		//                }

		//                return fShow;
		//            }
		//        }

		#endregion Not Used

		internal abstract void PositionScrollbar( bool resetScrollInfo );

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal int CachedScrollOrigin
		//{
		//    get 
		//    {
		//        // AS 1/8/03 fxcop
		//        // Cannot have 2 public/protected members that differ only by case
		//        //return this.cachedScrollOrigin - SCROLL_ORIGIN_BASE; 
		//        return this.cachedScrollOriginValue - SCROLL_ORIGIN_BASE; 
		//    }
		//}

		//internal int BrushOrigin
		//{ 
		//    get
		//    {
		//        // AS 1/8/03 fxcop
		//        // Cannot have 2 public/protected members that differ only by case
		//        //return this.cachedScrollOrigin % 8; 
		//        return this.cachedScrollOriginValue % 8; 
		//    }
		//}

		#endregion Not Used

        internal int PenOrigin
        {
            get
            {
				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return this.cachedScrollOrigin % 2;
				return this.cachedScrollOriginValue % 2;
            }
        }

	}

}
