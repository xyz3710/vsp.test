#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Collections;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{
	#region ProcessRowParams Class

	/// <summary>
	/// ProcessRowParams class.
	/// </summary>
	/// <remarks>
	/// <p class="body"><seealso cref="IUltraGridExporter"/></p>
	/// </remarks>
	public class ProcessRowParams
	{
		#region Private Variables

		private bool skipDescendats;
		private bool skipSiblings;
		private bool terminateExport;

		#endregion // Private Variables

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		public ProcessRowParams( )
		{
			this.Reset( );
		}

		#endregion // Constructorb

		#region SkipDescendants

		/// <summary>
		/// Specifies whether to skip the descendats of the current row.
		/// </summary>
		public bool SkipDescendants
		{
			get
			{
				return this.skipDescendats;
			}
			set
			{
				this.skipDescendats = value;
			}
		}

		#endregion // SkipDescendants

		#region SkipSiblings

		/// <summary>
		/// Specifies whether to skip sibling rows of the current row.
		/// </summary>
		public bool SkipSiblings
		{
			get
			{
				return this.skipSiblings;
			}
			set
			{
				this.skipSiblings = value;
			}
		}

		#endregion // SkipSiblings

		#region TerminateExport

		/// <summary>
		/// Specifies whether to terminate the export process. Current row will not be processed.
		/// </summary>
		public bool TerminateExport
		{
			get
			{
				return this.terminateExport;
			}
			set
			{
				this.terminateExport = value;
			}
		}

		#endregion // TerminateExport

		#region Reset

		internal void Reset( )
		{
			this.SkipDescendants = false;
			this.SkipSiblings = false;
			this.TerminateExport = false;
		}

		#endregion // Reset
	}

	#endregion // ProcessRowParams Class

	#region IUltraGridExporter Interface

	/// <summary>
	/// IUltraGridExporter interface.
	/// </summary>
	public interface IUltraGridExporter
	{
		/// <summary>
		/// Begins the exporting process.
		/// </summary>
		/// <param name="exportLayout">The layout to use during the export of the grid. This is not the layout being used by the on-screen grid.</param>
		/// <param name="rows">The root rows collection of the grid before exported.</param>
		void BeginExport( UltraGridLayout exportLayout, RowsCollection rows );

		/// <summary>
		/// Exports the passed in row.
		/// </summary>
		/// <param name="row">The row to process.</param>
		/// <param name="processRowParams">Params that allow modifying the export process.</param>
		void ProcessRow( UltraGridRow row, ProcessRowParams processRowParams );


		/// <summary>
		/// Called after all the rows have been processed. If the exporting process was canceled 
		/// (by setting TerminateExport of the ProcessRowParams in a ProcessRow call), then canceled 
		/// parameter will be true.
		/// </summary>
		/// <param name="canceled">true if the export process was cancelled.</param>
		void EndExport( bool canceled );
	}

	#endregion // IUltraGridExporter Interface
}
