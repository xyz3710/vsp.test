#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Summary description for PageFooterUIElement.
	/// </summary>
	public class PageFooterUIElement : UIElement
	{
		private TextUIElement leftTextElement = null;
		private TextUIElement rightTextElement = null;
		private TextUIElement centerTextElement = null;

//#if DEBUG
//		/// <summary>
//		/// Constructor
//		/// </summary>
//		/// <param name="parent">The parent element</param>
//#endif
		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>PageFooterUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		//internal PageFooterUIElement( UIElement parent ) : base( parent )
		public PageFooterUIElement( UIElement parent ) : base( parent )
		{
		}

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{							
				return ((UltraGrid)this.Control).PrintManager.CurrentPrintingLogicalPageInfo.LogicalPageLayoutInfo.PageFooterBorderStyle;
			}
		}



        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			UltraGrid grid = this.Grid;
			if ( grid != null && grid.PrintManager != null )
				grid.PrintManager.InternalResolveFooterAppearance( ref appearance, ref requestedProps );
		}

		internal UltraGrid Grid 
		{
			get
			{
				return ((UltraGridUIElement)this.Parent).Grid as UltraGrid;
			}
		}

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			// AS 1/9/03
			// What we were doing doesn't make sense.
			// First, we asked for the ChildElements collection and
			//	then we end up clearing the very same collection we got back
			//	making it impossible to reuse the elements.
			// Second, we have TextUIElement member variables that but they
			//	end up getting set to null because we set them to the results
			//	of calling ExtractExistingElement which is going to return null
			//	because the collection was cleared.
			// Third, we initialize an appearance data but never use it?!?
			// Lastly, we ask for the RectInsideBorders up to 3 times,
			//	which means it gets recalculated up to 3 times. Just ask
			//	for it once.
			// Since we don't want to create a new collection every time
			//	we reuse this element, I created properties to handle
			//	the creation of the elements for the member variables that
			//	the class already had and simply clear the collection when we start.
			//

			//grab childElements and clear
			//
			// AS 1/9/03 - see above
			// First clear the collection
			//Infragistics.Win.UIElementsCollection oldElements = this.ChildElements;			
			//this.childElementsCollection.Clear();
			this.ChildElements.Clear();

			if ( this.Grid == null )
			{
				System.Diagnostics.Debug.Fail("Grid null in PageFooterUIElement.PositionChildElements");
				return;
			}

			string pageFooter = this.Grid.PrintManager.CurrentPrintingLogicalPageInfo.LogicalPageLayoutInfo.PageFooter;
			string pageNum = this.Grid.PrintManager.CurrentPrintingPageNumber.ToString();

			// MD 1/12/09 - Printing total number of pages
			string totalPages = this.Grid.PrintManager.TotalNumberOfPages.ToString();

			//if we have a footer
			//
			if ( pageFooter != null )
			{
				// AS 1/9/03 - see above
				Rectangle workRect = this.RectInsideBorders;

				//Replace with page number
				//
				pageFooter = pageFooter.Replace( "<#>",pageNum );

				// MD 1/12/09 - Printing total number of pages
				pageFooter = pageFooter.Replace( "<##>", totalPages );
			
				//Chop footer up
				//
				string [] footerTextSection = pageFooter.Split('\t');
				int count = footerTextSection.Length;		
				

				if ( footerTextSection != null )
				{
					// AS 1/9/03 - see above
					// We weren't using the appearance data so we'll
					// pull it for now.
					//AppearanceData appData = new AppearanceData();
					//AppearancePropFlags propFlags = AppearancePropFlags.TextHAlign | AppearancePropFlags.TextVAlign;

					//try to get old element
					//
					
					this.LeftTextElement.Text = footerTextSection[0];

					//set rect, and alignment
					//
					// AS 1/9/03 - see above
					//this.leftTextElement.Rect = this.RectInsideBorders;
					this.LeftTextElement.Rect = workRect;

					// AS 1/9/03 - see above
					// We weren't using the appearance data so we'll
					// pull it for now.
					//this.InitAppearance( ref appData, ref propFlags);

					if ( count != 1 )
						//{
						// JJD 11/07/01
						// TextAlignment property has been removed
						//
						//this.leftTextElement.TextAlignment = Infragistics.Win.AppearanceData.ContentAlignmentFromHVAlign( appData.TextHAlign, appData.TextVAlign );				
						//}
						//else
					{
						// JJD 11/07/01
						// TextAlignment property has been removed
						//
						//this.leftTextElement.TextAlignment = Infragistics.Win.AppearanceData.ContentAlignmentFromHVAlign( HAlign.Left, appData.TextVAlign );				
						this.leftTextElement.TextHAlign = HAlign.Left;


						//try to get old element
						//
						
						this.CenterTextElement.Text = footerTextSection[1];

						//set rect, and alignment
						//
						// AS 1/9/03 - see above
						//this.centerTextElement.Rect = this.RectInsideBorders;
						this.CenterTextElement.Rect = workRect;

						// JJD 11/07/01
						// TextAlignment property has been removed
						//
						//this.centerTextElement.TextAlignment = Infragistics.Win.AppearanceData.ContentAlignmentFromHVAlign( HAlign.Center, appData.TextVAlign );				
						this.centerTextElement.TextHAlign = HAlign.Center;				
						
						//Get the right aligned text if not null
						//
						if ( count >= 3 )
						{
							//try to get old element
							//
							
							this.RightTextElement.Text = footerTextSection[2];

							//set rect, and alignment
							//
							// AS 1/9/03 - see above
							//this.rightTextElement.Rect = this.RectInsideBorders;
							this.RightTextElement.Rect = workRect;

							// JJD 11/07/01
							// TextAlignment property has been removed
							//
							//this.rightTextElement.TextAlignment =Infragistics.Win.AppearanceData.ContentAlignmentFromHVAlign( HAlign.Right, appData.TextVAlign );
							this.rightTextElement.TextHAlign = HAlign.Right;
							
							//Add element
							//
							this.ChildElements.Add( this.rightTextElement );
						}

						//Add element
						//
						this.ChildElements.Add( this.centerTextElement );
					}
					
					//Add element
					//
					this.ChildElements.Add( this.leftTextElement );				
				}				
					
			}
			
		}

		// AS 1/9/03
		// Added properties to wrap the textuielement member
		// properties.
		#region TextUIElement properties
		private TextUIElement LeftTextElement
		{
			get 
			{
				if (this.leftTextElement == null)
				{
					this.leftTextElement = new TextUIElement(this, string.Empty);

					// SSP 8/11/03 UWG2196
					//
					this.leftTextElement.MultiLine = true;
				}

				return this.leftTextElement;
			}
		}

		private TextUIElement RightTextElement
		{
			get 
			{
				if (this.rightTextElement == null)
				{
					this.rightTextElement = new TextUIElement(this, string.Empty);

					// SSP 8/11/03 UWG2196
					//
					this.rightTextElement.MultiLine = true;
				}

				return this.rightTextElement;
			}
		}
		
		private TextUIElement CenterTextElement
		{
			get 
			{
				if (this.centerTextElement == null)
				{
					this.centerTextElement = new TextUIElement(this, string.Empty);

					// SSP 8/11/03 UWG2196
					//
					this.centerTextElement.MultiLine = true;
				}

				return this.centerTextElement;
			}
		}
		#endregion //TextUIElement properties

		#region DrawImage

		// SSP 9/11/03 UWG2631
		// Overrode DrawImage so that we can draw the image in the page header and the footer.
		//
		/// <summary>
		/// Default image rendering - does nothing
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImage( ref UIElementDrawParams drawParams )
		{
			AppearancePropFlags flags = AppearancePropFlags.Image | AppearancePropFlags.ImageAlpha |
				AppearancePropFlags.ImageHAlign | AppearancePropFlags.ImageVAlign;

			AppearanceData appData = new AppearanceData( );

			this.InitAppearance( ref appData, ref flags );

			PageHeaderUIElement.DrawImageHelper( this, ref drawParams, ref appData, this.Grid );
		}

		#endregion DrawImage

		// SSP 8/12/02 UWG1191
		// Overrode DrawImageBackground for printing. When the ImageBackgroundOrigin
		// is set to form or container, when printing, we shouldn't use the grid's
		// or grid's form's dimensions because for printing it doesn't make sense.
		//
		/// <summary>
		/// Default background picture drawing
		/// </summary>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
			// Also call the base class for ImageBackgroundOrigins othe than Form and Container
			// since the base class will do the right thing for them as well.
			//
			if ( ImageBackgroundOrigin.Form != drawParams.AppearanceData.ImageBackgroundOrigin &&
				ImageBackgroundOrigin.Container != drawParams.AppearanceData.ImageBackgroundOrigin &&
				// SSP 10/9/03 UWG1191
				// Added below conditions. Skip Client as well.
				// 
				ImageBackgroundOrigin.Client != drawParams.AppearanceData.ImageBackgroundOrigin )
			{
				base.DrawImageBackground( ref drawParams );
				return;
			}

			if( drawParams.AppearanceData.ImageBackground  == null ) 
				return; 

			// get the rect inside the borders
			//
			Rectangle rectInsideBorders = this.RectInsideBorders;

			if ( !rectInsideBorders.IntersectsWith( drawParams.InvalidRect ) )
				return;
            
			// Get the rect inside the borders (which is where the background image
			// is drawn
			//
			Rectangle rectClip = rectInsideBorders;

			//Determine bounding rect based on imageBackground origin prop
            
			Rectangle rectBounds = new Rectangle(0,0,0,0);

			// For printing, its always the 
			switch(drawParams.AppearanceData.ImageBackgroundOrigin)
			{
				case ImageBackgroundOrigin.Form:
				case ImageBackgroundOrigin.Container:
					// SSP 10/9/03 UWG1191
					// Added below case for Client.
					// 
				case ImageBackgroundOrigin.Client:
				{
					rectBounds = rectInsideBorders;

					// Use the grid elements rect
					//
					UIElement elem = this.GetAncestor( typeof( UltraGridUIElement ) );

					if ( null != elem )
						rectBounds = elem.RectInsideBorders;

					break;
				}					
			}

			// JJD 8/2/01 - uWG62 and 65
			// Save the existing clipping region so that it can be
			// restored after the image drawing
			//
			// existingClipRegion is used to store the currrent clipping 
			// region so that we can restore it afterward
			//
			Region existingClipRegion = drawParams.Graphics.Clip;

			//set clipping rect
			System.Drawing.Region rgnInsideBorder = new System.Drawing.Region(rectClip);

			drawParams.Graphics.IntersectClip( rgnInsideBorder );
			
			rgnInsideBorder.Dispose();

			//Now that we have our bounding rect and clipping rect go ahead and draw 
			
			switch( drawParams.AppearanceData.ImageBackgroundStyle)
			{
				case ImageBackgroundStyle.Tiled:
				{

					//Tiling
					Rectangle imageRect = new Rectangle(0, 0, drawParams.AppearanceData.ImageBackground.Width, drawParams.AppearanceData.ImageBackground.Height);

					// use a nested loop to tile the image
					//
					for( int y = rectBounds.Top; y < rectBounds.Bottom; y += imageRect.Height )
					{
						// once we pass the invalid are we can break out of the loop
						//
						if ( y > rectClip.Bottom )
							break;

						// if we are into the invalid area then tile left to right
						//
						if ( y + imageRect.Height >= rectClip.Top )
						{
							for(int x = rectBounds.Left; x < rectBounds.Right; x += imageRect.Width )
							{
								// once we pass the invalid are we can break out of the loop
								//
								if ( x > rectClip.Right )
									break;

								// if we are in the invalid area then draw the image
								//
								if ( x + imageRect.Width >= rectClip.Left )
								{
									drawParams.Graphics.DrawImage( drawParams.AppearanceData.ImageBackground,
										x,
										y,
										imageRect.Width,
										imageRect.Height );
								}
							}
						}
					}
					break;
				}

				case ImageBackgroundStyle.Centered:
				{
					Rectangle imageRect = new Rectangle(0, 0, drawParams.AppearanceData.ImageBackground.Width, drawParams.AppearanceData.ImageBackground.Height);
					// center the image vertically and horizontally
					//
					DrawUtility.AdjustHAlign( HAlign.Center, ref imageRect, rectBounds  ); 
					DrawUtility.AdjustVAlign( VAlign.Middle, ref imageRect, rectBounds  ); 
					
					if ( imageRect.IntersectsWith( drawParams.InvalidRect ) )
						drawParams.Graphics.DrawImage(drawParams.AppearanceData.ImageBackground,imageRect);
									
					break;
				}

				case ImageBackgroundStyle.Stretched:
					//drawParams.Draw.Graphics.DrawImage(drawParams.AppearanceData.ImageBackground,rectClip);
					drawParams.Graphics.DrawImage(drawParams.AppearanceData.ImageBackground,rectBounds);
					
					break;

				default:
					//drawParams.Draw.Graphics.DrawImage(drawParams.AppearanceData.ImageBackground,rectClip);
					drawParams.Graphics.DrawImage(drawParams.AppearanceData.ImageBackground,rectBounds);
					break;
			}

			
			// JJD 8/2/01 - uWG62 and 65
			// Restore the original clipping region 
			//
			if ( null != existingClipRegion )
			{
				drawParams.Graphics.Clip = existingClipRegion;
				existingClipRegion.Dispose();
			}

		}	

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				UltraGrid grid = this.Grid;

				return null != grid && null != grid.PrintLayout
					? StyleUtils.GetRole( grid.PrintLayout, StyleUtils.Role.PageFooter )
					: null;
			}
		}

		#endregion // UIRole
	}
}
