#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;

	internal class UnBoundData
	{
		private object current;
		private object original;

		internal UnBoundData()
		{
			Clear();
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		///// <summary>
		///// Called when the object is disposed of
		///// </summary>
		//protected void OnDispose()
		//{
		//    Clear();
		//}

		#endregion Not Used


		internal void Clear()
		{
			current = null;
			original = null;
		}


		internal object Current
		{
			get { return current; }
			set 
			{	
				// SSP 6/30/03 UWG2300
				// What's the meaning of this !
				// Commented out the original code and added the new code. We want to set the Original
				// to the old value so the Cell.OriginValue property returns the original value.
				//
				
				original = current;
				current = value;
			}
		}


		internal object Original
		{
			get	{ return original; }
			set 
			{
				// SSP 7/16/03 - Optimzations
				// 
				
				this.original = value;
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal void Reset(object Value, object Original)
		//{
		//    this.Current = Value;
		//    this.Original = Original;
		//}

		#endregion Not Used


		internal void RestoreOriginal()
		{
			object OrigValue = Original;
			this.Current = OrigValue;
		}


		internal void SaveOriginal()
		{
			if ( current != null )
			this.Original  = current;
		}

	}
}
