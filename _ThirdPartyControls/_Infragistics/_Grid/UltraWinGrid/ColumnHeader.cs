#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Collections;
	using System.Security.Permissions;
    using System.Collections.Generic;
    using Infragistics.Shared.Serialization;


	/// <summary>
	///	The class represents the header for a specific column
	/// </summary>


	[Serializable()]
	public sealed class ColumnHeader : HeaderBase, ISerializable
	{
		private Infragistics.Win.UltraWinGrid.UltraGridColumn column = null;



		internal ColumnHeader( Infragistics.Win.UltraWinGrid.UltraGridColumn column )
		{
			this.column = column;

			this.visiblePos = -1;
			
			if ( null != this.column )
			{
				this.column.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}
		}

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        // AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//public ColumnHeader( SerializationInfo info, StreamingContext context )
		private ColumnHeader( SerializationInfo info, StreamingContext context )
		{
			this.visiblePos = -1;

			base.DeserializeHelper( info, context );

            // CDS NAS v9.1 Header CheckBox
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case "CheckBoxVisibility":
                        this.checkBoxVisibility = (HeaderCheckBoxVisibility)Utils.DeserializeProperty(entry, typeof(HeaderCheckBoxVisibility), this.checkBoxVisibility);
                        break;

                    case "CheckBoxAlignment":
                        this.checkBoxAlignment = (HeaderCheckBoxAlignment)Utils.DeserializeProperty(entry, typeof(HeaderCheckBoxAlignment), this.checkBoxAlignment);
                        break;

                    case "CheckBoxSynchronization":
                        this.checkBoxSynchronization = (HeaderCheckBoxSynchronization)Utils.DeserializeProperty(entry, typeof(HeaderCheckBoxSynchronization), this.checkBoxSynchronization);
                        break;
                }
            }
		}
    
		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{

			// JJD 10/22/01
			// Only forward the notification if it wasn't reflected
			// from the column.
			//
			if ( propChange.Source == this.column )
			{
				if ( propChange.PropId is Infragistics.Win.UltraWinGrid.PropertyIds )
				{
					if ( (Infragistics.Win.UltraWinGrid.PropertyIds)propChange.PropId == Infragistics.Win.UltraWinGrid.PropertyIds.Header )
						return;
				}
			
				this.NotifyPropChange( PropertyIds.Header, propChange );

				return;
			}
		
			base.OnSubObjectPropChanged( propChange );
		}

		// SSP 1/12/05 BR0155 UWG3736
		// Overrode OnDispose because we need to unselect the header when it's being disposed.
		//
		/// <summary>
		/// Called when the object is disposed of.
		/// </summary>
		protected override void OnDispose( )
		{			
			Infragistics.Win.UltraWinGrid.Selected.RemoveItemFromCollectionHelper( this );

			base.OnDispose( );
		}

		internal override string DefaultHeaderCaption 
		{ 
			get 
			{
				if ( this.column == null )
					return string.Empty;

				// SSP 1/11/05
				// Added support for defaulting the Caption to the DataColumn's Caption and then
				// to the PropertyDescriptor's DisplayName. Now the order will be 
				// ColumnHeader.Caption, DataColumn.Caption, PropertyDescriptor.DisplayName and
				// then the PropertyDescriptor.Name.
				//
				//return this.column.Key;
				System.Data.DataColumn dataColumn = this.column.GetDataColumn( );
				string caption = null != dataColumn ? dataColumn.Caption : null;
				if ( null != caption )					
					return caption;

				PropertyDescriptor pd = this.column.PropertyDescriptor;
				caption = null != pd ? pd.DisplayName : null;
				if ( null != caption )
					return caption;

				return this.column.Key;
			}
		}

		/// <summary>
		/// The header owner's band object. This property
		/// is read-only.
		/// </summary>
		[Browsable(false)]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override Infragistics.Win.UltraWinGrid.UltraGridBand Band 
		{
			get
			{
				return this.column != null ? this.column.Band : null;
			}
		}

		/// <summary>
		/// Property: Returns true only if Draggable
		/// </summary>
		internal protected override bool Draggable
		{ 
			get 
			{ 
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.Band == null || this.Column == null )
				UltraGridColumn column = this.Column;

				if ( this.Band == null || column == null )
					return false;

				// MRS 4/8/05 - Rowlayout column moving. 
				if (this.Band.UseRowLayoutResolved)
				{				
					if (Infragistics.Win.Layout.GridBagLayoutAllowMoving.None != this.Band.AllowRowLayoutColMovingResolved)
						return true;
				}
				// MRS 4/21/05 - BR03480
				// Added an else here. We should only do this if not in RowLayout mode. 
				else
					if ( AllowColMoving.NotAllowed != this.Band.AllowColMovingResolved )
						return true;

				if ( this.Band.Layout == null ||
					this.Band.Layout.Grid == null )
					return false;
						 				
				// Only return false if not in design mode.
				//
				// SSP 8/6/04 UWG3378
				// Don't allow dragging if the band is using row layout functionality.
				//
				//if ( this.Band.Layout.Grid.DesignMode )
				// MRS 4/8/05 - Rowlayout column moving. 
//				if ( this.Band.Layout.Grid.DesignMode && ! this.Band.UseRowLayoutResolved ) 
//					return true;
				if ( this.Band.Layout.Grid.DesignMode)
					return true;
				
				// SSP 10/8/01 UWG505
				// Added this code to allow for dragging so that
				// the user can drag and drop the column header in the group by box
				//
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//UIElement element = this.Column.Layout.UIElement;
				UIElement element = column.Layout.UIElement;

				if ( null != element )
					element = element.GetDescendant( typeof( GroupByBoxUIElement ) );
                
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( null != element && DefaultableBoolean.True == this.Column.AllowGroupByResolved )
				if ( null != element && DefaultableBoolean.True == column.AllowGroupByResolved )
					return true;

				return false;
			} 
		}

		
 
		internal void InitColumn( Infragistics.Win.UltraWinGrid.UltraGridColumn column )
		{
			this.column = column;

			base.InitAppearanceHolder();
		}



		internal override bool PivotOnRowScrollRegion { get { return false; } }
		internal override bool PivotOnColScrollRegion { get { return true; } }
   
		/// <summary>
		/// Returns the UltraGridGroup object that the object is associated with.  This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Group</b> property of an object refers to a specific group of columns in the grid as defined by an UltraGridGroup object. You use the <b>Group</b> property to access the properties of a specified Group object, or to return a reference to a Group object. A Group is a group of columns that appear together in the grid, and can be resized, moved or swapped together as a unit. Columns in the same group share a group header, and can be arranged into a multi-row layout within the group, with different columns occupying different vertical levels within a single row of data. Groups also help with the logical arrangement of columns within the grid.</p>
		/// </remarks>
		[Browsable(false)]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override Infragistics.Win.UltraWinGrid.UltraGridGroup Group
		{
			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
			//get { return this.column == null ? null : this.column.Group; }
			get { return this.column == null ? null : this.column.GroupResolved; }
		}

		internal override int Extent
		{
			get
			{
				if ( this.column == null )
					return 0;

				// SSP 2/25/03 - Row Layout Functionality
				//
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( null != this.Column && 
				//    null != this.Column.Band && 
				//    this.Column.Band.UseRowLayoutResolved )
				//{
				//    return this.Column.RowLayoutColumnInfo.CachedHeaderRect.Width;
				//}
				UltraGridColumn column = this.Column;

				if ( null != column &&
					null != column.Band &&
					column.Band.UseRowLayoutResolved )
				{
					return column.RowLayoutColumnInfo.CachedHeaderRect.Width;
				}

				return this.column.Extent + this.RowSelectorExtent;
			}
		}

		internal override int RowSelectorExtent
		{
			get
			{ 
				if ( this.FirstItem )
				{
					// return the row selector extent if this column is
					// the first column in the first group
					//
					// MD 1/21/09 - Groups in RowLayouts
					// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
					//if ( this.column.Group == null ||
					//    this.column.Group.FirstItem )
					if ( this.column.GroupResolved == null ||
						this.column.GroupResolved.FirstItem )
						return this.Band.RowSelectorExtent;				
				}

				return 0;
			}
		}


		internal override int GetActualWidth( bool ignoreDraggingWidth )
		{			
			return this.Column.GetActualWidth( ignoreDraggingWidth );
		}

		


		/// <summary>
		/// Returns the height of the column header.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Height</b> property is used to determine the vertical dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">For a Header object, this property is read-only. In a particular band, each column header has the same height. This height is determined by taking the largest height that results from the resolution of each column's header's <b>Appearance</b> attributes and the band's <b>ColHeaderLines</b> property.</p>
		/// <p class="body">If you are using the <b>Row-Layout</b> functionality then then height of the header can be controlled using the <see cref="RowLayoutColumnInfo.PreferredLabelSize"/> property.</p>
		/// <p class="body">If you want to auto-wrap the header caption if the width is not sufficient to fully display the caption then set the <see cref="UltraGridOverride.WrapHeaderText"/> property.</p>
		/// </remarks>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override int Height
		{
			get
			{
				// SSP 2/21/03 - Row Layout Functionality.
				//
				// --------------------------------------------------------------------
				if ( this.Band.UseRowLayoutResolved )
				{
					// SSP 7/21/03 Workaround for UWG1558
					// Added an overload of GetColumnHeaderHeight that ignores whether the header is
					// visible or not and returns a valid height. By default, it returns 0 if the
					// header is hidden but we want it to return a valid value.
					// Made GroupHeaderHeight into a method that takes in a ignoreHidden parameter.
					//
					//return this.Band.GetColumnHeaderHeight( true, true );
					// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
					// GetColumnHeaderHeight returns the max of heights of all column headers. We 
					// need to return the ideal height of this column header. Use column's 
					// CardLabelSize instead of band's GetColumnHeaderHeight method. Also to 
					// maintain old behavior where the GetColumnHeaderHeight adds extra 4 pixels
					// to the height, do the same here (that's what the
					// EXTRA_HEADER_HEIGHT_PADDING is for). This extra pixels also fix BR06209.
					// 
					//return this.Band.GetColumnHeaderHeight( true, true, true );
					return this.Column.CardLabelSize.Height + UltraGridBand.EXTRA_HEADER_HEIGHT_PADDING;
				}
				// --------------------------------------------------------------------

				// SSP 7/21/03 Workaround for UWG1558
				// Added an overload of GetBandHeaderHeight that ignores whether the header is
				// visible or not and returns a valid height. By default, it returns 0 if the
				// header is hidden but we want it to return a valid value.
				//
				//int headerHeight   = this.Band.GetColumnHeaderHeight( );
				int headerHeight   = this.Band.GetColumnHeaderHeightIgnoreHidden( );
				int displayLevels  = this.Band.ActualColDisplayLevels;

				if ( displayLevels > 1 )
				{
					int levelHeaderHeight = headerHeight / displayLevels;

					// MD 1/21/09 - Groups in RowLayouts
					// Use LevelResolved because it checks the band's row layout style
					//if ( this.Column.Level >= displayLevels - 1 )
					if ( this.Column.LevelResolved >= displayLevels - 1 )
						headerHeight -= levelHeaderHeight * ( displayLevels - 1 ) ;
					else
						headerHeight = levelHeaderHeight;
				}
    
				return headerHeight;
			}
		}



		internal override UltraGridColumn GetFirstVisibleCol( bool includeNonActivateable )
		{
            // MRS 2/23/2009 - Found while fixing TFS14354
			//return  this.column.Hidden 
            return this.column.HiddenResolved 
				? null 
				: includeNonActivateable || !this.IsDisabled 
				? this.column 
				: null;
		}
	
		internal override UltraGridColumn GetNextVisibleCol( UltraGridColumn column, bool includeNonActivateable )
		{
			return null;
		}
		
		internal override UltraGridColumn GetPrevVisibleCol( UltraGridColumn column, bool includeNonActivateable )
		{
			return null;
		}
		
		internal override UltraGridColumn GetLastVisibleCol( bool includeNonActivateable )
		{
			return this.GetFirstVisibleCol( includeNonActivateable );
		}
		
		internal override UltraGridColumn GetVisibleColAbove ( UltraGridColumn column, bool topMost, bool includeNonActivateable ) 
		{
			return (!topMost || this.Hidden) ? null : this.GetFirstVisibleCol( includeNonActivateable ); 
		}		 
		
		internal override UltraGridColumn GetVisibleColBelow ( UltraGridColumn column, bool bottomMost, bool includeNonActivateable ) 
		{
			return (!bottomMost || this.Hidden) ? null : this.GetFirstVisibleCol( includeNonActivateable );
		}		
		// MD 1/21/09 - Groups in RowLayouts
		// Use HiddenResolved because it checks the group's hidden if the column is in a group.
		//internal override bool Hidden { get { return this.column.Hidden; } }
		internal override bool Hidden 
		{ 
			get 
			{ 
				return this.column.HiddenResolved; 
			}
		}

		internal override void AdjustForCellLevel ( ref Rectangle rect, UltraGridRow row )
		{			
			// Save the original bottom of the rect
			//
			int origBottom  = rect.Bottom;
			int level       = this.column.GetDisplayLevel();
			
			int temp =  row.GetLevelOffset( level, true );
			rect.Y += temp;
			rect.Height -= temp;

			// make sure the bottom doesn't extend below its initial passed in
			// value
			//
			temp = System.Math.Min( origBottom, rect.Top + row.GetLevelHeight ( level ) );
			rect.Height = temp - rect.Top;
		}


		
		internal override bool AdjustForCellPicture ( ref Rectangle rect, ref Rectangle pictureRect, ref AppearanceData resolvedCellAppearance )
		{
			pictureRect = new Rectangle(0,0,0,0);	
			bool imageScaled = false;

			if ( null == resolvedCellAppearance.Image  )
				return false;

			System.Drawing.Image image = resolvedCellAppearance.GetImage( this.Band.Layout.Grid.ImageList );

			System.Drawing.Size sizImages = new System.Drawing.Size( 0,0 );		 	

			// If an actual picture object is passed in then do normal drawing, else use imagelist drawing
			if ( null != image )
			{
				sizImages = image.Size;
			}
			else
			{
				sizImages = this.Band.Layout.SizeOfImages;
			}

			if ( sizImages.Width < 1 || sizImages.Height < 1 )
				return false;

			// shrink the image size if necessary
			//
			if ( rect.Height > 2 && sizImages.Height > rect.Height - 2 )
			{
				//set flag
				//
				imageScaled = true;

				// proportinally shrink the width
				//
				sizImages.Width  = (int)((double)sizImages.Width * ( (double)(rect.Height - 2) / (double)sizImages.Height ));

				// shrink the height
				//
				sizImages.Height = rect.Height - 2;				
			}

			// make sure the width isn't too big
			//
			if ( rect.Width > 2 && sizImages.Width > rect.Width - 2 )
				sizImages.Width = rect.Width - 2;

			pictureRect = rect;
			pictureRect.Width = sizImages.Width;
			pictureRect.Height = sizImages.Height;


			//move image based on alignment
			DrawUtility.AdjustHAlign( resolvedCellAppearance.ImageHAlign, ref pictureRect, rect ); 
			DrawUtility.AdjustVAlign( resolvedCellAppearance.ImageVAlign, ref pictureRect, rect );

			// make sure the image has at least a 1 pixel 
			// padding on top and bottom
			//
			if ( pictureRect.Top == rect.Top )
				pictureRect.Y++;
			else
				if ( pictureRect.Bottom == rect.Bottom )
				pictureRect.Y--;


			// adjust the text rect appropriately
			//
			switch ( resolvedCellAppearance.ImageHAlign )
			{
				case HAlign.Center:
					break;

				case HAlign.Right:
					pictureRect.X--;
					rect.Width -= pictureRect.Width + 1;
					break;

				default:
				case HAlign.Left:
					pictureRect.X++;
					rect.Width	-= pictureRect.Width + 1;
					rect.X		+= pictureRect.Width + 1;
					break;
			}

			// return imageScaled so that the element can set it's scaled property
			//
			return imageScaled;


			// RobA UWG823 12/3/01
			// Commented code out below and added code above to 
			// shrink the image and position the text and image
			//
			// first align the rect left and right
			//
			
		}

		internal override bool AllowSizing
		{
			get
			{
				if ( !this.Enabled  )
					return false;
				
				Infragistics.Win.UltraWinGrid.AllowColSizing allowSizing = this.Band.AllowColSizingResolved;
    
				Debug.Assert( Infragistics.Win.UltraWinGrid.AllowColSizing.Default != allowSizing, "AllowColSizing set to Default!");
    
				//	Ignore LockedWidth property @ design time
				if( this.Band.Layout.Grid.DesignMode )
					return true;
				else
					//
					// Check the LockedWidth property to see if sizing is allowed.
					//
					return ( allowSizing != Infragistics.Win.UltraWinGrid.AllowColSizing.None && ! this.column.LockedWidth );
			}			
		}

		// SSP 5/31/02
		// Added code for summary rows feature.
		//
		#region AllowRowSummaries

		internal override bool AllowRowSummaries 
		{ 
			get
			{
				return this.Column.AllowRowSummariesResolved;
			}
		}

		#endregion // AllowRowSummaries

		// SSP 3/21/02
		// Version 2 Row Filter Implementation
		//
		#region AllowRowFiltering

		// SSP 4/16/05 - NAS 5.2 Filter Row
		// Changed the name from AllowRowFiltering to HasFilterIcons.
		//
		//internal override bool AllowRowFiltering
		internal override bool HasFilterIcons
		{
			get
			{
				// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// Support Filtering in Combo and DropDown
//				UltraGrid grid = this.Band.Layout.Grid as UltraGrid;				
//
//				// We only allow this on a grid. (And not on a UltraDropDown).
//				//
//				if ( null == grid )
//					return false;

				// By default it's false. The column header will override this
				// returning appropriate values.
				//
				return DefaultableBoolean.True == this.Column.AllowRowFilteringResolved 
					// SSP 4/16/05 - NAS 5.2 Filter Row
					// Changed the name from AllowRowFiltering to HasFilterIcons.
					//
					&& FilterUIType.HeaderIcons == this.Band.FilterUITypeResolved;
			}
		}

		#endregion //AllowRowFiltering

		// SSP 3/26/02
		// Version 2 Row Filter Implementation
		//
		#region AreFiltersActive

		internal bool AreFiltersActive( RowsCollection rows )
		{
			RowFilterMode filterMode = this.Band.RowFilterModeResolved;

			UltraGridColumn column = this.Column;

			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//Debug.Assert( null != this.Column && null != this.Band );
			Debug.Assert( null != column && null != this.Band );

			if ( null == column || null == this.Band )
				return false;

			if ( null != rows && rows.Band != this.Band )
			{
				Debug.Assert( false, "Rows does not belong to the same band as this header." );
				return false;
			}

			ColumnFilter columnFilter = null;

			if ( null == rows || RowFilterMode.AllRowsInBand == filterMode )
			{
				if ( this.Band.HasColumnFilters && this.Band.ColumnFilters.HasColumnFilter( column ) )
					columnFilter = this.Band.ColumnFilters[ column ];
			}
			else
			{
				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( rows.HasColumnFilters && rows.ColumnFilters.HasColumnFilter( column ) )
				//    columnFilter = rows.ColumnFilters[ column ];
				if ( rows.HasColumnFilters )
				{
					ColumnFiltersCollection columnFilters = rows.ColumnFilters;

					if ( columnFilters.HasColumnFilter( column ) )
						columnFilter = columnFilters[ column ];
				}
			}

			return null != columnFilter && columnFilter.HasFilterConditions;
		}
	

		#endregion //AreFiltersActive

		// SSP 3/21/02
		// Version 2 Row Filter Implementation
		//
		#region "RowFilterDropDownOwner class definition"

		internal class RowFilterDropDownOwner : IValueListOwner
		{
			#region Private Variables
			
			private Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader = null;			
			private ColumnFiltersCollection columnFilters = null;

			// SSP 8/5/03
			// Implemented code to show the cell values in the operator column of the row filter dialog.
			// Added the rows member variable.
			//
			private RowsCollection rows = null;
			
			#endregion // Private Variables

			#region contructor

			internal RowFilterDropDownOwner( 
				Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader, 
				ColumnFiltersCollection columnFilters,
				// SSP 8/5/03
				// Implemented code to show the cell values in the operator column of the row filter dialog.
				// Added the rows parameter to the constructor which can be null. It's the rows collection
				// associated with the header upon which the filter drop down is being dropped down on.
				//
				RowsCollection rows ) 
			{
				if ( null == columnHeader )
					throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_108"), Shared.SR.GetString("LE_ArgumentNullException_321") );

				if ( null == columnFilters )
					throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_110"), Shared.SR.GetString("LE_ArgumentNullException_322") );

				this.columnFilters = columnFilters;

				this.columnHeader = columnHeader;

				this.rows = rows;
			}

			#endregion //contructor

			#region ColumnHeader

			internal Infragistics.Win.UltraWinGrid.ColumnHeader ColumnHeader 
			{
				get
				{
					return this.columnHeader;
				}
			}

			#endregion //ColumnHeader

			#region IValueListOwner.Control

			Control IValueListOwner.Control
			{
				get
				{
					// SSP 5/19/06 BR12799
					// Since we re-implemented ValueListDropDown in NAS6.2 to make use of the drop-down manager,
					// we need to return the control that's hosting the column for which the row filter drop-down
					// is being dropped down. Commented out the original code and added new code.
					// 
					// --------------------------------------------------------------------------------------------
					UltraGridBase grid = this.ColumnHeader.Band.Layout.Grid;
					return null != grid ? grid.ControlForGridDisplay : null;

					
					// --------------------------------------------------------------------------------------------
				}
			}

			#endregion //IValueListOwner.Control

			#region IValueListOwner.UltraControl

			// SSP 4/26/06 - App Styling
			// Added UltraControl to the IValueListOwner.
			// 
			/// <summary>
			/// Returns the owner's IUltraControl.
			/// </summary>
			IUltraControl IValueListOwner.UltraControl
			{ 
				get
				{
					UltraGridLayout layout = this.ColumnHeader.Layout;
					return null != layout ? layout.Grid : null;
				}
			}

			#endregion // IValueListOwner.UltraControl

			#region IValueListOwner.EditControl

			Control IValueListOwner.EditControl
			{
				get
				{
					return null;
				}
			}

			#endregion //IValueListOwner.EditControl

			#region IValueListOwner.ImageList

			System.Windows.Forms.ImageList IValueListOwner.ImageList 
			{ 
				get
				{
					//Return grid's image list.
					//
					return this.ColumnHeader.Band.Layout.Grid.ImageList;
				}
			}

			#endregion //IValueListOwner.ImageList

			#region IValueListOwner.ValueLists

			ValueListsCollection IValueListOwner.ValueLists
			{
				get
				{
					return null;
				}
			}
			
			#endregion //IValueListOwner.ValueLists

			#region IValueListOwner.OnListGotFocus
			void IValueListOwner.OnListGotFocus()
			{
				//	No implementation
			}
			#endregion IValueListOwner.OnListGotFocus

			#region IValueListOwner.ResolveValueListAppearance

			void IValueListOwner.ResolveValueListAppearance( ref AppearanceData appData, AppearancePropFlags requestedProps )
			{
				// AS 4/7/06 ImageBackgroundDisabled
				//requestedProps &= ~( AppearancePropFlags.Image | AppearancePropFlags.ImageBackground );
				requestedProps &= ~GridUtils.ImageProperties;

				// MD 6/5/06 - BR13245
				// The don't resolve the bach hatch or back gradient styles.
				// This creates adverse effects when a selected item is being displayed
				requestedProps &= ~( AppearancePropFlags.BackHatchStyle | AppearancePropFlags.BackGradientStyle );

				ResolveAppearanceContext rc = new ResolveAppearanceContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridLayout ), requestedProps );

				// Resolve the colors first because otherwise we will end up picking up the 
				// layout's appearance which by default has gray back color which is not suitable
				// for the value list.
				//
				this.ColumnHeader.Band.Layout.ResolveColors( ref appData, ref rc, SystemColors.Window, SystemColors.WindowFrame, SystemColors.WindowText, false );

				this.ColumnHeader.Band.Layout.ResolveAppearance( ref appData, ref rc );
			}

			#endregion //IValueListOwner.ResolveValueListAppearance

			#region IValueListOwner.SizeOfImages

			Size IValueListOwner.SizeOfImages
			{
				get
				{
					return new Size(0,0);
				}
			}

			#endregion //IValueListOwner.SizeOfImages

			#region IValueListOwner.IsDropDownList

			/// <summary>
			/// True if this is a drop down list type where
			/// there is no edit control
			/// </summary>
			bool IValueListOwner.IsDropDownList
			{ 
				get
				{
					return true;
				}
			}

			#endregion //IValueListOwner.IsDropDownList

			#region IValueListOwner.OnCloseUp

			void IValueListOwner.OnCloseUp( )
			{		
				// Need to give the grid focus because for some reason after you drop 
				// the drop down again without giving the grid focus, the drop down 
				// gets focus
				//
                // MRS 5/4/06 - BR11919
                //UltraGrid grid = ((IValueListOwner)this).Control as UltraGrid;
                UltraGridBase grid = ((IValueListOwner)this).Control as UltraGridBase;

				if ( grid != null )
				{
					// AS - 1/14/02
					// We need to assert putting focus on the control in order for this to work
					// when the assembly is local but the executable is running under internet
					// type restricted security access. However, to prevent a security hole, we
					// will only do so on controls that have our public key token.
					//
					//grid.Focus();
					try
					{
						if ( DisposableObject.HasSamePublicKey( grid.GetType() ))
						{
							System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

							perm.Assert();
						}

						grid.Focus();
					}
					catch {}
				}
				
				if ( null != this.ColumnHeader )
					this.ColumnHeader.InvalidateItem( );
			}

			#endregion //IValueListOwner.OnCloseUp

			#region IValueListOwner.OnSelectedItemChanged
			
			void IValueListOwner.OnSelectedItemChanged( )
			{
				//Don't do anything
			}

			#endregion //IValueListOwner.OnSelectedItemChanged

			//	BF 3/20/06	NAS2006 Vol2 - ValueListDropDown
			#region IValueListOwner.ScrollBarLook
			/// <summary>
			/// Returns the ScrollBarLook instance which defines the appearance of the dropdown's scrollbar.
			/// </summary>
			Infragistics.Win.UltraWinScrollBar.ScrollBarLook IValueListOwner.ScrollBarLook
			{
				get
				{ 
					UltraGrid grid = ((IValueListOwner)this).Control as UltraGrid;
					UltraGridLayout layout = grid != null ? grid.DisplayLayout : null;
					return layout != null ? layout.ScrollBarLook : null;
				}
			}
			#endregion IValueListOwner.ScrollBarLook

			// AS 8/17/06 NA 2006 Vol 3 - Office 2007 L&F
			#region IValueListOwner.ScrollBarViewStyle
			Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle IValueListOwner.ScrollBarViewStyle
			{
				get
				{
					return Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.Default;
				}
			}
			#endregion IValueListOwner.ScrollBarViewStyle

            // MRS - NA v6.3 - Office2007 - DisplayStyle
            #region IValueListOwner.DisplayStyle
            /// <summary>
            /// Returns the preferred displaystyle for this editor.
            /// </summary>
            EmbeddableElementDisplayStyle IValueListOwner.DisplayStyle
            {
                get
                {
                    return EmbeddableElementDisplayStyle.Default;
                }
            }
            #endregion IValueListOwner.DisplayStyle

			#region ShowCustomFilterDialogHelper

			// SSP 5/16/05 - NAS 5.2 Filter Row
			// Added ShowCustomFilterDialogHelper method. Code in there was moved from the existing
			// IValueListOwner.OnSelectionChangeCommitted method.
			//
			internal static bool ShowCustomFilterDialogHelper( ColumnFilter columnFilter, RowsCollection rows )
			{
				bool applyNewFilters = true;
				UltraGridColumn column = columnFilter.Column;
				UltraGridBase grid = column.Layout.Grid;

				// SSP 8/28/03 - Optimizations
				// Instead of creating the form and keeping it around, recreate it every time it's displayed.
				//
				CustomRowFiltersDialog customRowFiltersDlg = null;
				try
				{
					// SSP 10/8/03 UWG2459
					// Added a property off the event args that returns the custom row filter dialog
					// so the user can change the appearance of the form.
					//
					customRowFiltersDlg = new CustomRowFiltersDialog( grid );
					customRowFiltersDlg.InitializeDialog( column, columnFilter, rows );

					// Fire BeforeCustomRowFilterDialog event.
					//
					// SSP 8/1/03
					// Added Column property to the event args so the user knows which column the 
					// dialog is being shown for.
					//
					//BeforeCustomRowFilterDialogEventArgs e = new BeforeCustomRowFilterDialogEventArgs( );
					BeforeCustomRowFilterDialogEventArgs e = new BeforeCustomRowFilterDialogEventArgs( column, 
						null != rows && RowFilterMode.AllRowsInBand != rows.Band.RowFilterModeResolved ? rows.TopLevelRowsCollection : null
						// SSP 10/8/03 UWG2459
						// Added a property off the event args that returns the custom row filter dialog
						// so the user can change the appearance of the form.
						//
						, customRowFiltersDlg );

					// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					// Support Filtering in Combo and DropDown
					//grid.FireEvent( GridEventIds.BeforeCustomRowFilterDialog, e );							
					grid.FireCommonEvent( CommonEventIds.BeforeCustomRowFilterDialog, e );

					// Only proceed if not cancelled
					//
					if ( !e.Cancel )
					{
						// SSP 10/8/03 UWG2459
						// Added a property off the event args that returns the custom row filter dialog
						// so the user can change the appearance of the form.
						//
						

						// Only show if not already visible.
						//
						// SSP 8/28/03 - Optimizations
						// Instead of creating the form and keeping it around, recreate it every time it's displayed.
						//
						//if ( !grid.CustomRowFiltersDialog.Visible )
						if ( !customRowFiltersDlg.Visible )
						{
							// SSP 8/1/03
							// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
							// Since we are passing in the cloned column filters into the BeforeRowFilterChanged event,
							// we need to use the cloned columnFilter instead of the original column filter object.
							// SSP 8/6/03
							// Also pass in the rows.
							//
							//DialogResult dialogResult = grid.CustomRowFiltersDialog.Show( this.ColumnHeader.Column, this.columnFilters[ this.ColumnHeader.Column ] );
							// SSP 8/28/03 - Optimizations
							// Instead of creating the form and keeping it around, recreate it every time it's displayed.
							//
							//DialogResult dialogResult = grid.CustomRowFiltersDialog.Show( this.ColumnHeader.Column, columnFilter, this.rows );
									
							// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
							// Support Filtering in Combo and DropDown										
							//DialogResult dialogResult = customRowFiltersDlg.InternalShowDialog( );
							DialogResult dialogResult;
							if ( grid is UltraGrid )
								dialogResult = customRowFiltersDlg.InternalShowDialog( );
							else
							{
								try
								{										
									Infragistics.Win.DropDownManager.SuspendAutoCloseUp();
									dialogResult = customRowFiltersDlg.InternalShowDialog( );
								}
								finally
								{
									Infragistics.Win.DropDownManager.ResumeAutoCloseUp();
								}
							}

							if ( DialogResult.OK == dialogResult )
							{
								Debug.WriteLine( "Custom row filter dialog okayed." );
							}
						}
					}
						// SSP 1/6/04 UWG2835
						// Added applyNewFilters flag. Set it to false so that we don't try to copy the new
						// filters to the original filters below since the BeforeCustomDialog event is canceled.
						// The reason for this is that the user could cancel that event and show his
						// own filter dialog that modifies the column filters in which case we will end
						// up erasing his new filters.
						// Added the else block.
						//
					else
					{
						applyNewFilters = false;
					}
				}
				finally
				{
					// SSP 8/28/03 - Optimizations
					// Instead of creating the form and keeping it around, recreate it every time it's displayed.
					// Dispose of the dialog.
					//
					if ( null != customRowFiltersDlg )
						customRowFiltersDlg.Dispose( );
					customRowFiltersDlg = null;
				}

				return applyNewFilters;
			}

			#endregion // ShowCustomFilterDialogHelper

			#region IValueListOwner.OnSelectionChangeCommitted

			void IValueListOwner.OnSelectionChangeCommitted( )
			{
				// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// Support Filtering in Combo and DropDown
				//UltraGrid grid = ((IValueListOwner)this).Control as UltraGrid;
				// MRS 10/20/05 - BR07014
				//UltraGridBase grid = ((IValueListOwner)this).Control as UltraGridBase;
				UltraGridBase grid = this.Grid;

				if ( grid == null )
					return;

				int index = ((IValueList)grid.FilterDropDown).SelectedItemIndex;
				
				if ( index < 0 || index >= grid.FilterDropDown.ValueListItems.Count )
					return;

				ValueListItem valueListItem = grid.FilterDropDown.ValueListItems[ index ];
				
                
				Debug.Assert( null != this.columnFilters );
				
				if ( null != this.columnFilters )
				{
					string selectedText = valueListItem.DisplayText;

					if ( null == selectedText )
						selectedText = "";

					ColumnFilter columnFilter = this.columnFilters[this.ColumnHeader.Column];

					// SSP 8/1/03
					// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
					// Clone the column filter because that's what we are going to pass into
					// the BeforeRowFilterChanged event. We want to give the user the ability
					// to compare the old filters with the new filters. So we have to pass in
					// the new filters as the clone rather than modifying the original. Once
					// the BeforeRowFilterChanged event returns and the user hasn't canceled it
					// we will apply the new filters.
					//
					// --------------------------------------------------------------------------
					ColumnFilter origColumnFilter = columnFilter;
					columnFilter = origColumnFilter.Clone( );
                    // --------------------------------------------------------------------------
					
					// SSP 1/6/04 UWG2835
					// Added applyNewFilters flag. It will be set to false if for example the
					// before custom filter dialog is canceled.
					//
					bool applyNewFilters = true;

					// SSP 8/5/03 - Extension to Filter Functionality
					// Added a way for the user to be able to modify the contents of the filter
					// drop down list so they can add custom conditions there (like "< 100" or
					// "< 0 || > 1000"). The DataValue of the ValueListItem should be either
					// a FilterCondition or ColumnFilter.
					// Added following two if-else-if blocks to the already existing series of
					// if-else-if blocks.
					//
					if ( valueListItem.DataValue is FilterCondition )
					{
						columnFilter.ClearFilterConditions( );						
						columnFilter.FilterConditions.Add( (FilterCondition)valueListItem.DataValue );
					}
					else if ( valueListItem.DataValue is ColumnFilter ) 
					{
						columnFilter.InitializeFrom( (ColumnFilter)valueListItem.DataValue );
					}
					// AS 1/8/03 - fxcop
					// Must supply an explicit CultureInfo to the String.Compare method.
					//if ( 0 == String.Compare( selectedText, ColumnFiltersCollection.FILTER_ALL,  true ) )
					else if ( 0 == String.Compare( selectedText, UltraGridBand.FilterAllText,  true, System.Globalization.CultureInfo.CurrentCulture ) )
					{
						columnFilter.ClearFilterConditions( );
					}					
					// AS 1/8/03 - fxcop
					// Must supply an explicit CultureInfo to the String.Compare method.
					//else if ( 0 == String.Compare( selectedText, ColumnFiltersCollection.FILTER_BLANKS,  true ) )
					else if ( 0 == String.Compare( selectedText, UltraGridBand.FilterBlanks,  true, System.Globalization.CultureInfo.CurrentCulture ) )
					{
						columnFilter.ClearFilterConditions( );
						columnFilter.FilterConditions.Add( FilterComparisionOperator.Equals, System.DBNull.Value );
						columnFilter.FilterConditions.Add( FilterComparisionOperator.Equals, null );
						columnFilter.FilterConditions.Add( FilterComparisionOperator.Equals, "" );
						columnFilter.LogicalOperator = FilterLogicalOperator.Or;
					}
					// AS 1/8/03 - fxcop
					// Must supply an explicit CultureInfo to the String.Compare method.
					//else if ( 0 == String.Compare( selectedText, ColumnFiltersCollection.FILTER_NONBLANKS,  true ) )
					else if ( 0 == String.Compare( selectedText, UltraGridBand.FilterNonBlanks, true, System.Globalization.CultureInfo.CurrentCulture ) )
					{
						columnFilter.ClearFilterConditions( );
						columnFilter.FilterConditions.Add( FilterComparisionOperator.NotEquals, System.DBNull.Value );
						columnFilter.FilterConditions.Add( FilterComparisionOperator.NotEquals, null );
						columnFilter.FilterConditions.Add( FilterComparisionOperator.NotEquals, "" );
						columnFilter.LogicalOperator = FilterLogicalOperator.And;
					}
					// AS 1/8/03 - fxcop
					// Must supply an explicit CultureInfo to the String.Compare method.
					//else if ( 0 == String.Compare( selectedText, ColumnFiltersCollection.FILTER_CUSTOM, true ) )
					else if ( 0 == String.Compare( selectedText, UltraGridBand.FilterCustomText, true, System.Globalization.CultureInfo.CurrentCulture ) )
					{
						// SSP 5/16/05 - NAS 5.2 Filter Row
						// Added ShowCustomFilterDialogHelper method. Moved the code from here into that
						// method. The original code is commented out below.
						//
						applyNewFilters = ShowCustomFilterDialogHelper( columnFilter, rows );
						
					}
                    // MBS 6/22/09 - TFS18639
                    // Added support for "(Errors)" and "(NonErrors)"
                    else if ( 0 == String.Compare( selectedText, UltraGridBand.FilterErrors, true, System.Globalization.CultureInfo.CurrentCulture ) )
                    {
                        columnFilter.ClearFilterConditions();
                        columnFilter.FilterConditions.Add(new DataErrorFilterCondition(true));
                        columnFilter.LogicalOperator = FilterLogicalOperator.Or;
                    }
                    else if (0 == String.Compare(selectedText, UltraGridBand.FilterNonErrors, true, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        columnFilter.ClearFilterConditions();
                        columnFilter.FilterConditions.Add(new DataErrorFilterCondition(false));
                        columnFilter.LogicalOperator = FilterLogicalOperator.Or;
                    }
					else 
					{
						columnFilter.ClearFilterConditions( );
						
						// SSP 8/2/02 UWG1483
						// Compare by values not the text.
						//
						//columnFilter.FilterConditions.Add( FilterComparisionOperator.Equals, selectedText );
						columnFilter.FilterConditions.Add( FilterComparisionOperator.Equals, valueListItem.DataValue );
					}

					// SSP 8/1/03
					// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
					//
					// ----------------------------------------------------------------------------------
					// SSP 1/6/04 UWG2835
					// Added applyNewFilters flag. Only apply the new filters if the flag says so. This
					// Also don't fire Before/After RowFilterChange event since we didn't change the 
					// filters.
					//
					if ( applyNewFilters )
					{
						// SSP 4/18/05 - NAS 5.2 Filter Row
						// Use the new ApplyNewFiltersHelper helper method instead. The code in that method
						// is moved from here (the code that's commented out below).
						//
						// --------------------------------------------------------------------------------
						UltraGridFilterRow.ApplyNewFiltersHelper( origColumnFilter, columnFilter, null );
						
						// --------------------------------------------------------------------------------
					}
					// ----------------------------------------------------------------------------------
				}

				// Call CloseUp which gives the focus back to the grid if
				// the value list is not dropped down anymore.
				//
				if ( null != grid )
				{
					IValueList valueList = grid.FilterDropDown as IValueList;

					if ( null != valueList )
					{
						if ( !valueList.IsDroppedDown )
							((IValueListOwner)this).OnCloseUp( );
					}
				}

				if ( null != this.ColumnHeader )
					this.ColumnHeader.InvalidateItem( );

				// SSP 5/20/05 - NAS 5.2 Filter Row
				// The following code was moved into the new UltraGridFilterRow's 
				// ApplyNewFiltersHelper method which we are calling above.
				//
				
			}

			#endregion //IValueListOwner.OnSelectionChangeCommitted

			#region IValueListOwner.MaxDropDownItems

			#endregion // IValueListOwner.MaxDropDownItems

			// MRS 10/20/05 - BR07014
			#region Grid
			private UltraGridBase Grid
			{
				get
				{
					ColumnHeader columnHeader = this.ColumnHeader;
					if (columnHeader == null)
						return null;

					UltraGridBand band = columnHeader.Band;
					if (band == null)
						return null;

					UltraGridLayout layout = band.Layout;
					if (layout == null)
						return null;

					return layout.Grid;

				}
			}
			#endregion Grid
		}


		#endregion // End of RowFilterDropDownOwner class definition

		// SSP 3/21/02
		// Version 2 Row Filter Implementation
		//
		#region OnClickFilterDropDown

		internal override void OnClickFilterDropDown( HeaderUIElement headerElem, bool shiftKeyDown )
		{
			Debug.Assert( null != headerElem, "Null header element passed in !" );

			if ( null == headerElem )
				return;

			Debug.Assert( this == headerElem.Header, "Header elem has different header than this header." );

			// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
			// Support Filtering in Combo and DropDown
			//UltraGrid grid = this.Band.Layout.Grid as UltraGrid;
			UltraGridBase grid = this.Band.Layout.Grid as UltraGridBase;

			Debug.Assert( null != this.Band, "No band !" );
			// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
			// Support Filtering in Combo and DropDown
			//Debug.Assert( null != grid, "OnClickFilterDropDown called on a non-UltraGrid column header." );
			Debug.Assert( null != grid, "OnClickFilterDropDown called on a non-UltraGridBase column header." );

			// Don't show the filter drop down if the grid is in design mode
			//
			if ( null == this.Band || null == grid || grid.DesignMode )
				return;

			ColumnFiltersCollection columnFilters = null;
			RowsCollection rows = null;

			if ( RowFilterMode.AllRowsInBand == this.Band.RowFilterModeResolved )
			{
				columnFilters = this.Band.ColumnFilters;
			}
			else if ( RowFilterMode.SiblingRowsOnly == this.Band.RowFilterModeResolved )
			{
				rows = 
					(Infragistics.Win.UltraWinGrid.RowsCollection)headerElem.GetContext( 
					typeof( Infragistics.Win.UltraWinGrid.RowsCollection ), true );

				// SSP 3/18/03 - Row Layout Functionality
				// In row layout mode, column headers could be in the row and we wouldn't have a
				// BandHeadersUIElement. Try to get a row and get the rows collection from there
				// if the header is located inside of a row.
				//
				// ------------------------------------------------------------------------------
				if ( null == rows )
				{
					UltraGridRow tmpRow = (UltraGridRow)headerElem.GetContext( typeof( UltraGridRow ), true );
					if ( null != tmpRow )
						rows = tmpRow.ParentCollection;
				}
				// ------------------------------------------------------------------------------

				Debug.Assert( null != rows, "No RowsCollection context on the header element when Band.RowFilterModeResolved is SiblingRowsOnly." );

				// If no rows context was provided and if this is a header from band 0, 
				// then use the rows off the band.
				// 
				if ( null == rows && null != this.Band && 0 == this.Band.Index )
					rows = this.Band.Layout.Rows;

				if ( null == rows )
					return;

				columnFilters = rows.ColumnFilters;
			}
			else
			{
				Debug.Assert( false, "Unknown value returned by this.Band.RowFilterModeResolved" );
				return;
			}

			Infragistics.Win.ValueList valueList = grid.FilterDropDown;

            // MBS 12/4/08 - NA9.1 Excel Style Filtering
            // Use the FilterUIProvider, if available.  If we're going to use that we don't really have
            // any reason to activate or use the valuelist ourselves.
            IFilterUIProvider filterUIProvider = this.Band != null ? this.Band.FilterUIProviderResolved : null;
            if (filterUIProvider == null)
            {
                // SSP 8/5/03
                // Changed the custom row filter dialog's operand field to display all the cell values.
                // Added rows parameter to RowFilterDropDownOwner constructor.
                //
                //((IValueList)valueList).Activate( new RowFilterDropDownOwner( this, columnFilters ) );
                ((IValueList)valueList).Activate(new RowFilterDropDownOwner(this, columnFilters, rows));
            }


			// Set the cursor to hour-glass because loading of filter values may
			// take a while, especially for lower level bands where all the rows
			// and all the rows collection have to be loaded.
			//
			Cursor.Current = Cursors.WaitCursor;
			
			try
			{
                // MBS 1/14/09 - TFS12274
                // We need to know the type of items to add to the list
                //
				//this.Band.LoadFilterValueList( valueList, headerElem, shiftKeyDown );
                bool hasFilterProvider = filterUIProvider != null;
                this.Band.LoadFilterValueList(valueList, headerElem, shiftKeyDown || hasFilterProvider,
                    hasFilterProvider ? FilterValueListOptions.FilterUIProvider : FilterValueListOptions.None);
			
				// Fire BeforeRowFilterDropDown event.
				//
				BeforeRowFilterDropDownEventArgs e = new BeforeRowFilterDropDownEventArgs( column, rows, valueList );

				// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// Support Filtering in Combo and DropDown
				//grid.FireEvent( GridEventIds.BeforeRowFilterDropDown, e );
				grid.FireCommonEvent( CommonEventIds.BeforeRowFilterDropDown, e );

				// If cancelled, then return.
				//
				if ( e.Cancel )
					return;

                // MBS 12/4/08 - NA9.1 Excel Style Filtering
                // Use the filter provider instead of the built-in ValueList, if applicable
                if (filterUIProvider != null)
                {
                    FilterDropDownButtonUIElement buttonElement = headerElem.GetDescendant(typeof(FilterDropDownButtonUIElement)) as FilterDropDownButtonUIElement;
                    if (buttonElement != null)
                    {
                        filterUIProvider.Show(
                            columnFilters[column],
                            rows,
                            this.Band.Layout.Grid.ControlForGridDisplay.RectangleToScreen(buttonElement.Rect), 
                            valueList);
                    }
                }
                else
                {
                    // MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
                    // Support Filtering in Combo and DropDown
                    //Rectangle dropDownRect = this.Band.Layout.Grid.RectangleToScreen( headerElem.Rect );                    
                    Rectangle dropDownRect = this.Band.Layout.Grid.ControlForGridDisplay.RectangleToScreen(headerElem.Rect);

                    ((IValueList)valueList).DropDown(dropDownRect, -1, null);
                }
			}		
			finally
			{
				// Set the cursor back to default when the value list is done loading.
				//
				Cursor.Current = Cursors.Default;
			}
		}

		#endregion //OnClickFilterDropDown

		// SSP 6/3/02
		// Summary rows.
		//
		internal override void OnClickSummaryRowsButton( HeaderUIElement headerElement )
		{
			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			Debug.Assert( null != grid, "No grid !" );

			// Don't show the summaries dialog in design mode.
			//
			// SSP 8/10/04 - Implemented design time serialization of summaries
			// Allow the developer to select summaries since now we have implemented the
			// serialization of summaries.
			//
			//if ( null == grid || grid.DesignMode )
			//	return;

			// SSP 5/12/05 BR03958
			// If the user explicitly enables the AllowSummaries on an UltraCombo or an
			// UltraDropDown then we will get here. In that case simply return.
			//
			if ( null == grid )
				return;

			// SSP 11/5/04 - Optimizations
			// Instead of creating the summary dialog and keeping it around, recreate it every time
			// it's displayed.
			//
			SummaryDialog rowSummariesDialog = new SummaryDialog( );

			// AS 1/29/07 NA 2007 Vol 1
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if (null != this.Column.Layout.Grid)
			//    this.Column.Layout.Grid.InitializeInboxControlStyler(rowSummariesDialog.InboxControlStyler);
			UltraGridColumn column = this.Column;
			UltraGridBase columnLayoutGrid = column.Layout.Grid;

			if ( null != columnLayoutGrid )
				columnLayoutGrid.InitializeInboxControlStyler( rowSummariesDialog.InboxControlStyler );

			// Fire the BeforeSummaryDialog event before showing the dialog.
			//
			// SSP 11/5/04 UWG3789
			// Added SummaryDialog property on the BeforeSummaryDialogEventArgs.
			//
			//BeforeSummaryDialogEventArgs e = new BeforeSummaryDialogEventArgs( this.Column );
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//BeforeSummaryDialogEventArgs e = new BeforeSummaryDialogEventArgs( this.Column, rowSummariesDialog );
			BeforeSummaryDialogEventArgs e = new BeforeSummaryDialogEventArgs( column, rowSummariesDialog );

			grid.FireEvent( GridEventIds.BeforeSummaryDialog, e );

			// If it wasn't cancelled then show the dialog.
			//
			if ( !e.Cancel )
			{
				Point point = headerElement.Rect.Location;

				point.Offset( 0, 5 + headerElement.Rect.Height );

				rowSummariesDialog.Location = grid.PointToScreen( point );

				// SSP 12/5/03 UWG2295
				// Fire AfterSummaryDialog event.
				//
				// ------------------------------------------------------------------------------
				//grid.RowSummariesDialog.Show( column );
				bool summariesChanged = rowSummariesDialog.Show( column );
				grid.FireEvent( GridEventIds.AfterSummaryDialog, new AfterSummaryDialogEventArgs( this.Column, summariesChanged ) );
				// ------------------------------------------------------------------------------

				// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
				// When the SummaryDisplayArea is Top or TopFixed and the first summary is added
				// it does not show up even if the scrollbar is at the top because the first row
				// remains the same. Maintain the scroll position to fix that.
				//
				if ( null != grid.Rows )
					grid.Rows.MaintainScrollPositionHelper( );
			}
		}

		internal override void Synchronize()
		{
			this.Column.Synchronize();
		}
		internal override int OverallOrigin
        {
            get
            {
                // MRS 3/10/2009 - TFS14963
                // Only use the group in Groups and Levels mode. The Column Extent is 
                // correct by itself in ColumnLayout or RowLayout mode.
                //
                if (this.column.Band.UseRowLayoutResolved)
                    return base.OverallOrigin;

				// MD 1/21/09 - Groups in RowLayouts
				// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
                //if ( null == this.column.Group )
				UltraGridGroup group = this.column.GroupResolved;
				if ( null == group )
                    return base.OverallOrigin;

				if ( this.Hidden )
                    return 0;

				// MD 1/21/09 - Groups in RowLayouts
				// Use the cached group.
                //return this.Origin + this.column.Group.Header.OverallOrigin;               
				return this.Origin + group.Header.OverallOrigin;
            }
        }		
		
		

		internal override void GetResizeRange( ref int minWidth, ref int maxWidth)
		{
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//this.Column.GetResizeRange( ref minWidth, ref maxWidth );
			UltraGridColumn column = this.Column;
			column.GetResizeRange( ref minWidth, ref maxWidth );

			//we have to adjust the minWidth because header.Extent and
			//column.Extent may differ 
			// MD 7/27/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.Column.Extent != this.Extent
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//int columnExtent = this.Column.Extent;
			int columnExtent = column.Extent;
			int extent = this.Extent;

			if ( columnExtent != extent
				// SSP 11/9/05 BR07642
				// Only add the row selector extent if the RowSelectorHeaderStyle is 
				// RowSelectorHeaderStyleResolved.
				// 
				&& RowSelectorHeaderStyle.ExtendFirstColumn == this.Band.RowSelectorHeaderStyleResolved )
			{
				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//minWidth += this.Extent - this.Column.Extent;
				minWidth += extent - columnExtent;
			}
		}

		internal override void InitSynchronization()
		{
			UltraGridColumn column = this.Column;
            column.synchronizedWidth = 0;
			column.isSynchronized = false;

			// JAS v5.2 Wrapped Header Text 4/26/05
			// If the header text is being wrapped and the columns are synchronized then
			// it is necessary that the column header heights be recalculated after a column
			// has been moved via drag-and-drop.  This is needed because the column could have
			// been very thin, but is being dropped into a column position which is very wide.
			// In that case the header text may no longer need to be wrapped, which means that
			// the header's height will be different.
			//
			if( column.Band.WrapHeaderTextResolved && column.Layout != null )
			{
				column.Layout.BumpColumnHeaderHeightVersionNumber();
			}
		}

		internal  override int GetRelativeColSyncPosition( UltraGridColumn column )
		{
			if ( column != this.Column )
			{
				Debug.Fail("Invalid column passed into ColumnHeader.GetRelativeColSyncPosition");
				return -1;
			}

			// MD 8/2/07 - 7.3 Performance
			// Use the equivalent column reference - Prevent calling expensive getters multiple times
			//return  this.Column.Hidden ? -1 : this.Column.ColSpan - 1;
            // MRS 2/23/2009 - Found while fixing TFS14354
			//return column.Hidden ? -1 : column.ColSpan - 1;
            return column.HiddenResolved ? -1 : column.ColSpan - 1;
		}

		internal override int GetTotalColSyncPositions ( ColScrollRegion exclusiveRegion )
		{		
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//return ( this.Column.Hidden ||	exclusiveRegion != this.ExclusiveColScrollRegion )
			//    ? 0 : (int)this.Column.ColSpan;
			UltraGridColumn column = this.Column;

            // MRS 2/23/2009 - Found while fixing TFS14354
			//return ( column.Hidden || exclusiveRegion != this.ExclusiveColScrollRegion )
            return (column.HiddenResolved || exclusiveRegion != this.ExclusiveColScrollRegion)
				? 0 : (int)column.ColSpan;
		}

			  

		internal override void InternalSetWidth ( int newWidth, bool autofit )
		{
			if ( this.FirstItem ) 
			{
				newWidth -= this.Band.RowSelectorExtent;
			}

			// JJD 1/2/02 
			// When we are doing an autofit we dont want to fire any events
			//
			// SSP 8/9/02 UWG1235
			// When doing auto fit columns logic, pass in false for byMouse otherwise
			// SetWidth will go through the logic of checkinf if the user is allowed
			// to resize the columns. We don't want it to go through that logic for
			// autofit columns. For example, when the grid is an UltraDropDown, colsizing
			// is not allowed and in which case SetWidth will not do anything.
			//
			// ---------------------------------------------------------------------
			//this.Column.SetWidth ( newWidth, true, !autofit, true, true, true );
			if ( autofit )
				// SSP 5/16/07 BR22393
				// Added an overload that takes in autoFit parameter. Pass in true for that.
				// 
				//this.Column.SetWidth ( newWidth, false, !autofit, true, true, true );
				this.Column.SetWidth( newWidth, false, !autofit, true, true, true, false, true );
			else
				this.Column.SetWidth ( newWidth, true, !autofit, true, true, true );
			// ---------------------------------------------------------------------
		}

		internal override bool AllowSwapping
		{
			get
			{
				return this.Column.AllowSwapping;
			}
		}
        

		internal override bool IsValidDropArea( DropLocationInfo dropLocationInfo, Point point )
		{
			// Treat exclusive colscrollregions as special
			// drop areas. You can't drop from one to another,
			// and you can't drop from an exclusive one to
			// a non-exclusive one.
			//
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//ColScrollRegionsCollection csrColl = this.Column.Layout.GetColScrollRegions( false );
			UltraGridLayout columnLayout = this.Column.Layout;

			ColScrollRegionsCollection csrColl = columnLayout.GetColScrollRegions( false );

			if ( null != csrColl )
			{
				// SSP 1/25/02
				// Use the new ExclusiveColScrollRegionResolved preoprty which will get
				// the group's ExclusiveColScrollRegion if the column has a group.
				//
				//ColScrollRegion sourceExclusiveRegion = this.ExclusiveColScrollRegion;
				ColScrollRegion sourceExclusiveRegion = this.ExclusiveColScrollRegionResolved;

				ColScrollRegion targetRegion = csrColl.GetRegionFromOrigin( point.X );

				// no drop area? bail...
				if ( null == targetRegion )
				{
					return false;
				}


				// if we have an exclusive source region, the two regions must be the same
				if ( null != sourceExclusiveRegion )
				{
					if ( !targetRegion.IsExclusive || 
						 targetRegion.SafeOrigin != sourceExclusiveRegion.SafeOrigin )
						return false;
				}
				else
					// if the source region isn't exclusive, the target can't be exclusive
				{
					// if target is exclusive don't allow the drop
					// SSP 1/25/02 
					// If the band is a card view or AutoFitColumns is enabled, we ignore the
					// EsclusiveColScrollRegion settins. So don't return false if that's the
					// case.
					//
					//if ( targetRegion.IsExclusive )
					if ( targetRegion.IsExclusive && !this.Band.CardView && !this.Band.Layout.AutoFitAllColumns )
						return false;
				}

			}
    
			AllowColMoving allowMoving = this.Band.AllowColMovingResolved;

			bool valid = false;
    
			if ( dropLocationInfo.IsDropTargetColumn )
			{
				Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader = dropLocationInfo.ColumnHeader;

				if ( null != columnHeader )
				{

					UltraGridGroup group = columnHeader.Group;

					//	Ignore ssAllowColMovingNotAllowed setting at design time
					bool designTimeMovingAllowed = false;

					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( null != this.Column.Layout.Grid )
					//    designTimeMovingAllowed = this.Column.Layout.Grid.DesignMode &&
					//                                allowMoving == AllowColMoving.NotAllowed;
					UltraGridBase columnLayoutGrid = columnLayout.Grid;

					if ( null != columnLayoutGrid )
					{
						designTimeMovingAllowed = 
							columnLayoutGrid.DesignMode && 
							allowMoving == AllowColMoving.NotAllowed;
					}

					// If Moving is Allowed Within Group..
					// and groups are present.
					if ( ( allowMoving == AllowColMoving.WithinGroup || designTimeMovingAllowed ) && null != group )
					{   
						// If the Groups are the same then the move is
						// allowed
						if ( group == this.Group )
							valid = true;
						else
							valid = false;                    

					}
						
					//	Ignore AllowColMovingNotAllowed setting at design time
					else if ( allowMoving == AllowColMoving.WithinBand ||
						( allowMoving == AllowColMoving.WithinGroup && null == group ) ||
						 designTimeMovingAllowed )
					{
						// If we don't have a Group, then treat 
						// the same as ssAllowColMovingWithinBand
						// so this move is valid.

						// If Column Moving is allowed within Band
						// check to make sure that they Band's
						// are the same.
						UltraGridBand band = dropLocationInfo.GridItem.Band;
						UltraGridBand thisBand = this.Band;

						valid = band == thisBand;

						// SSP 7/24/03 - Fixed headers
						// If the FixedHeaderIndicator settings are as such that the fixed header
						// state cannot be changed, set valid to false if the drop will cause the 
						// Fixed state to change. (If the non-fixed header is being dropped onto
						// a fixed header or a fixed header is being dropped onto a non-fixed header).
						//
						if ( FixedHeaderIndicator.None == this.FixedHeaderIndicatorResolved )
							// SSP 6/2/04 UWG3357
							// If valid was set to false due to bands being different above, don't
							// reset it to true here.
							//
							//valid = null != columnHeader && this.FixedResolved == columnHeader.FixedResolved;
							valid = valid && null != columnHeader && this.FixedResolved == columnHeader.FixedResolved;
					}
				}
			}
			else if ( dropLocationInfo.IsDropTargetGroup ||
				dropLocationInfo.IsDropTargetEmptyLevel )
			{
				UltraGridGroup group = dropLocationInfo.GroupHeader.Group;
        
				if ( null != group )
				{
					// Check to make sure that they Band's
					// are the same. You can never move columns
					// between bands
					if ( group.Band != this.Band )
						return false;

				}

				// Added case for Group.
				if ( allowMoving == AllowColMoving.WithinGroup && null != group )
				{   
					// If the Groups are the same then the move is
					// allowed
					valid = group == this.Group;           

				}
				else if ( allowMoving == AllowColMoving.WithinBand )
				{
					valid = true;

					// SSP 7/24/03 - Fixed headers
					// If the FixedHeaderIndicator settings are as such that the fixed header
					// state cannot be changed, set valid to false if the drop will cause the 
					// Fixed state to change. (If the non-fixed header is being dropped onto
					// a fixed header or a fixed header is being dropped onto a non-fixed header).
					//
					if ( FixedHeaderIndicator.None == this.FixedHeaderIndicatorResolved )
						// SSP 6/2/04 UWG3357
						// 
						//valid = null != group && null != group.Header && this.FixedResolved == group.Header.FixedResolved;
						valid = valid && null != group && null != group.Header && this.FixedResolved == group.Header.FixedResolved;
				}
			}

			return valid;
		}


		
		internal override void CreateSwapList( ValueList swapDropDown )
		{
			this.Column.CreateSwapList( swapDropDown );
		}

		internal override void Swap( HeaderBase swapItem )
		{			
			this.Column.Swap( swapItem.Column );
		}


		internal bool StartDragNoSelection( )
		{
			return this.StartDragNoSelection( false );
		}

		/// <summary>
		/// start a drag operation on the single column
		/// </summary>
		/// <param name="isGroupByButton">whether we are 
		/// dragging a GroupByButton in a group by box</param>
		/// <returns></returns>
		internal bool StartDragNoSelection( bool isGroupByButton )
		{
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//UltraGrid grid = this.Column.Layout.Grid as UltraGrid;
			UltraGridColumn column = this.Column;

			UltraGrid grid = column.Layout.Grid as UltraGrid;

			UltraGridBand band = this.Band;

			// if header is disabled, don't drag	
			if ( null == grid || this.IsDisabled )				
				return false;

			// MRS 3/14/05 - Allow drag and drop in RowLayout mode
			if (band.UseRowLayoutResolved)
			{	
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//bool rowLayoutDragStarted = grid.StartGridBagLayoutDrag(this.Column, isGroupByButton);
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//grid.StartGridBagLayoutDrag( this.Column, isGroupByButton );
				grid.StartGridBagLayoutDrag( column, isGroupByButton );

				// If there's no GroupByBox showing, we can simply exit. 
				// If there is one, we want to let this go ahead and create 
				// the usual dragColumnStrategy in addition to the GridBagLayoutDragStrategy.
				if (band.Layout.GroupByBox.HiddenResolved)
				{
					// SSP 6/28/05 - NAS 5.3 Column Chooser
					// 
					grid.ColumnChooser_OnDragStart( );

					return true;
				}
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//DragStrategyColumn dragColumnStrategy = new DragStrategyColumn( band, grid, null, isGroupByButton );
			DragStrategyColumn dragColumnStrategy = new DragStrategyColumn( band, grid, isGroupByButton );

			if ( dragColumnStrategy.BeginDrag( this ) )
			{
				DragEffect dragEffect = grid.DragEffect;

				if ( null != dragEffect )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Avoid unused private fields
					//dragEffect.Setup( dragColumnStrategy, DragType.Bitmap );
					dragEffect.Setup( dragColumnStrategy );
    
					dragEffect.Display( );

					// SSP 6/28/05 - NAS 5.3 Column Chooser
					// 
					grid.ColumnChooser_OnDragStart( );

					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// start a drag operation on column
		/// </summary>
		/// <returns></returns>
		internal bool StartDrag( )
		{
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//UltraGrid grid = this.Column.Layout.Grid as UltraGrid;
			UltraGridColumn column = this.Column;
			UltraGrid grid = column.Layout.Grid as UltraGrid;

			UltraGridBand band = this.Band;

			// if header is disabled, don't drag			
			if ( null == grid || this.IsDisabled )							
				return false;

			Selected selected = grid.Selected;

			// if nothing's selected
			if ( null == selected )
				return false;
			
			// MRS 3/14/05 - Allow drag and drop in RowLayout mode
			if (this.Band.UseRowLayoutResolved)
			{
				// Can only drag one column at a time in RowLayout mode. 
				// MRS 4/21/05 - BR03472
				// In RowLayout mode, the item could be a cell.
//				if (selected.Columns.Count != 1)
//					return false;
				if (this.Band.AreColumnHeadersInSeparateLayoutArea)
				{
					if (selected.Columns.Count != 1)
						return false;
				}
				else
				{
					if (selected.Cells.Count != 1)
						return false;
				}
				
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//bool rowLayoutDragStarted = grid.StartGridBagLayoutDrag(this.Column, false);
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//grid.StartGridBagLayoutDrag( this.Column, false );
				grid.StartGridBagLayoutDrag( column, false );

				// If no GroupByBox showing, we can simply exit. 
				// If there is one, we want to let this go ahead and create 
				// the usual dragColumnStrategy in addition to the GridBagLayoutDragStrategy.
				if (band.Layout.GroupByBox.HiddenResolved)
				{
					// SSP 6/28/05 - NAS 5.3 Column Chooser
					// 
					grid.ColumnChooser_OnDragStart( );

					return true;				
				}
			}

			if ( null == selected.Columns || 0 == selected.Columns.Count )
			{
				return false;
			}			

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//DragStrategyColumn dragColumnStrategy = new DragStrategyColumn( band, grid, selected );
			DragStrategyColumn dragColumnStrategy = new DragStrategyColumn( band, grid );

			if ( dragColumnStrategy.BeginDrag( this ) )
			{
				DragEffect dragEffect = grid.DragEffect;

				if ( null != dragEffect )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Avoid unused private fields
					//dragEffect.Setup( dragColumnStrategy, DragType.Bitmap );
					dragEffect.Setup( dragColumnStrategy );
    
					dragEffect.Display( );

					// SSP 6/28/05 - NAS 5.3 Column Chooser
					// 
					grid.ColumnChooser_OnDragStart( );

					return true;
				}
			}

			return false;
		}

		internal override void SetOrigin( int origin )
		{
			base.SetOrigin(origin);
			this.Column.RelativeOrigin = origin;
		}

		internal override void AddColsToSyncColsList ( SynchedBandAbove pSynchedBandAbove )
		{
			Debug.Assert( this.column.NextLinkedListItem == null && this.column.PrevLinkedListItem == null, 
				"Column is already in synched cols list in CColumn::AddColsToSyncColsList");
			Debug.Assert( this.Band.AllowColSizingResolved == AllowColSizing.Synchronized,
				"Column's band is not a synched band in CColumn::AddColsToSyncColsList");
			Debug.Assert( !this.Band.GroupsDisplayed, "Column's band displays groups in CColumn::AddColsToSyncColsList");

			// if there is no prior band or this column is locked 
			// then we don't have to do anything
			//
			if ( pSynchedBandAbove == null || this.Hidden )
				return;

			Debug.Assert( pSynchedBandAbove.Band.AllowColSizingResolved == AllowColSizing.Synchronized, 
					"Prior band is not a synched band in CColumn::AddColsToSyncColsList" );

			// Get this column's sync position
			//
			int nSyncPosition = this.Band.GetSyncPosition( this.column );

			UltraGridColumn pColToSyncWith  = null;
			SynchedBandAbove pBandAboveChain = pSynchedBandAbove;

			// MD 8/2/07 - 7.3 Performance
			// Cache values - Prevent calling expensive getters multiple times
			UltraGridColumn column = this.Column;
			ColScrollRegion exclusiveColScrollRegion = this.ExclusiveColScrollRegion;

			while ( pBandAboveChain != null && pColToSyncWith == null )
			{
				// try to find a column to sync with in this band
				//
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//pColToSyncWith = pBandAboveChain.Band.GetColAtSyncPosition ( nSyncPosition, this.Column.Level, this.ExclusiveColScrollRegion );
				// MD 1/21/09 - Groups in RowLayouts
				// Use LevelResolved because it checks the band's row layout style
				//pColToSyncWith = pBandAboveChain.Band.GetColAtSyncPosition( nSyncPosition, column.Level, exclusiveColScrollRegion );
				pColToSyncWith = pBandAboveChain.Band.GetColAtSyncPosition( nSyncPosition, column.LevelResolved, exclusiveColScrollRegion );

				// walk up the above synched band's chain
				//
				pBandAboveChain = pBandAboveChain.synchedBandAbove;
			}

			// add us to the linked list
			//
			if ( pColToSyncWith != null )
			{
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//this.Column.InsertAfterLinkedList ( pColToSyncWith );
				column.InsertAfterLinkedList( pColToSyncWith );
			}
		}


		internal override UltraGridColumn GetColAtRelativeSyncPosition( int nRelativeSyncPosition, int nLevel,
											int nTotalSyncPositions, ColScrollRegion exclusiveRegion )
		{
			// MD 1/21/09 - Groups in RowLayouts
			// When in group layout mode, make sure the level is 0.
			Debug.Assert(
				nLevel == 0 ||
				this.Band == null ||
				this.Band.RowLayoutStyle != RowLayoutStyle.GroupLayout, "A level other than 0 cannot be used when in GroupLayout style." );

            // MRS 2/23/2009 - Found while fixing TFS14354
			//if ( this.column.Hidden									||
            if (this.column.HiddenResolved ||
				this.column.ColSpan - 1 != nRelativeSyncPosition	||
				// MD 1/21/09 - Groups in RowLayouts
				// Use LevelResolved because it checks the band's row layout style
				//this.column.Level != nLevel							||
				this.column.LevelResolved != nLevel							||
				exclusiveRegion != this.column.Header.ExclusiveColScrollRegion )
			{
				return null;
			}

			return this.column;
		}

		internal override Activation ActivationResolved
		{
			get
			{
				// JJD 11/02/01
				// Return the column's resolved activation 
				//
				return this.column.ActivationResolved;
			}
		}
		internal override SortIndicator SortIndicator
		{
			get 
			{				
				return this.column.SortIndicator;
			}
		}


		internal override SelectChange SelectChangeType 
		{
			get
			{
				return SelectChange.Col;
			}
		}
		internal ColumnHeader Clone()
		{
			// SSP 5/20/04 UWG3097
			// Set the appearance holder on the cloned header to null so that when the 
			// cloned header is disposed off, it doesn't try to reset the appearance holder
			// that's referenced by the source header.
			//
			//return (Infragistics.Win.UltraWinGrid.ColumnHeader)this.MemberwiseClone();
			Infragistics.Win.UltraWinGrid.ColumnHeader header = (Infragistics.Win.UltraWinGrid.ColumnHeader)this.MemberwiseClone( );
			header.appearanceHolder = null;
			return header;
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info,
			StreamingContext context )
		{
			base.GetObjectData( info, context );

            // CDS NAS v9.1 Header CheckBox
            if (this.ShouldSerializeCheckBoxVisibility())
            {
                Utils.SerializeProperty(info, "CheckBoxVisibility", this.CheckBoxVisibility);
            }

            // CDS NAS v9.1 Header CheckBox
            if (this.ShouldSerializeCheckBoxAlignment())
            {
                Utils.SerializeProperty(info, "CheckBoxAlignment", this.CheckBoxAlignment);
            }

            // CDS NAS v9.1 Header CheckBox
            if (this.ShouldSerializeCheckBoxSynchronization())
            {
                Utils.SerializeProperty(info, "CheckBoxSynchronization", this.CheckBoxSynchronization);
            }
		}
		internal override ISelectionStrategy SelectionStrategyDefault 
		{
			get
			{
				return ((ISelectionStrategyProvider)(Band)).SelectionStrategyColumn;
			}
		}
		internal override void AddToCollection( Selected selected )
		{
			if ( selected == null )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_112"));

			selected.Columns.InternalAdd( this );
		}

		/// <summary>
		/// Removes itself to passed-in collection.
		/// </summary>
		internal override void RemoveFromCollection( Selected selected )
		{
			Debug.Assert( selected.Columns != null, "No collection to remove from!" );

			selected.Columns.Remove( this );
		}

		internal override void SetInitialSelection( Selected currentSelection, 
												    Selected initialSelection )
		{
			if ( currentSelection.Columns != null )
			{
				// JJD 10/09/01
				// You no longer have to create the columns collection like
				// this since the property get will create it lazily
				//
				//if ( initialSelection.Columns == null )
				//	initialSelection.Columns = new SelectedColsCollection();

				// add each column currently selected into the initial selection collection
				foreach( ColumnHeader col in currentSelection.Columns )
					initialSelection.Columns.InternalAdd( col );
			}
		}

		/// <summary>
		/// The overall visible position of the column within its band (read-only).
		/// </summary>
		/// <remarks>If groups are displayed this will be the relative position of the column within the group plus the total number of columns from all preceeding groups. The value returned includes columns that are hidden.</remarks>
		[Browsable(false)]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public int VisiblePositionWithinBand
		{
			get
			{
				int position = -1;

				// MD 8/3/07 - 7.3 Performance
				// Cached column - Prevent calling expensive getters multiple times
				UltraGridColumn column = this.Column;

				// First check if groups are displayed
				//
				// MD 1/20/09 - Groups in RowLayout
				//if ( this.Band.GroupsDisplayed )
				if ( this.Band.GroupsDisplayed && this.Band.RowLayoutStyle != RowLayoutStyle.GroupLayout )
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( this.Column.Group != null )
					if ( column.Group != null )
					{
						// get the relative position within the group.
						//
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//position = this.Column.Group.Columns.IndexOf( this.Column );
						position = column.Group.Columns.IndexOf( column );

						// add in the total columns from all preceeding groupd
						//
						for ( int i = 0; i < this.Band.OrderedGroupHeaders.Count; i++ )
						{
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//if ( this.Band.OrderedGroupHeaders[i].Group == this.Column.Group )
							UltraGridGroup headerGroup = this.Band.OrderedGroupHeaders[ i ].Group;

							if ( headerGroup == column.Group )
								break;

							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//position += this.Band.OrderedGroupHeaders[i].Group.Columns.Count;
							position += headerGroup.Columns.Count;
						}
					}
				}
				else
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//position = this.Band.OrderedColumnHeaders.IndexOf( this.Column.Header );
					position = this.Band.OrderedColumnHeaders.IndexOf( column.Header );
				}

				return position;
			}
		}

		internal override void CalcSelectionRange( Selected newSelection, 
												   bool clearExistingSelection,
												   bool select,
												   Selected initialSelection )
		{
			GridItemBase pivotItem = this.Band.Layout.Grid.ActiveColScrollRegion.GetPivotItem();

			Debug.Assert( pivotItem != null, "pivotItem null!" );

			// MD 8/3/07 - 7.3 Performance
			// Cached column - Prevent calling expensive getters multiple times
			UltraGridColumn headerColumn = this.Column;

			// bands must be the same
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//Debug.Assert( pivotItem.Band == this.Column.Band, "Bands don't match!" );
			Debug.Assert( pivotItem.Band == headerColumn.Band, "Bands don't match!" );

			ColumnHeader pivotCol = pivotItem as ColumnHeader;

			// JJD 1/14/02
			// If a group was the pivot and we are doing a range select to a
			// column in another group then set the pivot column to the first or
			// last selectable column from the pivot group depending on which 
			// direction we are going.
			//
			if ( pivotItem is GroupHeader )
			{
				UltraGridGroup group = ((GroupHeader)pivotItem).Group;

				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( group != this.Column.Group )
				// MD 1/21/09 - Groups in RowLayouts
				// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
				//if ( group != headerColumn.Group )
				UltraGridGroup headerColumnGroup = headerColumn.GroupResolved;
				if ( group != headerColumnGroup )
				{
					UltraGridColumn column;

					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( group.Header.VisiblePosition > this.Column.Group.Header.VisiblePosition )
					// MD 1/21/09 - Groups in RowLayouts
					// Use the cached group for the header column's group.
					// LastSelectableColumn and FirstSelectableColumn have been moved to the group.
					//if ( group.Header.VisiblePosition > headerColumn.Group.Header.VisiblePosition )
					//    column = group.Columns.LastSelectableColumn;
					//else
					//    column = group.Columns.FirstSelectableColumn;
					if ( group.Header.VisiblePosition > headerColumnGroup.Header.VisiblePosition )
						column = group.LastSelectableColumn;
					else
						column = group.FirstSelectableColumn;

					if ( column != null )
						pivotCol = column.Header;
				}
					// SSP 1/14/04 - Accessibility.
					// Added the else block. If the pivot group is the same as the this column's
					// group, then make the first column of the group the pivot column.
					//
				else
				{
					// MD 1/21/09 - Groups in RowLayouts
					// FirstSelectableColumn bas been moved to the group.
					//UltraGridColumn column = group.Columns.FirstSelectableColumn;
					UltraGridColumn column = group.FirstSelectableColumn;

					if ( null != column )
						pivotCol = column.Header;
				}
			}
			
			// SSP 1/14/04
			// Make sure the pivotCol is not null otherwise we will get null reference 
			// exception below.
			//
			if ( null == pivotCol )
				return;

			// SSP 3/03/03 - Row Layout Functionality
			// Following is the code for header selection in row layout mode. Since headers can be laid
			// out in verious ways that may not correspond with the visible positions of the headers, we
			// need to have special selection logic.
			// 
			// ------------------------------------------------------------------------------------------
			if ( this.Band.UseRowLayoutResolved )
			{
				if ( ! clearExistingSelection )
				{
					UltraGrid grid = this.Band.Layout.Grid as UltraGrid;
				    Infragistics.Win.UltraWinGrid.Selected selected = null != grid && grid.HasSelectedBeenAllocated ? grid.Selected : null;

					if ( null != selected )
					{
						foreach ( UltraGridRow row in selected.Rows )
						{
							newSelection.Rows.InternalAdd( row );
						}

						foreach ( UltraGridCell cell in selected.Cells )
						{
							newSelection.Cells.InternalAdd( cell );
						}
					}

					foreach ( ColumnHeader col in initialSelection.Columns )
					{
						newSelection.Columns.InternalAdd( col );
					}
				}

				if ( pivotCol == this )
				{
					if ( ! newSelection.Columns.Contains( this ) )
						newSelection.Columns.InternalAdd( this );
				}
				else
				{
					UltraGridBand band = this.Band;
					Infragistics.Win.UltraWinGrid.ColumnHeader colHeader1 = (ColumnHeader)pivotCol;
					Infragistics.Win.UltraWinGrid.ColumnHeader colHeader2 = this;

					HeadersCollection layoutOrderedColumnHeaders = band.LayoutOrderedVisibleColumnHeaders;

					if ( null != layoutOrderedColumnHeaders &&
						layoutOrderedColumnHeaders.Contains( colHeader1 ) &&
						layoutOrderedColumnHeaders.Contains( colHeader2 ) )
					{
						Rectangle boundingRect = Rectangle.Union( 
							colHeader1.Column.RowLayoutColumnInfo.CachedHeaderRect, 
							colHeader2.Column.RowLayoutColumnInfo.CachedHeaderRect );

						for ( int i = 0; i < layoutOrderedColumnHeaders.Count; i++ )
						{
							Infragistics.Win.UltraWinGrid.ColumnHeader header = (ColumnHeader)layoutOrderedColumnHeaders[i];

							Rectangle itemRect = header.Column.RowLayoutColumnInfo.CachedItemRectHeader;
							itemRect.Inflate( -1, -1 );

							if ( ! header.IsDisabled && 
								//boundingRect.Contains( header.Column.RowLayoutColumnInfo.CachedItemRectHeader ) 
								boundingRect.IntersectsWith( itemRect ) 
								&& ! newSelection.Columns.Contains( header ) )
								newSelection.Columns.InternalAdd( header );
						}
					
					}
					else
					{
						Debug.Assert( false );
					}
				}

				return;
			}
			// ------------------------------------------------------------------------------------------
			
			HeadersCollection headers = this.GetHeadersForSelection( select, pivotCol );
			
			for ( int i = 0; i < headers.Count; i++ )
			{
				newSelection.Columns.InternalAdd( (ColumnHeader)headers[i] );
			}

			headers = null;

			if ( ! clearExistingSelection )
			{
				int positionToTest;

				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//int position = this.Column.InternalGetVisiblePosition();
				int position = headerColumn.InternalGetVisiblePosition();

				int positionPivotItem = pivotCol.VisiblePosition;

				//MergeSelectionWithInitialSelection();
				foreach ( ColumnHeader col in initialSelection.Columns )
				{
					// JJD 10/15/01
					// Get the position once to avoid unnecessary
					// overhead
					//
					positionToTest = col.VisiblePosition;

					// JJD 10/15/01 - UWG523
					// Don't re-add the if the position is equal to the pivot
					//

					if ( position > positionPivotItem )
					{
						if ( positionToTest < positionPivotItem ||
							 positionToTest > position )
							newSelection.Columns.InternalAdd( col );							 
					}
					else 
					{
						if ( positionToTest < position ||
							 positionToTest > positionPivotItem )
							newSelection.Columns.InternalAdd( col );
					}
				}

				// JJD 10/12/01 - UWG522
				// Copy over the selected cells and rows also
				//
				UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

				if ( grid != null )
				{
					foreach ( UltraGridRow row in grid.Selected.Rows )
					{						
						newSelection.Rows.InternalAdd( row );
					}

					foreach ( UltraGridCell cell in grid.Selected.Cells )
					{
						newSelection.Cells.InternalAdd( cell );
					}
				}
			}
		}	
		internal override void InternalSelect( bool value )
		{
			if( this.selectedValue != value )
			{
				this.selectedValue = value;

				//if column has a group then 
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.Column.Group != null && this.Column.Group.Header != null )
				UltraGridColumn column = this.Column;

				// MD 1/21/09 - Groups in RowLayouts
				// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
				// Also, the Header can never be null.
				//if ( column.Group != null && column.Group.Header != null )
				UltraGridGroup group = column.GroupResolved;
				if ( group != null )
				{
					//deselect the group header if false
					if ( value == false )
					{
						// JJD 1/14/02
						// Call SetSelectedFlag instead. Otherwise, this
						// will generate a stck overflow.
						//
//						this.Column.Group.Header.Selected = value;
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//this.Column.Group.Header.SetSelectedFlag( value );
						// MD 1/21/09 - Groups in RowLayouts
						// Use the cached group
						//column.Group.Header.SetSelectedFlag( value );
						group.Header.SetSelectedFlag( value );
					}
					//possibly select group header
					else
					{	
						bool allColsSelected = true;

						//iterate through Group's columns collection
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						// Cached count so we don't have to get it multiple times
						//for ( int i = 0; i < this.Column.Group.Columns.Count; i++ )
						// MD 1/21/09 - Groups in RowLayouts
						// Use the cached group and the ColumnsResolved property instead because in GroupLayout style, the columns in the group 
						// may not be the columns in the Columns collection.
						//for ( int i = 0, count = column.Group.Columns.Count; i < count; i++ )
						foreach ( UltraGridColumn otherColumn in group.ColumnsResolved )
						{
							//if any of the column header is not selected then the
							//group can't be selected and bail
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//if ( !this.Column.Group.Columns[i].Header.Selected )
							// MD 1/21/09 - Groups in RowLayouts
							// Use the cached group
							//if ( !column.Group.Columns[ i ].Header.Selected )
							if ( otherColumn.Header.Selected == false )
							{
								allColsSelected = false;
								break;
							}
						}
						//all columns selected, so group is selected
						if ( allColsSelected )
						{
							// JJD 1/14/02
							// Call SetSelectedFlag instead. Otherwise, this
							// will generate a stck overflow.
							//
//							this.Column.Group.Header.Selected = value;
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//this.Column.Group.Header.SetSelectedFlag( value );
							// MD 1/21/09 - Groups in RowLayouts
							// Use the cached group
							//column.Group.Header.SetSelectedFlag( value );
							group.Header.SetSelectedFlag( value );
						}
					}
				}

				//this.NotifyPropChange( PropertyIds.Selected, null );
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//this.Column.NotifyPropChange( PropertyIds.Selected, null );
				column.NotifyPropChange( PropertyIds.Selected, null );

				// SSP 10/10/02 UWG1530
				// Bump the cell child elements cache version so that the cell elements reposition
				// their child elements whhen the column is selected or deselected.
				//
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//this.Column.BumpCellChildElementsCacheVersion( );
				column.BumpCellChildElementsCacheVersion();
			}
		}


		// This needed to be an internal method so that it didn't conflict 
		// with the Cell's public Column property
		//
		internal override Infragistics.Win.UltraWinGrid.UltraGridColumn GetColumn()
		{
			return this.column;
		}

		

		internal override int DefaultVisiblePosition
		{
			get 
			{
				if ( this.column == null ||
					 this.column.Band == null )
					return -1;

				return this.column.Index;
			}
		}
		
		/// <summary>
		/// Returns or sets the visible position of a header.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property can be used to specify the ordinal positions of groups and columns.</p>
		/// <p class="body">For group headers, this property returns or sets the position of the group within that group's band. For column headers, this property returns or sets the position of the column within its group, if the column belongs to a group, or its band, if the column belongs to a band.</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.Header"/>
		/// </remarks>
		public override int VisiblePosition
		{
			get
			{
				if ( this.column		!= null &&
					 this.column.Band	!= null )
				{
					// JJD 10/16/01
					// The first time through the visible position is -1 unless
					// a different value was persisted. In this case initialize
					// the position to the column's index
					//
					int serializedPosition = this.SerializedVisiblePosition;

					int position = this.column.InternalGetVisiblePosition();

					if ( position >= 0 )
						return position;
				}

				return this.visiblePos;
			}
			set
			{
				// SSP 4/11/07 BR21669
				// Moved the code into the new SetVisiblePosition method.
				// 
				// ------------------------------------------------------------
				this.SetVisiblePosition( value, false );
				
				// ------------------------------------------------------------
			}
		}

		// SSP 4/11/07 BR21669
		// Added SetVisiblePosition.
		// 
		/// <summary>
		/// Sets the visible position of the header.
		/// </summary>
        /// <param name="visiblePosition">The visible position to apply to the column</param>
        /// <param name="raiseColPosChangedNotifications">True to raise ColPosChanged notifications.</param>
		/// <remarks>
		/// <p class="body">This method can be used to specify the ordinal positions of groups and columns.</p>
		/// <p class="body">For group headers, this methos sets the position of the group within that group's band. For column headers, this method sets the position of the column within its group, if the column belongs to a group, or its band, if the column belongs to a band.</p>
		/// <p class="note"><b>Note:</b> This method does the same thing as the <see cref="VisiblePosition"/> property
		/// except that this method gives you the option of raising <see cref="UltraGridBase.BeforeColPosChanged"/>
		/// and <see cref="UltraGridBase.AfterColPosChanged"/> events where as the VisiblePosition property does
		/// not raise these notifications.</p>
		/// </remarks>
		public void SetVisiblePosition( int visiblePosition, bool raiseColPosChangedNotifications )
		{
			if ( this.column == null || this.column.Band == null )
				base.visiblePos = visiblePosition;
			else
			{
				// SSP 4/11/07 BR21669
				// If raiseColPosChangedNotifications is true then raise the Before and After events.
				// 
				bool beforeColPosChangedEventRaised = false;
				if ( raiseColPosChangedNotifications )
				{
					UltraGridBase grid = null != this.Layout ? this.Layout.Grid : null;
					int currPos = this.column.InternalGetVisiblePosition( );
					if ( currPos != visiblePosition && null != grid )
					{
						BeforeColPosChangedEventArgs eventArgs = new BeforeColPosChangedEventArgs( PosChanged.Moved, new ColumnHeader[] { this } );
						grid.FireBeforeColPosChanged( eventArgs );
						beforeColPosChangedEventRaised = true;
					}
				}

				this.column.InternalSetVisiblePosition( visiblePosition );

				//use the base implementation for set
				base.VisiblePosition = this.column.InternalGetVisiblePosition( );

                // MRS 5/14/2009 - TFS17620
                // Ripped this out because it's causing performance (TFS17620) and serilalization (TFS17457) issues.
                //
                //// MBS 10/7/08
                //// Discovered while addressing TFS8698.  If we set the visible position
                //// of a column, it is possible that this causes a row's height to change
                //// (such as if the summaries in a GroupByRow need to wrap), but the 
                //// existing metrics have not been recalculated so we will incorrectly
                //// clip data.  This will ensure that we have the updated metrics.
                //if (this.Column != null && this.Column.ParentCollection != null)
                //    this.Column.ParentCollection.RecalculateMetrics();

				// SSP 4/11/07 BR21669
				// If raiseColPosChangedNotifications is true then raise the Before and After events.
				// 
				if ( beforeColPosChangedEventRaised )
				{
					AfterColPosChangedEventArgs eventArgs = new AfterColPosChangedEventArgs( PosChanged.Moved, new ColumnHeader[] { this } );
					UltraGridBase grid = null != this.Layout ? this.Layout.Grid : null;
					if ( null != grid )
						grid.FireAfterColPosChanged( eventArgs );
				}
			}
		}
		
		/// <summary>
		/// Returns true if the visible position doesn't match the column index
		/// </summary>
		protected override bool ShouldSerializeVisiblePosition()
		{
			if ( this.column != null )
			{
				// JJD 11/26/01
				// If the column is part of a group then we don't need to serialize
				// its visible position since that will get serialized as part of the
				// group columns collection serialization logic.
				//
				if ( this.column.Group != null  ||
					 this.column.GroupId != 0 )
					return false;
			}

			int defaultPosition = this.DefaultVisiblePosition;

			if ( defaultPosition < 0 )
				return ( this.SerializedVisiblePosition >= 0 );
			
			// SSP 7/15/04 UWG3496
			// Always serialize out the visible positions of columns because we want the
			// grid to show columns at runtime in the same order as they are at design time.
			// 
			//return ( this.VisiblePosition != defaultPosition ); 
			return true;
		}

		#region SizeResolved

		// SSP 6/25/03 - Row Layout Functionality
		// Added SizeResolved method.
		//
		/// <summary>
		/// Resolved header size. This property returns the actual width and the height of the header.
		/// </summary>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override Size SizeResolved 
		{
			get
			{
				UltraGridBand band = this.Band;

				Debug.Assert( null != band, "No band !" );
				if ( null == band )
					return Size.Empty;

				if ( band.UseRowLayoutResolved )
				{
					return this.Column.RowLayoutColumnInfo.CachedItemRectHeader.Size;
				}
				else
				{
					return new Size( this.Column.Width, this.Height );
				}
			}
		}

		#endregion // SizeResolved

		// JJD 12/12/03 - Added Accessibility support
		#region ColumnHeaderAccessibleObject Class

		/// <summary>
		/// The Accessible object for a column header.
		/// </summary>
		public class ColumnHeaderAccessibleObject : HeaderBase.HeaderAccessibleObjectBase
		{
			#region Private Members

			#endregion Private Members

			#region Constructor
			
			/// <summary>
			/// Constructor
			/// </summary>
            /// <param name="columnHeader">The column header.</param>
            /// <param name="parent">The parent AccessibleObject</param>
			public ColumnHeaderAccessibleObject ( ColumnHeader columnHeader, HeadersCollection.HeadersAccessibleObject parent ) : base( columnHeader, parent )
			{
			}

			#endregion Constructor

			#region Base Class Overrides

				#region DefaultAction

			/// <summary>
			/// Gets a string that describes the default action of the object.
			/// </summary>
			public override string DefaultAction
			{
				get
				{
					if ( this.IsSortingAllowed )
						return Shared.SR.GetString("DefaultAction_SortColumn");
					
					return null;
				}
			}

				#endregion DefaultAction

				#region DoDefaultAction

			/// <summary>
			/// Performs the default action associated with this accessible object.
			/// </summary>
			public override void DoDefaultAction()
			{
				if ( this.IsSortingAllowed )
				{
					this.Column.ClickSortIndicator();
					this.GetMarshallingControl().Invalidate();
				}
			}

				#endregion DoDefaultAction

				#region GetChild

			/// <summary>
			/// Retrieves the accessible child corresponding to the specified index.
			/// </summary>
			/// <param name="index">The zero-based index of the accessible child.</param>
			/// <returns>An AccessibleObject that represents the accessible child corresponding to the specified index.</returns>
			public override AccessibleObject GetChild(int index)
			{
				return null;
			}

				#endregion GetChild

				#region GetChildCount

			/// <summary>
			/// Retrieves the number of children belonging to an accessible object.
			/// </summary>
			/// <returns>The number of children belonging to an accessible object.</returns>
			public override int GetChildCount()
			{
				return 0;
			}

				#endregion GetChildCount

				#region GetFocused

			/// <summary>
			/// Retrieves the object that has the keyboard focus.
			/// </summary>
			/// <returns>An AccessibleObject that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (Nothing in Visual Basic) if no object has focus.</returns>
			public override AccessibleObject GetFocused()
			{
				return null;
			}

				#endregion GetFocused

				#region GetSelected

			/// <summary>
			/// Retrieves the currently selected child.
			/// </summary>
			/// <returns>An AccessibleObject that represents the currently selected child. This method returns the calling object if the object itself is selected. Returns a null reference (Nothing in Visual Basic) if is no child is currently selected and the object itself does not have focus.</returns>
			public override AccessibleObject GetSelected()
			{
				
				return null;
			}

				#endregion GetSelected

				#region Navigate

            /// <summary>
            /// Navigates to another accessible object.
            /// </summary>
            /// <param name="navdir">One of the <see cref="System.Windows.Forms.AccessibleNavigation"/> values.</param>
            /// <returns>An AccessibleObject relative to this object.</returns>
            public override AccessibleObject Navigate(AccessibleNavigation navdir)
			{
				
				switch ( navdir )
				{
						
						#region case FirstChild

					case AccessibleNavigation.FirstChild:
						return null;

						#endregion case FirstChild

						
						#region case LastChild

					case AccessibleNavigation.LastChild:
					{
						return null;
					}

						#endregion case LastChild

				}

				// SSP - 1/14/04
				// Added support for group and column headers. Commented out the original code and added
				// the new code.
				//

				NavigateType navType = NavigateType.Topmost;
				switch ( navdir )
				{
					case AccessibleNavigation.Next:
					{
						int index = this.ParentInternal.Headers.IndexOf( this.Header );
						return index >= 0 && 1 + index < this.ParentInternal.Headers.Count
							// MD 8/10/07 - 7.3 Performance
							// Use generics
							//? this.ParentInternal.GetAccessibleObjectFor( (HeaderBase)this.ParentInternal.Headers[ 1 + index ] )
							? this.ParentInternal.GetAccessibleObjectFor( this.ParentInternal.Headers[ 1 + index ] )
							: null;
					}

					case AccessibleNavigation.Previous:
					{
						int index = this.ParentInternal.Headers.IndexOf( this.Header );
						return index > 0 
							// MD 8/10/07 - 7.3 Performance
							// Use generics
							//? this.ParentInternal.GetAccessibleObjectFor( (HeaderBase)this.ParentInternal.Headers[ index - 1 ] )
							? this.ParentInternal.GetAccessibleObjectFor( this.ParentInternal.Headers[ index - 1 ] )
							: null;
					}

					case AccessibleNavigation.Down:
					case AccessibleNavigation.Up:
					{
						HeaderBase result = null;
						// MD 1/20/09 - Groups in RowLayout
						//if ( this.Header.Band.GroupsDisplayed && null != this.Column.Group )
						if ( this.Header.Band.GroupsDisplayed 
							&& this.Header.Band.RowLayoutStyle != RowLayoutStyle.GroupLayout
							&& null != this.Column.Group )
						{
							UltraGridGroup group = this.Column.Group;							
							int maxArea = 0;

							int columnLeft = this.Column.RelativeOrigin;
							int columnRight = columnLeft + this.Column.Group.OverallWidth;

							for ( int i = 0; i < group.Columns.Count; i++ )
							{
								UltraGridColumn col = group.Columns[i];
								if ( AccessibleNavigation.Up == navdir && col.Level < this.Column.Level 
									|| AccessibleNavigation.Down == navdir && col.Level > this.Column.Level )
								{
									int left = col.RelativeOrigin;
									int right = left + col.Group.OverallWidth;
									int tmp = Math.Min( right, columnRight ) - Math.Max( left, columnLeft );

									if ( tmp > maxArea )
									{
										maxArea = tmp;
										result = col.Header;
									}
								}
							}

							if ( null != result )
								return this.ParentInternal.GetAccessibleObjectFor( result );

							// If there is no column above, then return the group header since that's what would
							// be positioned above.
							//
							if ( AccessibleNavigation.Up == navdir && ! this.Column.Group.Header.Hidden )
								return this.ParentInternal.GetAccessibleObjectFor( this.Column.Group.Header );

							return null;
						}

						if ( this.Column.Band.UseRowLayoutResolved )
						{
							HeadersCollection headers = this.Column.Band.LayoutOrderedVisibleColumnHeadersVertical;
							int index = null != headers ? headers.IndexOf( this.Column.Header ) : -1 ;							
							
							if ( AccessibleNavigation.Up == navdir && index > 0 )
								result = headers[ index - 1 ];

							if ( AccessibleNavigation.Down == navdir && index >= 0 && 1 + index < headers.Count )
								result = headers[ 1 + index ];

							if ( null != result )
							{
								RowLayoutColumnInfo ci1 = this.Column.RowLayoutColumnInfo;
								RowLayoutColumnInfo ci2 = result.Column.RowLayoutColumnInfo;
									
								bool horizontallyIntersects = Math.Min( ci1.OriginXResolved + ci1.SpanXResolved, ci2.OriginXResolved + ci2.SpanXResolved ) 
									- Math.Max( ci1.OriginXResolved, ci2.OriginXResolved ) > 0;

								if ( ! horizontallyIntersects )
									result = null;									
							}
						}

						// If we have no column above, then go to the band header if it's visible.
						//
						if ( null == result && AccessibleNavigation.Up == navdir && ! this.Column.Band.Header.Hidden )
							result = this.Column.Band.Header;

						return null != result
							? this.ParentInternal.GetAccessibleObjectFor( result )
							: null;
					}						

					case AccessibleNavigation.Right:
					{
						// MD 1/20/09 - Groups in RowLayout
						//if ( this.Header.Band.GroupsDisplayed && null != this.Column.Group && this.Column.LastItemInLevel )
						if ( this.Header.Band.GroupsDisplayed 
							&& this.Header.Band.RowLayoutStyle != RowLayoutStyle.GroupLayout
							&& null != this.Column.Group 
							&& this.Column.LastItemInLevel )
						{
							UltraGridGroup group = this.Column.Group.GetRelatedVisibleGroup( VisibleRelation.Next );
							if ( null != group )
							{
								for ( int i = 0; i < group.Columns.Count; i++ )
								{
									UltraGridColumn col = group.Columns[i];
									if ( ! col.Header.Hidden && col.Level == this.Column.Level )
										return this.ParentInternal.GetAccessibleObjectFor( col.Header );
								}
							}

							return null;
						}

						navType = NavigateType.Next;
						break;
					}

					case AccessibleNavigation.Left:
					{
						// MD 1/20/09 - Groups in RowLayout
						//if ( this.Header.Band.GroupsDisplayed && null != this.Column.Group && this.Column.FirstItemInLevel )
						if ( this.Header.Band.GroupsDisplayed
							&& this.Header.Band.RowLayoutStyle != RowLayoutStyle.GroupLayout
							&& null != this.Column.Group 
							&& this.Column.FirstItemInLevel )
						{
							UltraGridGroup group = this.Column.Group.GetRelatedVisibleGroup( VisibleRelation.Previous );
							if ( null != group )
							{
								for ( int i = 0; i < group.Columns.Count; i++ )
								{
									UltraGridColumn col = group.Columns[i];
									if ( ! col.Header.Hidden && col.LastItemInLevel && col.Level == this.Column.Level )
										return this.ParentInternal.GetAccessibleObjectFor( col.Header );
								}
							}

							return null;
						}

						navType = NavigateType.Prev;
						break;
					}
				}

				AccessibleObject acc = null;

				if ( navType != NavigateType.Topmost )
				{
					UltraGridColumn navigateToColumn = this.Column.Band.GetRelatedVisibleCol( this.Column, navType, true );

					if ( navigateToColumn == this.Column )
						acc = null;

					acc = this.ParentInternal.GetAccessibleObjectFor( navigateToColumn.Header );

					if ( null != acc && !this.Bounds.IsEmpty && !acc.Bounds.IsEmpty )
					{
						if ( navdir == AccessibleNavigation.Left &&
							acc.Bounds.Right > this.Bounds.Left )
							acc = null;
						else if ( navdir == AccessibleNavigation.Right &&
							acc.Bounds.Left < this.Bounds.Right )
							acc = null;
					}
				}

				return acc;


				// Original Code:
				
			}

				#endregion Navigate

				#region Role

			/// <summary>
			/// Gets the role of this accessible object.
			/// </summary>
			public override AccessibleRole Role
			{
				get
				{
					return AccessibleRole.ColumnHeader;
				}
			}

				#endregion Role

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region Column

			/// <summary>
			/// Returns the associated column.
			/// </summary>
			public UltraGridColumn Column 
			{ 
				get 
				{
					return ((ColumnHeader)this.Header).Column;
				} 
			}

					#endregion Column

				#endregion Public Properties

			#region Private Properties

			#region IsSortingAllowed
			
			private bool IsSortingAllowed
			{
				get
				{
					if ( !this.Column.Header.Hidden && 
						!this.Column.Header.IsDisabled &&
						this.Column.Header.SortIndicator != SortIndicator.Disabled)
					{
						if ( this.Column.Band.HeaderClickActionResolved == HeaderClickAction.SortSingle ||
							this.Column.Band.HeaderClickActionResolved == HeaderClickAction.SortMulti)
							return true;
					}

					return false;
				}
			}

			#endregion // IsSortingAllowed

			#endregion // Private Properties

			#endregion Properties

		}

		#endregion ColumnHeaderAccessibleObject Class

		// MRS 4/14/05 - BR03343
		// Added this handy property for determining if the column
		// header is hidden regardless of RowLayout mode. 
		#region HiddenResolved
		internal bool HiddenResolved
		{
			get
			{
				// SSP 8/16/06 BR05387
				// Commented out the original code and added new code.
				//
				// ----------------------------------------------------------------
				UltraGridColumn column = this.Column;
				UltraGridBand band = this.Band;
				if ( null != band && null != column )
				{
					if ( this.Hidden )
						return true;

					if ( band.UseRowLayoutResolved )
					{
						if ( LabelPosition.None == column.RowLayoutColumnInfo.LabelPositionResolved )
							return true;
					}
					else if ( ! band.ColHeadersVisible )
						return true;
				}

				return false;

				
				// ----------------------------------------------------------------
			}
		}
		#endregion HiddenResolved

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		#region DefaultTextOrientation

		internal override TextOrientationInfo DefaultTextOrientation
		{
			get
			{
				UltraGridBand band = this.Band;

				if ( band == null )
					return null;

				return band.ColumnHeaderTextOrientationResolved;
			}
		} 

		#endregion DefaultTextOrientation

        // CDS NAS v9.1 Header CheckBox
        #region ShouldSerialize

        internal protected override bool ShouldSerialize()
        {
            return base.ShouldSerialize() ||
                // CDS NAS v9.1 Header CheckBox
                this.ShouldSerializeCheckBoxAlignment() ||
                this.ShouldSerializeCheckBoxSynchronization() ||
                this.ShouldSerializeCheckBoxVisibility();
        }

        #endregion ShouldSerialize

        // CDS NAS v9.1 Header CheckBox
        #region Reset

        /// <summary>
        /// Resets all properties back to their default values.
        /// </summary>
        public override void Reset()
        {
            base.Reset();

            // CDS NAS v9.1 Header CheckBox
            this.ResetCheckBoxAlignment();
            this.ResetCheckBoxSynchronization();
            this.ResetCheckBoxVisibility();
        }

        #endregion Reset

        // CDS NAS v9.1 Header CheckBox
        #region NAS v9.1 Header CheckBox

        // CDS NAS v9.1 Header CheckBox
        private HeaderCheckBoxVisibility checkBoxVisibility = HeaderCheckBoxVisibility.Default;
        private HeaderCheckBoxAlignment checkBoxAlignment = HeaderCheckBoxAlignment.Default;
        private HeaderCheckBoxSynchronization checkBoxSynchronization = HeaderCheckBoxSynchronization.Default;

        // CDS NAS v9.1 Header CheckBox
        #region CheckBoxVisibility

        /// <summary>
        /// Determines whether the Header checkbox is visible.
        /// </summary>
        [LocalizedDescription("LD_HeaderCheckBoxVisibility")]
        public HeaderCheckBoxVisibility CheckBoxVisibility
        {
            get
            {
                return this.checkBoxVisibility;
            }
            set
            {
                if (this.checkBoxVisibility != value)
                {
                    GridUtils.ValidateEnum(typeof(HeaderCheckBoxVisibility), value);

                    this.checkBoxVisibility = value;
                    this.DirtyHeaderUIElements(null);
                    this.NotifyPropChange(PropertyIds.HeaderCheckBoxVisibility, null);
                }
            }
        }

        #endregion CheckBoxVisibility

        // CDS NAS v9.1 Header CheckBox
        #region CheckBoxAlignment

        /// <summary>
        /// Determines the position of the Header checkbox relative to the header caption.
        /// </summary>
        [LocalizedDescription("LD_HeaderCheckBoxAlignment")]
        public HeaderCheckBoxAlignment CheckBoxAlignment
        {
            get
            {
                return this.checkBoxAlignment;
            }
            set
            {

                if (this.checkBoxAlignment != value)
                {
                    GridUtils.ValidateEnum(typeof(HeaderCheckBoxAlignment), value);

                    this.checkBoxAlignment = value;

                    StyleUtils.DirtyCachedPropertyVals(this.Band);
                    this.DirtyHeaderUIElements(null);
                    this.NotifyPropChange(PropertyIds.HeaderCheckBoxAlignment, null);
                }
            }
        }

        #endregion CheckBoxAlignment

        // CDS NAS v9.1 Header CheckBox
        #region CheckBoxSynchronization

        /// <summary>
        /// Determines which cells will be synchronized with the Header checkbox.
        /// </summary>
        [LocalizedDescription("LD_HeaderCheckBoxSynchronization")]
        public HeaderCheckBoxSynchronization CheckBoxSynchronization
        {
            get
            {
                return this.checkBoxSynchronization;
            }
            set
            {
                if (this.checkBoxSynchronization != value)
                {
                    GridUtils.ValidateEnum(typeof(HeaderCheckBoxSynchronization), value);

                    this.checkBoxSynchronization = value;
                    this.NotifyPropChange(PropertyIds.HeaderCheckBoxSynchronization, null);
                }
            }
        }

        #endregion CheckBoxSynchronization

        // CDS NAS v9.1 Header CheckBox
        #region CheckBoxVisibilityResolved

        /// <summary>
        /// Returns the HeaderCheckBoxVisibility that will actually be used for ColumnHeader's CheckBoxVisiblity
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public HeaderCheckBoxVisibility CheckBoxVisibilityResolved
        {
            get
            {
                if (this.ShouldSerializeCheckBoxVisibility())
                    return this.checkBoxVisibility;

                if (null != this.Band)
                    return this.Band.HeaderCheckBoxVisibilityResolved;

                return HeaderCheckBoxVisibility.Never;
            }
        }

        #endregion CheckBoxVisibilityResolved

        // CDS NAS v9.1 Header CheckBox
        #region CheckBoxAlignmentResolved

        /// <summary>
        /// Returns the HeaderCheckBoxAlignment that will actually be used for ColumnHeader's CheckBoxAlignment
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public HeaderCheckBoxAlignment CheckBoxAlignmentResolved
        {
            get
            {

                HeaderCheckBoxAlignment fallbackVal = HeaderCheckBoxAlignment.Default;
                // check on the column level
                if (this.ShouldSerializeCheckBoxAlignment())
                {
                    if (this.checkBoxAlignment == HeaderCheckBoxAlignment.Center &&
                        this.Caption.Length > 0)
                        return this.DefaultHeaderCheckBoxAlignmentBasedOnTextOrientationResolved;
                    else
                        return this.checkBoxAlignment;
                }

                if (this.Band != null)
                {

                    fallbackVal = this.Band.HeaderCheckBoxAlignmentResolved;
                    if (fallbackVal == HeaderCheckBoxAlignment.Default)
                        fallbackVal = this.DefaultHeaderCheckBoxAlignmentBasedOnTextOrientationResolved;

                    if ((HeaderCheckBoxAlignment)fallbackVal == HeaderCheckBoxAlignment.Center &&
                        this.Caption.Length > 0)
                        fallbackVal = this.DefaultHeaderCheckBoxAlignmentBasedOnTextOrientationResolved;

                    return fallbackVal;
                }
                return this.DefaultHeaderCheckBoxAlignmentBasedOnTextOrientationResolved;
            }
        }

        #endregion CheckBoxAlignmentResolved

        // CDS NAS v9.1 Header CheckBox
        #region CheckBoxSynchronizationResolved

        /// <summary>
        /// Returns the HeaderCheckBoxSynchronization that will actually be used for ColumnHeader's CheckBoxSynchronization
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public HeaderCheckBoxSynchronization CheckBoxSynchronizationResolved
        {
            get
            {
                HeaderCheckBoxSynchronization resolvedSynchronization = HeaderCheckBoxSynchronization.Default;

                // CDS 2/24/09 TFS12452 - Base initial synchronization off of visibility
                //if (!this.column.IsColumnDataTypeRepresentableByCheckBox)
                if (!this.IsHeaderCheckBoxVisible)
                    resolvedSynchronization = HeaderCheckBoxSynchronization.None;
                else if (this.ShouldSerializeCheckBoxSynchronization())   
                    resolvedSynchronization = this.checkBoxSynchronization;
                else if (this.Band != null)
                {
                    resolvedSynchronization = this.Band.HeaderCheckBoxSynchronizationResolved;
                }
                if (resolvedSynchronization == HeaderCheckBoxSynchronization.Default)
                {
					// MD 2/24/09 - TFS12452
					// If we resolved to a default check box synchronization, use RowsCollection when a CheckEditor is used and None otherwise.
                    //if (this.IsHeaderCheckBoxVisible)
					if ( this.column != null && this.column.Editor is CheckEditor )
                        resolvedSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
                    else
                        resolvedSynchronization = HeaderCheckBoxSynchronization.None;
                }

                Debug.Assert(resolvedSynchronization != HeaderCheckBoxSynchronization.Default, "The resolved synchronization should never be Default when it gets to here.");

                // When HeaderPlacent is FixedToTop, we cannot synch by Rows, so synch by Band
                if (resolvedSynchronization == HeaderCheckBoxSynchronization.RowsCollection &&
                   this.Band.HeaderPlacementResolved == HeaderPlacement.FixedOnTop)
                    resolvedSynchronization = HeaderCheckBoxSynchronization.Band;

                return resolvedSynchronization;
            }
        }

        #endregion CheckBoxSynchronizationResolved

        // CDS NAS v9.1 Header CheckBox
        #region DefaultHeaderCheckBoxAlignmentBasedOnTextOrientationResolved

        private HeaderCheckBoxAlignment DefaultHeaderCheckBoxAlignmentBasedOnTextOrientationResolved
        {
            get
            {
                if (this.Caption.Length == 0)
                    return HeaderCheckBoxAlignment.Center;

                if (this.TextOrientationResolved == null ||
                    this.TextOrientationResolved.FlowDirection == TextFlowDirection.Horizontal)
                    return HeaderCheckBoxAlignment.Left;
                else
                    return HeaderCheckBoxAlignment.Top;
            }
        }

        #endregion DefaultHeaderCheckBoxAlignmentBasedOnTextOrientationResolved

        // CDS NAS v9.1 Header CheckBox
        #region GetHeaderUIElements

        internal List<HeaderUIElement> GetHeaderUIElements(RowsCollection rows)
        {
            List<HeaderUIElement> headerElementList = new List<HeaderUIElement>();
            // 1/14/08 CDS TFS12429 Put in a null check.
            if (this.Layout == null ||
                this.Layout.UIElement == null ||
                this.column == null)
                return headerElementList;

            DataAreaUIElement dataAreaUIElement = this.Layout.UIElement.GetDescendant(typeof(DataAreaUIElement)) as DataAreaUIElement;
            if (dataAreaUIElement == null)
                return headerElementList;

            foreach (UIElement element in dataAreaUIElement.ChildElements)
            {
                if ((element is RowColRegionIntersectionUIElement) == false)
                    continue;

                foreach (UIElement childElement in element.ChildElements)
                {
                    BandHeadersUIElement bandHeadersUIElement = childElement as BandHeadersUIElement;
                    if (bandHeadersUIElement != null)
                    {
                        if (null != rows &&
                            bandHeadersUIElement.GetContext(typeof(RowsCollection)) != rows)
                            continue;

                        foreach (UIElement bandHeadersChildElement in bandHeadersUIElement.ChildElements)
                        {
                            HeaderUIElement headerElement = bandHeadersChildElement as HeaderUIElement;
                            if (headerElement == null || headerElement.Header.Column != this.column)
                                continue;

                            headerElementList.Add(headerElement);
                        }
                        continue;
                    }

                    //find headers within cardview
                    if (childElement is CardAreaUIElement)
                    {
                        foreach (UIElement cardAreaChildElement in childElement.ChildElements)
                        {
                            if ((cardAreaChildElement is CardAreaScrollRegionUIElement) == false )
                                continue;

                            foreach (UIElement cardAreaScrollRegionChildElement in cardAreaChildElement.ChildElements)
                            {
                                if ((cardAreaScrollRegionChildElement is CardLabelAreaUIElement) == false)
                                    continue;

                                foreach (UIElement cardLabelAreaChildElement in cardAreaScrollRegionChildElement.ChildElements)
                                {
                                    CardLabelUIElement cardLabelElement = cardLabelAreaChildElement as CardLabelUIElement;

                                    if (cardLabelElement == null || cardLabelElement.Header.Column != this.column)
                                        continue;

                                    headerElementList.Add(cardLabelElement);
                                }
                            }                                    
                        }
                        continue;
                    }
                }
            }
            return headerElementList;
        }

        #endregion GetHeaderUIElements

        // CDS NAS v9.1 Header CheckBox
        #region DirtyHeaderUIElements

        internal void DirtyHeaderUIElements(RowsCollection rows)
        {
            System.Collections.Generic.List<HeaderUIElement> headerList = this.GetHeaderUIElements(rows);
            foreach (HeaderUIElement headerElement in headerList)
                headerElement.DirtyChildElements();
        }

        #endregion DirtyHeaderUIElements

        // CDS NAS v9.1 Header CheckBox
        #region ContainsVisibleHeaderCheckBoxOnLeftOrRight

        internal bool ContainsVisibleHeaderCheckBoxOnLeftOrRight
        {
            get
            {
                HeaderCheckBoxAlignment resolvedAlignment = this.CheckBoxAlignmentResolved;
                if (!this.HiddenResolved &&
                    this.IsHeaderCheckBoxVisible &&
                    (resolvedAlignment == HeaderCheckBoxAlignment.Left ||
                    resolvedAlignment == HeaderCheckBoxAlignment.Right ||
                    resolvedAlignment == HeaderCheckBoxAlignment.Center))
                    return true;
                return false;
            }
        }

        #endregion ContainsVisibleHeaderCheckBoxOnLeftOrRight

        // CDS NAS v9.1 Header CheckBox
        #region IsHeaderCHeckBoxVisible

        internal bool IsHeaderCheckBoxVisible
        {
            get
            {
                HeaderCheckBoxVisibility visibility = this.CheckBoxVisibilityResolved;
                if (visibility == HeaderCheckBoxVisibility.Always ||
                    (visibility == HeaderCheckBoxVisibility.WhenUsingCheckEditor &&
                    // CDS 2/24/09 TFS12452 No longer using DataType. Instead, we check for CheckEditor
                    //((this.column != null) ? this.column.IsColumnDataTypeRepresentableByCheckBox : false)))
                    ((this.column != null) ? this.column.Editor is CheckEditor : false)))
                    return true;
                return false;
            }
        }

        #endregion IsHeaderCHeckBoxVisible

        #region Serialization code

        // CDS NAS v9.1 Header CheckBox
        #region ShouldSerializeCheckBoxVisibility

        private bool ShouldSerializeCheckBoxVisibility()
        {
            return HeaderCheckBoxVisibility.Default != this.checkBoxVisibility;
        }

        #endregion // ShouldSerializeCheckBoxVisibility

        // CDS NAS v9.1 Header CheckBox
        #region ResetCheckBoxVisibility

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetCheckBoxVisibility()
        {
            this.CheckBoxVisibility = HeaderCheckBoxVisibility.Default;
        }

        #endregion // ResetCheckBoxVisibility

        // CDS NAS v9.1 Header CheckBox
        #region ShouldSerializeCheckBoxAlignment

        private bool ShouldSerializeCheckBoxAlignment()
        {
            return HeaderCheckBoxAlignment.Default != this.checkBoxAlignment;
        }

        #endregion // ShouldSerializeCheckBoxAlignment

        // CDS NAS v9.1 Header CheckBox
        #region ResetCheckBoxAlignment

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetCheckBoxAlignment()
        {
            this.CheckBoxAlignment = HeaderCheckBoxAlignment.Default;
        }

        #endregion // ResetCheckBoxAlignment

        // CDS NAS v9.1 Header CheckBox
        #region ShouldSerializeCheckBoxSynchronization

        private bool ShouldSerializeCheckBoxSynchronization()
        {
            return HeaderCheckBoxSynchronization.Default != this.checkBoxSynchronization;
        }

        #endregion // ShouldSerializeCheckBoxSynchronization

        // CDS NAS v9.1 Header CheckBox
        #region ResetCheckBoxSynchronization

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetCheckBoxSynchronization()
        {
            this.CheckBoxSynchronization = HeaderCheckBoxSynchronization.Default;
        }

        #endregion // ResetCheckBoxSynchronization

        #endregion Serialization code

        // CDS 1/27/09 TFS12927 Checkbox in Header properties are the first new properties on the ColumnHeader
        // so we need to override InitializeFrom and assign them appropriately. Otherwise, design-time changes will not take affect.
        #region InitializeFrom

        internal override void InitializeFrom(HeaderBase source, PropertyCategories propertyCategories)
        {
            base.InitializeFrom(source, propertyCategories);

            ColumnHeader sourceColumnHeader = source as ColumnHeader;
            if ( sourceColumnHeader != null )
            {
                this.checkBoxAlignment = sourceColumnHeader.checkBoxAlignment;
                this.checkBoxSynchronization = sourceColumnHeader.checkBoxSynchronization;
                this.checkBoxVisibility = sourceColumnHeader.checkBoxVisibility;
            }
        }

        #endregion InitializeFrom

        #endregion NAS v9.1 Header CheckBox

    }
}
