#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections;
using Infragistics.Shared;
using System.Drawing;
using Infragistics.Win;
using Infragistics.Win.AppStyling;
using Infragistics.Win.AppStyling.Definitions;
using System.Collections.Generic;

namespace Infragistics.Win.UltraWinGrid
{
	#region UltraGridBaseRole Class

	/// <summary>
	/// Role class for the <see cref="UltraGridBase"/> derived controls.
	/// </summary>
	public class UltraGridRole : UltraControlRole
	{
		internal const string GRID_ROLE_NAME = "UltraGrid";
		internal const string COMBO_ROLE_NAME = "UltraCombo";
		internal const string DROPDOWN_ROLE_NAME = "UltraDropDown";
		internal const string COLUMN_CHOOSER_ROLE_NAME = "UltraGridColumnChooser";        

		#region Private Vars

		private UltraGridBase grid = null;
		private string[] cachedRoleNames = null;

		#endregion // Private Vars

		#region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="grid"></param>
		public UltraGridRole( UltraGridBase grid ) 
			: base( grid, StyleUtils.GetComponentRoleName( StyleUtils.GetComponentType( grid ) ) )
		{
			this.grid = grid;

			// SSP 5/26/06 BR13063
			// Due to printing where we have multiple layouts using the same component role, we need to
			// cache the properties at each layout. Therefore there is no need to cache properties at
			// component role level. Commented out the following line of code.
			// 
			//this.PropertiesCacheCount = 1 + (int)StyleUtils.CachedProperty.LastValue;
		}
		#endregion // Constructor

		#region Grid

		/// <summary>
		/// Returns the associated UltraGridBase instance.
		/// </summary>
		public UltraGridBase Grid
		{
			get
			{
				return this.grid;
			}
		}

		#endregion // Grid

		#region Overridden Properties/Methods

		#region OnStyleChanged

		/// <summary>
		/// Invoked when the style information for the component has changed.
		/// </summary>
		protected override void OnStyleChanged( )
		{
			base.OnStyleChanged( );

            // MBS 7/10/09 - TFS19046
            UltraCombo combo = this.Component as UltraCombo;
            if(combo != null)
                combo.OnAppStyleChanged();

			if ( null != this.grid.DisplayLayout )
				this.grid.DisplayLayout.OnAppStyleChanged( );
		}

		#endregion // OnStyleChanged

		#region GetRoleNames

		/// <summary>
		/// Used by the associated <see cref="ComponentRole"/> to determine which <see cref="Infragistics.Win.AppStyling.UIRole"/> instances should be cached.
		/// </summary>
		/// <returns>An array of strings containing the names of the role names that should be cached.</returns>
		protected override string[] GetRoleNames( )
		{
			if ( null == this.cachedRoleNames )
				this.cachedRoleNames = StyleUtils.GetSortedRoleNamesForCaching( );

			return this.cachedRoleNames;
		}

		#endregion //GetRoleNames        

		#endregion // Overridden Properties/Methods
	}

	#endregion // UltraGridBaseRole Class

    // MBS 4/22/08 - RowEditTemplate NA2008 V2
    // As it turns out we also need a separate role for the proxy since it needs to cache its own
    // properties for AppStyling
    #region UltraGridCellProxyRole

    /// <summary>
    /// Component role used by an <see cref="UltraGridCellProxy"/> for its application styling information.
    /// </summary>
    public class UltraGridCellProxyRole : UltraControlRole
    {
        #region Members

        private string[] cachedRoleNames;
        private UltraGridCellProxy proxy;

        internal const int PropertyBorderStyle = 0;
        internal const int PropertyButtonStyle = 1;
        internal const int UseGridAppearences = 2;
        internal const string GRIDCELLPROXY_ROLE_NAME = "UltraGridCellProxy";

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the role.
        /// </summary>
        /// <param name="proxy">The associated proxy</param>
        public UltraGridCellProxyRole(UltraGridCellProxy proxy)
            : base(proxy, GRIDCELLPROXY_ROLE_NAME)
        {
            this.proxy = proxy;

            this.PropertiesCacheCount = 3;
        }
        #endregion //Constructor

        #region Base Class Overrides

        #region GetRoleNames

        /// <summary>
        /// Used by the associated <see cref="ComponentRole"/> to determine which <see cref="Infragistics.Win.AppStyling.UIRole"/> instances should be cached.
        /// </summary>
        /// <returns>An array of strings containing the names of the role names that should be cached.</returns>
        protected override string[] GetRoleNames()
        {
            if (this.cachedRoleNames == null)
                this.cachedRoleNames = StyleUtils.GetSortedRoleNamesForCaching(StyleUtils.ComponentType.CellProxy);

            return this.cachedRoleNames;
        }
        #endregion //GetRoleNames

        #region OnStyleChanged

        /// <summary>
        /// Invoked when the style information for the component has changed.
        /// </summary>
        protected override void OnStyleChanged()
        {
            if (this.proxy.IsDisposed == false)
                this.proxy.OnAppStyleChanged();
        }
        #endregion //OnStyleChanged

        #endregion //Base Class Overrides
    }
    #endregion //UltraGridCellProxyRole

    // MBS 4/18/08 - RowEditTemplate NA2008 V2
    // We need a separate role for the template because it does not derive from UltraControlBase
    #region UltraGridRowEditTemplateRole

    /// <summary>
    /// Component role used by an <see cref="UltraGridRowEditTemplate"/> for its application styling information.
    /// </summary>
    public class UltraGridRowEditTemplateRole : ComponentRole
    {
        #region Members

        private UltraGridRowEditTemplate template;
        private string[] cachedRoleNames;
        internal const string ROWEDITTEMPLATE_ROLE_NAME = "UltraGridRowEditTemplate";        

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Initializes a new <see cref="UltraGridRowEditTemplateRole"/>
        /// </summary>
        /// <param name="template">Associated template for which the component role is being created.</param>
        /// <param name="componentRoleName">Name of the component role that will be used to locate the associated application style information for the component level properties.</param>
        public UltraGridRowEditTemplateRole(UltraGridRowEditTemplate template, string componentRoleName)
            : base(template, componentRoleName)
        {
            this.template = template;
        }
        #endregion //Constructor

        #region Base Class Overrides

        #region GetRoleNames

        /// <summary>
        /// Used by the associated <see cref="ComponentRole"/> to determine which <see cref="Infragistics.Win.AppStyling.UIRole"/> instances should be cached.
        /// </summary>
        /// <returns>An array of strings containing the names of the role names that should be cached.</returns>
        protected override string[] GetRoleNames()
        {
            if (this.cachedRoleNames == null)
                this.cachedRoleNames = StyleUtils.GetSortedRoleNamesForCaching(StyleUtils.ComponentType.RowEditTemplate);

            return this.cachedRoleNames;
        }
        #endregion //GetRoleNames

        #region OnStyleChanged

        /// <summary>
        /// Invoked when the style information for the component has changed.
        /// </summary>
        protected override void OnStyleChanged()
        {            
            if (this.template.IsDisposed == false)
            {
                this.template.OnAppStyleChanged();
            }
        }
        #endregion //OnStyleChanged

        #region SynchronizingObject

        /// <summary>
        /// Returns an object that can be used to synchronize calls to the associated component's thread.
        /// </summary>
        protected override ISynchronizeInvoke SynchronizingObject
        {
            get { return this.template; }
        }
        #endregion //SynchronizeObject

        #endregion //Base Class Overrides
    }
    #endregion //UltraGridRowEditTemplateRole

	#region UltraWinGridAssemblyStyleInfo Class

	/// <summary>
	/// Class used by the app styling infrastructure that provides the role and 
	/// component role defitions used by the assembly.
	/// </summary>
	/// <remarks>
	/// <p class="note"><b>Note:</b> A parameterless constructor is required for all derived classes.</p>
	/// </remarks>
	public class UltraWinGridAssemblyStyleInfo : AssemblyStyleInfo
	{
		/// <summary>
		/// Returns an array of objects that define the roles provided by an assembly.
		/// </summary>
		/// <returns></returns>
		/// <seealso cref="UIRoleDefinition"/>
		public override UIRoleDefinition[] GetRoles()
		{
			return StyleUtils.GetRoleDefinitions( StyleUtils.ComponentType.All );
		}


		/// <summary>
		/// Returns an array of objects that define the component roles provided by an assembly.
		/// </summary>
		/// <returns></returns>
		public override ComponentRoleDefinition[] GetComponents()
		{
			const SupportedComponentProperties UltraProps = SupportedComponentProperties.ResolutionOrder
                | SupportedComponentProperties.UseFlatMode
                | SupportedComponentProperties.UseOsThemes
                // MRS NAS v8.2 - Glyphs
                | SupportedComponentProperties.CheckBoxGlyphInfo
                | SupportedComponentProperties.RadioButtonGlyphInfo;

			ComponentRoleDefinition gridDef = new ComponentRoleDefinition( UltraGridRole.GRID_ROLE_NAME, typeof( UltraGrid ) );
			ComponentRoleDefinition uddDef = new ComponentRoleDefinition( UltraGridRole.DROPDOWN_ROLE_NAME, typeof( UltraDropDown ) );
			ComponentRoleDefinition comboDef = new ComponentRoleDefinition( UltraGridRole.COMBO_ROLE_NAME, typeof( UltraCombo ) );
			ComponentRoleDefinition colChooserDef = new ComponentRoleDefinition( UltraGridRole.COLUMN_CHOOSER_ROLE_NAME, typeof( ColumnChooserGrid ) );            

			gridDef.EditorType = ComponentRoleDefinition.AllEditors;
			gridDef.RelatedRoles = StyleUtils.GetRoleNames( StyleUtils.ComponentType.Grid );
			gridDef.UsesScrollbars = true;
			gridDef.UsesToolTips = true; // AS 5/4/06
			gridDef.SharedObjects = new String[] { SharedObjectNames.DragDropIndicatorManager }; // AS 5/15/06
			gridDef.CustomProperties = StyleUtils.GetCustomProperties( StyleUtils.ComponentType.Grid );
			gridDef.SupportedProperties = SupportedComponentProperties.ButtonStyle | SupportedComponentProperties.HeaderStyle 
				//| SupportedComponentProperties.ViewStyle
				| UltraProps;

			uddDef.EditorType = ComponentRoleDefinition.AllEditors;
			uddDef.RelatedRoles = StyleUtils.GetRoleNames( StyleUtils.ComponentType.DropDown );
			uddDef.UsesScrollbars = true;
			uddDef.UsesToolTips = true; // AS 5/4/06
			uddDef.CustomProperties = StyleUtils.GetCustomProperties( StyleUtils.ComponentType.DropDown );
            uddDef.SupportedProperties = SupportedComponentProperties.ButtonStyle | SupportedComponentProperties.HeaderStyle
                //| SupportedComponentProperties.ViewStyle
                | UltraProps;                

			comboDef.EditorType = ComponentRoleDefinition.AllEditors;
			comboDef.RelatedRoles = StyleUtils.GetRoleNames( StyleUtils.ComponentType.Combo );
			comboDef.UsesScrollbars = true;
			comboDef.UsesToolTips = true; // AS 5/4/06
			comboDef.CustomProperties = StyleUtils.GetCustomProperties( StyleUtils.ComponentType.Combo );
			comboDef.SupportedProperties = SupportedComponentProperties.ButtonStyle | SupportedComponentProperties.HeaderStyle 
				| SupportedComponentProperties.ViewStyle
				| UltraProps;

			colChooserDef.EditorType = typeof( Infragistics.Win.CheckEditor );
			colChooserDef.RelatedRoles = StyleUtils.GetRoleNames( StyleUtils.ComponentType.ColumnChooser );
			colChooserDef.UsesScrollbars = true;
			colChooserDef.UsesToolTips = true; // AS 5/4/06
			colChooserDef.CustomProperties = StyleUtils.GetCustomProperties( StyleUtils.ComponentType.ColumnChooser );
			colChooserDef.SupportedProperties = SupportedComponentProperties.HeaderStyle 
				//| SupportedComponentProperties.ViewStyle
				| UltraProps;

            // MBS 4/17/08 - RowEditTemplate NA2008 V2
            ComponentRoleDefinition templateDef = new ComponentRoleDefinition(UltraGridRowEditTemplateRole.ROWEDITTEMPLATE_ROLE_NAME, typeof(UltraGridRowEditTemplate));
            templateDef.RelatedRoles = StyleUtils.GetRoleNames(StyleUtils.ComponentType.RowEditTemplate);
            templateDef.SupportedProperties = SupportedComponentProperties.ResolutionOrder;
            templateDef.CustomProperties = StyleUtils.GetCustomProperties(StyleUtils.ComponentType.RowEditTemplate);
            //
            ComponentRoleDefinition proxyDef = new ComponentRoleDefinition(UltraGridCellProxyRole.GRIDCELLPROXY_ROLE_NAME, typeof(UltraGridCellProxy));
            proxyDef.RelatedRoles = StyleUtils.GetRoleNames(StyleUtils.ComponentType.CellProxy);
            proxyDef.EditorType = ComponentRoleDefinition.AllEditors;
            proxyDef.SupportedProperties = SupportedComponentProperties.UseOsThemes | SupportedComponentProperties.UseFlatMode | SupportedComponentProperties.ButtonStyle;
            proxyDef.CustomProperties = StyleUtils.GetCustomProperties(StyleUtils.ComponentType.CellProxy);
            //
			//return new ComponentRoleDefinition[] { gridDef, uddDef, comboDef, colChooserDef };
            return new ComponentRoleDefinition[] { gridDef, uddDef, comboDef, colChooserDef, templateDef, proxyDef };
		}
	}

	#endregion // UltraWinGridAssemblyStyleInfo Class

	#region StyleUtils Class

	internal sealed class StyleUtils
	{
		private StyleUtils( )
		{
		}

		#region GetRole

		internal static bool UseControlInfo( UltraGridLayout layout )
		{
			return GetResolutionOrder( layout ).UseControlInfo;
		}

		internal static ResolutionOrder GetResolutionOrderEnum( UltraGridLayout layout )
		{
			ComponentRole role = GetComponentRole( layout );
			return null != role ? role.ResolutionOrder : ResolutionOrder.Default;
		}

		internal static ResolutionOrderInfo GetResolutionOrder( UltraGridLayout layout )
		{
			return ComponentRole.GetResolutionOrderInfo( GetComponentRole( layout ) );
		}

		internal static UIRole GetRole( UltraGridBand band, Role role )
		{
			ResolutionOrderInfo resOrder;
			return GetRole( band, role, out resOrder );
		}

		internal static UIRole GetRole( UltraGridBand band, Role role, out ResolutionOrderInfo resOrder )
		{
			return GetRole( band.Layout, role, out resOrder );
		}

		internal static UIRole GetRole( UltraGridColumn column, Role role )
		{
			ResolutionOrderInfo resOrder;
			return GetRole( column, role, out resOrder );
		}

		internal static UIRole GetRole( UltraGridColumn column, Role role, out ResolutionOrderInfo resOrder )
		{
			return GetRole( column.Layout, role, out resOrder );
		}

		internal static UIRole GetRole( HeaderBase header, Role role, out ResolutionOrderInfo resOrder )
		{
			return GetRole( header.Band, role, out resOrder );
		}

		internal static UIRole GetRole( UltraGridLayout layout, Role role )
		{
			ResolutionOrderInfo resOrder;
			return GetRole( layout, role, out resOrder );
		}

		internal static UIRole GetRole( UltraGridLayout layout, Role role, out ResolutionOrderInfo resOrder )
		{
            // MBS 12/29/08 - TFS11941
            // We should be checking for null here
            if (layout == null || layout.Grid == null)
            {
                resOrder = new ResolutionOrderInfo(ResolutionOrder.Default);
                return null;
            }

			ComponentRole componentRole = layout.Grid.ComponentRole;
			resOrder = componentRole.GetResolutionOrderInfo( );
			return componentRole.GetRole( (int)role );
		}

        // MBS 4/17/08 - RowEditTemplate NA2008 V2
        internal static UIRole GetRole(UltraGridRowEditTemplate template, TemplateRole role)
        {
            ResolutionOrderInfo resOrder;
            return GetRole(template, role, out resOrder);
        }
        //
        internal static UIRole GetRole(UltraGridRowEditTemplate template, TemplateRole role, out ResolutionOrderInfo resOrder)
        {
            ComponentRole componentRole = template.ComponentRole;
            resOrder = componentRole.GetResolutionOrderInfo();
            return componentRole.GetRole((int)role);
        }
        //
        internal static UIRole GetRole(UltraGridCellProxy proxy, CellProxyRole role)
        {
            ResolutionOrderInfo resOrder;
            return GetRole(proxy, role, out resOrder);
        }
        //
        internal static UIRole GetRole(UltraGridCellProxy proxy, CellProxyRole role, out ResolutionOrderInfo resOrder)
        {
            ComponentRole componentRole = proxy.ComponentRole;
            resOrder = componentRole.GetResolutionOrderInfo();
            return componentRole.GetRole((int)role);
        }        


		#endregion // GetRole

		#region GetCachedPropertyVal

		internal static bool GetCachedPropertyVal( UltraGridLayout layout, CustomProperty prop, out object val )
		{
			return GetCachedPropertyVal( layout, (int)prop, out val );
		}

		internal static bool GetCachedPropertyVal( UltraGridLayout layout, CachedProperty prop, out object val )
		{
			return GetCachedPropertyVal( layout, (int)prop, out val );
		}

		private static bool GetCachedPropertyVal( UltraGridLayout layout, int prop, out object val )
		{
			// SSP 5/26/06 BR13063
			// Due to printing where we have multiple layouts using the same component role, we need to
			// cache the properties at each layout.
			// 
			// --------------------------------------------------------------------------------------------
			if ( null != layout )
				return layout.propertyCache.GetCachedProperty( prop, out val );
			
			// --------------------------------------------------------------------------------------------

			val = null;
			return false;
		}

		internal static bool GetCachedPropertyVal( UltraGridBand band, CustomProperty prop, out object val )
		{
			return GetCachedPropertyVal( band, (int)prop, out val );
		}

		internal static bool GetCachedPropertyVal( UltraGridBand band, CachedProperty prop, out object val )
		{
			return GetCachedPropertyVal( band, (int)prop, out val );
		}

		private static bool GetCachedPropertyVal( UltraGridBand band, int prop, out object val )
		{
			return band.propertyCache.GetCachedProperty( prop, out val );
		}

		internal static object GetCachedPropertyVal( UltraGridLayout layout, CustomProperty prop, 
			object controlVal, object defVal, object resolveVal )
		{
			object val;
			if ( ! GetCachedPropertyVal( layout, prop, out val ) )
				val = CachePropertyValue( layout, prop, controlVal, defVal, resolveVal );

			return val;
		}

		#endregion // GetCachedPropertyVal

		#region CachedProperty Enum

		internal enum CachedProperty
		{
			BorderStyleCardArea = 1 + CustomProperty.LastValue,
			BorderStyleCell,
			BorderStyleFilterCell,
			BorderStyleFilterOperator,
			BorderStyleFilterRow,
			BorderStyleHeader,
			BorderStyleRow,
			BorderStyleRowSelector,
			BorderStyleSpecialRowSeparator,
			BorderStyleSummaryFooter,
			BorderStyleSummaryFooterCaption,
			BorderStyleSummaryValue,
			BorderStyleTemplateAddRow,
			BorderStyleLayout,
			BorderStyleGridCaption,
			BorderStyleCombo,

			/// <summary>
			/// Border style of the column headers in group-by box.
			/// </summary>
			BorderStyleGroupByBoxColumnHeader,

			/// <summary>
			/// Border style of the prompt in the group-by box.
			/// </summary>
			BorderStyleGroupByBoxPrompt,

			/// <summary>
			/// Border style of the epxansion indicator.
			/// </summary>
			BorderStyleExpansionIndicator,

			/// <summary>
			/// Border style of the group-by row epxansion indicator.
			/// </summary>
			BorderStyleGroupByExpansionIndicator,

			/// <summary>
			/// Border style of the scroll region splitbox.
			/// </summary>
			BorderStyleSplitBox,

			/// <summary>
			/// Button style of the combo.
			/// </summary>
			ButtonStyleCombo,

			/// <summary>
			/// Button style of the override.
			/// </summary>
			ButtonStyleLayout,

			/// <summary>
			/// Button style of the AddNewBoxButton role.
			/// </summary>
			ButtonStyleAddNewBoxButton,
			
			/// <summary>
			/// Button style of the CellButton role, which is associated with the buttons that
			/// are displayed when the column's Style is set to Button.
			/// </summary>
			ButtonStyleCellButton,

			/// <summary>
			/// Button style of the CellEditButton role, which is associated with the buttons that
			/// are displayed when the column's Style is set to EditButton.
			/// </summary>
			ButtonStyleCellEditButton,

			/// <summary>
			/// Button style of the filter clear button.
			/// </summary>
			ButtonStyleFilterClearButton,

			/// <summary>
			/// Button style of the column chooser button.
			/// </summary>
			ButtonStyleColumnChooserButton,

			/// <summary>
			/// UltraCombo's DisplayStyle property (of type EmbeddableElementDisplayStyle).
			/// </summary>
			DisplayStyle,

			/// <summary>
			/// Header style of the override.
			/// </summary>
			HeaderStyleLayout,

			// NOTE: If you modify this enum, you must make sure the LastValue member below is
			// intialized to the right value.

            LastValue = HeaderStyleLayout,
		}

		#endregion // CachedProperty Enum

		#region CacheBorderStylePropertyValue

		internal static object CacheBorderStylePropertyValue( UltraGridLayout layout, CachedProperty prop, Role role, 
			UIElementBorderStyle controlVal, UIElementBorderStyle resolveVal )
		{
			UIElementBorderStyle roleBorderStyle = GetRoleBorderStyle( layout, role );

			return CachePropertyValue( layout, prop, roleBorderStyle, controlVal, UIElementBorderStyle.Default, resolveVal );
		}

		internal static object CacheBorderStylePropertyValue( UltraGridBand band, CachedProperty prop, Role role, 
			UIElementBorderStyle controlVal, UIElementBorderStyle resolveVal )
		{
			UIElementBorderStyle roleBorderStyle = GetRoleBorderStyle( band, role );

			return CachePropertyValue( band, prop, roleBorderStyle, controlVal, UIElementBorderStyle.Default, resolveVal );
		}

		#endregion // CacheBorderStylePropertyValue

		#region CacheButtonStylePropertyValue

		internal static object CacheButtonStylePropertyValue( UltraGridBand band, CachedProperty prop, Role role, 
			UIElementButtonStyle controlVal, UIElementButtonStyle resolveVal )
		{
			UIElementButtonStyle roleButtonStyle = GetRoleButtonStyle( band.Layout, role );

			return CachePropertyValue( band, prop, roleButtonStyle, controlVal, UIElementButtonStyle.Default, resolveVal );
		}

		internal static object CacheButtonStylePropertyValue( UltraGridLayout layout, CachedProperty prop, Role role, 
			UIElementButtonStyle controlVal, UIElementButtonStyle resolveVal )
		{
			UIElementButtonStyle roleButtonStyle = GetRoleButtonStyle( layout, role );

			return CachePropertyValue( layout, prop, roleButtonStyle, controlVal, UIElementButtonStyle.Default, resolveVal );
		}

		internal static object CacheButtonStylePropertyValue( UltraGridLayout layout, CachedProperty prop, 
			UIElementButtonStyle controlVal, UIElementButtonStyle resolveVal )
		{
			UIElementButtonStyle roleButtonStyle = GetComponentRoleButtonStyle( layout );

			return CachePropertyValue( layout, prop, roleButtonStyle, controlVal, UIElementButtonStyle.Default, resolveVal );
		}

		#endregion // CacheButtonStylePropertyValue

		#region CachePropertyValue

		// SSP 5/26/06 BR13063
		// Due to printing where we have multiple layouts using the same component role, we need to
		// cache the properties at each layout.
		// Added an overload of CachePropertyValue that takes in propertyCache.
		// 
		internal static object CachePropertyValue( UltraGridLayout layout, StylePropertyCache propertyCache,
			int propIndex, object styleVal, object controlVal, object defVal, object resolveVal )
		{
			object result = StyleUtilities.CalculateResolvedValue( GetResolutionOrderEnum( layout ),
					styleVal, controlVal, defVal, resolveVal );

			propertyCache.CachePropertyValue( propIndex, result );

			return result;
		}

		internal static object CachePropertyValue( UltraGridLayout layout, CachedProperty prop, 
			object styleVal, object controlVal, object defVal, object resolveVal )
		{
			// SSP 5/26/06 BR13063
			// Due to printing where we have multiple layouts using the same component role, we need to
			// cache the properties at each layout.
			// 
			// --------------------------------------------------------------------------------------------
			if ( null != layout )
				return CachePropertyValue( layout, layout.propertyCache, (int)prop, styleVal, controlVal, defVal, resolveVal );
			
			// --------------------------------------------------------------------------------------------

			return ResolveValueHelper( controlVal, defVal, resolveVal );
		}

		internal static object CachePropertyValue( UltraGridBand band, CachedProperty prop, 
			object styleVal, object controlVal, object defVal, object resolveVal )
		{
			object finalVal = StyleUtilities.CalculateResolvedValue(
				GetResolutionOrderEnum( band.Layout ), styleVal, controlVal, defVal, resolveVal );

			band.propertyCache.CachePropertyValue( (int)prop, finalVal );
			return finalVal;
		}

        internal static object CachePropertyValue(UltraGridBand band, CustomProperty prop,
            object styleVal, object controlVal, object defVal, object resolveVal)
        {
            object finalVal = StyleUtilities.CalculateResolvedValue(
                GetResolutionOrderEnum(band.Layout), styleVal, controlVal, defVal, resolveVal);

            band.propertyCache.CachePropertyValue((int)prop, finalVal);
            return finalVal;
        }

		private static object ResolveValueHelper( object val, object defVal, object resolveVal )
		{
			return defVal == resolveVal || null != val && ! object.Equals( val, defVal ) ? val : resolveVal;
		}

		internal static object CachePropertyValue( UltraGridLayout layout, CustomProperty prop, object controlVal, object defVal, object resolveVal )
		{
			// SSP 5/26/06 BR13063
			// Due to printing where we have multiple layouts using the same component role, we need to
			// cache the properties at each layout.
			// 
			// --------------------------------------------------------------------------------------------
			if ( null != layout )
			{
				object styleVal = GetStylePropertyValue( layout, prop );
				return CachePropertyValue( layout, layout.propertyCache, (int)prop, styleVal, controlVal, defVal, resolveVal );
			}
			
			// --------------------------------------------------------------------------------------------

			return ResolveValueHelper( controlVal, defVal, resolveVal );
		}

		internal static object GetStylePropertyValue( UltraGridLayout layout, CustomProperty prop )
		{
			ComponentRole role = GetComponentRole( layout );
			string propName = GetCustomPropertyName( prop );
			return null != role && role.HasProperty( propName ) ? role.GetProperty( propName ) : null;
		}

		internal static object CachePropertyValue( UltraGridBand band, CustomProperty prop, object controlVal, object defVal, object resolveVal )
		{
			UltraGridLayout layout = band.Layout;
			ComponentRole role = GetComponentRole( layout );
			//Debug.Assert( null != role );
			if ( null != role )
			{
				object styleVal = GetStylePropertyValue( layout, prop );

				object finalVal = StyleUtilities.CalculateResolvedValue( 
					GetResolutionOrderEnum( layout ), styleVal, controlVal, defVal, resolveVal );

				band.propertyCache.CachePropertyValue( (int)prop, finalVal );
				return finalVal;
			}

			return ResolveValueHelper( controlVal, defVal, resolveVal );
		}

		#endregion // CachePropertyValue

		#region GetRoleBorderStyle

		internal static UIElementBorderStyle GetRoleBorderStyle( UltraGridBand band, Role eRole )
		{
			return GetRoleBorderStyle( band.Layout, eRole );
		}

		internal static UIElementBorderStyle GetRoleBorderStyle( UltraGridLayout layout, Role eRole )
		{
			UIRole role = GetRole( layout, eRole );
			return null != role ? role.BorderStyle : UIElementBorderStyle.Default;
		}

		#endregion // GetRoleBorderStyle

		#region GetComponentRoleButtonStyle

		internal static UIElementButtonStyle GetComponentRoleButtonStyle( UltraGridLayout layout )
		{
			ComponentRole ccRole = GetComponentRole( layout );
			return null != ccRole ? ccRole.ButtonStyle : UIElementButtonStyle.Default;
		}

		#endregion // GetComponentRoleButtonStyle

		#region GetRoleButtonStyle

		internal static UIElementButtonStyle GetRoleButtonStyle( UltraGridLayout layout, Role eRole )
		{
			UIRole role = GetRole( layout, eRole );
			ComponentRole ccRole = GetComponentRole( layout );
			return null != role ? role.GetButtonStyle( ccRole ) : UIElementButtonStyle.Default;
		}

		#endregion // GetRoleButtonStyle

		#region GetComponentRoleHeaderStyle

		internal static HeaderStyle GetComponentRoleHeaderStyle( UltraGridLayout layout )
		{
			ComponentRole role = GetComponentRole( layout );
			return null != role ? role.HeaderStyle : HeaderStyle.Default;
		}

		#endregion // GetComponentRoleHeaderStyle

		#region DirtyCachedPropertyVals

		internal static void DirtyCachedPropertyVals( UltraGridLayout layout )
		{
			ComponentRole role = GetComponentRole( layout );
			if ( null != role )
				role.ClearCachedPropertyValues( );

			if ( null != layout )
			{
				// SSP 6/5/06 BR13306
				// 
				if ( null != layout.propertyCache )
					layout.propertyCache.ClearCachedPropertyValues( );

                // MRS 2/4/2009 - TFS13235
				//if ( null != layout.SortedBands )
                if (null != layout.Bands)
				{
                    // MRS 2/4/2009 - TFS13235
					//foreach ( UltraGridBand band in layout.SortedBands )
                    foreach (UltraGridBand band in layout.Bands)
					{
						if ( null != band.propertyCache )
							band.propertyCache.ClearCachedPropertyValues( );
					}
				}
			}
		}

        // CDS NAS v9.1 Header CheckBox
        internal static void DirtyCachedPropertyVals(UltraGridBand band)
        {
            if (null != band &&
                null != band.propertyCache)
                band.propertyCache.ClearCachedPropertyValues();            
        }

		#endregion // DirtyCachedPropertyVals

		#region CustomProperty Enum

		internal enum CustomProperty
		{
			AddNewBoxButtonConnectorColor,	
			AddNewBoxButtonConnectorStyle,	
			//ButtonStyle,						
			CardCollapsedIndicatorImage,		
			CardExpandedIndicatorImage,		
			//CellPadding,
			FilterDropDownButtonImage,		
			FilterDropDownButtonImageActive,	
			FixedCellSeparatorColor,			
			FixedHeaderOffImage,				
			FixedHeaderOnImage,				
			FixedRowOffImage,					
			FixedRowOnImage,					
			GroupByBoxButtonConnectorColor,	
			GroupByBoxButtonConnectorStyle,	
			//HeaderStyle,
            // CDS NAS v9.1 Header CheckBox
		    HeaderCheckBoxAlignment,
			RowConnectorColor,				
			RowConnectorStyle,
			ColScrollRegionSplitBoxWidth,
			RowScrollRegionSplitBoxHeight,
			ColScrollRegionSplitterBarWidth,
			RowScrollRegionSplitterBarHeight,
			RowSelectorHeaderStyle,			
			RowSelectorStyle,					
			SummaryButtonImage,				
			WrapHeaderText,	

            // MBS 5/6/08 - RowEditTemplate NA2008 V2
            RowEditTemplateRowSelectorImage,
            UseGridAppearances,

            // MBS 3/31/09 - NA9.2 CellBorderColor
            ActiveCellBorderThickness,

            // MBS 4/6/09 - NA9.2 SelectionOverlayColor
            SelectionOverlayColor,
			SelectionOverlayBorderColor,
            SelectionOverlayBorderThickness,

            // CDS 9.2 Column Moving Indicators
            DragDropIndicatorImageBottom,
            DragDropIndicatorImageLeft,
            DragDropIndicatorImageRight,
            DragDropIndicatorImageTop,
            DragDropIndicatorColor,

            // CDS 9.2 RowSelector State-Specific Images
            RowSelectorActiveRowImage,
            RowSelectorDataChangedImage,
            RowSelectorAddNewRowImage,
            RowSelectorActiveAndDataChangedImage,
            RowSelectorActiveAndAddNewImage,

            // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (DefaultSelectedBackColor & DefaultSelectedForeColor)
            DefaultSelectedBackColor,
            DefaultSelectedForeColor,

			// NOTE: This enum must be synchronized with the array that GetCustomPropertiesInfo returns.
			// The order of enum values must correspond to the order in that array.
			// 
            LastValue = DefaultSelectedForeColor,
		};

		#endregion // CustomProperty Enum

		#region GetCustomPropertyNames

		private static string[] cachedCustomPropertyNames = null;
		private static string[] GetCustomPropertyNames( )
		{
			if ( null != cachedCustomPropertyNames )
				return cachedCustomPropertyNames;

			const int STEP = 7;
			object[] props = GetCustomPropertiesInfo( );
			string[] ret = new string[ props.Length / STEP ];

			for ( int i = 0; i < props.Length; i += STEP )
				ret[ i / STEP ] = (string)props[ i ];

			return cachedCustomPropertyNames = ret;
		}

		#endregion // GetCustomPropertyNames

		#region GetCustomPropertyName

		internal static string GetCustomPropertyName( CustomProperty prop )
		{
			string[] customPropertyNames = GetCustomPropertyNames( );
			return customPropertyNames[ (int)prop ];
		}

		#endregion // GetCustomPropertyName

		#region GetCustomPropertiesInfo

		private static object[] GetCustomPropertiesInfo( )
		{
			return new object[]
			{
				// Name								Type								Default Value					Converter				ValueConstraint,			Applicable Components					Description string resource name
				"AddNewBoxButtonConnectorColor",	typeof( Color ),					Color.Empty,					null,					null,						StyleUtils.ComponentType.Grid,			"LD_SpecialBoxBase_P_ButtonConnectorColor",
				"AddNewBoxButtonConnectorStyle",	typeof( UIElementBorderStyle ),		UIElementBorderStyle.Default,	null,					null,						StyleUtils.ComponentType.Grid,			"LD_SpecialBoxBase_P_ButtonConnectorStyle",
				//"ButtonStyle",						typeof( UIElementButtonStyle ),		UIElementButtonStyle.Default,	null,					null,						0,
				"CardCollapsedIndicatorImage",		typeof( Image ),					null,							null,					null,						StyleUtils.ComponentType.Grid,			"LD_Layout_CardCollapsedIndicatorImage",
				"CardExpandedIndicatorImage",		typeof( Image ),					null,							null,					null,						StyleUtils.ComponentType.Grid,			"LD_Layout_CardExpandedIndicatorImage",
				//"CellPadding",						typeof( Size ),						new Size( -1, -1 ),				null,					null,						StyleUtils.ComponentType.All,		null,
				"FilterDropDownButtonImage",		typeof( Image ),					null,							null,					null,						StyleUtils.ComponentType.AllGrids,		"LDR_FilterDropDownButtonImage",
				"FilterDropDownButtonImageActive",	typeof( Image ),					null,							null,					null,						StyleUtils.ComponentType.AllGrids,		"LDR_FilterDropDownButtonImageActive",
				"FixedCellSeparatorColor",			typeof( Color ),					Color.Empty,					null,					null,						StyleUtils.ComponentType.AllGrids,		"LDR_UltraGridOverride_P_FixedCellSeparatorColor",
				"FixedHeaderOffImage",				typeof( Image ),					null,							null,					null,						StyleUtils.ComponentType.AllGrids,		"LDR_FixedHeaderOffImage",
				"FixedHeaderOnImage",				typeof( Image ),					null,							null,					null,						StyleUtils.ComponentType.AllGrids,		"LDR_FixedHeaderOnImage",
				"FixedRowOffImage",					typeof( Image ),					null,							null,					null,						StyleUtils.ComponentType.AllGrids,		"LDR_FixedRowOffImage",
				"FixedRowOnImage",					typeof( Image ),					null,							null,					null,						StyleUtils.ComponentType.AllGrids,		"LDR_FixedRowOnImage",
				"GroupByBoxButtonConnectorColor",	typeof( Color ),					Color.Empty,					null,					null,						StyleUtils.ComponentType.Grid,			"LD_SpecialBoxBase_P_ButtonConnectorColor",
				"GroupByBoxButtonConnectorStyle",	typeof( UIElementBorderStyle ),		UIElementBorderStyle.Default,	null,					null,						StyleUtils.ComponentType.Grid,			"LD_SpecialBoxBase_P_ButtonConnectorStyle",
				//"HeaderStyle",						typeof( HeaderStyle ),				HeaderStyle.Default,			null,					null,						StyleUtils.ComponentType.AllGridBase,
                // CDS NAS v9.1 Header CheckBox
                "HeaderCheckBoxAlignment",	typeof( Infragistics.Win.UltraWinGrid.HeaderCheckBoxAlignment),		HeaderCheckBoxAlignment.Default,	null,					null,						StyleUtils.ComponentType.Grid,			"LD_HeaderCheckBoxAlignment",
                "RowConnectorColor",				typeof( Color ),					Color.Empty,					null,					null,						StyleUtils.ComponentType.Grid,			"LD_UltraGridLayout_P_RowConnectorColor",
				"RowConnectorStyle",				typeof( RowConnectorStyle ),		RowConnectorStyle.Default,		null,					null,						StyleUtils.ComponentType.Grid,			"LD_UltraGridLayout_P_RowConnectorStyle",
				"ColScrollRegionSplitBoxWidth",		typeof( int ),						0,								null,					null,						StyleUtils.ComponentType.Grid,			"LD_Layout_ColScrollRegionSplitBoxWidth",
				"RowScrollRegionSplitBoxHeight",	typeof( int ),						0,								null,					null,						StyleUtils.ComponentType.Grid,			"LD_Layout_RowScrollRegionSplitBoxHeight",
				"ColScrollRegionSplitterBarWidth",	typeof( int ),						0,								null,					null,						StyleUtils.ComponentType.Grid,			"LD_Layout_ColScrollRegionSplitterBarWidth",
				"RowScrollRegionSplitterBarHeight",	typeof( int ),						0,								null,					null,						StyleUtils.ComponentType.Grid,			"LD_Layout_RowScrollRegionSplitterBarHeight",
				"RowSelectorHeaderStyle",			typeof( RowSelectorHeaderStyle ),	RowSelectorHeaderStyle.Default,	null,					null,						StyleUtils.ComponentType.Grid,			"LD_UltraGridOverride_P_RowSelectorHeaderStyle",
				"RowSelectorStyle",					typeof( HeaderStyle ),				HeaderStyle.Default,			null,					null,						StyleUtils.ComponentType.Grid,			"LD_UltraGridOverride_P_RowSelectorStyle",
				"SummaryButtonImage",				typeof( Image ),					null,							null,					null,						StyleUtils.ComponentType.Grid,			"LDR_SummaryButtonImage",
				"WrapHeaderText",					typeof( DefaultableBoolean ),		DefaultableBoolean.Default,		null,					null,						StyleUtils.ComponentType.AllGrids,		"LD_UltraGridOverride_P_WrapHeaderText",
				//"DisplayStyle",						typeof( EmbeddableElementDisplayStyle ), EmbeddableElementDisplayStyle.Default, null,		null,						StyleUtils.ComponentType.Combo,		null,
                // MBS 5/6/08 - RowEditTemplate NA2008 V2
                "RowEditTemplateRowSelectorImage",  typeof(Image),                      null,                           null,                   null,                       StyleUtils.ComponentType.Grid,          "LD_UltraGridOverride_P_RowEditTemplateRowSelectorImage",
                "UseGridAppearances",               typeof(bool),                       true,                           null,                   null,                       StyleUtils.ComponentType.CellProxy,     "LD_UltraGridCellProxy_P_UseGridAppearances",                

                // MBS 3/31/09 - NA9.2 CellBorderColor
                "ActiveCellBorderThickness",        typeof(int),                        0,                              null,                   null,                       StyleUtils.ComponentType.Grid,          "LD_UltraGridOverride_P_ActiveCellBorderThickness",

                // MBS 4/6/09 - NA9.2 SelectionOverlay
                "SelectionOverlayColor",            typeof(Color),                      Color.Empty,                    null,                   null,                       StyleUtils.ComponentType.Grid,          "LD_UltraGridLayout_P_SelectionOverlayColor",
                "SelectionOverlayBorderColor",      typeof(Color),                      Color.Empty,                    null,                   null,                       StyleUtils.ComponentType.Grid,          "LD_UltraGridLayout_P_SelectionOverlayBorderColor",
                "SelectionOverlayBorderThickness",  typeof(int),                        -1,                             null,                   null,                       StyleUtils.ComponentType.Grid,          "LD_UltraGridLayout_P_SelectionOverlayBorderThickness",

                // CDS 9.2 Column Moving Indicators
                "DragDropIndicatorImageBottom",	typeof( Image),		                null,                               null,					null,						StyleUtils.ComponentType.Grid,			"LD_DragDropIndicatorSettings_ImageBottom",
                "DragDropIndicatorImageLeft",	typeof( Image),		                null,                               null,					null,						StyleUtils.ComponentType.Grid,			"LD_DragDropIndicatorSettings_ImageLeft",
                "DragDropIndicatorImageRight",	typeof( Image),		                null,                               null,					null,						StyleUtils.ComponentType.Grid,			"LD_DragDropIndicatorSettings_ImageRight",
                "DragDropIndicatorImageTop",	typeof( Image),		                null,                               null,					null,						StyleUtils.ComponentType.Grid,			"LD_DragDropIndicatorSettings_ImageTop",                
                "DragDropIndicatorColor",	typeof( Color),		                DragDropIndicatorSettings.DEFAULT_COLOR, null,		null,						StyleUtils.ComponentType.Grid,			"LD_DragDropIndicatorSettings_BrushColor",

                // CDS 9.2 RowSelector State-Specific Images
                "RowSelectorActiveRowImage",	typeof( Image),		                null,                               null,					null,						StyleUtils.ComponentType.Grid,			"LD_RowSelectorImageInfo_P_ActiveRowImage",
                "RowSelectorDataChangedImage",	typeof( Image),		                null,                               null,					null,						StyleUtils.ComponentType.Grid,			"LD_RowSelectorImageInfo_P_DataChangedImage",
                "RowSelectorAddNewRowImage",	typeof( Image),		                null,                               null,					null,						StyleUtils.ComponentType.Grid,			"LD_RowSelectorImageInfo_P_AddNewRowImage",
                "RowSelectorActiveAndDataChangedImage",	typeof( Image),		                null,                               null,					null,						StyleUtils.ComponentType.Grid,			"LD_RowSelectorImageInfo_P_ActiveAndDataChangedImage",
                "RowSelectorActiveAndAddNewImage",	typeof( Image),		                null,                               null,					null,						StyleUtils.ComponentType.Grid,			"LD_RowSelectorImageInfo_P_ActiveAndAddNewRowImage",

                // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties (DefaultSelectedBackColor & DefaultSelectedForeColor)
                "DefaultSelectedBackColor",	typeof( Color),		                SystemColors.Highlight, null,		null,						StyleUtils.ComponentType.Grid,			"LD_UltraGridLayout_P_DefaultSelectedBackColor",
                "DefaultSelectedForeColor",	typeof( Color),		                SystemColors.HighlightText, null,		null,						StyleUtils.ComponentType.Grid,			"LD_UltraGridLayout_P_DefaultSelectedForeColor",

				// NOTE: This array must be synchronized with the CustomProperty enum.
				// The order of enum values must correspond to the order in that array.
				// 
			};
		}

		#endregion // GetCustomPropertiesInfo

		#region GetCustomProperties

		internal static AppStyleProperty[] GetCustomProperties( StyleUtils.ComponentType component )
		{
			object[] PROPERTIES = GetCustomPropertiesInfo( );			
            
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<AppStyleProperty> list = new List<AppStyleProperty>();

			const int STEP = 7;
			for ( int i = 0; i < PROPERTIES.Length; i += STEP )
			{
				StyleUtils.ComponentType componentType = (StyleUtils.ComponentType)PROPERTIES[ 5 + i ];
				if ( 0 == ( component & componentType ) )
					continue;

				string name = (string)PROPERTIES[ i ];
				Type type = (Type)PROPERTIES[ 1 + i ];
				object defValue = (object)PROPERTIES[ 2 + i ];
				AppStylePropertyConverter converter = (AppStylePropertyConverter)PROPERTIES[ 3 + i ];
				ValueConstraint constraint = (ValueConstraint)PROPERTIES[ 4 + i ];

				AppStyleProperty prop = new AppStyleProperty( name, type, converter );
				prop.DefaultValue = defValue;
				prop.Constraint = constraint;

				string descriptionResourceName = (string)PROPERTIES[ 6 + i ];
				if ( null != descriptionResourceName && descriptionResourceName.Length > 0 )
					prop.Description = SR.GetString( descriptionResourceName );

				list.Add( prop );
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (AppStyleProperty[])list.ToArray( typeof( AppStyleProperty ) );
			return list.ToArray();
		}

		#endregion // GetCustomProperties

		#region ResolveAppearance

		internal static void ResolveAppearance( UIRole role, RoleState state, ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			if ( null != role )
				role.ResolveAppearance( ref appData, ref flags, state );
		}

		internal static void ResolveAppearance( UIRole role, RoleState state, ref AppearanceData appData, ref ResolveAppearanceContext context )
		{
			ResolveAppearance( role, state, ref appData, ref context.UnresolvedProps );
		}

		internal static void ResolveAppearance( RoleState state, ref AppearanceData appData, ref ResolveAppearanceContext context )
		{
			ResolveAppearance( context.Role, state, ref appData, ref context.UnresolvedProps );
		}

		#endregion // ResolveAppearance

		#region HasAppearance
		
		internal static bool HasAppearance( UIRole role, RoleState state )
		{
			return null != role && role.HasAppearance( state );
		}

		#endregion // HasAppearance

		#region Role Enum

		internal enum Role
		{
			/// <summary>
			/// Abstact base role for different kinds of grid headers (column, group and band headers).
			/// </summary>
			Header,

			/// <summary>
			/// Column header.
			/// </summary>
			ColumnHeader,

			/// <summary>
			/// Band header.
			/// </summary>
			BandHeader,

			/// <summary>
			/// Group header.
			/// </summary>
			GroupHeader,

			/// <summary>
			/// Cell.
			/// </summary>
			Cell,

			/// <summary>
			/// Filter operator.
			/// </summary>
			FilterOperator,

			/// <summary>
			/// Row.
			/// </summary>
			Row,

			/// <summary>
			/// Group-by row.
			/// </summary>
			GroupByRow,

			/// <summary>
			/// Group-by row connector. This is the vertical stript that connects sibling group-by rows.
			/// </summary>
			GroupByRowConnector,

			/// <summary>
			/// A prompt displayed inside a row.
			/// </summary>
			RowPrompt,

			/// <summary>
			/// Filter-row prompt. If enabled, the prompt is displayed inside the filter row. 
			/// By default it's transparent. Set BackColor or BackColorAlpha on the appearance 
			/// to make it non-transparent.
			/// </summary>
			FilterRowPrompt,

			/// <summary>
			/// Template add-row prompt. If enabled, this prompt is displayed inside the template
			/// add-row. By default it's transparent. Set BackColor or BackColorAlpha on the
			/// appearance to make it non-transparent.
			/// </summary>
			TemplateAddRowPrompt,

			/// <summary>
			/// Row selector. This element appears on the left of the row selector. It displays
			/// various icons to indicate the status of the row (whether the row is active, modified,
			/// add-row etc...).
			/// </summary>
			RowSelector,

			/// <summary>
			/// Row selector header. If enabled, this element appears above the row selectors in 
			/// the column headers area of the grid.
			/// </summary>
			RowSelectorHeader,

			/// <summary>
			/// Base role for grid buttons. Note that this is not necessarily the base role for
			/// all the buttons used by the grid. This is the base role for buttons that the WinGrid
			/// assembly defines.
			/// </summary>
			GridButton,

			/// <summary>
			/// This refers to the button that's displayed in each cell when the Style of the 
			/// column is set to Button. These buttons typically occpu the entire cell area.
			/// </summary>
			CellButton,

			/// <summary>
			/// This refers to the button that's displayed in each cell when the Style of the 
			/// column is set to EditButton. These buttons are displayed on the right side of 
			/// the cell.
			/// </summary>
			CellEditButton,

			/// <summary>
			/// Clear filter button. This is disaplayed by default in each filter cell and in the
			/// filter-row's row selector. This is used for clearing the filters.
			/// </summary>
			FilterClearButton,

			/// <summary>
			/// Add-new box. If enabled, add-new box is the area on the bottom of the grid that
			/// displays add-new buttons for each band for adding new rows.
			/// </summary>
			AddNewBox,

			/// <summary>
			/// Add-new box prompt (Add...). This refers to a prompt label that appears in
			/// the add-new box, left of the add-new buttons.
			/// </summary>
			AddNewBoxPrompt,

			/// <summary>
			/// Add row buttons that appear in the add-new box.
			/// </summary>
			AddNewBoxButton,

			/// <summary>
			/// Group-by box. This refers to the area on top of the grid where one drags
			/// columns into to group rows by those columns.
			/// </summary>
			GroupByBox,

			/// <summary>
			/// This refers to the column header that's displayed in group-by box when the rows
			/// are grouped by that column.
			/// </summary>
			GroupByBoxColumnHeader,

			/// <summary>
			/// Band labels that appear in the group-by box.
			/// </summary>
			GroupByBoxBandLabel,

			/// <summary>
			/// The prompt label that's displayed in the group-by box when rows aren't grouped
			/// by any column. As soon as rows are grouped by a column, this prompt is replaced
			/// with the column header.
			/// </summary>
			GroupByBoxPrompt,

			/// <summary>
			/// Card area. This is the area where cards (rows) are laid out in card-view mode.
			/// </summary>
			CardArea,

			/// <summary>
			/// Card label area. This is the area where cards labels (column labels) are laid out in card-view mode.
			/// </summary>
			CardLabelArea,

			/// <summary>
			/// Card caption. Caption that appears over each card-row.
			/// </summary>
			CardCaption,

			/// <summary>
			/// Special row separator. This refers to the horizontal ui element that separates 
			/// filter row, fixed add-row, summary row and fixed rows from each other.
			/// </summary>
			SpecialRowSeparator,

			/// <summary>
			/// Summary footer. This refers to the row element that displays summary values.
			/// </summary>
			SummaryFooter,

			/// <summary>
			/// Summary footer caption. This refers to the caption that's displayed above the 
			/// summary row.
			/// </summary>
			SummaryFooterCaption,
            
			/// <summary>
			/// Summary value cell. This refers to the element that displays a summary value.
			/// </summary>
			SummaryValue,

			/// <summary>
			/// Splitter bar for resizing a scroll region. Scroll region splitter bar is displayed
			/// when the grid is split into multiple scrolling regions.
			/// </summary>
			ScrollRegionSplitterBar,

			/// <summary>
			/// Splitter bar for resizing a column scroll region. When a grid is split horizontally
			/// into two or more column scrolling regions, a vertical splitter bar separates each
			/// column scrolling region from each other. This role refers to that splitter bar.
			/// </summary>
			ColScrollRegionSplitterBar,

			/// <summary>
			/// Splliter bar for resizing a row scroll region. When a grid is split vertically
			/// into two or more row scrolling regions, a horizontal splitter bar separates each
			/// row scrolling region from each other. This role refers to that splitter bar.
			/// </summary>
			RowScrollRegionSplitterBar,

			/// <summary>
			/// Split box element for splitting a scroll region. This element appears above the vertical
			/// scrollbar or left of the horizontal scrollbar. It lets you split the column or row scroll
			/// region into two.
			/// </summary>
			ScrollRegionSplitBox,

			/// <summary>
			/// Split box for splitting a column scroll region. This element appears left of the horizontal
			/// scrollbar. It lets you split the column scroll region into two.
			/// </summary>
			ColScrollRegionSplitBox,

			/// <summary>
			/// Split box for splitting a row scroll region. This element appears above the vertical
			/// scrollbar. It lets you split the row scroll region into two.
			/// </summary>
			RowScrollRegionSplitBox,

			/// <summary>
			/// This refers to the element that's displayed at the intersection of a vertical and
			/// a horizontal splitter bar.
			/// </summary>
			SplitterBarIntersection,

			/// <summary>
			/// Row preview area. This refers to the area in row that displays the row preview when the
			/// row preview functionality is enabled.
			/// </summary>
			RowPreview,

			/// <summary>
			/// Represents the control area of the grid. This supplies the defaults to the entire grid.
			/// This is the base role for GridControlArea and DropDownControlArea roles which represent
			/// the UltraGrid's control area and the control area of the drop down portion of the
			/// UltraCombo/UltraDropDown respectively.
			/// </summary>
			GridControlAreaBase,

			/// <summary>
			/// Represents the control area of the UltraGrid. This supplies the defaults to the entire grid.
			/// </summary>
			GridControlArea,

			/// <summary>
			/// Represents the grid area in the drop down of the UltraCombo and UltraDropDown controls.
			/// </summary>
			DropDownControlArea,

			/// <summary>
			/// Grid's caption. If enabled, grid caption is displayed in top area of the grid. It by
			/// default displays the grid control's name.
			/// </summary>
			GridCaption,

			/// <summary>
			/// Area where empty rows are laid out. This is by default a transparent element so the
			/// grid's background shows through.
			/// </summary>
			EmptyRowsArea,

			/// <summary>
			/// Page header.
			/// </summary>
			PageHeader,

			/// <summary>
			/// Page footer.
			/// </summary>
			PageFooter,

			/// <summary>
			/// Edit portion of the UltraCombo control.
			/// </summary>
			UltraComboEditPortion,

			/// <summary>
			/// Column chooser button. If enabled this is disaplayed in the row selector header area.
			/// </summary>
			ColumnChooserButton,

			/// <summary>
			/// Expansion indicator used by non-group-by rows.
			/// </summary>
			ExpansionIndicator,

			/// <summary>
			/// Expansion indicator used by group-by rows.
			/// </summary>
			GroupByExpansionIndicator,
		}

        // MBS 4/22/08 - RowEditTemplate NA2008 V2
        internal enum CellProxyRole
        {
            /// <summary>
            /// The main element of an UltraGridCellProxy.
            /// </summary>
            CellProxy,
        }
        //
        internal enum TemplateRole
        {
            /// <summary>
            /// The panel of the UltraGridRowEditTemplate
            /// </summary>
            RowEditTemplatePanel,
        }
		#endregion // Role Enum

        // These constants are for finding out the index offset in the ROLES array for getting specific information.
		// 
		internal const int ROLES_STEP = 6, ROLES_ROLE_ENUM = 0, ROLES_ROLE_NAME = 1, ROLES_BASE_ROLE_NAME = 2, ROLES_STATES = 3, ROLES_ASSOCIATED_COMPONENTS = 4, ROLES_SUPPORTED_PROPS = 5;

		private static object[] ROLES = 
		{
            // MBS 7/2/09 - ActiveCellHeaderAppearances
            // Added ActiveCell role state for the ColumnHeader and RowSelector
            //
			// Role enum					Role Name					Base Role Name		States
			Role.Header,					"Header",					UIRoleNames.Header,	new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.CardView }, ComponentType.All, SupportedRoleProperties.BorderStyle,
			Role.ColumnHeader,  			"ColumnHeader",  			"g_Header",			new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.FixedColumn, RoleState.GroupByColumn, RoleState.CardView, RoleState.ActiveCell }, ComponentType.All, SupportedRoleProperties.None,
			Role.BandHeader,				"BandHeader",				"g_Header",			new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.CardView }, ComponentType.Grid | ComponentType.ColumnChooser, SupportedRoleProperties.None,
			Role.GroupHeader,   			"GroupHeader",   			"g_Header",			new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.FixedColumn, RoleState.CardView }, ComponentType.AllGrids, SupportedRoleProperties.None,
			// SSP 8/26/06 - NAS 6.3
			// Added ReadOnly role state for the cell role.
			//             
            // MRS 4/8/2009 - TFS15580
            // Added the DataError state to Cell. 
            //			
            Role.Cell,  					"Cell",  					UIRoleNames.Cell,	new RoleState[] { RoleState.Normal, RoleState.Active, RoleState.EditMode, RoleState.HotTracked, RoleState.Selected, RoleState.FilterRow, RoleState.FilteredIn, RoleState.FilteredOut, RoleState.FixedColumn, RoleState.FixedRow, RoleState.EmptyRow, RoleState.AddRow, RoleState.FormulaError, RoleState.GroupByColumn, RoleState.HasActiveFilters, RoleState.MergedCell, RoleState.RowHotTracked, RoleState.TemplateAddRow, RoleState.CardView, RoleState.ReadOnly, RoleState.DataError }, ComponentType.All, SupportedRoleProperties.BorderStyle,
			Role.FilterOperator,			"FilterOperator",			"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.Row,   					"Row",   					UIRoleNames.Row,	new RoleState[] { RoleState.Normal, RoleState.Active, RoleState.HotTracked, RoleState.Selected, RoleState.AddRow, RoleState.DataError, RoleState.EmptyRow, RoleState.FilteredIn, RoleState.FilteredOut, RoleState.FilterRow, RoleState.FixedRow, RoleState.HasActiveFilters, RoleState.AlternateItem, RoleState.TemplateAddRow, RoleState.CardView }, ComponentType.All, SupportedRoleProperties.BorderStyle,
			Role.GroupByRow,				"GroupByRow",				"Base",				new RoleState[] { RoleState.Normal, RoleState.Selected }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.GroupByRowConnector,		"GroupByRowConnector",		"Base",				new RoleState[] { RoleState.Normal, RoleState.Selected }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.RowPrompt,   				"RowPrompt",   				"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.FilterRowPrompt,   		"FilterRowPrompt",   		"g_RowPrompt",		new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.TemplateAddRowPrompt,  	"TemplateAddRowPrompt",  	"g_RowPrompt",		new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.RowSelector,   			"RowSelector",   			"g_Header",			new RoleState[] { RoleState.Normal, RoleState.RowHotTracked, RoleState.DataError, RoleState.FixedRow, RoleState.FilterRow, RoleState.EmptyRow, RoleState.ActiveCell }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.RowSelectorHeader,   		"RowSelectorHeader",		"g_Header",			new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.GridButton,				"Button",					UIRoleNames.Button,	new RoleState[] { RoleState.Normal }, ComponentType.AllGrids, SupportedRoleProperties.ButtonStyle,
			Role.CellButton,				"CellButton",				"g_Button",			new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.ButtonStyle,
			Role.CellEditButton,			"CellEditButton",			"g_Button",			new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.ButtonStyle,
			Role.FilterClearButton, 		"FilterClearButton", 		"g_Button",			new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.ButtonStyle,
			Role.AddNewBox, 				"AddNewBox", 				"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.AddNewBoxPrompt,   		"AddNewBoxPrompt",   		"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.AddNewBoxButton,   		"AddNewBoxButton", 			"g_Button",			new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.ButtonStyle,
			Role.GroupByBox,				"GroupByBox",				"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.GroupByBoxColumnHeader,	"GroupByBoxColumnHeader",	"g_ColumnHeader",	new RoleState[] { RoleState.Normal, RoleState.FixedColumn, RoleState.GroupByColumn }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.GroupByBoxBandLabel,   	"GroupByBoxBandLabel",   	"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.GroupByBoxPrompt,  		"GroupByBoxPrompt",  		"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.CardArea,  				"CardArea",  				"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.CardLabelArea, 			"CardLabelArea",  			"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.CardCaption,   			"CardCaption",   			"Base",				new RoleState[] { RoleState.Normal, RoleState.Active, RoleState.Selected }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.SpecialRowSeparator,   	"SpecialRowSeparator",   	"Base",				new RoleState[] { RoleState.Normal }, ComponentType.AllGrids, SupportedRoleProperties.BorderStyle,
			Role.SummaryFooter,				"SummaryFooter",			"Base",				new RoleState[] { RoleState.Normal }, ComponentType.AllGrids, SupportedRoleProperties.BorderStyle,
			Role.SummaryFooterCaption, 		"SummaryFooterCaption", 	"Base",				new RoleState[] { RoleState.Normal }, ComponentType.AllGrids, SupportedRoleProperties.BorderStyle,
			Role.SummaryValue,   			"SummaryValue",   			"Base",				new RoleState[] { RoleState.Normal, RoleState.GroupByRow }, ComponentType.AllGrids, SupportedRoleProperties.BorderStyle,
			Role.ScrollRegionSplitterBar,   "ScrollRegionSplitterBar",  UIRoleNames.SplitterBar, new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.ColScrollRegionSplitterBar,"ColScrollRegionSplitterBar", "g_ScrollRegionSplitterBar",	new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.RowScrollRegionSplitterBar,"RowScrollRegionSplitterBar", "g_ScrollRegionSplitterBar",	new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.ScrollRegionSplitBox,  	"ScrollRegionSplitBox",  	UIRoleNames.SplitBox, new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.ColScrollRegionSplitBox,   "ColScrollRegionSplitBox",  "g_ScrollRegionSplitBox", new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.RowScrollRegionSplitBox,   "RowScrollRegionSplitBox",  "g_ScrollRegionSplitBox", new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.SplitterBarIntersection,	"SplitterBarIntersection",	UIRoleNames.SplitterBarIntersection, new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.None,
			Role.RowPreview,				"RowPreview",				"Base",				new RoleState[] { RoleState.Normal, RoleState.Active, RoleState.Selected, RoleState.RowHotTracked, RoleState.DataError, RoleState.TemplateAddRow, RoleState.AddRow, RoleState.EmptyRow, RoleState.FixedRow, RoleState.FilterRow, RoleState.AlternateItem, RoleState.FilteredIn, RoleState.FilteredOut, }, ComponentType.AllGrids, SupportedRoleProperties.BorderStyle,
			Role.GridControlAreaBase,		"ControlAreaBase",			UIRoleNames.ControlArea, new RoleState[] { RoleState.Normal }, ComponentType.All, SupportedRoleProperties.BorderStyle | SupportedRoleProperties.ButtonStyle,
			Role.GridControlArea,			"ControlArea",				"g_ControlAreaBase",new RoleState[] { RoleState.Normal }, ComponentType.Grid | ComponentType.ColumnChooser, SupportedRoleProperties.BorderStyle | SupportedRoleProperties.ButtonStyle,
			Role.DropDownControlArea,		"g-DropDownControlArea",	"g_ControlAreaBase",new RoleState[] { RoleState.Normal }, ComponentType.Combo | ComponentType.DropDown, SupportedRoleProperties.BorderStyle | SupportedRoleProperties.ButtonStyle,
			Role.GridCaption,				"Caption",					"g_Header",			new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.EmptyRowsArea,				"EmptyRowsArea",			"Base",				new RoleState[] { RoleState.Normal }, ComponentType.AllGrids, SupportedRoleProperties.None,
			Role.PageHeader,				"PageHeader",				"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.PageFooter,				"PageFooter",				"Base",				new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.UltraComboEditPortion,		"g-UltraComboEditPortion",	UIRoleNames.EditorControl,	new RoleState[] { RoleState.Normal, RoleState.EditMode, RoleState.Focused, RoleState.ReadOnly }, ComponentType.Combo, SupportedRoleProperties.BorderStyle | SupportedRoleProperties.ButtonStyle,
			Role.ColumnChooserButton,		"ColumnChooserButton",		"g_Button",			new RoleState[] { RoleState.Normal }, ComponentType.Grid, SupportedRoleProperties.ButtonStyle,
			Role.ExpansionIndicator,		"ExpansionIndicator",		UIRoleNames.ExpansionIndicator, new RoleState[] { RoleState.Normal, RoleState.Expanded }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,
			Role.GroupByExpansionIndicator,	"GroupByExpansionIndicator", UIRoleNames.ExpansionIndicator, new RoleState[] { RoleState.Normal, RoleState.Expanded }, ComponentType.Grid, SupportedRoleProperties.BorderStyle,            
		};

        private static object[] CELLPROXY_ROLES = 
		{
            CellProxyRole.CellProxy,                 "CellProxy",                UIRoleNames.Editor, new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.EditMode }, ComponentType.CellProxy, SupportedRoleProperties.BorderStyle | SupportedRoleProperties.ButtonStyle,
        };

        private static object[] TEMPLATE_ROLES = 
        {
            TemplateRole.RowEditTemplatePanel,      "RowEditTemplatePanel",     UIRoleNames.Base,   new RoleState[] { RoleState.Normal }, ComponentType.RowEditTemplate, SupportedRoleProperties.None,
        };

		#region ComponentType Enum

		[ Flags( ) ]
			internal enum ComponentType
		{
			Grid = 0x1,
			Combo = 0x2, 
			DropDown = 0x4,
			ColumnChooser = 0x8,

            // MBS 4/17/08 - RowEditTemplate NA2008 V2
            RowEditTemplate = 0x10,
            CellProxy = 0x20,

			AllGrids = Grid | Combo | DropDown,

            // MBS 4/17/08 - RowEditTemplate NA2008 V2
			//All = AllGrids | ColumnChooser,            
            All = AllGrids | ColumnChooser | RowEditTemplate | CellProxy,           
		}

		#endregion // ComponentType Enum

		#region GetComponentRole

		internal static ComponentRole GetComponentRole( UltraGridLayout layout )
		{
			if ( null != layout )
			{
				UltraGridBase grid = layout.Grid;
				if ( null != grid )
					return grid.ComponentRole;
			}

			return null;
		}

		#endregion // GetComponentRole

		#region GetRoleDefinitions

		internal static UIRoleDefinition[] GetRoleDefinitions( ComponentType matchComponent )
		{
			UIRoleDefinition[] roleDefs;
			string[] roleNames;

			GetRolesHelper( out roleNames, out roleDefs, matchComponent, false, true );

			return roleDefs;
		}

		#endregion // GetRoleDefinitions

		#region GetRoleNames

		internal static string[] GetRoleNames( ComponentType matchComponent )
		{
			UIRoleDefinition[] roleDefs;
			string[] roleNames;

			GetRolesHelper( out roleNames, out roleDefs, matchComponent, true, false );

			return roleNames;
		}

		#endregion // GetRoleNames

		#region GetSortedRoleNamesForCaching

		private static string TranslateTableRoleName( string role )
		{
			const string NO_GRID_PREFIX = "g-";
			if ( role.StartsWith( NO_GRID_PREFIX ) )
				role = role.Remove( 0, NO_GRID_PREFIX.Length );
			else
				role = "Grid" + role;

			return role;
		}

        internal static string[] GetSortedRoleNamesForCaching()
        {
            // MBS 4/22/08 - RowEditTemplate NA2008 V2
            // Added new overload that takes a ComponentType
            return GetSortedRoleNamesForCaching(ComponentType.All ^ ComponentType.CellProxy ^ ComponentType.RowEditTemplate);
        }

		internal static string[] GetSortedRoleNamesForCaching(ComponentType componentType)
		{
            // MBS 4/22/08 - RowEditTemplate NA2008 V2
            // Refactored code to search the appropriate ROLES table, since it is necessary
            // to have a separate role for each proxy/template, but no reason to copy
            // the entire ROLES table for each control that is created when we won't be using
            // most of them
            #region Old Code
            //string[] roles = new string[ ROLES.Length / ROLES_STEP ];

            //for ( int i = 0; i < ROLES.Length; i += ROLES_STEP )
            //{
            //    Role eRole = (Role)ROLES[ i + ROLES_ROLE_ENUM ];
            //    string roleName = TranslateTableRoleName( (string)ROLES[ i + ROLES_ROLE_NAME ] );

            //    int index = (int)eRole;

            //    if ( index >= 0 && index < roles.Length && null == roles[ index ] )
            //        roles[ index ] = roleName;
            //    else
            //        Debug.Assert( false, "Role enum is not in sync with the ROLES array." );
            //}
            #endregion //Old Code
            //
            object[] ROLES_TABLE;
            //
            // Only use the tables if they were specifically requested without flags
            if (componentType == ComponentType.RowEditTemplate)
                ROLES_TABLE = TEMPLATE_ROLES;
            else if (componentType == ComponentType.CellProxy)
                ROLES_TABLE = CELLPROXY_ROLES;
            else
                ROLES_TABLE = ROLES;
            //
            string[] roles = new string[ROLES_TABLE.Length / ROLES_STEP];
            //
            for (int i = 0; i < ROLES_TABLE.Length; i += ROLES_STEP)
            {
                Role eRole = (Role)ROLES_TABLE[i + ROLES_ROLE_ENUM];
                string roleName = TranslateTableRoleName((string)ROLES_TABLE[i + ROLES_ROLE_NAME]);

                int index = (int)eRole;

                if (index >= 0 && index < roles.Length && null == roles[index])
                    roles[index] = roleName;
                else
                    Debug.Assert(false, "Role enum is not in sync with the ROLES array.");
            }

			return roles;
		}

		#endregion // GetSortedRoleNamesForCaching

		#region GetRolesHelper

		internal static void GetRolesHelper( out string[] roleNames, out UIRoleDefinition[] roleDefs, 
			ComponentType matchComponent, bool getRoleNames, bool getRoleDefs )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList roleDefsList = getRoleDefs ? new ArrayList( ) : null;
			//ArrayList roleNamesList = getRoleNames ? new ArrayList( ) : null;
			List<UIRoleDefinition> roleDefsList = getRoleDefs ? new List<UIRoleDefinition>() : null;
			List<string> roleNamesList = getRoleNames ? new List<string>() : null;

			// Loop through the ROLES table (flat static array) and create UIRoleDefinition from it.
			// 
            // MBS 4/22/08 - RowEditTemplate NA2008 V2
            // Refactored code to search the appropriate ROLES table, since it is necessary
            // to have a separate role for each proxy/template, but no reason to copy
            // the entire ROLES table for each control that is created when we won't be using
            // most of them
            #region OLD CODE
            //for ( int i = 0; i < ROLES.Length; i += ROLES_STEP )
            //{
            //    ComponentType associatedComponents = (ComponentType)ROLES[ i + ROLES_ASSOCIATED_COMPONENTS ];

            //    if ( 0 == ( matchComponent & associatedComponents ) )
            //        continue;

            //    // MD 7/26/07 - 7.3 Performance
            //    // FxCop - Remove unused locals
            //    //Role eRole = (Role)ROLES[ i + ROLES_ROLE_ENUM ];

            //    string role = TranslateTableRoleName( (string)ROLES[ i + ROLES_ROLE_NAME ] );
            //    string baseRole = (string)ROLES[ i + ROLES_BASE_ROLE_NAME ];
            //    RoleState[] states = (RoleState[])ROLES[ i + ROLES_STATES ];
            //    SupportedRoleProperties supportedProps = (SupportedRoleProperties)ROLES[ i + ROLES_SUPPORTED_PROPS ];

            //    RoleState state = 0;
            //    for ( int j = 0; j < states.Length; j++ )
            //        state |= states[ j ];

            //    baseRole = baseRole.Replace( "g_", "Grid" );

            //    if ( null != roleDefsList )
            //        roleDefsList.Add( new UIRoleDefinition( role, baseRole, state, supportedProps ) );

            //    if ( null != roleNamesList )
            //        roleNamesList.Add( role );
            //}
            #endregion //OLD CODE
            //
            // First, build a list of all the roles, since this is what we were doing by default            
            GetRolesHelperHelper(matchComponent, roleDefsList, roleNamesList, ROLES);
            //
            // Next add the proxy, if necessary
            if ((matchComponent & ComponentType.CellProxy) != 0)
                GetRolesHelperHelper(matchComponent, roleDefsList, roleNamesList, CELLPROXY_ROLES);
            //
            // Finally add the template, if necessary
            if ((matchComponent & ComponentType.RowEditTemplate) != 0)
                GetRolesHelperHelper(matchComponent, roleDefsList, roleNamesList, TEMPLATE_ROLES);
            
            // MD 8/10/07 - 7.3 Performance
			// Use generics
			//roleDefs = null != roleDefsList ? (UIRoleDefinition[])roleDefsList.ToArray( typeof( UIRoleDefinition ) ) : null;
			//roleNames = null != roleNamesList ? (string[])roleNamesList.ToArray( typeof( string ) ) : null;
			roleDefs = null != roleDefsList ? roleDefsList.ToArray() : null;
			roleNames = null != roleNamesList ? roleNamesList.ToArray() : null;
		}

        // MBS 4/22/08 - RowEditTemplate NA2008 V2        
        private static void GetRolesHelperHelper(ComponentType matchComponent, List<UIRoleDefinition> roleDefsList, List<string> roleNamesList, object[] ROLES_TABLE)
        {
            for (int i = 0; i < ROLES_TABLE.Length; i += ROLES_STEP)
            {
                ComponentType associatedComponents = (ComponentType)ROLES_TABLE[i + ROLES_ASSOCIATED_COMPONENTS];

                if (0 == (matchComponent & associatedComponents))
                    continue;

                string role = TranslateTableRoleName((string)ROLES_TABLE[i + ROLES_ROLE_NAME]);
                string baseRole = (string)ROLES_TABLE[i + ROLES_BASE_ROLE_NAME];
                RoleState[] states = (RoleState[])ROLES_TABLE[i + ROLES_STATES];
                SupportedRoleProperties supportedProps = (SupportedRoleProperties)ROLES_TABLE[i + ROLES_SUPPORTED_PROPS];

                RoleState state = 0;
                for (int j = 0; j < states.Length; j++)
                    state |= states[j];

                baseRole = baseRole.Replace("g_", "Grid");

                if (null != roleDefsList)
                    roleDefsList.Add(new UIRoleDefinition(role, baseRole, state, supportedProps));

                if (null != roleNamesList)
                    roleNamesList.Add(role);
            }
        }
		#endregion // GetRolesHelper

		#region GetComponentRoleName

		internal static string GetComponentRoleName( StyleUtils.ComponentType componentType )
		{
			switch ( componentType )
			{
				case StyleUtils.ComponentType.Grid:
					return UltraGridRole.GRID_ROLE_NAME;
				case StyleUtils.ComponentType.Combo:
					return UltraGridRole.COMBO_ROLE_NAME;
				case StyleUtils.ComponentType.DropDown:
					return UltraGridRole.DROPDOWN_ROLE_NAME;
				case StyleUtils.ComponentType.ColumnChooser:
					return UltraGridRole.COLUMN_CHOOSER_ROLE_NAME;
				default:
					throw new ArgumentException( "Unknown component type" );
			}
		}

		#endregion // GetComponentRoleName

		#region GetComponentType

		internal static ComponentType GetComponentType( UltraGridBase grid )
		{
			if ( grid is ColumnChooserGrid )
				return ComponentType.ColumnChooser;
			if ( grid is UltraGrid )
				return ComponentType.Grid;
			else if ( grid is UltraCombo )
				return ComponentType.Combo;
			else if ( grid is UltraDropDown )
				return ComponentType.DropDown;            
			else
				throw new NotSupportedException( );
		}

		#endregion // GetComponentType

		#region GetHeaderRole

		internal static Role GetHeaderRole( HeaderBase header, bool isGroupByBoxButton )
		{
			StyleUtils.Role eRole;

			if ( header is ColumnHeader )
				eRole = ! isGroupByBoxButton ? StyleUtils.Role.ColumnHeader : StyleUtils.Role.GroupByBoxColumnHeader;
			else if ( header is GroupHeader )
				eRole = StyleUtils.Role.GroupHeader;
			else if ( header is BandHeader )
				eRole = StyleUtils.Role.BandHeader;
			else
			{
				Debug.Assert( false );
				eRole = StyleUtils.Role.Header;
			}

			return eRole;
		}

		#endregion // GetHeaderRole

		#region GetControlAreaRole

		internal static StyleUtils.Role GetControlAreaRole( UltraGridLayout layout )
		{
			return layout.Grid is UltraGrid ? Role.GridControlArea : Role.DropDownControlArea;
		}

		#endregion // GetControlAreaRole

		#region ViewStyleToDisplayStyle

		internal static EmbeddableElementDisplayStyle ViewStyleToDisplayStyle( Infragistics.Win.AppStyling.ViewStyle viewStyle )
		{
			EmbeddableElementDisplayStyle ret = EmbeddableElementDisplayStyle.Default;

			switch ( viewStyle )
			{
				case Infragistics.Win.AppStyling.ViewStyle.Office2000:
					ret = EmbeddableElementDisplayStyle.Office2000;
					break;
				case Infragistics.Win.AppStyling.ViewStyle.Office2003:
					ret = EmbeddableElementDisplayStyle.Office2003;
					break;
				case Infragistics.Win.AppStyling.ViewStyle.OfficeXp:
					ret = EmbeddableElementDisplayStyle.OfficeXP;
					break;
				case Infragistics.Win.AppStyling.ViewStyle.Standard:
					ret = EmbeddableElementDisplayStyle.Standard;
					break;
				case Infragistics.Win.AppStyling.ViewStyle.VisualStudio2005:
					ret = EmbeddableElementDisplayStyle.VisualStudio2005;
					break;

                // MBS 9/14/06
                case Infragistics.Win.AppStyling.ViewStyle.Office2007:
                    ret = EmbeddableElementDisplayStyle.Office2007;
                    break;
			}

			return ret;
		}

		#endregion // ViewStyleToDisplayStyle
    }

	#endregion // StyleUtils Class
}
