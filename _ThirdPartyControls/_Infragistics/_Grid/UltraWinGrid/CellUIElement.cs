#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Diagnostics;

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Split the CellUIElement into CellUIElementBase class and CellUIElement class so 
	// the filter cell element can derive from CellUIElementBase.
	// For original code look in the source safe for 3/11/05 version.
	//

	#region CellUIElementBase Class

	/// <summary>
	/// The ui element base class for representing cells.
	/// </summary>
	// SSP 3/11/03 - Row Layout Functionality
	// Changed the base class from UIElement to AdjustableUIElement
	//
	//public class CellUIElement : UIElement
	public abstract class CellUIElementBase : AdjustableUIElement
	{
		private int cellChildElementsCacheVersion = 0;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal bool hasAlreadyShownTheToolTipSinceMouseHover = false;

		// AS 1/24/03 UWG1960
		internal bool isMouseOverCell = false;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//// SSP 12/21/01
		//// Added this flag so that the cell only hides the tooltip if it's
		//// showing it.
		////
		//internal bool showingToolTip = false;

		// SSP 5/12/03 - Optimizations
		// Added a way to just draw the text without having to embedd an embeddable ui element in
		// cells to speed up rendering.
		//
		// SSP 6/30/03 UWG2379
		// Changed the cell display style member's default value from CellDisplayStyle.FullEditorDisplay to
		// CellDisplayStyle.Default. This should not have any effect whatsoever on the behaviour since
		// we are initializing this in the PositionChildElements to the right value every time. Reason
		// for this change is that in the row-layout designer creation filter is used to prevent positioning
		// of the embeddable elements and position a text ui element with the key of the column in cells.
		// In DrawBackcolor we only draw the back color if cellDisplayStyle is set to something other than
		// FullEditorDisplay. 
		//
		//private CellDisplayStyle cellDisplayStyle = CellDisplayStyle.FullEditorDisplay;
		internal CellDisplayStyle cellDisplayStyle = CellDisplayStyle.Default;

		// SSP 5/2/03 - Optimizations
		// Cache the border sides and border style.
		//
		internal UIElementBorderStyle borderStyle = UIElementBorderStyle.None;
		private Border3DSide borderSides = Border3DSide.All;

		// SSP 5/14/03 - Optimizations
		// This internal member variable will be used by the EmbeddableEditorOwnerInfo.GetEditor
		// to return the editor directly from the cell element rather than going through the normal
		// editor resolution mechanism which involves checking if the cell has been created or not
		// since now we support cell level value lists and editors. EmbeddableEditorOwnerInfo.GetEditor
		// method gets called quite a lot of times.
		//
		internal EmbeddableEditorBase embeddableEditorLastUsed = null;

		// SSP 5/19/03 - Fixed headers
		//
		private Rectangle clipSelfRect = Rectangle.Empty;

		// SSP 11/4/04 - Optimizations
		//
		private UltraGridRow row = null;

		// SSP 4/1/05 - Optimizations
		//
		private UltraGridColumn column = null;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//// SSP 5/14/03 - Hiding Individual Cells Feature
		//// A flag by the row cell area element's position child elements to indicate that the next
		//// cell is hidden.
		////
		//internal bool nextCellHidden = false;

		// MRS 4/6/05 - Span Resizing for RowLayouts
		private bool wasShowingSpanCursor = false;

        // MBS 3/22/09 - TFS14728
        internal bool hasFiredCellClick;

		// SSP 3/11/03 - Row Layout Functionality
		// Changed the base class from UIElement to AdjustableUIElement
		//
		// internal CellUIElement( UIElement parent ) : base( parent )
		internal CellUIElementBase( UIElement parent ) : base( parent, true, true )
		{	
		}

		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>CellUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="column">Associated <b>Column</b></param>
		public CellUIElementBase( UIElement parent, UltraGridColumn column ) : this( parent )
		{	
			// SSP 11/4/04 - Optimizations
			// If we pass in null then the Row property will get it using the GetContext method 
			// every time.
			//
			//this.InitializeCell(column);
			this.InitializeCell( column, null );
		}

		internal void InitializeCell( UltraGridColumn column, UltraGridRow row )
		{
			this.PrimaryContext = column;

			// SSP 11/4/04 - Optimizations
			// Also added the row parameter to this method.
			//
			this.row = row;

			// SSP 4/1/05 - Optimizations
			// Cache the column as well.
			//
			this.column = column;

			// SSP 5/6/05 - NAS 5.2 Extension of Summaries Functionality
			// 
			this.clipSelfRect = Rectangle.Empty;
		}
 
		internal int VerifiedCellChildElementsCacheVersion
		{
			get
			{
				return this.cellChildElementsCacheVersion;
			}
		}

		/// <summary>
		/// The UltraGridRow object represents a row of data in the grid. An UltraGridRow corresponds to a single record in an underlying data source. This property is read-only at run-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The UltraGridRow object represents a single row of data, and corresponds to a single record in the underlying recordset. Rows occupy a position in the data hierarchy of the UltraWinGrid between Cells and Bands. The UltraGridRow object is always the child of an UltraGridBand object, and its children are UltraGridCell objects.</p>
		/// <p class="body">Much of the data-binding functionality of the grid involves working with the UltraGridRow object. Whenever an UltraGridRow object is loaded by the grid, the <b>InitializeRow</b> event is fired.</p>
		/// <p class="body">UltraGridRow objects can influence the formatting of the cells they contain through the setting of the UltraGridRow's <b>CellAppearance</b> property. Rows can also be formatted independently of the cells they contain. Frequently, cells are drawn from the top of the row to the bottom and are aligned edge to edge so that they occupy the entire area of the row; the row itself is not visible because cells are always "above" the row in the grid's z-order. However it is possible to specify spacing between and around cells that lets the underlying UltraGridRow object show through. Only then will formatting applied directly to the UltraGridRow object be visible to the user.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				// SSP 11/4/04 - Optimizations
				// Added the row parameter to the InitializeCell method.
				//
				// --------------------------------------------------------------------------------------
				if ( null != this.row )
					return this.row;

				
				// --------------------------------------------------------------------------------------

				return (Infragistics.Win.UltraWinGrid.UltraGridRow)this.Parent.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ) );
			}
		}

 
		/// <summary>
		/// Returns the UltraGridColumn object. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Column</b> property of an object refers to a specific column in the grid as defined by an UltraGridColumn object. You use the <b>Column</b> property to access the properties of a specified UltraGridColumn object, or to return a reference to an UltraGridColumn object.</p>
		/// <p class="body">An UltraGridColumn object represents a single column in the grid. The UltraGridColumn object is closely linked with a single underlying data field that is used to supply the data for all the cells in the column (except in the case of unbound columns, which have no underlying data field). The UltraGridColumn object determines what type of interface (edit, dropdown list, calendar, etc.) will be used for individual cells, as well as controlling certain formatting and behavior-related settings, such as data masking, for the cells that make up the column.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridColumn Column
		{
			get
			{
				// SSP 4/1/05 - Optimizations
				// Cache the column as well.
				//
				//return this.PrimaryContext as Infragistics.Win.UltraWinGrid.UltraGridColumn;
				return this.column;
			}
		}

	
		/// <summary>
		/// Returns true if this  element needs to draw a focus rect. This should 
		/// be overridden since the default implementation always returns false. 
		/// </summary>
		/// <remarks>Even if this property returns true the focus will not be drawn unless the control has focus.</remarks>
		protected override bool DrawsFocusRect
		{
			get
			{
				// AS - 11/21/01
				// Even though HasCell is returning true, this.Cell returns null.
				//
				//				if ( !this.HasCell ||
				//					 !this.Cell.IsActiveCell )
				//					return false;
				// SSP 5/1/03 - Optimizations
				//
				
				// SSP 4/1/05 - Optimizations
				// In order for a cell to be active its row must be active. Also use the 
				// IsActiveRowInternal which does not go through the process of verifying that the
				// active row is valid.
				//
				if ( ! this.Row.IsActiveRowInternal )
					return false;

				UltraGridCell cell = this.Row.GetCellIfAllocated( this.Column );
				if ( null == cell || ! cell.IsActiveCell )
					return false;
 
				// JJD 1/21/02 - UWG951
				// Buttons draw the focus inside their borders 
				//
				// SSP 7/18/03 - Cell level Style property
				// Added Style property to the cell object.
				//
				//if ( this.Column.IsColumnStyleButton )
				if ( this.Column.IsColumnStyleButton( this.Row ) )
					return false;

				// SSP 5/1/03 - Cell Level Editor
				//
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//IValueList valueList = this.Column.ValueList;
				IValueList valueList = cell.ValueListResolved;

				if ( cell.IsInEditMode &&
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
					//( this.Column.StyleResolved != ColumnStyle.DropDownList ||
					( cell.StyleResolved != ColumnStyle.DropDownList ||
					  valueList == null || valueList.IsDroppedDown ) 
					// SSP 2/10/05
					// If the editor is a check editor, draw the focus rect even when it's in edit mode.
					//
					&& ! ( cell.EditorResolved is Infragistics.Win.CheckEditor )
					)
					return false;

				//UltraGridCell cell = this.Cell;

				RowScrollRegion rsr = this.GetContext( typeof( RowScrollRegion ) ) as RowScrollRegion;
				ColScrollRegion csr = this.GetContext( typeof( ColScrollRegion ) ) as ColScrollRegion;

				return ( rsr == cell.Layout.Grid.ActiveRowScrollRegion &&
					csr == cell.Layout.Grid.ActiveColScrollRegion );
			}
		}

		/// <summary>
		/// Returns true if the cell has already been created for
		/// this column and row
		/// </summary>
		public bool HasCell 
		{
			get
			{
				// SSP 7/29/02
				// Instead of forcing creation of cells collection, call the new HasCell off
				// the row which will return false if the cells collection has not been created
				// yet.
				//
				//return this.Row.Cells.HasCell( this.Column );
				return this.Row.HasCell( this.Column );
			}
		}
 
		#region IsInEditMode

		// SSP 12/2/05 BR07533
		// 
		internal bool IsInEditMode
		{
			get
			{
				UltraGridCell cell = null != this.Row ? this.Row.GetCellIfAllocated( this.Column ) : null;
				return null != cell && cell.IsInEditMode && GridUtils.IsElemFromActiveRsrCsr( this );
			}
		}

		#endregion // IsInEditMode

		/// <summary>
		/// Returns the cell associated with the object. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Cell</b> property of an object refers to a specific cell in the grid as defined by an UltraGridCell object. You use the <b>Cell</b> property to access the properties of a specified UltraGridCell object, or to return a reference to an UltraGridCell object.</p>
		/// </remarks>
		// The associated Cell object (read-only). This will create
		// the cell if it hasn't already been created
		public Infragistics.Win.UltraWinGrid.UltraGridCell Cell
		{
			get
			{
				try
				{
					return this.Row.Cells[ this.Column ]; 
				}
				catch( Exception )
				{
					return null;
				}
			}
		}


        /// <summary>
        /// Returns an object of requested type that relates to the element or null.
        /// </summary>
        /// <param name="type">The requested type or null to pick up default context object.</param>
        /// <param name="checkParentElementContexts">If true will walk up the parent chain looking for the context.</param>
        /// <returns>Returns null or an object of requested type that relates to the element.</returns>
        /// <remarks> Classes that override this method normally need to override the <see cref="Infragistics.Win.UIElement.ContinueDescendantSearch(System.Type,System.Object[])"/> method as well.</remarks>
        // AS - 10/31/01
		//public virtual object GetContext( Type type )
		public override object GetContext( Type type, bool checkParentElementContexts )
		{
			// Check for the cell type since this is not in the
			// Contexts collection so we can create the cell objects
			// lazily
			//
			// SSP 4/16/05 - NAS 5.2 Filter Row
			// Added UltraGridFilterCell class which derives from UltraGridCell. So
			// check for derived types as well.
			//
			//if ( typeof(Infragistics.Win.UltraWinGrid.UltraGridCell) == type )
			// SSP 8/26/06 - NAS 6.3 - Optimization
			// 
			//if ( UIElement.IsContextOfType( this.Cell, type ) )
			if ( GridUtils.IsObjectOfType( type, typeof( UltraGridCell ) )
				&& UIElement.IsContextOfType( this.Cell, type ) )
				return this.Cell;

			// Call the base class implementation
			//
			return base.GetContext( type, checkParentElementContexts );
		
		}

		#region ContinueDescendantSearch

		// SSP 12/19/02
		// Optimizations.
		// Overrode ContinueDescendantSearch method.
		//
		/// <summary>
		/// This method is called from <see cref="Infragistics.Win.UIElement.GetDescendant(Type, object[])"/> as an optimization to
		/// prevent searching down element paths that can't possibly contain the element that is being searched for. 
		/// </summary>
		/// <remarks><seealso cref="Infragistics.Win.UIElement.ContinueDescendantSearch"/></remarks>
        /// <returns>Returns false if the search should stop walking down the element chain because the element can't possibly be found.</returns>
		protected override bool ContinueDescendantSearch( Type type, object[] contexts )
		{
			if ( null != contexts )
			{
				UltraGridRow row = this.Row;
				UltraGridColumn column = this.Column;

				for ( int i = 0; i < contexts.Length; i++ )
				{
					object context = contexts[i];

					if ( null != context )
					{
						// MD 8/7/07 - 7.3 Performance
						// FxCop - Do not cast unnecessarily
						//if ( context is UltraGridCell )
						//{
						//    UltraGridCell cell = (UltraGridCell)context;
						UltraGridCell cell = context as UltraGridCell;

						if ( cell != null )
						{
							return cell.Row == row && cell.Column == column;
						}

						if ( context is UltraGridColumn )
							return column == context;
					}
				}
			}

			return true;
		}

		#endregion // ContinueDescendantSearch


		// SSP 4/5/02
		// This method was necessary to find a descendant that's a EmbeddableUIElementBase
		// because the GetDescendant requires an exact type (meaning it does == type
		// comparision so it won't find EmbeddableUIElementBase derived classes.)
		//
		internal EmbeddableUIElementBase FindEmbeddableUIElement( )
		{
			return this.FindEmbeddableUIElement( false );
		}
		
		// SSP 1/3/02 Optimization
		// Added an overload that takes in extract parameter.
		//
		internal EmbeddableUIElementBase FindEmbeddableUIElement( bool extract )
		{
			return CellUIElementBase.FindEmbeddableUIElement( this.childElementsCollection, extract );
		}

		// SSP 11/3/04 - Merged Cell Feature
		// Added a static overload so the MergedCellUIElement can call it.
		//
		internal static EmbeddableUIElementBase FindEmbeddableUIElement( 
			UIElementsCollection childElementsCollection, bool extract )
		{
			if ( null != childElementsCollection )
			{
				int count = childElementsCollection.Count;
				for ( int i = 0; i < count; i++ )
				{
					EmbeddableUIElementBase elem = childElementsCollection[i] as EmbeddableUIElementBase;
				
					if ( null != elem )
					{
						// If extract is true, then null out the entry.
						//						
						if ( extract )
						{
							// SSP 6/04/03 - Optimizations
							//
							if ( 1 == count )
								childElementsCollection.Clear( );
							else
								childElementsCollection[i] = null;
						}

						return elem;
					}
				}
			}

			return null;
		}

		// SSP 11/12/04 - Merged Cell Feature
		// Added ClipSelfRect property.
		//
		/// <summary>
		/// Returns the clip self rect which is used in fixed headers mode.
		/// </summary>
		// SSP 11/18/05 BR07743
		// Made public. This if for use by creation filters.
		// 
		//internal Rectangle ClipSelfRect
		public Rectangle ClipSelfRect
		{
			get
			{
				return this.clipSelfRect;
			}
			// SSP 11/18/05 BR07743
			// Added set as well. This if for use by creation filters.
			// 
			set
			{
				this.clipSelfRect = value;
			}
		}

		#region Offset

		// SSP 5/3/05 - Optimizations
		// Overrode Offset so we can offset the clipSelfRect as well.
		//
		/// <summary>
		/// Overridden. Offsets this element's rect and (optionally) all of its descendant elements.
		/// </summary>
		/// <param name="deltaX">The number of pixels to offset left/right</param>
		/// <param name="deltaY">The number of pixels to offset up/down </param>
		/// <param name="recursive">If true will offset all descendant elements as well</param>
		public override void Offset( int deltaX, int deltaY, bool recursive )
		{
			if ( ! this.clipSelfRect.IsEmpty )
				this.clipSelfRect.Offset( deltaX, deltaY );

			base.Offset( deltaX, deltaY, recursive );
		}

		#endregion // Offset

		// SSP 1/2/03
		// Optimizations.
		// Added methods off the owner that the emebeddable element can use to notify the owner
		// of mouse messages. As a result we don't need to hook into the emebddable element.
		//
		

		//private static int mouseDownCount = 0;
		//int count = 0;

		// SSP 1/2/03
		// Made this internal from private.
		//
		//private void OnEmbeddableElementMouseDown( object sender, Infragistics.Win.EmbeddableMouseDownEventArgs e )
		internal void OnEmbeddableElementMouseDown( object sender, Infragistics.Win.EmbeddableMouseDownEventArgs e )
		{
			//Debug.WriteLine( ++mouseDownCount + ": EmbeddableElement.MouseDown raised." );

			// SSP 3/20/02 - Row Layout Functionality
			// In layout designer, don't process the OnMouseDown.
			//
			if ( this.RowLayoutDesignerElement )
			{
				return;
			}

			UltraGridCell cell = this.Cell;
			UltraGridColumn column = this.Column;
			UltraGridRow row = this.Row;

			Debug.Assert( null != cell && null != row && null != column, "No row or column !" );
			if ( null == row || null == column || null == cell )
				return;

            // MBS 3/22/09 - TFS14728
            this.hasFiredCellClick = false;
			
			if (  e.MouseArgs.Button == MouseButtons.Left )
			{
				// JJD 10/12/01 - UWG521
				// If the control key is down then return false so that
				// the selection manager gets a crack at selecting this
				// cell.
				//
				Keys modifierKeys = System.Windows.Forms.Control.ModifierKeys;
				if ( ( modifierKeys & Keys.Control ) == Keys.Control
					// SSP 10/17/06 BR16793
					// The same goes for Shift key.
					// 
					|| ( modifierKeys & Keys.Shift ) == Keys.Shift )
				{
					// AS 1/13/06
					// The link editor (and possibly other editors) set EatMessage to true
					// and as such they will process this message and not go up the parent
					// chain. When the control key is down, if we want to select the cell
					// we need to make sure that EatMessage is false. To not introduce any
					// issues with other editors, I'm only going to do this for a link
					// element type.
					//
					if (EmbeddableClickEventArgs.EmbeddableElementType.Link == e.ElementType
						// SSP 9/12/06 - NAS 6.3
						// Don't set the EatMessage to false if in edit mode. In that case we don't
						// want the selection strategy to select cells.
						// 
						&& ( null == cell || ! cell.IsInEditMode ) )
						e.EatMessage = false;

					return;
				}

				// SSP 7/29/02 UWG1457
				// Checking for CellClickActionResolved is not sufficient as there are
				// other settings of individual columns and cells that need to be taken
				// int account. So commented out below code and added more appropriate
				// code.
				//
				// -----------------------------------------------------------------------
				
				// SSP 7/29/02 UWG1457
				// Also check for e.IsButton.
				//
				// SSP 10/15/03 UWG1706
				// When the cell click action is not edit (it's CellSelect or RowSelect), and the
				// editor is clicked upon, then only go into edit mode if it's a drop down button
				// of a valuelist or a datatime editor. To do that check for SupportsDropDown.
				//
				//if ( e.IsButton || CellClickAction.Edit == column.Band.CellClickActionResolved )
				if ( ( e.IsButton && null != cell.EditorResolved && cell.EditorResolved.SupportsDropDown 
					// SSP 10/26/05 - NAS 6.1 Formatted Link Label
					// Added below condition. If a link is being clicked, then don't select the cell.
					// 
					|| EmbeddableClickEventArgs.EmbeddableElementType.Link == e.ElementType ) 
					// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					// Added CellClickActionResolved on the cell object.
					//
					//|| CellClickAction.Edit == column.Band.CellClickActionResolved
					//// JAS v5.2 Select Text On Cell Click 4/7/05
					//|| CellClickAction.EditAndSelectText == column.Band.CellClickActionResolved 
					|| CellClickAction.Edit == cell.CellClickActionResolved
					|| CellClickAction.EditAndSelectText == cell.CellClickActionResolved 
					)
				{
					// Only activate the cell if not already in edit mode. If it's already
					// in edit mode, then don't call SetFocusActivate on it.
					//
					if ( !cell.IsInEditMode )
					{
						// Also only go into edit mode if the cell can enter edit mode (based on
						// various activation settings).
						// 
						if ( cell.CanEnterEditMode )
						{
							//cell.SetFocusAndActivate( true, true );
							cell.SetFocusAndActivate( true, true );

							// SSP 1/24/02 UWG960
							// If the cell is in edit mode and the cell element is clicked upon,
							// the grid will steal the focus, and the edit control won't
							// have the focus anymore. So set the focus to the edit control.
							//
							this.EnsureEditControlFocused( );

							e.EatMessage = true;
						}
							// SSP 10/26/05 - NAS 6.1 Formatted Link Label
							// Added below else-if block. If a link is being clicked, then don't 
							// select the cell.
							// 
						else if ( EmbeddableClickEventArgs.EmbeddableElementType.Link == e.ElementType )
						{
							// AS 1/13/06 BR08945
							// I discussed this with Joe and we only want the link editor to process
							// the message if the cell has been activated. Since the link editor sets
							// EatMessage to true, we need to explicitly set EatMessage to false
							// when the cell cannot be activated.
							//
							//e.EatMessage = true;
							// AS 1/18/06 BR08945
							// The cell could be active but disabled so don't show the link. I've 
							// also rewritten this assuming that we don't want it to process the 
							// message and only do so if the cell becomes activated. 
							
							
							
							
							
							
							//
							//if (cell.Activate() == false)
							//	e.EatMessage = true;
							//else
							//	e.EatMessage = false;
							//
							//// in either case, we don't want it percolating
							//// up the parent chain
							//e.NotifyParentElement = false;
							bool eatMessage = false;

							// AS 1/27/06 BR09428
							// We cannot activate the cell of an ultradropdownbase derived
							// control. We discussed this and the row area of that control
							// should not be interactive.
							//
							//if (cell.ActivationResolved != Activation.Disabled)
							if (cell.ActivationResolved != Activation.Disabled &&
								null != this.Grid)
							{
								UltraGrid grid = cell.Layout == null ? null : cell.Layout.Grid as UltraGrid;

								// try to clear the selection but that will not prevent
								// the link from being clicked if the cell is activated
								if (grid != null)
									grid.ClearAllSelected();

								cell.SetFocusAndActivate( true, false );

								if (cell.IsActiveCell)
									eatMessage = true;

								// whether it activated or not, we don't want it percolating
								// up the parent chain. this is important because
								// when the message gets to the parent editor element
								// it may cause us to try and activate the cell again
								// even though the programmer may have just cancelled it 
								// already
								e.NotifyParentElement = false;
							}
							else
							{
								// when its disabled, we want it to percolate up
								// since that will likely select the cell/row
								e.NotifyParentElement = true;
							}

							e.EatMessage = eatMessage;

						}

                        // MBS 3/23/09 - TFS14728
                        // If the cell isn't in edit mode and we're going to eat the message, we should fire the 
                        // CellClick event since no other processing of the click is going to occur
                        if (e.EatMessage && !e.NotifyParentElement)
                            this.OnClick();  
					}
						// SSP 9/11/03 UWG2401
					else
					{
						// Eat the message if the cell's already in edit mode. We don't want
						// the selection strategy to be processing the mouse down and selecting
						// the cell when it's already in edit mode.
						//
						e.EatMessage = true;
					}
				}
				// -----------------------------------------------------------------------
			}		
		
				// If mouse button other than left was clicked
			else
			{
				Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = this.Column.Layout.ActiveCell;

				// If the cell is in edit mode, then just make sure that the editor has
				// focus.
				//
				if ( activeCell == cell && cell.IsInEditMode )
				{
					// SSP 1/24/02 UWG960
					// If the cell is in edit mode and the cell element is clicked upon,
					// the grid will steal the focus, and the edit control won't
					// have the focus anymore. So set the focus to the edit control.
					//
					this.EnsureEditControlFocused( );
					e.EatMessage = true;
				}
					// If a different cell than the one that's in edit mode was clicked,
					// then exit the edit mode on the cell that's in edit mode.
					//
				else
				{
					if ( activeCell != null )
						activeCell.ExitEditMode();

					// SSP 10/21/04 UWG3643
					// Embeddable ui element does not call base element's OnMouseDown by default.
					// What this means that the ancestor elements of the editor element never get
					// OnMouseDown and things like mouse panning do not work. To fix this set 
					// the NotifyParentElement property off the embeddable mouse down event args 
					// to true so the embeddable ui element calls the base OnMouseDown.
					// NOTE: We are doing this only when a mouse button other than left is clicked
					// on a cell that's not in edit mode.
					//
					if ( null == activeCell || ! activeCell.IsInEditMode )
						e.NotifyParentElement = true;
				}
			}

			
		}

		// SSP 4/30/03 UWG1988
		// Moved the code from the PositionChildElement to this new helper method so we can also
		// make use of the same code from inside the CellButtonUIElement's PositionChildElements.
		//
		/// <summary>
		/// Creates a new image element if passed in imageElem is null and assigns it to imageElem. It
		/// positions the passed in image in the rect keeping the aspect ratio. It sets the image elem's
		/// Rect property to the calculated rect of the image element.
		/// </summary>
		/// <param name="image"></param>
		/// <param name="imageElem"></param>
		/// <param name="workRect"></param>
		/// <param name="hAlign"></param>
		/// <param name="vAlign"></param>
		internal static void PositionImageElement( Image image, ImageUIElement imageElem, Rectangle workRect, HAlign hAlign, VAlign vAlign )
		{
			Size imageSize = image.Size;

			Debug.Assert( imageSize.Height > 0 && imageSize.Width > 0,
				"Image with height and/or width 0 or less !" );

			if ( imageSize.Height > 0 && imageSize.Width > 0 
				&& workRect.Height > 0 && workRect.Width > 0 )
			{
				// Leave some padding.
				//
				if ( workRect.Height > 4 && workRect.Width > 4 )
					workRect.Inflate( -1, -1 );

				// Calculate the scaling factor. We want to maintain the
				// same aspect ration for the height and the width.
				//
				double xScaleFactor = (double)workRect.Width / imageSize.Width;
				double yScaleFactor = (double)workRect.Height / imageSize.Height;

				// Use the minimum of x and y scale factors.
				//
				double scaleFactor = Math.Min( xScaleFactor, yScaleFactor );

				// Only scale if the image size is bigger than the sapce we
				// have. If it's smaller, then don't need to stretch it.
				//
				if ( scaleFactor < 1.0 )
				{
					imageSize.Width = Math.Max( 1, (int)( scaleFactor * imageSize.Width ) );
					imageSize.Height = Math.Max( 1, (int)( scaleFactor * imageSize.Height ) );
				}

				Rectangle imageRect = new Rectangle( workRect.Location, imageSize );

				// If text alignments were not set, then center the image
				//
				if ( Infragistics.Win.HAlign.Default == hAlign )
					hAlign = Infragistics.Win.HAlign.Center;
				if ( Infragistics.Win.VAlign.Default == vAlign )
					vAlign = Infragistics.Win.VAlign.Middle;

				DrawUtility.AdjustHAlign( hAlign, ref imageRect, workRect );
				DrawUtility.AdjustVAlign( vAlign, ref imageRect, workRect );
				
				imageElem.Image = image;
				imageElem.Scaled = true;
				imageElem.Rect = imageRect;
			}
		}

		// SSP 6/5/03 - Fixed headers
		// Overrode ClipSelf and Region properties.
		//
		#region ClipSelf

		/// <summary>
		/// Returning true causes all drawing of this element to be expicitly clipped
		/// to its region
		/// </summary>
		protected override bool ClipSelf 
		{ 
			get 		  
			{ 
                return ! this.clipSelfRect.IsEmpty;
			} 
		}
 
		#endregion // ClipSelf

		#region Region

		/// <summary>
		/// Returns the region of this element. The deafult returns the element's
		/// Rect as a region. This method can be overriden to supply an irregularly
		/// shaped region 
		/// </summary>
		public override System.Drawing.Region Region
		{
			get
			{
				// The reason why we are creating a new instance of region is because the element's
				// Draw method disposes of this after accessing it.
				//
				return ! this.clipSelfRect.IsEmpty ? new Region( this.clipSelfRect ) : base.Region;
			}
		}
 
		#endregion // Region

		#region IntersectInvalidRect

		// SSP 8/6/04 UWG3380
		// Overrode IntersectInvalidRect to fix the problem where a check indicator in 
		// a non-fixed cell that was scrolled under a fixed cell was not clipped and
		// appeared through the fixed cell. This is because the themes-drawing does not
		// honor UIElement.Region property that we are overriding.
		//
		/// <summary>
		/// Returns the intersection of the element's rect with the invalid rect for the
		/// current draw operation.
		/// </summary>
		/// <param name="invalidRect">Invalid rect</param>
		/// <returns>The intersection of the element's rect with the invalid rect.</returns>
		protected override Rectangle IntersectInvalidRect( Rectangle invalidRect )
		{
			return this.clipSelfRect.IsEmpty
				? base.IntersectInvalidRect( invalidRect )
				: Rectangle.Intersect( invalidRect, this.clipSelfRect );
		}

		#endregion IntersectInvalidRect 

		#region Contains

		// SSP 6/17/03 - Fixed headers
		// Overrode Contains method to take care of cells that are partially scrolled underneath
		// the fixed column cells.
		//
        /// <summary>
        /// Checks if the point is over the element.
        /// </summary>
        /// <param name="point">In client coordinates.</param>
        /// <param name="ignoreClipping">Specifieds if we should ignore clipping or not</param>
        /// <returns>Returns true if the point is over the element.</returns>
        public override bool Contains(System.Drawing.Point point,
			bool ignoreClipping )
		{
			if( ! this.clipSelfRect.IsEmpty && ! this.clipSelfRect.Contains( point ) )
				return false;

			return base.Contains( point, ignoreClipping );
		}

		#endregion // Contains

		#region InternalSetClipSelfRegion
		
		internal void InternalSetClipSelfRegion( Rectangle clipSelfRect )
		{
			this.clipSelfRect = clipSelfRect;
		}

		#endregion // InternalSetClipSelfRegion

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			UltraGridColumn column = this.Column;
			UltraGridRow row = this.Row;

			// SSP 11/28/01
			// Reset the cache version to the layout's cache version.
			//
			// SSP 10/10/02 UWG1530
			// Take into account column.CellChildElementsCacheVersion as well.
			//
			//this.cellChildElementsCacheVersion = row.Band.Layout.CellChildElementsCacheVersion;
			this.cellChildElementsCacheVersion = column.CellChildElementsCacheVersion;

			// SSP 4/23/03 - Optimizations
			//
			//RowScrollRegion rsr = (RowScrollRegion)this.Parent.GetContext( typeof ( RowScrollRegion ) );
			//ColScrollRegion csr = (ColScrollRegion)this.Parent.GetContext( typeof ( ColScrollRegion ) );
			RowScrollRegion rsr = null;
			ColScrollRegion csr = null;
			RowColRegionIntersectionUIElement rcrElem = (RowColRegionIntersectionUIElement)this.Parent.GetAncestor( typeof( RowColRegionIntersectionUIElement ) );
			if ( null != rcrElem )
			{
				rsr = rcrElem.RowScrollRegion;
				csr = rcrElem.ColScrollRegion;
			}
			else
			{
				rsr = (RowScrollRegion)this.Parent.GetContext( typeof ( RowScrollRegion ) );
				csr = (ColScrollRegion)this.Parent.GetContext( typeof ( ColScrollRegion ) );
			}

			this.CalcBorderSides( csr );

			this.PositionChildElementsHelper( column, row, rsr, csr );

			// SSP 7/28/05 - Header, Row, Summary Tooltips
			// Changed the tooltip showing logic to make use of the UIElement.ToolTipItem mechanism.
			// This was necessary to be able to display the row tool tips.
			// 
			this.ToolTipItem =
				// SSP 7/21/06 BR14294
				// Tip style needs to be resolved differently based whether the ToolTipText has been explicitly set
				// or the cell's value (clipped) will be displayed.
				// 
				//TipStyle.Show == row.BandInternal.TipStyleCellResolved 
				TipStyle.Show == row.BandInternal.TipStyleCellResolved( row, column )
				? CellUIElementBase.CellElementToolTipItem.VALUE : null;
		}

		// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
		//
		internal abstract void PositionChildElementsHelper( 
			UltraGridColumn column, UltraGridRow row, RowScrollRegion rsr, ColScrollRegion csr );

		internal EmbeddableUIElementBase GetEmbeddableEditorElementHelper( 
			UltraGridColumn column,
			UltraGridRow row,
			RowScrollRegion rsr,
			ColScrollRegion csr,
			UltraGridCell cell,
			EmbeddableUIElementBase embeddableUIElement )
		{
			UltraGridLayout layout = row.Layout;
			UltraGridBase grid = layout.Grid;
			bool isUltraGrid = grid is UltraGrid;

			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			// If the resolved cell display style is something other than full editor display, then
			// don't add the embeddable element.
			//
			bool isCellActiveCell = null != cell && cell.IsActiveCell;
			RowScrollRegion activeRSR = layout.ActiveRowScrollRegion;
			ColScrollRegion activeCSR = layout.ActiveColScrollRegion;
			
			// SSP 8/12/03 - Optimizations
			// Pass in the cell instead of the row so the method doesn't have to check if
			// the cell is allocated and get it. NOTE: It takes care of nulls. Null means
			// the cell hasn't been allocated.
			//
			//this.cellDisplayStyle = column.GetCellDisplayStyleResolved( row );
			// SSP 4/22/05
			// Filter row cells should always be FullEditorDisplay;
			//
			//this.cellDisplayStyle = column.GetCellDisplayStyleResolved( cell );
			this.cellDisplayStyle = ! row.IsFilterRow ? column.GetCellDisplayStyleResolved( cell ) : CellDisplayStyle.FullEditorDisplay;
			if ( CellDisplayStyle.FullEditorDisplay != this.cellDisplayStyle )
			{
				if ( null == cell || 
					!( cell.IsInEditMode || cell == layout.CellGoingIntoEditMode ) 
					|| activeRSR != rsr || activeCSR != csr )
					return null;
			}

			// SSP 4/2/02
			// See if we have an embeddable editor off the column and if so, add that instead
			// of the text element below.
			//
			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//EmbeddableEditorBase editor = column.Editor;
			EmbeddableEditorBase editor = column.GetEditor( row );

			// SSP 5/14/03 - Optimizations
			// This internal member variable will be used by the EmbeddableEditorOwnerInfo.GetEditor
			// to return the editor directly from the cell element rather than going through the normal
			// editor resolution mechanism which involves checkinf if the cell has been created or not
			// since now we support cell level value lists and editors. EmbeddableEditorOwnerInfo.GetEditor
			// method gets called quite a lot of times.
			//
			this.embeddableEditorLastUsed = editor;

			Debug.Assert( null != editor, "Column.Editor returned null !" );

			if ( null != editor )
			{
				// We always want to reserve the space for elements.
				//
				// SSP 3/11/03 UWG1998
				//
				//bool reserveSpaceForEditElements = true;
				bool reserveSpaceForEditElements = false;
				bool includeEditElements = false;

				// SSP 3/27/03 - Row Layout Functionality
				// Don't show any edit elements (drop down buttons etc.) in design mode. In row layout
				// designer, drop down buttons were showing up in design mode.
				//
				//if ( isUltraGrid )
				if ( isUltraGrid && ! grid.DesignMode )
				{
					// SSP 5/13/03 - Optimizations
					//
					//if ( csr == column.Layout.ActiveColScrollRegion && 
					//	rsr == column.Layout.ActiveRowScrollRegion && 
					//	row.HasCell( column ) && row.Cells[column].IsActiveCell )
					//if ( csr == activeCSR && rsr == activeRSR && isCellActiveCell )
					if ( isCellActiveCell && csr == activeCSR && rsr == activeRSR )
					{
						// If the cell is the active cell, then always include the buttons.
						//
						// SSP 7/29/02 UWG1457
						// Don't show the buttons if the activation settings do not allow going into
						// edit mode or if the cell is read-only.
						// Enclosed the code in the if block.
						//
						// SSP 5/13/03 - Optimizations
						//
						//UltraGridCell cell = row.Cells[column];
						// SSP 10/17/03 UWG2643
						// If the button display style is Always, then include the edit buttons.
						// Added the if block.
						//
						if ( ButtonDisplayStyle.Always == column.ButtonDisplayStyle )
						{
							includeEditElements = reserveSpaceForEditElements = true;
						}
						else if ( cell.IsInEditMode || cell.CanEnterEditMode )
						{
							// If the cell is read-only, then don't show the drop down arrows
							// and other edit elements.
							//
							includeEditElements = cell.CanBeModified;

							// SSP 9/19/03 UWG1998
							// I don't think there is any case where you would want the space for edit elements to
							// be reserved but not include them. So just use the includeEditElements value for the
							// reserveSpaceForEditElements.
							//
							//reserveSpaceForEditElements = true;
							reserveSpaceForEditElements = includeEditElements;
						}
					}				
					else
					{
						// SSP 1/3/02 Optimizations
						// Moved the if condition that checks for whether the cell can be entered into
						// edit mode or can be modified. We should check for this after the switch block
						// only if the includeEditElements flag is set to true.
						//
						
						switch ( column.ButtonDisplayStyle )
						{
							case ButtonDisplayStyle.Always:
							{
								includeEditElements = true;
								reserveSpaceForEditElements = true;
								break;
							}
							case ButtonDisplayStyle.OnRowActivate:
							{
								if ( row.IsActiveRow )
								{
									includeEditElements = true;
									reserveSpaceForEditElements = true;
								}
								break;
							}
							case ButtonDisplayStyle.OnCellActivate:
							{
								// SSP 5/13/03 - Optimizations
								//
								//if ( row.HasCell( column ) && row.Cells[column].IsActiveCell )
								if ( isCellActiveCell )
								{
									includeEditElements = true;
									reserveSpaceForEditElements = true;
								}
								break;
							}
							case ButtonDisplayStyle.OnMouseEnter:
							{
								

								if (this.isMouseOverCell)
								{
									includeEditElements = true;
									reserveSpaceForEditElements = true;
								}
								break;
							}
						}
						//}

						// SSP 1/3/02 Optimizations
						// Moved the if condition that checks for whether the cell can be entered into
						// edit mode or can be modified. We should check for this after the switch block
						// only if the includeEditElements flag is set to true.
						//
						// SSP 7/29/02 UWG1457
						// Don't show the buttons if the activation settings do not allow going into
						// edit mode or if the cell is read-only.
						//
						includeEditElements = includeEditElements && 
							// SSP 10/17/03 UWG2643
							// If ButtonDisplayStyle is set to Always, then always display the buttons.
							//
							//column.CanCellEnterEditMode( row ) && column.CanBeModified( row );
							( ButtonDisplayStyle.Always == column.ButtonDisplayStyle 
							|| column.CanCellEnterEditMode( row ) && column.CanBeModified( row ) );

						// SSP 3/6/03 UWG1998
						// Also don't reserve space for edit elements if the cell can't enter edit mode.
						//
						// SSP 9/19/03 UWG1998
						// I don't think there is any case where you would want the space for edit elements to
						// be reserved but not include them. So just use the includeEditElements value for the
						// reserveSpaceForEditElements.
						//
						//reserveSpaceForEditElements = reserveSpaceForEditElements && column.CanCellEnterEditMode( row );							
						reserveSpaceForEditElements = includeEditElements;
					}
				}

				// If we are in edit mode, then we need to use the embeddable element that's
				// in edit mode.
				// Make sure it's the right row and col scroll regions.
				//
				if ( csr == layout.EmbeddableElementBeingEdited_ColScrollRegion && 
					rsr == layout.EmbeddableElementBeingEdited_RowScrollRegion )
				{
					// SSP 5/13/03 - Optimizations
					//
					//if ( row.HasCell( column ) )
					// SSP 5/18/04 UWG3324
					// We don't need to check for cell being non-null here. Not only that we
					// want to execute the code in the else block if the inner if-else regardless
					// of whether the cell is non-null.
					//
					//if ( null != cell )
					//{
					// SSP 5/13/03 - Optimizations
					//
					//UltraGridCell thisCell = row.Cells[ column ];
					//UltraGridCell activeCell = column.Layout.ActiveCell;
					//if ( activeCell == thisCell && thisCell.IsInEditMode )
					if ( isCellActiveCell && cell.IsInEditMode )
					{						
						if ( null != layout.EmbeddableElementBeingEdited )
						{
							embeddableUIElement = layout.EmbeddableElementBeingEdited;
							
							if ( null != embeddableUIElement )
							{
								// SSP 4/22/05 - NAS 5.2 Filter Row
								// Use the GetEditorOwnerInfo method instead.
								//
								//Debug.Assert( column.EditorOwnerInfo == embeddableUIElement.Owner, "Embeddable element in edit mode has the wrong column ????!" );
								Debug.Assert( row.GetEditorOwnerInfo( column ) == embeddableUIElement.Owner, "Embeddable element in edit mode has the wrong column ????!" );

								Debug.Assert( row == EmbeddableEditorOwnerInfo.GetRowFromOwnerContext( embeddableUIElement.OwnerContext ), "Embeddable element in edit mode has the wrong row ????!" );
							}
							
							//Debug.WriteLine( "Reusing element in edit mode. **********************" );
						}						
					}
					else
					{
						if ( null != layout.EmbeddableElementBeingEdited &&
                            // MBS 1/9/07 - BR18618
                            //embeddableUIElement == layout.EmbeddableElementBeingEdited )
                            embeddableUIElement == layout.EmbeddableElementBeingEdited &&
                            (embeddableUIElement.Editor == null || embeddableUIElement.Editor.IsEnteringEditMode == false))
						{
							embeddableUIElement = null;
							Debug.WriteLine( "Not using the element since cell not in edit mode but the element is. **********************" );
						}
					}					
					//}
				}

				// If somehow we have a different editor than before, then don't reuse
				// the embeddable element. The editor should take care of this, but just
				// in case.
				//
				if ( null != embeddableUIElement && embeddableUIElement.Editor != editor )
					embeddableUIElement = null;

				embeddableUIElement = 
					editor.GetEmbeddableElement( 
					this, 
					// SSP 4/22/05 - NAS 5.2 Filter Row
					// Use the GetEditorOwnerInfo method off the row instead.
					//
					//column.EditorOwnerInfo, 
					row.GetEditorOwnerInfo( column ), 
					EmbeddableEditorOwnerInfo.GetOwnerContext( this ), 
					includeEditElements, 
					includeEditElements || reserveSpaceForEditElements,
					false, // Don't include borders as the cell is going to draw it's own borders.
					embeddableUIElement );

				return embeddableUIElement;

				// SSP 1/2/03
				// Optimizations.
				// Added methods off the owner that the emebeddable element can use to notify the owner
				// of mouse messages. As a result we don't need to hook into the emebddable element.
				//
				//this.EnsureHookedIntoEmbeddableElement( embeddableUIElement );
			}

			return null;
		}

		#region Commented Out Code
		

				


		#endregion // Commented Out Code

		#endregion // PositionChildElements

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// SSP 12/23/02
			// Optimizations.
			// Use the new ResolvedCachedCellAppearance method.
			//
			
			this.InternalInitAppearance( ref appearance, ref requestedProps );
		}

		// SSP 12/1/04 - Merged Cell Feature
		// Added InternalInitAppearance method.
		//
		internal void InternalInitAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			UltraGridRow row = this.Row;

			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingCell and hotTrackingRow parameters.
			// 
			bool hotTrackingRow = GridUtils.IsRowHotTracking( this );
			bool hotTrackingCell = hotTrackingRow && GridUtils.IsHotTracking( this, this.isMouseOverCell );

            row.Layout.ResolvedCachedCellAppearance(
                row,
                this.Column,
                ref appearance,
                ref requestedProps
                // SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
                // Added hotTrackingCell and hotTrackingRow parameters.
                // 
                , hotTrackingCell, hotTrackingRow);
		}

		// SSP 12/23/02
		// Optimizations.
		// Overrode OnBeforeDraw and OnAfterDraw.
		// Added code to cache the resolved appearance of a cell since for a single cell
		// resolve cell appearance gets called at least twice.
		//
		/// <summary>
		/// A virtual method that gets called before the element draw process starts.
		/// </summary>
		/// <remarks>This is for notification purposes, the default implementation does nothing.</remarks>
		protected override void OnBeforeDraw( )
		{
			base.OnBeforeDraw( );

			if ( null != this.Column )
				this.Column.Layout.DirtyCachedCellAppearanceData( );
		}


		/// <summary>
		/// A virtual method that gets called after the element draw operation finishes.
		/// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		/// <remarks>This is for notification purposes, the default implementation does nothing.</remarks>
		protected override void OnAfterDraw( ref UIElementDrawParams drawParams )
		{
			base.OnAfterDraw( ref drawParams );

			if ( null != this.Column )
				this.Column.Layout.DirtyCachedCellAppearanceData( );
		}

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// SSP 5/1/03 - Optimizations
				// Cache the borderStyle and the border sides.
				//				
				//return this.Column.Band.BorderStyleCellResolved;
				return this.borderStyle;
			}
		}

		// SSP 11/29/04 - Merged Cell Feature
		// Added RowCellAreaElement property.
		//
		internal RowCellAreaUIElementBase RowCellAreaElement
		{
			get
			{
				RowCellAreaUIElementBase rowCellAreaElement = this.parentElement as RowCellAreaUIElementBase;
				if ( null == rowCellAreaElement )
					rowCellAreaElement = (RowCellAreaUIElementBase)this.Parent.GetAncestor( typeof( RowCellAreaUIElementBase ) );

				return rowCellAreaElement;
			}
		}

		// SSP 5/1/03 - Optimizations
		// Added CalcBorderSides method.
		//
		private void CalcBorderSides( ColScrollRegion csr )
		{
			this.borderSides = Border3DSide.All;

			// SSP 4/23/03 - Optimizations
			// Instead of calling this.Column more than once, use a stack variable.
			//
			UltraGridColumn column = this.Column;

			// SSP 6/5/03 - Fixed headers
			// Moved this from with the if statement.
			//
			// SSP 4/23/03 - Optimizations
			//
			//UIElement rowCellAreaElement = this.GetAncestor( typeof( RowCellAreaUIElement ) );
			RowCellAreaUIElementBase rowCellAreaElement = this.RowCellAreaElement;

			// SSP 9/26/02 UWG1700
			// 
			// SSP 6/5/03 - Optimizations
			// Use the cell border style that's cached off the row cell area element.
			//
			//this.borderStyle = column.Band.BorderStyleCellResolved;
			this.borderStyle = rowCellAreaElement.cellBorderStyle;

			if ( ! rowCellAreaElement.usingFixedHeaders || column.Band.UseRowLayoutResolved )
				this.clipSelfRect = Rectangle.Empty;

			// if cell spacing is zero we want to avoid painting the cell's 
			// border up against the row's border (assuming both border styles
			// can be merged (not raised, inset or none).
			// SSP 7/24/03 - Optimizations
			// Use the cell spacing resolved off the row cell area element.
			//
			//if ( column.Band.CellSpacingResolved == 0 )
			if ( 0 == rowCellAreaElement.cellSpacingResolved )
			{
				UltraGridBand band = column.Band;
				UltraGridLayout layout = band.Layout;

				if ( layout.CanMergeAdjacentBorders( this.borderStyle ) )
				{
					// SSP 6/5/03 - Fixed headers
					// Moved this above.
					//
					

					// SSP 2/20/03 - Row Layout Functionality.
					// Added if block for the row layout functionality and enclosed the already existing
					// code into the else block.
					//
					if ( band.UseRowLayoutResolved )
					{
						// SSP 12/1/03
						// Commented out following code. In row-layout mode this can be undesirable especially
						// with non-zero cell insets. Borders are already merged by the ILayoutContainer
						// implementation that lays out cells in the row cell area elem.
						//
						
					}
					else
					{
						if ( layout.CanMergeAdjacentBorders( rowCellAreaElement.BorderStyle ) )
						{
							Rectangle rcRowCellArea = rowCellAreaElement.Rect;
							Rectangle thisRect = this.Rect;

							if ( rcRowCellArea.Top + 1      == thisRect.Top )
								this.borderSides ^= Border3DSide.Top;

							if ( rcRowCellArea.Bottom - 1   == thisRect.Bottom )
								this.borderSides ^= Border3DSide.Bottom;

							if ( rcRowCellArea.Left + 1     == thisRect.Left )
								this.borderSides ^= Border3DSide.Left;
                            
							if ( rcRowCellArea.Right - 1    == thisRect.Right )
								this.borderSides ^= Border3DSide.Right;
						}

						// SSP 4/26/05 - BORDERS
						// Border drawing logic in the framework was modified (now it draws the 
						// left and the right borders in the same direction) so that the dotted
						// and dashed borders do not interpolate and end up solid. As a result
						// this is not necessary any more. The reason for taking this out is that
						// if the cell padding is 0 then when the cell is in edit mode the textbox
						// in the cell ends up drawing over the left border of the left cell. If
						// it turns out that below code is necessary then another potential fix
						// for that padding problem is to modify the EmbeddableEditorOwnerInfo to
						// always return 1 for the horizontal padding from GetPadding method.
						//
						// ------------------------------------------------------------------------
						
						// ------------------------------------------------------------------------
					}
				}
			}
		}

		/// <summary>
		/// Overridden property that returns an appropriate Border3DSide
		/// structure for drawing the borders for the cell UI element
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				// SSP 5/1/03 - Optimizations
				// Cache the borderStyle and the border sides.
				// Moved the original code into CalcBorderSides.
				//
				return this.borderSides;
				
			}
		}
		


		/// <summary>
        /// Called when the mouse up message is received over the element. 
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        protected override bool OnMouseUp( MouseEventArgs e )
		{
			

            // MBS 3/26/09 - TFS15979
            // We won't always get the OnEmbeddableMouseDown notification, such as with the EditorWithMask,
            // since it processes the MouseDown event and doesn't notify the parent elements, so we should
            // also reset this flag here
            this.hasFiredCellClick = false;

			return base.OnMouseUp( e );
		}

		private void EnsureEditControlFocused( )
		{
			// SSP 1/24/02 UWG960
			// If the cell is in edit mode and the cell element is clicked upon,
			// the grid will steal the focus, and the edit control won't
			// have the focus anymore. So set the focus to the edit control.
			//

			UltraGrid grid = this.Column.Layout.Grid as UltraGrid;

			// SSP 3/11/05 BR02751
			// Account for the condition where both this.Cell and this.ActiveCell are null.
			//
			//if ( null != grid && grid.ActiveCell == this.Cell && this.Cell.IsInEditMode )
			UltraGridCell activeCell = null != grid ? grid.ActiveCell : null;
			UltraGridCell cell = null != grid ? this.Cell : null;
			if ( null != activeCell && activeCell == cell && cell.IsInEditMode )
			{
				// If the grid has focus indeed
				//
				if ( grid.Focused )
				{
					
					// SSP 4/5/02
					// Use the embeddable editor instead of EditControl off the grid as it's
					// obsoleted due to embeddable editors.
					//
					//Control editControl = grid.EditControl;
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
					//EmbeddableEditorBase editor = this.Column.Editor;
					EmbeddableEditorBase editor = this.Column.GetEditor( this.Row );
					
					// Set the focus to the edit control
					//
					if ( null != editor && editor.CanFocus && editor.IsInEditMode )
					{
						try
						{
                            // MRS 12/19/06 - fxCop
                            // This is unneccessary and opens a security hole. The editor.Focus
                            // method will handle any neccessary permissions.
                            //if ( DisposableObject.HasSamePublicKey( editor.GetType() ))
                            //{
                            //    System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

                            //    perm.Assert();
                            //}

                            editor.Focus();
						}
						catch {}						
					}
				}
			}
		}


		// SSP 4/9/02 UWG1070
		// Overrode OnClik. Look at the note inside the method body.
		//
		/// <summary>
		/// Overridden OnClick metod.
		/// </summary>
		protected override void OnClick( )
		{
			// SSP 4/9/02
			// Overrode this method.
			// Override the OnClik in the cell ui element so that it does not
			// traverse up the ancestor hierarchy, because the RowCellAreaUIElement
			// overrides this methid and tries to set the active row to it's associated
			// row. We don't want it to do that since OnMouseDown of this element
			// already does that. This leads to a bug where BeforeCellDeactive fires
			// twice if the user cancels it because both the cell ui element and 
			// the row ui element are trying to activate a cell or a row and in 
			// doing so deactivating the previous active cell causing BeforeCellDeactivate
			// to be fired twice.
			//
			//
			// Don't do anything here.

            // MBS 7/1/08 - NA2008 V3
            if (this.Grid != null && this.Cell != null)
            {
                // MBS 3/22/09 - TFS14728
                // Check to see if we've already fired the event first
                if (this.hasFiredCellClick == false)
                {
                    this.hasFiredCellClick = true;
                    this.Grid.FireEvent(GridEventIds.ClickCell, new ClickCellEventArgs(this.Cell));
                }
            }
		}

	
		// SSP 4/30/02
		// EM Embeddable editors.
		// 
		

		// SSP 3/19/03 - Row Layout Functionality
		// Added a property to indicate whether we are in row layout designer mode.
		//
		private bool RowLayoutDesignerElement
		{
			get
			{
				DataAreaUIElement dataAreaElem = (DataAreaUIElement)this.GetAncestor( typeof( DataAreaUIElement ) );

				return null != dataAreaElem && dataAreaElem.RowLayoutDesignerElement;
			}
		}

        /// <summary>
        /// Called when the mouse down message is received over the element. 
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        /// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
        /// <returns>If true then bypass default processing</returns>
        protected override bool OnMouseDown( MouseEventArgs e, 
										 bool adjustableArea,
										 ref UIElement captureMouseForElement )
		{
			// SSP 3/20/02 - Row Layout Functionality
			// In layout designer, don't process the OnMouseDown.
			//
			if ( this.RowLayoutDesignerElement )
			{
				return base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );
			}

			bool ret = base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );
			
			Infragistics.Win.UltraWinGrid.UltraGridCell cell = this.Cell;

			// SSP 1/16/04 UWG2898
			// Moved this block of code form the following if block since this applies to both the
			// if and the else blocks.
			//
			// ----------------------------------------------------------------------------------------
			// SSP 4/8/02
			// If embeddable element was clicked upon, then return without
			// processing any further
			//
			// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			// Use the FindEmbeddableUIElement instead which searches only the child elements.
			// In filter cells there can be two embeddable elements. One for the operand and 
			// the other for operator. Here we are interested in getting the operand editor 
			// element and since the operator editor element not an immediate child (unlike
			// the operand editor element which) using the FindEmbeddableUIElement gets us
			// the operand editor element.
			// 
			//EmbeddableUIElementBase embeddableElem = (EmbeddableUIElementBase)this.GetDescendant( typeof( EmbeddableUIElementBase ) );
			EmbeddableUIElementBase embeddableElem = this.FindEmbeddableUIElement( false );
			
			if ( null != embeddableElem && embeddableElem.Rect.Contains( e.X, e.Y ) )
			{
				return ret;
			}
			// ----------------------------------------------------------------------------------------

			//RobA 8/28/01 we don't want to do this if it was a right click
			//
			if ( !adjustableArea && e.Button == MouseButtons.Left )
			{
				// SSP 1/16/04 UWG2898
				// Moved this block of code above, before the if statement.
				//
				

				// JJD 10/12/01 - UWG521
				// If the control key is down then return false so that
				// the selection manager gets a crack at selecting this
				// cell.
				//
				if ( ( System.Windows.Forms.Control.ModifierKeys & Keys.Control ) == Keys.Control )
					return false;

				// if cellClickAction is 'edit', set focus and acivate and return true
				// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
				//if ( this.Column.Band.CellClickActionResolved == CellClickAction.Edit ||
				if ( this.Cell.CellClickActionResolved == CellClickAction.Edit ||
					 // JAS v5.2 Select Text On Cell Click 4/7/05
					 // MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
					//this.Column.Band.CellClickActionResolved == CellClickAction.EditAndSelectText)					
					this.Cell.CellClickActionResolved == CellClickAction.EditAndSelectText)					
				{
					cell.SetFocusAndActivate( true, true );

					// SSP 1/24/02 UWG960
					// If the cell is in edit mode and the cell element is clicked upon,
					// the grid will steal the focus, and the edit control won't
					// have the focus anymore. So set the focus to the edit control.
					//
					this.EnsureEditControlFocused( );
					
					return true;
				}
				// .. else retun false
				else
					return false;
				//}
			}		
		
				// If mouse button other than left was clicked
			else
			{
				Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = this.Column.Layout.ActiveCell;

				// If the cell is in edit mode, then just make sure that the editor has
				// focus.
				//
				if ( activeCell == cell && cell.IsInEditMode )
				{
					// SSP 1/24/02 UWG960
					// If the cell is in edit mode and the cell element is clicked upon,
					// the grid will steal the focus, and the edit control won't
					// have the focus anymore. So set the focus to the edit control.
					//
					this.EnsureEditControlFocused( );
				}
				// If a different cell than the one that's in edit mode was clicked,
				// then exit the edit mode on the cell that's in edit mode.
				//
				else
				{
					if ( activeCell != null )
						activeCell.ExitEditMode();
				}
			}

			return ret;
		}

		// SSP 1/17/02 UWG168
		// This method was being overridden but no base class'
		// implementation was being called. And also it's not
		// doing anything as all the code in there is commented 
		// out. So commeted it out.
		//
		

		

		/// <summary>
		/// Returns the row if cellClickAction is 'RowSelect', the cell otherwise.
		/// </summary>
		public override ISelectableItem SelectableItem
		{
			get 
			{
				// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
				//if ( this.Column.Band.CellClickActionResolved == CellClickAction.RowSelect )
				if ( this.Cell.CellClickActionResolved == CellClickAction.RowSelect )
					return this.Row;
				else
					return this.Cell;
			}
		}

		#region OnDispose

		// SSP 10/19/04 UWG3751
		// Overrode OnDispose so we can exit edit mode when this cell element in edit mode
		// is disposed off. When some filtering is applied through code or some other action 
		// is taken via code that caues the cell in edit mode to be hidden then we need to
		// exit the edit mode on the editor. We already do that in the grid however the
		// problem is that at that point the embeddable element in edit mode is already 
		// disposed off and the EmbeddableEditorBase.IsInEditMode method simply returns
		// false if the embeddable element is disposed so the editor never gets a chance
		// to do any cleanup like hide the text box.
		//
		/// <summary>
		/// Overridden. Called when the element is disposed.
		/// </summary>
		protected override void OnDispose( )
		{
			UltraGridCell cell = null != this.Row && null != this.Column 
				// SSP 1/27/05
				// If the layout is being disposed, then don't bother doing this.
				//
				&& ! this.Column.Disposed 
				? this.Row.GetCellIfAllocated( this.Column ) : null;

			// SSP 5/24/05
			// If the layout is being disposed, then don't bother doing this.
			//
			//if ( null != cell && cell.IsInEditMode )
			if ( null != cell && cell.IsInEditMode && null != cell.Layout && ! cell.Layout.Disposed )
				cell.ExitEditMode( false, true );

			base.OnDispose( );
		}

		#endregion // OnDispose

		// SSP 3/11/03 - Row Layout Functionality
		// Made the cell adjustable.
		//
		#region Row Layout Code

		/// <summary>
		/// Overridden. Returns true cell can be resized.
		/// </summary>
		/// <returns></returns>
		public override bool Adjustable
		{
			get
			{
				UltraGridBand band = null != this.Row ? this.Row.Band : null;

				// SSP 3/17/03 - Row Layout Functionality
				//
				if ( null != band && null != this.Column && band.UseRowLayoutResolved )
				{
					return RowLayoutSizing.None != this.Column.RowLayoutColumnInfo.AllowCellSizingResolved;
				}

				// SSP 11/16/04
				// Implemented column sizing using cells in non-rowlayout mode. 
				//
				// --------------------------------------------------------------------------
				if ( null != band && null != this.Column && 
					( ColumnSizingArea.CellsOnly == band.ColumnSizingAreaResolved 
					  || ColumnSizingArea.EntireColumn == band.ColumnSizingAreaResolved ) )
					return true;
				// --------------------------------------------------------------------------

				return false;
			}
		}

        /// <summary>
        /// Called after a move/resize operation. 
        /// </summary>
        /// <param name="delta">The delta</param>
        public override void ApplyAdjustment(Point delta)
		{
			if ( !this.Adjustable )
				return;

			// Exit the edit mode.
			//
			UltraGridBand band = this.Column.Band;
			UltraGridLayout layout = band.Layout;

			// when resizing column/group, if we are in edit mode, then exit edit mode
			if ( null != layout.ActiveCell && layout.ActiveCell.IsInEditMode )
			{
				layout.ActiveCell.ExitEditMode( );

				// If the exitting the edit mode is cancelled then don't proceed with the resizing.
				//
				if ( null != layout.ActiveCell && layout.ActiveCell.IsInEditMode )
					return;
			}			

			// SSP 11/16/04
			// Implemented column sizing using cells in non-rowlayout mode.
			//
			// ------------------------------------------------------------------------------------
			//this.Row.Band.ResizeLayoutItem( this.Column, this.GetLayoutContainerSize( ), delta );
			if ( band.UseRowLayoutResolved )
			{
				// MRS 4/4/05 - Span-Resizing for row layouts
				// MRS 4/11/05
				//if ( this.Grid.gridBagLayoutDragStrategy != null)
				if ( this.Grid != null && 
					this.Grid.gridBagLayoutDragStrategy != null)
				{					
					Point pointInControlCoords = this.Grid.PointToClient( Control.MousePosition );										
					this.Grid.DragSpanEnd(pointInControlCoords, this, this.Column.Header, this.Column);
					return;
				}

				// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// Pass along the new elem parameter.
				// 
				//band.ResizeLayoutItem( this.Column, this.GetLayoutContainerSize( ), delta );
				band.ResizeLayoutItem( this, this.Column, this.GetLayoutContainerSize( ), delta );
			}
			else
				HeaderUIElement.ApplyAdjustmentNonRowLayoutMode( this.Column.Header, this.Column.Header.Extent + delta.X );
			// ------------------------------------------------------------------------------------
		}

		/// <summary>
		/// Returns the range limits for adjusting the element in either or both
		/// dimensions. It also returns the initial rects for the vertical and horizontal
		/// bars that will need to be inverted during the mouse drag operation.
		/// </summary>
		/// <param name="point">point</param>
		/// <param name="range">Returned limits</param>
		public override void GetAdjustmentRange ( System.Drawing.Point point,
			out UIElementAdjustmentRangeParams range )
		{
			base.GetAdjustmentRange( point, out range );

			UltraGridBand band = this.Row.Band;

			if ( null == band )
				return;

			Rectangle rect = this.Rect;

			// Take into account the cell spacing.
			//
			rect.Inflate( band.CellSpacingResolved, band.CellSpacingResolved );

			UIElement rcrElem = this.GetAncestor( typeof( RowColRegionIntersectionUIElement ) );
			// SSP 6/26/03 UWG2377
			// Don't limit the resize range to the scrolling region. Instead limit it to the desk top rect
			// like we do in the non-layout mode.
			//
			// SSP 10/26/04 UWG3729
			// Uncommented the following line out. The line following it where we define and set
			// the desktopRect was already there. We need limit the row resizing to the row col
			// region buttom but not the column because if you resized a row to be bigger than 
			// the region then you would never be able to resize it back to a smaller size.
			//
			Rectangle rowColRegionRect = null != rcrElem ? rcrElem.Rect : this.Control.ClientRectangle;

			// MD 2/5/09
			// Found while fixing TFS13341
			// The rect passed into GetDeskTopWorkArea is supposed to be in screen coordinates.
			//Rectangle desktopRect = Infragistics.Win.UIElement.GetDeskTopWorkArea( new Rectangle( rect.Right - 1, rect.Top + rect.Width / 2, 1, 1 ) );
			Rectangle tmpRect = new Rectangle( rect.Right - 1, rect.Top + rect.Width / 2, 1, 1 );
			tmpRect = this.Control.RectangleToScreen( tmpRect );
			Rectangle desktopRect = Infragistics.Win.UIElement.GetDeskTopWorkArea( tmpRect );

			int minWidth = 0;
			int maxWidth = 0;
			int minHeight = 0;
			int maxHeight = 0;

			// SSP 11/16/04
			// Implemented column sizing using cells in non-rowlayout mode. Check for row layout being
			// on since now Adjustable will return true in non-rowlayout mode if ColumnRowSizingArea
			// is set to allow resizing using cell borders.
			//
			// ------------------------------------------------------------------------------------------
			// band.GetLayoutItemResizeRange( this.Column, this.GetLayoutContainerSize( ), ref minWidth, ref maxWidth, ref minHeight, ref maxHeight );
			if ( band.UseRowLayoutResolved )
			{
				// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// Pass along the new elem parameter.
				// 
				//band.GetLayoutItemResizeRange( this.Column, this.GetLayoutContainerSize( ), ref minWidth, ref maxWidth, ref minHeight, ref maxHeight );
				band.GetLayoutItemResizeRange( this, this.Column, this.GetLayoutContainerSize( ), ref minWidth, ref maxWidth, ref minHeight, ref maxHeight );
			}
			else
			{
				this.Column.Header.GetResizeRange( ref minWidth, ref maxWidth );

				// If the header is extended over the row selectors then substract the row selector width
				// from the min and max widths since the GetResizeRange call above would have included
				// that in the returned min and max widths.
				//
                int extentDiff = this.Column.Header.Extent - this.Column.Extent;
				if ( minWidth > extentDiff )
					minWidth -= extentDiff;

				if ( maxWidth > extentDiff )
					maxWidth -= extentDiff;
			}
			// ------------------------------------------------------------------------------------------

			DataAreaUIElement dataAreaElem = (DataAreaUIElement)this.GetAncestor( typeof( DataAreaUIElement ) );
			Rectangle mainElemRect = this.ControlElement.Rect;
			Rectangle dataRect = Rectangle.Intersect( mainElemRect, null != dataAreaElem ? dataAreaElem.RectInsideBorders : mainElemRect );

			if ( this.SupportsLeftRightAdjustmentsFromPoint( point ) )
			{
				range.maxDeltaLeft = Math.Min( 0, rect.Left + minWidth - rect.Right );
						
				// SSP 6/26/03 UWG2377
				// Don't limit the resize range to the scrolling region. Instead limit it to the 
				// desk top rect like we do in the non-layout mode.
				//
				// ------------------------------------------------------------------------------
				
				if ( maxWidth > 0 )
					// SSP 10/26/04 UWG3729
					//
					//range.maxDeltaRight = Math.Max( 0, Math.Max( desktopRect.Right - rect.Right, maxWidth - rect.Width ) );
					range.maxDeltaRight = Math.Max( 0, Math.Min( desktopRect.Right - rect.Right, maxWidth - rect.Width ) );
				else 
					range.maxDeltaRight = Math.Max( 0, desktopRect.Right - rect.Right );
				// ------------------------------------------------------------------------------

				range.leftRightAdjustmentBar.Y = dataRect.Top;
				range.leftRightAdjustmentBar.Height = dataRect.Bottom - dataRect.Top;
			}

			if ( this.SupportsUpDownAdjustmentsFromPoint( point ) )
			{
				range.maxDeltaUp = Math.Min( 0, rect.Top + minHeight - rect.Bottom );
						
				// SSP 6/26/03 UWG2377
				// Don't limit the resize range to the scrolling region. Instead limit it to the 
				// desk top rect like we do in the non-layout mode.
				//
				// ------------------------------------------------------------------------------
				
				// SSP 10/26/04 UWG3729
				// We need limit the row resizing to the row col region buttom but not the 
				// column because if you resized a row to be bigger than  the region then 
				// you would never be able to resize it back to a smaller size.
				//
				
				if ( maxHeight > 0 )
					range.maxDeltaDown = Math.Max( 0, Math.Min( maxHeight - rect.Height, rowColRegionRect.Bottom - rect.Bottom ) );
				else 
					range.maxDeltaDown = Math.Max( 0, rowColRegionRect.Bottom - rect.Bottom );
				// ------------------------------------------------------------------------------

				range.upDownAdjustmentBar.X = dataRect.Left;
				range.upDownAdjustmentBar.Width = dataRect.Right - dataRect.Left;
			}
		}


		/// <summary>
		/// Returns true if the element can be moved or resized horizontally
		/// by clicking on the passed in mouse point
		/// </summary>
		/// <param name="point">In client coordinates</param>
		public override bool SupportsLeftRightAdjustmentsFromPoint( System.Drawing.Point point )
		{ 
			if ( ! base.SupportsLeftRightAdjustmentsFromPoint( point ) )
				return false;

			// if the positionitem doesn't allow sizing return false
			//
			if ( !this.Adjustable )
				return false;

			UltraGridBand band = this.Row.Band;

			if ( null == band )
				return false;

			// SSP 11/16/04
			// Implemented column sizing using cells in non-rowlayout mode. 
			//
			// ------------------------------------------------------------------------------
			int minWidth = 0;
			int maxWidth = 0;
			if ( ! band.UseRowLayoutResolved )
			{
				this.Column.Header.GetResizeRange(ref minWidth, ref maxWidth);
				return 0 != minWidth && minWidth != maxWidth || this.Rect.Width > minWidth;
			}
			// ------------------------------------------------------------------------------

			if ( RowLayoutSizing.Horizontal != this.Column.RowLayoutColumnInfo.AllowCellSizingResolved &&
				RowLayoutSizing.Both != this.Column.RowLayoutColumnInfo.AllowCellSizingResolved )
				return false;

			// In row-layout designer, if the control key is down then don't resize the header.
			// The row-layout designer has special logic for resizing the item span with control
			// key down.
			//
			if ( this.RowLayoutDesignerElement && Keys.Control == Control.ModifierKeys )
				return false;

			// If min width and max width are the same, then return false.
			//
			minWidth = 0;
			maxWidth = 0;
			int tmp1 = 0, tmp2 = 0;

			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// Pass along the new elem parameter.
			// 
			//band.GetLayoutItemResizeRange( this.Column, this.GetLayoutContainerSize( ), ref minWidth, ref maxWidth, ref tmp1, ref tmp2 );
			band.GetLayoutItemResizeRange( this, this.Column, this.GetLayoutContainerSize( ), ref minWidth, ref maxWidth, ref tmp1, ref tmp2 );

			if ( 0 != minWidth && 0 != maxWidth && minWidth != maxWidth )
				return false;

			return true;
		}

		/// <summary>
		/// Returns true if the element can be moved or resized vertically
		/// by clicking on the passed in mouse point
		/// </summary>
		/// <param name="point">In client coordinates</param>
		public override bool SupportsUpDownAdjustmentsFromPoint( System.Drawing.Point point )
		{
			if ( ! base.SupportsUpDownAdjustmentsFromPoint( point ) )
				return false;

			// if the positionitem doesn't allow sizing return false
			//
			if ( !this.Adjustable )
				return false;

			UltraGridBand band = this.Row.Band;

			if ( null == band )
				return false;

			// SSP 11/16/04
			// Implemented column sizing using cells in non-rowlayout mode. Return false for 
			// vertical resizing of cells if not in row-layout mode.
			//
			if ( ! band.UseRowLayoutResolved )
				return false;

			if ( RowLayoutSizing.Vertical != this.Column.RowLayoutColumnInfo.AllowCellSizingResolved &&
				RowLayoutSizing.Both != this.Column.RowLayoutColumnInfo.AllowCellSizingResolved )
				return false;

			// In row-layout designer, if the control key is down then don't resize the header.
			// The row-layout designer has special logic for resizing the item span with control
			// key down.
			//
			if ( this.RowLayoutDesignerElement && Keys.Control == Control.ModifierKeys )
				return false;

			// If min height and max height are the same, then return false.
			//
			int minHeight = 0;
			int maxHeight = 0;
			int tmp1 = 0, tmp2 = 0;

			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// Pass along the new elem parameter.
			// 
			//band.GetLayoutItemResizeRange( this.Column, this.GetLayoutContainerSize( ), ref tmp1, ref tmp2, ref minHeight, ref maxHeight );
			band.GetLayoutItemResizeRange( this, this.Column, this.GetLayoutContainerSize( ), ref tmp1, ref tmp2, ref minHeight, ref maxHeight );

			if ( 0 != maxHeight && minHeight == maxHeight )
				return false;

			return true;
		}

		
		private Size GetLayoutContainerSize( )
		{
			RowCellAreaUIElement rowCellAreaElem = (RowCellAreaUIElement)this.GetAncestor( typeof( RowCellAreaUIElement ) );

			if ( null != rowCellAreaElem )
				return rowCellAreaElem.GetLayoutContainerSize( );

			return Size.Empty;
		}

		// SSP 6/24/03 UWG2385
		// Implemented auto-resizing cells in the row-layout mode.
		// Added SupportsAutoSizing helper property.
		//
		private bool SupportsAutoSizing
		{
			get
			{
				// JJD 1/16/02
				// If the header is an adjustable column header (not in card view ),
				// we are not in design mode we support autosizing on a double click.
				//
				return null != this.Column && 
					this.Adjustable &&
					!this.Column.Band.CardView &&
					!this.Column.Layout.Grid.DesignMode &&
					// SSP 4/14/03
					// Added AutoSizeMode property off the column and ColumnAutoSizeMode off the override.
					//
					ColumnAutoSizeMode.None != this.Column.AutoSizeModeResolved;
			}
		}

		// SSP 6/24/03 UWG2385
		// Implemented auto-resizing cells in the row-layout mode.
		// Overrode GetAdjustableCursor method.
		//
        /// <summary>
        /// Returns the cursor to use over the adjustable area of the element.
        /// </summary>
        /// <param name="point">The point that should be used to determine if the area is adjustable.</param>
        /// <returns>The cursor that should be used to represent an adjustable region.</returns>
        public override System.Windows.Forms.Cursor GetAdjustableCursor(System.Drawing.Point point)
		{			
			// MRS 4/1/05 - Added a check for Span Resizing. 
			if (GridUtils.IsControlKeyDown &&
				this.SupportsSpanSizing)
			{				
				if (this.SupportsLeftRightSpanSizingFromPoint( point ) )
				{
					this.wasShowingSpanCursor = true;
					return this.SpanResizeCursorHoriz;
				}
				else if (this.SupportsUpDownSpanSizingFromPoint( point ))
				{
					this.wasShowingSpanCursor = true;
					return this.SpanResizeCursorVert;
				}
			}
			this.wasShowingSpanCursor = false;

			if ( this.SupportsAutoSizing &&
				this.SupportsLeftRightAdjustmentsFromPoint( point ) 
				&& ! this.SupportsUpDownAdjustmentsFromPoint( point ) )
			{
				System.Windows.Forms.Cursor cursor = this.Column.Band.Layout.AutoSizeCursor;

				if ( cursor != null )
					return cursor;
			}

			return base.GetAdjustableCursor( point );
		}

		// SSP 6/24/03 UWG2385
		// Implemented auto-resizing cells in the row-layout mode.
		// Overrode OnDoubleClick.
		//
        /// <summary>
        /// Called when the mouse is double clicked on this element.
        /// </summary>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        protected override void OnDoubleClick(bool adjustableArea)
		{
			// JAS v5.2 DoubleClick Events 4/28/05 
			// I moved the call to the base implementation down to the bottom of this method
			// because if the client code in a DoubleClickRow event handler manipulates the grid
			// then we want those changes to be made after our auto-sizing work is complete.
			//
			//base.OnDoubleClick( adjustableArea );
			
			UltraGridRow row = this.Row;
			UltraGridBand band = null != row ? row.Band : null;

			if ( adjustableArea && null != band 
				// SSP 11/16/04
				// Implemented column sizing using cells in non-rowlayout mode. Commented out 
				// below condition that checks for row layout being turned on since now we are 
				// supporting resizing columns using cells in non-rowlayout mode as well.
				//
				//&& band.UseRowLayoutResolved 
				&& this.SupportsAutoSizing
				// SSP 9/12/03
				// Only auto size if the mouse is in the area where the user can resize horizontally.
				// In other words only auto-resize the width if the cursor is near the right border.
				// Added below condition.
				//
				&& null != this.Control && this.SupportsLeftRightAdjustmentsFromPoint( 
						this.Control.PointToClient( System.Windows.Forms.Control.MousePosition ) )) 
			{
				// SSP 8/12/03 UWG2385
				// Changed the behavior of auto resizing in the row layout mode. Instead of
				// auto-resizing the item that's double clicked, auto-resize all the items that
				// are right-aligned to the item that was double-clicked.
				//
				// --------------------------------------------------------------------------------
				// SSP 11/16/04
				// Implemented column sizing using cells in non-rowlayout mode. Added 
				// ColumnSizingArea property to the override.
				//
				// --------------------------------------------------------------------------
				//band.AutoResizeLayoutItem( this.Column, row.ParentCollection );
				if ( band.UseRowLayoutResolved )
					// SSP 8/31/06 BR13795 BR15249
					// Added containerSize and elem params.
					// 
					//band.AutoResizeLayoutItem( this.Column, row.ParentCollection );
					band.AutoResizeLayoutItem( this.Column, row.ParentCollection, this.GetLayoutContainerSize( ), this );
				else
					HeaderUIElement.AutoResizeNonRowLayoutMode( this.Column.Header, this );
				// --------------------------------------------------------------------------
				#region Dead Code
				
				#endregion // Dead Code
				// --------------------------------------------------------------------------------
			}

			// JAS v5.2 DoubleClick Events 4/28/05 
			// ----------------------------------------------------------------
			// Fire the DoubleClickCell event before calling the base implementation because the base 
			// will eventually fire off the DoubleClickRow event.  The DoubleClickCell event should fire
			// before the DoubleClickRow event.
			//
			if( this.Grid != null && this.Cell != null )
				this.Grid.FireEvent( GridEventIds.DoubleClickCell, new DoubleClickCellEventArgs( this.Cell ) );

			base.OnDoubleClick( adjustableArea );
			// ----------------------------------------------------------------
		}

		#endregion // Row Layout Code

		// JJD 12/24/03 - Accessibility
		#region IsAccessibleElement property

		/// <summary>
		/// Indicates if the element supports accessibility
		/// </summary>
		public override bool IsAccessibleElement 
		{ 
			get 
			{
				RowColRegionIntersectionUIElement scrollingRegion = this.GetAncestor( typeof(RowColRegionIntersectionUIElement ) ) as RowColRegionIntersectionUIElement;

				if ( scrollingRegion != null &&
					 scrollingRegion.RowScrollRegion == this.Column.Layout.ActiveRowScrollRegion &&
					 scrollingRegion.ColScrollRegion == this.Column.Layout.ActiveColScrollRegion )
					return true;

				return false; 
			} 
		}

		#endregion IsAccessibleElement property

		// JJD 12/24/03 - Added Accessibility support
		#region AccessibilityInstance property

		/// <summary>
		/// Returns the accesible object representing this row.
		/// </summary>
		public override AccessibleObject AccessibilityInstance
		{
			get
			{
				return this.Cell.AccessibilityObject;
			}
		}

		#endregion AccessibilityInstance property

		// MRS 3/31/05 - SpanResizing support for RowLayouts
		#region SpanResizing support for RowLayouts

		#region Private / Internal Properties

		#region Band
		private UltraGridBand Band
		{			
			get
			{
				if (null == this.Column)
					return null;

				return this.Column.Band;
			}
		}
		#endregion Band

		#region Grid
		private UltraGrid Grid
		{			
			get
			{				
				if (null == this.Column ||
					null == this.Column.Layout )
				{
					return null;
				}

				// MRS 4/11/05
				//return this.Column.Layout.Grid.ControlForGridDisplay as UltraGrid;
				return this.Column.Layout.Grid as UltraGrid;
			}
		}
		#endregion Grid

		#region SpanResizeCursorVert
		private Cursor SpanResizeCursorVert
		{
			get
			{
				UltraGrid grid = this.Grid;
				if (null == grid)
					return null;

				return grid.SpanResizeCursorVert;
			}
		}
		#endregion SpanResizeCursorVert

		#region SpanResizeCursorHoriz
		private Cursor SpanResizeCursorHoriz
		{
			get
			{
				UltraGrid grid = this.Grid;
				if (null == grid)
					return null;

				return grid.SpanResizeCursorHoriz;
			}
		}
		#endregion SpanResizeCursorHoriz

		#endregion Private / Internal Properties

		#region Private / Internal Methods

		#region SupportsSpanSizing
		private bool SupportsSpanSizing
		{
			get
			{	
				if (!GridUtils.IsControlKeyDown)
					return false;
				
				if (!this.Band.UseRowLayoutResolved)
					return false;
								
				return true;
			}
		}
		#endregion SupportsSpanSizing

		#region SupportsLeftRightSpanSizingFromPoint
		/// <summary>
		/// Returns true if the element can be Span Sized horizontally
		/// by clicking on the passed in mouse point
		/// </summary>
		/// <param name="point">In client coordinates</param>
		internal bool SupportsLeftRightSpanSizingFromPoint( System.Drawing.Point point )
		{ 
			if ( ! base.SupportsLeftRightAdjustmentsFromPoint( point ) )
				return false;

			if (null == this.Band )
				return false;

			Layout.GridBagLayoutAllowSpanSizing allowSpanSizing = this.Band.AllowRowLayoutCellSpanSizingResolved;
			
			return (Layout.GridBagLayoutAllowSpanSizing.AllowAll == allowSpanSizing ||
				Layout.GridBagLayoutAllowSpanSizing.AllowSpanXChange == allowSpanSizing);
		}
		#endregion SupportsLeftRightSpanSizingFromPoint

		#region SupportsUpDownSpanSizingFromPoint
		/// <summary>
		/// Returns true if the element can be Span Sized vertically
		/// by clicking on the passed in mouse point
		/// </summary>
		/// <param name="point">In client coordinates</param>
		internal bool SupportsUpDownSpanSizingFromPoint( System.Drawing.Point point )
		{ 
			if ( ! base.SupportsUpDownAdjustmentsFromPoint( point ) )
				return false;

			if (null == this.Band )
				return false;

			Layout.GridBagLayoutAllowSpanSizing allowSpanSizing = this.Band.AllowRowLayoutCellSpanSizingResolved;
			
			return (Layout.GridBagLayoutAllowSpanSizing.AllowAll == allowSpanSizing ||
				Layout.GridBagLayoutAllowSpanSizing.AllowSpanYChange == allowSpanSizing);
		}
		#endregion SupportsUpDownSpanSizingFromPoint		

		#region OnKeyToggled
		internal void OnKeyToggled()
		{
			// MRS 4/11/05	
			if (null != this.Grid &&
				null != this.Band &&
				this.Band.UseRowLayoutResolved)
			{
				if (!this.Grid.IsHandleCreated)			
					return;

				Point point = this.Grid.PointToClient(Control.MousePosition);
				if (this.wasShowingSpanCursor == this.WillShowSpanCursor(point))
					return;
			}

            // MRS 12/19/06 - fxCop
            //// JJD 1/19/02
            //// Assert the UIPermission in case our assembly has access but the
            //// calling assembly doesn't. This is safe because what we are doing
            //// after this is benign.
            ////
            //System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

            //try { perm.Assert(); } 
            //catch(Exception){}

            //// JJD 1/18/02
            //// Set the cursor position to itself which will trigger an update 
            //// of the cursor but wrap it in a try/catch in case we don't have
            //// access
            ////
            //try
            //{
            //    System.Windows.Forms.Cursor.Position = System.Windows.Forms.Cursor.Position;
            //}
            //catch(Exception)
            //{
            //}
            GridUtils.SetCursorPosition(System.Windows.Forms.Cursor.Position);
		}
		#endregion OnKeyToggled
		
		#region WillShowSpanCursor
		private bool WillShowSpanCursor (Point point)
		{
			if (GridUtils.IsControlKeyDown &&
				this.SupportsSpanSizing &&
				(this.SupportsLeftRightSpanSizingFromPoint( point ) ||
				this.SupportsUpDownSpanSizingFromPoint( point ) ) )
			{
				return true;
			}
			return false;		
		}
		
		#endregion WillShowSpanCursor

		#endregion Private / Internal Methods

		#region Overrides

		#region CanStartAdjustment
		/// <summary>
		/// Invoked when the mouse is pressed down on the adjustable area 
		/// before an adjustment begins.
		/// </summary>
		/// <param name="e">Mouse event args from the MouseDown</param>
		/// <returns>True if an adjustment can be started</returns>
		protected override bool CanStartAdjustment(System.Windows.Forms.MouseEventArgs e)
		{
			Point p = new Point(e.X, e.Y);

			// MRS 4/11/05
			//if (this.SupportsSpanSizing &&
			if (null != this.Grid &&
				this.SupportsSpanSizing &&
				(this.SupportsLeftRightSpanSizingFromPoint( p ) || 
				this.SupportsUpDownSpanSizingFromPoint( p ) ) )
			{
				bool isDragSpanHorizontal = this.SupportsLeftRightAdjustmentsFromPoint( p );				
				bool isHeaderBeingDragged = false;
								
				return this.Grid.StartRowLayoutSpanResizeDrag(this.Column, isDragSpanHorizontal, isHeaderBeingDragged);				
			}

			return base.CanStartAdjustment(e);
		}
		#endregion CanStartAdjustment	

		#region OnNewDeltaX
		/// <summary>
		/// Called when the the X delta has changed.
		/// </summary>
		protected override void OnNewDeltaX(int newDeltaX)
		{
			// MRS 4/11/05
			//if (this.Grid.gridBagLayoutDragStrategy != null)
			if (null != this.Grid &&
				this.Grid.gridBagLayoutDragStrategy != null)
			{					
				Point pointInControlCoords = this.Grid.PointToClient( Control.MousePosition );										
				this.Grid.DragSpanMove(pointInControlCoords, this, this.Column.Header, this.Column);
				return;
			}

			base.OnNewDeltaX(newDeltaX);
		}
		#endregion OnNewDeltaX

		#region OnNewDeltaY	
		/// <summary>
		/// Called when the the Y delta has changed.
		/// </summary>
		protected override void OnNewDeltaY(int newDeltaY)
		{
			// MRS 4/11/05
			//if (this.Grid.gridBagLayoutDragStrategy != null)
			if (null != this.Grid &&
				this.Grid.gridBagLayoutDragStrategy != null)
			{					
				Point pointInControlCoords = this.Grid.PointToClient( Control.MousePosition );										
				this.Grid.DragSpanMove(pointInControlCoords, this, this.Column.Header, this.Column);
				return;
			}

			base.OnNewDeltaY(newDeltaY);
		}
		#endregion OnNewDeltaY

		#region DrawAdjustmentBarWhileSizing
		/// <summary>
		/// True if Adjustment Bar should be drawn while sizing.
		/// </summary>
		protected override bool DrawAdjustmentBarWhileSizing
		{
			get
			{
				// MRS 4/11/05
				//if (this.Grid.gridBagLayoutDragStrategy != null)
				if (null != this.Grid &&
					this.Grid.gridBagLayoutDragStrategy != null)
					return false;

				return base.DrawAdjustmentBarWhileSizing;
			}
		}
		#endregion DrawAdjustmentBarWhileSizing

		#region OnElementAdjustmentAborted
		/// <summary>
		/// Called after a CaptureAborted is received and the adjustment is canceled. 
		/// </summary>
		public override void OnElementAdjustmentAborted()
		{
			// MRS 4/11/05
			if (null == this.Grid)
				return;

			this.Grid.ResetGridBagLayoutDragStrategy();	
		}
		#endregion OnElementAdjustmentAborted

		#endregion Overrides		

		#endregion SpanResizing support for RowLayouts

		// SSP 10/14/05 BR06301
		// Added OnMouseEnterLeave method.
		// 
		internal void OnMouseEnterLeave( bool enter )
		{
			this.isMouseOverCell = enter;

			UltraGridRow row = this.Row;
			UltraGridLayout layout = null != row ? row.Layout : null;

			// SSP 8/28/06 - NAS 6.3
			// Added CellHottrackInvalidationStyle property on the layout.
			// 
			//UltraGrid grid = null != layout ? layout.Grid as UltraGrid : null;
			//if ( null == grid || grid.InvalidateCellOnMouseEnterLeave )
			CellHottrackInvalidationStyle invalidateStyle = null != layout ? layout.CellHottrackInvalidationStyleResolved : CellHottrackInvalidationStyle.Always;
			if ( CellHottrackInvalidationStyle.Never != invalidateStyle )
			{
				// SSP 8/28/06 - NAS 6.3
				// Added CellHottrackInvalidationStyle property on the layout.
				// 
				//this.DirtyChildElements();
				if ( CellHottrackInvalidationStyle.Always == invalidateStyle )
					this.DirtyChildElements();

				// SSP 10/14/05 BR06301
				// We need to invalidate the merged cell so it can repaint with the hot-tracking
				// appearance.
				// 
				if ( null != row && ( row.HasRowHotTrackingAppearances || row.HasCellHotTrackingAppearances( this.Column ) ) )
				{
					// SSP 8/28/06 - NAS 6.3
					// Added CellHottrackInvalidationStyle property on the layout.
					// 
					this.DirtyChildElements( );

					CellUIElement cellElem = this as CellUIElement;
					MergedCellUIElement mergedCellElem = null != cellElem ? cellElem.MergedCellElement : null;
					if ( null != mergedCellElem )
						mergedCellElem.DirtyChildElements( );
				}
			}
		}

		#region OnMouseEnter

		// SSP 5/7/02
		// EM EMbeddable editors.
		//
		/// <summary>
		/// Forces a redraw if activebuttons is true.
		/// </summary>
		protected override void OnMouseEnter( )
		{
			// SSP 10/14/05 BR06301
			// Moved the code into the new OnMouseEnterLeave method.
			// 
			this.OnMouseEnterLeave( true );
			

			// SSP 7/26/02
			// We should be doing this in the OnMouseHover. I can't think of any reason
			// why we would want to do this here. Besides this is very inefficient. When
			// the user moves the mouse accross the grid, potentially tens of cells would
			// be going through the trouble of calculating the ideal size of the embeddable
			// editors.
			// Moved the below code block in OnMouseHover.
			// And added this flag which will prevent the cell from showing the tooltip
			// more than once since the mous enter.
			//
			// SSP 7/28/05 - Header, Row, Summary Tooltips
			// Changed the tooltip showing logic to make use of the UIElement.ToolTipItem mechanism.
			// This was necessary to be able to display the row tool tips.
			// 
			//this.hasAlreadyShownTheToolTipSinceMouseHover = false;
			
		}

		#endregion // OnMouseEnter

		// SSP 7/28/05 - Header, Row, Summary Tooltips
		// Changed the tooltip showing logic to make use of the UIElement.ToolTipItem mechanism.
		// This was necessary to be able to display the row tool tips.
		// 
		#region Header, Row, Summary Tooltips

		#region CellElementToolTipItem Class

		private class CellElementToolTipItem : IToolTipItem
		{
			internal static CellElementToolTipItem VALUE = new CellElementToolTipItem( );

			ToolTipInfo Infragistics.Win.IToolTipItem.GetToolTipInfo( Point mousePosition, UIElement element, UIElement previousToolTipElement, ToolTipInfo toolTipInfoDefault )
			{
				CellUIElementBase cellElem = element as CellUIElementBase;
				if ( null != cellElem )
					cellElem.CellToolTipHelper( ref toolTipInfoDefault );

				// By default the tool tip info mechanism has no auto pop delay. Meaning the
				// tool tip stays visible until the mouse leaves the element. To maintain
				// the previous UltraGrid behavior of auto-popping, set the AutoPopDelay to
				// -1 which means use the default auto pop delay.
				// 
				toolTipInfoDefault.AutoPopDelay = -1;

				return toolTipInfoDefault;
			}
		}

		#endregion // CellElementToolTipItem Class
		
		#region CellToolTipHelper

		private void CellToolTipHelper( ref ToolTipInfo toolTipInfo )
		{
			// SSP 8/7/04 - Added Cell.ToolTipText property
			//
			UltraGridRow row = this.Row;
			UltraGridColumn column = this.Column;
			UltraGridCell cell = null != row && null != column 
				? row.GetCellIfAllocated( column ) : null;

			// MRS 10/27/05 - BR07085
			// Dunno why we were checking for a grid here. This should work with 
			// any UltraGridBase such as UltraDropDown or UltraCombo
			//
			//UltraGrid grid = null != row ? row.Layout.Grid as UltraGrid : null;
			UltraGridLayout layout = null != row ? row.Layout : null;
			UltraGridBase grid = null != layout ? layout.Grid: null;
			if ( null == row || null == column || null == grid )
				return;

			// SSP 4/20/05
			//
			//EmbeddableUIElementBase embeddableElement = (EmbeddableUIElementBase)this.GetDescendant( typeof( EmbeddableUIElementBase ) );
			EmbeddableUIElementBase embeddableElement = this.FindEmbeddableUIElement( false );

			EmbeddableEditorBase toolTipEditor = null;
			EmbeddableEditorOwnerBase toolTipEditorOwner = null;
			object toolTipEditorOwnerContext = null;
			string toolTipText = null;

			bool displayToolTipOverCell = true;

			// If the cell is in edit mode, don't display the tooltips.
			//
			if (
				// SSP 7/21/06 BR14294
				// Tip style needs to be resolved differently based whether the ToolTipText has been explicitly set
				// or the cell's value (clipped) will be displayed.
				// 
				//TipStyle.Hide != this.Column.Band.TipStyleCellResolved &&
				TipStyle.Hide != this.Column.Band.TipStyleCellResolved( cell ) &&
				// SSP 8/7/04
				// Use the cell local variable that we assigned above.
				//
				//( !this.HasCell || null == this.Cell || !this.Cell.IsInEditMode ) )
				( null == cell || ! cell.IsInEditMode ) )
			{
				EmbeddableEditorOwnerBase editorOwnerInfo = row.GetEditorOwnerInfo( column );

				// SSP 8/7/04 - Added ToolTipText property.
				// Added ToolTipText property on the cell.
				// Added below if block.
				//
				if ( null != cell && null != cell.ToolTipTextResolved && cell.ToolTipTextResolved.Length > 0 )
				{
					toolTipText = cell.ToolTipTextResolved;

					// Since the tool tip text is typically different from the cell contents, 
					// don't display it over the cell.
					// 
					displayToolTipOverCell = false;
				}
					// SSP 4/20/05 - NAS 5.2 Filter Row
					// Added the following else-if block.
					//
				else if ( row is UltraGridFilterRow )
				{
					UltraGridFilterRow filterRow = (UltraGridFilterRow)row;
					toolTipText = filterRow.GetCellTooltip( this.Column );
				}
				else if ( null != embeddableElement  )
				{
					//	BF 8.27.02	UWG1547
					if ( ! embeddableElement.IsDataFullyVisible )
					{
						toolTipEditor = embeddableElement.Editor;
						toolTipEditorOwner = embeddableElement.Owner;

						// SSP 9/8/05 BR06252
						// When a cell is being hot-tracked, we need to resolve the tool-tip appearance without
						// the hot-tracking appearances in it. ResolveAppearance of the embeddable owner needs to
						// know that it's a tool tip. The only way it can do so is using a owner context that
						// indicates that it's a tool tip embeddable element. That's what this class is for.
						// 
						//toolTipEditorOwnerContext = embeddableElement.OwnerContext;
						toolTipEditorOwnerContext = new ToolTipOwnerContext( embeddableElement.OwnerContext );
					}

											
				}
				else
				{
					DependentTextUIElement textElement = (DependentTextUIElement)this.GetDescendant( typeof ( DependentTextUIElement ) );

					// If no band or tipstylehide or nocell then no tip ...			
					// Don't display cell tips for html cells
					Infragistics.Win.UltraWinGrid.UltraGridBand band = this.Column.Band;

					if ( null != textElement &&	null != band )
					{
						// Add cell tips
						AppearanceData appCell = new AppearanceData();
						Rectangle tempRect = Rectangle.Empty;

						// Call GetCellRect to get the resolved appearance, rect is not used
						this.Row.GetCellRect( this.Column, ref tempRect, ref appCell );

						// If text is larger than cell wait for a mouse hover and then display tip
						Rectangle textRect;
						Rectangle clipRect = this.ClipRect;
				
						// Intersect the texts rect with the clipping rect.
						clipRect.Intersect( textElement.Rect );				

						int width = clipRect.Width;
						int height = clipRect.Height;

						textRect = this.GetTextSize( textElement );

						// SSP 12/4/01 UWG839
						// If the rect returned is empty that means there
						// is no text, so set the hasToolTip to false so that
						// OnHover doesn't display the tool tip.
						//
						if ( ! textRect.IsEmpty )
						{
							// Strip out borders & margin 
							textRect.Inflate( -1, -2 );

							// Add a little space for character at right
							textRect.Width++;

							// If width or height is larger then start the hover
							// If the left is negative then it is clipped 
							if ( textRect.Left < clipRect.Left || textRect.Right > clipRect.Right || 
								textRect.Width >= width || textRect.Height > height )
							{
								toolTipText = textElement.Text;
							}
						}
					}
						// SSP 5/12/03 - Optimizations
						// Added a way to just draw the text without having to embedd an embeddable ui element in
						// cells to speed up rendering.
						//
					else if ( CellDisplayStyle.FullEditorDisplay != this.cellDisplayStyle )
					{
						AppearanceData  appData = new AppearanceData();
						AppearancePropFlags flags = AppearancePropFlags.FontData;
						Rectangle       tempRect = Rectangle.Empty;

						// Call GetCellRect to get the resolved appearance, rect is not used

						this.InitAppearance( ref appData, ref flags );							

						Font font = grid.Font;
						Font createdFont = appData.CreateFont( font );
						if ( null != createdFont )
							font = createdFont;

						// AS 8/12/03 optimization - Use the new caching mechanism.
						//
						//Graphics gr = DrawUtility.CreateReferenceGraphics( this.Control );
						// MRS 6/1/04 - UWG2915
						//Graphics gr = DrawUtility.GetCachedGraphics( this.Control );
						Graphics gr = this.Column.Layout.GetCachedGraphics();

						Debug.Assert( null != gr, "No graphics !" );

						// SSP 4/20/05 - NAS 5.2 Filter Row
						//
						//string text = this.GetCellTextHelper( );
						string text = this is CellUIElement ? ((CellUIElement)this).GetCellTextHelper( ) : string.Empty;

						if ( null != gr )
						{
							// SSP 11/4/05 BR07555
							// Need to pass printing param when measuring in Whidbey. When printing,
							// in Whidbey we need use the GDI+ measuring.
							// 
							// ----------------------------------------------------------------------
							
							Size size = layout.MeasureString( gr, text, font, 
								editorOwnerInfo.IsMultiLine( this ) ? this.RectInsideBorders.Width : 0 );
							// ----------------------------------------------------------------------

							Rectangle clipRect = this.ClipRect;

							// MD 7/26/07 - 7.3 Performance
							// FxCop - Remove unused locals
							//Rectangle cellRect = this.ClipRect;

							if ( clipRect.Width < size.Width || clipRect.Height < size.Height )
								toolTipText = text;

							// AS 8/12/03 optimization - Use the new caching mechanism.
							//
							//gr.Dispose( );
							// MRS 6/1/04 - UWG2915
							//DrawUtility.ReleaseCachedGraphics(gr);
							this.Column.Layout.ReleaseCachedGraphics(gr);

							gr = null;
						}
					}
				}
			}

			if ( null != toolTipEditor && null != toolTipEditorOwner )
			{
				toolTipInfo.Editor = toolTipEditor;
				toolTipInfo.EditorOwner = toolTipEditorOwner;
				toolTipInfo.EditorOwnerContext = toolTipEditorOwnerContext;
			}
			else
			{
				toolTipInfo.ToolTipText = toolTipText;
			}

			if ( toolTipInfo.HasToolTip && displayToolTipOverCell )
			{
				Rectangle rect = this.Control.RectangleToScreen( this.Rect );
				toolTipInfo.Location = rect.Location;
				toolTipInfo.Size = rect.Size;
			}

		}

		#endregion // CellToolTipHelper

		#region GetTextSize
		
		private Rectangle GetTextSize( DependentTextUIElement textElement )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//Rectangle rect;

			Infragistics.Win.UltraWinGrid.UltraGridColumn column = this.Column;
			if ( null != column )
			{
				string sText = column.GetCellText( this.Row );

				if ( null == sText )
					sText = string.Empty;

				// Make sure that we have a text string before proceeding.
				// Eliminates Boundschecker warning
				//
				if ( sText.Length < 1 )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Remove unused locals
					//rect = Rectangle.Empty;
				}
				else
				{
					AppearanceData appCell = new AppearanceData();
					Rectangle      tempRect = Rectangle.Empty;

					// Call GetCellRect to get the resolved appearance, rect is not used
					this.Row.GetCellRect( this.Column, ref tempRect, ref appCell );

					
					// Seems the member rect takes different border styles into account
					Rectangle textRect = textElement.RectInsideBorders;

					// If GetCellRect returns an empty rect it means the cell is no longer
					// in view so return an empty rect an bail. Prevents a GPF on the call to					
					//
					if ( textRect.IsEmpty )
					{
						return Rectangle.Empty;
					}
					else
					{
						// get the control's font
						//
						Font font = this.Column.Layout.Grid.Font;
						
						Font createdFont = null;
						
						// if there is a font override then create a temp font
						//
						if ( appCell.HasFontData )
						{
							createdFont = appCell.CreateFont( font );

							if ( createdFont != null )
								font = createdFont;
						}

						StringFormat stringFormat;
						StringFormatFlags stringFormatFlag = 0;

						//if not multiLine set fomat flag to 'NoWrap'
						if ( !textElement.MultiLine ) 
							stringFormatFlag = StringFormatFlags.NoWrap;

						//if vertical, set the appropriate format flag.
						if ( textElement.Vertical ) 
							stringFormatFlag |= StringFormatFlags.DirectionVertical;

						//initialize StringFormat object with flag
						stringFormat = new StringFormat( stringFormatFlag );

						switch ( appCell.TextHAlign )
						{
							case HAlign.Left:
								stringFormat.Alignment = StringAlignment.Near;
								break;
							case HAlign.Right:
								stringFormat.Alignment = StringAlignment.Far;
								break;
							case HAlign.Center:
								stringFormat.Alignment = StringAlignment.Center;
								break;
						}
						switch ( appCell.TextVAlign )
						{
							case VAlign.Top:
								stringFormat.LineAlignment = StringAlignment.Near;
								break;
							case VAlign.Bottom:
								stringFormat.LineAlignment = StringAlignment.Far;
								break;
							case VAlign.Middle:
								// RobA 12/13/01 just happen to see this
								//stringFormat.Alignment = StringAlignment.Center;
								stringFormat.LineAlignment = StringAlignment.Center;
								break;
						}

						
						// AS 8/12/03 optimization - Use the new caching mechanism.
						//
						// SSP 7/2/03
						// Use the more robust CreateReferenceGraphics method instead of CreateGraphics.
						//
						//Graphics gr = column.Layout.Grid.CreateGraphics();
						//Graphics gr = Infragistics.Win.DrawUtility.CreateReferenceGraphics( column.Layout.Grid );
						//MRS 6/1/04 UWG2915
						//Graphics gr = Infragistics.Win.DrawUtility.GetCachedGraphics( column.Layout.Grid );
						Graphics gr = column.Layout.GetCachedGraphics();
						
						//  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
						//Size size = gr.MeasureString( sText, font, textRect.Location, stringFormat ).ToSize();
						// SSP 11/4/05 BR07555
						// Need to pass printing param when measuring in Whidbey. When printing,
						// in Whidbey we need use the GDI+ measuring.
						// 
						//Size size = DrawUtility.MeasureString( gr, sText, font, textRect.Location, stringFormat ).ToSize();
						Size size = column.Layout.MeasureString( gr, sText, font, stringFormat );
						
						// AS 8/12/03 optimization - Use the new caching mechanism.
						//
						//gr.Dispose();	
						// MRS 6/1/04 - UWG2915
						//DrawUtility.ReleaseCachedGraphics(gr);
						column.Layout.ReleaseCachedGraphics( gr );
					
						if ( createdFont != null )
							createdFont.Dispose();

						textRect.Size = size;

						// Adjust the rect based on alignment
						switch ( appCell.TextHAlign )
						{
							case HAlign.Left:
								// Leave space for margin
								textRect.Offset( 2, 0 );								
								break;

							case HAlign.Right:						
								
								textRect.X = this.Rect.Right - textRect.Width - 2;
								break;

							case HAlign.Center:
						
								int width = textRect.Width;
								int cellWidth = this.Rect.Width;

								// Now that text is always shifted don't shift again								
								// Offset text if a button is being displayed
								textRect.X  += ( cellWidth - width ) / 2;								
								break;							
						}

						return textRect;
					}
				}
			}
			
			return Rectangle.Empty;
		}

		#endregion // GetTextSize

		#region Commented Out Code

		

		#endregion // Commented Out Code

		#endregion // Header, Row, Summary Tooltips

		#region OnMouseLeave

		/// <summary>
		/// Forces a redraw if activebuttons is true.
		/// </summary>
		protected override void OnMouseLeave( )
		{
			// SSP 10/14/05 BR06301
			// Moved the code into the new OnMouseEnterLeave method.
			// 
			this.OnMouseEnterLeave( false );
			
		}

		#endregion // OnMouseLeave

		#region InvalidateIfHotTracked

		// SSP 9/9/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal bool InvalidateIfHotTracked( )
		{
			if ( this.isMouseOverCell )
			{
				UltraGridRow row = this.Row;
				UltraGridColumn column = this.Column;
				if ( null != row && null != column && row.HasCellHotTrackingAppearances( column ) )
					this.DirtyChildElements( );

				return true;
			}

			return false;
		}

		#endregion // InvalidateIfHotTracked

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Band, StyleUtils.Role.Cell );
			}
		}

		#endregion // UIRole
	}

	#endregion // CellUIElementBase Class

	#region CellUIElement

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Split the CellUIElement into CellUIElementBase class and CellUIElement class so 
	// the filter cell element can derive from CellUIElementBase.
	//
	/// <summary>
	/// The ui element class for representing cells.
	/// </summary>
	public class CellUIElement : CellUIElementBase
	{
		#region Private Vars

		// SSP 11/14/04 - Merged Cell Feature
		// This variable is set to the merged cell element over this cell if any.
		//
		private MergedCellUIElement mergedCellElem = null;

		#endregion // Private Vars

		#region Constructor
		
		internal CellUIElement( UIElement parent ) : base( parent )
		{	
		}

		/// <summary>
		/// Initializes a new <b>CellUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="column">Associated <b>Column</b></param>
		public CellUIElement( UIElement parent, UltraGridColumn column ) : base( parent, column )
		{	
		}

		#endregion // Constructor

		#region DrawForeground

		// SSP 4/23/03 - Optimizations.
		// Overrode DrawForground so we can draw the text right here.
		//
		internal string GetCellTextHelper( )
		{
			UltraGridRow row = this.Row;
			UltraGridColumn column = this.Column;

			if ( CellDisplayStyle.PlainText == this.cellDisplayStyle )
			{
				object dataVal = row.GetCellValue( column );
				return 
					null != dataVal && DBNull.Value != dataVal 
					? dataVal.ToString( )
					: column.NullTextResolved;
			}
			else // Formatted text
			{
				string text = row.GetCellText( column );
				return null != text ? text : column.NullTextResolved;
			}
		}

		/// <summary>
		/// Default foreground rendering - does nothing
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawForeground ( ref UIElementDrawParams drawParams )
		{
			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			//
			if ( CellDisplayStyle.FullEditorDisplay == this.cellDisplayStyle )
				return;

			EmbeddableEditorOwnerInfo ownerInfo = this.Column.EditorOwnerInfo;

			string text = this.GetCellTextHelper( );

			Rectangle rect = this.RectInsideBorders;
			
			// Adjust for the padding.
			//
			Size padding = new Size( 0, 0 );
			ownerInfo.GetPadding( this, out padding );
			rect.Inflate( - padding.Width, - padding.Height );

			drawParams.DrawString( 
				rect, 
				text, 
				ownerInfo.IsMultiLine( this ), 
				ownerInfo.WrapText( this ) );
		}


		#endregion DrawForeground

		#region DrawImageBackground

		/// <summary>
		/// This element may not draw an image background.
		/// </summary>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			//
			if ( CellDisplayStyle.FullEditorDisplay != this.cellDisplayStyle )
			{
				base.DrawImageBackground( ref drawParams );
				return;
			}

			// SSP 9/6/02
			// We don't want both the cell and the embeddable element drawing the background color
			// and the backround image. For columns styles of buttons, we don't use embeddable
			// element so do so only for column styles that are not buttons.
			//
			if ( !this.Column.IsColumnStyleButton( this.Row ) )
				return;
			else			
				// JJD 1/21/02 - UWG951
				// Buttons don't draw their backgrounds
				//
				if ( //this.Column.IsColumnStyleButton( this.Row ) &&
				drawParams.UsesThemes )
				return;

			base.DrawImageBackground( ref drawParams );			
		}

		#endregion // DrawImageBackground

		#region DrawBackColor

		/// <summary>
		/// This element may not draw its back color.
		/// </summary>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{
			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			//
			if ( CellDisplayStyle.FullEditorDisplay != this.cellDisplayStyle )
				base.DrawBackColor( ref drawParams );

			// AS 1/27/03 UWG1968/UWG1966
			// When the element is within another element and alphablending is
			// occuring, having the parent and this element render the backcolor
			// ends up alphablending this backcolor onto that rendered by the parent
			// element. When the backcolor is white, it appears as if the cell/element
			// is lighter than others that do not have this layering.
			//
			return;

			
		}

		#endregion // DrawBackColor

		#region PositionChildElementsHelper

		internal override void PositionChildElementsHelper( 
			UltraGridColumn column, UltraGridRow row, RowScrollRegion rsr, ColScrollRegion csr )
		{
			RowPromptUIElement extractedRowPromptElement;

			this.PositionChildElementsHelperHelper( column, row, rsr, csr, out extractedRowPromptElement );

			// Added ability to display prompts in add-rows. Also the new filter row feature displays
			// prompts in filter rows as well.
			//
			RowPromptUIElement.AddRowPromptElementHelper( row, this, null, extractedRowPromptElement );
		}

		private void PositionChildElementsHelperHelper( 
			UltraGridColumn column, UltraGridRow row, RowScrollRegion rsr, ColScrollRegion csr,
			out RowPromptUIElement extractedRowPromptElement )
		{
			// SSP 5/14/03 - Optimizations
			// This internal member variable will be used by the EmbeddableEditorOwnerInfo.GetEditor
			// to return the editor directly from the cell element rather than going through the normal
			// editor resolution mechanism which involves checkinf if the cell has been created or not
			// since now we support cell level value lists and editors. EmbeddableEditorOwnerInfo.GetEditor
			// method gets called quite a lot of times.
			//
			this.embeddableEditorLastUsed = null;

			// SSP 4/1/02
			// Modified to implement embeddable editors.
			//

			UltraGridCell cell = row.GetCellIfAllocated( column );

			EmbeddableUIElementBase		embeddableUIElement = null;
			CellButtonUIElement			buttonElement		= null;
			// SSP 4/2/02
			// Embeddable editors.
			//CellCheckBoxUIElement		checkBoxElement		= null;			
			//CellDropDownButtonUIElement dropDownButton		= null;
			//DependentTextUIElement		textElement			= null;
			// SSP 6/6/02
			// Embeddable editors. Took out the code that draws the appearance image.
			// Editors will be doing that now.
			//ImageUIElement				imageElement		= null;
			EditButtonUIElement			editButtonElement	= null;
			
			// SSP 12/22/04 - IDataErrorInfo Support
			//
			DataErrorIconUIElement dataErrorIconElement = null;

			extractedRowPromptElement = null;

			// SSP 3/1/02
			// Added support of displaying images in the bound list
			//
			// SSP 8/11/03 - Use the embeddable image renderer instead of the image ui element.
			// Commented out below line.
			//
			//ImageUIElement				dataImageElement	= null;

			// save the old elements for re-use
			//
			if ( this.childElementsCollection != null )
			{
				// SSP 1/3/02 Optimization.
				// Moved this here from below. Also pass in true for newly added parameter of extract.
				//
				// SSP 4/1/02
				// Get the embeddable element from the child elements collection.
				//
				embeddableUIElement = this.FindEmbeddableUIElement( true );

				if ( this.childElementsCollection.Count > 0 )
				{

					buttonElement		= (CellButtonUIElement)CellUIElement.ExtractExistingElement( this.childElementsCollection, typeof(CellButtonUIElement), true );		

					// SSP 4/2/02
					// Embeddable editors.
					//dropDownButton		= (CellDropDownButtonUIElement)CellUIElement.ExtractExistingElement( this.childElementsCollection, typeof(CellDropDownButtonUIElement), true );
					//checkBoxElement		= (CellCheckBoxUIElement)CellUIElement.ExtractExistingElement( this.childElementsCollection, typeof( CellCheckBoxUIElement ), true );
					// 4/26/02
					// EM Embeddable editors.
					//textElement			= (DependentTextUIElement)CellUIElement.ExtractExistingElement( this.childElementsCollection, typeof( DependentTextUIElement ), true );				

					//imageElement		= (ImageUIElement)CellUIElement.ExtractExistingElement( this.childElementsCollection, typeof(ImageUIElement), true );
					editButtonElement	= (EditButtonUIElement)CellUIElement.ExtractExistingElement( this.childElementsCollection, typeof(EditButtonUIElement), true );
				
					// SSP 12/22/04 - IDataErrorInfo Support
					//
					dataErrorIconElement = (DataErrorIconUIElement)CellUIElement.ExtractExistingElement( this.childElementsCollection, typeof( DataErrorIconUIElement ), true );

					// SSP 3/30/05 - NAS 5.2 Add row prompt
					//
					// Added ability to display prompts in add-rows. Also the new filter row feature displays
					// prompts in filter rows as well.
					//
					extractedRowPromptElement = (RowPromptUIElement)CellUIElement.ExtractExistingElement( this.childElementsCollection, typeof( RowPromptUIElement ), true );

					// SSP 3/1/02
					//
					// SSP 8/11/03 - Use the embeddable image renderer instead of the image ui element.
					// Commented out below line.
					//
					//dataImageElement	= (ImageUIElement)CellUIElement.ExtractExistingElement( this.childElementsCollection, typeof(ImageUIElement), true );

					// SSP 1/3/02 Optimization.
					// Moved this up.
					//
					// SSP 4/1/02
					// Get the embeddable element from the child elements collection.
					//
					//embeddableUIElement = this.FindEmbeddableUIElement( );
 
					this.childElementsCollection.Clear();
				}
			}

			// SSP 3/27/03 - Row Layout Functionality
			// Store the grid in a variable for use further down.
			//
			//bool isUltraGrid = column.Layout.Grid is UltraGrid;
			UltraGridLayout layout = row.Layout;
			UltraGridBase grid = layout.Grid;
			bool isUltraGrid = grid is UltraGrid;

			Rectangle textRect = this.RectInsideBorders;

			// SSP 12/22/04 - IDataErrorInfo Support
			//
			// ------------------------------------------------------------------------------            
            bool hasDataErrorIcon = row.HasDataError(column);
            if (hasDataErrorIcon)
            {
                // MBS 3/24/09 - TFS15730
                // Refactored this logic so that the GridCellProxy can also position a data error icon
                #region Refactored
                //    AppearanceData dataErrorImageData;
                //    Image dataErrorImage = row.GetDataErrorImage(column, out dataErrorImageData);
                //    int imageElemWidth = DataErrorIconUIElement.DEFAULT_WIDTH;

                //    if (null != dataErrorImage)
                //    {
                //        imageElemWidth = 2 * DataErrorIconUIElement.PADDING + (int)(dataErrorImage.Width * Math.Min(1.0f, (float)(textRect.Height - 2 * DataErrorIconUIElement.PADDING) / dataErrorImage.Height));
                //        imageElemWidth = Math.Min(imageElemWidth, Math.Max(textRect.Width / 3, DataErrorIconUIElement.DEFAULT_WIDTH));
                //        imageElemWidth = Math.Min(imageElemWidth, textRect.Width);
                //    }

                //    Rectangle dataErrorIconRect = textRect;
                //    dataErrorIconRect.Width = imageElemWidth;

                //    if (HAlign.Right == dataErrorImageData.ImageHAlign)
                //    {
                //        textRect.Width -= imageElemWidth;
                //        dataErrorIconRect.X = textRect.Right;
                //    }
                //    else
                //    {
                //        textRect.X += imageElemWidth;
                //        textRect.Width -= imageElemWidth;
                //    }

                //    if (null == dataErrorIconElement)
                //        dataErrorIconElement = new DataErrorIconUIElement(this);

                //    dataErrorIconElement.InitImageAppData(ref dataErrorImageData);
                //    dataErrorIconElement.Rect = dataErrorIconRect;
                //    this.ChildElements.Add(dataErrorIconElement);            
                #endregion //Refactored
                //
                PositionDataErrorIcon(column, row, this, this.ChildElements, dataErrorIconElement, ref textRect);
            }
			// ------------------------------------------------------------------------------

			// SSP 7/18/03 - Optimizations
			// Added Style property to the cell object. Resolving using StyleResolved is a bit
			// inefficient. Here we are only trying to check if the style is Button or EditButton.
			// To do that, just get the style off the cell and column if the cell has it not set.
			//
			ColumnStyle tmpStyle = null != cell && ColumnStyle.Default != cell.Style ? cell.Style : column.Style;

			bool isColumnStyleButton = ColumnStyle.Button == tmpStyle;
			bool isColumnStyleEditButton = ColumnStyle.EditButton == tmpStyle;

			// Give higher priority to cell.Editor and cell.EditorControl than column.Style.
			//
			if ( isColumnStyleButton || isColumnStyleEditButton )
			{
				if ( null != cell &&
					ColumnStyle.Default == cell.Style
                    // MRS - NAS 9.2 - Control Container Editor
					//&& ( null != cell.Editor || null != cell.EditorControl ) )
                    && (null != cell.Editor || null != cell.EditorComponentResolved))
					isColumnStyleButton = isColumnStyleEditButton = false;
			}

			if ( isUltraGrid 
				// SSP 7/18/03 - Optimizations
				// Added Style property to the cell object. Resolving using StyleResolved is a bit
				// inefficient. Here we are only trying to check if the style is Button or EditButton.
				// To do that, just get the style off the cell and column if the cell has it not set.
				//
				//&& column.IsColumnStyleButton 
				&& isColumnStyleButton
				&&  column.ShouldBtnElementBeCreated( row, rsr, csr ) )
			{
				if ( null == buttonElement )
				{
					buttonElement = new CellButtonUIElement( this );
				}
				
				//if its Style.Button then we want the button to occupy the 
				//whole cell text area
				buttonElement.Rect = textRect;
				
				this.ChildElements.Add( buttonElement );

				// SSP 1/18/05 BR01711
				// Set the cellDisplayStyle to FullEditorDisplay so we don't draw the text in
				// the DrawForground method since the button will be drawing the text and the
				// background.
				//
				this.cellDisplayStyle = CellDisplayStyle.FullEditorDisplay;

				return;
			}

			// JJD 08/17/01 - UWG59
			// Added support for displaying images in cells
			//
			// AS 10/29/02
			// Changed to use AllRender since the ResolveCellAppearance
			// will cache the appearance data if (amongst other things)
			// the prop flags passed to the method is AllRender. 
			//
			
			// SSP 4/17/03
			// Moved this into the if block where it's needed. We shouldn't resolve unless
			// the apeparance is needed.
			//
			//AppearanceData appData = new AppearanceData();
			//AppearancePropFlags flags = AppearancePropFlags.AllRender;
			//this.InitAppearance( ref appData, ref flags );


			// SSP 11/15/01 UWG720
			// If the cell's column has a value list with pictures in the value list items
			// reserve enough space for pictures so if the user selects a picture from
			// the drop down, it will be displayed here. This seems to be consistent with
			// activeX version of the grid.
			//
			// set displayImage flag to true by default so that if the appData
			// has an image set on it, we set the image on the image element.
			//
			
			// SSP 4/26/02
			// EM Embeddable editors.
			// Value list item images and reserving of space will be done by the editors.
			//
			

			// SSP 11/28/01 UWG626
			// Moved this code that adds the button to here from below ( before
			// it was after the code that adds image). This way we add the button
			// to the right edge of the cell and then add any possibly right algined
			// image.
			// 
			
			// Use new ShouldBtnElementSpaceBeReserved method to determine
			// whether we need to adjust the text rect
			// 
			if ( isUltraGrid &&
				// SSP 4/2/02
				// Only reserver space for edit buttons because drop down buttons are taken
				// care of by the editors themselves.
				//
				// SSP 7/18/03 - Optimizations
				// Added Style property to the cell object. Resolving using StyleResolved is a bit
				// inefficient. Here we are only trying to check if the style is Button or EditButton.
				// To do that, just get the style off the cell and column if the cell has it not set.
				//
				//column.IsColumnStyleEditButton &&
				isColumnStyleEditButton &&
				// SSP 4/14/03 Optimizations
				// Nested this inside the if block so we don't need to resolve the appearance
				// unless needed.
				//
				//column.ShouldBtnElementSpaceBeReserved( row, ref appData, rsr, csr ) &&
				//We don't want to show this if we are printing
				//ROBA UWG135 8/9/01
				// SSP 1/27/05 BR02053
				// Instead of checking IsPrinting flag, make use of IsDisplayLayout. What we are 
				// interested in finding out is this element is in the print layout or not. Simply
				// checking the IsPrinting flag won't tell us that the display layout could verify 
				// it's ui elements while printing (like when the grid gets a paint while printing,
				// especially with the new UltraGridPrintDocument).
				//
				//!column.Layout.Grid.IsPrinting 
				column.Layout.IsDisplayLayout 
				)
			{
				AppearanceData appData = new AppearanceData( );
				AppearancePropFlags requestedFlags = AppearancePropFlags.AllRender;
				this.InitAppearance( ref appData, ref requestedFlags );

				if ( column.ShouldBtnElementSpaceBeReserved( row, ref appData, rsr, csr ) )
				{
					Rectangle buttonRect = textRect;
				
					// SSP 9/26/02 UWG1674
					// I don't think we need to shrink the button rect. It causes
					// the cell's background to appear around the button.
					//
					//buttonRect.Inflate( -1, -1 );				
					//buttonRect.X += 1;

					int tmp = System.Math.Max( textRect.Left, buttonRect.Right - SystemInformation.VerticalScrollBarWidth );
					buttonRect.Width -= tmp - buttonRect.X;
					buttonRect.X = tmp;

					// SSP 9/26/02 UWG1674
					// Don't leave any space between the embeddable element and the button
					// otherwise the grid's background will show up in-between. The emebddable
					// editors have padding so it won't look bad.
					//
					//textRect.Width = buttonRect.Left - 1 - textRect.Left; 
					textRect.Width = buttonRect.Left - textRect.Left;

					// add the btn element
					//
					// SSP 4/2/02
					// Take out the code that adds the drop down button because that is taken care
					// of by the editors.
					//
					
					if (
						// SSP 4/2/02							
						//column.IsColumnStyleEditButton && 
						column.ShouldBtnElementBeCreated( row, rsr, csr ) )
					{
						// We always want to add the element if we are in here since
						// ShouldBtnElementBeCreated returned true 
						//

						if ( null == editButtonElement )
						{
							editButtonElement = new EditButtonUIElement( this );
						}

						editButtonElement.Rect = buttonRect;

						this.ChildElements.Add( editButtonElement );
					}
					// SSP 7/26/07 BR22926
					// Added the else block. We are carving out space for the button element however not
					// positioning the button element. This is correct however since the cell element
					// doesn't draw the backcolor (DrawBackColor method is overridden), the space reserved
					// for the edit button will end up showing the row element's back color, which could be
					// different than the cell's back color. For example, the editor element could draw
					// white back color while the row element could draw gray, resulting in the space
					// reserved for the edit button to be gray as well. Therefore add a ui element in place
					// of the reserved space to draw the backcolor.
					// 
					else
					{
						EditButtonSpaceHolderUIElement tmpElem = new EditButtonSpaceHolderUIElement( this );
						tmpElem.Rect = buttonRect;
						this.ChildElements.Add( tmpElem );
					}
				}
			}

			// SSP 6/6/02
			// Embeddable editors. Removed code for drawing the appearance image.
			// The editors do that now.
			//
			

			// We are going to use the editors for checkboxes
			//
			


			// SSP 3/1/02
			// Added support for displaying images if the content of the bound list
			// for the cell is an instance of Image.
			//
			//------------------------------------------------------------------
			// SSP 8/11/03 - Optimizations
			// In CellUIElement.PositionChildElements we check if the cell value is an Image or not.
			// In most cases we don't need to do that. So added a flag off the column that will 
			// prevent the cell ui element from checking if the cell value is an image for columns
			// that we know won't contain images.
			//
			object cellValue = ! column.ShouldCheckForImageCellValue ? null : row.GetCellValue( column );
			
			if ( cellValue is System.Drawing.Image )
			{
				// SSP 8/11/03 - Embeddable Image Renderer - UWG2477
				// Make use of the embeddable image renderer instead of the image ui element.
				// Commented the original code out and added the new one (the original code 
				// is below the newly added code).
				//
				// --------------------------------------------------------------------------------------
				// Set the cellDisplayStyle to FullEditorDisplay so we don't draw the 
				// cell text in the DrawForeground.
				// 
				this.cellDisplayStyle = CellDisplayStyle.FullEditorDisplay;
				// SSP 8/25/05
				// If the column's editor is already an EmbeddableImageRenderer then use that first.
				// ----------------------------------------------------------------------------------
				//EmbeddableEditorBase imageRender = layout.ImageRenderer;
				EmbeddableEditorBase imageRender = column.GetEditor( row );
				if ( ! ( imageRender is EmbeddableImageRenderer ) )
					imageRender = layout.ImageRenderer;

				this.embeddableEditorLastUsed = imageRender;
				// ----------------------------------------------------------------------------------

				embeddableUIElement = imageRender.GetEmbeddableElement( 
					this, // Parent element
					column.EditorOwnerInfo, // owner
					this, // owner context
					false, // include edit elements
					false, // reserve space for edit elements
					false, // draw outer borders
					null == embeddableUIElement || embeddableUIElement.Editor == imageRender 
					? embeddableUIElement : null // previous element
					);
				
				embeddableUIElement.Rect = textRect;
				this.ChildElements.Add( embeddableUIElement );
				return;
				
				// --------------------------------------------------------------------------------------
				
			}
			//------------------------------------------------------------------



			// SSP 2/4/02 UWG1014
			// If the value list's DisplayStyle is set to Picture then don't 
			// display the text.
			//
			// SSP 4/26/02
			// EM Emebddable editors.
			//
			//bool addTextElement = true;

			// SSP 4/2/02
			// Embeddable editors render this unnecessary.
			//
			

			// SSP 4/26/02
			// EM Embeddable editors.
			// See if we need to add edit button.
			//

			// SSP 8/15/02 UWG1561
			// We are adding the edit button somewhere above. So don't do it again.
			//
			

			// SSP 4/22/05 - NAS 5.2 Filter Row
			// Moved code from here moved into GetEmbeddableEditorElementHelper.
			//
			embeddableUIElement = this.GetEmbeddableEditorElementHelper( 
									column, row, rsr, csr, cell, embeddableUIElement );
			if ( null != embeddableUIElement )
			{
				embeddableUIElement.Rect = textRect;
				this.ChildElements.Add( embeddableUIElement );
			}


			// SSP 4/26/02
			// EM Emebddable editors.
			// Commented out the rest of the code that adds the text element.
			//
			

			// SSP 11/28/01 UWG626
			// Moved this code before the code that adds image so that if the
			// image is right aligned, it displays left of the button.
			//
			
			
			
		}

        // MBS 3/24/09 - TFS15730
        // Refactored from above
        internal static void PositionDataErrorIcon(UltraGridColumn column, UltraGridRow row, UIElement parent, UIElementsCollection childElements, DataErrorIconUIElement dataErrorIconElement, ref Rectangle textRect)
        {
            AppearanceData dataErrorImageData;
            Image dataErrorImage = row.GetDataErrorImage(column, out dataErrorImageData);
            int imageElemWidth = DataErrorIconUIElement.DEFAULT_WIDTH;

            if (null != dataErrorImage)
            {
                imageElemWidth = 2 * DataErrorIconUIElement.PADDING + (int)(dataErrorImage.Width * Math.Min(1.0f, (float)(textRect.Height - 2 * DataErrorIconUIElement.PADDING) / dataErrorImage.Height));
                imageElemWidth = Math.Min(imageElemWidth, Math.Max(textRect.Width / 3, DataErrorIconUIElement.DEFAULT_WIDTH));
                imageElemWidth = Math.Min(imageElemWidth, textRect.Width);
            }

            Rectangle dataErrorIconRect = textRect;
            dataErrorIconRect.Width = imageElemWidth;

            if (HAlign.Right == dataErrorImageData.ImageHAlign)
            {
                textRect.Width -= imageElemWidth;
                dataErrorIconRect.X = textRect.Right;
            }
            else
            {
                textRect.X += imageElemWidth;
                textRect.Width -= imageElemWidth;
            }

            if (null == dataErrorIconElement)
                dataErrorIconElement = new DataErrorIconUIElement(parent);

            dataErrorIconElement.InitImageAppData(ref dataErrorImageData);
            dataErrorIconElement.Rect = dataErrorIconRect;
            childElements.Add(dataErrorIconElement);
        }

		#endregion // PositionChildElementsHelper

		#region SupportsUpDownAdjustmentsFromPoint

		/// <summary>
		/// Returns true if the element can be moved or resized vertically
		/// by clicking on the passed in mouse point
		/// </summary>
		/// <param name="point">In client coordinates</param>
        /// <returns>Returns true if the element can be moved or resized vertically by clicking on the passed in mouse point</returns>
		public override bool SupportsUpDownAdjustmentsFromPoint( System.Drawing.Point point )
		{
			if ( ! base.SupportsUpDownAdjustmentsFromPoint( point ) )
				return false;

			// SSP 11/29/04 - Merged Cell Feature
			// Don't allow updown adjustments if the cell is under a merged cell and the
			// cell is not the last of the cells that the merged cell is comprised of.
			//
			MergedCellUIElement mergedCellElem = this.MergedCellElement;
			MergedCell mergedCell = null != mergedCellElem ? mergedCellElem.MergedCell : null;
			if ( null != mergedCell && this.Row != mergedCell.EndRow )
				return false;

			return true;
		}

		#endregion // SupportsUpDownAdjustmentsFromPoint

		// SSP 11/12/04 - Merged Cell Feature
		// 
		#region Merged Cell Feature

		internal void SetMergedCellElement( MergedCellUIElement mergedCellElem )
		{
			this.mergedCellElem = mergedCellElem;
		}

		internal MergedCellUIElement MergedCellElement
		{
			get
			{
				if ( null != this.mergedCellElem && this.mergedCellElem.Disposed )
					this.mergedCellElem = null;

				return this.mergedCellElem;
			}
		}

		/// <summary>
		/// Overridden. Returns false if this cell element is under a merged cell element.
		/// </summary>
		public override bool IsElementDrawn
		{
			get
			{
				return null == this.MergedCellElement && base.IsElementDrawn;
			}
		}

		#endregion // Merged Cell Feature

	}

	#endregion // CellUIElement
}




