#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;

// SSP 7/6/05 - NAS 5.3 Empty Rows
// Added this file.
//

namespace Infragistics.Win.UltraWinGrid
{
	#region EmptyRowsAreaUIElement Class

	/// <summary>
	/// This element hosts the empty row elements.
	/// </summary>
	public class EmptyRowsAreaUIElement : UIElement
	{
		#region Private Vars

		private int verifiedRowChildElementsCacheVersion = -1;

		#endregion // Private Vars

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public EmptyRowsAreaUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags flags)
		{
			UltraGridLayout layout = this.Layout;
			Debug.Assert( null != layout );
			if ( null != layout && layout.HasEmptyRowSettings )
				layout.EmptyRowSettings.ResolveEmptyAreaAppearance( ref appData, ref flags );

			base.InitAppearance( ref appData, ref flags );
		}

		#endregion // InitAppearance

		#region Layout

		internal UltraGridLayout Layout
		{
			get
			{
				return (UltraGridLayout)this.GetContext( typeof( UltraGridLayout ), true );
			}
		}

		#endregion // Layout

		#region ClipChildren

		/// <summary>
		/// Returning true causes all drawing of this element's children to be expicitly clipped
		/// to its region
		/// </summary>
		protected override bool ClipChildren 
		{ 
			get 
			{ 
				return true; 
			} 
		}

		#endregion // ClipChildren

		#region AddEmptyRowElemHelper

		private UIElement AddEmptyRowElemHelper( Rectangle rect, UIElementsCollection oldElems, 
			VisibleRow vr, RowColRegionIntersectionUIElement rcrElem )
		{
			return rcrElem.AddRowHelper( this, oldElems, ref rect, vr, vr.SkipDblTopBorderDisplay( ),
				this.verifiedRowChildElementsCacheVersion == rcrElem.RowScrollRegion.Layout.RowChildElementsCacheVersion );
		}

		#endregion // AddEmptyRowElemHelper

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			UIElementsCollection oldElems = this.childElementsCollection;
			this.childElementsCollection = null;

			RowColRegionIntersectionUIElement rcrElem = (RowColRegionIntersectionUIElement)this.Parent.GetAncestor( typeof( RowColRegionIntersectionUIElement ) );
			RowScrollRegion rsr = null != rcrElem ? rcrElem.RowScrollRegion : null;
			ColScrollRegion csr = null != rcrElem ? rcrElem.ColScrollRegion : null;

			try
			{
				if ( null == rsr || null == csr || null == rsr.Layout )
					return;

				UltraGridLayout layout = rsr.Layout;

				Rectangle thisRect = this.RectInsideBorders;
				EmptyRowSettings emptyRowSettings = layout.EmptyRowSettings;
				
				// Get the view style and the last visible row after which the empty rows are displayed.
				// This is so we can see if we need to merge the first empty row's top border with the
				// last visible actual row's bottom border.
				// 
				ViewStyleBase viewStyle = layout.ViewStyleImpl;
				VisibleRow lastVisibleRow = rsr.VisibleRows.GetFirstLastScrollableRow( false, false );
				VisibleRow origLastScrollableRow = lastVisibleRow;

				int top = thisRect.Y - rsr.Origin;

				// Emulate how the view style would have generated the visible rows if it had taken
				// into account the empty rows. We don't want the empty rows to be part of the visible
				// rows collection and therefore we have to do it here. This is necessary for things
				// like merging borders between rows, and leaving inter-band spacing between rows of
				// different bands (between the last visible row which may be from a child band and
				// the empty rows).
				// 
				VisibleRowFetchRowContext visibleRowFetchRowContext = new VisibleRowFetchRowContext( rsr );
				bool firstTime = true;
				while ( top < thisRect.Bottom )
				{
					VisibleRow vr = new VisibleRow( rsr );
					UltraGridRow dummyRow = firstTime ? emptyRowSettings.FirstEmptyRow : emptyRowSettings.EmptyRow;
					vr.Initialize( null, dummyRow );
					vr.SetTop( top );

					// This is so we can see if we need to merge the top border with the bottom border
					// of the last row.
					// 
					if ( null != lastVisibleRow )
					{
						visibleRowFetchRowContext.lastRowInserted = lastVisibleRow;
						viewStyle.InternalOrientVisibleRowForward( ref visibleRowFetchRowContext, vr );
					}

					Rectangle rect = vr.GetDimensions( csr, VisibleRowDimensions.OutsideRow, DimOriginBase.OnScreen );
					this.AddEmptyRowElemHelper( rect, oldElems, vr, rcrElem );
                    
					// Increase the top to where the next empty row should start.
					// 
					top += vr.GetTotalHeight( );

					// Make sure to dispose the visible rows properly. Once the ui elements are added
					// there is no need to hold on to the visible rows. Not only that the visible rows
					// hook into the underying rows so disposing is necessary.
					// 
					if ( null != lastVisibleRow && lastVisibleRow != origLastScrollableRow )
						lastVisibleRow.Dispose( );
					lastVisibleRow = vr;

					firstTime = false;
				}

				// Make sure to dispose the visible rows properly. Once the ui elements are added
				// there is no need to hold on to the visible rows. Not only that the visible rows
				// hook into the underying rows so disposing is necessary.
				// 
				if ( null != lastVisibleRow )
					lastVisibleRow.Dispose( );

				this.verifiedRowChildElementsCacheVersion = layout.RowChildElementsCacheVersion;
			}
			finally
			{
				if ( null != oldElems )
					oldElems.DisposeElements( );
			}
		}
		
		#endregion // PositionChildElements

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Layout, StyleUtils.Role.EmptyRowsArea );
			}
		}

		#endregion // UIRole
	}

	#endregion // EmptyRowsAreaUIElement Class

	#region EmptyRowUIElement Class

	/// <summary>
	/// This element represents an empty row.
	/// </summary>
	public class EmptyRowUIElement : RowUIElementBase
	{
		#region Constructor
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public EmptyRowUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#region EmptyRowSettings

		//internal EmptyRowSettings EmptyRowSettings
		//{
		//    get
		//    {
		//        UltraGridRow row = this.Row;
		//        return null != row ? row.Layout.EmptyRowSettings : null;
		//    }
		//}

		//#endregion // EmptyRowSettings

		#endregion Not Used

		#region Base Overrides

		#region CreateRowCellAreaUIElement

		/// <summary>
		/// Method for creating RowCellAreaUIElementBase derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>An existing RowCellAreaUIElementBase if one exists. Otherwise, a new instance.</returns>
		protected override RowCellAreaUIElementBase CreateRowCellAreaUIElement( UIElementsCollection oldElems )
		{
			EmptyRowCellAreaUIElement elem = (EmptyRowCellAreaUIElement)EmptyRowUIElement.ExtractExistingElement( oldElems, typeof( EmptyRowCellAreaUIElement ), true );
			return null != elem ? elem : new EmptyRowCellAreaUIElement( this );
		}

		#endregion // CreateRowCellAreaUIElement

		#region CreateRowSelectorUIElement

		/// <summary>
		/// Method for creating RowSelectorUIElementBase derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>An existing RowSelectorUIElementBase if one exists. Otherwise, a new instance.</returns>
		protected override RowSelectorUIElementBase CreateRowSelectorUIElement( UIElementsCollection oldElems )
		{
			EmptyRowSelectorUIElement elem = (EmptyRowSelectorUIElement)EmptyRowUIElement.ExtractExistingElement( oldElems, typeof( EmptyRowSelectorUIElement ), true );
			return null != elem ? elem : new EmptyRowSelectorUIElement( this );
		}

		#endregion // CreateRowSelectorUIElement

		#region CreatePreRowAreaUIElement

		/// <summary>
		/// Method for creating PreRowAreaUIElement derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>An existing PreRowAreaUIElement if one exists. Otherwise, a new instance.</returns>
		protected override PreRowAreaUIElement CreatePreRowAreaUIElement( UIElementsCollection oldElems )
		{
			PreRowAreaUIElement elem = (PreRowAreaUIElement)EmptyRowUIElement.ExtractExistingElement( oldElems, typeof( PreRowAreaUIElement ), true );
			return null != elem ? elem : new PreRowAreaUIElement( this );
		}

		#endregion // CreatePreRowAreaUIElement

		#endregion // Base Overrides
	}

	#endregion // EmptyRowUIElement Class

	#region EmptyRowCellAreaUIElement Class

	/// <summary>
	/// This element contains cell elements in empty rows.
	/// </summary>
	public class EmptyRowCellAreaUIElement : RowCellAreaUIElementBase
	{

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public EmptyRowCellAreaUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region CreateCellUIElement

		/// <summary>
		/// Method for creating CellUIElementBase derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>The existing CellUIElementBase or a new one if there was no existing instance.</returns>
		protected override CellUIElementBase CreateCellUIElement( UIElementsCollection oldElems )
		{
			EmptyRowCellUIElement elem = (EmptyRowCellUIElement)EmptyRowCellAreaUIElement.ExtractExistingElement( oldElems, typeof( EmptyRowCellUIElement ), true );
			return null != elem ? elem : new EmptyRowCellUIElement( this );
		}

		#endregion // CreateCellUIElement

		#region PositionChildElements

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void PositionChildElements( )
		{
			// If the empty row style is PrefixWithEmptyCell then prefix the row cell area with an empty cell.
			// 
			EmptyRowCellUIElement prefixCellElem = null;
			UltraGridEmptyRow row = this.Row as UltraGridEmptyRow;
			if ( null != row )
			{
				UltraGridBand band = row.BandInternal;
				EmptyRowStyle emptyRowStyle = row.EmptyRowSettings.Style;
				if ( EmptyRowStyle.PrefixWithEmptyCell == emptyRowStyle )
				{
					UltraGridColumn column = band.Columns.GetFirstNonChapteredColumn( );
					if ( null != column )
					{
						prefixCellElem = (EmptyRowCellUIElement)EmptyRowCellAreaUIElement.ExtractExistingElement( this.childElementsCollection, typeof( EmptyRowCellUIElement ), true );
						if ( null == prefixCellElem )
							prefixCellElem = new EmptyRowCellUIElement( this );

						// Preferrably initialize the prefix cell's column with the first cell element's
						// column. This way the prefix cell looks the same as the first cell. Another alternative
						// is to not add the prefix cell at all in which case the row element's background will
						// show up in the prefix area.
						// 
						CellUIElementBase firstCellElem = (CellUIElementBase)EmptyRowCellAreaUIElement.ExtractExistingElement( this.childElementsCollection, typeof( CellUIElementBase ), true );
						if ( null != firstCellElem && null != firstCellElem.Column )
							column = firstCellElem.Column;

						prefixCellElem.InitializeCell( column, row );

						// If the cell borders are merged with the row borders then use Rect insead of RectInsiderBorders.
						// 
						bool bordersMerged = band.Layout.CanMergeAdjacentBorders( row.BorderStyleCellResolved );
						Rectangle thisRect = bordersMerged ? this.Rect : this.RectInsideBorders;
						Rectangle rect =  thisRect;

						// The prefix cell's width should be the pre row area + the row selector extent.
						// 
						rect.Width = row.BeginCellsAt;

						// Account for row cell area merging its borders with the row selectors. This 
						// way the left borders of the first cells in empty rows are aligned with the
						// left borders of the first cells of data rows.
						// 
						if ( ! band.UseRowLayoutResolved && band.RowSelectorExtent > 0 && band.Layout.CanMergeAdjacentBorders( band.BorderStyleRowResolved ) )
							rect.Width--;

						// Merge the cell border with the cell right of it.
						// 
						if ( bordersMerged && ( ! band.UseRowLayoutResolved || 0 == band.RowSelectorExtent ) )
							rect.Width++;

						// Make sure the cell's rect doesn't go beyond the row cell area element.
						// 
						rect.Width = Math.Min( rect.Width, thisRect.Width );

						prefixCellElem.Rect = rect;
					}
				}
			}

			base.PositionChildElements( );
			
			// Add the prefix cell elem if any.
			// 
			if ( null != prefixCellElem )
				this.ChildElements.Add( prefixCellElem );
		}

		#endregion // PositionChildElements

	}

	#endregion // EmptyRowCellAreaUIElement Class

	#region EmptyRowCellUIElement Class

	/// <summary>
	/// This element represents a cell in an empty row.
	/// </summary>
	public class EmptyRowCellUIElement : CellUIElementBase
	{

		#region Constructor
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public EmptyRowCellUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region PositionChildElementsHelper

		internal override void PositionChildElementsHelper( 
			UltraGridColumn column, UltraGridRow rowParam, RowScrollRegion rsr, ColScrollRegion csr )
		{
		}

		#endregion // PositionChildElementsHelper

		#region Adjustable

		/// <summary>
		/// Overridden. Always returns false since the cells in empty rows should behave inactive.
		/// Returns true header can be resized
		/// </summary>
		/// <returns></returns>
		public override bool Adjustable
		{
			get
			{
				return false;
			}
		}

		#endregion // Adjustable

	}

	#endregion // EmptyRowCellUIElement Class

	#region EmptyRowSelectorUIElement Class

	/// <summary>
	/// This element represents a cell in an empty row.
	/// </summary>
	public class EmptyRowSelectorUIElement : RowSelectorUIElementBase
	{
		#region Constructor
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public EmptyRowSelectorUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

	}

	#endregion // EmptyRowSelectorUIElement Class

}
