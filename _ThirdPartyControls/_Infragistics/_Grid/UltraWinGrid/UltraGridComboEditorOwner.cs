#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Editor owner for the <see cref="UltraCombo"/>
	/// </summary>
	public class UltraGridComboEditorOwner : EmbeddableEditorOwnerBase
	{
		#region Member Variables

		private UltraCombo			control;
		private object				editorContext = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="UltraGridComboEditorOwner"/>
		/// </summary>
		/// <param name="control">Associated UltraCombo</param>
		public UltraGridComboEditorOwner(UltraCombo control)
		{
			this.control = control;
		}
		#endregion //Constructor

		#region Base class overrides

		#region GetControl	
		/// <summary>
		/// Returns the owner's control. 
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>The owner's control. This is used e.g. to re-parent the edit control.</returns>
		public override System.Windows.Forms.Control GetControl( object ownerContext )
		{
			return this.control;
		}
		#endregion //GetControl	

		#region GetDataType	
		/// <summary>
		/// Returns the data type.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>The data type.</returns>
		public override System.Type GetDataType( object ownerContext )
		{
			return typeof ( object );
		}
		#endregion //GetDataType

		#region GetPadding	

		/// <summary>
		/// The padding to place around the value to display.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="size">(out) The padding to place around the value to display.</param>
		/// <returns>A boolean indicating whether a meaningful value was returned.</returns>
		public override bool GetPadding( object ownerContext, out System.Drawing.Size size )
		{
			size = this.control.Padding;
			return true;
		}

		#endregion    //GetPadding	

		#region GetValue	

		/// <summary>
		/// Returns the value that should be rendered.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override object GetValue( object ownerContext )
		{
			// SSP 10/12/06 - NAS 6.3
			// Added SetInitialValue on the UltraCombo.
			// 
			object controlValue = this.control.Value;
			if ( this.control.dontLoadDataSource && null == controlValue 
				&& null != this.control.dontLoadDataSource_initialDataValue )
				return this.control.dontLoadDataSource_initialDataValue;

			// we we are entering edit mode and the value of
			// the control is null, pass in the text
			// so it is initialized with that value
			// MRS 6/27/05 - BR04469, BR04792, BR04781
//			if (ownerContext == this.control					&&
//				//this.control.InternalEditor.IsEnteringEditMode	&& 
//				this.control.Value == null)
			if (ownerContext == this.control					&&
				
				
				
				controlValue == null 
				&& ! this.MustSelectFromList(ownerContext) )
			{
				//MRS 4/18/06 - BR11468
				//return this.control.BaseText;
				string baseText = this.control.BaseText;
				switch (this.control.AllowNull)
				{
					case DefaultableBoolean.True:
						if (baseText == null || baseText.Length == 0)
							return null;

						break;
				}

				return baseText;
			}

			
			
			
			//return this.control.Value;
			return controlValue;
		}

		#endregion    //GetValue	

		#region IsEnabled	

		/// <summary>
		/// Returns whether the value is enabled for editing.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>True if the value is enabled for editing.</returns>
		public override bool IsEnabled( object ownerContext )
		{
			return this.control.Enabled;
		}

		#endregion    //IsEnabled	

		#region GetBorderStyle
		/// <summary>
		/// Returns the border style to be used by the embeddable editor element
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="borderStyle">The border style to be used by the embeddable editor element</param>
        /// <returns>The border style to be used by the embeddable editor element.</returns>
		public override bool GetBorderStyle( object ownerContext, out UIElementBorderStyle borderStyle )
		{
			borderStyle = this.control.BorderStyleResolved;
			return true;
		}
		#endregion //	GetBorderStyle

		#region GetButtonStyle

		// SSP 3/21/06 - App Styling
		// Added ButtonStyleResolved. We need to make use of the ButtonStyle setting of the role.
		// 
		/// <summary>
		/// Returns the ButtonStyle to be used by the embeddable element's button
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="buttonStyle">The ButtonStyle to be used by the embeddable element's buttons</param>
        /// <returns>the ButtonStyle to be used by the embeddable element's button</returns>
		/// <remarks>The default implementation returns <b>false</b>, with buttonStyle set to UIElementButtonStyle.Default.</remarks>
		public override bool GetButtonStyle( object ownerContext, out UIElementButtonStyle buttonStyle )
		{
			// just get the explicit setting first
			UIElementButtonStyle comboBtnStyle = this.control.ButtonStyleResolved;;
			if ( UIElementButtonStyle.Default != comboBtnStyle )
			{
				buttonStyle = comboBtnStyle;
				return true;
			}

			return base.GetButtonStyle( ownerContext, out buttonStyle );
		}

		#endregion GetButtonStyle

		#region GetDisplayStyle
		/// <summary>
		/// Returns the display style to be applied to the embeddable editor element
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>EmbeddableElementDisplayStyle</returns>
		public override EmbeddableElementDisplayStyle GetDisplayStyle( object ownerContext )
		{
			// SSP 4/10/06 - App Styling
			// Use the new DisplayStyleResolved which takes into account the app-style setting.
			// 
			//return this.control.DisplayStyle;
			return this.control.DisplayStyleResolved;
		}
		#endregion //	GetDisplayStyle

		#region IsReadOnly	

		/// <summary>
		/// Returns true is the value is read-only
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>A boolean indicating whether the text is read only</returns>
		public override bool IsReadOnly( object ownerContext )
		{
			return this.control.ReadOnly;
		}

		#endregion    //IsReadOnly	

		#region GetImageList
		/// <summary>
		/// Returns the image list to be used by the editor's ValueList, or null
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>An ImageList, or null if no ImageList exists</returns>
		public override ImageList GetImageList( object ownerContext )
		{
			return this.control.ImageList;
		}
		#endregion //	GetImageList
        
		#region GetContextMenu
		/// <summary>
		/// Returns the context menu to be used by the editor
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>A ContextMenu, or null if no ContextMenu exists</returns>
		public override ContextMenu GetContextMenu( object ownerContext )
		{
			return this.control.ContextMenu;
		}
		#endregion //	GetContextMenu

        // MRS 11/21/05 - BR07839
        #region GetContextMenuStrip
        /// <summary>
        /// Returns the context menu strip to be used by the editor
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>A ContextMenuStrip, or null if no ContextMenuStrip exists</returns>
        public override ContextMenuStrip GetContextMenuStrip(object ownerContext)
        {
            return this.control.ContextMenuStrip;
        }
        #endregion //	GetContextMenuStrip

		#region ShouldDrawFocus	

		/// <summary>
		/// Determines if a focus rect should be drawn.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>Returns true if a focus rect should be drawn.</returns>
		public override bool ShouldDrawFocus( object ownerContext )
		{
			//	If DropDownStyle = DropDownList, the list is not dropped down,
			//	and the control has focus, then draw the focus rect
			return ( this.control.DropDownStyle == UltraComboStyle.DropDownList &&
				!this.control.IsDroppedDown &&
				this.control.Focused );
		}

		#endregion    //ShouldDrawFocus	

		#region MustSelectFromList

		/// <summary>
		/// Returns whether a selection can only be made from the value list.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>If true will act as a combo with a style of DropDownList.</returns>
		/// <remarks>The default implementation returns false.</remarks>
		public override bool MustSelectFromList( object ownerContext )
		{
			return ( this.control.DropDownStyle == UltraComboStyle.DropDownList );
		}

		#endregion MustSelectFromList

		#region DrawAsActive
		/// <summary>
		/// Returns whether the element should be drawn as if it is in its "active" state.
		/// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>true if the element should be drawn as if it is in its "active" state.</returns>
		public override DefaultableBoolean DrawAsActive( object ownerContext )
		{
			//	Return true at design time so the active borders draw
			//	at design time (looks better)
			if ( this.DesignMode )
				return DefaultableBoolean.True;

			//	BF 6.4.02
			//	If the parent form is inactive, and the editor is not in edit mode,
			//	draw the borders as inactive
			//	BF 2.26.03	UWE454
			if ( 
				// SSP 4/10/06 - App Styling
				// Use the new DisplayStyleResolved which takes into account the app-style setting.
				// 
				//this.control.DisplayStyle != EmbeddableElementDisplayStyle.Standard &&
				this.control.DisplayStyleResolved != EmbeddableElementDisplayStyle.Standard &&
				//	BF 9.24.03	UWE705
				//! TextEditorControlBase.IsActiveForm( parentForm ) )
				! Utilities.IsControlOnActiveForm( this.control ) )
				return DefaultableBoolean.False;

			return DefaultableBoolean.Default;
		}
		#endregion //	DrawAsActive

		#region DesignMode
		/// <summary>
		/// Returns true if in design mode, false otherwise.
		/// </summary>
		public override bool DesignMode 
		{ 
			get { return this.control.DesignMode; }
		}
		#endregion //DesignMode

		#region GetEditorContext
		/// <summary>
		/// Gets the editor context that was set with SetEditorContext method.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>Editor context that was last set with SetEditorContext for the passed in ownerContext.</returns>
		/// <remarks>
		/// <p>GetEditorContext and <seealso cref="SetEditorContext"/> can be used to cache objects per owner context.</p>
		/// <p>Implementing owner will return the object that was last cached using SetEditorContext method.</p>
		/// </remarks>
		public override object GetEditorContext( object ownerContext )
		{
			return this.editorContext;
		}
		#endregion // GetEditorContext

		#region SetEditorContext

		/// <summary>
		/// Sets the editor context for the passed in ownerContext.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="editorContext"></param>
		/// <remarks>
		/// <p><seealso cref="GetEditorContext"/> and SetEditorContext can be used to cache objects per owner context.</p>
		/// <p>Implementing owner will return the object that was last cached using SetEditorContext method.</p>
		/// </remarks>
		public override void SetEditorContext( object ownerContext, object editorContext )
		{
			this.editorContext = editorContext;
		}
		#endregion // SetEditorContext

		#region GetValueList
		/// <summary>
		/// Returns the <see cref="UltraCombo"/>
		/// </summary>
		/// <param name="ownerContext">The owner context</param>
		/// <returns>The associated UltraCombo</returns>
		public override IValueList GetValueList( object ownerContext )
		{
			return this.control;
		}
		#endregion //GetValueList

        #region GetAutoEdit
        /// <summary>
        /// Indicates whether AutoEdit should enabled.
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>Indicates whether AutoEdit should enabled.</returns>
        public override bool GetAutoEdit( object ownerContext )
        {
            //  BF 2/28/08  FR09238 - AutoCompleteMode
            return this.control.AutoCompleteModeResolved == AutoCompleteMode.Append ? true : false;
        }
        #endregion //GetAutoEdit

        //  BF 2/28/08  FR09238 - AutoCompleteMode
        #region GetAutoComplete
        /// <summary>
        /// Returns the <see cref="Infragistics.Win.AutoCompleteMode"/> constant which
        /// determines the automatic completion mode for the <see cref="UltraGridComboEditor"/> editor.
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        public override AutoCompleteMode GetAutoCompleteMode(object ownerContext)
        {
            return this.control.AutoCompleteModeResolved;
        }
        #endregion GetAutoComplete

        #region IsKeyMapped
        /// <summary>
		/// Called by the editor to determine whether the specified key should be handled by the editor
		/// </summary>
		/// <param name="keyData">The key data</param>
		/// <param name="element">The EmbeddableUIElementBase-derived element</param>
        /// <returns>True if the key is used by the owner (e.g. an arrow or tab key used for internal navigation).</returns>
		public override bool IsKeyMapped( Keys keyData, EmbeddableUIElementBase element )
		{
			return this.control.InternalIsInputKey(keyData);
		}
		#endregion //IsKeyMapped

		#region GetEditor
		/// <summary>
		/// Returns the editor associated with this EmbeddableEditorOwnerBase-derived class
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override EmbeddableEditorBase GetEditor( object ownerContext )
		{
			return this.control.InternalEditor;
		}
		#endregion //	GetEditor

		#region GetSizeOfImages

		/// <summary>
		/// Returns the size of images to be used by the editor's ValueList
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="sizeOfImages">The size of the images</param>
		/// <returns>A boolean indicating whether a meaningful value was supplied</returns>
		/// <remarks>The default implementation returns <b>false</b>, with sizeOfImages set Size.Empty.</remarks>
		public override bool GetSizeOfImages( object ownerContext, out Size sizeOfImages )
		{
			sizeOfImages = this.control.DisplayLayout.SizeOfImages;

			//MRS 6/17/04 - UWG3393
			//return true;
			return (sizeOfImages.Width != 0 || sizeOfImages.Height != 0);

		}

		#endregion GetSizeOfImages

		#region ResolveAppearance	

		/// <summary>
		/// Resolves the appearance for an element.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="appearance">The appearance structure to initialize.</param>
		/// <param name="requestedProps">The appearance properties to resolve.</param>
		/// <param name="area">Enumeration describing the area of the embeddable element to which the appearance will be applied</param>
		/// <param name="hotTracking">Boolean indicating whether the owner should apply its 'HotTrackingAppearance'</param>
		/// <param name="customArea">A string that denotes which appearance to resolve. Applicable only when the 'area' parameter is set to Custom.</param>
		/// <returns>True if the owner recognizes and supports the named appearance.</returns>
		public override bool ResolveAppearance ( object ownerContext,
			ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps,
			EmbeddableEditorArea area,
			bool hotTracking,
			string customArea )
		{
			// AS 4/9/04 UWG3136
			// We should call the base impl in case there's a default owner.
			//
			//bool recognized = false;

			// this is pretty much borrowed from the UltraComboEditor's owner...
			//

			if ( area == EmbeddableEditorArea.Default || area == EmbeddableEditorArea.Text )
			{
				this.control.ResolveAppearance( ref appearance, ref requestedProps );
				return true;
			}
			else if (area == EmbeddableEditorArea.Button)
			{
				// SSP 3/14/06 - App Styling
				// Only check the UseControlInfo. The Before and After phases of style appearance setting resolution
				// will be done by the editor.
				// 
				//if (this.control.HasButtonAppearance)
				if ( this.control.HasButtonAppearance && StyleUtils.UseControlInfo( this.control.DisplayLayout ) )
					this.control.ButtonAppearance.MergeData(ref appearance, ref requestedProps);

				return true;
			}
			else
				//	BF 7.19.02	UWE36
				//	Use the custom "SelectedText" appearance, which the standalone UltraComboEditor
				//	control recognizes
				if ( area == EmbeddableEditorArea.Custom && customArea.Equals("SelectedText") )
			{
				// AS 4/9/04 UWG3136
				// Moved from above.
				bool recognized = false;

				//	First, merge in the control's Appearance, if there is one
				// SSP 3/14/06 - App Styling
				// Only check the UseControlInfo. The Before and After phases of style appearance setting resolution
				// will be done by the editor.
				// 
				//if ( this.control.HasAppearance )
				if ( this.control.HasAppearance && StyleUtils.UseControlInfo( this.control.DisplayLayout ) )
				{
					this.control.Appearance.MergeData( ref appearance, ref requestedProps );
					recognized = true;
				}

				// AS 4/9/04 UWG3136
				// Follow the UltraComboEditor in its "extended" use of the
				// return value.
				//
				if ( !this.control.Focused					|| 
					!this.MustSelectFromList(ownerContext)	||
					this.control.IsDroppedDown )
					return false;

				if ( this.control.Focused &&
					this.control.DropDownStyle == UltraComboStyle.DropDownList &&
					!this.control.IsDroppedDown &&
                    this.control.DisplayStyleResolved != EmbeddableElementDisplayStyle.WindowsVista) // MRS v7.3 - Windows Vista Style)
				{
					//	Now we are basically going to disregard the settings for
					//	BackColor and ForeColor so that we draw selected text correctly
					appearance.BackColor = SystemColors.Highlight;
					requestedProps	&= ~AppearancePropFlags.BackColor;

					appearance.ForeColor	= SystemColors.HighlightText;
					requestedProps	&= ~AppearancePropFlags.ForeColor;
					recognized = true;
				}

				// AS 4/9/04 UWG3136
				if (recognized)
					return true;
			}

			// AS 4/9/04 UWG3136
			// We should be calling the base implementation.
			return base.ResolveAppearance(ownerContext, ref appearance, ref requestedProps, area, hotTracking, customArea);
		}

		#endregion ResolveAppearance

		// AS 11/19/03 Accessibility
		#region GetEditorElement
		/// <summary>
		/// Returns the embeddable uielement associated with a particular object or null if none is available.
		/// </summary>
		/// <param name="ownerContext">Context used to identify the object to reference</param>
		/// <returns>The embeddable uielement representing the specified owner context.</returns>
		public override EmbeddableUIElementBase GetEditorElement( object ownerContext )
		{
			Infragistics.Win.UIElement element = ((IUltraControlElement)this.control).MainUIElement;

			if (element == null)
				return null;

			return element.GetDescendant( typeof(EmbeddableUIElementBase) ) as EmbeddableUIElementBase;
		}
		#endregion //GetEditorElement

		// SSP 12/10/03 UWG2785
		// Give the combo focus if it doesn't have it when it's clicked upon.
		//
		#region OnEditorMouseDown
		
		/// <summary>
		/// The editor calls this method whenever any of its embeddable elements gets a mouse down.
		/// </summary>
		/// <param name="embeddableElem"></param>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		public override void OnEditorMouseDown( EmbeddableUIElementBase embeddableElem, EmbeddableMouseDownEventArgs e )
		{
			UltraCombo combo = this.control;

			bool eatMessage = true;

			if ( null != combo && combo.Enabled && null != embeddableElem && embeddableElem.HasAncestor( combo.UIElement ) )
			{
				if ( ! combo.ContainsFocus )
				{
					try
					{
                        // MRS 12/19/06 - fxCop
                        //System.Security.Permissions.UIPermission perm = null;

                        //if ( DisposableObject.HasSamePublicKey( combo.GetType() ))
                        //{
                        //    perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

                        //    perm.Assert();
                        //}

                        //combo.Focus();

                        //// JJD 1/25/02
                        //// Revert the above security assert since we don't want to
                        //// give access when we possibly fire the dropdown event below.
                        ////
                        //if ( perm != null )
                        //    System.Security.Permissions.UIPermission.RevertAssert();
                        if (DisposableObject.HasSamePublicKey(combo.GetType()))
                            GridUtils.FocusControl(combo);
                        else
                            combo.Focus();
					}
					catch {}
				}

				eatMessage = ! combo.Enabled || ! combo.ContainsFocus;
			}

			e.EatMessage = eatMessage;
		}
		
		#endregion // OnEditorMouseDown

		// AS 12/15/03
		// There was a problem where if you cancelled the validating 
		// and clicked on an edit element, it would still dropdown.
		// This happens even when the OnEditorMouseDown indicates that 
		// the message should be eaten. Essentially, the element tries
		// to put the editor into edit mode - which succeeds even 
		// though the control does not have focus.
		//
		#region EnterEditModeOnClick

		/// <summary>
		/// If true is returned, the editor will enter edit mode on either
		/// MouseDown or MouseUp, depending on the nature of the editor
		/// </summary>
		/// <param name="ownerContext">The owner context</param>
		/// <returns>The default implemenation returns true</returns>
		public override bool EnterEditModeOnClick( object ownerContext )
		{
			return this.control.ContainsFocus;
		}

		#endregion //	EnterEditModeOnClick

		// AS 1/9/04 Accessibility
		#region AccessibilityNotifyClients
		/// <summary>
		/// Notifies the owner that an accessible event has occurred.
		/// </summary>
		/// <param name="ownerContext">Context used to identify the object to reference</param>
		/// <param name="eventId">Enumeration indicating the event that occurred.</param>
		public override void AccessibilityNotifyClients( object ownerContext, AccessibleEvents eventId )
		{
			// AS 1/16/04
			//this.control.AccessibilityNotifyClientsInternal(eventId, 0);

			int childIndex = 0;

			switch (eventId)
			{
				case AccessibleEvents.ValueChange:
					// use the selected item
					childIndex = 1;
					break;
				case AccessibleEvents.Selection:
					childIndex = 1;
					break;
			}

			this.control.AccessibilityNotifyClientsInternal(eventId, childIndex);
		}
		#endregion //AccessibilityNotifyClients

		#region GetCharacterCasing	

		// SSP 4/2/04 UWG3046
		// Added CharacterCasing property to UltraCombo.
		//
		/// <summary>
		/// Determines how the text will be cased. 
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>The CharacterCasing to be applied to the text</returns>
		public override CharacterCasing GetCharacterCasing( object ownerContext )
		{
			return null != this.control ? this.control.CharacterCasing : base.GetCharacterCasing( ownerContext );
		}

		#endregion    //GetCharacterCasing	

		// MRS 10/22/04 - UWG3633
		#region GetMaxLength
		/// <summary>
		/// Returns the maximum length for a string.
		/// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="maxLength">(out) The maximum value or 0 if there is no maximum.</param>
		/// <returns>A boolean indicating whether a meaningful value was returned.</returns>
		/// <remarks>The default implementation returns 0.</remarks>
		public override bool GetMaxLength(object ownerContext, out int maxLength)
		{
			maxLength = this.control.MaxLength;
			return true;
		}
		#endregion GetMaxLength

		#region GetTextRenderingMode

		// SSP 11/4/05 BR07555
		// Added TextRenderingMode property to UltraGrid and also the EmbeddableEditorOwnerBase.
		// Also added IsPirnting on the EmbeddableEditorOwnerBase.
		// 
		/// <summary>
		/// Returns the text rendering mode to use. Default implementation returns <b>Default</b> value.
		/// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override TextRenderingMode GetTextRenderingMode( object ownerContext )
		{
			return this.control.TextRenderingMode;
		}

		#endregion // GetTextRenderingMode

		//MRS 4/18/06 - BR11468
		#region GetNullText
		/// <summary>
		/// The string to display for a null value.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="nullText">(out) The string that should be used if the value is null or DB_Null.</param>
		/// <returns>A boolean indicating whether a meaningful value was returned.</returns>
		/// <remarks>The default implementation returns <b>false</b>.</remarks>
		public override bool GetNullText(object ownerContext, out string nullText)
		{
			nullText = this.control.NullText;
			return nullText != null && nullText.Length > 0;		
		}
		#endregion GetNullText

        //  BF NA 9.1 - UltraCombo MultiSelect
            #region GetCheckedListSettings
        /// <summary>
        /// Returns the <see cref="Infragistics.Win.EditorCheckedListSettings">EditorCheckedListSettings</see>
        /// instance which determines whether the editor's value is determined by the selected item or the checked items.
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>An <see cref="Infragistics.Win.EditorCheckedListSettings">EditorCheckedListSettings</see> instance.</returns>
        public override EditorCheckedListSettings GetCheckedListSettings(object ownerContext)
        {
            return this.control.CheckedListSettings;
        }
            #endregion GetCheckedListSettings

		#endregion //Base class overrides
	}
}
