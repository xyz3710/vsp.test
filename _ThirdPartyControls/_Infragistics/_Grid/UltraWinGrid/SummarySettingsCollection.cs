#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using Infragistics.Win;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.ComponentModel.Design;
using System.Reflection;
using System.Drawing.Design;
using System.Collections.Generic;

// SSP 4/23/02
// Added SummarySettingsCollection class for Summary Rows Feature
//

namespace Infragistics.Win.UltraWinGrid
{
	#region SummarySettingsCollection Class

	// SSP 8/15/02 UWG1435
	// Made SummarySettingsCollection keyed.
	//

	/// <summary>
	/// Collection of <see cref="SummarySettings"/> objects. See <see cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Summaries"/> for more information.
	/// </summary>
	/// <remarks>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Summaries"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummaryValue"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummaryValuesCollection"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.SummaryValues"/>
	/// </remarks>
	[ Serializable() ]
	// SSP 8/15/02 UWG1435
	//
	//public sealed class SummarySettingsCollection : SubObjectsCollectionBase, ISerializable
	public sealed class SummarySettingsCollection : KeyedSubObjectsCollectionBase, ISerializable
		// SSP 8/10/04 - Implemented design time serialization of summaries
		//
		, IList
	{
		#region Private Variables

		private const string NO_KEY_VALUE = null;

		private UltraGridBand band = null;

		// This version number is incremented whenever the collection itself is modifed
		// like an item is added or removed.
		//
		private int summariesVersion = 0;


		// This version number is incremeneted whenever something in the grid changes
		// causing all the summary values in the band to be recalculated like for example
		// group by hierarchy is modified.
		//
		// SSP 6/23/05 BR04577
		// 
		//private int summaryCalculationsVersion = 0;

		#endregion  // Private Variables

		#region Constructor

		internal SummarySettingsCollection( UltraGridBand band ) : base( )
		{
			this.Initialize( band );
		}

		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal SummarySettingsCollection( SerializationInfo info, StreamingContext context )
		private SummarySettingsCollection( SerializationInfo info, StreamingContext context )
		{
			// Everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			//
			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( Infragistics.Win.UltraWinGrid.SummarySettings ) )
				{
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					SummarySettings item = (SummarySettings)Utils.DeserializeProperty( entry, typeof(SummarySettings), null );

					// Add the item to the collection
					if ( item != null )
						this.InternalAdd( item );
				}
				else
				{
					switch ( entry.Name )
					{
						case "Tag":
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;
						default:
							Debug.Assert( false, "Invalid entry in SummarySettingsCollection de-serialization ctor" );
							break;
					}
				}
			}
		}

		#endregion // Constructor

		#region Protected properties

		#region InitialCapacity

		/// <summary>
		/// Property that specifies the initial capacity
		/// of the collection
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return 5;
			}
		}

		#endregion // InitialCapacity

		#region IsReadOnly

		/// <summary>
		/// Returns true if the collection is read-only
		/// </summary>
		/// <remarks>
        /// <seealso cref="SummarySettingsCollection.Add(string, SummaryType, UltraGridColumn, SummaryPosition)"/>
		/// <seealso cref="UltraGridBand.Summaries"/>
		/// </remarks>
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		#endregion // IsReadOnly

		#endregion // Protected properties

		#region Base Overrides

		#region OnSubObjectPropChanged

		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			this.NotifyPropChange( PropertyIds.Summaries, propChange );
		}

		#endregion // OnSubObjectPropChanged
		
		#region OnDispose

		/// <summary>
		/// Called when the object is being disposed.
		/// </summary>
		protected override void OnDispose( )
		{
			this.Clear( );

			base.OnDispose( );
		}

		#endregion // OnDispose

		#region ISerializable.GetObjectData

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			for ( int i = 0; i < this.Count; i++ )
			{
				SummarySettings summary = this[i];

				// Only do the summaries that are not custon because we can't serialize
				// ICustomSummaryCalculator instance that the user would have specified
				// for the custom summary.
				//
                // MRS 7/30/2009 - TFS20044
                // We can serialize it if it is Serializable.
                //
				//if ( null != summary && SummaryType.Custom != summary.SummaryType )                
                if (null != summary && summary.CanSerialize)
				{
					// JJD 8/20/02
					// Use SerializeProperty static method to serialize properties instead.
					Utils.SerializeProperty( info, i.ToString( ), summary );
					//info.AddValue( i.ToString( ), summary );
				}
			}
		}

		#endregion // ISerializable.GetObjectDataf

		#region ToString

		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Overrode ToString.
		//
		/// <summary>
		/// Returns an empty string so the property window displays nothing.
		/// </summary>
        /// <returns>An empty string so the property window displays nothing.</returns>
		public override string ToString( )
		{
			return string.Empty;
		}

		#endregion // ToString

		#endregion // Base Overrides

		#region Public methods

		#region indexer

		/// <summary>
		/// indexer 
		/// </summary>
		public SummarySettings this[ int index ]
		{
			get
			{
				return (SummarySettings)base.GetItem( index );
			}
		}

		// SSP 8/15/02 UWG1435
		// Made this a keyed collection.
		//
		/// <summary>
		/// indexer (by string key)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.SummarySettings this[ string key ] 
		{
			get
			{
				return (SummarySettings)base.GetItem( key );
			}
		}

		#endregion // indexer

		#region Add
		
		/// <summary>
		/// Adds a new SummarySettings object created using the supplied arguments and returns that new summary settings object.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		/// <param name="summaryType">SummaryType. With this overload, you must specify a non-Custom sumary type. Use a different overload for specifying custom summaries.</param>
		/// <param name="sourceColumn">Field that will be summarized.</param>
		/// <param name="summaryPosition">Specifies the place where the summary will be displayed in the grid.</param>
		/// <returns>The new SummarySettings object that was created and added to the collection with supplied arguments.</returns>
		/// <remarks>
		/// <p class="body">Creates a new SummarySettings object based on the passed in arguments and adds it to the collection.</p>
		/// <p class="body">If summaryType is SummaryType.Custom, then this overload will throw an exception. Use an overload that takes in an ICustomSummaryCalculator argument for specifying custom summaries.</p>
		/// <p class="body">SourceColumn is the column whose data is being summarized. The sourceColumn must be from the band associated with this SummarySettingsCollection or from one of the descendant bands.</p>
		/// <p class="body">If summaryPosition is <b>SummaryPosition.UseSummaryPositionColumn</b>, then it will be displayed under the source column.</p>
		/// </remarks>
		public SummarySettings Add( 
			string key,
			SummaryType summaryType,
			UltraGridColumn sourceColumn,
			SummaryPosition summaryPosition )
		{
			return this.Add( key, summaryType, null, sourceColumn, summaryPosition, null );
		}

		/// <summary>
		/// Adds a new SummarySettings object created using the supplied arguments and returns that new summary settings object.
		/// </summary>
		/// <param name="summaryType">SummaryType. With this overload, you must specify a non-Custom sumary type. Use a different overload for specifying custom summaries.</param>
		/// <param name="sourceColumn">Field that will be summarized.</param>
		/// <param name="summaryPosition">Specifies the place where the summary will be displayed in the grid.</param>
		/// <returns>The new SummarySettings object that was created and added to the collection with supplied arguments.</returns>
		/// <remarks>
		/// <p class="body">Creates a new SummarySettings object based on the passed in arguments and adds it to the collection.</p>
		/// <p class="body">If summaryType is SummaryType.Custom, then this overload will throw an exception. Use an overload that takes in an ICustomSummaryCalculator argument for specifying custom summaries.</p>
		/// <p class="body">SourceColumn is the column whose data is being summarized. The sourceColumn must be from the band associated with this SummarySettingsCollection or from one of the descendant bands.</p>
		/// <p class="body">If summaryPosition is <b>SummaryPosition.UseSummaryPositionColumn</b>, then it will be displayed under the source column.</p>
		/// </remarks>
		public SummarySettings Add( 
			SummaryType summaryType,
			UltraGridColumn sourceColumn,
			SummaryPosition summaryPosition )
		{
			return this.Add( NO_KEY_VALUE, summaryType, sourceColumn, summaryPosition );
		}

		/// <summary>
		/// Adds a new SummarySettings object created using the supplied arguments and returns that new summary settings object.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		/// <param name="summaryType">SummaryType. With this overload, you must specify a non-Custom sumary type. Use a different overload for specifying custom summaries.</param>
		/// <param name="sourceColumn">Field that will be summarized.</param>
		/// <returns>The new SummarySettings object that was created and added to the collection with supplied arguments.</returns>
		/// <remarks>
		/// <p class="body">Creates a new SummarySettings object based on the passed in arguments and adds it to the collection.</p>
		/// <p class="body">If summaryType is SummaryType.Custom, then this overload will throw an exception. Use an overload that takes in an ICustomSummaryCalculator argument for specifying custom summaries.</p>
		/// <p class="body">Summary will be positioned under the sourceColumn.</p>
		/// </remarks>
		public SummarySettings Add( 
			string key,
			SummaryType summaryType,
			UltraGridColumn sourceColumn )
		{
			return this.Add( key, summaryType, sourceColumn, SummaryPosition.UseSummaryPositionColumn );
		}

		/// <summary>
		/// Adds a new SummarySettings object created using the supplied arguments and returns that new summary settings object.
		/// </summary>
		/// <param name="summaryType">SummaryType. With this overload, you must specify a non-Custom sumary type. Use a different overload for specifying custom summaries.</param>
		/// <param name="sourceColumn">Field that will be summarized.</param>
		/// <returns>The new SummarySettings object that was created and added to the collection with supplied arguments.</returns>
		/// <remarks>
		/// <p class="body">Creates a new SummarySettings object based on the passed in arguments and adds it to the collection.</p>
		/// <p class="body">If summaryType is SummaryType.Custom, then this overload will throw an exception. Use an overload that takes in an ICustomSummaryCalculator argument for specifying custom summaries.</p>
		/// <p class="body">Summary will be positioned under the sourceColumn.</p>
		/// </remarks>
		public SummarySettings Add( 
			SummaryType summaryType,
			UltraGridColumn sourceColumn )
		{
			return this.Add( NO_KEY_VALUE, summaryType, sourceColumn );
		}

		/// <summary>
		/// Adds a new SummarySettings object created using the supplied arguments and returns that new summary settings object.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		/// <param name="summaryType">SummaryType. If you specify Custom, then a valid instance ICustomSummaryCalculator must be passed in as customSummaryCalculator.</param>
		/// <param name="customSummaryCalculator">Only required if SummaryType is Custom.</param>
		/// <param name="sourceColumn">Field that will be summarized.</param>
		/// <param name="summaryPosition">Specifies the place where the summary will be displayed in the grid.</param>
		/// <param name="summaryPositionColumn">Only required if summaryPosition is UseSummaryPositionColumn</param>
		/// <returns>The new SummarySettings object that was created and added to the collection with supplied arguments.</returns>
		/// <remarks>
		/// <p class="body">Creates a new SummarySettings object based on the passed in arguments and adds it to the collection.</p>
		/// <p class="body">If summaryType is SummaryType.Custom, then you must also pass in a valid instance of ICustomSummaryCalculator for customSummaryCalculator argument. If SummaryType is something other than Custom, then this parameter is not required and can be null.</p>
		/// <p class="body">If summaryPosition is <b>SummaryPosition.UseSummaryPositionColumn</b>, then it will be displayed under summaryPositionColumn column. If summaryPositionColumn is not specified, then it will be displayed under the source column. This column must be from the same band this SummaryCollection is asscocited with. If summaryPosition is something other than UseSummaryPositionColumn, then summaryPositionColumn parameter is not required and can be null.</p>
		/// <p class="body">SourceColumn is the column whose data is being summarized. The sourceColumn must be the band associated with this SummarySettingsCollection or from one of the descendant bands.</p>
		/// </remarks>
		public SummarySettings Add( 
			string key,
			SummaryType summaryType, 
			ICustomSummaryCalculator customSummaryCalculator, 
			UltraGridColumn sourceColumn, 
			SummaryPosition summaryPosition,
			UltraGridColumn summaryPositionColumn )
		{
			return this.AddHelper( key, summaryType, customSummaryCalculator, sourceColumn, summaryPosition, summaryPositionColumn, null );
		}

		/// <summary>
		/// Adds a new SummarySettings object created using the supplied arguments and returns that new summary settings object.
		/// </summary>
		/// <param name="summaryType">SummaryType. If you specify Custom, then a valid instance ICustomSummaryCalculator must be passed in as customSummaryCalculator.</param>
		/// <param name="customSummaryCalculator">Only required if SummaryType is Custom.</param>
		/// <param name="sourceColumn">Field that will be summarized.</param>
		/// <param name="summaryPosition">Specifies the place where the summary will be displayed in the grid.</param>
		/// <param name="summaryPositionColumn">Only required if summaryPosition is UseSummaryPositionColumn</param>
		/// <returns>The new SummarySettings object that was created and added to the collection with supplied arguments.</returns>
		/// <remarks>
		/// <p class="body">Creates a new SummarySettings object based on the passed in arguments and adds it to the collection.</p>
		/// <p class="body">If summaryType is SummaryType.Custom, then you must also pass in a valid instance of ICustomSummaryCalculator for customSummaryCalculator argument. If SummaryType is something other than Custom, then this parameter is not required and can be null.</p>
		/// <p class="body">If summaryPosition is <b>SummaryPosition.UseSummaryPositionColumn</b>, then it will be displayed under summaryPositionColumn column. If summaryPositionColumn is not specified, then it will be displayed under the source column. This column must be from the same band this SummaryCollection is asscocited with. If summaryPosition is something other than UseSummaryPositionColumn, then summaryPositionColumn parameter is not required and can be null.</p>
		/// <p class="body">SourceColumn is the column whose data is being summarized. The sourceColumn must be the band associated with this SummarySettingsCollection or from one of the descendant bands.</p>
		/// </remarks>
		public SummarySettings Add( 
			SummaryType summaryType, 
			ICustomSummaryCalculator customSummaryCalculator, 
			UltraGridColumn sourceColumn, 
			SummaryPosition summaryPosition,
			UltraGridColumn summaryPositionColumn )
		{
			return this.Add(
				"", // Key
				summaryType,
				customSummaryCalculator,
				sourceColumn,
				summaryPosition,
				summaryPositionColumn );
		}

		// SSP 8/2/04 - UltraCalc
		// Added overloads for adding formula summaries.
		//
		/// <summary>
		/// Adds a summary with the specified formula. This overload will display the summary in the left area of the summary footer.
		/// </summary>
		/// <param name="formula">Formula that the summary will evaluate and display the result.</param>
		/// <returns>The new SummarySettings object that was created and added to the collection with supplied arguments.</returns>
		/// <remarks>
		/// <p class="body">In order for formula summaries to work, you must assign an valid instance of UltraCalcManager to the <see cref="UltraGridBase.CalcManager"/> property. You can do that either at design time by simply adding an instance of UltraCalcManager to the form containing the grid or at runtime by setting the <see cref="UltraGridBase.CalcManager"/> property,</p>
		/// </remarks>
		public SummarySettings Add( string formula )
		{
			return this.Add( NO_KEY_VALUE, formula );
		}

		/// <summary>
		/// Adds a summary with the specified formula. This overload will display the summary in the left area of the summary footer.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		/// <param name="formula">Formula that the summary will evaluate and display the result.</param>
		/// <returns>The new SummarySettings object that was created and added to the collection with supplied arguments.</returns>
		/// <remarks>
		/// <p class="body">In order for formula summaries to work, you must assign an valid instance of UltraCalcManager to the <see cref="UltraGridBase.CalcManager"/> property. You can do that either at design time by simply adding an instance of UltraCalcManager to the form containing the grid or at runtime by setting the <see cref="UltraGridBase.CalcManager"/> property,</p>
		/// </remarks>
		public SummarySettings Add( string key, string formula )
		{
			return this.Add( key, formula, SummaryPosition.Left, null );
		}

		/// <summary>
		/// Adds a summary with the specified formula.
		/// </summary>
		/// <param name="formula">Formula that the summary will evaluate and display the result.</param>
		/// <param name="summaryPosition">Location where the summary will be displayed.</param>
		/// <param name="summaryPositionColumn">Only required if summaryPosition is UseSummaryPositionColumn.</param>
		/// <returns>The new SummarySettings object that was created and added to the collection with supplied arguments.</returns>
		/// <remarks>
		/// <p class="body">In order for formula summaries to work, you must assign an valid instance of UltraCalcManager to the <see cref="UltraGridBase.CalcManager"/> property. You can do that either at design time by simply adding an instance of UltraCalcManager to the form containing the grid or at runtime by setting the <see cref="UltraGridBase.CalcManager"/> property,</p>
		/// </remarks>
		public SummarySettings Add( string formula, SummaryPosition summaryPosition, UltraGridColumn summaryPositionColumn )
		{
			return this.Add( NO_KEY_VALUE, formula, summaryPosition, summaryPositionColumn );
		}

		/// <summary>
		/// Adds a summary with the specified formula.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		/// <param name="formula">Formula that the summary will evaluate and display the result.</param>
		/// <param name="summaryPosition">Location where the summary will be displayed.</param>
		/// <param name="summaryPositionColumn">Only required if summaryPosition is UseSummaryPositionColumn.</param>
		/// <returns>The new SummarySettings object that was created and added to the collection with supplied arguments.</returns>
		/// <remarks>
		/// <p class="body">In order for formula summaries to work, you must assign an valid instance of UltraCalcManager to the <see cref="UltraGridBase.CalcManager"/> property. You can do that either at design time by simply adding an instance of UltraCalcManager to the form containing the grid or at runtime by setting the <see cref="UltraGridBase.CalcManager"/> property,</p>
		/// </remarks>
		public SummarySettings Add( string key, string formula, SummaryPosition summaryPosition, UltraGridColumn summaryPositionColumn )
		{
			return this.AddHelper( key, SummaryType.Formula, null, null, summaryPosition, summaryPositionColumn, formula );
		}

		internal SummarySettings AddHelper( 
			string key,
			SummaryType summaryType, 
			ICustomSummaryCalculator customSummaryCalculator, 
			UltraGridColumn sourceColumn, 
			SummaryPosition summaryPosition,
			UltraGridColumn summaryPositionColumn,
			// SSP 8/2/04 - UltraCalc
			// Added formula parameter.
			//
			string formula )
		{
			SummarySettings summarySettings = new SummarySettings( 
				key,
				this, 
				summaryType, 
				customSummaryCalculator, 
				sourceColumn, 
				summaryPosition, 
				summaryPositionColumn,
				// SSP 8/2/04 - UltraCalc
				// Added formula parameter.
				//
				formula
				);

			// SSP 8/10/04 - Design time serialization of summaries.
			// Moved the following code into the new AddHelper method so we can use it
			// from IList.Insert.
			//
			// ----------------------------------------------------------------------------
			
            this.AddHelper( -1, summarySettings );
			// ----------------------------------------------------------------------------

			return summarySettings;
		}

		// SSP 8/10/04 - Design time serialization of summaries.
		//
		internal void AddHelper( int index, SummarySettings summarySettings )
		{
			if ( index >= 0 )
				this.InternalInsert( index, summarySettings );
			else
				this.InternalAdd( summarySettings );

			summarySettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// Bump the collection version number so the sumamry values collections verify
			// against this collection.
			//
			this.BumpSummariesVersion( );

			// SSP 8/10/04 - UltraCalc
			// Notify the calc manager.
			//
			summarySettings.FormulaHolder.AddReferenceToCalcNetwork( );

			this.NotifyPropChange( PropertyIds.Add );
		}

		#endregion // Add

		#region AddRange

		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Added AddRange method for serializing summaries at design time.
		//
		/// <summary>
		/// For internal use only. This method is used for serialization of summaries at design time.
		/// </summary>
		/// <param name="summaries"></param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public void AddRange( SummarySettings[] summaries )
		{
			foreach ( SummarySettings summary in summaries )
			{
				if ( null != summary )
				{
					summary.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					this.InternalAdd( summary );
				}
			}

			// SSP 10/21/04 UWG3640
			// Bump the summaries version number so the summary values off the rows collections
			// synchronize their collections with this collection.
			//
			this.BumpSummariesVersion( );
		}

		#endregion // AddRange

		#region Remove

		/// <summary>
		/// SummarySettings object to remove.
		/// </summary>
        /// <param name="summarySettings">The item to remove from the colletion.</param>        
		public void Remove( SummarySettings summarySettings )
		{
			// Unhook first.
			//
			summarySettings.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			this.InternalRemove( summarySettings );

			// SSP 8/10/04 - UltraCalc
			// Notify the calc manager.
			//
			summarySettings.FormulaHolder.RemoveReferenceFromCalcNetwork( );

			this.BumpSummariesVersion( );

			this.NotifyPropChange( PropertyIds.Remove );
		}

		#endregion // Remove

		#region RemoveAt

		/// <summary>
		/// Removes the SummarySettings object at the specified index from the collection.
		/// </summary>
        /// <param name="index">The index of the item to remove from the colletion.</param>
		public void RemoveAt( int index )
		{
			SummarySettings summarySettings = this[ index ];

			this.Remove( summarySettings );
		}

		#endregion // RemoveAt

		#region Clear

		/// <summary>
		/// Clears the collection.
		/// </summary>
		public void Clear( )
		{
			// Unhook from the summary settings objects.
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				SummarySettings summarySettings = this[i];

				if ( null != summarySettings )
				{
					summarySettings.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

					// SSP 8/10/04 - UltraCalc
					// Notify the calc manager.
					//
					summarySettings.FormulaHolder.RemoveReferenceFromCalcNetwork( );
				}
			}

			this.InternalClear( );

			this.BumpSummariesVersion( );

			this.NotifyPropChange( PropertyIds.Clear );
		}

		#endregion // Clear

		#endregion // Public methods

		#region Public Properties

		#region Band

		/// <summary>
		/// Returns the band this summary settings collection is associated with.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridBand.Summaries"/>
		/// <seealso cref="SummarySettings"/>
        /// <seealso cref="SummarySettingsCollection.Add(string, SummaryType, UltraGridColumn, SummaryPosition)"/>
		/// </remarks>
		public UltraGridBand Band
		{
			get
			{
				return this.band;
			}
		}

		#endregion // Band

		#endregion // Public Properties
		
		#region Private/Internal Methods

		#region GetFixedSummariesForLevel

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal void GetFixedSummariesForLevel( int level, ArrayList list )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal void GetFixedSummariesForLevel( SummaryDisplayAreaContext context, int level, ArrayList list )
		internal void GetFixedSummariesForLevel( SummaryDisplayAreaContext context, int level, List<SummarySettings> list )
		{
			list.Clear( );

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//if ( this.HasFixedSummaries )
			if ( this.HasFixedSummaries( context ) )
			{
				Hashtable hash = new Hashtable( this.Count );

				for ( int i = 0; i < this.Count; i++ )
				{
					SummarySettings summary = this[i];

					// Skip the hidden and invalid summaries.
					//
					// SSP 10/21/04 - Optimizations
					//
					//if ( summary.Hidden || !summary.IsValid )
					// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
					// Added ability to display summaries in group-by rows aligned with columns, 
					// summary footers for group-by row collections, fixed summary footers and
					// being able to display the summary on top of the row collection.
					// Took out the check for HiddenResolved. Instead replaced it with the
					// call to the new SummaryDisplayAreaContext class' DoesSummaryMatch.
					//
					//if ( summary.HiddenResolved )
					//	continue;
					if ( ! context.DoesSummaryMatch( summary ) )
						continue;

					UltraGridColumn summaryPositionColumn = summary.SummaryPositionColumnResolved;

					if ( null != summaryPositionColumn && summary.IsFixedSummary )
					{
						if ( !hash.Contains( summaryPositionColumn ) )						
							hash[ summaryPositionColumn ] = 1;
						else 
							hash[ summaryPositionColumn ] = 1 + (int)hash[ summaryPositionColumn ];

						if ( level == (int)hash[ summaryPositionColumn ] - 1 )
							list.Add( summary );
					}
				}
			}
		}

		#endregion // GetFixedSummariesForLevel

		#region GetFreeFormSummariesForLevel

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal void GetFreeFormSummariesForLevel( int level, ArrayList list )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal void GetFreeFormSummariesForLevel( SummaryDisplayAreaContext context, int level, ArrayList list )
		internal void GetFreeFormSummariesForLevel( SummaryDisplayAreaContext context, int level, List<SummarySettings> list )
		{
			list.Clear( );

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//if ( this.HasFreeFormSummaries )
			if ( this.HasFreeFormSummaries( context ) )
			{
				Hashtable hash = new Hashtable( this.Count );

				for ( int i = 0; i < this.Count; i++ )
				{
					SummarySettings summary = this[i];

					// Skip the hidden and invalid summaries.
					//
					// SSP 10/21/04 - Optimizations
					//
					//if ( summary.Hidden || !summary.IsValid )
					// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
					// Added ability to display summaries in group-by rows aligned with columns, 
					// summary footers for group-by row collections, fixed summary footers and
					// being able to display the summary on top of the row collection.
					// Took out the check for HiddenResolved. Instead replaced it with the
					// call to the new SummaryDisplayAreaContext class' DoesSummaryMatch.
					//
					//if ( summary.HiddenResolved )
					//	continue;
					if ( ! context.DoesSummaryMatch( summary ) )
						continue;

					if ( summary.IsFreeFormSummary )
					{
						if ( !hash.Contains( summary.SummaryPosition ) )						
							hash[ summary.SummaryPosition ] = 1;
						else 
							hash[ summary.SummaryPosition ] = 1 + (int)hash[ summary.SummaryPosition ];

						if ( level == (int)hash[ summary.SummaryPosition ] - 1 )
							list.Add( summary );
					}
				}
			}
		}

		#endregion // GetFreeFormSummariesForLevel

		#region HasFixedSummaries

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal bool HasFixedSummaries
		internal bool HasFixedSummaries( SummaryDisplayAreaContext context )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				SummarySettings summary = this[i];

				// Skip the hidden and invalid summaries.
				//
				// SSP 10/21/04 - Optimizations
				//
				//if ( summary.Hidden || !summary.IsValid )
				// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				// No need to check for HiddenResolved since the call to context's 
				// DoesSummaryMatch below takes care of this.
				//
				//if ( summary.HiddenResolved )
				//	continue;
				if ( ! context.DoesSummaryMatch( summary ) )
					continue;

				if ( SummaryPosition.UseSummaryPositionColumn == summary.SummaryPosition &&
					null != summary.SummaryPositionColumnResolved )
					return true;
			}

			return false;
		}

		#endregion // HasFixedSummaries

		#region HasFreeFormSummaries

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal bool HasFreeFormSummaries
		internal bool HasFreeFormSummaries( SummaryDisplayAreaContext context )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				SummarySettings summary = this[i];

				// Skip the hidden and invalid summaries.
				//
				// SSP 10/21/04 - Optimizations
				//
				//if ( summary.Hidden || !summary.IsValid )
				// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				// Took out the check for HiddenResolved. Instead replaced it with the
				// call to the new SummaryDisplayAreaContext class' DoesSummaryMatch.
				//
				//if ( summary.HiddenResolved )
				//	continue;
				if ( ! context.DoesSummaryMatch( summary ) )
					continue;

				if ( SummaryPosition.UseSummaryPositionColumn != summary.SummaryPosition )
					return true;
			}

			return false;
		}

		#endregion // HasFreeFormSummaries

		#region HasSummaryVisibleInFooter

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal bool HasSummaryVisibleInFooter
		internal bool HasSummaryVisibleInFooter( SummaryDisplayAreaContext context )
		{
			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//return this.HasFixedSummaries || this.HasFreeFormSummaries;
			return this.HasFixedSummaries( context ) || this.HasFreeFormSummaries( context );
		}

		#endregion // HasSummaryVisibleInFooter

		#region GetFixedSummariesForDisplayColumn

		// SSP 8/8/03 - Row Layout Functionality - Multiple Summaries
		//
		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal SummarySettings[] GetFixedSummariesForDisplayColumn( UltraGridColumn displayColumn )
		internal SummarySettings[] GetFixedSummariesForDisplayColumn( SummaryDisplayAreaContext context, UltraGridColumn displayColumn )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList retList = new ArrayList( );
			//ArrayList list = new ArrayList( );
			List<SummarySettings> retList = new List<SummarySettings>();
			List<SummarySettings> list = new List<SummarySettings>();

			bool summaryFound;

			int level = 0;
			do
			{
				list.Clear( );
				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				//
				//this.GetFixedSummariesForLevel( level++, list );
				this.GetFixedSummariesForLevel( context, level++, list );

				summaryFound = false;
				for ( int i = 0; i < list.Count; i++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//SummarySettings summary = (SummarySettings)list[i];
					SummarySettings summary = list[ i ];

					// SSP 10/21/04 - Optimizations
					// No need to check for hidden since GetFixedSummariesForLevel already excludes 
					// hidden summaries.
					//
					//if ( ! summary.Hidden && summary.SummaryPositionColumnResolved == displayColumn )
					if ( summary.SummaryPositionColumnResolved == displayColumn )
					{
						retList.Add( summary );
						summaryFound = true;
					}
				}
			}
			while( summaryFound );

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (SummarySettings[])retList.ToArray( typeof( SummarySettings ) );
			return retList.ToArray();
		}

		#endregion // GetFixedSummariesForDisplayColumn
		
		#region Commented Out Code

		

		#endregion // Commented Out Code

		#region FindSummary

		internal SummarySettings FindSummary( UltraGridColumn sourceColumn, SummaryType summaryType, bool onlyFixed )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				SummarySettings summary = this[i];

				if ( ( !onlyFixed || SummaryPosition.UseSummaryPositionColumn == summary.SummaryPosition  ) && sourceColumn == summary.SourceColumn && summaryType == summary.SummaryType )
					return summary;
			}

			return null;
		}

		// SSP 7/27/04 - UltraCalc
		//
		internal SummarySettings FindSummary( UltraGridColumn sourceColumn )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				SummarySettings summary = this[i];
				if ( sourceColumn == summary.SourceColumn )
					return summary;
			}

			return null;
		}

		#endregion // FindSummary

		#region BumpSummariesVersion

		internal void BumpSummariesVersion( )
		{
			this.summariesVersion++;

			// SSP 7/11/06 BR14108
			// 
			UltraGridLayout layout = this.Layout;
			if ( null != layout )
				layout.BumpCellChildElementsCacheVersion( );
		}
		
		#endregion // BumpSummariesVersion

		#region BumpSummaryCalculationsVersion

		internal void BumpSummaryCalculationsVersion( )
		{
			// SSP 6/23/05 BR04577
			// 
			//this.summaryCalculationsVersion++;
			for ( int i = 0, count = this.Count; i < count; i++ )
			{
				SummarySettings summary = this[i];
				if ( null != summary )
					summary.BumpSummaryCalculationsVersion( );
			}
		}

		#endregion // BumpSummaryCalculationsVersion

		#region DirtySummarySettings

		internal bool DirtySummarySettings( UltraGridBand band )
		{
			bool ret = false;

			for ( int i = 0; i < this.Count; i++ )
			{
				SummarySettings summary = this[i];

				UltraGridBand sourceColumnBand =
					null != summary.SourceColumn ? summary.SourceColumn.Band : null;

				if ( sourceColumnBand == band || sourceColumnBand.IsDescendantOfBand( band ) )
				{
					summary.BumpSummaryCalculationsVersion( );
					ret = true;
				}
			}

			return ret;
		}

		#endregion // DirtySummarySettings

		#region InitializeFrom

		internal void InitializeFrom( SummarySettingsCollection summarySettings, PropertyCategories categories )
		{
			// If the property categories say no, then don't initialize from
			// the source.
			//
			if ( 0 == ( PropertyCategories.Summaries & categories ) )
				return;

			this.Clear( );

			Debug.Assert( null != this.Band && null != this.Band.Layout, "No band or layout !" );

			// We are dependent in below code on having valid band and layout.
			//
			if ( null == this.Band || null == this.Band.Layout )
				return;

			for ( int i = 0; i < summarySettings.Count; i++ )
			{
                SummarySettings summary = summarySettings[i];

				// We only want to initialize from the summary object if the source column 
				// of that summary object is a valid column for our layout.
				//
				bool okToAdd = false;
				UltraGridColumn sourceColumn = summary.SourceColumn;				

				string sourceColumnKey = null != sourceColumn ? sourceColumn.Key : null;
				string sourceColumnBandKey = null != sourceColumn && null != sourceColumn.Band ? sourceColumn.Band.Key : null;

				if ( null == sourceColumnBandKey &&
					null != summary.InternalSerializedSourceColumnID && 
					null != summary.InternalSerializedSourceColumnBandKey )
				{
					sourceColumnBandKey = summary.InternalSerializedSourceColumnBandKey;
					sourceColumnKey = summary.InternalSerializedSourceColumnID.Key;
				}

				if ( null != sourceColumnBandKey && null != sourceColumnKey )
				{
					UltraGridBand sourceColumnBand = null;

					// SSP 7/26/02 UWG1367
					// If the key is null or empty string, then it must have been band 0 because
					// bands other than band 0 always have keys.
					//
                    // MBS 6/16/09 - TFS18498
                    // We shouldn't be using the SortedBands here, since they're not initialized at this point and
                    // really aren't necessary for copying summaries
                    //
                    //if (this.Band.Layout.SortedBands.Exists(sourceColumnBandKey))
                    //    sourceColumnBand = this.Band.Layout.SortedBands[sourceColumnBandKey];
                    //else if (null == sourceColumnBandKey || sourceColumnBandKey.Length <= 0)
                    //    sourceColumnBand = this.Band.Layout.SortedBands[0];
                    if (this.Band.Layout.Bands.Exists(sourceColumnBandKey))
                        sourceColumnBand = this.Band.Layout.Bands[sourceColumnBandKey];
                    else if (null == sourceColumnBandKey || sourceColumnBandKey.Length <= 0)
                        sourceColumnBand = this.Band.Layout.Bands[0];

					if ( null != sourceColumnBand && sourceColumnBand.Columns.Exists( sourceColumnKey ) &&
						// Also make sure the index of the band matches.
						//
						// SSP 9/3/02 UWG1623
						// Only compare if the InternalSerializedSourceColumnBandIndex is not -1.
						//
						//sourceColumnBand.Index == summary.InternalSerializedSourceColumnBandIndex
						( summary.InternalSerializedSourceColumnBandIndex < 0 ||
						sourceColumnBand.Index == summary.InternalSerializedSourceColumnBandIndex ) )
					{
						sourceColumn = sourceColumnBand.Columns[ sourceColumnKey ];

						if ( null != sourceColumn.Band && 
							( sourceColumn.Band == this.Band || sourceColumn.Band.IsDescendantOfBand( this.Band ) ) )
							okToAdd = true;
					}
				}
				// SSP 8/2/04 - UltraCalc
				// Also add the summary if the summary type is Formula because formula summaries
				// do not need any source columns.
				//
				// ------------------------------------------------------------------------------
				else if ( SummaryType.Formula == summary.SummaryType )
				{
					okToAdd = true;
				}
				// ------------------------------------------------------------------------------

				UltraGridColumn positionColumn = summary.SummaryPositionColumn;

				if ( null == positionColumn && null != summary.InternalSerializedSummaryPositionColumnId )
					positionColumn = this.Band.GetMatchingColumn( summary.InternalSerializedSummaryPositionColumnId );

				if ( okToAdd && null != positionColumn )
				{
					if ( this.Band.Columns.Exists( positionColumn.Key ) )
						positionColumn = this.Band.Columns[ positionColumn.Key ];
					else 
						okToAdd = false;
				}


				if ( okToAdd )
				{
					SummarySettings newSummary = this.AddHelper(
						summary.Key,
						summary.SummaryType,
						summary.CustomSummaryCalculator,
						sourceColumn,
						summary.SummaryPosition,
						positionColumn,
						// SSP 8/2/04 - UltraCalc
						// Also copy over the formula property.
						//
						summary.Formula );

					// Now initialize the rest of the settings on the summary by calling
					// InitializeFrom from the summary.
					//
					newSummary.InternalInitializeFrom( summary );
				}
			}
		}

		#endregion // InitializeFrom

		#region Initialize
		
		private void Initialize( UltraGridBand band )
		{
			if ( null == band )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_104"), Shared.SR.GetString("LE_ArgumentNullException_319") );

			this.band = band;
		}

		#endregion // Initialize

		#region OnDeserializationComplete
		
		// SSP 10/21/04 UWG3640
		// Added OnDeserializationComplete so the summaries can deseiralize the serialized
		// column ids when the deserialization is complete.
		//
		internal void OnDeserializationComplete( UltraGridBand band )
		{
			this.Initialize( band );
			
			foreach ( SummarySettings summary in this )
				summary.OnDeserializationComplete( this );
		}

		#endregion // OnDeserializationComplete

		#region CreateSummaryRow

		// SSP 6/23/05 BR04635 BR04640
		// 
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private UltraGridSummaryRow CreateSummaryRow( RowsCollection parentRows, SummaryDisplayAreaContext summaryDisplayAreaContext, UltraGridRow[] rowsToRecycle )
		private static UltraGridSummaryRow CreateSummaryRow( RowsCollection parentRows, SummaryDisplayAreaContext summaryDisplayAreaContext, UltraGridRow[] rowsToRecycle )
		{
			if ( null != rowsToRecycle )
			{
				for ( int i = 0; i < rowsToRecycle.Length; i++ )
				{
					UltraGridSummaryRow summaryRow = rowsToRecycle[i] as UltraGridSummaryRow;
					if ( null != summaryRow && summaryRow.ParentCollection == parentRows )
					{
						summaryRow.Initialize( summaryDisplayAreaContext );
						rowsToRecycle[i] = null;
						return summaryRow;
					}
				}
			}

			return new UltraGridSummaryRow( parentRows, summaryDisplayAreaContext );
		}

		#endregion // CreateSummaryRow

		#region GetSummaryRows

		// SSP 7/15/05 FR01423
		// Added the ability to show only the group-by row footers and only the top level footers.
		// 
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private void GetSummaryRow( ArrayList list, bool top, bool isFixed, RowsCollection parentRows, UltraGridRow[] rowsToRecycle )
		private void GetSummaryRow( List<UltraGridRow> list, bool top, bool isFixed, RowsCollection parentRows, UltraGridRow[] rowsToRecycle )
		{
			bool isGroupByRows = parentRows.IsGroupByRows;
			SummaryDisplayAreaContext context = null;
			
			// SSP 5/9/05 BR03824
			// Card-rows do not support summary footers yet.
			//
			if ( ! isGroupByRows && parentRows.IsCardRows )
				return;

			SummaryDisplayAreas matchAll  = top 
				? ( isFixed ? SummaryDisplayAreas.TopFixed : SummaryDisplayAreas.Top ) 
				: ( isFixed ? SummaryDisplayAreas.BottomFixed : SummaryDisplayAreas.Bottom );

			SummaryDisplayAreas matchAny  = SummaryDisplayAreas.None;
			SummaryDisplayAreas matchNone = SummaryDisplayAreas.None;
			
			bool isTopLevel = parentRows.IsTopLevel;

			// If the rows collection is not top level then exclude any summaries that have 
			// the RootRowsFootersOnly in their summary display area.
			// 
			if ( ! isTopLevel )
				matchNone |= SummaryDisplayAreas.RootRowsFootersOnly;

			// If the rows collection is group-by rows collection then include any summaries 
			// that have GroupByRowsFooter in their summary display area.
			// 
			if ( isGroupByRows )
				matchAny |= SummaryDisplayAreas.GroupByRowsFooter;
			else
				// If the rows are data rows then exclude any summaries that have HideDataRowFooters
				// in their summary display area.
				// 
				matchNone |= SummaryDisplayAreas.HideDataRowFooters;

			// If the rows collection is top level then include any summaries that have
			// RootRowsFootersOnly in their summary display area. Also only do so if 
			// matchAny is non-zero.
			// 
			if ( isTopLevel && 0 != matchAny )
				matchAny |= SummaryDisplayAreas.RootRowsFootersOnly;

			SummaryDisplayAreas displayArea = matchAll;
			context = new SummaryDisplayAreaContext( matchAny, matchAll, matchNone, displayArea );

			if ( this.HasSummaryVisibleInFooter( context ) )
				// SSP 6/23/05 BR04635 BR04640
				// 
				//list.Add( new UltraGridSummaryRow( parentRows, context ) );
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//list.Add( this.CreateSummaryRow( parentRows, context, rowsToRecycle ) );
				list.Add( SummarySettingsCollection.CreateSummaryRow( parentRows, context, rowsToRecycle ) );
		}


		// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		// SSP 6/23/05 BR04635 BR04640
		// 
		//internal void GetSummaryRows( ArrayList list, bool top, RowsCollection parentRows )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal void GetSummaryRows( ArrayList list, bool top, RowsCollection parentRows, UltraGridRow[] rowsToRecycle )
		internal void GetSummaryRows( List<UltraGridRow> list, bool top, RowsCollection parentRows, UltraGridRow[] rowsToRecycle )
		{
			// SSP 7/15/05 FR01423
			// Added the ability to show only the group-by row footers and only the top level footers.
			// Added GetSummaryRow helper method and moved the following commented out code into it.
			// 
			// If top then get the fixed and then non-fixed row. Bottom it's the opposite.
			// 
			this.GetSummaryRow( list, top, top, parentRows, rowsToRecycle );
			this.GetSummaryRow( list, top, ! top, parentRows, rowsToRecycle );
			
		}

		#endregion // GetSummaryRows

		#region GetLeftOfLeftMostSummary

        // MRS - NAS 9.2 - Inline Summaries in GroupByRows for DocumentExporter
        // Added this new overload for the DocumentExporter. 
        //
        /// <summary>
        /// Gets the left of the left-most summary relative to the band's origin that
        /// includes the row selectors but not the pre row area.
        /// </summary>
        /// <param name="summaryRowContext"></param>
        /// <returns></returns>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public int GetLeftOfLeftMostSummary(UltraGridSummaryRow summaryRowContext)
        {
            SummaryDisplayAreaContext context = null != summaryRowContext
                 ? summaryRowContext.SummaryDisplayAreaContext
                 : new SummaryDisplayAreaContext(SummaryDisplayAreas.Bottom);

            return this.GetLeftOfLeftMostSummary(context);
        }  

		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		internal int GetLeftOfLeftMostSummary( SummaryDisplayAreaContext context )
		{
			int leftMost = int.MaxValue;
			int bandOrigin = this.Band.GetOrigin( BandOrigin.RowSelector );
			for ( int i = 0, count = this.Count; leftMost > bandOrigin && i < count; i++ )
			{
				SummarySettings summary = this[ i ];

				if ( ! context.DoesSummaryMatch( summary ) )
					continue;

				if ( summary.IsFreeFormSummary )
				{
					int bandExtent = summary.Band.GetExtent( BandOrigin.RowSelector );

					switch ( summary.SummaryPosition )
					{
						case SummaryPosition.Left:
							leftMost = Math.Min( leftMost, bandOrigin );
							break;
						case SummaryPosition.Center:
							leftMost = Math.Min( leftMost, bandOrigin + bandExtent / 3 );
							break;
						case SummaryPosition.Right:
							leftMost = Math.Min( leftMost, bandOrigin + 2 * bandExtent / 3 );
							break;
						default:
							Debug.Assert( false );
							break;
					}
				}
				else // Fixed summaries
				{
					UltraGridColumn column = summary.SummaryPositionColumnResolved;
					Debug.Assert( null != column );
					if ( null != column )
					{
						// SSP 7/10/06 BR13844
						// 
						// ----------------------------------------------------------------
						//leftMost = Math.Min( leftMost, column.Header.Origin );
						int x = column.Header.Origin;

						// MD 1/21/09 - Groups in RowLayouts
						// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
						//if ( null != column.Group && column.Band.GroupsDisplayed )
						//    x += column.Group.Header.Origin;
						if ( null != column.GroupResolved && column.Band.GroupsDisplayed )
							x += column.GroupResolved.Header.Origin;

						leftMost = Math.Min( leftMost, x );
						// ----------------------------------------------------------------
					}
				}
			}

			return leftMost;
		}

		#endregion // GetLeftOfLeftMostSummary

		#endregion // Private/Internal Methods/Properties

		#region Private/Internal Properties

		#region Layout
		
		internal UltraGridLayout Layout
		{
			get
			{
				return null != this.Band ? this.Band.Layout : null;
			}
		}

		#endregion // Layout

		#region SummariesVersion

		internal int SummariesVersion
		{
			get
			{
				return this.summariesVersion;
			}
		}
		
		#endregion // SummariesVersion

		#region SummaryCalculationsVersion

		// SSP 6/23/05 BR04577
		// 
		

		#endregion // SummaryCalculationsVersion

		#endregion // Private/Internal Methods/Properties

		#region IList Implementation

		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Implemented IList interface so summaries can be added/removed through the
		// .NET collection editor.
		//
		void IList.Insert( int index, object value )
		{
			if ( index < 0 || index > this.Count )
				throw new ArgumentOutOfRangeException( "index" );

			SummarySettings summary = (SummarySettings)value;

			if ( null != summary.ParentCollection && this != summary.ParentCollection )
				throw new ArgumentException( "You can't add a summary from a different collection.", "value" );

			summary.Initialize( this ); 

			if ( ! this.Contains( summary ) )
				this.AddHelper( index, summary );
		}

		void IList.Remove( object value )
		{
			this.Remove( (SummarySettings)value );
		}

		int IList.Add( object value )
		{
			int index = this.Count;
			((IList)this).Insert( index, value );
			return index;
		}

		object IList.this[ int index ]
		{
			get
			{
				return this[ index ];
			}
			set
			{
				throw new NotSupportedException( );
			}
		}

		bool IList.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		bool IList.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		#endregion // IList Implementation
	}

	#endregion // SummarySettingsCollection Class
}
