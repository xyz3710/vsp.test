#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Runtime.Serialization;
using Infragistics.Shared.Serialization;

namespace Infragistics.Win.UltraWinGrid
{
    /// <summary>
    /// Defines a class for providing custom operands for filtering values.  This class is meant
    /// to provide additional context for filtering that cannot simply be achieved with a
    /// FilterComparisonOperator and a value.
    /// </summary>
    /// <remarks>
    /// <p class="note">
    /// <b>Note: </b>If this operand is used for filtering on a layout that needs to be serialized,
    /// then the derived operand needs to implement the <see cref="ISerializable"/> interface.
    /// </p>
    /// </remarks>
    [Serializable()]
    public abstract class SpecialFilterOperand
        : ISerializable
    {
        #region Members

        private string name;
        private string displayText;

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the operand.
        /// </summary>
        /// <param name="name">The name of the operand.  This will also be the default display text.</param>
        public SpecialFilterOperand(string name)
            : this(name, null)
        {            
        }

        /// <summary>
        /// Initializes a new instance of the operand.
        /// </summary>
        /// <param name="name">The name of the operand..</param>
        /// <param name="displayText">The text displayed to the user when the operand is shown in a menu.</param>
        public SpecialFilterOperand(string name, string displayText)
        {
            this.name = name;
            this.displayText = displayText;
        }

        /// <summary>
        /// Constructor used for deserialization.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The streaming context.</param>
        protected SpecialFilterOperand(SerializationInfo info, StreamingContext context)
        {
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case "name":
                        this.name = Utils.DeserializeProperty(entry, typeof(string), String.Empty).ToString();                        
                        break;

                    case "displayText":
                        this.displayText = Utils.DeserializeProperty(entry, typeof(string), String.Empty).ToString();
                        break;
                }
            }
        }

        #endregion //Constructor

        #region Properties

        #region Name

        /// <summary>
        /// Returns the name of the operand.
        /// </summary>
        public string Name
        {
            get { return this.name; }
        }
        #endregion //Name

        #region DisplayText

        /// <summary>
        /// Gets or sets the text that is displayed to the user when the operand is shown in a menu.
        /// </summary>
        public string DisplayText
        {
            get { return this.displayText ?? this.name;  }
            set
            {
                if (this.displayText == value)
                    return;

                this.displayText = value;
            }
        }
        #endregion //DisplayText

        #endregion //Properties

        #region Abstract/Virtual Methods

        #region GetObjectData

        /// <summary>
        /// Invoked during the serialization of the object.
        /// </summary>
        /// <param name="info">SerializationInfo</param>
        /// <param name="context">StreamingContext</param>        
        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
        }
        #endregion //GetObjectData

        #region Match

        /// <summary>
        /// Indicates whether the specified value matches the operand given the specified comparison operator.
        /// </summary>
        /// <param name="comparisonOperator">The operator to be used for comparing the value to the operand.</param>
        /// <param name="value">The value that should be compared to the operand.</param>
        /// <returns>True if the value matches the condition imposed by the operand and the operator.</returns>
        public virtual bool Match(FilterComparisionOperator comparisonOperator, object value)
        {            
            if (this.SupportsOperator(comparisonOperator) == false || 
                (value != null && this.SupportsDataType(value.GetType()) == false))
                return false;

            return true;
        }
        #endregion //Match

        #region SupportsDataType

        /// <summary>
        /// Indicates whether the operand supports filtering values of the specified type.
        /// </summary>
        /// <param name="dataType">The type of values to check for support.</param>
        /// <returns>True if the operand is capable of performing filtering on values of the specified type.</returns>
        public abstract bool SupportsDataType(Type dataType);

        #endregion //SupportsDataType

        #region SupportsOperator

        /// <summary>
        /// Indicates whether the operand is capable of performing filtering using the specified operator.
        /// </summary>
        /// <param name="comparisonOperator">The operator to check for support within the operand.</param>
        /// <returns>True if the operand is capable of performing filtering with the specified operator.</returns>
        public abstract bool SupportsOperator(FilterComparisionOperator comparisonOperator);

        #endregion //SupportsOperator

        #endregion //Abstract Methodd

        #region Base Class Overrides

        #region ToString

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>The name of the operand, if provided.  Otherwise, the base object representation.</returns>
        public override string ToString()
        {
            return String.IsNullOrEmpty(this.Name) ? base.ToString() : this.Name;
        }
        #endregion //ToString

        #endregion //Base Class Overrides

        #region ISerializable Members

        /// <summary>
        /// Serialize the operand.
        /// </summary>
        /// <param name="info">SerializationInfo</param>
        /// <param name="context">StreamingContext</param>   
        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, Flags = System.Security.Permissions.SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if(String.IsNullOrEmpty(this.Name) == false)
                Utils.SerializeProperty(info, "name", this.Name);

            // Serialize the member variable since the property will return the name
            if(String.IsNullOrEmpty(this.displayText) == false)
                Utils.SerializeProperty(info, "displayText", this.displayText);

            this.GetObjectData(info, context);
        }

        #endregion
    }

    internal class DateOperand : SpecialFilterOperand
    {
        #region Constructor

        public DateOperand(string name, string displayName)
        : base(name, displayName)
        {
        }

        /// <summary>
        /// Constructor used for deserialization.
        /// </summary>
        /// <param name="info">The serialization information.</param>
        /// <param name="context">The streaming context.</param>
        protected DateOperand(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion //Constructor

        #region Base Class Overrides

        #region Match

        public override bool Match(FilterComparisionOperator comparisonOperator, object value)
        {
            if (base.Match(comparisonOperator, value) == false)
                return false;

            // Excel doesn't consider null values to match dates
            if (value == null || value == DBNull.Value || Utilities.GetUnderlyingType(value.GetType()) != typeof(DateTime))
                return false;

            return true;
        }
        #endregion //Match

        #region SupportsDataType

        public override bool SupportsDataType(Type dataType)
        {
            return Utilities.GetUnderlyingType(dataType) == typeof(DateTime);
        }
        #endregion //SupportsDataType

        #region SupportsOperator

        public override bool SupportsOperator(FilterComparisionOperator comparisonOperator)
        {
            switch (comparisonOperator)
            {
                case FilterComparisionOperator.Equals:
                case FilterComparisionOperator.NotEquals:
                case FilterComparisionOperator.LessThan:
                case FilterComparisionOperator.LessThanOrEqualTo:
                case FilterComparisionOperator.GreaterThan:
                case FilterComparisionOperator.GreaterThanOrEqualTo:
                    return true;
            }

            return false;
        }
        #endregion //SupportsOperator

        #endregion //Base Class Overrides
    }

    internal class RelativeDateOperand : DateOperand
    {
        #region RelativeDate - Enum

        internal enum RelativeDate
        {
            Tomorrow = 0,
            Today,
            Yesterday,
            NextWeek,
            ThisWeek,
            LastWeek,
            NextMonth,
            ThisMonth,
            LastMonth,
            NextQuarter,
            ThisQuarter,
            LastQuarter,
            NextYear,
            ThisYear,
            LastYear,
            YearToDate,
        }
        #endregion //RelativeDate - Enum

        #region Members

        private bool useCustomCurrentDate;
        private DateTime customCurrentDate;
        private RelativeDate relativeDate;
        private DateTime cachedCurrentDate;
        private DateTime cachedLastWeekStart;
        private DateTime cachedLastWeekEnd;
        private DateTime cachedLastMonthStart;
        private DateTime cachedLastMonthEnd;
        private DateTime cachedLastQuarterStart;
        private DateTime cachedLastQuarterEnd;
        private DateTime cachedLastYearStart;
        private DateTime cachedLastYearEnd;
        private DateTime cachedThisWeekStart;
        private DateTime cachedThisWeekEnd;
        private DateTime cachedThisMonthStart;
        private DateTime cachedThisMonthEnd;
        private DateTime cachedThisQuarterStart;
        private DateTime cachedThisQuarterEnd;
        private DateTime cachedThisYearStart;
        private DateTime cachedThisYearEnd;
        private DateTime cachedNextWeekStart;
        private DateTime cachedNextWeekEnd;
        private DateTime cachedNextMonthStart;
        private DateTime cachedNextMonthEnd;
        private DateTime cachedNextQuarterStart;
        private DateTime cachedNextQuarterEnd;
        private DateTime cachedNextYearStart;
        private DateTime cachedNextYearEnd;
        private DateTime cachedYearToDateStart;

        #endregion //Members

        #region Constuctor

        public RelativeDateOperand(string name, string displayText, RelativeDate relativeDate)
            : base(name, displayText)
        {
            this.relativeDate = relativeDate;
        }

        protected RelativeDateOperand(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case "relativateDate":
                        this.relativeDate = (RelativeDate)Utils.DeserializeProperty(entry, typeof(RelativeDate), RelativeDate.Today);
                        break;
                }
            }
        }
        #endregion //Constructor

        #region Methods

        #region CalculateQuarterRange

        private static void CalculateQuarterRange(DateTime dateInQuarter, ref DateTime cachedStart, ref DateTime cachedEnd)
        {
            if (dateInQuarter.Month < 4)
            {
                cachedStart = new DateTime(dateInQuarter.Year, 1, 1);
                cachedEnd = new DateTime(dateInQuarter.Year, 3, DateTime.DaysInMonth(dateInQuarter.Year, 3));
            }
            else if (dateInQuarter.Month < 7)
            {
                cachedStart = new DateTime(dateInQuarter.Year, 4, 1);
                cachedEnd = new DateTime(dateInQuarter.Year, 6, DateTime.DaysInMonth(dateInQuarter.Year, 6));
            }
            else if (dateInQuarter.Month < 10)
            {
                cachedStart = new DateTime(dateInQuarter.Year, 7, 1);
                cachedEnd = new DateTime(dateInQuarter.Year, 9, DateTime.DaysInMonth(dateInQuarter.Year, 9));
            }
            else
            {
                cachedStart = new DateTime(dateInQuarter.Year, 10, 1);
                cachedEnd = new DateTime(dateInQuarter.Year, 12, DateTime.DaysInMonth(dateInQuarter.Year, 12));
            }
        }
        #endregion //CalculateQuarterRange

        #region MatchHelper

        private static bool MatchHelper(FilterComparisionOperator comparisonOperator, DateTime rangeStart, DateTime rangeEnd, DateTime comparisonValue)
        {
            // Sanity check to make sure that the range is valid
            if(rangeStart > rangeEnd)
            {                
                DateTime temp = rangeEnd;
                rangeEnd = rangeStart;
                rangeStart = temp;
            }

            switch (comparisonOperator)
            {
                case FilterComparisionOperator.Equals:
                    return rangeStart <= comparisonValue && rangeEnd >= comparisonValue;

                case FilterComparisionOperator.NotEquals:
                    return false == MatchHelper(FilterComparisionOperator.Equals, rangeStart, rangeEnd, comparisonValue);

                case FilterComparisionOperator.LessThan:
                    return comparisonValue < rangeStart;

                case FilterComparisionOperator.LessThanOrEqualTo:
                    // When a value is equal to a relative date/month/etc, it can span
                    // a range of dates, so we need to check both start and end
                    return comparisonValue < rangeStart || 
                        MatchHelper(FilterComparisionOperator.Equals, rangeStart, rangeEnd, comparisonValue);

                case FilterComparisionOperator.GreaterThan:
                    return comparisonValue > rangeEnd;

                case FilterComparisionOperator.GreaterThanOrEqualTo:
                    // When a value is equal to a relative date/month/etc, it can span
                    // a range of dates, so we need to check both start and end
                    return comparisonValue > rangeEnd ||
                        MatchHelper(FilterComparisionOperator.Equals, rangeStart, rangeEnd, comparisonValue);
            }

            Debug.Fail("We should not be trying to match an unsupported FilterComparisonOperator");
            return false;
        }
        #endregion //MatchHelper

        #region ResetCurrentDate

        private void ResetCurrentDate()
        {
            this.useCustomCurrentDate = false;
            this.cachedCurrentDate = DateTime.MinValue;
        }
        #endregion //ResetCurrentDate

        #region SetCurrentDate

        private void SetCurrentDate(DateTime currentDate)
        {
            this.useCustomCurrentDate = true;
            this.customCurrentDate = currentDate;
        }
        #endregion //SetCurrentDate

        #region VerifyCache

        private void VerifyCache()
        {
            if ((this.useCustomCurrentDate && this.cachedCurrentDate == this.customCurrentDate) ||
                (!this.useCustomCurrentDate && this.cachedCurrentDate == DateTime.Today))               
                return;

            // The custom dates are only meant to be used for unit testing
            if (this.useCustomCurrentDate)
                this.cachedCurrentDate = this.customCurrentDate;
            else
                this.cachedCurrentDate = DateTime.Today;

            int daysInWeek = Enum.GetValues(typeof(DayOfWeek)).Length;
            switch (this.relativeDate)
            {
                case RelativeDate.LastWeek:
                    DateTime dateInLastWeek = this.cachedCurrentDate.AddDays(-1 * daysInWeek);
                    if (dateInLastWeek.DayOfWeek > 0)
                        dateInLastWeek = dateInLastWeek.AddDays(-1 * Convert.ToInt32(dateInLastWeek.DayOfWeek));

                    this.cachedLastWeekStart = dateInLastWeek;
                    this.cachedLastWeekEnd = dateInLastWeek.AddDays(daysInWeek - 1);
                    break;

                case RelativeDate.ThisWeek:
                    DateTime dateInThisWeek = this.cachedCurrentDate;
                    if(dateInThisWeek.DayOfWeek > 0)
                        dateInThisWeek = dateInThisWeek.AddDays(-1 * Convert.ToInt32(dateInThisWeek.DayOfWeek));
                    
                    this.cachedThisWeekStart = dateInThisWeek;
                    this.cachedThisWeekEnd = dateInThisWeek.AddDays(daysInWeek - 1);
                    break;

                case RelativeDate.NextWeek:
                    DateTime dateInNextWeek = this.cachedCurrentDate.AddDays(daysInWeek);
                    if (dateInNextWeek.DayOfWeek > 0)
                        dateInNextWeek = dateInNextWeek.AddDays(-1 * Convert.ToInt32(dateInNextWeek.DayOfWeek));

                    this.cachedNextWeekStart = dateInNextWeek;
                    this.cachedNextWeekEnd = dateInNextWeek.AddDays(daysInWeek - 1);
                    break;

                case RelativeDate.LastMonth:
                    DateTime lastMonth = this.cachedCurrentDate.AddMonths(-1);
                    this.cachedLastMonthStart = new DateTime(lastMonth.Year, lastMonth.Month, 1);
                    this.cachedLastMonthEnd = new DateTime(lastMonth.Year, lastMonth.Month, DateTime.DaysInMonth(lastMonth.Year, lastMonth.Month));
                    break;

                case RelativeDate.ThisMonth:
                    DateTime thisMonth = this.cachedCurrentDate;
                    this.cachedThisMonthStart = new DateTime(thisMonth.Year, thisMonth.Month, 1);
                    this.cachedThisMonthEnd = new DateTime(thisMonth.Year, thisMonth.Month, DateTime.DaysInMonth(thisMonth.Year, thisMonth.Month));
                    break;

                case RelativeDate.NextMonth:
                    DateTime nextMonth = this.cachedCurrentDate.AddMonths(1);
                    this.cachedNextMonthStart = new DateTime(nextMonth.Year, nextMonth.Month, 1);
                    this.cachedNextMonthEnd = new DateTime(nextMonth.Year, nextMonth.Month, DateTime.DaysInMonth(nextMonth.Year, nextMonth.Month));
                    break;

                case RelativeDate.LastQuarter:
                    DateTime dateInLastQuarter = this.cachedCurrentDate.AddMonths(-3);
                    RelativeDateOperand.CalculateQuarterRange(dateInLastQuarter, ref this.cachedLastQuarterStart, ref this.cachedLastQuarterEnd);
                    break;

                case RelativeDate.ThisQuarter:
                    RelativeDateOperand.CalculateQuarterRange(this.cachedCurrentDate, ref this.cachedThisQuarterStart, ref this.cachedThisQuarterEnd);
                    break;

                case RelativeDate.NextQuarter:
                    DateTime dateInNextQuarter = this.cachedCurrentDate.AddMonths(3);
                    RelativeDateOperand.CalculateQuarterRange(dateInNextQuarter, ref this.cachedNextQuarterStart, ref this.cachedNextQuarterEnd);
                    break;

                case RelativeDate.LastYear:
                    DateTime dateInLastYear = this.cachedCurrentDate.AddYears(-1);
                    this.cachedLastYearStart = new DateTime(dateInLastYear.Year, 1, 1);
                    this.cachedLastYearEnd = new DateTime(dateInLastYear.Year, 12, DateTime.DaysInMonth(dateInLastYear.Year, 12));
                    break;

                case RelativeDate.ThisYear:
                    this.cachedThisYearStart = new DateTime(this.cachedCurrentDate.Year, 1, 1);
                    this.cachedThisYearEnd = new DateTime(this.cachedCurrentDate.Year, 12, DateTime.DaysInMonth(this.cachedCurrentDate.Year, 12));
                    break;

                case RelativeDate.NextYear:
                    DateTime dateInNextYear = this.cachedCurrentDate.AddYears(1);
                    this.cachedNextYearStart = new DateTime(dateInNextYear.Year, 1, 1);
                    this.cachedNextYearEnd = new DateTime(dateInNextYear.Year, 12, DateTime.DaysInMonth(dateInNextYear.Year, 12));
                    break;
                
                case RelativeDate.YearToDate:
                    this.cachedYearToDateStart = new DateTime(this.cachedCurrentDate.Year, 1, 1);
                    break;
            }
        }
        #endregion //VerifyCache

        #endregion //Methods

        #region Base Class Overrides

        #region GetObjectData

        /// <summary>
        /// Invoked during the serialization of the object.
        /// </summary>
        /// <param name="info">SerializationInfo</param>
        /// <param name="context">StreamingContext</param>        
        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            Utils.SerializeProperty(info, "relativeDate", this.relativeDate);

            base.GetObjectData(info, context);
        }
        #endregion //GetObjectData

        #region Match

        public override bool Match(FilterComparisionOperator comparisonOperator, object value)
        {
            // If the basic conditions fail, then we shouldn't bother doing any more processing.
            if (false == base.Match(comparisonOperator, value))
                return false;

            // We don't want to have to do the calculations of parsing what the next week, current week,
            // etc will be every time that we have to compare a value, so we'll cache what we need to
            // perform these comparions, only updating them if the current date has changed.
            this.VerifyCache();

            // Excel does not perform the filtering comparisons with respect to the time of day (i.e.
            // if a YearToDate filter is done, a value that has today's date but a time an hour in
            // advance will still be considered a match).
            DateTime comparisonDate = ((DateTime)value).Date;

            switch (this.relativeDate)
            {
                case RelativeDate.Yesterday:
                    DateTime yesterday = this.cachedCurrentDate.AddDays(-1);
                    return MatchHelper(comparisonOperator, yesterday, yesterday, comparisonDate.Date);

                case RelativeDate.Today:
                    return MatchHelper(comparisonOperator, this.cachedCurrentDate, this.cachedCurrentDate, comparisonDate.Date);

                case RelativeDate.Tomorrow:
                    DateTime tomorrow = this.cachedCurrentDate.AddDays(1);
                    return MatchHelper(comparisonOperator, tomorrow, tomorrow, comparisonDate.Date);

                case RelativeDate.LastWeek:                    
                    return MatchHelper(comparisonOperator, this.cachedLastWeekStart, this.cachedLastWeekEnd, comparisonDate);

                case RelativeDate.ThisWeek:
                    return MatchHelper(comparisonOperator, this.cachedThisWeekStart, this.cachedThisWeekEnd, comparisonDate);

                case RelativeDate.NextWeek:
                    return MatchHelper(comparisonOperator, this.cachedNextWeekStart, this.cachedNextWeekEnd, comparisonDate);

                case RelativeDate.LastMonth:
                    return MatchHelper(comparisonOperator, this.cachedLastMonthStart, this.cachedLastMonthEnd, comparisonDate);

                case RelativeDate.ThisMonth:
                    return MatchHelper(comparisonOperator, this.cachedThisMonthStart, this.cachedThisMonthEnd, comparisonDate);

                case RelativeDate.NextMonth:
                    return MatchHelper(comparisonOperator, this.cachedNextMonthStart, this.cachedNextMonthEnd, comparisonDate);

                case RelativeDate.LastQuarter:
                    return MatchHelper(comparisonOperator, this.cachedLastQuarterStart, this.cachedLastQuarterEnd, comparisonDate);

                case RelativeDate.ThisQuarter:
                    return MatchHelper(comparisonOperator, this.cachedThisQuarterStart, this.cachedThisQuarterEnd, comparisonDate);

                case RelativeDate.NextQuarter:
                    return MatchHelper(comparisonOperator, this.cachedNextQuarterStart, this.cachedNextQuarterEnd, comparisonDate);

                case RelativeDate.LastYear:
                    return MatchHelper(comparisonOperator, this.cachedLastYearStart, this.cachedLastYearEnd, comparisonDate);

                case RelativeDate.ThisYear:
                    return MatchHelper(comparisonOperator, this.cachedThisYearStart, this.cachedThisYearEnd, comparisonDate);

                case RelativeDate.NextYear:
                    return MatchHelper(comparisonOperator, this.cachedNextYearStart, this.cachedNextYearEnd, comparisonDate);

                case RelativeDate.YearToDate:                    
                    return MatchHelper(comparisonOperator, this.cachedYearToDateStart, this.cachedCurrentDate, comparisonDate);
            }

            Debug.Fail("Unexpected RelativeDate value");
            return false;
        }
        #endregion //Match

        #endregion //Base Class Overrides
    }

    internal class MonthOperand : DateOperand
    {
        #region Members

        private short month;

        #endregion //Members

        #region Constructor

        public MonthOperand(string name, string displayText, short month)
            : base(name, displayText)
        {
            if (month < 1 || month > 12)
                throw new ArgumentOutOfRangeException();

            this.month = month;
        }

        protected MonthOperand(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case "month":
                        this.month = (short)Utils.DeserializeProperty(entry, typeof(short), 1);
                        break;
                }
            }
        }
        #endregion //Constructor

        #region Base Class Overrides

        #region GetObjectData

        /// <summary>
        /// Invoked during the serialization of the object.
        /// </summary>
        /// <param name="info">SerializationInfo</param>
        /// <param name="context">StreamingContext</param>        
        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            Utils.SerializeProperty(info, "month", this.month);

            base.GetObjectData(info, context);
        }
        #endregion //GetObjectData

        #region Match

        public override bool Match(FilterComparisionOperator comparisonOperator, object value)
        {
            if (base.Match(comparisonOperator, value) == false)
                return false;

            // The base implementation will perform null and type checks
            int monthOfValue = ((DateTime)value).Month;
            switch (comparisonOperator)
            {
                case FilterComparisionOperator.Equals:
                    return this.month == monthOfValue;

                case FilterComparisionOperator.NotEquals:
                    return this.month != monthOfValue;

                case FilterComparisionOperator.LessThan:
                    return monthOfValue < this.month;

                case FilterComparisionOperator.LessThanOrEqualTo:
                    return monthOfValue <= this.month;

                case FilterComparisionOperator.GreaterThan:
                    return monthOfValue > this.month;

                case FilterComparisionOperator.GreaterThanOrEqualTo:
                    return monthOfValue >= this.month;
            }

            Debug.Fail("We shouldn't be trying to match a month with an unsupported FilterComparisonOperator");
            return false;
        }
        #endregion //Match
        
        #endregion //Base Class Overrides
    }

    internal class QuarterOperand : DateOperand
    {
        #region Members

        private short quarter;

        #endregion //Members

        #region Constructor

        public QuarterOperand(string name, string displayName, short quarter)
            : base(name, displayName)
        {
            if (quarter < 1 || quarter > 4)
                throw new ArgumentOutOfRangeException();

            this.quarter = quarter;
        }

        protected QuarterOperand(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case "quarter":
                        this.quarter = (short)Utils.DeserializeProperty(entry, typeof(short), 1);
                        break;
                }
            }
        }
        #endregion //Constructor

        #region Base Class Overrides

        /// <summary>
        /// Invoked during the serialization of the object.
        /// </summary>
        /// <param name="info">SerializationInfo</param>
        /// <param name="context">StreamingContext</param>        
        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, SerializationFormatter = true)]
        protected override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            Utils.SerializeProperty(info, "quarter", this.quarter);

            base.GetObjectData(info, context);
        }

        #region Match

        public override bool Match(FilterComparisionOperator comparisonOperator, object value)
        {
            if (base.Match(comparisonOperator, value) == false)
                return false;

            // The base implementation will perform null and type checks
            int quarterOfValue = (int)Math.Ceiling(((DateTime)value).Month / 3.0);

            switch (comparisonOperator)
            {
                case FilterComparisionOperator.Equals:
                    return quarterOfValue == this.quarter;

                case FilterComparisionOperator.NotEquals:
                    return quarterOfValue != this.quarter;

                case FilterComparisionOperator.LessThan:
                    return quarterOfValue < this.quarter;

                case FilterComparisionOperator.LessThanOrEqualTo:
                    return quarterOfValue <= this.quarter;

                case FilterComparisionOperator.GreaterThan:
                    return quarterOfValue > this.quarter;

                case FilterComparisionOperator.GreaterThanOrEqualTo:
                    return quarterOfValue >= this.quarter;
            }

            Debug.Fail("We shouldn't be trying to match a quarter with an unsupported FilterComparisonOperator");
            return false;
        }
        #endregion //Match

        #endregion //Base Class Override
    }
}
