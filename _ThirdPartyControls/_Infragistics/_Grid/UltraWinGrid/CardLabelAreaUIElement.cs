#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using System.Diagnostics;
using System.Collections.Generic;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Summary description for CardLabelAreaUIElement.
	/// </summary>
	public class CardLabelAreaUIElement : AdjustableUIElement
	{
		#region Member Variables

		private bool inFirstCardAreaRow	= false;

		#endregion Member Variables

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		public CardLabelAreaUIElement(UIElement parent) : base(parent, true, false)
		{
		}

		#endregion Constructor

		#region Base Class Overrides

			#region ApplyAdjustment

		/// <summary>
		/// Called after a move/resize operation. 
		/// </summary>
        /// <param name="delta">The delta</param>
		public override void ApplyAdjustment (Point delta)
		{
			this.Band.CardSettings.LabelWidth = this.Rect.Width + delta.X;

			this.ControlElement.DirtyChildElements();
		}

			#endregion ApplyAdjustment

			#region ClipChildren

		/// <summary>
		/// Returning true causes all drawing of this element's children to be explicitly clipped
		/// to its region
		/// </summary>
		protected override bool ClipChildren { get { return true; } }

			#endregion ClipChildren

			#region InitAppearance 

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
											   ref AppearancePropFlags	requestedProps)
		{
			// JJD 1/10/02
			// Resolve the appearance as a columns header.
			ResolveAppearanceContext context = new ResolveAppearanceContext(typeof(Infragistics.Win.UltraWinGrid.HeaderBase), requestedProps);

			// SSP 6/7/02
			// CVMS Row filtering. When all the rows are filtered out, FirstVisibleRow will
			// be null. So use this.Band instead of getting it off FirstVisibleRow.
			//
			//this.CardAreaElement.FirstVisibleRow.Band.ResolveAppearance(ref appearance, ref context);
			UltraGridBand band = this.Band;
			Debug.Assert( null != band, "No band !" );
			if ( null != band )
			{
				// SSP 3/13/06 - App Styling
				// 
				context.Role = StyleUtils.GetRole( band, StyleUtils.Role.CardLabelArea, out context.ResolutionOrder );

				band.ResolveAppearance(ref appearance, ref context);
			}
		}

			#endregion InitAppearance 

			#region OnElementAdjustmentStart

        /// <summary>
        /// Called when a mousedown is received and a resize operation is started.
        /// </summary>
        /// <param name="isUpDownAdjustment">isUpDownAdjustment</param>
        /// <param name="initialUpDownAdjustmentPointInBottomBorder">initialUpDownAdjustmentPointInBottomBorder</param>
        /// <param name="isLeftRightAdjustment">isLeftRightAdjustment</param>
        /// <param name="initialLeftRightAdjustmentPointInRightBorder">initialLeftRightAdjustmentPointInRightBorder</param>
        protected override void OnElementAdjustmentStart(bool isUpDownAdjustment, 
														 bool initialUpDownAdjustmentPointInBottomBorder,
														 bool isLeftRightAdjustment,
														 bool initialLeftRightAdjustmentPointInRightBorder)
		{
			// Exit edit mode.
			if (this.CardAreaElement.Band.Layout.ActiveCell != null)
				this.CardAreaElement.Band.Layout.ActiveCell.ExitEditMode();
		}

			#endregion OnElementAdjustmentStart

			#region SupportsLeftRightAdjustmentsFromRightBorder

		/// <summary>
		/// True if this element supports left to right adjustments by grabbing the right border
		/// </summary>
		protected override bool SupportsLeftRightAdjustmentsFromRightBorder
		{
			get
			{ 
				// SSP 3/14/03 - Row Layout Functionality
				// In row-layout mode disable resizing of the whole label area since individual
				// label resizing is enabled in the row-layout mode.
				//
				if ( this.Band.UseRowLayoutResolved )
					return false;

				// We can only be sized when our band is a card view band
				if (this.Band.CardSettings.AllowLabelSizing)
					return true;
				else
					return false;
			}
		}

			#endregion SupportsLeftRightAdjustmentsFromRightBorder

			#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Band, StyleUtils.Role.CardLabelArea );
			}
		}

			#endregion // UIRole

		#endregion Base Class Overrides

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			// SSP 2/28/03 - Row Layout Functionality
			// Added if block below.
			//
			if ( this.Band.UseRowLayoutResolved )
			{
				UltraGridBand band = this.Band;

				UIElementsCollection oldElems = this.childElementsCollection;
				this.childElementsCollection = null;

				LayoutContainerCardLabelArea lc = band.GetCachedLayoutContainerCardLabelArea( );
				lc.Initialize( band, this.Rect, this, oldElems );

				band.HeaderLayoutManager.LayoutContainer( lc, null );

				return;
			}

			Rectangle				rectWork;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//ColScrollRegion			colRegion	= ((RowColRegionIntersectionUIElement)this.GetAncestor(typeof(RowColRegionIntersectionUIElement))).ColScrollRegion;


			// Save the current child elements collection so we can
			// potentially reuse some of the elements.
			UIElementsCollection	oldElements = this.ChildElements;


			// Clear the current child elements collection.
			this.childElementsCollection = null;


			// If the top label border can be merged set a flag
			// so we can adjust their heights to overlap each other
			bool overlapTopLabelBorder = this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleHeaderResolved);


			// JM 01-24-02
			// Make a pass thru the headers to find the 'last' header so we can pass
			// the correct 'isLastItem' value to AddLabelElementHelper.  We cannot always
			// rely on the header's LastItem property since this may not be accurate
			// if the CardStyle is variable.  The reason is that if the LastItem
			// property = true but the header has a cell value that would cause it to
			// not be displayed when CardStyle is variable, then it will not be the
			// last card.
			// SSP 4/8/03 UWG2115
			// If all the rows are filtered out and there are no visible rows, then allow 
			// skip the logic for laying out compressed/variable-height rows.
			//
			//HeaderBase		lastDisplayableHeader = this.Band.GetLastDisplayableCardLabel(this.RowElement == null ? null : this.RowElement.Row);
			HeaderBase		lastDisplayableHeader = null != this.RowElement 
				? this.Band.GetLastDisplayableCardLabel( this.RowElement.Row ) 
				: null;

			// JM 01-24-02
			// Need to keep track of the first displayable header.  Can't rely on
			// the header's FirstItem for the same reason outlined above.
			bool			isFirstItem = true;


			// Iterate over the ordered headers and add label elements for each one.
			int				heightUsed	= 0;
			HeaderBase		header		= null;

			// MD 1/21/09 - Groups in RowLayouts
			// This is no longer needed
			//UltraGridColumn	column		= null;

			for (int index = 0; index < this.Band.OrderedHeaders.Count; index++)
			{
				header = this.Band.OrderedHeaders[index]; 
				if (header == null)
					continue;

				// If hidden, skip it.
				if (header.Hidden)
					continue;

				// If it's a group, drill down for the headers.
				if (header.IsGroup)
				{
					// MD 1/21/09 - Groups in RowLayouts
					// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
					// actually visible in the group
					//GroupColumnsCollection colList = header.Group.Columns;
					IEnumerable<UltraGridColumn> colList = header.Group.ColumnsResolved;

					if (colList != null)
					{
						// Iterate over the columns in this group and create an element for each
						// MD 1/21/09 - Groups in RowLayouts
						// The colList is now an IEnumarable<UltraGridColumn> , so we must use a foreach.
						//for ( int i = 0; i < colList.Count; i++ )
						//{
						//    column = colList[i];
						foreach ( UltraGridColumn column in colList )
						{
							// If hidden, skip it.
							if (column.Header.Hidden)
								continue;

							// If we're in variable height mode, make sure we
							// should display this cell.
							// SSP 4/8/03 UWG2115
							// If all the rows are filtered out and there are no visible rows, then allow 
							// skip the logic for laying out compressed/variable-height rows.
							//
							//if (this.CardAreaElement.VariableHeight)
							if ( null != this.RowElement && this.CardAreaElement.VariableHeight)

							{
								if (column.ShouldCellValueBeDisplayedInCard(this.RowElement.Row) == false)
									continue;
							}

							// Initialize the label's rect.
							rectWork		= this.RectInsideBorders;
							rectWork.Y		= this.Rect.Top + heightUsed;
							rectWork.Height	= column.CardCellHeight;
							heightUsed	   += rectWork.Height;

							// Create/find the label element and add it to our child elements collection.
							this.AddLabelElementHelper(oldElements,
													   rectWork,
													   column.Header,
													   overlapTopLabelBorder,
													   // JM 01-24-02 Use isFirstItem
													   //column.FirstItem && header.FirstItem,
													   isFirstItem,
													   // JM 01-24-02 Use lastDisplayableHeader 
													   //column.LastItem && header.LastItem);
													   (lastDisplayableHeader == null) ? (column.LastItem && header.LastItem) : column.Header.Equals(lastDisplayableHeader),
													   // SSP 2/26/03 - Row Layout Functionality
													   // Pass in false for the new rowLayoutMode parameter.
													   //
													   false );

							isFirstItem = false;
						}
					}
				}
				else
				{
					// MD 8/2/07 - 7.3 Performance
					// Cache column - Prevent calling expensive getters multiple times
					UltraGridColumn headerColumn = header.Column;

					// If we're in variable height mode, make sure we
					// should display this cell.
					// SSP 4/8/03 UWG2115
					// If all the rows are filtered out and there are no visible rows, then allow 
					// skip the logic for laying out compressed/variable-height rows.
					//
					//if (this.CardAreaElement.VariableHeight)
					if ( null != this.RowElement && this.CardAreaElement.VariableHeight )
					{
						// MD 8/2/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if (header.Column.ShouldCellValueBeDisplayedInCard(this.RowElement.Row) == false)
						if ( headerColumn.ShouldCellValueBeDisplayedInCard( this.RowElement.Row ) == false )
							continue;
					}

					// Initialize the cell's rect.
					rectWork		= this.RectInsideBorders;
					rectWork.Y		= this.Rect.Top + heightUsed;

					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//rectWork.Height	= header.Column.CardCellHeight;
					rectWork.Height = headerColumn.CardCellHeight;

					heightUsed	   += rectWork.Height;

					// Create/find the label element and add it to our child elements collection.
					this.AddLabelElementHelper(oldElements,
											   rectWork,
											   header,
											   overlapTopLabelBorder,
											   // JM 01-24-02 Use isFirstItem
											   //header.FirstItem,
											   isFirstItem,
											   // JM 01-24-02 Use lastDisplayableHeader 
											   //header.LastItem);
											   (lastDisplayableHeader == null) ? header.LastItem : header.Equals(lastDisplayableHeader),
											   // SSP 2/26/03 - Row Layout Functionality
											   // Pass in false for the new rowLayoutMode parameter.
											   //
											   false );

					isFirstItem = false;
				}
			}
		}

		#endregion PositionChildElements

		#region Private Methods

			#region AddLabelElementHelper

		// SSP 2/26/03 - Row Layout Functionality
		// Changed the access modifier from private to internal.
		//
		internal void AddLabelElementHelper(UIElementsCollection	oldElements,
										   Rectangle			rcElement,
										   HeaderBase			header,
										   bool					overlapTopLabelBorder,
										   bool					isFirstItem,
										   bool					isLastItem,
										   // SSP 2/26/03 - Row Layout Functionality
										   // Added rowLayoutMode parameter to indicate that it's being called from
										   // row layout logic.
										   // 
										   bool rowLayoutMode )
		{
			// Create a CardLabelUIElement
			//
			// Try to reuse one from the old list.
			CardLabelUIElement cardLabelElement = (CardLabelUIElement)CardLabelAreaUIElement.ExtractExistingElement(oldElements, typeof(CardLabelUIElement), true);


			// If we didn't find one in the old list then create a new one.
			if (cardLabelElement == null)
				cardLabelElement = new CardLabelUIElement(this);


			// SSP 2/26/03 - Row Layout Functionality
			// row layout code already takes care of overlapping borders so don't do it here.
			//
			if ( ! rowLayoutMode )
			{
				// Adjust the element's rect for border overlap if necessary.
				this.AdjustElementRectForBorderOverlap(ref rcElement,
					overlapTopLabelBorder,
					isFirstItem,
					isLastItem);
			}

			cardLabelElement.Rect	= rcElement;
			cardLabelElement.InitializeHeader(header);


			// Add the new element.
			this.ChildElements.Add(cardLabelElement);
		}

			#endregion AddLabelElementHelper

			#region AdjustElementRectForBorderOverlap

		private void AdjustElementRectForBorderOverlap(ref Rectangle rcElement,
													   bool			 overlapTopLabelBorder,
													   bool			 isFirstItem,
													   bool			 isLastItem)
		{

			bool canMergeRow		= this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved);
			bool canMergeCardArea	= this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved);
			bool canMergeHeader		= this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleHeaderResolved);
			bool showCaption		= this.Band.CardSettings.ShowCaption;
			// SSP 2/28/03 - Row Layout Functionality
			// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
			// to StandardLabels card style so use the StyleResolved instead of the Style property.
			//
			bool mergedLabels		= this.Band.CardSettings.StyleResolved == CardStyle.MergedLabels;
			bool inFirstCardAreaRow = this.InFirstCardAreaRow;

            // SSP 6/13/05
            // Whidbey gives warnings.
            // 
			//bool doNothing			= false;

			int  vertSpacing		= this.Band.CardSpacingVertical;
			int  horzSpacing		= this.Band.CardSpacingHorizontal;

			// Overlap the top label border if the conditions are right.
			if (overlapTopLabelBorder)
			{
				if (mergedLabels	 == true	&&
					showCaption		 == false	&&
					canMergeCardArea == false	&&
					canMergeRow)
				{
					rcElement.Height++;						
				}
				else if (mergedLabels		== true	 &&
						 showCaption		== false &&
						 canMergeCardArea	== true	 &&
						 canMergeRow		== true	 &&
						 vertSpacing		!= 0)
				{
					rcElement.Height++;						
				}
				else if (mergedLabels	== false	&&
						 showCaption	== false	&&
						 canMergeRow	== true)
				{
					rcElement.Y--;
					rcElement.Height++;						
				}
				else if (isFirstItem		== true	 &&
						 inFirstCardAreaRow == true	 &&
						 showCaption		== false &&
						 canMergeCardArea	== true	 &&
						 canMergeRow		== false &&
						 vertSpacing		!= 0)
				{

                    // SSP 6/13/05
                    // Whidbey gives warnings.
                    // 
					//doNothing = doNothing;
				}
				else if (isFirstItem		== true	 &&
						 inFirstCardAreaRow == false &&
						 showCaption		== false &&
						 canMergeCardArea	== true	 &&
						 mergedLabels		== true  &&
						 vertSpacing		== 0)
				{
					rcElement.Y--;
					rcElement.Height++;						
				}
				else if (showCaption		== false  &&
						 canMergeCardArea	== true	  &&
						 !(isFirstItem == true  && inFirstCardAreaRow == false))
				{
					rcElement.Y--;
					rcElement.Height++;						
				}
				else if (isFirstItem	== false &&
						 inFirstCardAreaRow	== false &&
						 showCaption			== false &&
						 canMergeHeader		== true)
				{
					rcElement.Y--;
					rcElement.Height++;						
				}
				else if (inFirstCardAreaRow	== false &&
						 mergedLabels		== true  &&
						 showCaption		== false &&
						 canMergeCardArea	== false &&
						 vertSpacing		== 0)
				{
					rcElement.Y--;
					rcElement.Height++;						
				}
				else if (inFirstCardAreaRow	== false &&
						 mergedLabels		== false &&
						 showCaption		== false &&
						 canMergeRow		== false &&
						 vertSpacing		== 0)
				{
					rcElement.Y--;
					rcElement.Height++;						
				}
				else if (!isFirstItem) 
				{
					rcElement.Y--;
					rcElement.Height++;						
				}
				else if (isFirstItem &&
						 showCaption		== false	&&
						 canMergeHeader		== true		&&
						 canMergeRow		== false	&&
						 canMergeCardArea	== false	&&
						 inFirstCardAreaRow == true		&&
						 vertSpacing		== 0		&&
						 this.Band.HeaderVisible == true)
				{
					rcElement.Y--;
					rcElement.Height++;						
				}
				else
				{
                    // SSP 6/13/05
                    // Whidbey gives warnings.
                    // 
					//doNothing = doNothing;
				}
			}

			// A little fudge for chocolate lovers.
			if (isLastItem)
			{
				if (mergedLabels	== false &&
					canMergeHeader	== true	 &&
					canMergeRow		== true)
				{
					rcElement.Height++;
				}
				else if (mergedLabels	== true &&
						 canMergeRow	== true)
				{
					rcElement.Height++;
				}
			}

			// Overlap the left edge of the label if the conditions are right.
			if (mergedLabels	== false &&
				canMergeHeader	== true	 &&
				canMergeRow		== true)
			{
				rcElement.X--;
				rcElement.Width++;
			}
			else if (mergedLabels		== false &&
					 canMergeHeader		== true	 &&
					 canMergeCardArea	== true  &&
					 horzSpacing		== 0)
			{
				rcElement.X--;
				rcElement.Width++;
			}
			else
			{
                // SSP 6/13/05
                // Whidbey gives warnings.
                // 
				//doNothing = doNothing;
			}
		}

			#endregion AdjustElementRectForBorderOverlap

		#endregion Private Methods

		#region Internal Properties

			#region CardAreaElement

		internal CardAreaUIElement CardAreaElement
		{
			get
			{
				return (this.GetAncestor(typeof(CardAreaUIElement)) as CardAreaUIElement);
			}
		}

			#endregion CardAreaElement

			#region Band

		internal UltraGridBand Band
		{
			get
			{
				CardAreaUIElement cardAreaElement = this.CardAreaElement;

				if (cardAreaElement != null)
					return cardAreaElement.Band;
				else
					return null;
			}
		}

			#endregion Band

			#region InFirstCardAreaRow

		internal bool InFirstCardAreaRow
		{
			get	{ return this.inFirstCardAreaRow; }
			set { this.inFirstCardAreaRow = value; }
		}

			#endregion InFirstCardAreaRow

			#region RowElement

		internal RowUIElementBase RowElement
		{
			get
			{
				return (this.GetAncestor(typeof(RowUIElementBase)) as RowUIElementBase);
			}
		}

			#endregion RowElement

		#endregion Internal Properties
	}
}
