#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
	using System.Windows.Forms;
    using System.Diagnostics;
	using System.Collections;
	using Infragistics.Shared;
	using Infragistics.Win;
	using Infragistics.Win.UltraWinScrollBar;
	using System.Collections.Generic;
    using System.Drawing.Drawing2D;

    /// <summary>
    ///    Summary description for RowColRegionIntersectionUIElement.
    /// </summary>
    public class RowColRegionIntersectionUIElement : UIElement
    {
        private RowScrollRegion rsr;
        private ColScrollRegion csr;

		// JJD 12/12/03 - Added Accessibility support
		private AccessibleObject accessibilityObject;

		// SSP 5/3/05 - Optimizations
		// 
		private int verifiedRowChildElementsCacheVersion = -1;

        // MBS 3/31/09 - NA9.2 CellBorderColor
        private int lastActiveCellBorderThickness;
        private Pen cachedActiveCellBorderPen;

        // MBS 4/13/09 - NA9.2 Selection Overlay
        private SolidBrush cachedSelectionOverlayBrush = null;
        private Pen cachedSelectionBorderPen;    

		/// <summary>
		/// consturctor
		/// </summary>
		/// <param name="parent">The parent element</param>
        public RowColRegionIntersectionUIElement( UIElement parent ) : base( parent )
        {
        }


        /// <summary>
        /// Initializes the row-col intersection ui element with the passed in rsr and csr.
        /// </summary>
        /// <param name="csr">The column scroll region</param>
        /// <param name="rsr">The row scroll region</param>        
		// SSP 7/22/03 - Row Layout Designer
		// Changed the access modifier of Initialize method from internal to internal protected.
		//
		//internal void Initialize( RowScrollRegion rsr, ColScrollRegion csr )
		internal protected void Initialize( RowScrollRegion rsr, ColScrollRegion csr )
        {
            this.rsr = rsr;
            this.csr = csr;

            this.PrimaryContext = rsr;

        }

		#region ContinueDescendantSearch

		// SSP 12/19/02
		// Optimizations.
		// Overrode ContinueDescendantSearch method.
		//
		/// <summary>
		/// This method is called from <see cref="Infragistics.Win.UIElement.GetDescendant(Type,object[])"/> as an optimization to
		/// prevent searching down element paths that can't possibly contain the element that is being searched for. 
		/// </summary>
		/// <remarks><seealso cref="Infragistics.Win.UIElement.ContinueDescendantSearch"/></remarks>
		/// <returns>Returns false if the search should stop walking down the element chain because the element can't possibly be found.</returns>
        protected override bool ContinueDescendantSearch( Type type, object[] contexts )
		{
			if ( null != contexts )
			{
				for ( int i = 0; i < contexts.Length; i++ )
				{
					object context = contexts[i];

					if ( null != context )
					{
						Type contextType = context.GetType( );

						if ( typeof( RowScrollRegion ) == contextType && this.rsr != context )
							return false;

						if ( typeof( ColScrollRegion ) == contextType && this.csr != context  )
							return false;
					}
				}
			}

			return true;
		}

		#endregion // ContinueDescendantSearch

		/// <summary>
		/// Returns the ColScrollRegion object to which a UIElement belongs. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the column scrolling region to which the UIElement belongs. You can use this reference to access any of the returned column scrolling region's properties or methods.</p>
		/// <p class="body">If the UIElement does not belong to a column scrolling region, Nothing is returned.</p>
		/// <p class="body">The <b>RowScrollRegion</b> property can be used to return a reference to an RowScrollRegion object to which a UIElement belongs.</p>
		/// </remarks>
		public RowScrollRegion RowScrollRegion
        {
            get
            {
                return this.rsr;
            }
        }

		/// <summary>
		/// Returns the RowScrollRegion object to which a UIElement belongs. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to an RowScrollRegion object that can be used to set properties of, and invoke methods on, the row scrolling region to which the UIElement belongs. You can use this reference to access any of the returned row scrolling region's properties or methods.</p>
		/// <p class="body">If the UIElement does not belong to a row scrolling region, Nothing is returned.</p>
		/// <p class="body">The <b>ColScrollRegion</b> property can be used to return a reference to an ColScrollRegion object to which a UIElement belongs.</p>
		/// </remarks>
        public ColScrollRegion ColScrollRegion
        {
            get 
            {
                return this.csr;
            }
        }

        /// <summary>
        /// Returns an object of requested type that relates to the element or null.
        /// </summary>
        /// <param name="type">The requested type or null to pick up default context object.</param>
        /// <param name="checkParentElementContexts">If true will walk up the parent chain looking for the context.</param>
        /// <returns>Returns null or an object of requested type that relates to the element.</returns>
        /// <remarks> Classes that override this method normally need to override the <see cref="Infragistics.Win.UIElement.ContinueDescendantSearch(System.Type,System.Object[])"/> method as well.</remarks>
        // AS - 10/31/01
		//public virtual object GetContext( Type type )
		public override object GetContext( Type type, bool checkParentElementContexts )
        {
            // If they arte looking for a colscrollregion
            // return the csr member
            //
            if ( typeof( ColScrollRegion ) == type )
                return this.csr;

            return base.GetContext( type, checkParentElementContexts );
        }
 
        /// <summary>
        /// Returning true causes all drawing of this element's children to be expicitly clipped
        /// to its region
        /// </summary>
        protected override bool ClipChildren { get { return true; } }


		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private int CalcVisibleRowLevel( VisibleRow vr )
		private static int CalcVisibleRowLevel( VisibleRow vr )
		{
			int level = 0;

			vr = vr.ParentVisibleRow;

			while ( null != vr )
			{
				level++;

				vr = vr.ParentVisibleRow;
			}

			return level;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Extracts a matching summary footer element from the old elements. If no
		//        /// matching element is found, then it returns the last one.
		//        /// </summary>
		//        /// <param name="oldElements"></param>
		//        /// <param name="summaryValues"></param>
		//        /// <returns></returns>
		//#endif
		//        private SummaryFooterUIElement ExtractSummaryFooterElement( UIElementsCollection oldElements, SummaryValuesCollection summaryValues )
		//        {
		//            int lastElemIndex = -1;

		//            for ( int i = 0; i < oldElements.Count; i++ )
		//            {
		//                SummaryFooterUIElement elem = oldElements[i] as SummaryFooterUIElement;

		//                if ( null == elem )
		//                    continue;

		//                lastElemIndex = i;

		//                if ( elem.SummaryValues == summaryValues )
		//                {
		//                    // Set it to null so we don't resuse it.
		//                    //
		//                    oldElements[i] = null;
		//                    return elem;
		//                }
		//            }

		//            if ( lastElemIndex >= 0 )
		//            {
		//                if ( oldElements[lastElemIndex] is SummaryFooterUIElement )
		//                {
		//                    oldElements[lastElemIndex] = null;
		//                    return (SummaryFooterUIElement)oldElements[lastElemIndex];
		//                }
		//            }

		//            return null;
		//        }

		#endregion Not Used

		// MD 7/26/07 - 7.3 Performance
		// GetSummaryFootersToBeDisplayed was changed and always returns null and consequently, this will always nothing
		#region Removed

		//internal void AddSummaryFooterElement( 
		//    UIElementsCollection oldElements,
		//    Rectangle rowElementRect,
		//    VisibleRow visibleRow,
		//    // SSP 10/30/03 UWG2454
		//    // Summary footers have to be taken into account when addedin group-by-row connectors.
		//    // Added summaryFooterElementTops parameter.
		//    //
		//    int[] summaryFooterElementTops
		//    )
		//{
		//    if ( null == visibleRow || null == visibleRow.Row )
		//        return;

		//    SummaryValuesCollection[] summaryValuesArr = visibleRow.GetSummaryFootersToBeDisplayed( visibleRow.Row );

		//    if ( null == summaryValuesArr || summaryValuesArr.Length <= 0 )
		//        return;

		//    // SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		//    // Added ability to display summaries in group-by rows aligned with columns, 
		//    // summary footers for group-by row collections, fixed summary footers and
		//    // being able to display the summary on top of the row collection.
		//    //
		//    SummaryDisplayAreaContext bottomSummariesContext = SummaryDisplayAreaContext.BottomScrollingSummariesContext;

		//    int rowHeight = 0;
		//    if ( visibleRow.Row.HiddenResolved )
		//        rowHeight = 0;
		//    else
		//        // 1 + to allow for 1 pixel spacing between the rows and the footer
		//        //
		//        rowHeight = 1 + rowElementRect.Height;

		//    int y = rowElementRect.Y + rowHeight;

		//    for ( int i = 0; i < summaryValuesArr.Length; i++ )
		//    {
		//        SummaryValuesCollection summaryValuesColl = summaryValuesArr[i];

		//        Debug.Assert( null != summaryValuesColl );

		//        if ( null == summaryValuesColl )
		//            continue;

		//        // SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		//        // Added ability to display summaries in group-by rows aligned with columns, 
		//        // summary footers for group-by row collections, fixed summary footers and
		//        // being able to display the summary on top of the row collection.
		//        //
		//        //int summaryFooterHeight = summaryValuesColl.GetSummaryFooterHeight( );
		//        int summaryFooterHeight = summaryValuesColl.GetSummaryFooterHeight( bottomSummariesContext );

		//        // SSP 8/8/03 UWG2551
		//        // Moved this down.
		//        //
		//        /*
		//        SummaryFooterUIElement footerElem = this.ExtractSummaryFooterElement( oldElements, summaryValuesColl );

		//        if ( null == footerElem )
		//            footerElem = new SummaryFooterUIElement( this, summaryValuesColl.Rows );

		//        footerElem.Initialize( summaryValuesColl.Rows );
		//        */

		//        // Now to get the x and width dimensions for the summary footer,
		//        // get the ancestor visible row that belongs to the same band as
		//        // the summary values collection and get it's dimensions.
		//        //
		//        VisibleRow vr = visibleRow;
		//        while ( null != vr && vr.Band != summaryValuesColl.Band )
		//        {
		//            vr = vr.ParentVisibleRow;
		//        }

		//        Debug.Assert( null != vr, "There must have been an ancestor row belonging to the same band as the summary values collection !" );

		//        if ( null == vr )
		//            continue;

		//        // get the row's on-screen rect
		//        //
		//        Rectangle vrRect = vr.GetDimensions( this.ColScrollRegion,
		//            VisibleRowDimensions.OutsideRow,
		//            DimOriginBase.OnScreen ); 

		//        int preRowArea = summaryValuesColl.Band.PreRowAreaExtent;
		//        int x = vrRect.X + preRowArea;
		//        int width = vrRect.Right - x;

		//        // SSP 8/8/03 UWG2551
		//        // Added GetBandHeader_BandHeader_SummaryFooter_RightDelta. Unlike the row ui element, the 
		//        // band headers and the summary footer should get scrolled all the way out.
		//        //
		//        // ------------------------------------------------------------------------------------------
		//        UltraGridBand band = summaryValuesColl.Band;
		//        if ( null != band && band.UseFixedHeadersResolved( this.ColScrollRegion ) )
		//        {
		//            int delta = band.GetFixedHeaders_BandHeader_SummaryFooter_RightDelta( this.ColScrollRegion );
		//            width += delta;
		//        }
		//        // ------------------------------------------------------------------------------------------

		//        // SSP 8/8/03 - Optimizations
		//        // Make use of the summaryFooterHeight local variable instead of calling the GetSummaryFooterHeight
		//        // method again.
		//        //
		//        //Rectangle footerRect = new Rectangle( x, y, width, summaryValuesColl.GetSummaryFooterHeight( ) );
		//        Rectangle footerRect = new Rectangle( x, y, width, summaryFooterHeight );

		//        // If we are going to have multiple summary footers, then allow
		//        // for spacing in-between.
		//        //
		//        if ( i > 0 || ( null != visibleRow.Row && visibleRow.Row.Band != summaryValuesColl.Band ) )
		//        {
		//            footerRect.Y += UltraGridBand.INTER_SUMMARY_FOOTER_SPACING;
		//            y += UltraGridBand.INTER_SUMMARY_FOOTER_SPACING;
		//        }

		//        y += footerRect.Height;

		//        // SSP 8/8/03 UWG2551
		//        //
		//        if ( footerRect.Width <= 0 || footerRect.Height <= 0 )
		//            continue;

		//        // SSP 8/8/03 UWG2551
		//        // Moved this down from above.
		//        //
		//        // ----------------------------------------------------------------------------
		//        SummaryFooterUIElement footerElem = this.ExtractSummaryFooterElement( oldElements, summaryValuesColl );

		//        if ( null == footerElem )
		//            footerElem = new SummaryFooterUIElement( this, summaryValuesColl.Rows );

		//        // SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		//        // Added ability to display summaries in group-by rows aligned with columns, 
		//        // summary footers for group-by row collections, fixed summary footers and
		//        // being able to display the summary on top of the row collection.
		//        //
		//        //footerElem.Initialize( summaryValuesColl.Rows );
		//        footerElem.Initialize( summaryValuesColl.Rows, bottomSummariesContext );
		//        // ----------------------------------------------------------------------------

		//        footerElem.Rect = footerRect;

		//        this.ChildElements.Add( footerElem );

		//        // SSP 10/30/03 UWG2454
		//        // Summary footers have to be taken into account when addedin group-by-row connectors.
		//        //
		//        // ------------------------------------------------------------------------------------
		//        int vrLevel = this.CalcVisibleRowLevel( vr );
		//        if ( null != summaryFooterElementTops && vrLevel >= 0 && vrLevel < summaryFooterElementTops.Length )
		//        {
		//            summaryFooterElementTops[ vrLevel ] = footerRect.Top;
		//        }
		//        // ------------------------------------------------------------------------------------
		//    }
		//}

		#endregion Removed

		// SSP 7/21/05 - NAS 5.3 Header Placement
		// 
		#region NAS 5.3 Header Placement

		private void AddAttachedHeaders( VisibleRow vr, UIElementsCollection oldElements,
			Rectangle[] levelRects, bool[] levelFlags, int[] rowConnectorCeilings )
		{
			VisibleRow iiVr = vr.AttachedHeadersRow;
			while ( null != iiVr )
			{
				BandHeadersUIElement bandHeadersElem = this.AddBandHeaderHelper( oldElements, iiVr.Band, iiVr, 0, false );
				if ( null != bandHeadersElem )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//int rowLevel = this.CalcVisibleRowLevel( iiVr );
					int rowLevel = RowColRegionIntersectionUIElement.CalcVisibleRowLevel( iiVr );

					this.AddAttachedHeaderHelper( bandHeadersElem, levelRects, levelFlags, rowConnectorCeilings, rowLevel );
				}

				iiVr = iiVr.AttachedHeadersRow;
			}
		}

		private int CalcRowLevel( UltraGridBand band )
		{
			int groupBySortCols = band.HasSortedColumns ? band.SortedColumns.GroupBySortColumnsCount : 0;
			
			int rowLevel = groupBySortCols;
			if ( null != band.ParentBand )
				rowLevel += 1 + this.CalcRowLevel( band.ParentBand );

			return rowLevel;
		}

		private void AddAttachedHeaderHelper( BandHeadersUIElement bandHeadersElem,
			Rectangle[] levelRects, bool[] levelFlags, int[] rowConnectorCeilings, int rowLevel )
		{
			if ( null != bandHeadersElem )
			{
				// If row level is not specified then calculate it.
				// 
				if ( rowLevel < 0 )
					rowLevel = this.CalcRowLevel( bandHeadersElem.Band );

				Rectangle elemRect = bandHeadersElem.Rect;
				int i;

				if ( null != levelRects && null != levelFlags )
				{
					for ( i = levelRects.Length - 1; i >= 0; i-- )
					{
						if ( levelFlags[i] && ! levelRects[i].IsEmpty 
							&& levelRects[ i ].X >= elemRect.X )
							levelRects[ i ].Y = Math.Max( levelRects[ i ].Y, elemRect.Bottom );
					}
				}

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.SetRowConnectorCeilings( rowConnectorCeilings, elemRect.Bottom, rowLevel );
				RowColRegionIntersectionUIElement.SetRowConnectorCeilings( rowConnectorCeilings, elemRect.Bottom, rowLevel );
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void SetRowConnectorCeilings( int[] rowConnectorCeilings, int ceiling, int rowLevel )
		private static void SetRowConnectorCeilings( int[] rowConnectorCeilings, int ceiling, int rowLevel )
		{
			if ( null != rowConnectorCeilings )
			{
				for ( int i = 1 + rowLevel; i < rowConnectorCeilings.Length; i++ )
					rowConnectorCeilings[ i ] = Math.Max( rowConnectorCeilings[ i ], ceiling );
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool IsPrevSiblingRowFromGroupByRowsCollection( UltraGridRow row )
		private static bool IsPrevSiblingRowFromGroupByRowsCollection( UltraGridRow row )
		{
			UltraGridRow prevSibling = row.GetVisibleSiblingRow( false, true, IncludeRowTypes.SpecialRows );

			return null != prevSibling && prevSibling.IsFromGroupByRowsParentCollection;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool IsNextSiblingRowFromGroupByRowsCollection( UltraGridRow row )
		private static bool IsNextSiblingRowFromGroupByRowsCollection( UltraGridRow row )
		{
			UltraGridRow nextSibling = row.GetVisibleSiblingRow( true, true, IncludeRowTypes.SpecialRows );

			return null != nextSibling && nextSibling.IsFromGroupByRowsParentCollection;
		}

		#endregion // NAS 5.3 Header Placement

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{

			try
			{
				this.csr.RegenerateVisibleHeaders();
                
			}
			catch(Exception)
			{
				Debug.Assert( false, "csr regen failed" );
			}
            
			try
			{
				this.rsr.RegenerateVisibleRows();
			}
			catch(Exception)
			{
				Debug.Assert( false, "rsr regen failed" );
			}

			int i, j;

			UIElementsCollection oldElements    = this.ChildElements;
			UIElementsCollection newConnectors  = null;

			this.childElementsCollection = null;

			// SSP 11/10/04 - Merged Cell Feature
			// Remove all the merged cell elements from the old elements. They should be at
			// the end of the child elements. The reason for doing this is that when we look
			// for row and row connector elements we don't bother going through the merged
			// cell elements. There could be a lot of merged cell elements.
			//
			// ------------------------------------------------------------------------------
			UIElementsCollection oldMergedCellElems = null;
			for ( i = oldElements.Count - 1; i >= 0; i-- )
			{
				MergedCellUIElement elem = oldElements[ i ] as MergedCellUIElement;
				if ( null != elem )
				{
					if ( null == oldMergedCellElems )
						oldMergedCellElems = new UIElementsCollection( i );
					oldMergedCellElems.Add( elem );
				}
				else
					break;
			}

			++i;
			if ( null != oldMergedCellElems )
				oldElements.RemoveRange( i, oldElements.Count - i );
			// ------------------------------------------------------------------------------

			UltraGridLayout layout = this.rsr.Layout;
			VisibleRowsCollection visibleRows = this.rsr.VisibleRows;
			int visibleRowsCount = visibleRows.Count;
			ViewStyleBase viewStyle = layout.ViewStyleImpl;

			// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			//
			

			VisibleRow  parentVisibleRow = null;
			UltraGridBand        band;  
			UltraGridRow         lastRow     = null;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridBand        lastBand    = null;

			Rectangle   rcWork;
			Rectangle   rcRow;
			Rectangle   thisRect = this.rectValue;
			Rectangle   rcLastRow = new Rectangle(0,0,0,0);;
			Rectangle   rcRowConnector = new Rectangle(0,0,0,0);
            
			SiblingRowConnectorUIElement[] connectors = null;
			SiblingRowConnectorUIElement rowConnector = null;
			int         index          = 0;
			int         bandLevel      = 0;
			int         lastBandLevel  = 0;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//int         preRowSelectorWidth;

			bool        multipleBandsDispayed = viewStyle.IsMultiBandDisplay;

			// SSP 11/6/01 UWG676
			//
			bool rowAdded = false;



			// JJD 9/26/00
			// If we have a single band display check band 0's ExpansionIndicator
			// property. If it is set to 'show' then treat this like a multi-band
			// display with expansion indicators and row connectors
			//
			if ( !multipleBandsDispayed )
			{
				UltraGridBand band_0 = layout.BandZero;
                
				if ( band_0 != null && 
					//RobA 11/6/01 use the new enum						
					//band_0.ExpansionIndicatorResolved == DefaultableBoolean.True )
					band_0.ExpansionIndicatorResolved == ShowExpansionIndicator.Always )
					multipleBandsDispayed = true;
			}

			//if this is a dropdown don't show multiband
			//
			if ( ! ( layout.Grid is UltraGrid ) )
				multipleBandsDispayed = false;

            // SSP 6/13/05
            // Whidbey warning.
            // 
			//bool hasRowConnectors = true || ViewStyleBand.OutlookGroupBy != layout.ViewStyleBand;
            bool hasRowConnectors = true;

			// MRS 10/20/04 - UWG3721
			// SSP 3/16/06 - App Styling
			// Use the new RowConnectorStyleResolved.
			// 
			//hasRowConnectors = hasRowConnectors && layout.RowConnectorStyle != RowConnectorStyle.None;
			hasRowConnectors = hasRowConnectors && layout.RowConnectorStyleResolved != RowConnectorStyle.None;

			if ( multipleBandsDispayed && hasRowConnectors )
			{
				// SSP 5/31/02 UWG1152
				// Use the new MaxBandDepthEnforced property instead of MAX_BAND_DEPTH.
				// 
				//connectors = new SiblingRowConnectorUIElement[ UltraGridBand.MAX_BAND_DEPTH ];
				connectors = new SiblingRowConnectorUIElement[ layout.MaxBandDepthEnforced ];
				newConnectors  = new UIElementsCollection( Math.Max(10, visibleRowsCount + 5 ) );
			}


			Rectangle[]						  levelRects     = null;
			bool[]							  levelFlags     = null;
			VisibleRow[]					  levelVisibleRows = null;
			
			bool showGroupByIndents = false;

			if ( ViewStyleBand.OutlookGroupBy == layout.ViewStyleBand )			
				showGroupByIndents = true;
			
			VisibleRow firstVisibleRow = visibleRowsCount > 0 ? visibleRows[ 0 ] : null;
			VisibleRow lowestLevelRow = firstVisibleRow;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//int		   lowestLevel    = null != lowestLevelRow ? this.CalcVisibleRowLevel( lowestLevelRow ) : 0;
			int lowestLevel = null != lowestLevelRow ? RowColRegionIntersectionUIElement.CalcVisibleRowLevel( lowestLevelRow ) : 0;

			for ( i = visibleRowsCount - 1; i >= 0; i-- )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//int level = this.CalcVisibleRowLevel( visibleRows[ i ] );
				// MD 8/15/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//int level = RowColRegionIntersectionUIElement.CalcVisibleRowLevel( visibleRows[ i ] );
				VisibleRow visibleRow = visibleRows[ i ];
				int level = RowColRegionIntersectionUIElement.CalcVisibleRowLevel( visibleRow );

				if ( level > lowestLevel )
				{
					lowestLevel = level;

					// MD 8/15/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//lowestLevelRow = visibleRows[ i ];
					lowestLevelRow = visibleRow;
				}
			}

			// SSP 10/30/03 UWG2454
			// Summary footers have to be taken into account when addedin group-by-row connectors.
			//
			int[] summaryFooterElementTops = null;

			// SSP 7/26/05 - NAS 5.3 Header Placement
			// Added rowConnectorCeilings.
			// 
			int[] rowConnectorCeilings = new int[ 1 + lowestLevel ];
			GridUtils.InitializeArray( rowConnectorCeilings, thisRect.Top );

			if ( lowestLevel > 0 )
			{
				levelRects = new Rectangle[ lowestLevel ];
				levelFlags = new bool[ lowestLevel ];
				levelVisibleRows = new VisibleRow[ lowestLevel ];

				// SSP 10/30/03 UWG2454
				// Summary footers have to be taken into account when addedin group-by-row connectors.
				//
				// --------------------------------------------------------------------------
				summaryFooterElementTops = new int[ lowestLevel ];
				for ( i = 0; i < summaryFooterElementTops.Length; i++ )
					summaryFooterElementTops[i] = 0;
				// --------------------------------------------------------------------------

				// SSP 12/8/06 BR17938
				// 
				// ------------------------------------------------------------------------------------------
				for ( i = 0; i < visibleRowsCount; i++ )
				{
					VisibleRow visibleRow = visibleRows[i];

					while ( null != visibleRow )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//int level = this.CalcVisibleRowLevel( visibleRow );
						int level = RowColRegionIntersectionUIElement.CalcVisibleRowLevel( visibleRow );

						if ( level < levelVisibleRows.Length )
						{
							if ( null != levelVisibleRows[level] )
								break;

							// get the row's on-screen rect
							//
							Rectangle rect = visibleRow.GetDimensions( this.ColScrollRegion,
								VisibleRowDimensions.OutsideRow,
								DimOriginBase.OnScreen );

							levelVisibleRows[level] = visibleRow;

							if ( visibleRow.Row is UltraGridGroupByRow )
							{
								levelRects[level] = Rectangle.Empty;

								levelRects[level].X = rect.X;
								// JAS 2005 v2 GroupBy Row Extensions
								//
								//						levelRects[ level ].Width	= GroupByRowUIElement.GROUP_BY_INDENT;
								levelRects[level].Width = visibleRow.Band.IndentationGroupByRowResolved;
								levelRects[level].Y = this.RectInsideBorders.Top;

								levelFlags[level] = true;
							}
							else
							{
								// since we are only going to have group by connectors
								// for group by rows, for regular rows, set the level rect
								// empty so that the below logic knows not to create
								// group by indent ui elements for those levels
								//
								levelRects[level] = Rectangle.Empty;
								levelFlags[level] = false;
							}
						}

						visibleRow = visibleRow.ParentVisibleRow;
					}
				}

				
				// ------------------------------------------------------------------------------------------
			}

			showGroupByIndents = lowestLevel > 0;
			
			VisibleRow lastVisibleRow = null;
			VisibleRow lastNonCardVisibleRow = null;
			int lastVisibleRowLevel = -1;
			int lastNonCardVisibleRowLevel = -1;
			
			// SSP 11/5/04
			//
			bool isPrinting = layout.IsPrintLayout;

			// SSP 5/25/05 - NAS 5.2 Fixed Rows
			// We need to clip the connectors so they don't overlap the fixed rows.
			// 
			int topFixedRows_Bottom = thisRect.Top;
			int bottomFixedRows_Top = thisRect.Bottom;

			// SSP 7/26/05 - NAS 5.3 Header Placement
			// Figure out the row connector ceilings. In order to do that we need to add fixed headers
			// first. However in order to maintain previous behavior of drawing headers over rows (in 
			// case of single pixel border overlap), make sure that the header elems are added afeter
			// the row elems.
			//
			// ----------------------------------------------------------------------------------------
			UIElementsCollection origColl = this.childElementsCollection;
			this.childElementsCollection = null;
			bool hasNon_BottomFixed_Rows = null != firstVisibleRow && FixedRowStyle.Bottom != firstVisibleRow.Row.FixedResolvedLocation;
			int fixedHeadersBottom;
			this.PositionFixedHeaders( oldElements, hasNon_BottomFixed_Rows, levelRects, levelFlags, rowConnectorCeilings, out fixedHeadersBottom );
			UIElementsCollection fixedBandHeaderElems = this.childElementsCollection;
			this.childElementsCollection = origColl;
			// ----------------------------------------------------------------------------------------

			// SSP 7/8/05 - NAS 5.3 Empty Rows
			// We need to find out where the the scrolling actual rows end in order to know
			// where to start the empty rows. When there are bottom fixed rows, the empty rows
			// appear between the the scrolling rows and the bottom fixed rows.
			// 
			bool showEmptyRows = layout.HasEmptyRowSettings && layout.EmptyRowSettings.ShowEmptyRowsResolved;
			int startEmptyRowsAt = Math.Max( thisRect.Top, fixedHeadersBottom );

			// loop over the visiblerows collection adding each
			// visible row
			//
			//foreach ( VisibleRow vr in rsr.VisibleRows )
			for ( int vrIterator = 0; vrIterator < visibleRowsCount; vrIterator++ )
			{
				if ( vrIterator > 0 )
					lastVisibleRow = visibleRows[ vrIterator - 1 ];

				VisibleRow vr = visibleRows[ vrIterator ];

				// SSP 7/21/05 - NAS 5.3 Header Placement
				// 
				this.AddAttachedHeaders( vr, oldElements, levelRects, levelFlags, rowConnectorCeilings );

				// get the row's on-screen rect
				//
				Rectangle rowElementRect = vr.GetDimensions( this.ColScrollRegion,
					VisibleRowDimensions.OutsideRow,
					DimOriginBase.OnScreen ); 

				// SSP 4/26/04 UWG2102
				// When printing, don't print last row cut-off.
				//
				// ----------------------------------------------------------------------------
				if ( isPrinting && vrIterator > 0 )
				{
					// If the row extends beyond the bottom of the rcr region then break out of
					// the loop.
					//
					// MRS 6/18/04 - UWG3252 The last row has to account for Summaries
					//if ( rowElementRect.Bottom > thisRect.Bottom )
					// SSP 11/5/04 UWG3771
					// Use the BottomInClientOverall property because 
					// rowElementRect.Top + vr.GetTotalHeight() included the headers twice if
					// the row had headers.
					//
					//if ( (rowElementRect.Top + vr.GetTotalHeight()) > thisRect.Bottom )
					if ( vr.BottomInClientOverall > thisRect.Bottom )
						break;
				}
				// ----------------------------------------------------------------------------

				if ( null != lastVisibleRow )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//lastVisibleRowLevel = this.CalcVisibleRowLevel( lastVisibleRow );
					lastVisibleRowLevel = RowColRegionIntersectionUIElement.CalcVisibleRowLevel( lastVisibleRow );
				}

				// JJD 1/19/02 - UWG940
				// Keep separate level for non-cards
				//
				if ( null != lastNonCardVisibleRow )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//lastNonCardVisibleRowLevel = this.CalcVisibleRowLevel( lastNonCardVisibleRow );
					lastNonCardVisibleRowLevel = RowColRegionIntersectionUIElement.CalcVisibleRowLevel( lastNonCardVisibleRow );
				}

				// get the row's band
				//
				band       = vr.Row.Band;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//int rowLevel = this.CalcVisibleRowLevel( vr );
				int rowLevel = RowColRegionIntersectionUIElement.CalcVisibleRowLevel( vr );

				bool isRowGroupByRow = vr.Row is UltraGridGroupByRow;

				// SSP 5/25/05 - NAS 5.2 Filter Row/Fixed Rows/Extension of Summaries Functionality
				// We want to check if the row is from a group-by rows collection here. For example a
				// filter row is not a group-by row however it may be part of a group-by rows collection
				// in which case we should not be adding any row connectors.
				//
				bool isRowFromGroupByRowsParentCollection = vr.Row.IsFromGroupByRowsParentCollection;

				// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				// Added genericRowElement and bandHeadersElem.
				// 
				UIElement genericRowElement = null;
				BandHeadersUIElement bandHeadersElement = null;

				// JJD 10/30/01
				// Call AddRowHelper which is optimiized to reuse an element
				// for this row if it already exists
				//
				// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				//
				// ------------------------------------------------------------------------------
				
				if ( isRowGroupByRow || vr.Band.CardView )
				{
					genericRowElement = this.AddRowHelper( oldElements, ref rowElementRect, vr, false );

					// SSP 7/18/05 - NAS 5.3 Header Placement
					// Header Placement functionality allows for group-by rows to have
					// headers attached to them as well. Added the following block.
					// 
					// --------------------------------------------------------------------
					if ( isRowGroupByRow && vr.HasHeader )
					{
						// if this row has a band header attached then add it now
						//
						// Note: the reason we add the row first is that
						//       in case we overlap the top border of the
						//       row we want to draw the header afterwards
						//       so we see the headers bottom border instead
						//       of the row's top border.
						//
						bandHeadersElement = this.AddBandHeaderHelper( oldElements, 
							vr.Band,
							vr,
							0,
							false ); 
					}
					// --------------------------------------------------------------------
				}
				// ------------------------------------------------------------------------------
				else
				{
					bool skipRowDblTopBorder = vr.SkipDblTopBorderDisplay( );
					bool shiftRowSelectorDown = skipRowDblTopBorder;

					// JJD 1/17/02
					// If we have a header with single line borders we don't want to 
					// shift the row selector down. 
					//
					if ( shiftRowSelectorDown && 
						vr.HasHeader &&
						layout.CanMergeAdjacentBorders( vr.Band.BorderStyleHeaderResolved ) &&
						// SSP 3/7/03 - Row Layout Functionality
						// Added BorderStyleRowSelector proeprty to the override. So now the row selector
						// and the headers could have different border styles. So see if both can be merged.
						//
						layout.CanMergeAdjacentBorders( vr.Band.BorderStyleRowSelectorResolved ) &&
						vr.Band.GetTotalHeaderHeight() > 0 )
					{
						shiftRowSelectorDown = false;
					}

					// SSP 3/21/02
					// Only add the row if it's not hidden
					// 
					if ( !vr.Row.HiddenResolved )
						// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
						//
						//rowElement = (RowUIElement)this.AddRowHelper( oldElements, rowElementRect, vr, shiftRowSelectorDown );
						genericRowElement = this.AddRowHelper( oldElements, ref rowElementRect, vr, shiftRowSelectorDown );
					
					if ( vr.HasHeader )
					{
						// if this row has a band header attached then add it now
						//
						// Note: the reason we add the row first is that
						//       in case we overlap the top border of the
						//       row we want to draw the header afterwards
						//       so we see the headers bottom border instead
						//       of the row's top border.
						//
						bandHeadersElement = this.AddBandHeaderHelper( oldElements, 
							vr.Band,
							vr,
							0,
							false ); 

						// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
						//
						RowUIElementBase rowElement = genericRowElement as RowUIElementBase;

						// SSP 11/30/04 - Merged Cell Feature
						// Added topBorderOverlappedWithHeaders flag to the RowUIElement which indicates 
						// whether the row's top is overlapped with the band headers. NOTE: Band header 
						// element is added after the row element even if the row element visually 
						// appears after the headers. What this means is that the headers will overwrite 
						// the top row border. This flag gets reset to false in 
						// InitializeOverlapTopBorder.
						//
						if ( null != rowElement && rowElement.Row == vr.Row && skipRowDblTopBorder )
							rowElement.InitializeTopBorderOverlappedWithHeaders( true );
					}
				}

				// SSP 10/30/03 UWG2454
				// Summary footers have to be taken into account when addedin group-by-row connectors.
				//
				// ------------------------------------------------------------------------------------
				//this.AddSummaryFooterElement( oldElements, rowElementRect, vr );
				// MD 7/26/07 - 7.3 Performance
				// AddSummaryFooterElement was removed because it doesn't do anything anymore
				//this.AddSummaryFooterElement( oldElements, rowElementRect, vr, summaryFooterElementTops );
				// ------------------------------------------------------------------------------------

				// SSP 11/6/01 UWG676
				// Set the rowAdded flag to true if any rows are added
				//
				// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				//
				//rowAdded = rowAdded || ( null != rowElement || null != groupByRowElement || null != cardAreaElement );
				rowAdded = rowAdded || null != genericRowElement;

				// SSP 5/25/05 - NAS 5.2 Fixed Rows
				// We need to clip the connectors so they don't overlap the fixed rows.
				// 
				FixedRowStyle rowFixedResolvedLoc = vr.Row.FixedResolvedLocation;
				if ( FixedRowStyle.Top == rowFixedResolvedLoc )
				{
					topFixedRows_Bottom = Math.Max( topFixedRows_Bottom, rowElementRect.Bottom );
					
					// SSP 7/21/05 - NAS 5.3 Header Placement
					// 
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.SetRowConnectorCeilings( rowConnectorCeilings, topFixedRows_Bottom, rowLevel );
					RowColRegionIntersectionUIElement.SetRowConnectorCeilings( rowConnectorCeilings, topFixedRows_Bottom, rowLevel );
				}
				else if ( FixedRowStyle.Bottom == rowFixedResolvedLoc )
					bottomFixedRows_Top = Math.Min( bottomFixedRows_Top, rowElementRect.Top );

				// SSP 7/8/05 - NAS 5.3 Empty Rows
				// We need to find out where the the scrolling actual rows end in order to know
				// where to start the empty rows. When there are bottom fixed rows, the empty rows
				// appear between the the scrolling rows and the bottom fixed rows.
				// 
				if ( showEmptyRows && FixedRowStyle.Bottom != rowFixedResolvedLoc )
					startEmptyRowsAt = rowElementRect.Bottom + vr.Row.RowSpacingAfterResolved;

				// SSP 5/25/05 - NAS 5.2 Special Row Separators
				// Moved the following if block from above, before the place where we add the
				// row element. The reason for it is to make sure that we take the row separators
				// into account.
				//
				// see if we need to setup any group by row connectors
				//
				if ( showGroupByIndents )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//int level = this.CalcVisibleRowLevel( vr );
					int level = RowColRegionIntersectionUIElement.CalcVisibleRowLevel( vr );

					// SSP 7/19/05 - NAS 5.3 Header Placement
					// When the HeaderPlacement is OncePerGroupedRowIsland the headers are displayed
					// above the top-level group-by rows. We extend the headers left to occupy space 
					// over the group-by row connectors. This means the group-by connectors have to 
					// start out where the headers ends (header area bottom).
					// 
					// ----------------------------------------------------------------------------------
					
					// ----------------------------------------------------------------------------------

					// close up any previous row connectors for levels
					// below this group by row's level only if this row
					// is not the first row in the rsr
					//
					if ( null != lastVisibleRow )
					{	
						for ( i = level; i < levelRects.Length; i++ )
						{
							// if previous visible row's level was lower then the current
							// level, then previous sibling of the current level row must 
							// have been expanded requiring an indent element.
							// If not then break out of the loop.
							//
							if ( lastVisibleRowLevel <= i )
								break;

							// skip the levels that are associated with regular
							// rows as they do not have group by connectors
							//
							if ( levelRects[ i ].IsEmpty )
								continue;

							// also skip the levels with levelFlags[ i ] set to false
							// indicating the last row in that level was not expanded, 
							// and thus it does not need a group by row connector
							//
							if ( !levelFlags[ i ] )
								continue;

							GroupByRowConnectorUIElement groupByRowConnector = (GroupByRowConnectorUIElement)
								RowColRegionIntersectionUIElement.ExtractExistingElement( oldElements, 
								typeof( GroupByRowConnectorUIElement ), true );
							
							if ( null == groupByRowConnector )
								groupByRowConnector = new GroupByRowConnectorUIElement( this );

							Debug.Assert( null != levelVisibleRows[ i ] &&
								( levelVisibleRows[ i ].Row is UltraGridGroupByRow ) );

							UltraGridGroupByRow context = levelVisibleRows[ i ].Row as UltraGridGroupByRow;
						
							groupByRowConnector.Initialize( context );
								

							// now connect the ui element from the last group by
							// row to this group by row
							//
							Rectangle tmpRect = levelRects[i];

							// SSP 5/25/05 - NAS 5.2 Fixed Rows
							// We need to clip the connectors so they don't overlap the fixed rows.
							// 
							tmpRect.Y = Math.Max( tmpRect.Y, topFixedRows_Bottom );

							// SSP 10/30/03 UWG2454
							// Summary footers have to be taken into account when addeding group-by-row connectors.
							//
							// ------------------------------------------------------------------------------------
							//tmpRect.Height = rowElementRect.Top - tmpRect.Top;
							int connectorBottom = rowElementRect.Top;
							if ( null != summaryFooterElementTops )
							{
								for ( j = 0; j < i && j < summaryFooterElementTops.Length; j++ )
								{
									if ( summaryFooterElementTops[j] > tmpRect.Top && summaryFooterElementTops[j] > 0 
										&& connectorBottom > summaryFooterElementTops[j] )
										connectorBottom = summaryFooterElementTops[j];
								}
							}
								
							// SSP 5/25/05 - NAS 5.2 Fixed Rows/Special Row Separators
							// Before the group-by row connector did not take into account the row headers. Since now we 
							// are adding the row before the group-by connector the group-by connector ends up drawing
							// over the header.
							// 
							if ( vr.HasHeader && null != bandHeadersElement )
								connectorBottom = bandHeadersElement.Rect.Top;

							tmpRect.Height = connectorBottom - tmpRect.Top;
							// ------------------------------------------------------------------------------------

							groupByRowConnector.Rect = tmpRect;

							this.ChildElements.Add( groupByRowConnector );

							levelFlags[i] = false;
						}
					}

					// if the level is the lowest level, then we are
					// not going to have group by row connectors for them
					//
					if ( level < levelRects.Length )
					{
						UltraGridGroupByRow groupByRow = vr.Row as UltraGridGroupByRow;

						if ( null != groupByRow )
						{
							// If the row is not expanded, then set the level flag to false so
							// that we don't create a group by indent ui element for that row
							//
							levelFlags[ level ] = groupByRow.Expanded;

							// If the row is expanded, set the top of the rect that will be used 
							// to create the indent ui element associated with this row but do not
							// actually create the element as it will be done in above loop.
							//
							levelRects[ level ].Y = levelFlags[ level ] ?
								rowElementRect.Bottom : -1;

							levelVisibleRows[ level ] = vr;
						}
					}


					// extend the height of higher level group by row connector elements
					// upto the row that was just added
					//
					for ( i = 0; i < level; i++ )
					{
						if ( levelRects[ i ].IsEmpty )
							continue;

						if ( !levelFlags[ i ] )
							continue;

						levelRects[i].Height = rowElementRect.Bottom - levelRects[i].Top;
					}

				}


				if ( vr.Band.CardView)
					continue;

				// get the row's on-screen rect
				//
				rcRow = rowElementRect;

				// we may have a situation where we have a multiband view style
				// but only 1 band visible. So on the first row we find
				// check if it is from band 0 and if band 0 doesn't have any
				// child bands. In that case reset our stack flag to false so
				// we don't add any sibling row connectors.
				//
				if ( null == lastRow && multipleBandsDispayed )
				{
					if ( band.ParentBand == null &&
						!band.HasChildBands() )
					{
						// JJD 9/26/00
						// Only reset the multi band flag if the ExpansionIndicator
						// property is NOT set to 'show'
						//
						//if ( band.ExpansionIndicatorResolved != DefaultableBoolean.True )
						//RobA 11/6/01 use the new enum						
						if ( band.ExpansionIndicatorResolved == ShowExpansionIndicator.Never ||
							 // SSP 11/9/01 UWG705
							 // Check for Default as well.
							 //
							 band.ExpansionIndicatorResolved == ShowExpansionIndicator.Default )
							multipleBandsDispayed = false;
					}
				}

				if ( multipleBandsDispayed && hasRowConnectors )
				{
					bool adjustConnector        = false;
                    
					// JJD 9/26/00
					// Use HasExpansionIndicator method off the row which 
					// factors in the ExpansionIndicator property
					//
					//bool hasExpansionIndicator = vr.Row.GetHasChildren( true );
					bool hasExpansionIndicator = vr.Row.HasExpansionIndicator;

					// SSP 3/20/06 BR10849 BR10849
					// Call to HasExpansionIndicator above can lead to loading of child rows of a parent row.
					// This will cause InitializeRow to get fired on the loaded child rows. If one were to
					// expand the parent row (by causing for example a cell to go into edit mode) syncrhonously
					// in the InitializeRow event then that will cause recursive repositioning of ui elements
					// as well as regeneration of the visible rows collection. Therefore check here if the count
					// has changed and if so return.
					// 
					if ( visibleRowsCount != visibleRows.Count )
						return;

					// get the band's level and pre row selector width
					//
					bandLevel              = band.HierarchicalLevel;

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Remove unused locals
					//preRowSelectorWidth    = band.PreRowAreaExtent;

					//initialize the row connector rect 
					//
					// JJD 10/04/00 - ult1898
					// Adjust rcRowConnector.Bottom to handle 1 pixel off bug
					//
					// SSP 10/1/01
					//int bottom              = rcRow.Top + 1 + (( rcRow.Bottom - rcRow.Top ) / 2 );
					// SSP 5/25/05 - NAS 5.2 Special Row Separators
					// rcRow is the bounding rect of all the row related elements. We need 
					// to get the actual rect of the row element.
					// 
					//int bottom              = rcRow.Top + 1 + ( rcRow.Height / 2 );
					Rectangle actualRowRect = null != genericRowElement ? genericRowElement.Rect : rcRow;
					int bottom              = actualRowRect.Top + 1 + ( actualRowRect.Height / 2 );

					// SSP 10/1/01
					// Added code to make row connecters in group by view style work
					//
					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//bool isParentRowGroupByRow = ( null != vr.Row.ParentRow ) && ( vr.Row.ParentRow is UltraGridGroupByRow );
					UltraGridRow parentRow = vr.Row.ParentRow;
					bool isParentRowGroupByRow = ( null != parentRow ) && ( parentRow is UltraGridGroupByRow );

					rcRowConnector.Y        = 
						( !( isParentRowGroupByRow 
						// SSP 11/11/03 Add Row Feature
						//
						//&& !vr.Row.HasPrevSibling( false, true ) ) && 
						&& !vr.Row.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) ) && 
						  ( null == lastRow &&
							// SSP 7/21/05 - NAS 5.3 Header Placement
							// Added below condition. Don't add row connectors for connecting to a previous
							// group-by row sibling (across bands where prev sibling band has group-by cols).
							// 
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//! this.IsPrevSiblingRowFromGroupByRowsCollection( vr.Row ) &&
							!RowColRegionIntersectionUIElement.IsPrevSiblingRowFromGroupByRowsCollection( vr.Row ) &&
							// SSP 7/19/05 - NAS 5.3 Header Placement
							// Took out !viewStyle.HasFixedHeaders condition. Noticed that the row connector
							// connecting a row to it's prev sibling that's expanded and is scrolle dout of view on
							// top did not extend all the way to the top (top of the RSR). That was because the
							// !viewStyle.HasFixedHeaders condition below caused it to end up using the bottom - 1.
							// Instead of HasFixedHeaders, we should be using the HasMultiRowTiers.
							// 
							//( ( !viewStyle.HasFixedHeaders && bandLevel > 0 ) ||
							( ( ! viewStyle.HasMultiRowTiers && bandLevel > 0 ) ||
							  // SSP 11/11/03 Add Row Feature
							  //
							  //vr.Row.HasPrevSibling(false, true) ||
							  vr.Row.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) ||
							  vr.Row.HasSiblingInPrevSiblingBand(true, true) )  ) )
						// SSP 7/21/05 - NAS 5.3 Header Placement
						// Use the rowConnectorCeilings.
						// 
						//? thisRect.Top
						? rowConnectorCeilings[ rowLevel ]
						: bottom - 1;

					//rcRowConnector.Y        = ( null == lastRow &&
					//	( ( !viewStyle.HasFixedHeaders && bandLevel > 0 ) ||
					//	vr.Row.HasPrevSibling(false, true) ||
					//	vr.Row.HasSiblingInPrevSiblingBand(true,true) )  )
					//	? thisRect.Top
					//	: bottom - 1;

					rcRowConnector.Height   = bottom - rcRowConnector.Top;

					//	BF 2.26.01	ULT1024/1379
					//	drawing logic for the vertical row connectors
					//
					//	Before we had the Indentation property, we positioned the row connector
					//	in the center of the band's pre-row area. We don't want to do that anymore, because
					//	then the expansion indicators and row connectors will not be aligned with respect to the parent band.
					// SSP 4/17/07 BR21747
					// Added RowConnectorLineOffset. Use that instead of the constant because that takes into
					// account the PreRowAreaExtent property setting.
					// 
					//rcRowConnector.X        = rcRow.Left + ( UltraGridBand.PRE_ROW_SELECTOR_WIDTH / 2 ) - 2;
					rcRowConnector.X = rcRow.Left + band.RowConnectorLineOffset - 2;
					rcRowConnector.Width    = 5;

					// if the row has an expansion indicator and the row connector 
					// doesn't extend to the top of the meta region (for a previous
					// non-displayed row) then offset the connector rect to allow
					// for the expansion indicator
					//
					if ( hasExpansionIndicator )
					{
						// JJD 10/04/00 - ult1898
						// Adjust rcRowConnector.Bottom to handle 1 pixel off bug
						//
						// SSP 7/21/05 - NAS 5.3 Header Placement
						// 
						//if ( rcRowConnector.Top > thisRect.Top )
						if ( rcRowConnector.Top > rowConnectorCeilings[ rowLevel ] )
							rcRowConnector.Offset(0, (UltraGridBand.EXPANSION_INDICATOR_WIDTH + 1) / 2 );
					}

					// On the first time init this bands row selector
					// and all parent bands as well
					//
					if ( null == lastRow )
					{
						bool prevRowConnectorAdded = false;

						parentVisibleRow = vr.ParentVisibleRow;

						// for all the band levels above this band 
						// check to see if they need to have connectors
						//
						while ( parentVisibleRow != null )
						{
							// SSP 10/1/01
							if( !( parentVisibleRow.Row is UltraGridGroupByRow ) 
								// SSP 11/11/03 Add Row Feature
								//
								//&& ( parentVisibleRow.Row.HasNextSibling(false, true) ||
								&& ( parentVisibleRow.Row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) ||
								     parentVisibleRow.Row.HasSiblingInNextSiblingBand(true,true) )
								// SSP 7/26/05 - NAS 5.3 Header Placement
								// 
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//&& ! this.IsNextSiblingRowFromGroupByRowsCollection( parentVisibleRow.Row ) )
								&& !RowColRegionIntersectionUIElement.IsNextSiblingRowFromGroupByRowsCollection( parentVisibleRow.Row ) )
							//if  ( parentVisibleRow.Row.HasNextSibling(false, true) ||
							//	parentVisibleRow.Row.HasSiblingInNextSiblingBand(true,true) )
							{
								rcWork  = rcRowConnector;

								// SSP 10/19/01
								// We have to take into account the group by rows for calculating the offset								
								//
								//rcWork.Offset( parentVisibleRow.Band.GetOrigin() - band.GetOrigin(), 0 );
								// SSP 6/16/03 - Fixed headers
								// Added the if block and enclosed the existing code into the else block.
								//
								//int offset =  parentVisibleRow.Band.GetOrigin() - band.GetOrigin();
								int offset = 0;
								if ( layout.ViewStyleImpl.HasMultiRowTiers &&
									band.UseFixedHeadersResolved( csr ) )
								{
									int tmpParentBandOrigin = csr.GetBandOrigin( parentVisibleRow.Band ) 
										+ parentVisibleRow.Band.GetFixedHeaders_OriginDelta( csr );
									int tmpBandOrigin = csr.GetBandOrigin( band ) 
										+ band.GetFixedHeaders_OriginDelta( csr );

									offset = tmpParentBandOrigin - tmpBandOrigin;
								}
								else
								{
									offset =  parentVisibleRow.Band.GetOrigin() - band.GetOrigin();
								}

								if ( vr.Row is UltraGridGroupByRow )
								{
									offset -= band.CalcGroupByRowIndent( (UltraGridGroupByRow)vr.Row );
								}

								rcWork.Offset( offset, 0 );
								
								// SSP 7/21/05 - NAS 5.3 Header Placement
								// 
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//int parentVisibleRowLevel = this.CalcVisibleRowLevel( parentVisibleRow ); 
								int parentVisibleRowLevel = RowColRegionIntersectionUIElement.CalcVisibleRowLevel( parentVisibleRow ); 
								
								// SSP 10/22/01
								// Added ( whole ?: statement ) code to make row connecters in group by view style work
								//
								//bool tmp = ( null != parentVisibleRow.Row.ParentRow ) && ( parentVisibleRow.Row.ParentRow is UltraGridGroupByRow );
								rcWork.Y        = 
									( //!( tmp && !parentVisibleRow.Row.HasPrevSibling( false, true ) ) && 
									( null == lastRow &&
									// SSP 7/21/05 - NAS 5.3 Header Placement
									// Added below condition. Don't add row connectors for connecting to a previous
									// group-by row sibling (across bands where prev sibling band has group-by cols).
									// 
									// MD 7/26/07 - 7.3 Performance
									// FxCop - Mark members as static
									//! this.IsPrevSiblingRowFromGroupByRowsCollection( parentVisibleRow.Row ) &&
									!RowColRegionIntersectionUIElement.IsPrevSiblingRowFromGroupByRowsCollection( parentVisibleRow.Row ) &&
									// SSP 7/19/05 - NAS 5.3 Header Placement
									// Took out !viewStyle.HasFixedHeaders condition. Noticed that the row connector
									// connecting a row to it's prev sibling that's expanded and is scrolle dout of view on
									// top did not extend all the way to the top (top of the RSR). That was because the
									// !viewStyle.HasFixedHeaders condition below caused it to end up using the bottom - 1.
									// Instead of HasFixedHeaders, we should be using the HasMultiRowTiers.
									// 
									//( ( !viewStyle.HasFixedHeaders && bandLevel > 0 ) ||
									( ( ! viewStyle.HasMultiRowTiers && bandLevel > 0 ) ||
									// SSP 11/11/03 Add Row Feature
									//
									//parentVisibleRow.Row.HasPrevSibling(false, true) ||
									parentVisibleRow.Row.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) ||
									parentVisibleRow.Row.HasSiblingInPrevSiblingBand(true,true) )  ) )
									// SSP 5/25/05 - NAS 5.2 Filter Row/Fixed Rows
									// Account for fixed rows on top.
									// 
									//? thisRect.Top
									// SSP 7/21/05 - NAS 5.3 Header Placement
									// Use the rowConnectorCeilings.
									// 
									//? topFixedRows_Bottom 
									? rowConnectorCeilings[ parentVisibleRowLevel ]
									: bottom - 1;

								// add the parent row connector
								//
								rowConnector = AddRowConnectorHelper( newConnectors, 
									oldElements,
									rcWork,
									parentVisibleRow.Band,
									parentVisibleRow.Row,
									false  );

								// JJD 9/13/00 - ult1147
								// Set tip enabled for row above
								//
								if ( rowConnector != null )
								{
									rowConnector.SetRowAboveTipEnabled( true );
									connectors[parentVisibleRow.Band.HierarchicalLevel] = rowConnector;
								}
							}

							parentVisibleRow = parentVisibleRow.ParentVisibleRow;
						}


						// set up a work rect in case we need to add a connector
						// for a previous sibling row
						//
						rcWork  = rcRowConnector;
						
						
						// SSP 10/1/01
						//
						// SSP 5/25/05 - NAS 5.2 Filter Row/Fixed Rows/Extension of Summaries Functionality
						// We want to check if the row is from a group-by rows collection here. For example a
						// filter row is not a group-by row however it may be part of a group-by rows collection
						// in which case we should not be adding any row connectors.
						//
						//if ( !( lastRow is UltraGridGroupByRow ) && !isRowGroupByRow )
						if ( ( null == lastRow || ! lastRow.IsFromGroupByRowsParentCollection ) && ! isRowFromGroupByRowsParentCollection )
						{
							// JJD 10/04/00 - ult1898
							// Adjust rcWork.Bottom to handle 1 pixel off bug
							//
							//                rcWork.Bottom--;
							if ( hasExpansionIndicator )
								rcWork.Height      -= ( UltraGridBand.EXPANSION_INDICATOR_WIDTH / 2 ); // + 1;

							// SSP 10/1/01
							//
							//if ( vr.Row.HasPrevSibling(false, true) ||
							//	 ( !isParentRowGroupByRow && vr.Row.HasSiblingInPrevSiblingBand(true,true) ) )
							if ( 
								// SSP 11/9/01 Added this condition
								//-------
								// MD 8/2/07 - 7.3 Performance
								// Reuse cached bool value of this test
								//!( null != vr.Row.ParentRow &&
								// vr.Row.ParentRow is UltraGridGroupByRow &&
								!( isParentRowGroupByRow &&
							     vr.Row.Band.GetChildBandCount( true ) <= 0
							    )
								//-------
								&&
								(
								// SSP 11/11/03 Add Row Feature
								//
								 //vr.Row.HasPrevSibling(false, true) ||
								vr.Row.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) ||
								 ( !isParentRowGroupByRow && vr.Row.HasSiblingInPrevSiblingBand(true,true) ) )
								// SSP 7/26/05 - NAS 5.3 Header Placement
								// 
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//&& ! this.IsPrevSiblingRowFromGroupByRowsCollection( vr.Row )
								&& !RowColRegionIntersectionUIElement.IsPrevSiblingRowFromGroupByRowsCollection( vr.Row )
								)
							 	 //if ( vr.Row.HasPrevSibling(false, true) ||
								 //	 vr.Row.HasSiblingInPrevSiblingBand(true,true)  )
							{
								// get the previous sibling row
								//
								Infragistics.Win.UltraWinGrid.UltraGridRow prevRow = vr.Row.GetPrevVisibleRelative( true, true, false );

								// SSP 10/1/01
								//
								if ( prevRow != null && !( prevRow is UltraGridGroupByRow ) )
								//if ( prevRow != null )
								{
									// add the row connector for the previous sibling row
									//
									rowConnector = AddRowConnectorHelper( newConnectors, 
										oldElements,
										rcWork,
										prevRow.Band,
										prevRow,
										false );

									// JJD 9/13/00 - ult1147
									// Set tip enabled for row above
									//
									if ( rowConnector != null )
										rowConnector.SetRowAboveTipEnabled( true );

									prevRowConnectorAdded = true;
								}
							}
						}

						// for vertical view styles we need to add an extra
						// row connector (if we didn't add one above) to connect
						// to our parent row since we didn't have a previous sibling
						//
						if ( !viewStyle.HasMultiRowTiers )
						{
							parentVisibleRow = vr.ParentVisibleRow;

							// SSP 10/1/01
							//
							if ( parentVisibleRow != null &&
								!( parentVisibleRow.Row is UltraGridGroupByRow ) &&
								// SSP 10/22/01
								// Also skip the row connectors if the child row
								// is a group by row
								//
								!( vr.Row is UltraGridGroupByRow ) &&

								 !prevRowConnectorAdded )
							//if ( parentVisibleRow != null &&
							//	 !prevRowConnectorAdded )
							{
								rowConnector = AddRowConnectorHelper( newConnectors, 
									oldElements,
									rcWork,
									parentVisibleRow.Band,
									parentVisibleRow.Row,
									true );

								// JJD 9/13/00 - ult1147
								// Set tip enabled for row above
								//
								if ( rowConnector != null )
									rowConnector.SetRowAboveTipEnabled( true );

								prevRowConnectorAdded = true;
							}
						}

						// if we added a pre row connector then adjust this row's
						// connector rect
						//
						if ( prevRowConnectorAdded )
						{            
							// JJD 10/04/00 - ult1898
							// Adjust rcRowConnector.Top to handle 1 pixel off bug
							//
							rcRowConnector.Y = rcWork.Bottom - 1;
                            
							if ( hasExpansionIndicator )
								rcRowConnector.Y += UltraGridBand.EXPANSION_INDICATOR_WIDTH;

							rcRowConnector.Height = 1;
						}

						// SSP 11/11/03 Add Row Feature
						//
						//if ( vr.Row.HasNextSibling(false, true) ||
						if ( vr.Row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) ||
							vr.Row.HasSiblingInNextSiblingBand(true,true)
							// SSP 7/26/05 - NAS 5.3 Header Placement
							// 
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//&& ! this.IsNextSiblingRowFromGroupByRowsCollection( vr.Row ) 
							&& !RowColRegionIntersectionUIElement.IsNextSiblingRowFromGroupByRowsCollection( vr.Row ) 
							)
							adjustConnector      = true;
					}
					else
					{
						// check for a band break
						//
						// SSP 10/1/01
						if ( lastRow.ParentCollection != vr.Row.ParentCollection )
						//if ( lastBand != band )
						{
							// if we are going up in the band level or the elem row
							// has a header and we are in vertical view then
							// null out our table entry from the band's level down
							// to the last band's level.
							//
							// This will stop our adjustment of the row connectors
							// we may have cached at each level.
							//
							if ( //bandLevel < lastBandLevel || 
								// JJD 1/19/02 - UWG940
								// Check against noncards only
								 //rowLevel < lastVisibleRowLevel ||
								 rowLevel < lastNonCardVisibleRowLevel ||
								!viewStyle.HasMultiRowTiers )
							{

								bool parentToChildConnectorAdded = false;

								// SSP 5/25/05 - NAS 5.2 Fixed Rows
								//
								// ----------------------------------------------------------------------
								VisibleRow ivr = vr;
								if ( null == connectors[ bandLevel ] )
								{
									UltraGridRow prevSiblingRow = ivr.Row.GetVisibleSiblingRow( false, true, IncludeRowTypes.SpecialRows );
									if ( null != ivr.ParentVisibleRow )
									{
										if ( ! visibleRows.HasAssociatedVisibleRow( ivr.ParentVisibleRow.Row )
											&& ! ivr.ParentVisibleRow.Row.IsFromGroupByRowsParentCollection 
											// SSP 7/26/05 - NAS 5.3 Header Placement
											// If the row has a prev sibling row then the line has to be connected to it
											// rather than the parent row.
											// 
											&& null == prevSiblingRow
											|| null != prevSiblingRow && ! visibleRows.HasAssociatedVisibleRow( prevSiblingRow )
											&& ! prevSiblingRow.IsFromGroupByRowsParentCollection 
											// SSP 11/17/05 BR07106
											// Also check for card-row.
											// 
											&& ! prevSiblingRow.IsCard )
										{
											rcWork = rcRowConnector;
											// SSP 7/21/05 - NAS 5.3 Header Placement
											// Use the rowConnectorCeilings.
											// 
											//rcWork.Y = topFixedRows_Bottom;
											// MD 7/26/07 - 7.3 Performance
											// FxCop - Mark members as static
											//rcWork.Y = rowConnectorCeilings[ this.CalcVisibleRowLevel( ivr ) ];
											rcWork.Y = rowConnectorCeilings[ RowColRegionIntersectionUIElement.CalcVisibleRowLevel( ivr ) ];

											int expansionIndicatorHeight = hasExpansionIndicator ? UltraGridBand.EXPANSION_INDICATOR_WIDTH : 0;
											int tmpBottom = rcRowConnector.Y - expansionIndicatorHeight;
											rcWork.Height = tmpBottom - rcWork.Y;

											if ( band != ivr.Band )
											{
												int offset = ivr.Band.GetOrigin() - band.GetOrigin();
												rcWork.Offset( offset, 0 );
											}

											AddRowConnectorHelper(
												newConnectors, oldElements, rcWork,
												band, ivr.Row, false );

											// Adjust the rcLastRow because the code further below relies on it being set up in
											// a certain manner.
											// 
											//rcLastRow.Height = tmpBottom + expansionIndicatorHeight - rcLastRow.Top;

											parentToChildConnectorAdded = true;
										}
									}
								}
								// ----------------------------------------------------------------------

								// first adjust the cached row connector at this 
								// level
								//
								if ( connectors[bandLevel] != null)
								{
									rcWork = connectors[bandLevel].Rect;
									rcWork.Height       = rcRowConnector.Top - rcWork.Top;
									rcLastRow.Height    = rcRowConnector.Top - rcLastRow.Top;
                                    
									// JJD 10/04/00 - ult1898
									// Adjust rcWork.Bottom to handle 1 pixel off bug
									//
									if ( hasExpansionIndicator )
										rcWork.Height      -= UltraGridBand.EXPANSION_INDICATOR_WIDTH - 1;

									connectors[bandLevel].Rect = rcWork;
								}

								// clear out the cached level ptrs
								//
								for ( index = bandLevel; index <= lastBandLevel; index++ )
									connectors[index] = null;								

								// for vertical views we need to add an extra connector
								// for our parent row when the band level increased
								// (we are drilling down to a new level)
								//
								// SSP - 10/19/01 
								//
								//if ( !viewStyle.HasMultiRowTiers && !( lastRow is GroupByRow ) )
								//if ( !( vr.Row is UltraGridGroupByRow) && !viewStyle.HasMultiRowTiers && !( lastRow is UltraGridGroupByRow ) )
								// SSP 3/27/02
								// Added !vr.Row.HiddenResolved condition.
								// 
								// SSP 5/25/05 - NAS 5.2 Filter Row/Fixed Rows
								// Last row could be fixed in which case it is possible that there is no relationship
								// between the last row and the current row. So don't assume it's the parent row of
								// the current row. Also there could be a previous sibling row that could be scrolled
								// underneath the top fixed rows. Account for that as well.
								// 
								UltraGridRow rowConnectorRowOverride = null;
								if ( !vr.Row.HiddenResolved &&
									// SSP 5/25/05 - NAS 5.2 Filter Row/Fixed Rows/Extension of Summaries Functionality
									// We want to check if the row is from a group-by rows collection here. For example a
									// filter row is not a group-by row however it may be part of a group-by rows collection
									// in which case we should not be adding any row connectors.
									//
									// ----------------
									//!( vr.Row is UltraGridGroupByRow) 
									! isRowFromGroupByRowsParentCollection
									// ----------------
									&& !viewStyle.HasMultiRowTiers 

									// SSP 5/25/05 - NAS 5.2 Filter Row/Fixed Rows
									// We want to check if the row is from a group-by rows collection here. For example a
									// filter row is not a group-by row however it may be part of a group-by rows collection
									// in which case we should not be adding any row connectors.
									//
									// --------------------------------------------------------------------------------
									//&& !( lastRow is UltraGridGroupByRow ) 
									&& ! lastRow.IsFromGroupByRowsParentCollection
									&& ! parentToChildConnectorAdded 
									// --------------------------------------------------------------------------------
									// SSP 7/26/05 - NAS 5.3 Header Placement
									// 
									// MD 7/26/07 - 7.3 Performance
									// FxCop - Mark members as static
									//&& ! this.IsPrevSiblingRowFromGroupByRowsCollection( vr.Row )
									&& !RowColRegionIntersectionUIElement.IsPrevSiblingRowFromGroupByRowsCollection( vr.Row )
									)
								{
									int oldTop              = rcRowConnector.Top;
									rcRowConnector.Y        = rcLastRow.Bottom;
									rcRowConnector.Height   = oldTop - rcLastRow.Bottom;
								

									if ( hasExpansionIndicator )
										rcRowConnector.Height -= UltraGridBand.EXPANSION_INDICATOR_WIDTH;

									rcRowConnector.Height = Math.Max ( 1, rcRowConnector.Height );

									if ( //bandLevel > lastBandLevel ||
										// JJD 1/19/02 - UWG940
										// Check against noncards only
										//rowLevel > lastVisibleRowLevel )
										rowLevel > lastNonCardVisibleRowLevel )
									{
										parentVisibleRow = vr.ParentVisibleRow;

										// SSP 10/1/01
										//
										// SSP 5/25/05 - NAS 5.2 Filter Row/Fixed Rows
										// Commented out the below condition and added new one. All we need to
										// check for is whether the current row has a parent row. Other conditions
										// are checked in the grand parent if condition.
										//
										
										if ( null != parentVisibleRow )
										{
											rcWork  = rcRowConnector;

											// add the connector back to our parent row
											//
											AddRowConnectorHelper( newConnectors, 
												oldElements,
												rcWork,
												// SSP 5/25/05 - NAS 5.2 Filter Row/Fixed Rows
												// Last row could be fixed in which case it is possible that there is no relationship
												// between the last row and the current row. So don't assume it's the parent row of
												// the current row. Also there could be a previous sibling row that could be scrolled
												// underneath the top fixed rows. Account for that as well.
												// 
												// ------------------------------------------------------------------------
												//parentVisibleRow.Band,
												//parentVisibleRow.Row,
												null == rowConnectorRowOverride ? parentVisibleRow.Band : rowConnectorRowOverride.Band,
												null == rowConnectorRowOverride ? parentVisibleRow.Row : rowConnectorRowOverride,
												// ------------------------------------------------------------------------
												true );

											rcRowConnector.Y = rcWork.Bottom;
                            
											if ( hasExpansionIndicator )
												rcRowConnector.Y        += UltraGridBand.EXPANSION_INDICATOR_WIDTH;

											rcRowConnector.Height = 1;

										}
									}

									adjustConnector     = true;
								}
								else
									// SSP 11/11/03 Add Row Feature
									//
									//if ( vr.Row.HasNextSibling(false, true) ||
									if ( vr.Row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) 
										|| vr.Row.HasSiblingInNextSiblingBand(true,true) 
										// SSP 7/26/05 - NAS 5.3 Header Placement
										// 
										// MD 7/26/07 - 7.3 Performance
										// FxCop - Mark members as static
										//&& ! this.IsNextSiblingRowFromGroupByRowsCollection( vr.Row ) 
										&& !RowColRegionIntersectionUIElement.IsNextSiblingRowFromGroupByRowsCollection( vr.Row ) 
										)
								{
									adjustConnector      = true;
								}
							}
							else
							{
								adjustConnector      = true;
							}
						}
						else
						{
							// SSP 11/11/03 Add Row Feature
							//
							//if ( vr.Row.HasPrevSibling(false, true) ||
							if ( vr.Row.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) ||
								// SSP 11/11/03 Add Row Feature
								//
								//vr.Row.HasNextSibling(false, true) ||
								vr.Row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) 
								|| vr.Row.HasSiblingInNextSiblingBand(true,true) 
								// SSP 7/26/05 - NAS 5.3 Header Placement
								// 
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//&& ! this.IsNextSiblingRowFromGroupByRowsCollection( vr.Row ) 
								&& !RowColRegionIntersectionUIElement.IsNextSiblingRowFromGroupByRowsCollection( vr.Row ) 
								|| vr.Row.HasSiblingInPrevSiblingBand(true,true)
								// SSP 7/26/05 - NAS 5.3 Header Placement
								// 
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//&& ! this.IsPrevSiblingRowFromGroupByRowsCollection( vr.Row ) 
								&& !RowColRegionIntersectionUIElement.IsPrevSiblingRowFromGroupByRowsCollection( vr.Row ) 
								)
							{
								adjustConnector      = true;
							}
							else
								connectors[index] = null;
						}
					}

					//
					// SSP 10/2/01
					//if ( !vr.Row.HasNextSibling( false, true ) && 
					//	!vr.Row.HasSiblingInNextSiblingBand(true,true))
					//{
					//	adjustConnector = false;
						//connectors[bandLevel] = null;
					//}

					if ( adjustConnector )
					{
						// adjust the rect of the cached row connector at this level
						//
						if ( connectors[bandLevel] != null )
						{
							rcWork = connectors[bandLevel].Rect;
							rcWork.Height = rcRowConnector.Top - rcWork.Top;
                                    
							if ( hasExpansionIndicator )
								rcWork.Height -= UltraGridBand.EXPANSION_INDICATOR_WIDTH;
							else
								// JJD 10/04/00 - ult1898
								// Adjust rcWork.Bottom to handle 1 pixel off bug
								//
								rcWork.Height++;

							connectors[bandLevel].Rect = rcWork;
						}

						// add a connector for this level if this row has a
						// next sibling
						//
						// SSP 11/9/01  UWG651
						//if ( !isRowGroupByRow && ( vr.Row.HasNextSibling(false, true) ||
						//	vr.Row.HasSiblingInNextSiblingBand(true,true) ) )
						if ( 
							// SSP 5/25/05 - NAS 5.2 Filter Row/Fixed Rows/Extension of Summaries Functionality
							// We want to check if the row is from a group-by rows collection here. For example a
							// filter row is not a group-by row however it may be part of a group-by rows collection
							// in which case we should not be adding any row connectors.
							//
							// --------
							//!isRowGroupByRow && 
							! isRowFromGroupByRowsParentCollection &&
							// --------
							// MD 8/2/07 - 7.3 Performance
							// Reuse cached bool value of this test
							//!( null != vr.Row.ParentRow &&
							//   vr.Row.ParentRow is UltraGridGroupByRow &&
							!( isParentRowGroupByRow &&
							   vr.Row.Band.GetChildBandCount( true ) <= 0
							 ) 
							&&
							// SSP 11/11/03 Add Row Feature
							//
							//( vr.Row.HasNextSibling(false, true) ||
							( vr.Row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) ||
							vr.Row.HasSiblingInNextSiblingBand(true,true)
							// SSP 7/26/05 - NAS 5.3 Header Placement
							// 
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//&& ! this.IsNextSiblingRowFromGroupByRowsCollection( vr.Row ) 
							&& !RowColRegionIntersectionUIElement.IsNextSiblingRowFromGroupByRowsCollection( vr.Row ) 
							) )
						{
							// JJD 9/25/00 - ult1147
							// Set tip enabled for row below if the OLD row
							// connector was below the region rect
							//
							if ( connectors[bandLevel] != null && 
								rcRowConnector.Bottom > thisRect.Bottom )
								((SiblingRowConnectorUIElement)connectors[bandLevel]).SetRowBelowTipEnabled( true );

							// add the row connector
							//
							connectors[bandLevel] = AddRowConnectorHelper( newConnectors, 
								oldElements,
								rcRowConnector,
								band,
								vr.Row,
								false );
							// JJD 9/25/00 - ult1147
							// Set tip enabled for row below if the NEW row
							// connector was below the region rect
							//
							if ( connectors[bandLevel] != null&& 
								rcRowConnector.Bottom > thisRect.Bottom )
								((SiblingRowConnectorUIElement)connectors[bandLevel]).SetRowBelowTipEnabled( true );
						}
							// SSP 10/22/01
							// set the connectors[bandLevel] to null otherwise
							//
						else
						{
							connectors[bandLevel] = null;
						}

						// also adjust the connectors for all band levels
						// higher than this level (extending them downward
						//
						parentVisibleRow = vr.ParentVisibleRow;

						while ( parentVisibleRow != null )
						{
							if ( !(parentVisibleRow.Row is UltraGridGroupByRow ) 
								// SSP 11/11/03 Add Row Feature
								//
								//&&  ( parentVisibleRow.Row.HasNextSibling(false,true) ||
								&&  ( parentVisibleRow.Row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) ||
								parentVisibleRow.Row.HasSiblingInNextSiblingBand(true,true) ) )
							{
								index = parentVisibleRow.Band.HierarchicalLevel;

								if ( connectors[index] != null )
								{
									rcWork = connectors[index].Rect;
									rcWork.Height = rcRowConnector.Top - rcWork.Top;
                                    
									if ( hasExpansionIndicator && rcWork.Height > UltraGridBand.EXPANSION_INDICATOR_WIDTH - 1 )
										rcWork.Height -= UltraGridBand.EXPANSION_INDICATOR_WIDTH - 1;

									connectors[index].Rect = rcWork;
								}
							}

							parentVisibleRow = parentVisibleRow.ParentVisibleRow;
						}
					}

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Remove unused locals
					//lastBand       = band;

					lastRow        = vr.Row;
					lastBandLevel  = bandLevel;

					// SSP 7/6/05 - NAS 5.3 Empty Rows
					// Moved this outside of this if block. Set the rcLastRow regardless of whether
					// multiple bands are displayed or not since the empty rows feature needs to use
					// it now.
					// 
					//rcLastRow      = rcRow;

					// JJD 11/19/02 - UWG940
					// Keep track of non-cards
					//
					lastNonCardVisibleRow = vr;
				}

				// SSP 7/6/05 - NAS 5.3 Empty Rows
				// 
				rcLastRow = rcRow;
			}

			// SSP 7/6/05 - NAS 5.3 Empty Rows
			// Add the empty rows if the functionality has been enabled.
			// 
			bool emptyRowsBeingDisplayed = false;
			if ( showEmptyRows )
			{
				//if ( rowAdded  )
				//{
					int endEmptyRowsAt = bottomFixedRows_Top;

					// EmptyRowsAreaUIElement contains the empty rows. This element spans the 
					// row col region intersection element.
					// 
					Rectangle rect = thisRect;
					rect.Y = startEmptyRowsAt;
					rect.Height = endEmptyRowsAt - rect.Y;
					if ( rect.Height > 0 )
					{
						this.AddEmptyRowsUIElement( oldElements, rect );
						emptyRowsBeingDisplayed = true;
					}
				//}
			}

			// now create any group by indent ui elements that needs to be
			// created for each level
			//
			for ( i = 0; null != levelRects && i < levelRects.Length; i++ )
			{				
				// skip the levels that are associated with regular
				// rows as they do not have group by connectors
				//
				if ( levelRects[ i ].IsEmpty )
					continue;

				// also skip the levels with levelFlags[ i ] set to false
				// indicating the last row in that level was not expanded, 
				// and thus it does not need a group by row connector
				//
				if ( !levelFlags[ i ] )
					continue;


				// now create the group by indent ui element for the level
				//
				GroupByRowConnectorUIElement groupByRowConnector = (GroupByRowConnectorUIElement)
				RowColRegionIntersectionUIElement.ExtractExistingElement( oldElements, 
					typeof( GroupByRowConnectorUIElement ), true );
							
				if ( null == groupByRowConnector )
					groupByRowConnector = new GroupByRowConnectorUIElement( this );

				Debug.Assert( null != levelVisibleRows[ i ] &&
					( levelVisibleRows[ i ].Row is UltraGridGroupByRow ) );


				groupByRowConnector.Initialize( levelVisibleRows[ i ].Row as UltraGridGroupByRow );

				Rectangle tmpRect = levelRects[i];

				// SSP 5/25/05 - NAS 5.2 Fixed Rows
				// We need to clip the connectors so they don't overlap the fixed rows.
				// 
				tmpRect.Y = Math.Max( tmpRect.Y, topFixedRows_Bottom );

				// SSP 10/30/03 UWG2454
				// Summary footers have to be taken into account when addedin group-by-row connectors.
				//
				// ------------------------------------------------------------------------------------
				//tmpRect.Height = this.RectInsideBorders.Bottom - tmpRect.Top;

				// SSP 5/25/05 - NAS 5.2 Fixed Rows
				// We need to clip the connectors so they don't overlap the fixed rows.
				//
				//int connectorBottom = this.RectInsideBorders.Bottom;
				int connectorBottom = bottomFixedRows_Top;

				// SSP 7/6/05 - NAS 5.3 Empty Rows
				// Also make sure that the group-by row connector doesn't span beyond where the empty 
				// rows start at.
				// 
				if ( emptyRowsBeingDisplayed )
					connectorBottom = Math.Min( connectorBottom, startEmptyRowsAt );

				if ( null != summaryFooterElementTops )
				{
					for ( j = 0; j < i && j < summaryFooterElementTops.Length; j++ )
					{
						if ( summaryFooterElementTops[j] > tmpRect.Top && summaryFooterElementTops[j] > 0 
							&& connectorBottom > summaryFooterElementTops[j] )
							connectorBottom = summaryFooterElementTops[j];
					}
				}
							
				tmpRect.Height = connectorBottom - tmpRect.Top;
				// ------------------------------------------------------------------------------------

				groupByRowConnector.Rect = tmpRect;

				this.ChildElements.Add( groupByRowConnector );				
			}


			if ( multipleBandsDispayed && lastRow != null && hasRowConnectors )
			{
				Infragistics.Win.UltraWinGrid.UltraGridRow row = lastRow;

				// walk up the last row's parent chain to see if we
				// need to extend the row connectors to the bottom of the
				// meta region
				//
				while ( row != null )
				{
					if ( !( row is UltraGridGroupByRow ) 
						// SSP 11/11/03 Add Row Feature
						//
						//&& ( ( row.HasNextSibling(false, true) ||
						&& ( ( row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) ||
						row.HasSiblingInNextSiblingBand(true,true) ) ) )
					{
						index = row.Band.HierarchicalLevel;

						if ( connectors[index] != null )
						{
							rcWork = connectors[index].Rect;
							rcWork.Height = thisRect.Bottom - rcWork.Top;

							connectors[index].Rect = rcWork;

							// JJD 9/13/00 - ult1147
							// Set tip enabled for row below
							//
							((SiblingRowConnectorUIElement)connectors[index]).SetRowBelowTipEnabled( true );
						}
					}

					row = row.ParentRow;
				}
			}


			// if we have fixed headers add those elements now
			//
			// Note: the reason we add the rows first is that
			//       in case we overlap the top border of the
			//       rows on the first tier, we want to draw
			//       the fixed headers afterwards so we see the
			//       header's bottom border instead of the row's
			//       top border.
			//			
			// SSP 11/6/01 UWG676
			// Added rowAdded flag that get's set when a row is added
			//
			//this.PositionFixedHeaders( oldElements, rowElement != null || null != groupByRowElement );
			// SSP 7/26/05 - NAS 5.3 Header Placement
			// Figure out the row connector ceilings. In order to do that we need to add fixed headers
			// first. However in order to maintain previous behavior of drawing headers over rows (in 
			// case of single pixel border overlap), make sure that the header elems are added afeter
			// the row elems.
			//
			// ----------------------------------------------------------------------------------------
			//this.PositionFixedHeaders( oldElements, rowAdded );
			if ( null != fixedBandHeaderElems )
				this.ChildElements.AddRange( fixedBandHeaderElems );
			// If no fixed headers have been added and no rows have been added then add the root band
			// header.
			// 
			else if ( ! rowAdded )
				this.PositionFixedHeaders( oldElements, false, null, null, null, out fixedHeadersBottom );
			// ----------------------------------------------------------------------------------------

			// Copy the connectors over into the child elements collection.
			//
			// We do this at the end so they get added last and therefore
			// get priority during hit testing.
			//
			if ( hasRowConnectors && newConnectors != null )
			{
                // MRS 3/26/2009 - TFS15233
                //for (i = 0; i < newConnectors.Count; i++)
                //{
                //    this.ChildElements.Add(newConnectors[i]);
                //}
                //
                // Deterine the first fixed-row element index. 
                int firstFixedObjectIndex = int.MaxValue;
                int childCount = this.ChildElements.Count;
                for (int k = 0; k < childCount; k++)
                {
                    UIElement element = this.ChildElements[k];
                    UltraGridRow row = element.GetContext(typeof(UltraGridRow)) as UltraGridRow;
                    if (row != null &&
                        row.FixedResolvedLocation == FixedRowStyle.Bottom)
                    {
                        firstFixedObjectIndex = k;
                        break;
                    }                    
                }
                
                // Insert the RowConnectors into the UIElements collection before the 
                // first Fixed Row Element so that the fixed elements have the higher
                // ZOrder. 
                //
                firstFixedObjectIndex = Math.Min(this.ChildElements.Count, firstFixedObjectIndex);
                this.ChildElements.InsertRange(firstFixedObjectIndex, newConnectors);
				
			}

			// SSP 11/10/04 - Merged Cell Feature
			// Add merged cell elements after adding all the row elements.
			//
			// ----------------------------------------------------------------------------------------
			this.PositionMergedCellElements( oldMergedCellElems );

			// Dispose any unused merged cell elements.
			//
			if ( null != oldMergedCellElems )
			{
				// SSP 5/17/05 - Optimization
				// Call the method off the UIElements collection instead to reduce duplication of code.
				//
				oldMergedCellElems.DisposeElements( );
				
			}
			// ----------------------------------------------------------------------------------------

			// Call dispose on any of the old elements that have not been used.
			// SSP 5/17/05 - Optimization
			// Call the method off the UIElements collection instead to reduce duplication of code.
			//
			oldElements.DisposeElements( );
			

			// SSP 5/3/05 - Optimizations
			// 
			this.verifiedRowChildElementsCacheVersion = layout.RowChildElementsCacheVersion;
		}

		private SiblingRowConnectorUIElement AddRowConnectorHelper( UIElementsCollection newConnectors, 
                                                                      UIElementsCollection oldElements,
                                                                      Rectangle rcRowConnector,
                                                                      UltraGridBand  parentBand,
                                                                      Infragistics.Win.UltraWinGrid.UltraGridRow  parentRow,
                                                                      bool firstChildConnector )
        {

            if ( !rcRowConnector.IntersectsWith( this.rectValue ) )
                return null;

			// SSP 4/25/05 - NAS 5.2 Filter Row
			// Now a filter row can be displayed in a group-by row collection. If that's the
			// case then that row should not draw any row connectors.
			//
			if ( null != parentRow && parentRow.IsFromGroupByRowsParentCollection )
				return null;

            // see if we have one in the old collection
            //
            SiblingRowConnectorUIElement rowConnectorElement = (SiblingRowConnectorUIElement)RowColRegionIntersectionUIElement.ExtractExistingElement( oldElements, typeof(SiblingRowConnectorUIElement), true );

            // if not, create one
            //
            if ( rowConnectorElement == null )
                rowConnectorElement = new SiblingRowConnectorUIElement( this );

            // initialize and set its rect
            //
            rowConnectorElement.Initialize( parentBand, parentRow, firstChildConnector );

            rowConnectorElement.Rect = rcRowConnector;

            newConnectors.Add( rowConnectorElement );

            return rowConnectorElement;
        }

		private void PositionFixedHeaders( UIElementsCollection oldElements, bool rowsHaveBeenAdded,
			// SSP 7/21/05 - NAS 5.3 Header Placement
			// 
			Rectangle[] levelRects, bool[] levelFlags, int[] rowConnectorCeilings, out int headersBottom )
        {
			// SSP 8/4/05 - NAS 5.3 Empty Rows
			// 
			headersBottom = this.Rect.Top;

            ViewStyleBase viewStyle = this.rsr.Layout.ViewStyleImpl;

            if ( !viewStyle.HasFixedHeaders )
            {
                // JJD 9/16/00 - ult1092
                // If there aren't any rows add a band 0 header anyway
                //
                if ( !rowsHaveBeenAdded && this.ColScrollRegion.Layout.SortedBands.Count > 0)
                {
                    BandHeadersUIElement bandHeadersElem = this.AddBandHeaderHelper( oldElements, 
                                              this.rsr.Layout.SortedBands[0],
                                              null, 0, false, 
											  // SSP 8/22/02 UWG1554
											  // Pass in true for addSummaryRow												
											  true );

					// SSP 8/4/05 - NAS 5.3 Empty Rows
					// 
					if ( null != bandHeadersElem )
						headersBottom = bandHeadersElem.Rect.Bottom;
                }
            }

            int     headerBandCount     = 0;
			// SSP 5/31/02 UWG1152
			// Use the new MaxBandDepthEnforced property instead of MAX_BAND_DEPTH.
			// 
			//UltraGridBand [] headerBands         = new UltraGridBand[ UltraGridBand.MAX_BAND_DEPTH ];
			UltraGridBand [] headerBands         = new UltraGridBand[ this.RowScrollRegion.Layout.MaxBandDepthEnforced ];           
            
            int fixedHeaderHeight = viewStyle.GetFixedHeaderHeight();

			// JJD 10/18/01 - UWG542
			// For single band displays force the headers to be left aligned
			//
			bool forceAlignLeft = !viewStyle.IsMultiBandDisplay;

            // Determine which bands appear in the fixed header area
            //
            viewStyle.GetFixedHeaderBands( this.RowScrollRegion.VisibleRows, 
                                           headerBands, 
                                           ref headerBandCount );

			// SSP 3/24/05 BR02974
			// While performing pixel level scrolling (by holding down the mouse over the down
			// scroll arrow), the row element may end up being underneath the header elements.
			// If there was alphablending on headers then the row would show through. Also in
			// row layout mode with gaps the row would show through as well. To fix it we need
			// to set the row element's region to exclude the band headers area element rect.
			// Commented out the original code and added the new code below it.
			//
			// ----------------------------------------------------------------------------------
			//for ( int i = 0; i < headerBandCount; i++ )
			//	this.AddBandHeaderHelper( oldElements, headerBands[i], null, fixedHeaderHeight, forceAlignLeft );

			int maxHeadersBottom = -1;
			// SSP 7/18/05 - NAS 5.3 Header Placement
			//
			int offsetFromTop = 0;

            for ( int i = 0; i < headerBandCount; i++ )
            {
				UltraGridBand band = headerBands[i];
				if ( null == band )
					continue;

				// SSP 4/12/06 BR11361
				// 
				Rectangle headerElemRect;

				BandHeadersUIElement bandHeadersElem = this.AddBandHeaderHelper( 
					oldElements, band, null, fixedHeaderHeight, forceAlignLeft,
					// SSP 7/18/05 - NAS 5.3 Header Placement
					// Added an overload that takes in offsetFromRsrTop parameter. Now the vertical band can have
					// fixed headers. Since they are stacked vertically we need to pass in some info on where the
					// next header should start.
					// 
					false, offsetFromTop,
					// SSP 4/12/06 BR11361
					// 
					out headerElemRect );

				// SSP 4/12/06 BR11361
				// 
				if ( ! headerElemRect.IsEmpty )
					maxHeadersBottom = Math.Max( maxHeadersBottom, headerElemRect.Bottom ); 

				if ( null != bandHeadersElem )
				{
					// SSP 4/12/06 BR11361
					// Moved this above.
					// 
					//maxHeadersBottom = Math.Max( maxHeadersBottom, bandHeadersElem.Rect.Bottom ); 

					// SSP 7/26/05 - NAS 5.3 Header Placement
					// This is for making sure that the row connectors do not go over the fixed headers.
					// 
					this.AddAttachedHeaderHelper( bandHeadersElem, levelRects, levelFlags, rowConnectorCeilings, -1 );
				}

				// SSP 7/18/05 - NAS 5.3 Header Placement
				// Now the vertical view style can have fixed headers. Since they are stacked 
				// vertically we need to pass in some info on where the next header should start.
				// 
				if ( ! viewStyle.HasMultiRowTiers )
					offsetFromTop = maxHeadersBottom - this.Rect.Top;
            }

			// SSP 8/4/05 - NAS 5.3 Empty Rows
			// 
			headersBottom = Math.Max( maxHeadersBottom, headersBottom );

			// Get the first row element. This should be the top most row element as well.
			//
			RowUIElementBase rowElem = (RowUIElementBase)this.GetDescendant( typeof( RowUIElementBase ) );
			if ( null != rowElem )
			{
				// Allow the row's top border to be merged with the headers' bottom borders.
				//
				maxHeadersBottom--;

				Rectangle rowElemRect = rowElem.Rect;
				if ( rowElemRect.Y < maxHeadersBottom )
				{
					int delta = Math.Min( maxHeadersBottom, rowElemRect.Bottom ) - rowElemRect.Y;
					rowElemRect.Y += delta;
					rowElemRect.Height -= delta;
					rowElem.InternalSetClipSelfRegion( rowElemRect );
				}
			}
			// ----------------------------------------------------------------------------------
        }

		#region ExtractRowElement

		// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
		// Added ExtractRowElement method.
		// 
		internal static UIElement ExtractRowElement( UIElement parent, UIElementsCollection elements, UltraGridRow row )
		{
			if ( null != elements )
			{
				UIElement elem = null;
				int lastElemIndex = -1;
				int i, count;

				for ( i = 0, count = elements.Count; i < count; i++ )
				{
					elem = elements[ i ];
					if ( null != elem )
					{
						if ( row.HasSameContext( parent, elem ) )
						{
							elements[ i ] = null;
							return elem;
						}

						lastElemIndex = i;
					}
				}

				UltraGridCell cellInEditMode = row.Layout.CellInEditMode;
				UltraGridRow skipRow = null != cellInEditMode && cellInEditMode.Row != row ? cellInEditMode.Row : null;
				for ( i = lastElemIndex; i >= 0; i-- )
				{
					elem = elements[ i ];
					if ( null != elem && row.IsUIElementCompatible( parent, elem ) && ( null == skipRow || ! skipRow.HasSameContext( parent, elem ) ) )
					{
						// Move the active row element, the one we may have had to skip up to where 
						// the current elem is.
						//
						if ( i != lastElemIndex )
							elements[ i ] = elements[ lastElemIndex ];

						elements[ lastElemIndex ] = null;

						return elem;
					}
				}
			}

			return null;
		}

		#endregion // ExtractRowElement

		#region AddEmptyRowsUIElement

		// SSP 7/6/05 - NAS 5.3 Empty Rows
		// Added AddEmptyRowsUIElement method.
		// 
		private void AddEmptyRowsUIElement( UIElementsCollection oldElements, Rectangle rect )
		{
			if ( rect.Height < 0 || rect.Width < 0 )
				return;

			EmptyRowsAreaUIElement emptyRowsAreaElem = (EmptyRowsAreaUIElement)RowColRegionIntersectionUIElement.ExtractExistingElement( oldElements, typeof( EmptyRowsAreaUIElement ), true );
			if ( null == emptyRowsAreaElem )
				emptyRowsAreaElem = new EmptyRowsAreaUIElement( this );

			emptyRowsAreaElem.Rect = rect;
			this.ChildElements.Add( emptyRowsAreaElem );
		}

		#endregion // AddEmptyRowsUIElement

		// SSP 7/7/05 - NAS 5.3 Empty Rows
		// Added an overload that takes in parentElement parameter.
		// 
		private UIElement AddRowHelper( 
			UIElementsCollection oldElements,
			// SSP 5/25/05 - NAS 5.2 Special Row Separators/Extension of Summaries Functionality
			// Made rcElement paremter by ref. This method may modify it to be the actual bounding rect
			// of the row related elements (row element and any other elements like the special row
			// separators).
			// 
			//Rectangle rcElement,
			ref Rectangle rcElement,
			VisibleRow visibleRow,
			bool overlapTopBorder )
		{
			return this.AddRowHelper( this, oldElements, ref rcElement, visibleRow, overlapTopBorder, 
				this.verifiedRowChildElementsCacheVersion == this.rsr.Layout.RowChildElementsCacheVersion );
		}
		
		internal UIElement AddRowHelper( 
			// SSP 7/7/05 - NAS 5.3 Empty Rows
			// Added parentElement parameter. Empty rows are parented by a different ui element.
			// 
			UIElement parentElement,

			UIElementsCollection oldElements,
			// SSP 5/25/05 - NAS 5.2 Special Row Separators/Extension of Summaries Functionality
			// Made rcElement parameter by ref. This method may modify it to be the actual bounding rect
			// of the row related elements (row element and any other elements like the special row
			// separators).
			// 
			//Rectangle rcElement,
			ref Rectangle rcElement,
			VisibleRow visibleRow,
			bool overlapTopBorder,
			
			// SSP 7/7/05 - NAS 5.3 Empty Rows
			// Added offsetElementIfPossible parameter. Empty rows are parented by a different ui element.
			// 
			bool offsetElementIfPossible
			)
		{
			UIElement	element = null;
			UltraGridRow	row = visibleRow.Row;

			// SSP 4/16/05 - NAS 5.2 Summaries Extention
			//
			row.AdjustUIElementRect( ref rcElement );

			// SSP 7/7/05 - NAS 5.3 Empty Rows
			// Added parentElement parameter. Empty rows are parented by a different ui element.
			//
			//if ( !rcElement.IntersectsWith( this.rectValue ) )
			if ( !rcElement.IntersectsWith( parentElement.Rect ) )
				return null;

			// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
			// Moved the logic for row extraction into the new static ExtractRowElement method.
			// 
			// ----------------------------------------------------------------------------------
			element = RowColRegionIntersectionUIElement.ExtractRowElement( this, oldElements, row );
			
			// ----------------------------------------------------------------------------------

			// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
			// Moved the logic for row extraction into the new static ExtractRowElement method.
			// 
			//if ( element != null )
			// SSP 7/7/05 - NAS 5.3 Empty Rows
			// Added parentElement parameter. Empty rows are parented by a different ui element.
			//
			//Rectangle newClipRect = Rectangle.Intersect( this.ClipRect, rcElement );
			Rectangle newClipRect = Rectangle.Intersect( parentElement.ClipRect, rcElement );

			if ( null != element 
				// SSP 7/7/05 - NAS 5.3 Empty Rows
				// Added offsetElementIfPossible parameter. Empty rows are parented by a different ui element.
				// 
				//&& this.verifiedRowChildElementsCacheVersion == this.rsr.Layout.RowChildElementsCacheVersion 
				&& offsetElementIfPossible 
				&& row.HasSameContext( this, element ) )
			{
				Rectangle oldRect = element.Rect;

				// SSP 5/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				// We want to check if the clip rect when the last time the element was positioned
				// is the same as this time.
				//
				
				bool offsetElement = oldRect.Left == rcElement.Left && oldRect.Size == rcElement.Size;

				// MD 12/9/08 - 9.1 - Column Pinning Right
				// If there are right-fixed headers, the element should not be offset anymore because even though the rect hasn't changed, 
				// the row could have been scrolled.
				HeaderBase lastHeader = row.Band.OrderedHeaders.LastVisibleHeader;
				if ( lastHeader != null &&
					lastHeader.FixOnRightResolved &&
					row.Band.IsHeaderFixed( this.ColScrollRegion, lastHeader ) )
				{
					offsetElement = false;
				}

				int deltaXX = rcElement.Left - oldRect.Left;
				int deltaYY = rcElement.Top - oldRect.Top;

				if ( offsetElement )
				{
					Rectangle lastClipRect = row.GetElementLastClipRect( element );
					if ( ! lastClipRect.IsEmpty )
					{
						lastClipRect.Offset( deltaXX, deltaYY );
						offsetElement = newClipRect == lastClipRect;
					}
					else
						offsetElement = false;

					// Change in overlapTopBorder also invalidates the caching.
					//
					RowUIElementBase rowElem = element as RowUIElementBase;
					if ( null != rowElem && rowElem.OverlapTopBorder != overlapTopBorder )
						offsetElement = false;
				}

				// If the size of the element is the same as the new size
				// we just offset the element (which is more efficient).
				// Otherwise we set its rect which will dirty it
				//
				// SSP 5/16/05
				//
				//if ( oldRect.Left == rcElement.Left && oldSize == rcElement.Size )
				if ( offsetElement )
					element.Offset( deltaXX, deltaYY, true );
				else
				{
					// Need to call InitializeRow since we cache the parent row's
					// size for comparison later
					//
					// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					// Use the new InitializeUIElement method off the row instead.
					//
					// ------------------------------------------------------------------------
					row.InitializeUIElement( element );
					
					// ------------------------------------------------------------------------

					element.Rect = rcElement;
				}

				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// This same code is duplicated at the end of this method. Use that code
				// instead.
				//
				
			}
			else
			{
				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// Moved the logic for row extraction into the new static ExtractRowElement method.
				// 
				// ----------------------------------------------------------------------------------
				
				// ----------------------------------------------------------------------------------

				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// Use the new IsUIElementCompatible, CreateRowUIElement and InitializeUIElement 
				// methods off the row.
				//
				// ------------------------------------------------------------------------------
				if ( null == element )
					// SSP 8/16/06 - NAS BR15058
					// Pass in the correct parent element that's going to be hosting the row element.
					// 
					//element = row.CreateRowUIElement( this );
					element = row.CreateRowUIElement( parentElement );

				row.InitializeUIElement( element );
				
				// ------------------------------------------------------------------------------
			
				element.Rect = rcElement;
			}

			RowUIElementBase rowElemBase = element as RowUIElementBase;
			if ( null != rowElemBase )
				rowElemBase.InitializeOverlapTopBorder( overlapTopBorder );

			// If we're dealing with a CardUIElement, do some additional calculations
			// on its size.
			if ( visibleRow.IsCardArea )
				this.CardRowSizingHelper(element as CardAreaUIElement);

			// Add the element to the child elements collection
			//
			// SSP 7/7/05 - NAS 5.3 Empty Rows
			// Added parentElement parameter. Empty rows are parented by a different ui element.
			//
			//this.ChildElements.Add( element );
			parentElement.ChildElements.Add( element );

			// JJD 12/26/01
			// Add a band header over the card area (if necessary)
			//
			if ( visibleRow.IsCardArea )
				this.AddCardAreaBandHeaderHelper( oldElements, element, visibleRow );

			// SSP 5/2/05 - NAS 5.2 Fixed Rows
			// Special row separators functionality. Call AfterElementAdded so the row can add
			// any special row separator elements if any.
			//
			// --------------------------------------------------------------------------------
			row.AfterElementAdded( element, oldElements, ref rcElement );

			Rectangle clipSelfRect = Rectangle.Empty;
			if ( visibleRow.ClipBottomTo >= 0 )
			{
				int clipBottomTo = visibleRow.GetTopInClient( ) + visibleRow.ClipBottomTo;
				Rectangle rowRect = element.Rect;
				if ( clipBottomTo >= rowRect.Y && clipBottomTo < rowRect.Bottom )
				{
					rowRect.Height = clipBottomTo - rowRect.Y;
					clipSelfRect = rowRect;
				}
			}

			// We do want to reset the clip self rect to Rectangle.Empty even if clipBottomTo was
			// not set.
			//
			row.SetElementClipSelfRect( element, clipSelfRect );

			// SSP 5/18/05
			//
			row.SetElementLastClipRect( element, newClipRect );
			// --------------------------------------------------------------------------------

			return element;
		}

		private void AddCardAreaBandHeaderHelper( UIElementsCollection oldElements,
						  						  UIElement cardAreaElement,
												  VisibleRow visibleRow )
        {
			if ( visibleRow.IsCardArea && visibleRow.HasHeader )
			{
				Rectangle headerRect = cardAreaElement.Rect;

				headerRect.Y		= visibleRow.GetTopInClient();
				headerRect.Height	= visibleRow.Band.GetTotalHeaderHeight();

				// JJD 12/26/01
				// Moved common logic into helper method so it could be also used to
				// add card view band headers
				//
				this.AddBandHeaderHelper( oldElements, visibleRow.Band, headerRect, false, visibleRow );
			}
		}

		private void CardRowSizingHelper(CardAreaUIElement cardRowElement)
		{
			// if this is a band 0 row and there are no groupby rows 
			// in the band then make the card area occupy the entire region
			//
			if ( cardRowElement.Rows.Band.Index == 0 &&
				!( cardRowElement.Rows.Band.HasGroupBySortColumns &&
 					cardRowElement.Rows.Band.Layout.ViewStyleImpl.SupportsGroupByRows ) )
			{
				Rectangle rcWork = this.RectInsideBorders;
			
				rcWork.Y		= cardRowElement.Rect.Top;
				rcWork.Height	= this.RectInsideBorders.Bottom - rcWork.Top;
				cardRowElement.Rect = rcWork;
				return;
			}

			// SSP 9/28/06 BR15510
			// CalculateRequiredCardAreaWidth call below relies upon the element's Rect's Height is assigned
			// a correct value. Therefore assign it before calling it.
			// 
			Rectangle thisRectInsideBorders = this.RectInsideBorders;
			Rectangle cardRowRect = cardRowElement.Rect;
			cardRowElement.Rect = Rectangle.Intersect( thisRectInsideBorders, cardRowRect );

			int availableWidth			= thisRectInsideBorders.Right - cardRowElement.Rect.Left;
			int requiredCardAreaWidth	= ((CardAreaUIElement)cardRowElement).CalculateRequiredCardAreaWidth(availableWidth);
			int widthToUse				= 0;

			if (requiredCardAreaWidth < 0)
				widthToUse	= availableWidth;
			else
				widthToUse	= Math.Min(availableWidth, requiredCardAreaWidth);

			// SSP 9/28/06 BR15510
			// 
			// ----------------------------------------------------------------------------
			cardRowRect.Width = widthToUse;
			cardRowElement.Rect = Rectangle.Intersect( this.RectInsideBorders, cardRowRect );
			
			// ----------------------------------------------------------------------------
		}

		// SSP 3/24/05 BR02974
		// Changed the return type from void to BandHeadersUIElement.
		//
		//private void AddBandHeaderHelper( 
		private BandHeadersUIElement AddBandHeaderHelper( 
			UIElementsCollection oldElements,
			UltraGridBand band,
			VisibleRow vr,
			int fixedHeaderHeight,
			bool forceAlignLeft )
		{
			return this.AddBandHeaderHelper( oldElements, band, vr, fixedHeaderHeight, forceAlignLeft, false );
		}

		// SSP 8/20/02 UWG1554
		// Added an overload that takes in addSummaryFooter parameter which indicates
		// whether to add a summary footer or not. This is necessary if there are
		// no rows and we still need to display the summary footer. (The summary footers
		// are hung off the visible row so if there aren't any rows there won't be
		// any visible rows. However we still need to display the summary footer so that
		// is what the addSummaryFooter parameter is for).
		//
		// SSP 3/24/05 BR02974
		// Changed the return type from void to BandHeadersUIElement.
		//
		//private void AddBandHeaderHelper( 
		private BandHeadersUIElement AddBandHeaderHelper( 
			UIElementsCollection oldElements,
			UltraGridBand band,
			VisibleRow vr,
			int fixedHeaderHeight,
			bool forceAlignLeft,
			bool addSummaryFooter )
		{
			// SSP 4/12/06 BR11361
			// 
			//return this.AddBandHeaderHelper( oldElements, band, vr, fixedHeaderHeight, forceAlignLeft, addSummaryFooter, 0 );
			Rectangle headerElemRect;
			return this.AddBandHeaderHelper( oldElements, band, vr, fixedHeaderHeight, forceAlignLeft, addSummaryFooter, 0, out headerElemRect );
		}

		// SSP 7/18/05 - NAS 5.3 Header Placement
		// Added an overload that takes in offsetFromRsrTop parameter. Now the vertical band can have
		// fixed headers. Since they are stacked vertically we need to pass in some info on where the
		// next header should start.
		// 
		private BandHeadersUIElement AddBandHeaderHelper( 
			UIElementsCollection oldElements,
			UltraGridBand band,
			VisibleRow vr,
			int fixedHeaderHeight,
			bool forceAlignLeft,
			bool addSummaryFooter,
			int offsetFromRsrTop,
			// SSP 4/12/06 BR11361
			// 
			out Rectangle headerElemRect )
		{

			// JJD 10/18/01 - UWG542
			// For single band outlook style we added the forceAlignLeft flag
			// so that the fixed header at the top of the grid will be left
			// aligned
			//
			int bandorigin = this.csr.GetBandOrigin( band );
			int bandextent = this.csr.GetBandExtent( band );

			UltraGridLayout layout = band.Layout;
			ViewStyleBase viewStyleImpl = layout.ViewStyleImpl;

			// SSP 7/19/05 - NAS 5.3 Header Placement
			// If the headers are displayed above the group-by rows (HeaderPlacement of 
			// OncePerGroupedRowIsland) then extend the first header (or the seprate element
			// depending on the row selector header style) left so the headers are aligned
			// with the left edge of the top-level group-by rows.
			// 
			int extendHeadersLeftDelta = ! forceAlignLeft 
				&& ( HeaderPlacement.OncePerGroupedRowIsland == band.HeaderPlacementResolved
					// SSP 10/6/06 BR15166 
					// Added the following line.
					// 
					|| viewStyleImpl.IsOutlookGroupBy && viewStyleImpl.BandHasFixedHeaders( band ) 
					)
				? viewStyleImpl.CalcGroupByConnectorsExtent( band )
				: 0;

			// SSP 11/27/01 UWG771
			// When forceAlignLeft is set to true, don't use band.PreRowAreaExtent
			// in calculating the band header rect since we want to line up
			// the band header's edge to the data area's left edge.
			//
			int preRowAreaExtent = band.PreRowAreaExtent;

			// JJD 10/18/01 - UWG542
			// If the new forceAlignLeft flag is true adjust the band header's
			// origin and extent
			//
			if ( forceAlignLeft )
			{
				bandextent += bandorigin;
				bandorigin = 0;

				// SSP 11/27/01 UWG771
				// If we have group by rows, then set the preRowAreaExtent to
				// 0
				if ( this.ColScrollRegion.Layout.Rows.IsGroupByRows )
				{
					preRowAreaExtent = 0;
				}
			}

			// MD 12/9/08 - 9.1 - Column Pinning Right
			// Determine the maximum width of the headers. If there are right-fixed headers, the right edge of the last visible header
			// marks the maximum extent of the headers element. This is so other headers scrolled under the right-fixed headers will not
			// show through on the other side of the headers.
            int maxBandHeadersWidth;
            if (band.RowLayoutStyle == RowLayoutStyle.None)
            {
                int bandOriginInColScrollRegion = bandorigin - this.ColScrollRegion.Position + band.GetFixedHeaders_OriginDelta(this.ColScrollRegion);
                maxBandHeadersWidth = this.ColScrollRegion.Extent - bandOriginInColScrollRegion;
                HeaderBase lastHeader = band.OrderedHeaders.LastVisibleHeader;
                if (lastHeader != null &&
                    lastHeader.FixOnRightResolved &&
                    band.IsHeaderFixed(this.ColScrollRegion, lastHeader))
                {
                    maxBandHeadersWidth = Math.Min(
                        maxBandHeadersWidth,
                        lastHeader.GetOnScreenOrigin(this.ColScrollRegion, band.HasRowSelectors) + lastHeader.Extent - bandOriginInColScrollRegion - this.ColScrollRegion.Origin) - 1;
                }
            }
            else            
                maxBandHeadersWidth = Int32.MaxValue;

			// SSP 5/19/03 - Fixed headers
			//
			
			Rectangle headerRect = new Rectangle( 
				bandorigin  //left
				//+ band.PreRowAreaExtent
				+ preRowAreaExtent
				+ this.csr.Origin
				- this.csr.Position
				+ band.GetFixedHeaders_OriginDelta( csr )
				// SSP 7/19/05 - NAS 5.3 Header Placement
				// If the headers are displayed above the group-by rows (HeaderPlacement of 
				// OncePerGroupedRowIsland) then extend the first header (or the seprate element
				// depending on the row selector header style) left so the headers are aligned
				// with the left edge of the top-level group-by rows.
				// 
				- extendHeadersLeftDelta,

				vr != null                        //top
				? vr.GetTop() + this.rsr.Origin 
				// SSP 7/18/05 - NAS 5.3 Header Placement
				// Added an overload that takes in offsetFromRsrTop parameter. Now the vertical band can have
				// fixed headers. Since they are stacked vertically we need to pass in some info on where the
				// next header should start.
				// 
				//: this.Rect.Top,
				: this.Rect.Top + offsetFromRsrTop,

				// MD 12/9/08 - 9.1 - Column Pinning Right
				// This logic was off a little. It has been changed to prevent unfixed column headers from showing on the right side 
				// of right-fixed headers.
				//bandextent    //width
				////- band.PreRowAreaExtent,
				//- preRowAreaExtent				
				//- band.GetFixedHeaders_ExtentDelta( csr )
				Math.Min(							//width
					maxBandHeadersWidth, 
					bandextent - band.GetFixedHeaders_ExtentDelta( csr ) )
				- preRowAreaExtent

				// SSP 8/8/03 UWG2551
				// Added GetBandHeader_BandHeader_SummaryFooter_RightDelta. Unlike the row ui element, the 
				// band headers and the summary footer should get scrolled all the way out.
				// Added below line of code.
				//
				+ band.GetFixedHeaders_BandHeader_SummaryFooter_RightDelta( csr )
				// SSP 7/19/05 - NAS 5.3 Header Placement
				// If the headers are displayed above the group-by rows (HeaderPlacement of 
				// OncePerGroupedRowIsland) then extend the first header (or the seprate element
				// depending on the row selector header style) left so the headers are aligned
				// with the left edge of the top-level group-by rows.
				// 
				+ extendHeadersLeftDelta,

				band.GetTotalHeaderHeight() );    //height


			// SSP 7/18/05 - NAS 5.3 Header Placement
			// Only align the headers bottom to the bottom of the fixed headers area if the
			// view style is not horizontal. Enlcosed the existing code into the if block.
			// 
			if ( viewStyleImpl.HasMultiRowTiers )
			{
				// if we're in the fixed headear area and this band's header is shorter
				// that the fixed area then offset the band header down so that it aligns
				// with the fixed header areas bottom
				//
				if ( fixedHeaderHeight > 0 && 
					fixedHeaderHeight > headerRect.Height )
				{
					headerRect.Offset( 0, fixedHeaderHeight - headerRect.Height );
				}
			}

			// SSP 4/12/06 BR11361
			// 
			headerElemRect = headerRect;

			// SSP 8/8/03 UWG2551
			// Return if the rectangle is empty.
			//
			if ( headerRect.Width <= 0 || headerRect.Height <= 0 )
				// SSP 3/24/05 BR02974
				// Changed the return type from void to BandHeadersUIElement.
				//
				//return;
				return null;

			// JJD 12/26/01
			// Moved common logic into helper method so it could be also used to
			// add card view band headers
			//
			// SSP 3/24/05 BR02974
			// Changed the return type from void to BandHeadersUIElement.
			//
			//this.AddBandHeaderHelper( oldElements, band, headerRect, forceAlignLeft, vr );
			BandHeadersUIElement bandHeadersElement = this.AddBandHeaderHelper( oldElements, band, headerRect, forceAlignLeft, vr );

			// SSP 8/22/02 UWG1554
			// If there are no rows, we still need to add a summary footer element.
			//
			// -------------------------------------------------------------------------------------------
			if ( addSummaryFooter )
			{
				//Debug.Assert( null != band && null == band.ParentBand && null == vr && null != band.Layout.Rows && 0 == band.Layout.Rows.Count, 
				//	"This parameter should only have been true of these conditions met." );

				if ( null != band && null == band.ParentBand && null != layout &&
					null != layout.Rows && 0 == layout.Rows.Count &&
					null == vr ) // and I mean null == vr
				{
					RowsCollection rows = layout.Rows;

					if ( rows.HasSummaryValues )
					{
						// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
						// Added ability to display summaries in group-by rows aligned with columns, 
						// summary footers for group-by row collections, fixed summary footers and
						// being able to display the summary on top of the row collection.
						//
						//int summaryFooterHeight = rows.SummaryValues.GetSummaryFooterHeight( );
						SummaryDisplayAreaContext bottomSummariesContext = SummaryDisplayAreaContext.BottomScrollingSummariesContext;
						int summaryFooterHeight = rows.SummaryValues.GetSummaryFooterHeight( bottomSummariesContext );

						if ( summaryFooterHeight > 0 )
						{
							Rectangle summaryRect = headerRect;

							summaryRect.Y = summaryRect.Bottom;
							summaryRect.Height = summaryFooterHeight;

							SummaryFooterUIElement summaryFooterElem = 
								(SummaryFooterUIElement)RowColRegionIntersectionUIElement.ExtractExistingElement( 
								oldElements, typeof( SummaryFooterUIElement ), true );

							if ( null == summaryFooterElem )
								summaryFooterElem = new SummaryFooterUIElement( this, rows );

							// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
							// Added ability to display summaries in group-by rows aligned with columns, 
							// summary footers for group-by row collections, fixed summary footers and
							// being able to display the summary on top of the row collection.
							//
							//summaryFooterElem.Initialize( rows );
							summaryFooterElem.Initialize( rows, bottomSummariesContext );

							summaryFooterElem.Rect = summaryRect;

							this.ChildElements.Add( summaryFooterElem );
						}
					}
				}
			}
			// -------------------------------------------------------------------------------------------

			return bandHeadersElement;
		}

		// SSP 3/24/05 BR02974
		// Changed the return type from void to BandHeadersUIElement.
		//
        //private void AddBandHeaderHelper( 
		private BandHeadersUIElement AddBandHeaderHelper( 
			UIElementsCollection oldElements,
            UltraGridBand band,
            Rectangle headerRect,
			bool forceAlignLeft,
			// SSP 3/15/02
			// Added visibleRow parameter for purposes of
			// setting a context of RowsCollection on the band header.
			// This is for row filtering.
			//
			VisibleRow visibleRow )
		{
            if ( !headerRect.IntersectsWith( this.rectValue ) )
				// SSP 3/24/05 BR02974
				// Changed the return type from void to BandHeadersUIElement.
				//
                //return;
				return null;

            BandHeadersUIElement bandHeadersElement = (BandHeadersUIElement)RowColRegionIntersectionUIElement.ExtractExistingElement( oldElements, typeof(BandHeadersUIElement), true );

            if ( null == bandHeadersElement)
            {
                bandHeadersElement = new BandHeadersUIElement( this );
            }

			// SSP 3/14/02
			// Added context of rows collection to the band
			//
			RowsCollection rows = null != visibleRow ? visibleRow.Row.ParentCollection : null;
			// SSP 3/26/02
			// If we are adding a bands header for top most band, then use the rows off
			// the layout as the context for the band headers if a visible row was not
			// specified.
			//
			if ( null == rows && 0 == band.Index )
                rows = band.Layout.Rows;

			// JJD 1/5/04
			// Added row context which is needed for accessibility
            //bandHeadersElement.InitializeBand( band, forceAlignLeft, rows );
            bandHeadersElement.InitializeBand( band, forceAlignLeft, rows, visibleRow != null && !visibleRow.IsCardArea ? visibleRow.Row : null );

            bandHeadersElement.Rect = headerRect;

            this.ChildElements.Add( bandHeadersElement );

			// SSP 3/24/05 BR02974
			// Changed the return type from void to BandHeadersUIElement.
			//
			return bandHeadersElement;
        }

		/// <summary>
		/// overridden method that draws appropriate background for this element
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
        {
        }

		/// <summary>
		/// this element doesn't draw an image background
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
		}

		// SSP 11/10/04 - Merged Cell Feature
		//
		#region Merged Cell Feature

		#region VerifyChildElements

	    // SSP 11/30/04 - Merged Cell Feature
		// Overrode VerifyChildElements method.
		//
        /// <summary>
        /// Called during a drawing operation to ensure that all child elements are created and positioned properly. If the ChildElementsDirty flag is true then the default implementation will call PositionChildElements and reset the flag.
        /// </summary>
        /// <param name="controlElement">The control's main UIElement</param>
        /// <param name="recursive">If true will call this method on all descendant elements</param>
        protected override void VerifyChildElements(ControlUIElementBase controlElement, bool recursive)
		{
            // MBS 1/9/08 - BR28933
            // We need to always recursively verify all the child elements in the row due to the fact that 
            // merged cells require the rows and cells to position themselves first; if this does not occur,
            // the underlying cells will not know that they lie underneath merged cells and will draw when
            // the entire grid is repositioned (i.e. the grid is docked to fill and the form is resized).
            recursive = true;

			base.VerifyChildElements( controlElement, recursive );

			// Merged cells while positioning themselves can determine that they are not needed after all
			// and they will dispose off themselves (see MergedCellUIElement.PositionSelf method). We need 
			// to remove such disposed elements from the child elements collection.
			//
			if ( recursive && null != this.childElementsCollection )
			{
				UIElementsCollection childElems = this.childElementsCollection;				
				int count = childElems.Count;
				int c = 0;

				for ( int i = 0; i < count; i++ )
				{
					UIElement elem = childElems[i];
					if ( ! elem.Disposed )
					{
						if ( i != c )
							childElems[ c ] = childElems[ i ];

						c++;
					}
				}

                if ( c < count )
					childElems.RemoveRange( c, count - c );
			}
		}

		#endregion // VerifyChildElements

		#region PositionMergedCellElements

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool AddColumnForMerging( ArrayList[] mergedCols, UltraGridColumn column )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private static bool AddColumnForMerging( ArrayList[] mergedCols, UltraGridColumn column )
		private static bool AddColumnForMerging( List<UltraGridColumn>[] mergedCols, UltraGridColumn column )
		{
			if ( column.MergedCellEnabledResolved )
			{
				int bandIndex = column.Band.Index;
				if ( bandIndex >= 0 && bandIndex < mergedCols.Length )
				{
					if ( null == mergedCols[ bandIndex ] )
					{
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//mergedCols[ bandIndex ] = new ArrayList();
						mergedCols[ bandIndex ] = new List<UltraGridColumn>();
					}

					mergedCols[ bandIndex ].Add( column );
					return true;
				}
				else
					Debug.Assert( false );
			}

			return false;
		}

		private void PositionMergedCellElements( UIElementsCollection oldMergedCellElems )
		{
			Infragistics.Win.UltraWinGrid.ColScrollRegion csr = this.ColScrollRegion;
			UltraGridLayout layout = csr.Layout;

			// Construct an array of ArrayList. ArrayLists will contain the columns. The
			// MergedCols array will be the same lengh as the number of bands. Indexing
			// into the array will give you an array list that contains the columns from
			// the band with that index that have cell merging turned on. We are only
			// interested in the columns that are visible in the col scroll region and 
			// that's the reason for using VisibleHeaders off the col scroll region.
			// 
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList[] mergedCols = new ArrayList[ layout.Bands.Count ];
			List<UltraGridColumn>[] mergedCols = new List<UltraGridColumn>[ layout.SortedBands.Count ];

			bool hasMergedCols = false;
			foreach ( VisibleHeader vh in csr.VisibleHeaders )
			{
				HeaderBase header = vh.Header;
				UltraGridColumn column = header.Column;
				if ( null != column )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//hasMergedCols = this.AddColumnForMerging( mergedCols, column ) || hasMergedCols;
					hasMergedCols = RowColRegionIntersectionUIElement.AddColumnForMerging( mergedCols, column ) || hasMergedCols;
				}
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//else if ( null != header.Group )
				//{
				//    foreach ( UltraGridColumn groupCol in header.Group.Columns )
				//    {
				//        // MD 7/26/07 - 7.3 Performance
				//        // FxCop - Mark members as static
				//        //hasMergedCols = this.AddColumnForMerging( mergedCols, groupCol ) || hasMergedCols;
				//        hasMergedCols = RowColRegionIntersectionUIElement.AddColumnForMerging( mergedCols, groupCol ) || hasMergedCols;
				//    }
				//}
				else
				{
					UltraGridGroup headerGroup = header.Group;

					if ( null != headerGroup )
					{
						// MD 1/21/09 - Groups in RowLayouts
						// Use the resolved column because in GroupLayout style, it will return a different enumarator which returns the columns 
						// actually visible in the group
						//foreach ( UltraGridColumn groupCol in headerGroup.Columns )
						foreach ( UltraGridColumn groupCol in headerGroup.ColumnsResolved )
						{
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//hasMergedCols = this.AddColumnForMerging( mergedCols, groupCol ) || hasMergedCols;
							hasMergedCols = RowColRegionIntersectionUIElement.AddColumnForMerging( mergedCols, groupCol ) || hasMergedCols;
						}
					}
				}
			}

			// If we have no merged columns then simply return.
			//
			if ( ! hasMergedCols )
				return;

			// Construct a map to keep track of which merged cells we have processed and their
			// corresponding merged cell elements.
			//
			Hashtable processedMergedCells = new Hashtable( );

			// Loop through all the row elements and add merged cells from those row.
			//
			UIElementsCollection elems = this.ChildElements;
			for ( int i = 0, elemCount = elems.Count; i < elemCount; i++ )
			{
				RowUIElement rowElem = elems[ i ] as RowUIElement;
				UltraGridRow row = null != rowElem ? rowElem.Row : null;
				if ( null == row || null == row.BandInternal )
					continue;

				// Reset the flag that indicates if the row element has merged cells over it.
				//
				rowElem.hasMergedCells = false;

				int bandIndex = row.BandInternal.Index;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList mergedColumnsFromBand = 
				List<UltraGridColumn> mergedColumnsFromBand = 
					bandIndex >= 0 && bandIndex < mergedCols.Length ? mergedCols[ bandIndex ] : null;

				// If the row's band doesn't have any columns that have cell merging enabled then 
				// skip the row.
				//
				if ( null == mergedColumnsFromBand )
					continue;

				for ( int j = 0, count = mergedColumnsFromBand.Count; j < count; j++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//UltraGridColumn column = (UltraGridColumn)mergedColumnsFromBand[ j ];
					UltraGridColumn column = mergedColumnsFromBand[ j ];

					MergedCell mergedCell = column.GetMergedCell( row );
					if ( null == mergedCell )
						continue;

					rowElem.hasMergedCells = true;

					// If we have already processed the merged cell then get the merged cell element.
					//
					MergedCellUIElement mergedCellElem = (MergedCellUIElement)processedMergedCells[ mergedCell ];
					if ( null != mergedCellElem )
					{
						mergedCellElem.RowElements.Add( rowElem );
						continue;
					}

					// Get the top of the merged cell relative to the current row.
					//
					int mergedCellTopRelativeToRow = mergedCell.GetTopRelativeTo( row );
					int mergedCellHeight = mergedCell.Height;
					if ( mergedCellHeight <= 0 || -mergedCellTopRelativeToRow >= mergedCellHeight )
					{
						Debug.Assert( false );
						continue;
					}

					// Figure out the what the top and bottom of the merged cell should be by making
					// use of the two pieces of information we obtained above; one being the height
					// of the entire merged cell and the second being the top of the merged cell 
					// relative to the current row.
					//
					int mergedCellTop = rowElem.Rect.Y + mergedCellTopRelativeToRow;
					mergedCellElem = this.AddMergedCellElement( mergedCell, 
						mergedCellTop, mergedCellHeight, oldMergedCellElems );

					if ( null != mergedCellElem )
					{
						mergedCellElem.RowElements.Add( rowElem );
						processedMergedCells.Add( mergedCell, mergedCellElem );
					}
				}
			}
		}

		#endregion // PositionMergedCellElements

		#region AddMergedCellElement

		private MergedCellUIElement AddMergedCellElement( MergedCell mergedCell, 
					int mergedCellTop, int mergedCellHeight, UIElementsCollection oldMergedCellElems )
		{
			MergedCellUIElement mergedCellElem = null != oldMergedCellElems ?
				(MergedCellUIElement)RowColRegionIntersectionUIElement.ExtractExistingElement( 
						oldMergedCellElems, typeof( MergedCellUIElement ), true, mergedCell ) : null;

            if ( null == mergedCellElem )
				mergedCellElem = new MergedCellUIElement( this );

			mergedCellElem.Initialize( mergedCell, mergedCellTop, mergedCellHeight );
			this.ChildElements.Add( mergedCellElem );
			return mergedCellElem;
		}

		#endregion // AddMergedCellElement

		#endregion // Merged Cell Feature

		//	BF 10.25.01
		#region Mouse Panning Support

		/// <summary>
		/// Gets whether this element is supporting vertical mouse panning
		/// </summary>
		protected override bool SupportsVerticalMousePanning
		{
			get
			{ 
				//	Determine whether the active row scroll region's scrollbar
				//	is going to be shown; if it is, return true so that we support
				//	vertical mouse panning
				//RobA UWG593 10/26/01 this might not be a grid
				//UltraGridBase grid = (UltraGridBase)( this.Control );				
				UltraGridBase grid = this.Control as UltraGridBase;
				if ( grid != null && grid.ActiveRowScrollRegion.WillScrollbarBeShown() )
					return true; 

				return false;
			}
		}

		/// <summary>
		/// Gets whether this element is supporting horizontal mouse panning
		/// </summary>
		protected override bool SupportsHorizontalMousePanning
		{
			get
			{ 
				//	Determine whether the active col scroll region's scrollbar
				//	is going to be shown; if it is, return true so that we support
				//	horizontal mouse panning
				//RobA UWG593 10/26/01 this might not be a grid
				//UltraGridBase grid = (UltraGridBase)( this.Control );
				UltraGridBase grid = this.Control as UltraGridBase;
				if ( grid != null && grid.ActiveColScrollRegion.WillScrollbarBeShown() )
					return true; 

				return false;
			}
		}

		/// <summary>
        /// Called each time the mouse pans vertically.
		/// </summary>
        /// <param name="delta">The number of pixels between the cursor position and the origin mark. This value is positive when the cursor is below the origin mark, negative when it is above the origin mark, and zero if it is within the origin mark threshold</param>
		protected override void OnMousePanVertical( int delta )
		{
			//	Get the magnitude of the pan
			int absoluteDelta = System.Math.Abs( delta );

			//	When the delta gets significantly large, scrolling will not be able
			//	to keep up with the frequency of calls to this method, because
			//	of the time required to paint the control. To make it seem faster,
			//	scroll by an amount that is proportional to the magnitude of the delta
			//	
			//	Note that the values I picked for 'significantly large' are arbitrary
			//
			int linesToScroll = 1;
			if ( absoluteDelta >= 200 )
				linesToScroll = absoluteDelta / 100;
			
			//	Whether we are scrolling up or down depends on the sign of delta
			RowScrollAction rowScrollAction = RowScrollAction.LineDown;
			if ( delta < 0 )
				rowScrollAction = RowScrollAction.LineUp;

			//	Iterate by the number of  lines we are scrolling
			for ( int i = 1; i <= linesToScroll; i ++ )
			{
				this.rsr.Scroll( rowScrollAction );
			}


		}	//	END protected override void OnMousePanVertical( int delta )

		/// <summary>
        /// Called each time the mouse pans horizontally.
		/// </summary>
        /// <param name="delta">The number of pixels between the cursor position and the origin mark. This value is positive when the cursor is to the right of the origin mark, negative when it is left of the origin mark, and zero if it is within the origin mark threshold.</param>
		protected override void OnMousePanHorizontal( int delta )
		{
			//	Get the magnitude of the pan
			int absoluteDelta = System.Math.Abs( delta );

			//	When the delta gets significantly large, scrolling will not be able
			//	to keep up with the frequency of calls to this method, because
			//	of the time required to paint the control. To make it seem faster,
			//	scroll by an amount that is proportional to the magnitude of the delta
			//	
			//	Note that the values I picked for 'significantly large' are arbitrary
			//
			int linesToScroll = 1;
			if ( absoluteDelta >= 100 )
				linesToScroll = absoluteDelta / 50;
			
			//	Whether we are scrolling up or down depends on the sign of delta
			ColScrollAction colScrollAction = ColScrollAction.LineRight;
			if ( delta < 0 )
				colScrollAction = ColScrollAction.LineLeft;

			//	Iterate by the number of  lines we are scrolling
			for ( int i = 1; i <= linesToScroll; i ++ )
			{
				this.csr.Scroll( colScrollAction );
			}

		}	//	END protected override void OnMousePanHorizontal( int delta )

		#endregion	//	Mouse Panning Support

		// JJD 12/23/03 - Accessibility
		#region IsAccessibleElement property

		/// <summary>
		/// Indicates if the element supports accessibility
		/// </summary>
		public override bool IsAccessibleElement { get { return true; } }

		#endregion IsAccessibleElement property

		// JJD 12/12/03 - Added Accessibility support
		#region AccessibilityInstance property

		/// <summary>
		/// Returns the accesible object representing the data area of the grid.
		/// </summary>
		public override AccessibleObject AccessibilityInstance
		{
			get
			{
				if ( this.accessibilityObject == null )
				{
					UltraGridBase grid = this.RowScrollRegion.Layout.Grid;

					if ( grid != null )
						this.accessibilityObject = grid.CreateAccessibilityInstance	( this );
				}

				return this.accessibilityObject;
			}
		}

		#endregion AccessibilityInstance property

		// JJD 12/12/03 - Added Accessibility support
		#region Public Class RowColRegionAccessibleObject

		/// <summary>
		/// The Accessible object for a row/column region intersection.
		/// </summary>
		public class RowColRegionAccessibleObject : UIElementAccessibleObject
		{
			#region Private Members

			private RowColRegionIntersectionUIElement rowColElement;

			#endregion Private Members

			#region Constructor
			
			/// <summary>
			/// Constructor
			/// </summary>
			public RowColRegionAccessibleObject ( RowColRegionIntersectionUIElement rowColElement ) : base( rowColElement, AccessibleRole.Grouping, Shared.SR.GetString("AccessibleName_RowColRegion") )
			{
				this.rowColElement = rowColElement;
			}

			#endregion Constructor

			#region Base Class Overrides

				#region DefaultAction

			/// <summary>
			/// Gets a string that describes the default action of the object.
			/// </summary>
			public override string DefaultAction
			{
				get
				{
					return Shared.SR.GetString("DefaultAction_RowColRegion");
				}
			}

				#endregion DefaultAction

				#region DoDefaultAction

			/// <summary>
			/// Performs the default action associated with this accessible object.
			/// </summary>
			public override void DoDefaultAction()
			{
				if ( this.rowColElement.Enabled == true )
				{
					RowScrollRegion rsr = this.rowColElement.RowScrollRegion;
					ColScrollRegion csr = this.rowColElement.ColScrollRegion;

					if ( rsr != null && csr != null )
					{
						rsr.Layout.Grid.ActiveRowScrollRegion = rsr;
						csr.Layout.Grid.ActiveColScrollRegion = csr;
					}
				}
			}

				#endregion DoDefaultAction

				#region GetChild

			/// <summary>
			/// Retrieves the accessible child corresponding to the specified index.
			/// </summary>
			/// <param name="index">The zero-based index of the accessible child.</param>
			/// <returns>An AccessibleObject that represents the accessible child corresponding to the specified index.</returns>
			public override AccessibleObject GetChild(int index)
			{
				return null;
			}

				#endregion GetChild

				#region GetChildCount

			/// <summary>
			/// Retrieves the number of children belonging to an accessible object.
			/// </summary>
			/// <returns>The number of children belonging to an accessible object.</returns>
			public override int GetChildCount()
			{
				return 0;
			}

				#endregion GetChildCount

				#region GetFocused

			/// <summary>
			/// Retrieves the object that has the keyboard focus.
			/// </summary>
			/// <returns>An AccessibleObject that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (Nothing in Visual Basic) if no object has focus.</returns>
			public override AccessibleObject GetFocused()
			{
				return null;
			}

				#endregion GetFocused

				#region GetSelected

			/// <summary>
			/// Retrieves the currently selected child.
			/// </summary>
			/// <returns>An AccessibleObject that represents the currently selected child. This method returns the calling object if the object itself is selected. Returns a null reference (Nothing in Visual Basic) if is no child is currently selected and the object itself does not have focus.</returns>
			public override AccessibleObject GetSelected()
			{
				UltraGridBase grid = this.Grid;

				if ( grid == null )
					return null;

				if ( grid.ActiveColScrollRegion != this.rowColElement.ColScrollRegion ||
					 grid.ActiveRowScrollRegion != this.rowColElement.RowScrollRegion )
					return null;

				
				return null;
			}

				#endregion GetSelected

				#region HitTest

			/// <summary>
			/// Retrieves the child object at the specified screen coordinates.
			/// </summary>
			/// <param name="x">The horizontal screen coordinate.</param>
			/// <param name="y">The vertical screen coordinate.</param>
			/// <returns>An AccessibleObject that represents the child object at the given screen coordinates. This method returns the calling object if the object itself is at the location specified. Returns a null reference (Nothing in Visual Basic) if no object is at the tested location.</returns>
			public override AccessibleObject HitTest(int x, int y)
			{
				UltraGridBase grid = this.Grid;

				if ( grid == null )
					return this.AccessibleObject;

				Point pt = new Point( x, y );

				if ( !this.Bounds.Contains( pt ) )
					return null;

				RowScrollRegion rsr = grid.DisplayLayout.ActiveRowScrollRegion;
				ColScrollRegion csr = grid.DisplayLayout.ActiveColScrollRegion;

				if ( this.rowColElement.RowScrollRegion == rsr &&
					 this.rowColElement.ColScrollRegion == csr )
				{
					// loop over the visible rows in the active row col
					// scrolling region to see if there is a match
					foreach ( VisibleRow vr in rsr.VisibleRows )
					{
						if ( vr.Row.AccessibilityObject.Bounds.Contains( pt ) )
							return vr.Row.AccessibilityObject.HitTest( x, y );
					}

					Infragistics.Win.UIElement element = this.UIElement.ElementFromPoint( grid.PointToClient( new Point( x, y ) ) );

					if ( element != null )
					{
						System.Windows.Forms.AccessibleObject acc;

						BandHeadersUIElement bandHeadersElement = element.GetAncestor( typeof ( BandHeadersUIElement ) ) as BandHeadersUIElement;

						if ( bandHeadersElement != null )
						{
							UltraGridRow row = bandHeadersElement.GetContext( typeof( UltraGridRow ), false ) as UltraGridRow;

							if ( row != null )
								acc = row.ColHeadersAccessibilityObject;
							else
								acc = bandHeadersElement.Band.ColHeadersAccessibilityObject;

							if ( acc != null && acc.Bounds.Contains( pt ))
								return acc.HitTest( x, y );
						}
						else
						{
							CardLabelAreaUIElement cardLabelAreaElement = element.GetAncestor( typeof ( CardLabelAreaUIElement ) ) as CardLabelAreaUIElement;
							if ( cardLabelAreaElement != null )
							{
								if ( cardLabelAreaElement.CardAreaElement != null && 
									 cardLabelAreaElement.CardAreaElement.Rows != null &&
									 cardLabelAreaElement.CardAreaElement.Rows.ParentRow != null)
								{
									UltraGridRow parentRow = cardLabelAreaElement.CardAreaElement.Rows.ParentRow;

									Debug.Assert( parentRow.Band == cardLabelAreaElement.Band.ParentBand );

									acc = parentRow.ChildBands[ cardLabelAreaElement.Band ].CardLabelsAccessibilityObject;
								}
								else
								{
									UltraGridRow row = cardLabelAreaElement.GetContext( typeof( UltraGridRow ), false ) as UltraGridRow;

									if ( row != null )
										acc = row.ColHeadersAccessibilityObject;
									else
										acc = cardLabelAreaElement.Band.ColHeadersAccessibilityObject;
								}

								if ( acc != null && acc.Bounds.Contains( pt ))
									return acc.HitTest( x, y );
							}
							else
							{
								CardAreaUIElement cardAreaElement = element.GetAncestor( typeof ( CardAreaUIElement ) ) as CardAreaUIElement;
								if ( cardAreaElement != null )
								{
									acc = cardAreaElement.Band.CardScrollbarAccessibilityObject;

									if ( acc != null && acc.Bounds.Contains( pt ))
										return acc.HitTest( x, y );
								}
							}
						}
					}
				}

				return base.HitTest( x, y );
			}

				#endregion HitTest

				#region Navigate

            /// <summary>
            /// Navigates to another accessible object.
            /// </summary>
            /// <param name="navdir">One of the <see cref="System.Windows.Forms.AccessibleNavigation"/> values.</param>
            /// <returns>An AccessibleObject relative to this object.</returns>
            public override AccessibleObject Navigate(AccessibleNavigation navdir)
			{
				DataAreaUIElement dataAreaElement = this.DataAreaElement;

				if ( dataAreaElement == null )
					return null;

				UltraGridBase grid = this.Grid;

				if ( grid == null )
					return null;

				RowScrollRegion rsr = this.rowColElement.RowScrollRegion;
				ColScrollRegion csr = this.rowColElement.ColScrollRegion;

				switch ( navdir )
				{
						#region case FirstChild

					case AccessibleNavigation.FirstChild:

						if ( grid.ActiveColScrollRegion == csr &&
							 grid.ActiveRowScrollRegion == rsr )
						{
							if ( grid.Rows.TemplateRowLocationDefault == TemplateAddRowLocation.Top )
								return grid.Rows.TemplateAddRow.AccessibilityObject;

							if( grid.Rows.Count > 0 )
								return grid.Rows[0].AccessibilityObject;

							if ( grid.DisplayLayout.SortedBands[0].CardScrollbarAccessibilityObject != null )
								return grid.DisplayLayout.SortedBands[0].CardScrollbarAccessibilityObject;

						}
						
						return null;

						#endregion case FirstChild

						#region case LastChild

					case AccessibleNavigation.LastChild:

						if ( grid.ActiveColScrollRegion == csr &&
							 grid.ActiveRowScrollRegion == rsr )
						{
							if ( grid.DisplayLayout.SortedBands[0].CardScrollbarAccessibilityObject != null )
								return grid.DisplayLayout.SortedBands[0].CardScrollbarAccessibilityObject;

							if ( grid.Rows.TemplateRowLocationDefault == TemplateAddRowLocation.Bottom )
								return grid.Rows.TemplateAddRow.AccessibilityObject;

							if ( grid.Rows.Count > 0 )
								return grid.Rows[grid.Rows.Count - 1].AccessibilityObject;
						}
						
						return null;

						#endregion case LastChild
				}

				return base.Navigate( navdir );

			}

				#endregion Navigate

				#region Select

			/// <summary>
			/// Modifies the selection or moves the keyboard focus of the accessible object.
			/// </summary>
			/// <param name="flags">One of the <see cref="System.Windows.Forms.AccessibleSelection"/> values.</param>
			public override void Select(System.Windows.Forms.AccessibleSelection flags)
			{
				if ( ( flags & ( AccessibleSelection.AddSelection | AccessibleSelection.ExtendSelection | AccessibleSelection.TakeFocus | AccessibleSelection.TakeSelection ) ) != 0 )
					this.DoDefaultAction();
			}

				#endregion Select

				#region State

			/// <summary>
			/// Gets the state of this accessible object.
			/// </summary>
			public override AccessibleStates State
			{
				get
				{
					AccessibleStates state;

					if ( this.rowColElement.Enabled == true )
						state = AccessibleStates.Selectable;
					else
						state = AccessibleStates.Unavailable;

					return state;
				}
			}

				#endregion State

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region Grid

			/// <summary>
			/// Returns the associated grid control.
			/// </summary>
			public UltraGridBase Grid 
			{ 
				get 
				{
					UltraGridUIElement gridElement = this.rowColElement.GetAncestor( typeof(UltraGridUIElement) ) as UltraGridUIElement;

					if ( gridElement != null )
						return gridElement.Grid;

					return null; 
				} 
			}

					#endregion Grid

				#endregion Public Properties

				#region Internal Properties

					#region DataAreaElement

			internal DataAreaUIElement DataAreaElement 
			{ 
				get 
				{
					return this.rowColElement.GetAncestor( typeof(DataAreaUIElement) ) as DataAreaUIElement;
				} 
			}

					#endregion DataAreaElement

				#endregion Internal Properties

			#endregion Properties

		}

		#endregion Public Class RowColRegionAccessibleObject

        // MBS 3/31/09 - NA9.2 CellBorderColor        
        // Overridden in order to be able to handle the custom drawing of the ActiveCell's borders on top of the existing ones
        #region OnAfterDraw

        /// <summary>
        /// A virtual method that gets called after the element draw operation finishes.
        /// </summary>        
        /// <param name="defaultDrawParams">The <see cref="UIElementDrawParams"/> used to provide rendering information.</param>
        protected override void OnAfterDraw(ref UIElementDrawParams defaultDrawParams)
        {
            base.OnAfterDraw(ref defaultDrawParams);

            UltraGrid grid = this.Control as UltraGrid;
            if (grid != null)
            {
                // MBS 4/13/09 - NA9.2 Selection Overlay
                // Draw the selection overlay before the active border
                Selected selected = grid.SelectedIfAllocated;
                if (selected != null)
                {
                    Color selectionOverlayColor = grid.DisplayLayout.SelectionOverlayColorResolved;
                    Color selectionOverlayBorderColor = grid.DisplayLayout.SelectionOverlayBorderColorResolved;
                    if (selectionOverlayColor != Color.Empty || selectionOverlayBorderColor != Color.Empty)
                    {
                        if (selected.HasRows)
                            DrawSelectionOverlay(ref defaultDrawParams, grid.Selected.Rows[0].Band, true, selectionOverlayColor, selectionOverlayBorderColor);
                        else if (selected.HasCells)
                            DrawSelectionOverlay(ref defaultDrawParams, grid.Selected.Cells[0].Band, false, selectionOverlayColor, selectionOverlayBorderColor);
                        else if (selected.HasColumns)
                            DrawSelectionOverlay(ref defaultDrawParams, grid.Selected.Columns[0].Band, false, selectionOverlayColor, selectionOverlayBorderColor);
                    }
                }

                UltraGridCell cell = grid.ActiveCellInternal;
                if (cell != null)
                {
                    int borderWidth = cell.Band.ActiveCellBorderThicknessResolved;

                    // MBS 5/15/09 - TFS17562
                    // The default border width is now 1 instead of 0, with the change that we won't actually draw the active
                    // cell overlay with a width of 1 if the user hasn't specified an ActiveCell border color
                    Color borderColor = Color.Empty;                    
                    if (borderWidth == 1)
                    {
                        // Ask for the band to resolve only the ActiveCell appearance
                        AppearanceData appearance = new AppearanceData();
                        AppearancePropFlags requestedProps = AppearancePropFlags.BorderColor;
                        cell.Row.ResolveCellAppearance(cell.Column, ref appearance, ref requestedProps, true, false, ForceActive.Cell, false, false, false, false);
                        borderColor = appearance.BorderColor;
                    }

                    // MBS 5/15/09 - TFS17562
                    //if (borderWidth > 0)
                    if(borderWidth > 1 || borderColor != Color.Empty)
                    {
                        CellUIElementBase cellElement = cell.GetUIElement(this.RowScrollRegion, this.ColScrollRegion) as CellUIElementBase;
                        if (cellElement != null)
                        {
                            // MBS 5/15/09 - TFS17562
                            if (borderColor == Color.Empty)
                            {
                                AppearanceData appearance = new AppearanceData();
                                AppearancePropFlags requestedProps = AppearancePropFlags.BorderColor;

                                cellElement.InternalInitAppearance(ref appearance, ref requestedProps);
                                borderColor = appearance.BorderColor;
                            }

                            // MBS 5/15/09 - TFS17562
                            //Pen pen = this.GetCachedActiveCellBorderPen(borderWidth, appearance.BorderColor);
                            Pen pen = this.GetCachedActiveCellBorderPen(borderWidth, borderColor);

                            Rectangle cellRect = UltraGridCell.CalculateCellOverlayRect(cellElement, borderWidth, false);
                            defaultDrawParams.Graphics.DrawRectangle(pen, cellRect);
                            this.lastActiveCellBorderThickness = borderWidth;                            
                        }
                    }
                }
            }
        }        
        #endregion //OnAfterDraw
        //
        #region LastActiveCellBorderThickness

        internal int LastActiveCellBorderThickness
        {
            get { return this.lastActiveCellBorderThickness; }
        }
        #endregion //LastActiveCellBorderThickness
        //
        #region GetCachedActiveCellBorderPen

        private Pen GetCachedActiveCellBorderPen(int borderThickeness, Color color)
        {
            if (this.cachedActiveCellBorderPen != null &&
                (int)this.cachedActiveCellBorderPen.Width == borderThickeness &&
                this.cachedActiveCellBorderPen.Color == color)
                return this.cachedActiveCellBorderPen;

            if (this.cachedActiveCellBorderPen != null)
                this.cachedActiveCellBorderPen.Dispose();

            this.cachedActiveCellBorderPen = new Pen(color, borderThickeness);
            return this.cachedActiveCellBorderPen;
        }
        #endregion //GetCachedActiveCellBorderPen
        //
        #region OnDispose

        /// <summary>
        /// Called when element is disposed of.
        /// </summary>
        protected override void OnDispose()
        {
            if (this.cachedActiveCellBorderPen != null)
            {
                this.cachedActiveCellBorderPen.Dispose();
                this.cachedActiveCellBorderPen = null;
            }

            if (this.cachedSelectionBorderPen != null)
            {
                this.cachedSelectionBorderPen.Dispose();
                this.cachedSelectionBorderPen = null;
            }

            if (this.cachedSelectionOverlayBrush != null)
            {
                this.cachedSelectionOverlayBrush.Dispose();
                this.cachedSelectionOverlayBrush = null;
            }

            base.OnDispose();
        }
        #endregion //OnDispose

        // MBS 4/13/09 - NA9.2 Selection Overlay
        #region DrawSelectionOverlay

        private void DrawSelectionOverlay(ref UIElementDrawParams drawParams, UltraGridBand band, bool isRowSelection, Color selectionColor, Color borderColor)
        {
            using (Region cellRegion = new Region(Rectangle.Empty))
            using (Region borderRegion = new Region(Rectangle.Empty))
            {
                Border3DSide borders = Border3DSide.All;
                bool hasFoundSelection = false;
                int borderThickness = band.Layout.SelectionOverlayBorderThicknessResolved;

                // Walk down the UIElement chain looking for the appropriate elements that are selected
                DrawSelectionOverlayHelper(this, cellRegion, borderRegion, borderThickness, isRowSelection, ref borders, ref hasFoundSelection);

                // Draw the selection overlay, but only if the user could actually see it.
                if (selectionColor != Color.Empty && selectionColor.A > 0)
                {
                    SolidBrush brush = this.GetCachedSelectionOverlayBrush(selectionColor);
                    drawParams.Graphics.FillRegion(brush, cellRegion);
                }

                // If we're not drawing a border color we can bail out here
                if (borderColor == Color.Empty || borderColor.A == 0 || borderThickness <= 0)
                    return;

                RectangleF[] rects = borderRegion.GetRegionScans(new System.Drawing.Drawing2D.Matrix());

                // Excel doesn't draw multiple different thick borders for different selection areas,
                // so we won't either.  We differ from Excel here in that we will draw the selection
                // rect if it's contiguous, even if that region is created by multiple different
                // selection operations.
                if (rects == null || rects.Length != 1)
                    return;

                Pen pen = this.GetCachedSelectionBorderPen(borderThickness, borderColor);
                Rectangle borderRect = Rectangle.Truncate(rects[0]);

                // If we have previously inflated the region by the cell spacing, we need to undo this logic now.
                // Note that this doesn't apply to row selection, since we highlight everythign in this case
                if (!isRowSelection)
                {
                    int cellSpacing = band.CellSpacingResolved;
                    borderRect.Inflate(-cellSpacing, -cellSpacing);
                }

                if (borders == Border3DSide.All)
                    drawParams.Graphics.DrawRectangle(pen, borderRect);
                else
                {
                    Point bottomLeft = new Point(borderRect.X, borderRect.Bottom);
                    Point bottomRight = new Point(bottomLeft.X + borderRect.Width, bottomLeft.Y);
                    Point topRight = new Point(borderRect.Right, borderRect.Top);

                    if ((borders & Border3DSide.Left) != 0)
                    {
                        // MBS 5/29/09 - TFS17997
                        // We need to expand the height of the line when drawing it manually since we will
                        // only have the total height of the cells.
                        //
                        //drawParams.Graphics.DrawLine(pen, borderRect.Location, bottomLeft);
                        drawParams.Graphics.DrawLine(
                            pen,
                            new Point(borderRect.Location.X, borderRect.Location.Y - (borderThickness / 2)),
                            new Point(bottomLeft.X, bottomLeft.Y + (borderThickness / 2)));
                    }

                    if ((borders & Border3DSide.Bottom) != 0)
                        drawParams.Graphics.DrawLine(pen, bottomLeft, bottomRight);

                    if ((borders & Border3DSide.Right) != 0)
                    {
                        // MBS 5/29/09 - TFS17997
                        // We need to expand the height of the line when drawing it manually since we will
                        // only have the total height of the cells.
                        //
                        //drawParams.Graphics.DrawLine(pen, bottomRight, topRight);
                        drawParams.Graphics.DrawLine(
                            pen,
                            new Point(bottomRight.X, bottomRight.Y + (borderThickness / 2)),
                            new Point(topRight.X, topRight.Y - (borderThickness / 2)));
                    }

                    if ((borders & Border3DSide.Top) != 0)
                        drawParams.Graphics.DrawLine(pen, topRight, borderRect.Location);
                }
            }
        }

        private void DrawSelectionOverlayHelper(UIElement parent, Region cellRegion, Region borderRegion, int borderThickness, bool isRowSelection, ref Border3DSide borders, ref bool hasFoundSelection)
        {
            foreach (UIElement childElem in parent.ChildElements)
            {
                RowCellAreaUIElement rcElem = childElem as RowCellAreaUIElement;
                if (rcElem == null)
                {
                    if (childElem.HasChildElements)
                        DrawSelectionOverlayHelper(childElem, cellRegion, borderRegion, borderThickness, isRowSelection, ref borders, ref hasFoundSelection);

                    continue;
                }

                // The previous behavior with row selection is that we would highlight the entire row's element, not the 
                // individual cells within that, so we should preserve that behavior here.  
                if (isRowSelection)
                {
                    UltraGridRow row = rcElem.Row;
                    if (row.Selected)
                    {
                        Rectangle rowRect = rcElem.Rect;
                        cellRegion.Union(rowRect);

                        // We want the innermost edges of the borders to be drawn over the borders around the row,
                        // so we need to inflate the rect accordingly, taking into account the various rounding issues
                        // that occur with odd-numbered thicknesses (and even)
                        int inflateAmount = (int)borderThickness / 2;
                        if (borderThickness % 2 == 0)
                            inflateAmount--;

                        rowRect.Inflate(inflateAmount, inflateAmount);
                        if (borderThickness % 2 != 0)
                        {
                            rowRect.Width--;
                            rowRect.Height--;
                        }

                        borderRegion.Union(rowRect);

                        // We only need to perform the check to see if we should draw the top border if we haven't 
                        // already disabled it
                        if (!hasFoundSelection && (borders & Border3DSide.Top) != 0)
                        {
                            UltraGridRow prevRow = row.GetPrevVisibleRow();
                            if (prevRow != null && prevRow.ParentCollection == row.ParentCollection && prevRow.Selected)
                            {
                                // If there is a selected row before this row, denoted by either the VisibleIndex being 
                                // negative or that this is the first selected row we've found in the UIElements that are
                                // rendered, we know that we can skip drawing the top border
                                if (prevRow.VisibleIndex == -1 || !hasFoundSelection)
                                    borders ^= Border3DSide.Top;
                            }
                        }

                        hasFoundSelection = true;
                    }

                    continue;
                }

                // MBS 5/14/09 - TFS17485
                bool foundFirstCellElem = false;
                CellUIElement lastCellElem = null;
                Rectangle lastBorderRegion = Rectangle.Empty;

                foreach (UIElement rcChildElem in rcElem.ChildElements)
                {
                    CellUIElement cellElem = rcChildElem as CellUIElement;
                    if (cellElem == null || !(cellElem.Cell.Selected || cellElem.Column.Header.Selected))
                        continue;                    

                    // Calculate the rect that should be used for the cell border and merge it with the existing region
                    //
                    // MBS 5/13/09 - TFS17589
                    // When calculating the rect that is only for the overlay, don't include the border thickness since
                    // it is not relevant until we actually need to draw the borders
                    //Rectangle rect = UltraGridCell.CalculateCellOverlayRect(cellElem, borderThickness, false);
                    Rectangle rect = UltraGridCell.CalculateCellOverlayRect(cellElem, 0, false);

                    cellRegion.Union(rect);
                    
                    // MBS 5/13/09 - TFS17589
                    // Recalculate the rect to now include the border widths
                    rect = UltraGridCell.CalculateCellOverlayRect(cellElem, borderThickness, false);

                    // MBS 5/14/09 - TFS17485
                    // Check to see if there are any selected cells to the left of the first cell element
                    // so that we know if we should draw the left border or not
                    if (!foundFirstCellElem)
                    {
                        foundFirstCellElem = true;

                        if ((borders & Border3DSide.Left) != 0)
                        {
                            UltraGridCell[] leftCells = cellElem.Cell.GetAllCellsFromPosition(false, null);
                            if (leftCells.Length == 1)
                            {
                                // If we only get one cell from this method, it will be the current cell, so
                                // we can see if the cell is not going to be drawn so that we can skip the
                                // drawing of the left border
                                if (cellElem.ClipRect.X > cellElem.Rect.X)
                                    borders ^= Border3DSide.Left;
                            }
                            else
                            {
                                for (int i = leftCells.Length - 2; i >= 0; i--)
                                {
                                    if (leftCells[i].Selected)
                                    {
                                        borders ^= Border3DSide.Left;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    // Inflate the rect by the cell spacing so that we can draw the border around a contiguous 
                    // rectangular block even with cell spacing
                    rect.Inflate(cellElem.Cell.Band.CellSpacingResolved, cellElem.Cell.Band.CellSpacingResolved);
                    if (lastBorderRegion != Rectangle.Empty)
                        borderRegion.Union(lastBorderRegion);

                    lastBorderRegion = rect;

                    // We only need to perform the check to see if we should draw the top border if we haven't 
                    // already disabled it
                    if (!hasFoundSelection && (borders & Border3DSide.Top) != 0)
                    {
                        UltraGridRow prevRow = cellElem.Row.GetPrevVisibleRow();
                        if (prevRow != null && prevRow.ParentCollection == cellElem.Row.ParentCollection && 
                            (cellElem.Column.Header.Selected || prevRow.Cells[cellElem.Cell.Column].Selected))
                        {
                            // If there is a selected row before this row, denoted by either the VisibleIndex being 
                            // negative or that this is the first selected row we've found in the UIElements that are
                            // rendered, we know that we can skip drawing the top border
                            if (prevRow.VisibleIndex == -1 || !hasFoundSelection)
                                borders ^= Border3DSide.Top;
                        }
                    }

                    hasFoundSelection = true;

                    // MBS 5/14/09 - TFS17485
                    lastCellElem = cellElem;
                }

                // MBS 5/14/09 - TFS17485
                // Check to see if there are any selected cells to the right of the last cell element
                // so that we know if we should draw the left border or not
                if (lastBorderRegion != Rectangle.Empty)
                {
                    if (lastCellElem != null && (borders & Border3DSide.Right) != 0)
                    {
                        UltraGridCell[] leftCells = lastCellElem.Cell.GetAllCellsFromPosition(true, null);
                        for (int i = 1; i < leftCells.Length; i++)
                        {
                            if (leftCells[i].Selected)
                            {
                                borders ^= Border3DSide.Right;

                                // If we know that we have a selected cell to the right that is out of view, we can
                                // ensure that we draw the border to the edge of the RowColRegionIntersection element.
                                if (this.Rect.Right > lastBorderRegion.Right)                                
                                    lastBorderRegion.Width += this.Rect.Right - lastBorderRegion.Right;                                

                                break;
                            }
                        }
                    }
                    borderRegion.Union(lastBorderRegion);
                }
            }
        }
        #endregion //DrawSelectionOverlay
        //
        #region GetCachedSelectionOverlayBrush

        private SolidBrush GetCachedSelectionOverlayBrush(Color color)
        {
            if (this.cachedSelectionOverlayBrush != null &&
                this.cachedSelectionOverlayBrush.Color == color)
                return this.cachedSelectionOverlayBrush;

            if (this.cachedSelectionOverlayBrush != null)
                this.cachedSelectionOverlayBrush.Dispose();

            this.cachedSelectionOverlayBrush = new SolidBrush(color);
            return this.cachedSelectionOverlayBrush;
        }
        #endregion //GetCachedSelectionOverlayBrush
        //
        #region GetCachedSelectionBorderPen

        private Pen GetCachedSelectionBorderPen(int borderThickeness, Color color)
        {
            if (this.cachedSelectionBorderPen != null &&
                (int)this.cachedSelectionBorderPen.Width == borderThickeness &&
                this.cachedSelectionBorderPen.Color == color)
                return this.cachedSelectionBorderPen;

            if (this.cachedSelectionBorderPen != null)
                this.cachedSelectionBorderPen.Dispose();

            this.cachedSelectionBorderPen = new Pen(color, borderThickeness);
            return this.cachedSelectionBorderPen;
        }
        #endregion //GetCachedSelectionBorderPen
    }
}
