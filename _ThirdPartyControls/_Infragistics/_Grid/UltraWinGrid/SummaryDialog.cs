#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Summary description for SummaryDialog.
	/// </summary>
	// SSP 11/5/04 UWG3789
	// Added SummaryDialog property on the BeforeSummaryDialogEventArgs.
	//
	//internal class SummaryDialog : System.Windows.Forms.Form
	public class SummaryDialog : System.Windows.Forms.Form
	{
		private UltraGridColumn column = null;
		private System.Windows.Forms.CheckBox averageCheckBox;
		private System.Windows.Forms.CheckBox countCheckBox;
		private System.Windows.Forms.CheckBox maximumCheckBox;
		private System.Windows.Forms.CheckBox minimumCheckBox;
		private System.Windows.Forms.CheckBox sumCheckBox;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Panel panelCheckboxes;
		private System.Windows.Forms.Panel panelRadios;
		private System.Windows.Forms.RadioButton averageRadio;
		private System.Windows.Forms.RadioButton countRadio;
		private System.Windows.Forms.RadioButton maximumRadio;
		private System.Windows.Forms.RadioButton minimumRadio;
		private System.Windows.Forms.RadioButton sumRadio;
		private System.Windows.Forms.RadioButton noneRadio;
		private Infragistics.Win.AppStyling.Runtime.InboxControlStyler inboxControlStyler1;
		private System.ComponentModel.IContainer components;

		internal SummaryDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			InitializeDialogStrings();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.averageCheckBox = new System.Windows.Forms.CheckBox();
			this.countCheckBox = new System.Windows.Forms.CheckBox();
			this.maximumCheckBox = new System.Windows.Forms.CheckBox();
			this.minimumCheckBox = new System.Windows.Forms.CheckBox();
			this.sumCheckBox = new System.Windows.Forms.CheckBox();
			this.okButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.panelCheckboxes = new System.Windows.Forms.Panel();
			this.panelRadios = new System.Windows.Forms.Panel();
			this.noneRadio = new System.Windows.Forms.RadioButton();
			this.sumRadio = new System.Windows.Forms.RadioButton();
			this.minimumRadio = new System.Windows.Forms.RadioButton();
			this.maximumRadio = new System.Windows.Forms.RadioButton();
			this.countRadio = new System.Windows.Forms.RadioButton();
			this.averageRadio = new System.Windows.Forms.RadioButton();
			this.inboxControlStyler1 = new Infragistics.Win.AppStyling.Runtime.InboxControlStyler(this.components);
			this.panelCheckboxes.SuspendLayout();
			this.panelRadios.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.inboxControlStyler1)).BeginInit();
			this.SuspendLayout();
			// 
			// averageCheckBox
			// 
			this.averageCheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.averageCheckBox.Location = new System.Drawing.Point(16, 8);
			this.averageCheckBox.Name = "averageCheckBox";
			this.averageCheckBox.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.averageCheckBox, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.averageCheckBox.TabIndex = 0;
			this.averageCheckBox.Text = "Average";
			this.averageCheckBox.CheckStateChanged += new System.EventHandler(this.checkBoxCheckStateChanged);
			// 
			// countCheckBox
			// 
			this.countCheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.countCheckBox.Location = new System.Drawing.Point(16, 34);
			this.countCheckBox.Name = "countCheckBox";
			this.countCheckBox.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.countCheckBox, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.countCheckBox.TabIndex = 1;
			this.countCheckBox.Text = "Count";
			this.countCheckBox.CheckStateChanged += new System.EventHandler(this.checkBoxCheckStateChanged);
			// 
			// maximumCheckBox
			// 
			this.maximumCheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.maximumCheckBox.Location = new System.Drawing.Point(16, 60);
			this.maximumCheckBox.Name = "maximumCheckBox";
			this.maximumCheckBox.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.maximumCheckBox, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.maximumCheckBox.TabIndex = 2;
			this.maximumCheckBox.Text = "Maximum";
			this.maximumCheckBox.CheckStateChanged += new System.EventHandler(this.checkBoxCheckStateChanged);
			// 
			// minimumCheckBox
			// 
			this.minimumCheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.minimumCheckBox.Location = new System.Drawing.Point(16, 86);
			this.minimumCheckBox.Name = "minimumCheckBox";
			this.minimumCheckBox.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.minimumCheckBox, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.minimumCheckBox.TabIndex = 3;
			this.minimumCheckBox.Text = "Minimum";
			this.minimumCheckBox.CheckStateChanged += new System.EventHandler(this.checkBoxCheckStateChanged);
			// 
			// sumCheckBox
			// 
			this.sumCheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.sumCheckBox.Location = new System.Drawing.Point(16, 112);
			this.sumCheckBox.Name = "sumCheckBox";
			this.sumCheckBox.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.sumCheckBox, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.sumCheckBox.TabIndex = 4;
			this.sumCheckBox.Text = "Sum";
			this.sumCheckBox.CheckStateChanged += new System.EventHandler(this.checkBoxCheckStateChanged);
			// 
			// okButton
			// 
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.okButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.okButton.Location = new System.Drawing.Point(16, 144);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(80, 32);
			this.inboxControlStyler1.SetStyleSettings(this.okButton, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.okButton.TabIndex = 5;
			this.okButton.Text = "O&k";
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.cancelButton.Location = new System.Drawing.Point(112, 144);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(96, 32);
			this.inboxControlStyler1.SetStyleSettings(this.cancelButton, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.cancelButton.TabIndex = 6;
			this.cancelButton.Text = "&Cancel";
			// 
			// panelCheckboxes
			// 
			this.panelCheckboxes.Controls.AddRange(new System.Windows.Forms.Control[] {
																						  this.sumCheckBox,
																						  this.averageCheckBox,
																						  this.maximumCheckBox,
																						  this.minimumCheckBox,
																						  this.countCheckBox});
			this.panelCheckboxes.Name = "panelCheckboxes";
			this.panelCheckboxes.Size = new System.Drawing.Size(224, 144);
			this.inboxControlStyler1.SetStyleSettings(this.panelCheckboxes, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.panelCheckboxes.TabIndex = 7;
			// 
			// panelRadios
			// 
			this.panelRadios.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this.noneRadio,
																					  this.sumRadio,
																					  this.minimumRadio,
																					  this.maximumRadio,
																					  this.countRadio,
																					  this.averageRadio});
			this.panelRadios.Name = "panelRadios";
			this.panelRadios.Size = new System.Drawing.Size(224, 144);
			this.inboxControlStyler1.SetStyleSettings(this.panelRadios, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.panelRadios.TabIndex = 8;
			// 
			// noneRadio
			// 
			this.noneRadio.Location = new System.Drawing.Point(16, 5);
			this.noneRadio.Name = "noneRadio";
			this.noneRadio.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.noneRadio, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.noneRadio.TabIndex = 5;
			this.noneRadio.Text = "None";
			// 
			// sumRadio
			// 
			this.sumRadio.Location = new System.Drawing.Point(16, 114);
			this.sumRadio.Name = "sumRadio";
			this.sumRadio.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.sumRadio, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.sumRadio.TabIndex = 4;
			this.sumRadio.Text = "Sum";
			// 
			// minimumRadio
			// 
			this.minimumRadio.Location = new System.Drawing.Point(16, 92);
			this.minimumRadio.Name = "minimumRadio";
			this.minimumRadio.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.minimumRadio, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.minimumRadio.TabIndex = 3;
			this.minimumRadio.Text = "Minimum";
			// 
			// maximumRadio
			// 
			this.maximumRadio.Location = new System.Drawing.Point(16, 70);
			this.maximumRadio.Name = "maximumRadio";
			this.maximumRadio.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.maximumRadio, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.maximumRadio.TabIndex = 2;
			this.maximumRadio.Text = "Maximum";
			// 
			// countRadio
			// 
			this.countRadio.Location = new System.Drawing.Point(16, 48);
			this.countRadio.Name = "countRadio";
			this.countRadio.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.countRadio, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.countRadio.TabIndex = 1;
			this.countRadio.Text = "Count";
			// 
			// averageRadio
			// 
			this.averageRadio.Location = new System.Drawing.Point(16, 26);
			this.averageRadio.Name = "averageRadio";
			this.averageRadio.Size = new System.Drawing.Size(200, 24);
			this.inboxControlStyler1.SetStyleSettings(this.averageRadio, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.averageRadio.TabIndex = 0;
			this.averageRadio.Text = "Average";
			// 
			// SummaryDialog
			// 
			this.AcceptButton = this.okButton;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(226, 182);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.panelCheckboxes,
																		  this.panelRadios,
																		  this.cancelButton,
																		  this.okButton});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SummaryDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.inboxControlStyler1.SetStyleSettings(this, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.Text = "Select Summaries";
			this.VisibleChanged += new System.EventHandler(this.SummaryDialog_VisibleChanged);
			this.panelCheckboxes.ResumeLayout(false);
			this.panelRadios.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.inboxControlStyler1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void InitializeDialogStrings()
		{
			// SSP 4/8/03 UWG2099
			//
			this.averageCheckBox.Text = SR.GetString("SummaryDialogAverage");//"Average";
			this.countCheckBox.Text = SR.GetString("SummaryDialogCount");//"Count";
			this.maximumCheckBox.Text = SR.GetString("SummaryDialogMaximum");//"Maximum";
			this.minimumCheckBox.Text = SR.GetString("SummaryDialogMinimum");//"Minimum";
			this.okButton.Text = SR.GetString("SummaryDialog_Button_OK");//"O&k";
			this.sumCheckBox.Text = SR.GetString("SummaryDialogSum");//"Sum";
			this.cancelButton.Text = SR.GetString("SummaryDialog_Button_Cancel");//"&Cancel";
			this.Text = SR.GetString("LDR_SelectSummaries");//"Select Summaries";
			
			// SSP 8/11/03
			//
			this.averageRadio.Text = SR.GetString("SummaryDialogAverage");//"Average";
			this.countRadio.Text = SR.GetString("SummaryDialogCount");//"Count";
			this.maximumRadio.Text = SR.GetString("SummaryDialogMaximum");//"Maximum";
			this.minimumRadio.Text = SR.GetString("SummaryDialogMinimum");//"Minimum";
			this.sumRadio.Text = SR.GetString("SummaryDialogSum");//"Sum";
			this.noneRadio.Text = SR.GetString("SummaryDialogNone");//"Average";
            
			

			// MD 11/15/06
			// Dialog Accessibility
			this.AccessibleName = this.Text;
			this.AccessibleDescription = SR.GetString( "LDR_SelectSummaries_AccessibleDescription" );
			this.averageCheckBox.AccessibleName = this.averageCheckBox.Text;
			this.averageCheckBox.AccessibleDescription = SR.GetString( "SummaryDialogAverage_AccessibleDescription" );
			this.averageRadio.AccessibleName = this.averageRadio.Text;
			this.averageRadio.AccessibleDescription = SR.GetString( "SummaryDialogAverage_AccessibleDescription" );
			this.countCheckBox.AccessibleName = this.countCheckBox.Text;
			this.countCheckBox.AccessibleDescription = SR.GetString( "SummaryDialogCount_AccessibleDescription" );
			this.countRadio.AccessibleName = this.countRadio.Text;
			this.countRadio.AccessibleDescription = SR.GetString( "SummaryDialogCount_AccessibleDescription" );
			this.maximumCheckBox.AccessibleName = this.maximumCheckBox.Text;
			this.maximumCheckBox.AccessibleDescription = SR.GetString( "SummaryDialogMaximum_AccessibleDescription" );
			this.maximumRadio.AccessibleName = this.maximumRadio.Text;
			this.maximumRadio.AccessibleDescription = SR.GetString( "SummaryDialogMaximum_AccessibleDescription" );
			this.minimumCheckBox.AccessibleName = this.minimumCheckBox.Text;
			this.minimumCheckBox.AccessibleDescription = SR.GetString( "SummaryDialogMinimum_AccessibleDescription" );
			this.minimumRadio.AccessibleName = this.minimumRadio.Text;
			this.minimumRadio.AccessibleDescription = SR.GetString( "SummaryDialogMinimum_AccessibleDescription" );
			this.sumCheckBox.AccessibleName = this.sumCheckBox.Text;
			this.sumCheckBox.AccessibleDescription = SR.GetString( "SummaryDialogSum_AccessibleDescription" );
			this.sumRadio.AccessibleName = this.sumRadio.Text;
			this.sumRadio.AccessibleDescription = SR.GetString( "SummaryDialogSum_AccessibleDescription" );
			this.noneRadio.AccessibleName = this.noneRadio.Text;
			this.noneRadio.AccessibleDescription = SR.GetString( "SummaryDialogNone_AccessibleDescription" );
			this.okButton.AccessibleName = this.okButton.Text;
			this.cancelButton.AccessibleName = this.cancelButton.Text;
		}

		private void checkBoxCheckStateChanged(object sender, System.EventArgs e)
		{
		
		}

		private UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		// SSP 12/5/03 UWG2295
		// Changed the return type from void to bool.
		// 
		/// <summary>
		/// If the check box for the summary type is checked then adds that summary type to the summaries collection if it doesn't already exist. If the check box is unchecked, then it removes it.
		/// </summary>
		/// <param name="checkBoxChecked"></param>
		/// <param name="summaryType"></param>
		/// <returns>Returns true if a summary was added or removed false otherwise.</returns>
		//private void SyncSummaryWithCheckBox( bool checkBoxChecked, SummaryType summaryType )
		private bool SyncSummaryWithCheckBox( bool checkBoxChecked, SummaryType summaryType )
		{
			SummarySettingsCollection summaries = this.Column.Band.Summaries;

			SummarySettings summary = summaries.FindSummary( this.Column, summaryType, true );

			if ( checkBoxChecked )
			{
				// If the summary check box is checked, then add the summary
				// if it already doesn't exist.
				//
				if ( null == summary )
				{
					summaries.Add( summaryType, 
						null, 
						this.Column, 
						SummaryPosition.UseSummaryPositionColumn, 
						this.Column );
					
					// SSP 12/5/03 UWG2295
					// Changed the return type from void to bool.
					// 
					return true;
				}
			}
			else
			{
				if ( null != summary )
				{
					summaries.Remove( summary );

					// SSP 12/5/03 UWG2295
					// Changed the return type from void to bool.
					// 
					return true;
				}
			}

			return false;
		}

		// SSP 12/5/03 UWG2295
		// Changed the return type from void to bool.
		// 
		//internal void Show( UltraGridColumn column )
		internal bool Show( UltraGridColumn column )
		{
			if ( null == column || null == column.Band || null == column.DataType )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_149"),													 Shared.SR.GetString("LE_ArgumentNullException_103") );

			this.column = column;

			IWin32Window ownerWindow = null;

			try
			{
				ownerWindow = this.Column.Layout.Grid.FindForm( );
			}
			catch ( Exception )
			{
			}
			

			DialogResult result = this.ShowDialog( ownerWindow );

			// SSP 12/5/03 UWG2295
			// Changed the return type from void to bool.
			// 
			bool summariesChanged = false;

			// If the user presses Ok, then apply the changes in the
			// selected summaries to the band's summaries property.
			//
			if ( DialogResult.OK == result )
			{
				// Now syncronize all the summaries with the checkbox states
				//
				if ( this.Column.SingleSummariesOnly )
				{
					summariesChanged = this.SyncSummaryWithCheckBox( this.averageRadio.Checked, SummaryType.Average ) || summariesChanged;
					summariesChanged = this.SyncSummaryWithCheckBox( this.sumRadio.Checked, SummaryType.Sum ) || summariesChanged;
					summariesChanged = this.SyncSummaryWithCheckBox( this.minimumRadio.Checked, SummaryType.Minimum ) || summariesChanged;
					summariesChanged = this.SyncSummaryWithCheckBox( this.maximumRadio.Checked, SummaryType.Maximum ) || summariesChanged;
					summariesChanged = this.SyncSummaryWithCheckBox( this.countRadio.Checked, SummaryType.Count ) || summariesChanged;
				}
				else
				{
					summariesChanged = this.SyncSummaryWithCheckBox( this.averageCheckBox.Checked, SummaryType.Average ) || summariesChanged;
					summariesChanged = this.SyncSummaryWithCheckBox( this.sumCheckBox.Checked, SummaryType.Sum ) || summariesChanged;
					summariesChanged = this.SyncSummaryWithCheckBox( this.minimumCheckBox.Checked, SummaryType.Minimum ) || summariesChanged;
					summariesChanged = this.SyncSummaryWithCheckBox( this.maximumCheckBox.Checked, SummaryType.Maximum ) || summariesChanged;
					summariesChanged = this.SyncSummaryWithCheckBox( this.countCheckBox.Checked, SummaryType.Count ) || summariesChanged;
				}
			}

			return summariesChanged;
		}

		private void SummaryDialog_VisibleChanged(object sender, System.EventArgs e)
		{	
			// When the form is shown, set the enable or disable the checkboxes for
			// summaries based on the data type of the column for which this summary
			// dialog is being shown. We do this because only some summaries make
			// sense for certain data types. (ie, date can not be summed).
			//
			if ( this.Visible )
			{
				// SSP 8/8/03 - Row Layout Functionality - Multiple Summaries
				// Added Single and SingleBasedOnType enum members to AllowSummaries.
				// Modified the dialog and added a panel with radios so the user 
				// can only select one summary.
				//

				bool singleSummaries = this.Column.SingleSummariesOnly;
				this.panelCheckboxes.Visible = ! singleSummaries;
				this.panelRadios.Visible = singleSummaries;

				UltraGridColumn column = this.Column;

				Debug.Assert( null != column, "No column !" );
				System.Type dataType = null != column ? column.DataType : null;
				Debug.Assert( null != dataType, "No column data type!" );

				if ( null != column && null != dataType )
				{
					// For numeric type, all these summaries are applicable
					//
					this.averageCheckBox.Enabled = false;
					this.countCheckBox.Enabled = false;
					this.minimumCheckBox.Enabled = false;
					this.maximumCheckBox.Enabled = false;
					this.sumCheckBox.Enabled = false;

					this.averageRadio.Enabled = false;
					this.countRadio.Enabled = false;
					this.minimumRadio.Enabled = false;
					this.maximumRadio.Enabled = false;
					this.sumRadio.Enabled = false;

					SummarySettingsCollection summaries = this.Column.Band.Summaries;
					Debug.Assert( null != summaries, "Band.Summaries property returned null !" );

					if ( null != summaries )
					{
						if ( singleSummaries )
						{
							this.averageRadio.Checked = null != summaries.FindSummary( this.Column, SummaryType.Average, true );
							this.countRadio.Checked = null != summaries.FindSummary( this.Column, SummaryType.Count, true );
							this.minimumRadio.Checked = null != summaries.FindSummary( this.Column, SummaryType.Minimum, true );
							this.maximumRadio.Checked = null != summaries.FindSummary( this.Column, SummaryType.Maximum, true );
							this.sumRadio.Checked = null != summaries.FindSummary( this.Column, SummaryType.Sum, true );
						}
						else
						{
							this.averageCheckBox.Checked = null != summaries.FindSummary( this.Column, SummaryType.Average, true );
							this.countCheckBox.Checked = null != summaries.FindSummary( this.Column, SummaryType.Count, true );
							this.minimumCheckBox.Checked = null != summaries.FindSummary( this.Column, SummaryType.Minimum, true );
							this.maximumCheckBox.Checked = null != summaries.FindSummary( this.Column, SummaryType.Maximum, true );
							this.sumCheckBox.Checked = null != summaries.FindSummary( this.Column, SummaryType.Sum, true );
						}
					}

					if ( column.IsColumnNumeric )
					{
						// For numeric type, all these summaries are applicable
						//
						if ( singleSummaries )
						{
							this.averageRadio.Enabled = true;
							this.countRadio.Enabled = true;
							this.minimumRadio.Enabled = true;
							this.maximumRadio.Enabled = true;
							this.sumRadio.Enabled = true;
						}
						else
						{
							this.averageCheckBox.Enabled = true;
							this.countCheckBox.Enabled = true;
							this.minimumCheckBox.Enabled = true;
							this.maximumCheckBox.Enabled = true;
							this.sumCheckBox.Enabled = true;
						}
					}
					else if ( typeof( DateTime ) == dataType )
					{
						// For DateTime type all except average and sum are
						// applicable
						//
						if ( singleSummaries )
						{
							this.countRadio.Enabled = true;
							this.minimumRadio.Enabled = true;
							this.maximumRadio.Enabled = true;
						}
						else
						{
							this.countCheckBox.Enabled = true;
							this.minimumCheckBox.Enabled = true;
							this.maximumCheckBox.Enabled = true;
						}
					}
					else if ( typeof( IComparable ).IsAssignableFrom( dataType ) )
					{
						// For types that implement IComparable minimum,
						// maximum and count are applicable.
						//
						if ( singleSummaries )
						{
							this.countRadio.Enabled = true;
							this.minimumRadio.Enabled = true;
							this.maximumRadio.Enabled = true;
						}
						else
						{
							this.countCheckBox.Enabled = true;
							this.minimumCheckBox.Enabled = true;
							this.maximumCheckBox.Enabled = true;
						}
					}
					else
					{
						// Of other types, only count is applicable because
						// we woudn't know how to calculate the other summaries.
						//
						if ( singleSummaries )
						{
							this.countRadio.Enabled = true;
						}
						else
						{
							this.countCheckBox.Enabled = true;
						}
					}
				}
			}
		}

		// AS 1/29/07 NA 2007 Vol 1
		internal Infragistics.Win.AppStyling.Runtime.InboxControlStyler InboxControlStyler
		{
			get { return this.inboxControlStyler1; }
		}
	}
}
