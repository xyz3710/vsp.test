#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Runtime.InteropServices;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Windows.Forms.Design;
	using System.Collections;
	using System.Diagnostics;
	using Infragistics.Shared;
	using System.Drawing.Printing;
	using Infragistics.Win.UltraWinMaskedEdit;
	using Infragistics.Win.Layout;
	using System.Collections.Generic;
	using Infragistics.Win.UltraWinGrid.Design;
	

	/// <summary>
	/// The Infragistics UltraGrid control is designed to present, and optionally edit, both flat data (containing a single set of rows and columns) as well as hierarchical data in a variety of view styles.
	/// </summary>
	/// <remarks>
	/// <p class="body">Most of the settings that apply to the control are set off the <see cref="Infragistics.Win.UltraWinGrid.UltraGridBase.DisplayLayout"/> property.</p>
	/// <p></p>
	/// <p class="body">
	/// The following code snippet illustrates.
	/// </p>
	/// <p></p>
	/// <code>
	/// private void button5_Click(object sender, System.EventArgs e)
	/// {
	/// 	this.ultraGrid1.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
	/// 	this.ultraGrid1.DisplayLayout.Bands[0].Columns[1].Hidden = true;
	/// }
	/// </code>
	/// </remarks>
	
	// AS 4/8/05
	//
	
	[ToolboxBitmap(typeof(UltraGridBase), AssemblyVersion.ToolBoxBitmapFolder + "UltraGrid.bmp")] // MRS 11/13/05
	[ DefaultProperty( "DisplayLayout" ) ]
	[ Designer( "Infragistics.Win.UltraWinGrid.Design.UltraGridDesigner, " + AssemblyRef.Design ) ]
	[ DefaultEvent( "InitializeLayout" ) ]
	// AS - 12/7/01  Moved layouts to a wizard accessible through a verb.
	//[ UltraPropertyPage( "Layouts", typeof(UltraGridLayoutPropControl) ) ]

	// AS - 12/7/01 Moved to UltraGridBase since this can be used for all controls.
	//[ UltraPropertyPage( "Groups And Columns", typeof(UltraGridGroupColumnsPropControl) )]
	[ComplexBindingProperties("DataSource", "DataMember")]	// AS 3/28/05 Drag-Once Binding
	[ LocalizedDescription("LD_UltraGrid") ] // JAS BR06191 10/10/05
	public class UltraGrid : UltraGridBase,
		Infragistics.Win.ISelectionManager,
		Infragistics.Shared.IUltraLicensedComponent,
		ISupportInitialize,
        // BF 4/21/08  NA 2008.2 - WinValidator
        IValidatorClient,
		IGridDesignInfo
	{
		#region Private members

		private UltraLicense				license = null;
 
		private GridKeyActionMappings       keyActionMappings = null;

		
		private Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = null;
		private Icon						activeRowIcon = null;
		private Icon						activeRowAddedIcon = null;
		private Icon						activeRowModifiedIcon = null;
		private Icon						rowAddedIcon = null;
		private Icon						rowModifiedIcon = null;
		private bool						isPrinting = false;

		
		// SSP 4/26/02
		// Em Embeddable editors.
		// 
		
		
		private const int CBN_CLOSEUP        = 8;
		private const int WM_COMMAND		= 0x0111;

		

		private	Infragistics.Win.UltraWinGrid.UpdateMode 
			updateMode;

		private ISelectionStrategyFilter    selectionStrategyFilter = null;

		private Infragistics.Win.UltraWinGrid.GridEventManager
			eventManager = null;
		
		private UltraGridLayout						printLayout = null;
		private PrintManager				printManager = null;

		private DragEffect					dragEffect	= null;

		private Selected					selected = null;
		private Selected					initialSelection = null;
		
		private ValueList					swapDropDown = null;
		private Infragistics.Win.ToolTip    toolTip = null;
		

		// SSP 9/27/01 UWG341
		// Added internal flag that indicates whether we are currently
		// dragging 
		//
		private bool						isDragging = false;
		
		// JJD 1/11/02
		// Added member variable to cache the card area during a drag
		//
		private CardAreaUIElement			cardAreaPivot = null;

		// SSP 1/23/02
		// Added in_getActiveCell anti-recursion flag
		//
		private bool in_getActiveCell					 = false;


		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		// SSP 3/21/02
//		// Added filterDropDown and filterDropDownButtonImage vars
//		//
//		private ValueList					filterDropDown = null;
		// SSP 8/28/03 - Optimizations
		// Instead of creating the form and keeping it around, recreate it every time
		// it's displayed.
		//
		//private CustomRowFiltersDialog		customRowFiltersDialog = null;


		// SSP 4/15/02
		// Embeddable editor vars
		//
		internal bool editorWantedLastKeyDown = false;

		// SSP 11/5/04 - Optimizations
		// Don't cache the row summaries dialog. Create it every time it's needed.
		//
		

		// SSP 6/18/02 UWG1229
		// Added a flag to prevent the rows from being loaded until the 
		// print layout and the main layout have been merged. We want to
		// carry over any unbound columns first before firing the initialize
		// row.
		//
		private bool printing_DontInitializeRows = false; 

		// SSP 8/20/02 UWG1583
		//
		// SSP 10/10/03
		// This property is not used anywhere so commented it out.
		//
		//private GridKeyActionMappings allActionsMappings = null;
		
		// SSP 8/29/02 UWG1564
		//
		private bool exitEditModeOnLeave = true;

		// SSP 9/25/02 UWG1375
		// Added a flag to fix the bug.
		//
		private bool settingActiveRowToTempActiveRow = false;

		// SSP 12/6/02 UWG1877
		// Added a flag to prevent the UltraGridCell.ExitEditMode method from giving grid the focus.
		//
		internal bool inOnLeave = false;

		// SSP 1/14/03 UWG1928
		// Added RowUpdateCancelAction property.
		//
		Infragistics.Win.UltraWinGrid.RowUpdateCancelAction rowUpdateCancelAction = RowUpdateCancelAction.CancelUpdate;


		// JM 03-25-04 NAS 2004 Vol 2
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//internal static readonly int		DESIGNER_PROMPT_AREA_HEIGHT					= 32;
		internal const int DESIGNER_PROMPT_AREA_HEIGHT = 32;

		// MRS 3/14/05 - Column Moving for RowLayouts
		internal GridBagLayoutDragStrategy gridBagLayoutDragStrategy = null;
		private Cursor spanResizeCursorVert = null;
		private Cursor spanResizeCursorHoriz = null;

		// SSP 5/18/05 BR03434
		// Added SyncWithCurrencyManager to prevent the UltraGrid from synchronizing with
		// the currency manager. When there are a lot of deep nested bands activating a 
		// different ancestor row caues a lot of slowdown.
		//
		private bool syncWithCurrencyManager = true;

        // MBS 3/12/08 - RowEditTemplate NA2008 V2
        private bool hookedIntoComponentChangeService;

        // MRS 2/4/2009 - TFS13513
        internal const double DRAG_INDICATOR_OPACITY = 0.6;

        // CDS 9.2 Column Moving Indicators
        private Image defaultDropIndicatorDownArrowImage = null;

        // CDS 4/29/09 9.2 Column Moving Indicators
        // Need to store a reference to a colorizable arrow image
        private Image colorizableDropIndicatorDownArrowImage = null;

        #endregion

		#region Static Event Objects
		// Instead of exposing public event fields, expose public event properties
		// which minimizes overhead when no one listens to the event.
		//
		private static readonly object EVENT_AFTERCELLACTIVATE = new object();
		private static readonly object EVENT_AFTERCELLCANCELUPDATE = new object();
		private static readonly object EVENT_AFTERCELLLISTCLOSEUP = new object();
		private static readonly object EVENT_AFTERCELLUPDATE = new object();
		
		
		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
		//private static readonly object EVENT_AFTERCOLPOSCHANGED = new object();

		private static readonly object EVENT_AFTERCOLREGIONSCROLL = new object();
		private static readonly object EVENT_AFTERCOLREGIONSIZE = new object();
		private static readonly object EVENT_AFTERENTEREDITMODE = new object();
		private static readonly object EVENT_AFTEREXITEDITMODE = new object();
		private static readonly object EVENT_AFTERGROUPPOSCHANGED = new object();
		private static readonly object EVENT_AFTERROWACTIVATE = new object();
		private static readonly object EVENT_AFTERROWCANCELUPDATE = new object();
		private static readonly object EVENT_AFTERROWCOLLAPSED = new object();
		private static readonly object EVENT_AFTERROWEXPANDED = new object();
		private static readonly object EVENT_AFTERROWINSERT = new object();
		private static readonly object EVENT_AFTERROWREGIONSCROLL = new object();
		private static readonly object EVENT_AFTERROWREGIONSIZE = new object();
		private static readonly object EVENT_AFTERROWRESIZE = new object();
		private static readonly object EVENT_AFTERROWUPDATE = new object();
		private static readonly object EVENT_AFTERROWSDELETED = new object();
		private static readonly object EVENT_AFTERSELECTCHANGE = new object();
		
		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
		//private static readonly object EVENT_AFTERSORTCHANGE = new object();

		private static readonly object EVENT_BEFOREAUTOSIZEEDIT = new object();
		private static readonly object EVENT_BEFORECELLACTIVATE = new object();
		private static readonly object EVENT_BEFORECELLCANCELUPDATE = new object();
		private static readonly object EVENT_BEFORECELLDEACTIVATE = new object();
		private static readonly object EVENT_BEFORECELLLISTDROPDOWN = new object();
		private static readonly object EVENT_BEFORECELLUPDATE = new object();
		
		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
		//private static readonly object EVENT_BEFORECOLPOSCHANGED = new object();
		
		private static readonly object EVENT_BEFORECOLREGIONREMOVED = new object();
		private static readonly object EVENT_BEFORECOLREGIONSCROLL = new object();
		private static readonly object EVENT_BEFORECOLREGIONSIZE = new object();
		private static readonly object EVENT_BEFORECOLREGIONSPLIT = new object();
		private static readonly object EVENT_BEFOREENTEREDITMODE = new object();
		private static readonly object EVENT_BEFOREEXITEDITMODE = new object();
		private static readonly object EVENT_BEFOREGROUPPOSCHANGED = new object();
		private static readonly object EVENT_BEFOREPRINT = new object();
		private static readonly object EVENT_BEFOREROWACTIVATE = new object();
		private static readonly object EVENT_BEFOREROWCANCELUPDATE = new object();
		private static readonly object EVENT_BEFOREROWCOLLAPSED = new object();
		private static readonly object EVENT_BEFOREROWDEACTIVATE = new object();
		private static readonly object EVENT_BEFOREROWEXPANDED = new object();
		private static readonly object EVENT_BEFOREROWINSERT = new object();
		private static readonly object EVENT_BEFOREROWREGIONREMOVED = new object();
		private static readonly object EVENT_BEFOREROWREGIONSCROLL = new object();
		private static readonly object EVENT_BEFOREROWREGIONSIZE = new object();
		private static readonly object EVENT_BEFOREROWREGIONSPLIT = new object();
		private static readonly object EVENT_BEFOREROWRESIZE = new object();
		private static readonly object EVENT_BEFOREROWUPDATE = new object();
		private static readonly object EVENT_BEFOREROWSDELETED = new object();
		private static readonly object EVENT_BEFORESELECTCHANGE = new object();
		
		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
		//private static readonly object EVENT_AFTERSORTCHANGE = new object();

		private static readonly object EVENT_CELLCHANGE = new object();
		private static readonly object EVENT_CELLLISTSELECT = new object();
		private static readonly object EVENT_CLICKCELLBUTTON = new object();
		private static readonly object EVENT_ERROR = new object();
		// SSP 5/2/02
		// Added new CellDataError event in version 2 of the grid.
		//
		private static readonly object EVENT_CELL_DATA_ERROR = new object();
		private static readonly object EVENT_INTIALIZEGROUPBYROW = new object();
		private static readonly object EVENT_INTIALIZELAYOUT = new object();
		private static readonly object EVENT_INTIALIZELOGICALPRINTPAGE = new object();
		private static readonly object EVENT_INTIALIZEPRINT = new object();
		private static readonly object EVENT_INTIALIZEPRINTPREVIEW = new object();
		private static readonly object EVENT_INTIALIZEROW = new object();
		private static readonly object EVENT_SELECTIONDRAG = new object();

		// JAS v5.2 DoubleClick Events 4/27/05
		//
		private static readonly object EVENT_DOUBLECLICKROW = new object();
		private static readonly object EVENT_DOUBLECLICKCELL = new object();
		private static readonly object EVENT_DOUBLECLICKHEADER = new object();



		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		// SSP 3/21/02
//		// Added events for row filtering
//		//------------------------------------------------------------
//		// SSP 8/1/03
//		// Moved the FilterRow event to the UltraGridBase.
//		//
//		//private static readonly object EVENT_FILTERROW = new object( );
//		private static readonly object EVENT_BEFOREROWFILTERDROPDOWN = new object( );
//		private static readonly object EVENT_BEFORECUSTOMROWFILTERDIALOG = new object();
//		//------------------------------------------------------------

		// SSP 5/17/02
		// Added events for summary rows.
		//------------------------------------------------------------
		private static readonly object EVENT_BEFORESUMMARYDIALOG = new object( );
		private static readonly object EVENT_SUMMARYVALUECHANGED = new object( );

		// SSP 12/5/03 UWG2295
		// Added AfterSummaryDialog event.
		// 
		private static readonly object EVENT_AFTERSUMMARYDIALOG  = new object( );
		//------------------------------------------------------------

		// SSP 6/24/03 UWG2413 - Row Layout Functioanlity
		// Added events that we need to fire when a cell or a header 
		// is resized in the row-layout mode.
		// ----------------------------------------------------------
		private static readonly object EVENT_BEFOREROWLAYOUTITEMRESIZED = new object( );
		private static readonly object EVENT_AFTERROWLAYOUTITEMRESIZED = new object( );
		// ----------------------------------------------------------

		// SSP 7/30/03 UWG2544
		// Added BeforePerformAction and AfterPerformAction events.
		//
		// ----------------------------------------------------------
		private static readonly object EVENT_AFTERPERFORMACTION = new object( );
		private static readonly object EVENT_BEFOREPERFORMACTION = new object( );
		// ----------------------------------------------------------

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		// SSP 8/1/03
//		// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
//		//
//		// ----------------------------------------------------------
//		private static readonly object EVENT_BEFOREROWFILTERCHANGED = new object( );
//		private static readonly object EVENT_AFTERROWFILTERCHANGED = new object( );
//		// ----------------------------------------------------------

		// SSP 11/10/03 Add Row Feature
		//
		// ----------------------------------------------------------
		private static readonly object EVENT_INITIALIZETEMPLATEADDROW = new object( );
		// ----------------------------------------------------------

		// SSP 12/5/03 UWG2295
		// Added AfterCardsScroll event.
		//
		private static readonly object EVENT_AFTERCARDSSCROLL = new object( );

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		// SSP 4/15/04 - Virtual Binding
//		// Added BeforeRowFilterDropDownPopulate to give the users a chance to prevent the ultragrid
//		// from populating the filter drop down list. This can be useful if there are lot of rows
//		// and it takes a long time for the ultragrid to populate the filter drop down list.
//		// 
//		private static readonly object EVENT_BEFOREROWFILTERDROPDOWNPOPULATE = new object( );

        // JDN 11/17/04 CardCompressedStateChanged events
        // Added BeforeCardCompressedStateChanged and AfterCardCompressedStateChanged events.
        // ----------------------------------------------------------
        private static readonly object EVENT_BEFORECARDCOMPRESSEDSTATECHANGED   = new object( );
        private static readonly object EVENT_AFTERCARDCOMPRESSEDSTATECHANGED    = new object( );
        // ----------------------------------------------------------

        // SSP 12/15/04 - IDataErrorInfo Support
		//
		private static readonly object EVENT_BEFOREDISPLAYDATAERRORTOOLTIP = new object( );

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Added FilterCellValueChanged and InitializeRowsCollection events.
		//
		private static readonly object EVENT_AFTERROWFIXEDSTATECHANGED = new object( );
		private static readonly object EVENT_BEFOREROWFIXEDSTATECHANGED = new object( );

		// SSP 11/22/05 - NAS 6.1 Multi-cell Operations
		// 
		private static readonly object EVENT_BEFOREMULTICELLOPERATION = new object( );

        // MBS 2/28/08 - RowEditTemplate NA2008 V2
        //
        private static readonly object EVENT_AFTERROWEDITTEMPLATECLOSED = new object();
        private static readonly object EVENT_AFTERROWEDITTEMPLATEDISPLAYED = new object();
        private static readonly object EVENT_BEFOREROWEDITTEMPLATECLOSED = new object();
        private static readonly object EVENT_BEFOREROWEDITTEMPLATEDISPLAYED = new object();
        private static readonly object EVENT_ROWEDITTEMPLATEREQUESTED = new object();

        // MBS 7/1/08 - NA2008 V3
        private static readonly object EVENT_CLICKCELL = new object();

        // CDS 02/02/09 TFS12512 - Moved to the UltraGridBase class for UltraCombo support
        //// CDS NAS v9.1 Header CheckBox
        //private static readonly object EVENT_BEFOREHEADERCHECKSTATECHANGED = new object();
        //private static readonly object EVENT_AFTERHEADERCHECKSTATECHANGED = new object();

		#endregion
        
		#region EventsOptimized

		/// <summary>
		/// Occurs after a cell becomes active.
		/// </summary>
		/// <remarks>
		///	<p class="body">This event is generated after a cell is activated, which means it has been given focus.</p>
		///	<p class="body">The <b>ActiveCell</b> property can be used to determine which cell was activated.</p>
		///	<p class="body">The <b>BeforeCellActivate</b> event, which occurs before a cell is activated, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterCellActivate")]	
		public event System.EventHandler			AfterCellActivate
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERCELLACTIVATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCELLACTIVATE, value ); }
		}

		/// <summary>
		/// Occurs after a cell accepts a new value.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>cell</i> argument returns a reference to an UltraGridCell object that can be used to set properties of, and invoke methods on, the cell whose value has been modified. You can use this reference to access any of the returned cell's properties or methods.</p>
		///	<p class="body">This event is generated when a cell's value has been changed. Note that the cell's new value is not necessarily committed to the data source at this time, since various factors such as the type of record locking employed by the data source, as well as the value of the 
		///	<b>UpdateMode</b> property, can affect when the update occurs. The <b>BeforeRowUpdate</b>  event is generated when the new value is to be committed to the data source.</p>
		///	<p class="body">The <b>BeforeCellUpdate</b> event, which is generated before this event, provides an opportunity to prevent the change from occurring.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterCellUpdate")]	
		public event CellEventHandler	AfterCellUpdate
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERCELLUPDATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCELLUPDATE, value ); }
		}

		
		/// <summary>
		/// Occurs when the display layout is initialized, such as when the control is loading data from the data source.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <see cref="InitializeLayoutEventArgs.Layout"/> argument returns a reference to a <see cref="UltraGridBase.DisplayLayout"/> object that can be used to set properties of, and invoke methods on, the layout of the control. You can use this reference to access any of the returned layout's properties or methods.</p>
		///	<p class="body">Like a form's <b>Load</b> event, this event provides an opportunity to configure the control before it is displayed. It is in this event procedure that actions such as creating appearances, valuelists, and unbound columns should take place.</p>
		///	<p class="body">This event is generated when the control is first preparing to display data from the data source. This may occur when the data source changes.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGrid_E_InitializeLayout")]	
		public event InitializeLayoutEventHandler   InitializeLayout
		{
			add { this.EventsOptimized.AddHandler( EVENT_INTIALIZELAYOUT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INTIALIZELAYOUT, value ); }
		}

		/// <summary>
		/// Occurs when a row is initialized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <see cref="InitializeRowEventArgs.Row"/> argument returns a reference to an <see cref="UltraGridRow"/> object that can be used to set properties of, and invoke methods on, the row being displayed. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">The <see cref="InitializeRowEventArgs.ReInitialize"/> argument can be used to determine if the row is being initialized for the first time, such as when the <see cref="UltraDropDown"/> is initially loading data, or whether it is being reinitialized, such as when the <see cref="RowsCollection.Refresh(RefreshRow)"/> method is called.</p>
		///	<p class="body">This event is generated once for each row being displayed or printed and provides an opportunity to perform actions on the row before it is rendered, such as populating an unbound cell or changing a cell's color based on its value.</p>
		///	<p class="body">The <see cref="UltraGridLayout.ViewStyle"/> and <see cref="UltraGridLayout.ViewStyleBand"/> properties of the <see cref="UltraGridBase.DisplayLayout"/> object are read-only in this event procedure.</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeGroupByRow"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeLayout"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_InitializeRow")]	
		public event InitializeRowEventHandler      InitializeRow
		{
			add { this.EventsOptimized.AddHandler( EVENT_INTIALIZEROW, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INTIALIZEROW, value ); }
		}

		// SSP 11/10/03 Add Row Feature
		//
		/// <summary>
		/// Occurs when a template add-row is initialized.
		/// </summary>
		[LocalizedDescription("LDR_UltraGrid_E_InitializeTemplateAddRow")]	
		public event InitializeTemplateAddRowEventHandler InitializeTemplateAddRow
		{
			add { this.EventsOptimized.AddHandler( EVENT_INITIALIZETEMPLATEADDROW, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INITIALIZETEMPLATEADDROW, value ); }
		}

		/// <summary>
		/// Occurs when a GroupBy row is initialized.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a band is in GroupBy mode, the user sees rows of data grouped together under GroupBy rows, which serve as temporary headers for the group of rows. GroupBy rows have their own attributes which can be manipulated through code using the <see cref="Infragistics.Win.UltraWinGrid.UltraGridGroupByRow"/> object. The <b>GroupByRowInitialize</b> event occurs whenever one of these GroupBy rows is created, giving you the chance to work with the GroupBy row before it is displayed.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGrid_E_InitializeGroupByRow")]	
		public event InitializeGroupByRowEventHandler   InitializeGroupByRow
		{
			add { this.EventsOptimized.AddHandler( EVENT_INTIALIZEGROUPBYROW, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INTIALIZEGROUPBYROW, value );} 
		}


		/// <summary>
		/// Occurs after a cell enters edit mode.
		/// </summary>
		///	<remarks>
		///	<p class="body">This event is generated after a cell enters edit mode, meaning that the cell is prepared to accept input from the user. This is different from cell activation, which occurs when the cell receives focus. The <b>BeforeCellActivate</b> event is generated before a cell is activated.</p>
		///	<p class="body">When a cell is in edit mode, the control's <b>IsInEditMode</b> property is set to True. </p>
		///	<p class="body">The <b>BeforeEnterEditMode</b> event, which occurs before a cell enters edit mode, is generated before this event.</p>
		///	<p class="body">The <b>BeforeExitEditMode</b> event is generated before a cell exits edit 
		///	mode.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterEnterEditMode")]	
		public event System.EventHandler			AfterEnterEditMode
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERENTEREDITMODE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERENTEREDITMODE, value );} 
		}

		/// <summary>
		/// Occurs after a cell exits edit mode.
		/// </summary>
		///	<remarks>
		///	<p class="body">When a cell is not in edit mode, the control's <b>IsInEditMode</b> property is set to False.</p>
		///	<p class="body">The <b>BeforeExitEditMode</b> event, which occurs before a cell exits edit mode, is generated before this event.</p>
		///	<p class="body">The <b>BeforeEnterEditMode</b> event is generated before a cell enters edit mode.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterExitEditMode")]	
		public event System.EventHandler		    AfterExitEditMode
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTEREXITEDITMODE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTEREXITEDITMODE, value ); }
		}

		/// <summary>
		/// Occurs after a row becomes active.
		/// </summary>
		///	<remarks>
		///	<p class="body">This event is generated after a row is activated, which means it has been given focus.</p>
		///	<p class="body">The <b>ActiveRow</b> property can be used to determine which row was activated.</p>
		///	<p class="body">Once a row has been activated, its Selected property is set to True.</p>
		///	<p class="body">The <b>BeforeRowActivate</b> event, which occurs before a row is activated, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterRowActivate")]	
		public event System.EventHandler			AfterRowActivate
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWACTIVATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWACTIVATE, value ); }
		}

		/// <summary>
		/// Occurs after one or more rows have been deleted.
		/// </summary>
		///	<remarks>
		///	<p class="body">This event is generated after one or more rows have been deleted, either programmatically, or by user interaction. To prevent the user from deleting rows, set the <b>AllowDelete</b> property to False. Rows can be deleted programmatically by invoking either the <b>Delete</b> method or the <b>DeleteSelectedRows</b> method.</p>
		///	<p class="body">The <b>BeforeRowsDeleted</b> event is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterRowsDeleted")]	
		public event System.EventHandler			AfterRowsDeleted
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWSDELETED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWSDELETED, value );} 
		}

		/// <summary>
		/// Occurs after a column scrolling region is scrolled.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>colscrollregion</i> argument returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the colscrollregion that was scrolled. You can use this reference to access any of the returned colscrollregion's properties or methods.</p>
		///	<p class="body">This event is generated after a colscrollregion is scrolled, either programmatically, or by user interaction. A colscrollregion can be scrolled programmatically by invoking its <b>Scroll</b> method.</p>
		///	<p class="Body">The <b>ScrollBar</b> property of a scrolling region determines whether a scroll bar is displayed for that scrolling region.</p>
		///	<p class="Body">The <b>BeforeColRegionScroll</b> event, which occurs before a column scrolling region is scrolled, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterColRegionScroll")]	
		public event ColScrollRegionEventHandler	AfterColRegionScroll
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERCOLREGIONSCROLL, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCOLREGIONSCROLL, value );} 
		}

		/// <summary>
		/// Occurs after a column scrolling region is resized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>colscrollregion</i> argument returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the colscrollregion that was sized. You can use this reference to access any of the returned colscrollregion's properties or methods.</p>
		///	<p class="body">This event is generated once for every colscrollregion affected by the sizing, meaning that this event is typically generated twice, as each sizing generally affects two adjacent colscrollregions.</p>
		///	<p class="body">This event is generated after a colscrollregion is sized, either programmatically, or by user interaction. A colscrollregion can be sized programmatically by setting its <b>Width</b> property.</p>
		///	<p class="body">The <b>BeforeColRegionSplit</b> event is generated before a colscrollregion is split into two colscrollregions.</p>
		///	<p class="body">The <b>BeforeColRegionSize</b> event, which occurs before a colscrollregion is sized, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterColRegionSize")]	
		public event ColScrollRegionEventHandler	AfterColRegionSize
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERCOLREGIONSIZE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCOLREGIONSIZE, value ); }
		}

		/// <summary>
		/// Occurs after a row scrolling region is scrolled.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>rowscrollregion</i> argument returns a reference to a RowScrollRegion object that can be used to set properties of, and invoke methods on, the rowscrollregion that was scrolled. You can use this reference to access any of the returned rowscrollregion's properties or methods.</p>
		///	<p class="body">This event is generated after a rowscrollregion is scrolled, either programmatically, or by user interaction. A rowscrollregion can be scrolled programmatically by invoking its <b>Scroll</b> method.</p>
		///	<p class="body">The <b>ScrollBar</b> property of a scrolling region determines whether a scroll bar is displayed for that scrolling region.</p>
		///	<p class="body">The <b>BeforeRowRegionScroll</b> event, which occurs before a rowscrollregion is scrolled, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterRowRegionScroll")]	
		public event RowScrollRegionEventHandler	AfterRowRegionScroll
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWREGIONSCROLL, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWREGIONSCROLL, value );} 
		}

		/// <summary>
		/// Occurs after a row scrolling region is resized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The<i>rowscrollregion</i>argument returns a reference to a RowScrollRegion object that can be used to set properties of, and invoke methods on, the rowscrollregion that was sized. You can use this reference to access any of the returned rowscrollregion's properties or methods.</p>
		///	<p class="body">This event is generated once for every rowscrollregion affected by the sizing, meaning that this event is typically generated twice, as each sizing generally affects two adjacent rowscrollregions.</p>
		///	<p class="body">This event is generated after a rowscrollregion is sized, either programmatically, or by user interaction. A rowscrollregion can be sized programmatically by setting its Height property.</p>
		///	<p class="body">The BeforeRowRegionSplit event is generated before a rowscrollregion is split into two rowscrollregions.</p>
		///	<p class="body">The BeforeRowRegionSize event, which occurs before a rowscrollregion is sized, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterRowRegionSize")]	
		public event RowScrollRegionEventHandler	AfterRowRegionSize
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWREGIONSIZE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWREGIONSIZE, value );}
		}

		/// <summary>
		/// Occurs after a row that has children has been collapsed.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>row</i> argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that was collapsed. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">This event is generated after a row has been collapsed, either programmatically, or by user interaction. A row can be collapsed programmatically by setting its <b>Expanded</b> property to False.</p>
		///	<p class="body">The expansion (plus/minus) indicators can be hidden for a row to prevent the user from expanding or collapsing it by setting the <b>ExpansionIndicator</b> property.</p>
		///	<p class="body">The <b>BeforeRowExpanded</b> and <b>AfterRowExpanded</b> events are generated before and after, respectively, a collapsed row has been expanded.</p>
		///	<p class="body">The <b>BeforeRowCollapsed</b> event, which occurs before a row has been collapsed, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterRowCollapsed")]	
		public event RowEventHandler				AfterRowCollapsed
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWCOLLAPSED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWCOLLAPSED, value );} 
		}

		/// <summary>
		/// Occurs after a row with children has been expanded.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>row</i> argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that was expanded. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">This event is generated after a row has been expanded, either programmatically, or by user interaction. A row can be expanded programmatically by setting its <b>Expanded</b> property to True.</p>
		///	<p class="body">The expansion (plus/minus) indicators can be hidden for a row to prevent the user from expanding or collapsing it by setting the <b>ExpansionIndicator</b> property.</p>
		///	<p class="body">The <b>BeforeRowCollapsed</b> and <b>AfterRowCollapsed</b> events are generated before and after, respectively, an expanded row has been collapsed.</p>
		///	<p class="body">The <b>BeforeRowExpanded</b> event, which occurs before a row has been expanded, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterRowExpanded")]	
		public event RowEventHandler				AfterRowExpanded
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWEXPANDED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWEXPANDED, value );} 
		}

		/// <summary>
		/// Occurs after a new row is inserted.
		/// </summary>
		///	<remarks>
		///	<p class="body">The<i>row</i>argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that was inserted. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">This event is generated after a new row has been inserted, either programmatically, or by user interaction. A new row can be inserted programmatically by invoking the<b>AddNew</b>method.</p>
		///	<p class="body">Note that the new row is not necessarily committed to the data source at the time of insert, however, since various factors such as the type of record locking employed by the data source, as well as the value of the<b>UpdateMode</b>property, can affect when the actual update occurs.</p>
		///	<p class="body">The<b>BeforeRowInsert</b>event, which occurs before a row is inserted, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterRowInsert")]	
		public event RowEventHandler				AfterRowInsert
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWINSERT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWINSERT, value );} 
		}

		/// <summary>
		/// Occurs after a row has been resized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The<i>row</i>argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that was resized. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">Depending on the value of the<b>RowSizing</b>property, more than one row can be affected by the resize. In this case,<i>row</i>refers to the original row being resized.</p>
		///	<p class="body">The<b>BeforeRowResize</b>event, which occurs before a row has been resized, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterRowResize")]	
		public event RowEventHandler				AfterRowResize
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWRESIZE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWRESIZE, value ); }
		}

		/// <summary>
		/// Occurs after a row is updated, meaning changes made to its cells are actually committed to the data source.
		/// </summary>
		///	<remarks>
		///	<p class="body">The<i>row</i>argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that was updated. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">This event is generated when a row is updated, meaning changes made to its cells are actually committed to the data source. Note that this is not necessarily when the row loses focus, since various factors such as the type of record locking employed by the data source, as well as the value of the<b>UpdateMode</b>property, can affect when the update occurs. The<b>BeforeCellUpdate</b>event is generated when a cell is accepting a new value.</p>
		///	<p class="body">To prevent the user from making changes to a cell, set the<b>AllowUpdate</b>property to 2 (AllowUpdateNo). A cell's value can be changed programmatically by setting its<b>Value</b>property.</p>
		///	<p class="body">A row can be updated programmatically by invoking its<b>Update</b>method.</p>
		///	<p class="body">The<b>BeforeRowUpdate</b>event, which occurs before a row is updated, is generated before this event.</p>
		///	<p class="body">If an error occurs while attempting to commit the changes to the data source, the<b>Error</b>event is generated.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterRowUpdate")]	
		public event RowEventHandler				AfterRowUpdate
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWUPDATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWUPDATE, value );} 
		}

		/// <summary>
		/// Occurs after the user cancels updates to a row by pressing the ESC key.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>row</i> argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that will be updated. You can use this reference to access any of the returned row's properties or methods. </p>
		///	<p class="body">This event is generated after the user presses the ESC key to cancel changes made to a cells in a row. It is not generated when the <b>CancelUpdate</b> method is invoked. </p>
		///	<p class="body">The <b>BeforeRowCancelUpdate</b> event, which occurs before the row's update is canceled, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterRowCancelUpdate")]	
		public event RowEventHandler				AfterRowCancelUpdate
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWCANCELUPDATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWCANCELUPDATE, value );} 
		}

		/// <summary>
		/// Occurs before a row is activated.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>row</i> argument  returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that will be activated. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">This event is generated before a row is activated, which means it has been given focus.</p>
		///	<p class="body">The <b>BeforeRowDeactivate</b> event is generated before a row is deactivated, meaning it will lose focus.</p>
		///	<p class="body">The <b>AfterRowActivate</b> event, which occurs after a row is activated, is generated after this event.</p>
		///	</remarks>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.BeforeRowDeactivate"/>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowActivate")]	
		public event RowEventHandler				BeforeRowActivate
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWACTIVATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWACTIVATE, value ); }
		}

		/// <summary>
		/// Occurs before a row is collapsed.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>row</i> argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that will collapse. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the row from collapsing. This argument can be used to prevent the user from collapsing a row unless a certain condition is met.</p>
		///	<p class="body">This event is generated before a row has been collapsed, ither programmatically, or by user interaction. A row can be collapsed programmatically by setting its <b>Expanded</b> property to False.</p>
		///	<p class="body">The expansion (plus/minus) indicators can be hidden for a row to prevent the user from expanding or collapsing it by setting the <b>ExpansionIndicator</b> property.</p>
		///	<p class="body">The <b>BeforeRowExpanded</b> and <b>AfterRowExpanded</b> events are generated before and after, respectively, a collapsed row has been expanded.</p>
		///	<p class="body">The <b>AfterRowCollapsed</b> event, which occurs after a row has been collapsed, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowCollapsed")]	
		public event CancelableRowEventHandler		BeforeRowCollapsed
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWCOLLAPSED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWCOLLAPSED, value );} 
		}

		/// <summary>
		/// Occurs before a row with children is expanded.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>row</i> argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that will expand. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">The <i>cancel</i>  argument enables you to programmatically prevent the row from expanding. This argument can be used to prevent the user from expanding a row unless a certain condition is met.</p>
		///	<p class="body">This event is generated before a row has been expanded, either programmatically, or by user interaction. A row can be expanded programmatically by setting its <b>Expanded</b> property to True.</p>
		///	<p class="body">The expansion (plus/minus) indicators can be hidden for a row to preventthe user from expanding or collapsing it by setting the <b>ExpansionIndicator</b> property.</p>
		///	<p class="body">The <b>BeforeRowCollapsed</b> and <b>AfterRowCollapsed</b>  events are generated before and after, respectively, an expanded row has been collapsed.</p>
		///	<p class="body">The <b>AfterRowExpanded</b> event, which occurs after a row has been expanded, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowExpanded")]	
		public event CancelableRowEventHandler		BeforeRowExpanded
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWEXPANDED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWEXPANDED, value );} 
		}

		/// <summary>
		/// Occurs before a row is updated, meaning changes made to its cells are actually committed to the data source.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>row</i> argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that will be updated. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the row from being updated and from committing changes to the data source. This argument can be used to prevent the row from being updated unless a certain condition is met.</p>
		///	<p class="body">This event is generated when a row is updated, meaning changes made to its cells are actually committed to the data source. Note that this is not necessarily when the row loses focus, since various factors such as the type of record locking employed by the data source, as well as the value of the <b>UpdateMode</b> property, can affect when the update occurs. The <b>BeforeCellUpdate</b> event is generated when a cell is accepting a new value.</p>
		///	<p class="body">To prevent the user from making changes to a cell, set the <b>AllowUpdate</b> property to 2 (AllowUpdateNo). A cell's value can be changed programmatically by setting its <b>Value</b> property.</p>
		///	<p class="body">A row can be updated programmatically by invoking its <b>Update</b> method.</p>
		///	<p class="body">The <b>AfterRowUpdate</b> event, which occurs after a row is updated, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowUpdate")]	
		public event CancelableRowEventHandler		BeforeRowUpdate
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWUPDATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWUPDATE, value ); }
		}

		/// <summary>
		/// Occurs before the user cancels updates to a row by pressing the ESC key.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>row</i> argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row whose update will be canceled. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the row's update from being canceled. This argument can be used to prevent the user from canceling an update unless a certain condition is met.</p>
		///	<p class="body">This event is generated when the user presses the ESC key to cancel changes made to cells in a row. It is also generated when the <b>CancelUpdate</b> method is invoked.</p>
		///	<p class="body">The <b>AfterRowCancelUpdate</b> event, which occurs after a row's update has been canceled, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowCancelUpdate")]	
		public event CancelableRowEventHandler		BeforeRowCancelUpdate
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWCANCELUPDATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWCANCELUPDATE, value );} 
		}
				
		/// <summary>
		/// Occurs after the user cancels changes to a cell's value by pressing the ESC key.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>cell</i> argument returns a reference to an UltraGridCell object that can be used to set properties of, and invoke methods on, the cell whose update was canceled. You can use this reference to access any of the returned cell's properties or methods.</p>
		///	<p class="body">This event is generated after the user presses the ESC key to cancel changes made to a cell's value. It is not generated when the <b>CancelUpdate</b> method is invoked. </p>
		///	<p class="body">The <b>BeforeCellCancelUpdate</b> event, which occurs before a cell's update is canceled, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterCellCancelUpdate")]	
		public event CellEventHandler				AfterCellCancelUpdate
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERCELLCANCELUPDATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCELLCANCELUPDATE, value ); }
		}
		
		/// <summary>
		/// Occurs when a cell in edit mode has its value modified by the user.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>cell</i> argument returns a reference to an UltraGridCell object that can be used to set properties of, and invoke methods on, the cell whose value is being modified. You can use this reference to access any of the returned cell's properties or methods.</p>
		///	<p class="body">This event is generated when the user is modifying the value of a cell in edit mode. Note that this does not necessarily mean that the changes will be committed to the data source, only that the user is editing the value of the cell.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_CellChange")]	
		public event CellEventHandler				CellChange
		{
			add { this.EventsOptimized.AddHandler( EVENT_CELLCHANGE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_CELLCHANGE, value ); }
		}

		/// <summary>
		/// Occurs when the user selects an item from a cell's dropdown list.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>cell</i> argument returns a reference to an UltraGridCell object that can be used to set properties of, and invoke methods on, the cell that had an item selected. You can use this reference to access any of the returned cell's properties or methods.</p>
		///	<p class="body">This event is generated when an item is selected from the cell's dropdown list. A dropdown list item is considered selected when the user clicks it or highlights it when navigating the list using navigation keys.</p>
		///	<p class="body">This event is only generated for a cell whose column's <b>Style</b> property is set to 4 (StyleDropDown), 5 (StyleDropDownList), 6 (StyleDropDownValidate), or 8 (StyleDropDownCalendar).</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_CellListSelect")]	
		public event CellEventHandler				CellListSelect
		{
			add { this.EventsOptimized.AddHandler( EVENT_CELLLISTSELECT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_CELLLISTSELECT, value );} 
		}

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		/// <summary>
//		/// Occurs after a sort action is completed.
//		/// </summary>
//		///	<remarks>
//		///	<p class="body">The <i>band</i>argument returns a reference to an UltraGridBand object that can be used to set properties of, and invoke methods on, the band that was sorted. You can use this reference to access any of the returned band's properties or methods.</p>
//		///	<p class="body">The UltraWinGrid can automatically sort the contents of columns without the addition of any code, provided the control is able to preload the rows in the band. Preloading is enabled by default if the recordset bound to the band contains less than 1000 rows. If you do not want to preload rows, but you still want to provide column sorting in the control, you must implement column sorting yourself using the <b>BeforeSortChange</b> and <b>AfterSortChange</b> events.</p>
//		///	<p class="body">The <b>BeforeSortChange</b> event, which occurs before a sort action is completed, is generated before this event.</p>
//		///	</remarks>
//		[LocalizedDescription("LD_UltraGrid_E_AfterSortChange")]	
//		public event BandEventHandler				AfterSortChange
//		{
//			add { this.EventsOptimized.AddHandler( EVENT_AFTERSORTCHANGE, value ); }
//			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERSORTCHANGE, value );} 
//		}

		/// <summary>
		/// Occurs when a column is using the Button or EditButton style and the user clicks a cell's button.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>cell</i> argument returns a reference to an UltraGridCell object that can be used to set properties of, and invoke methods on, the cell whose button was clicked. You can use this reference to access any of the returned cell's properties or methods.</p>
		///	<p class="body">This event is generated when the user clicks a cell's button. A cell may be represented by a button or contain a button, based on its style.</p>
		///	<p class="body">This event is only generated for a cell whose column's <b>Style</b> property is set to 2 (StyleEditButton) or 7 (StyleButton).</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_ClickCellButton")]	
		public event CellEventHandler				ClickCellButton
		{
			add { this.EventsOptimized.AddHandler( EVENT_CLICKCELLBUTTON, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_CLICKCELLBUTTON, value );} 
		}

		/// <summary>
		/// Occurs after a cell's dropdown list has been closed in a column using one of the dropdown list styles.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>cell</i> argument returns a reference to an UltraGridCell object that can be used to set properties of, and invoke methods on, the cell that had its dropdown list closed. You can use this reference to access any of the returned cell's properties or methods.</p>
		///	<p class="body">This event is generated when a cell's dropdown list has been closed, either programmatically, or by user interaction. A cell's dropdown list can be closed programmatically by setting the cell's <b>DroppedDown</b> property to False.</p>
		///	<p class="body">This event is only generated for a cell whose column's <b>Style</b> property is set to 4 (StyleDropDown), 5 (StyleDropDownList), 6 (StyleDropDownValidate), or 8 (StyleDropDownCalendar).</p>
		///	<p class="body">Set the column's <b>ValueList</b> property in order to populate the dropdown list.</p>
		///	<p class="body">The <b>BeforeCellListDropDown</b> event is generated when a cell's dropdown list is dropped down.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterCellListCloseUp")]	
		public event CellEventHandler				AfterCellListCloseUp
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERCELLLISTCLOSEUP, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCELLLISTCLOSEUP, value );} 
		}
		
					
		/// <summary>
		/// Occurs after one or more row, cell, or column objects were selected or deselected.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>selectchange</i> argument indicates what type of object or objects involved in the selection: rows, cells, or columns. When a row or column is selected, the cells contained in it are not considered selected.</p>
		///	<p class="body">This event is generated before one or more objects have been selected or deselected, either programmatically, or by user interaction.</p>
		///	<p class="body">The control's <b>Selected</b> property can be used to determine what object or objects are currently selected.</p>
		///	<p class="body">The <b>BeforeSelectChange</b> event, which occurs before one or more row, cell, or column objects have been selected or deselected, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterSelectChange")]	
		public event AfterSelectChangeEventHandler	AfterSelectChange
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERSELECTCHANGE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERSELECTCHANGE, value ); }
		}

		/// <summary>
		/// Occurs before one or more row, cell, or column objects are selected or deselected.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>selectchange</i> argument indicates what type of object or objects were involved in the selection: rows, cells, or columns. When a row or column is selected, the cells contained in it are not considered selected.</p>
		///	<p class="body">The <i>newselections</i> argument returns a reference to a Selected collection that can be used to retrieve references to the rows, cells, or columns that will be selected. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the object or objects from being selected. This argument can be used to prevent the user from changing the selection unless a certain condition is met.</p>
		///	<p class="body">This event is generated before one or more objects have been selected or deselected, either programmatically, or by user interaction.</p>
		///	<p class="body">The control's <b>Selected</b> property can be used to determine what object or objects were previously selected.</p>
		///	<p class="body">The <b>AfterSelectChange</b> event, which occurs after one or more row, cell, or column objects have been selected or deselected, is generated after this event.</p>
        ///	<p class="note"><b>Note: </b>This event will fire when one or more rows are deleted through the grid, but this cannot be cancelled.  If the action needs to be cancelled, the <see cref="BeforeRowsDeleted"/> event should be used instead.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeSelectChange")]	
		public event BeforeSelectChangeEventHandler	BeforeSelectChange
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORESELECTCHANGE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORESELECTCHANGE, value ); }
		}

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		/// <summary>
//		/// Occurs after a column has been moved, sized or swapped.
//		/// </summary>
//		///	<remarks>
//		///	<p class="body">The <i>action</i> argument indicates which action occurred to the column or columns: moving, swapping, or sizing.</p>
//		///	<p class="body">The <i>columns</i> argument returns a reference to a SelectedCols collection that can be used to retrieve references to the UltraGridColumn object or objects that were moved, swapped, or sized. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
//		///	<p class="body">This event is generated after one or more columns are moved, swapped, or sized, either programmatically, or by user interaction. A column can be sized programmatically by setting its <b>Width</b> property and can be moved programmatically by setting its header's <b>VisiblePosition</b> property.</p>
//		///	<p class="body">The <b>VisiblePosition</b> property of a column's header can be used to determine the new position of a column that was moved or swapped.</p>
//		///	<p class="body">To prevent the user from attempting to move, swap, or size a column, set the <b>AllowColMoving</b>, <b>AllowColSwapping</b>, or <b>AllowColSizing</b> properties, respectively.</p>
//		///	<p class="body">The <b>AfterGroupPosChanged</b> event is generated after one or more groups are moved, swapped, or sized.</p>
//		///	<p class="body">The <b>BeforeColPosChanged</b> event, which occurs before one or more columns are moved, swapped, or sized, is generated before this event.</p>
//		///	</remarks>
//		[LocalizedDescription("LD_UltraGrid_E_AfterColPosChanged")]	
//		public event AfterColPosChangedEventHandler AfterColPosChanged
//		{
//			add { this.EventsOptimized.AddHandler( EVENT_AFTERCOLPOSCHANGED, value ); }
//			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCOLPOSCHANGED, value );} 
//		}

		/// <summary>
		/// Occurs after an UltraGridGroup object has been moved, sized, or swapped.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>action</i> argument indicates which action occurred to the group or groups: moving, swapping, or sizing.</p>
		///	<p class="body">The <i>groups</i> argument returns a reference to a Groups collection that can be used to retrieve references to the UltraGridGroup object or objects that were moved, swapped, or sized. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
		///	<p class="body">This event is generated after one or more groups are moved, swapped, or sized, either programmatically, or by user interaction. A group can be sized programmatically by setting its <b>Width</b> property and can be moved programmatically by setting its header's <b>VisiblePosition</b> property.</p>
		///	<p class="body">The <b>VisiblePosition</b> property of a group's header can be used to determine the new position of a group that was moved or swapped.</p>
		///	<p class="body">To prevent the user from attempting to move or swap a group, set the <b>AllowGroupMoving</b> or <b>AllowGroupSwapping </b>properties, respectively.</p>
		///	<p class="body">The <b>AfterColPosChanged</b> event is generated after one or more columns are moved, swapped, or sized.</p>
		///	<p class="body">The <b>BeforeGroupPosChanged</b> event, which occurs before one or more groups are moved, swapped, or sized, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterGroupPosChanged")]	
		public event AfterGroupPosChangedEventHandler	AfterGroupPosChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERGROUPPOSCHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERGROUPPOSCHANGED, value );} 
		}

		/// <summary>
		/// Occurs before the multiple-line edit window is expanded.
		/// </summary>
		///	<remarks>
		///	<p class="body">This event can be used to adjust the size of the multiple-line edit window or prevent it from being displayed at all.</p>
		///	<p class="body">This event is passed a <see cref="CancelableAutoSizeEditEventArgs"/> object which contains information about the multi-line edit window that is about to be displayed. You can set the properties of this object to determine the appearance and behavior of the edit window. You can specify both the edit window's initial size (using the <b>StartHeight</b> and <b>StartWidth</b> properties) and its maximum size (using the <b>MaxHeight</b> and <b>MaxWidth</b> properties).</p>
		///	<p class="body">You can also cancel the display of the edit window. The <b>cancel</b> property of the object enables you to programmatically prevent the multiple-line edit window from being displayed. You can use this ability to make the display of the multi-line edit window conditional.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeAutoSizeEdit")]	
		public event CancelableAutoSizeEditEventHandler	BeforeAutoSizeEdit
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREAUTOSIZEEDIT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREAUTOSIZEEDIT, value );} 
		}

		/// <summary>
		/// Occurs before a cell becomes active.
		/// </summary>
		///	<remarks>
		///	<p class="body">The<i>cell</i>argument returns a reference to an UltraGridCell object that can be used to set properties of, and invoke methods on, the cell that will be activated. You can use this reference to access any of the returned cell's properties or methods.</p>
		///	<p class="body">The<i>cancel</i>argument enables you to programmatically prevent the cell from being activated. This argument can be used to prevent the cell from activating unless a certain condition is met.</p>
		///	<p class="body">This event is generated before a cell is activated, which means it has been given focus.</p>
		///	<p class="body">The<b>BeforeCellDeactivate</b>event is generated before a cell is deactivated, meaning it will lose focus.</p>
		///	<p class="body">The<b>AfterCellActivate</b>event, which occurs after a cell is activated, is generated after this event, provided<i>cancel</i>is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeCellActivate")]	
		public event CancelableCellEventHandler		BeforeCellActivate
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECELLACTIVATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECELLACTIVATE, value );} 
		}

		/// <summary>
		/// Occurs before a cell's dropdown list is dropped down in a column using one of the dropdown list styles.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>cell</i> argument returns a reference to an UltraGridCell object that can be used to set properties of, and invoke methods on, the cell that will have its dropdown list dropped down. You can use this reference to access any of the returned cell's properties or methods.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the cell's dropdown list from being dropped down. This argument can be used to prevent the dropdown list from dropping down unless a certain condition is met.</p>
		///	<p class="body">This event is generated when a cell's dropdown list is about to be dropped down, either programmatically, or by user interaction. A cell's dropdown list can be dropped down programmatically by setting the cell's <b>DroppedDown</b> property to True.</p>
		///	<p class="body">This event is only generated for a cell whose column's <b>Style</b> property is set to 4 (StyleDropDown), 5 (StyleDropDownList), 6 (StyleDropDownValidate), or 8 (StyleDropDownCalendar).</p>
		///	<p class="body">Set the column's <b>ValueList</b> property to a ValueList object to populate the dropdown list.</p>
		///	<p class="body">The <b>AfterCellListCloseUp</b> event is generated when a cell's dropdown list is closed.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeCellListDropDown")]	
		public event CancelableCellEventHandler		BeforeCellListDropDown
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECELLLISTDROPDOWN, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECELLLISTDROPDOWN, value );} 
		}
		
		/// <summary>
		/// Occurs before the user cancels changes to a cell's value by pressing the ESC key.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>cell</i> argument returns a reference to an UltraGridCell object that can be used to set properties of, and invoke methods on, the cell whose update is about to be canceled. You can use this reference to access any of the returned cell's properties or methods.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the cell's update from being canceled. This argument can be used to prevent the user from canceling an update unless a certain condition is met.</p>
		///	<p class="body">This event is generated when the user presses the ESC key to cancel changes made to a cell's value. It is not generated when the <b>CancelUpdate</b> method is invoked.</p>
		///	<p class="body">The <b>AfterCellCancelUpdate</b> event, which occurs after a cell's update has been canceled, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeCellCancelUpdate")]	
		public event CancelableCellEventHandler		BeforeCellCancelUpdate
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECELLCANCELUPDATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECELLCANCELUPDATE, value );} 
		}

		/// <summary>
		/// Occurs before a cell is deactivated.
		/// </summary>
		///	<remarks>
		///	<p class="body">The<i>cancel</i>argument enables you to programmatically prevent the the cell from deactivating, meaning it will not lose focus. This argument can be used to prevent the user from leaving the cell unless a certain condition is met.</p>
		///	<p class="body">This event is generated when the user attempts to move to a different cell, deactivating the original cell.</p>
		///	<p class="body">The<b>BeforeCellActivate</b>event is generated before a cell  is activated, which means it will get focus.</p>
		///	<p class="body">The<b>ActiveCell</b>property can be used to determine which cell is currently active.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeCellDeactivate")]	
		public event CancelEventHandler			BeforeCellDeactivate
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECELLDEACTIVATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECELLDEACTIVATE, value ); }
		}

		/// <summary>
		/// Occurs before a cell enters edit mode.
		/// </summary>
		///	<remarks>
		///	<p class="body">The<i>cancel</i>argument enables you to programmatically prevent the cell from entering edit mode, meaning that the cell is prepared to accept input from the user. This argument can be used to prevent the cell from entering edit mode unless a certain condition is met.</p>
		///	<p class="body">This event is generated before a cell enters edit mode. This is different from cell activation, which occurs when the cell receives focus. The<b>BeforeCellActivate</b>event is generated before a cell is activated.</p>
		///	<p class="body">When a cell is in edit mode, the control's<b>IsInEditMode</b>property is set to True.</p>
		///	<p class="body">The<b>AfterEnterEditMode</b>event, which occurs after a cell enters edit mode, is generated after this event, provided<i>cancel</i>is not set to True.</p>
		///	<p class="body">The<b>BeforeExitEditMode</b>event is generated before a cell exits edit mode.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeEnterEditMode")]	
		public event CancelEventHandler			BeforeEnterEditMode
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREENTEREDITMODE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREENTEREDITMODE, value );} 
		}


		// SSP 5/2/02
		// Changed the event args of BeforeExitEditMode from CancelEventArgs to
		// BeforeExitEditModeEventArgs
		//
		/// <summary>
		/// Occurs before a cell exits edit mode.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the cell from exiting edit mode. This argument can be used to prevent the cell from leaving edit mode unless a certain condition is met.</p>
		///	<p class="body">When a cell is not in edit mode, the control's <b>IsInEditMode</b> property is set to False.</p>
		///	<p class="body">The <b>AfterExitEditMode</b> event, which occurs after a cell exits edit mode, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	<p class="body">The <b>BeforeEnterEditMode</b> event is generated before a cell enters edit mode.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeExitEditMode")]	
		public event BeforeExitEditModeEventHandler			BeforeExitEditMode
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREEXITEDITMODE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREEXITEDITMODE, value );} 		
		}		
		

		/// <summary>
		/// Occurs before a row is deactivated.
		/// </summary>
		///	<remarks>
		///	<p class="body">The<i>cancel</i>argument enables you to programmatically prevent the the row from deactivating, meaning it does not lose focus. This argument can be used to prevent the user from leaving the row unless a certain condition is met.</p>
		///	<p class="body">This event is generated when the user attempts to move to a different row, deactivating the original row.</p>
		///	<p class="body">The<b>BeforeRowActivate</b>event is generated before a row is activated, which means it will get focus.</p>
		///	<p class="body">The<b>ActiveRow</b>property can be used to determine which row is currently active.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.BeforeRowActivate"/>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowDeactivate")]	
		public event CancelEventHandler			BeforeRowDeactivate
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWDEACTIVATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWDEACTIVATE, value );} 
		}

		/// <summary>
		/// Occurs when the user holds the primary mouse button down over a selected object for a short duration.
		/// </summary>
		///	<remarks>
		///	<p class="body">Since creating a new selection (of rows, columns, cells, etc.) and initiating a drag and drop operation can both be triggered by the same action (the user holding down the primarily mouse button and moving the mouse pointer), this event serves to differentiate between the two.</p>
		///	<p class="body">This event is generated when the user holds the primary mouse button down over a selected object for a short duration before actually moving the mouse pointer. If the mouse pointer is not moved before the duration expires, this event is generated; otherwise, a new selection is created and this event is not generated.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically restore the selection process, allowing the user to continue the selection action.</p>
		///	<p class="body">The programmer should use this event to implement drag and drop operations.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_SelectionDrag")]	
		public event CancelEventHandler			SelectionDrag
		{
			add { this.EventsOptimized.AddHandler( EVENT_SELECTIONDRAG, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_SELECTIONDRAG, value );} 
		}

		/// <summary>
		/// Occurs before one or more groups have been moved, swapped, or sized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>action</i> argument indicates which action will occur to the group or groups: moving, swapping, or sizing.</p>
		///	<p class="body">The <i>groups</i> argument returns a reference to a Groups collection that can be used to retrieve references to the UltraGridGroup object or objects that will be moved, swapped, or sized. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection. However, all properties of the affected groups are read-only in this event procedure.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the group or groups from being moved, swapped, or sized. This argument can be used to prevent the user from moving, swapping, or sizing groups unless a certain condition is met. To prevent the user from attempting to move or swap a group, set the <b>AllowGroupMoving</b> or <b>AllowGroupSwapping</b> properties, respectively.</p>
		///	<p class="body">This event is generated before one or more groups are moved, swapped, or sized, either programmatically, or by user interaction. A group can be sized programmatically by setting its <b>Width</b> property and can be moved programmatically by setting its header's <b>VisiblePosition</b> property.</p>
		///	<p class="body">The <b>VisiblePosition</b> property can be used to determine both the current and new positions of the group or groups that will be moved or swapped. New positions can be determined by reading the property off of the header of the group or groups in <i>groups</i>, while current positions can be determined by reading the property off of the header of the group or group in the appropriate band.</p>
		///	<p class="body">The <b>BeforeColPosChanged</b> event is generated before one or more columns are moved, swapped, or sized.</p>
		///	<p class="body">The <b>AfterGroupPosChanged</b> event, which occurs after one or more groups are moved, swapped, or sized, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeGroupPosChanged")]	
		public event BeforeGroupPosChangedEventHandler	BeforeGroupPosChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREGROUPPOSCHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREGROUPPOSCHANGED, value );} 
		}

		/// <summary>
		/// Occurs before a column scrolling region is removed.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>colscrollregion</i> argument returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the colscrollregion that was removed. You can use this reference to access any of the returned colscrollregion's properties or methods.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the colscrollregion from being removed. This argument can be used to prevent the user from removing the colscrollregion unless a certain condition is met.</p>
		///	<p class="body">This event is generated before a colscrollregion is removed, either programmatically, or by user interaction. A colscrollregion can be removed programmatically by invoking the <b>Remove</b> method of the ColScrollRegions collection.</p>
		///	<p class="body">The <b>BeforeColRegionSplit</b> event is generated before a colscrollregion is split in two.</p>
		///	<p class="body">The <b>BeforeRowRegionSplit</b> event is generated before a rowscrollregion is split in two.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeColRegionRemoved")]	
		public event BeforeColRegionRemovedEventHandler BeforeColRegionRemoved
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECOLREGIONREMOVED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECOLREGIONREMOVED, value );} 
		}

		/// <summary>
		/// Occurs before a column scrolling region is split into two column scrolling regions.
		/// </summary>
		///	<remarks>
		///	<p class="body">The<i>originalcolscrollregion</i>argument returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the colscrollregion as it exists before the split. You can use this reference to access any of the returned colscrollregion's properties or methods. However, the <b>Position</b> and <b>Width</b> properties of this colscrollregion are read-only in this event procedure.</p>
		///	<p class="body">The <i>newcolscrollregion</i> argument returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the colscrollregion as it will exist after the split. You can use this reference to access any of the returned colscrollregion's properties or methods.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the colscrollregion from being split. This argument can be used to prevent the user from splitting the colscrollregion unless a certain condition is met.</p>
		///	<p class="body">This event is generated before a colscrollregion is split, either programmatically, or by user interaction. A colscrollregion can be split programmatically by invoking its <b>Split</b> method.</p>
		///	<p class="body">The <b>BeforeColRegionRemoved</b> event is generated before a colscrollregion is removed.</p>
		///	<p class="body">The <b>BeforeColRegionSize</b> event is generated before a colscrollregion is sized.</p>
		///	<p class="body">The <b>BeforeRowRegionSplit</b> event is generated before a rowscrollregion is split.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeColRegionSplit")]	
		public event BeforeColRegionSplitEventHandler	BeforeColRegionSplit
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECOLREGIONSPLIT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECOLREGIONSPLIT, value );} 
		}
		
		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		/// <summary>
//		/// Occurs before one or more columns have been moved, swapped, or sized.
//		/// </summary>
//		///	<remarks>
//		///	<p class="body">The <i>action</i> argument indicates which action  will occur to the column or columns: moving, swapping, or sizing.</p>
//		///	<p class="body">The <i>columns</i> argument returns a reference to a SelectedCols collection that can be used to retrieve references to the UltraGridColumn object or objects that will be moved, swapped, or sized. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection. However, all properties of the affected columns are read-only in this event procedure.</p>
//		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the column or columns from being moved, swapped, or sized. This argument can be used to prevent the user from moving, swapping, or sizing columns unless a certain condition is met. To prevent the user from attempting to move, swap, or size a column, set the <b>AllowColMoving</b>, <b>AllowColSwapping</b>, <b>AllowColSizing</b> properties, respectively.</p>
//		///	<p class="body">This event is generated before one or more columns are moved, swapped, or sized, either programmatically, or by user interaction. A column can be sized programmatically by setting its <b>Width</b> property and can be moved programmatically by setting its header's <b>VisiblePosition</b> property.</p>
//		///	<p class="body">The <b>VisiblePosition</b> property can be used to determine both the current and new positions of the column or columns that will be moved or swapped. New positions can be determined by reading the property off of the header of the column or columns in <i>columns</i>, while current positions can be determined by reading the property off of the header of the column or columns in the appropriate band.</p>
//		///	<p class="body">The <b>BeforeGroupPosChanged</b> event is generated before one or more groups are moved, swapped, or sized.</p>
//		///	<p class="body">The <b>AfterColPosChanged</b> event, which occurs after one or more columns are moved, swapped, or sized, is generated after this event, provided <i>cancel</i> is not set to True.</p>
//		///	</remarks>
//		[LocalizedDescription("LD_UltraGrid_E_BeforeColPosChanged")]	
//		public event BeforeColPosChangedEventHandler	BeforeColPosChanged
//		{
//			add { this.EventsOptimized.AddHandler( EVENT_BEFORECOLPOSCHANGED, value ); }
//			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECOLPOSCHANGED, value ); }
//		}

		/// <summary>
		/// Occurs before a column scrolling region is scrolled.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>newstate</i> argument returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the colscrollregion as it exists before the scroll. You can use this reference to access any of the returned colscrollregion's properties or methods.</p>
		///	<p class="body">The <i>oldstate</i> argument returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the colscrollregion as it will exist after the scroll. You can use this reference to access any of the returned colscrollregion's properties or methods. However, the <b>Position</b> and <b>Width</b> properties of this colscrollregion are read-only in this event procedure.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the colscrollregion from scrolling. This argument can be used to prevent the user from scrolling unless a certain condition is met.</p>
		///	<p class="body">This event is generated before a colscrollregion is scrolled, either programmatically, or by user interaction. A colscrollregion can be scrolled programmatically by invoking its <b>Scroll</b> method.</p>
		///	<p class="body">The <b>ScrollBar</b> property of a scrolling region determines whether a scroll bar is displayed for that scrolling region.</p>
		///	<p class="body">The <b>AfterColRegionScroll</b> event, which occurs after a colscrollregion was scrolled, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	<p class="body">The <b>BeforeRowRegionScroll</b> event is generated before a rowscrollregion is scrolled.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeColRegionScroll")]	
		public event BeforeColRegionScrollEventHandler	BeforeColRegionScroll
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECOLREGIONSCROLL, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECOLREGIONSCROLL, value );} 
		}

		/// <summary>
		/// Occurs before two adjacent column scrolling regions are sized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>region1</i> argument returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the leftmost colscrollregion that will be sized. You can use this reference to access any of the returned colscrollregion's properties or methods. However, the <b>Width</b> property of this rowscrollregion is read-only in this event procedure.</p>
		///	<p class="body">The <i>region2</i> argument returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the rightmost colscrollregion that will be sized. You can use this reference to access any of the returned colscrollregion's properties or methods. However, the <b>Width</b> property of this rowscrollregion is read-only in this event procedure.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the colscrollregions from sizing. This argument can be used to prevent the user from resizing the colscrollregions unless a certain condition is met. To prevent users from actually moving the colscrollregion's splitter bar, set its <b>SizingMode</b> property to 0 (SizingModeFixed).</p>
		///	<p class="body">This event is generated before a colscrollregion is sized, either programmatically, or by user interaction. A colscrollregion can be sized programmatically by setting its <b>Width</b> property. Because colscrollregions are vertical scrolling regions, the height of all colscrollregions will always be identical. Attempting to set the <b>Width</b> property of a rowscrollregion being sized in this event procedure, however, will generate an error.</p>
		///	<p class="body">The <b>BeforeColRegionSplit</b> event is generated before a colscrollregion is split into two colscrollregions.</p>
		///	<p class="body">The <b>AfterColRegionSize</b> event, which occurs after a colscrollregion was sized, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeColRegionSize")]	
		public event BeforeColRegionSizeEventHandler	BeforeColRegionSize
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECOLREGIONSIZE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECOLREGIONSIZE, value );} 
		}

		/// <summary>
		/// Occurs before a row scrolling region is removed.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>rowscrollregion</i> argument returns a reference to a RowScrollRegion object that can be used to set properties of, and invoke methods on, the rowscrollregion that was removed. You can use this reference to access any of the returned rowscrollregion's properties or methods.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the colscrollregion from being removed. This argument can be used to prevent the user from removing the rowscrollregion unless a certain condition is met.</p>
		///	<p class="body">This event is generated before a rowscrollregion is removed, either programmatically, or by user interaction. A rowscrollregion can be removed programmatically by invoking the <b>Remove</b> method of the RowScrollRegions collection.</p>
		///	<p class="body">The <b>BeforeRowRegionSplit</b> event is generated before a rowscrollregion is split in two.</p>
		///	<p class="body">The <b>BeforeColRegionSplit</b> event is generated before a colscrollregion is split in two.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowRegionRemoved")]	
		public event BeforeRowRegionRemovedEventHandler	BeforeRowRegionRemoved
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWREGIONREMOVED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWREGIONREMOVED, value );} 
		}
		
		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		/// <summary>
//		/// Occurs before the sort indicator is changed.
//		/// </summary>
//		///	<remarks>
//		///	<p class="body">The <i>band</i> argument returns a reference to an UltraGridBand object that can be used to set properties of, and invoke methods on, the band that will be sorted. You can use this reference to access any of the returned band's properties or methods.</p>
//		///	<p class="body">The UltraGrid can automatically sort the contents of columns without the addition of any code, provided the control is able to preload the rows in the band. Preloading is enabled by default if the recordset bound to the band contains less than 1000 rows. If you do not want to preload rows, but you still want to provide column sorting in the control, you must implement column sorting yourself using the <b>BeforeSortChange</b> and <b>AfterSortChange</b> events.</p>
//		///	<p class="body">The <i>newsortedcols</i> argument returns a reference to a SortedCols collection that can be used to retrieve references to the UltraGridColumn object or objects being sorted. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
//		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the columns from being sorted. This argument can be used to prevent the user from sorting columns unless a certain condition is met.</p>
//		///	<p class="body">The <b>AfterSortChange</b> event, which occurs after a sort action is completed, is generated after this event, provided <i>cancel</i> is not set to True.</p>
//		///	</remarks>
//		[LocalizedDescription("LD_UltraGrid_E_BeforeSortChange")]	
//		public event BeforeSortChangeEventHandler		BeforeSortChange
//		{
//			add { this.EventsOptimized.AddHandler( EVENT_BEFORESORTCHANGE, value ); }
//			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORESORTCHANGE, value ); }
//		}

		/// <summary>
		/// Occurs before a new row is inserted.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>band</i> argument returns a reference to an UltraGridBand object that can be used to set properties of, and invoke methods on, the band into which the new row will be inserted. You can use this reference to access any of the returned band's properties or methods.</p>
		///	<p class="body">The <i>parentrow</i> argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that will be the parent of the row to be inserted. You can use this reference to access any of the returned row's properties or methods. If the row being inserted is not a child, <i>parentrow</i> will be set to Nothing.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the row from being inserted. This argument can be used to prevent the user from inserting a new row unless a certain condition is met.</p>
		///	<p class="body">This event is generated after a new row has been inserted, either programmatically, or by user interaction. A new row can be inserted programmatically by invoking the <b>AddNew</b> method.</p>
		///	<p class="body">The <b>AfterRowInsert</b> event, which occurs after a row is inserted, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowInsert")]	
		public event BeforeRowInsertEventHandler		BeforeRowInsert
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWINSERT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWINSERT, value );} 
		}

		/// <summary>
		/// Occurs before a cell accepts a new value.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>cell</i> argument returns a reference to an UltraGridCell object that can be used to set properties of, and invoke methods on, the cell whose value will be modified. You can use this reference to access any of the returned cell's properties or methods. However, the <b>Value</b>   property of this cell is read-only.</p>
		///	<p class="body">The <i>newvalue</i> argument indicates what the new value of the cell will be. The <b>Value</b> property of the UltraGridCell object returned by <i>cell</i> can be used to determine the existing value of the cell.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the cell from accepting the new value. This argument can be used to prevent the cell from accepting the new value unless a certain condition is met.</p>
		///	<p class="body">This event is generated when a cell's value has been changed, either programmatically, or by user interaction. Note that the cell's new value is not necessarily committed to the data source at this time, since various factors such as the type of record locking employed by the data source, as well as the value of the <b>UpdateMode</b> property, can affect when the update occurs. The <b>BeforeRowUpdate</b> event is generated when the new value is to be committed to the data source.</p>
		///	<p class="body">A cell's value can be changed programmatically by setting its <b>Value</b> property. Attempting to set the <b>Value</b> property of the cell whose value will be modified in this event procedure, however, will generate an error.</p>
		///	<p class="body">The <b>AfterCellUpdate</b> event, which occurs after a cell accepts a new value, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeCellUpdate")]	
		public event BeforeCellUpdateEventHandler		BeforeCellUpdate
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECELLUPDATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECELLUPDATE, value ); }
		}

		/// <summary>
		/// Occurs before a row has been resized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>row</i> argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row that will be resized. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">The <i>rowheight</i> argument indicates the new height of the row. The current height is indicated by the <b>Height</b> property of the UltraGridRow object returned by <i>row</i>.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the row from being resized. This argument can be used to prevent the row from resizing unless a certain condition is met.</p>
		///	<p class="body">Depending on the value of the <b>RowSizing</b> property, more than one row can be affected by the resize. In this case, <i>row</i> refers to the original row being resized.</p>
		///	<p class="body">The <b>AfterRowResize</b> event, which occurs after a row has been resized, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowResize")]	
		public event BeforeRowResizeEventHandler		BeforeRowResize
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWRESIZE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWRESIZE, value );} 
		}

		/// <summary>
		/// Occurs before one or more rows are deleted.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>rows</i> argument returns a reference to a Rows collection that can be used to retrieve references to the UltraGridRow object or objects being deleted. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
		///	<p class="body">The <i>displaypromptmsg</i> argument enables you to hide the default confirmation message. This argument can be used to display your own dialog.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the rows from being deleted. This argument can be used to prevent the user from deleting rows unless a certain condition is met.</p>
		///	<p class="body">This event is generated when rows are to be deleted, either programmatically, or by user interaction. To prevent the user from deleting rows, set the Override's <see cref="UltraGridOverride.AllowDelete"/> property to False. Rows can be deleted programmatically by invoking either the <b>Delete</b> method or the <b>DeleteSelectedRows</b> method.</p>
		///	<p class="body">The text displayed in the default confirmation dialog can be modified by setting the <b>DialogStrings</b> property.</p>
		///	<p class="body">The <b>AfterRowsDeleted</b> event, which occurs after rows are deleted, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowsDeleted")]	
		public event BeforeRowsDeletedEventHandler		BeforeRowsDeleted
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWSDELETED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWSDELETED, value );} 
		}

		/// <summary>
		/// Occurs before a row scrolling region is scrolled.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>newstate</i> argument returns a reference to a RowScrollRegion object that can be used to set properties of, and invoke methods on, the rowscrollregion as it exists after the scroll. You can use this reference to access any of the returned rowscrollregion's properties or methods.</p>
		///	<p class="body">The <i>oldstate</i> argument returns a reference to a RowScrollRegion object that can be used to set properties of, and invoke methods on, the rowscrollregion as it will exist after the scroll. You can use this reference to access any of the returned rowscrollregion's properties or methods.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the rowscrollregion from scrolling. This argument can be used to prevent the user from scrolling unless a certain condition is met.</p>
		///	<p class="body">This event is generated before a rowscrollregion is scrolled, either programmatically, or by user interaction. A rowscrollregion can be scrolled programmatically by invoking its <b>Scroll</b> method.</p>
		///	<p class="body">The <b>ScrollBar</b> property of a scrolling region determines whether a scroll bar is displayed for that scrolling region.</p>
		///	<p class="body">The <b>AfterRowRegionScroll</b> event, which occurs after a rowscrollregion was scrolled, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	<p class="body">The <b>BeforeColRegionScroll</b> event is generated before a colscrollregion is scrolled.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowRegionScroll")]	
		public event BeforeRowRegionScrollEventHandler  BeforeRowRegionScroll
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWREGIONSCROLL, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWREGIONSCROLL, value );} 
		}

		/// <summary>
		/// Occurs before a row scrolling region object is resized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>region1</i> argument returns a reference to a RowScrollRegion object that can be used to set properties of, and invoke methods on, the top rowscrollregion that will be sized. You can use this reference to access any of the returned rowscrollregion's properties or methods. However, the <b>Height</b> property of this rowscrollregion is read-only in this event procedure.</p>
		///	<p class="body">The <i>region2</i> argument returns a reference to a RowScrollRegion object that can be used to set properties of, and invoke methods on, the bottom rowscrollregion that will be sized. You can use this reference to access any of the returned rowscrollregion's properties or methods. However, the <b>Height</b> property of this rowscrollregion is read-only in this event procedure.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the rowscrollregions from sizing. This argument can be used to prevent the user from resizing the rowscrollregions unless a certain condition is met. To prevent users from actually moving the rowscrollregion's splitter bar, set its <b>SizingMode</b> property to 0 (SizingModeFixed).</p>
		///	<p class="body">This event is generated before a rowscrollregion is sized, either programmatically, or by user interaction. A rowscrollregion can be sized programmatically by setting its <b>Height</b> property. Because rowscrollregions are vertical scrolling regions, the width of all rowscrollregions will always be identical. Attempting to set the <b>Height</b> property of a rowscrollregion being sized in this event procedure, however, will generate an error.</p>
		///	<p class="body">The <b>BeforeRowRegionSplit</b> event is generated before a rowscrollregion is split into two rowscrollregions.</p>
		///	<p class="body">The <b>AfterRowRegionSize</b> event, which occurs after a rowscrollregion was sized, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowRegionSize")]	
		public event BeforeRowRegionSizeEventHandler	BeforeRowRegionSize
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWREGIONSIZE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWREGIONSIZE, value );} 
		}

		/// <summary>
		/// Occurs before a row scrolling region is split into two row scrolling regions.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>originalrowscrollregion</i> argument returns a reference to a RowScrollRegion object that can be used to set properties of, and invoke methods on, the rowscrollregion as it exists before the split. You can use this reference to access any of the returned rowscrollregion's properties or methods. However, the <b>Height</b> property of this rowscrollregion is read-only in this event procedure.</p>
		///	<p class="body">The <i>newrowscrollregion</i> argument returns a reference to a RowScrollRegion object that can be used to set properties of, and invoke methods on, the rowscrollregion as it will exist after the split. You can use this reference to access any of the returned rowscrollregion's properties or methods. However, the <b>Height</b> property of this rowscrollregion is read-only in this event procedure.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the rowscrollregion from being split. This argument can be used to prevent the user from splitting the rowscrollregion unless a certain condition is met.</p>
		///	<p class="body">This event is generated before a rowscrollregion is split, either programmatically, or by user interaction. A rowscrollregion can be split programmatically by invoking its <b>Split</b> method.</p>
		///	<p class="body">The <b>BeforeRowRegionRemoved</b> event is generated before a rowscrollregion is removed.</p>
		///	<p class="body">The <b>BeforeRowRegionSize</b> event is generated before a rowscrollregion is sized.</p>
		///	<p class="body">The <b>BeforeColRegionSplit</b> event is generated before a colscrollregion is split.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowRegionSplit")]	
		public event BeforeRowRegionSplitEventHandler	BeforeRowRegionSplit
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWREGIONSPLIT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWREGIONSPLIT, value );} 
		}

		/// <summary>
		/// Occurs when an error condition arises in the control.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>errorinfo</i> argument returns a reference to an Error object that can be used to set properties of, and invoke methods on, the error that generated this event. You can use this reference to access any of the returned error's properties or methods.</p>
		///	<p class="body">The <b>Code</b> and <b>Description</b> properties of <i>errorinfo</i> can be used to determine the number and description, respectively, of the error that generated this event.</p>
		///	<p class="body">When the error is related to the data source, the <b>DataError</b> property is set and can be used to further analyze what occurred.</p>
		///	<p class="body">Conversely, when the error is related to input validation, the <b>MaskError</b> property is set. The control can distinguish between numeric and alphabetic characters for input validation, but cannot validate for valid content, such as the correct month or time of day. In these cases, this event is not generated.</p>
		///	<p class="body">This event can be generated any time the control encounters an unexpected situation, such as if an update is attempted and the data source is not updateable.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_Error")]	
		public event ErrorEventHandler					Error
		{
			add { this.EventsOptimized.AddHandler( EVENT_ERROR, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_ERROR, value ); }
		}

		// SSP 5/2/02
		// Added CellDataError.
		//
		/// <summary>
		/// Occurs after BeforeExitEditMode is fired when the value user has typed is invalid. 
		/// </summary>
		///	<remarks>
		///	<p class="body">CellDataError is fired when the user tries to update the cell with an invalid value. It gets fired after BeforeExitEditMode is fired. If BeforeExitEditMode is cancalled, then this event and successive events related to exiting the edit mode are not fired.</p>
		///	<p class="body">Thie event gets fired either because the value in the editor is invalid, or when setting the value to bound datasource does not succed. If the grid fails to validate the value, then it will fire this event and will not attempt to update the cell with the value. If validations succeds, then the grid will attempt to update the cell, and if that fails, then it will fire this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_CellDataError")]	
		public event CellDataErrorEventHandler					CellDataError
		{
			add { this.EventsOptimized.AddHandler( EVENT_CELL_DATA_ERROR, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_CELL_DATA_ERROR, value ); }
		}

		/// <summary>
		/// Occurs when a print preview is first initiated by invoking the <b>PrintPreview</b> method.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>InitializePrintPreview</b> event occurs when the print job is first initiated via the <b>PrintPreview</b> method. It gives you the opportunity to set the default parameters for the print preview (level of zoom, preview window title and icon) and to apply default print job settings (such as page header and footer, margins, etc.) to the data being previewed. You use the <b>PrintInfo</b> object passed to the event via the <i>printinfo</i> parameter to apply these settings.</p>
		///	<p class="body">After you have set up the default print settings in the <b>InitializePrintPreview</b> event, the Print Preview screen will be displayed to the end user, previewing what the print job will look like using the settings you have specified. The user can view different parts of the report or change the settings of the print job by interacting directly with the provided interface. They can also choose to print directly from the preview screen, which will trigger the <b>InitializePrint</b> event. depending on how the <b>PrintPreview</b> method was invoked, the Print dialog may also be displayed.</p>
		///	<p class="body">The PrintInfo object is only accessible during this event, the <b>BeforePrint</b> event, the <b>InitializePrint</b> event and the <b>InitializeLogicalPrintPage</b> event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_InitializePrintPreview")]	
		public event InitializePrintPreviewEventHandler		InitializePrintPreview
		{
			add { this.EventsOptimized.AddHandler( EVENT_INTIALIZEPRINTPREVIEW, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INTIALIZEPRINTPREVIEW, value );} 
		}
		
		/// <summary>
		/// Occurs when a logical print page is being formatted for printing or previewing.
		/// </summary>
		///	<remarks>
		///	<p class="body">When you print a report using UltraWinGrid, you may find that the data from the grid does not easily fit onto a single sheet of paper. Although this is usually because there are too many rows to fit vertically, it is also possible that your data consists of too many columns to fit horizonatally. For this reason, the control must sometimes make a distinction between a single "logical" page and the "physical" page (or sheets of paper) that may be required to print it. Essentially, logical pages break only on row boundaries. If you print a report with enough columns to fill the widths of three sheets of paper, the first logical page will comprise three physical pages.</p>
		///	<p class="body">The <b>InitializeLogicalPrintPage</b> event occurs whenever the formatting of a new logical page is being calculated in preparation for printing or previewing. You can use the event to make changes to the logical page, such as changing the text of the page header or footer. You can access the settings of the print job (such as the text of the header and footer) by using the properties of LogicalPageLayoutInfo object that is passed into the event via the <i>LogicalPageLayoutInfo</i> parameter.</p>
		///	<p class="body">A common use of this event would be to increment the page number for each page of the report. The <i>LogicalPageNumber</i> parameter passed to the event makes this easy by providing you with the number of the current logical page. Automatic page numbering can be accomplished by setting the PageHeader or PageFooter to a string containing the token <b>&lt;#&gt;</b>. This token will be replaced by the page number.</p>
		///	<p class="body">If you wish to make changes to a print job based on the physical page of a report, you must use the <b>DrawFilter</b> interface.</p>
		///	<p class="body">The LogicalPageLayoutInfo object is only accessible during this event, the <b>BeforePrint</b> event, the <b>IntitializePrint</b> event and the <b>IntitializePrintPreview</b> event. Note that changes made to the print job in the <b>BeforePrint</b> event will cause the <b>InitializeLogicalPrintPage</b> event to occur again.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_InitializeLogicalPrintPage")]	
		public event InitializeLogicalPrintPageEventHandler	InitializeLogicalPrintPage
		{
			add { this.EventsOptimized.AddHandler( EVENT_INTIALIZELOGICALPRINTPAGE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INTIALIZELOGICALPRINTPAGE, value );}
		}

		/// <summary>
		/// Occurs when a print job is first initiated by invoking the <b>Print</b> method.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>InitializePrint</b> event occurs when the print job is first initiated via the <b>Print</b> method. It gives you the opportunity to set the default parameters for the print job (number of copies, page orientation, header and footer text, etc.) After you have set up the default print settings in the <b>InitializePrint</b> event, the Page Setup and Print dialogs may be displayed to the end user, depending on the parameters passed to the <b>Print</b> method. The user can change the defaults you have specified through these dialogs. Once the user has completed their changes, the <b>BeforePrint</b> event occurs, giving you the chance to examine the user's settings, change them if necessary or store them for future use.</p>
		///	<p class="body">The <b>PrintInfo</b> object is only accessible during this event, the <b>BeforePrint</b> event, the <b>IntitializePrintPreview</b> event and the <b>InitializeLogicalPrintPage</b> event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_InitializePrint")]	
		public event InitializePrintEventHandler				InitializePrint
		{
			add { this.EventsOptimized.AddHandler( EVENT_INTIALIZEPRINT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INTIALIZEPRINT, value ); }
		}

		/// <summary>
		/// Occurs after a print job has been initiated and configured by the user, just before data is sent to the printer.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>BeforePrint</b> event occurs before the report is sent to the printer, but after the user has had the opportunity to configure the print job using the Print and Page Setup dialogs. The Print and Page Setup dialogs can be made available to the user when invoking the <b>Print</b> method, and will contain any default settings you have specified for the print job in the <b>InitializePrint</b> event. The <b>BeforePrint</b> event is the last opportunity you have to change the parameters of a print job before it is committed to the print queue.</p>
		///	<p class="body">You can use the <b>BeforePrint</b> event to programmatically examine any changes to the PrintInfo object resulting from the user's actions. You can then choose to modify the user's settings where appropriate, or store them for later use.</p>
		///	<p class="body">The PrintInfo object is only accessible during this event, the <b>InitializeLogicalPrintPage</b> event, the <b>InitializePrint</b> event and the <b>InitializePrintPreview</b> event.</p>
		///	</remarks>
		// This event is fired before the document is sent to the printer.
		// Pages might not have been calculated yet, if they have not been or 
		// the print document has been modified, the pages will be recalculated
		[LocalizedDescription("LD_UltraGrid_E_BeforePrint")]	
		public event BeforePrintEventHandler					BeforePrint
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREPRINT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREPRINT, value );} 
		}


		// SSP 8/1/03
		// Moved FilterRow event to the base class.
		//
		

		
		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		// SSP 3/21/02
//		// Added BeforeRowFilterDropDownEventHandler for row filtering feature in version 2
//		//
//		/// <summary>
//		/// Occurs before the row filter drop down is dropped down when the user clicks on filter dropdown symbol on a column header.
//		/// </summary>
//		///	<remarks>
//		///	<p class="body">You can use the <b>BeforeRowFilterDropDown</b> event to cancel the drop down.</p>
//		///	<seealso cref="Infragistics.Win.UltraWinGrid.BeforeRowFilterDropDownEventArgs"/> <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.BeforeRowFilterDropDownPopulate"/> <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.AfterRowFilterChanged"/>
//		///	</remarks>
//		[LocalizedDescription("LD_UltraGrid_E_BeforeRowFilterDropDown")]
//		public event BeforeRowFilterDropDownEventHandler BeforeRowFilterDropDown
//		{
//			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWFILTERDROPDOWN, value ); }
//			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWFILTERDROPDOWN, value );} 
//		}

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		// SSP 4/15/04 - Virtual Binding
//		// Added BeforeRowFilterDropDownPopulate to give the users a chance to prevent the ultragrid
//		// from populating the filter drop down list. This can be useful if there are lot of rows
//		// and it takes a long time for the ultragrid to populate the filter drop down list.
//		// 
//		/// <summary>
//		/// Occurs before the row filter drop down is populated.
//		/// </summary>
//		///	<remarks>
//		///	<p class="body">You can use the <b>BeforeRowFilterDropDownPopulate</b> event to prevent the UltraGrid from populating the filter value list and instead populate it yourself. In <see cref="BeforeRowFilterDropDown"/> event, which is fired after this event, the filter value list is already populated. You can modify the filter value list there as well.</p>
//		///	<seealso cref="Infragistics.Win.UltraWinGrid.BeforeRowFilterDropDownPopulateEventArgs"/> <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.BeforeRowFilterDropDown"/>
//		///	</remarks>
//		[LocalizedDescription("LDR_UltraGrid_E_BeforeRowFilterDropDownPopulate")]
//		public event BeforeRowFilterDropDownPopulateEventHandler BeforeRowFilterDropDownPopulate
//		{
//			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWFILTERDROPDOWNPOPULATE, value ); }
//			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWFILTERDROPDOWNPOPULATE, value );} 
//		}

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		// SSP 3/21/02
//		// Added BeforeCustomRowFilterDialogEventHandler for row filtering feature in version 2
//		//
//		/// <summary>
//		/// Occurs when the user selects (Custom) from the row filter dropdown and before the grid displays the custom row filter dialog.
//		/// </summary>
//		///	<remarks>
//		///	<p class="body">You can use the <b>BeforeRowFilterDropDown</b> event to cancel the drop down.</p>
//		///	<seealso cref="Infragistics.Win.UltraWinGrid.BeforeRowFilterDropDownEventArgs"/>
//		///	</remarks>
//		[LocalizedDescription("LD_UltraGrid_E_BeforeCustomRowFilterDialog")]
//		public event BeforeCustomRowFilterDialogEventHandler BeforeCustomRowFilterDialog
//		{
//			add { this.EventsOptimized.AddHandler( EVENT_BEFORECUSTOMROWFILTERDIALOG, value ); }
//			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECUSTOMROWFILTERDIALOG, value );} 
//		}

		// SSP 5/17/02
		// Added events for summary rows.
		/// <summary>
		/// This event is fired before the grid is about to show the dialog for user to select summaries for a column. If its cancelled, the grid wont proceed.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>BeforeSummaryDialog</b> event to cancel the drop down.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeSummaryDialog")]
		public event BeforeSummaryDialogEventHandler BeforeSummaryDialog
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORESUMMARYDIALOG, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORESUMMARYDIALOG, value );} 
		}

		// SSP 12/5/03 UWG2295
		// Added AfterSummaryDialog event.
		// 
		/// <summary>
		/// This event is fired after the user closes the dialog for selecting summaries for a column.
		/// </summary>
		[LocalizedDescription("LDR_UltraGrid_E_AfterSummaryDialog")]
		public event AfterSummaryDialogEventHandler AfterSummaryDialog
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERSUMMARYDIALOG, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERSUMMARYDIALOG, value );} 
		}

		/// <summary>
		/// This event is fired when a summary value for a summary changes. It is also fired when the summary is calculated for the first time. The user can initialize appearance and other settings off the SummaryValue object that gets passed in the event handler.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use the <b>SummaryValueChanged</b> event to apply any appearance settings to passed in SummaryValue object.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGrid_E_SummaryValueChanged")]
		public event SummaryValueChangedEventHandler SummaryValueChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_SUMMARYVALUECHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_SUMMARYVALUECHANGED, value );} 
		}

		// SSP 6/25/03 UWG2413 - Row Layout Functionality
		// Added events that we need to fire when a cell or a header is resized in the row-layout mode.
		// Added BeforeRowLayoutItemResized event.
		//
		/// <summary>
		/// This event is fired when the user resizes a column header or a cell in the row-layout mode.
		/// </summary>
		/// <remarks>
		/// <p class="body">BeforeRowLayoutItemResized is fired when the user resizes a column header or a cell in the row-layout mode. It fires the event before applying the new width or the height to the item that's being resized. This event gives you an opportunity to prevent the user from resizing an item or change the new resized size.</p>
		/// <p><seealso cref="AfterRowLayoutItemResized"/></p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_BeforeRowLayoutItemResized")]
		public event BeforeRowLayoutItemResizedEventHandler BeforeRowLayoutItemResized
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWLAYOUTITEMRESIZED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWLAYOUTITEMRESIZED, value );} 
		}

		// SSP 6/25/03 UWG2413 - Row Layout Functionality
		// Added events that we need to fire when a cell or a header is resized in the row-layout mode.
		// Added AfterRowLayoutItemResized event.
		//
		/// <summary>
		/// This event is fired after the user has resized a column header or a cell in the row-layout mode.
		/// </summary>
		/// <remarks>
		/// <p class="body">AfterRowLayoutItemResized is fired after the user has resized a column header or a cell in the row-layout mode.</p>
		/// <p><seealso cref="BeforeRowLayoutItemResized"/></p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_AfterRowLayoutItemResized")]
		public event AfterRowLayoutItemResizedEventHandler AfterRowLayoutItemResized
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWLAYOUTITEMRESIZED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWLAYOUTITEMRESIZED, value );} 
		}

		// SSP 7/30/03 UWG2544
		// Added BeforePerformAction and AfterPerformAction events.
		//
		/// <summary>
		/// BeforePerformAction event gets fired before an action associated with a key action mapping
		/// is about to be performed.
		/// </summary>
		/// <remarks>
		/// <p class="body">BeforePerformAction event gets fired before an action associated with a key action mapping is about to be performed. It is a cancelable event. When it's cancled, the UltraGrid will not perform the action for which this event was fired.</p>
		/// <seealso cref="UltraGrid.KeyActionMappings"/> <seealso cref="BeforeUltraGridPerformActionEventArgs"/> <seealso cref="UltraGrid.AfterPerformAction"/>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_BeforePerformAction")]
		public event BeforeUltraGridPerformActionEventHandler BeforePerformAction
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREPERFORMACTION, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREPERFORMACTION, value );} 
		}

		/// <summary>
		/// AfterPerformAction event gets fired after an action associated with a key action mapping
		/// has been performed.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGrid.KeyActionMappings"/> <seealso cref="AfterUltraGridPerformActionEventArgs"/> <seealso cref="UltraGrid.BeforePerformAction"/>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_AfterPerformAction")]
		public event AfterUltraGridPerformActionEventHandler AfterPerformAction
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERPERFORMACTION, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERPERFORMACTION, value );} 
		}

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase		
//		// SSP 8/1/03
//		// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
//		//
//		/// <summary>
//		/// BeforeRowFilterChanged event gets fired when the user modifies row filters for a column. This event is cancelable.
//		/// </summary>
//		/// <remarks>
//		/// <seealso cref="UltraGrid.AfterRowFilterChanged"/> <seealso cref="BeforeRowFilterChangedEventArgs"/>
//		/// </remarks>
//		[LocalizedDescription("LDR_UltraGrid_E_BeforeRowFilterChanged")]
//		public event BeforeRowFilterChangedEventHandler BeforeRowFilterChanged
//		{
//			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWFILTERCHANGED, value ); }
//			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWFILTERCHANGED, value );} 
//		}

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase	
//		/// <summary>
//		/// AfterRowFilterChanged event gets fired after the user has modified row filters for a column.
//		/// </summary>
//		/// <remarks>
//		/// <seealso cref="UltraGrid.BeforeRowFilterChanged"/> <seealso cref="AfterRowFilterChangedEventArgs"/>
//		/// </remarks>
//		[LocalizedDescription("LDR_UltraGrid_E_AfterRowFilterChanged")]
//		public event AfterRowFilterChangedEventHandler AfterRowFilterChanged
//		{
//			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWFILTERCHANGED, value ); }
//			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWFILTERCHANGED, value );} 
//		}

		// SSP 12/5/03 UWG2295
		// Added AfterCardsScroll event.
		//
		/// <summary>
		/// AfterCardsScroll is fired when the user scrolls cards in a card area.
		/// </summary>
		/// <remarks>
		/// <seealso cref="AfterCardsScrollEventArgs"/>
		/// </remarks>
		[ LocalizedDescription( "LDR_UltraGrid_E_AfterCardsScroll" ) ]
		public event AfterCardsScrollEventHandler AfterCardsScroll
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERCARDSSCROLL, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCARDSSCROLL, value );} 
		}

        // JDN 11/17/04 CardCompressedStateChanged events
        /// <summary>
        /// BeforeCardCompressedStateChanged is fired before a Card Row is Expanded or Compressed.
        /// </summary>
        /// <remarks>
        /// <seealso cref="AfterCardCompressedStateChanged"/>
        /// <seealso cref="BeforeCardCompressedStateChangedEventArgs"/>
        /// </remarks>
        [ LocalizedDescription( "LDR_UltraGrid_E_BeforeCardCompressedStateChanged" ) ]
        public event BeforeCardCompressedStateChangedEventHandler BeforeCardCompressedStateChanged
        {
            add { this.EventsOptimized.AddHandler( EVENT_BEFORECARDCOMPRESSEDSTATECHANGED , value ); }
            remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECARDCOMPRESSEDSTATECHANGED , value );} 
        }

        // JDN 11/17/04 CardCompressedStateChanged events
        /// <summary>
        /// AfterCardCompressedStateChanged is fired after a Card Row is Expanded or Compressed.
        /// </summary>
        /// <remarks>
        /// <seealso cref="BeforeCardCompressedStateChanged"/>
        /// <seealso cref="AfterCardCompressedStateChangedEventArgs"/>
        /// </remarks>
        [ LocalizedDescription( "LDR_UltraGrid_E_AfterCardCompressedStateChanged" ) ]
        public event AfterCardCompressedStateChangedEventHandler AfterCardCompressedStateChanged
        {
            add { this.EventsOptimized.AddHandler( EVENT_AFTERCARDCOMPRESSEDSTATECHANGED , value ); }
            remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCARDCOMPRESSEDSTATECHANGED , value );} 
        }

		// SSP 12/15/04 - IDataErrorInfo Support
		// 
		/// <summary>
		/// BeforeDisplayDataErrorTooltip is fired before displaying data error tooltip.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridOverride.SupportDataErrorInfo"/>
		/// </remarks>
		[ LocalizedDescription( "LDR_UltraGrid_E_BeforeDisplayDataErrorTooltip" ) ]
		public event BeforeDisplayDataErrorTooltipEventHandler BeforeDisplayDataErrorTooltip
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREDISPLAYDATAERRORTOOLTIP, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREDISPLAYDATAERRORTOOLTIP, value );} 
		}

		#region AfterRowFixedStateChangedEventHandler

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		/// <summary>
		/// AfterRowFixedStateChanged event is fired after the user fixes or unfixes a row.
		/// </summary>
		[LocalizedDescription("LDR_UltraGrid_E_AfterRowFixedStateChanged")]
		public event AfterRowFixedStateChangedEventHandler AfterRowFixedStateChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWFIXEDSTATECHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWFIXEDSTATECHANGED, value );} 
		}

		#endregion AfterRowFixedStateChangedEventHandler

		#region BeforeRowFixedStateChangedEventHandler

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		/// <summary>
		/// AfterRowFixedStateChanged event is fired before the user fixes or unfixes a row.
		/// </summary>
		[LocalizedDescription("LDR_UltraGrid_E_BeforeRowFixedStateChanged")]
		public event BeforeRowFixedStateChangedEventHandler BeforeRowFixedStateChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWFIXEDSTATECHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWFIXEDSTATECHANGED, value );} 
		}

		#endregion BeforeRowFixedStateChangedEventHandler

		#region BeforeMultiCellOperation

		// SSP 11/22/05 - NAS 6.1 Multi-cell Operations
		// 
		/// <summary>
		/// BeforeMultiCellOperation event is fired before the user performs a multi-cell operation.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// You can enable multi-cell operations using the Override's
		/// <see cref="UltraGridOverride.AllowMultiCellOperations"/> property.
		/// </p>
		/// <seealso cref="UltraGridOverride.AllowMultiCellOperations"/>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_BeforeMultiCellOperation")]
		public event BeforeMultiCellOperationEventHandler BeforeMultiCellOperation
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREMULTICELLOPERATION, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREMULTICELLOPERATION, value );} 
		}

		#endregion BeforeMultiCellOperation

        // MBS 2/29/08 - RowEditTemplate NA2008 V2
        // Added various events related to the RowEditTemplate
        //
        #region AfterRowEditTemplateClosed

        /// <summary>
        /// AfterRowEditTemplateClosed is fired after the edit template of a row has been closed.
        /// </summary>
        /// <seealso cref="UltraGridBand.RowEditTemplate"/>
        /// <seealso cref="UltraGridRow.RowEditTemplate"/>        
        [LocalizedDescription("LD_UltraGrid_E_AfterRowEditTemplateClosed")]
        public event AfterRowEditTemplateClosedEventHandler AfterRowEditTemplateClosed
        {
            add { this.EventsOptimized.AddHandler(EVENT_AFTERROWEDITTEMPLATECLOSED, value); }
            remove { this.EventsOptimized.RemoveHandler(EVENT_AFTERROWEDITTEMPLATECLOSED, value); }
        }
        #endregion //AfterRowEditTemplateClosed
        //
        #region AfterRowEditTemplateDisplayed

        /// <summary>
        /// AfterRowEditTemplateDisplayed is fired after the edit template of a row has been shown.
        /// </summary>
        /// <seealso cref="UltraGridBand.RowEditTemplate"/>
        /// <seealso cref="UltraGridRow.RowEditTemplate"/>     
        [LocalizedDescription("LD_UltraGrid_E_AfterRowEditTemplateDisplayed")]
        public event AfterRowEditTemplateDisplayedEventHandler AfterRowEditTemplateDisplayed
        {
            add { this.EventsOptimized.AddHandler(EVENT_AFTERROWEDITTEMPLATEDISPLAYED, value); }
            remove { this.EventsOptimized.RemoveHandler(EVENT_AFTERROWEDITTEMPLATEDISPLAYED, value); }
        }
        #endregion //AfterRowEditTemplateDisplayed
        //
        #region BeforeRowEditTemplateClosed

        /// <summary>
        /// BeforeRowEditTemplateClosed is fired when the edit template of a row is about to be closed.
        /// </summary>
        /// <seealso cref="UltraGridBand.RowEditTemplate"/>
        /// <seealso cref="UltraGridRow.RowEditTemplate"/>  
        [LocalizedDescription("LD_UltraGrid_E_BeforeRowEditTemplateClosed")]
        public event BeforeRowEditTemplateClosedEventHandler BeforeRowEditTemplateClosed
        {
            add { this.EventsOptimized.AddHandler(EVENT_BEFOREROWEDITTEMPLATECLOSED, value); }
            remove { this.EventsOptimized.RemoveHandler(EVENT_BEFOREROWEDITTEMPLATECLOSED, value); }
        }
        #endregion //BeforeRowEditTemplateClosed
        //
        #region BeforeRowEditTemplateDisplayed

        /// <summary>
        /// BeforeRowEditTemplateDisplayed is fired before the edit template of a row is shown.
        /// </summary>
        /// <seealso cref="UltraGridBand.RowEditTemplate"/>
        /// <seealso cref="UltraGridRow.RowEditTemplate"/>  
        [LocalizedDescription("LD_UltraGrid_E_BeforeRowEditTemplateDisplayed")]
        public event BeforeRowEditTemplateDisplayedEventHandler BeforeRowEditTemplateDisplayed
        {
            add { this.EventsOptimized.AddHandler(EVENT_BEFOREROWEDITTEMPLATEDISPLAYED, value); }
            remove { this.EventsOptimized.RemoveHandler(EVENT_BEFOREROWEDITTEMPLATEDISPLAYED, value); }
        }
        #endregion //BeforeRowEditTemplateDisplayed
        //
        #region RowEditTemplateRequested

        /// <summary>
        /// RowEditTemplateRequested is fired when an action is performed that will cause a RowEditTemplate to be shown.
        /// </summary>
        /// <remarks>
        /// <p class="body">
        /// This event provides the ability to take the <see cref="UltraGridRowEditTemplate"/> associate with the row and manually
        /// reparent the template into a form or control.
        /// </p>
        /// <p class="note">
        /// <b>Note: </b>If this event is handled and the RowEditTemplate is reparented and manually shown, the grid
        /// no longer has control over when to close the template; as such, it is the responsibility of the developer
        /// to determine under which conditions the template should be closed.
        /// </p>
        /// </remarks>
        /// <seealso cref="UltraGridBand.RowEditTemplate"/>
        /// <seealso cref="UltraGridRow.RowEditTemplate"/>  
        /// <seealso cref="UltraGridOverride.RowEditTemplateUIType"/>
        [LocalizedDescription("LD_UltraGrid_E_RowEditTemplateRequested")]
        public event RowEditTemplateRequestedEventHandler RowEditTemplateRequested
        {
            add { this.EventsOptimized.AddHandler(EVENT_ROWEDITTEMPLATEREQUESTED, value); }
            remove { this.EventsOptimized.RemoveHandler(EVENT_ROWEDITTEMPLATEREQUESTED, value); }
        }
        #endregion //RowEditTemplateRequested

        // MBS 7/1/08 - NA2008 V3
        #region ClickCell

        /// <summary>
        /// Occurs when a cell is clicked.
        /// </summary>
        [LocalizedDescription("LD_UltraGrid_E_ClickCell")]
        public event ClickCellEventHandler ClickCell
        {
            add { this.EventsOptimized.AddHandler(EVENT_CLICKCELL, value); }
            remove { this.EventsOptimized.RemoveHandler(EVENT_CLICKCELL, value); }
        }
        #endregion //ClickCell

        // CDS 02/02/08 TFS12512 Moved to the UltraGridBase class
        //// CDS NAS v9.1 Header CheckBox
        //#region BeforeHeaderCheckStateChanged

        ///// <summary>
        ///// Occurs before the CheckState of the Header CheckBox is changed
        ///// </summary>
        //[LocalizedDescription("LD_UltraGrid_E_BeforeHeaderCheckStateChanged")]
        //public event BeforeHeaderCheckStateChangedEventHandler BeforeHeaderCheckStateChanged
        //{
        //    add { this.EventsOptimized.AddHandler(EVENT_BEFOREHEADERCHECKSTATECHANGED, value); }
        //    remove { this.EventsOptimized.RemoveHandler(EVENT_BEFOREHEADERCHECKSTATECHANGED, value); }
        //}
        //#endregion BeforeHeaderCheckStateChanged

        //// CDS NAS v9.1 Header CheckBox
        //#region AfterHeaderCheckStateChanged

        ///// <summary>
        ///// Occurs after the CheckState of the Header CheckBox is changed
        ///// </summary>
        //[LocalizedDescription("LD_UltraGrid_E_AfterHeaderCheckStateChanged")]
        //public event AfterHeaderCheckStateChangedEventHandler AfterHeaderCheckStateChanged
        //{
        //    add { this.EventsOptimized.AddHandler(EVENT_AFTERHEADERCHECKSTATECHANGED, value); }
        //    remove { this.EventsOptimized.RemoveHandler(EVENT_AFTERHEADERCHECKSTATECHANGED, value); }
        //}
        //#endregion AfterHeaderCheckStateChanged

        #endregion

        #region Constructor
        /// <summary>
		/// UltraGrid constructor. Initializes a new instance of <see cref="UltraGrid"/>.
		/// </summary>
		///	<remarks>
		///	<p class="body">The UltraGrid object represents the UltraWinGrid control itself. It occupies the top-most level of all control hierarchies. In the data hierarchy used by the control, bands, rows and cells are all child objects of the grid. Many of the properties that apply to an UltraGridBand object also apply to the grid. In addition, there are properties unique to the UltraGrid object, such as <b>MaxColScrollRegions</b> and <b>MaxRowScrollRegions</b>, which limit the number of column and row scrolling regions and affect all bands.</p>
		///	</remarks>
		public UltraGrid()
		{

			// verify and cache the license
			//
			// AS 3/5/03 DNF37
			// Wrapped in a try/catch for a FileNotFoundException.
			// When the assembly is loaded dynamically, VS seems 
			// to be trying to reload a copy of Shared even though 
			// one is in memory. This generates a FileNotFoundException
			// when the dll is not in the gac and not in the AppBase
			// for the AppDomain.
			//
			try
			{
				this.license = LicenseManager.Validate( typeof(UltraGrid), this ) as UltraLicense;
			}
			catch (System.IO.FileNotFoundException)	{}

			// set the default update mode to OnRowChange
			this.ResetUpdateMode();
           
			// Set the appropriate control styles
			//
			this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
			this.SetStyle( ControlStyles.ResizeRedraw, true );
			this.SetStyle( ControlStyles.Selectable, true );
            this.SetStyle( UltraGridBase.DoubleBufferControlStyle, false);
            this.SetStyle( ControlStyles.StandardClick, true );
			this.SetStyle( ControlStyles.StandardDoubleClick, true );
			
			// MRS 10/22/04 - UWG3542
			this.SetStyle( ControlStyles.SupportsTransparentBackColor, true);
		}

		#endregion

		#region About Dialog and Licensing Interface

		/// <summary>
		/// Display the about dialog
		/// </summary>
		[ DesignOnly( true ) ]
		[ LocalizedDescription("LD_UltraGrid_P_About") ]
		[ LocalizedCategory("LC_Design") ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ ParenthesizePropertyName( true ) ]
		[ Editor(typeof(AboutDialogEditor), typeof(System.Drawing.Design.UITypeEditor) ) ]
		public object About { get { return null; } }


		UltraLicense IUltraLicensedComponent.License { get { return this.license; } }
	
		#endregion


		#region Fire Event
		
		internal void FireEvent( GridEventIds id, EventArgs e )
		{
            
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( id ) )
				return;

			// SSP 1/22/04 - Optimizations
			// Commented following code out. Switch statement below has a default
			// case which Debug.Fails too.
			//
			

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( id );

			try
			{
				switch ( id )
				{
					case GridEventIds.AfterCellActivate:
						this.OnAfterCellActivate();
						break;

					case GridEventIds.AfterCellCancelUpdate:
						this.OnAfterCellCancelUpdate( e as CellEventArgs );
						break;

					case GridEventIds.AfterCellListCloseUp:
						this.OnAfterCellListCloseUp( e as CellEventArgs );
						break;

					case GridEventIds.AfterCellUpdate:
						this.OnAfterCellUpdate( e as CellEventArgs );
						break;

					case GridEventIds.AfterColPosChanged:
						this.OnAfterColPosChanged( e as AfterColPosChangedEventArgs );
						break;

					case GridEventIds.AfterColRegionScroll:
						this.OnAfterColRegionScroll( e as ColScrollRegionEventArgs );
						break;

					case GridEventIds.AfterColRegionSize:
						this.OnAfterColRegionSize( e as ColScrollRegionEventArgs );
						break;

					case GridEventIds.AfterEnterEditMode:
						this.OnAfterEnterEditMode();
						break;

					case GridEventIds.AfterExitEditMode:
						this.OnAfterExitEditMode();

						// JJD 12/17/03
						// Let accessible listeners know that the focus has changed
						this.AccessibilityNotifyClientsInternal( AccessibleEvents.Focus, 0 );
						break;

					case GridEventIds.AfterGroupPosChanged:
						this.OnAfterGroupPosChanged( e as AfterGroupPosChangedEventArgs );
						break;

					case GridEventIds.AfterRowActivate:
						this.OnAfterRowActivate();
						break;

					case GridEventIds.AfterRowCancelUpdate:
						this.OnAfterRowCancelUpdate( e as RowEventArgs );
						break;

					case GridEventIds.AfterRowCollapsed:
						this.OnAfterRowCollapsed( e as RowEventArgs );
						break;

					case GridEventIds.AfterRowExpanded:
						this.OnAfterRowExpanded( e as RowEventArgs );
						break;

					case GridEventIds.AfterRowInsert:
						this.OnAfterRowInsert( e as RowEventArgs );
						break;

					case GridEventIds.AfterRowRegionScroll:
						this.OnAfterRowRegionScroll( e as RowScrollRegionEventArgs );
						break;
                        							
					case GridEventIds.AfterRowRegionSize:
						this.OnAfterRowRegionSize( e as RowScrollRegionEventArgs );
						break;

					case GridEventIds.AfterRowResize:
						this.OnAfterRowResize( e as RowEventArgs );
						break;

					case GridEventIds.AfterRowsDeleted:
						this.OnAfterRowsDeleted();
						break;

					case GridEventIds.AfterRowUpdate:
						this.OnAfterRowUpdate( e as RowEventArgs );
						break;

					case GridEventIds.AfterSelectChange:
						this.OnAfterSelectChange( e as AfterSelectChangeEventArgs );
						break;

					case GridEventIds.AfterSortChange:
						this.OnAfterSortChange( e as BandEventArgs );
						break;

					case GridEventIds.BeforeAutoSizeEdit:
						this.OnBeforeAutoSizeEdit( e as CancelableAutoSizeEditEventArgs );
						break;

					case GridEventIds.BeforeCellActivate:
						this.OnBeforeCellActivate( e as CancelableCellEventArgs );
						break;

					case GridEventIds.BeforeCellCancelUpdate:
						this.OnBeforeCellCancelUpdate( e as CancelableCellEventArgs );
						break;

					case GridEventIds.BeforeCellDeactivate:
						this.OnBeforeCellDeactivate( e as CancelEventArgs );
						break;						

					case GridEventIds.BeforeCellListDropDown:
						this.OnBeforeCellListDropDown( e as CancelableCellEventArgs );
						break;
                        							
					case GridEventIds.BeforeCellUpdate:
						this.OnBeforeCellUpdate( e as BeforeCellUpdateEventArgs );
						break;

					case GridEventIds.BeforeColPosChanged:
						this.OnBeforeColPosChanged( e as BeforeColPosChangedEventArgs );
						break;

					case GridEventIds.BeforeColRegionRemoved:
						this.OnBeforeColRegionRemoved( e as BeforeColRegionRemovedEventArgs );
						break;

					case GridEventIds.BeforeColRegionScroll:
						this.OnBeforeColRegionScroll( e as BeforeColRegionScrollEventArgs );
						break;

					case GridEventIds.BeforeColRegionSize:
						this.OnBeforeColRegionSize( e as BeforeColRegionSizeEventArgs );
						break;

					case GridEventIds.BeforeColRegionSplit:
						this.OnBeforeColRegionSplit( e as BeforeColRegionSplitEventArgs );
						break;
							
					case GridEventIds.BeforeEnterEditMode:
						this.OnBeforeEnterEditMode( e as CancelEventArgs );
						break;

					case GridEventIds.BeforeExitEditMode:
						// SSP 5/2/02
						// Changed the event args of BeforeExitEditMode from CancelEventArgs to
						// BeforeExitEditModeEventArgs
						//
						//this.OnBeforeExitEditMode( e as CancelEventArgs );
						this.OnBeforeExitEditMode( e as BeforeExitEditModeEventArgs );
						break;

					case GridEventIds.BeforeGroupPosChanged:
						this.OnBeforeGroupPosChanged( e as BeforeGroupPosChangedEventArgs );
						break;

					case GridEventIds.BeforePrint:
						this.OnBeforePrint( e as CancelablePrintEventArgs );
						break;

					case GridEventIds.BeforeRowActivate:
						this.OnBeforeRowActivate( e as RowEventArgs );
						break;

					case GridEventIds.BeforeRowCancelUpdate:
						this.OnBeforeRowCancelUpdate( e as CancelableRowEventArgs );
						break;

					case GridEventIds.BeforeRowCollapsed:
						this.OnBeforeRowCollapsed( e as CancelableRowEventArgs );
						break;

					case GridEventIds.BeforeRowDeactivate:
						this.OnBeforeRowDeactivate( e as CancelEventArgs );
						break;

					case GridEventIds.BeforeRowExpanded:
						this.OnBeforeRowExpanded( e as CancelableRowEventArgs );
						break;

					case GridEventIds.BeforeRowInsert:
						this.OnBeforeRowInsert( e as BeforeRowInsertEventArgs );
						break;

					case GridEventIds.BeforeRowRegionRemoved:
						this.OnBeforeRowRegionRemoved( e as BeforeRowRegionRemovedEventArgs );
						break;
							
					case GridEventIds.BeforeRowRegionScroll:
						this.OnBeforeRowRegionScroll( e as BeforeRowRegionScrollEventArgs );
						break;

					case GridEventIds.BeforeRowRegionSize:
						this.OnBeforeRowRegionSize( e as BeforeRowRegionSizeEventArgs );
						break;

					case GridEventIds.BeforeRowRegionSplit:
						this.OnBeforeRowRegionSplit( e as BeforeRowRegionSplitEventArgs );
						break;

					case GridEventIds.BeforeRowResize:
						this.OnBeforeRowResize( e as BeforeRowResizeEventArgs );
						break;

					case GridEventIds.BeforeRowsDeleted:
						this.OnBeforeRowsDeleted( e as BeforeRowsDeletedEventArgs );
						break;

					case GridEventIds.BeforeRowUpdate:
						this.OnBeforeRowUpdate( e as CancelableRowEventArgs );
						break;

					case GridEventIds.BeforeSelectChange:
						this.OnBeforeSelectChange( e as BeforeSelectChangeEventArgs );
						break;

					case GridEventIds.BeforeSortChange:
						this.OnBeforeSortChange( e as BeforeSortChangeEventArgs );
						break;

					case GridEventIds.CellChange:
						this.OnCellChange( e as CellEventArgs );
						break;

					case GridEventIds.CellListSelect:
						this.OnCellListSelect( e as CellEventArgs );
						break;

					case GridEventIds.ClickCellButton:
						this.OnClickCellButton( e as CellEventArgs );
						break;

					case GridEventIds.Error:
						this.OnError( e as ErrorEventArgs );
						break;

						// SSP 5/2/02
						// Added CellDataError event.
					case GridEventIds.CellDataError:
						this.OnCellDataError( e as CellDataErrorEventArgs );
						break;

					case GridEventIds.InitializeGroupByRow:
						this.OnInitializeGroupByRow( e as InitializeGroupByRowEventArgs );
						break;

					case GridEventIds.InitializeLayout:
						this.OnInitializeLayout( e as InitializeLayoutEventArgs );
						break;

					case GridEventIds.InitializeLogicalPrintPage:
						this.OnInitializeLogicalPrintPage( e as CancelableLogicalPrintPageEventArgs );
						break;

					case GridEventIds.InitializeRow:
						this.OnInitializeRow( e as InitializeRowEventArgs );
						break;

					case GridEventIds.InitializePrint:
						this.OnInitializePrint( e as CancelablePrintEventArgs );
						break;

					case GridEventIds.InitializePrintPreview:
						this.OnInitializePrintPreview( e as CancelablePrintPreviewEventArgs );
						break;

					case GridEventIds.OnSelectionDrag:
						this.OnSelectionDrag( e as CancelEventArgs );
						break;

						// SSP 3/26/02
						// Added entries for BeforeRowFilterDropDown, FilterRow and 
						// BeforeCustomRowFilterDialog events.
						//
					case GridEventIds.BeforeRowFilterDropDown:
						this.OnBeforeRowFilterDropDown( e as BeforeRowFilterDropDownEventArgs );
						break;

					// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					// Moved this back here along with the FireCommonEvent method
						// SSP 8/1/03
						// Moved the FilterRow event to the UltraGridBase class.
						//
						
					case GridEventIds.FilterRow:
						this.OnFilterRow( e as FilterRowEventArgs );
						break;

					case GridEventIds.BeforeCustomRowFilterDialog:
						this.OnBeforeCustomRowFilterDialog( e as BeforeCustomRowFilterDialogEventArgs );
						break;

						// SSP 5/16/02
						// Added entries for BeforeSummaryDialog and SummaryValueChanged events.
						//
					case GridEventIds.BeforeSummaryDialog:
						this.OnBeforeSummaryDialog( e as BeforeSummaryDialogEventArgs );
						break;

					case GridEventIds.SummaryValueChanged:
						this.OnSummaryValueChanged( e as SummaryValueChangedEventArgs );
						break;

						// SSP 6/25/03 UWG2413 - Row Layout Functionality
						// Added BeforeRowLayoutItemResized	and AfterRowLayoutItemResized events.
						//
					case GridEventIds.BeforeRowLayoutItemResized:
						this.OnBeforeRowLayoutItemResized( e as BeforeRowLayoutItemResizedEventArgs );
						break;

						// SSP 6/25/03 UWG2413 - Row Layout Functionality
						// Added BeforeRowLayoutItemResized	and AfterRowLayoutItemResized events.
						//
					case GridEventIds.AfterRowLayoutItemResized:
						this.OnAfterRowLayoutItemResized( e as AfterRowLayoutItemResizedEventArgs );
						break;

						// SSP 7/30/03 UWG2544
						// Added BeforePerformAction and AfterPerformAction events.
						//
						// ----------------------------------------------------------------------------
					case GridEventIds.BeforePerformAction:
						this.OnBeforePerformAction( e as BeforeUltraGridPerformActionEventArgs );
						break;
					case GridEventIds.AfterPerformAction:
						this.OnAfterPerformAction( e as AfterUltraGridPerformActionEventArgs );
						break;
						// ----------------------------------------------------------------------------

						// SSP 8/1/03
						// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
						//
						// ----------------------------------------------------------------------------
					case GridEventIds.BeforeRowFilterChanged:
						this.OnBeforeRowFilterChanged( e as BeforeRowFilterChangedEventArgs );
						break;
					case GridEventIds.AfterRowFilterChanged:
						this.OnAfterRowFilterChanged( e as AfterRowFilterChangedEventArgs );
						break;
						// ----------------------------------------------------------------------------
						// SSP 11/10/03 Add Row Feature
						//
					case GridEventIds.InitializeTemplateAddRow:
						this.OnInitializeTemplateAddRow( e as InitializeTemplateAddRowEventArgs );
						break;
					// SSP 12/5/03 UWG2295
					// Added AfterSummaryDialog event.
					// 
					case GridEventIds.AfterSummaryDialog:
						this.OnAfterSummaryDialog( e as AfterSummaryDialogEventArgs );
						break;
						// SSP 12/5/03 UWG2295
						// Added AfterCardsScroll event.
						//
					case GridEventIds.AfterCardsScroll:
						this.OnAfterCardsScroll( e as AfterCardsScrollEventArgs );
						break;
					case GridEventIds.BeforeRowFilterDropDownPopulate:
						this.OnBeforeRowFilterDropDownPopulate( e as BeforeRowFilterDropDownPopulateEventArgs );
						break;
                        // JDN 11/17/04 CardCompressedStateChanged events
                        // Added BeforeCardCompressedStateChanged and AfterCardCompressedStateChanged events
                        // ----------------------------------------------------------------------------
                    case GridEventIds.BeforeCardCompressedStateChanged:
                        this.OnBeforeCardCompressedStateChanged( e as BeforeCardCompressedStateChangedEventArgs );
                        break;
                    case GridEventIds.AfterCardCompressedStateChanged:
                        this.OnAfterCardCompressedStateChanged( e as AfterCardCompressedStateChangedEventArgs );
                        break;
                        // ----------------------------------------------------------------------------
					// SSP 12/15/04 - IDataErrorInfo Support
					//
                    case GridEventIds.BeforeDisplayDataErrorTooltip:
						this.OnBeforeDisplayDataErrorTooltip( e as BeforeDisplayDataErrorTooltipEventArgs );
						break;
					// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					//
					// --------------------------------------------------------------------------------
					case GridEventIds.AfterRowFixedStateChanged:
						this.OnAfterRowFixedStateChanged( e as AfterRowFixedStateChangedEventArgs );
						break;
					case GridEventIds.BeforeRowFixedStateChanged:
						this.OnBeforeRowFixedStateChanged( e as BeforeRowFixedStateChangedEventArgs );
						break;
					case GridEventIds.FilterCellValueChanged:
						this.OnFilterCellValueChanged( e as FilterCellValueChangedEventArgs );
						break;
					case GridEventIds.InitializeRowsCollection:
						this.OnInitializeRowsCollection( e as InitializeRowsCollectionEventArgs );
						break;
					// --------------------------------------------------------------------------------

					// JAS v5.2 DoubleClick Events 4/27/05
					// --------------------------------------------------------------------------------
					case GridEventIds.DoubleClickRow:
						this.OnDoubleClickRow( e as DoubleClickRowEventArgs );
						break;
					case GridEventIds.DoubleClickCell:
						this.OnDoubleClickCell( e as DoubleClickCellEventArgs );
						break;
					case GridEventIds.DoubleClickHeader:
						this.OnDoubleClickHeader( e as DoubleClickHeaderEventArgs );
						break;
					// --------------------------------------------------------------------------------
					// SSP 6/17/05 - NAS 5.3 Column Chooser
					//
					case GridEventIds.BeforeColumnChooserDisplayed:
						this.OnBeforeColumnChooserDisplayed( e as BeforeColumnChooserDisplayedEventArgs );
						break;
						// SSP 10/18/05 - NAS 5.3 Column Chooser
						// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
						// TestAdvantage.
						// 
						// ----------------------------------------------------------------------------
					case GridEventIds.BeforeBandHiddenChanged:
						this.OnBeforeBandHiddenChanged( e as BeforeBandHiddenChangedEventArgs );
						break;
					case GridEventIds.AfterBandHiddenChanged:
						this.OnAfterBandHiddenChanged( e as AfterBandHiddenChangedEventArgs );
						break;
						// ----------------------------------------------------------------------------
						// SSP 11/22/05 - NAS 6.1 Multi-cell Operations
						// 
					case GridEventIds.BeforeMultiCellOperation:
						this.OnBeforeMultiCellOperation( e as BeforeMultiCellOperationEventArgs );
						break;
                    
                    // MBS 2/29/08 - RowEditTemplate NA2008 V2
                    //
                    case GridEventIds.AfterRowEditTemplateClosed:
                        this.OnAfterRowEditTemplateClosed(e as AfterRowEditTemplateClosedEventArgs);
                        break;
                    //
                    case GridEventIds.AfterRowEditTemplateDisplayed:
                        this.OnAfterRowEditTemplateDisplayed(e as AfterRowEditTemplateDisplayedEventArgs);
                        break;
                    //
                    case GridEventIds.BeforeRowEditTemplateClosed:
                        this.OnBeforeRowEditTemplateClosed(e as BeforeRowEditTemplateClosedEventArgs);
                        break;
                    //
                    case GridEventIds.BeforeRowEditTemplateDisplayed:
                        this.OnBeforeRowEditTemplateDisplayed(e as BeforeRowEditTemplateDisplayedEventArgs);
                        break;
                    //
                    case GridEventIds.RowEditTemplateRequested:
                        this.OnRowEditTemplateRequested(e as RowEditTemplateRequestedEventArgs);
                        break;

                    // MBS 7/1/08 - NA2008 V3
                    case GridEventIds.ClickCell:
                        this.OnClickCell(e as ClickCellEventArgs);
                        break;

                    // CDS NAS v9.1 Header CheckBox
                    case GridEventIds.BeforeHeaderCheckStateChanged:
                        this.OnBeforeHeaderCheckBoxStateChanged(e as BeforeHeaderCheckStateChangedEventArgs);
                        break;

                    // CDS NAS v9.1 Header CheckBox
                    case GridEventIds.AfterHeaderCheckStateChanged:
                        this.OnAfterHeaderCheckBoxStateChanged(e as AfterHeaderCheckStateChangedEventArgs);
                        break;

					default:
						Debug.Assert(false, "Unhandled Id passed into UltraGrid.FireEvent");
						break;
				}
			}
			finally
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( id );
			}
		}

		/// <summary>
		/// Called before a group's position has changed
		/// </summary>
		internal protected override void FireBeforeGroupPosChanged ( BeforeGroupPosChangedEventArgs e )
		{
			this.FireEvent( GridEventIds.BeforeGroupPosChanged, e );
		}

		/// <summary>
		/// Called before a column's position has changed
		/// </summary>
		internal protected override void FireBeforeColPosChanged ( BeforeColPosChangedEventArgs e )
		{
			this.FireEvent( GridEventIds.BeforeColPosChanged, e );
		}

		/// <summary>
		/// Called after a group's position has changed
		/// </summary>
		/// <param name="e">EventArgs</param>
		internal protected override void FireAfterGroupPosChanged ( AfterGroupPosChangedEventArgs e )
		{
			this.FireEvent( GridEventIds.AfterGroupPosChanged, e );
		}
		
		/// <summary>
		/// Called after a column's position has changed
		/// </summary>
		/// <param name="e">EventArgs</param>
		internal protected override void FireAfterColPosChanged ( AfterColPosChangedEventArgs e )
		{
			this.FireEvent( GridEventIds.AfterColPosChanged, e );			
		}

		/// <summary>
		/// Called after row region scroll takes place
		/// </summary>
		internal protected override void FireAfterRowRegionScroll( RowScrollRegionEventArgs e )
		{
			this.FireEvent( GridEventIds.AfterRowRegionScroll, e );
		}

		/// <summary>
		/// Called before a column region scrolls
		/// </summary>
		internal protected override void FireBeforeColRegionScroll ( BeforeColRegionScrollEventArgs e )
		{
			this.FireEvent( GridEventIds.BeforeColRegionScroll, e );
		}

		/// <summary>
		/// Called before a row region scrolls
		/// </summary>
		internal protected override void FireBeforeRowRegionScroll ( BeforeRowRegionScrollEventArgs e )
		{
			this.FireEvent( GridEventIds.BeforeRowRegionScroll, e );			
		}

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// This was not working right. There was not entry on the EventManager for combo and Dropdown, so I rearranged all of this code using a new FireCommonEvent method
//		// SSP 11/21/03 UWG2749
//		// Override FireFilterRow so we can check to see if the event is enabled.
//		//
//#if DEBUG
//		/// <summary>
//		/// Fires the FilterRow event.
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//#endif
//		internal override void FireFilterRow( FilterRowEventArgs e )
//		{
//			if ( null == this.EventManager || this.EventManager.CanFireEvent( GridEventIds.FilterRow ) )
//				base.FireFilterRow( e );
//		}

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		#region FireCommonEvent
		internal override bool FireCommonEvent( CommonEventIds id, EventArgs e, bool checkInProgress )
		{
			GridEventIds resolvedEventID;

			switch (id)
			{
				case CommonEventIds.AfterSortChange:
					resolvedEventID = GridEventIds.AfterSortChange;
					break;
				case CommonEventIds.BeforeSortChange:
					// SSP 1/10/05 - BR01607
					// I'm assuming this was a typo.
					//
					//resolvedEventID = GridEventIds.AfterSortChange;
					resolvedEventID = GridEventIds.BeforeSortChange;
					break;
				case CommonEventIds.FilterRow:
					resolvedEventID = GridEventIds.FilterRow;
					break;
				case CommonEventIds.BeforeColPosChanged:
					resolvedEventID = GridEventIds.BeforeColPosChanged;
					break;
				case CommonEventIds.AfterColPosChanged:
					resolvedEventID = GridEventIds.AfterColPosChanged;
					break;
				case CommonEventIds.BeforeRowFilterDropDown:
					resolvedEventID = GridEventIds.BeforeRowFilterDropDown;
					break;
				case CommonEventIds.AfterRowFilterChanged:
					resolvedEventID = GridEventIds.AfterRowFilterChanged;
					break;
				case CommonEventIds.BeforeRowFilterChanged:
					resolvedEventID = GridEventIds.BeforeRowFilterChanged;
					break;
				case CommonEventIds.BeforeCustomRowFilterDialog:
					resolvedEventID = GridEventIds.BeforeCustomRowFilterDialog;
					break;
				case CommonEventIds.BeforeRowFilterDropDownPopulate:
					resolvedEventID = GridEventIds.BeforeRowFilterDropDownPopulate;
					break;
				// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				// Added FilterCellValueChanged and InitializeRowsCollection events.
				//
				// ----------------------------------------------------------------------------
				case CommonEventIds.FilterCellValueChanged:
					resolvedEventID = GridEventIds.FilterCellValueChanged;
					break;
				case CommonEventIds.InitializeRowsCollection:
					resolvedEventID = GridEventIds.InitializeRowsCollection;
					break;
				// ----------------------------------------------------------------------------
				// SSP 6/17/05 - NAS 5.3 Column Chooser
				//
				case CommonEventIds.BeforeColumnChooserDisplayed:
					resolvedEventID = GridEventIds.BeforeColumnChooserDisplayed;
					break;
					// SSP 10/18/05 - NAS 5.3 Column Chooser
					// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
					// TestAdvantage.
					// 
					// ----------------------------------------------------------------------------
				case CommonEventIds.BeforeBandHiddenChanged:
					resolvedEventID = GridEventIds.BeforeBandHiddenChanged;
					break;
				case CommonEventIds.AfterBandHiddenChanged:
					resolvedEventID = GridEventIds.AfterBandHiddenChanged;
					break;
					// ----------------------------------------------------------------------------
                // CDS 02/02/08 TFS12512 - NAS v9.1 Header CheckBox - Moved these events to the UltraGridBase class, so firing them using CommonEvents.
                case CommonEventIds.BeforeHeaderCheckStateChanged:
                    resolvedEventID = GridEventIds.BeforeHeaderCheckStateChanged;
                    break;
                case CommonEventIds.AfterHeaderCheckStateChanged:
                    resolvedEventID = GridEventIds.AfterHeaderCheckStateChanged;
                    break;

				default:
					Debug.Assert(false, "Unhandled Id passed into UltraGrid.FireCommonEvent");
					return false;			
			}

			if (checkInProgress && 
				this.EventManager.InProgress(resolvedEventID))
			{
				return false;
			}

			this.FireEvent(resolvedEventID, e);
			return true;
		}
		#endregion FireCommonEvent

		/// <summary>
		/// Called when a row is initialized
		/// </summary>
		internal protected override void FireInitializeRow( InitializeRowEventArgs e )
		{
			// SSP 1/14/03 UWG1892
			// Added retainRowPropertyCategories parameter to Print and PrintPreview methods for
			// making WYSIWIG style printing. Before firing InitializeRow, copy the row properties.
			//
			// SSP 5/7/03 - Export Functionality
			// Also retain properties when exporting.
			//
			//if ( null != e.Row && null != e.Row.Layout && e.Row.Layout.IsPrintLayout )
			UltraGridRow row = e.Row;
			UltraGridLayout layout = null != row ? row.Layout : null;
			if ( null != layout && ( layout.IsPrintLayout || layout.IsExportLayout ) )
				this.CopyPropertiesToPrintRow( row );

			this.FireEvent( GridEventIds.InitializeRow, e );
		}

		/// <summary>
		/// Called when a row is initialized
		/// </summary>
		internal void FireInitializeGroupRow( InitializeGroupByRowEventArgs e )
		{
			// SSP 1/14/03 UWG1892
			// Added retainRowPropertyCategories parameter to Print and PrintPreview methods for
			// making WYSIWIG style printing. Before firing InitializeRow, copy the row properties.
			//
			// SSP 5/7/03 - Export Functionality
			//
			//if ( null != e.Row && null != e.Row.Layout && e.Row.Layout.IsPrintLayout )
			if ( null != e.Row && null != e.Row.Layout && ( e.Row.Layout.IsPrintLayout || e.Row.Layout.IsExportLayout ) )
				this.CopyPropertiesToPrintRow( e.Row );

			this.FireEvent( GridEventIds.InitializeGroupByRow, e );
		}

		/// <summary>
		/// Called when the layout is first initialized after the 
		/// datasource has been set 
		/// </summary>
		internal protected override void FireInitializeLayout( InitializeLayoutEventArgs e )
		{
         
			// JJD 12/03/01 - UWG813
			// Call base to maintain flag so we know if event was fired
			//
			base.FireInitializeLayout( e );

			this.FireEvent( GridEventIds.InitializeLayout, e );			
		}


		#endregion

		
		#region Mouse events

		

		/// <summary>
		/// Called when an element is entered (the mouse is moved
		/// over the element)
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnMouseEnterElement( UIElementEventArgs e )
		{
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( GridEventIds.MouseEnterElement ) )
				return;

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( GridEventIds.MouseEnterElement );

			try
			{
				// fire the event
				//
				base.OnMouseEnterElement( e );
			}
			catch(Exception)
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( GridEventIds.MouseEnterElement );

				// re-throw the error
				//
				throw;
			}

			// decrement the inProgress count
			//
			this.EventManager.DecrementInProgress( GridEventIds.MouseEnterElement );
		}

		/// <summary>
		/// Called when an element is left (the mouse is moved
		/// off the element)
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnMouseLeaveElement( UIElementEventArgs e )
		{
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( GridEventIds.MouseLeaveElement ) )
				return;

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( GridEventIds.MouseLeaveElement );

			try
			{
				// fire the event
				//
				base.OnMouseLeaveElement( e );
			}
			catch(Exception)
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( GridEventIds.MouseLeaveElement );

				// re-throw the error
				//
				throw;
			}

			// decrement the inProgress count
			//
			this.EventManager.DecrementInProgress( GridEventIds.MouseLeaveElement );
		}

		#endregion

		#region Before Events

		/// <summary>
		/// Called before a cell is activated
		/// </summary>
		virtual protected void OnBeforeCellActivate( CancelableCellEventArgs e )
		{
			CancelableCellEventHandler eDelegate = (CancelableCellEventHandler)EventsOptimized[EVENT_BEFORECELLACTIVATE];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		/// <summary>
		/// Called before a column region is moved
		/// </summary>
		virtual protected void OnBeforeColRegionRemoved ( BeforeColRegionRemovedEventArgs e )
		{
			BeforeColRegionRemovedEventHandler eDelegate = (BeforeColRegionRemovedEventHandler)EventsOptimized[EVENT_BEFORECOLREGIONREMOVED];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}

		/// <summary>
		/// Called before a column region splits
		/// </summary>
		virtual protected void OnBeforeColRegionSplit ( BeforeColRegionSplitEventArgs e )
		{
			BeforeColRegionSplitEventHandler eDelegate = (BeforeColRegionSplitEventHandler)EventsOptimized[EVENT_BEFORECOLREGIONSPLIT];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}


		/// <summary>
		/// Called before a row region is removed
		/// </summary>
		virtual protected void OnBeforeRowRegionRemoved ( BeforeRowRegionRemovedEventArgs e )
		{
			BeforeRowRegionRemovedEventHandler eDelegate = (BeforeRowRegionRemovedEventHandler)EventsOptimized[EVENT_BEFOREROWREGIONREMOVED];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}


		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		/// <summary>
//		/// Called before sort change
//		/// </summary>
//		virtual protected void OnBeforeSortChange ( BeforeSortChangeEventArgs e )
//		{
//			BeforeSortChangeEventHandler eDelegate = (BeforeSortChangeEventHandler)EventsOptimized[EVENT_BEFORESORTCHANGE];
//
//			if ( eDelegate != null )
//				eDelegate( this, e );
//		}

		/// <summary>
		/// Called before a row is inserted
		/// </summary>
		virtual protected void OnBeforeRowInsert ( BeforeRowInsertEventArgs e )
		{
			BeforeRowInsertEventHandler eDelegate = (BeforeRowInsertEventHandler)EventsOptimized[EVENT_BEFOREROWINSERT];

			if ( eDelegate != null )
				eDelegate( this, e );
		}


		/// <summary>
		/// Called before a cell is updated
		/// </summary>
		virtual protected void OnBeforeCellUpdate ( BeforeCellUpdateEventArgs e )
		{
			BeforeCellUpdateEventHandler eDelegate = (BeforeCellUpdateEventHandler)EventsOptimized[EVENT_BEFORECELLUPDATE];

			if ( eDelegate != null )
				eDelegate( this, e );
			
		}


		/// <summary>
		/// Called before a row is resized
		/// </summary>
		virtual protected void OnBeforeRowResize ( BeforeRowResizeEventArgs e )
		{
			BeforeRowResizeEventHandler eDelegate = (BeforeRowResizeEventHandler)EventsOptimized[EVENT_BEFOREROWRESIZE];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}

		/// <summary>
		/// Called before row(s) are deleted
		/// </summary>
		virtual protected void OnBeforeRowsDeleted ( BeforeRowsDeletedEventArgs e )
		{
			BeforeRowsDeletedEventHandler eDelegate = (BeforeRowsDeletedEventHandler)EventsOptimized[EVENT_BEFOREROWSDELETED];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}




		/// <summary>
		/// Called before a row region is resized
		/// </summary>
		virtual protected void OnBeforeRowRegionSize ( BeforeRowRegionSizeEventArgs e )
		{
			BeforeRowRegionSizeEventHandler eDelegate = (BeforeRowRegionSizeEventHandler)EventsOptimized[EVENT_BEFOREROWREGIONSIZE];

			if ( eDelegate != null )
				eDelegate( this, e );				
		}

		/// <summary>
		/// Called before a row region is split
		/// </summary>
		virtual protected void OnBeforeRowRegionSplit ( BeforeRowRegionSplitEventArgs e )
		{
			BeforeRowRegionSplitEventHandler eDelegate = (BeforeRowRegionSplitEventHandler)EventsOptimized[EVENT_BEFOREROWREGIONSPLIT];

			if ( eDelegate != null )
				eDelegate( this, e );	
		}


		/// <summary>
		/// Called before a column region size
		/// </summary>
		virtual protected void OnBeforeColRegionSize ( BeforeColRegionSizeEventArgs e )
		{
			BeforeColRegionSizeEventHandler eDelegate = (BeforeColRegionSizeEventHandler)EventsOptimized[EVENT_BEFORECOLREGIONSIZE];

			if ( eDelegate != null )
				eDelegate( this, e );
			
		}


		/// <summary>
		/// Called before auto size edit
		/// </summary>
		virtual protected void OnBeforeAutoSizeEdit ( CancelableAutoSizeEditEventArgs e )
		{
			CancelableAutoSizeEditEventHandler eDelegate = (CancelableAutoSizeEditEventHandler)EventsOptimized[EVENT_BEFOREAUTOSIZEEDIT];

			if ( eDelegate != null )
				eDelegate( this, e );
		}




		/// <summary>
		/// Called before a row has expanded
		/// </summary>
		virtual protected void OnBeforeRowExpanded ( CancelableRowEventArgs e )
		{
			CancelableRowEventHandler eDelegate = (CancelableRowEventHandler)EventsOptimized[EVENT_BEFOREROWEXPANDED];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}

		/// <summary>
		/// Called before a row is updated
		/// </summary>
		virtual protected void OnBeforeRowUpdate ( CancelableRowEventArgs e )
		{
			CancelableRowEventHandler eDelegate = (CancelableRowEventHandler)EventsOptimized[EVENT_BEFOREROWUPDATE];

			if ( eDelegate != null )
				eDelegate( this, e );				
		}

		/// <summary>
		/// Called before a row update cancel (OnBeforeRowCancelUpdate)
		/// </summary>
		virtual protected void OnBeforeRowCancelUpdate ( CancelableRowEventArgs e )
		{			
			CancelableRowEventHandler eDelegate = (CancelableRowEventHandler)EventsOptimized[EVENT_BEFOREROWCANCELUPDATE];

			if ( eDelegate != null )
				eDelegate( this, e );				
		}


		/// <summary>
		/// Called before a row has collapsed
		/// </summary>
		virtual protected void OnBeforeRowCollapsed ( CancelableRowEventArgs e )
		{
			CancelableRowEventHandler eDelegate = (CancelableRowEventHandler)EventsOptimized[EVENT_BEFOREROWCOLLAPSED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}


		/// <summary>
		/// Called before SelectChange
		/// </summary>
		virtual protected void OnBeforeSelectChange ( BeforeSelectChangeEventArgs e )
		{
			BeforeSelectChangeEventHandler eDelegate = (BeforeSelectChangeEventHandler)EventsOptimized[EVENT_BEFORESELECTCHANGE];

			if ( eDelegate != null )
				eDelegate( this, e );				
		}

		/// <summary>
		/// Called before a row is activated
		/// </summary>
		virtual protected void OnBeforeRowActivate ( RowEventArgs e )
		{
			RowEventHandler eDelegate = (RowEventHandler)EventsOptimized[EVENT_BEFOREROWACTIVATE];

			if ( eDelegate != null )
				eDelegate( this, e );
			
		}
		
		/// <summary>
		/// Called before an update to a cell is canceled
		/// </summary>
		virtual protected void OnBeforeCellCancelUpdate( CancelableCellEventArgs e )
		{
			CancelableCellEventHandler eDelegate = (CancelableCellEventHandler)EventsOptimized[EVENT_BEFORECELLCANCELUPDATE];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}


		/// <summary>
		/// Called before a cell list is dropped down
		/// </summary>
		virtual protected void OnBeforeCellListDropDown( CancelableCellEventArgs e )
		{
			CancelableCellEventHandler eDelegate = (CancelableCellEventHandler)EventsOptimized[EVENT_BEFORECELLLISTDROPDOWN];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		/// <summary>
		/// Called before document is sent to the printer
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnBeforePrint( CancelablePrintEventArgs e )
		{			
			BeforePrintEventHandler eDelegate = (BeforePrintEventHandler)EventsOptimized[EVENT_BEFOREPRINT];

			if ( eDelegate != null )
				eDelegate( this, e );
			
		}


		/// <summary>
		/// Called before a cell is deactivated
		/// </summary>
		virtual protected void OnBeforeCellDeactivate ( CancelEventArgs e )
		{
			CancelEventHandler eDelegate = (CancelEventHandler)EventsOptimized[EVENT_BEFORECELLDEACTIVATE];

			if ( eDelegate != null )
				eDelegate( this, e );
			
		}

		/// <summary>
		/// Called before entering edit mode
		/// </summary>
		virtual protected void OnBeforeEnterEditMode ( CancelEventArgs e )
		{
			CancelEventHandler eDelegate = (CancelEventHandler)EventsOptimized[EVENT_BEFOREENTEREDITMODE];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}


		/// <summary>
		/// Called before a row is deactivated
		/// </summary>
		virtual protected void OnBeforeRowDeactivate ( CancelEventArgs e )
		{
			CancelEventHandler eDelegate = (CancelEventHandler)EventsOptimized[EVENT_BEFOREROWDEACTIVATE];

			if ( eDelegate != null )
				eDelegate( this, e );
			
		}

		// SSP 5/2/02
		// Changed the event args of BeforeExitEditMode from CancelEventArgs to
		// BeforeExitEditModeEventArgs
		//
		/// <summary>
		/// Called before exiting edit mode
		/// </summary>
		protected virtual void OnBeforeExitEditMode( BeforeExitEditModeEventArgs e )
		{
			BeforeExitEditModeEventHandler eDelegate = (BeforeExitEditModeEventHandler)EventsOptimized[EVENT_BEFOREEXITEDITMODE];

			if ( eDelegate != null )
				eDelegate( this, e );		
		}
		

		/// <summary>
		/// Called before a column region scrolls
		/// </summary>
		virtual protected void OnBeforeColRegionScroll ( BeforeColRegionScrollEventArgs e )
		{
			BeforeColRegionScrollEventHandler eDelegate = (BeforeColRegionScrollEventHandler)EventsOptimized[EVENT_BEFORECOLREGIONSCROLL];

			if ( eDelegate != null )
				eDelegate( this, e );		
		}
		
		

		/// <summary>
		/// Called before row region scroll takes place
		/// </summary>
		virtual protected void OnBeforeRowRegionScroll( BeforeRowRegionScrollEventArgs e  )
		{
			BeforeRowRegionScrollEventHandler eDelegate = (BeforeRowRegionScrollEventHandler)EventsOptimized[EVENT_BEFOREROWREGIONSCROLL];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		/// <summary>
//		/// Called before a column postion changes
//		/// </summary>
//		virtual protected void OnBeforeColPosChanged ( BeforeColPosChangedEventArgs e )
//		{
//			BeforeColPosChangedEventHandler eDelegate = (BeforeColPosChangedEventHandler)EventsOptimized[EVENT_BEFORECOLPOSCHANGED];
//
//			if ( eDelegate != null )
//				eDelegate( this, e );
//		}



		/// <summary>
		/// Called before a group position has changed
		/// </summary>
		virtual protected void OnBeforeGroupPosChanged ( BeforeGroupPosChangedEventArgs e )
		{
			BeforeGroupPosChangedEventHandler eDelegate = (BeforeGroupPosChangedEventHandler)EventsOptimized[EVENT_BEFOREGROUPPOSCHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		// SSP 6/25/03 UWG2413 - Row Layout Functionality
		// Added events that we need to fire when a cell or a header is resized in the row-layout mode.
		// Added OnBeforeRowLayoutItemResized method.
		//
		/// <summary>
		/// Fires BeforeRowLayoutItemResized event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		/// <remarks>
		/// <p><seealso cref="BeforeRowLayoutItemResized"/></p>
		/// </remarks>
		protected virtual void OnBeforeRowLayoutItemResized( BeforeRowLayoutItemResizedEventArgs e )
		{
			BeforeRowLayoutItemResizedEventHandler eDelegate = (BeforeRowLayoutItemResizedEventHandler)EventsOptimized[EVENT_BEFOREROWLAYOUTITEMRESIZED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		// SSP 7/30/03 UWG2544
		// Added BeforePerformAction and AfterPerformAction events.
		//
		/// <summary>
		/// Called before a key action is about to be performed. It fires BeforeUltraGridPerformAction event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		/// <remarks>
		/// <seealso cref="PerformAction(UltraGridAction)"/>
		/// </remarks>
		protected virtual void OnBeforePerformAction( BeforeUltraGridPerformActionEventArgs e )
		{
			BeforeUltraGridPerformActionEventHandler eDelegate = (BeforeUltraGridPerformActionEventHandler)EventsOptimized[EVENT_BEFOREPERFORMACTION];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		// SSP 8/1/03
//		// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
//		//
//		/// <summary>
//		/// OnBeforeRowFilterChanged gets called when the user modifies row filters for a column. This method fires the associated event.
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected virtual void OnBeforeRowFilterChanged( BeforeRowFilterChangedEventArgs e )
//		{
//			BeforeRowFilterChangedEventHandler eDelegate = (BeforeRowFilterChangedEventHandler)EventsOptimized[EVENT_BEFOREROWFILTERCHANGED];
//
//			if ( eDelegate != null )
//				eDelegate( this, e );
//		}

        // JDN 11/17/04 CardCompressedStateChanged events
        /// <summary>
        /// OnBeforeCardCompressedStateChanged gets called before a Card Row is Expanded or Compressed. This method fires the associated event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        protected virtual void OnBeforeCardCompressedStateChanged( BeforeCardCompressedStateChangedEventArgs e )
        {
            BeforeCardCompressedStateChangedEventHandler  eDelegate = (BeforeCardCompressedStateChangedEventHandler)EventsOptimized[EVENT_BEFORECARDCOMPRESSEDSTATECHANGED];

            if ( eDelegate != null )
                eDelegate( this, e );
        }

		// SSP 12/15/04 - IDataErrorInfo Support
		//
		/// <summary>
		/// Fires BeforeDisplayDataErrorTooltip event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnBeforeDisplayDataErrorTooltip( BeforeDisplayDataErrorTooltipEventArgs e )
		{
			BeforeDisplayDataErrorTooltipEventHandler eDelegate = (BeforeDisplayDataErrorTooltipEventHandler)EventsOptimized[ EVENT_BEFOREDISPLAYDATAERRORTOOLTIP ];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

        // MBS 2/29/08 - RowEditTemplate NA2008 V2
        //
        #region OnBeforeRowEditTemplateClosed

        /// <summary>
        /// Fires the BeforeRowEditTemplateClosed event.
        /// </summary>
        /// <param name="e">A <see cref="BeforeRowEditTemplateClosedEventArgs"/> that contains the event data.</param>
        protected virtual void OnBeforeRowEditTemplateClosed(BeforeRowEditTemplateClosedEventArgs e)
        {
            BeforeRowEditTemplateClosedEventHandler eDelegate = (BeforeRowEditTemplateClosedEventHandler)EventsOptimized[EVENT_BEFOREROWEDITTEMPLATECLOSED];
            if (eDelegate != null)
                eDelegate(this, e);
        }
        #endregion //OnBeforeRowEditTemplateClosed
        //
        #region OnBeforeRowEditTemplateDisplayed

        /// <summary>
        /// Fires the BeforeRowEditTemplateDisplayed event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        protected virtual void OnBeforeRowEditTemplateDisplayed(BeforeRowEditTemplateDisplayedEventArgs e)
        {
            BeforeRowEditTemplateDisplayedEventHandler eDelegate = (BeforeRowEditTemplateDisplayedEventHandler)EventsOptimized[EVENT_BEFOREROWEDITTEMPLATEDISPLAYED];
            if (eDelegate != null)
                eDelegate(this, e);
        }
        #endregion //OnBeforeRowEditTemplateDisplayed
        //
        #region OnRowEditTemplateRequested

        /// <summary>
        /// Fires the RowEditTemplateRequested event.
        /// </summary>
        /// <param name="e">A <see cref="RowEditTemplateRequestedEventArgs"/> that contains the event data.</param>
        protected virtual void OnRowEditTemplateRequested(RowEditTemplateRequestedEventArgs e)
        {
            RowEditTemplateRequestedEventHandler eDelegate = (RowEditTemplateRequestedEventHandler)EventsOptimized[EVENT_ROWEDITTEMPLATEREQUESTED];
            if (eDelegate != null)
                eDelegate(this, e);
        }
        #endregion //OnRowEditTemplateRequested

        #endregion

        #region After Events

        // SSP 12/5/03 UWG2295
		// Added AfterCardsScroll event.
		//
		/// <summary>
		/// Called when the user scrolls cards in a card-area. This method fires <see cref="UltraGrid.AfterCardsScroll"/> event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnAfterCardsScroll( AfterCardsScrollEventArgs e )
		{
			AfterCardsScrollEventHandler eDelegate = (AfterCardsScrollEventHandler)EventsOptimized[ EVENT_AFTERCARDSSCROLL ];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		// SSP 12/5/03 UWG2295
		// Added AfterSummaryDialog event.
		// 
		/// <summary>
		/// Called after the user closes the summary dialog. This method fires <see cref="UltraGrid.AfterSummaryDialog"/> event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnAfterSummaryDialog( AfterSummaryDialogEventArgs e )
		{
			AfterSummaryDialogEventHandler eDelegate = (AfterSummaryDialogEventHandler)EventsOptimized[ EVENT_AFTERSUMMARYDIALOG ];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		/// <summary>
		/// Called after a group postion has changed
		/// </summary>
		/// <param name="e">EventArgs</param>
		virtual protected void OnAfterGroupPosChanged ( AfterGroupPosChangedEventArgs e )
		{
			AfterGroupPosChangedEventHandler eDelegate = (AfterGroupPosChangedEventHandler)EventsOptimized[EVENT_AFTERGROUPPOSCHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );		
		}

		/// <summary>
		/// Called after SelectChange
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnAfterSelectChange ( AfterSelectChangeEventArgs e )
		{
			AfterSelectChangeEventHandler eDelegate = (AfterSelectChangeEventHandler)EventsOptimized[EVENT_AFTERSELECTCHANGE];

			if ( eDelegate != null )
				eDelegate( this, e );
			
		}

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		/// <summary>
//		/// Called after a band has been sorted
//		/// </summary>
//		virtual protected void OnAfterSortChange ( BandEventArgs e )
//		{
//			BandEventHandler eDelegate = (BandEventHandler)EventsOptimized[EVENT_AFTERSORTCHANGE];
//
//			if ( eDelegate != null )
//				eDelegate( this, e );			
//		}
	

		/// <summary>
		/// Called after a row cancel update takes place
		/// </summary>
		virtual protected void OnAfterRowCancelUpdate ( RowEventArgs e )
		{
			RowEventHandler eDelegate = (RowEventHandler)EventsOptimized[EVENT_AFTERROWCANCELUPDATE];

			if ( eDelegate != null )
				eDelegate( this, e );				
		}	



		/// <summary>
		/// Called after a exiting edit mode
		/// </summary>
		virtual protected void OnAfterExitEditMode( )
		{
			EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTEREXITEDITMODE];

			if ( eDelegate != null )
				eDelegate( this, new System.EventArgs() );				
		}
		

		/// <summary>
		/// Called after a row has been update
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnAfterRowUpdate ( RowEventArgs e )
		{
			RowEventHandler eDelegate = (RowEventHandler)EventsOptimized[EVENT_AFTERROWUPDATE];

			if ( eDelegate != null )
				eDelegate( this, e );
		}


		/// <summary>
		/// Called after a row has been resized
		/// </summary>
		virtual protected void OnAfterRowResize ( RowEventArgs e )
		{
			RowEventHandler eDelegate = (RowEventHandler)EventsOptimized[EVENT_AFTERROWRESIZE];

			if ( eDelegate != null )
				eDelegate( this, e );		
		}



		/// <summary>
		/// Called after a row has been inserted
		/// </summary>
		virtual protected void OnAfterRowInsert ( RowEventArgs e )
		{
			RowEventHandler eDelegate = (RowEventHandler)EventsOptimized[EVENT_AFTERROWINSERT];

			if ( eDelegate != null )
				eDelegate( this, e );
			
		}



		/// <summary>
		/// Called after a row has expanded
		/// </summary>
		virtual protected void OnAfterRowExpanded ( RowEventArgs e )
		{
			RowEventHandler eDelegate = (RowEventHandler)EventsOptimized[EVENT_AFTERROWEXPANDED];

			if ( eDelegate != null )
				eDelegate( this, e );	
			
		}


		/// <summary>
		/// Called after a row has collapsed
		/// </summary>
		virtual protected void OnAfterRowCollapsed ( RowEventArgs e )
		{
			RowEventHandler eDelegate = (RowEventHandler)EventsOptimized[EVENT_AFTERROWCOLLAPSED];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}


		/// <summary>
		/// Called after rows have benn deleted
		/// </summary>
		virtual protected void OnAfterRowsDeleted( )
		{			
			EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERROWSDELETED];

			if ( eDelegate != null )
				eDelegate( this, new EventArgs() );	
			
		}


		/// <summary>
		/// Called after a row is activated
		/// </summary>		
		virtual protected void OnAfterRowActivate( )
		{
			
			EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERROWACTIVATE];

			if ( eDelegate != null )
				eDelegate( this, new EventArgs() );	
			
		}

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		/// <summary>
//		/// Called after a column postion has changed
//		/// </summary>
//		/// <param name="e">EventArgs</param>
//		virtual protected void OnAfterColPosChanged ( AfterColPosChangedEventArgs e )
//		{
//			AfterColPosChangedEventHandler eDelegate = (AfterColPosChangedEventHandler)EventsOptimized[EVENT_AFTERCOLPOSCHANGED];
//
//			if ( eDelegate != null )
//				eDelegate( this, e );		
//		}

		/// <summary>
		/// Called after a entering edit mode
		/// </summary>		
		virtual protected void OnAfterEnterEditMode( )
		{			
			EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERENTEREDITMODE];

			if ( eDelegate != null )
				eDelegate( this, new System.EventArgs() );					
		}


		/// <summary>
		/// Called after a cell has been activated
		/// </summary>
		virtual protected void OnAfterCellActivate( )
		{
			System.EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERCELLACTIVATE];

			if ( eDelegate != null )
				eDelegate( this, new System.EventArgs() );			
		}


		/// <summary>
		/// Called after a CellList is closed up
		/// </summary>
		/// <param name="e">EventArgs</param>
		virtual protected void OnAfterCellListCloseUp ( CellEventArgs e )
		{
			CellEventHandler eDelegate = (CellEventHandler)EventsOptimized[EVENT_AFTERCELLLISTCLOSEUP];

			if ( eDelegate != null )
				eDelegate( this, e );		
		}

		/// <summary>
		/// Called after a cell cancel update
		/// </summary>
		/// <param name="e">EventArgs</param>
		virtual protected void OnAfterCellCancelUpdate ( CellEventArgs e )
		{
			CellEventHandler eDelegate = (CellEventHandler)EventsOptimized[EVENT_AFTERCELLCANCELUPDATE];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}

		/// <summary>
		/// Called after a cell has been updated
		/// </summary>
		/// <param name="e">EventArgs</param>
		virtual protected void OnAfterCellUpdate ( CellEventArgs e )
		{
			CellEventHandler eDelegate = (CellEventHandler)EventsOptimized[EVENT_AFTERCELLUPDATE];

			if ( eDelegate != null )
				eDelegate( this, e );		
			
		}


		/// <summary>
		/// Called after a column region is sized
		/// </summary> 
		/// <param name="e">EventArgs</param>
		virtual protected void OnAfterColRegionSize( ColScrollRegionEventArgs e )
		{
			ColScrollRegionEventHandler eDelegate = (ColScrollRegionEventHandler)EventsOptimized[EVENT_AFTERCOLREGIONSIZE];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}

		
		/// <summary>
		/// Called after row region is sized
		/// </summary>
		virtual protected void OnAfterRowRegionSize( RowScrollRegionEventArgs e )
		{
			RowScrollRegionEventHandler eDelegate = (RowScrollRegionEventHandler)EventsOptimized[EVENT_AFTERROWREGIONSIZE];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		/// <summary>
		/// Called after row region is scrolled
		/// </summary>
		virtual protected void OnAfterRowRegionScroll ( RowScrollRegionEventArgs e )
		{
			RowScrollRegionEventHandler eDelegate = (RowScrollRegionEventHandler)EventsOptimized[EVENT_AFTERROWREGIONSCROLL];

			if ( eDelegate != null )
				eDelegate( this, e );
		}



	
		/// <summary>
		/// Called after col region scroll takes place
		/// </summary>
		/// <param name="e">EventArgs</param>
		virtual protected void OnAfterColRegionScroll( ColScrollRegionEventArgs e )
		{
			ColScrollRegionEventHandler eDelegate = (ColScrollRegionEventHandler)EventsOptimized[EVENT_AFTERCOLREGIONSCROLL];

			if ( eDelegate != null )
				eDelegate( this, e );		
		}

		// SSP 6/25/03 UWG2413 - Row Layout Functionality
		// Added events that we need to fire when a cell or a header is resized in the row-layout mode.
		// Added OnBeforeRowLayoutItemResized method.
		//
		/// <summary>
		/// Fires OnAfterRowLayoutItemResized event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		/// <remarks>
		/// <p><seealso cref="AfterRowLayoutItemResized"/></p>
		/// </remarks>
		protected virtual void OnAfterRowLayoutItemResized( AfterRowLayoutItemResizedEventArgs e )
		{
			AfterRowLayoutItemResizedEventHandler eDelegate = (AfterRowLayoutItemResizedEventHandler)EventsOptimized[EVENT_AFTERROWLAYOUTITEMRESIZED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		// SSP 7/30/03 UWG2544
		// Added BeforePerformAction and AfterPerformAction events.
		//
		/// <summary>
		/// Called after a key action is performed. It fires AfterUltraGridPerformAction event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		/// <remarks>
		/// <seealso cref="UltraGrid.PerformAction(UltraGridAction)"/>
		/// </remarks>
		protected virtual void OnAfterPerformAction( AfterUltraGridPerformActionEventArgs e )
		{
			AfterUltraGridPerformActionEventHandler eDelegate = (AfterUltraGridPerformActionEventHandler)EventsOptimized[EVENT_AFTERPERFORMACTION];

			if ( eDelegate != null )
				eDelegate( this, e );
		}


		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
//		// SSP 8/1/03
//		// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
//		//
//		/// <summary>
//		/// OnAfterRowFilterChanged gets after the user has modified row filters for a column. This method fires the associated event.
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected virtual void OnAfterRowFilterChanged( AfterRowFilterChangedEventArgs e )
//		{
//			AfterRowFilterChangedEventHandler eDelegate = (AfterRowFilterChangedEventHandler)EventsOptimized[EVENT_AFTERROWFILTERCHANGED];
//
//			if ( eDelegate != null )
//				eDelegate( this, e );
//		}

        // JDN 11/17/04 CardCompressedStateChanged events
        /// <summary>
        /// OnAfterCardCompressedStateChanged gets called after a Card Row is Expanded or Compressed. This method fires the associated event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        protected virtual void OnAfterCardCompressedStateChanged( AfterCardCompressedStateChangedEventArgs e )
        {
            AfterCardCompressedStateChangedEventHandler eDelegate = (AfterCardCompressedStateChangedEventHandler)EventsOptimized[EVENT_AFTERCARDCOMPRESSEDSTATECHANGED];

            if ( eDelegate != null )
                eDelegate( this, e );
        }

		#region OnAfterRowFixedStateChanged

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		/// <summary>
		/// OnAfterRowFixedStateChanged is called after the user fixes or unfixes a row. This method fires the <see cref="UltraGrid.AfterRowFixedStateChanged"/> event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnAfterRowFixedStateChanged( AfterRowFixedStateChangedEventArgs e )
		{
			AfterRowFixedStateChangedEventHandler eDelegate = (AfterRowFixedStateChangedEventHandler)EventsOptimized[EVENT_AFTERROWFIXEDSTATECHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		#endregion OnAfterRowFixedStateChanged

        // MBS 2/29/08 - RowEditTemplate NA2008 V2
        //
        #region OnAfterRowEditTemplateClosed

        /// <summary>
        /// Fires the AfterRowEditTemplateClosed event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        protected virtual void OnAfterRowEditTemplateClosed(AfterRowEditTemplateClosedEventArgs e)
        {
            AfterRowEditTemplateClosedEventHandler eDelegate = (AfterRowEditTemplateClosedEventHandler)EventsOptimized[EVENT_AFTERROWEDITTEMPLATECLOSED];
            if (eDelegate != null)
                eDelegate(this, e);
        }
        #endregion //OnAfterRowEditTemplateClosed
        //
        #region OnAfterRowEditTemplateDisplayed

        /// <summary>
        /// Fires the AfterRowEditTemplateDisplayed event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        protected virtual void OnAfterRowEditTemplateDisplayed(AfterRowEditTemplateDisplayedEventArgs e)
        {
            AfterRowEditTemplateDisplayedEventHandler eDelegate = (AfterRowEditTemplateDisplayedEventHandler)EventsOptimized[EVENT_AFTERROWEDITTEMPLATEDISPLAYED];
            if (eDelegate != null)
                eDelegate(this, e);
        }
        #endregion //OnAfterRowEditTemplateDisplayed

        #region OnBeforeRowFixedStateChanged

        // SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		/// <summary>
		/// OnAfterRowFixedStateChanged is called before the user fixes or unfixes a row. This method fires the <see cref="UltraGrid.BeforeRowFixedStateChanged"/> event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnBeforeRowFixedStateChanged( BeforeRowFixedStateChangedEventArgs e )
		{
			BeforeRowFixedStateChangedEventHandler eDelegate = (BeforeRowFixedStateChangedEventHandler)EventsOptimized[EVENT_BEFOREROWFIXEDSTATECHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		#endregion OnBeforeRowFixedStateChanged

		#region OnBeforeMultiCellOperation

		// SSP 11/22/05 - NAS 6.1 Multi-cell Operations
		// 
		/// <summary>
		/// OnAfterRowFixedStateChanged is called before the user fixes or unfixes a row. This method fires the <see cref="UltraGrid.BeforeRowFixedStateChanged"/> event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnBeforeMultiCellOperation( BeforeMultiCellOperationEventArgs e )
		{
			BeforeMultiCellOperationEventHandler eDelegate = (BeforeMultiCellOperationEventHandler)EventsOptimized[ EVENT_BEFOREMULTICELLOPERATION ];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		#endregion OnBeforeMultiCellOperation

        // MBS 7/1/08 - NA2008 V3
        #region OnClickCell

        /// <summary>
        /// Fires the OnClickCell event.
        /// </summary>
        /// <param name="e">The event args containing the event information.</param>
        protected virtual void OnClickCell(ClickCellEventArgs e)
        {
            ClickCellEventHandler eDelegate = (ClickCellEventHandler)EventsOptimized[EVENT_CLICKCELL];
            if (eDelegate != null)
                eDelegate(this, e);
        }
        #endregion //OnClickCell

        // CDS 02/02/09 TFS12512 - Moved to the UltraGridBase class for UltraCombo support.
        //// CDS NAS v9.1 Header CheckBox
        //#region OnBeforeHeaderCheckBoxStateChanged

        ///// <summary>
        ///// Fires the BeforeHeaderCheckBoxStateChanged event.
        ///// </summary>
        ///// <param name="e">The event args containing the event information.</param>
        //protected virtual void OnBeforeHeaderCheckBoxStateChanged(BeforeHeaderCheckStateChangedEventArgs e)
        //{
        //    BeforeHeaderCheckStateChangedEventHandler eDelegate = (BeforeHeaderCheckStateChangedEventHandler)EventsOptimized[EVENT_BEFOREHEADERCHECKSTATECHANGED];
        //    if (eDelegate != null)
        //        eDelegate(this, e);
        //}

        //#endregion OnBeforeHeaderCheckBoxStateChanged

        //// CDS NAS v9.1 Header CheckBox
        //#region OnAfterHeaderCheckBoxStateChanged

        ///// <summary>
        ///// Fires the AfterHeaderCheckBoxStateChanged event.
        ///// </summary>
        ///// <param name="e">The event args containing the event information.</param>
        //protected virtual void OnAfterHeaderCheckBoxStateChanged(AfterHeaderCheckStateChangedEventArgs e)
        //{
        //    AfterHeaderCheckStateChangedEventHandler eDelegate = (AfterHeaderCheckStateChangedEventHandler)EventsOptimized[EVENT_AFTERHEADERCHECKSTATECHANGED];
        //    if (eDelegate != null)
        //        eDelegate(this, e);
        //}

        //#endregion OnAfterHeaderCheckBoxStateChanged

        #endregion

        #region Other Events

        /// <summary>
		/// This function gets called whenever a cell button is clicked in 
		/// a cell with either Button or EditButton column styles. It calls
		/// hooked up event delegates
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnClickCellButton ( CellEventArgs e )
		{
			CellEventHandler eDelegate = (CellEventHandler)EventsOptimized[EVENT_CLICKCELLBUTTON];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}

		internal void OnActiveRowDeleted()
		{
			this.activeCell = null;
			this.currentActiveRow = null;
			this.currentActiveRow = this.DisplayLayout.SortedBands[0].GetCurrentRow(false);
		}




		/// <summary>
		/// Called after a a cell list is selected
		/// </summary>
		virtual protected void OnCellListSelect ( CellEventArgs e )
		{
			CellEventHandler eDelegate = (CellEventHandler)EventsOptimized[EVENT_CELLLISTSELECT];

			if ( eDelegate != null )
				eDelegate( this, e );				
		}

		/// <summary>
		/// Called when a data error occurs
		/// </summary>
		virtual protected void OnError( ErrorEventArgs e )
		{
			ErrorEventHandler eDelegate = (ErrorEventHandler)EventsOptimized[EVENT_ERROR];

			if ( eDelegate != null )
				eDelegate( this, e );				
		}

		// SSP 5/2/02
		// Added CellDataError event.
		//
		/// <summary>
		/// Called when the user tries to exit the edit mode on a cell with invalid value typed in.
		/// </summary>
		protected virtual void OnCellDataError( CellDataErrorEventArgs e )
		{
			CellDataErrorEventHandler eDelegate = (CellDataErrorEventHandler)EventsOptimized[EVENT_CELL_DATA_ERROR];

			if ( eDelegate != null )
				eDelegate( this, e );				
		}

        /// <summary>
        /// Called when the active row changes
        /// </summary>
        /// <param name="newActiveRow">The new active row.</param>
        /// <param name="scrollIntoView">true to scroll the new row into view.</param>
        /// <returns>true if the row was successfully changes. false it it was cancelled.</returns>
        internal protected override bool OnActiveRowChange(Infragistics.Win.UltraWinGrid.UltraGridRow newActiveRow, bool scrollIntoView)
		{
            if ( newActiveRow == null )
                return false;

			// SSP 11/11/03 Add Row Feature
			//
			if ( newActiveRow.IsTemplateAddRow && null != newActiveRow.ParentCollection )
			{
				if ( ! newActiveRow.ParentCollection.AddTemplateAddRowToDataList( false ) )
					return false;

				// SSP 3/31/05 BR03098
				// If this method was recursively called to activate the same row then simply return
				// without taking any action since the row has already been activated.
				//
				if ( this.ActiveRow == newActiveRow )
					return true;
			}

			//fire the before event
			//ROBA UWG198 8/21/01
			//RowEventArgs rowEventArgs = new RowEventArgs( activeRow );
			RowEventArgs rowEventArgs = new RowEventArgs( newActiveRow );
			this.FireEvent( GridEventIds.BeforeRowActivate, rowEventArgs );
			
			// ensure that all the ancestor rows are expanded
			//
			// SSP 9/25/02 UWG1375
			// Don't expand the ancestors on the first draw when the active row
			// is set as a result of binding manager's firing of position changed
			// event. Meaning unless the user sets the active row, we don't want to
			// expand the parent rows on or before the first draw. After that
			// we will do what we were doing before.
			//
			// SSP 6/6/05 BR04388
			// If setting active row via SetTempActiveRow then don't expand the ancestors
			// if tempActiveRow_ScrollIntoView specifies that we shouldn't scroll the row
			// into view.
			// 
			//bool expandAncestors = true;
			bool expandAncestors = ! this.settingActiveRowToTempActiveRow || this.tempActiveRow_ScrollIntoView;

			if ( this.FirstDraw && this.settingActiveRowToTempActiveRow )
			{
				expandAncestors = false;

				// Also don't scroll the row into view if the ancestors are not expanded.
				//
				if ( !newActiveRow.AllAncestorsExpanded )
					scrollIntoView = false;
			}

			// SSP 9/25/02 UWG1375
			// Related to above change.
			//
			//if ( !newActiveRow.ExpandAncestors() )
			if ( expandAncestors && !newActiveRow.ExpandAncestors() )
				return false;

			// SSP 8/28/02
			// If setting the corresponding position in the binding manager fails below, we
			// want to revert back to the old active row. So store the old active row.
			//
			UltraGridRow oldActiveRow = this.currentActiveRow;

			//set activerow to the newactiverow
			this.currentActiveRow = newActiveRow;
	
			// we want to position to the new active row
			//
			// JJD 1/28/02 - UWG1001
			// Check the ActiveRow property since that will make sure the
			// row is still valid
			//
			if ( this.ActiveRow != null )
			{
				// SSP 8/7/02 
				// If InternalPositionToRow fails because the current row has some invalid values
				// that doesn't satisfy constraints, then the binding manager will throw an exception
				// when changing the position (which InternalPositionToRow doesn't catch). So
				// catch it here and fire DataError event.
				//
				try
				{
					this.ActiveRow.InternalPositionToRow();
				}
				catch ( Exception exception )
				{	
					// SSP 8/28/02
					// If setting the corresponding position in the binding manager fails below, we
					// want to revert back to the old active row. So store the old active row.
					//
					this.currentActiveRow = oldActiveRow;

		
					DataErrorInfo dataErrorInfo = new DataErrorInfo( exception, null,
						currentActiveRow, null, DataErrorSource.RowUpdate, 
						SR.GetString("DataErrorRowUpdateUnableToUpdateRow", exception.Message ) ); // "Unable to update the row:\n" + exception.Message );

					this.InternalHandleDataError( dataErrorInfo );

					return false;
				}				
			}

			// SSP 4/23/05 - NAS 5.2 Filter Row
			// Added OnActiveStateChanged on row and cell.
			//
			if ( null != this.currentActiveRow )
				this.currentActiveRow.OnActiveStateChanged( true );

			// JJD 1/28/02 - UWG1001
			// Check the ActiveRow property since that will make sure the
			// row is still valid
			//
			if ( this.ActiveRow != null )
			{
			
				// Recalc rects true: e.g.: needed if activberowappearance defines a picture
				this.ActiveRow.InvalidateItemAllRegions( true );

				// We also need to invalidate the add new box since
				// the change of active row may cause some add new
				// buttons to get enabled/disabled
				//
				Infragistics.Win.UIElement element = this.DisplayLayout.GetUIElement( false );

				if ( element != null )
				{
					// June 01 2001, we want to get AddNewBoxUIElement not the AddNewBox
					//
					//element = element.GetDescendant(typeof(Infragistics.Win.UltraWinGrid.AddNewBox) );
					element = element.GetDescendant( typeof( Infragistics.Win.UltraWinGrid.AddNewBoxUIElement ) );
					if ( element != null )
						element.Invalidate();
				}

				// Dirty the new active row since the active row appearance
				// may change the layout of cells in the row (e.g. by adding or removing a picture)
				//
				// SSP 12/12/03 - Optimizations
				// Pass in false for verify (the third parameter) because if it's already dirty it will
				// position the child elements and then we will mark it dirty again. No point in doing all
				// that work of verifying if we are only going to mark it dirty the next moment.
				//
				//element = this.DisplayLayout.ActiveRow.GetUIElement( this.DisplayLayout.Grid.ActiveRowScrollRegion,
				//	this.DisplayLayout.Grid.ActiveColScrollRegion );
				element = this.DisplayLayout.ActiveRow.GetUIElement( this.DisplayLayout.Grid.ActiveRowScrollRegion,
					this.DisplayLayout.Grid.ActiveColScrollRegion, false );

				if ( element != null )
					element.DirtyChildElements();

				// SSP 12/7/04 - Merged Cell Feature
				//
				RowsCollection.MergedCell_RowStateChanged( this.currentActiveRow, MergedCell.State.Activation );

				// scroll the row into view
				//
				//RobA UWG312 9/21/01
				//if ( this.currentActiveRow.Band.ScrollRowIntoView )

				// RobA 12/26/01 
				// added scrollIntoView param
				//
				if ( scrollIntoView )
					this.DisplayLayout.Grid.ActiveRowScrollRegion.ScrollRowIntoView( this.DisplayLayout.ActiveRow );
				
				// MRS 5/20/05 - BR04105
				// If there are no selected rows, then set the PivotItem 
				// to the new Active Row. 
				if ( this.Selected.Rows.Count == 0 
					// SSP 8/17/05 BR05695
					// Also set the pivot if the only selected row is the same as the active row.
					// 
					|| 1 == this.Selected.Rows.Count && this.Selected.Rows[0] == this.ActiveRow )
                    ((ISelectionManager)this).SetPivotItem(this.ActiveRow, true);

                // MBS 3/7/08 - RowEditTemplate NA2008 V2
                UltraGridRowEditTemplate template = this.ActiveRow.RowEditTemplateResolved;
                if(template != null && template.DisplayMode == RowEditTemplateDisplayMode.Modeless &&
                    template.IsShown)
                {
                    // If we're showing a modeless form, we need to update the row being edited in the template
                    // since we will not be closing it.
                    template.Row = this.ActiveRow;
                }

                // MBS 4/21/08 - RowEditTemplate NA2008 V2
                UltraGridRowEditTemplate oldTemplate = oldActiveRow != null ? oldActiveRow.RowEditTemplateResolved : null;
                if (oldTemplate != null && oldTemplate.Row != null && oldTemplate.IsShown == false)
                {
                    oldTemplate.Row = this.ActiveRow;
                }

				// fire the after Row activate event
				//
				this.FireEvent( GridEventIds.AfterRowActivate, null );
			}

			return true;
		}


 
		/// <summary>
		/// Called when the focus leaves this control or any
		/// of its owned controls
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnLeave( EventArgs e )
		{
			base.OnLeave( e );

			this.InternalLostFocus( false );
		}

		internal bool HasFocus
		{
			get
			{
				// if the grid or one of its child controls is in focus then
				// return true.
				if ( this.ContainsFocus )
				{
					return true;
				}		
				
				// SSP 4/26/02
				// Em Embeddable editors.
				// Commented below code out.
				
				
				// if the tool tip has focus, return true
				if ( null != this.toolTip && this.toolTip.Control.ContainsFocus )
				{
					return true;
				}

				return false;
			}
		}
        
		// SSP 8/29/02 UWG1564
		// The .NET framework is the one causing this problem. What happens is that when 
		// the grid's OnLeave gets called (when the user clicks the second tab), the grid
		// exits the edit mode and hides the underlying control used for editing (in this
		// case a textbox). That edit control is the one that had focus however it gets 
		// hidden and thus it looses the focus and the grid receives it since it's the 
		// parent of that hidden edit control. This happens during the series of focus 
		// events that the framework is in midst of firing (including validating) and 
		// causes the framwork to not retain the focus on the grid and thus the second 
		// tab ends up gaining the focus. To workaround this I guess framework bug, 
		// ExitEditModeOnLeave property was added to the UltraWinGrid. Default is true 
		// however you can set it to false to prevent the grid from exiting the edit mode 
		// if you are having such a problem regarding validating event. This should solve 
		// the problem.
		//
		/// <summary>
		/// Indicates whether to exit the edit mode when the focus leaves the grid, specifically when OnLeave gets called. Default value is true.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use this property to prevent the grid from exiting the edit mode when focus leaves the grid. By default grid exits the edit mode when OnLeave gets called. However you can set this property to false to prevent the grid from doing that. Note: If this is set to false, the update modes of OnRowChangeOrLostFocus and OnCellChangeOrLostFocus will act like OnRowChange and OnCellChange respectively.</p>
		/// </remarks>
		[ EditorBrowsable( EditorBrowsableState.Advanced ), 
		// SSP 10/3/02 UWG1564
		// Commented out DesignerSerializationVisibility attribute as I don't see any reason for.
		//DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
		LocalizedDescription("LDR_UltraGrid_P_ExitEditModeOnLeave") ]
		public bool ExitEditModeOnLeave
		{
			get
			{
				return this.exitEditModeOnLeave;
			}
			set
			{
                // MRS NAS v8.2 - Unit Testing
				//this.exitEditModeOnLeave = value;
                if (this.exitEditModeOnLeave == value)
                    return;
                
                this.exitEditModeOnLeave = value;
                
                this.NotifyPropChange(PropertyIds.ExitEditModeOnLeave);
			}
		}

		/// <summary>
		/// Returns true if this property can be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeExitEditModeOnLeave( )
		{
			return !this.exitEditModeOnLeave;
		}

		/// <summary>
		/// Resets the ExitEditModeOnLeave property to its default value of true.
		/// </summary>
		public void ResetExitEditModeOnLeave( )
		{
            // MRS NAS v8.2 - Unit Testing
			//this.exitEditModeOnLeave = true;
            this.ExitEditModeOnLeave = true;
		}

		internal void InternalLostFocus( bool scrolling )
		{
			if ( ! scrolling )
			{
				// SSP 11/29/01
				// Moved all this code down to HasFocus internal property 
				//
				// SSP 5/14/02 UWG1114
				// We don't need to check if the grid contains the focus because Leave gets
				// fired when a control other than grid and it's children becomes the active
				// control. So checking if the grid or one of its children has the focus is
				// not necessary. The reason why we had it there because at one point in time
				// we were using OnLostFocus to do what we do below and with LostFocus, it
				// would get fired when the focus shifted from grid to one of its children
				// like the textbox, that why we had the ContainsFocus check there so we
				// would proceed if the focus went to one of our children. With OnLeave, we
				// don't need to do that anymore since Leave would only get fired if a control
				// other than the grid and its children becomes activated. Besides this causes
				// probles as in UWG1114. Because, as .NET framework document (look at 
				// Control.Leave event) indicates, Leave gets fired before LostFocus so at 
				// times grid would still contain the foucs in OnLeave (and we would end up
				// returning here instead of processing the code below).
				//
				// -------------------------------------------------------------------
				// Commented out the if statemenet below and added code that checks for 
				// month calendar and tooltip containing the focus.
				//
				
				
				// if the tool tip has focus, return true
				if ( null != this.toolTip && null != this.toolTip.Control &&
					this.toolTip.Control.ContainsFocus )
				{
					return;
				}
				// -------------------------------------------------------------------

				
			}
			
			// SSP 12/6/02 UWG1877
			// Added an anti-recursion flag and enclosed the below code in try-finally block.
			//
			bool origInOnLeave = this.inOnLeave;
			this.inOnLeave = true;

			try
			{
				// now the grid really doesn't have focus
				// SSP 8/29/02 UWG1564
				// Added new ExitEditModeOnLeave property. Don't exit the edit mode if it's true.
				// Enclosed the block of code in this if block.
				//
				if ( this.ExitEditModeOnLeave )
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( null != this.ActiveCell && this.ActiveCell.IsInEditMode )
					//{
					//    Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = this.ActiveCell;
					Infragistics.Win.UltraWinGrid.UltraGridCell activeCell = this.ActiveCell;

					if ( null != activeCell && activeCell.IsInEditMode )
					{
						activeCell.ExitEditMode();

						// SSP 7/18/02 UWG1391
						// When an EditorWithText is exitting the edit mode, it hides the underlying
						// text box control. Hiding of the text box causes grid's OnLeave to be fired
						// which in turn calls this method. So we should not try to do update
						// the row here because grid is going to retain the focus once the editor
						// completely exits the edit mode. Above we are calling ExitEditMode which will
						// return without doing anything if we are already in ExitEditMode method (which
						// has an anti-recursion flag). So IsInEditMode is still true then it's a a good 
						// indication that we are still in edit mode.
						//
						if ( !activeCell.IsInEditMode )
						{
							if ( UpdateMode.OnCellChangeOrLostFocus == this.UpdateMode ||
								UpdateMode.OnRowChangeOrLostFocus == this.UpdateMode )
							{
								if ( null != activeCell.Row )
								{
									// SSP 11/19/03 Add Row Feature
									// If the row is an unmodified add-row from template add-row (in other words it's an 
									// add-row that was added by giving focus to a template row however which hasn't been 
									// modified by the user) then instead of comitting the row, cancel it.
									// Added the if block while enclosed the existing code in the else block.
									//
                                    if (activeCell.Row.IsUnModifiedAddRowFromTemplate)
                                        // SSP 2/25/07 BR18124
                                        // 
                                        //activeCell.Row.CancelEdit( );
                                        activeCell.Row.CancelEditHelper_ActivateNearestRow();
                                    else
                                    {
                                        // MBS 6/27/08 - BR34070
                                        // We don't want to update the row if the template is shown,
                                        // since the template is responsible for ending, or cancelling,
                                        // any updates.
                                        UltraGridRowEditTemplate template = activeCell.Row.RowEditTemplateResolved;
                                        if(template == null || template.IsShown == false)
                                            activeCell.Row.Update();
                                    }
								}
							}
						}
					}
						// SSP 10/29/02 UWG1794
						// Added the else block. We want to update the active row if
						// the update mode is OnCellChangeOrLostFocus or OnRowChangeOrLostFocus 
						// when we lose the focus. Above if part only does so if a cell is
						// in edit mode, however we want to do it regardless of whether a
						// cell is in edit mode or not.
						//
					else
					{
						if ( UpdateMode.OnCellChangeOrLostFocus == this.UpdateMode ||
							UpdateMode.OnRowChangeOrLostFocus == this.UpdateMode )
						{
							UltraGridRow activeRow = this.ActiveRow;
							if ( null != activeRow )
							{
								// SSP 11/19/03 Add Row Feature
								// If the row is an unmodified add-row from template add-row (in other words it's an 
								// add-row that was added by giving focus to a template row however which hasn't been 
								// modified by the user) then instead of comitting the row, cancel it.
								// Added the if block while enclosed the existing code in the else block.
								//

								if ( activeRow.IsUnModifiedAddRowFromTemplate )
									// SSP 2/25/07 BR18124
									// 
									//activeRow.CancelEdit( );
									activeRow.CancelEditHelper_ActivateNearestRow( );
                                else if (activeRow.DataChanged)
                                {
                                    // MBS 6/27/08 - BR34070
                                    // We don't want to update the row if the template is shown,
                                    // since the template is responsible for ending, or cancelling,
                                    // any updates.
                                    UltraGridRowEditTemplate template = activeRow.RowEditTemplateResolved;
                                    if (template == null || template.IsShown == false)
                                        activeRow.Update();
                                }
							}
						}
					}
				}
			}
			finally
			{
				this.inOnLeave = origInOnLeave;
			}
		}

		/// <summary>
		///  Clears the active row and cell without firing any events
		/// </summary>
		internal protected override void ClearActiveRow()
		{
			this.activeCell		= null;
			base.ClearActiveRow();
		}

        /// <summary>
        /// Called when the active row is cleared
        /// </summary>
        ///<param name="update">true to update the row.</param>
        ///<returns>true if the row was successfully cleared. false it it was cancelled.</returns>
        internal protected override bool OnActiveRowCleared(bool update)
		{
			// when we clear the active row we also need to clear the active cell
			// but we don't need to invalidate the active cell since that will
			// be taken care of when we invalidate the row
			//
            // MBS 4/1/09 - NA9.2 CellBorderColor
            // If we've drawn a thicker border along the active cell, we do need to invalidate
            // the active cell, since by the time we invalidate the row we will not have an active
            // cell anymore and thus the row will not know that it needs to invalidate a larger region
            //
            //if ( this.ClearActiveCell( false, false ) )
            bool invalidateCell = this.activeCell != null && this.activeCell.Band.ActiveCellBorderThicknessResolved > 0;
            if (this.ClearActiveCell(invalidateCell, false))
				return false;

			if ( this.currentActiveRow != null )
			{
				// SSP 6/25/02 UWG1266
				// Store the current active row into a variable because currentActiveRow
				// member variable could get set to null when for example BeforeRowDeactivate
				// event is fired below.
				//
				UltraGridRow activeRow = this.currentActiveRow;

				// Fire the BeforeRowDeactivate event. If it is cancelled
				// then return false.
				// SSP 6/25/02 UWG1266
				// If the active row is invalid (when a group by row is the active row and
				// the group by hierarchy changes, we regenerate the group by rows and then 
				// the active row would become invalid and ActiveRow property's get will
				// return null. So we only want to fire the event if the ActiveRow property 
				// returns a row.
				// Added if check for the code inside.
				//
				if ( null != this.ActiveRow )
				{
					CancelEventArgs eventArgs = new CancelEventArgs();
					this.FireEvent( GridEventIds.BeforeRowDeactivate, eventArgs );
					if ( eventArgs.Cancel )
						// SSP 2/5/02 UWG641
						// Return false, not true when the clearing of the active row
						// is cancelled.
						//
						//return true;
						return false;

					// SSP 11/12/03 Add Row Feature
					// If this row is an add-row and the user has not modified its cell
					// contents then cancel the row.
					//
					// --------------------------------------------------------------------
					UltraGridRow tmpActiveRow = this.ActiveRow;
					if ( null != tmpActiveRow && tmpActiveRow.IsUnModifiedAddRowFromTemplate )
					{
						update = false;
						tmpActiveRow.CancelEdit( );
					}
					// --------------------------------------------------------------------
				}

				if ( update && 
					Infragistics.Win.UltraWinGrid.UpdateMode.OnUpdate != this.UpdateMode )
				{
					// SSP 6/25/02 UWG1266
					// Use the activeRow local variable instead of currentActiveRow member variable
					// because when above BeforeRowDeactivate is fired, the currentActiveRow may
					// get set to null.
					// 
					//bool succeeded = this.currentActiveRow.Update();					
					bool succeeded = activeRow.Update();

					if ( ! succeeded )
						return false;

					// SSP 8/29/04 UWG3610
					// If the row is a card row and the card style is variable height then updating of the row
					// could end up hiding some cells if they were emptied out. As a result we need
					// to make sure that the card area is repositioned. Dirty the grid element for
					// that.
					//
					if ( activeRow.IsCardStyleVariableHeight 
						&& activeRow.IsPotentiallyVisibleOnScreen
						&& null != this.DisplayLayout )
						this.DisplayLayout.DirtyDataAreaElement( );

					//RobA UWG434 10/3/01 this.currentActiveRow.Update() might set the active row
					//to null by calling this method within SyncRows, so we just want 
					//to return
					//
					if ( this.currentActiveRow == null )
						return true;
				}


				// SSP 11/14/01 UWG600
				// Commented this out because doesn't look like we need it since we will
				// be dirtying all the row elements associated with the row below.
				//
				// invalidate the cell
				//
				// Use new InvalidateItemAllRegions method
				//
				//this.ActiveRow.InvalidateItemAllRegions();

				// We also nned to invalidate the add new box since
				// the change of active row may cause some add new
				// buttons to get enabled/disabled
				//
				// SSP 5/29/02 UWG1156
				// Make sure the layout is not null in case this is called when the grid is being 
				// disposed of.
				//
				//Infragistics.Win.UIElement element = this.DisplayLayout.GetUIElement(false);
				Debug.Assert( null != this.DisplayLayout, "No layout !" );
				Infragistics.Win.UIElement element = null != this.DisplayLayout
					? this.DisplayLayout.GetUIElement(false)
					: null;

				if ( element != null )
				{
					// June 01 2001, we want to get AddNewBoxUIElement not the AddNewBox
					//
					//element = element.GetDescendant(typeof(Infragistics.Win.UltraWinGrid.AddNewBox) );
					element = element.GetDescendant( typeof( Infragistics.Win.UltraWinGrid.AddNewBoxUIElement ) );
					if ( element != null )
						element.Invalidate();
				}

				Infragistics.Win.UltraWinGrid.UltraGridRow tmpRow = this.currentActiveRow;

				// SSP 11/14/01 UWG600
				// We need to invalidate all the elements (in all the scroll regions)
				// not just the active. And we should do it after setting the activeRow
				// to null so active row appearances get resolved properly.
				

				// finally clear the active row
				//
				this.currentActiveRow = null;

				// SSP 4/23/05 - NAS 5.2 Filter Row
				// Added OnActiveStateChanged on row and cell.
				//
				if ( null != tmpRow )
					tmpRow.OnActiveStateChanged( false );

				// SSP 11/14/01 UWG600
				// Regarding the comment on UWG600 above.
				// Dirty the row elements in all the scroll regions.
				// Pass in true which will dirty the row elements.
				//
				if ( null != tmpRow )
				{
					tmpRow.InvalidateItemAllRegions( true );

					// SSP 12/7/04 - Merged Cell Feature
					//
					RowsCollection.MergedCell_RowStateChanged( tmpRow, MergedCell.State.Activation );
				}

			}
			return true;
		}

		/// <summary>
		/// Called after a cell change
		/// </summary>
		virtual protected void OnCellChange ( CellEventArgs e )
		{
			CellEventHandler eDelegate = (CellEventHandler)EventsOptimized[EVENT_CELLCHANGE];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}

		


		/// <summary>
		/// Called when a new logical page is initialize
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnInitializeLogicalPrintPage( CancelableLogicalPrintPageEventArgs e )
		{
			InitializeLogicalPrintPageEventHandler eDelegate = (InitializeLogicalPrintPageEventHandler)EventsOptimized[EVENT_INTIALIZELOGICALPRINTPAGE];

			if ( eDelegate != null )
				eDelegate( this, e );

		}

		/// <summary>
		/// Called when Print is Initialized
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnInitializePrint( CancelablePrintEventArgs e )
		{
			InitializePrintEventHandler eDelegate = (InitializePrintEventHandler)EventsOptimized[EVENT_INTIALIZEPRINT];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}


		


		/// <summary>
		/// Called when Print Preview is Initialized
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnInitializePrintPreview( CancelablePrintPreviewEventArgs e )
		{
			InitializePrintPreviewEventHandler eDelegate = (InitializePrintPreviewEventHandler)EventsOptimized[EVENT_INTIALIZEPRINTPREVIEW];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		



		/// <summary>
		/// Called when a selection is being dragged
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnSelectionDrag ( CancelEventArgs e )
		{
			CancelEventHandler eDelegate = (CancelEventHandler)EventsOptimized[EVENT_SELECTIONDRAG];

			if ( eDelegate != null )
				eDelegate( this, e );
		}


		


		/// <summary>
		/// Called when the layout is first initialized after the 
		/// datasource has been set 
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnInitializeLayout ( InitializeLayoutEventArgs e )
		{
			InitializeLayoutEventHandler eDelegate = (InitializeLayoutEventHandler)EventsOptimized[EVENT_INTIALIZELAYOUT];

			if ( eDelegate != null )
				eDelegate( this, e );						
		}


		/// <summary>
		/// Called when a row is initialized
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnInitializeRow( InitializeRowEventArgs e )
		{
			InitializeRowEventHandler eDelegate = (InitializeRowEventHandler)EventsOptimized[EVENT_INTIALIZEROW];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		// SSP 11/10/03 Add Row Feature
		//
		/// <summary>
		/// Called when a template add-row is initialized.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnInitializeTemplateAddRow( InitializeTemplateAddRowEventArgs e )
		{
			InitializeTemplateAddRowEventHandler eDelegate = (InitializeTemplateAddRowEventHandler)EventsOptimized[EVENT_INITIALIZETEMPLATEADDROW];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		/// <summary>
		/// Called when a group by row is initialized
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnInitializeGroupByRow( InitializeGroupByRowEventArgs e )
		{
			InitializeGroupByRowEventHandler eDelegate = (InitializeGroupByRowEventHandler)EventsOptimized[EVENT_INTIALIZEGROUPBYROW];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}

		/// <summary>
		/// Called when grid itself resizes.
		/// </summary>
        /// <param name="eventArgs">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnResize(System.EventArgs eventArgs )
		{
			// SSP 8/25/05 BR05929
			// Make sure that the main element is sized to the right size otherwise if
			// VisibleRows is queried in Resize event handler then it will be wrong because
			// it will be based on the grid element size.
			// 
			UltraGridLayout layout = this.DisplayLayout;
			if ( null != layout && ! layout.Disposed && layout.HasUIElement )
			{
				
				
				
				
				
				
				
				
				layout.UIElement.InternalInitializeRect( false );
				if ( null != layout.RowScrollRegions )
				{
					foreach ( RowScrollRegion rsr in layout.RowScrollRegions )
						rsr.VerifyGridRectAndVisibleRows = true;
				}
				
			}

			//call the base
			base.OnResize(eventArgs);

			
			
			// 
			
			if ( null != layout && null != layout.RowScrollRegions )
			{
				foreach ( RowScrollRegion rsr in layout.RowScrollRegions )
					rsr.VerifyGridRectAndVisibleRows = false;
			}
			

			// JJD 8/21/01
			// Only dirty the element if we have already been rendered
			//
			// JJD 12/04/01
			// Only set the rect and invalidate if the control has
			// been created. Otherwise, ignore this message. 
			// This fixed a problem at design time where scrollbars
			// were being created too early in the process which
			// was preventing thumb tracking from working.
			//
			if ( !this.FirstDraw && this.Created )
			{
				//dirty grid and both column and row scroll regions
				this.DisplayLayout.DirtyGridElement();
				this.DisplayLayout.ColScrollRegions.DirtyMetrics();
				this.DisplayLayout.RowScrollRegions.DirtyAllVisibleRows();
			}

			// JM 01-15-02
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if (this.ActiveCell != null && this.ActiveCell.IsInEditMode)
			//    this.ActiveCell.ExitEditMode();
			UltraGridCell activeCell = this.ActiveCell;

			if ( activeCell != null && activeCell.IsInEditMode )
				activeCell.ExitEditMode();

			// SSP 11/24/03 - Scrolling Till Last Row Visible
			// Added the functionality to not allow the user to scroll any further once the
			// last row is visible.
			//
			// ------------------------------------------------------------------------------
			if ( null != this.DisplayLayout && ! this.DisplayLayout.Disposed
				// SSP 5/26/06 - Optimization
				// 
				&& ! this.FirstDraw && this.Created )
			{
				
				
				
				//this.DisplayLayout.EnsureScrollRegionsFilled( );
				this.DisplayLayout.BumpGrandVerifyVersion( true );
			}
			// ------------------------------------------------------------------------------
		}

		// SSP 11/18/03 Add Row Feature
		// EnsureTempActiveRowAssigned method and moved code from OnPaint into this method.
		// We needed to invoke this logic from UltraGridBand.AddNewHelper method without
		// having to paint the grid which caused a flicker. So moved the logic into a seperate
		// method.
		//
		internal void EnsureTempActiveRowAssigned( )
		{
			// SSP 10/8/01
			// Now we are doing delayed active row change. When deleting
			// multiple rows for example, we don't want to be
			// setting the active row multiple times causing the grid
			// to be repainted everytime
			//
			UltraGridRow tmpActiveRow = this.TempActiveRow;
			if ( null != tmpActiveRow )
			{

				if ( this.ActiveRow != tmpActiveRow
					// SSP 1/29/03 UWG1969
					// Make sure the temp active row is from the same layout as the grid's display
					// layout. It could be from a print layout.
					// 
					&& tmpActiveRow.Layout == this.DisplayLayout )
				{
					// SSP 9/25/02 UWG1375
					// Added settingActiveRowToTempActiveRow flag.
					//
					this.settingActiveRowToTempActiveRow = true;
					try
					{
						// SSP 12/2/04 BR00017 BR00514
						// When deleting a row directly from the data table we get PositionChanged 
						// before we get ItemDeleted which ends up causing us to set TempActiveRow 
						// to a wrong row if the deleted row occurred before the active row in the 
						// table.
						//
						// ------------------------------------------------------------------------
						UltraGridBand band = tmpActiveRow.Band;
						if ( null != band.BindingManager 
							&& ( ! tmpActiveRow.IsStillValid || band.BindingManager.Position != tmpActiveRow.ListIndex ) )
						{
							RowsCollection rows = band.GetCurrentParentRows( false, false );
							if ( null != rows )
								tmpActiveRow = rows.GetRowWithListIndexHelper( band.BindingManager.Position, false );
						}
						// ------------------------------------------------------------------------

						// SSP 12/8/03 UWG2753
						// Only set ActiveRow if the row is a valid row. Added the if condition.
						//
						if ( null != tmpActiveRow && tmpActiveRow.IsStillValid 
							// SSP 11/18/05 BR07755
							// Only set the new active row if it's different from the current one.
							// 
							&& this.ActiveRow != tmpActiveRow )
						{
							// SSP 11/17/04 UWG2234 
							// Changed the behavior so that when a column is sorted we maintain the scroll position
							// instead of keeping the first row the first row.
							//
							//this.ActiveRow = this.TempActiveRow;
							this.SetActiveRow( tmpActiveRow, this.tempActiveRow_ScrollIntoView );
						}
					}
					finally
					{
						this.settingActiveRowToTempActiveRow = false;
					}
				}

				// SSP 10/16/01 UWG536
				// We must set the TempActiveRow to null
				//
				this.TempActiveRow = null;
			}
		}

		/// <summary>
		/// Calls the UIElementsCollection draw method
		/// </summary>
		protected override void OnPaint(PaintEventArgs pe)
		{
			
			// SSP 8/9/02 UWG1526
			// If the primary metrics has not been initialized, then initialize and reget
			// the bottom otherwise we will be scrolling unneccessarily (even when all 
			// the rows fit the grid area).
			//
			if ( this.FirstDraw )
			{				
				this.DisplayLayout.GetUIElement( true, true );
			}

			// SSP 11/18/03 Add Row Feature
			// Added new EnsureTempActiveRowAssigned method and moved code from here into this method.
			// We needed to invoke this logic from UltraGridBand.AddNewHelper method without
			// having to paint the grid which caused a flicker. So moved the logic into a seperate
			// method.
			//
			// ----------------------------------------------------------------------------------
			
			this.EnsureTempActiveRowAssigned( );
			// ----------------------------------------------------------------------------------

			// SSP 8/15/05 BR05529
			// If the control has been disposed in the mean time then return.
			// 
			if ( null == this.DisplayLayout )
				return;

			bool holdFirstDraw = this.FirstDraw;

			if ( holdFirstDraw )
			{
				this.ResetFirstDrawFlag();
				this.DisplayLayout.GetColScrollRegions( true ).DirtyMetrics();
			}

			// AS 1/8/03 fxcop
			// Cannot have 2 public/protected members that differ only by case
			//this.drawing = true;
			this.drawingValue = true;

			// JJD 10/31/01
			// Set drawing state on layout which is needed to perform
			// cell appearance cacahing optimizations
			//
			this.DisplayLayout.InitializeDrawingFlag( true );

			// JJD 10/05/01
			// Wrap OnPaint in try/finally so we can be assured that
			// the Cursor is redisplayed and the drawing flag is reset
			// 
			try
			{
				//if ( !this.IsUpdating )
				//	Debug.WriteLine( "OnPaint clip: " + pe.ClipRectangle.ToString() );
				base.OnPaint( pe );
			}
			finally
			{
				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//this.drawing = false;
				this.drawingValue = false;

				// JJD 10/31/01
				// Reset the drawing state on layout
				//
				this.DisplayLayout.InitializeDrawingFlag( false );

				// SSP 5/11/05 BR02633
				// Added lastPaintRect variable to the layout.
				//
				this.DisplayLayout.lastPaintRect = pe.ClipRectangle;
			}

			// On the first draw call VerifyAllScrollbarPositions to cover
			// the situation where rows are hidden on the first draw (where
			// we may be preloading all rows) and we need to adjust
			// the thumb positioning
			//
			if ( holdFirstDraw )
				this.DisplayLayout.RowScrollRegions.VerifyAllScrollbarPositions();

			//Reposition edit control if flag is dirty
			if ( this.DisplayLayout.TextControlPositionDirty )
			{
				this.DisplayLayout.TextControlPositionDirty = false;

				try
				{
					// SSP 8/14/02
					// Even thought this is enclosed in a try catch block, still
					// check for active cell being null. Below where we ensure 
					// the edit control is positioned properly will take care of
					// hiding the control when the active cell is null.
					//
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( null != this.ActiveCell )
					//    this.ActiveCell.RepositionTextBoxCtl();        			
					UltraGridCell activeCell = this.ActiveCell;

					if ( null != activeCell )
						activeCell.RepositionTextBoxCtl();  
				}
				catch( Exception e )
				{
					Debug.Fail("Failure in UltraGrid.OnPaint! ", e.Message );
				}
			}
		}

		/// <summary>
		/// Called when a property has changed
		/// </summary>
		protected override void OnPropertyChanged( Infragistics.Win.PropertyChangedEventArgs e )
		{

			if ( e.ChangeInfo.PropId is Infragistics.Win.UltraControlPropIds &&
				(Infragistics.Win.UltraControlPropIds)e.ChangeInfo.PropId == Infragistics.Win.UltraControlPropIds.FlatMode )
			{
				// Set the synchronized col list dirty flag on.
				// 
				this.DisplayLayout.SynchronizedColListsDirty = true;
				this.DisplayLayout.DirtyGridElement();
				// dirty metrics to make sure any changes made are visible
				//
				this.DisplayLayout.GetColScrollRegions( false ).DirtyMetrics( false );
			}

			// JJD 1/18/02
			// Call the base OnPropertyChanged
			//
			base.OnPropertyChanged(e);

		}
	
		/// <summary>
		/// Called by UltraGridBase when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnBasePropChanged( PropChangeInfo propChange ) 
		{
			try 
			{
				if ( propChange.Source == this.DisplayLayout )
				{
					// MD 4/17/08 - 8.2 - Rotated Column Headers
					// If any text orientations have changed, we should treat it as if the font has changed, 
					// because the height of headers will change if their rotations changes.
					//if ( null != propChange.FindPropId( AppearancePropIds.FontData ) )
					if ( null != propChange.FindPropId( AppearancePropIds.FontData ) ||
						null != propChange.FindPropId( PropertyIds.TextOrientation ) )
					{
						// Clear cached metrics and dirty grid if font changes
						//				
						this.DisplayLayout.ClearCachedCaptionHeight();

						this.DisplayLayout.OnBaseFontChanged();

						this.DisplayLayout.DirtyGridElement();
						return;
					}

					switch ( (PropertyIds)propChange.PropId )
					{
						case PropertyIds.ViewStyleBand:
							// Set the synchronized col list dirty flag on.
							// 
							this.DisplayLayout.SynchronizedColListsDirty = true;
							break;

						case PropertyIds.ViewStyle:
						{
							this.ClearAllSelected();

							// JJD 8/28/01
							// If this is not a multiband view style anymore 
							// make sure our active row is not a child row
							//
							if ( !this.DisplayLayout.ViewStyleImpl.IsMultiBandDisplay )
							{
								Infragistics.Win.UltraWinGrid.UltraGridRow row = this.ActiveRow;

								// JJD 8/28/01
								// walk up the active row's parent chain to get to 
								// the band zero row
								//
								while ( row != null && row.ParentRow != null ) 
								{
									row = row.ParentRow;
								}

								this.ActiveRow = row;
							}

							// SSP 9/24/01
							// If active row is a group by row and the new style doesn't
							// support group by rows, then set the active row to a 
							// regular row by looking at the position in binding manager
							//
							// SSP 8/2/02 UWG1481
							// Reordered the conditions.
							//
							//if ( null != this.ActiveRow && ( this.ActiveRow is UltraGridGroupByRow ) &&							
							//	null != this.DisplayLayout.ViewStyleImpl &&
							//	this.DisplayLayout.ViewStyleImpl.SupportsGroupByRows )
							if ( null != this.DisplayLayout.ViewStyleImpl &&
								!this.DisplayLayout.ViewStyleImpl.SupportsGroupByRows &&								
								null != this.ActiveRow && ( this.ActiveRow is UltraGridGroupByRow ) )
							{
								int pos = -1;
								UltraGridRow row = null;
								try
								{
									UltraGridBand tmpBand = this.DisplayLayout.SortedBands[0];

									// SSP 3/31/04
									// Check for binding manager being null.
									//
									if ( null != tmpBand.BindingManager )
									{
										pos = tmpBand.BindingManager.Position;
										row = tmpBand.Layout.Rows[ pos ];
									}
								}
								catch ( Exception )
								{
								}

								this.ActiveRow = row;
							}

							// Set the synchronized col list dirty flag on.
							// 
							this.DisplayLayout.SynchronizedColListsDirty = true;
							this.DisplayLayout.DirtyGridElement();
							// dirty metrics to make sure any changes made are visible
							//
							this.DisplayLayout.GetColScrollRegions( false ).DirtyMetrics( false );
							break;
						}

						// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
						// Obsoleted AutoFitColumns and added AutoFitStyle property.
						//
						//case PropertyIds.AutoFitColumns:
						case PropertyIds.AutoFitStyle:
						{
							// Set the synchronized col list dirty flag on.
							// 
							this.DisplayLayout.SynchronizedColListsDirty = true;
							this.DisplayLayout.DirtyGridElement();
							// dirty metrics to make sure any changes made are visible
							//
							this.DisplayLayout.GetColScrollRegions( false ).DirtyMetrics( false );

							// SSP 6/18/04 UWG3253
							// Added ResetAutoFit_ColumnLastAdjustmentFullyAppliedToIndex method.
							// To fix an issue where repeatedly toggling AutoFitColumns property was causing
							// the auto-fit columns logic to be executed multiple times and every time the
							// logic was executed, the auto-fit slack as a result of rounding was being applied
							// to a different column based on the columnLastAdjustmentFullyAppliedToIndex 
							// counter variable causing a wave like effect (see UWG3253 sample). To fix this 
							// issue, whenever the value of AutoFitColumns is changed, we will reset this
							// columnLastAdjustmentFullyAppliedToIndex variable on all bands.
							//
							this.DisplayLayout.ResetAutoFit_ColumnLastAdjustmentFullyAppliedToIndex( );

							// SSP 4/7/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
							//
							this.DisplayLayout.BumpRowLayoutVersion( );

							break;
						}
						case PropertyIds.Scrollbars:
						{
							//old code = m_pLayout->DirtyGridElement();
							//this code was already here
							if ( null != this.DisplayLayout )
								this.DisplayLayout.UIElement.DirtyChildElements();
							break;
						}						
						case PropertyIds.ColScrollRegions:
						case PropertyIds.RowScrollRegions:
						{
							if ( null != propChange.FindPropId( PropertyIds.Hidden ) ||
								null != propChange.FindPropId( PropertyIds.Scrollbar  ) ||
								null != propChange.FindPropId( PropertyIds.Remove  ) )
								this.DisplayLayout.DirtyGridElement();
							else
								this.Invalidate();
							break;
						}
						
							// Invalidate the grid when an appearance property is set
							//
						case PropertyIds.Appearance:
						case PropertyIds.CaptionAppearance:
							// SSP 1/5/04 UWG1606
							// Added appearances for the splitter bars.
							//
						case PropertyIds.SplitterBarHorizontalAppearance:
						case PropertyIds.SplitterBarVerticalAppearance:
						{
							bool dirtyGridElement = true;
							
							//ROBA UWG116 8/17/01 
							//assume that the fontdata has changed
							//
							if ( propChange.Trigger == null )
							{
								// Clear cached metrics and dirty grid if font changes
								//				
								this.DisplayLayout.ClearCachedCaptionHeight();

								if ( this.DisplayLayout.Bands != null )
									this.DisplayLayout.Bands.OnBaseFontChanged();
							}

							if ( propChange.FindPropId( AppearancePropIds.AlphaLevel ) != null )
								dirtyGridElement = false;
							if ( dirtyGridElement )
								this.DisplayLayout.DirtyGridElement( false );
							else
								this.Invalidate();

							break;
						}

						case PropertyIds.AlphaBlendEnabled:
						case PropertyIds.RowConnectorStyle:
						case PropertyIds.RowConnectorColor:
						{
							bool dirtyGridElement = true;
							
							if ( propChange.FindPropId( AppearancePropIds.AlphaLevel ) != null )
								dirtyGridElement = false;
							if ( dirtyGridElement )
								this.DisplayLayout.DirtyGridElement( false );
							else
								this.Invalidate();

							// SSP 3/20/06 - App Styling
							// 
							StyleUtils.DirtyCachedPropertyVals( this.DisplayLayout );

							break;
						}

							//RobA UWG638 11/7/01 added MaxColScrollRegions and MaxRowScrollRegions
						case PropertyIds.MaxColScrollRegions:
						case PropertyIds.MaxRowScrollRegions:
						case PropertyIds.BorderStyleCaption:
						case PropertyIds.Caption:
						// JAS 4/5/05 v5.2 CaptionVisible property 
						case PropertyIds.CaptionVisible:
						{
							this.DisplayLayout.DirtyGridElement();
							break;
						}
						
							// SSP 11/2/01 UWG640
							// Dirty the grid element when the interband spacing property changes
							// 
						case PropertyIds.InterBandSpacing:
						case PropertyIds.BorderStyle:
							this.DisplayLayout.DirtyGridElement();

                            // MBS 9/19/07 - BR26421
                            // After changing the border style, we need to recalculate the borders and column widths
                            // since they may need to be adjusted (i.e. if we went from a 1px border to a 2px one).
                            this.DisplayLayout.ColScrollRegions.DirtyMetrics();

							break;

                        // MBS 4/17/09 - NA9.2 Selection Overlay
                        // If we've changed how the selection overlay is drawn, we should invalidate the grid so that
                        // the overlay can be properly redrawn (or not drawn as the case may warrent)
                        case PropertyIds.SelectionOverlayBorderColor:
                        case PropertyIds.SelectionOverlayColor:
                        case PropertyIds.SelectionOverlayBorderThickness:
                            this.Invalidate();
                            break;
					}

				}
			}
			finally
			{
				// JJD 11/06/01
				// Call the base implementation which will fire
				// the PropertyChanged notification
				//  
				base.OnBasePropChanged( propChange );
			}
		}


		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
		#region Moved to UltraGridBase
		// SSP 3/21/02
		// Added code for row filtering for version 2
		//
//		#region Row filtering code
//
//		// SSP 8/1/03
//		// Moved FilterRow event to the base class.
//		//
//		/*
//		/// <summary>
//		/// Called when row filters are evaluated on a row.
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected virtual void OnFilterRow( FilterRowEventArgs e )
//		{
//			FilterRowEventHandler eDelegate = (FilterRowEventHandler)EventsOptimized[EVENT_FILTERROW];
//
//			if ( eDelegate != null )
//				eDelegate( this, e );
//		}
//		*/
//
//		/// <summary>
//		/// Called before row filters drop down is dopped down.
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected virtual void OnBeforeRowFilterDropDown( BeforeRowFilterDropDownEventArgs e )
//		{
//			BeforeRowFilterDropDownEventHandler eDelegate = (BeforeRowFilterDropDownEventHandler)EventsOptimized[EVENT_BEFOREROWFILTERDROPDOWN];
//
//			if ( eDelegate != null )
//				eDelegate( this, e );
//		}
//
//		// SSP 4/15/04 - Virtual Binding
//		// Added BeforeRowFilterDropDownPopulate to give the users a chance to prevent the ultragrid
//		// from populating the filter drop down list. This can be useful if there are lot of rows
//		// and it takes a long time for the ultragrid to populate the filter drop down list.
//		// 
//		/// <summary>
//		/// Called before UltraGrid populates the row filter drop down.
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected virtual void OnBeforeRowFilterDropDownPopulate( BeforeRowFilterDropDownPopulateEventArgs e )
//		{
//			BeforeRowFilterDropDownPopulateEventHandler eDelegate = (BeforeRowFilterDropDownPopulateEventHandler)EventsOptimized[EVENT_BEFOREROWFILTERDROPDOWNPOPULATE];
//
//			if ( eDelegate != null )
//				eDelegate( this, e );
//		}
//
//		/// <summary>
//		/// Called before grid displays custom row filters dialog.
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected virtual void OnBeforeCustomRowFilterDialog( BeforeCustomRowFilterDialogEventArgs e )
//		{
//			BeforeCustomRowFilterDialogEventHandler eDelegate = (BeforeCustomRowFilterDialogEventHandler)EventsOptimized[EVENT_BEFORECUSTOMROWFILTERDIALOG];
//
//			if ( eDelegate != null )
//				eDelegate( this, e );
//		}
//
//		#endregion // Row filtering code
		#endregion Moved to UltraGridBase


		// SSP 5/16/02
		// Added methods for firing sumamry events, BeforeSummaryDialog and SummaryValueChanged
		//
		/// <summary>
		/// Fires BeforeSummaryDialog event. This method is called before grid displays the summary dialog for a column.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnBeforeSummaryDialog( BeforeSummaryDialogEventArgs e )
		{
			BeforeSummaryDialogEventHandler eDelegate = (BeforeSummaryDialogEventHandler)EventsOptimized[EVENT_BEFORESUMMARYDIALOG];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		/// <summary>
		/// Fires SummaryValueChanged event. This method is called when a summary value is calculated.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnSummaryValueChanged( SummaryValueChangedEventArgs e )
		{
			SummaryValueChangedEventHandler eDelegate = (SummaryValueChangedEventHandler)EventsOptimized[EVENT_SUMMARYVALUECHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		//
		// JAS 9/22/04 UWG3536 - The tooltips need to use the same font as the control.
		//
		/// <summary>
		/// Overrides the OnFontChanged method of Control.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnFontChanged( EventArgs e )
		{
			base.OnFontChanged( e );
			if( this.toolTip != null )
				this.TooltipTool.Font = this.Font;
		}

		#endregion		
		

		/// <summary>
		/// Returns a reference to a Selected object containing collections of all the selected objects in the grid. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>Selected</b> property of the UltraWinGrid is used to work with any of the currently selected items in the grid. It provides access to the Selected object, which contains three collection sub-objects. Each collection holds one type of selected object; there are collections for rows, columns and cells. Whenever an UltraGridRow, UltraGridColumn or UltraGridCell object in the grid is selected, it is added to the corresponding collection of the Selected object. Deselecting an object removes it from the collection.</p>
		///	<p class="body">You can use the <b>Selected</b> property to iterate through the selected items of any type, or to examine or change the properties of any selected item.</p>
		///	<p>
		/// The following sample code copies CustomerID and ContactName fields of all the selected rows to the clipboard as text. It assumes that these fields exist in the table that the selected rows are from and that the rows are not UltraGridGroupByRows.
		/// </p>
		/// <p></p>
		/// <p>C#:</p>
		/// <p></p>
		/// <code>
		/// private void button1_Click(object sender, System.EventArgs e)
		/// {
		/// 	Infragistics.Win.UltraWinGrid.SelectedRowsCollection selectedRows;
		/// 
		/// 	// Get the selected rows.
		/// 	//
		/// 	selectedRows = this.ultraGrid1.Selected.Rows;
		/// 
		/// 	// If there are no selected rows, return
		/// 	//
		/// 	if ( selectedRows.Count &lt; 1 )
		/// 		return;
		/// 
		/// 	System.Text.StringBuilder sb = new System.Text.StringBuilder( );
		/// 
		/// 	// Loop through all the selected rows
		/// 	//
		/// 	for ( int i = 0; i &lt; selectedRows.Count; i++ )
		/// 	{
		/// 		Infragistics.Win.UltraWinGrid.UltraGridRow row;
		/// 
		/// 		row = selectedRows[i];
		/// 
		/// 		// Use Cells collection to get the values for 
		/// 		// CustomerID and ContactName columns.
		/// 		//
		/// 		sb.Append( row.Cells[ "CustomerID" ].Text );
		/// 		sb.Append( "," );
		/// 		sb.Append( row.Cells["ContactName"].Text );
		/// 
		/// 		sb.Append( "\r\n" );
		/// 	}
		/// 
		/// 	// Copy the text to the clipboard.
		/// 	//
		/// 	System.Windows.Forms.Clipboard.SetDataObject( sb.ToString( ) );
		/// }
		/// </code>
		/// <seealso cref="UltraGridOverride.AllowMultiCellOperations"/>
		///	</remarks>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public Selected Selected
		{
			get
			{
				if ( this.selected == null )
				{
					this.selected = new Selected();

					// SSP 12/13/02 UWG1835
					// Added a backreference to the grid in the selected.
					//
					this.selected.Initialize( this );
				}

				return this.selected;
			}
		}

		
		// SSP 7/22/02 UWG1344
		//
		internal bool HasSelectedBeenAllocated
		{
			get
			{
				return null != this.selected;
			}
		}

		// SSP 2/20/07 BR19262
		// 
		internal Selected SelectedIfAllocated
		{
			get
			{
				return this.selected;
			}
		}


		private Selected InitialSelection
		{
			get
			{
				if ( this.initialSelection == null )
					this.initialSelection = new Selected();

				return this.initialSelection;
			}
		}

		

		internal Icon ActiveRowIcon
		{
			get
			{
				if ( this.activeRowIcon == null )
				{

					// load the icon from the manifest resources. 
					//
					// SSP 6/14/02 UWG1238
					// this.GetType returns the derived class' type and Module.Assembly will be
					// the assembly that the derived class if from and the resources are in
					// our assembly. So below statemenet won't work if we use GetType( ).
					// Instead use the typeof operator with our class name as the operand.
					//
					//System.IO.Stream stream = this.GetType().Module.Assembly.GetManifestResourceStream(this.GetType(), "ActiveRow.ico");
					System.IO.Stream stream = 
						typeof( UltraGrid ).Module.Assembly.GetManifestResourceStream(typeof( UltraGrid ), "ActiveRow.ico");
					
					if (stream != null)
					{
						// AS - 2/20/02
						// Clone the icon so we can close the stream.
						//
						// MD 3/24/08 - BR31443
						// We were not disposing the icon we cloned from, which caused a memory leak.
						//this.activeRowIcon = new Icon(stream, 16, 16).Clone() as Icon;
						using ( Icon temp = new Icon( stream, 16, 16 ) )
							this.activeRowIcon = temp.Clone() as Icon;

						
						stream.Close();
					}

				}

				return this.activeRowIcon;
			}
		}


		internal Icon ActiveRowAddedIcon
		{
			get
			{
				if ( this.activeRowAddedIcon == null )
				{
					// load the icon from the manifest resources. 
					//
					// SSP 6/14/02 UWG1238
					// this.GetType returns the derived class' type and Module.Assembly will be
					// the assembly that the derived class if from and the resources are in
					// our assembly. So below statemenet won't work if we use GetType( ).
					// Instead use the typeof operator with our class name as the operand.
					//
					//System.IO.Stream stream = this.GetType().Module.Assembly.GetManifestResourceStream(this.GetType(), "ActiveRowAdded.ico");
					System.IO.Stream stream = 
						typeof( UltraGrid ).Module.Assembly.GetManifestResourceStream(typeof( UltraGrid ), "ActiveRowAdded.ico");

					if (stream != null)
					{
						// AS - 2/20/02
						// Clone the icon so we can close the stream.
						//
						// MD 3/24/08 - BR31443
						// We were not disposing the icon we cloned from, which caused a memory leak.
						//this.activeRowAddedIcon = new Icon(stream, 16, 16).Clone() as Icon;
						using ( Icon temp = new Icon( stream, 16, 16 ) )
							this.activeRowAddedIcon = temp.Clone() as Icon;

						stream.Close();
					}
				}

				return this.activeRowAddedIcon;
			}
		}

		internal Icon ActiveRowModifiedIcon
		{
			get
			{
				if ( this.activeRowModifiedIcon == null )
				{
					// load the icon from the manifest resources. 
					//
					// SSP 6/14/02 UWG1238
					// this.GetType returns the derived class' type and Module.Assembly will be
					// the assembly that the derived class if from and the resources are in
					// our assembly. So below statemenet won't work if we use GetType( ).
					// Instead use the typeof operator with our class name as the operand.
					//
					//System.IO.Stream stream = this.GetType().Module.Assembly.GetManifestResourceStream(this.GetType(), "ActiveRowModified.ico");
					System.IO.Stream stream =
						typeof( UltraGrid ).Module.Assembly.GetManifestResourceStream(typeof( UltraGrid ), "ActiveRowModified.ico");
					
					if (stream != null)
					{
						// AS - 2/20/02
						// Clone the icon so we can close the stream.
						//
						// MD 3/24/08 - BR31443
						// We were not disposing the icon we cloned from, which caused a memory leak.
						//this.activeRowModifiedIcon = new Icon(stream, 16, 16).Clone() as Icon;
						using ( Icon temp = new Icon( stream, 16, 16 ) )
							this.activeRowModifiedIcon = temp.Clone() as Icon;

						stream.Close();
					}
				}

				return this.activeRowModifiedIcon;
			}
		}

		internal Icon RowAddedIcon
		{
			get
			{
				if ( this.rowAddedIcon == null )
				{
					// load the icon from the manifest resources. 
					//
					// SSP 6/14/02 UWG1238
					// this.GetType returns the derived class' type and Module.Assembly will be
					// the assembly that the derived class if from and the resources are in
					// our assembly. So below statemenet won't work if we use GetType( ).
					// Instead use the typeof operator with our class name as the operand.
					//
					//System.IO.Stream stream = this.GetType().Module.Assembly.GetManifestResourceStream(this.GetType(), "RowAdded.ico");
					System.IO.Stream stream = 
						typeof( UltraGrid ).Module.Assembly.GetManifestResourceStream(typeof( UltraGrid ), "RowAdded.ico");
					
					if (stream != null)
					{
						// AS - 2/20/02
						// Clone the icon so we can close the stream.
						//
						// MD 3/24/08 - BR31443
						// We were not disposing the icon we cloned from, which caused a memory leak.
						//this.rowAddedIcon = new Icon(stream, 16, 16).Clone() as Icon;
						using ( Icon temp = new Icon( stream, 16, 16 ) )
							this.rowAddedIcon = temp.Clone() as Icon;

						stream.Close();
					}
				}

				return this.rowAddedIcon;
			}
		}


		internal Icon RowModifiedIcon
		{
			get
			{
				if ( this.rowModifiedIcon == null )
				{
					// load the icon from the manifest resources. 
					//
					// SSP 6/14/02 UWG1238
					// this.GetType returns the derived class' type and Module.Assembly will be
					// the assembly that the derived class if from and the resources are in
					// our assembly. So below statemenet won't work if we use GetType( ).
					// Instead use the typeof operator with our class name as the operand.
					//
					//System.IO.Stream stream = this.GetType().Module.Assembly.GetManifestResourceStream(this.GetType(), "RowModified.ico");
					System.IO.Stream stream = 
						typeof( UltraGrid ).Module.Assembly.GetManifestResourceStream( typeof( UltraGrid ), "RowModified.ico");
					
					if (stream != null)
					{
						// AS - 2/20/02
						// Clone the icon so we can close the stream.
						//
						// MD 3/24/08 - BR31443
						// We were not disposing the icon we cloned from, which caused a memory leak.
						//this.rowModifiedIcon = new Icon(stream, 16, 16).Clone() as Icon;
						using ( Icon temp = new Icon( stream, 16, 16 ) )
							this.rowModifiedIcon = temp.Clone() as Icon;

						stream.Close();
					}
				}

				return this.rowModifiedIcon;
			}
		}

		// SSP 4/9/07 BR20818 - Optimizations
		// 
		#region Fix for BR20818

		private Hashtable cachedBitmapsFromIcons = new Hashtable( );
		private class BitmapFromIconCache
		{
            // CDS 9.2 RowSelector State-Specific Images
            //private Icon icon;
            private Image icon;
            private Bitmap bitmap;
			private Color lastForeColor;
			private Point[] transparentPixels;

            //internal BitmapFromIconCache( Icon icon )
            //{
            //    this.icon = icon;
            //}

            // CDS 9.2 RowSelector State-Specific Images
            internal BitmapFromIconCache(object icon)
            {
                if (icon is Image)
                    this.icon = (Image)icon;
                else if (icon is Icon)
                    this.icon = ((Icon)icon).ToBitmap();
            }

			internal Bitmap GetBitmap( Color foreColor )
			{
				if ( null == this.bitmap )
                {
                    // CDS 9.2 RowSelector State-Specific Images
                    //Bitmap bmp = this.icon.ToBitmap();
                    Bitmap bmp = new Bitmap(this.icon);

					System.Collections.Generic.List<Point> transparentPixels =
						new System.Collections.Generic.List<Point>( );

					int ww = bmp.Width;
					int hh = bmp.Height;
					for ( int y = 0; y < hh; y++ )
					{
						for ( int x = 0; x < ww; x++ )
						{
							Color color = bmp.GetPixel( x, y );

							if ( color.R == 255 && color.G == 0 && color.B == 0 )
								transparentPixels.Add( new Point( x, y ) );
						}
					}

					this.transparentPixels = transparentPixels.ToArray( );
					this.bitmap = bmp;
				}

				if ( this.lastForeColor != foreColor )
				{
					Bitmap bmp = this.bitmap;

					Point[] points = this.transparentPixels;
					for ( int i = 0; i < points.Length; i++ )
					{
						Point p = points[i];
						bmp.SetPixel( p.X, p.Y, foreColor );
					}

					this.lastForeColor = foreColor;
				}

				return this.bitmap;
			}
		}

        // CDS 9.2 RowSelector State-Specific Images
        //internal Bitmap GetCachedBitmapFromIcon(Icon icon, Color foreColor)
  		internal Bitmap GetCachedBitmapFromIcon( object icon, Color foreColor )
        {
			if ( null == this.cachedBitmapsFromIcons )
				this.cachedBitmapsFromIcons = new Hashtable( );

			BitmapFromIconCache cache = (BitmapFromIconCache)this.cachedBitmapsFromIcons[icon];
			if ( null == cache )
				this.cachedBitmapsFromIcons[icon] = cache = new BitmapFromIconCache( icon );

			return cache.GetBitmap( foreColor );
		}

		#endregion // Fix for BR20818

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose( bool disposing )
		{

			if ( disposing )
			{
				// SSP 4/26/02
				// Em Embeddable editors.
				// Commented below code out.
				

				if ( this.license != null) 
				{
					this.license.Dispose();
					this.license = null;
				}

				if ( this.activeRowIcon != null )
				{
					this.activeRowIcon.Dispose();
					this.activeRowIcon = null;
				}

				// SSP 6/23/05
				// 
				// ----------------------------------------------------------------
				if ( null != this.activeRowAddedIcon )
				{
					this.activeRowAddedIcon.Dispose( );
					this.activeRowAddedIcon = null;
				}

				if ( null != this.activeRowModifiedIcon )
				{
					this.activeRowModifiedIcon.Dispose( );
					this.activeRowModifiedIcon = null;
				}

				if ( null != this.filterDropDown )
				{
					this.filterDropDown.Dispose( );
					this.filterDropDown = null;
				}

				if ( null != this.rowAddedIcon )
				{
					this.rowAddedIcon.Dispose( );
					this.rowAddedIcon = null;
				}

				if ( null != this.rowModifiedIcon )
				{
					this.rowModifiedIcon.Dispose( );
					this.rowModifiedIcon = null;
				}

				if ( null != this.swapDropDown )
				{
					this.swapDropDown.Dispose( );
					this.swapDropDown = null;
				}

                // CDS 9.2 Column Moving Indicators
                if (null != this.defaultDropIndicatorDownArrowImage)
                {
                    this.defaultDropIndicatorDownArrowImage.Dispose();
                    this.defaultDropIndicatorDownArrowImage = null;
                }

                // CDS 9.2 Column Moving Indicators
                if (null != this.colorizableDropIndicatorDownArrowImage)
                {
                    this.colorizableDropIndicatorDownArrowImage.Dispose();
                    this.colorizableDropIndicatorDownArrowImage = null;
                }
                // ----------------------------------------------------------------

				// SSP 6/23/05 BR04612
				// Dispose the tooltip as well.
				// 
				if ( null != this.toolTip )
				{
					// SSP 2/23/06 BR10351
					// Unhook from the tooltip's control as well.
					// 
					if ( null != this.toolTip.Control )
					{
						this.toolTip.Control.MouseDown -= new MouseEventHandler( this.ToolTip_MouseDown );
						this.toolTip.Control.MouseUp -= new MouseEventHandler( this.ToolTip_MouseUp );
						this.toolTip.Control.Click -= new EventHandler( this.ToolTip_Click );
					}

					this.toolTip.Dispose( );
					this.toolTip = null;
				}

                // MBS 3/12/08 - RowEditTemplate NA2008 V2
                this.UnhookComponentChangeService();
			}

			base.Dispose( disposing );

			// SSP 10/28/03 UWG2573
			// Set the activeColScrollRegion and activeRowScrollRegion to null so that if
			// for some reason some external object is holding a reference to UltraGrid
			// and preventing it from being collected by the garbase collector, then at 
			// least row bands/scrollregions/rows/cells and other grid objects get collected.
			//
			// ------------------------------------------------------------------------------
			if ( disposing )
			{
				this.activeCell = null;
				this.eventManager = null;
			}
			// ------------------------------------------------------------------------------
		}

		/// <summary>
		/// The object that enables, disables and controls firing of specific control events.
		/// </summary>
		/// <remarks>
		/// <p class="body">The UltraWinGrid event manager gives you a high degree of control over how the control invokes event procedures. You can use it to selectively enable and disable event procedures depending on the context of your application. You can also use the event manager to return information about the state of the control's events.</p>
        /// <p class="body">The event manager's methods are used to determine the enabled state of an event (<see cref="Infragistics.Win.UltraWinGrid.GridEventManager.IsEnabled(GridEventIds)"/>), to selectively enable or disable events (<see cref="Infragistics.Win.UltraWinGrid.GridEventManager.SetEnabled(GridEventIds,bool)"/>), and to tell whether an event procedure is currently being processed (<see cref="Infragistics.Win.UltraWinGrid.GridEventManager.InProgress"/>). There is also an <seealso cref="EventManagerBase.AllEventsEnabled"/> property that you can check to quickly determine whether any events have been disabled by the event manager.</p>
        /// </remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.UltraWinGrid.GridEventManager EventManager
		{
			get
			{
				if ( null == this.eventManager )
					this.eventManager = new Infragistics.Win.UltraWinGrid.GridEventManager( this );

				return this.eventManager;
			}
		}

		/// <summary>
		/// Returns bit flags that signify the current editing state of the control.
		/// </summary>
		/// <remarks>
		/// The <b>CurrentState</b> property is used primarily in conjunction with the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.KeyActionMappings"/><b>KeyActionMappings</b> property and the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.PerformAction(UltraGridAction)"/> method to return information about the state of the control with regards to user interaction. The setting of the <b>CurrentState</b> property indicates which object has focus in the control, whether the user has placed the control into edit mode, and other information such as whether a combo box is dropped down or whether a row is expanded.
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)]
		public UltraGridState CurrentState
		{
			get
			{
				int state = 0;

				// SSP 6/26/02
				// If the swap drop down is dropped down, then set the state
				// to IsSwapDropDownDroppedDown. We need one for swap drop down
				// because we want to disallow other actions if the swap drop
				// down is dropped down.
				//
				if ( null != this.swapDropDown && 
					((IValueList)this.swapDropDown).IsDroppedDown )
				{
					state |= (int)UltraGridState.SwapDroppedDown;
				}

				// set the current state
				//
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.ActiveCell != null )
				UltraGridCell activeCell = this.ActiveCell;

				if ( activeCell != null )
				{
					state |= (int) UltraGridState.Cell;

					// see if there are any visible columns in this band before or after
					// this cell's column
					//

					//RobA 10/27/01 implemented setting the state to CellFirst
					//and CellLast.
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//UltraGridColumn prevColumn = this.ActiveCell.Band.GetRelatedVisibleCol ( this.activeCell.Column, NavigateType.Prev, false );
					//UltraGridColumn nextColumn = this.ActiveCell.Band.GetRelatedVisibleCol ( this.activeCell.Column, NavigateType.Next, false );
					UltraGridColumn prevColumn = activeCell.Band.GetRelatedVisibleCol( this.activeCell.Column, NavigateType.Prev, false );
					UltraGridColumn nextColumn = activeCell.Band.GetRelatedVisibleCol( this.activeCell.Column, NavigateType.Next, false );

					// set the first and/or last bits accordingly
					//
					if ( prevColumn == null )
						state |= (int)UltraGridState.CellFirst;

					if ( nextColumn == null )
						state |= (int)UltraGridState.CellLast;

					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( this.ActiveCell.IsInEditMode )
					if ( activeCell.IsInEditMode )
					{
						state |= (int)UltraGridState.InEdit; 

						// SSP 2/17/06 BR09132
						// If the cell is in edit mode and it's a multi-line cell then don't process
						// the CommitRow action since the Enter key in such scenario should cause a carriage
						// return to be entered into the cell.
						// 
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( DefaultableBoolean.True == this.ActiveCell.Column.CellMultiLineResolved )
						if ( DefaultableBoolean.True == activeCell.Column.CellMultiLineResolved )
							state |= (int)UltraGridState.CellMultiline;
					}

					//ROBA UWG142 8/15/01
					// if we have a dropdown dropped down, set state appropriately
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					// Use the new HasDropDown property off the cell.
					//
					//if ( this.ActiveCell.Column.HasDropDown )
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( this.ActiveCell.HasDropDown )
					if ( activeCell.HasDropDown )
					{
						state |= (int)UltraGridState.HasDropdown;

						// SSP 4/23/02
						// Embeddable editors. Use the editor's IsDroppedDown instead of
						// the value lists'.
						//
						// SSP 5/1/03 - Cell Level Editor
						// Added Editor and ValueList properties off the cell so the user can set the editor
						// and value list on a per cell basis.
						//
						//EmbeddableEditorBase editor = this.ActiveCell.Column.Editor;
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//EmbeddableEditorBase editor = this.ActiveCell.EditorResolved;
						EmbeddableEditorBase editor = activeCell.EditorResolved;

						if ( null != editor && editor.SupportsDropDown && editor.IsDroppedDown )
							state |= (int)UltraGridState.IsDroppedDown;
						
					}
					
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
					//if ( this.ActiveCell.Column.StyleResolved == ColumnStyle.CheckBox ||
					//	this.ActiveCell.Column.StyleResolved == ColumnStyle.TriStateCheckBox )
					//	state |= (int)UltraGridState.IsCheckbox;
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( this.ActiveCell.StyleResolved == ColumnStyle.CheckBox ||
					//    this.ActiveCell.StyleResolved == ColumnStyle.TriStateCheckBox )
					if ( activeCell.StyleResolved == ColumnStyle.CheckBox ||
						activeCell.StyleResolved == ColumnStyle.TriStateCheckBox )
						state |= (int)UltraGridState.IsCheckbox;
				}

				if ( this.ActiveRow != null )
				{
					state |= (int)UltraGridState.Row; 

					// see if there are any visible rows before or after this
					// row and set the apprpriate bits
					//
					// SSP 11/11/03 Add Row Feature
					//
					//if ( !this.ActiveRow.HasPrevSibling(false, true) )
					// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// We have to take into account filter row, summary row etc...
					// Replaced TemplateAddRow enum member with SpecialRows.
					// 
					//if ( !this.ActiveRow.HasPrevSibling( false, true, IncludeRowTypes.TemplateAddRow ) )
					if ( !this.ActiveRow.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) )
					{
						state |= (int)UltraGridState.RowFirstChild;

						if ( !this.ActiveRow.HasSiblingInPrevSiblingBand(true) )
						{
							state |= (int)UltraGridState.RowFirst;

							// SSP 9/18/02 UWG1120
							// If this is the first absolute row in the grid, then assign the
							// newly added state of FirstRowInGrid. This was done to fix above
							// bug where pressing a tab when on the last absolute row should
							// got to the next control. And the same for the first absolute 
							// row.
							//
							if ( this.ActiveRow.IsAbsoluteFirstRow( ) )
								state |= (int)UltraGridState.FirstRowInGrid;
						}
					}

					// SSP 11/11/03 Add Row Feature
					//
					//if ( !this.ActiveRow.HasNextSibling(false, true) )
					// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// We have to take into account filter row, summary row etc...
					// Replaced TemplateAddRow enum member with SpecialRows.
					// 
					//if ( !this.ActiveRow.HasNextSibling( false, true, IncludeRowTypes.TemplateAddRow ) )
					if ( ! this.ActiveRow.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) )
					{
						state |= (int)UltraGridState.RowLastChild;

						if ( !this.ActiveRow.HasSiblingInNextSiblingBand(true) )
						{
							state |= (int)UltraGridState.RowLast;
							
							// SSP 9/18/02 UWG1120
							// If this is the first absolute row in the grid, then assign the
							// newly added state of FirstRowInGrid. This was done to fix above
							// bug where pressing a tab when on the last absolute row should
							// got to the next control. And the same for the first absolute 
							// row.
							//
							if ( this.ActiveRow.IsAbsoluteLastRow( true ) )
								state |= (int)UltraGridState.LastRowInGrid;
						}
					}

					if ( this.ActiveRow.IsExpanded && !this.ActiveRow.IsCard)
						state |= (int)UltraGridState.RowExpanded;

					// SSP 12/16/02 UWG1893
					// Instead of checking the IsExpandable property which checks the rows count,
					// check the HasExpansionIndicator property which is based on the expansion indicator
					// settings. The problem with IsExpandable is that it checks if there are any child
					// rows and in doing so it causes the child rows collection to be loaded. This effects
					// performance severely.
					//
					//if ( this.ActiveRow.IsExpandable )
//					if ( this.ActiveRow.HasExpansionIndicator )
//						state |= (int)UltraGridState.RowExpandable;
					//
					// JAS 2005 v2 GroupBy Row Extensions 
					//  Groupby rows need to be treated differently than normal rows.
					//
					if( ! this.ActiveRow.IsGroupByRow )
					{
						if ( this.ActiveRow.HasExpansionIndicator )
							state |= (int)UltraGridState.RowExpandable;
					}
					else
					{
						if( this.ActiveRow.IsExpandable )
							state |= (int)UltraGridState.RowExpandable;
					}
 
					if ( this.ActiveRow.ParentRow != null )
						state |= (int)UltraGridState.RowChild; 					 


					// SSP 11/14/01 UWG715
					//
					if ( this.ActiveRow is UltraGridGroupByRow )
						state |= (int)UltraGridState.GroupByRow;


					// SSP 7/19/02 UWG1379
					// If the active row is dirty or is a new row then add the RowDirty
					// state so the key action mapping for Escape get's executed if
					// Escape key is pressed.
					//
					if ( this.ActiveRow.DataChanged || this.ActiveRow.IsAddRow )
						state |= (int)UltraGridState.RowDirty;

					// SSP 4/23/05 - NAS 5.2 Filter Row/Fixed Add Row
					// Added CommitRow action. This is for applying filters when Enter key is 
					// pressed while the filter row is active depending on the 
					// FilterEvaluationTrigger settings. Also it's used for committing the
					// add-row. It can be used to commit regular rows as well however for that
					// the developer has to add a key action mapping.
					//
					// ------------------------------------------------------------------------
					if ( this.ActiveRow.IsFilterRow )
						state |= (int)UltraGridState.FilterRow;

					if ( this.ActiveRow.IsAddRow )
						state |= (int)UltraGridState.AddRow;
					// ------------------------------------------------------------------------
				}
				
				// SSP 11/22/05 - NAS 6.1 Multi-cell Operations Support
				// 
				if ( null != this.DisplayLayout )
					state |= (int)this.DisplayLayout.ActionHistory.MultiCellOperationInfo.GetMultiCellOperationRelatedState( );

				return (UltraGridState) state;
			}
		}

		/// <summary>
		/// Returns or sets the selection strategy filter for the control.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can specify what type of selection is allowed for various types of objects by using the properties of the UltraGridOverride object. The properties <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.SelectTypeCell"/>, <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.SelectTypeCol"/>, <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.SelectTypeGroupByRow"/> and <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.SelectTypeRow"/> are used to specify a selection strategy for the corresponding type of object.</p>
		/// <p class="body">UltraWinGrid uses <i>selection strategies</i> to control the types of selection the user can perform in the control. A selection strategy is an extended set of attributes covering options such as multiple vs. single selection, range vs. discrete selection, non-contiguous selection, what types of objects may be selected and how selection is initiated and completed using the keyboard and mouse. Selection strategies are implemented using the SelectionManager object and the ISelectionFilter interface. Selection strategies are defined in the Infragistics.Win assembly.</p>
		/// <p class="body">UltraWinGrid includes pre-defined strategies for the most common types of selection that are performed in a grid. You can also create your own selection strategies by deriving your own selection filter classes from those supplied and overriding selected virtual methods.</p>
		/// <p class="body">Since selection is primarily a function of the user interface, selection logic is based off of the UIElement for an object. The <b>Selectable</b> property of the UIElement determines whether the object can be selected.</p>
		/// <p class="body">The following table lists the default key mappings for the <see cref="UltraGrid"/> control:<br></br>
		/// <TABLE BORDER="1" WIDTH="100%">
		/// <THEAD>
		/// <TR>
		/// <TH>KeyCode</TH>
		/// <TH>ActionCode</TH>
		/// <TH>StateRequired</TH>
		/// <TH>StateDisallowed</TH>
		/// <TH>SpecialKeysRequired</TH>
		/// <TH>SpecialKeysDisallowed</TH>
		/// </TR>
		/// </THEAD>
		/// <TBODY>
		/// <TR>
		/// <TD class="body">Right</TD><TD class="body">NextCell</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Tab</TD><TD class="body">NextCellByTab</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">None</TD><TD class="body">All</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Left</TD><TD class="body">PrevCell</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Tab</TD><TD class="body">PrevCellByTab</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">Shift</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Up</TD><TD class="body">AboveCell</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Down</TD><TD class="body">BelowCell</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Home</TD><TD class="body">FirstRowInBand</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">End</TD><TD class="body">LastRowInBand</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Right</TD><TD class="body">FirstRowInGrid</TD><TD class="body">None</TD><TD class="body">Row</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Down</TD><TD class="body">FirstRowInGrid</TD><TD class="body">None</TD><TD class="body">Row</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Home</TD><TD class="body">FirstRowInGrid</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">Ctrl</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">End</TD><TD class="body">LastRowInGrid</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">Ctrl</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Right</TD><TD class="body">ExpandRow</TD><TD class="body">RowExpandable</TD><TD class="body">Cell, RowExpanded</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Left</TD><TD class="body">CollapseRow</TD><TD class="body">RowExpanded</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Right</TD><TD class="body">NextRow</TD><TD class="body">Row</TD><TD class="body">Cell, RowExpandable</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Right</TD><TD class="body">NextRow</TD><TD class="body">Row, RowExpanded</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Tab</TD><TD class="body">NextRowByTab</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">All</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Left</TD><TD class="body">PrevRow</TD><TD class="body">Row</TD><TD class="body">Cell, RowExpanded</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Tab</TD><TD class="body">PrevRowByTab</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">Shift</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Up</TD><TD class="body">AboveRow</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Down</TD><TD class="body">BelowRow</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Space</TD><TD class="body">ToggleCheckbox</TD><TD class="body">InEdit, IsCheckbox</TD><TD class="body">None</TD><TD class="body">None</TD><TD class="body">All</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Space</TD><TD class="body">ToggleCellSel</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">All</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Space</TD><TD class="body">ToggleRowSel</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">All</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Space</TD><TD class="body">DeactivateCell</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">Ctrl</TD><TD class="body">AltShift</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Space</TD><TD class="body">ActivateCell</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">Ctrl</TD><TD class="body">AltShift</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Right</TD><TD class="body">NextCellInBand</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">Ctrl</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Left</TD><TD class="body">PrevCellInBand</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">Ctrl</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Home</TD><TD class="body">FirstCellInRow</TD><TD class="body">Cell</TD><TD class="body">CellFirst, InEdit</TD><TD class="body">None</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">End</TD><TD class="body">LastCellInRow</TD><TD class="body">Cell</TD><TD class="body">CellLast, InEdit</TD><TD class="body">None</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Home</TD><TD class="body">FirstCellInBand</TD><TD class="body">CellFirst</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">End</TD><TD class="body">LastCellInBand</TD><TD class="body">CellLast</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Home</TD><TD class="body">FirstCellInGrid</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">Ctrl</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">End</TD><TD class="body">LastCellInGrid</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">Ctrl</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Prior</TD><TD class="body">PageUpCell</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Next</TD><TD class="body">PageDownCell</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Prior</TD><TD class="body">PageUpRow</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Next</TD><TD class="body">PageDownRow</TD><TD class="body">Row</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Escape</TD><TD class="body">UndoCell</TD><TD class="body">InEdit</TD><TD class="body">IsDroppedDown</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Escape</TD><TD class="body">UndoRow</TD><TD class="body">Row</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Escape</TD><TD class="body">CloseDropdown</TD><TD class="body">IsDroppedDown</TD><TD class="body">None</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Enter</TD><TD class="body">CloseDropdown</TD><TD class="body">IsDroppedDown</TD><TD class="body">None</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Enter</TD><TD class="body">ExpandRow</TD><TD class="body">GroupByRow</TD><TD class="body">IsDroppedDown, RowExpanded</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Enter</TD><TD class="body">CollapseRow</TD><TD class="body">RowExpanded, GroupByRow</TD><TD class="body">IsDroppedDown</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">F4</TD><TD class="body">ToggleDropdown</TD><TD class="body">HasDropdown, InEdit</TD><TD class="body">None</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Up</TD><TD class="body">ToggleDropdown</TD><TD class="body">HasDropdown, InEdit</TD><TD class="body">None</TD><TD class="body">Alt</TD><TD class="body">None</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Down</TD><TD class="body">ToggleDropdown</TD><TD class="body">HasDropdown, InEdit</TD><TD class="body">None</TD><TD class="body">Alt</TD><TD class="body">None</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">F2</TD><TD class="body">ToggleEditMode</TD><TD class="body">Cell</TD><TD class="body">None</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">F4</TD><TD class="body">EnterEditModeAndDropdown</TD><TD class="body">HasDropdown</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Up</TD><TD class="body">EnterEditModeAndDropdown</TD><TD class="body">HasDropdown</TD><TD class="body">InEdit</TD><TD class="body">Alt</TD><TD class="body">None</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Down</TD><TD class="body">EnterEditModeAndDropdown</TD><TD class="body">HasDropdown</TD><TD class="body">InEdit</TD><TD class="body">Alt</TD><TD class="body">None</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">F6</TD><TD class="body">NextRegion</TD><TD class="body">None</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">All</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">F6</TD><TD class="body">PrevRegion</TD><TD class="body">None</TD><TD class="body">InEdit</TD><TD class="body">Shift</TD><TD class="body">AltCtrl</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">Delete</TD><TD class="body">DeleteRows</TD><TD class="body">Row</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">All</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">F2</TD><TD class="body">EnterEditMode</TD><TD class="body">Cell</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// <TR>
		/// <TD class="body">F2</TD><TD class="body">ExitEditMode</TD><TD class="body">InEdit</TD><TD class="body">None</TD><TD class="body">None</TD><TD class="body">Alt</TD>
		/// </TR>
		/// </TBODY>
		/// </TABLE>
		/// </p>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)]
		public ISelectionStrategyFilter SelectionStrategyFilter
		{
			get
			{
				return this.selectionStrategyFilter;
			}
			set 
			{
				if ( value != this.selectionStrategyFilter )
				{
					this.selectionStrategyFilter = value;
			
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.SelectionStrategyFilter );
				}
			}
		}

		// SSP 10/10/03
		// This property is not used anymore. Commented out. We implemented framework-wide change
		// to fix the bug that this was added in the first to fix.
		//
		

		/// <summary>
		/// Simulates user interaction with the control.
		/// </summary>
		/// <returns>True if the action was performed successfully.</returns>
		///	<remarks>
		///	<p class="body">Invoke this method to simulate an action the user can perform.</p>
		///	<p class="body">Many actions are only appropriate in certain situations; if an action is inappropriate, it will not be performed. For example, attempting to delete rows by performing the DeleteRows action (37 - KeyActionDeleteRows) will have no effect if no rows are selected. Similarly, an attempt to toggle a cell's dropdown list by performing a droptown toggle action (14 - KeyActionToggleDropdown) will also be ignored if the column does not have a dropdown list associated with it.</p>
		///	<p class="body">You can use the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.CurrentState"/> property to determine the state of the control when the action is about to be performed.</p>
		///	</remarks>
        ///	<param name="actionCode">An <see cref="UltraGridAction"/> enumeration value that determines the user action to be performed.</param>
        ///	<param name="control">A boolean specifies whether the action should be performed as if the control key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is maintained, as it is when the user holds down the control key and selects a row in a grid.</param>
        ///	<param name="shift">A boolean specifies whether the action should be performed as if the shift key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is extended, as it is when the user holds down the shift key and selects a range of rows in a grid.</param>        
        ///	<returns>true if the action completed successfully, false if the action failed.</returns>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.CurrentState"/>
		public virtual bool PerformAction( UltraGridAction actionCode, bool shift, bool control ) 
		{
			// SSP 8/2/06 BR14616
			// Typically when certain changes are made to the datasource, the row collection is marked
			// dirty and when it's accessed the next time (like during the next paint), it's verified. 
			// However CurrentState property of the grid does not verify the rows. As a result when rows 
			// are dirty, certain actions may not be allowed becauses rows haven't been synced with the 
			// datasource yet.
			// 
			if ( null != this.DisplayLayout )
				this.DisplayLayout.EnsureRowsSyncedWithDataSource( );

			// SSP 8/20/02 UWG1583
			// When the user calls this method to perform an action, we shouldn't check
			// if that action is allowed by looking at the current key action mappings
			// collection which he could have modified but rather the default
			// key action mappings collection. We don't want to restrict the user
			// from performing only the actions that are in current key action mappings
			// collection.
			//
			// -------------------------------------------------------------------
			// see if action is allowed
			//if ( this.KeyActionMappings.IsActionAllowed( actionCode, this.CurrentState ) )
			//	return this.DisplayLayout.PerformAction( actionCode, shift, control );
			//if ( this.AllActionsMappings.IsActionAllowed( actionCode, (long)this.CurrentState ) )
			//DA 1.14.03 Modified to use KeyActionMappings.IsActionAllowed, which now keeps actions
			//in separate table.
			if ( this.KeyActionMappings.IsActionAllowed( actionCode, (long)this.CurrentState ) )
				return this.DisplayLayout.PerformAction( actionCode, shift, control );
			// -------------------------------------------------------------------

			return false;
		}

		// SSP 7/25/02 DNF19
		//
		/// <summary>
		/// An overload of PerformAction that passes in shift and control parameters as false.
		/// </summary>
        ///	<param name="actionCode">An <see cref="UltraGridAction"/> enumeration value that determines the user action to be performed.</param>
        ///	<returns>true if the action completed successfully, false if the action failed.</returns>
		/// <remarks>
		/// <p class="body">
		/// See the <see cref="PerformAction(UltraGridAction, bool, bool)"/> overload for more
		/// information.
		/// </p>
		/// </remarks>        
        public bool PerformAction(UltraGridAction actionCode)
		{
			return this.PerformAction( actionCode, false, false );
		}

		// SSP 2/7/02 UWG847
		// Overrode ProcessMnemonic to process any mnemonics on the 
		// add new buttons of the add new box.
		//
		/// <summary>
        /// Processes a mnemonic character.
		/// </summary>
        /// <param name="charCode">The character to process.</param>
        /// <returns>true if the character was processed as a mnemonic by the control; otherwise, false.</returns>
        // MRS 12/19/06 - fxCop
        [System.Security.Permissions.UIPermission(System.Security.Permissions.SecurityAction.InheritanceDemand, Window = System.Security.Permissions.UIPermissionWindow.AllWindows)]
        [System.Security.Permissions.UIPermission(System.Security.Permissions.SecurityAction.LinkDemand, Window = System.Security.Permissions.UIPermissionWindow.AllWindows)]
        protected override bool ProcessMnemonic(char charCode)
		{
			if ( this.DesignMode )
			{
				return base.ProcessMnemonic( charCode );
			}

			// See if the add new box has any add new button with a mnemonic.
			//
			if ( null != this.DisplayLayout &&
				this.DisplayLayout.HasAddNewBox &&
				!this.DisplayLayout.AddNewBox.Hidden )
			{
				// Get the add new box element so we can use it inside the loop
				// to get the add new button for each band.
				//
				UIElement mainElement = this.DisplayLayout.UIElement;
					
				AddNewBoxUIElement addNewBoxElement = 
					null != mainElement 
					? (AddNewBoxUIElement)mainElement.GetDescendant( typeof( AddNewBoxUIElement ) )
					: null;

				// If no add new box element, then return false.
				//
				if ( null == addNewBoxElement )
					return false;

				// Traverse through all the bands trying to find a add new button
				// that has a matching mnemonic.
				//
				for ( int i = 0; i < this.DisplayLayout.SortedBands.Count; i++ )
				{
					Infragistics.Win.UltraWinGrid.UltraGridBand band = 
						this.DisplayLayout.SortedBands[i];

					// Get the add new button element.
					//
					AddNewRowButtonUIElement addButtonElem = 
						null != addNewBoxElement
						? (AddNewRowButtonUIElement)addNewBoxElement.GetDescendant( typeof( AddNewRowButtonUIElement ), band )
						: null;

					// Once the button is found, see if it's enabled and if the
					// mnemonic matches.
					//
					if ( null != addButtonElem && 
						addButtonElem.Enabled && 
						null != addButtonElem.Text && 
						Control.IsMnemonic( charCode, addButtonElem.Text ) )
					{
						// Give the focus to the grid.
						//
						if ( !this.Focused )
						{
							try
							{
                                // MRS 12/19/06 - fxCop
                                //if ( DisposableObject.HasSamePublicKey( this.GetType() ))
                                //{
                                //    System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

                                //    perm.Assert();
                                //}

                                //this.Focus();
                                if (DisposableObject.HasSamePublicKey(this.GetType()))
                                    GridUtils.FocusControl(this);
                                else
                                    this.Focus();
							}
							catch ( Exception )
							{
								// If unable to give the focus, then don't process the mnemonic and
								// return false.
								//
								return false;
							}
						}

						// Add a row for the corressponding add new button and
						// return true.
						//
						band.InternalAddNew( );

						return true;
					}
				}
			}

			return false;
		}

 
		/// <summary>
		/// Gives you the ability to reconfigure the way the control responds to user keystrokes.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>KeyActionMappings</b> property provides access to the control's mechanism for handling keyboard input from users. All keystrokes for actions such as selection, navigation and editing are stored in a table-based system that you can examine and modify using this property. Through the <b>KeyActionsMappings</b> property, you can customize the keyboard layout of the control to match your own standards for application interactivity.</p>
		/// <p class="body">For example, if you wanted users to be able to navigate between cells by pressing the F8 key, you could add this behavior. You can specify the key code and any special modifier keys associated with an action, as well as determine whether a key mapping applies in a given context.</p>
		/// <p class="body">The following table lists the default key mappings for the <see cref="UltraGrid"/> control:</p>
		/// <p class="body">
		/// <table border="1" cellpadding="3" width="100%" class="FilteredItemListTable">
		/// <thead>
		/// <tr>
		/// <th>KeyCode</th>
		/// <th>ActionCode</th>
		/// <th>StateRequired</th>
		/// <th>StateDisallowed</th>
		/// <th>SpecialKeysRequired</th>
		/// <th>SpecialKeysDisallowed</th>
		/// </tr>
		/// </thead>
		/// <tbody>
		/// <tr><td class="body">Right</td><td class="body">NextCell</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body"></td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">Tab</td><td class="body">NextCellByTab</td><td class="body">Cell</td><td class="body"></td><td class="body"></td><td class="body">All</td></tr>
		/// <tr><td class="body">Left</td><td class="body">PrevCell</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body"></td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">Tab</td><td class="body">PrevCellByTab</td><td class="body">Cell</td><td class="body"></td><td class="body">Shift</td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">Up</td><td class="body">AboveCell</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Down</td><td class="body">BelowCell</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Home</td><td class="body">FirstRowInBand</td><td class="body">Row</td><td class="body">Cell</td><td class="body"></td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">End</td><td class="body">LastRowInBand</td><td class="body">Row</td><td class="body">Cell</td><td class="body"></td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">Right</td><td class="body">FirstRowInGrid</td><td class="body"></td><td class="body">Row</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Down</td><td class="body">FirstRowInGrid</td><td class="body"></td><td class="body">Row</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Home</td><td class="body">FirstRowInGrid</td><td class="body">Row</td><td class="body">Cell</td><td class="body">Ctrl</td><td class="body">Alt</td></tr>
		/// <tr><td class="body">End</td><td class="body">LastRowInGrid</td><td class="body">Row</td><td class="body">Cell</td><td class="body">Ctrl</td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Right</td><td class="body">ExpandRow</td><td class="body">RowExpandable</td><td class="body">Cell, RowExpanded</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Left</td><td class="body">CollapseRow</td><td class="body">RowExpanded</td><td class="body">Cell</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Right</td><td class="body">NextRow</td><td class="body">Row</td><td class="body">Cell, RowExpandable</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Right</td><td class="body">NextRow</td><td class="body">Row, RowExpanded</td><td class="body">Cell</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Tab</td><td class="body">NextRowByTab</td><td class="body">Row</td><td class="body">Cell, LastRowInGrid</td><td class="body"></td><td class="body">All</td></tr>
		/// <tr><td class="body">Left</td><td class="body">PrevRow</td><td class="body">Row</td><td class="body">Cell, RowExpanded</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Tab</td><td class="body">PrevRowByTab</td><td class="body">Row</td><td class="body">Cell, FirstRowInGrid</td><td class="body">Shift</td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">Up</td><td class="body">AboveRow</td><td class="body">Row</td><td class="body">Cell</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Down</td><td class="body">BelowRow</td><td class="body">Row</td><td class="body">Cell</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Space</td><td class="body">ToggleCheckbox</td><td class="body">InEdit, IsCheckbox</td><td class="body"></td><td class="body"></td><td class="body">All</td></tr>
		/// <tr><td class="body">Space</td><td class="body">ToggleCellSel</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body"></td><td class="body">All</td></tr>
		/// <tr><td class="body">Space</td><td class="body">ToggleRowSel</td><td class="body">Row</td><td class="body">Cell</td><td class="body"></td><td class="body">All</td></tr>
		/// <tr><td class="body">Space</td><td class="body">DeactivateCell</td><td class="body">Cell</td><td class="body"></td><td class="body">Ctrl</td><td class="body">AltShift</td></tr>
		/// <tr><td class="body">Space</td><td class="body">ActivateCell</td><td class="body">Row</td><td class="body">Cell</td><td class="body">Ctrl</td><td class="body">AltShift</td></tr>
		/// <tr><td class="body">Right</td><td class="body">NextCellInBand</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body">Ctrl</td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Left</td><td class="body">PrevCellInBand</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body">Ctrl</td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Home</td><td class="body">FirstCellInRow</td><td class="body">Cell</td><td class="body">CellFirst, InEdit</td><td class="body"></td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">End</td><td class="body">LastCellInRow</td><td class="body">Cell</td><td class="body">CellLast, InEdit</td><td class="body"></td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">Home</td><td class="body">FirstCellInBand</td><td class="body">CellFirst</td><td class="body">InEdit</td><td class="body"></td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">End</td><td class="body">LastCellInBand</td><td class="body">CellLast</td><td class="body">InEdit</td><td class="body"></td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">Home</td><td class="body">FirstCellInGrid</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body">Ctrl</td><td class="body">Alt</td></tr>
		/// <tr><td class="body">End</td><td class="body">LastCellInGrid</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body">Ctrl</td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Prior</td><td class="body">PageUpCell</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Next</td><td class="body">PageDownCell</td><td class="body">Cell</td><td class="body">InEdit</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Prior</td><td class="body">PageUpRow</td><td class="body">Row</td><td class="body">Cell</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Next</td><td class="body">PageDownRow</td><td class="body">Row</td><td class="body">Cell</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Escape</td><td class="body">UndoCell</td><td class="body">InEdit</td><td class="body">IsDroppedDown</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Escape</td><td class="body">UndoRow</td><td class="body">Row, RowDirty</td><td class="body">InEdit</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Enter</td><td class="body">ExpandRow</td><td class="body">GroupByRow</td><td class="body">IsDroppedDown, RowExpanded</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Enter</td><td class="body">CollapseRow</td><td class="body">RowExpanded, GroupByRow</td><td class="body">IsDroppedDown</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">F4</td><td class="body">ToggleDropdown</td><td class="body">HasDropdown, InEdit</td><td class="body"></td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Up</td><td class="body">ToggleDropdown</td><td class="body">HasDropdown, InEdit</td><td class="body"></td><td class="body">Alt</td><td class="body"></td></tr>
		/// <tr><td class="body">Down</td><td class="body">ToggleDropdown</td><td class="body">HasDropdown, InEdit</td><td class="body"></td><td class="body">Alt</td><td class="body"></td></tr>
		/// <tr><td class="body">F2</td><td class="body">ToggleEditMode</td><td class="body">Cell</td><td class="body">SwapDroppedDown</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">F4</td><td class="body">EnterEditModeAndDropdown</td><td class="body">HasDropdown</td><td class="body">InEdit</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Up</td><td class="body">EnterEditModeAndDropdown</td><td class="body">HasDropdown</td><td class="body">InEdit</td><td class="body">Alt</td><td class="body"></td></tr>
		/// <tr><td class="body">Down</td><td class="body">EnterEditModeAndDropdown</td><td class="body">HasDropdown</td><td class="body">InEdit</td><td class="body">Alt</td><td class="body"></td></tr>
		/// <tr><td class="body">F6</td><td class="body">NextRegion</td><td class="body"></td><td class="body">InEdit</td><td class="body"></td><td class="body">All</td></tr>
		/// <tr><td class="body">F6</td><td class="body">PrevRegion</td><td class="body"></td><td class="body">InEdit</td><td class="body">Shift</td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">Delete</td><td class="body">DeleteRows</td><td class="body">Row</td><td class="body">InEdit</td><td class="body"></td><td class="body">All</td></tr>
		/// <tr><td class="body">Right</td><td class="body">NextCellInBand</td><td class="body">Cell, InEdit, IsCheckbox</td><td class="body"></td><td class="body"></td><td class="body"></td></tr>
		/// <tr><td class="body">Left</td><td class="body">PrevCellInBand</td><td class="body">Cell, InEdit, IsCheckbox</td><td class="body"></td><td class="body"></td><td class="body"></td></tr>
		/// <tr><td class="body">Up</td><td class="body">AboveCell</td><td class="body">Cell, InEdit, IsCheckbox</td><td class="body"></td><td class="body"></td><td class="body"></td></tr>
		/// <tr><td class="body">Down</td><td class="body">BelowCell</td><td class="body">Cell, InEdit, IsCheckbox</td><td class="body"></td><td class="body"></td><td class="body"></td></tr>
		/// </tbody>
		/// </table>
		/// </p>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		public GridKeyActionMappings  KeyActionMappings
		{
			get
			{
				if ( null == this.keyActionMappings )
					this.keyActionMappings = new GridKeyActionMappings();

				return this.keyActionMappings;
			}
		}

		/// <summary>
		/// Returns or sets a value that determines how information will be updated in the datasource.		
		/// </summary>
		///	<remarks>
		///	<p class="body">Use this property to specify when updates are committed back to the data source, either based on user interaction, upon row or cell change, or programmatically, when the <b>UpdateData</b> method is invoked.</p>
		///	<p class="body">When this property is set to OnRowChange or OnCellChange, updates are committed to the data source when the user leaves a row or cell, respectively, that has been modified. When a cell that has been modified loses focus, its <b>DataChanged</b> property is set to True and the <b>BeforeCellUpdate</b> event is generated. Similarly, when a row that has been modified loses focus, its <b>DataChanged</b> property is set to True and the <b>BeforeRowUpdate</b> event is generated.</p>
		///	<p class="body">When this property is set to OnUpdate, no updates are actually committed to the data source until the <b>UpdateData</b> method is invoked or until EndEdit is received from the binding manager. Note that there are circumstances where the binding manager will invoke EndEdit whenever the Position is changed, which obviates the use of the OnUpdate setting. This behavior is due to the implementation of the binding manager and is outside the control of the component.</p>
		///	<p class="body">If an attempt is made to update a data source that cannot be updated, the <b>Error</b> event is generated.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGrid_P_UpdateMode")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.UpdateMode UpdateMode
		{
			get
			{
				return this.updateMode;
			}
			set
			{				
				if ( value != this.updateMode )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Infragistics.Win.UltraWinGrid.UpdateMode), value ) )
                        // MRS NAS v8.2 - Unit Testing
						//throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_290") );
                        throw new InvalidEnumArgumentException(Shared.SR.GetString("LE_ArgumentException_290"));

					if ( Infragistics.Win.UltraWinGrid.UpdateMode.OnCellChange == this.updateMode || Infragistics.Win.UltraWinGrid.UpdateMode.OnCellChangeOrLostFocus == this.updateMode )
					{
						
					}
					else if ( ( Infragistics.Win.UltraWinGrid.UpdateMode.OnRowChange == this.updateMode || Infragistics.Win.UltraWinGrid.UpdateMode.OnRowChangeOrLostFocus == this.updateMode ) && 
						Infragistics.Win.UltraWinGrid.UpdateMode.OnCellChange == value )
					{
						this.UpdateData();
					}

					this.updateMode = value;
			
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.UpdateMode );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeUpdateMode()
		{
			return UpdateMode.OnRowChangeOrLostFocus != this.updateMode;
		}

		/// <summary>
		/// Resets UpdateMode to its default value (OnRowChangeOrLostFocus)
		/// </summary>
		public void ResetUpdateMode()
		{
            // MRS NAS v8.2 - Unit Testing
			//this.updateMode = UpdateMode.OnRowChangeOrLostFocus;
            this.UpdateMode = UpdateMode.OnRowChangeOrLostFocus;
		}

		// SSP 4/24/05 - NAS 5.2 Filter Row
		//
		internal bool ClearSelectedItemsOfType( GridItemBase item )
		{
			if ( item is UltraGridRow )
			{
				return this.ClearSelectedRows( );
			}
			else if ( item is UltraGridCell )
			{
				return this.ClearSelectedCells( );
			}
			else if ( item is HeaderBase )
			{
				return this.ClearSelectedColumns( );
			}
			else
			{
				Debug.Assert( false, "Unknown type of item type !" );
				return false;
			}
		}

		// SSP 4/15/02 UWG1072
		// Added ClearSelectedCells method
		internal bool ClearSelectedCells( )
		{			
			if ( this.Selected.Cells != null && this.Selected.Cells.Count > 0 )
			{
				Selected selected = new Selected();
			
				bool cancel = SelectNewSelection( typeof( UltraGridCell ), selected );

				if ( cancel )
					return true;
				else
				{
					// clear pivotitem
					this.ActiveRowScrollRegion.SetPivotItem( null );
					this.ActiveColScrollRegion.SetPivotCell( null );

					return false;
				}
			}

			return false;
		}

		
		// SSP 4/15/02 UWG1072
		// Added ClearSelectedRows method
		internal bool ClearSelectedRows( )
		{
			if ( this.Selected.Rows != null && this.Selected.Rows.Count > 0 )
			{
				Selected selected = new Selected();
			
				bool cancel = SelectNewSelection( typeof( UltraGridRow ), selected );

				if ( cancel )
					return true;
				else
				{
					// clear pivotitem
					this.ActiveRowScrollRegion.SetPivotItem( null );

					return false;
				}
			}

			return false;
		}

		// SSP 12/11/02 UWG1835
		// Added ClearSelectedColumns method.
		//
		internal bool ClearSelectedColumns( )
		{
			if ( this.Selected.Columns != null && this.Selected.Columns.Count > 0 )
			{
				Selected selected = new Selected();
			
				// SSP 12/13/02
				// The type passed in should have been ColumnHeader and not UltraGridColumn,
				//
				//bool cancel = SelectNewSelection( typeof( UltraGridColumn ), selected );
				bool cancel = SelectNewSelection( typeof( ColumnHeader ), selected );

				if ( cancel )
					return true;
				else
				{
					// clear pivotitem
					this.ActiveRowScrollRegion.SetPivotItem( null );

					return false;
				}
			}

			return false;
		}

		internal bool ClearAllSelected() 
		{
			return this.ClearAllSelected( false );
		}

		// SSP 10/23/06 BR16793
		// Added an overload of ClearAllSelected that takes in dontClearPivotItems.
		// 
		internal bool ClearAllSelected( bool dontClearPivotItems )
		{
			System.Type typeCleared = null;

			// JJD 10/24/01
			// Get the type of the selection that is being cleared to
			// pass into SelectNewSelection which fires the events
			//
			if ( this.Selected.Cells != null && this.Selected.Cells.Count > 0 )
				typeCleared = this.Selected.Cells[0].GetType();
			else
				if ( this.Selected.Rows != null && this.Selected.Rows.Count > 0 )
				typeCleared = this.Selected.Rows[0].GetType();
			else
				if ( this.Selected.Columns != null && this.Selected.Columns.Count > 0 )
				typeCleared = this.Selected.Columns[0].GetType();
			else
			{
				// if there is nothing selected then just return false
				return false;
			}

			Selected selected = new Selected();
			
			// JJD 10/24/01
			// Pass the type to be cleared into SelectNewSelection for the
			// selection events
			//
			bool cancel = SelectNewSelection( typeCleared, selected );

			if ( cancel )
				return true;
			else
			{
				// SSP 10/23/06 BR16793
				// Added an overload of ClearAllSelected that takes in dontClearPivotItems.
				// Enclosed the existing code in if block.
				// 
				if ( !dontClearPivotItems )
				{
					// clear pivotitem
					this.ActiveRowScrollRegion.SetPivotItem( null );
					this.ActiveColScrollRegion.SetPivotCell( null );
				}

				return false;
			}
		}


		/// <summary>
		/// Returns true if the event is in progress
		/// </summary>
        /// <param name="eventId">Indicates the event.</param>
        /// <returns>true if the event is in progress.</returns>
		internal protected override bool IsEventInProgress( Enum eventId )
		{
			return this.EventManager.InProgress( (Infragistics.Win.UltraWinGrid.GridEventIds) eventId );
		}

		/// <summary>
		/// Returns true if the control supports printing
		/// </summary>
		internal protected override bool SupportsPrinting { get { return true; } }


		/// <summary>
		/// Associated Control UIElement object
		/// </summary>
		protected override ControlUIElementBase ControlUIElement 
		{ 
			get
			{
				// SSP 5/29/02 UWG1144
				// Check for DisplayLayout being null because it would be null
				// if we are in the middle of being disposed of.
				//
				//return this.DisplayLayout.UIElement; 
				if ( null != this.DisplayLayout )
					return this.DisplayLayout.UIElement;

				return null;
			}
		}

		internal bool HasCaption
		{
			get
			{
				// JAS 4/5/05 v5.2 CaptionVisible property
				// If the user explicitly specified whether the grid's caption area should be visible or not
				// then just return that value.  Otherwise, do the default logic to determine if it is hidden.
				//
				if( this.DisplayLayout != null && 
					this.DisplayLayout.CaptionVisible != DefaultableBoolean.Default )
				{
					return this.DisplayLayout.CaptionVisible == DefaultableBoolean.True;
				}

				// AS 1/8/03 - fxcop
				// Do not compare against string.empty - test the length instead
				//return !( this.Text == null || this.Text == string.Empty ) ;
				return !( this.Text == null || this.Text.Length == 0 ) ;
			}
		}


		// SSP 1/13/02
		// Commented out WndProc code below. We don't need it anymore since the value list 
		// accomplishes what we were overriding the WndProc for in the first place.
		//
		

		/// <summary>
		/// Returns or sets the active cell in the grid.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a Cell object that corrresponds to the active cell in the grid. You can set this property to an existing Cell object to have that cell become the active one.</p>
		/// <seealso cref="UltraGridBase.ActiveRow"/>
		/// <seealso cref="UltraGridBase.Rows"/>
		/// <seealso cref="UltraGridRow"/>
		/// <seealso cref="UltraGridCell"/>
		/// <seealso cref="UltraGrid.Selected"/>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		public Infragistics.Win.UltraWinGrid.UltraGridCell ActiveCell
		{
			get
			{
				// SSP 1/23/02
				// Make sure the row active cell belongs to is still valid
				//
				if ( null != this.activeCell && !this.in_getActiveCell )
				{
					// Set the anti-recursion flag
					//
					this.in_getActiveCell = true;

					//if ( null != this.activeCell.Row && !this.activeCell.Row.IsStillValid )
					UltraGridRow activeCellRow = this.activeCell.Row;					
					if ( null != activeCellRow && !activeCellRow.IsStillValid )
					{
						try
						{
							if ( this.activeCell.IsInEditMode )
							{
								this.activeCell.ExitEditMode( true, true );
							}
						}
						catch(Exception)
						{
						}

						// SSP 12/8/03 UWG2753
						// Maintain the tempActiveRow to what it is if it's valid. Use the new 
						// ActiveRowInvalidHelper method.
						//
						//this.ClearActiveRow( );
						this.ActiveRowInvalidHelper( );
					}
					
					this.in_getActiveCell = false;
				}

				return this.activeCell;
			}
			set
			{
				if ( this.activeCell != value )
				{
					// first clear the current active cell
					//
					// SSP 11/14/01 UWG600 
					// Pass in true for invalidate parameter so that all the
					// cell elements in all the scroll regions get dirtied 
					// so any ActiveCellAppearance (Image settings)
					// settings get cleared on the cell elements associated 
					//with all the old active cell.
					//
					//this.ClearActiveCell( false, false );
					this.ClearActiveCell( true, false );

					// if the value is null or the active cell wasn't
					// cleared then exit
					//
					if ( value == null ||
						this.activeCell != null )
						return;

					// if the value is not null then try setting the
					// active row 
					//
					if ( value != null )
					{
						this.ActiveRow = value.Row;

						// if the set of the active row was cancelled
						// then return
						//
						if ( this.ActiveRow != value.Row )
							return;
					}
					
					//ROBA UWG197 8/20/01 we are firing these events in cell.Activate()
					//
					// SSP 3/18/02 UWG1052
					// It looks like this is the right place to fire the event after all because
					// if the user sets the activeCell from code, then Cell.Activate does
					// not get called.
					//
					//
					CancelableCellEventArgs cellEventArgs = new CancelableCellEventArgs( value );
					// SSP 4/25/03 UWG2217
					// Check to see if the event is enabled or not before firing it. To do that
					// call FireEvent method which just does that.
					//
					//this.OnBeforeCellActivate( cellEventArgs );
					this.FireEvent( GridEventIds.BeforeCellActivate, cellEventArgs );
					if ( cellEventArgs.Cancel )
						return;					

					//set activecell to new cell
					this.activeCell = value;

					// SSP 4/23/05 - NAS 5.2 Filter Row
					// Added OnActiveStateChanged on row and cell.
					//
					if ( null != this.activeCell )
						this.activeCell.OnActiveStateChanged( true );

					// SSP 11/14/01 UWG600
					// We need to invalidate all the elements (in all the scroll regions)
					// Pass in true so that it will dirty the cell elements ( which
					// we need to do in case the active cell settings have images set ).
					//
					if ( null != this.activeCell )
					{
						this.activeCell.InvalidateItemAllRegions( true );

                        // MBS 7/1/09 - NA9.2 ActiveCellHeaderAppearances
                        // The header needs to be invalidated because we now allow the user to have the appearance
                        // automatically change based on the ActiveCell
                        this.activeCell.Column.DirtyHeaderUIElements(null);

						// SSP 11/22/04 - Merged Cell Feature
						//
						RowsCollection.MergedCell_CellStateChanged( this.activeCell, MergedCell.State.Activation );
					}

					// SSP 3/18/02 UWG1052
					// It looks like this is the right place to fire the event after all because
					// if the user sets the activeCell from code, then Cell.Activate does
					// not get called.
					// Uncommented the code for firing AfterCellActivate event.
					//
					//Fire After cell activate
					// SSP 4/25/03 UWG2217
					// Check to see if the event is enabled or not before firing it. To do that
					// call FireEvent method which just does that.
					//
					//this.OnAfterCellActivate( );				        
					this.FireEvent( GridEventIds.AfterCellActivate, new CellEventArgs( this.activeCell ) );

					// JJD 12/16/03 - Added accessibility support
					// Call AccessibilityNotifyClients  
					if ( this.ContainsFocus )
						this.AccessibilityNotifyClientsInternal( AccessibleEvents.Focus, 0 );
				}
			}
		}

		#region ActiveCellInternal
		
		// SSP 4/22/05 - Optimizations
		// Added ActiveCellInternal.
		//
		internal UltraGridCell ActiveCellInternal
		{
			get
			{
				return this.activeCell;
			}
		}

		#endregion // ActiveCellInternal

		// Called to set the pivot item during selection processing.
		internal void SetPivotItem( GridItemBase gridItem, bool shiftKeyDown )
		{
			this.GetSelectionManager().SetPivotItem( gridItem, shiftKeyDown );
		}

		// Returns the pivot item based on the type of GridItemBase.
		internal GridItemBase GetPivotItem( GridItemBase gridItem )
		{
			return (GridItemBase)this.GetSelectionManager().GetPivotItem( gridItem );
		}

		/// <summary>
		/// Internal property to determine if we are in the InitializeLayout event. 
		/// </summary>
		internal protected override bool InInitializeLayout 
		{ 
			get
			{
				return this.EventManager.InProgress(Infragistics.Win.UltraWinGrid.GridEventIds.InitializeLayout); 
			}
		}
		
		internal bool ClearActiveCell ()
		{
			return this.ClearActiveCell( true, false );
		}

		internal bool ClearActiveCell ( bool invalidate )
		{
			return this.ClearActiveCell( invalidate, false );
		}

		internal bool ClearActiveCell( bool invalidate ,
			bool shutdown  )
		{
			if ( this.activeCell == null )
				return false;

			if ( !shutdown )
			{
				// exit edit mode from the existing cell 
				//
				if ( this.activeCell.ExitEditMode() )
					return true;

				// Fire the BeforeCellDeactivate event. 
				//
				CancelableCellEventArgs eventArgs = new CancelableCellEventArgs(this.activeCell);
				this.FireEvent( GridEventIds.BeforeCellDeactivate, eventArgs );

				if ( eventArgs.Cancel )
					return true;

				// If the form was unloaded in the event above then return true
				// to cancel
				//

				if ( this.activeCell == null )
					return true;

				// if we're deactivating a ButtonCell and ButtonsAlwaysVisible is false,
				// we must morph into a text element
				//    
				// Always dirty the old active cell since the active cell or active row appearance
				// may change the layout of the cell (e.g. by adding or removing a picture)
				//

				Infragistics.Win.UIElement element = this.activeCell.GetUIElement( this.ActiveRowScrollRegion,
					this.ActiveColScrollRegion
					// SSP 5/17/04 UWG3330
					// Pass in false for verifyElements.
					//
					, false );

				if ( element != null )
					element.DirtyChildElements();
			
			}


			// SSP 11/14/01 UWG600
			// We need to invalidate all the elements (in all the scroll regions)
			// not just the one in active scroll region. 
			// And we should do it after setting the activeCell to null so
			// so any active cell appearances get resolved properly.
			Infragistics.Win.UltraWinGrid.UltraGridCell tmpCell = this.activeCell;
			
			// Finally clear the activeCell reference
			//
			this.activeCell = null;

			// SSP 4/23/05 - NAS 5.2 Filter Row
			// Added OnActiveStateChanged on row and cell.
			//
			tmpCell.OnActiveStateChanged( false );

            if (invalidate)
            {
                // invalidate the cell
                // SSP 11/14/01 UWG600 Invalidate the cell after setting the activeCell
                // to null.
                // Invalidate the cell
                //this.activeCell.InvalidateItemAllRegions( true );
                // SSP 5/17/04 UWG3330
                // Don't verify the elements when getting the cell element. So pass in
                // false for recalcRects instead of true.
                //
                //tmpCell.InvalidateItemAllRegions( true );
                tmpCell.InvalidateItemAllRegions(false);

                // MBS 7/1/09 - NA9.2 ActiveCellHeaderAppearances
                // The header needs to be invalidated because we now allow the user to have the appearance
                // automatically change based on the ActiveCell                
                tmpCell.Column.Header.DirtyHeaderUIElements(null);
            }
			

			// SSP 11/22/04 - Merged Cell Feature
			//
			RowsCollection.MergedCell_CellStateChanged( tmpCell, MergedCell.State.Activation );

			return false;
		}

		internal ValueList SwapDropDown
		{
			get
			{
				if ( this.swapDropDown == null )
					this.swapDropDown = new ValueList();

				return this.swapDropDown;
			}
		}

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved to UltraGridBase
		#region Moved to UltraGridBase
		// SSP 3/21/02
		//
//		#region "Filter code" 
//
//		#region FilterDropDown
//
//#if DEBUG
//		/// <summary>
//		/// Value list used for filter dropdown.
//		/// </summary>
//#endif
//		internal ValueList FilterDropDown
//		{
//			get
//			{
//				if ( null == this.filterDropDown )
//					this.filterDropDown = new Infragistics.Win.ValueList( );
//
//				return this.filterDropDown;
//			}
//		}
//
//		#endregion // FilterDropDown
//
//		#region CustomRowFiltersDialog
//
//		// SSP 8/28/03 - Optimizations
//		// Instead of creating the form and keeping it around, recreate it every time
//		// it's displayed.
//		//
//		/*
//#if DEBUG
//		/// <summary>
//		/// Returns a custom row filter form for showing the custom row filter dialog.
//		/// </summary>
//#endif
//		internal CustomRowFiltersDialog CustomRowFiltersDialog
//		{
//			get
//			{
//				if ( null == this.customRowFiltersDialog )
//				{
//					this.customRowFiltersDialog = new CustomRowFiltersDialog( this );					
//				}
//				
//				return this.customRowFiltersDialog;
//			}
//		}
//		*/
//		
//		#endregion // CustomRowFiltersDialog
//
//		#region HasFilterDropDown
//
//#if DEBUG
//		/// <summary>
//		/// Returns true if filter drop down has been created.
//		/// </summary>
//#endif
//		internal bool HasFilterDropDown
//		{
//			get
//			{
//				return null != this.filterDropDown;
//			}
//		}
//
//		#endregion // HasFilterDropDown
//
//		#endregion // End of "Filter Code" region
		#endregion Moved to UltraGridBase

		#region RowSummariesDialog

		// SSP 11/5/04 - Optimizations
		// Instead of creating the summary dialog and keeping it around, recreate it every time
		// it's displayed.
		//
		
		
		#endregion // RowSummariesDialog

		internal bool HasSwapDropDown
		{
			get
			{
				return this.swapDropDown != null;
			}
		}

		internal bool HasTooltipTool
		{
			get
			{
				return null != this.toolTip;
			}
		}

		/// <summary>
		/// Returns the tooltip tool object, allocating it the first time
		/// </summary>
		internal protected override Infragistics.Win.ToolTip TooltipTool
		{
			get
			{
				if ( this.DesignMode )
					return null;

				if ( null == this.toolTip )
				{
					this.toolTip = new Infragistics.Win.ToolTip( this );

					//RobA 10/8/01 we don't need to do this anymore because the tooltip
					//does not take focus away.
					//
					//this.toolTip.Control.Visible = true;
					//this.toolTip.Control.Visible = false;
					
					// SSP 8/20/01
					// when user clicks on the cell tips, we need to hide the tooltip 
					// and forward the message to the grid so that it goes into edit
					// mode.
					this.toolTip.Control.MouseDown += new MouseEventHandler( this.ToolTip_MouseDown );
					this.toolTip.Control.MouseUp += new MouseEventHandler( this.ToolTip_MouseUp );
					this.toolTip.Control.Click += new EventHandler( this.ToolTip_Click );

					//
					// JAS 9/22/04 UWG3536
					//
					if( this.Font != null )
						this.toolTip.Font = this.Font;
					
				}
			
				return this.toolTip;
			}
		}

		/// <summary>
		/// Forwards mouse down message from the tool tip to the grid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		private void ToolTip_MouseDown( object sender, MouseEventArgs e )
		{
			if ( null != this.toolTip )
			{
				Point screenPoint = this.toolTip.Control.PointToScreen( new Point( e.X, e.Y ) );

				Point gridPoint = this.PointToClient( screenPoint );

				// only forward if the point falls on the grid
				if ( this.Bounds.Contains( gridPoint ) )
				{
					this.toolTip.Hide( );

					this.OnMouseDown( new MouseEventArgs( e.Button, e.Clicks, gridPoint.X, gridPoint.Y, e.Delta ) );
				}
			}
		}

		/// <summary>
		/// Forwards mouse up message from the tool tip to the grid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		private void ToolTip_MouseUp( object sender, MouseEventArgs e )
		{
			if ( null != this.toolTip )
			{
				Point screenPoint = this.toolTip.Control.PointToScreen( new Point( e.X, e.Y ) );

				Point gridPoint = this.PointToClient( screenPoint );

				// only forward if the point falls on the grid
				if ( this.Bounds.Contains( gridPoint ) )
				{
					this.toolTip.Hide( );

					this.OnMouseUp( new MouseEventArgs( e.Button, e.Clicks, gridPoint.X, gridPoint.Y, e.Delta ) );
				}
			}
		}

		/// <summary>
		/// Forwards mouse click message from the tool tip to the grid 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		private void ToolTip_Click( object sender, EventArgs e )
		{
			if ( null != this.toolTip )
			{
				Point screenPoint = Cursor.Position;

				Point gridPoint = this.PointToClient( screenPoint );

				// only forward if the point falls on the grid
				if ( this.Bounds.Contains( gridPoint ) )
				{
					this.toolTip.Hide( );

					this.OnClick( new EventArgs( ) );
				}
			}
		}


		// returns a DragEffect instance ( creating one if not already created )
		// used for dragging columns and groups
		//
		internal DragEffect DragEffect
		{
			get
			{
				if( null == dragEffect )
				{
					this.dragEffect = new DragEffect( this );
				}

				return this.dragEffect;
			}
		}


		// SSP 3/11/02 UWG1039
		// Added NextControlOnLastCell functionality
		//
		internal bool IsActiveCellFirstVisibleCell( )
		{
			UltraGridCell cell = this.ActiveCell;

			if ( null == cell )
				return false;

			// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality/Fixed Add Row
			// Skip summary rows which are not activatable and also take into account the
			// filter row and the template add-row. For that use the new GetFirstLastActivatableRow
			// method.
			//
			//UltraGridRow firstVisibleRow = this.Rows.GetFirstVisibleRow( );
			UltraGridRow firstVisibleRow = this.Rows.GetFirstLastActivatableRow( true );

			if ( null != firstVisibleRow )
			{
				while ( firstVisibleRow is UltraGridGroupByRow )
				{					
					// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality/Fixed Add Row
					// Skip summary rows which are not activatable and also take into account the
					// filter row and the template add-row. For that use the new GetFirstLastActivatableRow
					// method.
					//
					// --------------------------------------------------------------------------------------
					firstVisibleRow = ((UltraGridGroupByRow)firstVisibleRow).Rows.GetFirstLastActivatableRow( true );
					
					// --------------------------------------------------------------------------------------
				}

				// SSP 7/29/05 - NAS 5.3 Tab Index
				// Since this method is used for determining when the active cell is the first 
				// or last tabbable cell when TabNavigation is NextControlOnLastCell, pass in
				// true for the new useTabOrderedColumns parameter to the GetFirstVisibleCol
				// method to get the last column in the tab order specified by the TabIndex.
				// 
				//UltraGridColumn column = firstVisibleRow.Band.GetFirstVisibleCol( null, false );
				UltraGridColumn column = firstVisibleRow.Band.GetFirstVisibleCol( null, false, true );

				// SSP 8/11/03 UWG1971
				// Skip non-activable columns. Above method GetLastVisibleCol has a includeNonActivable
				// parameter but it simply checks for HeaderBase.IsDisabled which doesn't look at the
				// activation settings at all. It's very misleading. However we should look at the
				// activation.
				//
				// --------------------------------------------------------------------------------
				while ( null != column )
				{
					UltraGridCell tmpCell = firstVisibleRow.GetCellIfAllocated( column );						
					
					// MRS 10/19/04 - UWG3130
					// Check for TabStop, also
					//bool canBeActivated = Activation.Disabled != ( null != tmpCell ? tmpCell.ActivationResolved : firstVisibleRow.GetResolvedCellActivation( column ) );
					bool canBeActivated = (Activation.Disabled != ( null != tmpCell ? tmpCell.ActivationResolved : firstVisibleRow.GetResolvedCellActivation( column ) ) ) 
						// SSP 12/9/05 BR07900
						// Cell also exposes TabStop property. Take that into account as well.
						// 
						//&& column.TabStop;
						&& ( null != tmpCell ? tmpCell.IsTabStop : column.TabStop );

					if ( canBeActivated )
						break;

					// SSP 7/29/05 - NAS 5.3 Tab Index
					// Since this method is used for determining when the active cell is the first 
					// or last tabbable cell when TabNavigation is NextControlOnLastCell, pass in
					// true for the new useTabOrderedColumns parameter to the GetFirstVisibleCol
					// method to get the last column in the tab order specified by the TabIndex.
					// 
					//column = firstVisibleRow.Band.GetRelatedVisibleCol( column, NavigateType.Next, false );
					column = firstVisibleRow.Band.GetRelatedVisibleCol( column, NavigateType.Next, false, true );
				}
				// --------------------------------------------------------------------------------

				if ( null == column )
					return false;

				if ( firstVisibleRow.Cells.HasCell( column ) &&
					firstVisibleRow.Cells[column] == cell )
					return true;
			}

			return false;
		}
	
		private UltraGridRow GetLastAbsoluteVisibleRow( )
		{
			// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality/Fixed Add Row
			// Skip summary rows which are not activatable and also take into account the
			// filter row and the template add-row. For that use the new GetFirstLastActivatableRow
			// method.
			//
			//UltraGridRow lastVisibleRow = this.Rows.GetLastVisibleRow( );
			UltraGridRow lastVisibleRow = this.Rows.GetFirstLastActivatableRow( false );

			// Drill down the visible descendant rows and find the last one.
			//
			while ( null != lastVisibleRow && lastVisibleRow.IsExpanded 
				// SSP 11/11/03 Add Row Feature
				//
				//&& lastVisibleRow.HasChild( true ) )
				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// We have to take into account filter row, summary row etc...
				// Replaced TemplateAddRow enum member with SpecialRows.
				//
				//&& lastVisibleRow.HasChild( true, IncludeRowTypes.TemplateAddRow ) )
				&& lastVisibleRow.HasChild( true, IncludeRowTypes.SpecialRows ) )
			{
				// SSP 11/11/03 Add Row Feature
				//
				//UltraGridRow row = lastVisibleRow.GetChild( ChildRow.Last );
				// MRS 3/10/05 - BR02716
				// GetChild includes hidden rows. We don't want that. 
				// Use GetLastVisibleChildRow, instead.
				//UltraGridRow row = lastVisibleRow.GetChild( ChildRow.Last, null, IncludeRowTypes.TemplateAddRow );
				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// We have to take into account filter row, summary row etc...
				// Got rid of IncludeRowTypes parameter from GetLastVisibleChildRow as it wasn't needed.
				// Every place we are calling it with IncludeRowTypes.TemplateAddRow as the param.
				// 
				//UltraGridRow row = lastVisibleRow.GetLastVisibleChildRow(IncludeRowTypes.TemplateAddRow);
				UltraGridRow row = lastVisibleRow.GetLastVisibleChildRow( );

				if ( null != row )
				{
					lastVisibleRow = row;
				}
				else
				{
					Debug.Assert( false, "We shouldn't get in here." );
					break;
				}
			}

			return lastVisibleRow;
		}

		// SSP 3/11/02 UWG1039
		// Added NextControlOnLastCell functionality
		//
		internal bool IsActiveCellLastVisibleCell( )
		{
			UltraGridCell cell = this.ActiveCell;

			if ( null == cell )
				return false;

			// SSP 12/4/02 UWG1874
			// Also take into account any descendant visible rows which the old code wasn't doing.
			//
			//UltraGridRow lastVisibleRow = this.Rows.GetLastVisibleRow( );
			UltraGridRow lastVisibleRow = this.GetLastAbsoluteVisibleRow( );

			// SSP 5/6/03 UWG2238
			// If the last visible row happens to be group-by row, then find a non-group-by row.
			//
			// ------------------------------------------------------------------------------
			while ( lastVisibleRow is UltraGridGroupByRow )
			{
				lastVisibleRow = lastVisibleRow.GetPrevVisibleRow( );
			}
			// ------------------------------------------------------------------------------

			if ( null != lastVisibleRow )
			{
				// SSP 12/4/02 UWG1874
				// Not necessary as the above GetLastAbsoluteVisibleRow method call takes care of it.
				//
				

				// SSP 7/29/05 - NAS 5.3 Tab Index
				// Since this method is used for determining when the active cell is the first 
				// or last tabbable cell when TabNavigation is NextControlOnLastCell, pass in
				// true for the new useTabOrderedColumns parameter to the GetLastVisibleCol
				// method to get the last column in the tab order specified by the TabIndex.
				// 
				//UltraGridColumn column = lastVisibleRow.Band.GetLastVisibleCol( null, false );
				UltraGridColumn column = lastVisibleRow.Band.GetLastVisibleCol( null, false, true );

				// SSP 12/4/02 UWG1874
				// Also take into account bands that are not activateable.
				//
				// -------------------------------------------------------------------------------
				while ( null == column )
				{
					UltraGridRow parentRow = lastVisibleRow.ParentRow;

					if ( null != parentRow )
					{
						lastVisibleRow = parentRow;

						// SSP 7/29/05 - NAS 5.3 Tab Index
						// Since this method is used for determining when the active cell is the first 
						// or last tabbable cell when TabNavigation is NextControlOnLastCell, pass in
						// true for the new useTabOrderedColumns parameter to the GetLastVisibleCol
						// method to get the last column in the tab order specified by the TabIndex.
						// 
						//column = lastVisibleRow.Band.GetLastVisibleCol( null, false );
						column = lastVisibleRow.Band.GetLastVisibleCol( null, false, true );

						// SSP 8/11/03 UWG1971
						// Skip non-activable columns. Above method GetLastVisibleCol has a includeNonActivable
						// parameter but it simply checks for HeaderBase.IsDisabled which doesn't look at the
						// activation settings at all. It's very misleading. However we should look at the
						// activation.
						//
						// --------------------------------------------------------------------------------
						while ( null != column )
						{
							UltraGridCell tmpCell = lastVisibleRow.GetCellIfAllocated( column );						
							
							// MRS 10/19/04 - UWG3130
							// Check for TabStop, also
							//bool canBeActivated = Activation.Disabled != ( null != tmpCell ? tmpCell.ActivationResolved : lastVisibleRow.GetResolvedCellActivation( column ) );							
							bool canBeActivated = (Activation.Disabled != ( null != tmpCell ? tmpCell.ActivationResolved : lastVisibleRow.GetResolvedCellActivation( column ) ) ) 
								// SSP 12/9/05 BR07900
								// Cell also exposes TabStop property. Take that into account as well.
								// 
								//&& column.TabStop;
								&& ( null != tmpCell ? tmpCell.IsTabStop : column.TabStop );

							if ( canBeActivated )
								break;

							// SSP 7/29/05 - NAS 5.3 Tab Index
							// Since this method is used for determining when the active cell is the first 
							// or last tabbable cell when TabNavigation is NextControlOnLastCell, pass in
							// true for the new useTabOrderedColumns parameter to the GetLastVisibleCol
							// method to get the last column in the tab order specified by the TabIndex.
							// 
							//column = lastVisibleRow.Band.GetRelatedVisibleCol( column, NavigateType.Prev, false );
							column = lastVisibleRow.Band.GetRelatedVisibleCol( column, NavigateType.Prev, false, true );
						}
						// --------------------------------------------------------------------------------
					}
					else
						break;
				}
				// -------------------------------------------------------------------------------

				// SSP 8/11/03 UWG1971
				// Skip non-activable columns. Above method GetLastVisibleCol has a includeNonActivable
				// parameter but it simply checks for HeaderBase.IsDisabled which doesn't look at the
				// activation settings at all. It's very misleading. However we should look at the
				// activation.
				//
				// --------------------------------------------------------------------------------
				while ( null != lastVisibleRow && null != column)
				{
					UltraGridCell tmpCell = lastVisibleRow.GetCellIfAllocated( column );

                    // MBS 10/14/08 - TFS8925 
                    // As a slight optimization, we can bail out here
                    // if the cell we found is also the active cell
                    if (tmpCell == cell)
                        return true;
							
					// MRS 10/19/04 - UWG3130
					// Check for TabStop, also
					//bool canBeActivated = Activation.Disabled != ( null != tmpCell ? tmpCell.ActivationResolved : lastVisibleRow.GetResolvedCellActivation( column ) );							
					bool canBeActivated = (Activation.Disabled != ( null != tmpCell ? tmpCell.ActivationResolved : lastVisibleRow.GetResolvedCellActivation( column ) ) ) 
						// SSP 12/9/05 BR07900
						// Cell also exposes TabStop property. Take that into account as well.
						// 
						//&& column.TabStop;
						&& ( null != tmpCell ? tmpCell.IsTabStop : column.TabStop );

					if ( canBeActivated )
						break;

					// SSP 7/29/05 - NAS 5.3 Tab Index
					// Since this method is used for determining when the active cell is the first 
					// or last tabbable cell when TabNavigation is NextControlOnLastCell, pass in
					// true for the new useTabOrderedColumns parameter to the GetLastVisibleCol
					// method to get the last column in the tab order specified by the TabIndex.
					// 
					//column = lastVisibleRow.Band.GetRelatedVisibleCol( column, NavigateType.Prev, false );
					column = lastVisibleRow.Band.GetRelatedVisibleCol( column, NavigateType.Prev, false, true );

                    // MBS 10/14/08 - TFS8925      
                    // It is possible that there could be multiple at the end of the grid that cannot be
                    // tabbed into, so we should keep going until we either hit the active cell or find a
                    // different cell that can be activated.
                    if (column == null)
                    {
                        lastVisibleRow = lastVisibleRow.GetPrevVisibleRow();
                        column = lastVisibleRow.Band.GetRelatedVisibleCol(null, NavigateType.Last, false, true);
                    }
				}
				// --------------------------------------------------------------------------------

				if ( null == column)
					return false;

				if ( lastVisibleRow.Cells.HasCell( column ) &&
					lastVisibleRow.Cells[column] == cell )
					return true;
			}

			return false;
		}
	
		// SSP 8/26/02 UWG1120
		// Added IsActiveRowFirstVisibleRow and IsActiveRowLastVisibleRow methods.
		//
		internal bool IsActiveRowFirstVisibleRow( )
		{
			UltraGridRow row = this.ActiveRow;

			if ( null == row )
				return false;

			UltraGridRow firstVisibleRow = this.Rows.GetFirstVisibleRow( );

			return row == firstVisibleRow;
		}


		// SSP 8/26/02 UWG1120
		// Added IsActiveRowFirstVisibleRow and IsActiveRowLastVisibleRow methods.
		//
		internal bool IsActiveRowLastVisibleRow( )
		{
			// SSP 12/4/02 UWG1874
			// Also take into account any descendant visible rows which the old code wasn't
			// doing.
			//
			
			return null != this.ActiveRow && this.GetLastAbsoluteVisibleRow( ) == this.ActiveRow;
		}

		// SSP 8/1/02 UWG1454
		// The same applies to the combo as well. So moved this in the UltraGridBase
		// class so that it would work for all the UltraGridBase derived controls 
		// including UltraGrid and UltraCombo.
		//
		

		// SSP 4/9/02
		// Added this method.
		internal bool InternalIsInputKey( Keys keyData )
		{
			return this.IsInputKey( keyData );
		}

        /// <summary>
        /// Determines whether the specified key is a regular input key or a special key that requires preprocessing.
        /// </summary>
        /// <param name="keyData">One of the System.Windows.Forms.Keys values.</param>
        /// <returns>true if the specified key is a regular input key; otherwise, false.</returns>
        protected override bool IsInputKey(Keys keyData)
		{
			// SSP 4/23/02
			// Embeddable editors.
			// If the editor in edit mode wants the key, then return true.
			//
			UltraGridCell cell = this.DisplayLayout.CellInEditMode;
			if ( null != cell )
			{
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//EmbeddableEditorBase editor = cell.Column.Editor;
				EmbeddableEditorBase editor = cell.EditorResolved;

				Debug.Assert( null != editor && editor.IsInEditMode, "Cell in edit mode but the associated column's editor not in edit mode !" );

				if ( null != editor && editor.IsInEditMode )
				{
					if ( !editor.CanFocus && !editor.Focused )
					{
						if ( editor.IsInputKey( keyData ) )
							return true;
					}
				}
			}

			// SSP 6/24/02
			// Changed the implementation of keyData to look at the 
			// keyaction mappings and return accrodingly. We don't want
			// to return true unless we are using the key. 
			// Below commented out code is the original implementation of 
			// this method.
			//
			// ------------------------------------------------------------------			
			// If the control doesn't have the focus, then return false.
			//
			if ( !this.ContainsFocus )
				return false;

			Keys strippedKeyData = (Keys)( (uint)0xFFFF & (uint)keyData );
			bool isShiftKeyDown = Keys.Shift == ( Keys.Shift & keyData );
			bool isAltKeyDown = Keys.Alt == ( Keys.Alt & keyData );
			
			// MD 8/3/07 - 7.3 Performance
			// Cached active cell - Prevent calling expensive getters multiple times
			UltraGridCell activeCell = this.ActiveCell;

			// If tab key was pressed, then look at the TabNavigation settings and
			// return true or false accoridingly.
			//
			if ( Keys.Tab == strippedKeyData )
			{
				if ( TabNavigation.NextControlOnLastCell == this.DisplayLayout.TabNavigation )
				{
					if ( Keys.Shift == ( Keys.Shift & keyData ) )
					{
						if ( this.IsActiveCellFirstVisibleCell( ) 
							// SSP 8/26/02 UWG1120
							// Added below clause to the condition.
							//
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//|| ( null == this.ActiveCell && this.IsActiveRowFirstVisibleRow( ) ) )
							|| ( null == activeCell && this.IsActiveRowFirstVisibleRow() ) )
							
							return false;
					}
					else
					{
						if ( this.IsActiveCellLastVisibleCell( ) 
							// SSP 8/26/02 UWG1120
							// Added below clause to the condition.
							//
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//|| ( null == this.ActiveCell && this.IsActiveRowLastVisibleRow( ) ) )
							|| ( null == activeCell && this.IsActiveRowLastVisibleRow() ) )
							return false;
					}
				}
				else if ( TabNavigation.NextControl == this.DisplayLayout.TabNavigation )
				{
					return false;
				}
			}

			// SSP 6/24/02
			// Following keys are being processed in OnKeyDown and we want to return true
			// here. However we only want to return true only if we are going to process 
			// the key.
			//

			ValueList valueList = this.swapDropDown;

			// Use the valueList instead of this.swapDropDown, so we can use the same code
			// below for filter drop down as well.
			//
			//if ( null != this.swapDropDown && ((IValueList)this.swapDropDown).IsDroppedDown )
			if ( null != valueList && ((IValueList)valueList).IsDroppedDown )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//IValueList dropDown = valueList as IValueList;

				// SSP 6/24/02
				//
				bool keyNeeded = false;

				if ( Keys.Escape == strippedKeyData )
				{
					keyNeeded = true;
				}
				else if ( Keys.Down == strippedKeyData )
				{
					if ( !isAltKeyDown )
					{
						keyNeeded = true;
					}
				}
				else if ( Keys.Right == strippedKeyData )
				{
					keyNeeded = true;	
				}
				else if ( Keys.Up == strippedKeyData )
				{
					if ( !isAltKeyDown )
					{	
						keyNeeded = true;
					}
				}
				else if ( Keys.Left == strippedKeyData )
				{
					keyNeeded = true;
				}

				else if ( Keys.Enter == strippedKeyData )
				{
					keyNeeded = true;
				}

				return keyNeeded;
			}
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				// Use the new HasDropDown property off the cell.
				//
			//else if ( null != this.ActiveCell && this.ActiveCell.IsInEditMode &&
			//	this.ActiveCell.Column.HasDropDown && this.ActiveCell.Column.HasValueList )
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//else if ( null != this.ActiveCell && this.ActiveCell.IsInEditMode &&
			//    this.ActiveCell.HasDropDown && this.ActiveCell.HasValueList )
			else if ( null != activeCell && activeCell.IsInEditMode &&
				activeCell.HasDropDown && activeCell.HasValueList )
			{
				// SSP 6/24/02
				// 
				bool keyNeeded = false;

				if ( Keys.Escape == strippedKeyData )
				{
					keyNeeded = true;
				}
				else if ( Keys.Down == strippedKeyData )
				{
					if ( !isAltKeyDown )
					{
						keyNeeded = true;	
					}
				}
				else if ( Keys.Up == strippedKeyData )
				{
					if ( !isAltKeyDown )
					{
						keyNeeded = true;
					}
				}
				else if ( Keys.Enter == strippedKeyData)
				{					
					keyNeeded = true;
				}
				else if ( Keys.Right == strippedKeyData )
				{
					keyNeeded = true;	
				}
				else if ( Keys.Left == strippedKeyData )
				{
					keyNeeded = true;	
				}

				
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//if ( this.ActiveCell.Column.ValueList.IsDroppedDown )
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.ActiveCell.ValueListResolved.IsDroppedDown )
				bool isValueListDroppedDown = activeCell.ValueListResolved.IsDroppedDown;

				if ( isValueListDroppedDown )
				{
					if ( Keys.Home == strippedKeyData && !isShiftKeyDown )
						keyNeeded = true;
					else if ( Keys.End == strippedKeyData && !isShiftKeyDown )
						keyNeeded = true;
					else if ( Keys.Prior == strippedKeyData )
						keyNeeded = true;
					else if ( Keys.Next == strippedKeyData )
						keyNeeded = true;
				}

				// SSP 10/10/02 UWG1748
				// Before we were just returning keyNeeded without checking if the key
				// is mapped. However we should only do that if the keyNeeded is true
				// or the value list is dropped down.
				//
				// return keyNeeded;
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//if ( keyNeeded || this.ActiveCell.Column.ValueList.IsDroppedDown )
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( keyNeeded || this.ActiveCell.ValueListResolved.IsDroppedDown )
				if ( keyNeeded || isValueListDroppedDown )
				{
					// SSP 4/8/03 UWG2100
					// If tab is pressed and the value list is dropped down, then return true.
					// Because returning false causes the control to process it and it gives
					// focus to the next control which is what we don't want.
					//
					if ( Keys.Tab == strippedKeyData &&
						this.KeyActionMappings.IsKeyMapped( keyData, (long)this.CurrentState ) )
						return true;

					return keyNeeded;
				}
			}

			// If the key is mapped for the key data, then return true, otherwise return
			// false.
			//
			return this.KeyActionMappings.IsKeyMapped( keyData, (long)this.CurrentState );
			// ------------------------------------------------------------------

			
		}

		private void OnKeyToggled()
		{
			// MRS 4/6/05 - RowLayout Span Resizing
			// Broke this up so the check for null doesn't have to be done more than once. 
//			// JJD 1/17/02
//			// If the last element entered is a row connector notify
//			// it so that it can re-ceheck the status of the ctrl key
//			// to possibly change its cursor and tooltip
//			//
//			if (
//				// SSP 5/29/02 UWG1144
//				// Check for the DisplayLayout being null, which it would be if we have been disposed of
//				// or are in the process of being disposed of.
//				// Added condition to check for DisplayLayout being non-null
//				//
//				null != this.DisplayLayout &&
//				this.DisplayLayout.UIElement.LastElementEntered is SiblingRowConnectorUIElement )
//			{
//				((SiblingRowConnectorUIElement)(this.DisplayLayout.UIElement.LastElementEntered)).OnKeyToggled();
//			}
			if (null == this.DisplayLayout)
				return;

			if (this.DisplayLayout.UIElement.LastElementEntered is SiblingRowConnectorUIElement )
				((SiblingRowConnectorUIElement)(this.DisplayLayout.UIElement.LastElementEntered)).OnKeyToggled();

			// MRS 4/6/05 - Span-Resizing in RowLayouts. 
			if (!this.IsHandleCreated)
				return;

            Point pointInGridCoords = this.PointToClient(Control.MousePosition);
            UIElement element = this.DisplayLayout.UIElement.AdjustableElementFromPoint(pointInGridCoords);
			if (null == element)
				return;

			CellUIElementBase cellElement = element as CellUIElementBase;
			if (null != cellElement)
			{
				cellElement.OnKeyToggled();
				return;
			}
            
			HeaderUIElement headerElement = element as HeaderUIElement;
			if (null != headerElement)
			{
				headerElement.OnKeyToggled();
				return;
			}
		}

		// SSP 4/8/02
		// Overrode this method to pass along key presses to the embeddable editor
		// 
		/// <summary>
		/// Key press event handler.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnKeyPress( KeyPressEventArgs e )
		{
			// SSP 5/29/02 UWG1144
			// Check for the DisplayLayout being null, which it would be if we have been disposed of
			// or are in the process of being disposed of. If it is null, then call the
			// base class and return.
			//
			if ( null == this.DisplayLayout )
			{
				base.OnKeyPress( e );
				return;
			}

			// SSP 6/25/02
			// Call the base class' implementation before doing the processing
			// so that we will fire the key event first and if that the listener
			// sets the Handled to true, we should just return.
			// Moved this here from the end of the method.
			//
			base.OnKeyPress( e );
			if ( e.Handled )
				return;


			// SSP 4/8/02
			// Embeddable edior logic. If there is an editor in edit mode, then
			// pass along the message to the editor. 
			// If the editor wanted the last key down (that is IsInputKey off
			// the editor returned true for the last key down), the forward
			// the associated key press as well.
			//
			// SSP 8/1/02
			// Regardless of wether the editor's IsInputKey returns true or false for
			// the pressed character, we should forward the key to the editor.
			//
			//if ( this.editorWantedLastKeyDown )
			//{
			UltraGridCell cell = this.ActiveCell;
			if ( null != cell && cell.IsInEditMode )
			{
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//EmbeddableEditorBase editor = cell.Column.Editor;
				// SSP 4/18/05 - NAS 5.2 Filter Row
				// We need to forward the navigation keys to the operator drop down if it's 
				// dropped down. Added below if block. Added GetEditorForKeyMessageForwarding
				// helper method.
				//
				// ------------------------------------------------------------------------
				//EmbeddableEditorBase editor = cell.EditorResolved;
				EmbeddableEditorBase editor;
				EmbeddableEditorOwnerBase editorOwnerInfo;
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.GetEditorForKeyMessageForwarding( cell, out editor, out editorOwnerInfo );
				UltraGrid.GetEditorForKeyMessageForwarding( cell, out editor, out editorOwnerInfo );
				// ------------------------------------------------------------------------
				
				if ( null != editor && editor.IsInEditMode && !editor.Focused )
				{
					Debug.Assert( null != cell.Column && null != cell.EditorOwnerInfo,
						"UltraGridColumn.EditorOwnerInfo returned null !" );

					// SSP 4/18/05 - NAS 5.2 Filter Row
					//
					//if ( null != cell.Column && null != cell.Column.EditorOwnerInfo )
					if ( null != editorOwnerInfo )
					{
						editorOwnerInfo.RaiseKeyPress( e );
						e.Handled = true;						
					}
				}
			}
			//}

			// SSP 6/25/02
			// Call the base class' implementation before doing the processing
			// so that we will fire the key event first and if that the listener
			// sets the Handled to true, we should just return.
			// Moved this to the beginning of the function.
			//
			//base.OnKeyPress( e );
		}

		/// <summary>
		/// Key up event handler.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnKeyUp( KeyEventArgs e )
		{
			// SSP 5/29/02 UWG1144
			// Check for the DisplayLayout being null, which it would be if we have been disposed of
			// or are in the process of being disposed of. If it is null, then call the
			// base class and return.
			//
			if ( null == this.DisplayLayout )
			{
				base.OnKeyUp( e );
				return;
			}

			// SSP 6/25/02
			// Call the base class' implementation before doing the processing
			// so that we will fire the key event first and if that the listener
			// sets the Handled to true, we should just return.
			// Moved this here from the end of the method.
			//
			base.OnKeyUp( e );
			if ( e.Handled )
				return;

			// SSP 4/8/02
			// Embeddable edior logic. If there is an editor in edit mode, then
			// pass along the message to the editor.
			// If the editor wanted the last key down (that is IsInputKey off
			// the editor returned true for the last key down), the forward
			// the associated key press as well.
			//
			if ( this.editorWantedLastKeyDown )
			{
				// SSP 3/24/05 BR02979
				// Reset the editorWantedLastKeyDown on key up as well as when we exit edit mode on a cell.
				//
				this.editorWantedLastKeyDown = false;

				UltraGridCell cell = this.ActiveCell;
				if ( null != cell && cell.IsInEditMode )
				{
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
					//EmbeddableEditorBase editor = cell.Column.Editor;
					// SSP 4/18/05 - NAS 5.2 Filter Row
					// We need to forward the navigation keys to the operator drop down if it's 
					// dropped down. Added below if block. Added GetEditorForKeyMessageForwarding
					// helper method.
					//
					// ------------------------------------------------------------------------
					//EmbeddableEditorBase editor = cell.EditorResolved;
					EmbeddableEditorBase editor;
					EmbeddableEditorOwnerBase editorOwnerInfo;
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.GetEditorForKeyMessageForwarding( cell, out editor, out editorOwnerInfo );
					UltraGrid.GetEditorForKeyMessageForwarding( cell, out editor, out editorOwnerInfo );
					// ------------------------------------------------------------------------
				
					if ( null != editor && editor.IsInEditMode && !editor.Focused )
					{
						if ( editor.IsInputKey( e.KeyData ) )
						{
							Debug.Assert( null != cell.Column && null != cell.Column.EditorOwnerInfo,
								"UltraGridColumn.EditorOwnerInfo returned null !" );

							// SSP 4/18/05 - NAS 5.2 Filter Row
							//
							//if ( null != cell.Column && null != cell.Column.EditorOwnerInfo )
							if ( null != editorOwnerInfo )
							{
								editorOwnerInfo.RaiseKeyUp( e );
								e.Handled = true;
							}
						}
					}
				}
			}

			// SSP 6/25/02
			// Call the base class' implementation before doing the processing
			// so that we will fire the key event first and if that the listener
			// sets the Handled to true, we should just return.
			// Moved this in the beginning of the method.
			//
			//base.OnKeyUp( e );

			// JJD 1/17/02
			// Call OnKeyToggled
			//
			this.OnKeyToggled();
		}
		
		#region GetEditorForKeyMessageForwarding

		// SSP 4/18/05 - NAS 5.2 Filter Row
		// We need to forward the navigation keys to the operator drop down if it's 
		// dropped down. Added below if block. Added GetEditorForKeyMessageForwarding
		// helper method.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void GetEditorForKeyMessageForwarding( UltraGridCell activeCell, 
		//    out EmbeddableEditorBase editor, out EmbeddableEditorOwnerBase editorOwnerInfo )
		private static void GetEditorForKeyMessageForwarding( UltraGridCell activeCell,
			out EmbeddableEditorBase editor, out EmbeddableEditorOwnerBase editorOwnerInfo )
		{
			// Forward the navigation keys to the operator drop down if it's dropped down.
			// Added below if block.
			//
			EmbeddableEditorBase filterOperatorEditor = activeCell.Column.FilterOperatorEditor;
			if ( activeCell.IsFilterRowCell && filterOperatorEditor.IsInEditMode
				&& filterOperatorEditor.SupportsDropDown && filterOperatorEditor.IsDroppedDown )
			{
				editor = filterOperatorEditor;
				editorOwnerInfo = activeCell.Column.FilterOperatorEditorOwnerInfo;
			}
			else
			{
				editor = activeCell.EditorResolved;
				editorOwnerInfo = activeCell.EditorOwnerInfo;
			}
		}

		#endregion // GetEditorForKeyMessageForwarding

		// SSP 6/28/05 BR04831
		// Split OnKeyDown into ForwardToValueListHelper and ForwardKeyToEditorHelper.
		// Moved the code from the overridden OnKeyDown to these methods. Before the OnKeyDown
		// was called by the framework directly. Now UltraControlBase.OnKeyDown is called. It calls
		// UltraGridUIElement.ProcessKeyDown which in turn calls these two methods. This change was 
		// made because we need to fire KeyDown first then process keys for swap/filter drop down 
		// value lists if they are dropped down and then process the key action mappings.
		// 
		#region BR04831 Bug Fix
		
		#region Original OnKeyDown - Commented Out

		

		#endregion // Original OnKeyDown - Commented Out

		#region ForwardToValueListHelper

		internal void ForwardToValueListHelper( KeyEventArgs e )
		{
			// SSP 4/15/02
			// Reset the editorWantedLastKeyDown to false
			//
			this.editorWantedLastKeyDown = false;


			// SSP 8/28/01 UWG251
			// We should only call the base classes OnKeyDown 
			// after below dropdown processing
			//base.OnKeyDown( e );

			ValueList valueList = this.swapDropDown;

			// SSP 3/21/02
			// Added code to handle arrow keys for filter drop down as well
			// when it's dropped down.
			//
			// If swapdropdown is not dropped down, then use the filter drop down
			//
			if ( null == valueList || !((IValueList)valueList).IsDroppedDown )				
				valueList = this.filterDropDown;

			// SSP 3/20/02
			// Use the valueList instead of this.swapDropDown, so we can use the same code
			// below for filter drop down as well.
			//
			//if ( null != this.swapDropDown && ((IValueList)this.swapDropDown).IsDroppedDown )
			if ( null != valueList && ((IValueList)valueList).IsDroppedDown )
			{
				// SSP 3/20/02
				//
				//IValueList dropDown = this.swapDropDown as IValueList;
				IValueList dropDown = valueList as IValueList;
				
				// SSP 6/24/02
				// Set the e.Handled to true if we handle the key
				//
				bool handled = false;

				if ( ( ( Keys.Escape & e.KeyData ) == Keys.Escape ) )
				{
					handled = true;
					dropDown.CloseUp();					
				}

                // MBS 9/22/08 - TFS7855
                // We should be checking specifically for the keys in question and not just
                // if the bits are present, since many keys will contain these bits as well
                //
                //else if ( ( Keys.Down & e.KeyData ) == Keys.Down )
                //{
                //    if ( ( Keys.Alt & e.KeyData ) != Keys.Alt )
                else if(e.KeyData == Keys.Down && (Keys.Alt & e.Modifiers) != Keys.Alt)
                {
					{
						handled = true;

						//we don't want to keep the first element highlighted
						//
						// SSP 12/4/06 BR17635
						// 
						//dropDown.SelectedItemIndex++;
						dropDown.MoveNextItemPreviousItem( false );
					}
				}
                // MBS 9/22/08 - TFS7855
                // We should be checking specifically for the keys in question and not just
                // if the bits are present, since many keys will contain these bits as well
                //
                //else if ( ( Keys.Right & e.KeyData ) == Keys.Right )
                else if(Keys.Right == e.KeyData)
				{
					handled = true;

					//we don't want to keep the first element highlighted
					//	
					// SSP 12/4/06 BR17635
					// 
					//dropDown.SelectedItemIndex++;
					dropDown.MoveNextItemPreviousItem( false );
				}
                // MBS 9/22/08 - TFS7855
                // We should be checking specifically for the keys in question and not just
                // if the bits are present, since many keys will contain these bits as well
                //
                //else if ( ( Keys.Up & e.KeyData ) == Keys.Up  )
                //{
                //    if ( ( Keys.Alt & e.KeyData ) != Keys.Alt )
                else if(e.KeyData == Keys.Up && (Keys.Alt & e.Modifiers) != Keys.Alt)
                {
					{
						handled = true;
						if ( dropDown.SelectedItemIndex > 0 )
						{
							// SSP 12/4/06 BR17635
							// 
							//dropDown.SelectedItemIndex--;
							dropDown.MoveNextItemPreviousItem( true );							
						}
					}
				}
                // MBS 9/22/08 - TFS7855
                // We should be checking specifically for the keys in question and not just
                // if the bits are present, since many keys will contain these bits as well
                //
				//else if ( ( Keys.Left & e.KeyData ) == Keys.Left )
                else if (e.KeyData == Keys.Left)
				{					
					handled = true;
					if ( dropDown.SelectedItemIndex > 0 )
					{
						// SSP 12/4/06 BR17635
						// 
						//dropDown.SelectedItemIndex--;
						dropDown.MoveNextItemPreviousItem( true );
					}
				}

				else if ( e.KeyData == Keys.Enter )
				{
					handled = true;

					int i = dropDown.SelectedItemIndex;
					dropDown.CloseUp();
					dropDown.SelectedItemIndex = i;
					if ( dropDown.SelectedItemIndex >= 0 )
					{
						// SSP 3/20/02
						// use the valueList local variable instead because it
						// could be assigned a filter drop down as well.
						//
						//this.SwapDropDown.Owner.OnSelectionChangeCommitted();
						if ( null != valueList.Owner )
							valueList.Owner.OnSelectionChangeCommitted();
					}
					
				}

				// RobA 12/19/01
				// Set handled to true
				e.Handled = handled;
			}
		}

		#endregion // ForwardToValueListHelper

		#region ForwardKeyToEditorHelper

		internal void ForwardKeyToEditorHelper( KeyEventArgs e )
		{
			// SSP 4/8/02
			// Embeddable edior logic. If there is an editor in edit mode, then
			// pass along the message to the editor.
			//
			UltraGridCell cell = this.ActiveCell;
			if ( null != cell && cell.IsInEditMode && !e.Handled )
			{
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//EmbeddableEditorBase editor = cell.Column.Editor;
				// SSP 4/18/05 - NAS 5.2 Filter Row
				// We need to forward the navigation keys to the operator drop down if it's 
				// dropped down. Added below if block. Added GetEditorForKeyMessageForwarding
				// helper method.
				//
				// ------------------------------------------------------------------------
				//EmbeddableEditorBase editor = cell.EditorResolved;
				EmbeddableEditorBase editor;
				EmbeddableEditorOwnerBase editorOwnerInfo;
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.GetEditorForKeyMessageForwarding( cell, out editor, out editorOwnerInfo );
				UltraGrid.GetEditorForKeyMessageForwarding( cell, out editor, out editorOwnerInfo );
				// ------------------------------------------------------------------------
				
				if ( null != editor && editor.IsInEditMode && !editor.Focused )
				{
					if ( editor.IsInputKey( e.KeyData ) )
					{
						this.editorWantedLastKeyDown = true;

						// SSP 4/18/05 - NAS 5.2 Filter Row
						//
						//if ( null != cell.Column && null != cell.Column.EditorOwnerInfo )
						//Debug.Assert( null != cell.Column && null != cell.Column.EditorOwnerInfo, "UltraGridColumn.EditorOwnerInfo returned null !" );
						Debug.Assert( null != editorOwnerInfo, "UltraGridColumn.EditorOwnerInfo returned null !" );
						if ( null != editorOwnerInfo )
						{
							editorOwnerInfo.RaiseKeyDown( e );
							e.Handled = true;
						}
					}
				}
			}
			
			
			// SSP 6/25/02
			// Call the base class' implementation before doing the processing
			// so that we will fire the key event first and if that the listener
			// sets the Handled to true, we should just return.
			// Moved this in the beginning of the method.
			//
			//base.OnKeyDown( e );

			// JJD 1/17/02
			// Call OnKeyToggled
			//
			this.OnKeyToggled();
		}

		#endregion // ForwardKeyToEditorHelper

		#endregion // BR04831 Bug Fix

		internal void InternalHandleDataError( DataErrorInfo dataErrorInfo )
		{
			this.InternalHandleDataError( dataErrorInfo, false );
		}

		// SSP 10/6/04 - UltraCalc
		// Added an overload of InternalHandleDataError so we can supress the error message
		// box when the formula evaluation result on a cell doesn't match the column's data type.
		//
		//internal void InternalHandleDataError( DataErrorInfo dataErrorInfo )
		internal void InternalHandleDataError( DataErrorInfo dataErrorInfo, bool defaultCancelValue )
		{
			ErrorEventArgs e = new ErrorEventArgs( dataErrorInfo );

						
			GridEventManager eventManager = this.EventManager;
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = this.DisplayLayout;
			
			if ( null == eventManager || null == layout)
				return;

			// AS 1/8/03 - fxcop
			// Do not compare against string.empty - test the length instead
			//if ( null == e.ErrorText || string.Empty.Equals( e.ErrorText ) )
			if ( null == e.ErrorText || e.ErrorText.Length == 0 )
			{
				if ( null != dataErrorInfo.Exception )
					e.ErrorText = dataErrorInfo.Exception.Message;
			}

			// SSP 10/6/04 - UltraCalc
			// Added defaultCancelValue parameter.
			//
            e.Cancel = defaultCancelValue;

			this.OnError( e );

			// if displaying of dialog is not cancelled 
			if ( !e.Cancel )
			{
				// Display messagebox
				//
				System.Windows.Forms.MessageBox.Show( e.ErrorText, 
					SR.GetString("DataErrorMessageTitle"), // "Data Error", 
					System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information );
			}			
		}

		#region InternalHandleMultiCellOperationError

		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// 
		internal void InternalHandleMultiCellOperationError( MultiCellOperationErrorInfo errorInfo, bool defaultCancelValue )
		{
			GridEventManager eventManager = this.EventManager;
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = this.DisplayLayout;
			
			if ( null == eventManager || null == layout)
				return;

			ErrorEventArgs e = new ErrorEventArgs( errorInfo );
			e.Cancel = defaultCancelValue;

			this.FireEvent( GridEventIds.Error, e );

			if ( ! e.Cancel && MultiCellOperationErrorInfo.ErrorAction.Stop != errorInfo.Action 
				&& MultiCellOperationErrorInfo.ErrorAction.Revert != errorInfo.Action
				&& null != e.ErrorText && e.ErrorText.Length > 0 )
			{
				Form form = layout.Form;

				MessageBoxButtons buttons = errorInfo.CanContinueWithRemainingCells
					? MessageBoxButtons.OKCancel : MessageBoxButtons.OK;

				DialogResult result = MessageBox.Show(
					form,
					e.ErrorText,
					errorInfo.GetErrorMsgBoxTitle( ),
					buttons,
					MessageBoxIcon.Exclamation );

				if ( DialogResult.Cancel == result )
					errorInfo.Action = MultiCellOperationErrorInfo.ErrorAction.Stop;
			}
		}

		#endregion // InternalHandleMultiCellOperationError

		// SSP 9/3/02 
		// Marked EditControl property to false to since it's obsoleted.
		//
		/// <summary>
		/// This property is obsoleted. It will always return null. Use <see cref="UltraGridColumn.Editor"/> instead. Returns a reference to the control being used while cell editing is taking place.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>Note:</b>This property is obsoleted. It will always return null. Use <see cref="UltraGridColumn.Editor"/> instead.</p>
		/// <p class="body">If the cell is not in edit mode, this property returns Null. The control returned by this property will either be a text box or an UltraMaskedEdit control.</p>
		/// <p class="body"><b>Note:</b> Use UltraGridColumn.Editor property instead to access the editor being used for rendering as well editing cells in that column.</p>
		/// </remarks>		
		[ 
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
		Browsable( false ),
		EditorBrowsable( EditorBrowsableState.Never ),
		Obsolete( "EditControl property is obsolete. Use UltraGridColumn.Editor property instead to access the editor being used for rendering as well editing cells in that column.", true ) 
		]
		public Control EditControl
		{
			get
			{
				// SSP 4/26/02
				// EM Embeddable editors. This property is deprecated now.
				//
				return null;

				
			}
		}

		// SSP 4/26/02
		// Em Embeddable editors.
		// Commented below code out.
		

		// SSP 4/26/02
		// Em. Embeddable editors.
		// Moved this code into layout becuase that's where the EditorWithMask is created
		// and kept.
		//
		

		// SSP 4/26/02
		// Em Embeddable editors.
		// Commented below code out.
		
	
		// SSP 10/20/04 UWG3713
		// Added IsItemSelectableWithCurrentSelection method and moved the code from 
		// ISelectionManager.IsItemSelectableWithCurrentSelection into this method so we can
		// call it from the set of the Selected property of row, cell and column objects.
		//
		#region IsItemSelectableWithCurrentSelection

		internal bool IsItemSelectableWithCurrentSelection( Infragistics.Shared.ISelectableItem item )
		{
			GridItemBase gridItem = item as GridItemBase;

			// if the item is a row check current selected rows
			//
			if ( item is Infragistics.Win.UltraWinGrid.UltraGridRow )
			{
				// MD 5/5/08
				// Found while making unit tests for 8.2 - Rotated Column Headers
				// The selected member variable could be null.
				if ( this.selected == null )
					return true;

				if ( this.selected.Rows.Count < 1 )
					return true;

				// Return false if the bands don't match
				//
				if ( this.selected.Rows[0].Band != gridItem.Band )
					return false;

				// check if the row is a group by row
				//
				if ( item is Infragistics.Win.UltraWinGrid.UltraGridGroupByRow )
				{
					// if the select row isn't a group by row return false
					//
					if ( !( this.selected.Rows[0] is Infragistics.Win.UltraWinGrid.UltraGridGroupByRow ) )
						return false;

					// Return true only if the group by coluns are the same
					//
					return ( ((UltraGridGroupByRow)item).Column == ((UltraGridGroupByRow)this.selected.Rows[0]).Column );
				}
				else
				{
					// if the select row is a group by row return false
					//
					if ( this.selected.Rows[0] is Infragistics.Win.UltraWinGrid.UltraGridGroupByRow )
						return false;
				}

				return true;
			}

			// if the item is a cell check current selected cells
			//
			if ( item is Infragistics.Win.UltraWinGrid.UltraGridCell )
			{
				// MD 5/5/08
				// Found while making unit tests for 8.2 - Rotated Column Headers
				// The selected member variable could be null.
				if ( this.selected == null )
					return true;

				if ( this.selected.Cells.Count < 1 )
					return true;

				// Return true only if the bands match
				//
				return ( this.selected.Cells[0].Band == gridItem.Band );
			}

			// if the item is a header check current selected columns
			//
			if ( item is Infragistics.Win.UltraWinGrid.HeaderBase )
			{
				// MD 5/5/08
				// Found while making unit tests for 8.2 - Rotated Column Headers
				// The selected member variable could be null.
				if ( this.selected == null )
					return true;

				if ( this.selected.Columns.Count < 1 )
					return true;

				// Return true only if the bands match
				//
				return ( this.selected.Columns[0].Band == gridItem.Band );
			}

			Debug.Fail( "Invalid Item passed into UltraGird IsItemSelectableWithCurrentSelection. Item: " + item.ToString() );

			return true;
		}
	
		#endregion // IsItemSelectableWithCurrentSelection
	
		/// <summary>
		/// Determines if selecting the item would be compatible
		/// with the current selection
		/// </summary>
		/// <param name="item">The selectable item</param>
		/// <returns>True if the item is selectable</returns>
		bool ISelectionManager.IsItemSelectableWithCurrentSelection( Infragistics.Shared.ISelectableItem item )
		{
			// JJD 10/04/01
			// Implemented 
			//
			GridItemBase gridItem = item as GridItemBase;

			if ( gridItem == null )
			{
				Debug.Fail( "Item not a GridItemBase derived class in UltraGird IsItemSelectableWithCurrentSelection. Item: " + item.ToString() );
				return false;
			}

			// SSP 10/20/04 UWG3713
			// Moved the code that checks if the band matches into the new 
			// IsItemSelectableWithCurrentSelection method. The new code should 
			// do the same as before.
			// 
			// --------------------------------------------------------------------
			// SSP 4/20/05 - NAS 5.2 Filter Row
			// Use the Selectable property instead.
			//
			//if ( item is UltraGridRow && ! ((UltraGridRow)item).CanSelectRow
			//	|| item is UltraGridCell && ! ((UltraGridCell)item).CanSelectCell )
			if ( ! gridItem.Selectable )
				return false;

			return this.IsItemSelectableWithCurrentSelection( item );
			
			
			// --------------------------------------------------------------------
		}
		
		

		/// <summary>
		/// Called when a dragging operation is about to begin.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="mousePosition">The position of the mouse at the start of the drag</param>
		/// <returns>Returning true means that it was handled internally and no further action should be taken</returns>
		// JM 01-15-02
		//bool ISelectionManager.OnDragStart(ISelectableItem item)
		bool ISelectionManager.OnDragStart(ISelectableItem item, Point mousePosition)
		{
			// JJD 1/22/02 - UWG876
			// Fire the event first to let the use have the chance to preempt the
			// default processing
			//
			CancelEventArgs e = new CancelEventArgs();

			this.isDragging = false;
            
			this.FireEvent( GridEventIds.OnSelectionDrag, e );

			if ( e.Cancel )
				return true;

			bool ret = false;

			// JJD 1/11/02
			// Cache the card area element during the drag
			//
			this.cardAreaPivot = this.GetCardAreaFromItem( item );

			// MRS 4/21/05 - BR03472
			// In RowLayout mode, the item could be a cell. 						
			UltraGridCell cell = item as UltraGridCell;
			if (cell != null && 
				cell.Band.UseRowLayoutResolved)
			{
				item = cell.Column.Header;
			}
			

			// RobA UWG679 11/21/01
			// added check for IsDraggable
			//
			// JJD 1/22/02 - UWG876
			// Check the Daraggable property since IsDraggable always returns true
			//
			if ( item is ColumnHeader && ((ColumnHeader)item).Draggable )
			{
				Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader = (ColumnHeader)item;
								
				ret = columnHeader.StartDrag( );
			}
			else if ( item is GroupHeader && ((GroupHeader)item).Draggable )
			{
				Infragistics.Win.UltraWinGrid.GroupHeader groupHeader = (GroupHeader)item;

				ret = groupHeader.StartDrag( );
			}

			// SSP 9/27/01 UWG341
			// set a flag that would indicate that we are currently dragging
			// 
			this.isDragging = ret;

			return ret;
		}

		private CardAreaUIElement GetCardAreaFromItem( ISelectableItem item )
		{
			UltraGridRow row = null;

			if ( item is UltraGridRow )
				row = item as UltraGridRow;
			else
				if ( item is UltraGridCell )
				row = ((UltraGridCell)item).Row;

			if ( row != null && row.IsCard )
			{
				return this.DisplayLayout.UIElement.GetDescendant( 
					typeof(CardAreaUIElement), 
					new object[]{ row.ParentCollection, this.ActiveColScrollRegion, this.ActiveRowScrollRegion} ) as CardAreaUIElement;
			}

			return null;
		}


		void ISelectionManager.OnDragMove( ref MouseMessageInfo msginfo )
		{	
			Point mouseLoc = new Point( msginfo.X, msginfo.Y );
			
			// MRS 3/15/04 - Column Moving for RowLayouts
//			if ( null != this.dragEffect )
//				this.dragEffect.OnDragMove( mouseLoc ); 			
			this.DragMoveHelper(mouseLoc);
		}
		
		void ISelectionManager.OnDragEnd( bool cancelled )
		{
			// SSP 8/17/05 BR05496
			// Added OnDragEndHelper and moved all the code from here into that method. Added 
			// code to suspend the designer change notifications while the drag operation is 
			// being performed otherwise multiple designer change notifications may be sent
			// at design time.
			// 
			bool origDesignerChangeNotificationsDisabled = this.DesignerChangeNotificationsDisabled;
			try
			{
				this.DesignerChangeNotificationsDisabled = true;

				this.OnDragEndHelper( cancelled );
			}
			finally
			{
				this.DesignerChangeNotificationsDisabled = origDesignerChangeNotificationsDisabled;
			}
		}

		// SSP 8/17/05 BR05496
		// Added OnDragEndHelper. Code in there was moved from the ISelectionManager.OnDragEnd.
		// 
		private void OnDragEndHelper( bool cancelled )
		{
			// MRS 4/12/05
			Point pointInControlCoords = this.PointToClient( Cursor.Position );
			
			// SSP 6/28/05 - NAS 5.3 Column Chooser
			//
			bool isMouseOverColumnChooser = this.IsOverColumnChooser( pointInControlCoords, false );

			// MRS 4/12/05 - The grid normally tries to check if the middle 
			// of the header being dragged is over the GroupBy Box. We need
			// to prevent this from happening when doing a RowLayoutDrag. 
			// So we store the gridBagLayoutDragStrategy in temporary variable
			// so we can fool it into thinking there's a rowLayout drag going
			// on - even though it has already completed. 
			GridBagLayoutDragStrategy tempGridBagLayoutDragStrategy = this.gridBagLayoutDragStrategy;

			// MRS 3/25/05 - RowLayout column dragging
			if (null != this.gridBagLayoutDragStrategy)
			{
				// SSP 6/28/05 - NAS 5.3 Column Chooser
				// If the column is being dragged from the grid and dropped over a column chooser then
				// don't take any normal drag-and-drop action. The call to ColumnChooser_OnDragEnd 
				// further below will take the necessary steps in that case (mainly hide the column).
				// 
				//if (!cancelled)
				if ( ! isMouseOverColumnChooser && ! cancelled )
				{	
					// MRS 4/12/05
					//Point pointInControlCoords = this.PointToClient( Cursor.Position );					
					
					// MRS 4/21/05 - BR03503
					//if (!GridUtils.IsPointInGroupByBox(this, pointInControlCoords))
					GridBagLayoutDragManager gridBagLayoutDragManager = this.gridBagLayoutDragStrategy.GridBagLayoutDragManager as GridBagLayoutDragManager;
					
					if (gridBagLayoutDragManager.isGroupByButtonBeingDragged &&
						!GridUtils.IsPointInGroupByBox(this, pointInControlCoords))
					{
						// MRS 5/20/05 - BR03632
						// Keep track of whether the point being dropped on
						// was in a valid layout area. 
						bool isPointInValidLayoutArea = gridBagLayoutDragManager.IsPointInValidLayoutArea(pointInControlCoords);

						RowLayoutColumnInfo ci = ((GridBagLayoutDragManager)(this.gridBagLayoutDragStrategy.GridBagLayoutDragManager)).GetDragItemRowLayoutColumnInfo();
						if (ci != null &&
							ci.Column != null )
						{
							ci.Column.SetGroupByColumnStatus(false);
						}

						// MRS 5/20/05 - BR03632
						// If the point was not in a valid layout area, we do 
						// not want to drop, just cancel. 
						if (! isPointInValidLayoutArea)
							this.ResetGridBagLayoutDragStrategy();
						else 
						{
							this.EndGridBagLayoutDrag(pointInControlCoords);

							// SSP 6/30/05 - NAS 5.3 Column Chooser
							//
							if ( null != this.cachedColumnChooserDragInfo )
								this.cachedColumnChooserDragInfo.groupByButtonDroppedOverColumnHeaders = DefaultableBoolean.True;
							// --------------------------------------------------------------------------------
						}
					}
					else
						this.EndGridBagLayoutDrag(pointInControlCoords);					
				}
				else
					this.ResetGridBagLayoutDragStrategy();
			}	

			if ( null != this.dragEffect )
			{
				// SSP 6/28/05 - NAS 5.3 Column Chooser
				// Don't move the item if the mouse is over a column chooser. The call to 
				// ColumnChooser_OnDragEnd further below will take the necessary steps in that case.
				// 
				//if ( !cancelled )
				if ( ! isMouseOverColumnChooser && ! cancelled )
				{
					try
					{
						// MRS 4/12/05 - The grid normally tries to check if the middle 
						// of the header being dragged is over the GroupBy Box. We need
						// to prevent this from happening when doing a RowLayoutDrag. 
						// So we store the gridBagLayoutDragStrategy in temporary variable
						// so we can fool it into thinking there's a rowLayout drag going
						// on - even though it has already completed. 
						this.gridBagLayoutDragStrategy = tempGridBagLayoutDragStrategy;

						// MRS 4/12/05
						//this.dragEffect.OnLeftButtonUp( this.PointToClient( Cursor.Position ) );
						this.dragEffect.OnLeftButtonUp( pointInControlCoords );
					}
					finally
					{
						this.gridBagLayoutDragStrategy = null;
					}
				}
				else
				{
					this.dragEffect.OnCancelMode( );
				}

				// reset the cursror to default so appropriate cursor
				// is displayed after dragging has finished
				Cursor.Current = Cursors.Default;
			}
			
			// SSP 9/27/01 UWG341
			// set isDragging to false
			// 
			this.isDragging = false;

			//			this.KillDragScrollColTimer();
			//			this.KillDragScrollRowTimer();
			//
			//			// set our dragging flag to false
			//			this.dragging = false;

			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// 
			this.ColumnChooser_OnDragEnd( cancelled, pointInControlCoords );

			// SSP 9/9/05 BR06315
			// Since we don't apply the hot-tracking appearance while dragging, when the
			// dragging ends, invalidate the hot-tracked element so it redraws with the
			// hot-tracked appearance.
			// 
			if ( null != this.DisplayLayout )
				this.DisplayLayout.InvalidateHotTrackedElement( );
		}

		Control ISelectionManager.GetControl( )
		{
			return this;
		}

		void ISelectionManager.EnterSnakingMode( ISelectableItem item )
		{
			Infragistics.Win.UltraWinGrid.UltraGridCell cell	= item as UltraGridCell;

			if ( cell == null )
				return;

			ISelectableItem pivotItem = ((ISelectionManager)this).GetPivotItem( item );

			Infragistics.Win.UltraWinGrid.UltraGridCell pivotCell = pivotItem as UltraGridCell;
			
			if ( pivotCell == null ) 
				return;

			// if the cell is on the same row as the pivot don't
			// go into snaking mode
			//
			if ( pivotCell.Row == cell.Row )
			{
				this.DisplayLayout.Snaking = false;
				return;
			}

			// set out internal flag that tells us we are
			// in snaking mode
			//
			this.DisplayLayout.Snaking = true;
		}

				
		/// <summary>
		/// Deletes all rows that are selected.
		/// </summary>
		///	<remarks>
		///	<p class="body">Invoke this method to delete all selected rows. A particular row, regardless of whether it is selected, can be deleted by invoking its <b>Delete</b> method.</p>
		///	<p class="body">When one or more selected rows are deleted, the <b>BeforeRowsDeleted</b> event is generated, which provides an opportunity to prevent a specific row from being deleted.</p>
		///	<p class="body">When a row is deleted, it is removed from the control and its corresponding record is deleted from the data source. If the record cannot be removed from the data source, the <b>Error</b> event is generated.</p>
		///	<p class="body">Selected UltraGridRow objects are contained in a SelectedRows collection, which can be accessed via the <b>Rows</b> property of the <b>Selected</b> property of the control.</p>
		///	</remarks>
		public void DeleteSelectedRows( )
		{
			this.DisplayLayout.DeleteSelectedRows( false );
		}

		// SSP 4/25/03 UWG2185
		// Added an overload of DeleteSelectedRows that takes in a parameter that specifies whether to
		// display the delete confirmation dialog.
		//
		/// <summary>
		/// Deletes all rows that are selected.
		/// </summary>
		/// <param name="displayPrompt">Specifies whether to display the delete confirmation prompt.</param>
		///	<remarks>
		///	<p class="body">Invoke this method to delete all selected rows. A particular row, regardless of whether it is selected, can be deleted by invoking its <b>Delete</b> method.</p>
		///	<p class="body">When one or more selected rows are deleted, the <b>BeforeRowsDeleted</b> event is generated, which provides an opportunity to prevent a specific row from being deleted.</p>
		///	<p class="body">When a row is deleted, it is removed from the control and its corresponding record is deleted from the data source. If the record cannot be removed from the data source, the <b>Error</b> event is generated.</p>
		///	<p class="body">Selected UltraGridRow objects are contained in a SelectedRows collection, which can be accessed via the <b>Rows</b> property of the <b>Selected</b> property of the control.</p>
		///	</remarks>
		public void DeleteSelectedRows( bool displayPrompt )
		{
			this.DisplayLayout.DeleteSelectedRows( false, displayPrompt );
		}

		ISelectionStrategy ISelectionManager.GetSelectionStrategy( ISelectableItem item )
		{
			ISelectionStrategy strategy = null;

			// if a filter was supplied call its GetSelectionStrategy
			// method so it gets first crack at supplying the
			// strategy
			//
			if ( this.SelectionStrategyFilter != null )
				strategy = this.SelectionStrategyFilter.GetSelectionStrategy( item );

			// if a strategy wasn't supplied by the filter then get the 
			// appropriate strategy from the band for the item type
			//
			if ( null == strategy )
			{
				GridItemBase gridItem = item as GridItemBase;

				Debug.Assert( gridItem != null, "Selectable item not grid item in GetSelectionStrategy" );

				if ( gridItem != null )
					strategy = gridItem.SelectionStrategyDefault;
			}

			return strategy;
		}

	
		/// <summary>
		/// Internal property that returns the PrintLayout during printing
		/// </summary>
		internal protected override UltraGridLayout PrintLayout
		{
			get
			{
				return this.printLayout;
			}
		}

		/// <summary>
		/// Initiates the creation of a printed report of the grid data.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Print</b> method initiates a print job. Invoking this method triggers the process of preparing a printed report based on the data in the grid and sending it to the printer. This process has several steps and involves interaction between print-specific objects and events within the control.</p>
		/// <p class="body">When the  print job begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		public void Print()
		{
			//this.Print( false, false, null, null );
			this.Print( null, null );
		}
		#region code commented out
		
		#endregion
		
		/// <summary>
		/// Initiates the creation of a printed report of the grid data.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Print</b> method initiates a print job. Invoking this method triggers the process of preparing a printed report based on the data in the grid and sending it to the printer. This process has several steps and involves interaction between print-specific objects and events within the control.</p>
		/// <p class="body">When you invoke this method, you specify a Layout object that controls the formatting of the grid data when printed. The Layout object gives you the opportunity to create a custom-formated report based on the data in teh grid, applying different attributes such as fonts, colors and arrangement of data specifically for the printed page.</p>
		/// <p class="body">When the  print job begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		/// <param name="layout">Optional DisplayLayout object</param>
		//public void Print ( bool showPageDialog, bool showPrintDialog, Layout layout )
		public void Print ( UltraGridLayout layout )
		{
			this.Print( layout, null );
		}

		/// <summary>
		/// Initiates the creation of a printed report of the grid data.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Print</b> method initiates a print job. Invoking this method triggers the process of preparing a printed report based on the data in the grid and sending it to the printer. This process has several steps and involves interaction between print-specific objects and events within the control.</p>
		/// <p class="body">When the  print job begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		/// <param name="retainRowPropertyCategories">Sepcifies which categories of the settings on the rows to apply to rows printed on the grid.</param>
		public void Print( RowPropertyCategories retainRowPropertyCategories )
		{
			this.Print( null, null, retainRowPropertyCategories );
		}

		/// <summary>
		/// Initiates the creation of a printed report of the grid data.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Print</b> method initiates a print job. Invoking this method triggers the process of preparing a printed report based on the data in the grid and sending it to the printer. This process has several steps and involves interaction between print-specific objects and events within the control.</p>
		/// <p class="body">When you invoke this method, you specify a Layout object that controls the formatting of the grid data when printed. The Layout object gives you the opportunity to create a custom-formated report based on the data in teh grid, applying different attributes such as fonts, colors and arrangement of data specifically for the printed page.</p>
		/// <p class="body">When the  print job begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		/// <param name="layout">Optional DisplayLayout object</param>
		/// <param name="retainRowPropertyCategories">Sepcifies which categories of the settings on the rows to apply to rows printed on the grid.</param>
		//public void Print ( bool showPageDialog, bool showPrintDialog, Layout layout )
		public void Print ( UltraGridLayout layout, RowPropertyCategories retainRowPropertyCategories )
		{
			this.Print( layout, null, retainRowPropertyCategories );
		}

		/// <summary>
		/// Initiates the creation of a printed report of the grid data.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Print</b> method initiates a print job. Invoking this method triggers the process of preparing a printed report based on the data in the grid and sending it to the printer. This process has several steps and involves interaction between print-specific objects and events within the control.</p>
		/// <p class="body">When you invoke this method, you specify a Layout object that controls the formatting of the grid data when printed. The Layout object gives you the opportunity to create a custom-formated report based on the data in teh grid, applying different attributes such as fonts, colors and arrangement of data specifically for the printed page.</p>
		/// <p class="body">When the  print job begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output. Any settings you have applied to the <b>PrintDocument</b> object specified as a paramter of the <b>Print</b> method will be passed along to the <b>PrintDocument</b> that appears in the <b>InitializePrint</b> method. You might do this to specify certain behaviors, such as displaying the Print or Print Setup dialogs prior to printing the report/.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		/// <param name="layout">Optional layout object</param>
		/// <param name="printDocument">Optional print document</param>		
		//public void Print( bool showPageDialog, bool showPrintDialog, Layout layout,
		//	PrintDocument printDocument )
		public void Print( UltraGridLayout layout, PrintDocument printDocument )
		{
			this.Print( layout, printDocument, RowPropertyCategories.None );
		}

		// SSP 1/14/03 UWG1892
		// Added overloads for Print and PrintPreview methods that take in 
		// retainRowPropertyCategories paramter.
		//
		/// <summary>
		/// Initiates the creation of a printed report of the grid data.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Print</b> method initiates a print job. Invoking this method triggers the process of preparing a printed report based on the data in the grid and sending it to the printer. This process has several steps and involves interaction between print-specific objects and events within the control.</p>
		/// <p class="body">When you invoke this method, you specify a Layout object that controls the formatting of the grid data when printed. The Layout object gives you the opportunity to create a custom-formated report based on the data in teh grid, applying different attributes such as fonts, colors and arrangement of data specifically for the printed page.</p>
		/// <p class="body">When the  print job begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output. Any settings you have applied to the <b>PrintDocument</b> object specified as a paramter of the <b>Print</b> method will be passed along to the <b>PrintDocument</b> that appears in the <b>InitializePrint</b> method. You might do this to specify certain behaviors, such as displaying the Print or Print Setup dialogs prior to printing the report/.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrint"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		/// <param name="layout">Optional layout object</param>
		/// <param name="printDocument">Optional print document</param>		
		/// <param name="retainRowPropertyCategories">Sepcifies which categories of the settings on the rows to apply to rows printed on the grid.</param>
		//public void Print( bool showPageDialog, bool showPrintDialog, Layout layout,
		//	PrintDocument printDocument )
		public void Print( UltraGridLayout layout, PrintDocument printDocument, RowPropertyCategories retainRowPropertyCategories )
		{

			//create a new layout based on passed in layout
			//create a printdocument based on passed in printdocument
			//create a new print manager
			//
			// SSP 1/14/03 UWG1892
			//
			//this.PrintHelper( layout, printDocument );
			this.PrintHelper( layout, printDocument, retainRowPropertyCategories );

			//Set flag so that when the BeginPrint event is called we 
			//know that we are printing
			//
			this.printManager.CalledFromPrint = true;
			
			try
			{	
				//check for a default printer
				//
				if ( this.printManager.PrintDocument.PrinterSettings.PrinterName == "<no default printer>" )
				{
					// AS - 2/20/02
					// We should not be showing a messagebox - instead we should throw
					// an error since this is only invoked externally.
					//
					//MessageBox.Show("No Default Printer Found!", "Printer error", MessageBoxButtons.OK );
					//return;
					throw new NotSupportedException( Shared.SR.GetString("LE_NotSupportedException_291") );
				}	
		
				//Fire InitializePrint event
				//
				// SSP 12/15/03 UWG2560
				// Added PrintLayout property to InitializePrint and InitializePrintPreview event args.
				//
				//CancelablePrintEventArgs e = new CancelablePrintEventArgs( this.printManager.PrintDocument, this.printManager.DefaultLayoutInfo );
				CancelablePrintEventArgs e = new CancelablePrintEventArgs( this.printManager.PrintDocument, this.printManager.DefaultLayoutInfo, this.PrintLayout );
				this.FireEvent( GridEventIds.InitializePrint, e );

				//return if cancelled
				//
				if ( e.Cancel )
					return;

				//MRS 6/10/04 - UWG2968
				//if the user changed FitWidthToPages and AutoFitColumns is true, 
				// dirty the metrics to make sure the columns are sized to the page. 
				if ( this.PrintLayout != null &&
					e.DefaultLogicalPageLayoutInfo.FitWidthToPages != 0 
					&& this.PrintLayout.AutoFitAllColumns )
				{
					//dirty metrics to make sure any changes made are visible
					this.PrintLayout.GetColScrollRegions( false ).DirtyMetrics( false );
				}

				// RobA UWG694 11/8/01 moved this, since expanding everything might take 
				// some time, we should wait until after we fire 
				// InitializePrintPreview or InitializePrint
				//
				this.FormatPrintLayout();
				
				
				//fires BeginPrint and PrintPage event
				//
				this.printManager.PrintDocument.Print();
			}
			
			finally
			{
				//Dispose what we created during this process
				//
				this.DisposePrintHelper();
			}
		}

		/// <summary>
		/// Internal property that returns true during a print operation
		/// </summary>
		internal protected override bool IsPrinting
		{
			get
			{
				return this.isPrinting;
			}

			set
			{
				if ( value != this.isPrinting )
					this.isPrinting = value;
			}
		}

		internal PrintManager PrintManager
		{
			get
			{
				return this.printManager;
			}
		}

		// SSP 6/18/02 UWG1229
		//
		internal bool Printing_DontInitializeRows
		{
			get
			{
				return this.printing_DontInitializeRows;
			}
		}

		// SSP 1/14/03 UWG1892
		// Added retainRowPropertyCategories parameter.
		//
		//private void PrintHelper( UltraGridLayout layout, PrintDocument printDocument )
		private void PrintHelper( UltraGridLayout layout, PrintDocument printDocument, RowPropertyCategories retainRowPropertyCategories )
		{
			//create a new layout
			//
			this.printLayout = new UltraGridLayout();

			// SSP 1/14/03 UWG1892
			// Added retainRowPropertyCategories parameter to Print and PrintPreview methods for
			// making WYSIWIG style printing. 
			//
			this.printLayout.RetainRowPropertyCategories = retainRowPropertyCategories;

			//Create a new printmanager
			//
			this.printManager = new PrintManager( this );

			// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
			// Copy over the print document settings.
			UltraGridPrintDocument gridPrintDocument = printDocument as UltraGridPrintDocument;

			if (gridPrintDocument != null)
			{
				this.printManager.DefaultLayoutInfo.FitWidthToPages = gridPrintDocument.FitWidthToPages;
				this.printManager.DefaultLayoutInfo.ColumnClipMode = gridPrintDocument.ColumnClipMode;
			}

			//set grid
			//
			this.printLayout.InitGrid( this );	

			// SSP 6/18/02 UWG1229
			// Added a flag to prevent the rows from being loaded until the 
			// print layout and the main layout have been merged. We want to
			// carry over any unbound columns first before firing the initialize
			// row.
			// Enclosed already existing code into try-finally block so we can
			// reset the printing_DontInitializeRows flag to false.
			//
			try
			{
				this.printing_DontInitializeRows = true;
			
				// SSP 3/12/04 - UWG2995
				// Copy the MaxBandDepth before loading the data structure.
				//
				if ( null != layout )
					this.printLayout.MaxBandDepth = layout.MaxBandDepth;
				else if ( null != this.DisplayLayout )
					this.printLayout.MaxBandDepth = this.DisplayLayout.MaxBandDepth;

				//Init Bands from datasource
				//
				this.printLayout.ListManagerUpdated();	
			
				//Need to set isprinting when we Initialize from
				//or else sorting does not work
				//
				this.isPrinting = true;
		
				//if passed in layout is null use grid's layout
				//
				if ( layout != null )
					this.printLayout.InitializeFrom(layout, PropertyCategories.All);
				else
					this.printLayout.InitializeFrom(this.DisplayLayout, PropertyCategories.All);

				// SSP 1/17/05 BR01753
				// Print and export layout won't calculate formulas but rather they will copy over 
				// the calculated values from the display layout. We need to ensure that all the 
				// columns are calculated before we go ahead with printing.
				//
				this.DisplayLayout.EnsureAllFormulasCalculated( );

				//reset flag
				//
				this.isPrinting = false;
			}
			finally
			{
				// SSP 6/18/02 UWG1229
				//
				this.printing_DontInitializeRows = false;

				// Then call the ApplyLoadedBands to apply the loaded bands.
				//
				this.printLayout.ApplyLoadedBands( );

				//MRS 5/11/05 - BR02804
				foreach (UltraGridBand sourceBand in this.DisplayLayout.SortedBands)
				{
					if (sourceBand.ParentBand == null)
					{
						UltraGridBand destinationBand = this.printLayout.SortedBands[0];
						destinationBand.CopyPrintRelatedProperties(sourceBand, this.printLayout.RetainRowPropertyCategories);
					}
					else if (this.printLayout.SortedBands.Exists(sourceBand.Key))
					{
						UltraGridBand destinationBand = this.printLayout.SortedBands[sourceBand.Key];
						destinationBand.CopyPrintRelatedProperties(sourceBand, this.printLayout.RetainRowPropertyCategories);
					}
				}

				// SSP 8/14/03 UWG2589 - Filter Action
				// When Filter Action functionality was added, the Hidden property's behavior was changed.
				// Now the Hidden property won't get set when the filter is evaluated. So we need to copy
				// the column filters on every rows collection when printing or exporting to excel.
				//
				// SSP 1/18/05 BR01753
				// Only enable calculations in display layout. Print and export layouts will
				// copy over the calculated values from the display layout.
				//
				//UltraGridRow.CopyColumnFilters( this.Rows, this.printLayout.Rows );
				UltraGridRow.CopyPrintRelatedSettings( this.Rows, this.printLayout.Rows );
			}

			// RobA UWG694 11/8/01 moved this, since expanding everything might take 
			// some time, we should wait until after we fire 
			// InitializePrintPreview or InitializePrint
			//
			//this.FormatPrintLayout();			
						
			//if passed in printDocument is not null set the
			//PrintManager's PrintDocument
			//
			if ( printDocument != null )
				this.printManager.PrintDocument = printDocument;
			

			//Hook up print events
			//
			this.printManager.PrintDocument.PrintPage += new PrintPageEventHandler(this.printManager.PrintPage);									
			this.printManager.PrintDocument.BeginPrint += new PrintEventHandler( this.printManager.BeginPrint );			

			// MRS 5/21/04 - UWG2915
			this.printManager.PrintDocument.EndPrint += new PrintEventHandler( this.printManager.EndPrint );			
		}

		private void FormatPrintLayout()
		{
			//RobA UWG559 10/17/01
			System.Windows.Forms.Cursor oldCursor = Cursor.Current;

			Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

			//Make sure there is only one ColScrollRegion and 
			//one RowScrollRegion
			//
			if ( this.printLayout.ColScrollRegions.Count > 1 )
				this.printLayout.ColScrollRegions.Clear();
			
			if ( this.printLayout.RowScrollRegions.Count > 1 )
				this.printLayout.RowScrollRegions.Clear();


			// make sure the columns don't have exclusive regions set since
			// this doesn't make sense for a print layout
			//
			foreach ( Infragistics.Win.UltraWinGrid.UltraGridBand band in this.printLayout.SortedBands )
			{
				foreach ( Infragistics.Win.UltraWinGrid.UltraGridColumn column in band.Columns )
				{
					column.Header.ExclusiveColScrollRegion = null;
				}
			}

			//Expand all bands
			//
			//for ( int i=0; i< this.printLayout.Rows.Count; ++i )
			//{
			//RobA 10/5/01 Use the ExpandAll method

			
			//	this.printLayout.Rows[i].ExpandAll();
			//}

			//RobA 11/7/01 use the new ExpandAll of the rows			
			// SSP 1/14/03 UWG1892
			// Don't expand all if the retainRowPropertyCategories includes Expanded.
			// Added the if condition to the ExpandAll call.
			//
			if ( RowPropertyCategories.Expanded  != ( RowPropertyCategories.Expanded & this.printLayout.RetainRowPropertyCategories ) )
				this.printLayout.Rows.ExpandAll( true );            

            // MRS NAS v8.2 - CardView Printing
            if ((RowPropertyCategories.CompressedCardExpansionState != (RowPropertyCategories.CompressedCardExpansionState & this.printLayout.RetainRowPropertyCategories)) &&
                this.printLayout.AllowCardPrinting == AllowCardPrinting.RootBandOnly)
            {
                this.printLayout.Rows.ExpandAllCards();    
            }

			//RobA UWG559 10/17/01
			Cursor.Current = oldCursor;
		}

		// AS 11/9/04 NA 2005 Vol 1 UltraGridPrintDocument
		internal bool PrintDocumentHelper(UltraGridPrintDocument printDocument)
		{
			this.PrintHelper(null, printDocument, printDocument.RowProperties);

			// While the InitializePrint is supposed to be based on the Print
			// method of the grid being called, a PrintDocument doesn't really
			// have a notion of why its print was invoked - albeit the printcontroller
			// could be interrogated but there is nothing to say that it will always
			// be distinguishable. However, we still want the users to be able to initialize
			// the print information (layout, default header, etc.) for the print
			// operation so we will raise the initialize print event.
			//
			CancelablePrintEventArgs e = new CancelablePrintEventArgs( this.printManager.PrintDocument, this.printManager.DefaultLayoutInfo, this.PrintLayout );
			this.FireEvent( GridEventIds.InitializePrint, e );

			//return if cancelled
			//
			if ( e.Cancel )
				return false;

			//MRS 6/10/04 - UWG2968
			//if the user changed FitWidthToPages and AutoFitColumns is true, 
			// dirty the metrics to make sure the columns are sized to the page. 
			if ( this.PrintLayout != null &&
				this.printManager.DefaultLayoutInfo.FitWidthToPages != 0 &&
				this.PrintLayout.AutoFitAllColumns )
			{
				//dirty metrics to make sure any changes made are visible
				this.PrintLayout.GetColScrollRegions( false ).DirtyMetrics( false );
			}

			this.FormatPrintLayout();

			return true;
		}

		
		/// <summary>
		/// Initiates the creation of a preview of what a printed report of the grid data would look like.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PrintPreview</b> method initiates a print preview. Invoking this method triggers the process of preparing a printed report based on the data in the grid and displaying the results in a print preview window. This process has several steps and involves interaction between print-preview-specific objects and events within the control.</p>
		/// <p class="body">When the print preview begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job being previewed. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		public void PrintPreview()
		{
			//this.PrintPreview( false, null, null );
			this.PrintPreview( null, null );
		}

		/// <summary>
		/// Initiates the creation of a preview of what a printed report of the grid data would look like.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PrintPreview</b> method initiates a print preview. Invoking this method triggers the process of preparing a printed report based on the data in the grid and displaying the results in a print preview window. This process has several steps and involves interaction between print-preview-specific objects and events within the control.</p>
		/// <p class="body">When the print preview begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job being previewed. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		/// <param name="retainRowPropertyCategories">Sepcifies which categories of the settings on the rows to apply to rows printed on the grid.</param>
		public void PrintPreview( RowPropertyCategories retainRowPropertyCategories )
		{
			//this.PrintPreview( false, null, null );
			this.PrintPreview( null, null, retainRowPropertyCategories );
		}


		#region code commented out
		
		#endregion
		
		/// <summary>
		/// Initiates the creation of a preview of what a printed report of the grid data would look like.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PrintPreview</b> method initiates a print preview. Invoking this method triggers the process of preparing a printed report based on the data in the grid and displaying the results in a print preview window. This process has several steps and involves interaction between print-preview-specific objects and events within the control.</p>
		/// <p class="body">When you invoke this method, you specify a Layout object that controls the formatting of how the grid data will look when printed, and also while being previewed. The Layout object gives you the opportunity to create a custom-formated report based on the data in teh grid, applying different attributes such as fonts, colors and arrangement of data specifically for the printed page.</p>
		/// <p class="body">When the print preview begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job being previewed. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output. </p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		/// <param name="layout">Optional layout object</param>
		//public void PrintPreview( bool showPrintDialog, Layout layout )
		public void PrintPreview( UltraGridLayout layout )
		{
			//this.PrintPreview( showPrintDialog, layout, null );
			this.PrintPreview( layout, null );
		}

		/// <summary>
		/// Initiates the creation of a preview of what a printed report of the grid data would look like.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PrintPreview</b> method initiates a print preview. Invoking this method triggers the process of preparing a printed report based on the data in the grid and displaying the results in a print preview window. This process has several steps and involves interaction between print-preview-specific objects and events within the control.</p>
		/// <p class="body">When you invoke this method, you specify a Layout object that controls the formatting of how the grid data will look when printed, and also while being previewed. The Layout object gives you the opportunity to create a custom-formated report based on the data in teh grid, applying different attributes such as fonts, colors and arrangement of data specifically for the printed page.</p>
		/// <p class="body">When the print preview begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job being previewed. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output. </p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		/// <param name="layout">Optional layout object</param>
		/// <param name="retainRowPropertyCategories">Sepcifies which categories of the settings on the rows to apply to rows printed on the grid.</param>
		//public void PrintPreview( bool showPrintDialog, Layout layout )
		public void PrintPreview( UltraGridLayout layout, RowPropertyCategories retainRowPropertyCategories )
		{
			//this.PrintPreview( showPrintDialog, layout, null );
			this.PrintPreview( layout, null, retainRowPropertyCategories );
		}

		/// <summary>
		/// Initiates the creation of a preview of what a printed report of the grid data would look like.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PrintPreview</b> method initiates a print preview. Invoking this method triggers the process of preparing a printed report based on the data in the grid and displaying the results in a print preview window. This process has several steps and involves interaction between print-preview-specific objects and events within the control.</p>
		/// <p class="body">When you invoke this method, you specify a Layout object that controls the formatting of how the grid data will look when printed, and also while being previewed. The Layout object gives you the opportunity to create a custom-formated report based on the data in teh grid, applying different attributes such as fonts, colors and arrangement of data specifically for the printed page.</p>
		/// <p class="body">When the print preview begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job being previewed. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output. Any settings you have applied to the <b>PrintDocument</b> object specified as a paramter of the <b>PrintPreview</b> method will be passed along to the <b>PrintDocument</b> that appears in the <b>InitializePrintPreview</b> method.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		/// <param name="layout">Optional layout object</param>
		/// <param name="printDocument">Optional print document</param>
		public void PrintPreview( UltraGridLayout layout, PrintDocument printDocument )
		{
			this.PrintPreview( layout, printDocument, RowPropertyCategories.None );
		}

		/// <summary>
		/// Initiates the creation of a preview of what a printed report of the grid data would look like.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PrintPreview</b> method initiates a print preview. Invoking this method triggers the process of preparing a printed report based on the data in the grid and displaying the results in a print preview window. This process has several steps and involves interaction between print-preview-specific objects and events within the control.</p>
		/// <p class="body">When you invoke this method, you specify a Layout object that controls the formatting of how the grid data will look when printed, and also while being previewed. The Layout object gives you the opportunity to create a custom-formated report based on the data in teh grid, applying different attributes such as fonts, colors and arrangement of data specifically for the printed page.</p>
		/// <p class="body">When the print preview begins, the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/> event occurs. This event receives a <b>PrintDocument</b> object that contains information about the print job being previewed. You examine and change the properties of this object to provide feedback for the user and control the look of the printed output. Any settings you have applied to the <b>PrintDocument</b> object specified as a paramter of the <b>PrintPreview</b> method will be passed along to the <b>PrintDocument</b> that appears in the <b>InitializePrintPreview</b> method.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializePrintPreview"/>
		/// <seealso cref="System.Drawing.Printing.PrintDocument"/>
		/// <param name="layout">Optional layout object</param>
		/// <param name="printDocument">Optional print document</param>
		/// <param name="retainRowPropertyCategories">Sepcifies which categories of the settings on the rows to apply to rows printed on the grid.</param>
		public void PrintPreview ( UltraGridLayout layout, PrintDocument printDocument, RowPropertyCategories retainRowPropertyCategories )
		{

			//create a new layout based on passed in layout
			//create a printdocument based on passed in printdocument
			//create a new print manager
			//
			// SSP 1/14/03 UWG1892
			//
			//this.PrintHelper( layout, printDocument );
			this.PrintHelper( layout, printDocument, retainRowPropertyCategories );

			//Set flag so that when the BeginPrint event is called we 
			//know that we are print previewing
			//
			this.PrintManager.CalledFromPrint = false;

			try
			{
				//check for a default printer
				//
				if ( this.printManager.PrintDocument.PrinterSettings.PrinterName == "<no default printer>" )
				{
					// AS - 2/20/02
					// We should not be showing a messagebox - instead we should throw
					// an error since this is only invoked externally.
					//
					//MessageBox.Show("No Default Printer Found!", "Printer error", MessageBoxButtons.OK );
					//return;
					throw new NotSupportedException( Shared.SR.GetString("LE_NotSupportedException_292") );
				}

				//Fire InitializePrintPreview event
				//
				//RobA 10/31/01 implemented CancelablePrintPreviewEventArgs with a zoom property
				//CancelablePrintEventArgs e = new CancelablePrintEventArgs( this.printManager.PrintDocument, this.printManager.DefaultLayoutInfo );
				CancelablePrintPreviewEventArgs e = new CancelablePrintPreviewEventArgs( this.printManager.PrintDocument, this.printManager.DefaultLayoutInfo,
					this.printManager.PrintPreviewSettings,
					// SSP 12/15/03 UWG2560
					// Added PrintLayout property to InitializePrint and InitializePrintPreview event args.
					// Pass in the print layout.
					//
					this.PrintLayout
					);
				this.FireEvent( GridEventIds.InitializePrintPreview, e );

				//return if cancelled
				//
				if ( e.Cancel )
					return;

				//MRS 6/10/04 - UWG2968
				//if the user changed FitWidthToPages and AutoFitColumns is true, 
				// dirty the metrics to make sure the columns are sized to the page. 
				if ( this.PrintLayout != null &&
					e.DefaultLogicalPageLayoutInfo.FitWidthToPages != 0 
					&& this.PrintLayout.AutoFitAllColumns )
				{
					//dirty metrics to make sure any changes made are visible
					this.PrintLayout.GetColScrollRegions( false ).DirtyMetrics( false );
				}

				// RobA UWG694 11/8/01 moved this, since expanding everything might take 
				// some time, we should wait until after we fire 
				// InitializePrintPreview or InitializePrint
				//
				this.FormatPrintLayout();			
				

				

				//Show printpreview dialog
				//
				this.printManager.ShowPrintPreviewDialog();						
			}

			finally
			{
				//Dispose what we created during this process
				//
				this.DisposePrintHelper();							
			}

		}

		internal void DisposePrintHelper()
		{
			// MRS 10/18/05 - BR06790
			// Added a null check
			if (this.printManager != null)
			{
				// Unhook up print event
				//
				this.printManager.PrintDocument.PrintPage -= new PrintPageEventHandler(this.printManager.PrintPage);									
				this.printManager.PrintDocument.BeginPrint -= new PrintEventHandler( this.printManager.BeginPrint );

				// MRS 5/21/04 - UWG2915
				this.printManager.PrintDocument.EndPrint -= new PrintEventHandler( this.printManager.EndPrint );			
				this.PrintManager.DisposePrinterMetricsGraphicsHelper();
			}

			// dispose of the temp layout we created and null out the reference
			//
			if ( this.printLayout != null )
			{
				this.printLayout.Dispose();
				this.printLayout = null;
			}

			// dispose of the print manager we created and null out the reference
			//
			this.printManager = null;
		}

        //
        // MBS 7/1/08 - NA2008 V3
        // Renamed since this is internal and conflicts with the ClickCell event
        //internal void ClickCell(GridItemBase cell)
        //{
        //    this.ClickCell(cell, true);
        //}
        internal void ClickCellInternal(GridItemBase cell)
        {
            this.ClickCellInternal(cell, true);
        }

        //
        // MBS 7/1/08 - NA2008 V3
        // Renamed since this is internal and conflicts with the ClickCell event        
        //internal void ClickCell(GridItemBase cell, bool byMouse)        
        //{
        //    this.ClickCell(cell, byMouse, true);
        //}
        internal void ClickCellInternal(GridItemBase cell, bool byMouse)
        {
            this.ClickCellInternal(cell, byMouse, true);
        }

        //
        // MBS 7/1/08 - NA2008 V3
        // Renamed since this is internal and conflicts with the ClickCell event 
        //internal void ClickCell(GridItemBase cell, bool byMouse, bool allowRangeSelection)
        //{
        //    this.ClearAllSelected();

        //    cell.SetFocusAndActivate(byMouse, true);
        //}
        internal void ClickCellInternal(GridItemBase cell, bool byMouse, bool allowRangeSelection)
        {
            this.ClearAllSelected();

            cell.SetFocusAndActivate(byMouse, true);
        }

		/// <summary>
		/// Return the KeyActionMappings collection.
		/// </summary>
		internal protected override KeyActionMappingsBase GetKeyActionMappings()
		{
			return this.KeyActionMappings;
		}

		/// <summary>
		/// Returns the current state.
		/// </summary>
        /// <returns>The current state.</returns>
		internal protected override Int64 GetCurrentState()
		{
			return (Int64)this.CurrentState;
		}

        /// <summary>
        /// Performs a specific key action
        /// </summary>
        ///	<param name="actionCode">An enumeration value that determines the user action to be performed.</param>
        ///	<param name="ctlKeyDown">A boolean specifies whether the action should be performed as if the control key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is maintained, as it is when the user holds down the control key and selects a row in a grid.</param>
        ///	<param name="shiftKeyDown">A boolean specifies whether the action should be performed as if the shift key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is extended, as it is when the user holds down the shift key and selects a range of rows in a grid.</param>                
        ///	<returns>true if the action completed successfully, false if the action failed.</returns>
        internal protected override bool PerformKeyAction(Enum actionCode, bool shiftKeyDown, bool ctlKeyDown)
		{
			// SSP 8/20/02 UWG1583
			// PerformAction is modified to check if the action is allowed in default
			// key action mappings collection and not the current key action mappings
			// which the user could have modified. However for actions that are
			// performed as a result of key-strokes, we need to check the current
			// key action mappings collection. So commented out below line of
			// code and added appropriate code that checks if the action is allowed
			// in current key action mappings collection.
			//
			//return this.PerformAction( (UltraGridAction)actionCode, shiftKeyDown, ctlKeyDown );
			
			// see if action is allowed
			//DA 1.15.03 Modified to use IsActionAllowed on 
			if ( this.KeyActionMappings.IsActionAllowed( (UltraGridAction)actionCode, (long)this.CurrentState ) )
			{
				// SSP 7/30/03 UWG2544
				// Added BeforePerformAction and AfterPerformAction events.
				//
				//return this.DisplayLayout.PerformAction( (UltraGridAction)actionCode, shiftKeyDown, ctlKeyDown );
				UltraGridAction gridAction = (UltraGridAction)actionCode;
				BeforeUltraGridPerformActionEventArgs eb = new BeforeUltraGridPerformActionEventArgs( gridAction );
				this.FireEvent( GridEventIds.BeforePerformAction, eb );

				if ( eb.Cancel )
					return false;

				bool retValue = this.DisplayLayout.PerformAction( gridAction, shiftKeyDown, ctlKeyDown );

				AfterUltraGridPerformActionEventArgs ea = new AfterUltraGridPerformActionEventArgs( gridAction );
				this.FireEvent( GridEventIds.AfterPerformAction, ea );

				return retValue;
			}

			return false;
		}


		/// <summary>
		/// Select the specified item.
		/// The clearExistingSelection parameter specifies if the existing selection
		/// should be cleared.
		/// Returns true if action was canceled, false otherwise.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="clearExistingSelection"></param>
		/// <returns></returns>
		bool ISelectionManager.SelectItem( ISelectableItem item, bool clearExistingSelection )
		{
            //  BF 6/23/09	TFS18018
            //  If the item being selected is a cell, and the cell is in
            //  edit mode, it got that way via programmatic intervention,
            //  so we should return silently and not try to select it or
            //  we will end the edit mode session.
            UltraGridCell cell = item as UltraGridCell;
            if ( cell != null && cell.IsInEditMode )
                return false;

            // call InternalSelectItem passing true for select parameter
			return InternalSelectItem( item, clearExistingSelection, true );
		}

		/// <summary>
		/// Unselect the specified item.
		/// The clearExistingSelection parameter specifies if the existing selection
		/// should be cleared.
		/// Returns true if action was canceled, false otherwise.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="clearExistingSelection"></param>
		/// <returns></returns>
		bool ISelectionManager.UnselectItem( ISelectableItem item, bool clearExistingSelection )
		{
			// call InternalSelectItem passing false for select parameter
			return InternalSelectItem( item, clearExistingSelection, false );
		}
	
		/// <summary>
		/// Select or unselect the specified item, based on the select parameter.
		/// The clearExistingSelection parameter specifies if the existing selection
		/// should be cleared.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="clearExistingSelection"></param>
		/// <param name="select"></param>
		/// <returns></returns>
		internal bool InternalSelectItem( ISelectableItem item, 
			bool clearExistingSelection,
			bool select )
		{
			GridItemBase GridItem = item as GridItemBase;
			
			//RobA UWG585 11/5/01
			// SSP 4/20/05 - NAS 5.2 Filter Row
			// Use the Selectable instead of IsDisabled. Selectable checks for IsDisabled. Also
			// clear the existing selection if clearExistingSelection is true.
			//
			//if ( GridItem!= null && GridItem.IsDisabled )
			//	return true;
			if ( GridItem != null && ! GridItem.Selectable )
			{
				if ( clearExistingSelection )
					this.ClearSelectedItemsOfType( GridItem );			
				return true;
			}

			// SSP 7/8/02 UWG1328
			// If the item is already selected, then don't select it again.
			//
			if ( !clearExistingSelection && select
				&& item.IsSelected && null != this.selected && this.Selected.IsItemInCollection( item ) )
			{
				return false;
			}

			// SSP 10/20/04 UWG3713
			// We don't allow selecting items across bands. If a row, cell, or column that's being 
			// selected doesn't match the band of already selected rows, cells or columns then
			// clear the current selection.
			//
			// ------------------------------------------------------------------------------------
			if ( ! clearExistingSelection && ! this.IsItemSelectableWithCurrentSelection( item ) )
				this.Selected.ClearItemsOfType( item );
			// ------------------------------------------------------------------------------------

			Selected selected = CalcNewSelection( GridItem, clearExistingSelection, select );
			
			// SSP 10/27/03 UWG2399
			// If the selection is not going to change, then return without doing 
			// anything (more importanty without firing any events) because if the
			// selection is not going to change, then we shouldn't fire any events
			// either.
			// 
			// ------------------------------------------------------------------------------
			if ( null != selected && null != this.selected && null != item 
				&& this.selected.IsSelectionSameForItemType( item.GetType( ), selected ) )
			{
				return true;
			}
			// ------------------------------------------------------------------------------

			bool cancel = SelectNewSelection( item.GetType(), selected );

			// update display if not canceled
			if ( cancel )
				return true;
			else
			{
				//*vl 072701 - don't think we need to do this here anymore because
				//			   each item is calling InvalidateItemAllRegions in 
				//			   InternalSelect
				//this.ControlUIElement.Invalidate();

				// JJD 1/11/02
				// call update to force a paint
				//				
				// SSP 7/25/02 UWG1407
				// Added code to set the dontUpdateWhenSelecting flag and reset it
				// after calling ProcessKeyBordItem. This was done to prevent flickering
				// (when tabbing from cell to cell, cell was getting selected for a split
				// second).
				//
				if ( null == this.DisplayLayout || !this.DisplayLayout.DontUpdateWhenSelecting )
					this.Update();

				return false;
			}
		}

		internal bool SelectNewSelection( System.Type type, Selected selected )
		{
			// Exit edit mode first.  If cancelled return true
			if ( this.activeCell != null && this.activeCell.ExitEditMode() )
				return true;

			// fire the before event. If cancelled return true
			BeforeSelectChangeEventArgs eventArgs = new BeforeSelectChangeEventArgs( type, selected  );
			this.FireEvent(GridEventIds.BeforeSelectChange, eventArgs );
			if ( eventArgs.Cancel )
				return true;

			// unselect all items that were originally selected but no longer are and
			// select items that weren't
			UpdateSelectedItems( selected );

			// fire after event
			AfterSelectChangeEventArgs afterEventArgs = new AfterSelectChangeEventArgs( type );
			this.FireEvent( GridEventIds.AfterSelectChange, afterEventArgs );

			return false;
		}

		
				
		/// <summary>
		/// Calculate the new selection based on the passed-in GridItemBase.  
		/// The select and clearExistingSelection parameters specify whether
		/// we are selecting or unselecting and whether to clear the existing
		/// selection or not, respectively.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="clearExistingSelection"></param>
		/// <param name="select"></param>
		/// <returns></returns>
		internal Selected CalcNewSelection( GridItemBase item, 
			bool clearExistingSelection,
			bool select )
		{
			// get the selected items and add/or remove specified item
			Selected selected = new Selected();

			if ( ! clearExistingSelection )
			{
				// copy existing selected items
				// if its a row, don't copy cells collection (mutually exclusive)
				//if ( item.GetType() != typeof(Row) && this.Selected.Cells.Count > 0 )
				// SSP 10/4/01
				if ( !(item is UltraGridRow ) && this.Selected.Cells.Count > 0 )
					foreach ( UltraGridCell i in this.Selected.Cells )
						selected.Cells.InternalAdd( i );
				// if its a cell, don't copy rows collection (mutually exclusive)
				// SSP 4/16/05 - NAS 5.2 Filter Row
				// Added UltraGridFilterCell class.
				//
				//if ( item.GetType() != typeof(UltraGridCell) && this.Selected.Rows.Count > 0 )
				if ( ! ( item is UltraGridCell ) && this.Selected.Rows.Count > 0 )
					foreach ( UltraGridRow i in this.Selected.Rows )						
						selected.Rows.InternalAdd( i );
				if ( this.Selected.Columns.Count > 0 )
					foreach ( ColumnHeader i in this.Selected.Columns )
						selected.Columns.InternalAdd( i );
			}
			else
				// if we are clearing the existing selection, we must still check 
				// for the special case of control key being down
			{
				bool ctlKeyDown = ( Control.ModifierKeys & Keys.Control ) == Keys.Control;

				// check for the ctl key and that the item is either a Cell or a Row				
				// SSP 10/4/01
				//if ( ctlKeyDown && ( item.GetType() == typeof(Cell) || item.GetType() == typeof(Row) ) )
				// SSP 4/16/05 - NAS 5.2 Filter Row
				// Added UltraGridFilterCell class.
				//
				//if ( ctlKeyDown && ( item.GetType() == typeof(UltraGridCell) || ( item is UltraGridRow ) ) )
				if ( ctlKeyDown && ( item is UltraGridCell || item is UltraGridRow ) )
				{
					// since the columns selection does not change with the control 
					// key down, add it to the selection
					if ( this.Selected.Columns.Count > 0 )
					{
						selected.SetColumns( this.Selected.Columns );
					}
				}

				// check for the ctl key and that the item is either a ColumnHeader or a GroupHeader
				if ( ctlKeyDown && ( item.GetType() == typeof(ColumnHeader) || item.GetType() == typeof(GroupHeader) ) )
				{
					// since the cells and rows selection does not change with the control 
					// key down, add them to the selection
					if ( this.Selected.Cells.Count > 0 )
					{
						selected.SetCells( this.Selected.Cells );
					}

					if ( this.Selected.Rows.Count > 0 )
					{
						selected.SetRows( this.Selected.Rows );
					}
				}			
			}

			if ( select )
				item.AddToCollection( selected );
			else
				item.RemoveFromCollection( selected );

			// SSP 10/16/02 UWG1701
			// Make sure the number of selected items don't exceed the max.
			//
			if ( null != item && select )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.EnsureItemsWithingMaxSelectedItemsBounds( item, selected );
				UltraGrid.EnsureItemsWithingMaxSelectedItemsBounds( item, selected );
			}

			// return the newly created Selected object
			return selected;
		}

		// SSP 10/16/02 UWG1701
		// Added EnsureItemsWithingMaxSelectedItemsBounds method.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void EnsureItemsWithingMaxSelectedItemsBounds( 
		//    GridItemBase item, Infragistics.Win.UltraWinGrid.Selected newSelection )
		internal static void EnsureItemsWithingMaxSelectedItemsBounds(
			GridItemBase item, Infragistics.Win.UltraWinGrid.Selected newSelection )
		{
			if ( item is UltraGridRow )
			{
				int maxSelectedRows = item.Band.MaxSelectedRowsResolved;

				if ( maxSelectedRows > 0 && newSelection.Rows.Count > maxSelectedRows )
				{
					for ( int i = newSelection.Rows.Count - 1; i >= maxSelectedRows; i-- )
					{
						newSelection.Rows.Remove( newSelection.Rows[i] );
					}
				}
			}
			else if ( item is UltraGridCell )
			{
				int maxSelectedCells = item.Band.MaxSelectedCellsResolved;

				if ( maxSelectedCells > 0 && newSelection.Cells.Count > maxSelectedCells )
				{
					for ( int i = newSelection.Cells.Count - 1; i >= maxSelectedCells; i-- )
					{
						newSelection.Cells.Remove( newSelection.Cells[ i ] );
					}
				}
			}
		}
	
		/// <summary>
		/// Calculate the new selection based on the passed-in GridItemBase.  
		/// The select and clearExistingSelection parameters specify whether
		/// we are selecting or unselecting and whether to clear the existing
		/// selection or not, respectively.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="clearExistingSelection"></param>
		/// <param name="select"></param>
		/// <returns></returns>
		internal Selected CalcNewSelectionRange( GridItemBase item, 
			bool clearExistingSelection,
			bool select )
		{
			// have each item calculate its new range
			Selected newSelection = new Selected();

            // MBS 7/25/08 - BR33767
            // If we're going to clear the existing selection, we should also
            // reset the snaking flag.
            if (clearExistingSelection)
                this.DisplayLayout.Snaking = false;
				
			item.CalcSelectionRange( newSelection, clearExistingSelection, select, this.InitialSelection );

			// SSP 10/16/02 UWG1701
			// Call the newly added EnsureItemsWithingMaxSelectedItemsBounds method to ensure that we
			// don't exceed the max selected item settings.
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.EnsureItemsWithingMaxSelectedItemsBounds( item, newSelection );
			UltraGrid.EnsureItemsWithingMaxSelectedItemsBounds( item, newSelection );

			return newSelection;
		}
		bool ISelectionManager.ActivateItem( ISelectableItem item )
		{
			GridItemBase GridItem = item as GridItemBase;

			return GridItem.SetFocusAndActivate();
		}
		void ISelectionManager.SetPivotItem( ISelectableItem item, bool IsRangeSelect )
		{
			// get the old pivot item
			//
			ISelectableItem oldPivotItem = this.GetSelectionManager().GetPivotItem( item );

			// Set pivot item if shift key is not pressed.
			// Also set if we don't have a pivot item (even if shift is pressed).
			//
			if( IsRangeSelect				&&
				(oldPivotItem != null)	)				
				return;

			GridItemBase gridItem = item as GridItemBase;

			// set the new pivot item on the active region
			//
			if( gridItem.PivotOnColScrollRegion )
				this.ActiveColScrollRegion.SetPivotItem(gridItem);
			else
				// JJD 1/11/02
				// Only set the pivot item if we can
				if( gridItem.PivotOnRowScrollRegion )
			{
				this.ActiveRowScrollRegion.SetPivotItem(gridItem);

				// if this is a cell then also set the pivot cell on the 
				// active col scroll region. We need to do this so that
				// we can scroll the col region during a drag select
				// operation.
				//
				Infragistics.Win.UltraWinGrid.UltraGridCell cell = gridItem as Infragistics.Win.UltraWinGrid.UltraGridCell;
				if( cell != null )
					this.ActiveColScrollRegion.SetPivotCell( cell );
			}

			// JJD 1/11/02
			// Hold card area if applicable
			//
			this.cardAreaPivot = this.GetCardAreaFromItem( item ); 
		}
		ISelectableItem ISelectionManager.GetPivotItem( ISelectableItem item )
		{
			GridItemBase oldPivotItem = null;

			GridItemBase gridItem = item as GridItemBase;

			// Get the pivot item that has been cached by the the row or
			// column scrolling region
			//
			if ( gridItem.PivotOnColScrollRegion )
				oldPivotItem = this.GetActiveColScrollRegion( false ).GetPivotItem();
			else
				if ( gridItem.PivotOnRowScrollRegion )
				oldPivotItem = this.ActiveRowScrollRegion.GetPivotItem();
			else
				Debug.Fail("Trying to get an invalid pivot item type. Type: " + gridItem.GetType().ToString() );

			return oldPivotItem;
			//			return null;
		}
		bool ISelectionManager.IsMaxSelectedItemsExceeded( ISelectableItem item )
		{
			// SSP 4/16/05 - NAS 5.2 Filter Row
			// Added UltraGridFilterCell class.
			//
			//if (item.GetType() == typeof(UltraGridCell) )
			if ( item is UltraGridCell )
			{
				// SSP 1/7/04 - Accessibility
				// Check for ActiveCell being null. If that's null, then use the passed
				// in item. Passed in item should belong to the same band anyways. However
				// just in case.
				//
				// Commented out the original code and added new code below.
				//
				// ------------------------------------------------------------------------
				//int maxCells = this.DisplayLayout.ActiveCell.Band.MaxSelectedCellsResolved;
				int maxCells = 
					null != this.DisplayLayout.ActiveCell
					? this.DisplayLayout.ActiveCell.Band.MaxSelectedCellsResolved
					: ( null != item ? ((UltraGridCell)item).Band.MaxSelectedCellsResolved : 0 );
				// ------------------------------------------------------------------------


				//zero maxCells mean unlimited selection, so only check if greater
				if ( maxCells > 0 )
				{
					// SSP 10/15/02 UWG1701
					// This method is supposed to return true or false depending on whether
					// the selected items have exceeded the max selected settings or if
					// selecting the passed in item will cause them to exceed. We shouldn't be
					// modified the selected here.
					// Commented out the code below this if statement.
					// --------------------------------------------------------------------------
					if ( this.Selected.Cells.Count >= maxCells )
						return true;

					
					// --------------------------------------------------------------------------
				}
			}

				// SSP 10/4/01
				//else if (item.GetType() == typeof(Row) )
			else if ( item is UltraGridRow )
			{
				// SSP 1/7/04 - Accessibility
				// Check for ActiveCell being null. If that's null, then use the passed
				// in item. Passed in item should belong to the same band anyways. However
				// just in case.
				// Commented out the original code and added new code below.
				//
				// ------------------------------------------------------------------------
				//int maxRows = this.DisplayLayout.ActiveRow.Band.MaxSelectedRowsResolved;
				int maxRows = 
					null != this.DisplayLayout.ActiveRow 
					? this.DisplayLayout.ActiveRow.Band.MaxSelectedRowsResolved
					: ( null != item ? ((UltraGridRow)item).Band.MaxSelectedRowsResolved : 0 );
				// ------------------------------------------------------------------------


				//zero maxRows mean unlimited selection, so only check if greater
				if ( maxRows > 0 )
					//	return this.Selected.Rows.Count >= maxRows;
				{
					// SSP 10/15/02 UWG1701
					// This method is supposed to return true or false depending on whether
					// the selected items have exceeded the max selected settings or if
					// selecting the passed in item will cause them to exceed. We shouldn't be
					// modified the selected here.
					// Commented out the code below this if statement.
					// --------------------------------------------------------------------------
					if ( this.Selected.Rows.Count >= maxRows )
						return true;

					
					// --------------------------------------------------------------------------
				}
			}

			return false;
		}

		/// <summary>
		/// Save initial selection settings for specified type
		/// </summary>
		/// <param name="item"></param>
		void ISelectionManager.SetInitialSelection( ISelectableItem item )
		{
			// save initial selection settings for specified type
			((GridItemBase)item).SetInitialSelection( this.Selected, this.InitialSelection );
		}

		/// <summary>
		/// Clear initialSelection.
		/// </summary>
		void ISelectionManager.ClearInitialSelection()
		{
			this.InitialSelection.SetCells( null );
			this.InitialSelection.SetRows( null );
			this.InitialSelection.SetColumns( null );

            // MBS 7/8/08 - BR33767
            // If we're clearing the selection that we have, we should make sure
            // that we no longer enter snaking mode either, since any subsequent 
            // selection operation should be re-evaluated.
            this.DisplayLayout.Snaking = false;
		}


		/// <summary>
		/// Selecting a range based on the specified item.  The clearExistingSelection
		/// parameter specifies if the existing selection should be cleared.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="clearExistingSelection"></param>
		/// <returns></returns>
		bool ISelectionManager.SelectRange( ISelectableItem item, bool clearExistingSelection )
		{
			return InternalSelectRange( item, clearExistingSelection, true );
		}
		bool ISelectionManager.UnselectRange( ISelectableItem item, bool clearExistingSelection )
		{
			return InternalSelectRange( item, clearExistingSelection, false );
		}

		internal bool InternalSelectRange( ISelectableItem item, 
			bool clearExistingSelection,
			bool select )
		{
			GridItemBase gridItem = item as GridItemBase;

			Selected newSelection = CalcNewSelectionRange( gridItem, 
				clearExistingSelection,
				select );

			bool cancel = SelectNewSelection( item.GetType(), newSelection );

			// update display if not canceled
			if ( cancel )
				return true;
			else
			{
				//*vl 072701 - don't think we need to do this here anymore because
				//			   each item is calling InvalidateItemAllRegions in 
				//			   InternalSelect
				//this.ControlUIElement.Invalidate();

				// JJD 1/11/02
				// call update to force a paint
				//
				this.Update();

				return false;
			}
		}

		/// <summary>
		/// unselect all items that were originally selected but no longer are and
		/// select items that weren't
		/// The selected parameter specifies the new selection.
		/// </summary>
		internal void UpdateSelectedItems( Selected selected )
		{
			// SSP 12/16/02
			// Optimizations.
			// Commented below code out and added the replacement.
			//
			// --------------------------------------------------------------------------
			

            // MBS 5/13/09 - TFS17593
            // Keep a list of the items that need to be invalidated so that we can do so after updating the 
            // selected items collection.
            List<GridItemBase> itemsToInvalidate = new List<GridItemBase>();

			// SSP 12/13/02 UWG1835
			// Added code to check if the array list in the two collections are the
			// same and if they are then there is no need to set the Selected to false
			// and then back to true on the same items.
			//
			// SSP 4/9/04 - Virtual Binding Related Optimizations
			//
			//if ( this.Selected.HasCells && !this.Selected.Cells.HasSameList( selected.Cells ) ) 
			if ( this.Selected.HasCells ) 
			{
				SelectedCellsCollection oldSelectedCells = this.Selected.Cells;				
				for ( int i = 0; i < oldSelectedCells.Count; i++ )
				{
					UltraGridCell item = oldSelectedCells[i];

					// Only set it to false if we are not going to be setting it back to true below.
					// InternalSelect in addition to updating the select flag also invalidates the
					// ui element which is inefficient and not necessary since it will be reset to
					// selected state in the below loop.
					//
					// SSP 1/24/03 UWG1862
					// We don't need to check this any more since invalidating rows and cells (more 
					// specifically finding the associated ui elements ) has been optimized so we 
					// don't need to do this any more and not only that this may slow us down since 
					// Contains is an O(n) operation.
					// 
					// SSP 2/20/03
					// Calling InternalSelect below does a notifypropchange and that hinders performance.
					// So go back to checking if the row is in the new selected rows collection and if
					// so don't call InternalSelect again. However, this time do so only if the number
					// fo items in the selected rows collection is less than 1000.
					//
					//if ( !selected.HasCells || !selected.Cells.Contains( item ) )
					// SSP 4/12/04 - Virtual Binding Related
					// Reverting back to calling Contains regardless of the count since now we have
					// a fast contains operation.
					//
					//if ( !selected.HasCells || selected.Cells.Count > 1000 || !selected.Cells.Contains( item ) )					
					//   item.InternalSelect( false );
                    //
                    // MBS 4/15/09 - NA9.2 Selection Overlay
                    // Even if an item is selected, we want to invalidate the item because the borders from 
                    // a previous overlay might need to be cleared
                    //
                    //if ( !selected.HasCells || !selected.Cells.Contains( item ) )
                    //    item.InternalSelect( false );
                    //
                    // MBS 5/13/09 - TFS17593
                    // It seems that we should always invalidate this item, but only after we've updated
                    // the selection, since otherwise the RowColRegionIntersectionUIElement could be
                    // drawing the SelectionOverlay with the incorrect selection, which could show
                    // the wrong borders, etc
                    //
                    //if (selected.HasCells)
                    //    item.InternalSelect(false);
                    //else
                    //{
                    //    if (!selected.Cells.Contains(item))
                    //        item.InternalSelect(false);
                    //    else                            
                    //        itemsToInvalidate.Add(item);
                    //}
                    if (!selected.HasCells || !selected.Cells.Contains(item))
                    {
                        item.InternalSelect(false);
                        itemsToInvalidate.Add(item);
                    }
				}
			}	

			if ( selected.HasCells )
			{
                foreach (UltraGridCell cell in selected.Cells)
                {
                    cell.InternalSelect(true);

                    // MBS 5/13/09 - TFS17593
                    // It seems that we should always invalidate this item, but only after we've updated
                    // the selection, since otherwise the RowColRegionIntersectionUIElement could be
                    // drawing the SelectionOverlay with the incorrect selection, which could show
                    // the wrong borders, etc
                    itemsToInvalidate.Add(cell);
                }
			}
			
			if ( this.Selected.HasRows )
			{
				SelectedRowsCollection oldSelectedRows = this.Selected.Rows;		
				for ( int i = 0; i < oldSelectedRows.Count; i++ )
				{
					UltraGridRow item = this.Selected.Rows[i];

					// Only set it to false if we are not going to be setting it back to true below.
					// InternalSelect in addition to updating the select flag also invalidates the
					// ui element which is inefficient and not necessary since it will be reset to
					// selected state in the below loop.
					//
					// SSP 1/24/03 UWG1862
					// We don't need to check this any more since invalidating rows and cells (more 
					// specifically finding the associated ui elements ) has been optimized so we 
					// don't need to do this any more and not only that this may slow us down since 
					// Contains is an O(n) operation.
					// 
					// SSP 2/20/03
					// Calling InternalSelect below does a notifypropchange and that hinders performance.
					// So go back to checking if the row is in the new selected rows collection and if
					// so don't call InternalSelect again. However, this time do so only if the number
					// fo items in the selected rows collection is less than 1000.
					//if ( !selected.HasRows || !selected.Rows.Contains( item ) )
					// SSP 4/12/04 - Virtual Binding Related
					//
					//if ( !selected.HasRows || selected.Rows.Count > 1000 || !selected.Rows.Contains( item ) )
					//   item.InternalSelect( false );
                    //                    
                    // MBS 4/15/09 - NA9.2 Selection Overlay
                    // Even if an item is selected, we want to invalidate the item because the borders from 
                    // a previous overlay might need to be cleared
                    //
                    //if (!selected.HasRows || !selected.Rows.Contains(item))                    
                    //    item.InternalSelect(false);
                    //
                    // MBS 5/13/09 - TFS17593
                    // It seems that we should always invalidate this item, but only after we've updated
                    // the selection, since otherwise the RowColRegionIntersectionUIElement could be
                    // drawing the SelectionOverlay with the incorrect selection, which could show
                    // the wrong borders, etc
                    //
                    //if (selected.HasRows)
                    //    item.InternalSelect(false);
                    //else
                    //{
                    //    if (!selected.Rows.Contains(item))
                    //        item.InternalSelect(false);
                    //    else
                    //        item.InvalidateItemAllRegions();                            
                    //}
                    if (!selected.HasRows || !selected.Rows.Contains(item))
                    {
                        item.InternalSelect(false);
                        itemsToInvalidate.Add(item);
                    }
				}
			}

			if ( selected.HasRows )
			{
                foreach (UltraGridRow row in selected.Rows)
                {
                    row.InternalSelect(true);

                    // MBS 5/13/09 - TFS17593
                    // It seems that we should always invalidate this item, but only after we've updated
                    // the selection, since otherwise the RowColRegionIntersectionUIElement could be
                    // drawing the SelectionOverlay with the incorrect selection, which could show
                    // the wrong borders, etc
                    itemsToInvalidate.Add(row);
                }
			}			

			if ( this.Selected.HasColumns && !this.Selected.Columns.HasSameList( selected.Columns ) )
			{			
				for ( int i = 0; i < this.Selected.Columns.Count; i++ )
				{
					GridItemBase item = this.Selected.Columns[i];

					// Only set it to false if we are not going to be setting it back to true below.
					// InternalSelect in addition to updating the select flag also invalidates the
					// ui element which is inefficient and not necessary since it will be reset to
					// selected state in the below loop.
					//
					// SSP 1/24/03 UWG1862
					// We don't need to check this any more since invalidating rows and cells (more 
					// specifically finding the associated ui elements ) has been optimized so we 
					// don't need to do this any more and not only that this may slow us down since 
					// Contains is an O(n) operation.
					// 
					//if ( !selected.HasColumns || !selected.Columns.Contains( item ) )
						item.InternalSelect( false );

                    // MBS 5/13/09 - TFS17593
                    // It seems that we should always invalidate this item, but only after we've updated
                    // the selection, since otherwise the RowColRegionIntersectionUIElement could be
                    // drawing the SelectionOverlay with the incorrect selection, which could show
                    // the wrong borders, etc
                    itemsToInvalidate.Add(item);
				}
			}

			if ( selected.HasColumns )
			{
				for ( int i = 0; i < selected.Columns.Count; i++ )
				{
					GridItemBase item = selected.Columns[i];
					item.InternalSelect( true );
				}
			}
			// --------------------------------------------------------------------------
			
			// SSP 12/13/02 UWG1835
			// Why are we changing the Selected property of the grid (so that it returns a different object
			// next time it's updated) everytime someone changes the selection. This will cause problems if
			// someone is holding a reference to the Selected object.
			//
			//this.selected = selected;
			this.selected.Rows.InternalSetList( selected.Rows );
			this.selected.Cells.InternalSetList( selected.Cells );
			this.selected.Columns.InternalSetList( selected.Columns );

            // MBS 5/13/09 - TFS17593
            // Invalidate any items that were previously stored now that we've updated the selection
            for (int i = 0; i < itemsToInvalidate.Count; i++)
                itemsToInvalidate[i].InvalidateItemAllRegions();
		}

        /// <summary>
        /// Potentially translates the passed-in item to a ISelectableItem of a
        /// different type.  For instance, the grid translates a passed-in cell 
        /// into its parent row if CellClickAction is CellClickActionRowSelect.
        /// </summary>
        void ISelectionManager.TranslateItem(ref ISelectableItem item)
		{
			// SSP 4/16/05 - NAS 5.2 Filter Row
			// Added UltraGridFilterCell class.
			//
			//if ( item != null && item.GetType() == typeof(UltraGridCell) )
			if ( item is UltraGridCell )
			{
				// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
				//if ( ((UltraGridCell)(item)).Band.CellClickActionResolved == CellClickAction.RowSelect )
				if ( ((UltraGridCell)(item)).CellClickActionResolved == CellClickAction.RowSelect )
					item = ((UltraGridCell)(item)).Row;
			}
		}

		// SSP 10/4/01 UWG484
		// Changed all the typeof(Row) to is Row to allow for GroupByRow
		// selection
		//
		// Returns the grid item (from the same band as the last item) that is nearest the point
		//
		ISelectableItem ISelectionManager.ItemNearestPoint ( Point point, ISelectableItem lastItem )
		{
			if(lastItem == null)
				return null;

			// SSP 3/4/03 - Row Layout Functionality
			//
			// --------------------------------------------------------------------------------------
			bool usingRowLayoutManager = false;

			if ( lastItem is ColumnHeader )
			{
				usingRowLayoutManager = ((ColumnHeader)lastItem).Band.UseRowLayoutResolved;
			}
			else if ( lastItem is UltraGridCell )
			{
				usingRowLayoutManager = ((UltraGridCell)lastItem).Band.UseRowLayoutResolved;
			}
            // MRS 2/10/2009 - TFS13728
            else if (lastItem is GroupHeader)
            {
                usingRowLayoutManager = ((GroupHeader)lastItem).Band.UseRowLayoutResolved;
            }

			if ( usingRowLayoutManager )
			{
				UIElement mainElem = this.DisplayLayout.UIElement;

				RowColRegionIntersectionUIElement rowColRegion = 
					null != mainElem ? 
					(RowColRegionIntersectionUIElement)mainElem.GetDescendant( typeof( RowColRegionIntersectionUIElement ), 
					new object[] { this.ActiveColScrollRegion, this.ActiveRowScrollRegion } )
					: null;

				Debug.Assert( null != rowColRegion, "No row col intersection element !" );

				if ( null != rowColRegion )
				{
					if ( lastItem is ColumnHeader )
					{
						return LayoutUtility.FindNearesetColumnHeader( rowColRegion, ((ColumnHeader)lastItem).Band, point );
					}
					else if ( lastItem is UltraGridCell )
					{
						return LayoutUtility.FindNearesetCell( rowColRegion, ((UltraGridCell)lastItem).Band, point );
					}
                    // MRS 2/10/2009 - TFS13728
                    else if (lastItem is GroupHeader)
                    {
                        return LayoutUtility.FindNearesetGroupHeader(rowColRegion, ((GroupHeader)lastItem).Band, point);
                    }
				}

				return null;
			}
			// --------------------------------------------------------------------------------------

			// JJD 1/11/02
			// If the item is a row or cell for a cardview band then
			// call GetItemNearestPoint off the cardelement
			//
			CardAreaUIElement cardArea = GetCardAreaFromItem( lastItem );

			if ( cardArea != null )
				return cardArea.GetItemNearestPoint( point, lastItem );

			Type itemType = lastItem.GetType();

			// SSP 5/5/05 - NAS 5.2
			// Assign null instead of assigning dummy visible row or visible header that has no underlying row or header.
			//
			//Infragistics.Win.UltraWinGrid.VisibleRow nearestRow = new Infragistics.Win.UltraWinGrid.VisibleRow( this.ActiveRowScrollRegion );
			//Infragistics.Win.UltraWinGrid.VisibleHeader nearestHeader = new Infragistics.Win.UltraWinGrid.VisibleHeader( this.ActiveColScrollRegion );
			Infragistics.Win.UltraWinGrid.VisibleRow nearestRow = null;
			Infragistics.Win.UltraWinGrid.VisibleHeader nearestHeader = null;
			
			bool draggingRight = false;
			bool draggingDown = false;

			// SSP 10/4/01
			//if ( itemType == typeof(Row) || itemType == typeof(Cell) )
			// MD 8/3/07 - 7.3 Performance
			// IsAssignableFrom is faster than IsSubclassOf
			//if ( itemType == typeof(UltraGridRow) || itemType.IsSubclassOf( typeof( UltraGridRow ) ) 
			if ( itemType == typeof( UltraGridRow ) || typeof( UltraGridRow ).IsAssignableFrom( itemType ) 
				// SSP 4/16/05 - NAS 5.2 Filter Row
				// Added UltraGridFilterCell class.
				//
				//|| itemType == typeof(UltraGridCell) 
				|| GridUtils.IsObjectOfType( itemType, typeof( UltraGridCell ) ) )
			{
				if ( this.ActiveRowScrollRegion == null )
					return lastItem;

				nearestRow = this.ActiveRowScrollRegion.GetNearestSameBandRow( point, (GridItemBase)lastItem );	//, fDraggingDown );

				if ( nearestRow == null ) 
					Debug.WriteLine ( "nearestRow is null" );
			}

			// SSP 10/4/01
			//if ( itemType != typeof(Row) )
			// MD 8/3/07 - 7.3 Performance
			// IsAssignableFrom is faster than IsSubclassOf
			//if ( itemType != typeof(UltraGridRow) && !itemType.IsSubclassOf( typeof( UltraGridRow ) ) )
			if ( itemType != typeof( UltraGridRow ) && !typeof( UltraGridRow ).IsAssignableFrom( itemType ) )
			{
				if ( this.ActiveColScrollRegion == null )
					return lastItem;

				nearestHeader = this.ActiveColScrollRegion.GetNearestSameBandHeader( point, (GridItemBase)lastItem );	//, fDraggingRight );
			}
			//			if( nearestHeader != null && nearestHeader.Header != null )
			//				Debug.WriteLine( "Header caption = " + nearestHeader.Header.Caption );

			// SSP 10/4/01
			//if ( itemType == typeof(Row) )
			// MD 8/3/07 - 7.3 Performance
			// IsAssignableFrom is faster than IsSubclassOf
			//if ( itemType == typeof(UltraGridRow) || itemType.IsSubclassOf( typeof( UltraGridRow ) ) )
			if ( itemType == typeof( UltraGridRow ) || typeof( UltraGridRow ).IsAssignableFrom( itemType ) )
			{
				return nearestRow != null ?	nearestRow.Row : lastItem;
			}

			else if ( itemType == typeof ( GroupHeader ))
			{
				return nearestHeader != null ?	nearestHeader.Header : lastItem;
			}

			else if ( itemType == typeof ( ColumnHeader ))
			{
				if ( nearestHeader != null )
				{
					// Check to see if the point is over a column in this band
					// if so return it
					//
					GridItemBase itemAtPoint = HeaderItemFromPoint( point,
						this.ActiveRowScrollRegion,
						this.ActiveColScrollRegion );

					if ( itemAtPoint != null  && itemAtPoint.Band == ((GridItemBase)lastItem).Band &&
						itemAtPoint.GetType() == typeof ( ColumnHeader ) )
					{
						// SSP 7/10/03 - Fixed headers
						// Commented out below code and added new code for fixed headers.
						//
						// ----------------------------------------------------------------------------
						// return ( ColumnHeader )itemAtPoint;
						ColScrollRegion csr = this.ActiveColScrollRegion;
						if ( csr.IsPivotItemFixedHeaderItem( ) 
							&& ! itemAtPoint.Band.IsHeaderFixed( csr, (HeaderBase)itemAtPoint ) 
							&& itemAtPoint.Band.AreNonFixedHeadersScrolledUnderneathFixedHeaders( csr )
							&& ! csr.fixedToNonFixedAreaBoundaryCrossed )
						{
						}
						else if ( itemAtPoint.Band.IsHeaderFixed( csr, (HeaderBase)itemAtPoint )
							&& ! csr.IsPivotItemFixedHeaderItem( )
							&& itemAtPoint.Band.AreNonFixedHeadersScrolledUnderneathFixedHeaders( csr ) )
						{
						}
						else
						{
							return ( ColumnHeader )itemAtPoint;
						}
						// ----------------------------------------------------------------------------
					}

					Infragistics.Win.UltraWinGrid.HeaderBase header = nearestHeader.Header;

					//The first header will have an offset of the PreRowAreaExtent, so if we subtract 
					//it from the header origin and get zero then we have the first header. Since the 
					//first column's RelativeOrigin doesn't include PreRowAreaExtent, but all of the 
					//following ones do -- make sure the first header's origin is zero.
					int origin = (nearestHeader.Origin - nearestHeader.Header.Band.PreRowAreaExtent) == 0 ? 0 : nearestHeader.Origin;
					int nOffsetX = point.X - ( origin - this.ActiveColScrollRegion.Position );

					// SSP 7/14/03 - Fixed headers
					// If the header is fixed, then take into account the fact that fixed headers don't scroll.
					// 
					// --------------------------------------------------------------------------------------
					if ( nearestHeader.Header.Band.IsHeaderFixed( this.ActiveColScrollRegion, nearestHeader.Header ) )
						nOffsetX -= nearestHeader.Header.Band.GetFixedHeaders_OriginDelta( this.ActiveColScrollRegion );
					// --------------------------------------------------------------------------------------

					int nOffsetY = 1;
					

					// Try to get the header uielement of the last grid item to calculate the y 
					// offset. We only need to do this if the last item was a column that was
					// part of a group on a band that had more than one level. 
					// Otherwise, we can just ignore the vertical offset.
					//
					// MD 1/21/09 - Groups in RowLayouts
					// Use LevelCountResolved because it checks the band's row layout style
					//if ( ((GridItemBase)lastItem).Band.LevelCount > 1 && 
					if ( ( (GridItemBase)lastItem ).Band.LevelCountResolved > 1 && 
						lastItem.GetType() != typeof ( GroupHeader ) &&
						// SSP 3/4/04
						// Changed the behavior to give row-layout higher priority than the groups.
						// Meaning if there are groups and they turn set UseRowLayout to true, we
						// will ignore groups and use the row-layout functionality. So instead of
						// using HasGroups, use GroupsDisplayed because that's the final say in
						// whether groups are being displayed or not.
						//
						//((GridItemBase)lastItem).Band.HasGroups )
						((GridItemBase)lastItem).Band.GroupsDisplayed )
					{
						UIElement element = this.DisplayLayout.GetUIElement( false );

						if ( element != null )
						{

							//RobA 10/30/01 call GetDescendant
							element = element.GetDescendant( typeof( Infragistics.Win.UltraWinGrid.HeaderUIElement),
								new object[]{this.ActiveColScrollRegion, this.ActiveRowScrollRegion,
												lastItem} );
							
							if ( element != null && element.Parent != null )
							{
								element = element.Parent;
								nOffsetY = point.Y - ( ((GridItemBase)lastItem).Band.GetTotalHeaderHeight() + element.Rect.Top );
							}
						}
					}

					UltraGridColumn column = null;

					if ( header.IsGroup )
						column = header.Group.GetColumnAtOffset( null, nOffsetX, nOffsetY,
							draggingRight, draggingDown );
					else
						column = header.Column;

					if ( column != null )
					{
						return column.Header;
					}
				}

			}
			// SSP 4/16/05 - NAS 5.2 Filter Row
			// Added UltraGridFilterCell class.
			//
			//else if ( itemType == typeof(UltraGridCell) )
			else if ( GridUtils.IsObjectOfType( itemType, typeof(UltraGridCell) ) )
			{
				if ( nearestHeader != null && nearestRow != null )
				{
					UltraGridRow row = nearestRow.Row;
					HeaderBase header = nearestHeader.Header;
                
					Debug.Assert ( header.Band == nearestRow.Band, "Different bands in header and row" );

					// JJD 1/15/02
					// Subtract the scroll region's position in case it has scrolled
					//
					int nOffsetX = point.X - ( nearestHeader.Origin
						+ this.ActiveColScrollRegion.Origin - this.ActiveColScrollRegion.Position );

					// SSP 7/14/03 - Fixed headers
					// If the header is fixed, then take into account the fact that fixed headers don't scroll.
					// 
					// --------------------------------------------------------------------------------------
					if ( nearestHeader.Header.Band.IsHeaderFixed( this.ActiveColScrollRegion, nearestHeader.Header ) )
						nOffsetX -= nearestHeader.Header.Band.GetFixedHeaders_OriginDelta( this.ActiveColScrollRegion );
					// --------------------------------------------------------------------------------------

					// JJD 1/15/02
					// Use top of row in client to calculate the y offset
					//
					int nOffsetY = point.Y - nearestRow.GetTopOfRowInClient();

					UltraGridColumn column = null;

					if(header.IsGroup)
						column = header.Group.GetColumnAtOffset( row, nOffsetX, nOffsetY,
							draggingRight, draggingDown );
					else
						column = header.Column;


					if ( column != null )
					{
						UltraGridCell cell = null;

						cell = nearestRow.Row.Cells[ column ];

						if ( cell != null )
						{
							//string tf = pCell.Selected ? "true" : "false";
							
							//Debug.WriteLine("Text " + pCell.Text + " " + tf);
							//Debug.WriteLine("Row = " + nearestRow.Row.Index.ToString() );
							return cell;
						}
						else Debug.WriteLine("Cell = null");
					}
					else Debug.WriteLine("Column = null");

				}
			}

			//Debug.WriteLine("Item is " + itemType.FullName );
			return lastItem;
		}

		private bool ContinueScrolling( ref Point mouseLoc )
		{
			// AS 8/31/01
			// The framework passes in the mouse buttons as logical buttons, not
			// physical ones so there is no need to swap the buttons.
			//
			// check to see if mouse buttons have been swapped
			//MouseButtons buttons = SystemInformation.MouseButtonsSwapped ?  MouseButtons.Right : MouseButtons.Left;
			MouseButtons buttons = MouseButtons.Left;
			
			// check if the left button is not down
			//
			if ( ( Control.MouseButtons & buttons ) != buttons )
			{
				return false;
			}

			mouseLoc = this.DisplayLayout.Grid.PointToClient ( System.Windows.Forms.Cursor.Position );
			return true;
		}

		void ISelectionManager.DoDragScrollVertical( int timerInterval )
		{
			Point mouseLoc = new Point(0,0);

			if (this.ContinueScrolling( ref mouseLoc ))
			{
				// JJD 1/11/02
				// If we are scrolling within a card area we never do any vertical
				// scrolling
				//
				if ( this.cardAreaPivot != null && !this.cardAreaPivot.Disposed )
				{
					// SSP 10/28/05 BR06561
					// Implemented scrolling vertically in card-view when dragging and dropping columns.
					// 
					this.cardAreaPivot.ScrollCardVertically( mouseLoc );

					return;
				}

				// if the region needs scrolling, have it actually do the scrolling
				//if( this.ActiveRowScrollRegion.DoesDragNeedScroll( mouseLoc, ref timerInterval ))
				this.ActiveRowScrollRegion.DoDragScroll ( mouseLoc, timerInterval );
			}
		}

		void ISelectionManager.DoDragScrollHorizontal( int timerInterval )
		{
			Point mouseLoc = new Point(0,0);

			if (this.ContinueScrolling( ref mouseLoc ))
			{
				// JJD 1/11/02
				// If we are scrolling within a card area then call the scroll
				// methods on it
				//
				if ( this.cardAreaPivot != null && !this.cardAreaPivot.Disposed )
				{
					// call update to force a paint
					//
					this.Update();

					if ( mouseLoc.X > this.cardAreaPivot.Rect.Left + ( this.cardAreaPivot.Rect.Width / 2 ) )
						this.cardAreaPivot.ScrollCardArea( 1 );
					else
						this.cardAreaPivot.ScrollCardArea( -1 );

					// call update to force a paint
					//
					this.Update();

					return;
				}

				// if the region needs scrolling, have it actually do the scrolling
				//if ( this.ActiveColScrollRegion.DoesDragNeedScroll( mouseLoc, ref timerInterval ))
				this.ActiveColScrollRegion.DoDragScroll( mouseLoc, timerInterval );
			}
		}

		bool ISelectionManager.DoesDragNeedScrollVertical( Point mouseLoc, ref int timerInterval )
		{
			// JJD 1/11/02
			// If we are dragging a card area then we never want to do vertical scrolling
			//
			if ( this.cardAreaPivot != null && !this.cardAreaPivot.Disposed )
			{
				// SSP 10/28/05 BR06561
				// Implemented scrolling vertically in card-view when dragging and dropping columns.
				// 
				//return false;
				return this.cardAreaPivot.DoesDragNeedScrollVertical( mouseLoc, ref timerInterval );
			}

			// SSP 9/27/01 UWG341
			// If a column is being dragged, we do not need to scroll vertically
			// 

			if ( this.isDragging && null != this.dragEffect &&
				null != this.dragEffect.DragStrategy && 
				this.dragEffect.DragStrategy is DragStrategyColumn )
				return false;

			return this.ActiveRowScrollRegion.DoesDragNeedScroll( mouseLoc, ref timerInterval );
		}

		bool ISelectionManager.DoesDragNeedScrollHorizontal( Point mouseLoc, ref int timerInterval )
		{
			// JJD 1/11/02
			// If we are dragging a card area then check against the card area rect 
			//
			if ( this.cardAreaPivot != null && !this.cardAreaPivot.Disposed )
			{
				// SSP 10/28/05 BR06561
				// Implemented scrolling vertically in card-view when dragging and dropping columns.
				// 
				//return this.cardAreaPivot.DoesDragNeedScroll( mouseLoc, ref timerInterval );
				return this.cardAreaPivot.DoesDragNeedScrollHorizontal( mouseLoc, ref timerInterval );
			}

			return this.ActiveColScrollRegion.DoesDragNeedScroll( mouseLoc, ref timerInterval );
		}

		// the grid doesn't need to do anything in OnMouseUp
		void ISelectionManager.OnMouseUp( ref MouseMessageInfo msginfo )
		{	
		}

		// Returns a CGridItem that the point is over; otherwise, null of there is 
		// no CGridItem under the specified point
		//
		GridItemBase HeaderItemFromPoint ( Point point, RowScrollRegion rowScrollRegion,
			ColScrollRegion colScrollRegion )
		{
			GridItemBase item = null;

			// SSP 2/6/02 UWG1024
			// Look below.
			UIElement mainElement = this.DisplayLayout.GetUIElement( false );

			// Use the new UIElements to get the grid item from point
			//
			// SSP 2/6/02 UWG1024
			// Don't verify the elements when getting the grid's main element.
			// This will lead to things like visible headers being regenerated
			// if they were marked dirty, causing the calling method 
			// (ISelectionManager.ItemNearestPoint) which may be holding a reference
			// to a VisibleHeader to not work properly because the contents (like Header )
			// of that visible header would be changed in the process of regenerating
			// the visible headers. So pass in false to GetUIElement.
			//
			//if ( this.DisplayLayout.GetUIElement( true ) != null )
			if ( null != mainElement )
			{
				// Since ElementFromPoint will generally return a TextUIElement, walk up the 
				// chain with GetAncestor() to see if it is of type HeaderUIElement
				//
				// SSP 2/6/02 UWG1024
				//
				//Infragistics.Win.UIElement pElement = this.DisplayLayout.GetUIElement( false ).ElementFromPoint( point );
				// SSP 6/20/02 UWG1197
				// Type. As you can see above, we were calling ElementFromPoint on the element returned by GetUIElement.
				// Below we are not calling that method. So I made the necessary change to call ElementFromPoint
				// and get the element under it.
				//
				//Infragistics.Win.UIElement pElement = mainElement;
				Infragistics.Win.UIElement pElement = mainElement.ElementFromPoint( point );
					
				if ( pElement != null )
					pElement = pElement.GetAncestor( typeof(HeaderUIElement) );
        
				if ( pElement != null )
				{
					Infragistics.Win.UIElement tmpElement = 
						pElement.GetAncestor( typeof(RowColRegionIntersectionUIElement) );

					Infragistics.Win.UltraWinGrid.RowColRegionIntersectionUIElement rowColElement = 
						(RowColRegionIntersectionUIElement)tmpElement;

					// If a row or col scroll region was passed in make sure the element is contained in it
					//
					if  ( rowColElement != null && 
						( rowScrollRegion == null || rowColElement.RowScrollRegion == rowScrollRegion ) &&
						( colScrollRegion == null || rowColElement.ColScrollRegion == colScrollRegion ) )
						item  = ((HeaderUIElement)pElement).Header;
				}
			}

			return item;
		}

		// AS - 12/13/01
		// Added a default size since we were originally only showing the a small
		// portion of the caption. We can change this at some point but for now
		// I default to the same size used by the inbox grid.
		//

		/// <summary>
		/// Returns the default size for the control.
		/// </summary>
		protected override Size DefaultSize
		{
			get 
			{ 
				// SSP 5/19/04 UWG3265
				// With the new designer showing a Start button and a line of
				// instruction beside it, we need to make the default size big enough
				// so that the Start button and the label is fully shown.
				//
				//return new Size(130, 80); 
				// MRS 8/10/04 - Made this even bigger to account for localization
				//return new Size( 384, 80 );
				return new Size( 550, 80 );

				
			}
		}


		// SSP 3/1/02 UWG168
		// Added method for forwarding Click and DoubleClick messages
		// to the grid from the text box.
		//
		internal void OnClickForwarded( EventArgs e )
		{
			this.OnClick( e );
		}

		internal void OnDoubleClickForwarded( EventArgs e )
		{
			this.OnDoubleClick( e );
		}

		#region RowCancelUpdateAction

		// SSP 1/14/03 UWG1928
		// Added RowCancelUpdateAction property.
		//

		/// <summary>
		/// Specifies the action that should be taken when <see cref="UltraGrid.BeforeRowUpdate"/> event is cancelled. Default value is <b>CancelUpdate</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <see cref="UltraGrid.BeforeRowUpdate"/> event is fired when UltraGrid is about to commit 
		/// changes made by the user to a row. This event allows you to validate the input. If
		/// validation fails you can cancel the <b>BeforeRowUpdate</b> event which by default causes the
		/// UltraGrid to revert back to the old cell values. You may however not want to revert back
		/// to the old cell values. In which case set the <b>RowUpdateCancelAction</b> property to
		/// <b>RetainDataAndActivation</b>.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.RowUpdateCancelAction"/>
		/// <seealso cref="UltraGrid.BeforeRowUpdate"/>
		/// </remarks>
        [LocalizedDescription("LD_UltraGrid_P_RowUpdateCancelAction")]
        [LocalizedCategory("LC_Behavior")]
		public Infragistics.Win.UltraWinGrid.RowUpdateCancelAction RowUpdateCancelAction
		{
			get
			{
				return this.rowUpdateCancelAction;
			}
			set
			{
                // MRS NAS v8.2 - Unit Testing
                //this.exitEditModeOnLeave = value;
                if (this.rowUpdateCancelAction == value)
                    return;

                if (!Enum.IsDefined(typeof(RowUpdateCancelAction), value))
                    throw new InvalidEnumArgumentException("RowUpdateCancelAction", (int)value, typeof(RowUpdateCancelAction));

                this.rowUpdateCancelAction = value;

                this.NotifyPropChange(PropertyIds.RowUpdateCancelAction);				
			}
		}

		#endregion // RowCancelUpdateAction

		// SSP 1/24/03 UWG1956
		// Added ShouldSerializeRowUpdateCancelAction and ResetRowUpdateCancelAction methods.
		//
		#region ShouldSerializeRowUpdateCancelAction

		/// <summary>
		/// Returns true if the RowUpdateCancelAction property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowUpdateCancelAction( )
		{
			return RowUpdateCancelAction.CancelUpdate != this.rowUpdateCancelAction;
		}

		#endregion // ShouldSerializeRowUpdateCancelAction

		#region ResetRowUpdateCancelAction

		/// <summary>
		/// Resets the RowUpdateCancelAction property to its default value of <b>CancelUpdate</b>.
		/// </summary>
		public void ResetRowUpdateCancelAction( )
		{
			this.RowUpdateCancelAction = RowUpdateCancelAction.CancelUpdate;
		}

		#endregion // ResetRowUpdateCancelAction

		#region Fix for UWG1892

		#region CopyPropertiesToPrintRow

		// SSP 1/14/03 UWG1892
		// Added retainRowPropertyCategories parameter to Print and PrintPreview methods for
		// making WYSIWIG style printing. 
		//
		/// <summary>
		/// If the printRow belongs to a print layout, gets the corresponding grid row and
		/// copies the property categories specified by the print layout's RetainRowPropertyCategories
		/// property value.
		/// </summary>
		/// <param name="printRow"></param>
		private void CopyPropertiesToPrintRow( UltraGridRow printRow )
		{			
			UltraGridLayout layout = printRow.Layout;

			// SSP 5/7/03 - Export Functionality
			//
			//if ( null != layout && layout.IsPrintLayout && 
			if ( null != layout && ( layout.IsPrintLayout || layout.IsExportLayout ) 
				// SSP 1/18/05 BR01753
				// We are copying formula calculations regardless of the property categories
				// so don't check for the property categories being None here.
				//
				//&& RowPropertyCategories.None != layout.RetainRowPropertyCategories 
				)
			{
                // MRS v7.2 - PDF Report Writer
                //UltraGridRow gridRow = this.GetRowFromPrintRow( printRow );
                //
                UltraGridRow gridRow = this.GetRowFromPrintRow( printRow, false );
				
				if ( null != gridRow )
					printRow.CopyRowPropertiesFrom( gridRow, layout.RetainRowPropertyCategories );

			}
		}

		#endregion CopyPropertiesToPrintRow

		#region GetRowFromPrintRow

		// SSP 1/14/03 UWG1892
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private UltraGridRow FindAncestorGroupByRow( UltraGridColumn column, UltraGridRow row )
		private static UltraGridRow FindAncestorGroupByRow( UltraGridColumn column, UltraGridRow row )
		{
			while ( null != row )
			{
				if ( row is UltraGridGroupByRow )
				{
					UltraGridGroupByRow groupByRow = (UltraGridGroupByRow)row;

					// Use the caninitializefrom to check if the columns match which ends up doing
					// key comparision but it also has logic for non-keyed unbound column.
					//
					if ( column.CanInitializeFrom( groupByRow.Column ) )
						return groupByRow;
				}
				
				row = row.ParentRow;
			}

			return null;
		}

        // MRS v7.2 - PDF Report Writer
        // Overload for backward compatibility
        //
        /// <summary>
        /// Gets the row in the grid corresponding to the passed in row from a print layout when printing.
        /// </summary>
        /// <param name="printRow">A row from a print layout.</param>
        /// <returns>Corresponding row or null if none found.</returns>
        /// <remarks>
        /// <p class="body">
        /// When printing, print-previewing or excel-exporting, the UltraGrid makes clone of the 
        /// <see cref="UltraGridBase.DisplayLayout"/> to make a print layout. 
        /// The print layout has it's own set of rows. This method is used to get the 
        /// row from the display layout that corresponds to a row from the print layout. This
        /// is useful, for example, in event handlers of printing, print-previewing or excel-exporting
        /// related events where you have access to a print row however you would like to
        /// get the corresponding row from the display layout.
        /// </p>
        /// </remarks>
        public UltraGridRow GetRowFromPrintRow( UltraGridRow printRow )        
        {
            return this.GetRowFromPrintRow(printRow, true);
        }

		// SSP 1/14/03 UWG1892
		// Added GetRowFromPrintRow method
		//
		/// <summary>
		/// Gets the row in the grid corresponding to the passed in row from a print layout when printing.
		/// </summary>
		/// <param name="printRow">A row from a print layout.</param>
        /// <param name="create">Specifies true to indicate that if the row is not already created, it should be created now. Specify false to indicate that if the row is not already created, the creation of the row should not be forced.</param>
		/// <returns>Corresponding row or null if none found.</returns>
		/// <remarks>
		/// <p class="body">
		/// When printing, print-previewing or excel-exporting, the UltraGrid makes clone of the 
		/// <see cref="UltraGridBase.DisplayLayout"/> to make a print layout. 
		/// The print layout has it's own set of rows. This method is used to get the 
		/// row from the display layout that corresponds to a row from the print layout. This
		/// is useful, for example, in event handlers of printing, print-previewing or excel-exporting
		/// related events where you have access to a print row however you would like to
		/// get the corresponding row from the display layout.
		/// </p>
		/// </remarks>
		// MRS v7.2 - PDF Report Writer
        //public UltraGridRow GetRowFromPrintRow( UltraGridRow printRow )
        public UltraGridRow GetRowFromPrintRow(UltraGridRow printRow, bool create)
		{
            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
            #region Old Code
            //if ( null == printRow )
            //    return null;

            //if ( printRow is UltraGridGroupByRow )
            //{
            //    UltraGridGroupByRow printGroupByRow = (UltraGridGroupByRow)printRow;

            //    // MRS v7.2 - PDF Report Writer
            //    //UltraGridRow tmpRow = this.GetRowFromPrintRow( printGroupByRow.FindFirstRegularRow( ) );
            //    UltraGridRow tmpRow = this.GetRowFromPrintRow(printGroupByRow.FindFirstRegularRow(), create);
                
            //    // MD 7/26/07 - 7.3 Performance
            //    // FxCop - Mark members as static
            //    //return this.FindAncestorGroupByRow( printGroupByRow.Column, tmpRow );
            //    return UltraGrid.FindAncestorGroupByRow( printGroupByRow.Column, tmpRow );
            //}
            //else
            //{
            //    UltraGridBand band = printRow.Band;

            //    if ( null == band.ParentBand )
            //    {
            //        // SSP 5/18/05 BR04116
            //        // Use the new GetRowFromPrintRow method instead.
            //        //
            //        //return this.Rows.GetRowWithListIndex( printRow.ListIndex );
            //        // MRS v7.2 - PDF Report Writer
            //        //return this.Rows.GetRowFromPrintRow( printRow );
            //        //
            //        return this.Rows.GetRowFromPrintRow(printRow, create);
            //    }
            //    else
            //    {
            //        UltraGridRow correspondingParentBandRow = this.GetRowFromPrintRow( printRow.FindActualAncestorRow );

            //        Debug.Assert( null != correspondingParentBandRow );
            //        if ( null == correspondingParentBandRow )
            //            return null;

            //        // MRS v7.2 - PDF Report Writer
            //        // Get the printRowGridBand and store it. 
            //        //
            //        // CDS 04/07/08 BR30140
            //        // Duplicate keys can exist within the DisplayLayout, so we need to match based on Key and HierarchicalLevel
            //        // which is done in GetBandFromPrintBand
            //        //UltraGridBand printRowGridBand = this.DisplayLayout.Bands.Exists(printRow.Band.Key)
            //        //    ? this.DisplayLayout.Bands[printRow.Band.Key]
            //        //    : null;  
            //        UltraGridBand printRowGridBand = this.GetBandFromPrintBand(printRow.Band);

            //        // This could happen if the band structure has changed, in which case return null.
            //        //
            //        // SSP 3/12/04 UWG2995
            //        // Check for ChildBands being null.
            //        //
            //        // MRS v7.2 - PDF Report Writer
            //        //if (null != correspondingParentBandRow.ChildBands)
            //        //
            //        if (printRowGridBand != null &&
            //            (create || correspondingParentBandRow.HaveChildRowsBeenCreated(printRowGridBand)) &&
            //            null != correspondingParentBandRow.ChildBands )
            //        {
            //            // MRS v7.2 - PDF Report Writer
            //            //if ( ! correspondingParentBandRow.ChildBands.Exists( printRow.Band.Key ) )
            //            //
            //            if (!correspondingParentBandRow.ChildBands.Exists(printRowGridBand))
            //                return null;

            //            // SSP 5/18/05 BR04116
            //            // Use the new GetRowFromPrintRow method instead.
            //            //
            //            //return correspondingParentBandRow.ChildBands[ printRow.Band.Key ].Rows.GetRowWithListIndex( printRow.ListIndex );
            //            // MRS v7.2 - PDF Report Writer
            //            //return correspondingParentBandRow.ChildBands[ printRow.Band.Key ].Rows.GetRowFromPrintRow( printRow );
            //            //
            //            return correspondingParentBandRow.ChildBands[printRowGridBand].Rows.GetRowFromPrintRow(printRow, create);

            //        }

            //        return null;
            //    }
            //}
            #endregion //Old Code
            //
            return this.GetCorrespondingRow(printRow, this.DisplayLayout, create);
        }

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        /// <summary>
        /// Gets the export or print row corresponding to the passed in row from the on-screen grid.
        /// </summary>
        /// <param name="gridRow">A row from a grid layout.</param>
        /// <param name="layout">The print or export layout from which to get a row.</param>
        /// <param name="create">Specifies true to indicate that if the row is not already created, it should be created now. Specify false to indicate that if the row is not already created, the creation of the row should not be forced.</param>
        /// <returns>Corresponding row or null if none found.</returns>
        /// <remarks>
        /// <p class="body">
        /// When printing, print-previewing or excel-exporting, the UltraGrid makes clone of the 
        /// <see cref="UltraGridBase.DisplayLayout"/> to make a print layout. 
        /// The print layout has it's own set of rows. This method is used to get the 
        /// print layout row that corresponds to a row from the real on-screen grid. This
        /// </p>
        /// </remarks>
        /// <seealso cref="GetRowFromPrintRow(UltraGridRow, bool)"/>
        public UltraGridRow GetPrintRowFromRow(UltraGridRow gridRow, UltraGridLayout layout, bool create)
        {
            return this.GetCorrespondingRow(gridRow, layout, create);
        }

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        /// <summary>
        /// Gets the rows collection in the grid corresponding to the passed in rows collection from a print or export layout.
        /// </summary>
        /// <param name="printRows">A rows collection from a print or export layout layout.</param>
        /// <param name="create">Specifies true to indicate that if the row is not already created, it should be created now. Specify false to indicate that if the row is not already created, the creation of the row should not be forced.</param>
        /// <returns>Corresponding rows or null if none found.</returns>
        /// <remarks>
        /// <p class="body">
        /// When printing, print-previewing or excel-exporting, the UltraGrid makes clone of the 
        /// <see cref="UltraGridBase.DisplayLayout"/> to make a print layout. 
        /// The print layout has it's own set of rows. This method is used to get the 
        /// row from the display layout that corresponds to a row from the print layout. This
        /// is useful, for example, in event handlers of printing, print-previewing or excel-exporting
        /// related events where you have access to a print row however you would like to
        /// get the corresponding row from the display layout.
        /// </p>
        /// </remarks>                
        public RowsCollection GetRowsFromPrintRows(RowsCollection printRows, bool create)
        {
            return this.GetCorrespondingRows(printRows, this.DisplayLayout, create);
        }

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        /// <summary>
        /// Gets the export or print rows corresponding to the passed in rows from the on-screen grid.
        /// </summary>
        /// <param name="gridRows">A rows collection from a grid layout.</param>
        /// <param name="layout">The print or export layout from which to get a rows collection.</param>
        /// <param name="create">Specifies true to indicate that if the row is not already created, it should be created now. Specify false to indicate that if the row is not already created, the creation of the row should not be forced.</param>
        /// <returns>Corresponding row or null if none found.</returns>
        /// <remarks>
        /// <p class="body">
        /// When printing, print-previewing or excel-exporting, the UltraGrid makes clone of the 
        /// <see cref="UltraGridBase.DisplayLayout"/> to make a print layout. 
        /// The print layout has it's own set of rows. This method is used to get the 
        /// print layout row that corresponds to a row from the real on-screen grid. This
        /// </p>
        /// </remarks>
        /// <seealso cref="GetRowsFromPrintRows(RowsCollection, bool)"/>
        public RowsCollection GetPrintRowsFromRows(RowsCollection gridRows, UltraGridLayout layout, bool create)
        {
            return this.GetCorrespondingRows(gridRows, layout, create);
        }

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        // This is basically a generic version of GetRowFromPrintRow so you can pass in the layout. This 
        // Allows you to get a row from a print row, or get a print row from a real row. 
        //
        /// <summary>
        /// Gets a row in the passed-in layout that corresponds to the specified row in a different layout. 
        /// </summary>
        /// <param name="originalRow">A grid row.</param>
        /// <param name="layout">The layout in which to find the corresponding row.</param>
        /// <param name="create">Specifies true to indicate that if the row is not already created, it should be created now. Specify false to indicate that if the row is not already created, the creation of the row should not be forced.</param>
        /// <returns>Corresponding row or null if none found.</returns>
        /// <remarks>
        /// <p class="body">
        /// When printing, print-previewing or excel-exporting, the UltraGrid makes clone of the 
        /// <see cref="UltraGridBase.DisplayLayout"/> to make a print layout. 
        /// The print layout has it's own set of rows. This method is used to get the 
        /// row from the display layout that corresponds to a row from the print layout. This
        /// is useful, for example, in event handlers of printing, print-previewing or excel-exporting
        /// related events where you have access to a print row however you would like to
        /// get the corresponding row from the display layout.
        /// </p>
        /// </remarks>
        // MRS v7.2 - PDF Report Writer
        //public UltraGridRow GetRowFromPrintRow( UltraGridRow printRow )
        private UltraGridRow GetCorrespondingRow(UltraGridRow originalRow, UltraGridLayout layout, bool create)
        {
            if (null == originalRow)
                return null;

            if (originalRow is UltraGridGroupByRow)
            {
                UltraGridGroupByRow printGroupByRow = (UltraGridGroupByRow)originalRow;

                // MRS v7.2 - PDF Report Writer
                //UltraGridRow tmpRow = this.GetRowFromPrintRow( printGroupByRow.FindFirstRegularRow( ) );
                UltraGridRow tmpRow = this.GetCorrespondingRow(printGroupByRow.FindFirstRegularRow(), layout, create);

                // MD 7/26/07 - 7.3 Performance
                // FxCop - Mark members as static
                //return this.FindAncestorGroupByRow( printGroupByRow.Column, tmpRow );
                return UltraGrid.FindAncestorGroupByRow(printGroupByRow.Column, tmpRow);
            }
            else
            {
                UltraGridBand band = originalRow.Band;

                if (null == band.ParentBand)
                {
                    // SSP 5/18/05 BR04116
                    // Use the new GetRowFromPrintRow method instead.
                    //
                    //return this.Rows.GetRowWithListIndex( printRow.ListIndex );
                    // MRS v7.2 - PDF Report Writer
                    //return this.Rows.GetRowFromPrintRow( printRow );
                    //
                    return layout.Rows.GetRowFromPrintRow(originalRow, create);
                }
                else
                {
                    UltraGridRow correspondingParentBandRow = this.GetCorrespondingRow(originalRow.FindActualAncestorRow, layout, true);

                    Debug.Assert(null != correspondingParentBandRow);
                    if (null == correspondingParentBandRow)
                        return null;

                    // MRS v7.2 - PDF Report Writer
                    // Get the printRowGridBand and store it. 
                    //
                    // CDS 04/07/08 BR30140
                    // Duplicate keys can exist within the DisplayLayout, so we need to match based on Key and HierarchicalLevel
                    // which is done in GetBandFromPrintBand
                    //UltraGridBand printRowGridBand = this.DisplayLayout.Bands.Exists(printRow.Band.Key)
                    //    ? this.DisplayLayout.Bands[printRow.Band.Key]
                    //    : null;  
                    UltraGridBand printRowGridBand = this.GetBandFromPrintBand(originalRow.Band, layout);

                    // This could happen if the band structure has changed, in which case return null.
                    //
                    // SSP 3/12/04 UWG2995
                    // Check for ChildBands being null.
                    //
                    // MRS v7.2 - PDF Report Writer
                    //if (null != correspondingParentBandRow.ChildBands)
                    //
                    if (printRowGridBand != null &&
                        (create || correspondingParentBandRow.HaveChildRowsBeenCreated(printRowGridBand)) &&
                        null != correspondingParentBandRow.ChildBands)
                    {
                        // MRS v7.2 - PDF Report Writer
                        //if ( ! correspondingParentBandRow.ChildBands.Exists( printRow.Band.Key ) )
                        //
                        if (!correspondingParentBandRow.ChildBands.Exists(printRowGridBand))
                            return null;

                        // SSP 5/18/05 BR04116
                        // Use the new GetRowFromPrintRow method instead.
                        //
                        //return correspondingParentBandRow.ChildBands[ printRow.Band.Key ].Rows.GetRowWithListIndex( printRow.ListIndex );
                        // MRS v7.2 - PDF Report Writer
                        //return correspondingParentBandRow.ChildBands[ printRow.Band.Key ].Rows.GetRowFromPrintRow( printRow );
                        //
                        return correspondingParentBandRow.ChildBands[printRowGridBand].Rows.GetRowFromPrintRow(originalRow, create);

                    }

                    return null;
                }
            }
        }

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        // This is basically a generic version of GetRowFromPrintRow so you can pass in the layout. This 
        // Allows you to get a row from a print row, or get a print row from a real row. 
        //
        /// <summary>
        /// Gets a row in the passed-in layout that corresponds to the specified row in a different layout. 
        /// </summary>
        /// <param name="originalRows">A grid row.</param>
        /// <param name="layout">The layout in which to find the corresponding row.</param>
        /// <param name="create">Specifies true to indicate that if the row is not already created, it should be created now. Specify false to indicate that if the row is not already created, the creation of the row should not be forced.</param>
        /// <returns>Corresponding row or null if none found.</returns>
        /// <remarks>
        /// <p class="body">
        /// When printing, print-previewing or excel-exporting, the UltraGrid makes clone of the 
        /// <see cref="UltraGridBase.DisplayLayout"/> to make a print layout. 
        /// The print layout has it's own set of rows. This method is used to get the 
        /// row from the display layout that corresponds to a row from the print layout. This
        /// is useful, for example, in event handlers of printing, print-previewing or excel-exporting
        /// related events where you have access to a print row however you would like to
        /// get the corresponding row from the display layout.
        /// </p>
        /// </remarks>
        private RowsCollection GetCorrespondingRows(RowsCollection originalRows, UltraGridLayout layout, bool create)
        {
            if (null == originalRows)
                return null;

            UltraGridRow parentRow = originalRows.ParentRow;
            if (parentRow == null)
                return layout.Rows;

            UltraGridRow correspondingParentRow = this.GetCorrespondingRow(parentRow, layout, create);
            UltraGridGroupByRow groupByRow = correspondingParentRow as UltraGridGroupByRow;
            if (groupByRow != null)
                return groupByRow.Rows;
            else                
                return correspondingParentRow.ChildBands[originalRows.Band.Key].Rows;
        }

		#endregion // GetRowFromPrintRow

		#endregion // Fix for UWG1892

        // CDS 04/07/08 BR30140
        // Added GetBandFromPrintBand method
        //
        #region GetBandFromPrintBand

        /// <summary>
        /// Gets the band in the grid corresponding to the passed in band from the print layout when printing.
        /// </summary>
        /// <param name="originalBand">A grid band</param>
        /// <param name="layout">The layout in which to find a corresponding band.</param>
        /// <returns>Corresponding band or null if none found</returns>
        /// <remarks>
        /// <p class="body">
        /// When printing, print-previewing or excel-exporting, the UltraGrid makes clone of the 
        /// <see cref="UltraGridBase.DisplayLayout"/> to make a print layout. 
        /// The print layout has it's own set of bands. This method is used to get the 
        /// band from the display layout that corresponds to a band from the print layout. This
        /// is useful, for example, in event handlers of printing, print-previewing or excel-exporting
        /// related events where you have access to a print band however you would like to
        /// get the corresponding band from the display layout.
        /// </p>
        /// </remarks>
        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        //private UltraGridBand GetBandFromPrintBand(UltraGridBand printBand)
        private UltraGridBand GetBandFromPrintBand(UltraGridBand originalBand, UltraGridLayout layout)
        {
            if (null == originalBand)
                return null;

            // Loop through all the Bands in the current DisplayLayout
            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
            //foreach (UltraGridBand band in this.DisplayLayout.Bands)
            foreach (UltraGridBand band in layout.SortedBands)
            {
                // Match based on Key and HierarchicalLevel as duplicate keys can exist
                if (band.Key == originalBand.Key &&
                    band.HierarchicalLevel == originalBand.HierarchicalLevel)
                    return band;
            }
            Debug.Fail("Corresponding band not found in the specified layout. (Key = " + originalBand.Key + ", HierarchicalLevel = " + originalBand.HierarchicalLevel.ToString() + ")");
            return null;
        }

        #endregion

		// SSP 5/7/03 - Excel Exporting Functionality
		// Added Export method.
		//
		#region Export

		/// <summary>
		/// Export method.
		/// </summary>
		/// <param name="exporter">An object that implements <see cref="IUltraGridExporter"/> interface.
		/// The <b>UltraGridExcelExporter</b> component is one such component that implements this interface.
		/// </param>
		/// <remarks>
		/// <p class="body">
		/// The UltraGridExcelExporter component makes use of this method.
		/// Typically there is no need to call this method directly. You typically use the 
		/// <b>UltraGridExcelExporter</b> compoenent to export an UltraGrid to an Excel file.
		/// </p>
		/// <seealso cref="PrintPreview()"/>
		/// <seealso cref="Print()"/>
		/// </remarks>
		// SSP 6/26/03 
		// Marked the method editor browsable never.
		//
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public void Export( IUltraGridExporter exporter )
		{
			if ( null == exporter )
				throw new ArgumentNullException( "exporter" );

			// Create a new layout.
			//
			UltraGridLayout exportLayout = new UltraGridLayout( );

			try
			{
				exportLayout.InternalSetIsExportLayout( true );

				// Set the retain row property categories to all.
				//
				exportLayout.RetainRowPropertyCategories = RowPropertyCategories.All;

				// Initialize the layout with the grid.
				//
				exportLayout.InitGrid( this );

				try
				{
					// SSP 6/18/02 UWG1229
					// Added a flag to prevent the rows from being loaded until the 
					// print layout and the main layout have been merged. We want to
					// carry over any unbound columns first before firing the initialize
					// row.
					//
					this.printing_DontInitializeRows = true;

					// SSP 3/12/04 - UWG2995
					// Copy the MaxBandDepth before loading the data structure.
					//
					if ( null != this.DisplayLayout )
						exportLayout.MaxBandDepth = this.DisplayLayout.MaxBandDepth;
			
					//Init Bands from datasource
					//
					exportLayout.ListManagerUpdated( );	
			
					//Need to set isprinting when we Initialize from
					//or else sorting does not work
					//
					//this.isPrinting = true;
		
					// Initialize the export layout to the display layout.
					//
					exportLayout.InitializeFrom(this.DisplayLayout, PropertyCategories.All);

					//reset flag
					//
					//this.isPrinting = false;
                    
                    // MRS 5/23/07 - BR23083
                    // I moved this up inside the try...finally block from below. This is the way it's done
                    // when printing, and apparently this needs to be called before CopyPrintRelatedSettings
                    //
                    // SSP 1/17/05 BR01753
                    // Print and export layout won't calculate formulas but rather they will copy over 
                    // the calculated values from the display layout. We need to ensure that all the 
                    // columns are calculated before we go ahead with printing.
                    //
                    this.DisplayLayout.EnsureAllFormulasCalculated();
				}
				finally
				{
					// SSP 6/18/02 UWG1229
					//
					this.printing_DontInitializeRows = false;

					// Then call the ApplyLoadedBands to fire the initialize layout and
					// intialize the rows.
					//
					exportLayout.ApplyLoadedBands( );

					// SSP 8/14/03 UWG2589 - Filter Action
					// When Filter Action functionality was added, the Hidden property's behavior was changed.
					// Now the Hidden property won't get set when the filter is evaluated. So we need to copy
					// the column filters on every rows collection when printing or exporting to excel.
					//
					// SSP 1/18/05 BR01753
					// Only enable calculations in display layout. Print and export layouts will
					// copy over the calculated values from the display layout.
					//
					//UltraGridRow.CopyColumnFilters( this.Rows, exportLayout.Rows );
					UltraGridRow.CopyPrintRelatedSettings( this.Rows, exportLayout.Rows );
				}

                // MRS 5/23/07 - BR23083
                // I moved this up inside the try...finally block above. This is the way it's done
                // when printing, and apparently this needs to be called before CopyPrintRelatedSettings
                //
                //// SSP 1/17/05 BR01753
                //// Print and export layout won't calculate formulas but rather they will copy over 
                //// the calculated values from the display layout. We need to ensure that all the 
                //// columns are calculated before we go ahead with printing.
                ////
                //this.DisplayLayout.EnsureAllFormulasCalculated( );

				exporter.BeginExport( exportLayout, exportLayout.Rows );

				bool canceled = ! exportLayout.Rows.InternalTraverseRowsHelper( exporter );

				exporter.EndExport( canceled );
			}
			finally
			{
				// SSP 1/22/04 UWG2891
				// Dispose of the layout.
				//
				exportLayout.Dispose( );
			}
		}

		#endregion // Export

		#region InvalidateActiveRowCell

		// SSP 2/10/05
		// Added InvalidateActiveRowCell helper method.
		//
		private void InvalidateActiveRowCell( )
		{
			UltraGridCell activeCell = this.ActiveCell;
			UltraGridRow activeRow = this.ActiveRow;
			
			if ( null != activeCell )
				activeCell.InvalidateItem( );
			else if ( null != activeRow )
				activeRow.InvalidateItem( );
		}

		#endregion // InvalidateActiveRowCell

		#region OnGotFocus

		/// <summary>
		/// Invokes the GotFocus event.
		/// </summary>
		/// <param name="e">Event args</param>
		protected override void OnGotFocus( EventArgs e )
		{
			// SSP 2/10/05
			// We need to invalidate the active cell or active row when the grid gets focus
			// so the focus rect gets drawn.
			//
			this.InvalidateActiveRowCell( );

			// AS 1/9/04
			// When the control gets focus, let the accessibility
			// clients know that focus is on a particular
			// cell/row/editor.
			//
			this.AccessibilityNotifyClientsInternal(AccessibleEvents.Focus, 0);

			base.OnGotFocus(e);
		}

		#endregion // OnGotFocus

		// MRS 6/14/04 - UWG3235
		// Need to invalidate if there was an Active Cell
		#region OnLostFocus
		/// <summary>
		/// Invokes the LostFocus event.
		/// </summary>
		/// <param name="e">Event args</param>
		protected override void OnLostFocus(System.EventArgs e)
		{
			base.OnLostFocus(e);

			// SSP 2/10/05
			// Use the InvalidateItem method instead for code simplification.
			//
			// ------------------------------------------------------------------------------
			
			this.InvalidateActiveRowCell( );
			// ------------------------------------------------------------------------------
		}
		#endregion OnLostFocus

		// AS 7/1/04 UWE985
		#region SupportsEditorWithMask
		/// <summary>
		/// This member supports the Infragistics infrastructure and is not meant to be invoked externally.
		/// </summary>
		/// <remarks>
		/// <p class="body">Indicates whether the control can utilize an <see cref="EditorWithMask"/></p>
		/// </remarks>
		protected override bool SupportsEditorWithMask
		{
			get { return true; }
		}
		#endregion //SupportsEditorWithMask

		#region HeaderClickActionDefault
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal override HeaderClickAction HeaderClickActionDefault
		{
			get
			{
				return 
					ViewStyleBand.OutlookGroupBy == this.DisplayLayout.ViewStyleBand
					? HeaderClickAction.SortMulti 
					: HeaderClickAction.Select;
			}
		}
		#endregion HeaderClickActionDefault

		// JAS 2005 v2 XSD Support
		//
		#region XSD Support

			#region ApplyBandLevelXsdConstraints

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void ApplyBandLevelXsdConstraints( Win.Xml.Schema.XsdValueSetConstraint xsdValueSetConstraint, UltraGridBand band, XsdConstraintFlags constraintFlags )
		private static void ApplyBandLevelXsdConstraints( Win.Xml.Schema.XsdValueSetConstraint xsdValueSetConstraint, UltraGridBand band, XsdConstraintFlags constraintFlags )
		{
			Debug.Assert( band != null, "'band' argument should not be null." );
			if( band == null )
				return;

			if( xsdValueSetConstraint.HasMaxOccurrences && (constraintFlags & XsdConstraintFlags.MaxRows) != 0 )
			{
				band.MaxRows = xsdValueSetConstraint.MaxOccurrences;	
				band.XsdSuppliedConstraints |= XsdConstraintFlags.MaxRows;
			}

			if( xsdValueSetConstraint.HasMinOccurrences && (constraintFlags & XsdConstraintFlags.MinRows) != 0 )
			{
				band.MinRows = xsdValueSetConstraint.MinOccurrences;
				band.XsdSuppliedConstraints |= XsdConstraintFlags.MinRows;
			}
		}

			#endregion // ApplyBandLevelXsdConstraints

			#region ApplyColumnLevelXsdConstraints

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void ApplyColumnLevelXsdConstraints( Win.Xml.Schema.XsdValueConstraint xsdValueConstraint, UltraGridColumn column, XsdConstraintFlags constraintFlags )
		private static void ApplyColumnLevelXsdConstraints( Win.Xml.Schema.XsdValueConstraint xsdValueConstraint, UltraGridColumn column, XsdConstraintFlags constraintFlags )
		{
			Debug.Assert( column != null, "'column' argument should not be null." );
			if( column == null )
				return;

			#region DefaultValue --> DefaultCellValue

			if( xsdValueConstraint.HasDefaultValue && (constraintFlags & XsdConstraintFlags.DefaultValue) != 0 )
			{
				object defaultValue = ValueConstraint.ValueToDataValue( xsdValueConstraint.DefaultValue, column.DataType, null, null );
				if( defaultValue != null )
				{
					column.DefaultCellValue = defaultValue;
					column.XsdSuppliedConstraints |= XsdConstraintFlags.DefaultValue;
				}
			}

			#endregion // DefaultValue --> DefaultCellValue

			ValueConstraint valueConstraint = xsdValueConstraint.Constraint;

			#region Enumeration --> ValueList

			if( valueConstraint.HasEnumeration && (constraintFlags & XsdConstraintFlags.Enumeration) != 0 )
			{
				column.ValueList = valueConstraint.Enumeration;
				column.XsdSuppliedConstraints |= XsdConstraintFlags.Enumeration;
			}

			#endregion // Enumeration --> ValueList

			#region FixedValue --> DefaultCellValue

			if( valueConstraint.HasFixedValue && (constraintFlags & XsdConstraintFlags.FixedValue) != 0 )
			{
				object fixedValue = ValueConstraint.ValueToDataValue( valueConstraint.FixedValue, column.DataType, null, null );
				if( fixedValue != null )
				{
					// The notion of a 'fixed' value does not make much sense in a grid.  In XSD a 'fixed' value
					// is one whose value cannot be anything other than a certain value (the 'fixed' value).  The grid
					// will enforce this constraint by ensuring that all cells in a given column are given the 'fixed'
					// value when a row is added or a cell in the 'fixed' column is edited.
					//
					column.DefaultCellValue = fixedValue;
					column.XsdSuppliedConstraints |= XsdConstraintFlags.FixedValue;

					// The column's Constraint needs to know that it has a fixed value.  Just because we set the
					// column's DefaultCellValue to the fixed value does not mean that the column's ValueConstraint 
					// knows that the column actually has a fixed value.  This is important when the editors perform 
					// validation because they check the ValueConstraint's HasAnyConstraints property to see if any 
					// validation needs to be performed.  If we did not set the FixedValue property here, the column's
					// internal ValueConstraint object would return false from HasAnyConstraints.
					//
					// Bear in mind that the ValueConstraint class does not have a property analogous to the column's
					// DefaultCellValue.  Instead, the XsdValueConstraint class has the DefaultValue property because it
					// was deemed illogical to have a generic constraint-oriented class contain a property which indicates 
					// a default value. 
					//
					column.Constraint.FixedValue = fixedValue;
				}
			}

			#endregion // FixedValue --> DefaultCellValue

			#region MaxExclusive --> MaxValueExclusive

			if( valueConstraint.HasMaxExclusive && (constraintFlags & XsdConstraintFlags.MaxExclusive) != 0 )
			{
				column.MaxValueExclusive = valueConstraint.MaxExclusive;
				column.XsdSuppliedConstraints |= XsdConstraintFlags.MaxExclusive;
			}

			#endregion // MaxExclusive --> MaxValueExclusive

			#region MaxInclusive --> MaxValue

			if( valueConstraint.HasMaxInclusive && (constraintFlags & XsdConstraintFlags.MaxInclusive) != 0 )
			{
				column.MaxValue = valueConstraint.MaxInclusive;
				column.XsdSuppliedConstraints |= XsdConstraintFlags.MaxInclusive;
			}

			#endregion // MaxInclusive --> MaxValue

			#region MaxLength --> MaxLength

			if( valueConstraint.HasMaxLength && (constraintFlags & XsdConstraintFlags.MaxLength) != 0 )
			{
				column.MaxLength = valueConstraint.MaxLength;
				column.XsdSuppliedConstraints |= XsdConstraintFlags.MaxLength;
			}

			#endregion // MaxLength --> MaxLength

			#region MinExclusive --> MinValueExclusive

			if( valueConstraint.HasMinExclusive && (constraintFlags & XsdConstraintFlags.MinExclusive) != 0 )
			{
				column.MinValueExclusive = valueConstraint.MinExclusive;
				column.XsdSuppliedConstraints |= XsdConstraintFlags.MinExclusive;
			}

			#endregion // MinExclusive --> MinValueExclusive

			#region MinInclusive --> MinValue

			if( valueConstraint.HasMinInclusive && (constraintFlags & XsdConstraintFlags.MinInclusive) != 0 )
			{
				column.MinValue = valueConstraint.MinInclusive;
				column.XsdSuppliedConstraints |= XsdConstraintFlags.MinInclusive;
			}

			#endregion // MinInclusive --> MinValue

			#region MinLength --> MinLength

			if( valueConstraint.HasMinLength && (constraintFlags & XsdConstraintFlags.MinLength) != 0 )
			{
				column.MinLength = valueConstraint.MinLength;
				column.XsdSuppliedConstraints |= XsdConstraintFlags.MinLength;
			}

			#endregion // MinLength --> MinLength

			#region Nullable --> Nullable

			if( valueConstraint.HasNullable && (constraintFlags & XsdConstraintFlags.Nullable) != 0 )
			{	
				Debug.Assert( valueConstraint.Nullable != DefaultableBoolean.Default, "HasNullable returned true, but the Nullable property was 'Default'." );

				column.Nullable = 
					valueConstraint.Nullable == DefaultableBoolean.True ? 
					Nullable.Automatic : 
					Nullable.Disallow;

				column.XsdSuppliedConstraints |= XsdConstraintFlags.Nullable;
			}

			#endregion // Nullable --> Nullable

			#region RegexPattern --> RegexPattern

			if( valueConstraint.HasRegexPattern && (constraintFlags & XsdConstraintFlags.RegexPattern) != 0 )
			{
				column.RegexPattern = valueConstraint.RegexPattern;
				column.XsdSuppliedConstraints |= XsdConstraintFlags.RegexPattern;
			}

			#endregion // RegexPattern --> RegexPattern
		}

			#endregion // ApplyColumnLevelXsdConstraints

			#region ClearBandLevelXsdConstraints

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void ClearBandLevelXsdConstraints( UltraGridBand band )
		private static void ClearBandLevelXsdConstraints( UltraGridBand band )
		{
			Debug.Assert( band != null, "'band' argument should not be null." );
			if( band == null )
				return;

			if( (band.XsdSuppliedConstraints & XsdConstraintFlags.MaxRows) != 0 )
				band.ResetMaxRows();

			if( (band.XsdSuppliedConstraints & XsdConstraintFlags.MinRows) != 0 )
				band.ResetMinRows();
		}

			#endregion // ClearBandLevelXsdConstraints

			#region ClearColumnLevelXsdConstraints

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void ClearColumnLevelXsdConstraints( UltraGridColumn column )
		private static void ClearColumnLevelXsdConstraints( UltraGridColumn column )
		{
			Debug.Assert( column != null, "'column' argument should not be null." );
			if( column == null )
				return;

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.DefaultValue) != 0 )
				column.ResetDefaultCellValue();

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.Enumeration) != 0 )
				column.ResetValueList();

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.FixedValue) != 0 )
				column.ResetDefaultCellValue();

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.MaxExclusive) != 0 )
				column.ResetMaxValueExclusive();

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.MaxInclusive) != 0 )
				column.ResetMaxValue();

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.MaxLength) != 0 )
				column.ResetMaxLength();

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.MinExclusive) != 0 )
				column.ResetMinValueExclusive();

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.MinInclusive) != 0 )
				column.ResetMinValue();

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.MinLength) != 0 )
				column.ResetMinLength();

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.Nullable) != 0 )
				column.ResetNullable();

			if( (column.XsdSuppliedConstraints & XsdConstraintFlags.RegexPattern) != 0 )
				column.ResetRegexPattern();

			//Debug.Assert( column.XsdSuppliedConstraints == 0, "All of the properties that were set have been reset, so XsdSuppliedConstraints should equal 0." );
		}

			#endregion // ClearColumnLevelXsdConstraints

			#region ClearXsdConstraints

		/// <summary>
        /// Resets all of the properties on the bands and column which were set by the <see cref="EnforceXsdConstraints( string, XsdConstraintFlags )"/> method.
		/// If a constraint property was set again after the EnforceXsdConstraints method set it, that property will not be reset.
		/// </summary>
		public void ClearXsdConstraints()
		{
			foreach( UltraGridBand band in this.DisplayLayout.SortedBands )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.ClearBandLevelXsdConstraints( band );
				UltraGrid.ClearBandLevelXsdConstraints( band );

				foreach( UltraGridColumn column in band.Columns )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.ClearColumnLevelXsdConstraints( column );
					UltraGrid.ClearColumnLevelXsdConstraints( column );
				}
			}

			// After the constraints have been cleared, it might be necessary to update the uielement hierarchy.
			// For example, if an AddNewButton had been disabled due to the MaxRows constraint on a band, that button
			// needs to be updated.
			//
			if( this.ControlUIElement != null )
			{
				this.ControlUIElement.DirtyChildElements( true );
				this.ControlUIElement.VerifyChildElements( true );
			}
		}

			#endregion // ClearXsdConstraints

			#region EnforceXsdConstraints Overloads

				#region EnforceXsdConstraints( Stream )

		/// <summary>
		/// Applies the constraints found in the XSD schema contained in the Stream to the bands and columns of the grid.
		/// </summary>
		/// <param name="stream">A Stream containing an XSD schema.</param>
		/// <remarks>
		/// <p class="body">
		/// There are a couple requirements imposed on the XSD schema passed to this method.
		///	<ol>
		///		<li>"The XSD namespace ("http://www.w3.org/2001/XMLSchema") must have a prefix associated with it (such as 'xs' or 'xsd').</li>
		///		<li>"The schema's target namespace must be the default namespace (i.e. cannot have a prefix associated with it).</li>
		///	</ol>
		///	<br/>
		///	This overload uses <b>All</b> to specify which constraints should be applied.
		/// </p>
		/// </remarks>
		public void EnforceXsdConstraints( System.IO.Stream stream )
		{
			this.EnforceXsdConstraints( stream, XsdConstraintFlags.All );
		}

				#endregion // EnforceXsdConstraints( Stream )

				#region EnforceXsdConstraints( String )

		/// <summary>
		/// Applies the constraints found in the XSD schema contained in the specified file to the bands and columns of the grid.
		/// </summary>
		/// <param name="filePath">The path to a file containing an XSD schema.</param>
		/// <remarks>
		/// <p class="body">
		/// There are a couple requirements imposed on the XSD schema passed to this method.
		///	<ol>
		///		<li>"The XSD namespace ("http://www.w3.org/2001/XMLSchema") must have a prefix associated with it (such as 'xs' or 'xsd').</li>
		///		<li>"The schema's target namespace must be the default namespace (i.e. cannot have a prefix associated with it).</li>
		///	</ol>
		///	<br/>
		///	This overload uses <b>All</b> to specify which constraints should be applied.
		/// </p>
		/// </remarks>
		public void EnforceXsdConstraints( string filePath )
		{
			this.EnforceXsdConstraints( filePath, XsdConstraintFlags.All );
		}

				#endregion // EnforceXsdConstraints( String )

				#region EnforceXsdConstraints( Stream, XsdConstraintFlags )

		/// <summary>
		/// Applies the constraints found in the XSD schema contained in the Stream to the bands and columns of the grid.
		/// </summary>
		/// <param name="stream">A Stream containing an XSD schema.</param>
		/// <param name="constraintFlags">A bit flag which specifies the constraints to be extracted from the XSD schema.</param>
		/// <remarks>
		/// <p class="body">
		/// There are a couple requirements imposed on the XSD schema passed to this method.
		///	<ol>
		///		<li>"The XSD namespace ("http://www.w3.org/2001/XMLSchema") must have a prefix associated with it (such as 'xs' or 'xsd').</li>
		///		<li>"The schema's target namespace must be the default namespace (i.e. cannot have a prefix associated with it).</li>
		///	</ol>
		/// </p>
		/// </remarks>
		public void EnforceXsdConstraints( System.IO.Stream stream, XsdConstraintFlags constraintFlags )
		{
			System.Xml.XPath.XPathDocument xpathDoc = new System.Xml.XPath.XPathDocument( stream );

			this.EnforceXsdConstraintsHelper( xpathDoc, constraintFlags );
		}

				#endregion // EnforceXsdConstraints( Stream, XsdConstraintFlags )

				#region EnforceXsdConstraints( String, XsdConstraintFlags )

		/// <summary>
		/// Applies the constraints found in the XSD schema contained in the specified file to the bands and columns of the grid.
		/// </summary>
		/// <param name="filePath">The path to a file containing an XSD schema.</param>
		/// <param name="constraintFlags">A bit flag which specifies the constraints to be extracted from the XSD schema.</param>
		/// <remarks>
		/// <p class="body">
		/// There are a couple requirements imposed on the XSD schema passed to this method.
		///	<ol>
		///		<li>"The XSD namespace ("http://www.w3.org/2001/XMLSchema") must have a prefix associated with it (such as 'xs' or 'xsd').</li>
		///		<li>"The schema's target namespace must be the default namespace (i.e. cannot have a prefix associated with it).</li>
		///	</ol>
		/// </p>
		/// </remarks>
		public void EnforceXsdConstraints( string filePath, XsdConstraintFlags constraintFlags )
		{
			System.Xml.XPath.XPathDocument xpathDoc = new System.Xml.XPath.XPathDocument( filePath );

			this.EnforceXsdConstraintsHelper( xpathDoc, constraintFlags );
		}

				#endregion // EnforceXsdConstraints( String, XsdConstraintFlags )

			#endregion // EnforceXsdConstraints Overloads

			#region EnforceXsdConstraintsHelper

		private void EnforceXsdConstraintsHelper( System.Xml.XPath.XPathDocument xpathDoc, XsdConstraintFlags constraintFlags )
		{
			if( this.DataSource == null )
				throw new Exception( SR.GetString( "LE_UltraGrid_XSD_MissingDataSource" ) );

			foreach( UltraGridBand band in this.DisplayLayout.SortedBands )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//string bandName = this.GetBandNameFromDataSource( band );
				string bandName = UltraGrid.GetBandNameFromDataSource( band );

				

				Win.Xml.Schema.XsdValueSetConstraint xsdValueSetConstraint = new Win.Xml.Schema.XsdValueSetConstraint( bandName, xpathDoc );

				if( xsdValueSetConstraint.FoundValueSet )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.ApplyBandLevelXsdConstraints( xsdValueSetConstraint, band, constraintFlags );
					UltraGrid.ApplyBandLevelXsdConstraints( xsdValueSetConstraint, band, constraintFlags );

					foreach( UltraGridColumn column in band.Columns )
					{
						string columnName = column.Key;
						Win.Xml.Schema.XsdValueConstraint xsdValueConstraint = xsdValueSetConstraint.GetConstraintsOnXsdValue( columnName );

						if( xsdValueConstraint != null && xsdValueConstraint.FoundValue )
						{
							// If the bit flag specifies that the constraints imposed by the built-in XSD base data type 
							// should be ignored, then we set the ValidateAsType to 'Unknown'.
							//
							if( (constraintFlags & XsdConstraintFlags.ImplicitXsdBaseTypeConstraints) == 0 )
								xsdValueConstraint.Constraint.ValidateAsType = ValidateAsType.Unknown;

							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//this.ApplyColumnLevelXsdConstraints( xsdValueConstraint, column, constraintFlags );
							UltraGrid.ApplyColumnLevelXsdConstraints( xsdValueConstraint, column, constraintFlags );
						}
					}
				}
			}
		}

			#endregion // EnforceXsdConstraintsHelper

			#region GetBandNameFromDataSource

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private string GetBandNameFromDataSource( UltraGridBand band )
		private static string GetBandNameFromDataSource( UltraGridBand band )
		{
			// If the band's data source is a DataView or DataRowView, then try to get the name of the
			// underlying table from which the view was created.
			//
			System.Data.DataView dataView = band.List as System.Data.DataView;

			if( dataView == null )
			{
				System.Data.DataRowView rowView = band.List as System.Data.DataRowView;
				if( rowView != null )
					dataView = rowView.DataView;
			}

			if( dataView != null && dataView.Table != null )
			{
				return dataView.Table.TableName;
			}				

			// If the band's data source is not a DataView or DataRowView, then return the band's Key.
			//
			return band.Key;
		}

			#endregion // GetBandNameFromDataSource

		#endregion // XSD Support

		// JAS v5.2 DoubleClick Events 4/27/05
		//
		#region DoubleClick Events Logic

			#region DoubleClickCell Event

		/// <summary>
		/// Fires when a cell is double-clicked.
		/// </summary>
		/// <remarks>
		/// <p class="body"></p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGrid_E_DoubleClickCell")]	
		public event DoubleClickCellEventHandler DoubleClickCell
		{
			add    { this.EventsOptimized.AddHandler(    EVENT_DOUBLECLICKCELL, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_DOUBLECLICKCELL, value ); } 
		}

			#endregion // DoubleClickCell Event

			#region DoubleClickHeader Event

		/// <summary>
		/// Occurs when a header is double-clicked.
		/// </summary>
		/// <remarks>
		/// <p class="body"></p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGrid_E_DoubleClickHeader")]	
		public event DoubleClickHeaderEventHandler DoubleClickHeader
		{
			add    { this.EventsOptimized.AddHandler(    EVENT_DOUBLECLICKHEADER, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_DOUBLECLICKHEADER, value ); } 
		}

			#endregion // DoubleClickHeader Event

			#region DoubleClickRow Event

		/// <summary>
		/// Occurs when a row is double-clicked.
		/// </summary>
		/// <remarks>
		/// <p class="body"></p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGrid_E_DoubleClickRow")]	
		public event DoubleClickRowEventHandler DoubleClickRow
		{
			add    { this.EventsOptimized.AddHandler(    EVENT_DOUBLECLICKROW, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_DOUBLECLICKROW, value ); } 
		}

			#endregion // DoubleClickRow Event

			#region OnDoubleClickCell

		/// <summary>
		/// Fires the DoubleClickCell event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnDoubleClickCell( DoubleClickCellEventArgs e )
		{
			DoubleClickCellEventHandler handler = (DoubleClickCellEventHandler)this.EventsOptimized[EVENT_DOUBLECLICKCELL];
			if( handler != null )
				handler( this, e );
		}

			#endregion // OnDoubleClickCell

			#region OnDoubleClickHeader

		/// <summary>
		/// Fires the DoubleClickHeader event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnDoubleClickHeader( DoubleClickHeaderEventArgs e )
		{
			DoubleClickHeaderEventHandler handler = (DoubleClickHeaderEventHandler)this.EventsOptimized[EVENT_DOUBLECLICKHEADER];
			if( handler != null )
				handler( this, e );
		}

			#endregion // OnDoubleClickHeader

			#region OnDoubleClickRow

		/// <summary>
		/// Fires the DoubleClickRow event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnDoubleClickRow( DoubleClickRowEventArgs e )
		{
			DoubleClickRowEventHandler handler = (DoubleClickRowEventHandler)this.EventsOptimized[EVENT_DOUBLECLICKROW];
			if( handler != null )
				handler( this, e );
		}

			#endregion // OnDoubleClickRow

		#endregion // DoubleClick Events Logic

		// MRS 3/14/05 - Added column moving support for rowlayouts
		#region Added column moving support for rowlayouts	

		#region StartGridBagLayoutDrag
        // MRS - NAS 9.1 - Groups in RowLayout
		//internal bool StartGridBagLayoutDrag(UltraGridColumn column, bool isGroupByButton)
        internal bool StartGridBagLayoutDrag(IProvideRowLayoutColumnInfo itemBeingDragged, bool isGroupByButton)
		{			
			this.ResetGridBagLayoutDragStrategy();

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//bool createdDragStrategy = this.CreateLayoutDragStrategy(column, GridBagLayoutDragManager.GridBagLayoutDragType.HeaderReposition);
			bool createdDragStrategy = UltraGrid.CreateLayoutDragStrategy( itemBeingDragged, GridBagLayoutDragManager.GridBagLayoutDragType.HeaderReposition );

			if (!createdDragStrategy)
				return false;
			
			GridBagLayoutDragManager gridBagLayoutDragManager = this.gridBagLayoutDragStrategy.GridBagLayoutDragManager as GridBagLayoutDragManager;

			// MRS 4/21/05 - BR03503
			gridBagLayoutDragManager.isGroupByButtonBeingDragged = isGroupByButton;

			UIElement[] dragElements = gridBagLayoutDragManager.GetDragElements(itemBeingDragged, isGroupByButton);
			
			if (dragElements != null)
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//Point mouseOffset = gridBagLayoutDragManager.GetMouseOffSet(dragElements);
				Point mouseOffset = GridBagLayoutDragManager.GetMouseOffSet( dragElements );

                // CDS 9.2 Column Moving Indicators
                // Resolve the DragDropIndicatorSettings off the band
                DragDropIndicatorSettings dragDropIndicatorSettings = (itemBeingDragged.Band != null) ? itemBeingDragged.Band.DragDropIndicatorSettingsResolved : null;

                // CDS 9.2 Column Moving Indicators
                // Pass the resolved DragDropIndicatorSettings to DragStart
                //// MRS 2/4/2009 - TFS13513
                ////return this.gridBagLayoutDragStrategy.DragStart(dragElements, mouseOffset);
                //return this.gridBagLayoutDragStrategy.DragStart(dragElements, mouseOffset, UltraGrid.DRAG_INDICATOR_OPACITY);
                return this.gridBagLayoutDragStrategy.DragStart(dragElements, mouseOffset, UltraGrid.DRAG_INDICATOR_OPACITY, dragDropIndicatorSettings);
            }	
		
			return false;
		}
		#endregion StartGridBagLayoutDrag	

		#region DragMoveHelper
		internal void DragMoveHelper(Point pointInControlCoords)
		{
			UltraGrid grid = this;

			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// 
			CurrentCursor cursorToDisplay = CurrentCursor.Default;
			bool callDragEffect = false;

			// SSP 6/28/05 - NAS 5.3 Column Chooser
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//bool isMouseOverColumnChooser = this.IsOverColumnChooser( pointInControlCoords, false );
			
			if (this.gridBagLayoutDragStrategy != null)
			{
				this.gridBagLayoutDragStrategy.DragMove(pointInControlCoords);

				// SSP 6/28/05 - NAS 5.3 Column Chooser
				// We need to use top level windows for the column chooser. For that changed to 
				// using the Infragistics.Win's DragDropIndicatorManager.
				// 
				//if (null != grid.DragEffect.DragWindow)
				//	grid.DragEffect.DragWindow.Hide();
				grid.DragEffect.HideDragIndicator( );
				
				// MRS 5/19/05 - BR03851
				// Instead of checking if we are outside the GroupByBox, 
				// check it we are inside the Layout area. 
				//if (!GridUtils.IsPointInGroupByBox(grid, pointInControlCoords))
				if (((GridBagLayoutDragManager)this.gridBagLayoutDragStrategy.GridBagLayoutDragManager).IsPointInValidLayoutArea(pointInControlCoords))
				{
					grid.DragEffect.HideDropGuides( );
				}
				else
				{
					// SSP 6/28/05 - NAS 5.3 Column Chooser
					// 
					//grid.DragEffect.OnDragMove( pointInControlCoords );
					callDragEffect = true;
				}
			}
			else
			{
				// SSP 6/28/05 - NAS 5.3 Column Chooser
				// 
				//grid.DragEffect.OnDragMove( pointInControlCoords );
				callDragEffect = true;
			}

			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// 
			// ------------------------------------------------------------------------------
			if ( callDragEffect )
			{
				grid.DragEffect.OnDragMove( pointInControlCoords );
				cursorToDisplay = grid.DragEffect.CurrentCursor;
			}

			CurrentCursor tmpCursorToDisplay;
			grid.ColumnChooser_OnDragMove( pointInControlCoords, out tmpCursorToDisplay );
			if ( CurrentCursor.Default != tmpCursorToDisplay )
				cursorToDisplay = tmpCursorToDisplay;

			GridUtils.SetCursorToCurrent( this, cursorToDisplay );
			// ------------------------------------------------------------------------------
		}
		#endregion DragMoveHelper		

		#region EndGridBagLayoutDrag

		// SSP 6/30/05 - NAS 5.3 Column Chooser
		// Return the success status.
		// 
		//internal void EndGridBagLayoutDrag(Point pointInGridCoords)
		internal bool EndGridBagLayoutDrag(Point pointInGridCoords)
		{
			bool ret = this.gridBagLayoutDragStrategy.DragEnd(pointInGridCoords);
			this.ResetGridBagLayoutDragStrategy();

			return ret;
		}
		#endregion EndGridBagLayoutDrag

		#region CreateLayoutDragStrategy

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool CreateLayoutDragStrategy(UltraGridColumn column, GridBagLayoutDragManager.GridBagLayoutDragType dragType)
        // MRS - NAS 9.1 - Groups in RowLayout
		//private static bool CreateLayoutDragStrategy( UltraGridColumn column, GridBagLayoutDragManager.GridBagLayoutDragType dragType )
        private static bool CreateLayoutDragStrategy(IProvideRowLayoutColumnInfo itemBeingDragged, GridBagLayoutDragManager.GridBagLayoutDragType dragType)
		{
			UltraGridBand band = itemBeingDragged.Band;
			if (band == null)
				return false;

			if (band.AllowRowLayoutColMovingResolved == Infragistics.Win.Layout.GridBagLayoutAllowMoving.None &&
				band.AllowGroupByResolved == DefaultableBoolean.False)
			{
					return false;
			}			

			GridBagLayoutDragManager gridBagLayoutDragManager = new GridBagLayoutDragManager(itemBeingDragged, dragType);

			// MD 1/22/09 - Groups in RowLayouts
			// The Control property has been renamed for clarity
			//UltraGrid grid = gridBagLayoutDragManager.Control as UltraGrid;						
			UltraGrid grid = gridBagLayoutDragManager.Grid as UltraGrid;

			if ( grid == null)
				return false;
			
			GridBagLayoutDragStrategy gridBagLayoutDragStrategy = new GridBagLayoutDragStrategy(gridBagLayoutDragManager);			
			grid.gridBagLayoutDragStrategy = gridBagLayoutDragStrategy;

			return true;
		}

		#endregion CreateLayoutDragStrategy

		#region ResetGridBagLayoutDragStrategy
		internal void ResetGridBagLayoutDragStrategy()
		{			
			if (null == this.gridBagLayoutDragStrategy)
				return;

			this.gridBagLayoutDragStrategy.Dispose();
			this.gridBagLayoutDragStrategy = null;

		}
		#endregion ResetGridBagLayoutDragStrategy

		#region SpanResizeCursorHoriz

		// SSP 7/2/03 UWG2446
		// Added different cursors for span resizing than the size resizing.
		// Added SpanResizeCursorHoriz and SpanResizeCursorVert properties.
		//
		internal System.Windows.Forms.Cursor SpanResizeCursorHoriz
		{
			get
			{
				if ( null == this.spanResizeCursorHoriz )
				{
					// Load the cursor resource.
					// SSP 6/14/02 UWG1238
					// this.GetType returns the derived class' type and Module.Assembly will be
					// the assembly that the derived class if from and the resources are in
					// our assembly. So below statemenet won't work if we use GetType( ).
					// Instead use the typeof operator with our class name as the operand.
					//
					//System.IO.Stream	stream	= this.GetType().Module.Assembly.GetManifestResourceStream(this.GetType(), "AutoSize.cur");
					System.IO.Stream	stream	= 
						typeof( UltraGrid ).Module.Assembly.GetManifestResourceStream( typeof( UltraGrid ), "SpanResizeHoriz.cur" );
					
					this.spanResizeCursorHoriz = new Cursor( stream );

					stream.Close();
				}

				Debug.Assert( null != this.spanResizeCursorHoriz, "No SpanResizeHoriz.cur resource !" );
				if ( null == this.spanResizeCursorHoriz )
					return Cursors.SizeWE;

				return this.spanResizeCursorHoriz;
			}
		}

		#endregion SpanResizeCursorHoriz

		#region SpanResizeCursorVert

		// SSP 7/2/03 UWG2446
		// Added different cursors for span resizing than the size resizing.
		// Added SpanResizeCursorHoriz and SpanResizeCursorVert properties.
		//
		internal System.Windows.Forms.Cursor SpanResizeCursorVert
		{
			get
			{
				if ( null == this.spanResizeCursorVert )
				{
					// Load the cursor resource.
					// SSP 6/14/02 UWG1238
					// this.GetType returns the derived class' type and Module.Assembly will be
					// the assembly that the derived class if from and the resources are in
					// our assembly. So below statemenet won't work if we use GetType( ).
					// Instead use the typeof operator with our class name as the operand.
					//
					//System.IO.Stream	stream	= this.GetType().Module.Assembly.GetManifestResourceStream(this.GetType(), "AutoSize.cur");
					System.IO.Stream	stream	= 
						typeof( UltraGrid ).Module.Assembly.GetManifestResourceStream( typeof( UltraGrid ), "SpanResizeVert.cur" );
					
					this.spanResizeCursorVert = new Cursor( stream );

					stream.Close();
				}

				Debug.Assert( null != this.spanResizeCursorVert, "No SpanResizeVert.cur resource !" );
				if ( null == this.spanResizeCursorVert )
					return Cursors.SizeNS;

				return this.spanResizeCursorVert;
			}
		}

		#endregion SpanResizeCursorVert

		#region StartRowLayoutSpanResizeDrag
        // MRS - NAS 9.1 - Groups in RowLayout
		//internal bool StartRowLayoutSpanResizeDrag(UltraGridColumn column, bool isDragSpanHorizontal, bool isHeaderBeingDragged)
        internal bool StartRowLayoutSpanResizeDrag(IProvideRowLayoutColumnInfo itemBeingDragged, bool isDragSpanHorizontal, bool isHeaderBeingDragged)
		{			
			this.ResetGridBagLayoutDragStrategy();
			GridBagLayoutDragManager.GridBagLayoutDragType dragType = (isHeaderBeingDragged) ? GridBagLayoutDragManager.GridBagLayoutDragType.HeaderSpanResize : GridBagLayoutDragManager.GridBagLayoutDragType.CellSpanResize;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.CreateLayoutDragStrategy(column, dragType);
			UltraGrid.CreateLayoutDragStrategy( itemBeingDragged, dragType );
			
			bool areColumnHeadersInSeparateArea = GridBagLayoutDragManager.GetAreColumnHeadersInSeparateArea(itemBeingDragged);
			bool isCardView = itemBeingDragged.Band.CardView;

			// MD 2/25/09 - TFS14602
			//bool isLabelOnSide = itemBeingDragged.Band.RowLayoutLabelPositionResolved == LabelPosition.Left ||
			//    itemBeingDragged.Band.RowLayoutLabelPositionResolved == LabelPosition.Right;
            // MRS 3/5/2009 - TFS14882 
			//LabelPosition resolvedLabelPosition = itemBeingDragged.Band.GetRowLayoutLabelPositionResolved( itemBeingDragged.RowLayoutColumnInfo.ContextType );
            LabelPosition resolvedLabelPosition = itemBeingDragged.RowLayoutColumnInfo.LabelPositionResolved;
			bool isLabelOnSide =
				resolvedLabelPosition == LabelPosition.Left ||
				resolvedLabelPosition == LabelPosition.Right;

            // MRS - NAS 9.1 - Groups in RowLayout
            //return this.gridBagLayoutDragStrategy.DragSpanStart(isDragSpanHorizontal, areColumnHeadersInSeparateArea, isHeaderBeingDragged, isCardView, isLabelOnSide);
            return this.gridBagLayoutDragStrategy.DragSpanStart(
                isDragSpanHorizontal,
                areColumnHeadersInSeparateArea,
                isHeaderBeingDragged,
                isCardView,
                isLabelOnSide,
                // CDS 9.2 Column Moving Indicators
                //itemBeingDragged is UltraGridGroup);
                itemBeingDragged is UltraGridGroup,
                itemBeingDragged.Band.DragDropIndicatorBrushColorResolved);
		}
		#endregion StartRowLayoutSpanResizeDrag

		#region DragSpanMove
		internal void DragSpanMove(Point pointInControlCoords, UIElement dragElement, ILayoutItem headerDragItem, ILayoutItem columnDragItem)
		{
			if (this.gridBagLayoutDragStrategy != null)
				this.gridBagLayoutDragStrategy.DragSpanMove(pointInControlCoords, dragElement, headerDragItem, columnDragItem);
		}
		#endregion DragSpanMove		

		#region DragSpanEnd
		internal void DragSpanEnd(Point pointInControlCoords, UIElement dragElement, ILayoutItem headerDragItem, ILayoutItem columnDragItem)
		{
			if (this.gridBagLayoutDragStrategy != null)
			{
				this.gridBagLayoutDragStrategy.DragSpanEnd(pointInControlCoords, dragElement, headerDragItem, columnDragItem);
				this.ResetGridBagLayoutDragStrategy();

			}
		}
		#endregion DragSpanEnd
		
		#endregion Added column moving support for rowlayouts

		#region InvalidateCellOnMouseEnterLeave

		// SSP 4/6/05
		// A customer requested a way to prevent UltraGrid from invalidating cells on 
		// mouse enter and leave in performance intensive scenarios. For that added 
		// InvalidateCellOnMouseEnterLeave property.
		//
		/// <summary>
		/// By default UltraGrid invalidates cell ui elements whenever mouse enters or 
		/// leaves them. You can override this property and return <b>false</b> to prevent 
		/// this behavior. The default implementation always retruns true.
		/// </summary>
		internal protected virtual bool InvalidateCellOnMouseEnterLeave
		{
			get
			{
				return true;
			}
		}

		#endregion // InvalidateCellOnMouseEnterLeave

		#region SyncWithCurrencyManager

		// SSP 5/18/05 BR03434
		// Added SyncWithCurrencyManager to prevent the UltraGrid from synchronizing with
		// the currency manager. When there are a lot of deep nested bands activating a 
		// different ancestor row caues a lot of slowdown.
		//
		/// <summary>
		/// Specifies whether to synchronize the active row with the currency manager's position. Default is <b>True</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can set the <b>SyncWithCurrencyManager</b> property to <b>False</b> to prevent the 
		/// UltraGrid from synchronizing the active row with the associated currency manager's position.</p>
		/// <p class="body">This is useful if you have a lot of deeply nested bands and navigating rows is very slow.</p>
		/// </remarks>
		[ DefaultValue( true ) ]
        [LocalizedDescription("LD_UltraGrid_P_SyncWithCurrencyManager")]
        [LocalizedCategory("LC_Behavior")]
		public bool SyncWithCurrencyManager
		{
			get
			{
				return this.syncWithCurrencyManager;
			}
			set
			{
                // MRS NAS v8.2 - Unit Testing
                //this.exitEditModeOnLeave = value;
                if (this.syncWithCurrencyManager == value)
                    return;

                this.syncWithCurrencyManager = value;

                this.NotifyPropChange(PropertyIds.SyncWithCurrencyManager);					
			}
		}

		#endregion // SyncWithCurrencyManager

		// SSP 6/16/05 - NAS 5.3 Column Chooser Feature
		// 
		#region Column Chooser Feature

		#region GetItemsBeingDragged

		internal override UltraGridColumn[] GetItemsBeingDragged( out bool isDraggingGroupByButton )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList retList = new ArrayList( );
			List<UltraGridColumn> retList = new List<UltraGridColumn>();

			GridBagLayoutDragManager gblmDrag = null == this.gridBagLayoutDragStrategy ? null
				: this.gridBagLayoutDragStrategy.GridBagLayoutDragManager as GridBagLayoutDragManager;
			if ( null != gblmDrag )
			{
				RowLayoutColumnInfo ci = gblmDrag.GetDragItemRowLayoutColumnInfo( );
				if ( null != ci && null != ci.Column )
					retList.Add( ci.Column );
			}
			else if ( null != this.DragEffect && null != this.DragEffect.DragStrategy 
				&& null != this.DragEffect.DragStrategy.DragItems )
			{
				foreach ( object item in this.DragEffect.DragStrategy.DragItems )
				{
					if ( item is Infragistics.Win.UltraWinGrid.ColumnHeader )
						retList.Add( ((Infragistics.Win.UltraWinGrid.ColumnHeader)item).Column );
				}
			}

			DragStrategyColumn colDragStrategy = null != this.DragEffect ? this.DragEffect.DragStrategy as DragStrategyColumn : null;
			isDraggingGroupByButton = null != colDragStrategy && colDragStrategy.IsDraggingGroupByButton;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (UltraGridColumn[])retList.ToArray( typeof( UltraGridColumn ) );
			return retList.ToArray();
		}

		#endregion // GetItemsBeingDragged

		#endregion // Column Chooser Feature

		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// 
		#region NAS 6.1 Multi-cell Operations

		#region ScrollSelectionIntoView

		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// Added an overload of SelectNewSelection that takes in newCells enumerator.
		// 
		internal void ScrollSelectionIntoView( )
		{
			UltraGridLayout layout = this.DisplayLayout;
			Infragistics.Win.UltraWinGrid.Selected selected = this.Selected;
			RowScrollRegion rsr = null != layout ? layout.ActiveRowScrollRegion : null;
			ColScrollRegion csr = null != layout ? layout.ActiveColScrollRegion : null;

			if ( null == layout || null == selected || layout.Disposed || null == rsr || null == csr )
				return;

			bool needsToEndUpdate = false;
			if ( ! this.IsUpdating )
			{
				this.BeginUpdate( );
				needsToEndUpdate = true;
			}

			if ( selected.HasRows )
			{
				UltraGridRow firstRow = selected.Rows[0];
				UltraGridRow lastRow = selected.Rows[ selected.Rows.Count - 1 ];

				rsr.ScrollRowIntoView( lastRow );
				rsr.ScrollRowIntoView( firstRow );
			}
			else if ( selected.HasCells )
			{
				UltraGridCell firstCell = selected.Cells[0];
				UltraGridCell lastCell = selected.Cells[ selected.Cells.Count - 1 ];

				rsr.ScrollCellIntoView( lastCell, csr );
				rsr.ScrollCellIntoView( firstCell, csr );
			}
			else if ( selected.HasColumns )
			{
				ColumnHeader firstColumn = selected.Columns[0];
				ColumnHeader lastColumn = selected.Columns[ selected.Columns.Count - 1 ];

				csr.ScrollHeaderIntoView( lastColumn, false );
				csr.ScrollHeaderIntoView( firstColumn, false );
			}

			if ( needsToEndUpdate )
				this.EndUpdate( );
		}

		#endregion // ScrollSelectionIntoView

		#region ScrollActiveItemIntoView

		// MRS 1/10/06 - BR08665
		// Sandip's notes say that we are following Excel behavior, 
		// but in my testing, Excel 2003 does not scroll the selection
		// into view, it only scrolls the Active Cell or active row into view, 
		// if it was not completely in view. 
		//
		internal void ScrollActiveItemIntoView( )
		{
			UltraGridLayout layout = this.DisplayLayout;
			Infragistics.Win.UltraWinGrid.Selected selected = this.Selected;
			UltraGridRow activeRow = this.ActiveRow;
			UltraGridCell activeCell = this.ActiveCell;
			RowScrollRegion rsr = null != layout ? layout.ActiveRowScrollRegion : null;
			ColScrollRegion csr = null != layout ? layout.ActiveColScrollRegion : null;

			if ( null == layout || layout.Disposed || null == rsr || null == csr )
				return;

            // MRS 1/8/2008 - BR29480
            // Check if the ActiveCell / ActiveRow is already in view.            
            if (activeCell != null)
            {
                UIElement element = activeCell.GetUIElement();
                if (element != null &&
                    element.ClipRect == element.Rect)
                {
                    return;
                }
            }
            else if (activeRow != null)
            {
                UIElement element = activeRow.GetUIElement();
                if (element != null &&
                    element.ClipRect == element.Rect)
                {
                    return;
                }
            }

			bool needsToEndUpdate = false;
			if ( ! this.IsUpdating )
			{
				this.BeginUpdate( );
				needsToEndUpdate = true;
			}

			if ( activeCell != null )
				csr.ScrollCellIntoView( activeCell, rsr );
			else if ( activeRow != null )
				rsr.ScrollRowIntoView( activeRow );
			else if ( selected != null && selected.HasColumns )
			{
				ColumnHeader firstColumn = selected.Columns[0];
				ColumnHeader lastColumn = selected.Columns[ selected.Columns.Count - 1 ];

				csr.ScrollHeaderIntoView( lastColumn, false );
				csr.ScrollHeaderIntoView( firstColumn, false );
			}

			if ( needsToEndUpdate )
				this.EndUpdate( );
		}

		#endregion // ScrollActiveItemIntoView

		#region SelectNewSelection

		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// Added an overload of SelectNewSelection that takes in newCells enumerator.
		// 
		internal bool SelectNewSelection( IEnumerable newCells )
		{
			Infragistics.Win.UltraWinGrid.Selected selected = new Selected( );
			foreach ( UltraGridCell cell in newCells )
				selected.Cells.InternalAdd( cell, false );

			return this.SelectNewSelection( typeof( UltraGridCell ), selected );
		}

		#endregion // SelectNewSelection

		#region ClearUndoHistory
		
		/// <summary>
		/// Clears the undo/redo history.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Undo/redo can be enabled using the <see cref="UltraGridOverride.AllowMultiCellOperations"/> property.
		/// </p>
		/// <seealso cref="UltraGridOverride.AllowMultiCellOperations"/>
		/// </remarks>
		public void ClearUndoHistory( )
		{
			if ( null != this.DisplayLayout )
				this.DisplayLayout.ClearUndoHistoryHelper( true );
		}

		#endregion // ClearUndoHistory

		#endregion // NAS 6.1 Multi-cell Operations

		// MD 1/11/08 - BR24622
		#region SupportsFormattedLinkEditor

		/// <summary>
		/// This member supports the Infragistics infrastructure and is not meant to be invoked externally.
		/// </summary>
		/// <remarks>
		/// <p class="body">Indicates whether the control can utilize an <see cref="Infragistics.Win.FormattedLinkLabel"/></p>
		/// </remarks>
		protected override bool SupportsFormattedLinkEditor
		{
			get { return true; }
		}

		#endregion SupportsFormattedLinkEditor

        // MBS 3/12/08 - RowEditTemplate NA2008 V2        
        #region OnEndInit

        /// <summary>
        /// Invoked during the <see cref="ISupportInitialize.EndInit"/> of the component.
        /// </summary>
        protected override void OnEndInit()
        {
            base.OnEndInit();

            if (this.DesignMode)
                this.HookComponentChangeService();
        }
        #endregion //OnEndInit
        //
        #region HookComponentChangeService

        private void HookComponentChangeService()
        {
            if (this.hookedIntoComponentChangeService)
                return;

            IComponentChangeService changeService = this.GetService( typeof( IComponentChangeService ) ) as IComponentChangeService;
            if (changeService != null)
            {
                this.hookedIntoComponentChangeService = true;
                changeService.ComponentRemoving += new ComponentEventHandler(this.OnComponentRemoving);
                changeService.ComponentRename += new ComponentRenameEventHandler(this.OnComponentRename);                
            }
        }
        #endregion //HookComponentChangeService
        //
        #region UnhookComponentChangeService

        private void UnhookComponentChangeService()
        {
            if (!this.hookedIntoComponentChangeService)
                return;

            IComponentChangeService changeService = this.GetService(typeof(IComponentChangeService)) as IComponentChangeService;
            if (changeService != null)
            {
                this.hookedIntoComponentChangeService = false;
                changeService.ComponentRemoving -= new ComponentEventHandler(this.OnComponentRemoving);                
                changeService.ComponentRename -= new ComponentRenameEventHandler(this.OnComponentRename);
            }
        }
        #endregion //UnhookComponentChangeService
        //
        #region OnComponentRename

        private void OnComponentRename(object sender, ComponentRenameEventArgs e)
        {
            if (e.Component is UltraGridRowEditTemplate)
            {
                UltraGridRowEditTemplate template = (UltraGridRowEditTemplate)e.Component;
                if (template.Grid == this && this.DisplayLayout.SortedBands.Contains(template.Band))
                {
                    // Set the RowEditTemplate property of the band using a PropertyDescriptor
                    // so that we will get Undo/Redo functionality.
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(UltraGridBand));
                    PropertyDescriptor templateProperty = properties.Find("RowEditTemplate", false);
                    templateProperty.SetValue(template.Band, e.Component);
                }
            }
        }
        #endregion //OnComponentRename
        //
        #region OnComponentRemoving
        
        private void OnComponentRemoving(object sender, ComponentEventArgs e)
        {
            if (e.Component == this)
                this.UnhookComponentChangeService();
            else if (e.Component is UltraGridRowEditTemplate)
            {
                UltraGridRowEditTemplate template = (UltraGridRowEditTemplate)e.Component;
                if (template.Grid == this && this.DisplayLayout.SortedBands.Contains(template.Band))
                {
                    // Null out the RowEditTemplate property of the band using a PropertyDescriptor
                    // so that we will get Undo/Redo functionality.
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(UltraGridBand));
                    PropertyDescriptor templateProperty = properties.Find("RowEditTemplate", false);
                    templateProperty.SetValue(template.Band, null);
                }
            }
        }
        #endregion //OnComponentRemoving
        //
        #region Site

        /// <summary>
        /// The site is set at design time only.
        /// </summary>
        public override ISite Site
        {
            get
            {
                return base.Site;
            }
            set
            {
                // if the site is being reset first try to unhook from the 
                // component change service events
                if (value == null)
                    this.UnhookComponentChangeService();

                base.Site = value;

                // Re-hook the ComponentChange with the new Site
                if (value != null && value.DesignMode)
                    this.HookComponentChangeService();
            }
        }
        #endregion //Site

        //  BF 4/21/08  NA 2008.2 - WinValidator
        #region IValidatorClient interface implementation
        //
        //  Note that the ValidationSettings extender property (see Infragistics.Win.Misc.UltraValidator)       
        //  is not extended to implementors of this interface.
        //
        /// <summary>
        /// Returns whether validation is supported through <see cref="Infragistics.Win.EmbeddableEditorBase">embeddable editors</see>.
        /// </summary>
        bool IValidatorClient.IsValidatorSupported
        {
            get { return true; }
        }
        #endregion IValidatorClient interface implementation

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        #region GetColumnFromPrintColumn

        /// <summary>
        /// Gets the column in the grid corresponding to the passed in column from a print layout when printing.
        /// </summary>
        /// <param name="printColumn">A column from a print layout.</param>
        /// <returns>Corresponding column or null if none found.</returns>
        /// <remarks>
        /// <p class="body">
        /// When printing, print-previewing or excel-exporting, the UltraGrid makes clone of the 
        /// <see cref="UltraGridBase.DisplayLayout"/> to make a print layout. 
        /// The print layout has it's own set of columns. This method is used to get the 
        /// column from the display layout that corresponds to a column from the print layout. This
        /// is useful, for example, in event handlers of printing, print-previewing or excel-exporting
        /// related events where you have access to a print column however you would like to
        /// get the corresponding column from the display layout.
        /// </p>
        /// </remarks>
        public UltraGridColumn GetColumnFromPrintColumn(UltraGridColumn printColumn)
        {
            if (null == printColumn)
                return null;
            
            UltraGridBand printBand = printColumn.Band;
            UltraGridBand gridBand = this.DisplayLayout.SortedBands[printBand.Index];

            if (printBand.Key != gridBand.Key)
            {
                Debug.Fail("Found the wrong band by index; unexpected");
                return null;
            }

            if (gridBand.Columns.Exists(printColumn.Key) == false)
            {
                Debug.Fail("Column does not exist; unexpected");
                return null;
            }

            return gridBand.Columns[printColumn.Key];
        }

        #endregion // GetColumnFromPrintColumn

		// MD 1/16/09 - Groups in RowLayout
        #region GridBagLayoutDragStrategy

		GridBagLayoutDragStrategy IGridDesignInfo.GridBagLayoutDragStrategy
		{
			get { return this.gridBagLayoutDragStrategy; }
			set { this.gridBagLayoutDragStrategy = value; }
		}

		#endregion GridBagLayoutDragStrategy
        	
        // MRS 5/21/2009 - TFS17817
        #region GetSummarySettingsFromPrintSummarySettings
        /// <summary>
        /// Gets the SummarySettings in the grid corresponding to the passed in SummarySettings from a print or export layout.
        /// </summary>
        /// <param name="summarySettings">A SummarySettings from a print or export layout.</param>
        /// <returns>Corresponding SummarySettings or null if none found.</returns>
        /// <remarks>
        /// <p class="body">
        /// When printing, print-previewing or excel-exporting, the UltraGrid makes clone of the 
        /// <see cref="UltraGridBase.DisplayLayout"/> to make a print layout. 
        /// The print layout has it's own set of SummarySettings objects. This method is used to get the 
        /// SummarySettings from the display layout that corresponds to a SummarySettings from the print layout. This
        /// is useful, for example, in event handlers of printing, print-previewing or excel-exporting
        /// related events where you have access to a print row however you would like to
        /// get the corresponding SummarySettings from the display layout.
        /// </p>
        /// </remarks>
        public SummarySettings GetSummarySettingsFromPrintSummarySettings(SummarySettings summarySettings)
        {
            UltraGridBand band = summarySettings.Band;
            if (band == null)
            {
                Debug.Fail("The specified summarySettings it not associated with a Band");
                return null;
            }

            UltraGridLayout layout = band.Layout;
            if (layout == null)
            {
                Debug.Fail("The specified summarySettings it not associated with a Layout");
                return null;
            }

            if ( ! layout.IsExportLayout &&
                 ! layout.IsPrintLayout)
            {
                throw new ArgumentException("The specified SummarySettings object does not belong to a Print or Export layout");                
            }

            return summarySettings.sourceSummarySettings;
        }
        #endregion //GetSummarySettingsFromPrintSummarySettings

        // CDS 9.2 Column Moving Indicators
        #region DefaultDropIndicatorDownArrowImage

        internal Image DefaultDropIndicatorDownArrowImage
        {
            get
            {
                if (null == this.defaultDropIndicatorDownArrowImage)
                {
                    // CDS 4/29/09 9.2 Column Moving Indicators
                    //
                    //this.defaultDropIndicatorDownArrowImage = UltraGridLayout.GetImageFromResourse("Images.DropIndicatorDownArrow.png");
                    this.defaultDropIndicatorDownArrowImage = UltraGridLayout.GetImageFromResourse("Images.DefaultDropIndicatorDownArrow.png");
                }

                return this.defaultDropIndicatorDownArrowImage;
            }
        }

        #endregion // DefaultDropIndicatorDownArrowImage


        // CDS 9.2 Column Moving Indicators
        #region ColorizableDropIndicatorDownArrowImage

        internal Image ColorizableDropIndicatorDownArrowImage
        {
            get
            {
                if (null == this.colorizableDropIndicatorDownArrowImage)
                {
                    this.colorizableDropIndicatorDownArrowImage = UltraGridLayout.GetImageFromResourse("Images.ColorizableDropIndicatorDownArrow.png");
                }

                return this.colorizableDropIndicatorDownArrowImage;
            }
        }

        #endregion // ColorizableDropIndicatorDownArrowImage

    }// end of class


		


}
