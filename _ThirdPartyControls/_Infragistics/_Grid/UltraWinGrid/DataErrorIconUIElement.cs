#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using System.Diagnostics;

// SSP 12/22/04 - IDataErrorInfo Support
//

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// The element class for representing data error icons in the cells and the row selectors.
	/// </summary>
	public class DataErrorIconUIElement : UIElement
	{
		#region Private Vars

		internal const int DEFAULT_WIDTH  = 16;
		internal const int DEFAULT_HEIGHT = 16;
		internal const int PADDING = 2;

		private AppearanceData imageAppData = new AppearanceData( );
		private bool hasShownTooltipSinceLastMouseEnter = false;       
		private bool showingTooltip = false;

		#endregion // Private Vars

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public DataErrorIconUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags flags)
		{
			AppearanceData.Merge( ref appData, ref this.imageAppData, ref flags );
			base.InitAppearance( ref appData, ref flags );
		}

		#endregion // InitAppearance

		#region InitImageAppData

		internal void InitImageAppData( ref AppearanceData imageAppData )
		{
			this.imageAppData = imageAppData;
		}

		#endregion // InitImageAppData

		#region Row

		/// <summary>
		/// Returns the associated row object.
		/// </summary>
		public UltraGridRow Row
		{
			get
			{
				return (UltraGridRow)this.GetContext( typeof( UltraGridRow ) );
			}
		}

		#endregion // Row

		#region Column

		/// <summary>
		/// If the data error icon is positioned in a cell then this property returns the 
		/// associated column. If the data error icon is positioned in a row selector then
		/// this property returns null.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return (UltraGridColumn)this.GetContext( typeof( UltraGridColumn ) );
			}
		}

		#endregion // Column

		#region IsOverCell

		private bool IsOverCell
		{
			get
			{
				return null != this.Column;
			}
		}

		#endregion // IsOverCell

		#region IsOverRowSelector

		private bool IsOverRowSelector
		{
			get
			{
				return ! this.IsOverCell;
			}
		}

		#endregion // IsOverRowSelector

		#region OnMouseEnter

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseEnter( )
		{
			this.hasShownTooltipSinceLastMouseEnter = false;
			base.OnMouseEnter( );
		}

		#endregion // OnMouseEnter

		#region OnMouseHover

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseHover( )
		{
			UltraGridRow row = this.Row;
			if ( ! this.hasShownTooltipSinceLastMouseEnter && null != row )
			{
				UltraGridColumn column = this.Column;
				string errorMessage = row.GetDataError( column );

				// MRS 8/19/05 - We were not honoring e.Cancel
				bool cancelled = false;

				// SSP 4/7/05 
				// Added code to fire BeforeDisplayDataErrorTooltip.
				//
				UltraGridBase grid = row.Layout.Grid;
				if ( grid is UltraGrid )
				{
					BeforeDisplayDataErrorTooltipEventArgs e = new BeforeDisplayDataErrorTooltipEventArgs( 
						row, column, row.GetDataErrorInfo( ), errorMessage );
					((UltraGrid)grid).FireEvent( GridEventIds.BeforeDisplayDataErrorTooltip, e );
					errorMessage = e.TooltipText;

					// MRS 8/19/05 - We were not honoring e.Cancel
					cancelled = e.Cancel;
				}

				// MRS 8/19/05 - We were not honoring e.Cancel
				//if ( null != errorMessage && errorMessage.Length > 0 )
				if ( !cancelled && null != errorMessage && errorMessage.Length > 0 )
				{
					Infragistics.Win.ToolTip tt = null != grid ? grid.TooltipTool : null;
					if ( null != tt )
					{
						tt.ToolTipText = errorMessage;
						tt.Show( );						
						this.showingTooltip = true;
						this.hasShownTooltipSinceLastMouseEnter = true;
					}
				}
			}
		}

		#endregion // OnMouseHover

		#region OnMouseLeave

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseLeave( )
		{
			if ( this.showingTooltip )
			{
				UltraGridBase grid = this.Row.Layout.Grid;
				Infragistics.Win.ToolTip tt = null != grid ? grid.TooltipTool : null;
				if ( null != tt )
					tt.Hide( );

				this.showingTooltip = false;
			}

			base.OnMouseLeave( );
		}

		#endregion // OnMouseLeave

		#region WantsMouseHoverNotification

		/// <summary>
		/// Overridden. Always returns true.
		/// </summary>
		protected override bool WantsMouseHoverNotification
		{
			get
			{
				return true;
			}
		}

		#endregion // WantsMouseHoverNotification

		#region DrawBackColor

		/// <summary>
		/// this element may not draw its back color
		/// </summary>
		protected override void DrawBackColor( ref UIElementDrawParams drawParams )
		{
			if ( this.IsOverCell )
			{
				CellDisplayStyle cellDisplayStyle = this.Column.GetCellDisplayStyleResolved( this.Row );
				if ( CellDisplayStyle.FullEditorDisplay == cellDisplayStyle )
					base.DrawBackColor( ref drawParams );
			}
		}

		#endregion // DrawBackColor

		#region DrawImageBackground

		/// <summary>
		/// this element may not draw an image background
		/// </summary>
		protected override void DrawImageBackground( ref UIElementDrawParams drawParams )
		{
			if ( this.IsOverCell )
			{
				CellDisplayStyle cellDisplayStyle = this.Column.GetCellDisplayStyleResolved( this.Row );
				if ( CellDisplayStyle.FullEditorDisplay == cellDisplayStyle )
					base.DrawImageBackground( ref drawParams );
			}
		}

		#endregion // DrawImageBackground

		#region PositionChildElements

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void PositionChildElements( )
		{
			ImageUIElement imageElem = (ImageUIElement)DataErrorIconUIElement.ExtractExistingElement( 
				this.childElementsCollection, typeof( ImageUIElement ), true );

			UltraGridBase grid = this.Row.Layout.Grid;
			Image dataErrorIcon = this.imageAppData.GetImage( null != grid ? grid.ImageList : null );
			
			if ( null == imageElem )
				imageElem = new ImageUIElement( this, dataErrorIcon );
			else if ( imageElem.Image != dataErrorIcon )
				imageElem.Image = dataErrorIcon;

			Rectangle rect = this.RectInsideBorders;

			// SSP 1/24/05 BR01800
			// Add padding of 2 pixels around the data error image.
			//
			rect.Inflate( - PADDING, - PADDING );

			imageElem.Rect = rect;
			imageElem.MaintainAspectRatio = true;
			imageElem.Scaled = null != dataErrorIcon && 
				( dataErrorIcon.Width > rect.Width || dataErrorIcon.Height > rect.Height );

			this.ChildElements.Clear( );
			this.ChildElements.Add( imageElem );
		}

		#endregion // PositionChildElements
	}
}

