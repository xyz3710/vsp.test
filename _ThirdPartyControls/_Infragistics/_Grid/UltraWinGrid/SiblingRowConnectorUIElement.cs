#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Diagnostics;

    /// <summary>
    /// Daws the connector lines between sibling rows
    /// </summary>
    public class SiblingRowConnectorUIElement : UIElement
    {
        private Infragistics.Win.UltraWinGrid.UltraGridBand parentBand;
        private Infragistics.Win.UltraWinGrid.UltraGridRow  parentRow;
        private Infragistics.Win.UltraWinGrid.UltraGridRow  rowBelow;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
        //private Infragistics.Win.UltraWinGrid.UltraGridRow  currentTooltipRow;

        private bool                        firstChildConnector;
        private bool                        rowAboveTipEnabled;
        private bool                        rowBelowTipEnabled;
		private System.Windows.Forms.Cursor lastLineCursor = null;

        internal SiblingRowConnectorUIElement( UIElement parent ) : base( parent )
        {
        }

		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>SiblingRowConnectorUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="parentBand">Associated parent <b>Band</b></param>
		/// <param name="parentRow">Associated parent <b>Row</b></param>
		/// <param name="firstChildConnector">True if this is the first child connector</param>
        public SiblingRowConnectorUIElement( UIElement parent,
									Infragistics.Win.UltraWinGrid.UltraGridBand parentBand,
									Infragistics.Win.UltraWinGrid.UltraGridRow  parentRow,
									bool firstChildConnector ) : this( parent )
        {
			this.Initialize(parentBand, parentRow, firstChildConnector);
        }

		internal void Initialize( Infragistics.Win.UltraWinGrid.UltraGridBand parentBand,
                                  Infragistics.Win.UltraWinGrid.UltraGridRow  parentRow,
                                  bool firstChildConnector )
        {
            this.parentBand             = parentBand;
            this.parentRow              = parentRow;
            this.firstChildConnector    = firstChildConnector;
            this.rowBelow               = null;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
            //this.currentTooltipRow      = null;

            this.rowAboveTipEnabled     = false;
            this.rowBelowTipEnabled     = false;
        }

        // JJD 9/13/00 - ult1147
        // Added flags to restrict line tips
        //
        internal void SetRowAboveTipEnabled( bool newValue ) 
        { 
            this.rowAboveTipEnabled = newValue; 
        }

        internal void SetRowBelowTipEnabled( bool newValue ) 
        { 
            this.rowBelowTipEnabled = newValue; 
        }

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
                                                 ref AppearancePropFlags requestedProps )
		{
			// SSP 3/16/06
			// Use the new RowConnectorColorResolved property.
			// 
			//appearance.BorderColor = this.parentBand.Layout.RowConnectorColor;
			appearance.BorderColor = this.parentBand.Layout.RowConnectorColorResolved;

			// SSP 8/23/02 UWG1209
			// Also set the bordercolor3dbase to the border color so if the
			// border style was a 3d border, then it would still draw with
			// the border color rather than basing it on the back color.
			//
			appearance.BorderColor3DBase = appearance.BorderColor;
		}
  
		/// <summary>
		/// does nothing since this element doesn't draw a background
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
        protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
        {
            // this element doesn't draw a background
        }
 
		/// <summary>
		/// this element doesn't draw an image background
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
		}
 
		/// <summary>
		/// draws the element
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
        protected override void DrawForeground ( ref UIElementDrawParams drawParams )
        {
			
			
			
			
			

			
			
			Rectangle thisRect = this.Rect;

            Rectangle rcWork = this.rectValue;

            rcWork.X = (thisRect.Left + thisRect.Right) / 2;
            rcWork.Width = 1;
            rcWork.Height++;

            Border3DSide  sides   = Border3DSide.Left;

            UIElementBorderStyle eStyle = this.parentBand.Layout.RowConnectorBorderStyle;

            switch ( eStyle )
            {
            case UIElementBorderStyle.Dotted:
            case UIElementBorderStyle.Dashed:
                {
                    int penOriginAdjustment = Math.Abs( ( (RowScrollRegion)this.Parent.GetContext( typeof( RowScrollRegion ) ) ).PenOrigin );

                    // adjust the top and height of the rect based on the pen origin
                    //
                    rcWork.Y        += penOriginAdjustment;
                    rcWork.Height   -= penOriginAdjustment;
                    break;
                }
            
            case UIElementBorderStyle.InsetSoft:
                rcWork.Width++;
                sides   |= Border3DSide.Right;
                break;

            case UIElementBorderStyle.RaisedSoft:
                rcWork.X--;
                rcWork.Width++;
                sides   |= Border3DSide.Right;
                break;

            }

            drawParams.DrawBorders( eStyle, 
                                    sides,
                                    drawParams.AppearanceData.BorderColor,
                                    rcWork,
                                    drawParams.InvalidRect );

        }

		/// <summary>
		/// Return true if this element wants to be notified when the mouse hovers over it. This property is read-only.
		/// </summary>
		/// <remarks>Returns true since row connectos need MouseHover notifications for showing the tooltips.</remarks>
		protected override bool WantsMouseHoverNotification
		{
			get
			{
				return true;
			}
		}
				
		/// <summary>
		/// Activates the tooltip for the row connector vert line. 
		/// </summary>
		protected override void OnMouseHover( )
		{	
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = (Infragistics.Win.UltraWinGrid.UltraGridLayout)this.Parent.GetContext( typeof ( Infragistics.Win.UltraWinGrid.UltraGridLayout ), true );

			UltraGrid grid = layout.Grid as UltraGrid;

			if ( grid == null )
				return;

			
			// SSP 1/8/02
			// Get the mouse point. OnMouseHover's signature was changed
			// and point parameter was taken out making it necessary to
			// get the mouse position.
			//
			Point pt = grid.PointToClient( System.Windows.Forms.Control.MousePosition );


			UltraGridRow row = this.GetTooltipRow();

			if ( null != row && layout.Grid.IsHandleCreated )
			{				
				Infragistics.Win.ToolTip toolTip = grid.TooltipTool;

				if ( null != toolTip )
				{
					Point ptTip = pt;

					ptTip = layout.Grid.PointToScreen( ptTip );

					// Adjusted tip so that it is below cursor pos
					//
					ptTip.Y += 20;

					int lines = 0;
					// SSP 7/19/05
					// TipStyleScroll is used by the scrolling as well as the row connector tooltips. We 
					// need to resolve the TipStyleScroll differently based on whether the scrolling logic 
					// or the row connector logic is calling it.
					// 
					//string tipText = row.GetScrollTip( ref lines );
					string tipText = row.GetScrollTip( ref lines, true );

					// SSP 7/19/05
					// Don't bother showing the tooltip if the text is empty.
					// 
					if ( null != tipText && tipText.Length > 0 )
					{
						toolTip.SetMargin( 3, 3, 0, 0 );
					
			
						toolTip.ToolTipText = tipText;			

						Rectangle exclusionAreaRect;

						// set up the exclusion rect so that the tooltip won't overlap
						// the line area
						//
						exclusionAreaRect = this.Rect;

						exclusionAreaRect.Inflate( 6, 0 );
					
						exclusionAreaRect = layout.Grid.RectangleToScreen( exclusionAreaRect );

						toolTip.Show( ptTip, 0, true, true, true, exclusionAreaRect, true, false );

						// MD 7/26/07 - 7.3 Performance
						// FxCop - Avoid unused private fields
						//this.currentTooltipRow = row;
					}
				}
			}
    
		}

		private System.Windows.Forms.Cursor LineCursor
		{
			get
			{
				// JJD 1/17/02
				// Added support for line up and line down cursors
				//
				if ( !this.Disposed )
				{
					UltraGridRow tooltipRow = this.GetTooltipRow();

					if ( tooltipRow != null )
					{
						if ( tooltipRow == this.parentRow )
							return tooltipRow.Layout.LineUpCursor;
						else
							return tooltipRow.Layout.LineDownCursor;
					}
				}

				return null;
			}
		}

        /// <summary>
        /// Returns the cursor that should be used when the mouse 
        /// is over the element. By default this just walks up
        /// the parent chain by returning its parent's cursor
        /// </summary>
        public override System.Windows.Forms.Cursor Cursor
		{
			get 
			{
				// JJD 1/17/02
				// Get and cache the lineCursor
				//
				this.lastLineCursor = this.LineCursor;

				if ( this.lastLineCursor != null )
					return this.lastLineCursor;

				return base.Cursor;
			}
		}

		internal void OnKeyToggled()
		{
			// If we hadn't cached the cursor the return
			//
			if ( this.lastLineCursor == null )
				return;

			System.Windows.Forms.Cursor newLineCursor = this.LineCursor;

			// If the line cursor hasn't changed then exit
			//
			if ( newLineCursor == null ||
				 this.lastLineCursor == newLineCursor )
				return;

            // MRS 12/19/06 - fxCop
            //try { perm.Assert(); } catch(Exception){}

            //// JJD 1/18/02
            //// Set the cursor position to itself which will trigger an update 
            //// of the cursor but wrap it in a try/catch in case we don't have
            //// access
            ////
            //try
            //{
            //    System.Windows.Forms.Cursor.Position = System.Windows.Forms.Cursor.Position;
            //}
            //catch(Exception)
            //{
            //}
            GridUtils.SetCursorPosition(System.Windows.Forms.Cursor.Position);
			
			// call on mouse hover which will display the new tooltip
			//
			this.OnMouseHover();

		}
		
		internal UltraGridRow GetTooltipRow()
		{
			if ( null == this.parentRow )
				return null;

			// Check flags that restrict line tips
			//
			if ( 0 != ( Control.ModifierKeys & Keys.Control ) &&
				 this.rowAboveTipEnabled &&
                 Infragistics.Win.UltraWinGrid.TipStyle.Hide != this.parentRow.Band.TipStyleRowConnectorResolved )
				return this.parentRow;

			// Check flags that restrict line tips
			//
			if ( ! this.rowBelowTipEnabled )
			{
				if ( this.rowAboveTipEnabled &&
					 Infragistics.Win.UltraWinGrid.TipStyle.Hide != this.parentRow.Band.TipStyleRowConnectorResolved )
					return this.parentRow;
				
				return null;
			}

			if ( null != this.rowBelow )
			{
                return Infragistics.Win.UltraWinGrid.TipStyle.Hide != this.rowBelow.Band.TipStyleRowConnectorResolved
						? this.rowBelow 
						: null;			
			}
        
			Infragistics.Win.UltraWinGrid.UltraGridRow row = null;

			if ( this.firstChildConnector )
			{
				// get the first child row
				//
				
				row = this.parentRow.GetChild( Infragistics.Win.UltraWinGrid.ChildRow.First, null );
				
				if ( null == row )
					return null;

				// if the first child row is hidden get its next visible
				// sibling row
				//
				if ( row.HiddenResolved )
				{
					row = row.GetNextVisibleRelative( true, true, false );
				}
			}
			else
			{
				// get the next sibling row
				//
				row = this.parentRow.GetNextVisibleRelative( true, true, false );				
			}

			if ( null != row )
				this.rowBelow = row;
				//m_pRowBelow = (CRow*)pSSRow;

			//	In this rare scenario where the user is clicking like mad
			//	on a column header, and then quickly moves the mouse over
			//	the pre-row area, m_pRowBelow was NULL, and we were faulting here
			if ( null != this.rowBelow )
			{
				return Infragistics.Win.UltraWinGrid.TipStyle.Hide != this.rowBelow.Band.TipStyleRowConnectorResolved
					? this.rowBelow 
					: null;
			}
			else
				return null;

			
		}


		/// <summary>
		/// Called when mouse has left the ui element.
		/// </summary>
		protected override void OnMouseLeave()
		{
			// SSP 11/29/01 UWG788
			// If the tool tip is showing, hide it.
			//
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = (Infragistics.Win.UltraWinGrid.UltraGridLayout)this.Parent.GetContext( typeof ( Infragistics.Win.UltraWinGrid.UltraGridLayout ), true );
						
			UltraGrid grid = layout.Grid as UltraGrid;
			if ( null != grid && grid.HasTooltipTool )
			{
				grid.TooltipTool.Hide( );
			}

			base.OnMouseLeave( );
		}

        /// <summary>
        /// Called when the mouse is double clicked on this element.
        /// </summary>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        protected override void OnDoubleClick(bool adjustableArea)
		{
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = (Infragistics.Win.UltraWinGrid.UltraGridLayout)this.Parent.GetContext( typeof ( Infragistics.Win.UltraWinGrid.UltraGridLayout ), true );
			
			Debug.Assert( null != layout, "no layout context" );
			
			// SSP 11/29/01 UWG788
			// Hide the tool tip once the user double clicks on the
			// row connector lines.
			//
			UltraGrid grid = layout.Grid as UltraGrid;
			if ( null != grid && grid.HasTooltipTool )
			{
				grid.TooltipTool.Hide( );
			}

			if ( null != layout && null != layout.Grid )
			{
				UltraGridRow row = this.GetTooltipRow();

				if ( null != row )
				{
					// If the target row is already the active row then
					// instead of calling put_ActiveRow (which won't do anything)
					// we need to call ScrollRowIntoView
					//
					if ( row == layout.ActiveRow )
						layout.ActiveRowScrollRegion.ScrollRowIntoView( row );
					else
						layout.Grid.ActiveRow = row;                    
				}
			}
		}

		

	}
}
