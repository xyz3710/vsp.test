#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;

using Infragistics.Win.CalcEngine;
using System.Collections.Generic;

// SSP 6/29/04 UltraCalc
// Added UltraCalcReferences file.
//

namespace Infragistics.Win.UltraWinGrid
{
	#region FormulaHolder

	internal class FormulaHolder
	{
		#region Private Vars

		private FormulaRefBase calcReference = null;
		private string formula = null;
		private object clientObject = null;

		// SSP 11/9/04
		// Added FormulaValueConverter property.
		//
		private IFormulaValueConverter formulaValueConverter = null;

		#endregion // Private Vars

		#region Constructor

		internal FormulaHolder( object clientObject )
		{
			if ( null == clientObject )
				throw new ArgumentNullException( "clientObject" );

			if ( ! ( clientObject is SummarySettings || clientObject is UltraGridColumn ) )
				throw new ArgumentException( );

			this.clientObject = clientObject;
		}

		#endregion // Constructor

		#region Formula

		internal string Formula
		{
			get
			{
				return this.formula;
			}
			set
			{
				if ( this.formula != value )
				{
					this.formula = value;

					this.ReaddFormulaToCalcNetwork( );
				}
			}
		}

		#endregion // Formula

		#region FormulaValueConverter 

		// SSP 11/9/04
		// Added FormulaValueConverter property.
		//
		internal IFormulaValueConverter FormulaValueConverter 
		{
			get
			{
				return this.formulaValueConverter;
			}
			set
			{
				this.formulaValueConverter = value;
			}
		}

		#endregion // FormulaValueConverter 

		#region HasActiveFormula

		internal bool HasActiveFormula
		{
			get
			{
				if ( null != this.formula && this.formula.Length > 0 && null != this.CalcManager )
				{
					string elemName = this.CalcReference.ElementName;
					return null != elemName && elemName.Length > 0;
				}

				return false;
			}
		}

		#endregion // HasActiveFormula

		#region Layout

		private UltraGridLayout Layout
		{
			get
			{
				if ( this.clientObject is SummarySettings )
					return ((SummarySettings)this.clientObject).Layout;
				else if ( this.clientObject is UltraGridColumn )
					return ((UltraGridColumn)this.clientObject).Layout;

				Debug.Assert( false );
				return null;
			}
		}

		#endregion // Layout

		#region EnsureReferenceNameInitialized

		private string referenceName = null;
		private string normalizedReferenceName = null;
		private int clientObjectIndex = -1;
		private SubObjectsCollectionBase clientObjectCollection = null;

		// SSP 2/4/05 BR02241
		// If the Key of the column or the summary changes, we need to clear the cached 
		// refernece name.
		//
		private void DirtyCachedReferenceName( )
		{
			this.referenceName = null;
		}

		private void EnsureReferenceNameInitialized( )
		{
			// Verify that the object is at the same location.
			//
			if ( this.clientObjectIndex >= 0 
				&& ( this.clientObjectIndex >= this.clientObjectCollection.Count 
				|| this.clientObject != this.clientObjectCollection.GetItem( this.clientObjectIndex ) ) )
				this.referenceName = null;

			if ( null != this.referenceName && this.referenceName.Length > 0 )
				return;

			if ( this.clientObject is SummarySettings )
			{
				SummarySettings summary = (SummarySettings)this.clientObject;
				this.referenceName = summary.Key;

				if ( null == this.referenceName || 0 == this.referenceName.Length )
				{
					int index = summary.ParentCollection.IndexOf( summary );
					if ( index >= 0 )
						this.referenceName = "_Summary_" + index;

					this.clientObjectIndex = index;
					this.clientObjectCollection = summary.ParentCollection;
				}
			}
			else if ( this.clientObject is UltraGridColumn )
			{
				UltraGridColumn column = (UltraGridColumn)this.clientObject;
				this.referenceName = column.Key;

				if ( null == this.referenceName || 0 == this.referenceName.Length )
				{
					int index = column.ParentCollection.IndexOf( column );
					if ( index >= 0 )
						this.referenceName = "_Unbound_" + index;

					this.clientObjectIndex = index;
					this.clientObjectCollection = column.ParentCollection;
				}
			}
			else
				Debug.Assert( false );

			this.referenceName = Infragistics.Win.CalcEngine.RefParser.EscapeString( this.referenceName, false );
			this.normalizedReferenceName = this.referenceName.ToLower( RefUtils.UltraCalcCulture );
		}

		#endregion // EnsureReferenceNameInitialized

		#region ReferenceName

		internal string ReferenceName
		{
			get
			{
				this.EnsureReferenceNameInitialized( );
				return this.referenceName;
			}
		}

		#endregion // ReferenceName

		#region NormalizedCalcReferenceName
		
		internal string NormalizedCalcReferenceName
		{
			get
			{
				this.EnsureReferenceNameInitialized( );
				return this.normalizedReferenceName;
			}
		}

		#endregion // NormalizedCalcReferenceName

		#region CalcManager

		internal IUltraCalcManager CalcManager
		{
			get
			{
				return null != this.Layout ? this.Layout.CalcManager : null;
			}
		}

		#endregion // CalcManager

		#region CalcReference

		internal FormulaRefBase CalcReference
		{
			get
			{
				if ( null == this.calcReference && null != this.Layout )
				{
					if ( this.clientObject is UltraGridColumn )
						this.calcReference = new ColumnReference( (UltraGridColumn)this.clientObject );
					else if ( this.clientObject is SummarySettings )
						this.calcReference = new SummarySettingsReference( (SummarySettings)this.clientObject );
					else
						Debug.Assert( false );
				}

				return this.calcReference;
			}
		}

		#endregion // CalcReference
		
		#region HasValidReference
		
		internal bool HasValidReference
		{
			get
			{
				if ( null != this.CalcReference )
				{
					string elemName = this.CalcReference.ElementName;

					if ( null != elemName && elemName.Length > 0 )
						return true;
				}

				return false;
			}
		}

		#endregion // HasValidReference

		#region CalcManagerNotificationsSuspended
		
		internal bool CalcManagerNotificationsSuspended
		{
			get
			{
				return null == this.Layout || this.Layout.CalcManagerNotificationsSuspended;
			}
		}

		#endregion // CalcManagerNotificationsSuspended

		#region HasCalcReference
		
		internal bool HasCalcReference
		{
			get
			{
				return null != this.calcReference;
			}
		}

		#endregion // HasCalcReference

		#region AddReferenceToCalcNetwork

		internal void AddReferenceToCalcNetwork( )
		{
			this.ReaddReferenceToCalcNetwork( null );
		}

		#endregion // AddReferenceToCalcNetwork

		#region RemoveReferenceFromCalcNetwork
		
		internal void RemoveReferenceFromCalcNetwork( )
		{
			IUltraCalcManager calcManager = this.CalcManager;

			if ( null != calcManager && this.HasValidReference 
				&& ! this.CalcManagerNotificationsSuspended )
			{
				calcManager.RemoveReference( this.CalcReference );

				// SSP 2/4/05 BR02241
				// If the Key of the column or the summary changes, we need to clear the cached 
				// refernece name.
				//
				this.DirtyCachedReferenceName( );
			}
		}

		#endregion // RemoveReferenceFromCalcNetwork

		#region ReaddReferenceToCalcNetwork

		internal void ReaddReferenceToCalcNetwork( IUltraCalcManager oldCalcManager )
		{
			if ( this.HasValidReference && ! this.CalcManagerNotificationsSuspended )
			{
				IUltraCalcManager newCalcManager = this.CalcManager;

				if ( null != oldCalcManager && null != this.CalcReference )
					oldCalcManager.RemoveReference( this.CalcReference );

				// SSP 2/4/05 BR02241
				// If the Key of the column or the summary changes, we need to clear the cached 
				// refernece name.
				//
				this.DirtyCachedReferenceName( );

				if ( null != newCalcManager && null != this.CalcReference )
				{
					newCalcManager.AddReference( this.CalcReference );
					this.ReaddFormulaToCalcNetwork( );
				}
			}
		}

		#endregion // ReaddReferenceToCalcNetwork

		#region ReaddFormulaToCalcNetwork

		internal void ReaddFormulaToCalcNetwork( )
		{
			if ( null != this.CalcManager && this.HasValidReference
				&& ! this.CalcManagerNotificationsSuspended )
			{
				this.CalcReference.RegisterFormula( this.formula );
			}
		}

		#endregion // ReaddFormulaToCalcNetwork
	}

	#endregion // FormulaHolder

	#region UltraGridRefBase Class
	
	internal abstract class UltraGridRefBase : Infragistics.Win.CalcEngine.RefBase
	{
		#region Member variables

		private UltraGridLayout layout = null;
		private object context = null;
		private int verifiedParserInstanceVersion = 0;

		// SSP 1/20/05 BR01804
		// If a formula column is sorted or grouped by we need to make sure it's fully 
		// recalculated before we can sort the row collections based on its values.
		// Added disableRecalcDeferredOverride flag to disable recalc deferred on sort
		// columns.
		//
		internal bool disableRecalcDeferredOverride = false;

		#endregion // Member variables

		#region Constructor
		
		internal UltraGridRefBase( UltraGridLayout layout, object context ) : base( )
		{
			if ( null == layout )
				throw new ArgumentNullException( "layout" );

			this.layout = layout;
			this.context = context;
		}

		#endregion // Constructor
	
		#region ParserInstanceVersion

		// Parsed references on rows, cells, rows collections and nested column references
		// have to be recreated whenever their absolute names change due to a row getting
		// inserted, deleted, the rows collection getting sorted or resynced. For example,
		// When the first row is deleted from a rows collection then the next row's absolute
		// name changes from //ultraGrid1/Customers(1) to //ultraGrid1/Customers(0). However
		// the parsed reference as returned by RefBase.ParsedReference property is still 
		// based on the old absolute name. So we need to recreate the parsed reference.
		// 
		internal virtual int ParserInstanceVersion
		{
			get
			{
				return 0;
			}
		}

		#endregion // ParserInstanceVersion

		#region ParsedReference
		
		public override RefParser ParsedReference
		{ 
			get 
			{
				// If the row indexes have so that the absolute name of the reference would be
				// different than before then recreate the parser instance because the old
				// parser instance was created using the old absolute name.
				//
				if ( this.verifiedParserInstanceVersion != this.ParserInstanceVersion )
				{
					this.ParsedReference = null;
					this.verifiedParserInstanceVersion = this.ParserInstanceVersion;
				}

				return base.ParsedReference;
			}
			set
			{
				base.ParsedReference = value;
			}
		}

		#endregion // ParsedReference

		#region Layout

		internal UltraGridLayout Layout
		{
			get
			{
				return this.layout;
			}
		}

		#endregion // Layout

		#region Grid

		internal UltraGridBase Grid
		{
			get
			{
				return this.Layout.Grid;
			}
		}

		#endregion // Grid

		#region BandContext
		
		internal virtual UltraGridBand BandContext
		{
			get
			{
				if ( this.Context is UltraGridBand )
					return (UltraGridBand)this.Context;
				else if ( null != this.ColumnContext )
					return this.ColumnContext.Band;

				return this.BaseParent is UltraGridRefBase 
					? ((UltraGridRefBase)this.BaseParent).BandContext : null;
			}
		}

		#endregion // BandContext

		#region ColumnContext

		internal virtual UltraGridColumn ColumnContext
		{
			get
			{
				if ( this.Context is UltraGridColumn )
					return (UltraGridColumn)this.Context;

				return this.BaseParent is UltraGridRefBase 
					? ((UltraGridRefBase)this.BaseParent).ColumnContext : null;
			}
		}

		#endregion // ColumnContext

		#region CalcManager
		
		protected IUltraCalcManager CalcManager
		{
			get
			{
				return this.Layout.CalcManager;
			}
		}

		#endregion // CalcManager

		#region InvalidateUIElement
		
		internal virtual void InvalidateUIElement( )
		{
			if ( null != this.Layout )
				this.Layout.DirtyGridElement( );
		}

		#endregion // InvalidateUIElement

		#region NotifyCalcEngineValueChanged
		
		internal void NotifyCalcEngineValueChanged( )
		{
			if ( null != this.CalcManager )
				this.CalcManager.NotifyValueChanged( this );
		}

		#endregion // NotifyCalcEngineValueChanged

		#region RefBase Overrides

		#region FindRoot

		protected override RefBase FindRoot( )
		{
			return this.Layout.CalcReference;
		}

		#endregion // FindRoot

		// SSP 9/16/04
		// Overrode Find methods so they don't have to be overridden on deriving classes where these
		// methods will never be called.
		//

		#region FindItem

		public override IUltraCalcReference FindItem( string name )
		{
			return new UltraCalcReferenceError( name, new NotSupportedException() );
		}

		#endregion // FindItem

		#region FindAll

		public override IUltraCalcReference FindAll( string name )
		{
			return new UltraCalcReferenceError( name, new NotSupportedException() );
		}

		#endregion // FindAll

		#region FindItem

		public override IUltraCalcReference FindItem( string name, string index )
		{
			return new UltraCalcReferenceError( name, new NotSupportedException() );
		}

		#endregion // FindItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name, int index, bool isRelative )
		{
			return new UltraCalcReferenceError( name, new NotSupportedException() );
		}

		#endregion // FindItem

		#region FindSummaryItem

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return new UltraCalcReferenceError( name, new NotSupportedException() );
		}

		#endregion // FindSummaryItem

		#region Context

		public override object Context
		{ 
			get
			{
				return this.context;
			} 
		}

		#endregion // Context

		#region ContainsReferenceHelper

		protected virtual bool ContainsReferenceHelper( IUltraCalcReference inReference, bool isProperSubset )
		{
			RefBase inRef = inReference as RefBase;
			UltraGridRefBase gridRef = RefUtils.GetUnderlyingReference( inRef ) as UltraGridRefBase;
			bool isGridRef = null != gridRef;

			// SSP 12/11/06 BR18268
			// Moved this optimization in RowsCollectionReference.ContainsReferenceHelper override.
			// 
			

			if ( null != inRef && isGridRef )
			{
				// SSP 9/17/04 - Optmizations
				//
				if ( inRef is CellReference && this is ColumnReference )
					return ((CellReference)inRef).Cell.Column == ((ColumnReference)this).Column;

				// SSP 9/12/04
				// Multiple LHS for a single summary related.
				//
				if ( this is SummarySettingsReference && inRef is SummarySettingsReference )
					return string.Equals( this.NormalizedAbsoluteName, inRef.NormalizedAbsoluteName );

				RefParser thisRP = this.ParsedReference;
				RefParser testRP = inRef.ParsedReference;

				if ( ! thisRP.IsFullyQualified && ! thisRP.IsRoot 
					|| ! testRP.IsFullyQualified && ! testRP.IsRoot
					|| thisRP.TupleCount <= 0 || testRP.TupleCount <= 0 )
					return false;
				
				bool rootTuplesMatch = isProperSubset 
						? thisRP[0].IsSubset( testRP[0] ) 
						: thisRP[0].Contains( testRP[0] );

				if ( ! rootTuplesMatch )
					return false;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList list1 = RefUtils.InsertGroupByColumnTuples( this.Layout, thisRP );
				//ArrayList list2 = RefUtils.InsertGroupByColumnTuples( this.Layout, testRP );
				List<RefTuple> list1 = RefUtils.InsertGroupByColumnTuples( this.Layout, thisRP );
				List<RefTuple> list2 = RefUtils.InsertGroupByColumnTuples( this.Layout, testRP );

				if ( null != list1 && null != list2 && list1.Count <= list2.Count )
				{
					bool allTuplesMatch = true;

					for ( int i = 0; allTuplesMatch && i < list1.Count; i++ )
						allTuplesMatch = isProperSubset
							// MD 8/10/07 - 7.3 Performance
							// Use generics
							//? ((RefTuple)list1[i]).IsSubset( (RefTuple)list2[i] )
							//: ((RefTuple)list1[i]).Contains( (RefTuple)list2[i] );
							? list1[ i ].IsSubset( list2[ i ] )
							: list1[ i ].Contains( list2[ i ] );

					return allTuplesMatch;
				}
			}

			return base.ContainsReference( inReference );
		}

		#endregion // ContainsReferenceHelper

		// SSP 9/3/04
		// Overrode ContainsReference so we can take into account the group-by tuples.
		//
		#region ContainsReference

		public override bool ContainsReference( IUltraCalcReference inReference )
		{
			return this.ContainsReferenceHelper( inReference, false );
		}

		#endregion // ContainsReference

		// SSP 9/8/04
		// Overrode IsSubsetReference so we can take into account the group-by tuples.
		//
		#region IsSubsetReference

		/// <summary>
		/// Returns true if inReference is a proper subset of this reference
		/// </summary>
		/// <param name="inReference">The subset candidate.</param>
		/// <returns>true if inReference is contained by this reference.</returns>
		public override bool IsSubsetReference( IUltraCalcReference inReference )
		{
			return this.ContainsReferenceHelper( inReference, true );
		}

		#endregion // IsSubsetReference

		#endregion // RefBase Overrides

		#region FilterReference
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal IUltraCalcReference FilterReference( IUltraCalcReference result )
		internal static IUltraCalcReference FilterReference( IUltraCalcReference result )
		{
			NestedReference reference = RefUtils.GetUnderlyingReference(  result ) as NestedReference;
			RefBase newReference = null;

			if ( null != reference && reference.Rows.IsRootRowsCollection )
			{
				if ( reference is NestedColumnReference )
					newReference = RefUtils.GetColumnReference( ((NestedColumnReference)reference).Column );
				else if ( reference is NestedBandReference )
					newReference = RefUtils.GetBandReference( ((NestedBandReference)reference).Band );
				else if ( reference is NestedSummarySettingsReference )
					newReference = ((NestedSummarySettingsReference)reference).SummaryReference;
			}
			else
			{
				SummaryValueReference summaryValueRef = RefUtils.GetUnderlyingReference( result ) as SummaryValueReference;
				if ( null != summaryValueRef && summaryValueRef.SummaryValue.ParentRows.IsRootRowsCollection )
					newReference = RefUtils.GetSummarySettingsReference( summaryValueRef.SummaryValue.SummarySettings );
			}

			if ( null != newReference && newReference != reference )
			{
				if ( result is RefUnAnchored )
				{
					RefUnAnchored ru = new RefUnAnchored( newReference );
					ru.RelativeReference = ((RefUnAnchored)result).RelativeReference;
					newReference = ru;
				}

				return newReference;
			}

			return result;
		}

		#endregion // FilterReference

		#region CreateReference
		
		public override IUltraCalcReference CreateReference( string referenceName )
		{
			// SSP 9/13/04
			// Read the comment above FilterReference for more info.
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//return this.FilterReference( base.CreateReference( referenceName ) );
			return UltraGridRefBase.FilterReference( base.CreateReference( referenceName ) );
		}

		#endregion // CreateReference

		#region ResolveReference

		// SSP 9/11/04
		// Overrode ResolveReference.
		//
		public override IUltraCalcReference ResolveReference( IUltraCalcReference inReference, ResolveReferenceType referenceType )
		{
			// If the reference being resolved is a SummarySettingsReference or GroupLevelSummarySettingsReference
			// or a NestedSummarySettingsReference (in other words a summary settings), then just return
			// the passed in reference in the case this is a column or a band. Also if this is a rows collection
			// then return the nested summary reference for the passed in summary reference.
			//
			if ( inReference is SummarySettingsReference || inReference is NestedSummarySettingsReference )
			{
				// SSP 9/18/04
				//
				//if ( this is ColumnReference || this is BandReference )
				if ( this is ColumnReference || this is BandReference || this is SummarySettingsReference )
					return inReference;

				// If we can get more specific.
				//
				if ( this is NestedReference && inReference is SummarySettingsReference )
				{
					RowsCollection rows = ((NestedReference)this).Rows;

                    // MRS 10/10/2008 - TFS8793
                    // Don't do this if the reference are not from the same grid. 
                    //                    
                    //if ( ! rows.Band.IsDescendantOfBand( ((SummarySettingsReference)inReference).Summary.Band ) )
                    //  return RefUtils.GetNestedSummarySettingsReference(rows, (SummarySettingsReference)inReference);
                    UltraGridBand thisBand = rows.Band;
                    SummarySettingsReference summarySettingsReference = (SummarySettingsReference)inReference;
                    UltraGridBand summaryBand = summarySettingsReference.Summary.Band;
                    if (thisBand.Layout == summaryBand.Layout &&
                        !thisBand.IsDescendantOfBand(summaryBand))
                    {
                        return RefUtils.GetNestedSummarySettingsReference(rows, summarySettingsReference);
                    }

                    return inReference;
				}
			}

			// SSP 9/13/04
			// Read the comment above FilterReference for more info.
			//
			//return base.ResolveReference( inReference );
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//return this.FilterReference( base.ResolveReference( inReference, referenceType ) );
			return UltraGridRefBase.FilterReference( base.ResolveReference( inReference, referenceType ) );
		}

		#endregion // ResolveReference

		#region CreateRange

		// SSP 9/16/04
		// Overrode CreateRange because the base class implementation (off RefBase) uses RefRange
		// class which doesn't know anything about group-by tupes so it doesn't work when you
		// have group-by on. Ranges like [Col(0)]..[Col] fail because from-reference is a cell 
		// reference and includes group-by tuples whereas to-reference is a column reference and 
		// doesn't include group-by tuples. As far as the RefRange is concerned, all the tuples
		// upto the last tuple have to match. To fix this created RangeReference class.
		//

		private RefTuple GetLastRangeRefTuple( RefParser rp, out string errorMessage )
		{
			errorMessage = null;
			RefTuple lastTuple = new RefTuple( rp.LastTuple );
			
			RefTuple nextToLast = rp[ rp.TupleCount - 2 ];

			if ( RefTuple.RefScope.Any == lastTuple.Scope )
			{
				lastTuple.Scope = nextToLast.Scope;
				lastTuple.ScopeIndex = nextToLast.ScopeIndex;
			}
			
			// Ranges only work with no scope like in [Col] in [Col(0)]..[Col]
			// or All scope like in [Col(0)]..[Col(*)] which means from 0th row to the last row
			// or a relative index or a specific index like in [Col(0)]..[Col(-1)] respectively.
			// 
			if ( RefTuple.RefScope.Any == lastTuple.Scope 
				|| RefTuple.RefScope.All == lastTuple.Scope 
				|| RefTuple.RefScope.Index == lastTuple.Scope 
				// Relative indexes on range references only make sense in column formulas since
				// there is a cell context. On a summary saying [Col(-1)] doesn't make sense 
				// since summary doesn't have a context of a row (except the parent row which
				// would be either a group-by row or from a different band.
				//
				|| RefTuple.RefScope.RelativeIndex == lastTuple.Scope && this is ColumnReference )
				return lastTuple;

			errorMessage = "Invalid scope.";

			// If there is an invalid tuple, then return null to indicate invalid range
			// reference specification.
			//
			return null;
		}

		public override IUltraCalcReference CreateRange( string fromReferenceName, string toReferenceName )
		{
			IUltraCalcReference createdFromRef = this.CreateReference( fromReferenceName );

			if ( createdFromRef is UltraCalcReferenceError )
				return createdFromRef;

			IUltraCalcReference createdToRef = this.CreateReference( toReferenceName );
		
			if ( createdToRef is UltraCalcReferenceError )
				return createdToRef;

			UltraGridRefBase fromRef = RefUtils.GetUnderlyingReference( createdFromRef ) as UltraGridRefBase;
			RefParser fromRefRP = createdFromRef is RefUnAnchored 
				? ((RefUnAnchored)createdFromRef).ParsedReference : fromRef.ParsedReference;

			UltraGridRefBase toRef = RefUtils.GetUnderlyingReference( createdToRef ) as UltraGridRefBase;
			RefParser toRefRP = createdToRef is RefUnAnchored 
				? ((RefUnAnchored)createdToRef).ParsedReference : toRef.ParsedReference;

			// If the reference were not grid references then call the base implementation 
			// since we wouldn't know how to handle reference from something other than a grid.
			//
			if ( null == fromRef || null == toRef || null == fromRefRP || null == toRefRP
				|| fromRefRP.TupleCount < 2 || toRefRP.TupleCount < 2 )
				return base.CreateRange( fromReferenceName, toReferenceName );

			string errorMessage = null;

			if ( null == fromRef.ColumnContext || null == toRef.ColumnContext )
				errorMessage = "From or to reference does not specify a column.";
			else if ( fromRef.ColumnContext != toRef.ColumnContext )
				errorMessage = "From and to references in the range reference are not the same columns.";

			RefTuple fromLastTuple = null;
			RefTuple toLastTuple   = null;

			if ( null == errorMessage )
				fromLastTuple = this.GetLastRangeRefTuple( fromRefRP, out errorMessage );

			if ( null == errorMessage )
				toLastTuple   = this.GetLastRangeRefTuple( toRefRP, out errorMessage   );

			string rangeReferenceName = fromReferenceName + RefUtils.RANGE_REFERENCE_SEPARATOR + toReferenceName;
			if ( null != errorMessage || null == fromLastTuple || null == toLastTuple )
				return new UltraCalcReferenceError( rangeReferenceName, errorMessage );

			return new RangeReference( fromRef.ColumnContext, fromRefRP, toRefRP, fromLastTuple, toLastTuple );
		}

		#endregion // CreateRange

		// SSP 9/30/04
		// Commented out the original code and added the new code below.
		//
		#region IsSiblingReference

		public override bool IsSiblingReference( IUltraCalcReference reference )
		{
			UltraGridRefBase ref1 = RefUtils.GetUnderlyingReference( reference ) as UltraGridRefBase;
			UltraGridRefBase ref2 = this;

			if ( ref1 is NestedColumnReference && ((NestedColumnReference)ref1).Rows.IsRootRowsCollection )
				ref1 = ((NestedColumnReference)ref1).Column.CalcReference;

			if ( ref2 is NestedColumnReference && ((NestedColumnReference)ref2).Rows.IsRootRowsCollection )
				ref2 = ((NestedColumnReference)ref2).Column.CalcReference;

			if ( ref1 is ColumnReference && ref2 is ColumnReference )
				return ((ColumnReference)ref1).BandContext == ((ColumnReference)ref2).BandContext;

			return false;
		}

		#endregion // IsSiblingReference

		#region RecalcDeferred

		// SSP 1/17/05 BR01753
		// Print and export layout won't calculate formulas but rather they will copy over 
		// the calculated values from the display layout. We need to turn off the deferred
		// calculations while printing so that all of the cells, visible or otherwise get
		// calculated.
		//
		public override bool RecalcDeferred
		{
			get
			{
				// Turn off recalc deferred while printing so.
				//
				return base.RecalcDeferred && ! this.Layout.CalcReference.recalcDeferredSuspended
					// SSP 1/20/05 BR01804
					// If a formula column is sorted or grouped by we need to make sure it's fully 
					// recalculated before we can sort the row collections based on its values.
					// Added disableRecalcDeferredOverride flag to disable recalc deferred on sort
					// columns.
					//
					&& ! this.disableRecalcDeferredOverride;
			}
			set
			{
				// SSP 1/20/05 BR01804
				// If a formula column is sorted or grouped by we need to make sure it's fully 
				// recalculated before we can sort the row collections based on its values.
				// Added disableRecalcDeferredOverride flag to disable recalc deferred on sort
				// columns.
				//
                base.RecalcDeferred = value && ! this.disableRecalcDeferredOverride;
			}
		}

		internal bool RecalcDeferredBase
		{
			get
			{
				return base.RecalcDeferred;
			}
		}

		#endregion // RecalcDeferred
	}

	#endregion // UltraGridRefBase Class

	#region GridReference Class

	internal class GridReference : UltraGridRefBase
	{
		#region Private Variables

		private string gridName = null;
		
		// SSP 1/18/05 BR01753
		//
		internal bool recalcDeferredSuspended = false;

		#endregion Private Variables

		#region GridReference

		internal GridReference( UltraGridLayout layout ) : base( layout, layout.Grid )
		{
			// SSP 11/15/04 - UWC159
			// Use the Name off the ISite instead of the Name property of the control.
			//
			//this.gridName = layout.Grid.Name;
			ISite site = layout.Grid.Site;
			this.gridName = null == site ? layout.Grid.Name : site.Name;

			
			//
			if ( null == this.gridName )
				this.gridName = "";

			this.gridName = Infragistics.Win.CalcEngine.RefParser.EscapeString( this.gridName, false );
		}

		#endregion // GridReference
		
		#region RefBase Overrides

		public override string ElementName 
		{
			get 
			{
				return this.gridName;
			}
		}

		public override RefBase BaseParent
		{ 
			get 
			{
				// SSP 9/13/04
				// Return null instead of throwing an exception.
				//
				//throw new InvalidOperationException( SR.GetString( "LER_Calc_NoParentReference", this.ElementName ) );
				return null;
			}
		}

		public override IUltraCalcReference FindItem( string name )
		{
			return RefUtils.GetBandRowsReference(this.Layout, name, this.Layout.Rows);
		}

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return new UltraCalcReferenceError(name, SR.GetString( "LER_Calc_RootBandNameExpected", this.Layout.CalcReference.ElementName, name ) );
		}

		public override IUltraCalcReference FindItem( string name, string index )
		{
			// Call GetBand to make sure the name is the name of the first band. 
			// GetBand will throw an exception if name is not the name of the 
			// root band.
			// 
			IUltraCalcReference bandRef = RefUtils.GetBandReference(this.Layout, null, name);

			if ( !(bandRef is BandReference) )
				return bandRef;

			return RefUtils.FindRowReference(this.Layout.Rows, ((BandReference)bandRef).Band, index, name);
		}

		public override IUltraCalcReference FindItem( string name, int index, bool isRelative )
		{
			// Call GetBand to make sure the name is the name of the first band. 
			// GetBand will throw an exception if name is not the name of the 
			// root band.
			// 
			IUltraCalcReference bandRef = RefUtils.GetBandReference( this.Layout, null ,name );

			if ( !(bandRef is BandReference) )
				return bandRef;

			if ( isRelative ) 
				return new UltraCalcReferenceError( name, SR.GetString( "LER_Calc_RelativeIndexInvalid" ) );

			return RefUtils.GetRowReference(this.Layout.Rows, index, name);
		}

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItem( name );
		}

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get 
			{ 
				return null == this.Layout || this.Layout.Disposed; 
			}
		}

		#endregion // IsDisposedReference

		public override string AbsoluteName
		{
			get 
			{
				return RefParser.RefFullyQualifiedString + this.ElementName;
			}
		}

		private IUltraCalcReference BuildReferenceHelper( RefParser newRef, bool forceDataRef, out bool returnedRefFromThisGrid )
		{
			returnedRefFromThisGrid = false;
			IEnumerator tupleEnum = newRef.GetEnumerator( );

			// If fully qualified, advance past the control name. Make sure it's this control.
			// If not, pass it off to the right control
			//
			if ( newRef.IsFullyQualified ) 
			{
				if ( ! tupleEnum.MoveNext( ) ) 
					return new UltraCalcReferenceError( newRef.ToString( ), SR.GetString( "LER_Calc_InvalidReference", newRef.ToString( ) ) );

				RefTuple tuple = (RefTuple)tupleEnum.Current;
				if ( tuple.Type != RefTuple.RefType.Identifier )
					return new UltraCalcReferenceError( newRef.ToString( ), SR.GetString( "LER_Calc_InvalidReference", newRef.ToString( ) ) );

				if ( tuple.Scope != RefTuple.RefScope.Any ) 
					return new UltraCalcReferenceError( newRef.ToString( ), SR.GetString( "LER_Calc_InvalidReference", newRef.ToString( ) ) );

				if ( 0 != String.Compare( this.ElementName, tuple.Name, true ) ) 
					return (RefBase)this.CalcManager.GetReference( newRef.ToString( ) );
			}

			returnedRefFromThisGrid = true;
			return RefLoop( this, tupleEnum, forceDataRef );
		}

		protected override IUltraCalcReference BuildReference( RefParser newRef, bool forceDataRef )
		{
			bool refFromThisGrid;
			return this.BuildReferenceHelper( newRef, forceDataRef, out refFromThisGrid );
		}

		public override IUltraCalcReference CreateReference( string reference )
		{
			try 
			{
				RefParser rp = new RefParser( reference );
				if ( 0 == rp.TupleCount ) 
					return new UltraCalcReferenceError( reference, SR.GetString( "LER_Calc_InvalidReference", reference ) );

				bool refFromThisGrid;
				IUltraCalcReference refLoopResult = this.BuildReferenceHelper( rp, false, out refFromThisGrid );

				if ( ! refFromThisGrid || refLoopResult is UltraCalcReferenceError || ! ( refLoopResult is RefBase ) )
					return refLoopResult;

				RefBase result = (RefBase)refLoopResult;
				if ( result.IsAnchored ) 
					result = new RefUnAnchored( result );

				result.RelativeReference = rp;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//return this.FilterReference( result );
				return UltraGridRefBase.FilterReference( result );
			}
			catch( Exception e ) 
			{
				return new UltraCalcReferenceError( reference, e.Message );
			}
		}

		#region GetChildReferences

		// SSP 9/7/04
		// Added GetChildReferences method to IUltraCalcReference interface.
		//
		public override IUltraCalcReference[] GetChildReferences( ChildReferenceType referenceType )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<IUltraCalcReference> list = new List<IUltraCalcReference>();

			if ( null != this.Layout && ! this.IsDisposedReference && null != this.Layout.SortedBands )
			{
				foreach ( UltraGridBand band in this.Layout.SortedBands )
				{
					if ( ! band.Disposed && null != band.CalcReference )
						list.Add( band.CalcReference );
				}
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return list.Count > 0 ? (IUltraCalcReference[])list.ToArray( typeof( IUltraCalcReference ) ) : null;
			return list.Count > 0 ? list.ToArray() : null;
		}

		#endregion // GetChildReferences

		#endregion // RefBase Overrides
	}

	#endregion // GridReference Class

	#region BandReference Class

	internal class BandReference : UltraGridRefBase
	{
		#region Private Variables

		private UltraGridBand band = null;

        // MRS 12/7/06 - BR18268
        private Hashtable columnsByNormalizedCalcReferenceName = null;
        private int verifiedColumnsVersion = -1;

		#endregion // Private Variables

		#region Constructor

		internal BandReference( UltraGridBand band ) : base( band.Layout, band )
		{
			this.band = band;
		}

		#endregion // Constructor

		#region Band
		
		internal UltraGridBand Band
		{
			get
			{
				return this.band;
			}
		}

		#endregion // Band

        // MRS 12/7/06 - BR18268
        #region ColumnsByNormalizedCalcReferenceName
        private Hashtable ColumnsByNormalizedCalcReferenceName
        {
            get
            {
                if (this.columnsByNormalizedCalcReferenceName == null ||
                    this.verifiedColumnsVersion != this.band.ColumnsVersion)
                {
                    this.columnsByNormalizedCalcReferenceName = new Hashtable();
                    foreach (UltraGridColumn column in this.band.Columns)
                    {
                        string normalizedCalcReferenceName = column.FormulaHolder.NormalizedCalcReferenceName;
                        this.columnsByNormalizedCalcReferenceName[normalizedCalcReferenceName] = column;
                        this.verifiedColumnsVersion = this.band.ColumnsVersion;
                    }
                }

                return this.columnsByNormalizedCalcReferenceName;
            }
        }
        #endregion ColumnsByNormalizedCalcReferenceName

        // MRS 12/7/06 - BR18268
        #region GetColumnFromNormalizedCalcReferenceName
        internal UltraGridColumn GetColumnFromNormalizedCalcReferenceName(string normalizedCalcReferenceName)
        {
            return this.ColumnsByNormalizedCalcReferenceName[normalizedCalcReferenceName] as UltraGridColumn;
        }
        #endregion GetColumnFromNormalizedCalcReferenceName

		// SSP 9/9/04
		//
		#region VerifyGroupLevelSummaryFormulas

		internal int verifiedSummariesVersion = -1;
		private int verifiedGroupByHierarchyVersion = -1;

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private ArrayList groupLevelRelatedReferences = null;
		private List<GroupLevelSummarySettingsReference> groupLevelRelatedReferences = null;

		// SSP 3/30/06 BR11065
		// 
		#region Bug fix for BR11065

		private int suspendVerifyGroupLevelSummaryFormulas_Counter = 0;
		internal void SuspendVerifyGroupLevelSummaryFormulas( )
		{
			this.suspendVerifyGroupLevelSummaryFormulas_Counter++;
		}

		private bool IsVerifyGroupLevelSummaryFormulasSuspended
		{
			get
			{
				return this.suspendVerifyGroupLevelSummaryFormulas_Counter > 0;
			}
		}

		internal void ResumeVerifyGroupLevelSummaryFormulas( bool verifyOnResume )
		{
			if ( this.IsVerifyGroupLevelSummaryFormulasSuspended )
			{
				this.suspendVerifyGroupLevelSummaryFormulas_Counter--;

				if ( ! this.IsVerifyGroupLevelSummaryFormulasSuspended && verifyOnResume )
					this.VerifyGroupLevelSummaryFormulas( );
			}
		}

		#endregion // Bug fix for BR11065

		internal void VerifyGroupLevelSummaryFormulas( )
		{
			// SSP 2/16/05 BR01753
			// Create the group-by level and group-by summary references regardless of
			// whether the calc is suspended or there is no calc manager. Otherwise when a
			// summary's CalcReference is being created, it won't be able to find
			// appropriate group-by level reference (if group-by hierarchy have changed
			// since suspend, a common situation when printing with group-by columns) and
			// will end up throwing an exception in the constructor of the summary value
			// reference.
			//
			//if ( this.verifiedGroupByHierarchyVersion == this.Band.GroupByHierarchyVersion
			//	|| this.Layout.CalcManagerNotificationsSuspended || null == this.CalcManager )
			//	return;
			// SSP 5/26/05 BR04280
			// We also need to recreate the group-by level summary references if the summaries 
			// are added or removed.
			//
			// --------------------------------------------------------------------------------
			
			int groupByVersion   = this.Band.GroupByHierarchyVersion;
			int summariesVersion = this.Band.HasSummaries ? this.Band.Summaries.SummariesVersion : 0;

			if ( this.verifiedGroupByHierarchyVersion == groupByVersion 
				&& this.verifiedSummariesVersion == summariesVersion )
				return;

			// SSP 3/30/06 BR11065
			// 
			if ( this.IsVerifyGroupLevelSummaryFormulasSuspended )
				return;

			// SSP 6/13/05 BR01753
			// Don't cause the other summaries to be recalculated when a summary is added.
			// 
			bool reuseReferences = this.verifiedGroupByHierarchyVersion == groupByVersion;

			this.verifiedGroupByHierarchyVersion = groupByVersion;
			this.verifiedSummariesVersion = summariesVersion;			
			// --------------------------------------------------------------------------------

			IUltraCalcManager calcManager = this.CalcManager;

			// SSP 2/16/05 BR01753
			// Related to the change above regarding creating the group-by level references
			// regardless of whether the calc is suspended.
			//
			bool isSuspended = null == calcManager || this.Layout.CalcManagerNotificationsSuspended;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList reuseReferencesList = new ArrayList( );
			List<GroupLevelSummarySettingsReference> reuseReferencesList = new List<GroupLevelSummarySettingsReference>();

			if ( null != this.groupLevelRelatedReferences )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList list = this.groupLevelRelatedReferences;
				List<GroupLevelSummarySettingsReference> list = this.groupLevelRelatedReferences;

				this.groupLevelRelatedReferences = null;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//foreach ( UltraGridRefBase reference in list )
				//{
				//    GroupLevelSummarySettingsReference glsr = reference as GroupLevelSummarySettingsReference;
				foreach ( GroupLevelSummarySettingsReference glsr in list )
				{
					if ( null != glsr )
					{
						// SSP 6/13/05 BR01753
						// Don't cause the other summaries to be recalculated when a summary is added.
						// 
						// ------------------------------------------------------------------------------
						if ( reuseReferences && this.Band.Summaries.Contains( glsr.Summary ) )
						{
							string oldFormula = null != glsr.Formula ? glsr.Formula.FormulaString : string.Empty;
							string newFormula = null != glsr.Summary.Formula ? glsr.Summary.Formula : string.Empty;
							if ( oldFormula == newFormula )
							{
								reuseReferencesList.Add( glsr );
								continue;
							}
						}
						// ------------------------------------------------------------------------------

						if ( null != glsr.Formula )
						{
							// SSP 2/4/05
							// Use the AddFormulaHelper and RemoveFormulaHelper utility methods 
							// instead. See comments above those method definitions for more info.
							//
							//calcManager.RemoveFormula( glsr.Formula );
							// SSP 2/16/05 BR01753
							// Only remove if not suspended.
							//
							if ( ! isSuspended )
								RefUtils.RemoveFormulaHelper( calcManager, glsr.Formula );

							// SSP 6/10/05 
							// Moved this below after the this if block because we need to set disposed
							// on the reference regardless of whether it had any formulas.
							// 
							//glsr.InternalSetIsDisposed( true );
						}

						// SSP 6/10/05 
						// Moved this here from above if block because we need to set disposed
						// on the reference regardless of whether it had any formulas.
						// 
						glsr.InternalSetIsDisposed( true );
					}
					
					// SSP 2/16/05 BR01753
					// Only remove if not suspended.
					//
					if ( ! isSuspended )
					{
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//calcManager.RemoveReference( reference );
						calcManager.RemoveReference( glsr );
					}
				}
			}

			if ( band.HasGroupBySortColumns )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//this.groupLevelRelatedReferences = new ArrayList( );
				this.groupLevelRelatedReferences = new List<GroupLevelSummarySettingsReference>();

				foreach ( SummarySettings summary in band.Summaries )
				{
					// SSP 2/16/05 BR01753
					// Create the summary reference regardless of whether it has a formula.
					//
					//if ( summary.HasActiveFormula )
					//{
					UltraGridRefBase parentReference = this;
					for ( int i = 0; i < band.SortedColumns.Count; i++ )
					{
						UltraGridColumn groupByColumn = band.SortedColumns[ i ];

						// Break out once we encounter a sort column that's not a group-by column
						// since the sorted columns collection always maintains group-by sort 
						// columns before the non-group-by sort columns.
						//
						if ( ! groupByColumn.IsGroupByColumn )
							break;

						GroupLevelSummarySettingsReference reference = null;

						// SSP 6/13/05 BR01753
						// Don't cause the other summaries to be recalculated when a summary is added.
						// 
						// ------------------------------------------------------------------------------
						if ( reuseReferences && null != reuseReferencesList )
						{
							for ( int j = 0; j < reuseReferencesList.Count; j++ )
							{
								// MD 8/10/07 - 7.3 Performance
								// Use generics
								//GroupLevelSummarySettingsReference glsr = reuseReferencesList[j] as GroupLevelSummarySettingsReference;
								GroupLevelSummarySettingsReference glsr = reuseReferencesList[ j ];

								if ( null != glsr && glsr.Summary == summary && glsr.GroupByColumn == groupByColumn )
								{
									reuseReferencesList[j] = null;
									reference = glsr;
									break;
								}
							}
						}
						// ------------------------------------------------------------------------------

						if ( null == reference )
						{
							parentReference = new GroupLevelReference( parentReference, groupByColumn );
							reference = new GroupLevelSummarySettingsReference( 
								(GroupLevelReference)parentReference, summary );
						}

						this.groupLevelRelatedReferences.Add( reference );
					}
					//}
				}

				// SSP 2/16/05 BR01753
				// Enclosed the existing code into the if block.
				//
				if ( ! isSuspended )
				{
					foreach ( object item in this.groupLevelRelatedReferences )
					{
						GroupLevelSummarySettingsReference reference = item as GroupLevelSummarySettingsReference;
						if ( null != reference )
						{
							calcManager.AddReference( reference );
							reference.EnsureFormulaRegistered( reference.Summary.Formula );
						}
					}
				}

				if ( this.groupLevelRelatedReferences.Count <= 0 )
					this.groupLevelRelatedReferences = null;
			}
		}

		#endregion // VerifyGroupLevelSummaryFormulas

		#region GetSummarySettingsReference

		// SSP 6/13/05 BR04539
		// Added GetSummarySettingsReference overload that takes in summaryValue.
		// 
		internal SummarySettingsReference GetSummarySettingsReference( SummaryValue summaryValue )
		{
			UltraGridGroupByRow groupByRow = summaryValue.ParentRows.ParentRow as UltraGridGroupByRow;
			UltraGridColumn groupByColumn = null != groupByRow ? groupByRow.Column : null;
			return this.GetSummarySettingsReference( summaryValue.SummarySettings, groupByColumn );
		}

		// SSP 9/12/04
		//		
		internal SummarySettingsReference GetSummarySettingsReference( SummarySettings summary, UltraGridColumn groupByColumn )
		{
			this.VerifyGroupLevelSummaryFormulas( );

			if ( null != this.groupLevelRelatedReferences )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//foreach ( UltraGridRefBase reference in this.groupLevelRelatedReferences )
				//{
				//    if ( reference is GroupLevelSummarySettingsReference 
				//        // SSP 6/10/05 BR04539
				//        // Added below condition that checks to see if the the summary is the same.
				//        // 
				//        && ((GroupLevelSummarySettingsReference)reference).Summary == summary
				//        && groupByColumn == ((GroupLevelSummarySettingsReference)reference).GroupByColumn )
				//        return (SummarySettingsReference)reference;
				//}
				foreach ( GroupLevelSummarySettingsReference reference in this.groupLevelRelatedReferences )
				{
					// SSP 6/10/05 BR04539
					// Added below condition that checks to see if the the summary is the same.
					// 
					if ( reference.Summary == summary &&
						groupByColumn == reference.GroupByColumn )
					{
						return (SummarySettingsReference)reference;
					}
				}
			}

			if ( null == groupByColumn )
				return RefUtils.GetSummarySettingsReference( summary );

			return null;
		}

		#endregion // GetSummarySettingsReference

		#region RefBase Overrides

		#region ElementName

		public override string ElementName 
		{
			get 
			{
				return this.Band.CalcReferenceName;
			}
		}

		#endregion // ElementName

		#region Parent

		public override RefBase BaseParent
		{ 
			get 
			{
				return null != this.Band.ParentBand
					? (RefBase)this.Band.ParentBand.CalcReference
					: (RefBase)this.Layout.CalcReference;
			}
		}

		#endregion // Parent

		#region FindItem

		public override IUltraCalcReference FindItem( string name )
		{
			UltraGridColumn column = RefUtils.GetColumn( this.Band, name, false );

			if (column == null)
				return RefUtils.CreateMissingColumnReference(this.Band, name);

			if ( column.IsChaptered )
			{
				return RefUtils.GetBandReference(this.Layout, name);
			} 
			else 
			{
				return RefUtils.GetColumnReference( column );
			}
		}

		#endregion // FindItem

		#region FindSummaryItem

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return RefUtils.GetSummarySettingsReference( this.Band, name );
		}

		#endregion // FindSummaryItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name, string index )
		{
			return this.FindItem( name );
		}

		#endregion // FindItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name, int index, bool isRelative )
		{
			return this.FindItem( name );
		}

		#endregion // FindItem

		#region FindAll

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItem( name );
		}

		#endregion // FindAll

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get 
			{ 
				return null == this.band || ! this.band.IsStillValid; 
			}
		}

		#endregion // IsDisposedReference

		#region GetChildReferences

		// SSP 9/7/04
		// Added GetChildReferences method to IUltraCalcReference interface.
		//
		public override IUltraCalcReference[] GetChildReferences( ChildReferenceType referenceType )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<IUltraCalcReference> list = new List<IUltraCalcReference>();

			if ( null != this.Layout && null != this.Band && ! this.IsDisposedReference )
			{
				foreach ( UltraGridColumn column in this.Band.Columns )
				{
					// Note the check for column.CalcReference being null. This was intentional. We
					// want to create calc references for all the columns regardless of whether they
					// have already been allocated or not. Only time CalcReference will return null 
					// is when the column is disconnected from a layout and calc-reference was never
					// allocated for it. When its connected to a layout, it will create a reference on
					// fly and return it.
					//
					if ( ! column.Disposed && null != column.CalcReference )
						list.Add( column.CalcReference );
				}

				if ( this.Band.HasSummaries )
				{
					foreach ( SummarySettings summary in this.Band.Summaries )
					{
						// Note the check for summary.CalcReference being null. This was intentional. We
						// want to create calc references for all the summaries regardless of whether they
						// have already been allocated or not. Only time CalcReference will return null 
						// is when the summary is disconnected from a layout and calc-reference was never
						// allocated for it. When its connected to a layout, it will create a reference on
						// fly and return it.
						//
						if ( ! summary.Disposed && null != summary.CalcReference )
							list.Add( summary.CalcReference );
					}
				}
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return list.Count > 0 ? (IUltraCalcReference[])list.ToArray( typeof( IUltraCalcReference ) ) : null;
			return list.Count > 0 ? list.ToArray() : null;
		}

		#endregion // GetChildReferences

		// SSP 10/12/04
		// Overrode GetHashCode, Equals, ContainsReference and IsSubsetReference methods.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			return this.band.GetHashCode( );
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored or NestedBandReference then delegate the call to it.
			//
			if ( obj is RefUnAnchored || obj is NestedBandReference )
				return obj.Equals( this );

			BandReference bandRef = obj as BandReference;
			return null != bandRef && this.band == bandRef.band;
		}

		#endregion // Equals

		#endregion // RefBase Overrides
	}

	#endregion // BandReference Class

	#region RowsCollectionReference Class
	
	internal class RowsCollectionReference : UltraGridRefBase
	{
		#region Private Variables

		private RowsCollection rows = null;

		// Parsed references on rows, cells, rows collections and nested column references
		// have to be recreated whenever their absolute names change due to a row getting
		// inserted, deleted, the rows collection getting sorted or resynced. For example,
		// When the first row is deleted from a rows collection then the next row's absolute
		// name changes from //ultraGrid1/Customers(1) to //ultraGrid1/Customers(0). However
		// the parsed reference as returned by RefBase.ParsedReference property is still 
		// based on the old absolute name. So we need to recreate the parsed reference.
		// 
		private int parserInstanceVersion = 0;

		internal int resyncEventCounter = 0;

		// SSP 9/21/06 BR16000 - Optmization
		// 
		internal int resyncEventSuspended = 0;

		#endregion // Private Variables

		#region Constructor

		internal RowsCollectionReference( RowsCollection rows ) : base( rows.Layout, rows )
		{
			this.rows = rows;
		}

		#endregion // Constructor

		#region BumpParserInstanceVersion

		internal void BumpParserInstanceVersion( )
		{
			this.parserInstanceVersion++;
		}

		#endregion // BumpParserInstanceVersion

		#region ParserInstanceVersion

		// Parsed references on rows, cells, rows collections and nested column references
		// have to be recreated whenever their absolute names change due to a row getting
		// inserted, deleted, the rows collection getting sorted or resynced. For example,
		// When the first row is deleted from a rows collection then the next row's absolute
		// name changes from //ultraGrid1/Customers(1) to //ultraGrid1/Customers(0). However
		// the parsed reference as returned by RefBase.ParsedReference property is still 
		// based on the old absolute name. So we need to recreate the parsed reference.
		// 
		internal override int ParserInstanceVersion
		{
			get
			{
				int ret = this.parserInstanceVersion;
				RowsCollection rows = this.Rows;
				while ( null != rows.ParentRow )
				{
					rows = rows.ParentRow.ParentCollection;
					ret += rows.CalcReference.parserInstanceVersion;
				}

				return ret;
			}
		}

		#endregion // ParserInstanceVersion

		#region Rows
		
		internal RowsCollection Rows
		{
			get
			{
				return this.rows;
			}
		}

		#endregion // Rows

		#region Band
		
		internal UltraGridBand Band
		{
			get
			{
				return this.Rows.Band;
			}
		}

		#endregion // Band

		#region BandContext
		
		internal override UltraGridBand BandContext
		{
			get
			{
				return this.Rows.Band;
			}
		}

		#endregion // BandContext

		#region RefBase Overrides

		#region ElementName

		public override string ElementName 
		{
			get 
			{
				return this.Band.CalcReferenceName;
			}
		}

		#endregion // ElementName

		#region GroupByColumnName

		private string GroupByColumnReferenceName
		{
			get
			{
				UltraGridColumn col = this.Rows.GroupByColumn;
				return null != col ? col.FormulaHolder.NormalizedCalcReferenceName : null;
			}
		}

		#endregion // GroupByColumnName

		#region BaseParent

		public override RefBase BaseParent
		{ 
			get 
			{
				return null != this.Rows.ParentRow 
					? (RefBase)this.Rows.ParentRow.CalcReference
					: (RefBase)this.Band.Layout.CalcReference;
			}
		}

		#endregion // BaseParent

		#region FindItem

		public override IUltraCalcReference FindItem( string name )
		{
			UltraGridColumn column = RefUtils.GetColumn( this.Band, name, false );

			if (column == null)
				return RefUtils.CreateMissingColumnReference(this.Band, name);

			if ( column.IsChaptered )
			{
				return RefUtils.GetNestedBandReference(this.Rows, name);
			} 
			else 
			{
				return RefUtils.GetNestedColumnReference( this.Rows, column );
			}
		}

		#endregion // FindItem

		#region FindSummaryItem

		public override IUltraCalcReference FindSummaryItem( string name )
		{
//			if ( this.Rows.IsGroupByRows )
//			{
//				SummarySettings summary = RefUtils.GetSummarySettings( this.Band, name );
//				return this.Layout.CalcReference.GetNestedReference( this.Rows, summary );
//			}
//			else
			{
				return RefUtils.GetSummaryValueReference( this.Rows, name );
			}
		}

		#endregion // FindSummaryItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name, string index )
		{
			if ( this.Rows.IsGroupByRows )
			{
				if ( 0 == string.Compare( this.GroupByColumnReferenceName, name, true ) )
					return RefUtils.GetRowReference( RefUtils.FindGroupByRow( this.Rows, index ) );
			}

			return this.FindItem( name );
		}

		#endregion // FindItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name, int index, bool isRelative )
		{
			if ( this.Rows.IsGroupByRows )
			{
				if ( 0 == string.Compare( this.GroupByColumnReferenceName, name, true ) )
				{
					if ( isRelative )
						return new UltraCalcReferenceError( name, SR.GetString( "LER_Calc_RelativeIndexInvalid" ) );

					return RefUtils.GetRowReference( this.Rows, index, name );
				}
			}

			return this.FindItem( name );
		}

		#endregion // FindItem

		#region FindAll

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItem( name );
		}

		#endregion // FindAll

		// SSP 9/5/04
		// Overrode ResolveReference.
		//
		#region ResolveReference

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private IUltraCalcReference ResolveReferenceHelperHelper( RefTuple referenceTuple, UltraGridRefBase reference, 
		//    ResolveReferenceType resolveReferenceType, RowsCollection rows, UltraGridRow rowContext )
		private static IUltraCalcReference ResolveReferenceHelperHelper( RefTuple referenceTuple, UltraGridRefBase reference,
			ResolveReferenceType resolveReferenceType, RowsCollection rows, UltraGridRow rowContext )
		{
			Debug.Assert( null == rowContext || ! rowContext.IsGroupByRow );
			Debug.Assert( reference is ColumnReference || reference is SummarySettingsReference );

			// Is the incoming reference being referenced via relative index or with a scope
			// of All, in which case we need to recalculate the whole column.
			//
			bool isMarked = referenceTuple.Marked;
			if ( ! isMarked )
			{
				foreach ( RefTuple t in reference.ParsedReference )
				{
					if ( t.Marked )
					{
						isMarked = true;
						break;
					}
				}
			}

			// SSP 9/16/04 UWC114
			// If a tuple other than the last tuple has a relative index then return the column
			// reference itself. We could optimize this however typically indicates a relative 
			// reference from a different band. This would be rare.
			//
			if ( isMarked && ! referenceTuple.Marked && ResolveReferenceType.LeftHandSide == resolveReferenceType )
				return reference;

			if ( reference is ColumnReference )
			{
				UltraGridColumn column = ((ColumnReference)reference).Column;

				IUltraCalcReference retVal = null;

				RefTuple.RefScope scopeResolved = ResolveReferenceType.LeftHandSide == resolveReferenceType
					&& isMarked ? RefTuple.RefScope.All : referenceTuple.Scope;

				switch ( scopeResolved )
				{
						// If no scope.
						//
					case RefTuple.RefScope.Any:
						retVal = null != rowContext && rowContext.Band == column.Band 
							? RefUtils.GetCellReference( rowContext, column ) 
							: RefUtils.GetNestedColumnReference( rows, column );
						break;
						// Is this a Column(*) reference (has scope of all).
						//
					case RefTuple.RefScope.All:
						retVal = RefUtils.GetNestedColumnReference( rows, column );
						break;
					case RefTuple.RefScope.RelativeIndex:
					{
						IUltraCalcReference rowRef = null != rowContext && rowContext.Band == column.Band 
							? RefUtils.GetRowRelativeReference( rowContext, referenceTuple.ScopeIndex, null )
							: RefUtils.GetNestedColumnReference( rows, column );

						if ( !(rowRef is RowReferenceBase) )
							return rowRef;

						retVal = RefUtils.GetCellReference( ((RowReferenceBase)rowRef).Row, column );
						break;
					}
					case RefTuple.RefScope.Index:
					{
						// MD 11/20/08 - TFS5843
						// The row reference returned might be a group by row, but if we are looking for a column reference,
						// we actually want a non-group by row, so we have to do a descendant search because we could have many 
						// levels of group-by nesting.
						//IUltraCalcReference rowRef = RefUtils.GetRowReference( rows, referenceTuple.ScopeIndex, null );
						//
						//if ( !(rowRef is RowReferenceBase ) )
						//    return rowRef;
						//
						//retVal = RefUtils.GetCellReference( ((RowReferenceBase)rowRef).Row, column );
						RefBase nestedColumnReference = (RefBase)RefUtils.GetNestedColumnReference( rows, column );

						int count = 0;
						foreach ( IUltraCalcReference childRef in nestedColumnReference.References )
						{
							if ( count == referenceTuple.ScopeIndex )
								return childRef;

							count++;
						}
						break;
					}
					case RefTuple.RefScope.Identifier:
					{
						IUltraCalcReference rowRef = RefUtils.FindRowReference( rows, rowContext.Band, referenceTuple.ScopeID, null );

						if ( !(rowRef is RowReferenceBase ) )
							return rowRef;

						retVal = RefUtils.GetCellReference( ((RowReferenceBase)rowRef).Row, column );
						break;
					}
				}

				return retVal;
			}
			else 
			{
				SummarySettingsReference summarySettingsRef = (SummarySettingsReference)reference;

				if ( ResolveReferenceType.LeftHandSide == resolveReferenceType )
					return RefUtils.GetNestedSummarySettingsReference( rows.TopLevelRowsCollection, summarySettingsRef );

				return rows.Band == summarySettingsRef.Summary.Band 
					? RefUtils.GetSummaryValueReference( rows, summarySettingsRef.Summary )
					: RefUtils.GetNestedSummarySettingsReference( rows, summarySettingsRef );
			}
		}
			
		
		internal IUltraCalcReference ResolveReferenceHelper( IUltraCalcReference inReference, 
			ResolveReferenceType referenceType, UltraGridRow rowContext )
		{
			RefUnAnchored refUnAnchored = inReference as RefUnAnchored;
			UltraGridRefBase reference = null != refUnAnchored
				? refUnAnchored.WrappedReference as UltraGridRefBase 
				: inReference as UltraGridRefBase;

			// SSP 9/17/04
			// Added code to take care of range references in group-by situation. Added a new RangeReference
			// class for representing grid range references.
			//
			if ( reference is RangeReference )
			{
				RangeReference range = (RangeReference)reference;
				return range.GetResolvedRangeReference( rows, rowContext );
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//RefParser absoluteRP = null != refUnAnchored ? refUnAnchored.ParsedReference 
			//    : ( null != reference ? reference.ParsedReference : null );

			RefParser relativeRP = null != refUnAnchored ? refUnAnchored.RelativeReference 
				: ( null != reference ? reference.ParsedReference : null );

			// SSP 9/9/04
			// We are going to treat "Column(*)" and "Summary()" referneces optionally
			// preceded by series of "../" specially to support group-by stuff.
			//
			bool calledFromSummaryValueResolve = null == rowContext;

			RefTuple relativeRPLastTuple = null != relativeRP ? relativeRP.LastTuple : null;

			//bool isMarked = null != absoluteRP && null != absoluteRP.LastTuple && absoluteRP.LastTuple.Marked;

			try
			{
				if ( null != relativeRPLastTuple
					&& ( reference is ColumnReference || reference is SummarySettingsReference ) )
				{
					UltraGridBand referenceBand = reference is ColumnReference
						? ((ColumnReference)reference).Column.Band
						: ((SummarySettingsReference)reference).Summary.Band;
				
					// SSP 9/13/04 UWC96
					// If reference being resolved is from a different band than this rows collection then
					// relative reference specification doesn't apply.
					// Added the below if block.
					//
					if ( referenceBand != this.Band )
					{
						if ( this.Band.IsDescendantOfBand( referenceBand ) )
						{
							// If the reference' band is an ancestor band find the ancestor row 
							// corresponding to the ancestor band.
							//
							UltraGridRow row = rows.ParentRow;
							while ( null != row && referenceBand != row.Band )
								row = row.ParentRow;

							Debug.Assert( null != row );

							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//IUltraCalcReference result = this.ResolveReferenceHelperHelper( relativeRPLastTuple, reference, referenceType, row.ParentCollection, row );
							IUltraCalcReference result = RowsCollectionReference.ResolveReferenceHelperHelper( relativeRPLastTuple, reference, referenceType, row.ParentCollection, row );

							if (result is UltraCalcReferenceError)
								return new UltraCalcReferenceError(relativeRP.ToString(), ((UltraCalcReferenceError)result).Message );

							return result;
						}
						else 
						{
							// If the reference' band is a sibling, descendant of this band or a descendant of a 
							// a sibling band.
							//
							RowsCollection rows = null;
						
							if ( referenceBand.IsDescendantOfBand( this.Band ) )
							{
								// If the reference' band is a descendant band then get the appropriate rows 
								// collection based on whether there is a row context.
								//
								if ( null != rowContext )
								{
									UltraGridBand tmpBand = referenceBand;
									while ( this.Band != tmpBand.ParentBand )
										tmpBand = tmpBand.ParentBand;

									Debug.Assert( null != tmpBand );
									Debug.Assert( null != rowContext.ChildBands && rowContext.ChildBands.Exists( tmpBand ) );

									rows = rowContext.ChildBands[ tmpBand ].Rows;
								}
								else
								{
									rows = this.Rows;
								}
							}
								// SSP 2/17/05
								// If the inReference is from a different grid then return null which
								// will cause the caller to use the base implementation which will lead
								// to the same reference being returned. If the column is from a 
								// different 
							else if ( referenceBand.Layout != this.Layout )
							{
								return null;
							}
							else 
							{
								// Here the reference band is not a descendant band or an ancestor band. So it
								// must be either a sibling band or a descendant of a sibling band. In that 
								// case find sibling band that the reference band is a trivial descendant of.
								//
								UltraGridBand tmpBand = referenceBand;
								while ( this.Band.ParentBand != tmpBand.ParentBand )
									tmpBand = tmpBand.ParentBand;

								// If the reference' band is sibling, get the sibling rows collection.
								//
								rows = this.Rows.TopLevelRowsCollection.ParentRow.ChildBands[ tmpBand ].Rows;
							}

							Debug.Assert( null != rows );

							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//IUltraCalcReference result = this.ResolveReferenceHelperHelper( relativeRPLastTuple, reference, referenceType, rows, null );
							IUltraCalcReference result = RowsCollectionReference.ResolveReferenceHelperHelper( relativeRPLastTuple, reference, referenceType, rows, null );

							if (result is UltraCalcReferenceError)
								return new UltraCalcReferenceError(relativeRP.ToString(), ((UltraCalcReferenceError)result).Message );

							return result;
						}
					}
					else
					{
						RowsCollection rows = this.Rows;

						if ( reference is ColumnReference 
							&& ( RefTuple.RefScope.All == relativeRPLastTuple.Scope 
								|| calledFromSummaryValueResolve && RefTuple.RefScope.Any == relativeRPLastTuple.Scope )
							|| reference is SummarySettingsReference )
						{
							// If the name was specified using a relative string like [summary()] or [../../summary()]
							// or [../../column] etc..., then in a group-by situation we want to get the proper rows
							// collection at the right level. Such relative references are valid from a cell as well
							// as a summary formula.
							//
							if ( null != relativeRP && relativeRP.IsRelative )
							{
								bool fistParentTupleSkipped = false;
								foreach ( RefTuple t in relativeRP )
								{
									if ( RefTuple.RefType.Parent == t.Type )
									{
										// Since the first set of double dots is reduntant, skip it. For example,
										// [../Col] and [Col] are the same references from since from a context
										// of a column or a summary (which are the only items that can have formulas)
										// ".." gets you to band and the band will look for the column in it's columns 
										// collection giving back the same column that a column in the band would
										// have found. In other words, column searches its siblings to find items
										// where as a band searches for its children to find items.
										//
										if ( ! fistParentTupleSkipped )
										{
											fistParentTupleSkipped = true;
										}
										else
										{
											if ( null == rows.ParentRow )
												return new UltraCalcReferenceError( inReference.AbsoluteName, "Invalid reference." );

											rows = rows.ParentRow.ParentCollection;
										}
									}
									else
										break;
								}

								// If there were too many sets of double-dots then retrun a reference error.
								//
								if ( rows.Band != this.Band )
									return new UltraCalcReferenceError( relativeRP.ToString( ), "Invalid reference." );
							}
							else
							{
								// Here the reference name was specified using an absolute name.
								//

								// Take care of group-by level summary formulas.
								//
								if ( reference is GroupLevelSummarySettingsReference )
								{
									
									//
									GroupLevelSummarySettingsReference glsr = (GroupLevelSummarySettingsReference)reference;

									RowsCollection tmpRows = rows;
									while ( tmpRows.ParentRow is UltraGridGroupByRow
										&& glsr.ParentReference.GroupByColumn != ((UltraGridGroupByRow)tmpRows.ParentRow).Column )
										tmpRows = tmpRows.ParentRow.ParentCollection;

									if ( null == tmpRows.ParentRow || ! tmpRows.ParentRow.IsGroupByRow )
										return RefUtils.GetNestedSummarySettingsReference( rows, (GroupLevelSummarySettingsReference)reference );

									rows = tmpRows;
								}
								else
								{					
									// If a column or a summary is being referred to using the absolute name
									// like [//ultraGrid1/Customers/Freight] or [//ultraGrid1/Customers/Freight Total()]
									// then give the column or the summary of the overall rows collection.
									//
									rows = rows.TopLevelRowsCollection;
								}
							}
						}

						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//IUltraCalcReference result = this.ResolveReferenceHelperHelper( relativeRPLastTuple, reference, referenceType, rows, rowContext );
						IUltraCalcReference result = RowsCollectionReference.ResolveReferenceHelperHelper( relativeRPLastTuple, reference, referenceType, rows, rowContext );

						if (result is UltraCalcReferenceError)
							return new UltraCalcReferenceError(relativeRP.ToString(), ((UltraCalcReferenceError)result).Message );

						return result;
					}
				}
			}
			catch ( Exception )
			{
				return new UltraCalcReferenceError( relativeRP.ToString( ), "Invalid reference." );
			}

			return null;
		}

		public override IUltraCalcReference ResolveReference( IUltraCalcReference reference, ResolveReferenceType referenceType )
		{
			IUltraCalcReference tmp = this.ResolveReferenceHelper( reference, referenceType, null );
			if ( null != tmp )
				return tmp;

			return base.ResolveReference( reference, referenceType );
		}

		#endregion // ResolveReference

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get 
			{
				return null == this.rows || !this.rows.IsStillValid
					// SSP 9/21/06 BR16000 
					// 
					|| null != this.rows.ParentRow && this.rows.ParentRow.CalcReference.IsDisposedReference;
			}
		}

		#endregion // IsDisposedReference

		// SSP 10/12/04
		// Overrode GetHashCode and Equals. Calc engine will make use of these instead of
		// normalized absolute name comparisions.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			return this.rows.GetHashCode( );
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored instance then delegate the call to it.
			//
			if ( obj is RefUnAnchored )
				return obj.Equals( this );

			RowsCollectionReference rowsRef = obj as RowsCollectionReference;
			return null != rowsRef && this.rows == rowsRef.rows;
		}

		#endregion // Equals

		#region ContainsReferenceHelper

		// SSP 12/11/06 BR18268
		// 
		protected override bool ContainsReferenceHelper( IUltraCalcReference inReference, bool isProperSubset )
		{
			RefBase inRef = inReference as RefBase;
			UltraGridRefBase gridRef = RefUtils.GetUnderlyingReference( inRef ) as UltraGridRefBase;

			// SSP 10/1/04
			// Columns or bands are not subset of rows collections even though their absolute names
			// may say otherwise.
			//
			if ( isProperSubset && ( inRef is ColumnReference || inRef is BandReference ) )
				return false;

			if ( !isProperSubset )
			{
				if ( gridRef is ColumnReference || gridRef is BandReference )
				{
					// SSP 12/5/07 BR28652
					// Since it's Contains, we also have to check for descendant bands.
					// 
					// --------------------------------------------------------------------------------------------
					//return gridRef.BandContext == this.Band;
					UltraGridBand thisBand = this.Band;
					UltraGridBand gridRefBand = gridRef.BandContext;
					return null != gridRefBand && null != thisBand && gridRefBand.IsTrivialDescendantOf( thisBand );
					// --------------------------------------------------------------------------------------------
				}
			}

			if ( gridRef is NestedSummarySettingsReference )
			{
				return ( (NestedSummarySettingsReference)gridRef ).Rows.IsTrivialDescendantOf( ( (RowsCollectionReference)this ).Rows );
			}

			return base.ContainsReferenceHelper( inReference, isProperSubset );
		}

		#endregion // ContainsReferenceHelper

		#endregion // RefBase Overrides
	}

	#endregion // RowsCollectionReference Class

	#region NestedReference

	// SSP 9/10/04
	//
	internal abstract class NestedReference : UltraGridRefBase
	{
		#region Private Vars

		private RowsCollection rows = null;
		private UltraGridRefBase nestingReference = null;

		#endregion // Private Vars

		#region Constructor
		
		internal NestedReference( RowsCollection rows, UltraGridRefBase nestingReference, object context ) 
			: base( rows.Layout, context )
		{
			if ( null == rows )
				throw new ArgumentNullException( "rows" );

			if ( null == nestingReference )
				throw new ArgumentNullException( "nestingReference" );

			this.nestingReference = nestingReference;
			this.rows = rows;
		}

		#endregion // Constructor

		#region Rows
		
		internal RowsCollection Rows
		{
			get
			{
				return this.rows;
			}
		}

		#endregion // Rows

		#region RecalcDeferred

		// SSP 11/1/04
		// 
		public override bool RecalcDeferred
		{
			get
			{
				return this.nestingReference.RecalcDeferred;
			}
		}

		#endregion // RecalcDeferred

		#region InvalidateUIElement
		
		internal override void InvalidateUIElement( )
		{
			// Invalidate the whole grid if the rows collection's ancestors are expanded.
			//
			if ( null != this.Rows && null != this.Layout && this.Rows.IsPotentiallyVisibleOnScreen )
				this.Layout.DirtyGridElement( );
		}

		#endregion // InvalidateUIElement
	}

	#endregion // NestedReference

	#region NestedBandReference Class

	internal class NestedBandReference : NestedReference
	{
		#region Private Variables

		private UltraGridBand band = null;

		#endregion // Private Variables

		#region Constructor

		internal NestedBandReference( RowsCollection rows, UltraGridBand band ) : base( rows, band.CalcReference, band )
		{
			this.band = band;
		}

		#endregion // Constructor

		#region Band
		
		internal UltraGridBand Band
		{
			get
			{
				return this.band;
			}
		}

		#endregion // Band

		#region RefBase Overrides

		#region ElementName

		public override string ElementName 
		{
			get 
			{
				return this.Band.CalcReferenceName;
			}
		}

		#endregion // ElementName

		#region BaseParent

		public override RefBase BaseParent
		{ 
			get 
			{
				if ( this.Rows.Band == this.band.ParentBand || this.Rows.Band == this.band )
					return RefUtils.GetRowsReference( this.Rows );
				else
					return RefUtils.GetNestedBandReference( this.Rows, this.band.ParentBand );
			}
		}

		#endregion // BaseParent

		#region FindItem

		public override IUltraCalcReference FindItem( string name )
		{
			UltraGridColumn column = RefUtils.GetColumn( this.Band, name, false );

			if (column == null)
				return RefUtils.CreateMissingColumnReference(this.Band, name);

			if ( column.IsChaptered )
			{
				return RefUtils.GetNestedBandReference(this.Rows, name);
			} 
			else 
			{
				return RefUtils.GetNestedColumnReference( this.Rows, column );
			}
		}

		#endregion // FindItem

		#region FindSummaryItem

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			IUltraCalcReference reference = RefUtils.GetSummarySettingsReference( this.Band, name );

			if (reference is UltraCalcReferenceError)
				return reference;

			SummarySettingsReference summaryReference = (SummarySettingsReference)reference;

			return RefUtils.GetNestedSummarySettingsReference( this.Rows, summaryReference );
		}

		#endregion // FindSummaryItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name, string index )
		{
			IUltraCalcReference reference = this.FindItem( name );

			if (reference is RefBase)
				return new RefUnAnchored( reference as RefBase );
			else
			{
				Debug.Assert(reference is UltraCalcReferenceError, "Unexpected reference type was returned.");
				return reference;
			}
		}

		#endregion // FindItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name, int index, bool isRelative )
		{
			return this.FindItem( name );
		}

		#endregion // FindItem

		#region FindAll

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItem( name );
		}

		#endregion // FindAll

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get 
			{ 
				return null == this.band || ! this.band.IsStillValid 
					|| null == this.Rows || ! this.Rows.IsStillValid;
			}
		}

		#endregion // IsDisposedReference

		// SSP 10/12/04
		// Overrode GetHashCode and Equals. Calc engine will make use of these instead of
		// normalized absolute name comparisions.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			int hashCode = this.band.GetHashCode( );
			if ( ! this.Rows.IsRootRowsCollection )
				hashCode ^= this.Rows.GetHashCode( );

			return hashCode;
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored instance then delegate the call to it.
			//
			if ( obj is RefUnAnchored )
				return obj.Equals( this );

			NestedBandReference nestedBandRef = obj as NestedBandReference;
			if ( null != nestedBandRef )
				return this.band == nestedBandRef.band && this.Rows == nestedBandRef.Rows;

			BandReference bandRef = obj as BandReference;
			return null != bandRef && this.Rows.IsRootRowsCollection && this.band == bandRef.Band;
		}

		#endregion // Equals

		#endregion // RefBase Overrides
	}

	#endregion // NestedBandReference Class

	#region NestedColumnReference Class

	internal class NestedColumnReference : NestedReference
	{
		#region Private Variables

		private UltraGridColumn column = null;
		internal RowsCollection rowsParameter = null;

		#endregion // Private Variables

		#region Constructor

		internal NestedColumnReference( RowsCollection rows, UltraGridColumn column, bool forSummary ) 
			// SSP 10/29/04 - Formula Row Index Source
			// If the formula row index type is list index then use the top level row collection because
			// the list indexes go across all the group-by rows. Also for summary value evaluation do not
			// get the top level row collection because when evaluating summaries (like summing a column)
			// you want the sum to be the sum of cells in the current island rather than all the cells in
			// the top level row collection.
			//
			: base( forSummary || rows.Band != column.Band || FormulaRowIndexSource.ListIndex != rows.Band.FormulaRowIndexSourceResolved 
					? rows : rows.TopLevelRowsCollection, column.CalcReference, column )
		{
			if ( rows.Band.IsDescendantOfBand( column.Band ) )
				throw new ArgumentException( "Rows must be at a higher level than the column." );

			this.column = column;
			this.rowsParameter = rows;
		}

		#endregion // Constructor

		#region ParserInstanceVersion

		// Parsed references on rows, cells, rows collections and nested column references
		// have to be recreated whenever their absolute names change due to a row getting
		// inserted, deleted, the rows collection getting sorted or resynced. For example,
		// When the first row is deleted from a rows collection then the next row's absolute
		// name changes from //ultraGrid1/Customers(1) to //ultraGrid1/Customers(0). However
		// the parsed reference as returned by RefBase.ParsedReference property is still 
		// based on the old absolute name. So we need to recreate the parsed reference.
		// 
		internal override int ParserInstanceVersion
		{
			get
			{
				RowsCollection rows = this.Rows;
				return null != rows ? rows.CalcReference.ParserInstanceVersion : 0;
			}
		}

		#endregion // ParserInstanceVersion

		#region Column

		internal UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		#endregion // Column
		
		#region RefBase Overrides

		#region ElementName

		public override string ElementName
		{
			get 
			{
				return this.Column.FormulaHolder.ReferenceName;
			}
		}

		#endregion // ElementName

		#region BaseParent

		public override RefBase BaseParent
		{ 
			get 
			{
				if ( this.Rows.Band == this.Column.Band )
				{
					return RefUtils.GetRowsReference( this.Rows );
				}
				else
				{
					return RefUtils.GetNestedBandReference( this.Rows, this.column.Band );
				}
			}
		}

		#endregion // BaseParent

		#region FindItem

		public override IUltraCalcReference FindItem( string name )
		{
			RowsCollectionReference rowsRef = RefUtils.GetRowsReference( this.Rows );
			return rowsRef.FindItem( name );
		}

		#endregion // FindItem

		#region FindSummaryItem

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			RowsCollectionReference rowsRef = RefUtils.GetRowsReference( this.Rows );
			return rowsRef.FindSummaryItem( name );
		}

		#endregion // FindSummaryItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name, string index )
		{
			return this.FindItem( name );
		}

		#endregion // FindItem

		#region FindAll

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItem( name );
		}

		#endregion // FindAll

		#region FindItem

		public override IUltraCalcReference FindItem( string name,int index,bool isRelative )
		{
			return this.FindItem( name );
		}

		#endregion // FindItem

		#region ScopedReferences

		public override IUltraCalcReferenceCollection ScopedReferences( RefParser scopeRP )
		{
			return new RefCellCollection( this, this.Column, scopeRP );
		}

		#endregion // ScopedReferences

		#region Formula

		public override IUltraCalcFormula Formula
		{
			get 
			{
				ColumnReference colRef = this.column.CalcReference;
				return null != colRef ? colRef.Formula : null;
			}
		}

		#endregion // Formula

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get
			{
				return null == this.column || ! this.column.IsStillValid 
					|| null == this.Rows || ! this.Rows.IsStillValid;
			}
		}

		#endregion // IsDisposedReference

		// SSP 10/12/04
		// Overrode GetHashCode and Equals. Calc engine will make use of these instead of
		// normalized absolute name comparisions.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			int hashCode = this.column.GetHashCode( );
			if ( ! this.Rows.IsRootRowsCollection )
				hashCode ^= this.Rows.GetHashCode( );

			return hashCode;
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored instance then delegate the call to it.
			//
			if ( obj is RefUnAnchored )
				return obj.Equals( this );

			NestedColumnReference nestedColRef = obj as NestedColumnReference;
			if ( null != nestedColRef )
				return this.column == nestedColRef.column && this.Rows == nestedColRef.Rows;

			ColumnReference colRef = obj as ColumnReference;
			return null != colRef && this.Rows.IsRootRowsCollection && this.column == colRef.Column;
		}

		#endregion // Equals

		#endregion // RefBase Overrides
		
		#region Overrides of IUltraCalcReference

		#region References

		public override IUltraCalcReferenceCollection References
		{
			get
			{
				return this.ScopedReferences( this.ParsedReference );
			}
		}

		#endregion // References

		#region IsEnumerable

		public override bool IsEnumerable
		{ 
			get 
			{
				return true;
			}
		}

		#endregion // IsEnumerable

		#endregion // IUltraCalcReference Overrides
	}

	#endregion // NestedColumnReference Class

	#region NestedSummarySettingsReference Class

	internal class NestedSummarySettingsReference : NestedReference
	{
		#region Private Variables

		private SummarySettingsReference summaryReference = null;

		#endregion // Private Variables

		#region Constructor

		internal NestedSummarySettingsReference( RowsCollection rows, 
			SummarySettingsReference summaryReference ) : base( rows, summaryReference, summaryReference.Context )
		{
			this.summaryReference = summaryReference;
		}

		#endregion // Constructor

		#region Formula

		public override IUltraCalcFormula Formula
		{
			get 
			{
				return this.SummaryReference.Formula;
			}
		}

		#endregion // Formula

		#region SummaryReference
		
		internal SummarySettingsReference SummaryReference
		{
			get
			{
				return this.summaryReference;
			}
		}

		#endregion // SummaryReference

		#region Summary
		
		internal SummarySettings Summary
		{
			get
			{
				return this.SummaryReference.Summary;
			}
		}

		#endregion // Summary

		#region RefBase Overrides

		public override string ElementName 
		{
			get 
			{
				return this.SummaryReference.ElementName;
			}
		}

		public override RefBase BaseParent
		{ 
			get 
			{
				RowsCollection rows = this.Rows;
				SummarySettings summary = this.Summary;
				if ( rows.Band == summary.Band )
				{
					if ( this.SummaryReference is GroupLevelSummarySettingsReference )
					{
						GroupLevelSummarySettingsReference groupLevelSummaryReference = this.SummaryReference as GroupLevelSummarySettingsReference;

                        // MBS 3/19/09 - TFS13679
                        // The fix to BR30242 here causes a series of issues by changing the tuples that created based on the BaseParent.
                        // After discussing this with Sandip, we're not quite sure which scenario this optimization 
                        // particularly addresses, but the time gained by changing a cell's value within multiple
                        // group nestings (in my test, 3 GroupByRows) yielded a minimal gain with this fix, and 
                        // re-sorting the entire grid only gained a couple seconds (out of around 30 total), so this
                        // fix isn't really worth keeping in now since it breaks other customers.
                        //
                        #region Old Code
                        //// SSP 5/2/08 BR30242
                        //// Ideally we should be returning a reference that intersects the rows collection and
                        //// the summary reference's parent however since we don't have any that represents that,
                        //// simply return the the group-by level's parent reference unless rows collection 
                        //// reference is more specific.
                        //// ------------------------------------------------------------------------------------
                        ////return this.SummaryReference.BaseParent;
                        //UltraGridColumn groupByCol = rows.GroupByColumn;
                        //if ( null != groupByCol )
                        //{
                        //    UltraGridBand band = rows.Band;
                        //    int rowsGroupLevelIndex = band.SortedColumns.GetColumnIndex( groupByCol );
                        //    int refGroupLevelIndex = band.SortedColumns.GetColumnIndex( groupLevelSummaryReference.GroupByColumn );
                        //    if ( rowsGroupLevelIndex >= 0 && rowsGroupLevelIndex < refGroupLevelIndex - 1 )
                        //        return groupLevelSummaryReference.BaseParent;
                        //}
                        //// ------------------------------------------------------------------------------------
                        #endregion //Old Code
                        //
                        return groupLevelSummaryReference.BaseParent;
                    }

					return RefUtils.GetRowsReference( rows );
				}
				else
					return RefUtils.GetNestedBandReference( rows, summary.Band );
			}
		}

		private RowsCollectionReference RowsReference
		{
			get
			{
				return RefUtils.GetRowsReference( this.Rows );
			}
		}

		public override IUltraCalcReference FindItem( string name )
		{
			return this.RowsReference.FindItem( name );
		}

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return this.RowsReference.FindSummaryItem( name );
		}

		public override IUltraCalcReference FindItem( string name, string index )
		{
			return this.RowsReference.FindItem( name, index );
		}

		public override IUltraCalcReference FindItem( string name, int index, bool isRelative )
		{
			return this.RowsReference.FindItem( name, index, isRelative );
		}

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItem( name );
		}

		public override IUltraCalcReferenceCollection ScopedReferences( RefParser scopeRP )
		{
			return new RefSummaryValueCollection( this, this.Summary, scopeRP );
		}

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get 
			{ 
				return null == this.Summary || ! this.Summary.IsStillValid
					|| null == this.Rows || ! this.Rows.IsStillValid;
			}
		}

		#endregion // IsDisposedReference

		// SSP 10/12/04
		// Overrode GetHashCode and Equals. Calc engine will make use of these instead of
		// normalized absolute name comparisions.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			int hashCode = this.Summary.GetHashCode( );
			if ( ! this.Rows.IsRootRowsCollection )
				hashCode ^= this.Rows.GetHashCode( );

			return hashCode;
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored instance then delegate the call to it.
			//
			if ( obj is RefUnAnchored )
				return obj.Equals( this );

			NestedSummarySettingsReference nestedSummaryRef = obj as NestedSummarySettingsReference;
			if ( null != nestedSummaryRef )
				return this.Rows == nestedSummaryRef.Rows && this.summaryReference.Equals( nestedSummaryRef.summaryReference );

			SummarySettingsReference summaryRef = obj as SummarySettingsReference;
			return null != summaryRef && this.Rows.IsRootRowsCollection && this.SummaryReference.Equals( summaryRef );
		}

		#endregion // Equals

		#endregion // RefBase Overrides
	
		#region IUltraCalcReference Overrides

		public override IUltraCalcReferenceCollection References
		{
			get
			{
				return this.ScopedReferences( this.ParsedReference );
			}
		}

		public override bool IsEnumerable 
		{ 
			get 
			{
				return true;
			}
		}

		#endregion IUltraCalcReference Overrides
	}

	#endregion // NestedSummarySettingsReference Class

	#region ColumnReference Class

	internal class ColumnReference : FormulaRefBase
	{
		#region Private Variables

		private UltraGridColumn column = null;

		#endregion // Private Variables

		#region Constructor

		internal ColumnReference( UltraGridColumn column ) : base( column.Layout, column, column.FormulaHolder )
		{
			this.column = column;
		}

		#endregion // Constructor

		#region Column

		internal UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		#endregion // Column
		
		#region RefBase Overrides

		#region ElementName

		public override string ElementName
		{
			get 
			{
				return this.Column.FormulaHolder.ReferenceName;
			}
		}

		#endregion // ElementName

		#region BaseParent

		public override RefBase BaseParent
		{ 
			get 
			{
				return RefUtils.GetBandReference( this.Column.Band );
			}
		}

		#endregion // BaseParent

		#region FindItem

		public override IUltraCalcReference FindItem( string name )
		{
			return this.BaseParent.FindItem( name );
		}

		#endregion // FindItem

		#region FindSummaryItem

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return this.BaseParent.FindSummaryItem( name );
		}

		#endregion // FindSummaryItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name,string index )
		{
			return this.FindItem( name );
		}

		#endregion // FindItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name,int index,bool isRelative )
		{
			return this.FindItem( name );
		}

		#endregion // FindItem

		#region ScopedReferences

		public override IUltraCalcReferenceCollection ScopedReferences( RefParser scopeRP )
		{
			return new RefCellCollection( this, this.Column, scopeRP );
		}

		#endregion // ScopedReferences

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get 
			{ 
				return null == this.column || ! this.column.IsStillValid;
			}
		}

		#endregion // IsDisposedReference

		#region References

		public override IUltraCalcReferenceCollection References
		{
			get
			{
				return this.ScopedReferences( this.ParsedReference );
			}
		}

		#endregion // References

		#region IsEnumerable

		public override bool IsEnumerable
		{ 
			get 
			{
				return true;
			}
		}

		#endregion // IsEnumerable

		#region FindAll

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItem( name );
		}

		#endregion // FindAll

		#endregion // RefBase Overrides

		// SSP 10/12/04
		// Overrode GetHashCode, Equals, ContainsReference and IsSubsetReference methods.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			return this.column.GetHashCode( );
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored or NestedColumnReference then delegate the call to it.
			//
			if ( obj is RefUnAnchored || obj is NestedColumnReference )
				return obj.Equals( this );

			ColumnReference columnRef = obj as ColumnReference;
			return null != columnRef && this.column == columnRef.column;
		}

		#endregion // Equals

		#region ContainsReferenceHelper

		protected override bool ContainsReferenceHelper( IUltraCalcReference inReference, bool isProperSubset )
		{
			UltraGridRefBase testRef = RefUtils.GetUnderlyingReference( inReference ) as UltraGridRefBase;
			return null != testRef && this.ColumnContext == testRef.ColumnContext;
		}

		#endregion // ContainsReferenceHelper
	}

	#endregion // ColumnReference Class

	#region RowReferenceBase Class

	internal abstract class RowReferenceBase : UltraGridRefBase
	{
		#region Private Variables

		private UltraGridRow row = null;
		protected int lastValidIndex = -1;

		#endregion // Private Variables

		#region Constructor

		internal RowReferenceBase( UltraGridRow row ) : base( row.Layout, row )
		{
			this.row = row;
		}

		#endregion // Constructor

		#region ParserInstanceVersion

		// Parsed references on rows, cells, rows collections and nested column references
		// have to be recreated whenever their absolute names change due to a row getting
		// inserted, deleted, the rows collection getting sorted or resynced. For example,
		// When the first row is deleted from a rows collection then the next row's absolute
		// name changes from //ultraGrid1/Customers(1) to //ultraGrid1/Customers(0). However
		// the parsed reference as returned by RefBase.ParsedReference property is still 
		// based on the old absolute name. So we need to recreate the parsed reference.
		// 
		internal override int ParserInstanceVersion
		{
			get
			{
				RowsCollection rows = this.Row.ParentCollection;
				return null != rows ? rows.CalcReference.ParserInstanceVersion : 0;
			}
		}

		#endregion // ParserInstanceVersion

		#region Row

		internal UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}

		#endregion // Row		

		#region BandContext
		
		internal override UltraGridBand BandContext
		{
			get
			{
				return this.Row.Band;
			}
		}

		#endregion // BandContext

		#region RefBase Overrides

		public override bool IsDataReference 
		{
			get 
			{
				// SSP 9/1/04
				// Row doesn't represent a data reference.
				//return true;
				return false;
			} 
		}

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return RefUtils.GetSummaryValueReference( this.Row.ParentCollection, name );
		}

		// SSP 8/9/04
		// Overrode ContainsReference to fix the problem where when a row got deleted,
		// the calc engine was adding the summary value to the recalc chain and then
		// removing it thinking that the summary reference was contained by the row
		// reference which is not the case for summaries that are from the same band.
		//
		/// <summary>
		/// Returns true if this reference contains inReference
		/// </summary>
		/// <param name="inReference">The contained candidate.</param>
		/// <returns>true if inReference is contained by this reference.</returns>
		public override bool ContainsReference( IUltraCalcReference inReference )
		{
			IUltraCalcReference inRef = RefUtils.GetUnderlyingReference( inReference );

			// Return false if the passed in reference is a summary reference.
			//
			if ( inRef is SummarySettingsReference
				|| inRef is SummaryValueReference
				&& ((SummaryValueReference)inRef).SummaryValue.Band == this.Row.BandInternal )
				return false;

			return base.ContainsReference( inReference );
		}

		#region ResolveReferenceHelper

		internal IUltraCalcReference ResolveReferenceHelper( IUltraCalcReference inReference, ResolveReferenceType referenceType, UltraGridCell cellContext )
		{
			RefBase inRef = inReference as RefBase;
			if ( null != inRef && ! inRef.IsRange )
			{
				try
				{
					UltraGridColumn col = null;
					RefUnAnchored refUnanchored = inRef as RefUnAnchored;
					if ( null != refUnanchored )
						inRef = refUnanchored.WrappedReference;
					
					if ( inRef is ColumnReference )
						col = ((ColumnReference)inRef).Column;
					else if ( inRef is NestedColumnReference )
						col = ((NestedColumnReference)inRef).Column;

					if ( null != col && col.Band == this.Row.BandInternal && ! this.Row.IsGroupByRow )
					{
						// Also check to see that the requested reference doesn't have a scope of All,
						// for example a reference with * like [Column(*)].
						// 
						RefParser rp = null != refUnanchored ? refUnanchored.ParsedReference : inRef.ParsedReference;
						if ( null == rp || rp.TupleCount <= 0 
							|| ( ! rp.HasRelativeIndex && ! rp.HasAbsoluteIndex 
							&& ! rp.LastTuple.Marked && RefTuple.RefScope.All != rp.LastTuple.Scope ) )
							return RefUtils.GetCellReference( this.Row.Cells[ col ] );
					}
				}
				catch ( Exception exc )
				{
					return new UltraCalcReferenceError( inRef.AbsoluteName, exc.Message );
				}
					
				return this.Row.ParentCollection.CalcReference.ResolveReferenceHelper( inReference, referenceType, this.Row );
			}

			return null;
		}

		#endregion // ResolveReferenceHelper

		#region ResolveReference

		// SSP 9/5/04
		// Overrode ResolveReference.
		//
		public override IUltraCalcReference ResolveReference( IUltraCalcReference reference, ResolveReferenceType referenceType )
		{
			IUltraCalcReference tmp = this.ResolveReferenceHelper( reference, referenceType, null );
			if ( null != tmp )
				return tmp;

            return base.ResolveReference( reference, referenceType );
		}

		#endregion // ResolveReference

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get 
			{ 
				// SSP 9/13/04
				// Return true from IsDisposedReference as a fix to the problem that when
				// the calc-engine goes to process the enqued Delete event, the row is disposed
				// and they don't dirty the necessary dependants that would have been affected.
				// This fix should work out fine since when the calc-engine processes Delete, 
				// it will disconnect any tokens referncing the row as well.
				//
				return null == this.row || !this.row.IsStillValid
					|| null == this.row.Band || !this.row.Band.IsStillValid
					// SSP 9/21/06 BR16000 
					// 
					|| RefUtils.GetRowCalcIndex( this.row ) < 0;

				//return null == this.row.Band || ! this.row.Band.IsStillValid;
			}
		}

		#endregion // IsDisposedReference

		// SSP 10/12/04
		// Overrode GetHashCode and Equals. Calc engine will make use of these instead of
		// normalized absolute name comparisions.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			return this.row.GetHashCode( );
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored instance then delegate the call to it.
			//
			if ( obj is RefUnAnchored )
				return obj.Equals( this );

			RowReferenceBase rowRef = obj as RowReferenceBase;
			return null != rowRef && this.row == rowRef.row;
		}

		#endregion // Equals

		#region ContainsReferenceHelper

		protected override bool ContainsReferenceHelper( IUltraCalcReference inReference, bool isSubset )
		{
			UltraGridRefBase testRef = RefUtils.GetUnderlyingReference( inReference ) as UltraGridRefBase;

			if ( testRef is CellReference )
				return ((CellReference)testRef).Cell.Row.IsTrivialDescendantOf( this.row );
			
			if ( testRef is ColumnReference )
				return ! isSubset && testRef.BandContext.IsTrivialDescendantOf( this.BandContext );
			
			return base.ContainsReferenceHelper( inReference, isSubset );
		}

		#endregion // ContainsReferenceHelper

		#endregion // RefBase Overrides
	}

	#endregion // RowReference Class

	#region RowReference Class

	internal class RowReference : RowReferenceBase
	{
		#region Private Variables

		internal bool inCellReferenceValueSet = false;

		#endregion // Private Variables

		#region Constructor

		internal RowReference( UltraGridRow row ) : base( row )
		{
		}

		#endregion // Constructor

		#region RefBase Overrides

		public override RefBase BaseParent
		{ 
			get 
			{
				UltraGridRow parentRow = RefUtils.GetParentRow( this.Row );
				return null != parentRow 
					? (RefBase)RefUtils.GetRowReference( parentRow ) 
					: (RefBase)this.Layout.CalcReference;
			}
		}

		public override string ElementName 
		{
			get 
			{
				int rowIndex = RefUtils.GetRowCalcIndex( this.Row );
				if ( rowIndex < 0 && lastValidIndex >= 0 )
					rowIndex = lastValidIndex;

				lastValidIndex = rowIndex;
				
				string bandKey = this.Row.Band.CalcReferenceName;
				System.Text.StringBuilder sb = new System.Text.StringBuilder( bandKey.Length + 8 );
				sb.Append( bandKey ).Append( RefParser.RefBeginScope ).Append( rowIndex ).Append( RefParser.RefEndScope );
				return sb.ToString( );
			}
		}

		private IUltraCalcReference FindItemHelper( string name, bool all )
		{
			UltraGridColumn column = RefUtils.GetColumn( this.Row.Band, name, false );

			if (column == null)
				return RefUtils.CreateMissingColumnReference(this.Row.Band, name);

			if ( column.IsChaptered ) 
			{
				return RefUtils.GetChildBandRowsReference( this.Row, name );
			} 
			else 
			{
				return ! all ? RefUtils.GetCellReference( this.Row.Cells[ column ] )
					: RefUtils.GetNestedColumnReference( this.Row.ParentCollection, column );
			}
		}

		public override IUltraCalcReference FindItem( string name )
		{
			return this.FindItemHelper( name, false );
		}

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItemHelper( name, true );
		}

		public override IUltraCalcReference FindItem( string name, string index )
		{
			UltraGridColumn column = RefUtils.GetColumn( this.Row.Band, name, false );

			if (column == null)
				return RefUtils.CreateMissingColumnReference(this.Row.Band, name);

			if ( column.IsChaptered ) 
			{
				return RefUtils.FindChildBandRowReference(this.Row, name, index);
			} 
			else 
			{
				IUltraCalcReference rowRef = RefUtils.FindRowReference(this.Row.ParentCollection, this.Row.Band, index, name);

				if ( !(rowRef is RowReferenceBase) )
					return rowRef;

				UltraGridRow childRow = ((RowReferenceBase)rowRef).Row;
				UltraGridCell cell = childRow.Cells[ column ];
				return RefUtils.GetCellReference( cell );
			}
		}

		public override IUltraCalcReference FindItem( string name, int index, bool isRelative )
		{
			UltraGridColumn column = RefUtils.GetColumn( this.Row.Band, name, false );

			if (column == null)
				return RefUtils.CreateMissingColumnReference(this.Row.Band, name);

			if ( column.IsChaptered ) 
			{
				if( isRelative ) 
					return new UltraCalcReferenceError( name, SR.GetString( "LER_Calc_RelativeIndexInvalid" ) );

				return RefUtils.GetChildBandRowReference(this.Row, name, index);
			} 
			else 
			{
				IUltraCalcReference rowRef = isRelative
					? RefUtils.GetRowRelativeReference(this.Row, index, name)
					: RefUtils.GetRowReference(this.Row.ParentCollection, index, name);

				if ( !(rowRef is RowReferenceBase) )
					return rowRef;

				UltraGridRow childRow = ((RowReferenceBase)rowRef).Row;

				UltraGridCell cell = childRow.Cells[ column ];
				return RefUtils.GetCellReference( cell );
			}
		}

		#endregion // RefBase Overrides
	}

	#endregion // RowReference Class
	
	#region GroupByRowReference Class

	// SSP 9/1/04
	//

	internal class GroupByRowReference : RowReferenceBase
	{
		#region Constructor

		internal GroupByRowReference( UltraGridGroupByRow row ) : base( row )
		{
		}

		#endregion // Constructor

		#region GroupByRow

		internal UltraGridGroupByRow GroupByRow
		{
			get
			{
				return (UltraGridGroupByRow)this.Row;
			}
		}

		#endregion // GroupByRow		

		#region RefBase Overrides

		public override RefBase BaseParent
		{ 
			get 
			{
				UltraGridRow parentRow = RefUtils.GetParentRow( this.Row );
				return null == parentRow || ! parentRow.IsGroupByRow 
					? RefUtils.GetRowsReference( this.Row.ParentCollection )
					: (RefBase)RefUtils.GetRowReference( parentRow );
			}
		}

		public override string ElementName 
		{
			get 
			{
				
				// Make sure the implementation is correct, especially regarding the group-by rows.
				//
				int rowIndex = RefUtils.GetRowCalcIndex( this.Row );
				if ( rowIndex < 0 && lastValidIndex >= 0 )
					rowIndex = lastValidIndex;

				lastValidIndex = rowIndex;
				
				string groupByColumnKey = this.GroupByRow.Column.CalcReference.ElementName;
				System.Text.StringBuilder sb = new System.Text.StringBuilder( groupByColumnKey.Length + 8 );
				sb.Append( groupByColumnKey).Append( RefParser.RefBeginScope ).Append( rowIndex ).Append( RefParser.RefEndScope );
				return sb.ToString( );
			}
		}

		private IUltraCalcReference FindItemHelper( string name, bool all )
		{
			UltraGridColumn column = RefUtils.GetColumn( this.Row.Band, name, false );

			if (column == null)
				return RefUtils.CreateMissingColumnReference(this.Row.Band, name);

			if( column.IsChaptered ) 
			{
				return RefUtils.GetChildBandRowsReference(this.Row, name);
			} 
			else 
			{
				if ( ! all )
				{
					return new UltraCalcReferenceError( name, "Column invalid on a group-by row." );
				}
				else
				{
					return RefUtils.GetNestedColumnReference( this.Row.ParentCollection, column );
				}
			}
		}

		public override IUltraCalcReference FindItem( string name )
		{
			return this.FindItemHelper( name, false );
		}

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItemHelper( name, true );
		}

		public override IUltraCalcReference FindItem( string name, string index )
		{
			UltraGridColumn column = RefUtils.GetColumn( this.Row.Band, name, false );

			if (column == null)
				return RefUtils.CreateMissingColumnReference(this.Row.Band, name);

			if ( column.IsChaptered ) 
			{
				return RefUtils.FindChildBandRowReference(this.Row, name, index);
			} 
			else 
			{
				if ( column == RefUtils.GetNextGroupByColumn( this.GroupByRow.Column ) )
				{
					UltraGridGroupByRow groupByRow = RefUtils.FindGroupByRow( this.GroupByRow.Rows, index );

					
					if (groupByRow == null)
						return new UltraCalcReferenceError(name, string.Format("GroupBy Row '{0)' not found.", index) );

					return RefUtils.GetRowReference( groupByRow );
				}

				
				return new UltraCalcReferenceError( name, "Next group-by column name or child band name expected." );
			}
		}

		public override IUltraCalcReference FindItem( string name, int index, bool isRelative )
		{
			UltraGridColumn column = RefUtils.GetColumn( this.Row.Band, name, false );

			if (column == null)
				return RefUtils.CreateMissingColumnReference( this.Row.Band, name );

			if( column.IsChaptered ) 
			{
				if( isRelative ) 
					return new UltraCalcReferenceError( name, SR.GetString( "LER_Calc_RelativeIndexInvalid" ) );

				return RefUtils.GetChildBandRowReference( this.Row, name, index );
			} 
			else 
			{
				if ( column == RefUtils.GetNextGroupByColumn( this.GroupByRow.Column ) )
				{
					if ( isRelative )
						return new UltraCalcReferenceError( name, SR.GetString( "LER_Calc_RelativeIndexInvalid" ) );

					// SSP 5/17/05
					// Typo. Meant to return the return value from the method call.
					//
					//RefUtils.GetRowReference(this.GroupByRow.Rows, index, name);
					return RefUtils.GetRowReference( this.GroupByRow.Rows, index, name );
				}

				return new UltraCalcReferenceError( name, "Next group-by column name or child band name expected." );
			}
		}

		// SSP 5/18/05
		// Overrode FindSummaryItem so the group-by row can return the summary of the child rows
		// instead of the summary of its parent row collection.
		//
		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return RefUtils.GetSummaryValueReference( this.GroupByRow.Rows, name );
		}

		#region ResolveReference

		// SSP 10/28/04
		// Overrode ResolveReference.
		//
		public override IUltraCalcReference ResolveReference( IUltraCalcReference reference, ResolveReferenceType referenceType )
		{
			IUltraCalcReference tmp = this.GroupByRow.Rows.CalcReference.ResolveReferenceHelper( reference, referenceType, null );
			if ( null != tmp )
			{
				if ( tmp is NestedColumnReference )
					return tmp;
			}

			return base.ResolveReference( reference, referenceType );
		}

		#endregion // ResolveReference

		#endregion // RefBase Overrides
	}

	#endregion // GroupByRowReference Class

	#region SummarySettingsReference Class

	internal class SummarySettingsReference : FormulaRefBase
	{
		#region Private Variables

		private SummarySettings summary = null;

		#endregion // Private Variables

		#region Constructor

		internal SummarySettingsReference( SummarySettings summary ) : base( summary.Layout, summary, summary.FormulaHolder )
		{
			this.summary = summary;
		}

		#endregion // Constructor

		#region Summary
		
		internal SummarySettings Summary
		{
			get
			{
				return this.summary;
			}
		}

		#endregion // Summary

		#region RefBase Overrides

		#region ElementName 

		public override string ElementName 
		{
			get 
			{
				return this.Summary.FormulaHolder.ReferenceName + RefUtils.SUMMARY_REFERENCE_POSTFIX;
			}
		}

		#endregion // ElementName 

		#region BaseParent

		public override RefBase BaseParent
		{ 
			get 
			{
				return this.BandReference;
			}
		}

		#endregion // BaseParent

		#region BandReference

		private BandReference BandReference
		{
			get
			{
				return RefUtils.GetBandReference( this.Summary.Band );
			}
		}

		#endregion // BandReference

		#region FindItem

		public override IUltraCalcReference FindItem( string name )
		{
			return this.BandReference.FindItem( name );
		}

		#endregion // FindItem

		#region FindSummaryItem

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return this.BandReference.FindSummaryItem( name );
		}

		#endregion // FindSummaryItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name, string index )
		{
			return this.BandReference.FindItem( name, index );
		}

		#endregion // FindItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name, int index, bool isRelative )
		{
			return this.BandReference.FindItem( name, index, isRelative );
		}

		#endregion // FindItem

		#region FindAll

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItem( name );
		}

		#endregion // FindAll

		#region ScopedReferences

		public override IUltraCalcReferenceCollection ScopedReferences( RefParser scopeRP )
		{
			return new RefSummaryValueCollection( this, this.Summary, scopeRP );
		}

		#endregion // ScopedReferences

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get 
			{ 
				return null == this.summary || ! this.summary.IsStillValid;
			}
		}

		#endregion // IsDisposedReference

		#endregion // RefBase Overrides
	
		#region IUltraCalcReference Overrides

		#region References

		public override IUltraCalcReferenceCollection References
		{
			get
			{
				return this.ScopedReferences( this.ParsedReference );
			}
		}

		#endregion // References

		#region IsRootSummary

		private bool IsRootSummary
		{
			get
			{
				return ! ( this is GroupLevelSummarySettingsReference ) && null == this.Summary.Band.ParentBand;
			}
		}

		#endregion // IsRootSummary

		#region IsEnumerable 

		public override bool IsEnumerable 
		{ 
			get 
			{
				// SSP 9/13/04
				// Return false from IsEnumerable for the root summary because there is a single
				// value associated with it anyways. This change was made because the 
				// //ultraGrid1/Customers/Total() summary reference should refer to the value of 
				// the root summary since there is only one. There is no need for this reference 
				// to be enumerable because if it were then the you would have to enclose it
				// in a SUM function to get the value externally from a text box like 
				// sum([//ultraGrid1/Customers/Total()]).
				//
				//return true;
				return ! this.IsRootSummary;
			}
		}

		#endregion // IsEnumerable 

		#region Value

		// SSP 9/13/04
		// Overrode the Value property. Now we are returning false from IsEnumerable for the root 
		// summary because there is a single value associated with it anyways. This change was 
		// made because the //ultraGrid1/Customers/Total() summary reference should refer to the 
		// value of the root summary since there is only one. There is no need for this reference 
		// to be enumerable because if it were then the you would have to enclose it in a SUM 
		// function to get the value externally from a text box like 
		// sum([//ultraGrid1/Customers/Total()]).
		//
		public override UltraCalcValue Value
		{
			get
			{
				if ( this.IsRootSummary )
				{
					SummaryValueReference summaryValueRef = (SummaryValueReference)RefUtils.GetSummaryValueReference( this.Layout.Rows, this.Summary );
					return summaryValueRef.Value;
				}

				return base.Value;
			}
			set
			{
				if ( this.IsRootSummary )
				{
					SummaryValueReference summaryValueRef = (SummaryValueReference)RefUtils.GetSummaryValueReference( this.Layout.Rows, this.Summary );
					summaryValueRef.Value = value;
				}
				else
				{
					base.Value = value;
				}
			}
		}

		#endregion // Value

		#region ResolveReference

		// SSP 11/1/04
		// Overrode ResolveReference.
		//
		public override IUltraCalcReference ResolveReference( IUltraCalcReference reference, ResolveReferenceType referenceType )
		{
			if ( this.IsRootSummary )
			{
				SummaryValueReference summaryValueRef = (SummaryValueReference)RefUtils.GetSummaryValueReference( this.Layout.Rows, this.Summary );
				return summaryValueRef.ResolveReference( reference, referenceType );
			}

			return base.ResolveReference( reference, referenceType );
		}

		#endregion // ResolveReference

		// SSP 10/12/04
		// Overrode GetHashCode, Equals, ContainsReference and IsSubsetReference methods.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			return this.summary.GetHashCode( );
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored or NestedSummarySettingsReference or 
			// GroupLevelSummarySettingsReference then delegate the call to it.
			//
			if ( obj is RefUnAnchored || obj is NestedSummarySettingsReference 
				|| obj is GroupLevelSummarySettingsReference )
				return obj.Equals( this );

			SummarySettingsReference summaryRef = obj as SummarySettingsReference;
			return null != summaryRef && this.summary == summaryRef.summary;
		}

		#endregion // Equals

		#endregion IUltraCalcReference Overrides
	}

	#endregion // SummarySettingsReference Class

	#region GroupLevelReference

	// SSP 9/9/04
	// Added GroupLevelReference that represents a group level in a group-by situation. This
	// is used for summaries only.
	//
	internal class GroupLevelReference : UltraGridRefBase
	{
		#region Private Vars

		private UltraGridRefBase parentReference = null;
		private UltraGridColumn groupByColumn = null;

		#endregion // Private Vars

		#region Constructor
		
		internal GroupLevelReference( UltraGridRefBase parentReference, UltraGridColumn groupByColumn ) 
			: base( groupByColumn.Layout, groupByColumn.Band )
		{
			if ( null == parentReference )
				throw new ArgumentNullException( "parentReference" );

			if ( ! ( parentReference is GroupLevelReference ) && ! ( parentReference is BandReference ) )
				throw new ArgumentException( "parentReference must be either a BandReference or a GroupLevelReference." );

			if ( ! groupByColumn.IsGroupByColumn )
				throw new ArgumentException( "Column must be a group-by column." );

			this.groupByColumn = groupByColumn;
			this.parentReference = parentReference;
		}

		#endregion // Constructor

		#region BandReference

		private BandReference BandReference
		{
			get
			{
				return RefUtils.GetBandReference( this.groupByColumn.Band );
			}
		}

		#endregion // BandReference

		#region GroupByColumn
		
		internal UltraGridColumn GroupByColumn
		{
			get
			{
				return this.groupByColumn;
			}
		}

		#endregion // GroupByColumn

		#region RefBase Overrides

		#region BaseParent

		public override RefBase BaseParent
		{ 
			get 
			{
				return this.parentReference;
			}
		}

		#endregion // BaseParent

		#region ElementName

		public override string ElementName 
		{
			get 
			{
				return this.groupByColumn.FormulaHolder.ReferenceName;
			}
		}

		#endregion // ElementName

		#region FindItem

		public override IUltraCalcReference FindItem( string name )
		{
			return this.BandReference.FindItem( name );
		}

		#endregion // FindItem

		#region FindSummaryItem

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return this.BandReference.FindSummaryItem( name );
		}

		#endregion // FindSummaryItem

		#region FindItem

		public override IUltraCalcReference FindItem( string name,string index )
		{
			return this.FindItem( name );
		}

		#endregion // FindItem

		#region FindAll

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItem( name );
		}

		#endregion // FindAll

		#region FindItem

		public override IUltraCalcReference FindItem( string name,int index,bool isRelative )
		{
			return this.FindItem( name, index, isRelative );
		}

		#endregion // FindItem

		// SSP 10/12/04
		// Overrode GetHashCode and Equals. Calc engine will make use of these instead of
		// normalized absolute name comparisions.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			return 0x7fedcba ^ this.groupByColumn.GetHashCode( );
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored instance then delegate the call to it.
			//
			if ( obj is RefUnAnchored )
				return obj.Equals( this );

			GroupLevelReference glRef = obj as GroupLevelReference;
			return null != glRef && this.groupByColumn == glRef.groupByColumn;
		}

		#endregion // Equals

		#endregion // RefBase Overrides
	}

	#endregion // GroupLevelReference

	#region GroupByLevelSummarySettingsReference Class

	// SSP 9/9/04
	// Added GroupLevelSummarySettingsReference class.
	//
	internal class GroupLevelSummarySettingsReference : SummarySettingsReference
	{
		#region Private Vars

		private GroupLevelReference parentReference = null;
		private bool isDisposed = false;

		#endregion // Private Vars

		#region Constructor

		internal GroupLevelSummarySettingsReference( GroupLevelReference parentReference, SummarySettings summary ) : base( summary )
		{
			this.parentReference = parentReference;
		}

		#endregion // Constructor

		#region ParentReference 
		
		internal GroupLevelReference ParentReference 
		{
			get
			{
				return this.parentReference;
			}
		}

		#endregion // ParentReference 

		#region GroupByColumn

		internal UltraGridColumn GroupByColumn
		{
			get
			{
				return this.ParentReference.GroupByColumn;
			}
		}

		#endregion // GroupByColumn

		#region RefBase Overrides

		public override RefBase BaseParent
		{ 
			get 
			{
				return this.ParentReference;
			}
		}

		#endregion // RefBase Overrides

		#region InternalSetIsDisposed

		internal void InternalSetIsDisposed( bool disposed )
		{
			this.isDisposed = disposed;
		}

		#endregion // InternalSetIsDisposed

		#region IsDisposedReference

		// SSP 9/12/04
		//
		public override bool IsDisposedReference
		{
			get 
			{ 
				return this.isDisposed || base.IsDisposedReference;
			}
		}

		// SSP 10/12/04
		// Overrode GetHashCode and Equals. Calc engine will make use of these instead of
		// normalized absolute name comparisions.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			return 0x7abcdef ^ this.parentReference.GetHashCode( );
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is RefUnAnchored or NestedSummarySettingsReference then delegate the 
			// call to it.
			//
			if ( obj is RefUnAnchored || obj is NestedSummarySettingsReference )
				return obj.Equals( this );

			GroupLevelSummarySettingsReference glRef = obj as GroupLevelSummarySettingsReference;
			return null != glRef && this.parentReference.Equals( glRef.parentReference )
				&& this.Summary == glRef.Summary;
		}

		#endregion // Equals

		#endregion // IsDisposedReference
	}

	#endregion // GroupByLevelSummarySettingsReference Class

	#region FormulaRefBase Class
	
	internal abstract class FormulaRefBase : UltraGridRefBase
	{
		#region Private Variables

		private IUltraCalcFormula formula = null;
		internal int valueDirtyCounter = 0;
		internal int visibleCounter = 0;
		private FormulaHolder formulaHolder = null;

		// SSP 6/24/05 BR04577
		// If dummyFormula is true then this summary is using a formula strictly to emulate
		// dependancy chaining of non-formula summaries. If a non-summary formula is referenced
		// by a formula whenever the source column of the summary is changed, this formula needs
		// to be added to the calc chain. For example, B() = A (non-formula), C = B(). When A
		// changes, B is considered dirty and C needs to be enqued into the re-calc chain. A 
		// dummy formula is used for this.
		// 
		internal bool dummyFormula = false;

		#endregion // Private Variables

		#region Constructor
		
		internal FormulaRefBase( UltraGridLayout layout, object context, 
			FormulaHolder formulaHolder ) : base( layout, context )
		{
			this.formulaHolder = formulaHolder;
		}

		#endregion // Constructor

		#region FormulaHolder
		
		internal FormulaHolder FormulaHolder
		{
			get
			{
				return this.formulaHolder;
			}
		}

		#endregion // FormulaHolder

		#region HasFormulaSyntaxError
		
		internal bool HasFormulaSyntaxError
		{
			get
			{
				return null != this.Formula && this.Formula.HasSyntaxError;
			}
		}

		#endregion // HasFormulaSyntaxError

		#region FormulaSyntaxError
		
		internal string FormulaSyntaxError
		{
			get
			{
				return this.HasFormulaSyntaxError ? this.Formula.SyntaxError : null;
			}
		}

		#endregion // FormulaSyntaxError

		#region EnsureFormulaRegistered

		internal void EnsureFormulaRegistered( string formulaString )
		{
			if ( null != this.Formula && formulaString == this.Formula.FormulaString )
				return;

			this.RegisterFormula( formulaString );
		}

		#endregion // EnsureFormulaRegistered

		#region OnFormulaStringChanged

		internal void RegisterFormula( string newFormulaString )
		{
			IUltraCalcManager calcManager = this.CalcManager;

			if ( null != calcManager )
			{
				// Remove the old formula first.
				//
				if ( null != this.formula )
				{
					// SSP 2/4/05
					// Use the AddFormulaHelper and RemoveFormulaHelper utility methods 
					// instead. See comments above those method definitions for more info.
					//
					//calcManager.RemoveFormula( this.formula );
					// MD 1/28/09 - TFS13109
					// We need to clear the formula before calling RemoveFormulaHelper. If we don't, there is a weird timing issue with synchronous 
					// calculations where removing the formula tries to recalculate it and it causes a null reference exception.
					//RefUtils.RemoveFormulaHelper( calcManager, this.formula );
					IUltraCalcFormula tempFormula = this.formula;
					this.formula = null;
					RefUtils.RemoveFormulaHelper( calcManager, tempFormula );
				}

				// MD 1/28/09 - TFS13109
				// The formula is already being cleared above if it is non-null.
				//this.formula = null;

				// SSP 6/24/05 BR04577
				// If dummyFormula is true then this summary is using a formula strictly to emulate
				// dependancy chaining of non-formula summaries. If a non-summary formula is referenced
				// by a formula whenever the source column of the summary is changed, this formula needs
				// to be added to the calc chain. For example, B() = A (non-formula), C = B(). When A
				// changes, B is considered dirty and C needs to be enqued into the re-calc chain. A 
				// dummy formula is used for this.
				// 
				// ------------------------------------------------------------------------------------
				SummarySettingsReference summaryRef = this as SummarySettingsReference;
				if ( null != summaryRef )
				{
					// SSP 12/28/05 BR08479
					// Moved this from inside the below if block. 
					// Reset the dummyFormula flag.
					// 
					summaryRef.dummyFormula = false;

					if ( ! summaryRef.IsDisposedReference 
						&& ( null == newFormulaString || 0 == newFormulaString.Length ) )
					{
						// SSP 12/28/05 BR08479
						// Moved this above, outside of this if block.
						// 
						

						UltraGridColumn sourceCol = summaryRef.Summary.SourceColumn;
						string sourceColRefName = null != sourceCol ? sourceCol.FormulaHolder.ReferenceName : null;
						if ( null != sourceColRefName && sourceColRefName.Length > 0 )
						{
							string dummyFormula = "SUM( [" + sourceColRefName + "] )";

							// Set the dummyFormula to true to indicate that the formula results should be
							// discarded.
							// 
							summaryRef.dummyFormula = true;
							newFormulaString = dummyFormula;
						}
					}
				}
				// ------------------------------------------------------------------------------------

				if ( null != newFormulaString && newFormulaString.Length > 0 )
				{
					this.formula = calcManager.CompileFormula( this, newFormulaString );

					if ( this.formula.HasSyntaxError )
						return;
				
					// SSP 2/4/05
					// Use the AddFormulaHelper and RemoveFormulaHelper utility methods 
					// instead. See comments above those method definitions for more info.
					//
					//calcManager.AddFormula( formula );
					RefUtils.AddFormulaHelper( calcManager, this.formula );
				}
			}
		}

		#endregion // OnFormulaStringChanged

		#region RefBase Overrides

		#region Formula

		public override IUltraCalcFormula Formula
		{
			get 
			{
				return this.formula;
			}
		}

		#endregion // Formula

		#region HasFormula

		internal bool HasFormula
		{
			get
			{
				return null != this.Formula;
			}
		}

		#endregion // HasFormula

		#region CreateReference
		
		private RefBase CreateReferenceHelper( string referenceName )
		{
			Debug.Assert( this is ColumnReference || this is SummarySettingsReference );

			UltraGridBand band = this is SummarySettingsReference 
				? ((SummarySettingsReference)this).Summary.Band
				: ((ColumnReference)this).Column.Band;

			System.Text.RegularExpressions.Match match =
				System.Text.RegularExpressions.Regex.Match(
					referenceName, @"^(\.\.\/)*([^\/\(]+)(\(\s*(\*?)\s*\))?$" );

			if ( null != match && match.Success )
			{
				string name = match.Groups[2].Value;

				// Determine if the reference is a summary or a column.
				// Something(*) and Something are column references where as Something()
				// is a summary reference.
				//
				bool isSummary = 0 != match.Groups[3].Value.Length && 0 == match.Groups[4].Value.Length;
				bool isColumnWithSubStar = ! isSummary && "*" == match.Groups[4].Value;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//int doubleDotsCount = match.Groups[1].Captures.Count;

				if ( isSummary )
				{
					SummarySettings summary = RefUtils.GetSummarySettings( band, name  );
					if ( null != summary )
					{
						// The following code is for maintaining the same group-by level summary tokens. 
						// For example, with State and City group-by columns, s1() = [s2()] formula
						// is associated with the root level, State/s1() = [State/s2()] is associated
						// with the second level, and State/City/s1() = [State/City/s2()] is associated
						// with the city level.
						//
						GroupLevelSummarySettingsReference glsr = this as GroupLevelSummarySettingsReference;
						return null != glsr 
							? summary.Band.CalcReference.GetSummarySettingsReference( summary, glsr.GroupByColumn )
							: RefUtils.GetSummarySettingsReference( summary );
					}
				}
				else if ( isColumnWithSubStar )
				{
					UltraGridColumn column = RefUtils.GetColumn( band, name, false );
					if ( null != column )
						return RefUtils.GetColumnReference( column );
				}
			}

			return null;
		}

		// SSP 9/8/04
		// Overrode CreateReference method so from the formula base references we can return
		// column and summary formula base references from the CreateReference. This fixes
		// the problem in Connect/Reconnect code of the calc engine where a token referring
		// to a column reference gets disconnected and reconnected to a NestedColumnReference.
		//
		public override IUltraCalcReference CreateReference( string referenceName )
		{
			Debug.Assert( this is ColumnReference || this is SummarySettingsReference );

			// We are going to treat "Column(*)" and "Summary()" referneces, optionally
			// preceded by series of "../", specially to support group-by stuff.
			//
			// SSP 9/13/04 UWC89
			// First attempt to see if the specified reference exists in the ancestor/sibling
			// bands. For example, with a reference of "../../Column 1", if Column 1 exists in
			// the parent band then we want to return that regardless of whether there is such
			// a column in this band and there are group-by columns in this band.
			// Also if this is a group level summary reference then call CreateReference on the
			// top level summary settings reference because that's the reference relative to 
			// which the user would've entered the relative references in the formula. For example,
			// in a group-by situation with State and City group-by columns in Orders band, 
			// //ultraGrid1/Customers/Orders/Total() == "../../CustomerID" should get the CustomerID
			// of the Customers band and not from the Orders band as far as the user is thinking of
			// when he enters the formula. As far the user sees it, it's relative to the Orders
			// band and not relative to //ultraGrid1/Customers/State/Total() or 
			// //ultraGrid1/Customers/State/City/Total().
			// 
			IUltraCalcReference createdReference = this is GroupLevelSummarySettingsReference 
				? (IUltraCalcReference)((GroupLevelSummarySettingsReference)this).Summary.CalcReference.CreateReference( referenceName )
				: (IUltraCalcReference)base.CreateReference( referenceName );

			if ( null == createdReference || createdReference is UltraCalcReferenceError )
			{				 
				RefBase tmpReference = this.CreateReferenceHelper( referenceName );
				if ( null != tmpReference )
				{
					RefUnAnchored ru = new RefUnAnchored( tmpReference );
					ru.RelativeReference = new RefParser( referenceName );
					createdReference = ru;
				}
			}

			return createdReference;
		}

		#endregion // CreateReference

		#endregion // RefBase Overrides
	}

	#endregion // FormulaRefBase Class

	#region FormulaTargetRefBase Class

	internal abstract class FormulaTargetRefBase : UltraGridRefBase
	{
		#region Private Variables

		protected UltraCalcValue formulaEvaluationResult = null;
		internal int verifiedValueDirtyCounter = -1;

		// The formula ref class that contains this target ref. For example, a column reference
		// contains all the associated cell references. So in the case of a cell reference,
		// containingFormulaRef is the column reference.
		//
		private FormulaRefBase containingFormulaRef = null;

		// This is associated with the FormulaCalculationErrorEventArgs.ErrorDisplayText property.
		//
		internal string formulaCalculationErrorDisplayText = null;

		// This is associated with the FormulaCalculationErrorEventArgs.ErrorValue property.
		//
		internal object formulaCalculatiorErrorStoreValue = null;

		internal int verifiedRecalcVisibleVersion = -1;

		#endregion // Private Variables

		#region FormulaTargetRefBase

		protected FormulaTargetRefBase( UltraGridLayout layout, FormulaRefBase containingFormulaRef, object context ) : base( layout, context )
		{
			if ( null == containingFormulaRef )
				throw new ArgumentNullException( "containingFormulaRef" );

			this.containingFormulaRef = containingFormulaRef;

			// SSP 8/18/04
			// By default mark everything visible (that is it needs to be calculated). The
			// row iterator is the one that will return only the rows that are visible in the
			// scroll regions.
			// 
			this.RecalcVisible = true;
		}

		#endregion // FormulaTargetRefBase
		
		#region ContainingFormulaRef
		
		internal FormulaRefBase ContainingFormulaRef
		{
			get
			{
				return this.containingFormulaRef;
			}
		}

		#endregion // ContainingFormulaRef

		#region FormulaEvaluationResult
		
		internal UltraCalcValue FormulaEvaluationResult
		{
			get
			{
				return this.formulaEvaluationResult;
			}
		}

		#endregion // FormulaEvaluationResult

		#region HasFormulaSyntaxError
		
		internal bool HasFormulaSyntaxError
		{
			get
			{
				return this.containingFormulaRef.HasFormulaSyntaxError;
			}
		}

		#endregion // HasFormulaSyntaxError

		#region FormulaSyntaxError
		
		internal string FormulaSyntaxError
		{
			get
			{
				return this.containingFormulaRef.FormulaSyntaxError;
			}
		}

		#endregion // FormulaSyntaxError

		#region Formula

		public override IUltraCalcFormula Formula
		{
			get 
			{
				return this.containingFormulaRef.Formula;
			}
		}

		#endregion // Formula

		#region HasFormula
		
		internal bool HasFormula
		{
			get
			{
				return null != this.Formula;
			}
		}

		#endregion // HasFormula

		#region IsValueDirty
		
		internal bool IsValueDirty
		{
			get
			{
				return this.verifiedValueDirtyCounter < this.containingFormulaRef.valueDirtyCounter 
					&& this.HasFormula
					// SSP 1/18/05 BR01753
					// Only enable calculations in display layout. Print and export layouts will
					// copy over the calculated values from the display layout.
					//
					&& this.Layout.IsDisplayLayout;
			}
		}

		#endregion // IsValueDirty

		#region Value

		public override UltraCalcValue Value
		{
			get
			{
				return this.formulaEvaluationResult;
			}
			set
			{
				this.formulaEvaluationResult = value;
				this.verifiedValueDirtyCounter = this.containingFormulaRef.valueDirtyCounter;

				// Reset the error display text and the error store value when we get a set
				// on the Value.
				//
				this.formulaCalculationErrorDisplayText = null;
				this.formulaCalculatiorErrorStoreValue = null;
				if ( null != (object)value && value.IsError )
				{
					FormulaCalculationErrorEventArgs e = 
						this.CalcManager.RaiseFormulaError( this, null,
							value.ToErrorValue(), null );

					if ( null != e )
					{
						this.formulaCalculatiorErrorStoreValue = e.ErrorValue;
						this.formulaCalculationErrorDisplayText = e.ErrorDisplayText;
						if ( "" == this.formulaCalculationErrorDisplayText )
							this.formulaCalculationErrorDisplayText = null;
					}
				}

				// Repaint the necessary portions of the grid control to reflect the new
				// calculated value on the screen.
				//
				this.InvalidateUIElement( );
			}
		}

		#endregion // Value

		// SSP 9/6/04
		// Overrode RecalcVisible. There is a problem with the canc-engine that needs to be
		// fixed. There shouldn't be any need to override this if that problem is fixed.
		//
		#region RecalcVisible

		public override bool RecalcVisible
		{
			get
			{
				if ( this.verifiedRecalcVisibleVersion != this.containingFormulaRef.visibleCounter )
					return true;

				return base.RecalcVisible;
			}
			set
			{
				this.verifiedRecalcVisibleVersion = this.containingFormulaRef.visibleCounter;

				base.RecalcVisible = value;
			}
		}

		#endregion // RecalcVisible

		#region InternalCopyFrom

		// SSP 1/18/05 BR01753
		// Only enable calculations in display layout. Print and export layouts will
		// copy over the calculated values from the display layout.
		//
		internal void InternalCopyValueFrom( FormulaTargetRefBase source )
		{
			this.formulaEvaluationResult = source.formulaEvaluationResult;
			this.formulaCalculationErrorDisplayText = source.formulaCalculationErrorDisplayText;
			this.formulaCalculatiorErrorStoreValue = source.formulaCalculatiorErrorStoreValue;
		}

		#endregion // InternalCopyFrom
	}

	#endregion // FormulaTargetRefBase Class

	#region CellReference Class

	internal class CellReference : FormulaTargetRefBase
	{
		#region Private Variables

		private UltraGridCell cell = null;
		internal bool inValueSet = false;

		#endregion // Private Variables

		#region ParserInstanceVersion

		// Parsed references on rows, cells, rows collections and nested column references
		// have to be recreated whenever their absolute names change due to a row getting
		// inserted, deleted, the rows collection getting sorted or resynced. For example,
		// When the first row is deleted from a rows collection then the next row's absolute
		// name changes from //ultraGrid1/Customers(1) to //ultraGrid1/Customers(0). However
		// the parsed reference as returned by RefBase.ParsedReference property is still 
		// based on the old absolute name. So we need to recreate the parsed reference.
		// 
		internal override int ParserInstanceVersion
		{
			get
			{
				RowsCollection rows = this.Cell.Row.ParentCollection;
				return null != rows ? rows.CalcReference.ParserInstanceVersion : 0;
			}
		}

		#endregion // ParserInstanceVersion

		#region Constructor
		
		internal CellReference( UltraGridCell cell ) : base( cell.Layout, cell.Column.CalcReference, cell )
		{
			this.cell = cell;
		}

		#endregion // Constructor

		#region Cell
		
		internal UltraGridCell Cell
		{
			get
			{
				return this.cell;
			}
		}

		#endregion // Cell

		#region ColumnContext

		internal override UltraGridColumn ColumnContext
		{
			get
			{
				return this.Cell.Column;
			}
		}

		#endregion // ColumnContext

		#region RefBase Overrides

		public override bool IsDataReference 
		{
			get 
			{
				return true;
			} 
		}

		public override string ElementName 
		{
			get 
			{
				return this.Cell.Column.FormulaHolder.ReferenceName;
			}
		}

		public override RefBase BaseParent
		{ 
			get 
			{
				return RefUtils.GetRowReference( this.Cell.Row );
			}
		}


		public override IUltraCalcReference FindItem( string name )
		{
			return this.BaseParent.FindItem( name );
			//return this.FindItemHelper( name, false );
		}

		public override IUltraCalcReference FindAll( string name )
		{
			return this.BaseParent.FindAll( name );
		}

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return this.BaseParent.FindSummaryItem( name );
		}

		public override IUltraCalcReference FindItem( string name, string index )
		{
			return this.BaseParent.FindItem( name, index );
		}

		public override IUltraCalcReference FindItem( string name, int index, bool isRelative )
		{
			return this.BaseParent.FindItem( name, index, isRelative );
		}

		#region ResolveReference

		public override IUltraCalcReference ResolveReference( IUltraCalcReference reference, ResolveReferenceType referenceType )
		{
			IUltraCalcReference tmp = this.Cell.Row.CalcReference.ResolveReferenceHelper( reference, referenceType, this.Cell );
			if ( null != tmp )
				return tmp;

			return base.ResolveReference( reference, referenceType );
		}

		#endregion // ResolveReference

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get 
			{ 
				return null == this.cell 
					|| this.cell.Disposed 
					|| null == this.cell.Row
					|| null == this.cell.Column 
					|| ! this.cell.Column.IsStillValid 
					|| this.cell.Row.Disposed 
					|| ! this.cell.Row.IsStillValid; 
			}
		}

		#endregion // IsDisposedReference

		// SSP 10/12/04
		// Overrode GetHashCode and Equals. Calc engine will make use of these instead of
		// normalized absolute name comparisions.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			return this.cell.GetHashCode( );
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored instance then delegate the call to it.
			//
			if ( obj is RefUnAnchored )
				return obj.Equals( this );

			CellReference cellRef = obj as CellReference;
			return null != cellRef && this.cell == cellRef.cell;
		}

		#endregion // Equals
       
		#endregion // RefBase Overrides
		
		#region IUltraCalcReference Overrides

		public override UltraCalcValue Value
		{
			get
			{
				UltraCalcValue val = base.Value;
				if ( null != (object)val && this.HasFormula )
					return val;

				return RefUtils.GetCalcValue( this.Cell.Value, this );
			}
			set
			{
				// MD 3/25/09 - TFS15830
				// Cache the original calc reference value because we may have to restore it later.
				UltraCalcValue originalValue = base.Value;

				base.Value = value;

				// SSP 1/17/05 BR01753
				// If the cell is disposed then don't bother setting the value.
				//
				if ( this.Cell.Disposed )
					return;

				bool isError;
				object newCellValue = RefUtils.GetValueFormCalcValue( value, this.Cell.Column.FormatInfo, out isError, this );

				// Set the inValueSet to true so that we don't set the DataChanged on the
				// row to true and cause the pencil icon to show up in the row selector when 
				// we are setting a cell values due to a formula evaluation.
				//
				// --------------------------------------------------------------------------
				bool origInValueSet = this.inValueSet;
				RowReference rowReference = (RowReference)this.Cell.Row.CalcReference;
				bool origInRowCellReferenceValueSet = rowReference.inCellReferenceValueSet;
				this.inValueSet = true;
				rowReference.inCellReferenceValueSet = true;
				try
				{
					// SSP 6/7/05 BR04458
					// Fire the InitializeRow event.
					// 
					// --------------------------------------------------------------------------
					
					if ( isError )
					{
						// If an error value was specified in the FormulaCalculationError event then
						// use that otherwise use the FormulaErrorValue setting.
						//
						newCellValue = null != this.formulaCalculatiorErrorStoreValue 
							? this.formulaCalculatiorErrorStoreValue
							: this.Cell.Column.FormulaErrorValueResolved;
					}

					// SSP 2/1/2007 BR19496
					// If the value being set on the cell is of different type than the column's 
					// data type then convert it first to the column's data type before setting 
					// it. This is to prevent the SetValueInternal from raising Before/After Cell
					// Update events unnecessarily if the new value and the old value is the 
					// same. The reason for this change is that the cell's SetValueInternal first 
					// compares the new value with the old value and returns if they are the 
					// same. It converts the new value to the column's data type after this 
					// comparison. It would make more sense for it to convert before and then do 
					// the comparison. However in the interest of minimizing side-effects, we 
					// will localize this change to here.
					// 
					// --------------------------------------------------------------------------
					UltraGridCell cell = this.Cell;
					UltraGridColumn column = cell.Column;
					if ( null != newCellValue && DBNull.Value != newCellValue 
						&& ! GridUtils.IsObjectOfType( newCellValue, column.DataType ) )
					{
						object tmp = column.ConvertValueToDataType( newCellValue );
						if ( null != tmp )
							newCellValue = tmp;
					}
					// --------------------------------------------------------------------------

					// MD 3/25/09 - TFS15830
					// Setting the value may cause an exception if the calculated value doesn't match the column's data type.
					// Events need to be fired for this so it can be handled by the application.
					//cell.SetValueInternal( newCellValue, true, true );
					try
					{
						cell.SetValueInternal( newCellValue, true, true );
					}
					catch ( Exception exc )
					{
						UltraGrid grid = cell.Layout.Grid as UltraGrid;

						if ( grid != null )
						{
							CellDataErrorEventArgs e = new CellDataErrorEventArgs( true );
							grid.FireEvent( GridEventIds.CellDataError, e );

							if ( e.RaiseErrorEvent )
							{
								DataErrorInfo dataErrorInfo = new DataErrorInfo(
									exc,
									newCellValue,
									cell.Row,
									cell,
									DataErrorSource.CellUpdate,
									SR.GetString( "DataErrorCellUpdateUnableToUpdateValue", ( null != exc ? exc.Message : "" ) ) );

								grid.InternalHandleDataError( dataErrorInfo, true );
							}

							base.Value = e.RestoreOriginalValue
								? originalValue
								: new UltraCalcValue( new UltraCalcErrorValue( UltraCalcErrorCode.Value, SR.GetString( "DataErrorCalculatedValueDoesntMatchDataType" ) ) );
						}
					}
					// --------------------------------------------------------------------------
				}
				finally
				{
					// Reset the flags to their original values.
					//
					this.inValueSet = origInValueSet;
					rowReference.inCellReferenceValueSet = origInRowCellReferenceValueSet;
				}
				// --------------------------------------------------------------------------
			}
		}

		#endregion // IUltraCalcReference Overrides

		#region InvalidateUIElement
		
		internal override void InvalidateUIElement( )
		{
			if ( null != this.Cell )
				this.Cell.InvalidateItemAllRegions( );
		}

		#endregion // InvalidateUIElement
	}

	#endregion // CellReference Class

	#region SummaryValueReference Class

	internal class SummaryValueReference : FormulaTargetRefBase
	{
		#region Private Variables

		private SummaryValue summaryValue = null;

		#endregion // Private Variables

		#region Constructor
		
		// SSP 9/12/04
		//
		//internal SummaryValueReference( SummaryValue summaryValue ) : base( summaryValue.Layout, summaryValue.SummarySettings.CalcReference, summaryValue )
		internal SummaryValueReference( SummaryValue summaryValue ) 
			: base( summaryValue.Layout, 
					// SSP 6/13/05 BR04539
					// Use the new overload of GetSummarySettingsReference instead.
					// 
					//summaryValue.Band.CalcReference.GetSummarySettingsReference2(
					//		summaryValue.SummarySettings, summaryValue.ParentRows.GroupByColumn ),
					summaryValue.Band.CalcReference.GetSummarySettingsReference( summaryValue ),
					summaryValue )
		{
			this.summaryValue = summaryValue;
		}

		#endregion // Constructor

		#region SummaryValue
		
		internal SummaryValue SummaryValue
		{
			get
			{
				return this.summaryValue;
			}
		}

		#endregion // SummaryValue

		#region ParserInstanceVersion

		// Parsed references on rows, cells, rows collections and nested column references
		// have to be recreated whenever their absolute names change due to a row getting
		// inserted, deleted, the rows collection getting sorted or resynced. For example,
		// When the first row is deleted from a rows collection then the next row's absolute
		// name changes from //ultraGrid1/Customers(1) to //ultraGrid1/Customers(0). However
		// the parsed reference as returned by RefBase.ParsedReference property is still 
		// based on the old absolute name. So we need to recreate the parsed reference.
		// 
		internal override int ParserInstanceVersion
		{
			get
			{
				RowsCollection rows = this.SummaryValue.ParentRows;
				return null != rows ? rows.CalcReference.ParserInstanceVersion : 0;
			}
		}

		#endregion // ParserInstanceVersion

		#region RefBase Overrides

		public override bool IsDataReference 
		{
			get 
			{
				return true;
			} 
		}

		public override string ElementName 
		{
			get 
			{
				return this.SummaryValue.SummarySettings.CalcReference.ElementName;
			}
		}

		public override RefBase BaseParent
		{ 
			get 
			{
				return RefUtils.GetRowsReference( this.SummaryValue.ParentRows );
			}
		}

		public override IUltraCalcReference FindItem( string name )
		{
			return RefUtils.GetNestedColumnReference( this.SummaryValue.ParentRows, name );
		}

		public override IUltraCalcReference FindSummaryItem( string name )
		{
			return RefUtils.GetSummaryValueReference( this.SummaryValue.ParentRows, name );
		}

		public override IUltraCalcReference FindItem( string name, string index )
		{
			IUltraCalcReference colRef = RefUtils.GetNestedColumnReference( this.SummaryValue.ParentRows, name );

			if (!(colRef is NestedColumnReference))
			{
				Debug.Assert(colRef is UltraCalcReferenceError, "Unexpected nested column reference type.");
				return colRef;
			}

			return ((NestedColumnReference)colRef).FindItem( name, index );
		}

		public override IUltraCalcReference FindItem( string name,int index,bool isRelative )
		{
			IUltraCalcReference colRef = RefUtils.GetNestedColumnReference( this.SummaryValue.ParentRows, name );

			if (!(colRef is NestedColumnReference))
			{
				Debug.Assert(colRef is UltraCalcReferenceError, "Unexpected nested column reference type.");
				return colRef;
			}

			return ((NestedColumnReference)colRef).FindItem( name, index, isRelative );
		}

		public override IUltraCalcReference FindAll( string name )
		{
			return this.FindItem( name );
		}

		#region IsDisposedReference

		// SSP 9/1/04
		// Added IsDisposedReference.
		//
		public override bool IsDisposedReference
		{
			get 
			{
				return null == this.summaryValue || !this.summaryValue.IsStillValid
					// SSP 9/21/06 BR16000 
					// 
					|| this.summaryValue.ParentRows.CalcReference.IsDisposedReference;
			}
		}

		#endregion // IsDisposedReference

		#endregion // RefBase Overrides
		
		#region IUltraCalcReference Overrides

		public override UltraCalcValue Value
		{
			get
			{
				// SSP 6/24/05 BR04577
				// If dummyFormula is true then this summary is using a formula strictly to emulate
				// dependancy chaining of non-formula summaries. If a non-summary formula is referenced
				// by a formula whenever the source column of the summary is changed, this formula needs
				// to be added to the calc chain. For example, B() = A (non-formula), C = B(). When A
				// changes, B is considered dirty and C needs to be enqued into the re-calc chain. A 
				// dummy formula is used for this.
				// Enclosed the existing code into the if block. Don't return the calculated value when
				// using dummy formula.
				// 
				if ( ! this.ContainingFormulaRef.dummyFormula )
				{
					UltraCalcValue val = base.Value;
					if ( null != (object)val && this.HasFormula )
						return val;
				}

				return RefUtils.GetCalcValue( this.SummaryValue.Value, this );
			}
			set
			{
				// SSP 6/24/05 BR04577
				// If dummyFormula is true then this summary is using a formula strictly to emulate
				// dependancy chaining of non-formula summaries. If a non-formula summary is referenced
				// by a formula whenever the source column of the summary is changed, this formula needs
				// to be added to the calc chain. For example, B() = A (non-formula), C = B(). When A
				// changes, B is considered dirty and C needs to be enqued into the re-calc chain. A 
				// dummy formula is used for this.
				// 
				if ( this.ContainingFormulaRef.dummyFormula )
					return;

				// SSP 6/10/05 BR04553
				// Fire SummaryValueChanged notification.
				// 
				// ------------------------------------------------------------------
				//base.Value = value;

				// Get the old value.
				//
				object oldVal = base.Value;

				// Set the new calc value by calling the base implementation.
				// 
				base.Value = value;

				// Get the new value. SummaryValue's Value property makes use of the 
				// calc value we just set.
				// 
				object newVal = this.SummaryValue.Value;
				if ( ! this.IsDisposedReference )
					this.SummaryValue.OnSummaryValueChanged( oldVal, newVal );
				// ------------------------------------------------------------------
			}
		}

		#region InvalidateUIElement
		
		internal override void InvalidateUIElement( )
		{
			if ( null != this.SummaryValue )
				this.SummaryValue.InvalidateItemAllRegions( );
		}

		#endregion // InvalidateUIElement

		#region ResolveReference

		public override IUltraCalcReference ResolveReference( IUltraCalcReference reference, ResolveReferenceType referenceType )
		{
			if( !(reference is RefBase) ) return reference;

            RowsCollectionReference rowsRef = RefUtils.GetRowsReference( this.SummaryValue.ParentRows );
			IUltraCalcReference retRef = rowsRef.ResolveReference( reference, referenceType );

			// SSP 10/29/04 - Formula Row Index Source
			// This is for formula row index source support.
			//
			NestedColumnReference nestedColRef = retRef as NestedColumnReference;
			if ( null != nestedColRef && null != nestedColRef.rowsParameter 
				&& nestedColRef.rowsParameter != nestedColRef.Rows )
				return new NestedColumnReference( nestedColRef.rowsParameter, nestedColRef.Column, true );

			return retRef;
		}

		#endregion // ResolveReference

		// SSP 10/12/04
		// Overrode GetHashCode and Equals. Calc engine will make use of these instead of
		// normalized absolute name comparisions.
		//
		#region GetHashCode

		public override int GetHashCode( )
		{
			return this.summaryValue.GetHashCode( );
		}

		#endregion // GetHashCode

		#region Equals

		public override bool Equals( object obj )
		{
			// If the object is a RefUnAnchored instance then delegate the call to it.
			//
			if ( obj is RefUnAnchored )
				return obj.Equals( this );

			SummaryValueReference summaryValRef = obj as SummaryValueReference;
			return null != summaryValRef && this.summaryValue == summaryValRef.summaryValue;
		}

		#endregion // Equals

		#endregion // IUltraCalcReference Overrides
	}

	#endregion // SummaryValueReference Class

	#region RefCellCollection Class

	internal class RefCellCollection : IUltraCalcReferenceCollection
	{
		#region Private Variables

		private UltraGridColumn column = null;
		private RefParser scopeRP = null;
		private UltraGridRefBase referenceBeingEnumerated = null;

		#endregion // Private Variables

		#region Constructor

		public RefCellCollection( UltraGridRefBase referenceBeingEnumerated, UltraGridColumn column, RefParser scopeRP )
		{
			this.referenceBeingEnumerated = referenceBeingEnumerated;
			this.column = column;
			this.scopeRP = scopeRP;
		}

		#endregion // Constructor

		#region IEnumerable

		public IEnumerator GetEnumerator( )
		{
			return new RefCellCollectionEnumerator(this);
		}

		#endregion //IEnumerable

		#region RefCellCollectionEnumerator

		private class RefCellCollectionEnumerator : IEnumerator
		{
			private RefCellCollection collection;
			private UltraGridCell cell = null;
			private RefRowIterator rowIterator = null;

			internal RefCellCollectionEnumerator(RefCellCollection collection)
			{
				this.collection = collection;
				this.rowIterator = RefRowIterator.CreateRowIterator( collection.column.Layout, collection.scopeRP, collection.referenceBeingEnumerated );
			}

			public object Current 
			{
				get 
				{
					if ( null == this.cell ) 
						throw new InvalidOperationException( );

					return RefUtils.GetCellReference( this.cell );
				}
			}

			public bool MoveNext( )
			{
				if ( ! this.rowIterator.MoveNext( ) ) 
					return false;

				this.cell = this.rowIterator.Current.Cells[ this.collection.column ];
				return null != this.cell;
			}

			public void Reset( )
			{
				this.cell = null;
				this.rowIterator.Reset( );
			}
		}
		#endregion
	}

	#endregion // RefCellCollection Class

	#region RefRowIterator Class

	internal class RefRowIterator
	{
		private UltraGridBand band = null;
		private RefTuple tuple = null;
		private RefTuple childTuple = null;
		private RefRowIterator child = null;

		private RowsCollection rows = null;
		private UltraGridRow row = null;

		private bool recalcDeferredIterator = false;

		// SSP 10/29/04 - Formula Row Index Source
		// 
		bool usedInRangeRecalc = false;
		private FormulaRowIndexSource rowIndexType = FormulaRowIndexSource.Default;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private RefRowIterator lowestLevelIterator = null;

		// SSP 11/9/04 UWC51
		// RefRowIterator is being used for traversing summaries as well. If a row collection
		// contains no rows then before we were skipping the summaries of that row collection.
		// This is to fix that problem.
		//
		private bool firstTime = true;

		private RefRowIterator( UltraGridBand band, RefTuple tuple )
		{
			this.band = band;
			this.tuple = tuple;
			this.rowIndexType = this.band.FormulaRowIndexSourceResolved;
		}

		internal static RefRowIterator CreateRowIterator( UltraGridLayout layout, 
			RefParser rp, UltraGridRefBase referenceBeingEnumerated )
		{
			// SSP 9/11/04
			// Since we are adding more than one LHS for summaries in group-by situation
			// we should only loop through the summaries of the LHS that's being enumerated.
			//			
            UltraGridBand exemptBandFromInsertingGroupByTuples = null;

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( referenceBeingEnumerated is SummarySettingsReference )
			//    exemptBandFromInsertingGroupByTuples = ((SummarySettingsReference)referenceBeingEnumerated).Summary.Band;
			//else if ( referenceBeingEnumerated is NestedSummarySettingsReference )
			//    exemptBandFromInsertingGroupByTuples = ((NestedSummarySettingsReference)referenceBeingEnumerated).Summary.Band;
			SummarySettingsReference summarySettingsRef = referenceBeingEnumerated as SummarySettingsReference;

			if ( summarySettingsRef != null )
				exemptBandFromInsertingGroupByTuples = summarySettingsRef.Summary.Band;
			else
			{
				NestedSummarySettingsReference nestedSummarySettingsRef = referenceBeingEnumerated as NestedSummarySettingsReference;

				if ( nestedSummarySettingsRef != null )
					exemptBandFromInsertingGroupByTuples = nestedSummarySettingsRef.Summary.Band;
			}

			// SSP 10/29/04 - Formula Row Index Source Type
			// This is to support row index type of ListIndex in group-by mode.
			//
			// ------------------------------------------------------------------------------------------
			bool usedInRangeRecalc = false;
			if ( referenceBeingEnumerated is ColumnReference || referenceBeingEnumerated is NestedColumnReference )
			{
				UltraGridColumn column = ((UltraGridRefBase)referenceBeingEnumerated).ColumnContext;
				if ( null != column && FormulaRowIndexSource.ListIndex == column.Band.FormulaRowIndexSourceResolved )
				{
					RowsCollection rows = referenceBeingEnumerated is ColumnReference ? layout.Rows 
						: ((NestedColumnReference)referenceBeingEnumerated).Rows;

					if ( null != rows && ( rows.Band != column.Band || rows.IsTopLevel ) )
						usedInRangeRecalc = true;
				}
			}
			// ------------------------------------------------------------------------------------------

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = RefUtils.InsertGroupByColumnTuples( layout, rp, exemptBandFromInsertingGroupByTuples );
			List<RefTuple> list = RefUtils.InsertGroupByColumnTuples( layout, rp, exemptBandFromInsertingGroupByTuples );

			if ( null == list || 0 == list.Count )
			{
				// AS 10/8/04
				// Don't throw an exception. This is currently called during the creation
				// of the refcell and summary enumerators.
				//
				//throw new Exception( string.Format( "Invalid reference: {0}", rp.ToString( ) ) );
				return null;
			}

			bool recalcDeferredIterator = referenceBeingEnumerated.RecalcDeferred;
			UltraGridBand lastBand = null;
			RefTuple lastBandTuple = null;
			RefRowIterator rootIterator = null;
			RefRowIterator lastBandRowIterator = null;

			// This flag keeps track of whether we've already created an iterator for this band so
			// we don't create another one. The reason for having this is that in the case of the
			// group-by the band tuple is repeated in the reference name.
			//
			bool lastBandProcessed = false;

			foreach ( RefTuple tuple in list )
			{
				// See if any of the tuple in the reference requires that the iterator be not 
				// recalc deferred iterator.
				//
				recalcDeferredIterator = recalcDeferredIterator 
					&& ( RefTuple.RefScope.Any == tuple.Scope || RefTuple.RefScope.Index == tuple.Scope );

				// SSP 10/15/04
				// Check for the scope type of the tuple. If it's summary then don't get the column.
				//
				//UltraGridColumn column = null != lastBand 
				UltraGridColumn column = null != lastBand && RefTuple.RefScope.SummaryValue != tuple.Scope 
					? RefUtils.GetColumn( lastBand, tuple.Name, false ) : null;
                
				RefRowIterator ri = null;

				// If the lastBand has not been initialized then that means the the first tuple should
				// be the root band tuple. If the lastBand has been initialized and the current tuple 
				// is a chaptered column (which must be associated with the child band of the last band
				// for the reference to be a valid reference) then get the associated child band.
				//
				if ( null == lastBand || null != column && column.IsChaptered )
				{
					lastBand = RefUtils.GetBand( layout, lastBand, tuple.Name );
					lastBandTuple = tuple;
					lastBandProcessed = false;

					// If the band has group-by columns, then ignore the first band tuple
					// because we are repeating the band tuple twice.
					//
					if ( ! lastBand.HasGroupBySortColumns )
					{
						ri = new RefRowIterator( lastBand, tuple );
						lastBandProcessed = true;
					}
				}
				else
				{
					// Create a child row iterator if we encounter a group-by or band tuple.
					//
					if ( ! lastBandProcessed )
					{
						if ( RefTuple.RefScope.SummaryValue != tuple.Scope
							&& lastBand.NormalizedCalcReferenceName == tuple.Name )
						{
							lastBandProcessed = true;
							ri = new RefRowIterator( lastBand, tuple );
						}
						else if ( null != column && column.IsGroupByColumn )
						{
							ri = new RefRowIterator( lastBand, tuple );
						}
						else
						{
							// SSP 9/11/04
							// This is the case where the current tuple is something other than
							// the group-by column even though the band has group-by columns. 
							// This could occur we are exmpting a band from inserting group-by
							// tuples (see the beginning of the method for more info).
							//
							lastBandProcessed = true;
							ri = new RefRowIterator( lastBand, lastBandTuple );
							ri.childTuple = tuple;
						}
					}
					else
					{
						lastBandRowIterator.childTuple = tuple;
					}
				}

				if ( null != ri )
				{
					if ( null == rootIterator )
						rootIterator = ri;

					if ( null != lastBandRowIterator )
					{
						lastBandRowIterator.child = ri;
						lastBandRowIterator.childTuple = ri.tuple;
					}

					lastBandRowIterator = ri;
				}
			}

			// Set the recalcDeferredIterator flag on all the iterators starting from the root iterator.
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//RefRowIterator lowestLevelRowIterator = lastBandRowIterator;

			lastBandRowIterator = rootIterator;
			while ( null != lastBandRowIterator )
			{
				// SSP 10/29/04 - Formula Row Index Source
				//
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//lastBandRowIterator.lowestLevelIterator = lowestLevelRowIterator;

				lastBandRowIterator.usedInRangeRecalc = usedInRangeRecalc;

				lastBandRowIterator.recalcDeferredIterator = recalcDeferredIterator;
				lastBandRowIterator = lastBandRowIterator.child;
			}

			rootIterator.rows = layout.Rows;

			return rootIterator;
		}

		private UltraGridLayout Layout
		{
			get
			{
				return this.band.Layout;
			}
		}

		public UltraGridRow Current 
		{
			get 
			{
				return null != this.child ? this.child.Current : this.row;
			}
		}

		// SSP 11/9/04 UWC51
		// RefRowIterator is being used for traversing summaries as well. If a row collection
		// contains no rows then before we were skipping the summaries of that row collection.
		// This is to fix that problem.
		//
		public RowsCollection CurrentRowsCollection
		{
			get
			{
				return null != this.child ? this.child.CurrentRowsCollection : this.rows;
			}
		}

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private ArrayList recalcDeferredRowsList = null;
		private List<UltraGridRow> recalcDeferredRowsList = null;

		private int rowIndexInRowsSortedByListIndex = -1;
		private UltraGridRow[] rowsSortedByListIndex = null;

		private UltraGridRow GetFirstRow( )
		{
			FormulaRowIndexSource resolvedRowIndexType = this.rowIndexType;
			if ( FormulaRowIndexSource.ListIndex == resolvedRowIndexType )
			{
				if ( null == this.child )
				{
					// If the row index type is list index and this iterator is being used in a range
					// recalc then we have to return rows in the same order as they are in the bound
					// list otherwise relative index calculations will be thrown off.
					//
					this.rowsSortedByListIndex = null;
					this.rowIndexInRowsSortedByListIndex = -1;
					if ( this.usedInRangeRecalc )
					{
						UltraGridRow firstRow = this.rows.UnSortedActualRows.Count > 0
							? this.rows.UnSortedActualRows.GetItem( 0, true ) : null;

						if ( null == firstRow || ! firstRow.IsDescendantOf( this.rows ) )
							return null;

						
						
						
						//this.rowsSortedByListIndex = this.rows.UnSortedActualRows.ToArray( true );
						this.rowsSortedByListIndex = this.rows.UnSortedActualRows.ToArray( true, true );
					}
					else if ( ! this.rows.IsGroupByRows )
					{
						this.rowsSortedByListIndex = (UltraGridRow[])this.rows.All;
						Infragistics.Win.Utilities.SortMerge( this.rowsSortedByListIndex, new RowsCollection.ListIndexComparer( ) );
					}

					if ( null != this.rowsSortedByListIndex )
					{
						if ( this.rowsSortedByListIndex.Length > 0 )
						{
							return this.rowsSortedByListIndex[ this.rowIndexInRowsSortedByListIndex = 0 ];
						}
						else
						{
							this.rowsSortedByListIndex = null;
							return null;
						}
					}
				}

				// If this iterator iterates either the group-by rows or is the lowest level row iterator
				// (as indicated by child iterator being null) then use the row index type of RowIndex 
				// instead of ListIndex because for group-by rows there are no list indexes and for 
				// non-lowest level iterators it doesn't make any difference.
				//
				resolvedRowIndexType = FormulaRowIndexSource.RowIndex;
			}

			return RefUtils.GetRow( this.rows, 0, resolvedRowIndexType );
		}

		private UltraGridRow GetNextRow( )
		{
			FormulaRowIndexSource resolvedRowIndexType = this.rowIndexType;
			if ( FormulaRowIndexSource.ListIndex == resolvedRowIndexType )
			{
				if ( null == this.child && ! this.row.IsGroupByRow )
				{
					++this.rowIndexInRowsSortedByListIndex;
					if ( null == this.rowsSortedByListIndex
						|| this.rowIndexInRowsSortedByListIndex >= this.rowsSortedByListIndex.Length )
						return null;

					return this.rowsSortedByListIndex[ this.rowIndexInRowsSortedByListIndex ];
				}

				// If this iterator iterates either the group-by rows or is the lowest level row iterator
				// (as indicated by child iterator being null) then use the row index type of RowIndex 
				// instead of ListIndex because for group-by rows there are no list indexes and for 
				// non-lowest level iterators it doesn't make any difference.
				//
				resolvedRowIndexType = FormulaRowIndexSource.RowIndex;
			}

			int calcIndex = RefUtils.GetRowCalcIndex( this.row, resolvedRowIndexType );
			return calcIndex >= 0 ? RefUtils.GetRow( this.rows, ++calcIndex, resolvedRowIndexType ) : null;
		}

		public bool MoveNext( )
		{
			// SSP 11/19/04 BR00885
			// Moved this here from the beginning of the do loop below.
			//
			if ( null != this.child && ! this.firstTime && this.child.MoveNext( ) )
				return true;

			do
			{
				// SSP 11/9/04 UWC51
				// RefRowIterator is being used for traversing summaries as well. If a row collection
				// contains no rows then before we were skipping the summaries of that row collection.
				// This is to fix that problem.
				//
				// ------------------------------------------------------------------------------------
				//bool firstTime = null == this.row;
				bool firstTime = this.firstTime;
				this.firstTime = false;
				// ------------------------------------------------------------------------------------

				// SSP 11/19/04 BR00885
				// Moved this before the do loop starts.
				//
				//if ( null != this.child && ! firstTime && this.child.MoveNext( ) )
				//	return true;

				// SSP 8/16/04 - Recalc Deferred
				// Added the following if block and enclosed the existing code into the else block.
				//
				if ( this.recalcDeferredIterator )
				{
					if ( firstTime )
					{
						this.recalcDeferredRowsList = 
							this.Layout.GetVisibleRowsFromAllScrollRegions( this.rows, false, false, true );
					}

					do
					{
						// Get the next row from the visible rows collection.
						//
						int index = null != this.row ? 1 + this.recalcDeferredRowsList.IndexOf( this.row ) : 0;
						this.row = index >= 0 && index < this.recalcDeferredRowsList.Count
							? (UltraGridRow)this.recalcDeferredRowsList[ index ]
							: null;

						// Keep doing so until the row has the right scope index. RecalcDeferred would
						// be true only for scopes of Any and Index. In the case of scope Any, all the 
						// rows match. In the case of the scope Index, match the row's index with the 
						// scope.
						//
					} while ( null != this.row && RefTuple.RefScope.Index == this.tuple.Scope 
						&& this.tuple.ScopeIndex != RefUtils.GetRowCalcIndex( this.row ) );
				}
				else
				{
					RefTuple.RefScope scopeResolved = this.tuple.Scope;

					if ( null != this.childTuple )
					{
						if ( RefTuple.RefScope.All == this.childTuple.Scope )
						{
							// If the child tupple has a scope of All (like in the reference [Price(*)])
							// Then traverse all rows even when the current tupple has a sepcified scope
							// index. 
							//
							scopeResolved = RefTuple.RefScope.All;
						}
						else if ( RefTuple.RefScope.SummaryValue == this.childTuple.Scope && ! firstTime )
						{
							// If the child is a summary value then break out after processing the first row
							// because summary values are associated with rows collection and not individual
							// rows.
							//
							this.row = null;
							return false;
						}
					}

					switch ( scopeResolved )
					{
						case RefTuple.RefScope.Any:
						case RefTuple.RefScope.All:
						{
							if ( firstTime )
							{
								// SSP 10/27/04 Formula Row Index Source
								//
								//this.row = this.rows.FirstRow;
								this.row = this.GetFirstRow( );

								// SSP 11/9/04 UWC51
								// RefRowIterator is being used for traversing summaries as well. If a row collection
								// contains no rows then before we were skipping the summaries of that row collection.
								// This is to fix that problem.
								//
								// --------------------------------------------------------------------------------------
								if ( null == this.row && null != this.childTuple && RefTuple.RefScope.SummaryValue == this.childTuple.Scope )
								{
									return true;
								}
								// --------------------------------------------------------------------------------------
							}
							else
								// SSP 10/25/04 VINDEX
								//this.row = this.row.GetSibling( SiblingRow.Next, false, false, IncludeRowTypes.DataRowsOnly );
								this.row = this.GetNextRow( );
						}
							break;

						case RefTuple.RefScope.Identifier:
						{
							// SSP 5/17/05
							// First time in the MoveNext we have to get the first row. Commented out the
							// original code and added the new code below.
							//
							// ------------------------------------------------------------------------------
							this.row = firstTime ? this.GetFirstRow( ) : this.GetNextRow( );

							RefParser.NameValuePair[] pairs = null;
							if ( null != this.row )
							{
								if ( this.row.IsGroupByRow )
								{
									pairs = new RefParser.NameValuePair[] { new RefParser.NameValuePair( 
																			  ((UltraGridGroupByRow)row).Column.FormulaHolder.NormalizedCalcReferenceName, this.tuple.ScopeID ) };
								}
								else
								{
									string error;
									pairs = RefParser.ParseNameValuePairs( this.tuple.ScopeID, out error );
									if ( null != error )
										throw new UltraCalcException( error );
								}
							}

							while ( null != this.row && ! RefUtils.DoesRowMatchCriteria( this.row, pairs ) )
								this.row = this.GetNextRow( );

							
							// ------------------------------------------------------------------------------
						}
							break;
					
						case RefTuple.RefScope.Index:
						{
							if ( firstTime )
							{
								this.row = RefUtils.GetRow( this.rows, this.tuple.ScopeIndex );

								if ( FormulaRowIndexSource.ListIndex == this.rowIndexType 
									&& null != this.row && this.rows != this.row.ParentCollection )
									this.row = null;
							}
							else
								this.row = null;
						}
							break;

						default:
							throw new UltraCalcException( SR.GetString( "LER_Calc_InvalidReference_InvalidScope", this.tuple.ToString( ) ) );
					}
				}

				if ( null != this.row && null != this.child )
					this.child.InitRowsCollection( this.row.ChildBands[ this.child.band ].Rows );

			} while ( null != this.row && null != this.child && ! this.child.MoveNext( ) );

			// Return true if we found a row. If we got here then it also means that 
			// the child iterator if any also found a row matching row.
			//
			return null != this.row;
		}

		public void Reset( )
		{
			this.row = null;
			
			// SSP 11/9/04 UWC51
			// RefRowIterator is being used for traversing summaries as well. If a row collection
			// contains no rows then before we were skipping the summaries of that row collection.
			// This is to fix that problem.
			//
			this.firstTime = true;
		}

		private void InitRowsCollection( RowsCollection rows )
		{
			this.rows = rows;
			this.Reset( );
		}
	}

	#endregion // RefRowIterator Class

	#region RefSummaryValueCollection Class

	internal class RefSummaryValueCollection : IUltraCalcReferenceCollection
	{
		private SummarySettings summarySettings = null;
		private RefParser scopeRP = null;
		private UltraGridRefBase referenceBeingEnumerated;

		public RefSummaryValueCollection( UltraGridRefBase referenceBeingEnumerated, SummarySettings summarySettings, RefParser scopeRP )
		{
			if ( null == summarySettings )
				throw new ArgumentNullException( "summarySettings" );

			this.summarySettings = summarySettings;
			this.referenceBeingEnumerated = referenceBeingEnumerated;
			this.scopeRP = scopeRP;
		}

		#region IEnumerable

		public IEnumerator GetEnumerator( )
		{
			return new RefSummaryValueCollectionEnumerator(this);
		}

		#endregion //IEnumerable

		#region Implementation of IEnumerator

		private class RefSummaryValueCollectionEnumerator : IEnumerator
		{			
			private RefSummaryValueCollection collection;
			private RefRowIterator rowIterator;

			internal RefSummaryValueCollectionEnumerator(RefSummaryValueCollection collection)
			{
				this.collection = collection;
				this.rowIterator = RefRowIterator.CreateRowIterator( collection.summarySettings.Layout, collection.scopeRP, collection.referenceBeingEnumerated );
			}

			public object Current 
			{
				get 
				{
					// SSP 11/9/04 UWC51
					// RefRowIterator is being used for traversing summaries as well. If a row collection
					// contains no rows then before we were skipping the summaries of that row collection.
					// This is to fix that problem.
					//
					//UltraGridRow row = (UltraGridRow)this.rowIterator.Current;
					//return RefUtils.GetSummaryValueReference( row.ParentCollection.SummaryValues[ this.collection.summarySettings ] );
					return RefUtils.GetSummaryValueReference( this.rowIterator.CurrentRowsCollection, this.collection.summarySettings );
				}
			}

			public bool MoveNext( )
			{
				return this.rowIterator.MoveNext( );
			}

			public void Reset( )
			{
				this.rowIterator.Reset( );
			}
		}

		#endregion
	}

	#endregion // RefSummaryValueCollection Class

	#region RefUtils Class

	internal class RefUtils
	{
		internal const string SUMMARY_REFERENCE_POSTFIX = "()";
		internal const string RANGE_REFERENCE_SEPARATOR = RefParser.RangeSeparator;

		#region Constructor

		private RefUtils( )
		{
		}

		#endregion // Constructor

		#region GetFormulaCalcValue
		
		// SSP 6/30/04 - UltraCalc
		// Added GetFormulaCalcValue method.
		//
		internal static object GetFormulaCalcValue( FormulaTargetRefBase formulaTargetRef, 
			out bool isError, IFormatProvider formatProvider )
		{
			string errorInfoText;
			return RefUtils.GetFormulaCalcValue( formulaTargetRef, out isError, 
				out errorInfoText, formatProvider );
		}

		internal static object GetFormulaCalcValue( FormulaTargetRefBase formulaTargetRef, 
			out bool isError, out string errorInfoText, IFormatProvider formatProvider )
		{
			isError = false;
			errorInfoText = null;

			// SSP 8/9/04 - UltraCalc
			// If the cell's column has a formula then display the formula in the cells
			// of the column at design time.
			//
			// --------------------------------------------------------------------------
			if ( formulaTargetRef.Grid.DesignMode )
			{
				if ( formulaTargetRef.HasFormula )
					return formulaTargetRef.Formula.FormulaString;
			}
			// --------------------------------------------------------------------------

			Infragistics.Win.CalcEngine.UltraCalcValue ultraCalcVal = formulaTargetRef.FormulaEvaluationResult;

			if ( formulaTargetRef.HasFormulaSyntaxError )
			{
				isError = true;
				errorInfoText = formulaTargetRef.FormulaSyntaxError;
				return Utilities.GetLocalizedString( "UltraCalc_ErrorCode_Syntax" );
			}

			if ( formulaTargetRef.IsValueDirty || null == (object)ultraCalcVal )
			{
				// MRS 9/14/04
				// Added a property to control whether or not the grid displays 
				// a string in a cell while calculating
				//
				bool shouldDisplayErrorStatus = false;

				// MD 8/7/07 - 7.3 Performance
				// FxCop - Do not cast unnecessarily
				//if ( formulaTargetRef is CellReference )
				//    shouldDisplayErrorStatus = ((CellReference)formulaTargetRef).Cell.Column.ShowCalculatingTextResolved;
				//else if ( formulaTargetRef is SummaryValueReference )
				//    shouldDisplayErrorStatus = ((SummaryValueReference)formulaTargetRef).SummaryValue.SummarySettings.ShowCalculatingTextResolved;
				CellReference cellRef = formulaTargetRef as CellReference;

				if ( cellRef != null )
					shouldDisplayErrorStatus = cellRef.Cell.Column.ShowCalculatingTextResolved;
				else
				{
					SummaryValueReference summaryValueRef = formulaTargetRef as SummaryValueReference;

					if ( summaryValueRef != null )
						shouldDisplayErrorStatus = summaryValueRef.SummaryValue.SummarySettings.ShowCalculatingTextResolved;
				}

				// If they localize the string to an empty string, don't display #Calculating in cells.
				//
				//if ( null != errorCode && errorCode.Length > 0 )
				if ( shouldDisplayErrorStatus )
				{
					isError = true;
					errorInfoText = Utilities.GetLocalizedString( "UltraCalc_Status_Calculating" );
					return Utilities.GetLocalizedString( "UltraCalc_StatusCode_Calculating" );
				}
				else if ( null == (object)ultraCalcVal )
				{
					return DBNull.Value;
				}
			}

			object ret = RefUtils.GetValueFormCalcValue( ultraCalcVal, formatProvider, out isError, formulaTargetRef );

			if ( isError )
				errorInfoText = formulaTargetRef.formulaCalculationErrorDisplayText;

			return ret;
		}

		#endregion // GetFormulaCalcValue

		#region UltraCalcCulture

		internal static System.Globalization.CultureInfo UltraCalcCulture
		{
			get
			{
				return System.Globalization.CultureInfo.InvariantCulture;
			}
		}

		#endregion // UltraCalcCulture

		#region GetUnderlyingReference

		internal static IUltraCalcReference GetUnderlyingReference( IUltraCalcReference reference )
		{
			RefUnAnchored ru = reference as RefUnAnchored;
			return null != ru ? ru.WrappedReference : reference;
		}

		#endregion // GetUnderlyingReference

		#region GetValueFormCalcValue

		internal static object GetValueFormCalcValue( UltraCalcValue ultraCalcValue, 
			IFormatProvider formatProvider, out bool isErrorValue, FormulaTargetRefBase formulaTargetRef )
		{
			isErrorValue = false;

			if ( ultraCalcValue.IsError )
			{
				isErrorValue = true;
				return ultraCalcValue.ToString( formatProvider );
			}

			// SSP 11/9/04 
			// Added FormulaValueConverter property on column and summary.
			//
			// ------------------------------------------------------------------------------------------
			IFormulaValueConverter formulaValueConverter = formulaTargetRef.ContainingFormulaRef.FormulaHolder.FormulaValueConverter;
			if ( null != formulaValueConverter )
				return formulaValueConverter.ConvertFromUltraCalcValue( ultraCalcValue, formulaTargetRef.Context );
			// ------------------------------------------------------------------------------------------

			if ( ultraCalcValue.IsNull )
				// SSP 8/19/04
				// Use DBNull instead of null.
				//
				//return null;
				return DBNull.Value;

			// If the calc value is a reference value then get the reference. Keep doing so
			// until you encounter a calc value that's not a reference.
			//
			while ( ultraCalcValue.IsReference )
			{
				IUltraCalcReference tmp = ultraCalcValue.ToReference();
				Debug.Assert( null != tmp );
				if ( null == tmp )
					return ultraCalcValue.ToString( formatProvider );

				ultraCalcValue = tmp.Value;
			}

			// Get the value obejct from the ultra calc value.
			//
			object data = ultraCalcValue.Value;

			// If the value's type is a known data type then return it.
			//
			if ( null != data && Infragistics.Win.DataBindingUtils.IsKnownType( data.GetType( ) ) )
				return data;

			// Otherwise convert the value to string and return the string.
			//
			return ultraCalcValue.ToString( formatProvider );
		}

		#endregion // GetValueFormCalcValue

		#region GetCalcValue

		internal static UltraCalcValue GetCalcValue( object val, FormulaTargetRefBase formulaTargetRef )
		{
			// SSP 11/9/04 
			// Added FormulaValueConverter property on column and summary.
			//
			//return new UltraCalcValue( val );
            IFormulaValueConverter formulaValueConverter = formulaTargetRef.ContainingFormulaRef.FormulaHolder.FormulaValueConverter;
			return null == formulaValueConverter 
				? new UltraCalcValue( val ) 
				: formulaValueConverter.ConvertToUltraCalcValue( val, formulaTargetRef.Context );
		}

		#endregion // GetCalcValue

		#region GetChildBand

		private static UltraGridChildBand GetChildBand( UltraGridRow row, string name )
		{
			// SSP 9/21/04
			// Escapement of characters in references.
			//
			//if ( null != row.ChildBands && row.ChildBands.Exists( name ) )
			//	return row.ChildBands[ name ];
			if ( null != row.ChildBands )
			{
				foreach ( UltraGridChildBand childBand in row.ChildBands )
				{
					if ( childBand.Band.NormalizedCalcReferenceName == name )
						return childBand;
				}
			}

			// AS 10/7/04 Exception management
			//throw new Exception( SR.GetString( "LER_Calc_ChildBandNotFound", name ) );
			return null;
		}

		#endregion // GetChildBand

		#region GetChildBandRowsReference
		internal static IUltraCalcReference GetChildBandRowsReference( UltraGridRow row, string name )
		{
			UltraGridChildBand childBand = RefUtils.GetChildBand( row, name );

			if (childBand == null)
				return new UltraCalcReferenceError(name, SR.GetString( "LER_Calc_ChildBandNotFound", name ) );

			return RefUtils.GetRowsReference( childBand.Rows );
		}
		#endregion //GetChildBandRowsReference

		#region FindChildBandRowReference
		internal static IUltraCalcReference FindChildBandRowReference( UltraGridRow row, string name, string index )
		{
			UltraGridChildBand childBand = RefUtils.GetChildBand( row, name );

			if (childBand == null)
				return new UltraCalcReferenceError(name, SR.GetString( "LER_Calc_ChildBandNotFound", name ) );

			return RefUtils.FindRowReference(childBand.Rows, childBand.Band, index, name);
		}
		#endregion //FindChildBandRowReference

		#region GetChildBandRowReference
		internal static IUltraCalcReference GetChildBandRowReference( UltraGridRow row, string name, int index )
		{
			UltraGridChildBand childBand = RefUtils.GetChildBand( row, name );

			if (childBand == null)
				return new UltraCalcReferenceError(name, SR.GetString( "LER_Calc_ChildBandNotFound", name ) );

			return RefUtils.GetRowReference(childBand.Rows, index, name);
		}
		#endregion //GetChildBandRowReference

		#region GetBandReference

		internal static BandReference GetBandReference( UltraGridBand band )
		{
			return band.CalcReference;
		}

		#endregion // GetBandReference

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#region GetGridReference

		//internal static UltraGridRefBase GetGridReference( UltraGridLayout layout )
		//{
		//    return layout.CalcReference;
		//}

		//#endregion // GetGridReference

		#endregion Not Used

		#region GetSummaryValueReference
		
		internal static IUltraCalcReference GetSummaryValueReference( RowsCollection rows, SummarySettings summarySettings )
		{
			return GetSummaryValueReference(rows, summarySettings, null);
		}

		private static IUltraCalcReference GetSummaryValueReference( RowsCollection rows, SummarySettings summarySettings, string name )
		{
			SummaryValue summaryVal = rows.SummaryValues.GetSummaryValueFor( summarySettings );
			if ( null == summaryVal )
				return new UltraCalcReferenceError( name, SR.GetString( "LER_Calc_SummaryNotFound", summarySettings.Key ) );

			return RefUtils.GetSummaryValueReference( summaryVal );
		}

		internal static IUltraCalcReference GetSummaryValueReference( RowsCollection rows, string name )
		{
			SummarySettings summary = RefUtils.GetSummarySettings( rows.Band, name );

			if (summary == null)
				return new UltraCalcReferenceError( name, SR.GetString( "LER_Calc_SummaryNotFound", rows.Band.CalcReferenceName, name ) );

			return RefUtils.GetSummaryValueReference( rows, summary, name );
		}

		#endregion // GetSummaryValueReference

		#region GetNextGroupByColumn

		internal static UltraGridColumn GetNextGroupByColumn( UltraGridColumn groupByColumn )
		{
			int index = groupByColumn.Band.SortedColumns.IndexOf( groupByColumn );
            
			if ( index >= 0 && 1 + index < groupByColumn.Band.SortedColumns.Count )
			{
				UltraGridColumn nextSortColumn = groupByColumn.Band.SortedColumns[ 1 + index ];

				if ( nextSortColumn.IsGroupByColumn )
					return nextSortColumn;
			}

			return null;
		}

		private static UltraGridColumn GetNextGroupByColumn( UltraGridBand band, UltraGridColumn groupByColumn )
		{
			if ( band.HasGroupBySortColumns )
			{
				// If the passed in group-by column is null then return the first group-by column in
				// the band.
				//
				if ( null == groupByColumn )
					return band.SortedColumns[0];

				return RefUtils.GetNextGroupByColumn( groupByColumn );
			}

			return null;
		}

		#endregion // GetNextGroupByColumn

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        #region HasNextGroupByColumn

		//#if DEBUG
		//        /// <summary>
		//        /// Returns true if there is a next group-by column after the passed in group-by column.
		//        /// </summary>
		//        /// <param name="groupByColumn"></param>
		//        /// <returns></returns>
		//#endif
		//        internal static bool HasNextGroupByColumn( UltraGridColumn groupByColumn )
		//        {
		//            return null != RefUtils.GetNextGroupByColumn( groupByColumn );
		//        }

		//        #endregion // HasNextGroupByColumn

		//        #region GetPrevGroupByColumn

		//#if DEBUG
		//        /// <summary>
		//        /// Returns the group-by column previous to the the passed in group-by column.
		//        /// </summary>
		//        /// <param name="groupByColumn"></param>
		//        /// <returns></returns>
		//#endif
		//        internal static UltraGridColumn GetPrevGroupByColumn( UltraGridColumn groupByColumn )
		//        {
		//            int index = groupByColumn.Band.SortedColumns.IndexOf( groupByColumn );

		//            if ( index > 0 )
		//            {
		//                UltraGridColumn prevSortColumn = groupByColumn.Band.SortedColumns[ index - 1 ];
		//                Debug.Assert( prevSortColumn.IsGroupByColumn );
		//                return prevSortColumn;
		//            }

		//            return null;
		//        }

		//#if DEBUG
		//        /// <summary>
		//        /// Returns the group-by column previous to the the passed in group-by column. If the passed 
		//        /// in group-by column is null then returns the last group-by column in the band if any.
		//        /// </summary>
		//        /// <param name="band"></param>
		//        /// <param name="groupByColumn"></param>
		//        /// <returns></returns>
		//#endif
		//        private static UltraGridColumn GetPrevGroupByColumn( UltraGridBand band, UltraGridColumn groupByColumn )
		//        {
		//            if ( band.HasGroupBySortColumns )
		//            {
		//                // If the passed in group-by column is null then return the last group-by column in
		//                // the band.
		//                //
		//                if ( null == groupByColumn )
		//                    return band.SortedColumns.LastGroupByColumn;

		//                return RefUtils.GetPrevGroupByColumn( groupByColumn );
		//            }

		//            return null;
		//        }

		//        #endregion // GetPrevGroupByColumn

		//        #region HasPrevGroupByColumn

		//#if DEBUG
		//        /// <summary>
		//        /// Returns true if there is a next group-by column after the passed in group-by column.
		//        /// </summary>
		//        /// <param name="groupByColumn"></param>
		//        /// <returns></returns>
		//#endif
		//        internal static bool HasPrevGroupByColumn( UltraGridColumn groupByColumn )
		//        {
		//            return null != RefUtils.GetPrevGroupByColumn( groupByColumn );
		//        }

		//        #endregion // HasPrevGroupByColumn

		#endregion Not Used

		#region InsertGroupByColumnTuples

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static ArrayList InsertGroupByColumnTuples( UltraGridLayout layout, RefParser rp )
		internal static List<RefTuple> InsertGroupByColumnTuples( UltraGridLayout layout, RefParser rp )
		{
			return RefUtils.InsertGroupByColumnTuples( layout, rp, null );
		}

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static ArrayList InsertGroupByColumnTuples( UltraGridLayout layout, RefParser rp, UltraGridBand skipBand )
		internal static List<RefTuple> InsertGroupByColumnTuples( UltraGridLayout layout, RefParser rp, UltraGridBand skipBand )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<RefTuple> list = new List<RefTuple>();

			try
			{
				// If the typle count of an absolute name is 1 or less than the only component in the
				// absolute name can be the name of the grid. This method can't do anything with such
				// a reference name.
				//
				if ( rp.TupleCount < 2 )
					return list;

				// Get the root band. First tuple in rp should be the grid tuple and the second
				// should be the band tuple.
				//
				UltraGridBand lastBand = RefUtils.GetBand( layout, null, ((RefTuple)rp[1]).Name );
				list.Add( rp[1] );
				int lastBandTupleIndex = 0;
				bool lastBandAlreadyProcessed = false;
				UltraGridColumn lastGroupByColumn = null;

				for ( int i = 2; i < rp.TupleCount; i++ )
				{
					RefTuple tuple = rp[ i ];
					bool isLastTuple = i + 1 == rp.TupleCount;

					UltraGridColumn col = RefTuple.RefScope.SummaryValue != tuple.Scope
						? RefUtils.GetColumn( lastBand, tuple.Name, false ) : null;

					// Last tuple can be a column that's a group-by column. In such a case
					// we don't want to treat the tuple as a group-by type but rather a column
					// tuple as in a formula.
					//
					if ( null != col && col.IsGroupByColumn && ! isLastTuple )
					{
						lastGroupByColumn = col;
					}
					else
					{
						if ( ! lastBandAlreadyProcessed && lastBand != skipBand )
						{
							//int addedGroupByColumnTuples = list.Count;
							while ( null != ( lastGroupByColumn = RefUtils.GetNextGroupByColumn( lastBand, lastGroupByColumn ) ) )
								list.Add( new RefTuple( lastGroupByColumn.FormulaHolder.NormalizedCalcReferenceName ) );

							//if ( addedGroupByColumnTuples != list.Count )
							if ( lastBand.HasGroupBySortColumns 
								&& lastBand.NormalizedCalcReferenceName != tuple.Name )
							{
								// In group-by mode, the band tuple is repeated. The first band
								// tuple shouldn't have any scope index. The one after all the group-by
								// columns should have the scope index. For example, 
								// //ultraGrid1/Customers/State(NY)/City(Albany)/Customers(0).
								// However a reference without the group tuples will be like
								// //ultraGrid1/Customers(0). When transforming this to contain
								// group-by tuples, the Customers(0) tuple needs to be added after
								// the group-by column tuples. Following three lines do exactly this.
								//
								// MD 8/10/07 - 7.3 Performance
								// Use generics
								//RefTuple originalTuple = (RefTuple)list[ lastBandTupleIndex ];
								RefTuple originalTuple = list[ lastBandTupleIndex ];

								list.Add( originalTuple );
								list[ lastBandTupleIndex ] = new RefTuple( lastBand.NormalizedCalcReferenceName );
							}
						}

						lastBandAlreadyProcessed = true;
						lastGroupByColumn = null;

						if ( null != col && col.IsChaptered )
						{
							lastBand = RefUtils.GetBand( layout, lastBand, tuple.Name );
							lastBandAlreadyProcessed = false;
							lastBandTupleIndex = list.Count;
						}
					}

					list.Add( tuple );
				}
			}
			catch ( Exception exc )
			{
				Debug.Assert( false, "InsertGroupByColumnTuples: " + exc.ToString( ) );
				return null;
			}

			return list;
		}

		#endregion // InsertGroupByColumnTuples

		#region GetSummaryValueReference

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal static IUltraCalcReference GetSummaryValueReference( UltraGridRow row, string name )
		//{
		//    return GetSummaryValueReference( row.ParentCollection, name );
		//}

		#endregion Not Used

		internal static IUltraCalcReference GetSummaryValueReference( SummaryValue summaryValue )
		{
			return summaryValue.CalcReference;
		}

		#endregion // GetSummaryValueReference

		#region GetSummarySettingsReference

		internal static SummarySettingsReference GetSummarySettingsReference( SummarySettings summarySettings )
		{
			return summarySettings.CalcReference;
		}

		#endregion // GetSummarySettingsReference

		#region GetSummarySettings

		internal static SummarySettings GetSummarySettings( UltraGridBand band, string name )
		{
			// See if a summary with name key exists and if so return that.
			//
			// SSP 9/21/04
			//
			//if ( band.Summaries.Exists( name ) )
			//	return band.Summaries[ name ];

			foreach ( SummarySettings summary in band.Summaries )
			{
				if ( name == summary.FormulaHolder.NormalizedCalcReferenceName )
					return summary;
			}

			// AS 10/8/04 Removed unnecessary exceptions.
			//if ( raiseException )
			//	return new UltraCalcReferenceError( name, SR.GetString( "LER_Calc_SummaryNotFound", band.CalcReferenceName, name ) );

			return null;
		}

		#endregion // GetSummarySettings

		#region GetSummarySettingsReference

		internal static IUltraCalcReference GetSummarySettingsReference( UltraGridBand band, string name )
		{
			SummarySettings summarySettings = RefUtils.GetSummarySettings( band, name );
			
			if (summarySettings == null)
				return new UltraCalcReferenceError( name, SR.GetString( "LER_Calc_SummaryNotFound", band.CalcReferenceName, name ) );
			
			return summarySettings.CalcReference;
		}

		#endregion // GetSummarySettingsReference

		#region FindRow

		internal static bool DoesRowMatchCriteria( UltraGridRow row, RefParser.NameValuePair[] nameValuePairs )
		{
			Debug.Assert( nameValuePairs.Length > 0 );

			foreach ( RefParser.NameValuePair nv in nameValuePairs )
			{
				UltraGridColumn col = RefUtils.GetColumn( row.Band, nv.Name, true );
				if ( ! RefUtils.EqualValue( row, col, nv.Value ) )
					return false;
			}

			return true;
		}

		internal static UltraGridGroupByRow FindGroupByRow( RowsCollection rows, string index )
		{
			if ( ! rows.IsGroupByRows )
				throw new UltraCalcException( "Specified rows collection does not contain group-by rows." );

			// VINDEX
			foreach ( UltraGridGroupByRow row in rows )
			{
				if ( RefUtils.EqualValue( row, rows.GroupByColumn, index ) )
					return row;
			}

			return null;
		}

		
		internal static UltraGridRow FindRow( RowsCollection rows, UltraGridBand band, string index, out string exception )
		{
			// assume no errors
			exception = null;
			UltraGridRow matchingRow = null;

			if ( band != rows.Band && band.IsDescendantOfBand( rows.Band ) )
				exception = string.Format( "{0} is not a descendant band of the {1} band.", band, rows.Band );

			if (exception == null)
			{
				RefParser.NameValuePair[] columnValuePairs = RefParser.ParseNameValuePairs( index, out exception );

				if (exception == null)
				{
					// SSP 12/13/05 - Recursive Row Enumerator
					// Use the new GetRowEnumerator method of the rows collection.
					// 
					// --------------------------------------------------------------------------
					
					foreach ( UltraGridRow row in rows.GetRowEnumerator( GridRowType.DataRow, band, band ) )
					{
						if ( RefUtils.DoesRowMatchCriteria( row, columnValuePairs ) )
						{
							matchingRow = row;
							break;
						}
					}
					// --------------------------------------------------------------------------

					if ( null == matchingRow )
						exception = SR.GetString( "LER_Calc_IndexedRowNotFound", index );
				}
			}

			return matchingRow;
		}

		#endregion // FindRow

		#region FindRowReference

		internal static IUltraCalcReference FindRowReference( RowsCollection rows, UltraGridBand band, string index, string name )
		{
			string error;

			UltraGridRow row = RefUtils.FindRow(rows, band, index, out error);

			if (row == null)
				return new UltraCalcReferenceError(name, error);

			return RefUtils.GetRowReference(row);
		}

		#endregion // FindRowReference

		#region GetParentRow

		internal static UltraGridRow GetParentRow( UltraGridRow row )
		{
			return row.ParentRow;
		}

		#endregion // GetParentRow

		#region GetRowRelativeReference

		internal static IUltraCalcReference GetRowRelativeReference( UltraGridRow row, int offset, string name )
		{
			int rowCalcIndex = RefUtils.GetRowCalcIndex( row );
			if ( rowCalcIndex >= 0 )
				rowCalcIndex += offset;

			return RefUtils.GetRowReference( row.ParentCollection, rowCalcIndex , name );
		}

		#endregion // GetRowRelativeReference

		#region GetRow

		internal static UltraGridRow GetRow( RowsCollection rows, int calcIndex )
		{
			return RefUtils.GetRow( rows, calcIndex, rows.Band.FormulaRowIndexSourceResolved );
		}

		internal static UltraGridRow GetRow( RowsCollection rows, int calcIndex, FormulaRowIndexSource rowIndexType )
		{
			if ( FormulaRowIndexSource.ListIndex == rowIndexType && ! rows.IsGroupByRows )
				return rows.TopLevelRowsCollection.GetRowWithListIndexHelper( calcIndex, false );

			if ( FormulaRowIndexSource.VisibleIndex == rowIndexType )
				return rows.SparseArray.GetItemAtVisibleIndex( calcIndex );
			
			return calcIndex >= 0 && calcIndex < rows.Count ? rows[ calcIndex ] : null;
		}

		#endregion // GetRow

		#region GetRowCalcIndex

		internal static int GetRowCalcIndex( UltraGridRow row )
		{
			return RefUtils.GetRowCalcIndex( row, row.Band.FormulaRowIndexSourceResolved );
		}

		internal static int GetRowCalcIndex( UltraGridRow row, FormulaRowIndexSource rowIndexType )
		{
			if ( FormulaRowIndexSource.ListIndex == rowIndexType && ! row.IsGroupByRow )
				return row.ListIndex;

			if ( FormulaRowIndexSource.VisibleIndex == rowIndexType )
				return row.ParentCollection.SparseArray.VisibleIndexOf( row );

			return row.Index;
		}

		#endregion // GetRowCalcIndex

		#region GetMaxCalcIndex

		internal static int GetMaxCalcIndex( RowsCollection rows )
		{
			return RefUtils.GetMaxCalcIndex( rows, rows.Band.FormulaRowIndexSourceResolved );
		}

		internal static int GetMaxCalcIndex( RowsCollection rows, FormulaRowIndexSource rowIndexType )
		{
			if ( FormulaRowIndexSource.ListIndex == rowIndexType && ! rows.IsGroupByRows )
				return rows.TopLevelRowsCollection.ActualRowsCount - 1;

			if ( FormulaRowIndexSource.VisibleIndex == rowIndexType )
				return rows.SparseArray.VisibleCount - 1;

			return rows.Count - 1;
		}

		#endregion // GetMaxCalcIndex

		#region GetRowReference

		internal static IUltraCalcReference GetRowReference( RowsCollection rows, int index, string name )
		{
			UltraGridRow row = RefUtils.GetRow( rows, index );

			if (row == null)
				return new UltraCalcReferenceError(name, SR.GetString( "LER_Calc_RowNotFound", index ) );

			return RefUtils.GetRowReference(row);
		}

		internal static RowReferenceBase GetRowReference( UltraGridRow row )
		{
			return row.CalcReference;
		}

		#endregion // GetRowReference

		#region GetRowsReference

		internal static RowsCollectionReference GetRowsReference( RowsCollection rows )
		{
			return rows.CalcReference;
		}

		#endregion // GetRowsReference

		#region ThrowRootBandNameExpectedException

		internal static void ThrowRootBandNameExpectedException( UltraGridLayout layout, string name )
		{
			throw new UltraCalcException( SR.GetString( "LER_Calc_RootBandNameExpected", layout.CalcReference.ElementName, name ) );
		}

		#endregion // ThrowRootBandNameExpectedException

		#region GetBand

		internal static UltraGridBand GetBand( UltraGridLayout layout, UltraGridBand ensureParentBand, string name )
		{
			// SSP 12/13/06 BR18342
			// Added an overload of GetBand that takes in preferredParentBand parameter. This is in case there are
			// multiple bands with the same name.
			// 
			//UltraGridBand band = GetBand( layout, name, true );
			UltraGridBand band = GetBand( layout, name, ensureParentBand, true );

			if ( ensureParentBand != band.ParentBand )
			{
				if ( null == ensureParentBand )
					RefUtils.ThrowRootBandNameExpectedException( layout, name );
				else
					throw new UltraCalcException( string.Format( "{0} is not a child band of {1}", ensureParentBand.CalcReferenceName, name ) );
			}

			return band;
		}

		// SSP 12/13/06 BR18342
		// Added an overload of GetBand that takes in preferredParentBand parameter. This is in case there are
		// multiple bands with the same name.
		// 
		//internal static UltraGridBand GetBand( UltraGridLayout layout, string name, bool raiseException )
		internal static UltraGridBand GetBand( UltraGridLayout layout, string name, UltraGridBand preferredParentBand, bool raiseException )
		{
			UltraGridBand retBand = null;

			foreach ( UltraGridBand band in layout.SortedBands )
			{
				if ( band.NormalizedCalcReferenceName == name )
				{
					// SSP 12/13/06 BR18342
					// Added an overload of GetBand that takes in preferredParentBand parameter. This is in case there are
					// multiple bands with the same name.
					// 
					// --------------------------------------------------------------------------------
					//return band;
					retBand = band;

					// If the band's parent band is the preferredParentBand then look no further and
					// break out. If not then keep looking for a matching band.
					// 
					if ( null == preferredParentBand || preferredParentBand == band.ParentBand )
						break;
					// --------------------------------------------------------------------------------
				}
			}

			// SSP 12/13/06 BR18342
			// 
			//if ( raiseException ) 
			if ( null == retBand && raiseException ) 
				throw new UltraCalcException( SR.GetString( "LER_Calc_BandNameExpected", name ) );

			return retBand;
		}

		#endregion // GetBand

		#region GetBandReference

		internal static IUltraCalcReference GetBandReference( UltraGridLayout layout, UltraGridBand ensureParentBand, string name )
		{
			// SSP 12/13/06 BR18342
			// Added an overload of GetBand that takes in preferredParentBand parameter. This is in case there are
			// multiple bands with the same name.
			// 
			//UltraGridBand band = GetBand( layout, name, false );
			UltraGridBand band = GetBand( layout, name, ensureParentBand, false );

			if (band == null)
				return new UltraCalcReferenceError( name, SR.GetString( "LER_Calc_BandNameExpected", name ) );

			if ( ensureParentBand != band.ParentBand )
			{
				if ( null == ensureParentBand )
					return new UltraCalcReferenceError(name, SR.GetString( "LER_Calc_RootBandNameExpected", layout.CalcReference.ElementName, name ));
				else
					return new UltraCalcReferenceError(name, string.Format( "{0} is not a child band of {1}", ensureParentBand.CalcReferenceName, name ) );
			}

			return band.CalcReference;
		}

		internal static IUltraCalcReference GetBandReference(UltraGridLayout layout, string name)
		{
			foreach ( UltraGridBand band in layout.SortedBands )
			{
				if ( band.NormalizedCalcReferenceName == name )
					return RefUtils.GetBandReference(band);
			}
			
			return new UltraCalcReferenceError(name, SR.GetString( "LER_Calc_BandNameExpected", name ) );
		}
		#endregion //GetBandReference

		#region GetBandRowsReference
		internal static IUltraCalcReference GetBandRowsReference( UltraGridLayout layout, string name, RowsCollection rows)
		{
			IUltraCalcReference bandRef = RefUtils.GetBandReference(layout, name);

			if ( !(bandRef is BandReference)  )
			{
				Debug.Assert(bandRef is UltraCalcReferenceError, "Unexpected band reference type.");
				return bandRef;
			}

			return RefUtils.GetRowsReference(rows);
		}
		#endregion //GetBandRowsReference

		#region GetColumnReference

		internal static ColumnReference GetColumnReference( UltraGridColumn column )
		{
			return column.CalcReference;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal static IUltraCalcReference GetColumnReference( UltraGridBand band, string name )
		//{
		//    UltraGridColumn column = RefUtils.GetColumn( band, name, false );

		//    if (column == null)
		//        return RefUtils.CreateMissingColumnReference(band, name);

		//    return RefUtils.GetColumnReference( column );
		//}

		#endregion Not Used

		#endregion // GetColumnReference

		#region GetColumn

		internal static UltraGridColumn GetColumn( UltraGridBand band, string name, bool raiseException )
		{
            // MRS 12/7/06 - BR18268
            // This is slowing things down. I added a method to that will do it faster 
            // using a Hashtable. 
            //foreach ( UltraGridColumn column in band.Columns )
            //{
            //    if ( name == column.FormulaHolder.NormalizedCalcReferenceName )
            //        return column;
            //}
            UltraGridColumn column = band.CalcReference.GetColumnFromNormalizedCalcReferenceName(name);
            if (column != null)
                return column;

			if ( raiseException )
				throw new UltraCalcException( SR.GetString( "LER_Calc_ColumnNotFound", band.CalcReferenceName, name ) );

			return null;
		}

		#endregion // GetColumn

		#region GetNestedColumnReference

		internal static IUltraCalcReference GetNestedColumnReference( RowsCollection rows, UltraGridColumn column )
		{
			return new NestedColumnReference( rows, column, false );
		}

		internal static IUltraCalcReference GetNestedColumnReference( RowsCollection rows, string name )
		{
			UltraGridColumn column = RefUtils.GetColumn( rows.Band, name, false );

			if (column == null)
				return RefUtils.CreateMissingColumnReference(rows.Band, name);

			return RefUtils.GetNestedColumnReference( rows, column );
		}

		internal static UltraCalcReferenceError CreateMissingColumnReference(UltraGridBand band, string name)
		{
			return new UltraCalcReferenceError(name, SR.GetString( "LER_Calc_ColumnNotFound", band.CalcReferenceName, name ) );
		}
		#endregion // GetNestedColumnReference

		#region GetNestedBandReference

		internal static NestedBandReference GetNestedBandReference( RowsCollection rows, UltraGridBand band )
		{
			return new NestedBandReference( rows, band );
		}

		internal static IUltraCalcReference GetNestedBandReference( RowsCollection rows, string name )
		{
			IUltraCalcReference bandRef = RefUtils.GetBandReference(rows.Layout, name);

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( !(bandRef is BandReference) )
			//{
			//    Debug.Assert(bandRef is UltraCalcReferenceError, "Unexpected band reference type.");
			//    return bandRef;
			//}
			//
			//return GetNestedBandReference(rows, ((BandReference)bandRef).Band);
			BandReference bandReference = bandRef as BandReference;

			if ( bandReference == null )
			{
				Debug.Assert( bandRef is UltraCalcReferenceError, "Unexpected band reference type." );
				return bandRef;
			}

			return GetNestedBandReference( rows, bandReference.Band );
		}

		#endregion // GetNestedBandReference

		#region GetNestedSummarySettingsReference

		internal static NestedSummarySettingsReference GetNestedSummarySettingsReference( RowsCollection rows, SummarySettingsReference summaryReference )
		{
			return new NestedSummarySettingsReference( rows, summaryReference );
		}

		#endregion // GetNestedSummarySettingsReference

		#region GetCellReference

		internal static CellReference GetCellReference( UltraGridCell cell )
		{
			return cell.CalcReference;
		}

		internal static IUltraCalcReference GetCellReference( UltraGridRow row, UltraGridColumn column )
		{
			if ( row.BandInternal != column.Band )
				return new UltraCalcReferenceError(column.CalcReference.ElementName,  string.Format( "Invalid column {0}. Row band does not match with the column band.", column.Key ) );

            // MRS 1/31/2008 - BR30242
            // This will cause an exception for a GroupbyRow and ultimately end up returning a 
            // Reference error. So just return a reference error and skip the exception, so 
            // at least the performance is better. 
            if (row.IsGroupByRow)
               return new UltraCalcReferenceError(row.CalcReference.ElementName, string.Format("Attempted to get a cell referenced from a GroupByRow; invalid.", column.Key));
            
			return RefUtils.GetCellReference( row.Cells[ column ] );
		}

		#endregion // GetCellReference

		#region EqualValue

		internal static bool EqualValue( UltraGridRow row, UltraGridColumn column, string testVal ) 
		{
			UltraGridGroupByRow groupByRow = row as UltraGridGroupByRow;
			if ( null != groupByRow && column == groupByRow.Column )
			{
				object val = groupByRow.Value;
				return val == (object)testVal || null != val && null != testVal && val.Equals( testVal );
			}

			
			//
			string text = column.GetCellText( row );
			return 0 == String.Compare( text, testVal, true );
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal static bool EqualValue( UltraGridCell cell, string testVal ) 
		//{
		//    return RefUtils.EqualValue( cell.Row, cell.Column, testVal );
		//}

		#endregion Not Used

		#endregion // EqualValue

		#region AddFormulaHelper

		// SSP 2/4/05 
		// Added AddFormulaHelper and RemoveFormulaHelper methods. Apparently the calc-engine does
		// not handle call to RemoveFormula properly if the formula has syntax errors, even if the
		// formula was never added to it, which is what we have been doing, not adding any formulas
		// with syntax errors.
		//
		internal static bool AddFormulaHelper( IUltraCalcManager calcManager, IUltraCalcFormula formula )
		{
			if ( ! formula.HasSyntaxError )
			{
				calcManager.AddFormula( formula );
				return true;
			}

			return false;
		}

		#endregion // AddFormulaHelper

		#region RemoveFormulaHelper

		// SSP 2/4/05 
		// Added AddFormulaHelper and RemoveFormulaHelper methods. Apparently the calc-engine does
		// not handle call to RemoveFormula properly if the formula has syntax errors, even if the
		// formula was never added to it, which is what we have been doing, not adding any formulas
		// with syntax errors.
		//
		internal static void RemoveFormulaHelper( IUltraCalcManager calcManager, IUltraCalcFormula formula )
		{
			if ( ! formula.HasSyntaxError )
				calcManager.RemoveFormula( formula );
		}

		#endregion // RemoveFormulaHelper
	}

	#endregion // RefUtils Class

	#region RangeReference Class

	internal class RangeReference : UltraGridRefBase
	{
		#region Private Vars

		private UltraGridColumn column    = null;
		private string referenceName	  = null;		
		private RefParser fromRP		  = null;
		private RefParser toRP			  = null;
		private RefTuple fromTuple		  = null;
		private RefTuple toTuple		  = null;

		#endregion // Private Vars

		#region Constructor

		internal RangeReference( UltraGridColumn column, 
			RefParser fromRP, RefParser toRP, RefTuple fromTuple, RefTuple toTuple  ) : base( column.Layout, column )
		{
			if ( null == column || null == fromTuple || null == toTuple || null == fromRP || null == toRP )
				throw new ArgumentNullException( );

			this.column			= column;
			this.fromRP			= fromRP;
			this.toRP			= toRP;
			this.fromTuple		= fromTuple;
			this.toTuple		= toTuple;
			this.referenceName = this.fromRP.ToString( ) + RefUtils.RANGE_REFERENCE_SEPARATOR + this.toRP.ToString( );
		}

		#endregion // Constructor

		#region ColumnContext

		internal override UltraGridColumn ColumnContext
		{
			get
			{
				return this.column;
			}
		}

		#endregion // ColumnContext

		#region RefBase Overrides

		public override RefBase BaseParent
		{ 
			get 
			{
				return this.column.Band.CalcReference;
			}
		}

		public override string ElementName 
		{
			get 
			{
				return this.fromTuple.ToString( ) + RefUtils.RANGE_REFERENCE_SEPARATOR + this.toTuple.ToString( );
			}
		}

		public override string AbsoluteName
		{
			get
			{
				return this.fromRP.ToString( ) + RefUtils.RANGE_REFERENCE_SEPARATOR + this.toRP.ToString( );
			}
		}

		public override string NormalizedAbsoluteName
		{
			get
			{
				return this.AbsoluteName;
			}
		}

		public override RefParser ParsedReference
		{
			get
			{
				return this.fromRP;
			}
		}

		public override bool ContainsReference( IUltraCalcReference reference )
		{
			return this.column.CalcReference.ContainsReference( reference );
		}

		public override bool IsEnumerable
		{
			get
			{
				return true;
			}
		}

		public override bool HasRelativeIndex
		{
			get
			{
				return true;
			}
		}

		public override void MarkRelativeIndices( IUltraCalcReference formulaReference )
		{
			if ( formulaReference is UltraGridRefBase )
			{
				RefParser columnRP = null != this.column.CalcReference ? this.column.CalcReference.ParsedReference : null;
				RefParser targetRP = ((UltraGridRefBase)formulaReference).ParsedReference;

				Debug.Assert( null != columnRP && null != targetRP );
				if ( null != columnRP && null != targetRP )
					columnRP.MarkRelativeIndices( targetRP, true );
			}
		}

		// SSP 2/4/05 BR02240
		// Overrode the Formula property so we can return the column's formula here. The reason
		// for doing this is that if some formula has this range reference as one of its tokens, 
		// dependancy calculations need to know about the formula of the range reference. 
		//
		public override IUltraCalcFormula Formula
		{
			get 
			{
				ColumnReference colRef = RefUtils.GetColumnReference( this.column );
				return null != colRef ? colRef.Formula : null;
			}
		}

		#endregion // RefBase Overrides

		#region RangeEnumerator

		private class RangeEnumerator : IEnumerator
		{
			private ResolvedRangeReference resolvedRange = null;
			private int index = 0;
			private IUltraCalcReference currentCell = null;


			internal RangeEnumerator( ResolvedRangeReference resolvedRange )
			{
				this.resolvedRange = resolvedRange;
				this.Reset( );
			}

			public void Reset( )
			{
				this.currentCell = null;
				this.index = this.resolvedRange.from - 1;
			}

			public bool MoveNext( )
			{
				this.index++;
				this.currentCell = null;
				if ( this.index <= this.resolvedRange.to )
				{
					UltraGridRow row = RefUtils.GetRow( this.resolvedRange.rows, this.index );
					if ( null != row )
						this.currentCell = RefUtils.GetCellReference( row, this.resolvedRange.range.column );
				}

				return null != this.currentCell;
			}

			public object Current
			{
				get
				{
					return this.currentCell;
				}
			}
		}

		#endregion // RangeEnumerator

		#region RangeReference 

		private class ResolvedRangeReference : UltraGridRefBase, IUltraCalcReferenceCollection
		{
			internal RangeReference range = null;
			internal RowsCollection rows = null;
			internal int from = 0;
			internal int to = 0;

			internal ResolvedRangeReference( RowsCollection rows, int from, int to, RangeReference range ) : base( rows.Layout, range.column )
			{
				if ( rows.Band != range.column.Band || rows.IsGroupByRows )
					throw new ArgumentException( "Invalid rows collection. Rows must be from the same band as the columns in the range reference." );

				this.range = range;
				this.rows = rows;
				this.from = from;
				this.to   = to;
			}

			public override IUltraCalcReferenceCollection References
			{
				get
				{
					return this;
				}
			}

			public IEnumerator GetEnumerator( )
			{
				return new RangeEnumerator( this );
			}

			public override RefBase BaseParent
			{ 
				get 
				{
					return this.range;
				}
			}

			public override string ElementName 
			{
				get 
				{
					
					//
					return this.range.ElementName;
				}
			}

			public override string AbsoluteName
			{
				get
				{
					this.CreateParsedReference( );
					return this.fromRP.ToString( ) + RefParser.RangeSeparatorWithSpaces + this.toRP.ToString( );
				}
			}

			private RefParser fromRP = null;
			private RefParser toRP = null;

			protected override RefParser CreateParsedReference( )
			{
				if ( null == this.fromRP )
				{
					RowsCollectionReference rowsRef = RefUtils.GetRowsReference( rows );
					string rowsName = rowsRef.AbsoluteName;
					System.Text.StringBuilder sb = new System.Text.StringBuilder( rowsName );
					int len = sb.Length;
					sb.Append( RefParser.RefBeginScope ).Append( this.from ).Append( RefParser.RefEndScope );

					this.fromRP = new RefParser( sb.ToString( ) );

					sb.Remove( len, sb.Length - len );				
					sb.Append( RefParser.RefBeginScope ).Append( this.to ).Append( RefParser.RefEndScope );
				
					this.toRP = new RefParser( sb.ToString( ) );
				}

				return this.fromRP;
			}

			public override string NormalizedAbsoluteName
			{
				get
				{
					return this.AbsoluteName;
				}
			}
			
			public override bool IsEnumerable
			{
				get
				{
					return true;
				}
			}
		}

		#endregion // RangeReference 

		#region GetIndexFromTuple

		private int GetIndexFromTuple( RefTuple tuple, RowsCollection rows, UltraGridRow rowContext, out UltraCalcReferenceError error )
		{
			error = null;
			int index = 0;

			// SSP 10/17/04 - Formula row source index
			// 
			
			if ( RefTuple.RefScope.RelativeIndex == tuple.Scope )
			{
				int rowCalcIndex = null != rowContext ? RefUtils.GetRowCalcIndex( rowContext ) : -1;
				index = rowCalcIndex >= 0 ? rowCalcIndex + tuple.ScopeIndex : -1;
			}
			else if ( RefTuple.RefScope.Index == tuple.Scope )
				index = tuple.ScopeIndex;
			else if ( RefTuple.RefScope.Any == tuple.Scope )
				index = null != rowContext ? RefUtils.GetRowCalcIndex( rowContext ) : -1;
			else if ( RefTuple.RefScope.All == tuple.Scope )
				index = tuple == this.fromTuple ? 0 : RefUtils.GetMaxCalcIndex( rows );

			if ( index < 0 || index > RefUtils.GetMaxCalcIndex( rows ) )
				error = new UltraCalcReferenceError( this.referenceName, "Invalid range for the cell." );
			
			return index;
		}

		#endregion // GetIndexFromTuple

		#region GetResolvedRangeReference

		internal IUltraCalcReference GetResolvedRangeReference( RowsCollection rows, UltraGridRow rowContext )
		{
			if ( rows.Band != this.BandContext || null != rowContext && rowContext.Band != this.BandContext )
				return new UltraCalcReferenceError( this.referenceName, "Formula not from the same band as the columns in the range reference." );

			// Row context must not be a group-by row. It can be null if from and to don't have scope of
			// RelativeIndex.
			//
			if ( rowContext is UltraGridGroupByRow || rows.IsGroupByRows )
				return new UltraCalcReferenceError( this.referenceName, "Invalid context of group-by row." );

			if ( null == rowContext 
				&& ( RefTuple.RefScope.RelativeIndex == this.fromTuple.Scope 
				|| RefTuple.RefScope.RelativeIndex == this.toTuple.Scope ) )
				return new UltraCalcReferenceError( this.referenceName, "Range refernece doesn't have any context of row." );

			UltraCalcReferenceError error;

			int fromIndex = this.GetIndexFromTuple( this.fromTuple, rows, rowContext, out error );
			if ( null != error )
				return error;

			int toIndex   = this.GetIndexFromTuple( this.toTuple, rows, rowContext, out error );            
			if ( null != error )
				return error;

			return new ResolvedRangeReference( rows, fromIndex, toIndex, this );
		}

		#endregion // GetResolvedRangeReference
	}

	#endregion // RangeReference Class

	#region RowsEnumerator Class

	
	
	
	#region Commented Out Code

	

	#endregion // Commented Out Code

	internal class RowsEnumerator : IEnumerator
	{
		#region Enumerable Class

		internal class Enumerable : IEnumerable
		{
			private RowsCollection rows;
			private UltraGridBand lowestLevelBand;

			internal Enumerable( RowsCollection rows, UltraGridBand lowestLevelBand )
			{
				this.rows = rows;
				this.lowestLevelBand = lowestLevelBand;
			}

			public IEnumerator GetEnumerator( )
			{
				return new RowsEnumerator( this.rows, this.lowestLevelBand );
			}
		}

		#endregion // Enumerable Class

		private RowsCollection rows = null;
		private IEnumerator rowsEnumerator = null;
		private UltraGridRow rowsEnumerator_CurrentRow = null;

		private UltraGridBand lowestLevelBand = null;
		private RowsEnumerator child = null;
		private UltraGridRow currentRow = null;

		internal RowsEnumerator( RowsCollection rows, UltraGridBand lowestLevelBand )
		{
			this.rows = rows;
			this.lowestLevelBand = lowestLevelBand;
			this.Reset( );
		}

		public void Reset( )
		{
			this.rowsEnumerator = this.rows.GetEnumerator( );
			this.child = null;
			this.currentRow = null;
		}

		private UltraGridChildBand GetNextChildBand( ChildBandsCollection childBands, UltraGridBand band )
		{
			
			
			
			
			bool bandEncountered = null == band;
			for ( int i = 0, count = childBands.Count; i < count; i++ )
			{
				UltraGridChildBand childBand = childBands[i];

				if ( bandEncountered )
				{
					if ( null == this.lowestLevelBand || this.lowestLevelBand.IsTrivialDescendantOf( childBand.Band ) )
						return childBand;
				}
				else
					bandEncountered = childBand.Band == band;
			}
			

			return null;
		}

		public bool MoveNext( )
		{
			do
			{
				if ( null != this.child )
				{
					if ( this.child.MoveNext( ) )
					{
						this.currentRow = this.child.Current;
						return true;
					}
					else
						this.currentRow = this.rowsEnumerator_CurrentRow;
				}

				ChildBandsCollection childBands = null != this.currentRow ? this.currentRow.ChildBands : null;
				if ( null != childBands && childBands.Count > 0 )
				{
					
					
					
					
					
					UltraGridChildBand childBand = this.GetNextChildBand( childBands, null == this.child ? null : this.child.rows.Band );
					if ( null != childBand )
					{
						this.child = new RowsEnumerator( childBand.Rows, this.lowestLevelBand );
						this.currentRow = null;
						continue;
					}
				}
				
				if ( this.rowsEnumerator.MoveNext( ) )
				{
					this.currentRow = this.rowsEnumerator_CurrentRow = (UltraGridRow)this.rowsEnumerator.Current;
					this.child = null;
				}
				else
				{
					this.currentRow = this.rowsEnumerator_CurrentRow = null;
					break;
				}

			} while ( null == this.currentRow );
	
			return null != this.currentRow;
		}

		public UltraGridRow Current
		{
			get
			{
				return this.currentRow;
			}
		}

		object IEnumerator.Current
		{
			get
			{
				return this.Current;
			}
		}
	}

	#endregion // RowsEnumerator Class

} 
