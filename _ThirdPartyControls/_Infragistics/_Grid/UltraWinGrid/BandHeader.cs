#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
    using System.Drawing;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Windows.Forms;
    using System.Diagnostics;
	using System.Runtime.Serialization;
    using Infragistics.Shared;
    using Infragistics.Win;
	using System.Security.Permissions;

    /// <summary>
    ///	The class represents the header for a specific band
    /// </summary>
    [ Serializable() ]
    public sealed class BandHeader : HeaderBase, ISerializable
    {
		private Infragistics.Win.UltraWinGrid.UltraGridBand band = null;

        internal BandHeader( Infragistics.Win.UltraWinGrid.UltraGridBand band )
        {
			this.band = band;
        }
 
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//public BandHeader( SerializationInfo info, StreamingContext context )
		private BandHeader( SerializationInfo info, StreamingContext context )
		{
			this.visiblePos = -1;

			base.DeserializeHelper( info, context );
		}

		internal void InitBand( Infragistics.Win.UltraWinGrid.UltraGridBand band)
		{
			this.band = band;

			base.InitAppearanceHolder();
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info,
			StreamingContext context )
		{
			base.GetObjectData( info, context );
		}

		internal override string DefaultHeaderCaption 
        { 
            get 
            { 
                return this.Band.Key;
            }
        }
		internal override int Extent
        {
            get
            {
                return this.Band.GetExtent( BandOrigin.RowSelector );
            }
        }

		// JJD 1/23/02 - UWG971
		// Added GetExtent method since the extent will be different for
		// exclusive col regions
		//
		internal int GetExtent( ColScrollRegion csr )
		{
			if ( csr != null && csr.ExclusiveColScrollRegion != null )
				return csr.GetBandExtent( this.Band ) - this.Band.PreRowAreaExtent;

			return this.Extent;
		}

		internal override void InternalSetWidth ( int newWidth, bool autofit )
		{
			if ( this.FirstItem ) 
			{
				newWidth -= this.RowSelectorExtent;
			}
			this.Column.SetWidth ( newWidth, true, true, true, true, true );
		}

		/// <summary>
		/// Returns the UltraGridBand that the object belongs to, if any. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Band</b> property of an object refers to a specific band in the grid as defined by an UltraGridBand object. You use the <b>Band</b> property to access the properties of a specified UltraGridBand object, or to return a reference to the UltraGridBand object that is associated with the current object.</p>
		/// <p class="body">UltraGridBand objects are the foundation of the hierarchical data structure used by UltraWinGrid. Any row or cell in the grid must be accessed through its UltraGridBand object. Bands are also used to apply consistent formatting and behavior to the rows that they comprise. A UltraGridBand object is used to display all the data rows from a single level of a data hierarchy. UltraGridBand objects contain multiple sets of child UltraGridRow objects that actually display the data of the recordset. All of the rows that are drawn from a single Command in the DataEnvironment make up a band.</p>
		/// <p class="body">The rows of a band are generally displayed in groups of one more in order to show rows from subsequent bands that are linked to rows in the current band via the structure of the data hierarchy. For example, if a hierarchical recordset has Commands that display Customer, Order and Order Detail data, each one of these Commands maps to its own UltraGridBand in the UltraWinGrid. The rows in the Customer band will appear separated by any Order data rows that exist for the customers. By the same token, rows in the Order band will be appear separated to make room for Order Detail rows. How this looks depends on the ViewStyle settings selected for the grid, but the concept of visual separation is readily apparent when the UltraWinGrid is used with any hierarchical recordset.</p>
		/// <p class="body">Although the rows in a band may appear to be separated, they are treated contiguously. When selecting a column in a band, you will see that the cells of that column become selected in all rows for the band, regardless of any intervening rows. Also, it is possible to collapse the hierarchical display so that any children of the rows in the current band are hidden.</p>
		/// </remarks>
        [Browsable(false)]
		public override Infragistics.Win.UltraWinGrid.UltraGridBand Band 
		{
			get
			{
				return this.band;
			}
		}


		internal override int GetActualWidth( bool ignoreDraggingWidth )
		{
			return this.Band.GetActualWidth( ignoreDraggingWidth );
		}

		
		/// <summary>
		/// Returns the height of the band header.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Height</b> property is used to determine the vertical dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">For a Header object, this property is read-only.</p>
		/// </remarks>
		public override int Height
		{
			get
			{
				// SSP 7/21/03 Workaround for UWG1558
				// Added an overload of GetColumnHeaderHeightthat ignores whether the header is
				// visible or not and returns a valid height. By default, it returns 0 if the
				// header is hidden but we want it to return a valid value.
				//
				//return this.GetBandHeaderHeight( ); 
				return this.GetBandHeaderHeight( true );
			}
		}

		/// <summary>
		/// Calculate and return the height of this band headers,
		///	without column or group headers
		/// </summary>
		/// <returns></returns>
		internal int GetBandHeaderHeight( )
		{
			return this.GetBandHeaderHeight( false );
		}
	
		/// <summary>
		/// Calculate and return the height of this band headers,
		///	without column or group headers
		/// </summary>
		/// <returns></returns>
		// SSP 7/21/03 Workaround for UWG1558
		// Added an overload of GetBandHeaderHeight that ignores whether the header is
		// visible or not and returns a valid height. By default, it returns 0 if the
		// header is hidden but we want it to return a valid value.
		//
		//internal int GetBandHeaderHeight(  )
		internal int GetBandHeaderHeight( bool ignoreHidden )
		{	
			//	return 0 if headers are hidden
			// SSP 7/21/03 Workaround for UWG1558
			// Added an overload of GetBandHeaderHeight that ignores whether the header is
			// visible or not and returns a valid height. By default, it returns 0 if the
			// header is hidden but we want it to return a valid value.
			//
			//if( !this.Band.HeaderVisible )
			if( ! ignoreHidden && !this.Band.HeaderVisible )
				return 0;

			int maxHeight = 0;
			int fontHeight = 0;

			// get the resolved appearance for the band's header
			AppearanceData appData = new AppearanceData( );			
			AppearancePropFlags flags = AppearancePropFlags.FontData | AppearancePropFlags.Image;
			
			this.ResolveAppearance( ref appData, flags );

			// JJD 12/14/01
			// Moved font height logic into CalculateFontHeight
			//
			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// If the text is rotated, the header height depends on the contents of the caption, 
			// so we need to meaure the font with the text.
			//fontHeight = this.Band.Layout.CalculateFontHeight( ref appData );
			TextOrientationInfo textOrientation = this.Band.Header.TextOrientationResolved;

			if ( textOrientation != null && Object.Equals( textOrientation, TextOrientationInfo.Horizontal ) == false )
			{
				// MD 4/30/08 - BR32311
				// GetExtent returns 1 when the band is in card view, so we should instead pick some 'max' for the extent.
				//fontHeight = this.Band.Layout.CalculateFontSize( ref appData, this.Band.Header.Caption, this.Band.GetExtent( BandOrigin.RowSelector ), textOrientation ).Height;
				int widthConstraint;

				if ( this.Band.CardView )
					widthConstraint = 100000;
				else
					widthConstraint = this.Band.GetExtent( BandOrigin.RowSelector );

				fontHeight = this.Band.Layout.CalculateFontSize( ref appData, this.Band.Header.Caption, widthConstraint, textOrientation ).Height;
			}
			else
				fontHeight = this.Band.Layout.CalculateFontHeight( ref appData );
			
			//	Calculate the height of the header's picture
			Size imageSize = Size.Empty;

			if ( null != appData.Image )
			{
				Image image = appData.GetImage( this.Band.Layout.Grid.ImageList );
				if( null != image )
				{
					imageSize = ((Image)image).Size;
				}
			}

			//	take whichever is bigger, font height or picture height			
			maxHeight = Math.Max( fontHeight, imageSize.Height );

			//	Adjust here for borders, and some arbitrary amount
			//	that we seem to do for all headers
			//
			// JAS 4/27/05 - Use the HeaderBorderThickness property in case the HeaderStyle does not use
			// the conventional UIElementBorderStyle settings.
			//
			//maxHeight += 2 * this.Band.Layout.GetBorderThickness( this.Band.BorderStyleHeaderResolved );
			maxHeight += 2 * this.Band.HeaderBorderThickness;			
			maxHeight += 4; // same adjustment made for column (refer to column code for reasoning)

			return maxHeight;
		}



		/// <summary>
		/// Returns or sets the visible position of a header.
		/// </summary>
		/// <remarks>
		/// <p class="body">For band headers, this property is read-only and always returns zero.</p>
		/// </remarks>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]  
		[ Browsable( false ) ]  
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override int VisiblePosition
		{
			get
			{
				return base.VisiblePosition;
			}
			set
			{
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_21"));
			}
		}
		
		/// <summary>
		/// Returns false since we never need to serialize the visible position
		/// of a band header
		/// </summary>
		/// <returns></returns>
		protected override bool ShouldSerializeVisiblePosition() { return false; }


		internal override SelectChange SelectChangeType 
		{
			get
			{
				return SelectChange.Band;
			}
		}
		internal override bool Hidden 
        { 
            get 
            {
                return !this.Band.HeaderVisible; 
            } 
        }

		internal override bool PivotOnRowScrollRegion { get { return false; } }
		internal override bool PivotOnColScrollRegion { get { return false; } }

		internal override UltraGridColumn GetRelatedVisibleCol( UltraGridColumn column, 
            Infragistics.Win.UltraWinGrid.NavigateType navType, bool includeNonActivateable )
        {
            HeadersCollection headers = this.Band.OrderedHeaders;

            if ( null == headers )
                return null;

			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
            //UltraGridGroup   group = column.Group;			
			UltraGridGroup group = column.GroupResolved;

            UltraGridColumn  relatedCol = null;			
            ColScrollRegion  exclusiveColScrollRegion = column.Header.ExclusiveColScrollRegion;
            bool             columnFound = false;

            if ( NavigateType.Above == navType || 
                NavigateType.Below == navType ||
                NavigateType.Topmost == navType ||
                NavigateType.Bottommost == navType )
            {
                // Above or below only have meaning if we are in a band with
                // groups that have a level count > 1.
                // Otherwise just return the passed in column.
                //
                if ( null != group )
                    //return pGroup->GetRelatedVisibleCol( Column, enumNavType, bIncludeNonActivateable );
                    return group.Header.GetRelatedVisibleCol( column, navType, includeNonActivateable );
                else
                    //return Column.GetRelatedVisibleCol( Column, enumNavType, bIncludeNonActivateable );
                    return column.Header.GetRelatedVisibleCol ( column, navType, includeNonActivateable );
            }

            foreach (HeaderBase h in headers ) 
            {
                if ( h.Hidden ||
                    h.ExclusiveColScrollRegion != exclusiveColScrollRegion )					
                    continue;

                if ( NavigateType.First == navType )
                {
                    // get the first visible col of this position item
                    // if found we can stop here
                    //
                    relatedCol = h.GetFirstVisibleCol( includeNonActivateable );
                    if ( null != relatedCol )
                        break;
                }
                else if ( NavigateType.Next == navType )
                {
                    if ( columnFound )
                    {
                        // if we have already found the passed in column
                        // then get the first visible from this position item
                        // and stop if it has one
                        //
                        relatedCol = h.GetFirstVisibleCol( includeNonActivateable );
                        if ( null != relatedCol )
                            break;
                    }
                }

                if ( ( null != group && group.Header ==  h )  ||
                    column.Header ==  h )
                {
                    // we found the passed in column so set the stack flag
                    //
                    columnFound = true;

                    if ( NavigateType.Next == navType )
                    {
                        // call get next off the position item. If found
                        // we can stop
                        //
                        relatedCol = h.GetNextVisibleCol( column, includeNonActivateable );
                        if ( null != relatedCol )
                            break;
                    }
                    else if ( NavigateType.Prev == navType )
                    {
                        // get the prev col from the position item
                        //
                        UltraGridColumn prevCol = h.GetPrevVisibleCol( column, includeNonActivateable );

                        // if it has a prev column use it. Otherwise use the
                        // pRelatedCol that we have kept track of during the
                        // iteration. Either way we eed to stop
                        //
                        if ( null != prevCol )
                            relatedCol = prevCol;
                        break;
                    }
                }

                if ( NavigateType.Last == navType   ||
                    NavigateType.Prev == navType )
                {
                    // for last and prev we need to keep track of the 
                    // last visible col 
                    //
                    //CColumn* pLastVisibleCol = pPositionItem->GetLastVisibleCol( bIncludeNonActivateable );
                    UltraGridColumn lastVisibleCol = h.GetLastVisibleCol( includeNonActivateable );

                    if ( null != lastVisibleCol )
                        relatedCol = lastVisibleCol;
                }

            }
            return relatedCol;
        }
		internal BandHeader Clone()
		{
			BandHeader header = (Infragistics.Win.UltraWinGrid.BandHeader)this.MemberwiseClone();

			if ( null == header )
				return null;

			return header;
		}


		internal override Activation ActivationResolved
		{
			get
			{
				// Only return disabled if the header is marked disabled
				//
				// MD 8/14/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//return ( this.Band.Header != null && this.Band.Header.Enabled )
				UltraGridBand band = this.Band;

				return ( band.Header != null && band.Header.Enabled )
					? Activation.ActivateOnly
					: Activation.Disabled;
			}
		}
		internal override ISelectionStrategy SelectionStrategyDefault
		{
			get
			{
				return null;
			}
		}

		internal override bool IsValidDropArea( DropLocationInfo dropLocationInfo, Point point )
		{
			return false;
		}
  

		internal override void CalcSelectionRange( Selected newSelection, 
												   bool clearExistingSelection,
												   bool select,
												   Selected initialSelection )
		{}


		#region SizeResolved

		// SSP 6/25/03 - Row Layout Functionality
		// Added SizeResolved method.
		//
		/// <summary>
		/// Resolved header size. This property returns the actual width and the height of the header.
		/// </summary>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override Size SizeResolved 
		{
			get
			{
				UltraGridBand band = this.Band;

				Debug.Assert( null != band, "No band !" );
				if ( null == band )
					return Size.Empty;

				return new Size( band.GetExtent( BandOrigin.RowSelector ), this.Height );
			}
		}

		#endregion // SizeResolved


		// SSP 1/14/04 - Accessibility
		//
		#region BandHeaderAccessibleObject Class

		/// <summary>
		/// The Accessible object for a column header.
		/// </summary>
		public class BandHeaderAccessibleObject : HeaderBase.HeaderAccessibleObjectBase
		{
			#region Private Members

			#endregion Private Members

			#region Constructor
			
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="bandHeader">BandHeader</param>
			/// <param name="parent">The BandHeadersAccessibleObject representing the parent of the header.</param>
			public BandHeaderAccessibleObject( BandHeader bandHeader, HeadersCollection.HeadersAccessibleObject parent ) : base( bandHeader, parent )
			{
			}

			#endregion Constructor

			#region Base Class Overrides

				// MRS 6/3/04 - UWG2963
				// We have to override this because HeaderBase will end up returning
				// the bounds of the first column header instead of the band header.
				#region Bounds

				/// <summary>
				/// Gets the location and size of the accessible object.
				/// </summary>
				public override System.Drawing.Rectangle Bounds
				{
					get
					{
						Infragistics.Win.UIElement element = this.ParentInternal.UIElement;

						if ( element == null )
							return Rectangle.Empty;

						bool foundElement = false;

						foreach ( Infragistics.Win.UIElement childElement in element.ChildElements )
						{						
							if ( childElement != null && childElement.GetType() != typeof(BandHeadersUIElement ) && childElement.HasContext(this.Header, false) )
							{
								element = childElement;
								foundElement = true;
								break;
							}
						}

						if ( !foundElement || element == null )
							return Rectangle.Empty;

						Control control = element.Control;

						if ( control == null )
							return Rectangle.Empty;

						return control.RectangleToScreen( element.ClipRect );
					}
				}

					#endregion Bounds

				#region Navigate

			/// <summary>
			/// Navigates to another accessible object.
			/// </summary>
			/// <param name="navdir">One of the <see cref="System.Windows.Forms.AccessibleNavigation"/> values.</param>
			/// <returns>An AccessibleObject relative to this object.</returns>
			public override AccessibleObject Navigate(AccessibleNavigation navdir)
			{
				switch ( navdir )
				{
						
						#region case FirstChild

					case AccessibleNavigation.FirstChild:
						return null;

						#endregion case FirstChild

						
						#region case LastChild

					case AccessibleNavigation.LastChild:
					{
						return null;
					}

						#endregion case LastChild

				}

				switch ( navdir )
				{
					case AccessibleNavigation.Down:
						// Fall through. For the band header, Down and Next should get the
						// first group header if groups are turned on otherwise the first column
						// header.
						//
					case AccessibleNavigation.Next:
						if (  this.Band.UseRowLayoutResolved )
							return this.ParentInternal.GetAccessibleObjectFor( this.Band.LayoutOrderedVisibleColumnHeaders.FirstVisibleHeader );
						else
							return this.ParentInternal.GetAccessibleObjectFor( this.Band.OrderedHeaders.FirstVisibleHeader );
				}

				return null;
			}

				#endregion Navigate

				#region Role

			/// <summary>
			/// Gets the role of this accessible object.
			/// </summary>
			public override AccessibleRole Role
			{
				get
				{
					return AccessibleRole.StaticText;
				}
			}

				#endregion Role

				#region State

			/// <summary>
			/// Gets the state of this accessible object.
			/// </summary>
			public override AccessibleStates State
			{
				get
				{
					if ( this.HiddenResolved )
						return AccessibleStates.Invisible;

					return AccessibleStates.ReadOnly;
				}
			}

				#endregion State

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region Band

			/// <summary>
			/// Returns the associated UltraGridBand object.
			/// </summary>
			public UltraGridBand Band
			{ 
				get 
				{
					return ((BandHeader)this.Header).Band;
				} 
			}

					#endregion Band

				#endregion Public Properties

			#endregion Properties

		}

		#endregion BandHeaderAccessibleObject Class

		#region Fixed

		// SSP 7/1/05 BR04857
		// Made the property virtual and overrode it here so it can be marked Browsable( false ).
		// 
		/// <summary>
		/// Overridden. Always throws an exception since this is not applicable to band headers.
		/// </summary>
		/// <remarks>
		/// <p class="body">This method always throws an exception since this is not applicable to band headers.</p>
		/// </remarks>
		[ Browsable( false ) ]
		public override bool Fixed
		{
			get
			{
				return base.Fixed;
			}
			set
			{
				throw new NotSupportedException( "Fixed property is not supported on BandHeaders." );
			}
		}

		#endregion // Fixed

		#region FixedHeaderIndicator

		/// <summary>
		/// Overridden. This property is not applicable to BandHeaders.
		/// </summary>
		/// <remarks>
		/// <p class="body">Overridden. This property is not applicable to BandHeaders.</p>
		/// </remarks>
		// SSP 7/1/05 BR04857
		// Made virtual and overrode it on the BandHeader to mark it Browsable( false ) since
		// the band headers can't be fixed.
		// 
		[ Browsable( false ) ]
		public override Infragistics.Win.UltraWinGrid.FixedHeaderIndicator FixedHeaderIndicator
		{
			get
			{
				return base.FixedHeaderIndicator;
			}
			set
			{
				throw new NotSupportedException( "FixedHeaderIndicator property is not supported on BandHeaders." );
			}
		}

		#endregion // FixedHeaderIndicator

		#region ResetFixed

		// SSP 7/29/05 BR04857
		// Made virtual and overrode it on the BandHeader since set of BandHeader.Fixed 
		// throws an exception.
		// 
		/// <summary>
		/// Resets the property to its default value of false.
		/// </summary>
		public override void ResetFixed( )
		{
			// Take no action since the Fixed property is not supported on BandHeader.
			// 
		}

		#endregion // ResetFixed

		#region ResetFixedHeaderIndicator

		// SSP 7/29/05 BR04857
		// Made virtual and overrode it on the BandHeader since set of BandHeader.Fixed 
		// throws an exception.
		// 
		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public override void ResetFixedHeaderIndicator( )
		{
			// Take no action since the FixedHeaderIndicator property is not supported on BandHeader.
			// 
		}

		#endregion // ResetFixedHeaderIndicator

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		#region 8.2 - Rotated Column Headers

		internal override TextOrientationInfo DefaultTextOrientation
		{
			get { return null; }
		} 

		#endregion 8.2 - Rotated Column Headers
    }
}
