#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;


    /// <summary>
    /// Collection of all VisibleRows within a specific
    /// row scrolling region.
    /// </summary>
	public class VisibleRowsCollection : SubObjectsCollectionBase
	{
		private bool dirty = true;
		private bool internalCopyInProgress = false;
		private RowScrollRegion	rowScrollRegion;
        private System.Collections.Queue cachedVisibleRows = null;

		/// <summary>
		/// consutructor that takes a rowScrollRegion as a parameter
		/// </summary>
		/// <param name="rowScrollRegion">row scroll region to associate
		/// this visible rows colleciton with</param>
		public VisibleRowsCollection( RowScrollRegion rowScrollRegion )
		{
			this.rowScrollRegion = rowScrollRegion;
		}

		/// <summary>
		/// The row scroll region that owns this visible rows
		/// collection
		/// </summary>
		public RowScrollRegion RowScrollRegion
		{
			get
			{
				return this.rowScrollRegion;
			}
		}

		/// <summary>
		/// returns whether the collection is read-only
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				if ( this.internalCopyInProgress )
					return false;

				return true;
			}
		}

		// SSP 4/12/04
		// Renamved the method name because we are clearing the source in addition to
		// copying from the source.
		//
		//internal void CopyFrom( VisibleRowsCollection source )
		internal void CopyRowsFromHelper( VisibleRowsCollection source )
		{
			this.internalCopyInProgress = true;

			// SSP 4/12/04
			// Make sure that visible rows unhook from rows. In InternalClear, InternalRemove
			// etc of VisibleRows, we ensure that the visible row unhooks from the row. We need
			// to do the same here.
			// Commented out the original code and added new code below it.
			//
			// --------------------------------------------------------------------------------
			//this.All = source.All;
			this.InternalClear( );
			this.All = source.All;
			
			// Also make sure that copied visible rows point to the right row scroll region.
			// 
			for ( int i = 0; i < this.Count; i++ )
				this[i].InternalSetRowScrollRegion( this.rowScrollRegion );

			// Call source.List.Clear instead of source.InternalClear because we don't want
			// the source.InternalClear to unhook the visible rows from the rows.
			//
			source.List.Clear( );
			// --------------------------------------------------------------------------------

			this.internalCopyInProgress = false;
		}

 

		/// <summary>
		/// indexer 
		/// </summary>
		public Infragistics.Win.UltraWinGrid.VisibleRow this[ int index ]
		{
			get
			{
				return (VisibleRow)this.GetItem( index );
			}
		}

		/// <summary>
		/// Specifies the initial capacity of the collection
		/// </summary>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		protected override int InitialCapacity
		{
			get
			{
				return 30;
			}
		}

		/// <summary>
		/// Returns true if the visible rows collection needs to
		/// be refreshed
		/// </summary>
		public bool Dirty
		{
			get 
			{
				return this.dirty;
			}
			set
			{
				this.dirty = value;
			}
		}


		
		internal VisibleRow LastRow
		{
			get
			{
				return this.Count <= 0 ? null : this[this.Count-1];
			}
		}
	
        /// <summary>
        /// A queue to hold the cached rows.
        /// </summary>
        protected System.Collections.Queue CachedVisibleRows
        {
            get
            {
                if ( this.cachedVisibleRows == null )
                    this.cachedVisibleRows = new Queue(30);

                return this.cachedVisibleRows;
            }
        }

		internal VisibleRow VisibleRowFromCache
        {
			get
			{
				// only return a VisibleRow from the cache if there are more
				// than 2 vrs in the cache
				//
				if ( this.cachedVisibleRows != null &&
					this.cachedVisibleRows.Count > 2 )
					return (VisibleRow)this.cachedVisibleRows.Dequeue();

				return new VisibleRow( this.rowScrollRegion );
			}

        }
		internal int InternalAdd( VisibleRow visibleRow ) 
        {
            return this.List.Add( visibleRow );
        }
		internal void InternalInsert( int index, VisibleRow visibleRow ) 
        {
            base.InternalInsert( index, visibleRow );
        }
		internal void InternalInsert( VisibleRow visibleRow, VisibleRow insertBeforeVisibleRow ) 
        {
            int index = -1;

            // get the index of the passed in 'before' row
            //
            if ( insertBeforeVisibleRow != null )
                index = this.IndexOf( insertBeforeVisibleRow );

            // if a valid before row was supplied then insert the new row 
            // before it. Otherwise, append the new row
            //
            if ( index >= 0 )
                this.InternalInsert( index, visibleRow );
            else
                this.InternalAdd( visibleRow );

        }

        /// <summary>
        /// Pushes the VisibleRow onto the stack of visiblerows that
        /// we cache for later re-use
        /// </summary>
        protected void CacheVisibleRowForRecycling( VisibleRow visibleRow ) 
        {

            visibleRow.UnhookFromRowNotifications();

			// SSP 11/27/01 UWG667
			// Unhook the parent visible rows as well otherwise we are going
			// to end up with multiple visible rows hooked into a single row's
			// SubObjectPropChanged event even though there should be only one 
			// visible row associated with a row at a time. This will slow down 
			// certain things (like selecting the row ) extremely because multiple 
			// visible rows will be handling the same notification even though 
			// only one visible row (one that's currently associated with the row) 
			// needs to handle it.
			//
			// Traverse all the ancestor visible rows and unhook them.
			// This may lead to UnhookFromRowNotifications being called on the
			// same visible row multiple times (since multiple visible rows could
			// have the same parent visible row), however that's not a problem
			// since UnhookFromRowNotifications keeps a flag indicating whether it's
			// hooked into the row's SubObjectPropChanged event and unhooks accordingly.
			//
			VisibleRow vr = visibleRow.ParentVisibleRow;
			while ( null != vr )
			{
				// JJD 1/18/02 - UWG941
				// Only unhook the row if it isn't in the collection
				//
				if ( !this.Contains( vr ) )
					vr.UnhookFromRowNotifications( );

				vr = vr.ParentVisibleRow;
			}

            
            //this.CachedVisibleRows.Enqueue( visibleRow );
        }
		internal void InternalRemove( VisibleRow visibleRow ) 
        {
            // first cache the row for re-use later
            //
            this.CacheVisibleRowForRecycling( visibleRow );

            this.List.Remove( visibleRow );
        }

		#region InternalRemoveAt

		// SSP 4/28/05 - NAS 5.2 Fixed Rows_
		// Added InternalRemoveAt that takes in an index.
		//
		internal void InternalRemoveAt( int index )
		{
			VisibleRow vr = this[ index ];
			this.List.RemoveAt( index );

			this.CacheVisibleRowForRecycling( vr );
		}

		#endregion // InternalRemoveAt

		internal new void InternalClear() 
        {
            // first cache each row for re-use later
            //
            foreach( VisibleRow vr in this )
			{
                this.CacheVisibleRowForRecycling( vr );
			}

            this.List.Clear();
        }

		#region NAS 5.2 Fixed Rows

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        #region HasFixedRow

		//        // SSP 3/28/05 - NAS 5.2 Fixed Rows
		//        // Added HasFixedRow method.
		//        //
		//#if DEBUG
		//        /// <summary>
		//        /// Returns true if any of the row in the visible row collection is fixed.
		//        /// </summary>
		//        /// <returns></returns>
		//#endif
		//        internal bool HasFixedRow( )
		//        {
		//            for ( int i = 0; i < this.Count; i++ )
		//            {
		//                VisibleRow vr = this[i];
		//                UltraGridRow row = null != vr ? vr.Row : null;
		//                if ( null != row && row.FixedResolved )
		//                    return true;
		//            }

		//            return false;
		//        }

		//        #endregion // HasFixedRow

		#endregion Not Used

		#region GetAssociatedVisibleRow
		
		// SSP 4/28/05 - NAS 5.2 Fixed Rows_
		// 
		internal VisibleRow GetAssociatedVisibleRow( UltraGridRow row )
		{
			int index = this.IndexOfAssociatedVisibleRow( row );
			return index >= 0 ? this[ index ] : null;
		}

		#endregion // GetAssociatedVisibleRow

		#region GetFirstLastScrollableRow

		internal VisibleRow GetFirstLastScrollableRow( bool first, bool fullyVisible )
		{
			return this.GetFirstLastScrollableRow( first, fullyVisible, ScrollbarVisibility.Check );
		}
		
		internal VisibleRow GetFirstLastScrollableRow( bool first, bool fullyVisible, ScrollbarVisibility colScrollbarVisibility )
		{
			int start, end, step;
			if ( first )
			{
				start = 0;
				end = this.Count - 1;
				step = 1;
			}
			else
			{
				start = this.Count - 1;
				end = 0;
				step = -1;
			}

			for ( int i = start; first ? i <= end : i >= end; i += step )
			{
				VisibleRow vr = this[ i ];
				if ( ! vr.Row.FixedResolved )
				{
					if ( ! fullyVisible || this.IsFullyVisible( vr, colScrollbarVisibility ) )
						return vr;
				}
			}

			return null;
		}

		#endregion // GetFirstLastScrollableRow

		#region IsFullyVisible

		// SSP 7/21/05 BR05030
		// 
		internal bool IsFullyVisible( VisibleRow vr, ScrollbarVisibility colScrollbarVisibility )
		{
            return this.RowScrollRegion.IsRowFullyVisible( colScrollbarVisibility, vr );
		}

		#endregion // IsFullyVisible

		#region IsFullyVisible

		// SSP 7/21/05 BR05030
		// 
		internal bool IsFullyVisible( UltraGridRow row, ScrollbarVisibility colScrollbarVisibility )
		{
			VisibleRow vr = this.GetAssociatedVisibleRow( row );
			return null != vr && this.IsFullyVisible( vr, colScrollbarVisibility );
		}

		#endregion // IsFullyVisible

		#region GetFirstScrollableRow

		internal VisibleRow GetFirstScrollableRow( )
		{
			return this.GetFirstLastScrollableRow( true, false );
		}

		#endregion // GetFirstScrollableRow

		#region IndexOfAssociatedVisibleRow

		internal int IndexOfAssociatedVisibleRow( UltraGridRow row )
		{
			for ( int i = 0, count = this.Count; i < count; i++ )
			{
				VisibleRow vr = this[ i ];
				if ( row == vr.Row )
					return i;
			}

			return -1;
		}

		#endregion // IndexOfAssociatedVisibleRow

		#region HasAssociatedVisibleRow

		internal bool HasAssociatedVisibleRow( UltraGridRow row )
		{
			return this.IndexOfAssociatedVisibleRow( row ) >= 0;
		}

		#endregion // HasAssociatedVisibleRow

		#region InternalMoveSectionToEnd

		internal void InternalMoveRowsToEnd( int start, int count )
		{
			ArrayList list = this.List;
			for ( int i = 0; i < count; i++ )
				list.Add( list[ start + i ] );

			list.RemoveRange( start, count );
		}

		#endregion // InternalMoveSectionToEnd

		#endregion // NAS 5.2 Fixed Rows

		/// <summary>
		/// IEnumerable Interface Implementation
        /// </summary>
        /// <returns>A type safe enumerator</returns>
        public VisibleRowEnumerator GetEnumerator() // non-IEnumerable version
        {
           return new VisibleRowEnumerator(this);
        }

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.VisibleRow[] array, int index)
		{
			base.CopyTo( array, index );
		}
		#endregion //CopyTo

		/// <summary>
		/// inner class implements IEnumerator interface
		/// </summary>
        public class VisibleRowEnumerator : DisposableObjectEnumeratorBase
        {   
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="visibleRows">The visible rows collection to enumerate.</param>
            public VisibleRowEnumerator(VisibleRowsCollection visibleRows) : base( visibleRows )
            {
            }

			/// <summary>
			/// non-IEnumerator version: type-safe
			/// </summary>
            public VisibleRow Current
            {
                get
                {
                    return (VisibleRow)((IEnumerator)this).Current;
                }
            }			
        }
    }
}
