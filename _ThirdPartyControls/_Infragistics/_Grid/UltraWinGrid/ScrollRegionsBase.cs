#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Collections;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Security.Permissions;


    /// <summary>
    /// Collection of all row scrolling regions in a layout
    /// </summary>
	

    public abstract class ScrollRegionsCollectionBase : SubObjectsCollectionBase
    {
        private Infragistics.Win.UltraWinGrid.UltraGridLayout layout;
		internal ScrollRegionsCollectionBase( UltraGridLayout layout )
        {
            this.layout = layout;
        }
		internal ScrollRegionsCollectionBase()
		{		
		}

		/// <summary>
		/// Returns the Layout object that determines the layout of the object. 
		/// This property is read-only at run-time.
		/// </summary><remarks><para>The Layout property of an object is used to access 
		/// the Layout object that determines the settings of various properties 
		/// related to the appearance and behavior of the object. The Layout object 
		/// provides a simple way to maintain multiple layouts for the grid and 
		/// apply them as needed. You can also save grid layouts to disk, the 
		/// registry or a storage stream and restore them later.</para><para>The 
		/// Layout object has properties such as Appearance and Override, so the 
		/// Layout object has sub-objects of these types, and their settings are 
		/// included as part of the layout. However, the information that is 
		/// actually persisted depends on how the settings of these properties were 
		/// assigned. If the properties were set using the Layout object's intrinsic 
		/// objects, the property settings will be included as part of the layout. 
		/// However, if a named object was assigned to the property from a 
		/// collection, the layout will only include the reference into the collection, 
		/// not the actual settings of the named object. 
		/// (For an overview of the difference between named and intrinsic objects, 
		/// please see the <see cref="UltraGridLayout.Appearance"/>property.</para>
		/// <para>
		/// For example, if the Layout object's Appearance property is used to 
		/// set values for the intrinsic Appearance object like this:</para><pre>
		/// UltraGrid1.DisplayLayout.Appearance.ForeColor = vbBlue</pre><para>
		/// Then the setting (in this case, ForeColor) will be included as part 
		/// of the layout, and will be saved, loaded and applied along with the 
		/// other layout data. However, suppose you apply the settings of a named 
		/// object to the UltraGridLayout's <b>Appearance</b> property in this manner:</para>
		/// <p class="code">UltraGrid1.Appearances.Add "New1"</p>
		/// <p class="code">UltraGrid1.Appearances("New1").ForeColor = System.Drawing.Color.Blue</p>
		/// <p class="code">UltraGrid1.Layout.Appearance = UltraGrid1.Appearances("New1")</p>
		/// <para>In this case, the ForeColor setting will not be persisted as 
		/// part of the layout. Instead, the layout will include a reference to 
		/// the "New1" Appearance object and use whatever setting is present in 
		/// that object when the layout is applied.</para><para>By default, the 
		/// layout includes a copy of the entire Appearances collection, so if the 
		/// layout is saved and restored using the default settings, the object 
		/// should always be present in the collection when it is referred to. 
		/// However, it is possible to use the Load and Save methods of the Layout object 
		/// in such a way that the collection will not be re-created when the layout 
		/// is applied. If this is the case, and the layout contains a reference to 
		/// a nonexistent object, the default settings for that object's properties 
		/// will be used.</para>
		/// </remarks>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
         Browsable(false)   ]
        public UltraGridLayout Layout
        {
            get 
            {
                return this.layout;
            }
			set
			{
				this.layout = value;
			}
        }
 
        /// <summary>
        /// Specifies the initial capacity of the collection
        /// </summary>
        [ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
        protected override int InitialCapacity
        {
            get
            {
                return 3;
            }
        }

        internal abstract void CheckIfSizeChanged();
        
		internal abstract void InitFirstRegion(); 

        internal ScrollRegionBase FirstVisibleRegion 
        {
			get 
			{
				// make sure we have at least one region;
				//
				InitFirstRegion();

				// loop thru all regions looking for the first one
				// that isn't hidden
				//
				for ( int i = 0; i < this.Count; i++ )
				{
					if ( !((ScrollRegionBase)this.GetItem( i )).Hidden )
						return (ScrollRegionBase)this.GetItem( i );
				}

				// if none are visible then return the first 
				//
				return (ScrollRegionBase)this.GetItem( 0 );
			}

        }
        
        internal ScrollRegionBase LastVisibleRegion 
        {
			get
			{
				// JJD 01/02/02
				// If autofit columns is true then we only show 1 scroll region
				//
				if ( this is ColScrollRegionsCollection && 
					this.layout.AutoFitAllColumns )
					return this.FirstVisibleRegion;

				// make sure we have at least one region;
				//
				InitFirstRegion();

				// loop thru all regions backwards looking for 
				// the first one that isn't hidden (which because
				// we are looping backwards is actually the last
				// visible region
				//
				for ( int i = this.Count - 1; i >= 0; i-- )
				{
					if ( !((ScrollRegionBase)this.GetItem( i )).Hidden )
						return (ScrollRegionBase)this.GetItem( i );
				}

				// if none are visible then return the first 
				//
				return (ScrollRegionBase)this.GetItem( 0 );
			}

        }

        /// <summary>
        /// Returns true is any of the properties have been
        /// set to non-default values
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerialize() 
        {
			if ( this.Count > 1 )
				return true;

            foreach ( ScrollRegionBase region in this ) 
            {
                if ( region.ShouldSerialize() )
                    return true;
            }

            return false;
        }
 
        /// <summary>
        /// Resets all properties back to their default values
        /// </summary>
		// AS - 11/21/01
		// Changed to virtual since the derived objects need to 
		// clean up and clear the collection.
		//
		public virtual void Reset() 
        {
            foreach ( ScrollRegionBase region in this ) 
            {
                region.Reset();
            }
        }
		// SSP 11/4/04 UWG3593
		// Changed the parameter for assumeColScrollbarsVisible from boolean to an enum
		// that specifies whether to assume and if so to whether to assume the scrollbar
		// is hidden or visible. This is to fix UWG3593 where the scrollbars are displayed 
		// even when they are not needed because we assume the horizontal scrollbar is 
		// visible to find out if vertical scrollbar should be displayed or not.
		//
		//internal abstract bool AreScrollbarsVisible ( bool fAssumeColScrollbarsVisible );
		internal abstract bool AreScrollbarsVisible( ScrollbarVisibility assumeColScrollbarsVisible );
	
		/// <summary>
		/// IEnumerable Interface Implementation
        /// </summary>
        /// <returns>A type safe enumerator</returns>
        public ScrollRegionBaseEnumerator GetEnumerator() // non-IEnumerable version
        {
           return new ScrollRegionBaseEnumerator(this);
        }

		/// <summary>
		/// inner class implements IEnumerator interface
		/// </summary>
        public class ScrollRegionBaseEnumerator : DisposableObjectEnumeratorBase
        {   
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="rowScrollRegions">The collection of scroll regions to enumerate.</param>
            public ScrollRegionBaseEnumerator(ScrollRegionsCollectionBase rowScrollRegions) : base( rowScrollRegions )
            {
            }

			/// <summary>
			/// non-IEnumerator version: type-safe
			/// </summary>
            public ScrollRegionBase Current
            {
                get
                {
                    return (ScrollRegionBase)((IEnumerator)this).Current;
                }
            }
        }
    }
}
