#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Collections.Generic;

// SSP 4/23/02
// Added classes for summary footer area
//


namespace Infragistics.Win.UltraWinGrid
{
	#region SummaryFooterUIElement

	/// <summary>
	/// Summary footer ui element.
	/// </summary>
	public class SummaryFooterUIElement : UIElement
	{
		#region Private variables

		// SSP 6/13/03 - Fixed headers
		//
		internal bool usingFixedHeaders = false;

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Made this method public and added SummaryDisplayAreas parameter.
		//
		private SummaryDisplayAreaContext summaryDisplayAreaContext = null;
		private Rectangle clipSelfRect = Rectangle.Empty;

		// SSP 5/18/05 - Optimization
		//
		internal Rectangle lastClipRect = Rectangle.Empty;

		#endregion // Private variables

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		/// <param name="rows">The rows collection to which the summary footer element belongs.</param>
		// SSP 12/18/02
		// Made the cunstructor public.
		//
		//internal SummaryFooterUIElement( UIElement parent, RowsCollection rows ) : base( parent )
		public SummaryFooterUIElement( UIElement parent, RowsCollection rows ) : this( parent, rows, SummaryDisplayAreaContext.ALL_EXCEPT_GROUPBY_SUMMARIES )
		{
		}

		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Added following constructor overload.
		//
		internal SummaryFooterUIElement( UIElement parent, RowsCollection rows, SummaryDisplayAreaContext summaryDisplayAreaContext ) 
			: base( parent )
		{
			this.Initialize( rows, summaryDisplayAreaContext );
		}

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Added following constructor overload.
		//
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
        /// <param name="rows">The rows collection to which the summary footer element belongs.</param>        
		/// <param name="summaryDisplayArea">Summaries matching one of the specified display area will be displayed.</param>
		public SummaryFooterUIElement( UIElement parent, RowsCollection rows, SummaryDisplayAreas summaryDisplayArea ) 
			: this( parent, rows, new SummaryDisplayAreaContext( summaryDisplayArea ) )
		{
		}

		#endregion // Constructor

		#region Private/Internal methods

		#region Initialize

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added public Initialize.
		//
		/// <summary>
		/// Initializes this SummaryFooterUIElement instance with rows.
		/// </summary>
        /// <param name="rows">The rows collection to which the summary footer element belongs.</param>        
        /// <param name="summaryDisplayArea">Summaries matching one of the specified display area will be displayed.</param>
		public void Initialize( RowsCollection rows, SummaryDisplayAreas summaryDisplayArea )
		{
			this.Initialize( rows, new SummaryDisplayAreaContext( summaryDisplayArea ) );
		}

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		internal void Initialize( RowsCollection rows, SummaryDisplayAreaContext summaryDisplayAreaContext )
		{
			if ( null == rows )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_342"), Shared.SR.GetString("LE_ArgumentNullException_343") );

			this.PrimaryContext = rows;

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			GridUtils.ValidateNull( summaryDisplayAreaContext );
			this.summaryDisplayAreaContext = summaryDisplayAreaContext;
			this.clipSelfRect = Rectangle.Empty;
		}

		#endregion // Initialize

		#region Rows

		// SSP 4/30/05
		// Made this public.
		//
		/// <summary>
		/// Returns the associated rows collection.
		/// </summary>
		public RowsCollection Rows
		{
			get
			{
				return (RowsCollection)this.PrimaryContext;
			}
		}
		
		#endregion // Rows
		
		#region SummaryValues

		internal SummaryValuesCollection SummaryValues
		{
			get
			{
				RowsCollection rows = this.Rows;
			
				return null != rows ? rows.SummaryValues : null;
			}
		}

		#endregion // SummaryValues

		#region SummaryDisplayAreaContext

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		internal Infragistics.Win.UltraWinGrid.SummaryDisplayAreaContext SummaryDisplayAreaContext
		{
			get
			{
				return this.summaryDisplayAreaContext;
			}
		}

		#endregion // SummaryDisplayAreaContext

		#endregion

		#region Protected overridden methods/properties

		#region GetContext

        /// <summary>
        /// Returns an object of requested type that relates to the element or null.
        /// </summary>
        /// <param name="type">The requested type or null to pick up default context object.</param>
        /// <param name="checkParentElementContexts">If true will walk up the parent chain looking for the context.</param>
        /// <returns>Returns null or an object of requested type that relates to the element.</returns>
        /// <remarks> Classes that override this method normally need to override the <see cref="Infragistics.Win.UIElement.ContinueDescendantSearch(System.Type,System.Object[])"/> method as well.</remarks>
        public override object GetContext(Type type, bool checkParentElementContexts)
		{
			// Check for the SummaryValuesCollection type since the primary context is
			// the rows collection.
			//
			if ( typeof( Infragistics.Win.UltraWinGrid.SummaryValuesCollection ) == type )
				return null != this.Rows ? this.Rows.SummaryValues : null;

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			if ( typeof( SummaryDisplayAreaContext ) == type && null != this.SummaryDisplayAreaContext )
				return this.SummaryDisplayAreaContext;

			// Call the base class implementation
			//			
			return base.GetContext( type, checkParentElementContexts );
		}

		#endregion // GetContext

		#region ContinueDescendantSearch

		// SSP 12/19/02
		// Optimizations.
		// Overrode ContinueDescendantSearch method.
		//
		/// <summary>
		/// This method is called from <see cref="Infragistics.Win.UIElement.GetDescendant(Type,object[])"/> as an optimization to
		/// prevent searching down element paths that can't possibly contain the element that is being searched for. 
		/// </summary>
		/// <remarks><seealso cref="Infragistics.Win.UIElement.ContinueDescendantSearch"/></remarks>
		/// <returns>Returns false if the search should stop walking down the element chain because the element can't possibly be found.</returns>
        protected override bool ContinueDescendantSearch( Type type, object[] contexts )
		{
			if ( null != contexts )
			{
				object primaryContext = this.PrimaryContext;
				Type   primaryContextType = null != primaryContext ? primaryContext.GetType( ) : null;

				for ( int i = 0; i < contexts.Length; i++ )
				{
					object context = contexts[i];

					if ( null != context )
					{
						if ( context is UltraGridRow || context is UltraGridCell )
							return false;

						Type contextType = context.GetType( );

						if ( typeof( SummaryValuesCollection ) == contextType && context != this.SummaryValues )
							return false;

						if ( primaryContextType == contextType && context != primaryContext )
							return false;
					}
				}
			}

			return true;
		}

		#endregion // ContinueDescendantSearch

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			SummaryValuesCollection summaryValues = this.SummaryValues;
			Debug.Assert( null != summaryValues, "No summary values !" );

			if ( null != summaryValues )
				// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
				// Added an overload of ResolveSummaryFooterAppearance that takes in 
				// summaryDisplayAreaContext parameter.
				// 
				//this.SummaryValues.ResolveSummaryFooterAppearance( ref appearance, ref requestedProps );
				summaryValues.ResolveSummaryFooterAppearance( this.SummaryDisplayAreaContext, ref appearance, ref requestedProps );
		}

		#endregion // InitAppearance

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>        
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				// Return None if this summary footer is inside a group-by row.
				//
				//return this.Rows.Band.BorderStyleSummaryFooterResolved;
				return this.Rows.Band.GetBorderStyleSummaryFooterResolved( this.SummaryDisplayAreaContext );
			}
		}

		#endregion // BorderStyle

		#region ClipChildren

		// SSP 6/13/03 - Fixed headers
		// Overrode ClipChildren so we can clip the cells in the row cell area element.
		//
		/// <summary>
		/// Overridden. Returning true causes all drawing of this element's child elements to be
		/// expicitly clipped to the area inside this elements borders
		/// </summary>
		protected override bool ClipChildren 
		{ 
			get 
			{ 
				if ( this.usingFixedHeaders )
					return true;
				else
					return base.ClipChildren;
			}
		}

		#endregion // ClipChildren

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			// SSP 6/13/03 - Fixed headers
			//
			ColScrollRegion csr = (ColScrollRegion)this.GetContext( typeof( ColScrollRegion ), true );
			this.usingFixedHeaders = null != csr && null != this.Rows && this.Rows.Band.UseFixedHeadersResolved( csr );

			UIElementsCollection oldElements = this.childElementsCollection;
			this.childElementsCollection = null;

			RowsCollection rows = this.Rows;

			Debug.Assert( null != rows, "Null rows !" );
			if ( null == rows )				
				return;

			UltraGridBand band = rows.Band;

			if ( !band.HasSummaries )
				return;
			
			SummaryValuesCollection summaryValues = rows.SummaryValues;

			Debug.Assert( null != summaryValues, "RowsCollection.SummaryValues returned null !" );
			if ( null == summaryValues )
				return;

            SummarySettingsCollection summarySettings = band.Summaries;

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//bool hasFixedSummaryArea = summarySettings.HasFixedSummaries;
			//bool hasFreeFormSummaryArea = summarySettings.HasFreeFormSummaries;
			bool hasFixedSummaryArea = summarySettings.HasFixedSummaries( this.SummaryDisplayAreaContext );
			bool hasFreeFormSummaryArea = summarySettings.HasFreeFormSummaries( this.SummaryDisplayAreaContext );
			
			Rectangle rectInsideBorders = this.RectInsideBorders;

			// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
			//
			//bool hasCaptionArea = band.SummaryFooterCaptionVisibleResolved;
			//if ( hasCaptionArea )
			int captionAreaHeight = summaryValues.GetCaptionAreaHeight( this.SummaryDisplayAreaContext );
			bool hasCaptionArea = captionAreaHeight > 0;
			if ( hasCaptionArea )
			{
				// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
				//
				//int height = summaryValues.GetCaptionAreaHeight( );
				int height = captionAreaHeight;

				SummaryFooterCaptionUIElement summaryCaptionElem = 
					(SummaryFooterCaptionUIElement)SummaryFooterUIElement.ExtractExistingElement( 
					oldElements, typeof( SummaryFooterCaptionUIElement ), true );

				if ( null == summaryCaptionElem )
					summaryCaptionElem = new SummaryFooterCaptionUIElement( this );

				Rectangle rect = rectInsideBorders;
				rect.Height = height;

				// Take into account the merged borders of the summary footer and the caption.
				//
				if ( 
					// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
					// Use this.BorderStyle instead which returns None based on whether the footer is
					// inside a group-by row.
					//
					//band.Layout.CanMergeAdjacentBorders( band.BorderStyleSummaryFooterResolved ) &&
					band.Layout.CanMergeAdjacentBorders( this.BorderStyle ) &&
					band.Layout.CanMergeAdjacentBorders( band.BorderStyleSummaryFooterCaptionResolved ) )
				{
					// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
					// Use this.BorderStyle instead which returns None based on whether the footer is
					// inside a group-by row.
					//
					//int borderWidth = band.Layout.GetBorderThickness( band.BorderStyleSummaryFooterResolved );
					int borderWidth = band.Layout.GetBorderThickness( this.BorderStyle );

					rectInsideBorders.Y -= borderWidth;
					rectInsideBorders.Height += borderWidth;
					height += borderWidth;
					
					// Also if we don't have fixed area and free form area, then merge the bottom border
					// as well.
					//
					if ( !hasFixedSummaryArea && !hasFreeFormSummaryArea )
						height += borderWidth;

					// Top merged border.
					//
					rect.Y = rectInsideBorders.Y;
					rect.Height = height;

					// Left and the right merged borders.
					//
					rect.X -= borderWidth;
					rect.Width += 2 * borderWidth;
				}

				rectInsideBorders.Height = rectInsideBorders.Bottom - rect.Bottom;
				rectInsideBorders.Y = rect.Bottom;
				summaryCaptionElem.Rect = rect;

				this.ChildElements.Add( summaryCaptionElem );
			}

			
			// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
			// Use this.BorderStyle instead which returns None based on whether the footer is
			// inside a group-by row.
			//
			//if ( band.Layout.CanMergeAdjacentBorders( band.BorderStyleSummaryFooterResolved, band.BorderStyleSummaryValueResolved ) )
			if ( band.Layout.CanMergeAdjacentBorders( this.BorderStyle, band.BorderStyleSummaryValueResolved ) )
			{
				int borderWidth = band.Layout.GetBorderThickness( band.BorderStyleSummaryValueResolved );

				// Left and right merged border
				//
				rectInsideBorders.X -= borderWidth;
				rectInsideBorders.Width += 2 * borderWidth;
			
				// Top merged border
				//
				if ( !hasCaptionArea )
				{
					rectInsideBorders.Y -= borderWidth;
					rectInsideBorders.Height += borderWidth;
				}

				// Bottom merged border
				//
				rectInsideBorders.Height += borderWidth;
			}

			if ( hasCaptionArea && band.Layout.CanMergeAdjacentBorders( band.BorderStyleSummaryFooterCaptionResolved, 
							band.BorderStyleSummaryValueResolved ) )
			{
				int borderWidth = band.Layout.GetBorderThickness( band.BorderStyleSummaryValueResolved );
				rectInsideBorders.Y -= borderWidth;
				rectInsideBorders.Height += borderWidth;
			}

			// Fixed area and free form area elements don't have any borders, so no need
			// to merge them. Their children summary line elements will take care of that.
			//
			if ( hasFixedSummaryArea )
			{
				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				//
				//int height = summaryValues.GetFixedFooterAreaHeight( );
				int height = summaryValues.GetFixedFooterAreaHeight( this.SummaryDisplayAreaContext );

				FixedSummaryAreaUIElement fixedSummaryElem = 
					(FixedSummaryAreaUIElement)SummaryFooterUIElement.ExtractExistingElement( 
					oldElements, typeof( FixedSummaryAreaUIElement ), true );

				if ( null == fixedSummaryElem )
					fixedSummaryElem = new FixedSummaryAreaUIElement( this );

				Rectangle rect = rectInsideBorders;

				if ( !hasFreeFormSummaryArea )
					height = rectInsideBorders.Height;

				rect.Height = height;
				rectInsideBorders.Height = rectInsideBorders.Bottom - rect.Bottom;
				rectInsideBorders.Y = rect.Bottom;
				fixedSummaryElem.Rect = rect;

				this.ChildElements.Add( fixedSummaryElem );
			}

			if ( hasFreeFormSummaryArea )
			{			
				FreeFormSummaryAreaUIElement freeFormSummaryElem = 
					(FreeFormSummaryAreaUIElement)SummaryFooterUIElement.ExtractExistingElement( 
					oldElements, typeof( FreeFormSummaryAreaUIElement ), true );

				if ( null == freeFormSummaryElem )
					freeFormSummaryElem = new FreeFormSummaryAreaUIElement( this );
		
				if ( hasFixedSummaryArea && band.Layout.CanMergeAdjacentBorders( band.BorderStyleSummaryValueResolved ) )
				{
					int borderWidth = band.Layout.GetBorderThickness( band.BorderStyleSummaryValueResolved );
					rectInsideBorders.Y -= borderWidth;
					rectInsideBorders.Height += borderWidth;
				}

				freeFormSummaryElem.Rect = rectInsideBorders;

				this.ChildElements.Add( freeFormSummaryElem );				
			}			

			if ( null != oldElements )
				oldElements.Clear( );
			oldElements = null;
		}


		#endregion // PositionChildElements

		#region DrawBackColor

		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Overrode DrawBackColor because if this summary footer is inside a group-by
		// row then it shouldn't draw anything.
		//
		/// <summary>
		/// Overridden. Does nothing if the footer is being displayed in a group-by row.
		/// </summary>
		protected override void DrawBackColor( ref UIElementDrawParams drawParams )
		{
			if ( ! this.SummaryDisplayAreaContext.IsGroupByRowSummaries )
				base.DrawBackColor( ref drawParams );
		}

		#endregion // DrawBackColor

		// SSP 5/3/05 - NAS 5.2 Fixed Rows
		//
		#region NAS 5.2 Fixed Rows

		#region ClipSelf

		/// <summary>
		/// Returning true causes all drawing of this element to be expicitly clipped
		/// to its region
		/// </summary>
		protected override bool ClipSelf 
		{ 
			get 		  
			{ 
				return ! this.clipSelfRect.IsEmpty;
			} 
		}
 
		#endregion // ClipSelf

		#region Region

		/// <summary>
		/// Returns the region of this element. The deafult returns the element's
		/// Rect as a region. This method can be overriden to supply an irregularly
		/// shaped region 
		/// </summary>
		public override System.Drawing.Region Region
		{
			get
			{
				// The reason why we are creating a new instance of region is because the element's
				// Draw method disposes of this after accessing it.
				//
				return ! this.clipSelfRect.IsEmpty ? new Region( this.clipSelfRect ) : base.Region;
			}
		}
 
		#endregion // Region

		#region IntersectInvalidRect

		// SSP 8/6/04 UWG3380
		// Overrode IntersectInvalidRect to fix the problem where a check indicator in 
		// a non-fixed cell that was scrolled under a fixed cell was not clipped and
		// appeared through the fixed cell. This is because the themes-drawing does not
		// honor UIElement.Region property that we are overriding.
		//
		/// <summary>
		/// Returns the intersection of the element's rect with the invalid rect for the
		/// current draw operation.
		/// </summary>
		/// <param name="invalidRect">Invalid rect</param>
		/// <returns>The intersection of the element's rect with the invalid rect.</returns>
		protected override Rectangle IntersectInvalidRect( Rectangle invalidRect )
		{
			return this.clipSelfRect.IsEmpty
				? base.IntersectInvalidRect( invalidRect )
				: Rectangle.Intersect( invalidRect, this.clipSelfRect );
		}

		#endregion IntersectInvalidRect 

		#region InternalSetClipSelfRegion
		
		internal void InternalSetClipSelfRegion( Rectangle clipSelfRect )
		{
			this.clipSelfRect = clipSelfRect;
		}

		#endregion // InternalSetClipSelfRegion

		#endregion // NAS 5.2 Fixed Rows

		#region Offset

		// SSP 5/18/05 - Optimizations
		// Overrode Offset so we can offset the clipSelfRect as well.
		//
		/// <summary>
		/// Overridden. Offsets this element's rect and (optionally) all of its descendant elements.
		/// </summary>
		/// <param name="deltaX">The number of pixels to offset left/right</param>
		/// <param name="deltaY">The number of pixels to offset up/down </param>
		/// <param name="recursive">If true will offset all descendant elements as well</param>
		public override void Offset( int deltaX, int deltaY, bool recursive )
		{
			if ( ! this.clipSelfRect.IsEmpty )
				this.clipSelfRect.Offset( deltaX, deltaY );

			base.Offset( deltaX, deltaY, recursive );
		}

		#endregion // Offset

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Rows.Band, StyleUtils.Role.SummaryFooter );
			}
		}

		#endregion // UIRole

		#endregion // Protected overridden methods/properties
	}

	#endregion // SummaryFooterUIElement

	#region FixedSummaryAreaUIElement

	/// <summary>
	/// Fixed area of summary footer.
	/// </summary>
	public class FixedSummaryAreaUIElement : UIElement
	{
		#region Constructor

		internal FixedSummaryAreaUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region Private/Internal methods

		#region Rows

		private RowsCollection Rows
		{
			get
			{
				return (RowsCollection)this.GetContext( typeof( RowsCollection ), true );
			}
		}
		
		#endregion // Rows
		
		#region SummaryValues

		internal SummaryValuesCollection SummaryValues
		{
			get
			{	 
				return (SummaryValuesCollection)this.GetContext( typeof( SummaryValuesCollection ), true );
			}
		}

		#endregion // SummaryValues

		#endregion // Private/Internal methods

		#region Protected overridden methods/properties

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// Don't do anything since ui elements for fixed and free-form summary
			// areas don't draw anything.
			//
		}

		#endregion // InitAppearance

		#region SummaryDisplayAreaContext

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		internal SummaryDisplayAreaContext SummaryDisplayAreaContext
		{
			get
			{
				return (SummaryDisplayAreaContext)this.GetContext( typeof( SummaryDisplayAreaContext ) );
			}
		}

		#endregion // SummaryDisplayAreaContext

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// Does nothing as ui elements for different levels of fixed and free-form
				// summary areas don't need to draw any borders either. Summary footer 
				// ui element is the one that draw the border.
				//
				return UIElementBorderStyle.None;
			}
		}

		#endregion // BorderStyle

		#region DrawBackColor

		/// <summary>
		/// Default backcolor drawing just does a FillRectangle with the backcolor.
		/// </summary>
		protected override void DrawBackColor( ref UIElementDrawParams drawParams )
		{
			// Does nothing as ui elements for fixed and free form areas 
			// don't need to draw the back color.
			//
		}

		#endregion // DrawBackColor

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			UIElementsCollection oldElements = this.childElementsCollection;
			this.childElementsCollection = null;

			RowsCollection rows = this.Rows;

			Debug.Assert( null != rows, "Null rows !" );
			if ( null == rows )				
				return;

			UltraGridBand band = rows.Band;

			if ( !band.HasSummaries )
				return;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//SummarySettingsCollection summarySettings = band.Summaries;
			
			SummaryValuesCollection summaryValues = rows.SummaryValues;

			Debug.Assert( null != summaryValues, "RowsCollection.SummaryValues returned null !" );
			if ( null == summaryValues )
				return;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//bool hasCaptionArea = band.SummaryFooterCaptionVisibleResolved;

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//bool hasFreeFormArea = summarySettings.HasFreeFormSummaries;
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//bool hasFreeFormArea = summarySettings.HasFreeFormSummaries( this.SummaryDisplayAreaContext );

			Rectangle rectInsideBorders = this.RectInsideBorders;

			for ( int level = 0; ; level++ )
			{
				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				//
				//int levelElementHeight = summaryValues.GetFixedSummaryLineHeight( level );
				int levelElementHeight = summaryValues.GetFixedSummaryLineHeight( this.SummaryDisplayAreaContext, level );

				// If levelElementHeight is 0, then there are no more levels, break out of
				// the loop.
				//
				if ( levelElementHeight <= 0 )
					break;

				FixedSummaryLineUIElement levelElem = 
						(FixedSummaryLineUIElement)FixedSummaryAreaUIElement.ExtractExistingElement( oldElements, typeof( FixedSummaryLineUIElement ), true );

				if ( null == levelElem )
					levelElem = new FixedSummaryLineUIElement( this, level );
				else
					levelElem.Initialize( level );

				Rectangle rect = rectInsideBorders;
				rect.Height = levelElementHeight;

				bool isFirstLevel = 0 == level;

				// Merge the borders with the summary value elemenets in the previous level.
				//
				if ( !isFirstLevel && band.Layout.CanMergeAdjacentBorders( band.BorderStyleSummaryValueResolved ) )
				{
					int borderWidth = band.Layout.GetBorderThickness( band.BorderStyleSummaryValueResolved );

					rect.Y -= borderWidth;
					rect.Height += borderWidth;
				}

				// Current level would be the last level if the next level's height is 0.
				//
				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				//
				//bool isLastLevel = summaryValues.GetFixedSummaryLineHeight( 1 + level ) <= 0;
				bool isLastLevel = summaryValues.GetFixedSummaryLineHeight( this.SummaryDisplayAreaContext, 1 + level ) <= 0;

				// If this is the last level, then occupy the rest of the area.
				//
				if ( isLastLevel )
					rect.Height = rectInsideBorders.Bottom - rect.Top;

				rectInsideBorders.Height = rectInsideBorders.Bottom - rect.Bottom;
				rectInsideBorders.Y = rect.Bottom;			
				levelElem.Rect = rect;

				this.ChildElements.Add( levelElem );
			}
						
			if ( null != oldElements )
				oldElements.Clear( );
			oldElements = null;
		}

		#endregion // PositionChildElements

		#endregion // Protected overridden methods
	}

	#endregion // FixedSummaryAreaUIElement

	#region FreeFormSummaryAreaUIElement

	/// <summary>
	/// Free-form area of summary footer.
	/// </summary>
	public class FreeFormSummaryAreaUIElement : UIElement
	{
		#region Constructor

		internal FreeFormSummaryAreaUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region Private/Internal methods

		private RowsCollection Rows
		{
			get
			{
				return (RowsCollection)this.GetContext( typeof( RowsCollection ), true );
			}
		}

		#endregion // Private/Internal methods
	
		#region SummaryValues

		internal SummaryValuesCollection SummaryValues
		{
			get
			{	 
				return (SummaryValuesCollection)this.GetContext( typeof( SummaryValuesCollection ), true );
			}
		}

		#endregion // SummaryValues

		#region SummaryDisplayAreaContext

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		internal SummaryDisplayAreaContext SummaryDisplayAreaContext
		{
			get
			{
				return (SummaryDisplayAreaContext)this.GetContext( typeof( SummaryDisplayAreaContext ) );
			}
		}

		#endregion // SummaryDisplayAreaContext

		#region Protected overridden methods

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// Don't do anything since ui elements for fixed and free-form summary
			// areas don't draw anything.
			//
		}

		#endregion // InitAppearance

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// Does nothing as ui elements for different levels of fixed and free-form
				// summary areas don't need to draw any borders either. Summary footer 
				// ui element is the one that draw the border.
				//
				return UIElementBorderStyle.None;
			}
		}

		#endregion // BorderStyle

		#region DrawBackColor

		/// <summary>
		/// Default backcolor drawing just does a FillRectangle with the backcolor.
		/// </summary>
		protected override void DrawBackColor( ref UIElementDrawParams drawParams )
		{
			// Does nothing as ui elements for fixed and free form areas 
			// don't need to draw the back color.
			//
		}

		#endregion // DrawBackColor

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			UIElementsCollection oldElements = this.childElementsCollection;
			this.childElementsCollection = null;

			RowsCollection rows = this.Rows;

			Debug.Assert( null != rows, "Null rows !" );
			if ( null == rows )				
				return;

			UltraGridBand band = rows.Band;

			if ( !band.HasSummaries )
				return;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//SummarySettingsCollection summarySettings = band.Summaries;
			
			SummaryValuesCollection summaryValues = rows.SummaryValues;

			Debug.Assert( null != summaryValues, "RowsCollection.SummaryValues returned null !" );
			if ( null == summaryValues )
				return;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//bool hasCaptionArea = band.SummaryFooterCaptionVisibleResolved;

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//bool hasFixedArea = summarySettings.HasFixedSummaries;
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//bool hasFixedArea = summarySettings.HasFixedSummaries( this.SummaryDisplayAreaContext );

			Rectangle rectInsideBorders = this.RectInsideBorders;

			for ( int level = 0; ; level++ )
			{
				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				//
				//int levelElementHeight = summaryValues.GetFreeFormSummaryLineHeight( level );
				int levelElementHeight = summaryValues.GetFreeFormSummaryLineHeight( this.SummaryDisplayAreaContext, level );

				// If levelElementHeight is 0, then there are no more levels, break out of
				// the loop.
				//
				if ( levelElementHeight <= 0 )
					break;

				FreeFormSummaryLineUIElement levelElem = 
						(FreeFormSummaryLineUIElement)FreeFormSummaryAreaUIElement.ExtractExistingElement( oldElements, typeof( FreeFormSummaryLineUIElement ), true );

				if ( null == levelElem )
					levelElem = new FreeFormSummaryLineUIElement( this, level );
				else
					levelElem.Initialize( level );

				Rectangle rect = rectInsideBorders;
				rect.Height = levelElementHeight;

				bool isFirstLevel = 0 == level;

				// Merge the borders with the summary value elemenets in the previous level.
				//
				if ( !isFirstLevel && band.Layout.CanMergeAdjacentBorders( band.BorderStyleSummaryValueResolved ) )
				{
					int borderWidth = band.Layout.GetBorderThickness( band.BorderStyleSummaryValueResolved );

					rect.Y -= borderWidth;
					rect.Height += borderWidth;
				}

				// Current level would be the last level if the next level's height is 0.
				//
				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				//
				//bool isLastLevel = summaryValues.GetFreeFormSummaryLineHeight( 1 + level ) <= 0;
				bool isLastLevel = summaryValues.GetFreeFormSummaryLineHeight( this.SummaryDisplayAreaContext, 1 + level ) <= 0;

				// If this is the last level, then occupy the rest of the area.
				//
				if ( isLastLevel )
					rect.Height = rectInsideBorders.Bottom - rect.Top;

				rectInsideBorders.Height = rectInsideBorders.Bottom - rect.Bottom;
				rectInsideBorders.Y = rect.Bottom;			
				levelElem.Rect = rect;

				this.ChildElements.Add( levelElem );
			}
						
			if ( null != oldElements )
				oldElements.Clear( );
			oldElements = null;
		}

		#endregion // PositionChildElements

		#endregion // Protected overridden methods
	}

	#endregion // FreeFormSummaryAreaUIElement
	
	#region FreeFormSummaryLineUIElement

	/// <summary>
	/// A level in free form summary area.
	/// </summary>
	public class FreeFormSummaryLineUIElement : UIElement
	{
		#region Private variables

		int level = -1;

		#endregion // Private variables

		#region Constructor

		internal FreeFormSummaryLineUIElement( UIElement parent, int level ) : base( parent )
		{
			this.Initialize( level );
		}

		#endregion // Constructor

		#region Private/Internal methods

		#region Initialize

		internal void Initialize( int level )
		{
			if ( level < 0 )
				throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_148"), level, Shared.SR.GetString("LE_ArgumentOutOfRangeException_344") );

			this.level = level;
		}

		#endregion // Initialize

		#region Rows

		private RowsCollection Rows
		{
			get
			{
				return (RowsCollection)this.GetContext( typeof( RowsCollection ), true );
			}
		}

		#endregion // Rows

		#endregion // Private/Internal methods

		#region Protected overridden methods
		
		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// Does nothing as ui elements for different levels of fixed and free-form
			// summary areas don't draw anything.
			//
		}

		#endregion // InitAppearance

		#region SummaryDisplayAreaContext

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		internal SummaryDisplayAreaContext SummaryDisplayAreaContext
		{
			get
			{
				return (SummaryDisplayAreaContext)this.GetContext( typeof( SummaryDisplayAreaContext ) );
			}
		}

		#endregion // SummaryDisplayAreaContext

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return UIElementBorderStyle.None;
			}
		}

		#endregion // BorderStyle

		#region DrawBackColor

		/// <summary>
		/// Default backcolor drawing just does a FillRectangle with the backcolor.
		/// </summary>
		protected override void DrawBackColor( ref UIElementDrawParams drawParams )
		{
			// Does nothing as ui elements for different levels of fixed and free-form
			// summary areas themselves don't draw any background
			//
		}

		#endregion // DrawBackColor

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			UIElementsCollection oldElements = this.childElementsCollection;
			this.childElementsCollection = null;

			RowsCollection rows = this.Rows;

			Debug.Assert( null != rows, "Null rows !" );
			if ( null == rows )				
				return;

			UltraGridBand band = rows.Band;

			if ( !band.HasSummaries )
				return;
			
			SummaryValuesCollection summaryValues = rows.SummaryValues;

			Debug.Assert( null != summaryValues, "RowsCollection.SummaryValues returned null !" );
			if ( null == summaryValues )
				return;

			SummarySettingsCollection summarySettings = band.Summaries;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList summariesList = new ArrayList( );
			List<SummarySettings> summariesList = new List<SummarySettings>();

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//summarySettings.GetFreeFormSummariesForLevel( this.level, summariesList );
			summarySettings.GetFreeFormSummariesForLevel( this.SummaryDisplayAreaContext, this.level, summariesList );
			
			Rectangle rectInsideBorders = this.RectInsideBorders;

			bool canMergeBorders = band.Layout.CanMergeAdjacentBorders( band.BorderStyleSummaryValueResolved );
			int borderWidth = band.Layout.GetBorderThickness(  band.BorderStyleSummaryValueResolved );

			for ( int i = 0; i < summariesList.Count; i++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//SummarySettings summary = summariesList[i] as SummarySettings;
				SummarySettings summary = summariesList[ i ];

				Debug.Assert( null != summary, "GetFreeFormSummariesForLevel returned a list with an element that is not SummarySettings !" );

				// Skip the hidden summaries.
				//
				if ( null == summary )
					continue;

				SummaryValue summaryValue = summaryValues.GetSummaryValueFor( summary );

				Debug.Assert( null != summaryValue, "No summary value for summary !" );

				if ( null == summaryValue )
					continue;

				Rectangle summaryRect = rectInsideBorders;

				summaryRect.Width = rectInsideBorders.Width / 3;
				int extraPixels = rectInsideBorders.Width % 3;

				switch ( summary.SummaryPosition )
				{
					case SummaryPosition.Left:
						break;
					case SummaryPosition.Center:
						summaryRect.X += summaryRect.Width;
						
						// Merge with the left summary's border.
						//
						if ( canMergeBorders )
						{
							summaryRect.X -= borderWidth;
							summaryRect.Width += borderWidth;
						}
						break;
					case SummaryPosition.Right:
						summaryRect.X += 2 * summaryRect.Width;
						summaryRect.Width += extraPixels;

						// Merge with the center summary's borders.
						//
						if ( canMergeBorders )
						{
							summaryRect.X -= borderWidth;
							summaryRect.Width += borderWidth;
						}
						break;
					default:
						Debug.Assert( false, "SummaryPosition is something other than Left, Center or Right." );
						continue;
				}
			
				SummaryValueUIElement summaryElem = (SummaryValueUIElement)FreeFormSummaryLineUIElement.ExtractExistingElement( oldElements, typeof( SummaryValueUIElement ), true );

				if ( null == summaryElem )
					summaryElem = new SummaryValueUIElement( this, summaryValue );
				else
					summaryElem.Initialize( summaryValue );

				summaryElem.Rect = summaryRect;

				this.ChildElements.Add( summaryElem );
			}

			if ( null != oldElements )
				oldElements.Clear( );
			oldElements = null;
		}

		#endregion // PositionChildElements

		#endregion // Protected overridden methods
	}

	#endregion // FreeFormSummaryLineUIElement

	#region FixedSummaryLineUIElement

	/// <summary>
	/// A level in free form summary area.
	/// </summary>
	public class FixedSummaryLineUIElement : UIElement
	{
		#region Private variables

		int level = -1;

		#endregion // Private variables

		#region Constructor

		internal FixedSummaryLineUIElement( UIElement parent, int level ) : base( parent )
		{
			this.Initialize( level );
		}

		#endregion // Constructor

		#region Private/Internal methods/properties

		#region Initialize

		internal void Initialize( int level )
		{
			if ( level < 0 )
				throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_148"), level, Shared.SR.GetString("LE_ArgumentOutOfRangeException_344") );

			this.level = level;
		}

		#endregion // Initialize

		#region Rows

		private RowsCollection Rows
		{
			get
			{
				return (RowsCollection)this.GetContext( typeof( RowsCollection ), true );
			}
		}

		#endregion // Rows

		#region UsingFixedHeaders
		
		// SSP 6/13/03 - Fixed headers
		//
		private bool UsingFixedHeaders
		{
			get
			{
				SummaryFooterUIElement summaryFooterElem = (SummaryFooterUIElement)this.Parent.GetAncestor( typeof( SummaryFooterUIElement ) );

				return null != summaryFooterElem && summaryFooterElem.usingFixedHeaders;
			}
		}

		#endregion // UsingFixedHeaders

		#region GetSummaryLocation

		internal void GetSummaryLocation( UltraGridColumn column, out int x, out int width )
		{
			ColScrollRegion csr = (ColScrollRegion)this.GetContext( typeof( ColScrollRegion ), true );

			x = -1; width = -1;
			
			// SSP 1/14/03 UWG1936
			// If the band has groups, then GetDimensions returns the dimensions relative
			// to the group. I didn't change the GetDimensions itselft because it may potentially
			// break things. Instead implementing the workaround here.
			// Added the if block and enclosed the already existing code in the else block.
			//
			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
			//if ( null != column.Group && null != column.Group.Header )
			//{
			//    HeaderBase header = column.Group.Header;
			UltraGridGroup groupResolved = column.GroupResolved;
			if ( null != groupResolved && null != groupResolved.Header )
			{
				HeaderBase header = groupResolved.Header;

				int tmpWidth = 0;
				int groupOffset = 0;

				header.GetDimensions( 
					// SSP 12/21/04 - BR00985
					// There was a bug in GetDimensions that caused the returned origin to be off
					// by row pre row area extent. That's fixed now however we need to adjust the
					// call here and pass in InsidePreRowSelectors instead of Outside.
					//
					//PositionDimensions.Outside,
					PositionDimensions.InsidePreRowSelectors,
					DimOriginBase.OnScreen, 
					ref groupOffset, //return value
					ref tmpWidth, //return value
					csr );

				Rectangle rect = new Rectangle( 0, 0, 0, 0 );

				rect.X = header.GetOnScreenOrigin( csr, true );

				Rectangle columnHeaderRect = new Rectangle( 0, 0, 0, 0 );
				Rectangle fixedHeaderRect = new Rectangle( 0, 0, 0, 0 );
				column.CalculateHeaderRect( ref columnHeaderRect, ref fixedHeaderRect );

				// SSP 10/20/03 UWG2707 UWG2617
				// Although above bugs are not caused by the following line of code, I noticed it 
				// when I was fixing above two bugs. Commented out the code. This code doesn't
				// make sense at all. Why are we offsetting by -PreRowAreaExtent. Either we should
				// be offsetting every header or no headers.
				// Commented out below line of code.
				//
				//if ( header.FirstItem )
				//	columnHeaderRect.Offset( -header.Band.PreRowAreaExtent, 0 );

				// Took the code from BandHeadersUIElement's header positioning logic.
				//
				x = groupOffset + columnHeaderRect.X + column.RelativeOrigin;
				width = columnHeaderRect.Width;
			}
			else
			{
				column.Header.GetDimensions(
					// SSP 12/21/04 - BR00985
					// There was a bug in GetDimensions that caused the returned origin to be off
					// by row pre row area extent. That's fixed now however we need to adjust the
					// call here and pass in InsidePreRowSelectors instead of Outside.
					//
					//PositionDimensions.Outside,
					PositionDimensions.InsidePreRowSelectors,
					DimOriginBase.OnScreen, 
					ref x, //return value
					ref width, //return value
					csr );
			}

			// SSP 7/1/05 BR04748
			// In AutoFitStyle mode of ExtendLastColumn the band extents are adjusted to
			// occupy the extra visible space however header extents (column widths) aren't.
			// We need to do them here. If the column or group is the last in the row cell 
			// area then extend it so it occupies the rest of the space in the row cell area.
			//
			// ------------------------------------------------------------------------------
			if ( column.Layout.AutoFitExtendLastColumn &&
				 ( null != column.Group && column.Band.GroupsDisplayed 
				   && column.Group.LastItem && column.LastItemInLevel || column.LastItem ) )
			{
				Rectangle thisRect = this.RectInsideBorders;
				if ( x + width < thisRect.Right )
					width = thisRect.Right - x;
			}
			// ------------------------------------------------------------------------------
	
			Debug.Assert( width >= 0, "GetDimentions did not calculate properly !" );
		}

		#endregion // GetSummaryLocation

		#region InternalGetSummaryValueUIElement

		internal SummaryValueUIElement InternalGetSummaryValueUIElement( UIElementsCollection oldElements, SummaryValue summaryValue )
		{
			SummaryValueUIElement summaryElem = (SummaryValueUIElement)FixedSummaryLineUIElement.ExtractExistingElement( oldElements, typeof( SummaryValueUIElement ), true );

			if ( null == summaryElem )
				summaryElem = new SummaryValueUIElement( this, summaryValue );
			else
				summaryElem.Initialize( summaryValue );

			return summaryElem;
		}

		#endregion // InternalGetSummaryValueUIElement

		#region SummaryDisplayAreaContext

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		internal SummaryDisplayAreaContext SummaryDisplayAreaContext
		{
			get
			{
				return (SummaryDisplayAreaContext)this.GetContext( typeof( SummaryDisplayAreaContext ) );
			}
		}

		#endregion // SummaryDisplayAreaContext

		#endregion // Private/Internal methods

		#region Protected overridden methods

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// Does nothing as ui elements for different levels of fixed and free-form
			// summary areas don't draw anything.
			//
		}

		#endregion // InitAppearance

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// Does nothing as ui elements for different levels of fixed and free-form
				// summary areas don't need to draw any borders either. Summary footer 
				// ui element is the one that draw the border.
				//
				return UIElementBorderStyle.None;
			}
		}

		#endregion // BorderStyle

		#region DrawBackColor

		/// <summary>
		/// Overridden. Does nothing since the parent <see cref="SummaryFooterUIElement"/> 
		/// does the background drawing.
		/// </summary>
		protected override void DrawBackColor( ref UIElementDrawParams drawParams )
		{
			// Does nothing as ui elements for different levels of fixed and free-form
			// summary areas themselves don't draw any background
			//
		}

		#endregion // DrawBackColor

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			UIElementsCollection oldElements = this.childElementsCollection;
			this.childElementsCollection = null;

			RowsCollection rows = this.Rows;

			Debug.Assert( null != rows, "Null rows !" );
			if ( null == rows )				
				return;

			UltraGridBand band = rows.Band;

			if ( !band.HasSummaries )
				return;
			
			SummaryValuesCollection summaryValues = rows.SummaryValues;

			Debug.Assert( null != summaryValues, "RowsCollection.SummaryValues returned null !" );
			if ( null == summaryValues )
				return;

			SummarySettingsCollection summarySettings = band.Summaries;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList summariesList = new ArrayList( );
			List<SummarySettings> summariesList = new List<SummarySettings>();

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//summarySettings.GetFixedSummariesForLevel( this.level, summariesList );
			Infragistics.Win.UltraWinGrid.SummaryDisplayAreaContext context = this.SummaryDisplayAreaContext;
			summarySettings.GetFixedSummariesForLevel( context, this.level, summariesList );
			
			Rectangle rectInsideBorders = this.RectInsideBorders;

			bool canMergeBorders = band.Layout.CanMergeAdjacentBorders( band.BorderStyleSummaryValueResolved );
			int borderWidth = band.Layout.GetBorderThickness(  band.BorderStyleSummaryValueResolved );

			// SSP 2/27/03 - Row Layout Functionality
			// Added code for laying out summaries according to the row layout.
			// Added if block below and enclosed the already existing code in the else block.
			//
			// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			// ------------------------------------------------------------------------------
			

			// SSP 4/26/05 - NAS 5.2 Extension of Summaries Functionality
			// If summaries are being displayed in a group-by row then make sure that the
			// summaries do not overwrite the group-by row description.
			//
			int groupByRowSummaryFooter_ClipLeft = int.MinValue;
			if ( context.IsGroupByRowSummaries )
			{
				GroupByRowUIElement groupByRowElem = (GroupByRowUIElement)this.GetAncestor( typeof( GroupByRowUIElement ) );
				if ( null != groupByRowElem )
					groupByRowSummaryFooter_ClipLeft = groupByRowElem.groupByRowSummaryFooter_ClipLeft;
			}

			Infragistics.Win.Layout.LayoutManagerBase layoutManager = band.UseRowLayoutResolved 
				? band.GetSummaryLayoutManager( context ) : null;
			if ( null != layoutManager )
			{
				layoutManager.LayoutContainer( new LayoutContainerSummaryArea( this, oldElements, groupByRowSummaryFooter_ClipLeft ), null );
			}
			// ------------------------------------------------------------------------------
			else
			{
				// SSP 6/13/03 - Fixed headers
				//
				// --------------------------------------------------------------------------------------
				ColScrollRegion csr = (ColScrollRegion)this.GetContext( typeof( ColScrollRegion ) );
				int fixedHeadersAreaRight = this.Rect.X;				
				// SSP 8/8/03
				// Check if there are any fixed headers.
				//
				//if ( null != csr && this.UsingFixedHeaders )
				if ( null != csr && this.UsingFixedHeaders && band.HasFixedHeaders( csr ) )
				{
					// MD 2/17/09 - TFS14105
					// Only use the left fixed header extent here, not the entire fixed header extent.
					//fixedHeadersAreaRight += band.RowSelectorExtent + band.GetFixedHeadersExtent( csr );
					fixedHeadersAreaRight += band.RowSelectorExtent + band.GetFixedHeadersExtent( csr, false );

					// SSP 4/26/05 - NAS 5.2 Extension of Summaries Functionality
					// Summary footers of group-by rows are extended left beyond the row selectors.
					// Adjust for that.
					//
					if ( UltraGridSummaryRow.ShouldLeftAlignGroupByRowsFooter( context, rows ) )
						fixedHeadersAreaRight += band.CalcGroupBySummariesIndent( rows );
				}

				// SSP 4/26/05 - NAS 5.2 Extension of Summaries Functionality
				// If summaries are being displayed in a group-by row then make sure that the
				// summaries do not overwrite the group-by row description.
				//
				bool itemPartiallyClipped = false;
				fixedHeadersAreaRight = Math.Max( fixedHeadersAreaRight, groupByRowSummaryFooter_ClipLeft );
				// --------------------------------------------------------------------------------------

				for ( int i = 0; i < summariesList.Count; i++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//SummarySettings summary = summariesList[i] as SummarySettings;
					SummarySettings summary = summariesList[ i ];

					Debug.Assert( null != summary, "GetFixedSummariesForLevel returned a list with an element that is not SummarySettings !" );

					// Skip the hidden summaries.
					//
					if ( null == summary )
						continue;

					SummaryValue summaryValue = summaryValues.GetSummaryValueFor( summary );

					Debug.Assert( null != summaryValue, "No summary value for summary !" );

					if ( null == summaryValue )
						continue;
			
					UltraGridColumn showColumn = summary.SummaryPositionColumnResolved;

					Debug.Assert( null != showColumn, "summary.SummaryPositionColumnResolved returned null for a fixed summary !" );

					if ( null == showColumn )
						continue;
				
					int x;
					int width;
					this.GetSummaryLocation( showColumn, out x, out width );

					Debug.Assert( width >= 0 );
					if ( width < 0 )
						continue;

					int delta = 0;

					// Account for the border of the summary footer. It header rect may be
					// off by the border width ( 1 or 2 pixles ).
					//
					if ( x < rectInsideBorders.X )
					{
						delta = rectInsideBorders.X - x;

						// SSP 5/23/03 - Fixed headers
						// In fixed headers mode, we will have summary values scrolled out of view.
						//
						//Debug.Assert( delta <= 2, "It should not have been off by that much, just the border width." );
						//Debug.Assert( delta <= 2 || band.UseFixedHeadersResolved( csr ), "It should not have been off by that much, just the border width." );

						if ( band.UseFixedHeadersResolved( csr ) && delta > 2 )
							delta = 0;
					}

					x += delta;
					width -= delta;

					Rectangle summaryRect = new Rectangle( x, rectInsideBorders.Y, width, rectInsideBorders.Height );

					// SSP 6/13/03 - Fixed headers.
					// Skip the summaries that are scrolled below the fixed column summaries.
					//
					// ----------------------------------------------------------------------------------------
					bool isColumnFixed = band.IsHeaderFixed( csr, showColumn.Header );
					if ( ! isColumnFixed && summaryRect.Right < fixedHeadersAreaRight )
						continue;

					Rectangle clipSelfRect = Rectangle.Empty;
					if ( ! isColumnFixed && summaryRect.X < fixedHeadersAreaRight )
					{
						clipSelfRect = summaryRect;
						clipSelfRect.X = fixedHeadersAreaRight;
						clipSelfRect.Width = summaryRect.Right - clipSelfRect.X;

						// SSP 4/26/05 - NAS 5.2 Extension of Summaries Functionality
						// If summaries are being displayed in a group-by row then only add the fixed header
						// separator element if one of the summaries is partially visible as a result of
						// the clipping.
						//
						itemPartiallyClipped = true;
					}
					// ----------------------------------------------------------------------------------------

					// SSP 2/27/03 - Row Layout Functioanlity
					// Use the new helper method for extracting and creating a new summary value ui element.
					//
					// ----------------------------------------------------------------------------------------
					
					SummaryValueUIElement summaryElem = (SummaryValueUIElement)this.InternalGetSummaryValueUIElement( oldElements, summaryValue );
					// ----------------------------------------------------------------------------------------

					if ( canMergeBorders )
					{
						if ( summaryRect.X > this.Rect.X )
						{
							summaryRect.X -= borderWidth;
							summaryRect.Width += borderWidth;
						}
					}
					else
					{
						// Make sure the summary does not extend beyond the summary footer element.
						//
						if ( summaryRect.Right > rectInsideBorders.Right )
							summaryRect.Width = rectInsideBorders.Right - summaryRect.Left;

						if ( summaryRect.Left < rectInsideBorders.Left )
							summaryRect.X = rectInsideBorders.Left;
					}

					summaryElem.Rect = summaryRect;

					// SSP 6/13/03 - Fixed headers.
					// Set the clip self rect of the summary element so that the fixed column summaries
					// partially scrolled under the fixed header summaries do no paint over those fixed
					// header summaries.
					//
					summaryElem.InternalSetClipSelfRegion( clipSelfRect );

					this.ChildElements.Add( summaryElem );
				}

				if ( fixedHeadersAreaRight > this.Rect.X && fixedHeadersAreaRight < this.Rect.Right
					// SSP 4/26/05 - NAS 5.2 Extension of Summaries Functionality
					// If summaries are being displayed in a group-by row then only add the fixed header
					// separator element if one of the summaries is partially visible as a result of
					// the clipping. Also commented out the check for UsingFixedHeaders as it's not
					// necessary because if fixedHeadersAreaRight is set to something other than 
					// this.Rect.X then either UsingFixedHeaders must have been true or we would have
					// wanted the separator.
					//
					// && this.UsingFixedHeaders 
					&& ( itemPartiallyClipped || ! context.IsGroupByRowSummaries ) )
				{
					// SSP 5/6/05 - NAS 5.2 Extension of Summaries Functionality
					// Use the RowCellAreaUIElementBase.AddFixedCellSeparatorUIElement instead.
					//
					// ------------------------------------------------------------------------
					RowCellAreaUIElementBase.AddFixedCellSeparatorUIElement( this, band, oldElements, fixedHeadersAreaRight );
					
					// ------------------------------------------------------------------------
				}
			}

			if ( null != oldElements )
				oldElements.Clear( );
			oldElements = null;
		}

		#endregion // PositionChildElements

		#endregion // Protected overridden methods
	}

	#endregion // FixedSummaryLineUIElement

	#region SummaryValueUIElement

	/// <summary>
	/// A level in free form summary area.
	/// </summary>
	public class SummaryValueUIElement : UIElement
	{
		#region Private variables

		private SummaryValue summaryValue = null;

		// SSP 6/13/03 - Fixed headers
		//
		private Rectangle clipSelfRect = Rectangle.Empty;

		// SSP 7/28/05 - Header, Row, Summary Tooltips
		// Commented out the following code. Now this is handled via the UIElement.ToolTipItem
		// mechanism.
		// 
		

		#endregion

		#region Constructor

		internal SummaryValueUIElement( UIElement parent, SummaryValue summaryValue ) : base( parent )
		{
			this.Initialize( summaryValue );
		}

		#endregion // Constructor

		#region Private/Internal properties/methods
		
		#region Initialize

		internal void Initialize( SummaryValue summaryValue )
		{
			if ( null == summaryValue )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_345"), Shared.SR.GetString("LE_ArgumentNullException_346") );

			this.summaryValue = summaryValue;

			// SSP 4/23/04 UWG3167
			// Set the context to the summary value.
			//
			this.PrimaryContext = summaryValue;

			// SSP 5/6/05 - NAS 5.2 Extension of Summaries Functionality
			// 
			this.clipSelfRect = Rectangle.Empty;
		}

		#endregion

		#region SummaryValue

		// SSP 4/30/05
		// Made this public.
		//
		/// <summary>
		/// Returns the associated summary value object.
		/// </summary>
		public SummaryValue SummaryValue
		{
			get
			{
				return this.summaryValue;
			}
		}

		#endregion // SummaryValue

		#region Rows

		private RowsCollection Rows
		{
			get
			{
				return (RowsCollection)this.GetContext( typeof( RowsCollection ), true );
			}
		}

		#endregion // Rows

		#region SummaryDisplayAreaContext

		// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		internal SummaryDisplayAreaContext SummaryDisplayAreaContext
		{
			get
			{
				return (SummaryDisplayAreaContext)this.GetContext( typeof( SummaryDisplayAreaContext ) );
			}
		}

		#endregion // SummaryDisplayAreaContext

		#region InternalSetClipSelfRegion
		
		// SSP 6/13/03 - Fixed headers.
		//
		internal void InternalSetClipSelfRegion( Rectangle clipSelfRect )
		{
			this.clipSelfRect = clipSelfRect;
		}

		#endregion // InternalSetClipSelfRegion

		#endregion // Private/Internal methods

		#region Protected overridden methods/properties

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			Debug.Assert( null != this.SummaryValue, "Null summmary value !" );
			
			if ( null != this.SummaryValue )
				// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
				// Added an overload of ResolveAppearance that takes in context parameter.
				//
				//this.SummaryValue.ResolveAppearance( ref appearance, ref requestedProps );
				this.SummaryValue.ResolveAppearance( ref appearance, ref requestedProps, this.SummaryDisplayAreaContext );
		}

		#endregion // InitAppearance

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.SummaryValue.BorderStyleResolved;
			}
		}

		#endregion // BorderStyle

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			Infragistics.Win.UltraWinGrid.SummaryValue summaryValue = this.SummaryValue;
			bool isMultiline = summaryValue.SummarySettings.Lines > 1;
			bool wrapText	 = false;

			TextUIElement textElem = (TextUIElement)SummaryValueUIElement.ExtractExistingElement( this.ChildElements, typeof( TextUIElement ), true );

			this.ChildElements.Clear( );

			if ( null == textElem )
				textElem = new TextUIElement( this, summaryValue.SummaryText );
			else 
				textElem.Text = summaryValue.SummaryText;

			textElem.MultiLine = isMultiline;
			textElem.WrapText = wrapText;

			textElem.Rect = this.RectInsideBorders;

			this.ChildElements.Add( textElem );


            // MRS 11/18/2008 - TFS10343
            #region Added image support to summaries

            ImageUIElement imageElement = null;
            Rectangle textRect = textElem.Rect;
            AppearanceData appData = new AppearanceData();
            AppearancePropFlags flags = AppearancePropFlags.Image
                                        | AppearancePropFlags.ImageHAlign
                                        | AppearancePropFlags.ImageVAlign
                                        | AppearancePropFlags.TextHAlign
                                        | AppearancePropFlags.TextVAlign;

            if (summaryValue != null)
                summaryValue.ResolveAppearance(ref appData, ref flags);
            else
                this.InitAppearance(ref appData, ref flags);

            if (appData.Image != null)
            {
                Image image = appData.GetImage(((UltraGridBase)this.Control).ImageList);

                if (image != null)
                {
                    if (null == imageElement)
                    {
                        imageElement = new ImageUIElement(this, image);
                    }
                    else
                    {
                        imageElement.Image = image;
                    }

                    imageElement.Scaled = true;

                    Size size = image.Size;

                    // shrink the image size if necessary
                    //
                    if (textRect.Height > 2 && size.Height > textRect.Height - 2)
                    {
                        // proportinally shrink the width
                        //
                        size.Width = (int)((double)size.Width * ((double)(textRect.Height - 2) / (double)size.Height));

                        // shrink the height
                        //
                        size.Height = textRect.Height - 2;
                    }

                    // make sure the width isn't too big
                    //
                    if (textRect.Width > 2 && size.Width > textRect.Width - 2)
                        size.Width = textRect.Width - 2;

                    Rectangle imgRect = textRect;
                    imgRect.Width = size.Width;
                    imgRect.Height = size.Height;

                    //move image based on alignment
                    DrawUtility.AdjustHAlign(appData.ImageHAlign, ref imgRect, textRect);
                    DrawUtility.AdjustVAlign(appData.ImageVAlign, ref imgRect, textRect);

                    // make sure the imgae has at least a 1 pixel 
                    // padding on top and bottom
                    //
                    if (imgRect.Top == textRect.Top)
                        imgRect.Y++;
                    else
                        if (imgRect.Bottom == textRect.Bottom)
                            imgRect.Y--;


                    // adjust the text rect appropriately
                    //
                    switch (appData.ImageHAlign)
                    {
                        case HAlign.Center:
                            break;

                        case HAlign.Right:
                            imgRect.X--;
                            textRect.Width -= imgRect.Width + 1;
                            break;

                        default:
                        case HAlign.Left:
                            imgRect.X++;
                            textRect.Width -= imgRect.Width + 1;
                            textRect.X += imgRect.Width + 1;
                            break;
                    }

                    imageElement.Rect = imgRect;

                    this.ChildElements.Add(imageElement);

                }
            }

            textElem.Rect = textRect;

            #endregion // Added image support to summaries

			// SSP 7/28/05 - Header, Row, Summary Tooltips
			// 
			GridUtils.SetupElementToolTip( this, summaryValue.ToolTipTextResolved, true );
		}

		#endregion // PositionChildElements

		#region ClipSelf

		// SSP 6/13/03 - Fixed headers
		// Overrode ClipSelf and Region properties.
		//
		/// <summary>
		/// Returning true causes all drawing of this element to be expicitly clipped
		/// to its region
		/// </summary>
		protected override bool ClipSelf 
		{ 
			get 		  
			{ 
				return ! this.clipSelfRect.IsEmpty;
			} 
		}
 
		#endregion // ClipSelf

		#region Region

		// SSP 6/13/03 - Fixed headers
		// Overrode ClipSelf and Region properties.
		//
		/// <summary>
		/// Returns the region of this element. The deafult returns the element's
		/// Rect as a region. This method can be overriden to supply an irregularly
		/// shaped region 
		/// </summary>
		public override System.Drawing.Region Region
		{
			get
			{
				// The reason why we are creating a new instance of region is because the element's
				// Draw method disposes of this after accessing it.
				//
				return ! this.clipSelfRect.IsEmpty ? new Region( this.clipSelfRect ) : base.Region;
			}
		}
 
		#endregion // Region

		#region Contains

		// SSP 6/17/03 - Fixed headers
		// Overrode Contains method to take care of cells that are partially scrolled underneath
		// the fixed column cells.
		//
        /// <summary>
        /// Checks if the point is over the element.
        /// </summary>
        /// <param name="point">In client coordinates.</param>
        /// <param name="ignoreClipping">Specifieds if we should ignore clipping or not</param>
        /// <returns>Returns true if the point is over the element.</returns>
        public override bool Contains(System.Drawing.Point point,
			bool ignoreClipping )
		{
			if( ! this.clipSelfRect.IsEmpty && ! this.clipSelfRect.Contains( point ) )
				return false;

			return base.Contains( point, ignoreClipping );
		}

		#endregion // Contains

		#region GetContext

		// SSP 8/19/04
		// Overrode GetContext so one can search for a summary value element using a
		// summary settings object rather than a summary value.
		//
		/// <summary>
		/// Returns null or an object of requested type that relates to the element.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="checkParentElementContexts"></param>
		/// <returns></returns>
		public override object GetContext( Type type, bool checkParentElementContexts )
		{
			if ( typeof( SummarySettings ) == type && null != this.SummaryValue )
				return this.SummaryValue.SummarySettings;

			// Call the base class implementation.
			//			
			return base.GetContext( type, checkParentElementContexts );
		}

		#endregion // GetContext

		#region Commented Out Code

		// SSP 7/28/05 - Header, Row, Summary Tooltips
		// Commented out the following code. Now this is handled via the UIElement.ToolTipItem
		// mechanism.
		// 
		

		#endregion // Commented Out Code

		#region Offset

		// SSP 5/18/05 - Optimizations
		// Overrode Offset so we can offset the clipSelfRect as well.
		//
		/// <summary>
		/// Overridden. Offsets this element's rect and (optionally) all of its descendant elements.
		/// </summary>
		/// <param name="deltaX">The number of pixels to offset left/right</param>
		/// <param name="deltaY">The number of pixels to offset up/down </param>
		/// <param name="recursive">If true will offset all descendant elements as well</param>
		public override void Offset( int deltaX, int deltaY, bool recursive )
		{
			if ( ! this.clipSelfRect.IsEmpty )
				this.clipSelfRect.Offset( deltaX, deltaY );

			base.Offset( deltaX, deltaY, recursive );
		}

		#endregion // Offset

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Rows.Band, StyleUtils.Role.SummaryValue );
			}
		}

		#endregion // UIRole

		#endregion // Protected overridden methods
	}

	#endregion // FixedSummaryLineUIElement

	#region SummaryFooterCaptionUIElement

	/// <summary>
	/// UIElement for the caption area of the summary footers.
	/// </summary>
	public class SummaryFooterCaptionUIElement : UIElement
	{
		#region Constructor

		internal SummaryFooterCaptionUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region Private/Internal properties/methods

		#region Rows

		private RowsCollection Rows
		{
			get
			{
				return (RowsCollection)this.GetContext( typeof( RowsCollection ), true );
			}
		}

		#endregion // Rows

		#region SummaryValues

		internal SummaryValuesCollection SummaryValues
		{
			get
			{	 
				 return (SummaryValuesCollection)this.GetContext( typeof( SummaryValuesCollection ), true );
			}
		}

		#endregion // SummaryValues

		#endregion // Private/Internal methods

		#region Protected overridden methods

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			SummaryValuesCollection summaryValues = this.SummaryValues;
			Debug.Assert( null != summaryValues, "No summary values !" );

			if ( null != summaryValues )
				summaryValues.ResolveSummaryFooterCaptionAppearance( ref appearance, ref requestedProps );
		}

		#endregion // InitAppearance

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.Rows.Band.BorderStyleSummaryFooterCaptionResolved;
			}
		}

		#endregion // BorderStyle

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			TextUIElement textElem = (TextUIElement)SummaryValueUIElement.ExtractExistingElement( this.ChildElements, typeof( TextUIElement ), true );
			
			this.ChildElements.Clear( );
			
			string summaryFooterHeaderText =
				null != this.SummaryValues
				? this.SummaryValues.GetSummaryFooterCaption( )
				: "";
	
			if ( null == textElem )
				textElem = new TextUIElement( this, summaryFooterHeaderText );
			else 
				textElem.Text = summaryFooterHeaderText;

			textElem.Rect = this.RectInsideBorders;

			this.ChildElements.Add( textElem );
		}

		#endregion // PositionChildElements

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Rows.Band, StyleUtils.Role.SummaryFooterCaption );
			}
		}

		#endregion // UIRole

		#endregion // Protected overridden methods
	}

	#endregion // SummaryFooterHeaderUIElement
}
