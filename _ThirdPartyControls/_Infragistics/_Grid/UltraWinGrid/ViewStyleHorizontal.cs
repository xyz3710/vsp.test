#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Windows.Forms;

	/// <summary>
	///	View style for a multi-band display where child rows are 
	///	spread out horizontally
	/// </summary>
	internal class ViewStyleHorizontal : ViewStyleMultiBase
	{
		internal ViewStyleHorizontal( Infragistics.Win.UltraWinGrid.UltraGridLayout layout ) : base( layout )
		{
		}
		internal override bool HasMultiRowTiers 
		{ 
			get
			{
				return true; 
			}
		}

		#region FitColumnsToWidth

		internal override void FitColumnsToWidth()
		{
			// JJD 1/05/02
			// Added logic to autofit columns
			//
			if ( this.Layout.AutoFitAllColumns &&
				 this.Layout.Grid != null	&&
				 this.Layout.Grid.Created )
			{
				System.Drawing.Rectangle clientRect = this.Layout.Grid.ClientRectangle;

				if ( !clientRect.IsEmpty )
				{
					int right = clientRect.Right 
								- SystemInformation.VerticalScrollBarWidth
								// SSP 6/1/05 BR03774
								// Right should be relative to the row scroll region which does not
								// include the left border of the grid.
								//
								//- this.Layout.GetBorderThickness( this.Layout.BorderStyleResolved );
								- 2 * this.Layout.GetBorderThickness( this.Layout.BorderStyleResolved );

					if ( right > 0 )
					{
						// allocate enough ints for all the bands even though
						// we only need enough for each hierarchical level
						//
						int [] levelExtents = new int [ this.Layout.SortedBands.Count ];
						
						int maxRight = 0;
						int maxLevel = -1;
						int maxNonCardLevel = -1;

						// get the max extent at each level       
						//
						for ( int i = 0; i < this.Layout.SortedBands.Count; i++ )
						{
							UltraGridBand band = this.Layout.SortedBands[i];

							// Bypass hidden bands
							//
							if ( band.HiddenResolved )
								continue;

							int level = band.HierarchicalLevel;
	
							maxLevel = Math.Max( level, maxLevel );

							// Bypass card view
							//
							if ( band.CardView )
								continue;

							maxNonCardLevel = Math.Max( level, maxNonCardLevel );
							maxRight = Math.Max( maxRight, band.GetOrigin( BandOrigin.PreRowArea ) + band.GetExtent( BandOrigin.PreRowArea ) );

							levelExtents[level] = Math.Max ( levelExtents[level], band.GetExtent( BandOrigin.PreRowArea ) );
						}

						// if band zero is in card view then exit
						//
						if ( maxNonCardLevel < 0 )
							return;

						// If we have card view bands to the right then add enough room to 
						// accomodate them based on the average extents of the other bands
						// This is arbitrary but should be a reasonable approach.
						//
						if ( maxLevel > maxNonCardLevel )
						{
							int totalNonCardExtents = 0;
							
							// total up all non card level extents
							//
							for ( int i = 0; i <= maxNonCardLevel; i++ )
								totalNonCardExtents += levelExtents[i];

							int averageNonCardExtents = totalNonCardExtents / ( maxNonCardLevel + 1 );

							maxRight += averageNonCardExtents * ( maxLevel - maxNonCardLevel );

						}

						if ( maxRight == right )
							return;

						// compute the factor
						//
						double factor = (double)(right) / (double)maxRight;

						// get the max extent at each level       
						//
						for ( int i = 0; i < this.Layout.SortedBands.Count; i++ )
						{
							UltraGridBand band = this.Layout.SortedBands[i];

							// JJD 12/27/01
							// Bypass card view or hidden bands
							//
							if ( band.HiddenResolved || band.CardView )
								continue;

							int level = band.HierarchicalLevel;
							int newRight;

							if ( level == maxLevel )
								newRight = right;
							else
							{
								int totalExtent = 0;
								
								// total up all extents up to and including this level
								//
								for ( int j = 0; j <= level; j++ )
									totalExtent += levelExtents[j];

								newRight = (int)(factor * (double)totalExtent );
							}

							band.FitColumnsToWidth( newRight );

							// Adjust child band origins before proceeding
							//
							band.ResetBandOrigins( true );
						}
					}
				}
			}
		}

		#endregion FitColumnsToWidth


		internal override bool RecreateHeaderList( ColScrollRegion csr )
        {
            csr.VisibleHeaders.InternalClear();

            if ( csr.Layout.SortedBands.Count < 1 )
                return false;
            
            // verify that the band origin and extents have been
            // calculated properly
            //
//            csr.Layout.Bands.VerifyBandOrigins();

            bool columnAdded = false;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
            //int extent          = csr.Extent;

            int scrollPosition  = csr.Position;
            int bandOrigin;
            int bandExtent;

			// SSP 6/10/03 - Fixed headers
			// Added the if block and enclosed the already existing code in the else block.
			//
			// ----------------------------------------------------------------------------------
			bool usingFixedHeaders = csr.Layout.UseFixedHeaders && ! csr.HasExclusiveItems;
            if ( usingFixedHeaders )
			{
				for ( int i = 0; i < csr.Layout.SortedBands.Count; i++ )
				{
					UltraGridBand band      = csr.Layout.SortedBands[i];

					if ( band.HiddenResolved )
						continue;

					// Use the GetBandOriginExtent helper method to get the band origin and extent 
					// in fixed headers mode.
					//
					int tmpScrollPosition = csr.Position;
					UltraGridBand.GetBandOriginExtent_HorizontalViewStyle( band, csr, ref tmpScrollPosition, out bandOrigin, out bandExtent );
					columnAdded = this.AddBandHeaders( csr, band ) || columnAdded;
				}
			}
			else
			{
				for ( int i = 0; i < csr.Layout.SortedBands.Count; i++ )
				{
					UltraGridBand band      = csr.Layout.SortedBands[i];

					// Use the new GetBandOrigin and GetBandExtent methods
					// off the header sink to allow for the different metrics
					// of exclusive col scroll regions
					//
					bandOrigin     = csr.GetBandOrigin( band );
					bandExtent     = csr.GetBandExtent( band );

					// if the band is to the left of the scroll region then just
					// continue
					//
					if ( band.HiddenResolved || 
						bandOrigin + bandExtent < 
						scrollPosition )
						continue;

					columnAdded = this.AddBandHeaders( csr, band ) || columnAdded;
				}
			}

			// SSP 8/17/04 - UltraCalc
			// Added OnAfterRowListCreated and OnAfterHeaderListCreated methods. Call
			// OnAfterHeaderListCreated after the visible headers are regenerated.
			//
			// ----------------------------------------------------------------------------
			// MD 7/26/07 - 7.3 Performance
			// OnAfterHeaderListCreated does nothing right now, uncomment if logic is added to OnAfterHeaderListCreated
			//this.OnAfterHeaderListCreated( csr );
			// ----------------------------------------------------------------------------

            return columnAdded;
        }

		/// <summary>
		/// Initializes the context's band header array for tier zero elements. 
		/// </summary>
		/// <param name="context"></param>
		/// <param name="visibleRow"></param>
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//protected void SetContextHeaderArray ( ref VisibleRowFetchRowContext context, 
		//                                       VisibleRow visibleRow )
		protected static void SetContextHeaderArray( ref VisibleRowFetchRowContext context,
											  VisibleRow visibleRow )
        {
            int nLevel = visibleRow.Band.HierarchicalLevel;    
            
            if ( context.headerBands[ nLevel ] == null )
            {
                // init the array that keeps track of band headers
                //
                context.headerBands[ nLevel ] = visibleRow.Band;

                // walk up the parent chain
                //
                while ( nLevel > 0  && 
                        null == context.headerBands[ nLevel - 1 ] )
                {
                    context.headerBands[ nLevel - 1 ] = context.headerBands[ nLevel ].ParentBand;
                    nLevel--;
                }
            }
        }

		/// <summary>
		/// Sets the row's top, tier and hasheader flag while fetching forward
		/// </summary>
		/// <param name="context"></param>
		/// <param name="visibleRow"></param>
        protected override void OrientVisibleRowForward ( ref VisibleRowFetchRowContext context, 
                                                            VisibleRow visibleRow )
        {
			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
            //visibleRow.HasHeader        = false;
			this.SetHasHeaderHelper( ref context, visibleRow, false );

            visibleRow.PostRowSpacing   = 0;

            if ( context.lastRowInserted == null )
            {
                int nOrigTop = visibleRow.GetTop();
                int deltaTop;

                visibleRow.Tier = 0;
                visibleRow.SetTop( GetFixedHeaderHeight( true ) + context.rowScrollRegion.ScrollOffset );

                // Set the special flags here
                //
                visibleRow.SpecialFlags = this.CalculateRowSpecialFlags ( visibleRow, context.lastRowInserted );

                deltaTop = visibleRow.GetTop() - nOrigTop;

                // if we started on a row that was not from band 0
                // we need to initialize the top of its ancestor rows
                //
                VisibleRow parentVisibleRow = visibleRow.ParentVisibleRow;

                // walk up the parent chain to initialize the top property
                // maintaining the parent/child relative positions
                //
                while ( parentVisibleRow != null && deltaTop != 0 )
                {
                    parentVisibleRow.SetTop( parentVisibleRow.GetTop() + deltaTop );
                    parentVisibleRow = parentVisibleRow.ParentVisibleRow;
                }

                // Call new SetContextHeaderArray to initialize the band header
                // table which lets us know when we need to add a header to a row
                //
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
                //this.SetContextHeaderArray ( ref context, visibleRow );
				ViewStyleHorizontal.SetContextHeaderArray( ref context, visibleRow );
                
                return;
            }

            // start with the tier number of the previous row element
            //
            int  tier          = context.lastRowInserted.Tier;
            bool fBumpTier      = false;
            bool fNeedsBandGap  = false;
            bool fForceHeader   = false;
            
            UltraGridRow thisRow      = visibleRow.Row;
            UltraGridRow lastRow      = context.lastRowInserted.Row;

            if ( thisRow.Band == lastRow.Band )
            {
                // the last row element was from the same band
                // so we need to bump the tier
                //
                fBumpTier      = true;
                
                visibleRow.SetTop( context.lastRowInserted.OrigTop + context.lastRowInserted.GetTotalHeight( true ) );
            }
            else
            {
                fForceHeader = thisRow.Band.HasSiblingBands;

				// SSP 9/12/02 UWG1637
				// 
				// ---------------------------------------------------------------------
				if ( !fForceHeader )
				{
					for ( int i = 0; i < this.Layout.SortedBands.Count; i++ )
					{
						UltraGridBand band = this.Layout.SortedBands[i];
                        if (band.HiddenResolved)
                            continue;

                        // MBS 3/26/09 - TFS12665
                        // Reworked this logic for efficiency so that we don't unnecessarily get these potentially
                        // expensive properties
                        #region Old Code

                        //if ( band != thisRow.Band && band.HierarchicalLevel == thisRow.Band.HierarchicalLevel )
                        //{
                        //    fForceHeader = true;
                        //    break;
                        //}

                        //// SSP 5/20/05 BR03194
                        //// If one of the ancestor rows has headers then also force the descendant row to 
                        //// have headers. The following code checks for if there is some non-ancestor band
                        //// with the same hierarchy level as an ancestor band.
                        ////
                        //if ( band != thisRow.Band && band.HierarchicalLevel < thisRow.Band.HierarchicalLevel 
                        //    && ! thisRow.Band.IsDescendantOfBand( band ) )
                        //{
                        //    fForceHeader = true;
                        //    break;
                        //}
                        #endregion //Old Code
                        //
                        if (band != thisRow.Band)
                        {
                            // The accessors for the HierarchicalLevel are expensive, so get them here
                            int bandHierarchicalLevel = band.HierarchicalLevel;
                            int rowHierarchicalLevel = thisRow.Band.HierarchicalLevel;

                            if (bandHierarchicalLevel == rowHierarchicalLevel)
                            {
                                fForceHeader = true;
                                break;
                            }

                            // SSP 5/20/05 BR03194
                            // If one of the ancestor rows has headers then also force the descendant row to 
                            // have headers. The following code checks for if there is some non-ancestor band
                            // with the same hierarchy level as an ancestor band.
                            //
                            if (bandHierarchicalLevel < rowHierarchicalLevel && !thisRow.Band.IsDescendantOfBand(band))
                            {
                                fForceHeader = true;
                                break;
                            }
                        }
					}
				}
				// ---------------------------------------------------------------------

                // check if the last row inserted was our parent row
                //
                if ( visibleRow.ParentVisibleRow == context.lastRowInserted )
                {
                    // set the top equal to the top of our parent row
                    //
                    visibleRow.SetTop( visibleRow.ParentVisibleRow.OrigTop );
                }
                else
                {
                    // The last row wasn't our parent so we need to
                    // bump the tier number
                    //
                    fBumpTier               = true;
                    fNeedsBandGap           = true;

                    UltraGridRow commonParent = thisRow.GetCommonParent( lastRow );

                    // we need to figure out where the top of the next row needs to be
                    // we do this be walking up the parent chain to get the row that
                    // extends the farthest down and we use that as the top of
                    // this row
                    //
                    int maxYCoord = context.lastRowInserted.GetTop() + context.lastRowInserted.GetTotalHeight( true );
                    
                    VisibleRow lastRowParent = context.lastRowInserted.ParentVisibleRow;


                    // If the last insrted row and its parent were on the same tier
                    // then set the fNeedsBandGap flag to false so we don't add the
                    // interband gap. This occurs when there is a a single child
                    // row that is expanded
                    //
                    if ( lastRowParent != null && 
                         lastRowParent.Tier == context.lastRowInserted.Tier )
                    {
                        // JJD 5/19/00
                        // We only don't want a band gap if the previous hierarchical level
                        // is less that this row's band gap
                        //
                        if ( context.lastRowInserted.Band.HierarchicalLevel
                              < visibleRow.Band.HierarchicalLevel )
                        {
                             fNeedsBandGap   = false;
                        }
                    }

                    while ( lastRowParent != null )
                    {
                        // stop walking up the parent chain when we get to a
                        // common parent
                        //
                        if ( commonParent == lastRowParent.Row )
                            break;
                    
						// SSP 8/17/05 BR05457
						// Pass in true for the adjustForOverlappedBorders parameter to the GetTotalHeight, just like
						// how we do above.
						// 
                        //maxYCoord      = Math.Max( maxYCoord, lastRowParent.GetTop() + lastRowParent.GetTotalHeight( false ) );
						maxYCoord      = Math.Max( maxYCoord, lastRowParent.GetTop() + lastRowParent.GetTotalHeight( true ) );

                        lastRowParent  = lastRowParent.ParentVisibleRow;
                    }

                    visibleRow.SetTop( maxYCoord + ( fNeedsBandGap ? this.Layout.InterBandSpacing : 0 ) );

                    // reset fNeedsBandGap flag to true so we set the post row spacing
                    // below correctly
                    //
                    fNeedsBandGap           = true;
                }
            }

            if ( fBumpTier )
                tier++;

            // set the tier
            //
            visibleRow.Tier = tier;

            // set the last row's PostRowSpacing
            //
            context.lastRowInserted.PostRowSpacing = ( fNeedsBandGap 
                                                            ? this.Layout.InterBandSpacing
                                                            : 0 );

            bool needsHeader = false;

            if ( fForceHeader && visibleRow.OrigTop > this.GetFixedHeaderHeight( false ) + 1 )
            {
                int nLevel = visibleRow.Band.HierarchicalLevel;    
                
                if ( null == context.headerBands[ nLevel ] )
                {
                    // init this array entry with the first visible band at
                    // this level
                    //
                    for ( int i = 0; i < this.Layout.SortedBands.Count; i++ )
                    {
                        UltraGridBand band = this.Layout.SortedBands[i];

                        // if the band is hidden then ignore it
                        //
                        if ( band.HiddenResolved )
                            continue;

                        if ( band.HierarchicalLevel == nLevel )
                        {							
							// SSP 9/12/02 UWG1637
							// Following block ensures that we add a header when its necessary
							// and only when its necessary on a band. When you have sibling bands,
							// situations arise where if one of the child band has no rows, then
							// its sibling band will end up with a header when it didn't need
							// one or without a header when one was needed. The logic before
							// was assuming that sibling child bands always had rows but that 
							// can be false at times.
							//
							// --------------------------------------------------------------------- 
							if ( nLevel > 0  )
							{	
								UltraGridBand tmpBand = null;
								int levelToFind = nLevel;

								while ( levelToFind >= 0 )
								{
									for ( int c = 0; c < context.rowScrollRegion.VisibleRows.Count; c++ )
									{
										VisibleRow vr = context.rowScrollRegion.VisibleRows[c];

										// When verifiying the list, the visible row may already be in the
										// collection. So only loop until that row.
										//
										if ( vr == visibleRow )
											break;

										if ( thisRow.IsDescendantOf( vr.Row ) )
											continue;

										if ( vr.Row.Band.HierarchicalLevel == levelToFind - 1 )
										{
											tmpBand = vr.Row.Band;
											break;
										}
									}

									if ( null != tmpBand )
										break;

									levelToFind--;
								}

								if ( null != tmpBand && !band.IsDescendantOfBand( tmpBand ) )
									continue;
							}
							// ---------------------------------------------------------------------

                            context.headerBands[ nLevel ] = band;
                            break;
                        }
                    }
                }

                if ( context.headerBands[ nLevel ] != visibleRow.Band )
                {
                    context.headerBands[ nLevel ] = visibleRow.Band;
                    needsHeader = true;
                }
            }
            else
            {

                // JJD 5/19/00
                // Call new SetContextHeaderArray to initialize the band header
                // table which lets us know when we need to add a header to a row
                //
                if ( 0 == tier )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
                    //this.SetContextHeaderArray ( ref context, visibleRow );
					ViewStyleHorizontal.SetContextHeaderArray( ref context, visibleRow );
				}
            }

			// SSP 9/19/02 UWG1637
			// VerifyRowList calls AdjustListToFillRegion which pops the last row from the visible rows 
			// collection and calls AddSiblingRows again to restart filling of the region from that
			// row. This however causes problems with VisibleRowContext's headerBands member gets
			// out of sync with the visible rows collection as a result of popping the last visible row.
			// The side effect is that the header for the last visible row that was popped doesn't show
			// up. So added lastVisibleRowHadHeaders so that the OrientVisibleRowsForward knows that
			// we are in this situation so it can compensate for it.
			//
			if ( visibleRow == this.lastVisibleRowHadHeaders )
				needsHeader = true;

            // set this row's 'HasHeader' flag
            //
			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
            //visibleRow.HasHeader = needsHeader;
			this.SetHasHeaderHelper( ref context, visibleRow, needsHeader );

            // JJD 3/28/00
            // Set the special flags here
            //
            visibleRow.SpecialFlags = CalculateRowSpecialFlags ( visibleRow, context.lastRowInserted );

            // finally we need to align the tops of all rows on the
            // same tier
            //
            if (context.lastRowInserted != null &&
                // MBS 4/23/09 - TFS12665
                // As an optimization, when we're recreating the entire row list, only align the tops 
                // of the same tier row if this row doesn't have any children.  This is because when we
                // do finally position the children, they will walk up the parent chain and call this method
                // again, basically causing several duplicate alignments to occur.
                (!this.IsInRecreateRowList || !visibleRow.Row.HasChild()))
                AlignTopsOfSameTierRows(visibleRow);

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
            //this.SetNextCrossBandSibling( ref context, visibleRow );
			ViewStyleBase.SetNextCrossBandSibling( ref context, visibleRow );

        }

		/// <summary>
		/// Checks for parent rows on the same tier and aligns their tops
        /// (ignoring headers).
		/// </summary>
		/// <param name="visibleRow"></param>
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
        //protected void AlignTopsOfSameTierRows( VisibleRow visibleRow )
		protected static void AlignTopsOfSameTierRows( VisibleRow visibleRow )
        {
            // JJD 5/1/00 
            // We only need to align the tops of rows for tiers > 0
            //
			// SSP 1/20/04 UWG2893
			// We need to do this when the Tier is 0 as well because below code is not
			// only aligning the tops, but also syncing the heights of the same tier rows.
			// Took out the visibleRow.Tier > 0 condition.
			//
            //if ( visibleRow.Tier > 0        &&
			if ( !visibleRow.HasPrevSibling && 
                 !visibleRow.Row.HasSiblingInPrevSiblingBand(true) )
            {
                int  tier              = visibleRow.Tier;
                int  nMaxRowTop         = visibleRow.GetTopOfRow ( false );
                bool fSameTierRowFound  = false;

                VisibleRow parentVisibleRow = visibleRow.ParentVisibleRow;

                // walk up the parent chain (on this tier) and get the highest
                // top coordinate
                //
                while ( parentVisibleRow != null && 
                        parentVisibleRow.Tier == tier )
                {
                    fSameTierRowFound = true;

                    nMaxRowTop = Math.Max( nMaxRowTop, parentVisibleRow.GetTopOfRow( false ) );

                    // if this element has a previous sibling then it can't
                    // be on the same tier
                    //
					// SSP 11/11/03 Add Row Feature
					//
                    //if ( parentVisibleRow.Row.HasPrevSibling(true)    || 
					if ( parentVisibleRow.Row.HasPrevSibling( true, IncludeRowTypes.SpecialRows )    || 
                         parentVisibleRow.Row.HasSiblingInPrevSiblingBand(true) )
                         break;

                    // get the next parent in the chain
                    //
                    parentVisibleRow = parentVisibleRow.ParentVisibleRow;
                }

                // set this element's top
                //
                visibleRow.SetTopOfRow ( nMaxRowTop, false ); 

                parentVisibleRow = visibleRow.ParentVisibleRow;

                // walk up the parent chain (on this tier) again to set the
                // top of each element
                //
                while ( parentVisibleRow != null  && 
                        parentVisibleRow.Tier == tier )
                {
                    parentVisibleRow.SetTopOfRow( nMaxRowTop, false );

                    // if this element has a previous sibling then it can't
                    // be on the same tier
                    //
					// SSP 11/11/03 Add Row Feature
					//
                    //if ( parentVisibleRow.Row.HasPrevSibling(false, true)    || 
					if ( parentVisibleRow.Row.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows )    || 
                         parentVisibleRow.Row.HasSiblingInPrevSiblingBand(true) )
                         break;

                    // get the next parent in the chain
                    //
                    parentVisibleRow = parentVisibleRow.ParentVisibleRow;
                }

                // JJD 2/22/00
                // If a same tier row was found then reset the height of the row
                // (which will force a resync of same tier row heights)
                //
                if ( fSameTierRowFound )
                    visibleRow.Row.SetHeight( visibleRow.Row.BaseHeight, false );
            }
        }  


		/// <summary>
		/// Sets the row's top, tier and hasheader flag while fetching
        /// backward 
		/// </summary>
		/// <param name="context"></param>
		/// <param name="visibleRow"></param>
        protected override void OrientVisibleRowBackward ( ref VisibleRowFetchRowContext context, 
                                                           VisibleRow visibleRow )

        {
			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
            //visibleRow.HasHeader        = false;
			this.SetHasHeaderHelper( ref context, visibleRow, false );

            visibleRow.PostRowSpacing   = 0;

            if ( context.lastRowInserted == null )
            {
                // set tier to arbitrary high number since after we fetch bacward we 
                // always do a fixup going forward that will set the tier back to zero
                //
                visibleRow.Tier = 5000;

                visibleRow.SetTop( context.regionHeight - visibleRow.GetTotalHeight() );

                if ( GetFixedHeaderHeight( false ) >= visibleRow.OrigTop + context.rowScrollRegion.ScrollOffset )
                    visibleRow.SetTop( GetFixedHeaderHeight() + context.rowScrollRegion.ScrollOffset );
                
                return;
            }

            // start with the tier number of the previous row element
            //
            int  tier          = context.lastRowInserted.Tier;
            bool fBumpTier      = false;
            bool fNeedsBandGap  = false;
            bool fForceHeader   = false;
            
            UltraGridRow thisRow      = visibleRow.Row;
            UltraGridRow lastRow      = context.lastRowInserted.Row;

            if ( thisRow.Band == thisRow.Band )
            {
                // the last row element was from the same band
                // so we need to bump the tier
                //
                fBumpTier      = true;
                
                visibleRow.SetTop( context.lastRowInserted.GetTop() - visibleRow.GetTotalHeight() );

                if ( GetFixedHeaderHeight( false ) >= visibleRow.GetTop() + context.rowScrollRegion.ScrollOffset )
                    visibleRow.SetTop( GetFixedHeaderHeight() + context.rowScrollRegion.ScrollOffset );
            }
            else
            {
                fForceHeader = lastRow.Band.HasSiblingBands;

                // check if the last row's parent row is this row
                //
                if ( context.lastRowInserted.ParentVisibleRow == visibleRow )
                {
                    // set the top equal to the top of of the last
                    //
                    visibleRow.SetTop( context.lastRowInserted.GetTop() );
                }
                else
                {
                    // The last row's parent wasn't this row so we need to
                    // bump the tier number
                    //
                    fBumpTier      = true;
                    fNeedsBandGap  = true;
                
                    visibleRow.SetTop( context.lastRowInserted.GetTop() - visibleRow.GetTotalHeight() );

                    if ( GetFixedHeaderHeight( false ) >= visibleRow.GetTop() + context.rowScrollRegion.ScrollOffset )
                        visibleRow.SetTop( GetFixedHeaderHeight() + context.rowScrollRegion.ScrollOffset );

                    UltraGridRow commonParent = lastRow.GetCommonParent( thisRow );

                    // we need to figure out where the top of the next row needs to be
                    // we do this be walking up the parent chain to get the row that
                    // extends the farthest down and we use that as the top of
                    // last row
                    //
                    int maxYCoord = visibleRow.GetTop() + visibleRow.GetTotalHeight();
                    VisibleRow parent = visibleRow.ParentVisibleRow;

                    while ( parent != null )
                    {
                        // stop walking up the parent chain when we get to a
                        // common parent
                        //
                        if ( commonParent == parent.Row )
                            break;
                    
                        maxYCoord  = Math.Max( maxYCoord, parent.GetTop() + parent.GetTotalHeight() );
                        parent     = parent.ParentVisibleRow;
                    }

                    context.lastRowInserted.SetTop( maxYCoord + this.Layout.InterBandSpacing );
                }
            }

            if ( fBumpTier )
                tier--;

            // set the tier
            //
            visibleRow.Tier = tier;

            // set this row's PostRowSpacing
            //
            visibleRow.PostRowSpacing = ( fNeedsBandGap 
                                        ? this.Layout.InterBandSpacing
                                        : 0 );

            // set the last row's 'HasHeader' flag
            //
			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
            //context.lastRowInserted.HasHeader = ( fForceHeader && context.lastRowInserted.GetTop() > GetFixedHeaderHeight( false ) + 1 );
			bool hasHeader = ( fForceHeader && context.lastRowInserted.GetTop() > GetFixedHeaderHeight( false ) + 1 );
			this.SetHasHeaderHelper( ref context, context.lastRowInserted, hasHeader );

            // finally we need to align the tops of all rows on the
            // same tier
            //
            if ( context.lastRowInserted != null )
                AlignTopsOfSameTierRows( context.lastRowInserted );
         
        }
		internal override void AdjustDescendantBandOrigins( Infragistics.Win.UltraWinGrid.UltraGridBand band, int adjustment ) 
        { 
            Infragistics.Win.UltraWinGrid.UltraGridBand  parentBand;
            bool  bandReached = false;

            // iterate over the bands and adjust the origins of all descendant
            // bands
            //
            for ( int i = 0; i < this.Layout.SortedBands.Count; i++  )
            {
                if ( this.Layout.SortedBands[i].HiddenResolved )
                    continue;

                // we don't have to do anything until after we get to the
                // passed in band
                //
                if ( this.Layout.SortedBands[i] == band )
                {
                    bandReached = true;
                    continue;
                }
                else
                if ( !bandReached )
                    continue;

                parentBand = this.Layout.SortedBands[i].ParentBand;

                // walk up the parent chain looking for the passed in band
                //
                while ( parentBand != null )
                {
                    if ( band == parentBand )
                    {
                        // we found the band in the parent chain so adjust the
                        // descendant band's origin and break out of the
                        // while loop
                        //
                        this.Layout.SortedBands[i].AdjustOrigin ( adjustment );
                        break;
                    }

                    // get the next parent band up the chain
                    //
                    parentBand = parentBand.ParentBand;
                }
            }
        }
		internal override void AdjustBandOrigin( UltraGridBand band, 
			                                     UltraGridBand priorBand,
			                                     ref int origin )
		{
    	    origin = 0;

            while ( priorBand != null )
            {
                if ( priorBand == band.ParentBand )
                {
                    origin = band.ParentBand.GetOrigin() + 
					    	 band.ParentBand.GetExtent();
                    break;
                }

                priorBand = priorBand.ParentBand;
            }

		}
		internal override void AdjustExclusiveMetrics( Infragistics.Win.UltraWinGrid.ColScrollRegion csr,
			                                            HeaderBase header,
			                                            HeaderBase priorHeader)
		{
			int nOrigin;

            // on a band break recalc the origin 
            //
            if ( priorHeader != null )
            {
                // if there is a parent band indent so that this child band starts indented
                // by VSVERTICAL_BAND_OFFSET pixels
                // 
                if ( header.Band != priorHeader.Band &&
                     header.Band.ParentBand != priorHeader.Band)
                    nOrigin = csr.GetBandOrigin( header.Band.ParentBand ) 
                            + csr.GetBandExtent( header.Band.ParentBand );
                else
                    nOrigin = priorHeader.Origin + priorHeader.Extent;
            
            }
            else
            {
                nOrigin = 0;
            }

			this.SetExclusiveMetrics( header, nOrigin );		    
		}
		internal override void GetFixedHeaderBands( VisibleRowsCollection visibleRows,
			                                        UltraGridBand [] headerBands,
			                                        ref int headerBandCount )
		{
			// init the return count
			//
			headerBandCount = 0;

            // JJD 4/19/00
            // If we don't have any bands then return
            //
            if ( this.Layout.SortedBands.Count < 1 )
                return;

            int bandLevel = 0;

            // get the first row elem
            //
            VisibleRow firstTierLeaf = visibleRows.Count > 0 
                                        ? visibleRows[0]
                                        : null;

            UltraGridBand firstTierLeafBand = null;

            // we need to get the lowest level child/leaf row on the first tier
            // since that will determine which sibling bands to use down to that point
            //
            //
            if ( firstTierLeaf != null )
            {
                int firstTier = firstTierLeaf.Tier;

                // iterate over the rows until we get to a tier break
                //
                for ( int i = 0; i < visibleRows.Count; i++ )
                {
					// JJD 12/13/01
					// If the visiblerow is a card area then break out
					//
					if ( visibleRows[i].IsCardArea ||
						 visibleRows[i].Tier != firstTier )
                        break;

                    // the row is on the first tier o save its pointer since it
                    // is the leafiest row so far
                    //
                    firstTierLeaf = visibleRows[i];
                }
            }

            if ( firstTierLeaf != null )
            {
                // get the row from the first tier
                //
                Infragistics.Win.UltraWinGrid.UltraGridRow  row = firstTierLeaf.Row;

                // JJD 10/04/00
                // Save the ptr to the first tier leaf row's band
                // (see comments below)
                //
                firstTierLeafBand = row.Band;

                // get the hierarchical level of the row's band.
                // This we use as the index into the headerBands 
                // stack array
                //
                bandLevel  = row.Band.HierarchicalLevel;

                // save the BandLevel count
                //
                headerBandCount = bandLevel + 1;

                // walk up the row parent chain (which should be
                // exactly as int as headerBandCount
                //
                while ( row != null )
                {
                    // save this row's band in our array
                    //
                    headerBands[ bandLevel ] = row.Band;

                    // decrement the band level since we are going up
                    //
                    bandLevel--;

                    System.Diagnostics.Debug.Assert(bandLevel >= -1, "Invalid band level count");

                    // get the next parent in the chain
                    //
                    row     = row.ParentRow;
                }

            }


            // we still might have identified all the bands we nned to draw
            // headers for. For example, if the first tier leaf row was not at the
            // lowest hierarchical level
            //
            int deepestBandLevel;
            int thisLevel;

            // if there aren't any bands added to the array yet then
            // add band 0 now
            //
            if ( headerBandCount < 1 )
            {
                headerBandCount = 1;
                bandLevel  = 0;
                headerBands[ bandLevel ] = this.Layout.SortedBands[0];
            }
            else
            {
                // set bandLevel to point to the last band we added above
                //
                bandLevel = headerBandCount - 1;
            }

            deepestBandLevel = bandLevel;

            if ( deepestBandLevel + 1 < this.Layout.SortedBands.Count )
            {
                // make one pass thru the bands collection trying to fill the array
                // with direct children of the lowest level band in the array
                //
                for ( int i = 0; i < this.Layout.SortedBands.Count; i++ )
                {
                    Infragistics.Win.UltraWinGrid.UltraGridBand band =  this.Layout.SortedBands[i];

                    // if the band is hidden then ignore it
                    //
                    if ( band.HiddenResolved )
                        continue;

					// JJD 12/13/01
					// If the band is set to card view then it doesn't need 
					// any column headers
					//
					if ( band.CardView )
						continue;

                    // if this band is a direct child band of the lowest
                    // level band in the array then add it to the array
                    // (making it the lowest now)
                    //
                    if ( band.ParentBand == headerBands[ bandLevel ] )
                    {
                        bandLevel++;
                        headerBands[ bandLevel ] = band;
                        headerBandCount++;
                        thisLevel = bandLevel;
                    }
                    else
                    {
                        thisLevel = band.HierarchicalLevel;
                    }

                    // we want to keep track of the deepest(visible) band level
                    //
                    deepestBandLevel = Math.Max( deepestBandLevel, thisLevel );

                }
            }

            if ( deepestBandLevel >= headerBandCount )
            {
                // make a second pass thru the bands collection is case there are
                // lower level bands that aren't direct children of the bands already
                // in the array
                //
                for ( int i = 0; i < this.Layout.SortedBands.Count; i++ )
                {
                    Infragistics.Win.UltraWinGrid.UltraGridBand band =  this.Layout.SortedBands[i];

                    // if the band is hidden then ignore it
                    //
                    if ( band.HiddenResolved )
                        continue;

                    // JJD 10/04/00
                    // Make sure that the band is a descendant of the first
                    // tier leaf band. If it isn't then we don't want to
                    // display it. This prevents some wierd results on some
                    // band configurations. For example:
                    //
                    // Customers
                    //     |
                    //     |--- Orders
                    //     |      |
                    //     |      |--- OrderDetails
                    //     |
                    //     |--- Contacts
                    //
                    // In the above cofiguration if the first tire leaf row was 
                    // in band 'Contacts' we don't want the 'OrderDetails' headers
                    // to end up in the fixed header area
                    //
                    if ( firstTierLeafBand != null &&
                            !band.IsDescendantOfBand( firstTierLeafBand ) )
                        continue;

                    // get the hierarchical level of the band
                    //
                    thisLevel = band.HierarchicalLevel;

					// SSP 5/20/05 BR03194
					// This is to take care of the configuration.
					//
					// Root Band
					//     |
					//     |--- Band 1
					//     |      |
					//     |      |--- Band 11
					//     |
					//     |--- Band 2
					//            |
					//            |--- Band 22
					//
					// Above if Band 1 is displaying its headers then don't display the 
					// headers of Band 22. Also if Band 2 headers are displayed then don't
					// display the headers of Band 11.
					//
					// SSP 7/22/05 BR05087
					// Also don't display descendant band headers if ancestor band headers 
					// aren't displayed.
					// 
					//if ( thisLevel > 0 && null != headerBands[ thisLevel - 1 ] 
					//	&& ! band.IsDescendantOfBand( headerBands[ thisLevel - 1 ] ) )
					if ( thisLevel > 0 && ( null == headerBands[ thisLevel - 1 ] 
						|| ! band.IsDescendantOfBand( headerBands[ thisLevel - 1 ] ) ) )
						continue;

                    // if that level slot in our array has not
                    // been filled yet then fill it now
                    //
                    if ( null == headerBands[ thisLevel ] )
                    {
                        headerBands[ thisLevel ] = band;
                        headerBandCount++;
                    }
                }

                // JJD 10/04/00
                // Remove assertion since this condition is valid if the first
                // tier leaf band doesn't have descendants down to the deepest
                // level of other bands.
                //
    //            ASSERT( headerBandCount == deepestBandLevel + 1, "Not all band level slots were filled");
            }
        }

		#region SupportsFixedRows

		// SSP 4/13/05 - NAS 5.2 Fixed Rows
		//
		internal override bool SupportsFixedRows
		{
			get
			{
				return false;
			}
		}

		#endregion // SupportsFixedRows

	}
}
