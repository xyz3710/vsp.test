#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;
using Infragistics.Win.Layout;

// SSP 4/14/05 - NAS 5.2 Filter Row
// 

namespace Infragistics.Win.UltraWinGrid
{
	#region UltraGridFilterRow Class

	/// <summary>
	/// A class to represent a filter row.
	/// </summary>
	public class UltraGridFilterRow : UltraGridSpecialRowBase
	{
		#region Private Vars

		#endregion // Private Vars

		#region Constructor
		
		internal UltraGridFilterRow( RowsCollection parentCollection ) : base( parentCollection )
		{
		}

		#endregion // Constructor

		#region ColumnFilters

		internal ColumnFiltersCollection ColumnFilters
		{
			get
			{
				return this.ParentCollection.ActiveColumnFiltersForceAllocation;
			}
		}

		#endregion // ColumnFilters

		#region ApplyFilters

		/// <summary>
		/// Applies the filters currently input into the filter row.
		/// </summary>
		public void ApplyFilters( )
		{
			this.ApplyFilters( true );
		}

		internal void ApplyFilters( bool commitEditValues )
		{
			foreach ( UltraGridColumn column in this.Band.Columns )
				this.ApplyFilters( column, commitEditValues );
		}
		
		internal void ApplyFilters( UltraGridColumn column, bool commitEditValues )
		{
			if ( commitEditValues )
			{
				UltraGridCell activeCell = column.Layout.ActiveCell;
				if ( null != activeCell && activeCell.IsInEditMode 
					&& activeCell.Row == this && activeCell.Column == column )
				{
					bool stayInEdit = true;
					activeCell.CommitEditValue( ref stayInEdit, true );
				}
			}

			ColumnFiltersCollection columnFilters = this.ColumnFilters;
			ColumnFilter cf = columnFilters.HasColumnFilter( column ) ? columnFilters[ column ] : null;

			// Only modify the column filter if the user changed the value of an operand or
			// operator. FilterRowOperandValue and filterRowOperatorValue member vars of the
			// column filter would have been set in that case.
			//
			if ( null != cf && ( cf.filterOperandModifiedSinceLastApply || cf.filterOperatorModifiedSinceLastApply ) )
			{
				object operatorVal = this.GetOperatorValue( column );
				object operandVal = this.GetCellValue( column );

                // MBS 9/12/08 - TFS6402
                // If the user has specified that the filter cell should use the same editor
                // as the rest of the column and they haven't specifically set a new
                // ValueList on the filter cell, then we should use the text that the user
                // sees so that the filter cell behaves in the same way as regular cells
                if (FilterOperandStyle.UseColumnEditor == column.FilterOperandStyleResolved)
                {
                    IValueList vl = this.GetValueListResolved(column);
                    if (vl != null && vl.Equals(column.ValueList))
                    {
                        int index = 0;
                        operandVal = vl.GetText(operandVal, ref index);
                    }
                }

				ColumnFilter cloneColumnFilter = cf.Clone( );
					
				// Clear the existing conditions.
				//
				cloneColumnFilter.FilterConditions.Clear( );

				// SSP 10/23/07 BR25866
				// Clear this member var which we will set below if necessary.
				// 
				cloneColumnFilter.displayTextOverride = null;
					
				Debug.Assert( operatorVal is FilterComparisionOperator, "Value in the operator editor should have been an instance of FilterComparisionOperator." );

				// If the operand cell's contents have been cleared then that means the column filters
				// should be cleared, which we do above.
				//
				if ( null != operandVal && DBNull.Value != operandVal )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//FilterComparisionOperator comparisionOperator = this.GetComparisionOperatorFromOperatorValue( column, operatorVal );
					FilterComparisionOperator comparisionOperator = UltraGridFilterRow.GetComparisionOperatorFromOperatorValue( column, operatorVal );

					// If (Blanks) or (NonBlanks) is selected then use the BlankCellValue as
					// the filter operator value and use either Equals or NotEquals as the
					// operator.
					//
					string operandAsString = operandVal.ToString( );
					if ( UltraGridBand.FilterBlanks == operandAsString
						|| UltraGridBand.FilterNonBlanks == operandAsString )
					{
						operandVal = UltraGridBand.FilterBlanks == operandAsString 
							? FilterCondition.BlankCellValue : FilterCondition.NonBlankCellValue;

						// For (Blanks) and (NonBlanks) items = and != are the only valid operators.
						//
						if ( FilterComparisionOperator.Equals != comparisionOperator
							&& FilterComparisionOperator.NotEquals != comparisionOperator )
						{
							comparisionOperator = FilterComparisionOperator.Equals;
							this.SetOperatorValue( column, comparisionOperator );
						}
					}
                    // MBS 6/22/09 - TFS18639
                    // Added support for "(Errors)" and "(NonErrors)"
                    else if (UltraGridBand.FilterErrors == operandAsString ||
                        UltraGridBand.FilterNonErrors == operandAsString)
                    {
                        operandVal = new DataErrorFilterCondition(UltraGridBand.FilterErrors == operandAsString);

                        // For (Errors) and (NonErrors) items = and != are the only valid operators.
                        //
                        if (FilterComparisionOperator.Equals != comparisionOperator
                            && FilterComparisionOperator.NotEquals != comparisionOperator)
                        {
                            comparisionOperator = FilterComparisionOperator.Equals;
                            this.SetOperatorValue(column, comparisionOperator);
                        }
                    }

					// SSP 6/9/06 BR13443
					// Check for operandVal being invalid for the specified comparison operatior. For 
					// example, if operator is Match, then we have to make sure operand is a valid regular
					// expression. Otherwise FilterConditions.Add will throw an exception.
					// 
					// --------------------------------------------------------------------------------------
					//cloneColumnFilter.FilterConditions.Add( comparisionOperator, operandVal );
					try
					{
						// SSP 12/14/06 BR18566
						// Support custom filter conditions. OperandVal would be a FilterCondition 
						// instance in case of custom filter conditions.
						// 
						// ------------------------------------------------------------------------
						//cloneColumnFilter.FilterConditions.Add( comparisionOperator, operandVal );
						if ( operandVal is FilterCondition )
							cloneColumnFilter.FilterConditions.Add( (FilterCondition)operandVal );
						// SSP 10/23/07 BR25866
						// OperandVal could also be a ColumnFilter instance. Added else-if block below.
						// 
						else if ( operandVal is ColumnFilter )
						{
							ColumnFilter tmp = operandVal as ColumnFilter;
							cloneColumnFilter.InitializeFrom( tmp );
						}
						else
							cloneColumnFilter.FilterConditions.Add( comparisionOperator, operandVal );
						// ------------------------------------------------------------------------
					}
					catch ( Exception )
					{
						return;
					}
					// --------------------------------------------------------------------------------------
				}

				// Use the ApplyNewFiltersHelper method to fire BeforeRowFilterChanged and 
				// AfterRowFilterChanged events and also copy the new column filter to the 
				// original column filter.
				//
				bool canceled = !UltraGridFilterRow.ApplyNewFiltersHelper( cf, cloneColumnFilter, this );

				// If BeforeRowFilterChanged was canceled and the filter cell is still in edit
				// mode then reset it's value. Calling SetCellValue and SetOperatorValue will
				// accomplish that.
				//
				if ( !canceled )
				{
					// SSP 10/23/07 BR25866
					// If the column filter or filter condition was created by the user and populated 
					// inside the filter drop-down from BeforeFilterDropDown event then we need to use
					// whatever display text was assigned to the associated value list item.
					// 
					if ( operandVal is FilterCondition || operandVal is ColumnFilter )
						cf.displayTextOverride = this.GetCellText( column );
					else
						cf.displayTextOverride = null;

					cf.ClearEditModeInfo( );

					// Update the visibility of modified row icon.
					//
					this.UpdateRowDataChangedHelper( );
				}
			}
		}

		#endregion // ApplyFilters

		#region ApplyNewFiltersHelper

        // MBS 2/25/09 - TFS14473
        // Made the method public since the FilterUIProvider needs to be able to fire these events through the grid
        //
		/// <summary>
		/// For Infragistics internal use only.  Applies the new filters and fires any necessary events.
		/// </summary>
		/// <param name="origColumnFilter"></param>
		/// <param name="newColumnFilter"></param>
		/// <param name="filterRow">Optional. Passed in only if filtering is being done through a filter row.</param>
		/// <returns></returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
		public static bool ApplyNewFiltersHelper( ColumnFilter origColumnFilter, ColumnFilter newColumnFilter, UltraGridFilterRow filterRow )
		{
			UltraGridColumn column = origColumnFilter.Column;
			UltraGridBand band = column.Band;
			UltraGridLayout layout = null != band ? band.Layout : null;
			UltraGridBase grid = null != layout ? layout.Grid : null;

            // MRS 6/5/2008 - BR33648
            // No need to fire the events if the column filters are the same. 
            if (origColumnFilter.InternalEquals(newColumnFilter))
                return false;

			BeforeRowFilterChangedEventArgs beforeRowFilterChnagedArgs = new BeforeRowFilterChangedEventArgs( newColumnFilter );
			if ( null != grid )
				// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// Support Filtering in Combo and DropDown
				//grid.FireEvent( GridEventIds.BeforeRowFilterChanged, beforeRowFilterChnagedArgs );
				grid.FireCommonEvent( CommonEventIds.BeforeRowFilterChanged, beforeRowFilterChnagedArgs );

			// If the event is canceled then return.
			//
			if ( beforeRowFilterChnagedArgs.Cancel )
				return false;

			// Copy the new column filters from the clone column filter object to the original
			// column filter object.
			//
			origColumnFilter.InitializeFrom( newColumnFilter );

			// Fire the AfterRowFilterChanged event.
			//
			if ( null != grid )
			{
				// JDN 11/16/04 Added Synchronous Sorting and Filtering
				// Ensure filtering is synchronously processed for either all rows or all expanded rows
				// if the BeforeSortChangeEventArgs.ProcessMode is set to a synchronous value
				// SSP 2/4/05
				// Use the new EnsureSortedAndFilteredHelper method instead of duplicating the code
				// in a few places. Also make sure to pass in the lowest level band otherwise it
				// will sort and filter lower level bands than this band.
				//
				grid.Rows.EnsureSortedAndFilteredHelper( beforeRowFilterChnagedArgs.ProcessMode, column.Band );
				

				// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// Support Filtering in Combo and DropDown
				//grid.FireEvent( GridEventIds.AfterRowFilterChanged, new AfterRowFilterChangedEventArgs( this.ColumnHeader.Column ) );
				// SSP 3/15/05 - NAS 5.2 Filter Row
				// Added NewColumnFilter property to the AfterRowFilterChangedEventArgs. Pass in
				// the changed ColumnFilter instance to the constructor of the event args.
				//
				//grid.FireCommonEvent( CommonEventIds.AfterRowFilterChanged, new AfterRowFilterChangedEventArgs( this.ColumnHeader.Column ) );
				grid.FireCommonEvent( CommonEventIds.AfterRowFilterChanged, new AfterRowFilterChangedEventArgs( column, origColumnFilter ) );

				// SSP 8/6/02 UWG1394
				// After filtering rows, see if we need to have the scroll bar at all.
				// Because when the rows get filtered out, all rows may be able to fit
				// the row scroll region in which case we shouldn't have the scrollbar
				// visible.
				//
				// --------------------------------------------------------------------------
				RowScrollRegion rsr = layout.ActiveRowScrollRegion;
				if ( null != rsr && layout.Rows.VisibleRowCount > 0 )
				{
					// SSP 5/20/05
					// Only do so if ScrollBounds is not set to ScrollToFill because ScrollToFill 
					// logic will take care of it.
					//
					if ( ScrollBounds.ScrollToFill != layout.ScrollBounds )
					{
						UltraGridRow oldFirstRow = rsr.FirstRow;

						rsr.SetDestroyVisibleRows( );
						rsr.RegenerateVisibleRows( );

						// SSP 4/28/05 - NAS 5.2 Fixed Rows
						// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
						// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
						//
						//if ( rsr.IsLastRowVisible( ) && !rsr.IsFirstRowVisible( ) )
						if ( rsr.IsLastScrollableRowVisible( ) && ! rsr.IsFirstScrollableRowVisible( ) )
						{
							rsr.FirstRow = layout.Rows.GetFirstVisibleRow( );

							rsr.SetDestroyVisibleRows( );
							rsr.RegenerateVisibleRows( false );

							// SSP 4/28/05 - NAS 5.2 Fixed Rows
							// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
							// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
							//
							//if ( !rsr.IsLastRowVisible( ) )
							if ( !rsr.IsLastScrollableRowVisible( ) )
							{
								rsr.FirstRow = oldFirstRow;
								rsr.SetDestroyVisibleRows( );
							}
						}
					}
				}
				// --------------------------------------------------------------------------

				// As a result of filtering rows the filter row could get scrolled out of view.
				// Scroll it back into view.
				//
                if (null != filterRow && null != rsr &
                    // MRS 12/6/2007 - BR28952
                    filterRow.IsActiveRow)
                {
                    rsr.ScrollRowIntoView(filterRow, false, true);
                }
			}

			return true;
		}

		#endregion // ApplyNewFiltersHelper

		#region UpdateRowDataChangedHelper

		private void UpdateRowDataChangedHelper( )
		{
			bool cellDirty = false;
			foreach ( UltraGridColumn column in this.Band.Columns )
			{
				ColumnFilter cf = this.GetColumnFilterIfAllocated( column );
				if ( null != cf && ( cf.filterOperandModifiedSinceLastApply || cf.filterOperatorModifiedSinceLastApply ) )
				{
					cellDirty = true;
				}
				else
				{
					UltraGridCell cell = this.GetCellIfAllocated( column );
					if ( null != cell )
						cell.SetDataChanged( false );
				}
			}

			if ( ! cellDirty )
				this.SetDataChanged( false );
		}

		#endregion // UpdateRowDataChangedHelper

		#region GetComparisionOperatorFromOperatorValue

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal FilterComparisionOperator GetComparisionOperatorFromOperatorValue( UltraGridColumn column, object operatorValue )
		internal static FilterComparisionOperator GetComparisionOperatorFromOperatorValue( UltraGridColumn column, object operatorValue )
		{
			FilterComparisionOperator comparisionOperator = FilterCondition.GetFilterComparisonOperator( operatorValue );

			// If the operator value is Custom then use the default value. This means the developer
			// had added multiple filters or custom filters and the user modifies the operand value
			// however doesn't touch the operator value and thus the operator value stays Custom.
			//
			if ( FilterComparisionOperator.Custom == comparisionOperator )
				comparisionOperator = column.Layout.GetAssociatedFilterComparisionOperator( column.FilterOperatorDefaultValueResolved );

			return comparisionOperator;
		}

		internal FilterComparisionOperator GetOperatorValueAsEnum( UltraGridColumn column )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//return this.GetComparisionOperatorFromOperatorValue( column, this.GetOperatorValue( column ) );
			return UltraGridFilterRow.GetComparisionOperatorFromOperatorValue( column, this.GetOperatorValue( column ) );
		}

		#endregion // GetComparisionOperatorFromOperatorValue

		#region GetColumnFilter

		internal ColumnFilter GetColumnFilter( UltraGridColumn column )
		{
			return this.ColumnFilters[ column ];
		}

		#endregion // GetColumnFilter

		#region GetColumnFilterIfAllocated

		internal ColumnFilter GetColumnFilterIfAllocated( UltraGridColumn column )
		{
			ColumnFiltersCollection cfColl = this.ColumnFilters;
			return cfColl.HasColumnFilter( column ) ? cfColl[ column ] : null;
		}

		#endregion // GetColumnFilterIfAllocated

		#region IsOperandEditable

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool IsOperandEditable( UltraGridColumn column )
		internal static bool IsOperandEditable( UltraGridColumn column )
		{
			FilterOperandStyle style = column.FilterOperandStyleResolved;
			return FilterOperandStyle.Disabled != style && FilterOperandStyle.None != style;
		}

		#endregion // IsOperandEditable

		#region GetOperatorValueListItem

		internal ValueListItem GetOperatorValueListItem( FilterComparisionOperator filterComparisonOperator )
		{
			int index = -1;
			ValueList valueList = this.Layout.FilterOperatorValueListAll;
			((IValueList)valueList).GetText( filterComparisonOperator, ref index );

			ValueListItem item = index >= 0 && index < valueList.ValueListItems.Count
				? valueList.ValueListItems[ index ] : null;

			Debug.Assert( null != item || FilterComparisionOperator.Custom == filterComparisonOperator );
			return item;
		}

		#endregion // GetOperatorValueListItem

		#region GetCurrentOperatorImage

		internal object GetCurrentOperatorImage( UltraGridColumn column )
		{
			object currentOperatorObj = this.GetOperatorValue( column );
			object image = null;
			if ( currentOperatorObj is FilterComparisionOperator )
			{
				FilterComparisionOperator currentOperator = (FilterComparisionOperator)currentOperatorObj;
				ValueListItem item = this.GetOperatorValueListItem( currentOperator );
				if ( null != item )
					image = item.Appearance.Image;
			}

			return image;
		}

		#endregion // GetCurrentOperatorImage

		#region GetOperatorValue

		internal object GetOperatorValue( UltraGridColumn column )
		{
			ColumnFilter cf = this.GetColumnFilterIfAllocated( column );
			object val = null != cf ? cf.filterRowOperatorValue : null;
			if ( null == cf || ! cf.filterOperatorModifiedSinceLastApply )
			{
				FilterConditionsCollection filterConditions = null != cf ? cf.FilterConditions : null;

				if ( null == filterConditions || 0 == filterConditions.Count )
					// If the user has chosen one already then use that. Look in ApplyFilters for more info.
					//
					val = val is FilterComparisionOperator && FilterComparisionOperator.Custom != (FilterComparisionOperator)val 
						? val : column.Layout.GetAssociatedFilterComparisionOperator( column.FilterOperatorDefaultValueResolved );
				// SSP 10/23/07 BR25866
				// If the column filter or filter condition was created by the user and populated 
				// inside the filter drop-down from BeforeFilterDropDown event then we need to use
				// whatever display text was assigned to the associated value list item.
				// 
				else if ( ! string.IsNullOrEmpty( cf.displayTextOverride ) )
					val = FilterComparisionOperator.Custom;
				else if ( 1 == filterConditions.Count )
					val = filterConditions[0].ComparisionOperator;
				else // > 1
					val = FilterComparisionOperator.Custom;
			}

			return val;
		}

		#endregion // GetOperatorValue

		#region SetOperatorValue

		internal void SetOperatorValue( UltraGridColumn column, object val )
		{
			ColumnFilter columnFilter = this.GetColumnFilter( column );
			columnFilter.filterRowOperatorValue = val;
			columnFilter.filterOperatorModifiedSinceLastApply = true;
			
			// Set DataChanged on the cell. It will also set the row's DataChanged.
			//
			this.Cells[ column ].SetDataChanged( true );

			this.InvalidateCellAllRegions( column, true, true );

			// If the operator editor is in edit mode then update it's value as well.
			//
			UltraGridCell filterCell = this.GetCellIfAllocated( column );
			if ( null != filterCell && filterCell.IsInEditMode && column.FilterOperatorEditor.IsInEditMode )
				column.FilterOperatorEditor.Value = val;
		}

		#endregion // SetOperatorValue

		#region GetIdealClearButtonSize

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal Size GetIdealClearButtonSize( )
		internal static Size GetIdealClearButtonSize()
		{
			int w = Math.Max( SystemInformation.VerticalScrollBarWidth, FilterClearButtonUIElement.DEFAULT_FILTER_CLEAR_BUTTON_SIZE );

			return new Size( w, w );
		}

		#endregion // GetIdealClearButtonSize

		#region GetIdealOperatorButtonSize

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal Size GetIdealOperatorButtonSize( )
		internal static Size GetIdealOperatorButtonSize()
		{
			int w = Math.Max( SystemInformation.VerticalScrollBarWidth, FilterOperatorUIElement.DEFAULT_FILTER_OPERATOR_BUTTON_SIZE );

			return new Size( w, w );
		}

		#endregion // GetIdealOperatorButtonSize
		
		#region GetIdealOpearatorCellHeight

		internal int GetIdealOpearatorCellHeight( UltraGridColumn column )
		{
			int fontHeight = this.BandInternal.GetBLOverrideAppearanceFontHeight( UltraGridOverride.OverrideAppearanceIndex.FilterOperatorAppearance 
				// SSP 3/13/06 - App Styling
				// Added app-styling related params.
				// 
				, StyleUtils.Role.FilterOperator, AppStyling.RoleState.Normal );

			// Max with DEFAULT_FILTER_OPERATOR_BUTTON_SIZE so the operator icons don't have to scale.
			//
			return Math.Max( FilterOperatorUIElement.DEFAULT_FILTER_OPERATOR_BUTTON_SIZE, fontHeight + 2 * this.BandInternal.CellPaddingResolved );
		}

		#endregion // GetIdealOpearatorCellHeight

		#region GetIdealOperandCellHeight

		internal int GetIdealOperandCellHeight( UltraGridColumn column )
		{
			int fontHeight = Math.Max(
				// SSP 3/13/06
				// Seems like this was a typo. It should've been FilterRowAppearance instead of FilterOperatorAppearance.
				// 
				//this.BandInternal.GetBLOverrideAppearanceFontHeight( UltraGridOverride.OverrideAppearanceIndex.FilterOperatorAppearance ),
				this.BandInternal.GetBLOverrideAppearanceFontHeight( UltraGridOverride.OverrideAppearanceIndex.FilterRowAppearance 
					// SSP 3/13/06 - App Styling
					// Added app-styling related params.
					// 
					, StyleUtils.Role.Row, AppStyling.RoleState.FilterRow ),

				this.BandInternal.GetBLOverrideAppearanceFontHeight( UltraGridOverride.OverrideAppearanceIndex.FilterRowAppearanceActive
					// SSP 3/13/06 - App Styling
					// Added app-styling related params.
					// 
					, StyleUtils.Role.Row, AppStyling.RoleState.HasActiveFilters )
				);

			return fontHeight + 2 * this.BandInternal.CellPaddingResolved;
		}

		#endregion // GetIdealOperandCellHeight

		#region GetIdealFilterCellHeight

		internal int GetIdealFilterCellHeight( UltraGridColumn column )
		{
			UltraGridBand band = this.BandInternal;
			int height = this.GetIdealOperandCellHeight( column );

			// Max with the clear button height since the clear button is positioned inside
			// the operand cell (not the entire filter cell).
			// 
			if ( column.FilterClearButtonVisibleResolved )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//height = Math.Max( height, this.GetIdealClearButtonSize().Height );
				height = Math.Max( height, UltraGridFilterRow.GetIdealClearButtonSize().Height );
			}

			// Add or max with the operator cell depending upon whether the operator is 
			// positioned above the filter operand cell or next to it.
			// 
			FilterOperatorLocation operatorLocation = column.FilterOperatorLocationResolved;
			if ( FilterOperatorLocation.AboveOperand == operatorLocation )
				height += this.GetIdealOpearatorCellHeight( column );
			else if ( FilterOperatorLocation.WithOperand == operatorLocation )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//height = Math.Max( height, this.GetIdealOperatorButtonSize( ).Height );
				height = Math.Max( height, UltraGridFilterRow.GetIdealOperatorButtonSize().Height );
			}
            
			// SSP 6/8/05 BR04481
			// Account for cell spacing and cell border style.
			// 
			int cellSpacing = band.CellSpacingResolved;
			height += 2 * cellSpacing;

			UIElementBorderStyle cellBorderStyle = this.BorderStyleCellResolved;
			if ( cellSpacing > 0 || ! band.Layout.CanMergeAdjacentBorders( cellBorderStyle ) )
				height += 2 * band.Layout.GetBorderThickness( cellBorderStyle );

			return height;
		}

		#endregion // GetIdealFilterCellHeight

		#region FireFilterCellValueChangedHelper

		// SSP 5/27/05 BR04286
		// Added FireFilterCellValueChangedHelper method.
		// 
		internal bool FireFilterCellValueChangedHelper( UltraGridColumn column, bool applyNewFilters )
		{
			UltraGridBase grid = this.Layout.Grid;
			if ( null != grid )
			{
				FilterCellValueChangedEventArgs filterCellValueChangedEventArgs = 
					new FilterCellValueChangedEventArgs( (UltraGridFilterCell)this.Cells[ column ], applyNewFilters );

				grid.FireCommonEvent( CommonEventIds.FilterCellValueChanged, filterCellValueChangedEventArgs, true );
				applyNewFilters = filterCellValueChangedEventArgs.ApplyNewFilter;
			}

			return applyNewFilters;
		}

		#endregion // FireFilterCellValueChangedHelper

		#region OnFilterClearButtonClicked

		internal void ClearFilterButtonClickedHelper( UltraGridColumn column, bool applyNewFilters )
		{
			object origVal = this.GetCellValue( column );

			UltraGridCell filterCell = this.GetCellIfAllocated( column );
			bool filterCellInEditMode = null != filterCell && filterCell.IsInEditMode;

			this.SetCellValue( column, null );			

			if ( null != origVal && DBNull.Value != origVal && ! filterCellInEditMode )
			{
				applyNewFilters = this.FireFilterCellValueChangedHelper( column, applyNewFilters );

				if ( applyNewFilters )
					this.ApplyFilters( column, false );
			}
		}
		
		internal void OnFilterClearButtonClicked( UltraGridColumn column )
		{
			if ( null != column )
			{
				// If some cell other than the filter cell associated with the column is in
				// edit mode then exit the edit mode on that cell before taking any action.
				//
				UltraGridCell filterCell = this.Cells[ column ];
				bool filterCellInEditMode = null != filterCell && filterCell.IsInEditMode;
				if ( filterCellInEditMode || this.Layout.ExitEditModeHelper( ) )
				{
					// SSP 6/8/05 BR04488
					// Enter the filter cell into edit mode if not alread in edit mode when 
					// the filter clear button in the cell is clicked.
					// 
					// ------------------------------------------------------------------------
					//this.ClearFilterButtonClickedHelper( column, FilterEvaluationTrigger.OnCellValueChange == column.FilterEvaluationTriggerResolved );

					FilterEvaluationTrigger trigger = column.FilterEvaluationTriggerResolved;
					bool apply = FilterEvaluationTrigger.OnCellValueChange == trigger
						|| FilterEvaluationTrigger.OnEnterKeyOrLeaveCell == trigger;

					this.ClearFilterButtonClickedHelper( column, apply );

					if ( ! filterCellInEditMode && null != filterCell 
						 && filterCell.CanEnterEditMode && filterCell.CanBeModified )
					{
						filterCell.Activate( );
						if ( filterCell.IsActiveCell )
							filterCell.EnterEditMode( );
					}
					// ------------------------------------------------------------------------
				}
			}
			else
			{
				if ( this.Layout.ExitEditModeHelper( ) )
				{
					ColumnFiltersCollection columnFilters = this.ColumnFilters;
					for ( int i = 0; i < columnFilters.Count; i++ )
					{
						ColumnFilter cf = columnFilters.HasColumnFilter( i ) ? columnFilters[ i ] : null;
						if ( null != cf )
							this.ClearFilterButtonClickedHelper( cf.Column, false );
					}

					// Apply the new filters.
					//
					this.ApplyFilters( );
				}
			}
		}

		#endregion // OnFilterClearButtonClicked

		#region GetFilterCellTooltip

		internal string GetCellTooltip( UltraGridColumn column )
		{
			object operandVal = this.GetCellValue( column );

			// SSP 10/23/07 BR25866
			// 
			if ( operandVal is ColumnFilter )
			{
				ColumnFilter tmpCF = operandVal as ColumnFilter;
				if ( !string.IsNullOrEmpty( tmpCF.displayTextOverride ) )
					return tmpCF.displayTextOverride;
			}

			if ( null != operandVal && DBNull.Value != operandVal )
			{
				// If the user has selected some custom filter conditions (possibly via the custom 
				// filter dialog) then call ToString on the ColumnFilter. We can't do this always
				// because if the filter hasn't been applied then we have to wait till it gets
				// applied before getting hold of a ColumnFilter instance (this would be the case
				// when the FilterEvaluationTrigger is note OnCellValueChange).
				//
				if ( null != operandVal && operandVal.ToString( ) == UltraGridBand.FilterCustomText )
					return this.GetColumnFilter( column ).ToString( );

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//FilterComparisionOperator operatorVal = this.GetComparisionOperatorFromOperatorValue( column, this.GetOperatorValue( column ) );
				FilterComparisionOperator operatorVal = UltraGridFilterRow.GetComparisionOperatorFromOperatorValue( column, this.GetOperatorValue( column ) );

				StringBuilder sb = new StringBuilder( );
				FilterCondition.ToString( column, sb, false, operatorVal , operandVal );
				return sb.ToString( );
			}

			return null;
		}

		#endregion // GetFilterCellTooltip

		#region HasActiveFilters

		internal bool HasActiveFilters( UltraGridColumn column )
		{
			ColumnFilter cf = this.GetColumnFilterIfAllocated( column );
			return null != cf && cf.HasFilterConditions;
		}

		#endregion // HasActiveFilters

		#region IsFilterCellInputValid

		internal bool IsFilterCellInputValid( UltraGridColumn column, object operandVal, out string errorMessage )
		{
			errorMessage = null;

			object operatorObj = this.GetOperatorValue( column );
			if ( operatorObj is FilterComparisionOperator )
			{
				FilterComparisionOperator comparisonOperator = (FilterComparisionOperator)operatorObj;

				// SSP 8/31/05 BR06017
				// ValidateSearchPattern validates for other operators as well.
				// 
				//if ( FilterComparisionOperator.Match == comparisonOperator )
				//{
					try
					{
						FilterCondition.ValidateSearchPattern( comparisonOperator, operandVal );
					}
					catch ( Exception e )
					{
						errorMessage = e.Message;
						return false;
					}
				//}
			}

			return true;
		}

		#endregion // IsFilterCellInputValid

		#region RowSelectorHasClearButton

		internal bool RowSelectorHasClearButton
		{
			get
			{
				FilterClearButtonLocation loc = this.BandInternal.FilterClearButtonLocationResolved;
				return FilterClearButtonLocation.Row == loc || FilterClearButtonLocation.RowAndCell == loc;
			}
		}

		#endregion // RowSelectorHasClearButton

		#region Base Overrides

		#region GetResolvedCellActivation
		
		internal override Activation GetResolvedCellActivation( UltraGridColumn column )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//return this.IsOperandEditable( column ) ? Activation.AllowEdit : Activation.Disabled;
			return UltraGridFilterRow.IsOperandEditable( column ) ? Activation.AllowEdit : Activation.Disabled;
		}

		#endregion // GetResolvedCellActivation

		#region GetCellValue

		/// <summary>
		/// Overridden. Returns the filter operand value.
		/// </summary>
        /// <param name="column">The column intersecting the row from which the value will be retrieved.</param>
        /// <returns>The filter operand value.</returns>
		public override object GetCellValue( Infragistics.Win.UltraWinGrid.UltraGridColumn column )
		{
			ColumnFilter cf = this.GetColumnFilterIfAllocated( column );
			object val = null != cf ? cf.filterRowOperandValue : null;
			if ( null == val && ( null == cf || ! cf.filterOperandModifiedSinceLastApply ) )
			{
				FilterConditionsCollection filterConditions = null != cf ? cf.FilterConditions : null;
				if ( null == filterConditions || 0 == filterConditions.Count )
					val = DBNull.Value;
				// SSP 10/23/07 BR25866
				// If the column filter or filter condition was created by the user and populated 
				// inside the filter drop-down from BeforeFilterDropDown event then we need to use
				// whatever display text was assigned to the associated value list item.
				// 
				else if ( !string.IsNullOrEmpty( cf.displayTextOverride ) )
					val = cf;
				else if ( 1 == filterConditions.Count )
					val = filterConditions[0].CompareValue;
				else // > 1
					val = UltraGridBand.FilterCustomText;
			}

			return val;
		}

		#endregion // GetCellValue

		#region SetCellValue

		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// Changed the return type from void to bool. We need to know if the action was canceled or not.
		// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
		// 
		//internal override void SetCellValue( 
		internal override bool SetCellValue( 
			Infragistics.Win.UltraWinGrid.UltraGridColumn column, 
			object val, bool suppressErrorMessagePrompt,
			// SSP 2/21/06 BR10249
			// Added an overload that takes in throwExceptionOnError parameter. Pass that along.
			// 
			bool throwExceptionOnError )
		{
			ColumnFilter columnFilter = this.GetColumnFilter( column );
			columnFilter.filterRowOperandValue = val;
			columnFilter.filterOperandModifiedSinceLastApply = true;

			// Set DataChanged on the cell. It will also set the row's DataChanged.
			//
			this.Cells[ column ].SetDataChanged( true );

			this.InvalidateCellAllRegions( column, true, true );

			// If the operand editor is in edit mode then update it's value as well.
			//
			UltraGridCell filterCell = this.GetCellIfAllocated( column );
			if ( null != filterCell && ! filterCell.InCommitEditValue &&
				filterCell.IsInEditMode && column.FilterOperandEditor.IsInEditMode )
				column.FilterOperandEditor.Value = val;

			// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
			// Changed the return type from void to bool. We need to know if the action was canceled or not.
			// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
			// 
			return true;
		}

		#endregion // SetCellValue

		#region BorderStyleResolved

		internal override UIElementBorderStyle BorderStyleResolved
		{
			get
			{
				return this.BandInternal.BorderStyleFilterRowResolved;
			}
		}

		#endregion // BorderStyleResolved

		#region BorderStyleCellResolved


		/// <summary>
		/// Gets the resolved border style of the cells in this row.
		/// </summary>
        // MRS v7.2 - Document Exporter
		//internal override UIElementBorderStyle BorderStyleCellResolved
        public override UIElementBorderStyle BorderStyleCellResolved
		{
			get
			{
				return this.BandInternal.BorderStyleFilterCellResolved;
			}
		}

		#endregion // BorderStyleCellResolved

		#region AutoPreviewHeight 

		/// <summary>
		/// Overridden. Always returns 0 as the filter row never displays an auto-preview.
		/// </summary>
		public override int AutoPreviewHeight 
		{
			get
			{
				// No auto previews for filter row.
				//
				return 0;
			}
		}

		#endregion // AutoPreviewHeight 

		#region IsFilterRow
		
		/// <summary>
		/// Overridden. Returns true since this is the filter row.
		/// </summary>
		public override bool IsFilterRow
		{
			get
			{
				return true;
			}
		}

		#endregion // IsFilterRow

		#region CreateCell
		
		internal override UltraGridCell CreateCell( UltraGridColumn column )
		{
			return new UltraGridFilterCell( this.Cells, column );
		}

		#endregion // CreateCell

		#region IsUIElementCompatible
		
		internal override bool IsUIElementCompatible( UIElement parent, UIElement elem )
		{
			return elem is FilterRowUIElement;
		}

		#endregion // IsUIElementCompatible

		#region HasSameContext

		internal override bool HasSameContext( UIElement parent, UIElement elem )
		{
			FilterRowUIElement filterRowElem = elem as FilterRowUIElement;
			return null != filterRowElem && this == filterRowElem.contextRow;
		}

		#endregion // HasSameContext

		#region RowSelectorExtentResolved

		internal override int RowSelectorExtentResolved
		{
			get
			{
				int extent = this.BandInternal.RowSelectorExtent;
				Rectangle rect = Rectangle.Empty;
				this.AdjustUIElementRect( ref rect );
				extent += rect.Width;
				return extent;
			}
		}

		#endregion // RowSelectorExtentResolved

		#region AdjustUIElementRect
		
		internal override void AdjustUIElementRect( ref Rectangle elemRect )
		{
			if ( this.ParentCollection.IsGroupByRows && ! this.Layout.ViewStyleImpl.IsMultiBandDisplay )
			{
				int indent = this.BandInternal.CalcGroupByRowIndent( this.ParentCollection );
				elemRect.X += indent;
				elemRect.Width -= indent;
			}
		}

		#endregion // AdjustUIElementRect

		#region CreateRowUIElement
		
		internal override UIElement CreateRowUIElement( UIElement parent )
		{
			return new FilterRowUIElement( parent );
		}

		#endregion // CreateRowUIElement

		#region InitializeUIElement

		internal override void InitializeUIElement( UIElement elem )
		{
			FilterRowUIElement filterRowElem = (FilterRowUIElement)elem;
			filterRowElem.InitializeRow( this );
		}

		#endregion // InitializeUIElement

		#region GetEditorOwnerInfo

		internal override GridEmbeddableEditorOwnerInfoBase GetEditorOwnerInfo( UltraGridColumn column )
		{
			return column.FilterOperandEditorOwnerInfo;
		}

		#endregion // GetEditorOwnerInfo

		#region GetEditorResolved

		/// <summary>
		/// Gets the resolved editor for the cell associated with this row and the specified column.
		/// </summary>
        /// <param name="column">The column intersecting the row from which the value will be retrieved.</param>
        /// <returns>The resolved editor for the cell associated with this row and the specified column.</returns>
        // MRS v7.2 - Document Exporter
		//internal override EmbeddableEditorBase GetEditorResolved( UltraGridColumn column )
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public override EmbeddableEditorBase GetEditorResolved(UltraGridColumn column)
		{
			UltraGridCell cell = this.GetCellIfAllocated( column );

			if ( null != cell )
				return cell.EditorResolved;
			else 
				return column.FilterOperandEditor;
		}

		#endregion // GetEditorResolved

		#region GetValueListResolved
		
		internal override IValueList GetValueListResolved( UltraGridColumn column )
		{
			UltraGridCell cell = this.GetCellIfAllocated( column );

			if ( null != cell && null != cell.ValueList )
				return cell.ValueList;
			else if ( FilterOperandStyle.UseColumnEditor == column.FilterOperandStyleResolved )
				return column.ValueList;
			else
			{
				UltraGridBase grid = this.Layout.Grid;
				if ( null != grid )
					return grid.FilterDropDown;
			}

			return null;
		}

		#endregion // GetValueListResolved

		#region OnActiveStateChanged

		internal override void OnActiveStateChanged( bool activated )
		{
			base.OnActiveStateChanged( activated );

			if ( ! activated && ! this.Disposed )
			{
				foreach ( UltraGridColumn column in this.Band.Columns )
				{
					FilterEvaluationTrigger trigger = column.FilterEvaluationTriggerResolved;
					if ( FilterEvaluationTrigger.OnLeaveRow == trigger
						|| FilterEvaluationTrigger.OnEnterKeyOrLeaveRow == trigger )
						this.ApplyFilters( );
				}
			}
		}

		#endregion // OnActiveStateChanged

		#region FixedResolvedLocation

		internal override FixedRowStyle FixedResolvedLocation
		{
			get
			{
				return this.ParentCollection.SupportsFixedRows ? FixedRowStyle.Top : FixedRowStyle.Default;
			}
		}

		#endregion // FixedResolvedLocation

		#region HasSpecialRowSeparatorBefore

		internal override bool HasSpecialRowSeparatorBefore
		{
			get
			{
				// Filter row is never displayed on the bottom so it never has a separator before.
				//
				return false;
			}
		}

		#endregion // HasSpecialRowSeparatorBefore

		#region HasSpecialRowSeparatorAfter

		internal override bool HasSpecialRowSeparatorAfter
		{
			get
			{
				return 0 != ( SpecialRowSeparator.FilterRow & this.BandInternal.GetSpecialRowSeparatorResolved( this ) );
			}
		}

		#endregion // HasSpecialRowSeparatorAfter
		
		#region RowSpacingBeforeResolvedBase

		internal override int RowSpacingBeforeResolvedBase
		{
			get
			{
				int ret = this.RowSpacingBefore;
				if ( ret < 0 )
					ret = this.BandInternal.FilterRowSpacingBeforeDefault;

				return ret;
			}
		}

		#endregion // RowSpacingBeforeResolvedBase

		#region RowSpacingAfterResolvedBase

		internal override int RowSpacingAfterResolvedBase
		{
			get
			{
				int ret = this.RowSpacingAfter;
				if ( ret < 0 )
					ret = this.BandInternal.FilterRowSpacingAfterDefault;

				return ret;
			}
		}

		#endregion // RowSpacingAfterResolvedBase

		#region Hidden

		/// <summary>
		/// Overridden. Determines whether the row will be displayed. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Hidden</b> property determines whether an object is visible. Hiding an object may have have effects that go beyond simply removing it from view. For example, hiding a band also hides all the rows in that band. Also, changing the <b>Hidden</b> property of an object affects all instances of that object. For example, a hidden column or row is hidden in all scrolling regions.</p>
		/// </remarks>
		public override bool Hidden
		{
			get
			{
				if ( base.Hidden )
					return true;

                // MRS 6/3/2008 - BR33595
                // If the FilterUIType is no FilterRow, the filter row will always be 
                // considered hidden. 
                if (this.BandInternal.FilterUITypeResolved != FilterUIType.FilterRow)
                    return true;

				// SSP 3/31/06 BR11014
				// Moved the following code into the new AllColumnsDisallowFilteringHelper method.
				// 
				// --------------------------------------------------------------------------------
				return UltraGridFilterRow.AllColumnsDisallowFilteringHelper( this.BandInternal );
				
				// --------------------------------------------------------------------------------
			}
		}

		#region GetHiddenHelper

		// SSP 3/31/06 BR11014
		// 
		internal static bool GetHiddenHelper( UltraGridFilterRow filterRow, UltraGridBand band )
		{
			return null != filterRow ? filterRow.Hidden 
				: UltraGridFilterRow.AllColumnsDisallowFilteringHelper( band );
		}

		// SSP 3/31/06 BR11014
		// 
		internal static bool AllColumnsDisallowFilteringHelper( UltraGridBand band )
		{
			ColumnsCollection cols = band.Columns;
			int count = cols.Count;
			bool allCellsHidden = count > 0;
			for ( int i = 0; allCellsHidden && i < count; i++ )
			{
				UltraGridColumn col = cols[i];
				allCellsHidden = FilterOperandStyle.None == col.FilterOperandStyleResolved
					// SSP 6/10/05
					// If AllowRowFilteringResolved is false on all columns then hide the filter row as well.
					//
					|| DefaultableBoolean.False == col.AllowRowFilteringResolved;
			}

			return allCellsHidden;
		}

		#endregion // GetHiddenHelper

		#endregion // Hidden
	
		#region RowPromptResolved

		internal override string RowPromptResolved
		{
			get
			{
				string prompt = this.BandInternal.FilterRowPromptResolved;

				// Once filter row has some active filters then don't displaye the prompt.
				//
				return null == prompt || 0 == prompt.Length || this.HasFilters ? null : prompt;
			}
		}

		#endregion // RowPromptResolved

		#region IsCellHidden

		// SSP 6/10/05 BR04499
		// In a filter row a cell that's not allocated could be hidden if the FilterOperandStyle
		// of the associated column resolves to None.
		// 
		internal override bool IsCellHidden( UltraGridColumn column )
		{
			UltraGridCell cell = this.GetCellIfAllocated( column );
			return null != cell 
				? cell.Hidden
				: FilterOperandStyle.None == column.FilterOperandStyleResolved;
		}

		#endregion // IsCellHidden
		
		internal override int BaseHeight
		{
			get
			{
				int maxHeight = 0;
				if ( this.BandInternal.UseRowLayoutResolved )
				{
					// Call GetFilterRowLayoutManager to initialize the cachedRowLayoutPreferredHeight.
					//
					// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
					// 
					//this.GetFilterRowLayoutManager( );
					//maxHeight = this.cachedRowLayoutPreferredHeight;
					maxHeight = this.RowAutoSizeLayoutManagerHolder.RowLayoutPreferredSize.Height;
				}
				else
				{
					for ( int i = 0; i < this.BandInternal.Columns.Count; i++ )
						maxHeight = Math.Max( maxHeight, this.GetIdealFilterCellHeight( this.BandInternal.Columns[ i ] ) );
				}

				maxHeight += 2 * this.Layout.GetBorderThickness( this.BorderStyleResolved );

				maxHeight = Math.Max( maxHeight, base.BaseHeight );

				return maxHeight;
			}
		}

		#region LayoutManager
		
		internal override Infragistics.Win.Layout.LayoutManagerBase LayoutManager
		{
			get
			{
				// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// 
				//return this.GetFilterRowLayoutManager( );
				return this.RowAutoSizeLayoutManagerHolder.RowLayoutManager;
			}
		}

		// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
		// 
		

		#endregion // LayoutManager

		#endregion // Base Overrides
	}

	#endregion // UltraGridFilterRow Class

}
