#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    /// This ui element contains the expansion indicator of a non-group-by row.
    /// </summary>
    public class PreRowAreaUIElement : UIElement
    {
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
        public PreRowAreaUIElement( UIElement parent ) : base( parent )
        {
        }

		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		//
		/// <summary>
		/// Initializes the pre row area with the context of a row. Pre row area element 
		/// requires the context of a row. If an ancestor element provides a row context
		/// then initializing the pre row area with a row context is not necessary.
		/// </summary>
		/// <param name="row">The row associated with this element.</param>
		public void Initialize( UltraGridRow row )
		{
			this.PrimaryContext = row;
		}

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
                                                 ref AppearancePropFlags requestedProps )
		{
            // we only need to set the bordercolor
            //
			// SSP 3/16/06
			// Use the new RowConnectorColorResolved property.
			// 
			//appearance.BorderColor = this.Row.Layout.RowConnectorColor;
			appearance.BorderColor = this.Row.Layout.RowConnectorColorResolved;
			
			// SSP 8/23/02 UWG1209
			// Also set the bordercolor3dbase to the border color so if the
			// border style was a 3d border, then it would still draw with
			// the border color rather than basing it on the back color.
			//
			appearance.BorderColor3DBase = appearance.BorderColor;
		}
 
		/// <summary>
		/// Returns the UltraGridRow object associated with the UIElement. This property is not available at design time. This property is read-only at run time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Row</b> property of an object refers to a specific row in the grid as defined by an UltraGridRow object. You use the <b>Row</b> property to access the properties of a specified UltraGridRow object, or to return a reference to the UltraGridRow object that is associated with the current object.</p>
		/// <p class="body">An UltraGridRow object represents a single row in the grid that displays the data from a single record in the underlying data source. The UltraGridRow object is one mechanism used to manage the updating of data records either singly or in batches (the other is the UltraGridCell object). When the user is interacting with the grid, one UltraGridRow object is always the active row, and determines both the input focus of the grid and the position context of the data source to which the grid is bound.</p>
		/// </remarks>
        public Infragistics.Win.UltraWinGrid.UltraGridRow Row
        {
            get
            {
				// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
				// Added Initialize method to this element. Call GetContext on this element rather
				// than the parent.
				//
                //return (Infragistics.Win.UltraWinGrid.UltraGridRow)this.Parent.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ) );
				return (Infragistics.Win.UltraWinGrid.UltraGridRow)this.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ) );
            }
        }

		/// <summary>
		/// Override and return null for the pre row area since we don't want to select
		/// the row when they click here.
		/// </summary>
		public override ISelectableItem SelectableItem
		{
			get
			{
				return null;
			}
		}

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
        protected override void PositionChildElements()
        {
			UltraGridRow row = this.Row;

            // if the row doesn't have an expansion indicator then
            // clear the row and return
            //
            if ( !row.HasExpansionIndicator )
            {
				if ( this.childElementsCollection != null &&
						this.childElementsCollection.Count > 0 )
					this.childElementsCollection.Clear();

				return;
            }

			ExpansionIndicatorUIElement expansionIndicator = null;

            // check if the element is already in our cchild elements
            // collection (without removing it from the collection)
            //
            if ( this.childElementsCollection != null  &&
                 this.childElementsCollection.Count > 0 )
                 expansionIndicator = (ExpansionIndicatorUIElement)PreRowAreaUIElement.ExtractExistingElement( this.childElementsCollection, typeof( ExpansionIndicatorUIElement ), false);

            // if we don't have one already then create it and add
            // it to the childelements collection
            //
            if ( expansionIndicator == null )
            {
                expansionIndicator = new ExpansionIndicatorUIElement( this );
                this.ChildElements.Add( expansionIndicator );
            }

			UltraGridBand band = row.BandInternal;

            // set the expansion indicators rect
            //
			Rectangle thisRect = this.Rect;
            expansionIndicator.Rect = new Rectangle( thisRect.Left
				// SSP 4/17/07 BR21747
				// Added RowConnectorLineOffset. Use that instead of the constant because that takes into
				// account the PreRowAreaExtent property setting.
				// 
				//+ ( UltraGridBand.PRE_ROW_SELECTOR_WIDTH / 2 ) 
				+ band.RowConnectorLineOffset 
				- ( UltraGridBand.EXPANSION_INDICATOR_WIDTH / 2 ),
                                                     thisRect.Top + 1 + ( ( thisRect.Height - UltraGridBand.EXPANSION_INDICATOR_WIDTH ) / 2 ),
                                                     UltraGridBand.EXPANSION_INDICATOR_WIDTH,
                                                     UltraGridBand.EXPANSION_INDICATOR_WIDTH );

        }

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
        protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
        {
            // this element doesn't draw a background
        }

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
		}
 
        /// <summary>
        /// Draws the row connector horz line. 
        /// </summary>
        protected override void DrawForeground ( ref UIElementDrawParams drawParams )
        {
			
			Infragistics.Win.UltraWinGrid.UltraGridRow row = this.Row;


			// SSP 10/1/01
			// if the parent row is a group by row then don't draw any row
			// connector lines since they will cross the group by connectors
			//
			// MD 7/27/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( row.ParentRow is UltraGridGroupByRow  )
			UltraGridRow parentRow = row.ParentRow;

			if ( parentRow is UltraGridGroupByRow )
			{
				if (  !row.HasExpansionIndicator 
					// SSP 12/11/01
					// Also check for never, otherwise we could have a row that does not have
					// any child rows (so that it does not show any expansion indicator ), 
					// but the other rows in the same rows collection are displaying the expansion
					// indicator because band's setting is not never, so check for that.
					//
					&& 
					 (
						ShowExpansionIndicator.Never == row.BandInternal.ExpansionIndicatorResolved
					    ||
						// SSP 11/11/03 Add Row Feature
						//
					    // ------------------------------------------------------------------------
						//( !row.HasNextSibling( false, true ) &&
						//  !row.HasPrevSibling( false, true ) ) 
						( !row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) &&
						  !row.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) ) 
						// ------------------------------------------------------------------------
					 )
					)
					return;
			}

            // If this element's rect isn't in the invalid area then we can
            // return
            //
            if ( !drawParams.InvalidRect.IntersectsWith( this.rectValue ) )
                return;

            Rectangle rcWork = this.rectValue;

            int startX;

            // JJD 10/04/00 - ult1898
            // Tweek calculation to align center if top + bottom is odd number
            //
            int centerY = (rcWork.Top + rcWork.Bottom + 1) / 2;



            // SSP 11/15/01
            // Use the same logic as in RowColIntersectionUIElement to decide whether
			// to draw the horizontal line or not.
			//
			//----------------------------------------------------------------------------
			Infragistics.Win.UltraWinGrid.UltraGridBand band = row.BandInternal;
			UltraGridLayout layout = band.Layout;

			bool        multipleBandsDispayed = layout.ViewStyleImpl.IsMultiBandDisplay;
			

			if ( !multipleBandsDispayed )
			{
				if ( //RobA 11/6/01 use the new enum						
					//band_0.ExpansionIndicatorResolved == DefaultableBoolean.True )
					band.ExpansionIndicatorResolved == ShowExpansionIndicator.Always )
					multipleBandsDispayed = true;
			}

			if ( ! ( layout.Grid is UltraGrid ) )
				multipleBandsDispayed = false;

			if ( multipleBandsDispayed )
			{
				if ( band.ParentBand == null &&
					!band.HasChildBands() )
				{
					// JJD 9/26/00
					// Only reset the multi band flag if the ExpansionIndicator
					// property is NOT set to 'show'
					//
					//if ( band.ExpansionIndicatorResolved != DefaultableBoolean.True )
					//RobA 11/6/01 use the new enum						
					if ( band.ExpansionIndicatorResolved == ShowExpansionIndicator.Never ||
						// SSP 11/9/01 UWG705
						// Check for Default as well.
						//
						band.ExpansionIndicatorResolved == ShowExpansionIndicator.Default )
						multipleBandsDispayed = false;
				}
			}

			

			// SSP 11/15/01 
			// Replaced below code with above
			
			if ( !multipleBandsDispayed )
				return;

			//----------------------------------------------------------------------------


            if ( layout.ViewStyleImpl.HasMultiRowTiers &&   
				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
                 //row.ParentRow != null                          && 
				 parentRow != null && 
				// SSP 11/11/03 Add Row Feature
				//
                //!row.HasPrevSibling(false, true)                    && 
				!row.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows )   && 
                !row.HasSiblingInPrevSiblingBand(true) )
                startX = rcWork.Left;
            else
            // JJD 10/26/00 - ult2124
            // Only draw the horizontal connector line if there
            // is at least one other visible row.
            //
			// MD 7/27/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
            //if ( row.ParentRow != null                   || 
			if ( parentRow != null || 
				 // SSP 11/11/03 Add Row Feature
				 //
				 // ----------------------------------------------------------------------------
                 //row.HasNextSibling(false, true)         || 
                 //row.HasPrevSibling(false, true)         || 
				 row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows )         || 
				 row.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows )         || 
				 // ----------------------------------------------------------------------------
                 row.HasSiblingInNextSiblingBand(true)   ||
                 row.HasSiblingInPrevSiblingBand(true)   ||
				 // SSP 11/11/03 Add Row Feature
				 //
                 //row.HasChild(true) )
				 row.HasChild( true, IncludeRowTypes.SpecialRows ) )
 
        //	BF 2.26.01	ULT1024/1379
        //	drawing logic for the horizontal row connectors
        //
        //	Before we had the Indentation property, we positioned the row connector
        //	in the center of the band's pre-row area. We don't want to do that anymore, because
        //	then the expansion indicators and row connectors will not be aligned with respect to the parent band.

        //		startX = ( (this.rectValue.left + this.rectValue.right) / 2 ) + 1;
				// SSP 4/17/07 BR21747
				// Added RowConnectorLineOffset. Use that instead of the constant because that takes into
				// account the PreRowAreaExtent property setting.
				// 
                //startX = rcWork.Left + ( UltraGridBand.PRE_ROW_SELECTOR_WIDTH / 2 ) + 1;
				startX = rcWork.Left + band.RowConnectorLineOffset + 1;
            else
                return;

			// SSP 4/25/05 - NAS 5.2 Filter Row
			// Now a filter row can be displayed in a group-by row collection. If that's the
			// case then that row should not draw any row connectors.
			//
			if ( row.ParentCollection.IsGroupByRows )
				return;

	        //	BF 11.6.00	ULT1496
	        //	The horizontal row connector line needs to start
	        //	at the right edge of the expansion indicator, when there is one
            //	rcWork.left     = startX;
	        if(row.HasExpansionIndicator )
	        {
		        //	BF 2.26.01	The fix for ULT1496 neccesitates this conditional...
		        
		        //	from the right edge of the previous band to the left edge of the +/-,
		        //	then from the right edge of the +/- to the left edge of the current band
		        //	so that the horz line is not drawn over the +/-
		        //
		        //	This requires different drawing based on whether this is the first row in the band
		        //
		        if( layout.ViewStyleBand == ViewStyleBand.Vertical )
			        rcWork.X     = startX + (UltraGridBand.EXPANSION_INDICATOR_WIDTH / 2);
		        else
			        rcWork.X     = startX;
	        }
	        else
	        {
		        rcWork.X         = startX;
	        }

            //rcWork.left     = startX;
            rcWork.Width    = 1 + this.Rect.Right - rcWork.Left;
            rcWork.Y        = centerY;
            rcWork.Height   = 1; 


            Border3DSide  sides   = Border3DSide.Top;

            UIElementBorderStyle eStyle = layout.RowConnectorBorderStyle;

            switch ( eStyle )
            {
            case UIElementBorderStyle.Dotted:
            case UIElementBorderStyle.Dashed:
                {
                    int penOriginAdjustment = Math.Abs( ( (ColScrollRegion)this.Parent.GetContext( typeof( ColScrollRegion ) ) ).PenOrigin );

                    // adjust the top and height of the rect based on the pen origin
                    //
                    rcWork.X        += penOriginAdjustment;
                    rcWork.Width    -= penOriginAdjustment;
                    break;
                }
            
            case UIElementBorderStyle.InsetSoft:
                rcWork.Height++;
                sides   |= Border3DSide.Bottom;
                break;

            case UIElementBorderStyle.RaisedSoft:
                rcWork.Y--;
                rcWork.Height++;
                sides   |= Border3DSide.Bottom;
                break;

            }

            drawParams.DrawBorders( eStyle, 
                                    sides,
                                    drawParams.AppearanceData.BorderColor,
                                    rcWork,
                                    drawParams.InvalidRect );
        }

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return UIElementBorderStyle.None;
			}
		}

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return 0;
			}
		}
    }
}
