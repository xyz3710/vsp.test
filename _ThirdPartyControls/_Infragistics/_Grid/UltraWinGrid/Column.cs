#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Collections;
	using System.Globalization;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.ComponentModel.Design.Serialization;
	using System.Reflection;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using Infragistics.Win.UltraWinMaskedEdit;
	using System.Drawing.Design;
	using System.Windows.Forms.Design;
	using System.Text;
	using System.Security.Permissions;

	using Infragistics.Shared.Serialization;

	// SSP 1/30/03 - Row Layout Functionality
	// 
	using Infragistics.Win.Layout;

	// MRS 3/5/04
	// Added this one so we can derive from DataTypeUITypeEditor, which I moved to Win
	using Infragistics.Win.Design;

	// SSP 6/29/04 - UltraCalc
	//
	using Infragistics.Win.CalcEngine;
	using System.Collections.Generic;

	/// <summary>
	/// An object that represents a single column of data.
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>Column</b> property of an object refers to a specific column in the grid as defined by an UltraGridColumn object. You use the <b>Column</b> property to access the properties of a specified UltraGridColumn object, or to return a reference to an UltraGridColumn object.</p>
	/// <p class="body">An UltraGridColumn object represents a single column in the grid. The UltraGridColumn object is closely linked with a single underlying data field that is used to supply the data for all the cells in the column (except in the case of unbound columns, which have no underlying data field). The UltraGridColumn object determines what type of interface (edit, dropdown list, calendar, etc.) will be used for individual cells, as well as controlling certain formatting and behavior-related settings, such as data masking, for the cells that make up the column.</p>
	/// <seealso cref="UltraGridBand.Columns"/>
	/// <seealso cref="ColumnsCollection"/>
	/// <seealso cref="ColumnsCollection.Add()"/>
	/// <seealso cref="UltraGridLayout.Bands"/>
	/// </remarks>
	[ TypeConverter( typeof( UltraGridColumn.UltraGridColumnTypeConverter ) ) ]
	[ Serializable()  ]
	[ DefaultProperty("Key") ]
	// SSP 4/1/02
	// Added IEmbeddableEditorOwner interface
	//
	// SSP 8/19/02
	// Added IValueListsCollectionProvider to provide the ValueListEditor with control and value lists collection so
	// column's ValueList property shows a list of value lists to choose from during design time.
	//
	public class UltraGridColumn : KeyedSubObjectBase, ISerializable, IValueListsCollectionProvider, 
		// SSP 4/26/02
		// EM Embeddable editors. Column does not have to implement the IValueListOwner
		// anymore since the embeddable editors are going to take care of value list stuff.
		//
		// , IValueListOwner
		// SSP 1/31/03 - Row Layout Functionality
		// Column and header will be implementing the ILayoutItem and IGridBagConstraint interfaces.
		//
        // MRS - NAS 9.1 - Groups in RowLayout
		//Infragistics.Win.Layout.ILayoutItem,		
        Infragistics.Win.Layout.ILayoutChildItem,		
		Infragistics.Win.Layout.IGridBagConstraint
		// SSP 5/3/04 - Designer Enhancements
		// Added mask input standard values. To do that we need to implement IEditType
		// interface and add a MaskTypeConverter attribute to the mask properties.
		// 
		, IEditType
		// SSP 6/29/04 - UltraCalc
		// Added IFormatProvider.
		//
        // MBS 12/21/06 - Conditional Formatting
        // Replaced with new derived interface
		//, Infragistics.Win.CalcEngine.IFormulaProvider
        , Infragistics.Win.CalcEngine.IFormulaProviderEx
        // MRS - NAS 9.1 - Groups in RowLayout
        , IProvideRowLayoutColumnInfo

	{
		private PropertyDescriptor	propertyDescriptor = null;
		private int					index = 0;
		internal int				width = 0; 
		internal bool               hasWidthSet = false;
		private int					level = 0; 
		private IComparer			comparer = null;
		private IGroupByEvaluator	groupByEvaluator = null;
		private string				format = null;

		// SSP 11/10/05 BR07680
		// Added RowFilterComparer property.
		// 
		private IComparer			rowFilterComparer = null;

		// JJD 1/09/02 - UWG295, UWG885
		// Added formatinfo property
		//
		private IFormatProvider		formatInfo = null;

				
		//enums
		private Activation			activation = 0;			//Activation.AllowEdit;
		private DefaultableBoolean	autoSizeEdit = DefaultableBoolean.Default;
		private ButtonDisplayStyle	buttonDisplayStyle = 0;	//ButtonDisplayStyle.OnCellActivate;
		
		// MRS 4/7/05 - Obsoleted Case and replaced with CharacterCasing
		//private Case				textcase = 0;			//Case.Unchanged;
		private CharacterCasing		characterCasing = CharacterCasing.Normal;

		private DefaultableBoolean	cellMultiLine = DefaultableBoolean.Default;
		private System.Type			dataType = typeof( System.String );			//DataType.Text;
		private MaskMode			maskClipMode = 0;		//MaskMode.Raw;
		private MaskMode			maskDataMode = 0;		//MaskMode.Raw;
		private	MaskMode			maskDisplayMode = 0;	//MaskMode.Raw;
		private ParsedMask			parsedMask		= null; // cached parsed mask
		private Nullable			nullable = 0;			//Nullable.Null;

		// JJD 1/17/02
		// Added hiddenWhenGroupBy property
		//
		private DefaultableBoolean  hiddenWhenGroupBy = DefaultableBoolean.Default;
		
		private ColumnStyle			style = 0;				//Style.Default;
		private bool				tabStop = true;
		private SortIndicator       tempSortIndicator;
		private SortIndicator		sortIndicator = SortIndicator.None;
		private bool				isGroupByColumn	= false;
		private bool				tempIsGroupByColumn = false;
		
		// SSP 4/29/02
		// EM Embeddable editors.
		// This flag is not used anywhere anymore, so commented it out.
		//
		//private bool				dropdownEventFired = false;

		// SSP 1/16/02 
		// Since we are only looking at the SortDirection, we don't
		// need to have a version. We just verify against the sort direction.
		//
		//private int					groupBySortVersion = 0;
		
		private int					groupByHierarchyVersion = 0;


		//string
		private string				maskInputString;
		
		private DefaultableBoolean	allowGroupBy = DefaultableBoolean.Default;
		
		internal const char			DEFAULT_PROMPT_CHAR		= '_';
		internal const char			DEFAULT_PAD_CHAR		= ' ';
		
		// SSP 8/14/01 
		// PromptChar and PadChar should be char types
		private char				promptChar	= DEFAULT_PROMPT_CHAR;
		private char				padChar		= DEFAULT_PAD_CHAR;

		private int					minWidth=0;
		private int					maxWidth=0;

		//short
		private short				colSpan=1;

		//int

		// JAS 2005 v2 XSD Support
		// fieldLen is now stored in the Constraint.MaxLength property.
//		private int					fieldLen=0;

		private int					widthDuringDrag = 0;
		private int					relativeOrigin = 0;
		private const int			MIN_COL_WIDTH_SETTING = 8;

        //  BF 3/7/08   FR09238 - AutoCompleteMode
		//private bool				autoEdit = false;
        private Infragistics.Win.AutoCompleteMode autoCompleteMode = Infragistics.Win.AutoCompleteMode.Default;

		private bool				proportionalResize = false;
		private bool				lockedWidth = false;

		private bool				vertScrollBar = false;
		private bool				bound = false;

		private bool				firstItemInLevel = false; 
		private bool				lastItemInLevel = false;

		//misc
		private Infragistics.Win.AppearanceHolder	cellAppearanceHolder = null;

		// SSP 12/6/01 UWG546
		//
		private Infragistics.Win.AppearanceHolder cellButtonAppearanceHolder = null;

		// MRS 12/9/04 - BR01204
//		// SSP 11/13/01 UWG360
//		private Infragistics.Win.AppearanceHolder maskListeralsAppearanceHolder = null;
		private Infragistics.Win.AppearanceHolder maskLiteralsAppearanceHolder = null;

		private Infragistics.Win.UltraWinGrid.UltraGridGroup 
			group = null;

		// JAS 2005 v2 XSD Support
		// Made the ValueList property delegate to the Constraint.Enumeration property.
		//
//		private IValueList			valuelist = null;
		private Infragistics.Win.UltraWinGrid.UltraGridBand	band;
        
		private bool				hidden = false;

		private Infragistics.Win.UltraWinGrid.ColumnHeader   header = null;


		
		private Infragistics.Win.UltraWinGrid.ColumnCloneData cloneData;


		//for keeping track of synced list
		private UltraGridColumn prevLinkedColumn = null; 
		private UltraGridColumn nextLinkedColumn = null;
				
		// SSP 4/29/02
		// EM Embeddable editors.
		// isUpdatingTextFromOnSelectionChange is not being used anywhere anymore so commented
		// it out. I think this flag was being used for AutoEdit feature which the embeddable
		// editors take care of now.
		//// JJD 1/28/03 - UWG1003 added flag
		// private bool isUpdatingTextFromOnSelectionChange = false;

		internal bool isSynchronized = false;
		internal int synchronizedWidth = 0;
		//private bool inBeforePosChanged = false;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//private static readonly int MinColWidthSetting = 10;
		private const int MinColWidthSetting = 10;

		// JJD 8/19/02
		// Deprecate minDate and MaxDate (use minValue and maxValue instead. 
//		private System.DateTime minDate;
//		private System.DateTime maxDate;

		// AS - 10/30/01
		private int valueListId = 0;
		private string valueListControlName = null;
		private int unboundRelativePosition = 0;
		private int sortIndex = -1;
		private int groupId = 0;		
		private int indexInGroup = -1;

		// JJD 12/13/01
		// Added caching logic for card label size
		private Size cachedCardLabelSize = Size.Empty;

		// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
		// Added CellSizeDefault. Code in there was copied from the CellWidthDefault. Made the 
		// CellWidthDefault delegate to CellSizeDefault.
		// 
		//private int  cachedCellWidthDefault = 0;
		private Size cachedCellSizeDefault = Size.Empty;

		// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
		// Added cachedCardLabelSize_Prev.
		// 
		private Size cachedCardLabelSize_Prev = Size.Empty;

		// JJD 1/21/02 - UWG815 Moved nulltext from layout to override and column
		//
		private string	nullText = null;

		// SSP 12/11/01
		// We already have a isGropByColumn variable that indicates whether it's
		// a group by or not.
		//
		//private bool isGroupBy = false;

		// SSP 3/21/02
		// Version 2 Row Filter Implementation
		//
		private DefaultableBoolean allowRowFiltering = DefaultableBoolean.Default;
		
		
		// SSP 4/23/02
		// Row summaries.
		//
		private AllowRowSummaries allowRowSummaries = AllowRowSummaries.Default;

		// SSP 4/1/02
		// Adde vars for embeddable editors
		//
		private Infragistics.Win.UltraWinGrid.EmbeddableEditorOwnerInfo editorOwnerInfo = null;
		private EmbeddableEditorBase embeddableEditor = null;
		private Control				 editorControl = null;
		private EmbeddableEditorBase lastEmbeddableEditor = null;
		private EditorWithCombo defaultEditorWithCombo = null;
		private bool defaultEditorDirtyFlag = true;

		// SSP 8/8/05 - NAS 5.3 New Column Styles
		//
		private EmbeddableEditorBase lastCachedStyleEditor = null;
		private ColumnStyle lastCachedStyle = ColumnStyle.Default;

		// SSP 7/17/03 - Cell level Editor/EditorControl/Style properties
		//
		internal int verifyEditorVersionNumber = 0;

		// SSP 6/5/02
		// EM Embeddable editors.
		// Added MinValue and MaxValue properties.
		//
		// JAS 2005 v2 XSD Support
		// These values are now stored in the Constraint property.
		// The 'minValue' object is stored in the MinInclusive property.
		// The 'maxValue' object is stored in the MaxInclusive property.
		//
//		private object minValue = null;
//		private object maxValue = null;

		// SSP 8/20/02
		// For deserializing the EditorControl property. Since it's a control, we have to 
		// serialize it as the control's name so we can find that control when we are
		// deserialized.
		//
		private string editorControlName = null;

		// SSP 10/10/02 UWG1530
		// Added cellChildElementsCacheVersion at column level. This will be incremenet
		// every time this column in selected or deselected.
		//
		private int cellChildElementsCacheVersion = 0;

		// SSP 1/30/03 - Row Layout Functionality
		private RowLayoutColumnInfo rowLayoutColumnInfo = null;

		// SSP 4/14/03
		// Added AutoSizeMode property
		//
		private ColumnAutoSizeMode autoSizeMode;

		// SSP 5/12/03 - Optimizations
		// Added a way to just draw the text without having to embedd an embeddable ui element in
		// cells to speed up rendering.
		//
		private CellDisplayStyle cellDisplayStyle;

		// SSP 8/11/03 - Optimizations
		// In CellUIElement.PositionChildElements we check if the cell value is an Image or not.
		// In most cases we don't need to do that. So added a flag off the column that will 
		// prevent the cell ui element from checking if the cell value is an image for columns
		// that we know won't contain images.
		// Added ShouldCheckForImageCellValue internal property.
		//
		private bool shouldCheckForImageCellValue = true;

		// SSP 8/12/03 - Optimizations
		// Instead of checking if the property descriptor's type is assignable from IList
		// like we do in IsChaptered, cache whether the property descriptor is chaptered or not
		// when the column is initialized with a property descriptor.
		//
		private bool cachedIsChaptered = false;

		// SSP 11/10/03 Add Row Feature
		//
		private object defaultAddRowCellValue;

		// SSP 12/11/03 UWG2781
		// Since the MaskMode enum doesn't have Default as its member, we need to let
		// the user be able to specify that we pickup properties from the owner rather
		// than use the column's settings.
		//
		private bool useEditorMaskSettings = false;

		// SSP 12/12/03 DNF135
		// Added a way to control whether ink buttons get shown.
		// Added ShowInkButton property.
		//
		// SSP 6/29/04
		//
        //private ShowInkButton showInkButton;
		private ShowInkButton showInkButton = ShowInkButton.Default;

		// MRS 9/14/04
		// Added a property to control whether or not the grid displays 
		// a string in a cell while calculating
		//
		// SSP 10/21/04
		// Initialize the var to Default, not False.
		//
		//private DefaultableBoolean showCalculatingText = DefaultableBoolean.False;
		private DefaultableBoolean showCalculatingText = DefaultableBoolean.Default;

		// SSP 6/29/04 - UltraCalc
		//
		private FormulaHolder formulaHolder = null;
		private object formulaErrorValue = null;
		private Infragistics.Win.AppearanceHolder formulaErrorAppearanceHolder = null;
		
		// SSP 10/20/04 UWG3747
		// Added GroupByComparer property to allow the user to sort group-by rows 
		// using a custom comparer.
		//
		private IComparer groupByComparer = null;

		// SSP 11/2/04 UWG3764
		// Added anti-recursion flag for the ValueList property.
		//
		private bool inValueListSet = false;

		// SSP 11/3/04 - Merged Cell Feature
		//
		private AppearanceHolder mergedCellAppearanceHolder = null;
		private MergedCellStyle mergedCellStyle = MergedCellStyle.Default;
		private MergedCellEvaluationType mergedCellEvaluationType = MergedCellEvaluationType.Default;
		private MergedCellContentArea mergedCellContentArea = MergedCellContentArea.Default;
		private IMergedCellEvaluator mergedCellEvaluator = null;

		// SSP 12/14/04 - IDataErrorInfo Support
		//
		private DefaultableBoolean supportDataErrorInfo = DefaultableBoolean.Default;

		// SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		// ----------------------------------------------------------------------------------
		private FilterEvaluationTrigger filterEvaluationTrigger = FilterEvaluationTrigger.Default;
		private FilterOperandStyle filterOperandStyle = FilterOperandStyle.Default;
		private FilterOperatorDefaultValue filterOperatorDefaultValue = FilterOperatorDefaultValue.Default;
		private FilterOperatorDropDownItems filterOperatorDropDownItems = FilterOperatorDropDownItems.Default;
		private FilterOperatorLocation filterOperatorLocation = FilterOperatorLocation.Default;
		private AppearanceHolder filterCellAppearanceHolder = null;
		private AppearanceHolder filterOperatorAppearanceHolder = null;
		private FilterOperandEmbeddableEditorOwnerInfo filterOperandEditorOwnerInfo = null;
		private FilterOperatorEmbeddableEditorOwnerInfo filterOperatorEditorOwnerInfo = null;
		private FilterOperatorDefaultValue cachedDefaultFilterOperatorDefaultValue = FilterOperatorDefaultValue.Default;
		private FilterOperatorDropDownItems cachedDefaultFilterOperatorDropDownItems = FilterOperatorDropDownItems.Default;
		private FilterOperandStyle cachedDefaultFilterOperandStyle = FilterOperandStyle.Default;
		private FilterOperatorLocation cachedDefaultFilterOperatorLocation = FilterOperatorLocation.Default;
		private FilterComparisonType filterComparisonType = FilterComparisonType.Default;
		private DefaultableBoolean filterClearButtonVisible = DefaultableBoolean.Default;
		private int verifiedFilterRowCacheVersion = -1;
		// ----------------------------------------------------------------------------------

		// SSP 1/11/05
		// Added support for defaulting the Caption to the DataColumn's Caption and then
		// to the PropertyDescriptor's DisplayName. Now the order will be 
		// ColumnHeader.Caption, DataColumn.Caption, PropertyDescriptor.DisplayName and
		// then the PropertyDescriptor.Name. Added code to cache the underlying DataColumn 
		// object.
		//
		private PropertyDescriptor cachedDataColumn_lastPropDesc = null;
		private System.Data.DataColumn cachedDataColumn = null;

		// JAS 2005 v2 XSD Support
		//
		private Win.ValueConstraint valueConstraint = null;
		private XsdConstraintFlags xsdSuppliedConstraints = 0;

		// JAS v5.2 Wrapped Header Text 4/18/05
		//
		private int verifiedColumnHeaderHeightVersionNumber = 0;

		// JAS v5.2 GroupBy Break Behavior 5/3/05
		//
		private GroupByMode groupByMode = GroupByMode.Default;

		// JAS v5.2 GroupBy Break Behavior 5/4/05
		//
		private SortComparisonType sortComparisonType = SortComparisonType.Default;

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		// 
		private ExcludeFromColumnChooser excludeFromColumnChooser = ExcludeFromColumnChooser.Default;
		private string columnChooserCaption = null;
		private DefaultableBoolean hiddenWhenGroupByOverride = DefaultableBoolean.Default;

		// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
		//
		private CellClickAction                 cellClickAction;

		// SSP 7/29/05 - NAS 5.3 Tab Index
		// 
		private int tabIndex = -1;

		// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
		// 
		private InvalidValueBehavior invalidValueBehavior = InvalidValueBehavior.Default;

		// SSP 2/17/06 BR08864
		// Added IgnoreMultiCellOperation property on the column.
		// 
		private DefaultableBoolean ignoreMultiCellOperation = DefaultableBoolean.Default;

		// SSP 4/5/06 - NAS 6.2 - Support for New SpellCheck functionality 
		// 
		private Infragistics.Win.UltraWinSpellChecker.IUltraSpellChecker spellChecker = null;

        // MBS 11/15/06
        private IValueAppearance valueBasedAppearance = null;

        // CDS 04/14/08 NA 8.2 ImeMode
        private System.Windows.Forms.ImeMode imeMode = System.Windows.Forms.ImeMode.Inherit;

		// MD 9/23/08 - TFS6601
		private ReserveSortIndicatorSpaceWhenAutoSizing reserveSortIndicatorSpaceWhenAutoSizing;

        // MRS - NAS 9.2 - Control Container Editor
        private Component editorComponent = null;
        private string editorComponentName = null;

        // MBS 5/11/09 - NA9.2 GroupByRowConnector Appearance
        private Infragistics.Win.AppearanceHolder groupByRowConnectorAppearanceHolder;
        private Infragistics.Win.AppearanceHolder groupByRowAppearanceHolder;

        // MBS 6/19/09 - TFS18639
        private FilterOperandDropDownItems filterOperandDropDownItems = FilterOperandDropDownItems.Default;
        internal const FilterOperandDropDownItems DefaultFilterOperandDropDownItems =
            FilterOperandDropDownItems.All |
            FilterOperandDropDownItems.Blanks |
            FilterOperandDropDownItems.CellValues |
            FilterOperandDropDownItems.Custom |
            FilterOperandDropDownItems.NonBlanks;            


		#region Cell appearance caching

		private sealed class AppearanceCache
		{
			internal AppearanceData data;

            // MRS 11/21/06 - BR17614
            // We need to store flags indicating which properties were resolved.
            internal AppearancePropFlags unresolvedProps;
        }

		// JJD 10/31/01
		// Added caching logic to optimize cell painting
		//
		private int cellAppearanceCacheSequence = 0;
		private int selectedCellAppearanceCacheSequence = 0;
		private AppearanceCache cellAppearanceCache = null;
		private AppearanceCache selectedCellAppearanceCache = null;

		internal bool GetCachedCellAppearance( bool selected, ref AppearanceData appData, ref ResolveAppearanceContext context )
		{
			UltraGridLayout layout = this.Layout;

			if ( ! layout.IsDrawing  || ( context.UnresolvedProps & AppearancePropFlags.Cursor ) != 0 )
				return false;

			if ( selected )
			{
				if ( this.selectedCellAppearanceCacheSequence != layout.DrawSequence )
					return false;

                // MRS 4/10/07 - BR21921
                if (this.selectedCellAppearanceCache == null)
                    return false;

				if ( context.UnresolvedProps == AppearancePropFlags.AllRender )
				{
					// JJD 11/01/01
					// If all the properties need to be copied over
					// then it is more effienet to just do the straight copy
					// instead of the merge
					//
					appData = this.selectedCellAppearanceCache.data;

                    // MRS 11/21/06 - BR17614
                    //context.UnresolvedProps = (AppearancePropFlags)0;
                    context.UnresolvedProps = this.selectedCellAppearanceCache.unresolvedProps;
				}
				else
				{
					// JJD 12/12/02 - Optimization
					// Call the Appearance object's MergeData method instead so
					// we don't make unnecessary copies of the data structure
					//AppearanceData.MergeAppearance( ref appData, 
					//	this.selectedCellAppearanceCache.data,
					//	ref context.UnresolvedProps );
					AppearanceData.Merge( ref appData, 
						ref this.selectedCellAppearanceCache.data,
						ref context.UnresolvedProps );
				}
			}
			else
			{
				if ( this.cellAppearanceCacheSequence != layout.DrawSequence )
					return false;

                // MRS 4/10/07 - BR21921
                if (this.cellAppearanceCache == null)
                    return false;

				if ( context.UnresolvedProps == AppearancePropFlags.AllRender )
				{
					// JJD 11/01/01
					// If all the properties need to be copied over
					// then it is more effienet to just do the straight copy
					// instead of the merge
					//
					appData = this.cellAppearanceCache.data;

                    // MRS 11/21/06 - BR17614
                    //context.UnresolvedProps = (AppearancePropFlags)0;
                    context.UnresolvedProps = this.cellAppearanceCache.unresolvedProps;
                }
				else
				{
					// JJD 12/12/02 - Optimization
					// Call the Appearance object's MergeData method instead so
					// we don't make unnecessary copies of the data structure
					//AppearanceData.MergeAppearance( ref appData, 
					//	this.cellAppearanceCache.data,
					//	ref context.UnresolvedProps );
					AppearanceData.Merge( ref appData, 
						ref this.cellAppearanceCache.data,
						ref context.UnresolvedProps );
				}
			}

			return true;

		}

        // MRS 11/21/06 - BR17614
        //internal void SetCachedCellAppearance( bool selected, ref AppearanceData appData )
        internal void SetCachedCellAppearance(bool selected, ref AppearanceData appData, ref AppearancePropFlags unresolvedProps)
        {
			if ( !this.Layout.IsDrawing )
				return;

			if ( selected )
			{
				if ( this.selectedCellAppearanceCache == null )
					this.selectedCellAppearanceCache = new AppearanceCache();

				this.selectedCellAppearanceCache.data 		= appData;
				this.selectedCellAppearanceCacheSequence	= this.Layout.DrawSequence;

                // MRS 11/21/06 - BR17614
                this.selectedCellAppearanceCache.unresolvedProps = unresolvedProps;
			}
			else
			{
				if ( this.cellAppearanceCache == null )
					this.cellAppearanceCache = new AppearanceCache();

				this.cellAppearanceCache.data				= appData;
				this.cellAppearanceCacheSequence			= this.Layout.DrawSequence;

                // MRS 11/21/06 - BR17614
                this.cellAppearanceCache.unresolvedProps = unresolvedProps;
            }
		}

		internal void ClearDrawCache()
		{
			// JJD 11/06/01
			// Clear each bands cache
			//
			this.cellAppearanceCache			= null;
			this.selectedCellAppearanceCache	= null;
		}

		internal void ClearFontCache()
		{
			this.ClearFontCache( true );
		}

		// SSP 4/24/08 BR27115
		// Added an overload of ClearFontCache that takes in dirtyColMetrics parameter.
		// 
		internal void ClearFontCache( bool dirtyColMetrics )
		{
			this.cachedCardLabelSize	= Size.Empty;

            // MRS 6/15/2009 - TFS18449
            // When the font cache is cleared, we need to recalculate the cachedCardLabelSize
            // the next time it's asked for. 
            //
            this.verifiedColumnHeaderHeightVersionNumber--;

			// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
			// Changed the cachedCellWidthDefault to cachedCellSizeDefault.
			// 
			//this.cachedCellWidthDefault = 0;
			this.cachedCellSizeDefault = Size.Empty;

			// SSP 6/17/02 UWG1218
			// When anything that effects the default width of a column changes,
			// we need to dirty the metrics because the widths of the columns
			// will change but the column headers won't be regenerated. To
			// cause them to be regenerated dirty the metrics.
			//
			if ( 
				// SSP 4/24/08 BR27115
				// Added an overload of ClearFontCache that takes in dirtyColMetrics parameter.
				// Only dirty the metrics if dirtyColMetrics is true. Note that before we were 
				// always dirtying the metrics.
				// 
				dirtyColMetrics 
				&& null != this.band && null != this.band.Layout 
				&& null != this.band.Layout.ColScrollRegions )
			{
				this.Band.Layout.DirtyGridElement( true );
				this.band.Layout.ColScrollRegions.DirtyMetrics( );
			}
		}

		#endregion

		#region constructors

		/// <summary>
		/// Constructor used only for de-serialization.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public UltraGridColumn( string key ) : base( key )
		{
			this.Initialize( -1,
				0,
				-1,
				0,
				-1,
				SortIndicator.None,
				false,
				null );
		}

		/// <summary>
		/// Constructor used only for de-serialization.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="unboundRelativePosition">The unbound relative position of the column.</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public UltraGridColumn( string key, 
			int unboundRelativePosition ) : base( key )
		{
			this.Initialize( unboundRelativePosition,
				0,
				-1,
				0,
				-1,
				SortIndicator.None,
				false,
				null );
		}

		/// <summary>
		/// Constructor used only for de-serialization.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="unboundRelativePosition">The unbound relative position of the column.</param>
        /// <param name="valueListLink">Used to link a value list to the column.</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public UltraGridColumn( string key, 
			int unboundRelativePosition,
			object valueListLink ) : base( key )
		{
			this.Initialize( unboundRelativePosition,
				0,
				-1,
				0,
				-1,
				SortIndicator.None,
				false,
				valueListLink );
		}

		/// <summary>
		/// Constructor used only for de-serialization.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="unboundRelativePosition">The unbound relative position of the column.</param>
        /// <param name="valueListLink">Used to link a value list to the column.</param>
        /// <param name="groupId">Group ID</param>
        /// <param name="indexInGroup">Index of the column in the group.</param>
        /// <param name="level">Level</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public UltraGridColumn( string key, 
			int unboundRelativePosition,
			object valueListLink,
			int groupId,
			int indexInGroup,
			int level ) : base( key )
		{
			this.Initialize( unboundRelativePosition,
				groupId,
				indexInGroup,
				level,
				-1,
				SortIndicator.None,
				false,
				valueListLink );
		}

		/// <summary>
		/// Constructor used only for de-serialization.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="unboundRelativePosition">The unbound relative position of the column.</param>
        /// <param name="valueListLink">Used to link a value list to the column.</param>
        /// <param name="sortIndex">Sorted index.</param>
        /// <param name="isGroupByColumn">Specifies whether the grid is grouped by this column.</param>
        /// <param name="sortIndicator">Sort indicator setting of the column header.</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public UltraGridColumn( string key, 
			int unboundRelativePosition,
			object valueListLink,
			int sortIndex,
			SortIndicator sortIndicator,
			bool isGroupByColumn ) : base( key )
		{
			this.Initialize( unboundRelativePosition,
				0,
				-1,
				0,
				sortIndex,
				sortIndicator,
				isGroupByColumn,
				valueListLink );
		}


		/// <summary>
		/// Constructor used only for de-serialization.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="unboundRelativePosition">The unbound relative position of the column.</param>
        /// <param name="valueListLink">Used to link a value list to the column.</param>
        /// <param name="groupId">Group ID</param>
        /// <param name="indexInGroup">Index of the column in the group.</param>
        /// <param name="level">Level</param>
        /// <param name="sortIndex">Sorted index.</param>
        /// <param name="isGroupByColumn">Specifies whether the grid is grouped by this column.</param>
        /// <param name="sortIndicator">Sort indicator setting of the column header.</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public UltraGridColumn( string key, 
			int unboundRelativePosition,
			object valueListLink,
			int groupId,
			int indexInGroup,
			int level,
			int sortIndex,
			SortIndicator sortIndicator,
			bool isGroupByColumn ) : base( key )
		{
			this.Initialize( unboundRelativePosition,
				groupId,
				indexInGroup,
				level,
				sortIndex,
				sortIndicator,
				isGroupByColumn,
				valueListLink );
		}

		internal UltraGridColumn( UltraGridBand band )
		{
			this.band = band;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			////RobA 10/23/01 implemented inBeforePosChanged flag
			//this.Header.SetInBeforePosChanged(false);

			this.Reset();			
			
		}
        
		internal UltraGridColumn( UltraGridBand band, String key ) : base( key )
		{
			this.band = band;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			////RobA 10/23/01 implemented inBeforePosChanged flag
			//this.Header.SetInBeforePosChanged(false);

			this.Reset();
						
		}

		private void Initialize( int unboundRelativePosition,
			int groupId,
			int indexInGroup,
			int level,
			int sortIndex,
			SortIndicator sortIndicator,
			bool isGroupByColumn,
			object valueListLink )
		{

			this.Reset();

			this.unboundRelativePosition 
				= unboundRelativePosition;
			this.bound				= ( unboundRelativePosition < 0 ); 
			this.sortIndex			= sortIndex;
			this.sortIndicator		= sortIndicator;
			this.isGroupByColumn	= isGroupByColumn;
			this.level				= level;
			this.groupId			= groupId;
			this.indexInGroup		= indexInGroup;

			if ( valueListLink != null )
			{
				// MD 8/7/07 - 7.3 Performance
				// FxCop - Do not cast unnecessarily
				//if ( valueListLink is string )
				//    this.valueListControlName = valueListLink as string;
				string stringValue = valueListLink as string;

				if ( stringValue != null )
					this.valueListControlName = stringValue;
				else
				{
					try 
					{
						this.valueListId = (int)valueListLink;
					}
					catch ( Exception )
					{
					}
				}
			}
		}
		#endregion

		private ColumnCloneData CloneData
		{
			get
			{
				return this.cloneData;
			}
		}

		internal int SortIndex { get { return this.sortIndex; } }
		internal int GroupId { get { return this.groupId; } } 
		internal int IndexInGroup { get { return this.indexInGroup; } }

		internal UltraGridColumn ClonedFromColumn
		{
			get 
			{
				return (this.cloneData != null) ? this.cloneData.clonedFromCol : null;
			}
		}
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void SetMinDateMaxDateDefault( out System.DateTime minDate, out System.DateTime maxDate )
		internal static void SetMinDateMaxDateDefault( out System.DateTime minDate, out System.DateTime maxDate )
		{
			//	set the significant values for each
			//	The default MinDate for MSComCtl's MonthView
			//	is 1/1/1753, so we will default to that too
			minDate = new System.DateTime( 1753, 1, 1, 0, 0, 0, 0 );

			//	The default MaxDate for MSComCtl's MonthView
			//	is 12/31/9999, so we will default to that too
			maxDate = new System.DateTime( 9999, 12, 31, 0, 0, 0, 0 );		
		}

 
		/// <summary>
		/// Called when the object is disposed of
		/// </summary>
		protected override void OnDispose()
		{
			// unhook notifications and call dispose on the header object
			//
			if ( null != this.header )
			{
				this.header.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.header.Dispose();
				this.header = null;
				
			}
			
			// It is importantant to unhook the holder and call reset on it
			// when we are disposed
			//
			if ( this.cellAppearanceHolder != null )
			{
				this.cellAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.cellAppearanceHolder.Reset();
				this.cellAppearanceHolder = null;
			}

			if ( this.maskLiteralsAppearanceHolder != null )
			{
				this.maskLiteralsAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.maskLiteralsAppearanceHolder.Reset();
				this.maskLiteralsAppearanceHolder = null;
			}

			// SSP 4/22/05 BR03449
			//
			// ----------------------------------------------------------------------------------
			if ( null != this.cellButtonAppearanceHolder )
			{
				this.cellButtonAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.cellButtonAppearanceHolder.Reset();
				this.cellButtonAppearanceHolder = null;
			}

			if ( null != this.formulaErrorAppearanceHolder )
			{
				this.formulaErrorAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.formulaErrorAppearanceHolder.Reset();
				this.formulaErrorAppearanceHolder = null;
			}

			if ( null != this.mergedCellAppearanceHolder )
			{
				this.mergedCellAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.mergedCellAppearanceHolder.Reset();
				this.mergedCellAppearanceHolder = null;
			}

			if ( null != this.filterCellAppearanceHolder )
			{
				this.filterCellAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.filterCellAppearanceHolder.Reset();
				this.filterCellAppearanceHolder = null;
			}

			if ( null != this.filterOperatorAppearanceHolder )
			{
				this.filterOperatorAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.filterOperatorAppearanceHolder.Reset();
				this.filterOperatorAppearanceHolder = null;
			}

			if ( null != this.valueConstraint && this.valueConstraint.Enumeration is ValueList )
			{
				Infragistics.Win.ValueList vl = (ValueList)this.valueConstraint.Enumeration;
				vl.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.valueConstraint.Enumeration = null;

                // MBS 7/9/08 - BR33993
                vl.ReleaseResources();
			}
			// ----------------------------------------------------------------------------------

			// SSP 7/30/02
			// Dispose of defaultEditorWithCombo if allocated.
			//
			if ( null != this.defaultEditorWithCombo )
			{
				this.defaultEditorWithCombo.Dispose( );
				this.defaultEditorWithCombo = null;
			}

			// SSP 8/8/05 - NAS 5.3 New Column Styles
			//
			if ( null != this.lastCachedStyleEditor )
			{
				this.lastCachedStyleEditor.Dispose( );
				this.lastCachedStyleEditor = null;
			}

			// SSP 3/20/03 - Row Layout Functionality
			// 
			if ( null != this.rowLayoutColumnInfo )
			{
				this.rowLayoutColumnInfo.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.rowLayoutColumnInfo = null;
			}

			// SSP 1/31/05
			// Make sure we unhook from the editor control's Disposed.
			//
			this.InternalSetEditorControl( null );

            // MBS 1/25/07
            if (this.valueBasedAppearance != null)
            {
                this.valueBasedAppearance.PropertyChanged -= new EventHandler(this.OnValueBasedAppearancePropertyChanged);
                this.valueBasedAppearance = null;
            }

            // MBS 7/9/08 - BR33993
            if (this.filterOperatorEditorOwnerInfo != null)
                this.filterOperatorEditorOwnerInfo.ReleaseResources();            

			base.OnDispose();
		}

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values
		/// </summary>
        /// <returns>Returns true if the object needs to be serialized.</returns>
		//[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal protected bool ShouldSerialize() 
		{
			// JJD 11/28/01 - UWG784
			// If the column is part of a group it needs to be serialized
			//
			if ( !this.IsBound || this.group != null || this.groupId != 0 )
				return true;

            return this.ShouldSerializeCellActivation() ||
                this.ShouldSerializeAutoEdit() ||
                this.ShouldSerializeAutoSizeEdit() ||
                this.ShouldSerializeButtonDisplayStyle() ||
                // MRS 4/7/05 - Replaced Case with CharacterCasing
                //this.ShouldSerializeCase()  ||
                this.ShouldSerializeCharacterCasing() ||

                this.ShouldSerializeCellMultiLine() ||
                this.ShouldSerializeColSpan() ||
                this.ShouldSerializeDataType() ||
                // JAS 2005 v2 XSD Support - FieldLen was deprecated and replaced with MaxLength.
                //				this.ShouldSerializeFieldLen()  ||
                this.ShouldSerializeMaxLength() ||
                this.ShouldSerializeHeader() ||
                this.ShouldSerializeHidden() ||
                this.ShouldSerializeKey() ||
                this.ShouldSerializeLevel() ||
                this.ShouldSerializeLockedWidth() ||
                this.ShouldSerializeMaskClipMode() ||
                this.ShouldSerializeMaskDataMode() ||
                this.ShouldSerializeMaskDisplayMode() ||
                this.ShouldSerializeMaskInput() ||
                //this.ShouldSerializeMaxDate() ||
                this.ShouldSerializeMinWidth() ||
                this.ShouldSerializeMaxWidth() ||
                //this.ShouldSerializeMinDate() ||
                this.ShouldSerializeNullable() ||
                this.ShouldSerializePromptChar() ||

                //	BF 10.25.04	UWG3518
                this.ShouldSerializePadChar() ||

                this.ShouldSerializeProportionalResize() ||
                this.ShouldSerializeSortIndicatorInternal() ||
                this.ShouldSerializeStyle() ||
                this.ShouldSerializeTabStop() ||
                this.ShouldSerializeTag() ||
                this.ShouldSerializeValueList() ||
                this.ShouldSerializeVertScrollBar() ||
                this.ShouldSerializeFormat() ||
                this.ShouldSerializeWidth() ||
                this.ShouldSerializeAllowGroupBy() ||
                this.ShouldSerializeIsGroupByColumn() ||
                this.ShouldSerializeMaskLiteralsAppearance() ||
                // RobA UWG728 11/14/01
                this.ShouldSerializeCellAppearance() ||
                // JJD 1/21/02 - UWG815 Moved nulltext from layout to override and column
                //
                this.ShouldSerializeNullText() ||
                // JJD 1/17/02
                this.ShouldSerializeHiddenWhenGroupBy() ||
                // SSP 6/5/02
                // EM Embeddable editors
                // Added MinValue and MaxValue propertries.
                //
                this.ShouldSerializeMinValue() ||
                this.ShouldSerializeMaxValue() ||
                // SSP 8/20/02
                //
                this.ShouldSerializeAllowRowFiltering() ||
                this.ShouldSerializeAllowRowSummaries() ||

                // SSP 8/20/02
                // Check if editor control needs to be serialized.
                //
                this.ShouldSerializeEditorControl() ||

                // MRS - NAS 9.2 - Control Container Editor
                this.ShouldSerializeEditorComponent() ||

                // SSP 1/30/03 - Row Layout Functionality
                //
                this.ShouldSerializeRowLayoutColumnInfo() ||

                // SSP 5/12/03 - Optimizations
                // Added a way to just draw the text without having to embedd an embeddable ui element in
                // cells to speed up rendering.
                //
                this.ShouldSerializeCellDisplayStyle() ||

                // SSP 11/13/03 Add Row Feature
                //
                this.ShouldSerializeDefaultCellValue() ||

                // SSP 12/11/03 UWG2781
                // Added UseEditorMaskSettings property.
                //
                this.ShouldSerializeUseEditorMaskSettings() ||
                // SSP 12/12/03 DNF135
                // Added a way to control whether ink buttons get shown.
                // Added ShowInkButton property.
                //
                this.ShouldSerializeShowInkButton() ||

                // MRS 9/14/04
                // Added a property to control whether or not the grid displays 
                // a string in a cell while calculating
                //
                this.ShouldSerializeShowCalculatingText() ||

                // SSP 7/19/04 - UltraCalc
                //
                this.ShouldSerializeFormula() ||
                this.ShouldSerializeFormulaErrorAppearance() ||

                // SSP 8/19/04 - UltraCalc
                //
                this.ShouldSerializeFormulaErrorValue() ||

                // SSP 11/3/04 - Merged Cell Feature
                //
                this.ShouldSerializeMergedCellAppearance() ||
                this.ShouldSerializeMergedCellContentArea() ||
                this.ShouldSerializeMergedCellStyle() ||
                this.ShouldSerializeMergedCellEvaluationType() ||

                // SSP 12/14/04 - IDataErrorInfo Support
                //
                this.ShouldSerializeSupportDataErrorInfo() ||
                // SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
                //
                // ----------------------------------------------------------------------
                this.ShouldSerializeFilterEvaluationTrigger() ||
                this.ShouldSerializeFilterOperandStyle() ||
                this.ShouldSerializeFilterOperatorDefaultValue() ||
                this.ShouldSerializeFilterOperatorDropDownItems() ||
                this.ShouldSerializeFilterOperatorLocation() ||
                this.ShouldSerializeFilterCellAppearance() ||
                this.ShouldSerializeFilterOperatorAppearance() ||
                this.ShouldSerializeFilterComparisonType() ||
                // SSP 6/8/05 - NAS 5.2 Filter Row
                // Added FilterClearButtonLocation on the column as well for greater flexibility.
                //
                this.ShouldSerializeFilterClearButtonVisible() ||
                // ----------------------------------------------------------------------

                // JAS 2005 v2 XSD Support
                //
                // ----------------------------------------------------------------------
                this.ShouldSerializeMinLength() ||
                this.ShouldSerializeMaxValueExclusive() ||
                this.ShouldSerializeMinValueExclusive() ||
                this.ShouldSerializeRegexPattern() ||
                // ----------------------------------------------------------------------

                // JAS v5.2 GroupBy Break Behavior 5/3/05
                //
                this.ShouldSerializeGroupByMode() ||

                // JAS v5.2 GroupBy Break Behavior 5/4/05
                //
                this.ShouldSerializeSortComparisonType() ||

                // SSP 6/17/05 - NAS 5.3 Column Chooser
                // 
                this.ShouldSerializeColumnChooserCaption() ||
                this.ShouldSerializeExcludeFromColumnChooser() ||
                // SSP 7/29/05 - NAS 5.3 Tab Index
                // 
                this.ShouldSerializeTabIndex() ||
                // SSP 2/17/06 BR08864
                // Added IgnoreMultiCellOperation property on the column.
                // 
                this.ShouldSerializeIgnoreMultiCellOperation() ||
                // SSP 4/5/06 - NAS 6.2 - Support for New SpellCheck functionality 
                // Added SpellChecker property.
                // 
                this.ShouldSerializeSpellChecker() ||
                // MBS 11/29/06 - NAS 7.1 - Conditional Formatting
                //
                this.ShouldSerializeValueBasedAppearance() ||
                //  BF 3/12/08  FR09238 - AutoCompleteMode
                //
                this.ShouldSerializeAutoCompleteMode() ||

                // CDS 04/14/08 NA 8.2 ImeMode
                this.ShouldSerializeImeMode() ||

                // MD 9/23/08 - TFS6601
                this.ShouldSerializeReserveSortIndicatorSpaceWhenAutoSizing() ||

                // MBS 5/11/09 - NA9.2 GroupByRowConnector Appearance
                this.ShouldSerializeGroupByRowConnectorAppearance() ||
                this.ShouldSerializeGroupByRowAppearance() ||

                // MBS 6/23/09 - TFS18639
                this.ShouldSerializeFilterOperandDropDownItems();
		}
 
		/// <summary>
		/// Resets the properties of the object to their default values.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void Reset() 
		{
			this.ResetCellActivation();
			this.ResetAutoEdit();
			this.ResetAutoSizeEdit();
			this.ResetButtonDisplayStyle();
			// MRS 4/7/05 - Replaced Case with CharacterCasing
			//this.ResetCase();
			this.ResetCharacterCasing();

			this.ResetCellAppearance();
			this.ResetCellMultiLine();
			this.ResetColSpan();
			this.ResetDataType();
			// JAS 2005 v2 XSD Support
//			this.ResetFieldLen();
			this.ResetMaxLength();

			this.ResetHeader();
			
			this.ResetHidden();
			//this.ResetKey();
			this.ResetLevel();
			this.ResetLockedWidth();
			this.ResetMaskClipMode();
			this.ResetMaskDataMode();
			this.ResetMaskDisplayMode();
			this.ResetMaskInput();
			//			this.ResetMaxDate(); // JJD 8//19/02 - deprecated
			this.ResetMaxWidth();
			//			this.ResetMinDate(); // JJD 8//19/02 - deprecated
			this.ResetMinWidth();
			this.ResetNullable();
			this.ResetPromptChar();
			this.ResetProportionalResize();
			this.ResetSortIndicator();
			this.ResetStyle();
			this.ResetTabStop();
			this.ResetValueList();
			this.ResetVertScrollBar();
			this.ResetWidth();
			this.ResetFormat();            
			this.ResetAllowGroupBy( );
			this.ResetIsGroupByColumn( );
			// MRS 12/9/04 - BR01204
			//this.ResetMaskListeralsAppearance( );
			this.ResetMaskLiteralsAppearance( );
			this.ResetHiddenWhenGroupBy();

			// JJD 1/21/02 - UWG815 Moved nulltext from layout to override and column
			//
			this.ResetNullText();
			// SSP 6/5/02
			// EM Embeddable editors
			// Added MinValue and MaxValue propertries.
			//
			this.ResetMinValue( );
			this.ResetMaxValue( );

			// SSP 8/20/02
			//
			this.ResetAllowRowFiltering( );
			this.ResetAllowRowSummaries( );

			// SSP 8/20/02
			// Reset the editor and editor control.
			//
			this.ResetEditor( );
			this.ResetEditorControl( );

            // MRS - NAS 9.2 - Control Container Editor
            this.ResetEditorComponent();

			// SSP 1/30/03 - Row Layout Functionality
			//
			this.ResetRowLayoutColumnInfo( );

			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			//
			this.ResetCellDisplayStyle( );

			// SSP 11/13/03 Add Row Feature
			//
			this.ResetDefaultCellValue( );

			// SSP 12/11/03 UWG2781
			// Added UseEditorMaskSettings property.
			//
			this.ResetUseEditorMaskSettings( );

			// SSP 12/12/03 DNF135
			// Added a way to control whether ink buttons get shown.
			// Added ShowInkButton property.
			//
			this.ResetShowInkButton( );

			// MRS 9/14/04
			// Added a property to control whether or not the grid displays 
			// a string in a cell while calculating
			//
			this.ResetShowCalculatingText();

			// SSP 7/19/04 - UltraCalc
			//
			this.ResetFormulaErrorAppearance( );
			this.ResetFormula( );

			// SSP 8/19/04 - UltraCalc
			//
			this.ResetFormulaErrorValue( );

			// SSP 11/3/04 - Merged Cell Feature
			//
			this.ResetMergedCellAppearance( );
			this.ResetMergedCellContentArea( );
			this.ResetMergedCellStyle( );
			this.ResetMergedCellEvaluationType( );

			// SSP 12/14/04 - IDataErrorInfo Support
			//
			this.ResetSupportDataErrorInfo( );

			// SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			//
			// ----------------------------------------------------------------------
			this.ResetFilterEvaluationTrigger( );
			this.ResetFilterOperandStyle( );
			this.ResetFilterOperatorDefaultValue( );
			this.ResetFilterOperatorDropDownItems( );
			this.ResetFilterOperatorLocation( );
			this.ResetFilterCellAppearance( );
			this.ResetFilterOperatorAppearance( );
			this.ResetFilterComparisonType( );
			// SSP 6/8/05 - NAS 5.2 Filter Row
			// Added FilterClearButtonLocation on the column as well for greater flexibility.
			//
			this.ResetFilterClearButtonVisible( );
			// ----------------------------------------------------------------------

			// JAS 2005 v2 XSD Support
			//
			// ----------------------------------------------------------------------
			this.ResetMinLength();
			this.ResetMaxValueExclusive();
			this.ResetMinValueExclusive();
			this.ResetRegexPattern();
			this.XsdSuppliedConstraints = 0;
			// ----------------------------------------------------------------------

			// JAS v5.2 GroupBy Break Behavior 5/3/05
			//
			this.ResetGroupByMode();

			// JAS v5.2 GroupBy Break Behavior 5/4/05
			//
			this.ResetSortComparisonType();

			// SSP 6/17/05 - NAS 5.3 Column Chooser
			// 
			this.ResetColumnChooserCaption( );
			this.ResetExcludeFromColumnChooser( );

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			this.ResetTabIndex( );

			// SSP 2/17/06 BR08864
			// Added IgnoreMultiCellOperation property on the column.
			// 
			this.ResetIgnoreMultiCellOperation( );

			// SSP 4/5/06 - NAS 6.2 - Support for New SpellCheck functionality 
			// Added SpellChecker property.
			// 
			this.ResetSpellChecker( );

            //  BF 3/12/08  FR09238 - AutoCompleteMode
            this.ResetAutoCompleteMode();

            // CDS 04/14/08 NA 8.2 ImeMode
            this.ResetImeMode();

            // MRS NAS v8.3 - Unit Testing
            this.ResetCellButtonAppearance();
            this.ResetPadChar();
            this.ResetAutoSizeMode();
            this.ResetCellClickAction();
            this.ResetInvalidValueBehavior();
            this.ResetValueBasedAppearance();


			// MD 9/23/08 - TFS6601
			this.ResetReserveSortIndicatorSpaceWhenAutoSizing();

            // MBS 5/11/09 - NA9.2 GroupByRowConnector Appearance
            this.ResetGroupByRowConnectorAppearance();
            this.ResetGroupByRowAppearance();

            // MBS 6/23/09 - TFS18639
            this.ResetFilterOperandDropDownItems();
        }

		/// <summary>
		/// Returns the associated Band object.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// UltraGridColumn maintains a reference to the band that the column belongs to. 
		/// The <b>Band</b> property returns that band.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Columns"/>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		[ Browsable(false) ]
		[LocalizedCategory("LC_Default")]
		public Infragistics.Win.UltraWinGrid.UltraGridBand Band
		{
			get 
			{
				return this.band;
			}
		}

		// SSP 8/31/04
		// Added ParentCollection property.
		//
		internal Infragistics.Win.UltraWinGrid.ColumnsCollection ParentCollection
		{
			get
			{
				return null != this.Band ? this.Band.Columns : null;
			}
		}

		/// <summary>
		/// Determines whether the object will be displayed. This property is not available at design-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>Hidden</b> property determines whether an object is visible. Hiding an object may have have effects that go beyond simply removing it from view. For example, hiding a band also hides all the rows in that band. Also, changing the <b>Hidden</b> property of an object affects all instances of that object. For example, a hidden column or row is hidden in all scrolling regions.</p>
		///	<p class="body">There may be instances where the <b>Hidden</b> property cannot be changed. For example, you cannot hide the currently active rowscrollregion or colscrollregion. If you attempt to set the <b>Hidden</b> property of the active rowscrollregion to True, an error will occur.</p>
		///	<p class="body">This property is ignored for chaptered columns; that is, columns whose <b>DataType</b> property is set to 136 (DataTypeChapter).</p>
		///	</remarks>
		[LocalizedDescription("LD_AppearanceCache_P_Hidden")]
		[LocalizedCategory("LC_Appearance")]
		public bool Hidden
		{
			get 
			{
				if ( this.IsChaptered )
					return true;

				// AS - 10/25/01
				// We were checking the band when it did not exist.
				//
				if ( this.band != null &&  
					this.IsGroupByColumn && 
					ViewStyleBand.OutlookGroupBy == this.Band.Layout.ViewStyleBand )
				{
					bool hiddenIfGrouped = false;

					// JJD 1/17/02
					// Check new hiddenWhenGroupBy property setting 
					//
					// SSP 6/30/05 - NAS 5.3 Column Chooser
					// Added HiddenWhenGroupByResolved on the column.
					//
					// ----------------------------------------------------------------------
					hiddenIfGrouped = this.HiddenWhenGroupByResolved;
					
					// ----------------------------------------------------------------------

					if  (hiddenIfGrouped)
					{
						// JJD 10/04/01 - UWG482
						// If this is the last group by column make sure there is at least 
						// one other column that is visible. If not return false so this
						// column will be visible
						//
						if ( this == this.band.SortedColumns.LastGroupByColumn &&
							!this.band.Columns.AreThereAnyOtherVisibleColumns( this ) )
						{
							return false;
						}

						return true;
					}
				}

				return this.hidden;
			}
			set
			{
				if( this.hidden != value )
				{
					if ( this.IsChaptered && !value )
						throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_65"));

					this.hidden = value;

                    // MRS 3/25/2008 - BR31396
                    this.DirtyHasColSpan();

					// SSP 6/24/03 UWG2392
					// When column's Hidden property is changed, it fires the SubObjPropChanged notification
					// which is picked up by the header before the band and it ends up propagating through the
					// event chain upto the grid's propertychanged without dirtying the row layout info.
					// This poses a slight problem that's apparent in UWG2392. So dirty the row layout here.
					//
					if ( null != this.Band && this.Band.UseRowLayoutResolved )
						this.Band.DirtyRowLayoutCachedInfo( );

					// SSP 7/18/03 - Fixed headers
					// Since hiding a column could potentially change the number of
					// fixed visible columns, bump the verify version number.
					//
					if ( null != this.Band )
						this.Band.BumpFixedHeadersVerifyVersion( );

					this.NotifyPropChange( PropertyIds.Hidden, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeHidden() 
		{
			return this.hidden != false;
		}
 
		/// <summary>
		/// Resets Hidden to its default value (False.)
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetHidden() 
		{
			// JJD 11/29/01
			// Don't call property set since that throws an error
			// for chaptered columns
			//
			this.hidden = false;
			this.NotifyPropChange( PropertyIds.Hidden, null );
		}

		// JJD 1/17/02
		// Added hiddenWhenGroupBy property
		//
		/// <summary>
		/// Determines if this column will be hidden when it is a groupby column. 
		/// By default groupby columns are hidden.
		/// </summary>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.GroupByColumnsHidden"/>
		[ LocalizedDescription("LD_AppearanceCache_P_HiddenWhenGroupBy")]
		[ LocalizedCategory("LC_Display") ]
		public DefaultableBoolean HiddenWhenGroupBy
		{
			get
			{
				return this.hiddenWhenGroupBy;
			}
			set
			{
				if ( value != this.hiddenWhenGroupBy )
				{
                    // MRS NAS v8.3 - Unit Testing
                    //// test that the value is in range
                    ////
                    //if ( !Enum.IsDefined( typeof( DefaultableBoolean ), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_66") );
                    if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
                        throw new InvalidEnumArgumentException("HiddenWhenGroupBy", (int)value, typeof(DefaultableBoolean));

					this.hiddenWhenGroupBy = value;

					// SSP 7/18/03 - Fixed headers
					//
					if ( null != this.Band )
						this.Band.BumpFixedHeadersVerifyVersion( );

					this.NotifyPropChange( PropertyIds.HiddenWhenGroupBy, null );
				}
			}
		}

		/// <summary>
		/// Returns true if property needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeHiddenWhenGroupBy() 
		{
			return ( this.hiddenWhenGroupBy != DefaultableBoolean.Default );
		}
 
		/// <summary>
		/// Resets the property to its default value
		/// </summary>
		public void ResetHiddenWhenGroupBy() 
		{
			this.HiddenWhenGroupBy = DefaultableBoolean.Default;
		}


		/// <summary>
		/// Returns the Header object associated with the column. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Header</b> property of an object refers to a column or group header, as defined by an Header object. You use the <b>Header</b> property to access the properties of a specified Header object, or to return a reference to an Header object.</p>
		/// <p class="body">A Header object represents a column or group header that specifies information about the column or group, and can also serve as the interface for functionality such as moving, swapping or sorting the column or group. Group headers have the added functionality of serving to aggregate multiple columns under a single heading.</p> 
		/// <p class="body">The <b>Header</b> property provides access to the header that is associated with an object. In some instances, the type of header may be ambiguous, such as when accessing the <b>Header</b> property of a UIElement object. You can use the <b>Type</b> property of the Header object returned by the <b>Header</b> property to determine whether the header belongs to a column or a group.</p>
		/// </remarks>
		[ LocalizedDescription("LD_AppearanceCache_P_Header")]
		[ LocalizedCategory("LC_Default")]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ]
		public Infragistics.Win.UltraWinGrid.ColumnHeader Header
		{
			get
			{
				if ( null == this.header )
				{
					this.header = new ColumnHeader( this );

					// hook up the prop change notifications
					//
					this.header.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					
				}

				return this.header;
			}
		}

		// SSP 10/27/03
		// Added IsHeaderAllocated property.
		//
		internal bool IsHeaderAllocated
		{
			get
			{
				return null != this.header;
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeHeader() 
		{
			return ( null != this.header && 
				this.header.ShouldSerialize() );
		}
 
		/// <summary>
		/// Resets Header to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetHeader() 
		{
			if ( null != this.header )
				this.header.Reset();
		}

		internal int CellWidthDefault
		{
			get
			{
				// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
				// Added CellSizeDefault. Code in there was copied from here.
				// 
				return this.CellSizeDefault.Width;
				
			}
		}

		// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
		// Added CellSizeDefault. Code in there was copied from the CellWidthDefault.
		// 
		internal Size CellSizeDefault
		{
			get
			{
				// JJD 12/14/01
				// Make sure the cached value is still valid
				//
				this.band.ValidateColumnCardFontCache();

				Size size = this.cachedCellSizeDefault;
				if ( size.Width <= 0 || size.Height <= 0 )
				{
					if ( this.Layout.Grid == null )
						return new Size( 100, 18 );

					AppearanceData appData = new AppearanceData();

					ResolveAppearanceContext context = new ResolveAppearanceContext( typeof ( UltraGridCell ), AppearancePropFlags.FontData );

					context.IsCellOnly = true;

					this.ResolveAppearance( ref appData, ref context );

					string text = null;

					if ( this.DataType == typeof( string ) )
						text = "One sample string";
					else if ( this.DataType == typeof( DateTime ) )
						text = "23_23_2002";
					else if ( this.DataType == typeof( bool ) )
						text = "false";
					else if ( this.DataType == typeof( int )  ||
						this.DataType == typeof( float ) ||
						this.DataType == typeof( uint ) )
						text = "3,456,789";
					else if ( this.DataType == typeof( long )  ||
						this.DataType == typeof( ulong ) ||
						this.DataType == typeof( double ) ||
						this.DataType == typeof( decimal ) )
						text = "1,123,456,789";
					else if ( this.DataType == typeof( short )  ||
						this.DataType == typeof( ushort ) )
						text = "-13,456";
					else if ( this.DataType == typeof( byte )  ||
						this.DataType == typeof( sbyte ) ||
						this.DataType == typeof( char ) )
						text = "W";
					else if ( this.DataType == typeof( Guid ) )
						text = "{C0E65F47-F307-41d7-ADB3-04CA00C01F10}";
						// SSP 5/7/02
						// Added support for System.Drawing.Color
						//
					else if ( typeof( System.Drawing.Color ) == this.DataType )
						text = "(255, 255, 255)";
					else 
						text = "WWWg";

					int borderWidth = this.Layout.GetBorderThickness( this.band.BorderStyleCellResolved );

					// JJD 12/14/01
					// Moved font height logic into CalculateFontHeight
					//
					size = this.Layout.CalculateFontSize( ref appData, text );
					int delta = 2 * ( borderWidth + this.band.CellPaddingResolved + this.band.CellSpacingResolved );
					size.Width += delta;
					size.Height += delta;

					// Allow room for a button if necessary
					//
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
					//switch ( this.StyleResolved )
					switch ( this.GetStyleResolved( null ) )
					{
						case ColumnStyle.DropDown:
						case ColumnStyle.DropDownList:
						case ColumnStyle.DropDownCalendar:
						case ColumnStyle.DropDownValidate:
						case ColumnStyle.EditButton:
							size.Width += SystemInformation.VerticalScrollBarWidth + 3;
							break;
					}

					// JJD 12/17/01
					// Make sure the column isn't wider than the grid.
					//
					if ( this.Layout.Grid != null  &&
						// SSP 8/16/02 
						// Only make sure the width is less than the grid's client area if the
						// grid is an UltraGrid. For UltraCombo and UltraDropDown, we shouldn't
						// do this because this may get called when the drop down has not
						// been dropped down and its width could be 0 in which case we do not
						// want the default width of the column to be 0 as well.
						//
						this.Layout.Grid is UltraGrid &&
						this.Layout.Grid.ControlForGridDisplay != null &&
						this.Layout.Grid.ControlForGridDisplay.Created	&&
						this.Layout.Grid.ControlForGridDisplay.IsHandleCreated )
						size.Width = Math.Min( size.Width, 
							this.Layout.Grid.ControlForGridDisplay.ClientRectangle.Width - ( 2 * this.Layout.GetBorderThickness( this.Layout.BorderStyleResolved ) ) ); 

					this.cachedCellSizeDefault = size;
				}

				// SSP 12/9/02 UWG1768
				// Return a default width that falls within the min and max width settings.
				//
				// -----------------------------------------------------------------------
				if ( this.maxWidth > 0 && size.Width > this.maxWidth )
					size.Width = this.maxWidth;

				if ( this.minWidth > 0 && size.Width < this.minWidth )
					size.Width = this.minWidth;
				// -----------------------------------------------------------------------

				return size;
			}
		}

		/// <summary>
		/// Determines the width of the column.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Width</b> property is used to determine the horizontal dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">When proportional resizing is used for the UltraGridColumn and UltraGridGroup objects, the width of the column increases or decreases proportionally as the area occupied by the column changes size, due to the resizing of adjacent columns or of the grid itself. This property is ignored for chaptered columns; that is, columns whose <b>DataType</b> property is set to 136 (DataTypeChapter).</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_Width")]
		[LocalizedCategory("LC_Appearance")]
		public int Width
		{
			get
			{
				// SSP 2/26/03 - Row Layout Functionality
				// In row layout mode, column synchronization is off. So don't use any
				// possibly previously calculated synchronized width.
				// Enclosed the code in the following if block.
				//
				UltraGridBand band = this.Band;
				if ( null == band || ! band.UseRowLayoutResolved )
				{
					if ( this.synchronizedWidth > 0 )
						return this.synchronizedWidth;
				}

				// JJD 8/20/01
				// Check to make sure band is not null
				//
				if ( this.width < 1 && this.band != null )
				{	
					if ( band.ColWidthResolved > 0 )
						return band.ColWidthResolved;
					else
					{
                        // CDS NAS v9.1 Header CheckBox
                        // Adjust the width to provide space for the checkbox if it is visible in the header.
                        //// JJD 12/17/01
                        //// Instead of passing back a fixed size of 100 return a more
                        //// reasonable width based on the column type
                        ////
                        //// SSP 6/2/05 BR03774
                        //// Changed CardLabelSize to make use of the header appearance instead of the cell appearance.
                        //// What this means is that if the headers are displayed on the left or right of the cells then
                        //// the header and cell widths are independent.
                        ////
                        //// --------------------------------------------------------------------------------------------
                        ////return Math.Max( this.CardLabelSize.Width + 4, this.CellWidthDefault );
                        //if (band.CardView || band.UseRowLayoutResolved && !band.AreColumnHeadersInSeparateLayoutArea)
                        //    return this.CellWidthDefault;
                        //else
                        //    return Math.Max(this.CardLabelSize.Width + 4, this.CellWidthDefault);
                        int adjustedWidth = 0;
                        if (band.CardView || band.UseRowLayoutResolved && !band.AreColumnHeadersInSeparateLayoutArea)

                            adjustedWidth = this.CellWidthDefault;
                        else
                            adjustedWidth = Math.Max(this.CardLabelSize.Width + 4, this.CellWidthDefault);


                        if (this.ContainsVisibleHeaderCheckBoxOnLeftOrRight)
                            //adjustedWidth += UIElementDrawParams.CheckBoxGlyphInfo.GlyphSize.Height + HeaderCheckBoxUIElement.HEADERCHECKBOXPADDING;
                            adjustedWidth += UIElementDrawParams.GetGlyphSize(GlyphType.CheckBox, this.Layout.UIElement).Width + HeaderCheckBoxUIElement.HEADERCHECKBOXPADDING;

                        return adjustedWidth;
						// --------------------------------------------------------------------------------------------
					}
				}

				return this.width;
			}
			set
			{
				// SSP 3/29/02
				// I noticed that PerformAutoSize was not working correctly and apparently
				// because this property has not been implemented properly. In activeX, 
				// it calls SetWidth to set the width. So change to code to call SetWidth
				// here too which takes into account synchronized col widths and groups and
				// other stuff. So added below code and commented out the original 
				// implementation.
				// Also not, change in width property is notified in SetWidth so no
				// need to do that here.
				//--------------------------------------------------------------------
				// If width is negative, then throw argument out of range exception.
				//
				if ( value < 0 )
					throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_67"), value, Shared.SR.GetString("LE_ArgumentOutOfRangeException_310") );

				// If this column is chaptered, then just return without changing the width
				//
				if ( this.IsChaptered )
				{
					//Debug.Assert( false, "Set of Width called on a chaptered column." );
					return;
				}

				// Only do this if we are attached to a band and a layout because
				// the logic in SetWidth assumes that the column has a valid band and
				// a layout (which would not be the case for example during 
				// deserialization).
				//
				if ( null != this.Band && null != this.Band.Layout )
				{
					this.SetWidth( value, false, true, true, true, true );
				}
				else
				{
					if ( this.width != value )
					{
						this.width = value;
						this.NotifyPropChange( PropertyIds.Width, null );
					}
				}
				//--------------------------------------------------------------------
				// Below is original code commented out.
				
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeWidth() 
		{
			return this.width > 0;
		}
 
		/// <summary>
		/// Resets Width to its default value (0).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetWidth() 
		{
            // MRS NAS v8.3 - Unit Testing
            //this.width = 0;
            if (this.width != 0)
            {
                this.width = 0;
                this.NotifyPropChange(PropertyIds.Width, null);
            }
		}

		/// <summary>
		/// Gets the width of the column. This property simply delegates to the <see cref="Width"/> property.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Width"/>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		[Browsable(false)]
		// SSP 9/8/06 - NAS 6.3
		// Added EditorBrowsable attribute. This property is no different than Width so hiding it.
		// 
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		[LocalizedDescription("LD_AppearanceCache_P_Extent")]
		public int Extent
		{
			get
			{
				return this.Width;
			}
		}

		private void VerifyCardLabelSizeCache()
		{
			// JJD 12/14/01
			// Make sure the cached value is still valid
			//
			this.band.ValidateColumnCardFontCache();



			// JAS v5.2 Wrapped Header Text 4/18/05
			//
			bool versionNumberIsIdentical = 
				this.Layout != null && 
				this.verifiedColumnHeaderHeightVersionNumber == this.Layout.ColumnHeaderHeightVersionNumber;

			// SSP 8/25/05 BR05932
			// Below we access Width property which may access CardLabelSize which calls this
			// method. To prevent recursion, reset the version number here.
			// 
			this.verifiedColumnHeaderHeightVersionNumber = this.Layout.ColumnHeaderHeightVersionNumber;

			bool isHeaderTextWrapped = this.Band != null && this.Band.WrapHeaderTextResolved;

			// MD 7/8/08 - BR34519
			// Just as with wrapped header text, we need to do special calculations for rotated text as well.
			TextOrientationInfo textOrientation = this.Header.TextOrientationResolved;
			bool isHeaderTextRotated = textOrientation != null && Object.Equals( textOrientation, TextOrientationInfo.Horizontal ) == false;

			// MD 7/8/08 - BR34519
			// Added a new local variable to represent both values declared above and to simplify if statements below.
			bool isSpecialMeasuringNeeded = isHeaderTextRotated || isHeaderTextWrapped;

			//if ( !this.cachedCardLabelSize.IsEmpty )
			// MD 7/8/08 - BR34519
			// Check the new flag which checks if any special measuring is needed
			//if ( !this.cachedCardLabelSize.IsEmpty && ! isHeaderTextWrapped )
			if ( !this.cachedCardLabelSize.IsEmpty && !isSpecialMeasuringNeeded )
				return;

			// MD 7/8/08 - BR34519
			// Check the new flag which checks if any special measuring is needed
			//if( versionNumberIsIdentical && isHeaderTextWrapped )
			if ( versionNumberIsIdentical && isSpecialMeasuringNeeded )
				return;

			if ( this.Layout.Grid == null )
				return;

			AppearanceData appData = new AppearanceData();

			// SSP 6/2/05 BR03774
			// Changed CardLabelSize to make use of the header appearance instead of the cell appearance.
			// Using the cell's appearance here causes the wrap header text to not work properly if the 
			// cell and header fonts were different.
			// 
			// ----------------------------------------------------------------------------------------------
			
			this.Header.ResolveAppearance( ref appData, AppearancePropFlags.FontData );
			// ----------------------------------------------------------------------------------------------

			string caption = this.Header.Caption;

			if ( caption == null || caption.Length < 1 )
				caption = "Wg";

			// SSP 7/13/05 BR03751
			// 
			// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
			// Apparently the cachedCardLabelSize gets assigned Size.Empty in the ClearFontCache
			// and therefore we can't rely on cachedCardLabelSize actually being the previouse 
			// calculated cachedCardLabelSize. Therefore added cachedCardLabelSize_Prev member
			// var which we use further below towards the end of this method.
			// 
			//Size originalCardLabelSize = this.cachedCardLabelSize;

			// JAS v5.2 Wrapped Header Text 4/18/05
			// --------------------------------------------------------------------------------------------
			// JJD 12/14/01
			// Moved font height logic into CalculateFontHeight
			//this.cachedCardLabelSize = this.Layout.CalculateFontSize( ref appData, caption );

			// MD 8/15/07 - 7.3 Performance
			// Cache the header style resolved so we don't have to get it multiple times.
			HeaderStyle headerStyleResolved = this.Band.HeaderStyleResolved;

			if( isHeaderTextWrapped )
			{
				int widthConstraint;

				// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout/Card-View
				// Added the two if-else-if blocks and enclosed the existing code into the else block.
				// 
				// ------------------------------------------------------------------------------------
				if ( this.Band.UseRowLayoutResolved )
				{
					widthConstraint = this.RowLayoutColumnInfo.CachedItemRectHeader.Width;
					if ( widthConstraint <= 0 )
						widthConstraint = this.Width;
				}
				else if ( this.Band.CardView )
				{
					widthConstraint = this.Band.CardLabelWidthResolvedHelper( true );
				}
				// ------------------------------------------------------------------------------------
				else
				{
					widthConstraint = this.synchronizedWidth;				

					if( widthConstraint < 1 )
					{
						if( this.ShouldSerializeWidth() )
							widthConstraint = this.Width;
						else
							widthConstraint = this.MaxWidthActual;
					}

					// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
					// Moved this here from below.
					// 
					// JAS 5/6/05 BR03771 - The RowSelectorExtent property does not take the
					// RowSelectorHeaderStyleResolved property into account, so we must do that here.
					//
					if( this.Band.RowSelectorHeaderStyleResolved == RowSelectorHeaderStyle.ExtendFirstColumn 
						// SSP 8/30/05 BR05863
						// Added the following condition.
						// 
						&& widthConstraint > 0 )
						widthConstraint += this.Header.RowSelectorExtent;
				}

				if( widthConstraint > 0 )
				{
					widthConstraint -= this.WidthOfHeaderAdornments;

					// MD 8/15/07 - 7.3 Performance
					// Use the cached value
					//if( this.Band.HeaderStyleResolved == HeaderStyle.WindowsXPCommand )
					if ( headerStyleResolved == HeaderStyle.WindowsXPCommand )
						widthConstraint -= 6;
					else
						widthConstraint -= 2;

					// SSP 12/27/05 BR08272
					// If the header border style is mergeable then the BandHeadersUIElement merges the borders
					// of adjacent headers. Take that into account.
					// 
					if ( this.Layout.CanMergeAdjacentBorders( this.Band.BorderStyleHeaderResolved ) )
					{
						if ( ! this.Header.FirstItem )
							widthConstraint++;
					}

					// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
					// Moved this above.
					// 
					

					if( widthConstraint > 40 
						&&			
						(this.SortIndicator == SortIndicator.Ascending  ||
						 this.SortIndicator == SortIndicator.Descending ||
						 this.SortIndicator == SortIndicator.Disabled ) 
						)
					{
						widthConstraint -= SortIndicatorUIElement.SORTIND_WIDTH;
						widthConstraint -= 2;
					}
				}

				if( widthConstraint > 0 )
				{
					// MD 4/17/08 - 8.2 - Rotated Column Headers
					// Pass off the text rotation of the caption, because this will play a part in the size.
					//this.cachedCardLabelSize = this.Layout.CalculateFontSize( ref appData, caption, widthConstraint );
					this.cachedCardLabelSize = this.Layout.CalculateFontSize( ref appData, caption, widthConstraint, this.Header.TextOrientationResolved );
				}
				else
				{
					// MD 4/17/08 - 8.2 - Rotated Column Headers
					// Pass off the text rotation of the caption, because this will play a part in the size.
					//this.cachedCardLabelSize = this.Layout.CalculateFontSize( ref appData, caption );
					this.cachedCardLabelSize = this.Layout.CalculateFontSize( ref appData, caption, this.Header.TextOrientationResolved );
				}

				if( ! versionNumberIsIdentical && this.Layout != null )
					this.verifiedColumnHeaderHeightVersionNumber = this.Layout.ColumnHeaderHeightVersionNumber;
			}
			else
			{
				// MD 4/17/08 - BR32441 - 8.2 - Rotated Column Headers
				// We need to pass in the text orientation for this case too.
				//this.cachedCardLabelSize = this.Layout.CalculateFontSize( ref appData, caption );
				this.cachedCardLabelSize = this.Layout.CalculateFontSize( ref appData, caption, this.Header.TextOrientationResolved );
			}

			// MD 8/15/07 - 7.3 Performance
			// Use the cached value
			//if( this.Band.HeaderStyleResolved == HeaderStyle.WindowsXPCommand )
			if ( headerStyleResolved == HeaderStyle.WindowsXPCommand )
				this.cachedCardLabelSize.Width += 7;
			// --------------------------------------------------------------------------------------------

			// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
			// We need to add the border thickness.
			// 
			this.cachedCardLabelSize.Height += 2 * this.Band.HeaderBorderThickness;

			// SSP 7/21/03
			// Add the width for the sort indicators.
			//
			this.cachedCardLabelSize.Width += SortIndicatorUIElement.SORTIND_WIDTH;

            // CDS NAS v9.1 Header CheckBox
            // Add the width and height for the Checkbox
            if (this.ContainsVisibleHeaderCheckBoxOnLeftOrRight)
                this.cachedCardLabelSize.Width += UIElementDrawParams.GetGlyphSize(GlyphType.CheckBox, this.Layout.UIElement).Width + HeaderCheckBoxUIElement.HEADERCHECKBOXPADDING;
            else if (this.band.ContainsColumnWithVisibleHeaderCheckBoxOnTopOrBottom)
                this.cachedCardLabelSize.Height += UIElementDrawParams.GetGlyphSize(GlyphType.CheckBox, this.Layout.UIElement).Height + HeaderCheckBoxUIElement.HEADERCHECKBOXPADDING;


			// SSP 7/13/05 BR03751
			// Whenever the height of a column header changes we need to regenerate the visible rows.
			// 
			// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
			// Apparently the cachedCardLabelSize gets assigned Size.Empty in the ClearFontCache
			// and therefore we can't rely on cachedCardLabelSize actually being the previouse 
			// calculated cachedCardLabelSize. Therefore added cachedCardLabelSize_Prev member
			// var. Use that instead.
			// 
			//if ( originalCardLabelSize.Height != this.cachedCardLabelSize.Height && null != this.Layout )
			if ( this.cachedCardLabelSize_Prev.Height != this.cachedCardLabelSize.Height && null != this.Layout )
			{
				// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
				// The row-layout may need to be recalculated as a result of change the height of the header.
				// 
				if ( isHeaderTextWrapped )
					this.Band.DirtyRowLayoutCachedInfo( );

				this.Layout.DirtyGridElement( true );
			}

			// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
			// 
			this.cachedCardLabelSize_Prev = this.cachedCardLabelSize;
		}

		// JAS v5.2 Wrapped Header Text 4/18/05
		//
		#region WidthOfHeaderAdornments
		private int WidthOfHeaderAdornments
		{
			get
			{
				int width = 0;
				HeaderBase header = this.Header;
				if ( null != header )
				{
					if ( header.AllowSwapping )
						width += SwapButtonUIElement.SWAP_DROPDOWN_BUTTON_WIDTH;

					if ( header.HasFilterIcons )
						width += FilterDropDownButtonUIElement.FILTER_DROPDOWN_BUTTON_WIDTH;

					if ( header.AllowRowSummaries )
						width += RowSummariesButtonUIElement.ROW_SUMMARIES_BUTTON_WIDTH;

					// SSP 7/21/03
					// Add the width for the fixed header indicator.
					//
					if ( this.Band.UseFixedHeaders && FixedHeaderIndicator.None != header.FixedHeaderIndicatorResolved )
						width += FixedHeaderIndicatorUIElement.FIXED_HEADER_INDICATOR_BUTTON_WIDTH;
				}

				return width;
			}
		}
		#endregion // WidthOfHeaderAdornments

		internal Size CardLabelSize
		{
			get
			{
				// JJD 12/14/01
				// Make sure the cached value is still valid. If not recalculate it.
				//
				this.VerifyCardLabelSizeCache();

				// SSP 3/03/03 - Row Layout Functionality
				// Also take into account the row filter icon, row summary icon and the
				// swap drop down icon.
				//
				//return this.cachedCardLabelSize;
				Size size = this.cachedCardLabelSize;
				//
				// JAS v5.2 Wrapped Header Text 4/18/05
				// Moved the following code into the WidthOfHeaderAdornments property.
				//
//				if ( null != this.Header )
//				{
//					if ( this.Header.AllowSwapping )
//						size.Width += SwapButtonUIElement.SWAP_DROPDOWN_BUTTON_WIDTH;
//
//					// SSP 4/16/05 - NAS 5.2 Filter Row
//					// Changed the name from AllowRowFiltering to HasFilterIcons.
//					//
//					//if ( this.Header.AllowRowFiltering )
//					if ( this.Header.HasFilterIcons )
//						size.Width += FilterDropDownButtonUIElement.FILTER_DROPDOWN_BUTTON_WIDTH;
//
//					if ( this.Header.AllowRowSummaries )
//						size.Width += RowSummariesButtonUIElement.ROW_SUMMARIES_BUTTON_WIDTH;
//
//					// SSP 7/21/03
//					// Add the width for the fixed header indicator.
//					//
//					if ( this.Band.UseFixedHeaders && FixedHeaderIndicator.None != this.Header.FixedHeaderIndicatorResolved )
//						size.Width += FixedHeaderIndicatorUIElement.FIXED_HEADER_INDICATOR_BUTTON_WIDTH;
//				}

				size.Width += this.WidthOfHeaderAdornments;

				return size;
			}
		}


		internal int CardCellHeight
		{
			get
			{
				// JJD 12/14/01
				// Make sure the cached value is still valid. If not recalculate it.
				//
				this.VerifyCardLabelSizeCache();

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//UIElementBorderStyle cellBorderStyle = this.Band.BorderStyleCellResolved;
				//
				//int cellPadding = this.band.CellPaddingResolved;
				//int cellSpacing = this.band.CellSpacingResolved;
				//
				//int borderThickness	= this.Band.Layout.GetBorderThickness( cellBorderStyle );

				// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
				// 
				// ----------------------------------------------------------------------------------------
				
				int height = this.CellSizeDefault.Height;
				if ( null == this.Layout || ! this.Band.UseRowLayoutResolved )
					height = Math.Max( height, this.CardLabelSize.Height );

				return height;
				// ----------------------------------------------------------------------------------------
			}
		}

		/// <summary>
		/// Returns true if this is a chaptered column (represents a child
		/// set of rows)
		/// </summary>
		[Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
		public bool IsChaptered
		{
			get 
			{
				// SSP 8/12/03 - Optimizations
				// Instead of checking if the property descriptor's type is assignable from IList
				// like we do in IsChaptered, cache whether the property descriptor is chaptered or not
				// when the column is initialized with a property descriptor.
				// Commented out the original code and moved it to the InitPropertyDescriptor method.
				//
				// ------------------------------------------------------------------------------------
				
				return this.cachedIsChaptered;
				// ------------------------------------------------------------------------------------
			}
		}

		// SSP 5/25/04 - Extract Data Structure Related
		// Added InternalSetCachedIsChaptered method. When there is no data source and we are storing
		// the data structure only, then we need to set the chaptered state of columns that are 
		// chaptered. Before we were simply checking the property descriptor's property type. However
		// in the "Extract Data Structure Mode", we don't have any property descriptors to check.
		//
		internal void InternalSetCachedIsChaptered( bool val )
		{
			this.cachedIsChaptered = val;
		}

		// SSP 4/26/02
		// EM Embeddable editors.
		// Commented out the IValueListOwner implementation since that's not neccessary
		// anymore since the embeddable editors are going to take care of that.
		//
        

		// SSP 4/26/02
		// EM Embeddable editors.
		// Commented out UpdateDisplayText.
		//
		

		// SSP 4/26/02
		// EM Embeddable editors.
		// Commented out HasDataChanged. It was used for value list, but since we 
		// have editors taking care of that, I don't think we need this any more.
		//
		

		// SSP 10/24/06 BR15430
		// Added CreateChildBandBindingManagerHelper. Code in there was moved from the InitPropertyDescriptor
		// method below.
		// 
		internal BindingManagerBase CreateChildBandBindingManagerHelper( out string newDataMember )
		{
			UltraGridBand band = this.Band;
			UltraGridLayout layout = null != band ? band.Layout : null;

			// format the child band's data member name
			//
			if ( band.DataMember == null || band.DataMember.Length < 1 )
			{
				newDataMember = this.propertyDescriptor.Name;
			}
			else
			{
				newDataMember = band.DataMember + "." + this.propertyDescriptor.Name;
			}

			// get the child bands binding manager
			//
			// SSP 6/8/05 BR03609
			// This is to fix that issue where if a parent binding manager doesn't have at least one row 
			// in it and the associated IBindingList doesn't allow adding rows then the child binding 
			// manager can't be created (the BindingContext indexer throws an exception).
			// 
			// ------------------------------------------------------------------------------------------
			//BindingManagerBase childLM = this.Band.Layout.Grid.BindingContext[this.Band.Layout.Grid.DataSource, newDataMember];
			BindingManagerBase childLM = null;
			try
			{
				// SSP 5/22/06 BR12433
				// NOTE: This doesn't really help because the BindingSource GetRelatedCurrencyManager
				// is just too slow for high depth band levels.
				// 
				// SSP 9/13/07 BR26192
				// Commented out the following code since it causes BR26192. We are skipping the
				// recursion check that we do below. However since this doesn't help as indicated above,
				// simply commenting it out.
				// 
				// --------------------------------------------------------------------------------------
				//if ( null == childLM )
				//{
				//    try
				//    {
				//        CurrencyManager parentCM = band.BindingManager as CurrencyManager;
				//        ICurrencyManagerProvider cmProvider = null != parentCM ? parentCM.List as ICurrencyManagerProvider : null;
				//        if ( null != cmProvider )
				//            childLM = cmProvider.GetRelatedCurrencyManager( this.propertyDescriptor.Name );
				//    }
				//    catch { }
				//}
				// --------------------------------------------------------------------------------------

				if ( null == childLM )
				{
					// SSP 12/6/05 - Binding Source
					// Use the new GetCurrencyManager helper method that takes into account the BindingSource.
					//
					//childLM = this.Band.Layout.Grid.BindingContext[this.Band.Layout.Grid.DataSource, newDataMember];
					// SSP 6/7/06 BR11351
					// BindingSource's ICurrencyManagerProvider.GetCurrencyManager implementation is
					// exponentially slow for lower level bands. So much so that the app will freeze
					// if bound to a recursive datasource even though we only load only upto 100 bands.
					// This change is to workaround that issue. Don't load the binding manager if the 
					// datasource is BindingSource and we have a recursive data source. To check if
					// there is recursion in band structure, check if the parent band's data member is 
					// the same as child bands data member.
					// 
					// ------------------------------------------------------------------------------------
					//childLM = this.Layout.Grid.GetCurrencyManager( newDataMember );
					if ( null != band.BindingManager
						 && ( this.propertyDescriptor.Name != band.Key || !( band.List is BindingSource ) )
						)
					{
						childLM = layout.Grid.GetCurrencyManager( newDataMember );
					}
					// ------------------------------------------------------------------------------------
				}
			}
			catch ( Exception exception1 )
			{
				
				// SSP 9/15/06 - NAS 6.3
				// Raise Error event.
				// 
				if ( null != layout && null != layout.Grid )
					layout.Grid.InternalHandleDataError( SR.GetString( "DataError_CurrencyManagerRetrievalFailure" ), exception1, true );
			}

			return childLM;
		}

		internal void InitPropertyDescriptor( PropertyDescriptor propertyDescriptor,
			Infragistics.Win.UltraWinGrid.UltraGridBand [] oldBands )
		{
			this.propertyDescriptor = propertyDescriptor;

			UltraGridBand band = this.Band;
			UltraGridLayout layout = null != band ? band.Layout : null;

			// SSP 4/29/03 UWG1970
			// Honor the browsable attribute on the property descriptor and hide the column
			// if browsable is set to hide.
			//
			// ----------------------------------------------------------------------------------
			if ( null != this.propertyDescriptor && null != this.propertyDescriptor.Attributes )
			{
				// Hide the column if the browsable attribute of the property is No.
				//
				if ( this.propertyDescriptor.Attributes.Contains( BrowsableAttribute.No ) )
					this.hidden = true;
			}
			// ----------------------------------------------------------------------------------

			// SSP 9/23/03 UWG2672
			// If the property descriptor is not browsable then hide the column. Apparently 
			// IsBrowsable can be false even when the Attributes doesn't contain 
			// BrowsableAttribute.No (look at the above code).
			//
			// ----------------------------------------------------------------------------------
			if ( ! this.hidden && null != this.propertyDescriptor && ! this.propertyDescriptor.IsBrowsable )
				this.hidden = true;
			// ----------------------------------------------------------------------------------

			// SSP 8/12/03 - Optimizations
			// Instead of checking if the property descriptor's type is assignable from IList
			// like we do in IsChaptered, cache whether the property descriptor is chaptered or not
			// when the column is initialized with a property descriptor.
			//
			// --------------------------------------------------------------------------------------
            //
            // MBS 10/20/08 - TFS9024
            // Reworked this logic to take into consideration IList<>
            //
            //this.cachedIsChaptered = ( null != this.propertyDescriptor  &&
            //    typeof(IList).IsAssignableFrom(this.propertyDescriptor.PropertyType)   && 
            //    !typeof(Array).IsAssignableFrom(this.propertyDescriptor.PropertyType) );
            if (null != this.propertyDescriptor)
            {
                Type propertyType = this.propertyDescriptor.PropertyType;

                // We will now support a child band if it implements IEnumerable<T>, since we can get the
                // underlying type information.
                bool isGenericAssignable = DataBindingUtils.ImplementsGenericIEnumerable(propertyType);

                this.cachedIsChaptered = (typeof(IList).IsAssignableFrom(propertyType) || isGenericAssignable) &&
                    !typeof(Array).IsAssignableFrom(propertyType);
            }
            else
                this.cachedIsChaptered = false;
			// --------------------------------------------------------------------------------------

			// SSP 8/11/03 - Optimizations
			// In CellUIElement.PositionChildElements we check if the cell value is an Image or not.
			// In most cases we don't need to do that. So added a flag off the column that will 
			// prevent the cell ui element from checking if the cell value is an image for columns
			// that we know won't contain images.
			//
			// --------------------------------------------------------------------------------------
			this.shouldCheckForImageCellValue = true;
			if ( null != this.propertyDescriptor && 
				! ( this.propertyDescriptor is ValuePropertyDescriptor ) )
			{
				Type type = this.propertyDescriptor.PropertyType;
                
				if ( typeof( object ) != type && typeof( System.Drawing.Image ) != type &&
					// MD 8/3/07 - 7.3 Performance
					// IsAssignableFrom is faster than IsSubclassOf
					//! type.IsSubclassOf( typeof( System.Drawing.Image ) ) )
					!typeof( System.Drawing.Image ).IsAssignableFrom( type ) )
					this.shouldCheckForImageCellValue = false;
			}
			// --------------------------------------------------------------------------------------

			// JJD 10/26/01
			// If we have a property descriptor then we are bound
			//
			this.bound = true;

			Debug.WriteLine( "Name: " + this.propertyDescriptor.Name
				+ " descr: " + this.propertyDescriptor.Description
				+ " type: " + this.propertyDescriptor.PropertyType );
			
			Debug.WriteLine( "nullable = " + propertyDescriptor.Converter.CanConvertFrom( typeof( System.DBNull ) ) );
		
			if ( this.IsChaptered ) 
			{
				this.dataType = typeof( ChapteredColumnType );

				// SSP 3/16/04 - Virtual Binding
				// If the child band of this chaptered column already exists, then make
				// use of that instead of adding a new band.
				//
				// ------------------------------------------------------------------------
				UltraGridBand existingChildBand = null != layout
					? layout.Bands.GetChildBand( this ) 
					: null;
				// ------------------------------------------------------------------------

				// JJD 10/02/01 - UWG379
				// Stop adding bands when we reach the max. This prevents
				// endless recursion in certian situations
				//
				// SSP 5/31/02 UWG1152
				// Use the new MaxBandDepthEnforced property instead of MAX_BAND_DEPTH.
				// 
				//if ( this.Band.HierarchicalLevel < UltraGridBand.MAX_BAND_DEPTH - 1 )
				Debug.Assert( null != layout, "No layout off the band !" );
				int maxBandDepthInUse = null != layout ? layout.MaxBandDepthEnforced : UltraGridBand.MAX_BAND_DEPTH;
				if ( band.HierarchicalLevel < maxBandDepthInUse - 1 )				
				{
					// SSP 4/11/02 UWG1055
					// Enclosed band initialization into a try-catch block because
					// the call to BindingContext's indexer property could throw an
					// exception (which is what happened with UWG1055. When the child
					// list has 0 items in it and the BindingContext's indexer
					// property is accessed to get the child binding manager, it throws
					// an exception, and it seems that the binding manager tries to
					// add a row to the list since there are no rows (I assume 
					// temporarily), and if the list does not implement IBindingList
					// it just throws a NotSupportedException since adding of rows
					// is not supported to lists that is not IBindingList).
					// So just catch the exception and don't add the band. This
					// is how the .NET DataGrid behaves. (This I guess just masks
					// the problem as the band won't be loaded, however there is
					// nothing we can do about it since the binding manager is the
					// one that's throwing the exception.)
					//
					try
					{
						// SSP 10/24/06 BR15430
						// Moved code from here into new CreateChildBandBindingManagerHelper method.
						// 
						string newDataMember;
						BindingManagerBase childLM = this.CreateChildBandBindingManagerHelper( out newDataMember );

						// SSP 3/16/04 - Virtual Binding
						// If the child band of this chaptered column already exists, then make
						// use of that instead of adding a new band.
						//
						UltraGridBand childBand = existingChildBand;
							
						// Add the child band 
						//
						if ( null == childBand )
						{
							// SSP 7/23/07 BR24065
							// 
							//childBand = layout.Bands.InternalAdd( this.Band, this, oldBands );
							bool newBandCreated;
							childBand = layout.Bands.InternalAdd( this.Band, this, oldBands, out newBandCreated );

							// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
							// 
							if ( null != layout && NewBandLoadStyle.Hide == layout.NewBandLoadStyleInEffect
								// SSP 7/23/07 BR24065
								// 
								&& newBandCreated )
								childBand.Hidden = true;
						}

						// call the new child bands InitListManager method 
						//
						childBand.InitListManager( childLM, newDataMember, oldBands );
					}
					catch ( Exception exception2 )
					{
						//Debug.Assert( false, "Exception thrown while trying to initialize child binding manager.\n" + exception.Message );

						// SSP 9/15/06 - NAS 6.3
						// Raise Error event.
						// 
						if ( null != layout && null != layout.Grid )
							layout.Grid.InternalHandleDataError( SR.GetString( "DataError_CurrencyManagerRetrievalFailure" ), exception2, true );
					}
				}
                
				return;
			}
 
			this.dataType = this.propertyDescriptor.PropertyType;

			// SSP 2/23/06 BR09967
			// Change in the data type means that the column's CellWidthDefault property should return a
			// different value.
			// 
			// ------------------------------------------------------------------------------------------
			this.ClearFontCache( );

			if ( null != band )
				band.DirtyRowLayoutCachedInfo( );
			// ------------------------------------------------------------------------------------------

			// SSP 3/10/04 - Virtual Binding Related
			// Since the data type could have changed, dirty the default editor.
			//
			this.DirtyDefaultEditor( );

			// SSP 1/11/05
			// Added support for defaulting the Caption to the DataColumn's Caption and then
			// to the PropertyDescriptor's DisplayName. Now the order will be 
			// ColumnHeader.Caption, DataColumn.Caption, PropertyDescriptor.DisplayName and
			// then the PropertyDescriptor.Name. Added code to cache the underlying DataColumn 
			// object.
			//
			this.cachedDataColumn_lastPropDesc = null;
			this.cachedDataColumn = null;
		}

		/// <summary>
		/// The property descriptor is used for data binding
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public PropertyDescriptor PropertyDescriptor
		{
			get
			{
				return this.propertyDescriptor;
			}
		}
        

		/// <summary>
		/// Returns or sets the level of a band in which a column resides.
		/// </summary>
		/// <remarks>
		/// <p class="body">Typically, each data record in a band occupies a single row of the grid, with all of the cells stretching from left to right. In some circumstances, you may want to have a single record occupy more than one row. For example, if you have address data stored in a record, you may want to have the first and last name fields on one level, the street address on a second level, and city, state and postal code fields on a third level. The <b>LevelCount</b> property is used to specify how many levels of data a band will display for each record in the data source. The <b>Level</b> property is used to determine which level of the band a column occupies.</p>
		/// <p class="body">Levels work in conjunction with groups to create blocks of fields within a band. If you do not have any groups specified for a band, the <b>Level</b> property will have no effect. If one or more groups are specified (and column moving is enabled within the group or band) you can re-arrange fields vertically within the group by dragging their column headers to different levels.</p>
		/// <p class="body">This property is 0-based; to specify that a column should reside in the first level of a band, set this property to 0.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_Level")]
		[LocalizedCategory("LC_Appearance")]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int Level
		{
			get
			{
				return this.level;
			}
			set
			{
				// SSP 11/16/01 UWG474
				// Check this before the below condition because we want to throw 
				// the exception whenever user tries to set the level to out of
				// range value.
				//
				if ( value < 0 || value >= this.Band.LevelCount )
					throw new ArgumentOutOfRangeException(Shared.SR.GetString("LE_ArgumentOutOfRangeException_69"), value, Shared.SR.GetString("LE_ArgumentOutOfRangeException_311") );
					
				if ( this.level != value && this.Band.LevelCount > 1 )
				{					
					
					this.InternalSetLevel( value, true );
				}
			}
		}

		/// <summary>
		/// Returns true if this is the first visible column on this level.
		/// Note: Multiple levels are only supported within groups.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		[Browsable(false)]
		public bool IsFirstVisibleColumnOnLevel
		{
			get
			{
				
				//
				return ( this == this.band.OrderedColumnHeaders.FirstVisibleHeader.Column );
			}
		}
 
		/// <summary>
		/// The index of this column in the band's column collection
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Index</b> property is set by default to the order of the creation of objects in a collection. The index for the first object in a collection will always be zero.</p>
		/// <p class="body">The value of the <b>Index</b> property of an object can change when objects in the collection are reordered, such as when objects are added to or deleted from the collection. Since the <b>Index</b> property may change dynamically, it may be more useful to refer to objects in a collection by using its <b>Key</b> property.</p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		[LocalizedDescription("LD_AppearanceCache_P_Index")]
		[LocalizedCategory("LC_Default")]
		[MergableProperty(false)]
		public int Index
		{
			get
			{
				try
				{
					// if the index hasn't changed for the last index
					// get (the normal case then we can return it without
					// having to call IndexOf below)
					//
					// SSP 1/27/05
					//
					//if ( this == this.band.Columns[this.index] )
					// SSP 4/5/05 - Optimizations
					//
					//if ( this.index >= 0 && this == this.band.Columns[this.index] )
					// MD 5/22/09
					// Found while fixing TFS16348
					// An index out of range was occurring here when changing the column name of the data source.
					// We just needed to check the upper bound when checking the cached index as well.
					//if ( this.index >= 0 && this == this.band.columns.listInternal[this.index] )
					if ( this.index >= 0 &&
						this.index < this.band.columns.listInternal.Count &&
						this == this.band.columns.listInternal[ this.index ] )
					{
						return this.index;
					}
				}
				catch(Exception)
				{
					Debug.Assert( null != this.band.Columns && null != this.band.Columns.listInternal );
				}
            
				// cache the index before returning it so that
				// if the index doesn';t change (the normal case)
				// the test above will be more efficient
				//
				this.index = this.band.Columns.IndexOf(this);

				return this.index;

			}
		}

		/// <summary>
		/// Returns or sets a string used to control the formatting of displayed text.
		/// </summary>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.FormatInfo"/>
		/// <remarks>
		/// <p class="body">The <b>Format</b> property is similar to the Visual Basic <b>Format</b> function, and supports all of the named arguments and literal strings supported by that function when the UltraGrid is being used in Visual Basic. In other host environments, the <b>Format</b> property provides a subset of the <b>Format</b> function's capabilites, including the use of named arguments.</p>
		/// <p class="body">The <b>Format</b> property applies only to cells that are not in edit mode.</p>
		/// <p class="body">The underlying .NET formatting mechanism is used to format the value. See .NET help for more information regarding list of the named formats that are supported.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_Format")]
		[LocalizedCategory("LC_Appearance")]
		[ Localizable( true ) ]
		public string Format
		{
			get
			{
				return this.format;
			}
			set
			{
				if ( this.format != value )
				{
					this.format = value;
					this.NotifyPropChange( PropertyIds.ColumnFormat );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		// SSP 8/8/02
		// Made this internal protected.
		//
		//protected bool ShouldSerializeFormat() 
		internal protected bool ShouldSerializeFormat( ) 
		{
			return this.format != null;
		}

		/// <summary>
		/// Resets Format to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetFormat() 
		{
            // MRS NAS v8.3 - Unit Testing
			//this.format = null;
            this.Format = null;
		}
		

		// JJD 1/09/02 - UWG295, UWG885
		// Added formatinfo property
		//
		/// <summary>
		/// Gets or sets the culture specific information used to determine how values are formatted.
		/// </summary>
		/// <value>An object that implements the IFormatProvider interface, such as the CultureInfo class.</value>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.Format"/>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IFormatProvider FormatInfo
		{
			get
			{
				return this.formatInfo;
			}

			set
			{
				if ( this.formatInfo != value )
				{
					this.formatInfo = value;
					
					// SSP 1/14/02
					// Also clear the cached parsed mask so that it gets
					// recreated according to the new culture info settings
					// (if formatInfo is a CultureInfo instance).
					//
					this.ClearCachedParsedMask( );

					this.NotifyPropChange( PropertyIds.ColumnFormat );
				}
			}
		}

		/// <summary>
		/// Returns whether the column is read only (based on its bindings)
		/// </summary>
		/// <returns></returns>
		[Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
		public bool IsReadOnly
		{
			get
			{
				
				// SSP 4/23/04 UWG3010
				// Changed the name of IsReadOnly off UltraGridBand to IsBoundListReadOnly. When the 
				// underlying bound list is read-only we should still allow editing unbound columns.
				//				
				//if ( this.Band.IsReadOnly )
				if ( this.Band.IsBoundListReadOnly && this.IsBound )
					return true;

				// SSP 12/9/05 BR07972
				// Check for null PropertyDescriptor. This comes up at design time when
				// data structure has been extracted from a data source by there is no
				// data source attached. Columns have no property descriptors in this
				// case.
				// 
				//return ( this.IsBound && this.PropertyDescriptor.IsReadOnly );
				PropertyDescriptor pd = this.PropertyDescriptor;
				return this.IsBound && ( null == pd || pd.IsReadOnly );
			}
		}

		/// <summary>
		/// Returns or sets the UltraGridGroup object that the column is associated with. This property is not available at design-time. 
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Group</b> property of an object refers to a specific group of columns in the grid as defined by an UltraGridGroup object. You use the <b>Group</b> property to access the properties of a specified UltraGridGroup object, or to return a reference to an UltraGridGroup object. An UltraGridGroup is a group of columns that appear together in the grid, and can be resized, moved or swapped together as a unit. Columns in the same group share a group header, and can be arranged into a multi-row layout within the group, with different columns occupying different vertical levels within a single row of data. UltraGridGroups also help with the logical arrangement of columns within the grid.</p>
		/// </remarks>
		[Browsable(false)]
		[LocalizedDescription("LD_AppearanceCache_P_Group")]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.UltraWinGrid.UltraGridGroup Group
		{
			get
			{
				return this.group;
			}
			set
			{
				if ( value != this.Group )
				{
					this.SetGroup(value, -1, this.level );
				}
			}
		}

		// MD 1/19/09 - Groups in RowLayout
        /// <summary>
        /// Returns the group which contains the column.
        /// </summary>
        /// <remarks>
        /// <para class="body">The containing group is determined differently depending on the <see cref="UltraGridBand.RowLayoutStyle"/> property. When RowLayoutStyle is None, then the Group property of the Column is used. When RowLayoutStyle is ColumnLayout, no grouping is supported and so this property always returns null. When RowLayoutStyle is set to GroupLayout, then the group is determined by the <see cref="Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo.ParentGroup"/> property of the RowLayoutColumnInfo on the column.</para>
        /// </remarks>
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public UltraGridGroup GroupResolved
		{
			get
			{
				if ( this.band == null )
					return null;

				switch ( this.band.RowLayoutStyle )
				{
					case RowLayoutStyle.ColumnLayout:
						return null;

					case RowLayoutStyle.GroupLayout:
						return this.RowLayoutColumnInfo.ParentGroup;

					case RowLayoutStyle.None:
						return this.Group;

					default:
						Debug.Fail( "Unknown row layout style: " + this.band.RowLayoutStyle );
						return null;
				}
			}
		}
	
		/// <summary>
		/// Indicators whether this is a bound or unbound column.
		/// This property is read-only.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public bool IsBound
		{
			get
			{
				// JJD 10/29/01
				// Return the bound flag since a saved layout doesn't
				// have its column's propertyDescriptor set. Therefore,
				// we need to rely on the bound flag that gets serialized.
				//
				// return (null != this.propertyDescriptor);
				return this.bound;
			}
		}

		/// <summary>
		/// Returns or sets a value that uniquely identifies an object in a collection.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Key</b> property is a unique, user-defined object identification string that can be used interchangeably with the <b>Index</b> of an object when accessing it through code. If you attempt to assign a value to the <b>Key</b> property is not unique in the collection, an error will occur.</p>
		/// <p class="body">The value of the <b>Index</b> property of an object can change when objects in the collection are reordered, such as when you add or remove objects. If you expect the <b>Index</b> property to change dynamically, refer to objects in a collection using the <b>Key</b> property. In addition, you can use the <b>Key</b> property to make your program "self-documenting" by assigning meaningful names to the objects in a collection.</p>
		/// <p class="body">You can set the <b>Key</b> property when you use the <b>Add</b> method to add an object to a collection. In some instances, the <b>Key</b> property of an object may be blank if that object does not appear in a collection.</p>
		/// <p class="body">Also, note that the uniqueness of keys is only enforced when the <b>Key</b> property is set to a value. If a collection supports objects with blank keys, that collection may contain multiple objects that whose <b>Key</b> property is empty. In that case, you must use <b>Index</b> property to differentiate between the objects that have blank keys.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_Key")]
		[LocalizedCategory("LC_Default")]
		[MergableProperty(false)]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override String Key
		{
			get
			{
				if ( this.IsBound && this.propertyDescriptor != null )
					return this.propertyDescriptor.Name;

				return base.Key;
			}
			set
			{
				if ( this.IsBound && 
					this.band != null &&
					this.band.Layout != null &&
					// SSP 3/10/06
					// Added the following line. Allow setting the band's and column's Key at design time.
					// This is useful when one creates a data structure by "Manually Define Data Schema"
					// feature of the designer and then decides to change the key of a column or a band 
					// because the runtime data source will have a different name for the band or the 
					// column.
					// 
					( null != this.Layout.Grid && null != this.Layout.Grid.DataSource ) &&
					// SSP 5/7/03 - Export Functionality
					//
					//( this.band.Layout.IsDisplayLayout || this.band.Layout.IsPrintLayout ) )
					( this.band.Layout.IsDisplayLayout || this.band.Layout.IsPrintLayout || this.band.Layout.IsExportLayout ) )
					throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_71"));

				if ( ! this.Key.Equals( value ) )
				{
					// SSP 8/31/04 UWC68
					//
					this.FormulaHolder.RemoveReferenceFromCalcNetwork( );

					base.Key = value;

					// SSP 8/31/04 UWC68
					//
					this.FormulaHolder.AddReferenceToCalcNetwork( );

					this.NotifyPropChange( PropertyIds.Key, null );
				}			
			}
		}

		// SSP 3/10/04 - Virtual Binding Related
		//
		internal void InternalResetInternedKey( )
		{
			this.ResetInternedKey( );
		}

		/// <summary>
		/// Returns True if this property is not set to its default value.
		/// </summary>
		/// <returns></returns>
		public override bool ShouldSerializeKey() 
		{
			// JJD 10/26/01
			// Column key's for bound columns can't be set 
			// so we should return false 
			//
			if ( this.bound )
				return false;

			return this.Key.Length > 0;
			
		}
 
		/// <summary>
		/// Resets Key to its default value ("").
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public override void ResetKey() 
		{
			if ( !this.IsBound )
				base.Key = string.Empty;
		}


		/// <summary>
		/// Returns or sets a value that determines how an object will behave when it is activated.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Activation</b> property of the UltraGridCell object is subordinate to the settings of the Activation properties of the UltraGridRow and UltraGridColumn objects that contain the cell. If either the cell's row or column has its <b>Activation</b> property set to False, the cell cannot be activated, regardless of its own setting for <b>Activation</b>. The setting of the other type of parent also has no effect; setting <b>Activation</b> to False on a cell's row makes the cell inactive regardless of the setting of its column.</p>
		/// <seealso cref="UltraGridOverride.CellClickAction"/>
		/// <seealso cref="UltraGridRow.Activation"/>
		/// <seealso cref="UltraGridCell.Activation"/>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_CellActivation")]
		[LocalizedCategory("LC_Behavior")]
		public Activation CellActivation
		{
			get 
			{
				return this.activation;
			}
			set
			{
				if(this.activation != value)
				{
                    // MRS NAS v8.3 - Unit Testing
                    //if ( !Enum.IsDefined( typeof(Activation), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_32") );
                    if (!Enum.IsDefined(typeof(Activation), value))
                        throw new InvalidEnumArgumentException("CellActivation", (int)value, typeof(Activation));

					this.activation=value;
					this.NotifyPropChange( PropertyIds.Activation, null );
				}
			}
		}
		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeCellActivation() 
		{
			return this.activation != Activation.AllowEdit;
		}
 
		/// <summary>
		/// Resets Activation to its default value (Allow Edit).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetCellActivation() 
		{
			this.CellActivation = Activation.AllowEdit;
		}
 
		/// <summary>
		/// Determines the formatting attributes that will be applied to the cells in a band or the grid.
		/// <see cref="Infragistics.Win.Appearance"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>CellAppearance</b> property is used to specify the appearance of all the cells in a band or the grid. When you assign an Appearance object to the <b>CellAppearance</b> property, the properties of that object will be applied to all the cells belonging to the object specified. You can use the <b>CellAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the cells, for example:</p> 
		/// <p class="code">UltraWinGrid1.Override.CellAppearance.BackColor = vbYellow</p>
		/// <p class="body">Because you may want the cells to look different at different levels of a hierarchical record set, <b>CellAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different cell appearances for each band by assigning each Band object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the cells of that band will use the grid-level setting of <b>CellAppearance</b>.</p>
		/// <p class="body">You can override the <b>CellAppearance</b> setting for specific cells by setting the <b>Appearance</b> property of the UltraGridCell object directly. The cell will always use the values of its own Appearance object before it will use the values inherited from the Appearance object specified by the <b>CellAppearance</b> property of the band it occupies.</p>
		/// <p class="body">If any of the properties of the Appearance object specified for the <b>CellAppearance</b> property are set to default values, the properties from the Appearance object of the row containing the cell are used.</p>
		/// <seealso cref="UltraGridOverride.CellAppearance"/>
		/// <seealso cref="UltraGridOverride.RowAppearance"/>
		/// <seealso cref="UltraGridRow.CellAppearance"/>
		/// <seealso cref="UltraGridCell.Appearance"/>
		/// <seealso cref="UltraGridOverride.BorderStyleCell"/>
		/// <seealso cref="UltraGridOverride.BorderStyleRow"/>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_CellAppearance")]
		[LocalizedCategory("LC_Appearance")]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		public Infragistics.Win.AppearanceBase CellAppearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.cellAppearanceHolder )
				{
					this.cellAppearanceHolder = new Infragistics.Win.AppearanceHolder();
                    
					// hook up the prop change notifications
					//
					this.cellAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.Layout != null)
					this.cellAppearanceHolder.Collection = this.Layout.Appearances;

				return this.cellAppearanceHolder.Appearance;
			}
			set
			{
				if ( this.cellAppearanceHolder == null				|| 
					!this.cellAppearanceHolder.HasAppearance		|| 
					value != this.cellAppearanceHolder.Appearance )
				{
					if ( null == this.cellAppearanceHolder )
					{
						this.cellAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.cellAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Layout != null)
						this.cellAppearanceHolder.Collection = this.Layout.Appearances;

					this.cellAppearanceHolder.Appearance = value;

					// notify listeners
					//
					this.NotifyPropChange( PropertyIds.CellAppearance );
				}
			}
		}





		/// <summary>
		/// Determines the formatting attributes that will be applied to the buttons in the cells of this column.
		/// </summary>
		[LocalizedDescription("LD_AppearanceCache_P_CellButtonAppearance")]
		[LocalizedCategory("LC_Appearance")]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		public Infragistics.Win.AppearanceBase CellButtonAppearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.cellButtonAppearanceHolder )
				{
					this.cellButtonAppearanceHolder = new Infragistics.Win.AppearanceHolder();
                    
					// hook up the prop change notifications
					//
					this.cellButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.Layout != null)
					this.cellButtonAppearanceHolder.Collection = this.Layout.Appearances;

				return this.cellButtonAppearanceHolder.Appearance;
			}
			set
			{
				if ( this.cellButtonAppearanceHolder == null			|| 
					!this.cellButtonAppearanceHolder.HasAppearance		|| 
					value != this.cellButtonAppearanceHolder.Appearance )
				{
					if ( null == this.cellButtonAppearanceHolder )
					{
						this.cellButtonAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.cellButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Layout != null)
						this.cellButtonAppearanceHolder.Collection = this.Layout.Appearances;

					this.cellButtonAppearanceHolder.Appearance = value;

					// notify listeners
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.CellButtonAppearance );
				}
			}
		}


		/// <summary>
		/// Returns true if an CellButtonAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasCellButtonAppearance
		{
			get
			{
				return ( null != this.cellButtonAppearanceHolder &&
					this.cellButtonAppearanceHolder.HasAppearance );
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeCellButtonAppearance() 
		{
			return ( this.HasCellButtonAppearance &&
				this.cellButtonAppearanceHolder.ShouldSerialize() );
		}
 
		/// <summary>
		/// Resets CellButtonAppearance
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetCellButtonAppearance() 
		{
			if ( this.HasCellButtonAppearance )
			{

				//				// remove the prop change notifications
				//				//
				//				this.cellButtonAppearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				//                    
				//				this.cellButtonAppearanceHolder.Reset();
				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//this.cellButtonAppearanceHolder.Appearance.Reset();

				// Call reset on the holder instead of the appearance
				//
				this.cellButtonAppearanceHolder.Reset();

				// Notify listeners that the appearance has changed. 
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.CellButtonAppearance );
			}
		}

		/// <summary>
		/// Returns a reference to a ValueList object containing the list of values used by a column. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a ValueList object that can be used to set properties of, and invoke methods on, the valuelist that is associated with a column. You can use this reference to access any of the returned valuelist's properties or methods.</p>
		/// <p class="body">This property is also used to assign a particular valuelist object to a column. Once assigned, the valuelist enables a column to use the dropdown list styles and intelligent data entry, specified by the <b>Style</b> and <b>AutoEdit</b> properties, respectively, of the column for which this property is set.</p>
		/// </remarks>
		[ LocalizedDescription("LD_AppearanceCache_P_ValueList")]
		[ LocalizedCategory("LC_Behavior")]
		[Editor( typeof( Infragistics.Win.ValueListEditor ), typeof( UITypeEditor ) )]
		[ TypeConverter(typeof(IValueListTypeConverter))]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public IValueList ValueList
		{
			get
			{
				IValueList valueList = this.Constraint.Enumeration;
				// AS - 10/30/01
				// Return the valuelist if we have it.
				//
				if (valueList != null)
				{
					// SSP 9/13/02 UWG1656
					// See if the valueList has been disposed. If so set this property to
					// null and return null.
					//
					// MD 8/7/07 - 7.3 Performance
					// FxCop - Do not cast unnecessarily
					//if ( valueList is Control && ((Control)valueList).IsDisposed )
					Control valueListControl = valueList as Control;

					if ( valueListControl != null && valueListControl.IsDisposed )
					{
						// SSP 11/2/04 UWG3764
						// Added anti-recursion flag for the ValueList property. Enclosed the existing
						// code into the if block.
						//
						if ( ! this.inValueListSet )
						{
							this.ValueList = null;
							return null;
						}
					}

					return valueList;
				}

				// SSP 2/22/06 BR10292
				// 
				bool valueListChanged = false;

				// Otherwise, if we have a valuelist id, try to get the valuelist from that.
				UltraGridLayout layout = this.Layout;
				bool hasIdOrControlName = false;
				if (this.valueListId != 0 && null != layout )
				{
					valueList = layout.ValueLists.GetValueListFromInternalID(this.valueListId);

					// SSP 2/22/06 BR10292
					// Set valueListChanged flag to true if the value list actually changed.
					// 
					valueListChanged = valueList != this.Constraint.Enumeration;

					// JAS 4/22/05 BR03540
					// We need to store a reference to this object now otherwise we will lose it forever.
					//
					this.ValueList = valueList;

					// SSP 8/13/02 UWG1567
					// Dirty the default editor so that we use an appropriate editor since
					// the column will have a value list now.
					//
					// SSP 2/22/06 BR10292
					// Call DirtyDefaultEditor only if valueListChanged is true.
					// 
					if ( valueListChanged )
						this.DirtyDefaultEditor( );

					hasIdOrControlName = true;
				}
				else if (this.valueListControlName != null) // or if we have a control name, try to locate the control
				{
					// JAS 4/22/05 BR03540
					// The return value of this method must be given a reference to the object first,
					// then we can copy it into the member variable.
					//
					// try to find the dropdown control
					//this.ValueList = FindDropDownFromName(this.valueListControlName);
					valueList = FindDropDownFromName(this.valueListControlName);

					// SSP 2/22/06 BR10292
					// Set valueListChanged flag to true if the value list actually changed.
					// 
					valueListChanged = valueList != this.Constraint.Enumeration;

					this.ValueList = valueList;
					
					// SSP 8/13/02 UWG1567
					// Dirty the default editor so that we use an appropriate editor since
					// the column will have a value list now.
					//
					// SSP 2/22/06 BR10292
					// Call DirtyDefaultEditor only if valueListChanged is true.
					// 
					if ( valueListChanged )
						this.DirtyDefaultEditor( );

					hasIdOrControlName = true;
				}

				// if we've failed or succeeded, clear the links back to the id and control name
				// SSP 8/30/05 BR05725
				// Only clear the valueListControlName and valueListId if we were able to get a value
				// list or if we aren't deserializing. During deserialization this property gets called 
				// valuelist from valueListControlName or valueListId.
				// 
				if ( hasIdOrControlName && ( null != valueList || null != layout 
					&& ! layout.InitializingAllSubItems && null != layout.Grid 
					&& ! layout.Grid.Initializing && ! layout.Grid.DesignMode ) )
				{
					this.valueListId = 0;
					this.valueListControlName = null;
				}

				return valueList;

				// JAS 2005 v2 XSD Support
				// Made the ValueList property delegate to the Constraint.Enumeration property.
				//
				#region Obsolete Code
//				// AS - 10/30/01
//				// Return the valuelist if we have it.
//				//
//				if (this.valuelist != null)
//				{
//					// SSP 9/13/02 UWG1656
//					// See if the valueList has been disposed. If so set this property to
//					// null and return null.
//					//
//					if ( this.valuelist is Control && ((Control)this.valuelist).IsDisposed )
//					{
//						// SSP 11/2/04 UWG3764
//						// Added anti-recursion flag for the ValueList property. Enclosed the existing
//						// code into the if block.
//						//
//						if ( ! this.inValueListSet )
//						{
//							this.ValueList = null;
//							return null;
//						}
//					}
//
//					return this.valuelist;
//				}
//
//				// Otherwise, if we have a valuelist id, try to get the valuelist from that.
//				if (this.valueListId != 0 && this.band != null && this.band.Layout != null)
//				{
//					this.valuelist = this.band.Layout.ValueLists.GetValueListFromInternalID(this.valueListId);
//
//					// SSP 8/13/02 UWG1567
//					// Dirty the default editor so that we use an appropriate editor since
//					// the column will have a value list now.
//					//
//					this.DirtyDefaultEditor( );
//				}
//				else if (this.valueListControlName != null) // or if we have a control name, try to locate the control
//				{
//					// try to find the dropdown control
//					this.valuelist = FindDropDownFromName(this.valueListControlName);
//					
//					// SSP 8/13/02 UWG1567
//					// Dirty the default editor so that we use an appropriate editor since
//					// the column will have a value list now.
//					//
//					this.DirtyDefaultEditor( );
//				}
//
//				// if we've failed or succeeded, clear the links back to the id and control name
//				this.valueListId = 0;
//				this.valueListControlName = null;
//
//				return this.valuelist;
				#endregion // Obsolete Code
			}

			set
			{
				if( this.Constraint.Enumeration != value )
				{
					// SSP 11/2/04 UWG3764
					// Added flag so the ValueList get above knows we are currently in the set so it
					// doesn't call the set from there. Look in the get of the property for more info.
					//
					bool origValueListSet = this.inValueListSet;
					this.inValueListSet = true;
					try
					{
						// SSP 11/14/01 UWG732
						// If the ValueList being assigned does not have a layout set, then
						// set the layout to our layout, otherwise make sure that the layout
						// on the ValueList is the same as ours.
						//
						// MD 8/7/07 - 7.3 Performance
						// FxCop - Do not cast unnecessarily
						//if ( null != value &&  value is ValueList )
						//{
						//    Infragistics.Win.ValueList vl = (ValueList)value;
						ValueList vl = value as ValueList;

						if ( vl != null )
						{
							// SSP 1/7/03 UWG1905
							// Check for the layout being null. This gets called from the deserialization contructor
							// of the column. At that point the band backreference is not hooked up yet.
							//
							if ( null != this.Layout )
							{							
								// SSP 9/19/02 UWG1685
								// Also if the Appearances collection is disposed of, then 
								// initialize it to our layout's appearances collection as well.
								//
								//if ( null == vl.Appearances )
								if ( null == vl.Appearances || vl.Appearances.Disposed )
									vl.InitAppearances( this.Band.Layout.Appearances );

								else if ( vl.Appearances != this.band.Layout.Appearances )
									throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_72"), Shared.SR.GetString("LE_ArgumentException_312") );
							}
						}

						// SSP 8/8/02 UWG1310
						// When a valueList is assigned to a column, exit the edit mode on the
						// cell otherwise it will be chaotic (cell when exitting the edit mode 
						// will work on the new value list rather than the old value list that 
						// it went in edit mode with and activated).
						//
						if ( null != this.Layout && null != this.Layout.ActiveCell &&
							this.Layout.ActiveCell.IsInEditMode &&
							this == this.Layout.ActiveCell.Column )
						{
							this.Layout.ActiveCell.ExitEditMode( false, true );
						}
					
						// MD 8/7/07 - 7.3 Performance
						// This is not neeed anymore
						//Infragistics.Win.ValueList valuelist = value as Infragistics.Win.ValueList;

						// remove the old prop change notifications
						//
						if ( null != this.Constraint.Enumeration &&
							this.Constraint.Enumeration is Infragistics.Win.ValueList )
							//valuelist.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
							//RobA UWG385 9/28/01 as of now we are only hooking up Infragistics.Win.UltraWinGrid.ValueList 
							//so we only have to unhook ValueList
							((ValueList)this.Constraint.Enumeration).SubObjectPropChanged -= this.SubObjectPropChangeHandler;
                  
						this.Constraint.Enumeration = value;

						// JAS 2005 v2 XSD Support
						// If this property had been set by the EnforceXsdConstraints method, then
						// clear the bit which represents this property so that the ClearXsdConstraints
						// method will know not to reset this value.
						//
						if( (this.XsdSuppliedConstraints & XsdConstraintFlags.Enumeration) != 0 )
							this.XsdSuppliedConstraints &= ~XsdConstraintFlags.Enumeration;

						// hook up the new prop change notifications
						//
						// MD 8/7/07 - 7.3 Performance
						// Use the other cached value list variable from above
						//if ( null != valuelist )
						//    valuelist.SubObjectPropChanged += this.SubObjectPropChangeHandler;
						if ( null != vl )
							vl.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					
						// SSP 4/8/02
						// Dirty the default editor flag because we may need to use a
						// different editor since we have a value list now.
						//
						this.DirtyDefaultEditor( );

						//else if ( null != this.dropDown )
						//	this.dropDown.ResetContents();

						// Initialize dropdown, if necessary
						//	if ( null != this.Layout.ActiveCell &&
						//		this.Layout.ActiveCell.Column == this )
						//		this.Layout.ActiveCell.DisplayColumnControl();

						this.NotifyPropChange( PropertyIds.ValueList, null );
					}
					finally
					{
						this.inValueListSet = origValueListSet;
					}
				}

				// JAS 2005 v2 XSD Support
				// Made the ValueList property delegate to the Constraint.Enumeration property.
				//
				#region Obsolete Code
//				if( this.valuelist != value )
//				{
//					// SSP 11/2/04 UWG3764
//					// Added flag so the ValueList get above knows we are currently in the set so it
//					// doesn't call the set from there. Look in the get of the property for more info.
//					//
//					bool origValueListSet = this.inValueListSet;
//					this.inValueListSet = true;
//					try
//					{
//						// SSP 11/14/01 UWG732
//						// If the ValueList being assigned does not have a layout set, then
//						// set the layout to our layout, otherwise make sure that the layout
//						// on the ValueList is the same as ours.
//						//
//						if ( null != value &&  value is ValueList )
//						{
//							Infragistics.Win.ValueList vl = (ValueList)value;
//
//							// SSP 1/7/03 UWG1905
//							// Check for the layout being null. This gets called from the deserialization contructor
//							// of the column. At that point the band backreference is not hooked up yet.
//							//
//							if ( null != this.Layout )
//							{							
//								// SSP 9/19/02 UWG1685
//								// Also if the Appearances collection is disposed of, then 
//								// initialize it to our layout's appearances collection as well.
//								//
//								//if ( null == vl.Appearances )
//								if ( null == vl.Appearances || vl.Appearances.Disposed )
//									vl.InitAppearances( this.Band.Layout.Appearances );
//
//								else if ( vl.Appearances != this.band.Layout.Appearances )
//									throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_72"), Shared.SR.GetString("LE_ArgumentException_312") );
//							}
//						}
//
//						// SSP 8/8/02 UWG1310
//						// When a valueList is assigned to a column, exit the edit mode on the
//						// cell otherwise it will be chaotic (cell when exitting the edit mode 
//						// will work on the new value list rather than the old value list that 
//						// it went in edit mode with and activated).
//						//
//						if ( null != this.Layout && null != this.Layout.ActiveCell &&
//							this.Layout.ActiveCell.IsInEditMode &&
//							this == this.Layout.ActiveCell.Column )
//						{
//							this.Layout.ActiveCell.ExitEditMode( false, true );
//						}
//					
//						Infragistics.Win.ValueList valuelist = value as Infragistics.Win.ValueList;
//						// remove the old prop change notifications
//						//
//						if ( null != this.valuelist &&
//							this.valuelist is Infragistics.Win.ValueList )
//							//valuelist.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
//							//RobA UWG385 9/28/01 as of now we are only hooking up Infragistics.Win.UltraWinGrid.ValueList 
//							//so we only have to unhook ValueList
//							((ValueList)this.valuelist).SubObjectPropChanged -= this.SubObjectPropChangeHandler;
//                  
//						this.valuelist = value;
//
//						// hook up the new prop change notifications
//						//
//						if ( null != valuelist )
//							valuelist.SubObjectPropChanged += this.SubObjectPropChangeHandler;
//					
//						// SSP 4/8/02
//						// Dirty the default editor flag because we may need to use a
//						// different editor since we have a value list now.
//						//
//						this.DirtyDefaultEditor( );
//
//						//else if ( null != this.dropDown )
//						//	this.dropDown.ResetContents();
//
//						// Initialize dropdown, if necessary
//						//	if ( null != this.Layout.ActiveCell &&
//						//		this.Layout.ActiveCell.Column == this )
//						//		this.Layout.ActiveCell.DisplayColumnControl();
//
//						this.NotifyPropChange( PropertyIds.ValueList, null );
//					}
//					finally
//					{
//						this.inValueListSet = origValueListSet;
//					}
//				}
				#endregion // Obsolete Code
			}
		}
		
		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeValueList() 
		{
			//RobA 10/22/01 implemented
			return this.HasValueList;		
		}
 
		/// <summary>
		/// Resets the ValueList property to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetValueList() 
		{
			// JAS 2005 v2 XSD Support
			//
			//RobA 10/22/01 implemented
//			this.valuelist = null;
			this.ValueList = null;

			// AS - 10/30/01
			this.valueListId = 0;
			this.valueListControlName = null;

			// SSP 8/13/02 UWG1567
			// 
			this.DirtyDefaultEditor( );
		}

		internal bool HasValueList			// AS 9/25/01 Changed from public to internal
		{ 
			get
			{
				// JAS 2005 v2 XSD Support
				//
				// AS - 10/30/01
				//return (null != this.valuelist || this.valueListId != 0 || null != this.valueListControlName) ;
				return (this.Constraint.HasEnumeration || this.valueListId != 0 || null != this.valueListControlName) ;
			}
		}


		/// <summary>
		/// Returns true if an CellAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasCellAppearance
		{
			get
			{
				return ( null != this.cellAppearanceHolder &&
					this.cellAppearanceHolder.HasAppearance );
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeCellAppearance() 
		{
			return ( this.HasCellAppearance &&
				this.cellAppearanceHolder.ShouldSerialize() );
		}
 
		/// <summary>
		/// Reset Cells appearance
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetCellAppearance() 
		{
			if ( this.HasCellAppearance )
			{

				//				// remove the prop change notifications
				//				//
				//				this.cellAppearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				//                    
				//				this.cellAppearanceHolder.Reset();
				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//this.cellAppearanceHolder.Appearance.Reset();

				// Call reset on the holder instead of the appearance
				//
				this.cellAppearanceHolder.Reset();

				// Notify listeners that the appearance has changed. 
                // MRS NAS v8.3 - Unit Testing
				//this.NotifyPropChange(PropertyIds.Appearance);
                this.NotifyPropChange(PropertyIds.CellAppearance);
			}
		}


        //  BF 3/7/08  FR09238 - AutoCompleteMode
        #region Obsolete code
        ///// <summary>
        ///// Determines if the column will support <b>AutoEdit</b> (automatic value completion).
        ///// </summary>
        ///// <remarks>
        ///// <p class="body">This property applies only to columns that have their <b>ValueList</b> property set to a populated value list. When a list of pre-defined values exists for the column, setting the <b>AutoEdit</b> property to True will enable automatic edit value completion for the cells of that column, based on the values in the value list.</p>
        ///// <p class="body">When <b>AutoEdit</b> is True and the user types a character in a cell's editing area, the control will search the contents of the ValueList to see if it contains a value that begins with the same character. If it does, this value will appear in the editing area, with all of its characters highlighted except the one that the user typed. If the user types a second character, the control will check to see if it is the next highlighted character is in the value that appeared. If it is, the value stays and the character typed becomes deselected. If the second character does not appear in the value, the control searches the ValueList again for a value that begins with the first two characters that were typed. If one exists, it appears in the edit area; otherwise the selected text is removed and no more searching takes place. This process continues until the user shifts the input focus away from the cell.</p>
        ///// <p class="body">If no ValueList is specified for the column, the <b>AutoEdit</b> property has no effect.</p>
        ///// </remarks>
        //[LocalizedDescription("LD_AppearanceCache_P_AutoEdit")]
        //[LocalizedCategory("LC_Behavior")]
        //public bool AutoEdit
        //{
        //    get 
        //    {
        //        return this.autoEdit;
        //    }
        //    set
        //    {
        //        if(this.autoEdit != value)
        //        {
        //            this.autoEdit = value;
        //            this.NotifyPropChange( PropertyIds.AutoEdit, null );
        //        }
        //    }
        //}
        ///// <summary>
        ///// Returns true if this property is not set to its default value.
        ///// </summary>
        ///// <returns>Returns true if this property is not set to its default value.</returns>
        //protected bool ShouldSerializeAutoEdit() 
        //{
        //    return this.autoEdit != true;
        //}
 
        ///// <summary>
        ///// Resets AutoEdit to its default value.
        ///// </summary>
        //[EditorBrowsable(EditorBrowsableState.Advanced)]
        //public void ResetAutoEdit() 
        //{
        //    this.autoEdit = true;
        //}
        #endregion Obsolete code

        //  BF 3/7/08  FR09238 - AutoCompleteMode
        #region FR09238 - AutoCompleteMode

            #region AutoEdit
        /// <summary>
        /// Determines if the column will support <b>AutoEdit</b> (automatic value completion).
        /// </summary>
		/// <remarks>
		/// <p class="note">With the 2008 Volume 2 release of NetAdvantage, the AutoEdit property has been deprecated, and replaced by the <see cref="AutoCompleteMode"/> property.</p>
		/// </remarks>
        [LocalizedDescription("LD_AppearanceCache_P_AutoEdit")]
        [LocalizedCategory("LC_Behavior")]
        [Obsolete("This property has been deprecated; use the AutoCompleteMode property instead.", false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
		public bool AutoEdit
		{
			get { return this.AutoEdit_Legacy; }
			set { this.AutoEdit_Legacy = value; }
		}

        internal bool AutoEdit_Legacy
        {
            // MBS 9/15/08 - TFS7306
            // If the user hasn't specifically set the AutoCompleteMode property then this member
            // variable is never going to be set.  Therefore existing applications that use the
            // AutoEdit functionality will be broken because this.autoCompleteMode = Default.
            //
            //get { return this.autoCompleteMode == Infragistics.Win.AutoCompleteMode.Append; }
            get { return this.AutoCompleteModeResolved == Infragistics.Win.AutoCompleteMode.Append; }
            set
            { 
                this.AutoCompleteMode = value ?
                    Infragistics.Win.AutoCompleteMode.Append :
                    Infragistics.Win.AutoCompleteMode.None;
            }
        }

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
		/// <remarks>
		/// <p class="note">With the 2008 Volume 2 release of NetAdvantage, the AutoEdit property has been deprecated, and replaced by the <see cref="AutoCompleteMode"/> property.</p>
		/// </remarks>
        /// <returns>Returns false unconditionally since the property is obsolete.</returns>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
		protected bool ShouldSerializeAutoEdit( )
		{
			return false;
		}

		/// <summary>
		/// Resets the <see cref="AutoEdit"/> property to its default value. (true).
		/// </summary>
		/// <remarks>
		/// <p class="note">With the 2008 Volume 2 release of NetAdvantage, the AutoEdit property has been deprecated, and replaced by the <see cref="AutoCompleteMode"/> property.</p>
		/// </remarks>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
		public void ResetAutoEdit( )
		{
			this.AutoEdit_Legacy = true;
        }
            #endregion AutoEdit

            #region AutoCompleteMode

        /// <summary>
		/// Gets/sets the mode for automatic completion of text typed in the edit portion.
		/// </summary>
		/// <remarks>
		/// <p class="body"></p>
		/// <p class="note">
        /// <b>Note: </b>This property replaces the <see cref="AutoEdit"/> property, which is now obsolete.
        /// Setting the AutoCompleteMode property to 'Append' is the functional equivalent of setting AutoEdit to true.
        /// </p>
		/// <p class="body">
        /// Prior to the addition of the AutoCompleteMode property, automatic value completion was supported in the form
        /// of the now obsolete AutoComplete property, which, when enabled, modified the control's text by appending characters
        /// to the string typed by the end user so as to match the text of the first item found in the list whose
        /// text begins with the typed characters. For example, given a list which contains an item whose text is "Apple",
        /// when the end user types the string "Ap", the edit portion would then be updated to include the remaining characters, "ple",
        /// so that the text of the first item that matches the typed string, "Apple", becomes selected. That same functionality is now
        /// enabled by setting AutoCompleteMode to 'Append'. The appended characters are selected so that continuing to type causes
        /// these characters to be removed.
        /// </p>
		/// <p class="body">
        /// The AutoCompleteMode property extends two additional modes for value completion, 'Suggest' and 'SuggestAppend'. When the
        /// property is set to 'Suggest', no characters are appended to the edit portion, but rather the dropdown is automatically
        /// displayed, showing only the items whose text begins with the string that was typed by the end user. For example, given a
        /// list containing the items, "Apple", "Application", and "Apprehend", upon typing the "A" character, the dropdown list will
        /// appear, displaying all of those items. The list is continually filtered as more characters are typed, eliminating the entries
        /// whose text no longer matches the typed string. For example, if the continues typing until the edit portion contains "Appl", the
        /// "Apprehend" item would be removed from the list, since the presence of the character "l" in the typed string now precludes that
        /// item.
        /// </p>
		/// <p class="body">
        /// The 'SuggestAppend' setting essentially combines the funtionality extended by the 'Append' and 'Suggest' settings. The dropdown
        /// list is automatically displayed and filtered as needed, and text is appended to the edit portion to complete the typed text to
        /// match the first item with matching text.
        /// </p>
		/// <p class="note">
        /// Setting the <see cref="UltraGridColumn.ValueList">ValueList</see> property can in certain circumstances interfere with the
        /// suggest mode functionality. If, for example, the property is set to reference an <see cref="UltraDropDown"/> control, suggest
        /// mode is disabled. Suggest mode support for multi-column lists is extended through the <see cref="UltraCombo"/> control; this
        /// can be enabled by assigning a reference to the UltraCombo control to the column's <see cref="EditorControl"/> property (which
        /// enables UltraCombo to serve an <see cref="UltraGridComboEditor"/> instance via its <see cref="Infragistics.Win.IProvidesEmbeddableEditor">IProvidesEmbeddableEditor</see>
        /// implementation), then setting the AutoCompleteMode property on either the column or the control.
        /// </p>
		/// <p class="note">
        /// When a <see cref="Infragistics.Win.ValueList">ValueList</see> instance is assigned to the ValueList property, and the <see cref="Editor"/>
        /// property is left at its default value, or references an instance of the <see cref="EditorWithCombo"/> class, suggest mode is fully supported.
        /// </p>
		/// </remarks>
        /// <seealso cref="ValueList"/>
        /// <seealso cref="UltraCombo"/>
        /// <seealso cref="UltraCombo.AutoCompleteMode">AutoCompleteMode (UltraCombo control)</seealso>
        /// <seealso cref="EditorWithCombo"/>
        /// <seealso cref="Infragistics.Win.AutoCompleteMode">AutoCompleteMode enumeration</seealso>
        /// <seealso cref="UltraGridComboEditor"/>
		[ LocalizedDescription("UltraCombo_P_AutoCompleteMode")]
        [LocalizedCategory("LC_Behavior")]
		public Infragistics.Win.AutoCompleteMode AutoCompleteMode
		{
			get
			{ 
				return this.autoCompleteMode; 
			}
			set
			{ 
				if ( value != this.autoCompleteMode )
				{
                    // MRS NAS v8.3 - Unit Testing
                    if (!Enum.IsDefined(typeof(Infragistics.Win.AutoCompleteMode), value))
                        throw new InvalidEnumArgumentException("AutoCompleteMode", (int)value, typeof(Infragistics.Win.AutoCompleteMode));

					this.autoCompleteMode = value;

					this.NotifyPropChange( PropertyIds.AutoCompleteMode );
				}
			}
		}

        /// <summary>
        /// Returns the resolved value of the <see cref="AutoCompleteMode"/> property.
        /// </summary>
		/// <remarks>
		/// <p class="body">In the absence of an explicit setting, the AutoCompleteMode property resolves to 'Append'.</p>
		/// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Infragistics.Win.AutoCompleteMode AutoCompleteModeResolved
        {
            get
            {
                return this.autoCompleteMode != Infragistics.Win.AutoCompleteMode.Default ?
                    this.autoCompleteMode :
                    Infragistics.Win.AutoCompleteMode.Append; 
            }
        }

		/// <summary>
		/// Returns true if the <see cref="AutoCompleteMode"/> property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeAutoCompleteMode( )
		{
			return this.autoCompleteMode != Infragistics.Win.AutoCompleteMode.Default;
		}


		/// <summary>
		/// Resets the <see cref="AutoCompleteMode"/> property to its default value.
		/// </summary>
		public void ResetAutoCompleteMode( )
		{
			this.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Default;
		}
		
		    #endregion AutoCompleteMode

        #endregion FR09238 - AutoCompleteMode

		/// <summary>
		/// This method is obsolete. Use <see cref="ResetMinValue"/> instead.
		/// </summary>
		/// <seealso cref="UltraGridColumn.ResetMinValue"/>	
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMinDate()
		{
			
			// JJD 8/19/02
			// Reset the minValue member instead
//			System.DateTime discard;
//			this.SetMinDateMaxDateDefault( out this.minDate, out discard );

			this.ResetMinValue();
		}
		/// <summary>
		/// This method is obsolete. Use <see cref="ResetMaxValue"/> instead.
		/// </summary>
		/// <seealso cref="UltraGridColumn.ResetMaxValue"/>	
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMaxDate()
		{
			// JJD 8/19/02
			// Reset the maxValue member instead
			//System.DateTime discard;
			//this.SetMinDateMaxDateDefault( out discard, out this.maxDate );

			this.ResetMaxValue();
		}

		/// <summary>
		/// This property is obsolete. Use <see cref="MinValue"/> instead.
		/// </summary>
		/// <seealso cref="UltraGridColumn.MinValue"/>	
//		[LocalizedDescription("LD_AppearanceCache_P_MinDate")]
//		[LocalizedCategory("LC_Behavior")]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden) ]
		[ Browsable(false) ]
		[ EditorBrowsable(EditorBrowsableState.Never) ]
		[ Obsolete("Use MinValue property instead", false ) ]
		public System.DateTime MinDate
		{
			get
			{
				//this variable is initialized when the constructor is called (throgh reset())
				//return this.minDate;

				// MD 7/26/07 - 7.3 Performance
				// Moved below, we only have to call this code if the MinValue is not a DateTime
				//// JJD 8/19/02
				//// We are now using MinValue and MaxValue instead.
				//System.DateTime defMinDate, defMaxDate;
				//
				//// MD 7/26/07 - 7.3 Performance
				//// FxCop - Mark members as static
				////this.SetMinDateMaxDateDefault( out defMinDate, out defMaxDate );
				//UltraGridColumn.SetMinDateMaxDateDefault( out defMinDate, out defMaxDate );

				// JAS 2005 v2 XSD Support
				//
//				if ( this.minValue is DateTime )
//					return (DateTime)this.minValue;
				if ( this.MinValue is DateTime )
					return (DateTime)this.MinValue;

				// JJD 8/19/02
				// We are now using MinValue and MaxValue instead.
				System.DateTime defMinDate, defMaxDate;
				UltraGridColumn.SetMinDateMaxDateDefault( out defMinDate, out defMaxDate );

				return defMinDate;
			}
			set
			{
				// JJD 8/19/02
				// We are now using MinValue and MaxValue instead.
				//
				// JAS 2005 v2 XSD Support
				//
//				if ( !value.Equals( this.minValue ) )
				if ( !value.Equals( this.MinValue ) )
				{
					if ( value.CompareTo( this.MaxDate ) > 0 )
						throw new ArgumentOutOfRangeException(SR.GetString("LER_Exception_311"),
							value, SR.GetString("LER_Exception_331"));
					
					//this.minDate = value;
					//
					// JAS 2005 v2 XSD Support
					//
					//this.minValue = value;
					this.MinValue = value;
					//this.NotifyPropChange( PropertyIds.MinDate ); 					
				}
			}
		}

		/// <summary>
		/// This property is obsolete. Use <see cref="MaxValue"/> instead.
		/// </summary>
		/// <seealso cref="UltraGridColumn.MaxValue"/>	
		[ Browsable(false) ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden) ]
		[ EditorBrowsable(EditorBrowsableState.Never) ]
		[ Obsolete("Use MaxValue property instead", false ) ]
		public System.DateTime MaxDate
		{
			get
			{
				//this variable is initialized when the constructor is called (throgh reset())
				//return this.maxDate;

				// MD 7/26/07 - 7.3 Performance
				// Moved below, we only have to call this code if the MaxValue is not a DateTime
				//// JJD 8/19/02
				//// We are now using MinValue and MaxValue instead.
				//System.DateTime defMinDate, defMaxDate;
				//
				//// MD 7/26/07 - 7.3 Performance
				//// FxCop - Mark members as static
				////this.SetMinDateMaxDateDefault( out defMinDate, out defMaxDate );
				//UltraGridColumn.SetMinDateMaxDateDefault( out defMinDate, out defMaxDate );

				// JAS 2005 v2 XSD Support
				//
//				if ( this.maxValue is DateTime )
//					return (DateTime)this.maxValue;
				if ( this.MaxValue is DateTime )
					return (DateTime)this.MaxValue;

				// JJD 8/19/02
				// We are now using MinValue and MaxValue instead.
				System.DateTime defMinDate, defMaxDate;
				UltraGridColumn.SetMinDateMaxDateDefault( out defMinDate, out defMaxDate );

				return defMaxDate;
			}
			set
			{
				// JJD 8/19/02
				// We are now using MinValue and MaxValue instead.
				//
				// JAS 2005 v2 XSD Support
				//
//				if ( !value.Equals( this.maxValue ) )
				if ( !value.Equals( this.MaxValue ) )
				{
					if ( value.CompareTo( this.MinDate ) < 0 )
						throw new ArgumentOutOfRangeException(SR.GetString("LER_Exception_311"),
							value, SR.GetString("LER_Exception_332"));

					// JAS 2005 v2 XSD Support
					//
					this.MaxValue = value;
					//this.maxValue = value;
					//this.maxDate = value;
					//this.NotifyPropChange( PropertyIds.MaxDate ); 
				}
			}
		}


		// SSP 6/5/02
		// EM Embeddable editors
		// Added MinValue and MaxValue propertries.
		//
		#region MinValue

		/// <summary>
		/// Specifies the minimum value that can be entered in the cell. Default value is null meaning no minimum constraint. The object assigned to this property should be the same type as the column's <see cref="DataType"/> or compatible otherwise it won't be honored.
		/// </summary>
		[ LocalizedDescription("LD_AppearanceCache_P_MinValue")]
		[ LocalizedCategory("LC_Behavior") ]
		[ System.ComponentModel.Editor( typeof( PrimitiveTypeUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		[ TypeConverter(typeof(PrimitiveTypeConverter))]
		// SSP 2/22/05 BR02373
		// Added Localizable attribute so min and max of strings can be localized.
		//
		[ Localizable( true ) ]
		public object MinValue
		{
			get
			{
				// JAS 2005 v2 XSD Support
				//
				//return this.minValue;
				return this.Constraint.MinInclusive;
			}
			set
			{
                // MRS NAS v8.3 - Unit Testing
                //
                if (this.Constraint.MinInclusive == value)
                    return;

				// JAS 2005 v2 XSD Support
				//
				//this.minValue = value;
				this.Constraint.MinInclusive = value;

				// JAS 2005 v2 XSD Support
				// If this property had been set by the EnforceXsdConstraints method, then
				// clear the bit which represents this property so that the ClearXsdConstraints
				// method will know not to reset this value.
				//
				if( (this.XsdSuppliedConstraints & XsdConstraintFlags.MinInclusive) != 0 )
					this.XsdSuppliedConstraints &= ~XsdConstraintFlags.MinInclusive;

                // MRS NAS v8.3 - Unit Testing
                this.NotifyPropChange(PropertyIds.MinValue);
			}
		}

		#endregion // MinValue

		#region ShouldSerializeMinValue

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeMinValue()
		{
			// JAS 2005 v2 XSD Support
			//
			// AS 8/15/02
			// This could allow unsafe objects to get serialized.
			//return null != this.minValue;
		
			//	BF 10.19.04	UWG3711
			//	Looks like a copy/paste error here; we should be passing
			//	this.minValue, not this.maxValue
			//
			//return Utils.IsSafelySerializable(this.maxValue);
			//return Utils.IsSafelySerializable(this.minValue);
			return Utils.IsSafelySerializable( this.MinValue );
		}

		#endregion // ShouldSerializeMinValue

		#region ResetMinValue

		/// <summary>
		/// Resets MinValue to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMinValue( ) 
		{
			// JAS 2005 v2 XSD Support
			//
			//this.minValue = null;
			this.MinValue = null;
		}

		#endregion // ResetMinValue

		#region MaxValue

		/// <summary>
		/// Specifies the maximum value that can be entered in the cell. Default value is null meaning no maximum constraint. The object assigned to this property should be the same type as the column's <see cref="DataType"/> or compatible otherwise it won't be honored.
		/// </summary>
		[ LocalizedDescription("LD_AppearanceCache_P_MaxValue")]
		[ LocalizedCategory("LC_Behavior") ]
		[ System.ComponentModel.Editor( typeof( PrimitiveTypeUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		[ TypeConverter(typeof(PrimitiveTypeConverter))]
		// SSP 2/22/05 BR02373
		// Added Localizable attribute so min and max of strings can be localized.
		//
		[ Localizable( true ) ]
		public object MaxValue
		{
			get
			{
				// JAS 2005 v2 XSD Support
				//
				//return this.maxValue;
				return this.Constraint.MaxInclusive;
			}
            set
            {
                // MRS NAS v8.3 - Unit Testing
                //
                if (this.Constraint.MaxInclusive == value)
                    return;

                // JAS 2005 v2 XSD Support
                //
                //this.maxValue = value;
                this.Constraint.MaxInclusive = value;

                // JAS 2005 v2 XSD Support
                // If this property had been set by the EnforceXsdConstraints method, then
                // clear the bit which represents this property so that the ClearXsdConstraints
                // method will know not to reset this value.
                //
                if ((this.XsdSuppliedConstraints & XsdConstraintFlags.MaxInclusive) != 0)
                    this.XsdSuppliedConstraints &= ~XsdConstraintFlags.MaxInclusive;

                // MRS NAS v8.3 - Unit Testing
                this.NotifyPropChange(PropertyIds.MaxValue);
            }
		}

		#endregion // MaxValue
		
		#region ResetMaxValue

		/// <summary>
		/// Resets MaxValue to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMaxValue( ) 
		{
			// JAS 2005 v2 XSD Support
			//
			//this.maxValue = null;
			this.MaxValue = null;

		}

		#endregion // ResetMaxValue

		#region ShouldSerializeMaxValue

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeMaxValue()
		{
			// JAS 2005 v2 XSD Support
			//
			// AS 8/15/02
			// This could allow unsafe objects to get serialized.
			//return null != this.maxValue;
			//return Utils.IsSafelySerializable(this.maxValue);
			return Utils.IsSafelySerializable( this.MaxValue );
		}

		#endregion // ShouldSerializeMaxValue

		/// <summary>
		/// Determines if the column will allow auto-expanding pop-up edit windows.
		/// </summary>
		/// <remarks>
		/// <p class="body">One of the features the UltraWinGrid offers is the ability to expand a cell when it is in edit mode to provide a greater area for the user to enter data. This is controlled by the <b>AutoSizeEdit</b> property. When set to True, text editing for any cell takes place in a pop-up window that expands to accommodate the amount of text being entered. When the user shifts the input focus away, the edit window disappears and the cell is shown normally.</p>
		/// <p class="body">The attributes of the pop-up edit window are determined by the properties of the <see cref="Infragistics.Win.UltraWinGrid.CancelableAutoSizeEditEventArgs"/> object. Available properties let you specify the starting and maximum height and width of the pop-up window. This object is available only inside of the <b><see cref="Infragistics.Win.UltraWinGrid.UltraGrid.BeforeAutoSizeEdit"/></b> event, where it is accessed via the e object of the event.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_AutoSizeEdit")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean AutoSizeEdit
		{
			get 
			{
				return this.autoSizeEdit;
			}
			set
			{
				if( this.autoSizeEdit != value )
				{
                    // MRS NAS v8.3 - Unit Testing
                    //if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
                    //    throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_74"));
                    if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
                        throw new InvalidEnumArgumentException("AutoSizeEdit", (int)value, typeof(DefaultableBoolean));					

					this.autoSizeEdit = value;
					this.NotifyPropChange( PropertyIds.AutoSizeEdit, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAutoSizeEdit() 
		{
			return this.autoSizeEdit != DefaultableBoolean.Default;
		}
 
		/// <summary>
		/// Resets AutoSizeEdit to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetAutoSizeEdit() 
		{
			this.AutoSizeEdit = DefaultableBoolean.Default;
		}


		/// <summary>
		/// Returns or sets a value that determines how cell buttons are displayed for a column's cells.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property is used to indicate how cell buttons are displayed for the cells of a column. Setting 1 (ButtonDisplayStyleAlways) always displays the buttons while the other settings cause the buttons to be displayed only as a result of user interaction with the control.</p>
		/// <p class="body">This property only has an effect if the column's <b>Style</b> property is set to 2 (StyleEditButton), 4 (StyleDropDown), 5 (StyleDropDownList), 6 (StyleDropDownValidate), 7 (StyleDropDownButton), or 8 (StyleDropDownCalendar).</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_ButtonDisplayStyle")]
		[LocalizedCategory("LC_Appearance")]
		public ButtonDisplayStyle ButtonDisplayStyle
		{
			get 
			{
				return this.buttonDisplayStyle;
			}
			set
			{
				if(this.buttonDisplayStyle != value)
				{
                    // MRS NAS v8.3 - Unit Testing
                    //if (!Enum.IsDefined(typeof(ButtonDisplayStyle), value))
                    //    throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_75"));
                    if (!Enum.IsDefined(typeof(ButtonDisplayStyle), value))
                        throw new InvalidEnumArgumentException("ButtonDisplayStyle", (int)value, typeof(ButtonDisplayStyle));

					this.buttonDisplayStyle = value;
					this.NotifyPropChange( PropertyIds.ButtonDisplayStyle, null );
				}
			}
		}
		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeButtonDisplayStyle() 
		{
            
			return this.buttonDisplayStyle != ButtonDisplayStyle.OnMouseEnter;
		}
 
		/// <summary>
		/// Resets ButttonDisplayStyle to its default value (OnMouseEnter).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetButtonDisplayStyle() 
		{
			this.ButtonDisplayStyle = ButtonDisplayStyle.OnMouseEnter;
		}
		
		// MRS 4/7/05 - Marked this property obsolete and replaced it with CharacterCasing
		// This was done so that we use the same enum across all controls. 
		#region Obsolete Case property
		/// <summary>
		/// Returns or sets the case to use when editing or displaying column text.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Case</b> property specifies whether the column should display text in a specific case and change the case of the text being edited. This property actually changes the case of edited text; if you set <b>Case</b> to a non-zero value, any text you edit or enter will be stored in the database as either all uppercase or all lowercase. Note that while the text is displayed using the specified case, the changed case text is not committed back into the database unless a change is made to the value of the cell. Simply placing the cell into edit mode will not change the data to the displayed case.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_Case")]
		[LocalizedCategory("LC_Behavior")]		
		[Obsolete("This property has been replaced by the CharacterCasing property")]
		public Infragistics.Win.UltraWinGrid.Case Case
		{
			get 
			{
				// MRS 4/7/05
				//return this.textcase;
				return GridUtils.CharacterCasingToCase(this.characterCasing);				
			}
			set
			{
				if ( !Enum.IsDefined( typeof(Case), value ) )
					throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_76") );

				// MRS 4/7/05
//				if ( value != this.textcase )
//				{
//					this.textcase = value;
//
//					// SSP 10/30/01 UWG607
//					// 
//					this.NotifyPropChange( PropertyIds.Case, null );
//				}
				CharacterCasing newValue = GridUtils.CaseToCharacterCasing(value);
				this.CharacterCasing = newValue;
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		[Obsolete("Replaced by ShouldSerializeCharacterCasing")]
		protected bool ShouldSerializeCase() 
		{
			//return this.textcase != Case.Unchanged;
			return false;
		}
 
		/// <summary>
		/// Reset Case to its default value (unchanged).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Obsolete("Replaced by ResetCharacterCasing")]
		public void ResetCase() 
		{
			//this.Case = Case.Unchanged;
			this.ResetCharacterCasing();
		}
		#endregion Obsolete Case property

		// MRS 4/7/05 - Marked Case property obsolete and replaced it with CharacterCasing
		// This was done so that we use the same enum across all controls. 
		#region CharacterCasing
		/// <summary>
		/// Returns or sets the case to use when editing or displaying column text.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Case</b> property specifies whether the column should display text in a specific case and change the case of the text being edited. This property actually changes the case of edited text; if you set <b>Case</b> to a non-zero value, any text you edit or enter will be stored in the database as either all uppercase or all lowercase. Note that while the text is displayed using the specified case, the changed case text is not committed back into the database unless a change is made to the value of the cell. Simply placing the cell into edit mode will not change the data to the displayed case.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_CharacterCasing")]
		[LocalizedCategory("LC_Behavior")]
		public System.Windows.Forms.CharacterCasing CharacterCasing
		{
			get 
			{
				return this.characterCasing;
			}
			set
			{
				if ( value != this.characterCasing )
				{
                    // MRS NAS v8.3 - Unit Testing
                    //if ( !Enum.IsDefined( typeof(CharacterCasing), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_99") );                    
                    if (!Enum.IsDefined(typeof(CharacterCasing), value))
                        throw new InvalidEnumArgumentException("CharacterCasing", (int)value, typeof(CharacterCasing));

					this.characterCasing = value;

					this.NotifyPropChange( PropertyIds.CharacterCasing, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeCharacterCasing() 
		{
			return this.characterCasing != CharacterCasing.Normal;
		}
 
		/// <summary>
		/// Reset CharacterCasing to its default value (Normal).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetCharacterCasing() 
		{
			this.CharacterCasing  = CharacterCasing.Normal;
		}
		#endregion CharacterCasing

		/// <summary>
		/// Returns or sets a value that determines the number of columns to skip when synchronizing columns across multiple bands.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property performs a function similar to the COLSPAN attribute used in HTML tables. <b>ColSpan</b> is commonly used with the multi-band vertical view style when a band is indented from its parent. You can use it to "unlock" column synchronization for the first column in the child band so that it does not become too narrow by aligning itself with the edge of a column that ends directly above it in the parent band.</p>
		/// <p class="body"><b>ColSpan</b> and column synchronization have no effect on bands that contain groups; only bands that do not have groups will participate in column synchronization.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_ColSpan")]
		[LocalizedCategory("LC_Behavior")]
		public short ColSpan
		{
			get 
			{
				return this.colSpan;
			}
			set
			{
				if(this.colSpan != value)
				{
					this.colSpan = value;

                    // MRS 3/25/2008 - BR31396
                    this.DirtyHasColSpan(); 
                    
                    this.NotifyPropChange(PropertyIds.ColSpan, null);
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeColSpan() 
		{
			return this.colSpan != 1;
		}
 
		/// <summary>
		/// Resets ColSpan to its default value (1).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetColSpan() 
		{
			this.ColSpan = 1;
		}

		/// <summary>
		/// Returns or sets a value that determines how cell values for a column will be copied to the clipboard when data masking is in enabled.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property is used to determine how mask literals and prompt characters are handled when the text of a masked cell is copied to the Windows clipboard. Based on the setting of this property, the text in the clipboard will contain no prompt characters or literals (just the raw data), the data and just the literals, the data and just the prompt characters, or all the text including both prompt characters and literals. The formatted spacing of partially masked values can be preserved by indicating to include literals with padding, which includes data and literal characters, but replaces prompt characters with spaces.</p>
		/// <p class="body">The <b>MaskInput</b> property is used to specify how data input will be masked for the cells in a column. The mask usually includes literal characters that are used to delimit the data entered by the user. This property has no effect unless the <b>MaskInput</b> property is set, meaning that data masking is enabled. </p>
		/// <p class="body">When data masking is enabled, the <b>MaskDataMode</b> property determines how cell values are stored by the data source, the <b>MaskDisplayMode</b> property indicates how cell values are displayed, and the <b>PromptChar</b> property specifies which character will be used to prompt the user to input data.</p>
		/// <seealso cref="PadChar"/>
		/// <seealso cref="PromptChar"/>
		/// <seealso cref="MaskDataMode"/>
		/// <seealso cref="MaskDisplayMode"/>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_MaskClipMode")]
		[LocalizedCategory("LC_Mask")]
		public MaskMode MaskClipMode
		{
			get 
			{
				return this.maskClipMode;
			}
			set
			{
				if(this.maskClipMode != value)
				{
                    // MRS NAS v8.3 - Unit Testing
                    //if ( !Enum.IsDefined( typeof(MaskMode), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_77") );
                    if (!Enum.IsDefined(typeof(MaskMode), value))
                        throw new InvalidEnumArgumentException("MaskClipMode", (int)value, typeof(MaskMode));
                    
					this.maskClipMode = value;
					this.NotifyPropChange( PropertyIds.MaskClipMode, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMaskClipMode() 
		{
			return this.maskClipMode != MaskMode.Raw;
		}
 
		/// <summary>
		/// Resets MaskClipMode to its default value (raw data).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMaskClipMode() 
		{
			this.MaskClipMode = MaskMode.Raw;
		}

		/// <summary>
		/// Returns or sets a value that determines how cell values for a column will be stored by the data source when data masking is enabled.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property is used to determine how mask literals and prompt characters are handled when a cell values are stored by the data source. Based on the setting of this property, the text in the clipboard will contain no prompt characters or literals (just the raw data), the data and just the literals, the data and just the prompt characters, or all the text including both prompt characters and literals. The formatted spacing of partially masked values can be preserved by indicating to include literals with padding, which includes data and literal characters, but replaces prompt characters with spaces.</p>
		/// <p class="body">Generally, simply the raw data is committed to the data source and data masking is used to format the data when it is displayed. In some cases, however, it may be appropriate in your application to store mask literals as well as data.</p>
		/// <p class="body">The <b>MaskInput</b> property is used to specify how data input will be masked for the cells in a column. The mask usually includes literal characters that are used to delimit the data entered by the user. This property has no effect unless the <b>MaskInput</b> property is set, meaning that data masking is enabled.</p>
		/// <p class="body">When data masking is enabled, the <b>MaskClipMode</b> property determines how cell values are copied to the clipboard, the <b>MaskDisplayMode</b> property indicates how cell values are displayed, and the <b>PromptChar</b> property specifies which character will be used to prompt the user to input data.</p>
		/// <seealso cref="PadChar"/>
		/// <seealso cref="PromptChar"/>
		/// <seealso cref="MaskDisplayMode"/>
		/// <seealso cref="MaskClipMode"/>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_MaskDataMode")]
		[LocalizedCategory("LC_Mask")]
		public MaskMode MaskDataMode
		{
			get 
			{
				return this.maskDataMode;
			}
			set
			{
				if(this.maskDataMode != value)
				{
                    // MRS NAS v8.3 - Unit Testing
                    //if ( !Enum.IsDefined( typeof(MaskMode), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_78") );
                    if (!Enum.IsDefined(typeof(MaskMode), value))
                        throw new InvalidEnumArgumentException("MaskDataMode", (int)value, typeof(MaskMode));

					this.maskDataMode = value;
					this.NotifyPropChange( PropertyIds.MaskDataMode, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMaskDataMode() 
		{
            
			return this.maskDataMode != MaskMode.Raw;
		}
 
		/// <summary>
		/// Resets MaskDataMode to its default value (raw).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMaskDataMode() 
		{
			this.MaskDataMode = MaskMode.Raw;
		}

		/// <summary>
		///Returns or sets a value that determines cell values will be displayed when the cells are not in edit mode and data masking is enabled.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property is used to determine how mask literals and prompt characters are displayed when a cell is not in edit mode. Based on the setting of this property, the text in the clipboard will contain no prompt characters or literals (just the raw data), the data and just the literals, the data and just the prompt characters, or all the text including both prompt characters and literals. The formatted spacing of partially masked values can be preserved by indicating to include literals with padding, which includes data and literal characters, but replaces prompt characters with spaces.</p>
		/// <p class="body">Generally, prompt characters disappear when a cell is no longer in edit mode, as a visual cue to the user. In some cases, however, it may be appropriate in your application to display mask literals as well as data when a cell is no longer in edit mode.</p>
		/// <p class="body">The <b>MaskInput</b> property is used to specify how data input will be masked for the cells in a column. The mask usually includes literal characters that are used to delimit the data entered by the user. This property has no effect unless the <b>MaskInput</b> property is set, meaning that data masking is enabled. You should also note that masking can only be applied to single-line cells. If a cell is displaying ultiple lines of text, no masking will be applied to the cell.</p>
		/// <p class="body">When data masking is enabled, the <b>MaskClipMode</b> property determines how cell values are copied to the clipboard, the <b>MaskDataMode</b> property indicates how cell values are stored by the data source, and the <b>PromptChar</b> property specifies which character will be used to prompt the user to input data.</p>
		/// <seealso cref="PadChar"/>
		/// <seealso cref="PromptChar"/>
		/// <seealso cref="MaskDataMode"/>
		/// <seealso cref="MaskClipMode"/>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_MaskDisplayMode")]
		[LocalizedCategory("LC_Mask")]
		public MaskMode MaskDisplayMode
		{
			get 
			{
				return this.maskDisplayMode;
			}
			set
			{
				if(this.maskDisplayMode != value)
				{
                    // MRS NAS v8.3 - Unit Testing
                    //if ( !Enum.IsDefined( typeof(MaskMode), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_79") );
                    if (!Enum.IsDefined(typeof(MaskMode), value))
                        throw new InvalidEnumArgumentException("MaskDisplayMode", (int)value, typeof(MaskMode));

					this.maskDisplayMode = value;
					this.NotifyPropChange( PropertyIds.MaskDisplayMode, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMaskDisplayMode() 
		{
			return this.maskDisplayMode != MaskMode.Raw;
		}
 
		/// <summary>
		/// Resets MaskDisplayMode to its default value (raw).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMaskDisplayMode() 
		{
			this.MaskDisplayMode = MaskMode.Raw;
		}

		// AS 9/24/01 - Not needed
		//		/// <summary>
		//		/// Returns the field name for the database field associated with the column.
		//		/// </summary>
		//		[Description("Returns the field name for the database field associated with the column.")]
		//		[Category("Data")]
		//		public string DataField
		//		{
		//			get 
		//			{
		//				return "not implemented";
		//			}
		//
		//		}

		// SSP 12/11/03 UWG2781
		// Since the MaskMode enum doesn't have Default as its member, we need to let
		// the user be able to specify that we pickup properties from the owner rather
		// than use the column's settings. Added UseEditorMaskSettings property.
		//
		/// <summary>
		/// Specifies whether to use the editor's mask related settings rather than the column's mask settings. Default value of this property is false.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default the UltraGrid maskes use of MaskDataMode, MaskDisplayMode and MaskClipMode settings of the column ignoring any settings off the editor's default owner. This is due to the fact that the associated <b>MaskMode</b> enum doesn't have Default as its member. You can override this default behavior by setting this property to <b>true</b>. When this property is set to <b>true</b>, UltraGrid will always use the settings off the editor's owner. This fascilitates different mask data/display/clip modes on different cells of the same column by using the cell's Editor property and specifying the mask settings on the editors assigned to cells of the column. This property will also change the resolution order for MaskInput property. It will use the editor's MaskInput settings if they are non-nulls or else use the column's MaskInput settings.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_Column_P_UseEditorMaskSettings") ]
		[ LocalizedCategory("LC_Behavior") ]
		public bool UseEditorMaskSettings
		{
			get
			{
				return this.useEditorMaskSettings;
			}
			set
			{
				if ( value != this.useEditorMaskSettings )
				{
					this.useEditorMaskSettings = value;

					this.NotifyPropChange( PropertyIds.UseEditorMaskSettings );
				}
			}
		}

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeUseEditorMaskSettings( )
		{
			return this.useEditorMaskSettings;
		}

		/// <summary>
		/// Resets the UseEditorMaskSettings property to its default value of false.
		/// </summary>
		public void ResetUseEditorMaskSettings( )
		{
			this.UseEditorMaskSettings = false;
		}

		/// <summary>
		/// Returns or sets the maximum length of the text that can be entered into a cell. Zero indicates that there is no limit.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>FieldLen</b> property gives you the ability to limit the amount of text that can be entered in column cells. You can use this property to enforce database-specific or application specific limitations.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_FieldLen")]
		[LocalizedCategory("LC_Behavior")]
		// JAS 2005 v2 XSD Support - Deprecated FieldLen.
		[Obsolete("Use the MaxLength property instead.", false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int FieldLen
		{
			get 
			{
				//check for bound property
				if(!this.bound)
				{
					// JAS 2005 v2 XSD Support
					//
					return this.MaxLength;
//					return this.fieldLen;
				}
				else
				{
					// SSP 9/11/03 UWG1976
					// As per this bug, made the FieldLen property settable even on bound columns.
					// 
					// ------------------------------------------------------------------------
					if ( this.ShouldSerializeFieldLen( ) )
						// JAS 2005 v2 XSD Support
//						return this.fieldLen;
						return this.MaxLength;
					// ------------------------------------------------------------------------

					// SSP 1/24/03 UWG1944
					// Implemented.
					// If the column has an underlying data column associated with it, then get the
					// MaxLength off of it.
					//
					System.Data.DataColumn dataColumn = this.GetDataColumn( );
					
					if ( null != dataColumn )
						return dataColumn.MaxLength;

					// Return 0 otherwise which means that there are no restrictions on the field len.
					//
					return 0;
				}
				
			}
			set
			{
				// SSP 9/11/03 UWG1976
				// As per this bug, made the FieldLen property settable even on bound columns.
				// 
				// --------------------------------------------------------------------------
				if ( value < 0 )
					throw new ArgumentOutOfRangeException( "FieldLen", value, SR.GetString( "LER_ArgumentException_15" ) );

				// JAS 2005 v2 XSD Support
				//
//				this.fieldLen = value;
				this.MaxLength = value;
				
				// --------------------------------------------------------------------------
			}
		}

		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		// JAS 2005 v2 XSD Support - Deprecated FieldLen.
		[Obsolete("Use the ShouldSerializeMaxLength method instead.", false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		internal protected bool ShouldSerializeFieldLen() 
		{
			// JAS 2005 v2 XSD Support
			//
			//return this.fieldLen > 0;
			return this.Constraint.HasMaxLength;
		}
 
		/// <summary>
		/// Reset FieldLen to its default value (0).
		/// </summary>
		// JAS 2005 v2 XSD Support - Deprecated FieldLen.
//		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Obsolete("Use the ResetMaxLength method instead.", false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void ResetFieldLen() 
		{
			// AS - 10/29/01
			// We are throwing an exception when we are invoking this on a bound column
			// since its field length cannot be set. We should skip bound columns.
			if (!this.IsBound)
				this.FieldLen = 0;
		}

		// SSP 1/24/03 UWG1944
		// Added a way to get underlying data column in the data table associated with this column.
		// This will only work when the underlying data source is a data table or something that
		// uses data columns.
		//
		internal System.Data.DataColumn GetDataColumn( )
		{
			// SSP 1/11/05
			// Added support for defaulting the Caption to the DataColumn's Caption and then
			// to the PropertyDescriptor's DisplayName. Now the order will be 
			// ColumnHeader.Caption, DataColumn.Caption, PropertyDescriptor.DisplayName and
			// then the PropertyDescriptor.Name. Added code to cache the underlying DataColumn 
			// object.
			//
			if ( this.cachedDataColumn_lastPropDesc == this.propertyDescriptor )
				return this.cachedDataColumn;

			if ( null != this.Band && null != this.propertyDescriptor )
			{
				try
				{
					System.Data.DataView dataView = this.Band.BindingList as System.Data.DataView;

					// SSP 2/22/06 BR10161
					// In CLR2 the data view may be wrapped inside a BindingSource.
					// 
					// ------------------------------------------------------------
					if ( null == dataView )
					{
						BindingSource bindingSource = this.Band.BindingList as BindingSource;
						if ( null != bindingSource )
							dataView = bindingSource.List as System.Data.DataView;
					}
					// ------------------------------------------------------------

					if ( null != dataView )
					{
						System.Data.DataTable dataTable = dataView.Table;

						if ( null != dataTable && null != dataTable.Columns &&
							dataTable.Columns.Contains( this.propertyDescriptor.Name ) )
						{
							System.Data.DataColumn dataColumn = dataTable.Columns[ propertyDescriptor.Name ];

							// SSP 1/11/05
							// Added support for defaulting the Caption to the DataColumn's Caption and then
							// to the PropertyDescriptor's DisplayName. Now the order will be 
							// ColumnHeader.Caption, DataColumn.Caption, PropertyDescriptor.DisplayName and
							// then the PropertyDescriptor.Name. Added code to cache the underlying DataColumn 
							// object.
							//
							//return dataColumn;
							this.cachedDataColumn_lastPropDesc = this.propertyDescriptor;
							this.cachedDataColumn = dataColumn;
							return dataColumn;
						}
					}
				}
				catch ( Exception )
				{
				}
			}

			return null;
		}

		internal bool IsNullable
		{
			get
            {
                // SSP 7/18/02
                // Commented out below code and added code below it.
                //
                // --------------------------------------------------------------------------------
                

                // Default to true for unbound columns and to true for bound columns
                // just like we were doing before.
                //
                // SSP 11/30/04 - BR02429 - Added Disallow to Nullable
                // Changed the default behavior of the column's IsNullable to return true instead
                // of false in case the underlying property descriptor is not a DataColumn. Also 
                // added AllowDBNull property to UltraDataColumn.
                //
                // --------------------------------------------------------------------------------
                //bool allowDBNull = null != this.PropertyDescriptor 
                //	?  this.PropertyDescriptor.PropertyType.IsAssignableFrom( typeof( DBNull ) ) 
                //	: true;

                // If the Nullable is set to Disallow then return false.
                //
                bool allowDBNull = true;

                if (Nullable.Disallow == this.Nullable)
                    return false;

                // Check if the column is an UltraDataColumn from UltraDataSource.
                //
                if (this.PropertyDescriptor is Infragistics.Win.UltraWinDataSource.IUltraDataColumn)
                    return ((Infragistics.Win.UltraWinDataSource.IUltraDataColumn)this.PropertyDescriptor).AllowDBNull;
                // --------------------------------------------------------------------------------

                // SSP 11/7/02
                // Check for property descriptor being null.
                //
                //if ( null != this.Band )
                // SSP 1/24/03 UWG1944
                // Moved the code that gets the underlying data column into GetDataColumn method.
                //
                System.Data.DataColumn dataColumn = this.GetDataColumn();

                if (null != dataColumn)
                    allowDBNull = dataColumn.AllowDBNull;

                return allowDBNull;
                // --------------------------------------------------------------------------------  
            }
        }

        #region ConvertValueToDataType

        //  BF 3/24/09  TFS15870
        //  A return value of null from this method implies failure to convert,
        //  which makes it impossible to support converting Nullable<T> types
        //  to null. I added an overload that returns a boolean and the result
        //  in an [out] parameter, and preserved the original signature.

        /// <summary>
		/// Converts valueToConvert to the underlying data type of the column.
		/// If the column is bound, it will use the property descriptor's 
		/// PropertyType otherwise it will use the DataType.
		/// </summary>
		/// <returns>
		/// Returns the converted value, otherwise null if conversion was 
		/// unsuccessful.
		/// </returns>
		internal object ConvertValueToDataType( object valueToConvert )
		{
            object convertedValue = null;
            this.ConvertValueToDataType( valueToConvert, out convertedValue );
            return convertedValue;
        }

        /// <summary>
		/// Converts valueToConvert to the underlying data type of the column.
		/// If the column is bound, it will use the property descriptor's 
		/// PropertyType otherwise it will use the DataType.
		/// </summary>
		/// <returns>
		/// A boolean which indicates whether the conversion was successful; a return value of true indicates that the conversion was successful.
		/// </returns>
		internal bool ConvertValueToDataType( object valueToConvert, out object convertedValue )
		{
            convertedValue = null;

			//RobA UWG296 10/1/01
			//Convert the passed in value to the column's property descriptor's value
			//
			// SSP 10/24/01 UWG529
			// If the value being assigned is DBNull, then do not try
			// to convert to property type
			//
			// JJD 1/24/02
			// Make sure the property descriptor is not null
			//
			// SSP 2/7/02
			//if (  val is DBNull || column.PropertyDescriptor == null )
			//{
			//	convertedValue = val;
			//}
			if ( valueToConvert is DBNull )
			{
				convertedValue = valueToConvert;
			}
			// SSP 2/7/02
			// If there is no property descriptor, then use the DataType
			// to convert the value.
			//
			else if ( this.PropertyDescriptor == null )
			{
				// SSP 2/7/02
				// Check for the valueToConvert being null
				//
				//if ( valueToConvert.GetType() == this.PropertyDescriptor.PropertyType )
				// SSP 7/18/02 UWG1410
				// PropertyDescriptior will always be null here otherwise we wouln't have
				// gotten in else if ( this.PropertyDescriptor == null ) block. So rather
				// check for this.DataType instead,
				//
				//if ( null != valueToConvert && valueToConvert.GetType() == this.PropertyDescriptor.PropertyType )
				if ( null != valueToConvert && valueToConvert.GetType() == this.DataType )
					convertedValue = valueToConvert;
			

				// SSP 10/19/01 UWG518
				// Try using the static method of the Convert class to convert the
				// data type if the above ConvertFrom call failed
				//
				// JJD 1/24/02
				// First try converting with the format info if we have it
				//
				if ( null == convertedValue )
				{
					try
					{
						convertedValue = Convert.ChangeType( valueToConvert, this.DataType, this.FormatInfo );
					}
					catch ( Exception )
					{
					}
				}

				// JJD 1/24/02
				// Then try without the format info
				//
				if ( null == convertedValue )
				{
					try
					{
						convertedValue = Convert.ChangeType( valueToConvert, this.DataType );
					}
					catch ( Exception )
					{
					}
				}
			}
			else
			{
				// SSP 2/7/02
				// Check for the valueToConvert being null
				//
				//if ( valueToConvert.GetType() == this.PropertyDescriptor.PropertyType )
				if ( null != valueToConvert && valueToConvert.GetType() == this.PropertyDescriptor.PropertyType )
					convertedValue = valueToConvert;

					// SSP 3/25/02
					// If the value we are trying to convert is an instance of property type
					// then don't need to convert below. It's already of desired type.
					// Added bewlow else-if statement.
					//
				else if ( null != valueToConvert && null != this.PropertyDescriptor.PropertyType &&
					this.PropertyDescriptor.PropertyType.IsInstanceOfType( valueToConvert ) )
				{
					convertedValue = valueToConvert;
				}
					
				//RobA 10/9/01
				//check with the newer version of .Net
				//for some reason as of now it can not convert a int32 to a string
				//CanConvertFrom returns false
				//
				// SSP 10/19/01 UWG518
				// Took out the CanConvertFrom check below and enclosed the ConvertFrom
				// call into a try catch block
				//
				// JJD 9/15/04
				// Only attempt the conversion if we are converting to or from a string or
				// the converter's CanConvertFrom returns true. This eliminates extra exceptions
				// when converting for example from double to Decimal. This type of conversion
				// is more common now since values pushed into cells via a formula are always
				// doubles.
				//else //if ( column.PropertyDescriptor.Converter.CanConvertFrom( val.GetType() ) )
				else if (valueToConvert is string || 
						 this.DataType == typeof(string) ||
						 (valueToConvert != null && // JAS 5/2/05 - Added the check for null.
						 this.PropertyDescriptor.Converter.CanConvertFrom( valueToConvert.GetType() )) )
				{
					// JJD 1/24/02
					// First try converting with the culture info if we have it
					//
					try
					{					
						convertedValue = this.PropertyDescriptor.Converter.ConvertFrom(null, this.FormatInfo as System.Globalization.CultureInfo, valueToConvert );
					}
					catch ( Exception )
					{
					}
				}
                //  BF 3/24/09  TFS15870
                //  Added a special case for Nullable<T> types...at least when Nullable
                //  is not explicitly set, conversion from a value of null should be
                //  considered valid.
                else
                if ( valueToConvert == null &&
                     this.Nullable == Nullable.Automatic &&
                     // MRS 8/25/2009 - TFS20507
                     // Replaced with a helper method, just because I can.
                     //
                     //this.PropertyDescriptor.PropertyType.IsGenericType &&
                     //this.PropertyDescriptor.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) 
                    Infragistics.Win.Utilities.IsNullableType(this.PropertyDescriptor.PropertyType) )
                {
                    convertedValue = valueToConvert;
                    return true;
                }

                // SSP 10/19/01 UWG518
                // Try using the static method of the Convert class to convert the
                // data type if the above ConvertFrom call failed
                //
                // JJD 1/24/02
                // First try converting with the format info if we have it
                //
                if ( null == convertedValue )
                {
                    try
                    {
                        convertedValue = Convert.ChangeType( valueToConvert, this.DataType, this.FormatInfo );
                    }
                    catch ( Exception )
                    {
                    }
                }


                // JJD 1/24/02
                // Then try without the format info
                //
                if ( null == convertedValue )
                {
                    try
                    {
                        convertedValue = this.PropertyDescriptor.Converter.ConvertFrom( valueToConvert );
                    }
                    catch ( Exception )
                    {
                    }
                }

                // JJD 1/24/02
                // Then try without the format info
                //
                if ( null == convertedValue )
                {
                    try
                    {
                        convertedValue = Convert.ChangeType( valueToConvert, this.DataType );
                    }
                    catch ( Exception )
                    {
                    }
                }	
			}

            //  BF 3/24/09  TFS15870
			//return convertedValue;
			return convertedValue != null;
		}

        #endregion ConvertValueToDataType


		/// <summary>
		/// Returns the column's underlying data type. This property is read-only for bound columns.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use this property to determine what type of data from the data source is expected or supplied by the field that is bound to the column. <b>DataType</b> values correspond to the standard data field types available through the data provider.</p>
		/// <p class="body">When this property is set to 136 (DataTypeChapter), the Hidden, Locked, Width, MinWidth, MaxWidth, and Selected properties are ignored for the column.</p>
		/// <p class="body">This property cannot be set to 72 (DataTypeGuid) or 136 (DataTypeChapter) for unbound columns.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_DataType")]
		[LocalizedCategory("LC_Data")]
		[MergableProperty(false)]
		[Editor(typeof(ColumnDataTypeUITypeEditor), typeof(UITypeEditor))] 	// AS - 1/3/02 UWG884
		public System.Type DataType
		{
			get 
			{
				return this.dataType;
			}
			set
			{
				// for bound columns, we do no allow setting data types
				if ( this.IsBound )				
					throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_81") );
				
				
				// SSP 5/30/02 UWG1159
				// Don't allow setting the data type to null.
				//
				if ( null == value )
					throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_82"), Shared.SR.GetString("LE_ArgumentNullException_313") );


				if( this.dataType != value )
				{
					this.dataType = value;

					// SSP 4/8/02
					// Dirty the default editor flag because we may need to use a
					// different editor since we have a mask now.
					//
					this.DirtyDefaultEditor( );

					this.NotifyPropChange( PropertyIds.DataType, null );
				}
			}
		}

		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDataType() 
		{
			// for bound columns, we do not want to serialize data type
			if ( this.IsBound )
				return false;

			return this.dataType != typeof( System.String );
		}
 
		/// <summary>
		/// Resets DataType to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetDataType() 
		{
			//ROBA UWG125 8/8/01
			if ( !this.IsBound )
				this.DataType = typeof( System.String );
		}

		// SSP 8/11/03 - Optimizations
		// In CellUIElement.PositionChildElements we check if the cell value is an Image or not.
		// In most cases we don't need to do that. So added a flag off the column that will 
		// prevent the cell ui element from checking if the cell value is an image for columns
		// that we know won't contain images.
		// Added ShouldCheckForImageCellValue internal property.
		//
		internal bool ShouldCheckForImageCellValue
		{
			get
			{
				return this.shouldCheckForImageCellValue;
			}
		}

		// SSP 12/10/02 UWG1768
		// Added ProportionalResizeResolved method.
		//
		internal bool ProportionalResizeResolved
		{
			get
			{
				// SSP 12/8/03 UWG2769
				// When the user resizes a column in auto-fit columns mode, we do not want to apply
				// the extra delta width to the column that's being resized in Band.FitColumnsToWidth
				// method. Added this internal flag which is temporarily set to accomplish this.
				//
				// ----------------------------------------------------------------------------
				if ( null != this.header && DefaultableBoolean.Default != this.header.proportionalResizeOverride )
					return DefaultableBoolean.True == this.header.proportionalResizeOverride;
				// ----------------------------------------------------------------------------

				// If the ProportionalResize property has the default value of false, then return true
				// when the column is in a group and we are autofitting columns because when a group
				// is resized, only the last column gets resized by default. However when autofitting
				// columns, we need to proportionally resize all of the columns in the group.
				//
				if ( null != this.Layout && this.Layout.AutoFitAllColumns && !this.ProportionalResize )
				{
					for ( int i = 0; i < this.Band.Columns.Count; i++ )
					{
						UltraGridColumn column = this.Band.Columns[i];

						// Skip non-visible columns.
						// MD 1/20/09 - Groups in RowLayout
						// Use the HiddenResolved property because it takes into account nested groups.
						//if ( column.Hidden || ( null != column.Group && column.Group.Hidden ) )
						if ( column.HiddenResolved )
							continue;

						// MD 1/20/09 - Groups in RowLayout
						// This condition no longer applies.
						//// When we have a group in a band, all the columns with no groups are not visible 
						//// since they don't belong to any groups.
						////
						//// SSP 3/4/04
						//// Changed the behavior to give row-layout higher priority than the groups.
						//// Meaning if there are groups and they turn set UseRowLayout to true, we
						//// will ignore groups and use the row-layout functionality. So instead of
						//// using HasGroups, use GroupsDisplayed because that's the final say in
						//// whether groups are being displayed or not.
						////
						////if ( this.Band.HasGroups && null == column.Group )
						//if ( this.Band.GroupsDisplayed && null == column.Group )
						//    continue;

						// If any one of the columns has ProportionalResize set to a non-default value, 
						// then honor the value of this property.
						//
						if ( column.ProportionalResize )
							return false;
					}

					// If the user has not set proportional resize on any one of the columns to true, 
					// then return true when auto fitting columns in a group.
					//
					return true;
				}
				
				return this.ProportionalResize;
			}
		}

		/// <summary>
		/// Determines adjustment of column width when a group is resized.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a group is resized, all columns with this property set to True and the <b>AllowColSizing</b> property of their band set to 2 (AllowColSizingSync) or 3 (AllowColSizingFree) will have their width's adjusted proportionally. If no columnin the group satisfies these conditions, the rightmost column in a band with its <b>AllowColSizing </b>property set to 2 (AllowColSizingSync) or 3 (AllowColSizingFree) will be adjusted when the group is resized.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_ProportionalResize")]
		[LocalizedCategory("LC_Behavior")]
		public bool ProportionalResize
		{
			get 
			{
				// SSP 12/10/02 UWG1768
				// We should honor the proportional resize even when auto fit columns is true and
				// the column is in a group. The user may have explicitly set the ProportionalResize
				// on this column false. To fix the problem that below code was supposed to fix,
				// added a ProportionalResizeResolved method with a better logic that takes into
				// account the user explicitly setting the property to false.
				// Commented below code out.
				//
				

				return this.proportionalResize;
			}
			set
			{
				if(this.proportionalResize != value)
				{
					this.proportionalResize = value;
					this.NotifyPropChange( PropertyIds.ProportionalResize, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeProportionalResize() 
		{
			return this.proportionalResize != false;
		}
 
		/// <summary>
		/// Resets ProportionalResize to its default value (False).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetProportionalResize() 
		{
			this.ProportionalResize = false;
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeLevel() 
		{
			return this.level > 0;
		}
 
		/// <summary>
		/// Resets Level to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetLevel() 
		{
			this.level = 0;
		}



		/// <summary>
		/// Determines if the width of the column can be changed by the user.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use the <b>LockedWidth</b> property to disable user resizing of a column. Columns can still be resized through code even when this property is True. Note that setting this property to True may disable the resizing of other columns than the one specified. If the specified column is synchronized with a column in a child band, that column will also become locked. Similarly, setting the <b>LockedWidth</b> property to True for certain columns in a group may result in the user being unable to resize the group, depending on the position of the locked columns.</p>
		/// <p class="body">This property is ignored for chaptered columns; that is, columns whose <b>DataType</b> property is set to 136 (DataTypeChapter).</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_LockedWidth")]
		[LocalizedCategory("LC_Behavior")]
		public bool LockedWidth
		{
			get 
			{
				return this.lockedWidth;
			}
			set
			{
				if(this.lockedWidth != value)
				{
					this.lockedWidth = value;
					this.NotifyPropChange( PropertyIds.LockedWidth, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeLockedWidth() 
		{
			return this.lockedWidth != false;
		}
 
		/// <summary>
		/// Reset LockedWidth to its default value (False).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetLockedWidth() 
		{
			this.LockedWidth = false;
		}


		/// <summary>
		/// Returns or sets the input mask associated with the masked edit control.
		/// </summary>
		/// <remarks>
		/// <p class="body">When an input mask is defined, placeholders are defined by the <see cref="UltraGridColumn.PromptChar"/> property. When inputting data, the user can only replace a placeholder with a character that is of the same type as the one specified in the input mask. If the user enters an invalid character, the control rejects the character. The control can distinguish between numeric and alphabetic characters for validation, but cannot validate for valid content, such as the correct month or time of day.</p>
		/// <p class="Body">The input mask can consist of the following characters:</p>
		/// <p>
		/// <table border="1" cellpadding="3" width="100%" class="FilteredItemListTable">
		/// <thead>
		/// <tr><td class="body" width="23%"><p>Character</p></td><td class="body" width="66%"><p>Description</p></td></tr></thead>
		/// <tbody>
		/// <tr><td class="body" width="23%"><p>#</p></td><td class="body" width="66%"><p>Digit placeholder. Character must be numeric (0-9) and entry is required. </p></td></tr>
		/// <tr><td class="body" width="23%"><p>.</p></td><td class="body" width="66%"><p>Decimal placeholder. The actual character used is the one specified as the decimal placeholder by the system's international settings. This character is treated as a literal for masking purposes. </p></td></tr>
		/// <tr><td class="body" width="23%"><p>,</p></td><td class="body" width="66%"><p>Thousands separator. The actual character used is the one specified as the thousands separator by the system's international settings. This character is treated as a literal for masking purposes.</p></td></tr>
		/// <tr><td class="body" width="23%">:</td><td class="body" width="66%">Time separator. The actual character used is the one specified as the time separator by the system's international settings. This character is treated as a literal for masking purposes</td></tr>
		/// <tr><td class="body" width="23%">/</td><td class="body" width="66%">Date separator. The actual character used is the one specified as the date separator by the system's international settings. This character is treated as a literal for masking purposes.</td></tr>
		/// <tr><td class="body" width="23%">\</td><td class="body" width="66%">Treat the next character in the mask string as a literal. This allows you to include the '#', '&amp;', 'A', and '?' as well as other characters with special meanings in the mask. This character is treated as a literal for masking purposes.</td></tr>
		/// <tr><td class="body" width="23%">&amp;</td><td class="body" width="66%">Character placeholder. Valid values for this placeholder are ANSI characters in the following ranges: 32-126 and 128-255 (keyboard and foreign symbol characters).</td></tr>
		/// <tr><td class="body" width="23%">&gt;</td><td class="body" width="66%">Convert all the characters that follow to uppercase.</td></tr>
		/// <tr><td class="body" width="23%">&lt;</td><td class="body" width="66%">Convert all the characters that follow to lowercase.</td></tr>
		/// <tr><td class="body" width="23%">A</td><td class="body" width="66%">Alphanumeric character placeholder. For example: a-z, A-Z, or 0-9. Character entry is required.</td></tr>
		/// <tr><td class="body" width="23%">a</td><td class="body" width="66%">Alphanumeric character placeholder. For example: a-z, A-Z, or 0-9. Character entry is not required.</td></tr>
		/// <tr><td class="body" width="23%">9</td><td class="body" width="66%">Digit placeholder. Character must be numeric (0-9) but entry is not required.</td></tr>
		/// <tr><td class="body" width="23%">-</td><td class="body" width="66%">Minus sign when followed by a number section defined by series of 'n's (like in "-nn,nnn.nn") indicates that negative numbers are allowed. When not followed by a series of 'n's, it's taken as a literal. Minus sign will only be shown when the number is actually negative.</td></tr>
		/// <tr><td class="body" width="23%">+</td><td class="body" width="66%">Plus sign when followed by a number section defined by series of 'n's (like in "-nn,nnn.nn") indicates that negative numbers are allowed. However, it differs from '-' in the respect that it will always show a '+' or a '-' sign depending on whether the number is positive or negative.</td></tr>
		/// <tr><td class="body" width="23%">C</td><td class="body" width="66%">Character or space placeholder. Character entry is not required. This operates exactly like the '&amp;' placeholder, and ensures compatibility with Microsoft Access.</td></tr>
		/// <tr><td class="body" width="23%">?</td><td class="body" width="66%">Letter placeholder. For example: a-z or A-Z. Character entry is not required.</td></tr>		
		/// <tr><td class="body" width="23%">n</td><td class="body" width="66%">Digit placeholder. A group of n's can be used to create a numeric section where numbers are entered from right to left. Character must be numeric (0-9) but entry is not required.</td></tr>
		/// <tr><td class="body" width="23%">mm, dd, yy</td><td class="body" width="66%">Combination of these three special tokens can be used to define a date mask. mm for month, dd for day, yy for two digit year and yyyy for four digit year. Examples: mm/dd/yyyy, yyyy/mm/dd, mm/yy.</td></tr>
		/// <tr><td class="body" width="23%">hh, mm, ss, tt</td><td class="body" width="66%">Combination of these three special tokens can be used to define a time mask. hh for hour, mm for minute, ss for second, and tt for AP/PM. Examples: hh:mm, hh:mm tt, hh:mm:ss.</td></tr>
		/// <tr><td class="body" width="23%">{date}</td><td class="body" width="66%"><b>{date}</b> token is a place holder for short date input. The date mask is derived using the underlying culture settings.</td></tr>
		/// <tr><td class="body" width="23%">{time}</td><td class="body" width="66%"><b>{time}</b> token is a place holder for short time input. Short time typically does not include the seconds portion. The time mask is derived using the underlying culture settings.</td></tr>
		/// <tr><td class="body" width="23%">{longtime}</td><td class="body" width="66%"><b>{longtime}</b> token is a place holder for long time input. Long time typically includes the seconds portion. The long time mask is derived using the underlying culture settings.</td></tr>
		/// <tr><td class="body" width="23%">{double:i.f:c}</td><td class="body" width="66%"><b>{double:i.f:c}</b> is a place holder for a mask that allows floating point input where <b>i</b> and <b>f</b> in <b>i.f</b> specify the number of digits in the integer and fraction portions respectively. The <b>:c</b> portion of the mask is optional and it specifies that the inputting of the value should be done continous across fraction and integer portions. For example, with <b>:c</b> in the mask, in order to enter 12.34 the user types in "1234". Notice that the decimal separator character is missing. This allevietes the user from having to type in the decimal separator.</td></tr>
		/// <tr><td class="body" width="23%">{double:-i.f:c}</td><td class="body" width="66%">Same as <b>{double:i.f:c}</b> except this allows negative numbers.</td></tr>
		/// <tr><td class="body" width="23%">{currency:i.f:c}</td><td class="body" width="66%">Same as <b>{double:i.f:c}</b> except the mask is constructed based on currency formatting information of the underlying format provider or the culture. It typically has the currency symbol and also displays the group characters.</td></tr>
		/// <tr><td class="body" width="23%">{currency:-i.f:c}</td><td class="body" width="66%">Same as <b>{currency:i.f:c}</b> except this allows negative numbers.</td></tr>
		/// <tr><td class="body" width="23%">Literal</td><td class="body" width="66%">All other symbols are displayed as literals; that is, they appear as themselves.</td></tr>
		/// </tbody></table></p>
		/// <p>You can also escape the mask with {LOC} character sequence to indicate that symbols in the following table should be mapped to the associated symbols in the underlying culture settings.</p>
		/// <p><table border="1" cellpadding="3" width="100%" class="FilteredItemListTable">
		/// <thead>
		/// <tr><td class="body" width="23%"><p>Character</p></td><td class="body" width="66%"><p>Description</p></td></tr></thead>
		/// <tbody>
		/// <tr><td class="body" width="23%">$</td><td class="body" width="66%">Currency symbol.</td></tr>
		/// <tr><td class="body" width="23%">/</td><td class="body" width="66%">Date seperator.</td></tr>
		/// <tr><td class="body" width="23%">:</td><td class="body" width="66%">Time seperator.</td></tr>
		/// <tr><td class="body" width="23%">,</td><td class="body" width="66%">Thousands seperator.</td></tr>
		/// <tr><td class="body" width="23%">.</td><td class="body" width="66%">Decimal seperator.</td></tr>
		/// <tr><td class="body" width="23%">+</td><td class="body" width="66%">Positive sign.</td></tr>
		/// <tr><td class="body" width="23%">-</td><td class="body" width="66%">Negative sign.</td></tr>
		/// </tbody></table></p>
		/// <seealso cref="PadChar"/>
		/// <seealso cref="PromptChar"/>
		/// <seealso cref="MaskDataMode"/>
		/// <seealso cref="MaskDisplayMode"/>
		/// <seealso cref="MaskClipMode"/>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_MaskInput")]
		[LocalizedCategory("LC_Mask")]
		[ Localizable( true ) ]
		// SSP 5/3/04 - Designer Enhancements
		// Added mask input standard values. To do that we need to implement IEditType
		// interface and add a MaskTypeConverter attribute to the mask properties.
		// 
		[ TypeConverter( typeof( Infragistics.Win.MaskTypeConverter ) ) ]
		// SSP 4/11/05 BR03077
		//
		[ Editor( typeof( MaskUITypeEditor ), typeof( UITypeEditor ) ) ]
		public string MaskInput
		{
			get 
			{
				return this.maskInputString;
			}
			set
			{
				if(this.maskInputString != value)
				{
					this.maskInputString = value;

					// SSP 8/14/01 if mask is changed, then set the cached
					// parsed mask to null so that it gets recreated next time
					// it's asked for
					this.ClearCachedParsedMask( );

					// SSP 4/8/02
					// Dirty the default editor flag because we may need to use a
					// different editor since we have a mask now.
					//
					this.DirtyDefaultEditor( );
					
					this.NotifyPropChange( PropertyIds.MaskInput, null );
				}
			}
		}


		internal void ResolveMaskLiteralsAppearance( ref AppearanceData appData, ref AppearancePropFlags propFlags )
		{
			// SSP 3/12/06 - App Styling
			// 
			AppStyling.ResolutionOrderInfo orderInfo = StyleUtils.GetResolutionOrder( this.Layout );

			// If this column has mask literals appearance set, then get
			// the set properties from it.
			//
			// SSP 3/8/06 - App Styling
			// 
			//if ( this.HasMaskLiteralsAppearance )
			if ( this.HasMaskLiteralsAppearance && orderInfo.UseControlInfo )
			{
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( 
				//	ref appData, 
				//	this.MaskLiteralsAppearance.Data, 
				//	ref propFlags );
				this.MaskLiteralsAppearance.MergeData( ref appData, ref propFlags );
			}
			

			// SSP 3/12/06 - Optimization
			// Use the MergeBLOverrideAppearances instead.
			// 
			// --------------------------------------------------------------------------------------
			// SSP 3/8/06 - App Styling
			// Only resolve if useControlInfo is true.
			// 
			if ( orderInfo.UseControlInfo )
				this.Band.MergeBLOverrideAppearances( ref appData, ref propFlags, UltraGridOverride.OverrideAppearanceIndex.MaskLiterals );

			
			// --------------------------------------------------------------------------------------
		}


		/// <summary>
		/// If the cell has MaskInput set or is using a UltraMaskedEdit control
		/// then this appearance will be applied to the literal chars while in
		/// edit mode.
		/// </summary>
		[LocalizedDescription("LD_AppearanceCache_P_MaskLiteralsAppearance")]
		[LocalizedCategory("LC_Appearance")]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		public Infragistics.Win.AppearanceBase MaskLiteralsAppearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.maskLiteralsAppearanceHolder )
				{
					this.maskLiteralsAppearanceHolder = new Infragistics.Win.AppearanceHolder();
                    
					// hook up the prop change notifications
					//
					this.maskLiteralsAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.Layout != null)
					this.maskLiteralsAppearanceHolder.Collection = this.Layout.Appearances;

				return this.maskLiteralsAppearanceHolder.Appearance;
			}
			set
			{
				if ( this.maskLiteralsAppearanceHolder == null				|| 
					!this.maskLiteralsAppearanceHolder.HasAppearance		|| 
					value != this.maskLiteralsAppearanceHolder.Appearance )
				{
					// remove the old prop change notifications
					//
					// SSP 8/20/02
					//
					//if ( null != this.maskLiteralsAppearanceHolder )
					if ( null == this.maskLiteralsAppearanceHolder )
					{
						this.maskLiteralsAppearanceHolder = new Infragistics.Win.AppearanceHolder();
            
						// hook up the new prop change notifications
						//
						this.maskLiteralsAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					// Initialize the collection
					//
					if (this.Layout != null)
						this.maskLiteralsAppearanceHolder.Collection = this.Layout.Appearances;

					this.maskLiteralsAppearanceHolder.Appearance = value;

					// notify listeners
					//
					this.NotifyPropChange( PropertyIds.MaskLiteralsAppearance );
				}
			}
		}

		/// <summary>
		/// Returns true if an CellAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasMaskLiteralsAppearance
		{
			get
			{
				return ( null != this.maskLiteralsAppearanceHolder &&
					this.maskLiteralsAppearanceHolder.HasAppearance );
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeMaskLiteralsAppearance() 
		{
			return ( this.HasMaskLiteralsAppearance &&
				this.maskLiteralsAppearanceHolder.ShouldSerialize() );
		}
 
		/// <summary>
		/// Reset Cells appearance
		/// </summary>		
		// MRS 12/9/04 - BR01204
		// Marked Obsolete (mis-spelled), Hidden, and pointed to the new spelling
		//[EditorBrowsable(EditorBrowsableState.Advanced)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[ Obsolete("Use ResetMaskLiteralsAppearance property instead", false ) ]
		public void ResetMaskListeralsAppearance() 
		{
			if ( this.HasMaskLiteralsAppearance )
			{
				// MRS 12/9/04 - BR01204
				// Marked Obsolete. Point to the correctly spelled method
				this.ResetMaskLiteralsAppearance();

				//				//				// remove the prop change notifications
				//				//				//
				//				//				this.cellAppearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				//				//                    
				//				//				this.cellAppearanceHolder.Reset();
				//				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//				//this.maskListeralsAppearanceHolder.Appearance.Reset();
				//				
				//				// Call reset on the holder instead of the appearance
				//				//
				//				this.maskListeralsAppearanceHolder.Reset();
				//
				//				// Notify listeners that the appearance has changed. 
				//				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.MaskLiteralsAppearance );
			}
		}


		// MRS 12/9/04 - BR01204
		// Added a shiny, new, correctly-spelled method to replace the mis-spelled one.
		/// <summary>
		/// Reset Cells appearance
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMaskLiteralsAppearance() 
		{
			if ( this.HasMaskLiteralsAppearance )
			{				
				this.maskLiteralsAppearanceHolder.Reset();

				// Notify listeners that the appearance has changed. 
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.MaskLiteralsAppearance );
			}
		}

		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		// SSP 8/8/02
		// Made this internal protected.
		//
		//protected bool ShouldSerializeMaskInput() 
		internal protected bool ShouldSerializeMaskInput() 
		{
			// SSP 7/29/02
			// Use null as the default mask input. Why an empty string ?. 
			// The reason I changed it though was that we need to differentiate
			// between the default setting and the user setting it to "" to
			// indicate "No Mask".
			//
			//return this.maskInputString.Length > 0;
			return null != this.maskInputString;
		}
 
		/// <summary>
		/// Resets MaskInput to its default value ("").
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMaskInput() 
		{
			// SSP 7/29/02
			// Use null as the default mask input. Why an empty string ?. 
			// The reason I changed it though was that we need to differentiate
			// between the default setting and the user setting it to "" to
			// indicate "No Mask".
			//
			//this.MaskInput = string.Empty;
			this.MaskInput = null;
		}

		/// <summary>
		/// Determines how the control stores null or empty data in the database.
		/// </summary>
		/// <remarks>
		/// <p class="body">Different databases deal with null values in different ways. Since the UltraGrid is designed to work with a variety of data sources, it has the ability to query the back end and find out which way to store null values. Depending on the type of connection to the database, this can have a significant impact on performance. If you know how the database handles the storage of null values, you can improve performance by setting the <b>Nullable</b> property to either 1 (NullableNull) or 2 (NullableEmptyString). Setting this value to 0 (NullableAutomatic) will provide a greater range of compatibility, but performance will suffer.</p>
		/// <p class="body">If the database does not support null values, and you attempt to force the storage of nulls by setting <b>Nullable</b> to 1 (NullableNull), an error will result. If you encounter problems when you attempt to save a record that contains a null value, you can change the setting of <b>Nullable</b> which should fix the problem. In any case, you should implement error-checking code to insure that the storage operation succeeded.</p>
		/// <p class="body">The setting of this property controls how the UltraWinGrid control will attempt to store the null value. In some cases, the mechanism used for data binding may change the null value before actually committing it to the database.</p> 
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_Nullable")]
		[LocalizedCategory("LC_Data")]
		public Nullable Nullable
		{
			get 
			{
				// JAS 2005 v2 XSD Support
				// The Nullable property is not stored in the column's ValueConstraint object
				// because this property is of type 'Nullable' while the ValueConstraint's property
				// is of type 'DefaultableBoolean.'
				//
				return this.nullable;
			}
			set
			{
				if(this.nullable != value)
				{
                    // MRS NAS v8.3 - Unit Testing
                    //if (!Enum.IsDefined(typeof(Nullable), value))
                    //    throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_84"));
                    if (!Enum.IsDefined(typeof(Nullable), value))
                        throw new InvalidEnumArgumentException("Nullable", (int)value, typeof(Nullable));

					this.nullable = value;

					// JAS 2005 v2 XSD Support
					// If this property had been set by the EnforceXsdConstraints method, then
					// clear the bit which represents this property so that the ClearXsdConstraints
					// method will know not to reset this value.
					//
					if( (this.XsdSuppliedConstraints & XsdConstraintFlags.Nullable) != 0 )
						this.XsdSuppliedConstraints &= ~XsdConstraintFlags.Nullable;

					this.NotifyPropChange( PropertyIds.Nullable, null );
				}
			}
		}

		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeNullable() 
		{            
			return this.nullable != Nullable.Automatic;
		}
 
		/// <summary>
		/// Resets Nullable to its default value (Automatic).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetNullable() 
		{
			this.Nullable = Nullable.Automatic;
		}

		/// <summary>
		/// Returns the string that will actually be used for cells that are null in this column..
		/// </summary>
		/// <remarks>
		/// You can use this property to customize the text displayed to the user when a null value is present in a cell. For example, you may want the cell to display "(empty)" or "no value". The default setting for this property is "null".
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		[ Browsable(false) ]
		public string NullTextResolved
		{
			get
			{
				// JJD 1/21/02 - UWG815 Moved nulltext from layout to override and column
				//
				if ( this.nullText != null )
					return this.nullText;

				if ( this.band == null )
					return string.Empty;

				if ( this.band.HasOverride &&
					this.band.Override.NullText != null )
					return this.band.Override.NullText;

				if ( this.Layout == null )
					return string.Empty;

				if ( this.Layout.HasOverride &&
					this.Layout.Override.NullText != null )
					return this.Layout.Override.NullText;

				return string.Empty;
			}
		}
		
		// JJD 1/21/02 - UWG815 Moved nulltext from layout to override and column
		//
		/// <summary>
		/// Returns or sets the string displayed in cells with null values.
		/// </summary>
		/// <remarks>
		/// You can use this property to customize the text displayed to the user when a null value is present in a cell. For example, you may want the cell to display "(empty)" or "no value". The default setting for this property is "null".
		/// </remarks>
		[ LocalizedDescription("LD_AppearanceCache_P_NullText")]
		[ LocalizedCategory("LC_Display") ]
		[ Localizable( true ) ]
		public string NullText
		{
			get
			{
				return this.nullText;
			}
			set			
			{			
				if ( this.nullText != value )
				{
					this.nullText = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.NullText );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeNullText() 
		{
			return ( this.nullText != null );
		}
 
		/// <summary>
		/// Resets NullText to its default value ("null").
		/// </summary>
		public void ResetNullText() 
		{
			this.NullText = null;
		}

		internal bool HasMask
		{
			get
			{
				// AS 1/8/03 - fxcop
				// Do not compare against string.empty - test the length instead
				//return null != this.MaskInput && !string.Empty.Equals( this.MaskInput );
				return null != this.MaskInput && this.MaskInput.Length > 0;
			}
		}


		internal ParsedMask ParsedMask
		{
			get
			{
				if ( this.HasMask )
				{
					if ( null == this.parsedMask )					
						this.parsedMask = new ParsedMask( this.MaskInput );
					
					return this.parsedMask;
				}

				return null;
			}
		}

		internal void ClearCachedParsedMask( )
		{
			this.parsedMask = null;
		}


		// SSP 8/14/01 - UWG36
		// Added this function to format the cell text with the mask assigned
		// to this column
		internal string FormatWithMaskMode( string text, MaskMode maskMode )
		{
			if (
				// SSP 7/9/02 UWG1322
				// If the column is multiline, then don't apply the mask.
				//
				DefaultableBoolean.True != this.CellMultiLineResolved &&
				this.HasMask && null != this.ParsedMask )
			{
				Debug.Assert( null != this.ParsedMask, "ParsedMask null when column has mask" );

				// SSP 8/17/01 UWG39
				// We would still need to format even if the mask mode is raw
				// because the data may include literals that the user may not
				// want shown in cells. So format anyways.
				

				if ( null != this.parsedMask )
				{
					int numOfCharsMatched = 0;

					string formattedText = this.ParsedMask.ApplyMask( text, 
						// SSP 3/6/02 UWG1042
						// Use the maskMode passed into the method instead of the mask display mode.
						//
						//this.MaskDisplayMode, this.PromptChar, this.PadChar, out numOfCharsMatched );
						maskMode, this.PromptChar, this.PadChar, out numOfCharsMatched );


					// Return the formatted string if all the characters of the
					// text matched the mask. Otherwise return the original text
					if ( numOfCharsMatched == text.Length )
						return formattedText;
				}
			}

			return text;
		}


		/// <summary>
		/// Returns or sets a value that determines the prompt character used during masked input.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this property to set the character used to indicate that user input is required at a specific location in the input mask. Although you can use any character, the standard input prompt character is the underscore.</p>
		/// <p class="body">This property will only accept a single character. If an attempt is made to assign a multi-character string to it, an error will occur.</p>
		/// <p class="body">The <b>MaskInput</b> property is used to specify how data input will be masked for the cells in a column. The mask usually includes literal characters that are used to delimit the data entered by the user. This property has no effect unless the <b>MaskInput</b> property is set, meaning that data masking is enabled.</p>
		/// <p class="body">When data masking is enabled, the <b>MaskClipMode</b> property determines how cell values are copied to the clipboard, the <b>MaskDataMode</b> property indicates how cell values are stored by the data source, and the <b>MaskDisplayMode</b> property specifies how cell values are displayed.</p>
		/// <seealso cref="PromptChar"/>
		/// <seealso cref="MaskInput"/>
		/// <seealso cref="MaskDataMode"/>
		/// <seealso cref="MaskDisplayMode"/>
		/// <seealso cref="MaskClipMode"/>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_PromptChar")]
		[LocalizedCategory("LC_Mask")]
		public char PromptChar
		{
			get 
			{
				return this.promptChar;
			}
			set
			{
				if( this.promptChar != value )
				{
					// SSP 1/24/03 UWG1941
					// Take 0 to be ' ' (space) character.
					//
					if ( 0 == (int)value )
						value = ' ';

					this.promptChar = value;
					this.NotifyPropChange( PropertyIds.PromptChar, null );
				}
			}
		}

		/// <summary>
		/// Determines the pad character used for formatting text with mask.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>PadChar</b> has effect when the <see cref="MaskInput"/> property is set
		/// and <see cref="MaskDisplayMode"/>, <see cref="MaskDataMode"/> or
		/// <see cref="MaskClipMode"/> is set to include pad characters.
		/// </p>
		/// <seealso cref="PromptChar"/>
		/// <seealso cref="MaskInput"/>
		/// <seealso cref="MaskDataMode"/>
		/// <seealso cref="MaskDisplayMode"/>
		/// <seealso cref="MaskClipMode"/>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_PadChar")]
		[LocalizedCategory("LC_Mask")]
		public char PadChar
		{
			get 
			{
				return this.padChar;
			}
			set
			{
				if ( this.padChar != value )
				{
					// SSP 1/24/03 UWG1941
					// Take 0 to be ' ' (space) character.
					//
					if ( 0 == (int)value )
						value = ' ';

					this.padChar = value;
					this.NotifyPropChange( PropertyIds.PadChar, null );
				}
			}
		}

		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializePromptChar() 
		{
			return this.promptChar != DEFAULT_PROMPT_CHAR;
		}
 
		/// <summary>
		/// Resets PromptChar to its default value (_).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetPromptChar() 
		{
			this.PromptChar = DEFAULT_PROMPT_CHAR;
		}

		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializePadChar() 
		{
			return this.padChar != DEFAULT_PAD_CHAR;
		}
 
		/// <summary>
		/// Resets PadChar to its default value ( ).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetPadChar() 
		{
			this.PadChar = DEFAULT_PAD_CHAR;
		}


		/// <summary>
		/// Property used to perform non-default group by comparisons. 
		/// </summary>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.HiddenWhenGroupBy"/>
		/// <remarks>
		/// <p class="body">This GroupByEvaluator object will be used for determining which rows are included in group by rows. Essentially
		/// it lets you use custom logic for grouping rows.</p>
		/// <p class="body">Care should be taken to ensure that the GroupByEvaluator is logically consistent with the <see cref="SortComparer"/>. For example, if the sort comparer does a case insensitive sort the group by evaluator logic should be case-insensitive as well.
		/// If your grouping logic requires that it be inconsistent with how the rows are sorted, then you need to implement
		/// <see cref="IGroupByEvaluatorEx"/> interface. <b>IGroupByEvaluatorEx</b> interface lets you supply logic for sorting rows
		/// for grouping purposes. See <see cref="IGroupByEvaluatorEx"/> interface for more information.
		/// </p>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		public IGroupByEvaluator GroupByEvaluator
		{
			get
			{
				return this.groupByEvaluator;
			}

			set
			{
                // MRS NAS v8.3 - Unit Testing
                // This code didn't make sense. We were always bumping the versions
                // even when setting the same value to the property. 
                // We were also not sending any PropertyChanged notification. 
                #region Old Code
                //if ( this.groupByEvaluator != value )
                //    this.groupByEvaluator = value;

                //if ( this.IsGroupByColumn )
                //{
                //    this.band.BumpGroupByHierarchyVersion();
                //    this.band.SortedColumns.BumpSortVersion();				
                //}
                #endregion //Old Code
                //
                if (this.groupByEvaluator != value)
                {
                    this.groupByEvaluator = value;

                    if (this.IsGroupByColumn)
                    {
                        this.band.BumpGroupByHierarchyVersion();
                        this.band.SortedColumns.BumpSortVersion();
                    }

                    // MRS NAS v8.3 - Unit Testing
                    this.NotifyPropChange(PropertyIds.GroupByEvaluator, null);
                }
			}
		}
		
		// SSP 10/20/04 UWG3747
		// Added GroupByComparer property to allow the user to sort group-by rows 
		// using a custom comparer.
		//
		/// <summary>
		/// Property used for specifying a custom comparer to sort group-by rows.
		/// </summary>
		/// <remarks>
		/// <p>This IComparer instance will be used for sorting group-by rows associated with this column if this column were a group-by column.</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.GroupByEvaluator"/> <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.SortComparer"/>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ), Browsable( false ) ]
		public IComparer GroupByComparer
		{
			get
			{
				return this.groupByComparer;
			}
			set
			{
				if ( this.groupByComparer != value )
				{
					this.groupByComparer = value;

					if ( null != this.Band && this.IsGroupByColumn && this.Band.HasSortedColumns )
						this.Band.SortedColumns.BumpSortVersion( );

                    // MRS NAS v8.3 - Unit Testing
                    this.NotifyPropChange(PropertyIds.GroupByComparer, null);
				}
			}
		}

		/// <summary>
		/// Property used to perform custom sort comparisons when sorting rows by this column.
		/// The values passed in the Compare method of the IComparer will be two 
		/// <see cref="UltraGridCell"/> objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This IComparer object will be used for sorting the rows by this column.
		/// </p>
		/// <p class="body">The values passed in the ICompare.Compare will be UltraGridCell objects needing comparison.</p>
		/// <p class="body">Care should be taken to ensure that the <see cref="GroupByEvaluator"/> is logically consistent with the <b>SortComparer</b>. For example, if the sort comparer does a case insensitive sort the group by evaluator logic should be case-insensitive as well.</p>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		public IComparer SortComparer
		{
			get
			{
				return this.comparer;
			}

			set
			{
                // MRS NAS v8.3 - Unit Testing
                // This code didn't make sense. We were always bumping the versions
                // even when setting the same value to the property. 
                // We were also not sending any PropertyChanged notification. 
                #region Old Code
                //if ( this.comparer != value )
                //    this.comparer = value;

                ////RobA UWG557 10/17/01
                ////if the column is in the sortedcols, re-sort
                ////
                //if ( this.band.SortedColumns.Exists( this.Key ) )
                //    this.band.SortedColumns.BumpSortVersion();
                #endregion //Old Code
                //
                if (this.comparer != value)
                {
                    this.comparer = value;

                    //RobA UWG557 10/17/01
                    //if the column is in the sortedcols, re-sort
                    //
                    if (this.band.SortedColumns.Exists(this.Key))
                        this.band.SortedColumns.BumpSortVersion();

                    // MRS NAS v8.3 - Unit Testing
                    this.NotifyPropChange(PropertyIds.SortComparer);
                }
            }
		}

		#region RowFilterComparer

		// SSP 11/10/05 BR07680
		// Added RowFilterComparer property.
		// 
		/// <summary>
		/// Comparer used to evaluate comparison operators for the row filtering functionality.
		/// The values passed in the Compare method of the IComparer will be the two values that
		/// are to be compareed.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>RowFilterComparer</b> is used to evaluate comparison operators for the row filtering
		/// functionality. This is useful if you want to compare values using custom logic for row 
		/// filtering functionality.
		/// </p>
		/// <p class="body">
		/// Unlike the <see cref="UltraGridColumn.SortComparer"/>, the values passed in the 
		/// Compare method of the RowFilterComparer will be the two values that are to be compared.
		/// The Compare method of the <see cref="UltraGridColumn.SortComparer"/> gets passed in two
		/// <see cref="UltraGridCell"/> instances. Also if filter row functionality is enabled, the 
		/// user can enter any value, typically in the form of a string, to filter the rows by. In 
		/// this case the Compare method may get called on this comparer where one value is a cell 
		/// value which is column's data type and the other value is a string. Therefore this 
		/// comparer should handle string values. It should also handle null or DBNull if those 
		/// value occur in the column.
		/// </p>
		/// </remarks>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		 Browsable(false) ]
		public IComparer RowFilterComparer
		{
			get
			{
				return this.rowFilterComparer;
			}

			set
			{
				if ( this.rowFilterComparer != value )
				{
					this.rowFilterComparer = value;

					if ( null != this.Band )
						this.Band.BumpRowFiltersVersion( );

                    // MRS NAS v8.3 - Unit Testing
                    this.NotifyPropChange(PropertyIds.RowFilterComparer);
				}
			}
		}

		#endregion // RowFilterComparer

		

		/// <summary>
		/// Returns or sets a value that indicates the sorted order of the column.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The UltraGrid can automatically sort the contents of 
		/// columns without the addition of any code. While the UltraGrid can sort the data 
		/// in columns automatically, it also gives you tools to implement your own sorting 
		/// behavior through code.
		/// </p>
		/// <p class="body">
		/// Column headers can display a sort indicator in a column's 
		/// header. When clicked and the <b>HeaderClickAction</b> property is set to  
		/// HeaderClickAction.SortSingle or HeaderClickAction.SortMulti, the 
		/// <b>SortIndicator</b> property is set to specify the order in which the column 
		/// should be sorted, and the column is added to a special Columns collection 
		/// just for sorted columns. If automatic sorting is disabled, in addition to 
		/// adding the column to the Columns collection accessed by <b>SortedCols</b>, the 
		/// control fires the <b>BeforeSortChange</b> and <b>AfterSortChange</b> events so that you 
		/// can examine the contents of the collection, check the value of the 
		/// <b>SortIndicator</b> property of each column, and perform the sort.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SortIndicator"/>
		/// <seealso cref="UltraGridOverride.HeaderClickAction"/>
		/// <seealso cref="UltraGridColumn.IsGroupByColumn"/>
		/// <seealso cref="UltraGridBand.SortedColumns"/>
		/// <seealso cref="SortedColumnsCollection.Add(UltraGridColumn, bool, bool)"/>
		/// </remarks>
		[ LocalizedDescription("LD_AppearanceCache_P_SortIndicator")]
		[ LocalizedCategory("LC_Behavior")]
		//[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.UltraWinGrid.SortIndicator SortIndicator
		{
			get 
			{
				return this.sortIndicator;
			}
			set
			{
                // MRS NAS v8.3 - Unit Testing
                //// SSP 4/24/03
                //// Check for the invalid enum was at the end of the property. Moved this here.
                ////
                //if ( !Enum.IsDefined( typeof(SortIndicator), value ) )
                //    throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_85") );                
                if (!Enum.IsDefined(typeof(SortIndicator), value))
                    throw new InvalidEnumArgumentException("SortIndicator", (int)value, typeof(SortIndicator));

				// SSP 8/12/03 - Optimizations - Grand Verify Version Number
				//
				if ( null != this.Layout )
					this.Layout.BumpGrandVerifyVersion( );

				// SSP 10/1/01 UWG388
				// Added InternalSetSortIndicator moved the below code to 
				// InternalSetSortIndicator.
				//
				// We should be calling SetSortedColumn so that the programmer
				// can sort by a column through code instead of just setting the
				// sort indicator to the value
				//
				if(null != this.Band)
				{
					// SSP 3/19/04 UWG3017
					// If the column is a group-by column, simply toggle the sort direction.
					// Calling SetSortedColumn will ungroup the column if isGroupBy parameter
					// is false and if isGroupBy parameter is true, it will rearrange the sorted
					// cols collection and make the group-by column the last group-by column. 
					// Either of these behaviors are undesirable.
					// Added the if block and enclosed the existing code into the else block.
					//
					if ( this.IsGroupByColumn 
						&& ( SortIndicator.Ascending == value || SortIndicator.Descending == value ) )
					{
						this.InternalSetSortIndicator( value );
						this.Band.SortedColumns.OnSortChanged( );
					}
					else
					{
						this.Band.SetSortedColumn( this, value, false,
							HeaderClickAction.SortSingle == this.Band.HeaderClickActionResolved );
					}
				}
				else
				{
					//DA 8.10.02
					//uwg1243 In order to handle proper persistance of the disabled enum,
					//we serialize it out, during de-serialization we set it explicitly if
					//we do not have a band object.
					if( this.sortIndicator != value &&
						SortIndicator.Disabled == value)
					{
						this.sortIndicator = value;					

						
					}
				}

				// SSP 4/24/03
				// What's this doing in the end here. It should be in the beginning.
				// Moved this in the beginning.
				//
								
			}
		}

		// SSP 10/1/01 UWG388
		// Added InternalSetSortIndicator
		internal void InternalSetSortIndicator( SortIndicator sortIdicator )
		{
			if ( !Enum.IsDefined( typeof(SortIndicator), sortIdicator ) )
				throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_85") );

			if( this.sortIndicator != sortIdicator )
			{
				this.sortIndicator = sortIdicator;					

				// SSP 1/16/02 
				// Since we are only looking at the SortDirection, we don't
				// need to have a version. We just verify against the sort direction.
				//
				//this.groupBySortVersion++;
				// Since the sort indicator has changed, bump the sort version number.
				//
				if ( this.Band.HasSortedColumns )
					this.Band.SortedColumns.BumpSortVersion( );

				// SSP 8/12/03 - Optimizations - Grand Verify Version Number
				//
				else if ( null != this.Layout )
					this.Layout.BumpGrandVerifyVersion( );

				// JAS v5.2 Wrapped Header Text 4/25/05
				// The insertion/removal of a sort indicator requires the header heights to be recalculated.
				//
				if( this.Layout != null )
					this.Layout.BumpColumnHeaderHeightVersionNumber();

				this.NotifyPropChange( PropertyIds.SortIndicator );
			}
		}


		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSortIndicator() 
		{
			if(this.SortIndicator == SortIndicator.Disabled)
				return true;
			else 
				return false;
		}

				private bool ShouldSerializeSortIndicatorInternal()
		{
			return (this.sortIndicator != SortIndicator.None );
		}
 
		/// <summary>
		/// Reset sort Indicator to none
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetSortIndicator() 
		{
			//RobA 10/2/01 UWG388
			//this.SortIndicator = SortIndicator.None;
			this.InternalSetSortIndicator( SortIndicator.None );
		}


		/// <summary>
		/// Determines the style of the column.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property specifies what type of cell will be used to display and input data for the column.</p>
		/// <p class="body">The setting of this property for a column may affect other aspects of the control's operation. For example, using one of the dropdown styles requires the <b>ValueList</b> property of the column to be set in order to fill the dropdown list with text. It will also cause the <b>CellListSelect</b> event to be fired whenever an item is selected from the list. Similarly, setting this property to one of the button styles will cause the control to fire the <b>ClickCellButton</b> event.</p>
		/// <p class="body">
		/// <b>Note: </b>This property is for convenience. It lets you set the edit style of a column to one of the 
		/// commonly used edit styles. This list does not include all the styles that are available. If a 
		/// style is not listed in this enum then you can use the <see cref="UltraGridColumn.Editor"/> 
		/// property along with other properties that the <see cref="UltraGridColumn"/> object exposes, 
		/// like <see cref="UltraGridColumn.MinValue"/>, <see cref="UltraGridColumn.MaxValue"/>, 
		/// <see cref="UltraGridColumn.MinValueExclusive"/>, <see cref="UltraGridColumn.MaxValueExclusive"/>, 
		/// <see cref="UltraGridColumn.MaskInput"/>, <see cref="UltraGridColumn.FormatInfo"/>, 
		/// <see cref="UltraGridColumn.Format"/>, <see cref="UltraGridColumn.MaxLength"/>, 
		/// <see cref="UltraGridColumn.RegexPattern"/> etc... to accomplish the desired column style.
		/// As a matter of fact some the styles set some of the these properties to accomplish the desired
		/// behavior. For example the <b>CurrencyPositive</b> style sets the <see cref="UltraGridColumn.Editor"/>
		/// to <see cref="Infragistics.Win.EditorWithMask"/> instance and the <see cref="UltraGridColumn.MinValueExclusive"/> 
		/// to 0. Also you can set these properties to further refine the behavior of a column style. This is 
		/// because these properties take precedence over the <b>Style</b> property.
		/// </p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_Style")]
		[LocalizedCategory("LC_Behavior")]
		public Infragistics.Win.UltraWinGrid.ColumnStyle Style
		{
			get 
			{
				return this.style;
			}
			set
			{
				if(this.style != value)
				{
                    // MRS NAS v8.3 - Unit Testing
                    //if ( !Enum.IsDefined( typeof(ColumnStyle), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_86") );                    
                    if (!Enum.IsDefined(typeof(ColumnStyle), value))
                        throw new InvalidEnumArgumentException("Style", (int)value, typeof(ColumnStyle));

					// SSP 7/17/02
					// Make sure the check box style is appropriate for the column.
					//
					if ( ColumnStyle.CheckBox == value || ColumnStyle.TriStateCheckBox == value )
					{
						if ( null != this.Band && null != this.Band.Layout &&
							!this.Band.Layout.CheckEditor.CanRenderType( this.DataType ) )
							// SSP 6/23/03 - Localization
							//
							//throw new ArgumentException( "CheckBox style is not supported for the column's data type." );
							throw new ArgumentException( SR.GetString( "LER_ArgumentException_10" ) );
					}
                    // MBS 1/3/08 - BR29118
                    else if (ColumnStyle.TrackBar == value)
                    {
                        // MBS 4/24/08
                        // Check to make sure that the band exists at this point
                        // before trying to validate the DataType, since if we're just initializing
                        // the grid, the DataType will still be a String, which will cause this
                        // to throw an exception.
                        if (this.Band != null)
                        {
                            Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings editorSettings;
                            Infragistics.Win.TrackBarEditor cachedEditor = this.GetCachedEditor(
                                typeof(Infragistics.Win.TrackBarEditor),
                                ColumnStyle.TrackBar,
                                out editorSettings) as Infragistics.Win.TrackBarEditor;

                            if (cachedEditor != null && cachedEditor.CanRenderType(this.DataType) == false)
                                throw new ArgumentException(SR.GetString("LER_ArgumentException_17"));
                        }
                    }

					this.style = value;

					// SSP 10/14/02
					// Dirty the default editor flag because we may need to use a
					// different editor since we have a value list now.
					//
					this.DirtyDefaultEditor( );

					this.NotifyPropChange( PropertyIds.Style, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeStyle() 
		{
			return this.style != ColumnStyle.Default;
		}
 
		/// <summary>
		/// Resets Style to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetStyle() 
		{
			this.Style = ColumnStyle.Default;
		}

		/// <summary>
		/// Determines if the cells in the column can get focus when tabbing through the columns.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this property to specify whether the user can navigate to a cell or the cells in a column by pressing the TAB key.</p>
		/// <p class="body">The <b>TabNavigation</b> property is used to specify how the control will respond when the TAB key is pressed.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_TabStop")]
		[LocalizedCategory("LC_Behavior")]
		public bool TabStop
		{
			get 
			{
				return this.tabStop;
			}
			set
			{
				if( this.tabStop != value )
				{
					this.tabStop = value;
					this.NotifyPropChange( PropertyIds.TabStop, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeTabStop() 
		{
			return this.tabStop != true;
		}
 
		/// <summary>
		/// Resets TabStop to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetTabStop() 
		{
			this.TabStop = true;
		}

		
		/// <summary>
		/// Returns or sets a value that determines if a vertical scrollbar is displayed in a multiline cell.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property can be used to allow the user to scroll a column whose cells contain too much text to be displayed at once.</p>
		/// <p class="body">If the <b>CellMultiLine</b> property, which is used to indicate whether a cell's text should be displayed in multiple lines, is set to False for the column, this property is ignored.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_VertScrollBar")]
		[LocalizedCategory("LC_Appearance")]
		public bool VertScrollBar
		{
			get 
			{
				return this.vertScrollBar;
			}
			set
			{
				if(this.vertScrollBar != value)
				{
					this.vertScrollBar = value;
					this.NotifyPropChange( PropertyIds.VertScrollBar, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeVertScrollBar() 
		{
			return this.vertScrollBar != false;
		}
 
		/// <summary>
		/// Resets VertScrollBar to its default value (False).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetVertScrollBar() 
		{
			this.VertScrollBar = false;
		}


		/// <summary>
		/// Returns or sets the minimum width for a column.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MinWidth</b> property limits the width of the object to no less than the value specified. Setting the value of <b>MinWidth</b> to 0 indicates that there is no minimum width limit for the object, although a 120 twip minimum is imposed system-wide.</p>
		/// <p class="body">You cannot set <b>MinWidth</b> to a value greater than that specified by the <b>MaxWidth</b> property.</p>
		/// <p class="body">This property is ignored for chaptered columns; that is, columns whose <b>IsChaptered</b> property returns <b>true</b>.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_MinWidth")]
		[LocalizedCategory("LC_Behavior")]
		public int MinWidth
		{
			get 
			{
				//return this.minWidth;
				return GetMinWidth( );
			}
			set
			{
				if(this.minWidth != value)
				{
					// AS - 10/31/01
					// The maxwidth can be 0 and you should still be able to set the
					// minwidth to a value greater than 0 - i.e. you should be able
					// to set the minwidth without setting the maxwidth.
					//
					// SSP 10/31/01 UWG610
					//
					//if ( value > this.MaxWidth )
					if ( value > this.MaxWidth && this.MaxWidth != 0)
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_86") );

					// SSP 12/9/02 UWG1768
					// Ensure the current width satisfies the minWidth constraint.
					//
					// ----------------------------------------------------------------
					if ( this.Width < value )
					{
						// We can't set the minWidth before the if condition because Width will return a 
						// value truncated so that it is less than minWidth and we will never get in here.
						//
						this.minWidth = value;

						if ( this.width > 0 && this.width < this.minWidth )
						{
							this.Width = this.minWidth;
						}
						// MD 1/21/09 - Groups in RowLayouts
						// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
						//else if ( null != this.Group )
						//{
						//    // Adjust the group to take into account increased or reduced width of the
						//    // column.
						//    //
						//    this.Group.AdjustAllColumnWidths( true, false, false, false, false, this );
						//}
						else
						{
							UltraGridGroup group = this.GroupResolved;

							if ( null != group )
							{
								// Adjust the group to take into account increased or reduced width of the
								// column.
								//
								group.AdjustAllColumnWidths( true, false, false, false, false, this );
							}
						}
						
						if ( null != this.Band && null != this.Band.Layout )
						{							
							this.Band.Layout.SynchronizedColListsDirty = true;
							this.Band.Layout.DirtyGridElement( );
						}
					}
					// ----------------------------------------------------------------

					this.minWidth = value;
					this.NotifyPropChange( PropertyIds.MinWidth, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMinWidth() 
		{
			return this.minWidth != 0;
		}
 
		/// <summary>
		/// Resets MinWidth to its default value (0, meaning there is no minimum).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMinWidth() 
		{
			this.MinWidth = 0;
		}


		/// <summary>
		/// Returns or sets the maximum width of the object in container units.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MaxWidth</b> property limits the width of the object to no more than the value specified. Setting the value of <b>MaxWidth</b> to 0 indicates that there is no maximum width limit for the object, or that the object's width is limited only by available screen area.</p>
		/// <p class="body">If the object has a <b>MinWidth</b> property, you cannot set <b>MaxWidth</b> to a value less than that specified by the <b>MinWidth</b> property.</p>
		/// <p class="body">This property is ignored for chaptered columns; that is, columns whose <b>IsChaptered</b> property returns <b>true</b>.</p>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_MaxWidth")]
		[LocalizedCategory("LC_Behavior")]
		public int MaxWidth
		{
			get 
			{
				return this.maxWidth;
			}
			set
			{
				if(this.maxWidth != value)
				{
					// SSP 10/31/01 UWG610
					//
					// SSP 7/9/02
					// Allow setting the max width to 0 because that's the default
					// and indicates that the user has not specified any width.
					//
					if ( 0 != value && value < this.MinWidth )
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_87") );

					// SSP 12/9/02 UWG1768
					// Ensure the current width satisfies the maxWidth constraint.
					//
					// ----------------------------------------------------------------
					//
					// JAS 5/23/05 BR03773
					// Only perform the resizing code if the new MaxWidth is greater than zero,
					// otherwise the column will attempt to resize itself to a width of zero.
					//
					//if ( this.Width > value )
					if ( this.Width > value && value > 0 )
					{			
						// We can't set the maxWidth before the if condition because Width will return a 
						// value truncated so that it is less than maxWidth and we will never get in here.
						//
						this.maxWidth = value;

						if ( this.width > 0 && this.width > this.maxWidth )
						{
							this.Width = this.maxWidth;
						}
						// MD 1/21/09 - Groups in RowLayouts
						// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
						//else if ( null != this.Group )
						//{
						//    // Adjust the group to take into account increased or reduced width of the
						//    // column.
						//    //
						//    this.Group.AdjustAllColumnWidths( true, false, false, false, false, this );
						//}
						else
						{
							UltraGridGroup group = this.GroupResolved;

							if ( null != group )
							{
								// Adjust the group to take into account increased or reduced width of the
								// column.
								//
								group.AdjustAllColumnWidths( true, false, false, false, false, this );
							}
						}

						if ( null != this.Band && null != this.Band.Layout )
						{
							this.Band.Layout.SynchronizedColListsDirty = true;
							this.Band.Layout.DirtyGridElement( );
						}
					}
					// ----------------------------------------------------------------

					this.maxWidth = value;
					this.NotifyPropChange( PropertyIds.MaxWidth, null );
				}
			}
		}
		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMaxWidth() 
		{
			return this.maxWidth != 0;
		}
 
		/// <summary>
		/// Resets MaxWidth to its default value (0, meaning there is no minimum).
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMaxWidth() 
		{
			this.MaxWidth = 0;
		}
        
		/// <summary>
		/// Determines if the cell's data should be displayed in a multi-line format.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property controls the display of multiple lines of text in edit cells in the band or the grid controlled by the specified override. When True, text will wrap in the area of the cell. If the <b>RowSizing</b> property is set to automatically resize the row, the row will expand in height until all lines of text are displayed (or the number of lines specified by the <b>RowSizingAutoMaxLines</b> property is reached).</p>
		/// <p class="body">The <b>CellMultiLine</b> property does not pertain to multi-line editing, only display. Also, you should note that setting a cell to multi-line mode will disable data masking. Only single-line cells can be masked (using the <b>MaskInput</b> and <b>MaskDisplayMode</b> properties.)</p>
		/// <seealso cref="UltraGridOverride.RowSizing"/>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_CellMultiLine")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean CellMultiLine
		{
			get 
			{
				return this.cellMultiLine;
			}
			set
			{
				if(this.cellMultiLine != value)
				{
                    // MRS NAS v8.3 - Unit Testing
                    //if ( !Enum.IsDefined( typeof(DefaultableBoolean), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_88") );
                    if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
                        throw new InvalidEnumArgumentException("CellMultiLine", (int)value, typeof(DefaultableBoolean));

					this.cellMultiLine = value;
					this.NotifyPropChange( PropertyIds.CellMultiLine, null );
				}
			}
		}

		// MRS 1/10/05 - BR01870
		// Excel Export needs to know about this so I made it public and marked it Browsable.Never. 
//#if DEBUG
		/// <summary>
		/// Returns the resolved value, if the band's Override value 
		/// is default it will call the Layout's GetDefaultCellMultiLine 
		/// method.
		/// </summary>
//#endif
		//internal DefaultableBoolean CellMultiLineResolved
		[ EditorBrowsable(EditorBrowsableState.Never), Browsable( false ) ]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
		public DefaultableBoolean CellMultiLineResolved
		{
			get
			{
				if ( this.cellMultiLine == DefaultableBoolean.Default )
					return this.band.CellMultiLineResolved;

				else
					return this.cellMultiLine;
			}
		}


		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeCellMultiLine() 
		{
			return this.cellMultiLine != DefaultableBoolean.Default;
		}
 
		/// <summary>
		/// Resets CellMultiLine to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetCellMultiLine() 
		{
			this.CellMultiLine = DefaultableBoolean.Default;
		}

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// SSP 8/13/04 UWG3605
			// If the column is not connected to a layout then there there's no point
			// in responding to sub object prop change. The reason for doing this is that
			// by prematurely accessing AppearanceHolder.Appearance property when it's
			// appearances collection is not initialized because the header/column isn't
			// connected to a layout yet, we are causing it's RootAppearance to get
			// initialized to a brand new appearance without finding the appropriate
			// linked appearance from the layout's appearance's collection. This happens
			// in initialize component when the transaction is canceled and might happen
			// other times as well.
			// 
			if ( null == this.Layout )
				return;

			if ( propChange.Source == this.Header )
			{
				if ( null != propChange.FindPropId( AppearancePropIds.FontData ) )
				{
					this.band.ClearCachedHeaderFontHeights();

					this.ClearFontCache();
				}
				else
				{
					if ( propChange.PropId is PropertyIds )
					{
						switch ( (PropertyIds)propChange.PropId )
						{
							case PropertyIds.Caption:
							{
								// When the caption changes clear the cached size
								//
								this.ClearFontCache();

								// SSP 7/13/05 BR04974
								// This fix introduced BR04974 bug. I'm taking it out. Syncrhonously
								// causing the metrics to be verified in OnSubObjectPropChanged is not good.
								// 
								

								// MD 7/8/08 - BR34519
								// If the text is rotated when the caption changes, the row layout version should be bumped so the 
								// columns can recalcuate their heights and widths.
								TextOrientationInfo textOrientation = this.Header.TextOrientationResolved;

								if ( textOrientation != null &&
									Object.Equals( textOrientation, TextOrientationInfo.Horizontal ) == false )
								{
									this.Layout.BumpRowLayoutVersion();
								}

								break;
							}
								// SSP 7/21/03 - Fixed headers
								// 
							case PropertyIds.Fixed:
							case PropertyIds.ExclusiveColScrollRegion:
							case PropertyIds.FixedHeaderIndicator:
                            {
                                // If the exclusive col scroling region is changing then
                                // dirty the synched col lists and the col metrics
                                //
                                if (null != this.Band && null != this.Band.Layout)
                                {
                                    this.Band.Layout.SynchronizedColListsDirty = true;
                                    this.band.Layout.GetColScrollRegions(false).DirtyMetrics();
                                }
                                break;
                            }	
                                // CDS NAS v9.1 Header CheckBox
                            case PropertyIds.HeaderCheckBoxVisibility:
                            case PropertyIds.HeaderCheckBoxAlignment:
							{
                                this.ClearFontCache();
								break;			
							}
                            // CDS 02/04/08 - TFS12517 - NAS v9.1 Header CheckBox - When the synchronization changes, we need to invalidate all the cached CheckState values, so bump the version number.
                            case PropertyIds.HeaderCheckBoxSynchronization:
                            {
                                if (this.Band != null)
                                    this.Band.BumpHeaderCheckBoxSynchronizationVersion();
                                this.DirtyHeaderUIElements(null);
                                break;
                            }
						}
					}
				}
				
				this.NotifyPropChange( PropertyIds.Header, propChange );
				
				return;
			}

			// JAS 2005 v2 XSD Support
			//
//			else if ( propChange.Source == this.valuelist)
			else if( propChange.Source == this.ValueList )
			{
				this.NotifyPropChange( PropertyIds.ValueList, propChange );
				return;
			}

			else if ( this.HasCellAppearance &&
				propChange.Source == this.cellAppearanceHolder.RootAppearance )
			{
				
				// SSP 12/3/01 UWG810
				// Bump the cell cache version so that cell elements
				// reprosition their children.
				//
				this.Layout.BumpCellChildElementsCacheVersion( );

				this.band.ClearCachedHeaderFontHeights();

				// JJD 12/17/01
				// Clear the font cache
				//
				this.ClearFontCache();

				this.NotifyPropChange( PropertyIds.CellAppearance, propChange );
				return;
			}
			else if ( this.HasCellButtonAppearance &&
				propChange.Source == this.cellButtonAppearanceHolder.RootAppearance )
			{
				
				// SSP 12/3/01 UWG810
				// Bump the cell cache version so that cell elements
				// reprosition their children.
				//
				this.Layout.BumpCellChildElementsCacheVersion( );

				this.NotifyPropChange( PropertyIds.CellButtonAppearance, propChange );
				return;
			}
			else if ( this.HasMaskLiteralsAppearance &&
				propChange.Source == this.maskLiteralsAppearanceHolder.RootAppearance )
			{
				this.NotifyPropChange( PropertyIds.MaskLiteralsAppearance, propChange );
				return;
			}
				// SSP 1/31/03 - Row Layout Functionality
				// Added the following else-if block for the row layout info.
				//
			else if ( this.HasRowLayoutColumnInfo && propChange.Source == this.RowLayoutColumnInfo )
			{
				// SSP 2/4/03 - Row Layout Functionality
				// Added case for RowLayoutColumnInfo.
				//
				if ( null != this.Band )
					this.Band.DirtyRowLayoutCachedInfo( );

				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.RowLayoutColumnInfo, propChange );
				return;
			}
				// SSP 1/10/05 - Merged Cell Feature
				//
			else if ( this.HasMergedCellAppearance 
				&& propChange.Source == this.mergedCellAppearanceHolder.RootAppearance )
			{
				this.Layout.DirtyDataAreaElement( false, true, true );
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.MergedCellAppearance, propChange );
				return;
			}
				// SSP 1/10/05 - UltraCalc 
				// 
			else if ( this.HasFormulaErrorAppearance
				&& propChange.Source == this.formulaErrorAppearanceHolder.RootAppearance )
			{
				this.Layout.DirtyDataAreaElement( false, true, true );
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FormulaErrorAppearance, propChange );
				return;
			}
				// SSP 5/19/05 BR04157
				// 
			else if ( this.HasFilterCellAppearance 
				&& propChange.Source == this.filterCellAppearanceHolder.RootAppearance )
			{
				this.Layout.DirtyDataAreaElement( false, true, true );
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FilterCellAppearance, propChange );
				return;
			}
				// SSP 5/19/05 BR04157
				// 
			else if ( this.HasFilterOperatorAppearance
				&& propChange.Source == this.filterOperatorAppearanceHolder.RootAppearance )
			{
				this.Layout.DirtyDataAreaElement( false, true, true );
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FilterOperatorAppearance, propChange );
				return;
			}
            // MBS 5/11/09 - NA9.2 GroupByRowConnector Appearance
            else if (this.HasGroupByRowConnectorAppearance &&
                propChange.Source == this.groupByRowConnectorAppearanceHolder.RootAppearance)
            {
                this.Layout.DirtyDataAreaElement(false, true, true);
                this.NotifyPropChange(Infragistics.Win.UltraWinGrid.PropertyIds.GroupByRowConnectorAppearance, propChange);
                return;
            }
            //
            else if (this.HasGroupByRowAppearance &&
                propChange.Source == this.groupByRowAppearanceHolder.RootAppearance)
            {
                this.Layout.DirtyDataAreaElement(false, true, true);
                this.NotifyPropChange(Infragistics.Win.UltraWinGrid.PropertyIds.GroupByRowAppearance, propChange);
                return;
            }

			Debug.Assert( false, "Unknown sub object in Column.OnSubObjectPropChanged" );
		}

		internal void ResolveAppearance( ref AppearanceData appData,
			ref ResolveAppearanceContext context )
		{

			if (context.ObjectType == typeof(Infragistics.Win.UltraWinGrid.HeaderBase) )
			{
				// SSP 3/12/06 - Optimization
				// Don't force allocation of the Appearance object.
				// 
				//if (this.Header != null)
				HeaderBase header = this.Header;
				if ( header != null && header.HasAppearance )
				{
					// SSP 3/8/06 - App Styling
					// Added if condition to the existing code.
					// 
					if ( context.ResolutionOrder.UseControlInfo )
					{
						// we are resolving for the header so merge in its appearance
						//
					
						// JJD 12/12/02 - Optimization
						// Call the Appearance object's MergeData method instead so
						// we don't make unnecessary copies of the data structure
						//AppearanceData.MergeAppearance( ref appData, 
						//	//RobA 10/23/01 we should be using the header's appearance here
						//	this.header.Appearance.Data,
						//	//ref this.cellAppearanceHolder.Appearance.Data,
						//	ref context.UnresolvedProps );
						header.Appearance.MergeData( ref appData, ref context.UnresolvedProps );
					}
				}
			}

			else if ( this.HasCellAppearance )
			{
				// SSP 3/8/06 - App Styling
				// Added if condition to the existing code.
				// 
				if ( context.ResolutionOrder.UseControlInfo )
				{
					// For rows and cells merge in our appearance
					//
					// JJD 12/12/02 - Optimization
					// Call the Appearance object's MergeData method instead so
					// we don't make unnecessary copies of the data structure
					//AppearanceData.MergeAppearance( ref appData, 
					//	this.cellAppearanceHolder.Appearance.Data,
					//	ref context.UnresolvedProps );
					this.cellAppearanceHolder.Appearance.MergeData( ref appData, ref context.UnresolvedProps );
				}
			}

			// if we have all the appearance properties requested we can
			// exit here
			//
			if (context.UnresolvedProps == 0)
			{
				return;
			}

			// We never want to resolve the group appearance properties for the header
			// so moved to group to the bootom (in the else clause)
			//
			// we don't want to go up to the band since its okay to have unresolved 
			// properties at this point. Unless of course if this is a column header
			// resolution. In which case, we do want to get the band's input
			//
			if (context.ObjectType == typeof(Infragistics.Win.UltraWinGrid.HeaderBase))
			{
				this.Band.ResolveAppearance( ref appData, ref context);

				// finally call ResolveColors to make sure we have valid
				// fore, back and borders colors
				//
				this.Band.Layout.ResolveColors( ref appData, ref context, SystemColors.Control, 
					SystemColors.WindowFrame, SystemColors.WindowText, false );
                
				return;
			}

			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
			//if (this.Group != null)
			//{
			//    this.Group.ResolveAppearance( ref appData, ref context );
			UltraGridGroup group = this.GroupResolved;
			if ( group != null )
			{
				group.ResolveAppearance( ref appData, ref context );
				
				return;
			}

			// JJD 12/13/01
			// When we are resolving cell's and the IsCellOnly flag is set
			// we want to call the band's resolve. 
			// This is called to resolve a cell's appearance in card view
			//
			if ( context.ObjectType == typeof(UltraGridCell) &&
				context.UnresolvedProps != 0 && 
				context.IsCellOnly )
			{
				this.Band.ResolveAppearance( ref appData, ref context);
			}
		}


		/// <summary>
		/// Returns the UltraGridLayout object that determines the layout of the object. 
		/// This property is read-only at run-time.
		/// </summary><remarks><para>The <b>Layout</b> property of an object is used to access 
		/// the UltraGridLayout object that determines the settings of various properties 
		/// related to the appearance and behavior of the object. The UltraGridLayout object 
		/// provides a simple way to maintain multiple layouts for the grid and 
		/// apply them as needed. You can also save grid layouts to disk, the 
		/// registry or a storage stream and restore them later.</para><para>The 
		/// UltraGridLayout object has properties such as Appearance and Override, so the 
		/// UltraGridLayout object has sub-objects of these types, and their settings are 
		/// included as part of the layout. However, the information that is 
		/// actually persisted depends on how the settings of these properties were 
		/// assigned. If the properties were set using the UltraGridLayout object's intrinsic 
		/// objects, the property settings will be included as part of the layout. 
		/// However, if a named object was assigned to the property from a 
		/// collection, the layout will only include the reference into the collection, 
		/// not the actual settings of the named object. 
		/// (For an overview of the difference between named and intrinsic objects, 
		/// please see the <see cref="UltraGridLayout.Appearance"/>property.</para></remarks><example><para>
		/// For example, if the UltraGridLayout object's Appearance property is used to 
		/// set values for the intrinsic Appearance object like this:</para><pre>
		/// UltraGrid1.DisplayLayout.Appearance.ForeColor = vbBlue</pre><para>
		/// Then the setting (in this case, ForeColor) will be included as part 
		/// of the layout, and will be saved, loaded and applied along with the 
		/// other layout data. However, suppose you apply the settings of a named 
		/// object to the SSLayout's Appearance property in this manner:</para>
		/// <pre>SSUltraGrid1.Appearances.Add "New1"
		/// SSUltraGrid1.Appearances("New1").ForeColor = vbBlue
		/// SSUltraGrid1.Layout.Appearance = SSUltraGrid1.Appearances("New1")</pre>
		/// <para>In this case, the ForeColor setting will not be persisted as 
		/// part of the layout. Instead, the layout will include a reference to 
		/// the "New1" Appearance object and use whatever setting is present in 
		/// that object when the layout is applied.</para><para>By default, the 
		/// layout includes a copy of the entire SSAppearances collection, so if the 
		/// layout is saved and restored using the default settings, the object 
		/// should always be present in the collection when it is referred to. 
		/// However, it is possible to use the Load and Save methods of the UltraGridLayout object 
		/// in such a way that the collection will not be re-created when the layout 
		/// is applied. If this is the case, and the layout contains a reference to 
		/// a nonexistent object, the default settings for that object's properties 
		/// will be used.</para></example>
		[Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
		public UltraGridLayout Layout
		{
			get
			{
				// JJD 11/27/01
				// Check if band reference is null
				//
				if ( this.band != null )
					return this.band.Layout;

				return null;
			}
		}
 		
		// SSP 5/1/03 - Cell Level Editor
		// Added Editor and ValueList properties off the cell so the user can set the editor
		// and value list on a per cell basis.
		// Added GetStyleResolved method since we resolve style differently based on whether
		// the we have value list or not. Since the cell can have the value list, we have to
		// have the cell (or row) to properly resolve the style. So changed the StyleResolved
		// property into GetStyleResolved method.
		//
		internal ColumnStyle GetStyleResolved( UltraGridRow row )
		{
			// SSP 7/18/03 - Added Style property on the cell
			//
			// ----------------------------------------------------------------
			if ( null != row )
			{
				UltraGridCell cell = row.GetCellIfAllocated( this );

				if ( null != cell )
				{
					if ( ColumnStyle.Default != cell.Style )
						return cell.Style;

					// If the cell's Style is set to Default and if the cell has it's own
					// editor set, then return ColumnStyle resolved of DropDown or Edit
					// depending on whether it has an editor or value list. The reason
					// for doing this is that we want to give the cell's Editor and 
					// EditorControl higher priority than the column's Style property.
					//
                    // MRS - NAS 9.2 - Control Container Editor
					//if ( null != cell.Editor || null != cell.EditorControl )
                    // MRS 8/26/2009 - TFS20362
                    // We get in here when we are getting the Value property of an unbound cell because we want 
                    // the value on unbound CheckBox cells to return False when the value is null. Otherwise, 
                    // unbound checkbox columns always end up in the indeterminate state unless they are 
                    // explicitly set by the developer, which is a hassle. 
                    // But, this GetStyleResolved method is only checking the editor control/component on the cell, 
                    // not the column. So if the user set the editor control or editor on the cell explicitly
                    // to ANY editor, this method will return Edit style and thus the code in the cell's Value
                    // getter will not recognize it as a checkbox cell, even if the editor is a CheckBox. 
                    // Since most people are probably not even setting the Editor on a CheckBox column, and if
                    // they do, they are setting it on the column, not a single cell, it's easiest just to leave
                    // this as it was and the convenient code that translate null into false for unbound columns
                    // won't work for individual cells, only for whole columns. 
                    //
                    //if (null != cell.Editor || null != cell.EditorComponentResolved)                    
                    if (null != cell.Editor || null != cell.EditorComponent)
					{
						// SSP 6/25/04 UWG3157
						// If the cell's editor is a drop down type editor (like the combo editor) and if
						// the column's style is set to DropDownList or DropDownValidate then use that 
						// instead of DropDown.
						//
						// --------------------------------------------------------------------------------
						//return null != cell.ValueList ? ColumnStyle.DropDown : ColumnStyle.Edit;
						bool hasDropDown = null != cell.ValueList || 
							null != cell.EditorResolved && cell.EditorResolved.SupportsDropDown;

						ColumnStyle retVal = ColumnStyle.Edit;
						if ( hasDropDown )
						{
							retVal = ColumnStyle.DropDownList == this.style 
								|| ColumnStyle.DropDownValidate == this.style
								? this.style 
								: ColumnStyle.DropDown;
						}

						return retVal;
						// --------------------------------------------------------------------------------
					}
				}
			}
			// ----------------------------------------------------------------

			//if style is set then return that style
			if ( this.style > 0 )
			{
				// SSP 5/17/05 BR04064 - NAS 5.2 Filter Row
				// Don't use the column's style for the filter row.
				//
				//return this.style; 
				// SSP 10/21/05 BR07013 BR07093
				// 
				//if ( null == row || row.IsDataRow )
				if ( null == row || ! row.IsFilterRow )
					return this.style; 
			}

			//if we have a value list, then style is dropdown
			// SSP 8/13/02 UWG1367
			// Use the ValueList property rather than the valueList variable.
			//				
			//if( this.valuelist != null )
			// SSP 5/1/03 - Cell Level Editor
			IValueList vl = this.GetValueList( row );
			if( vl != null )
			{
				// SSP 2/4/02 UWG1014
				// If the value list's DisplayStyle is set to Picture, then treat
				// the column as a DropDownList rather than the DropDown
				//
				// SSP 8/13/02 UWG1367
				// Use the ValueList property rather than the valueList variable.
				//				
				//Infragistics.Win.ValueList valueList = this.ValueList as Infragistics.Win.ValueList;
				Infragistics.Win.ValueList valueList = vl as Infragistics.Win.ValueList;
				if ( null != valueList &&
					Infragistics.Win.ValueListDisplayStyle.Picture == valueList.DisplayStyle )
				{
					return Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList;
				}

				return Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown;
			}

			// SSP 1/27/05 BR01987
			// Enclosed the existing code into the if block. If the column has a formula
			// then use the text editor by default.
			//
			if ( ! this.HasActiveFormula )
			{
				//check for underlying data if date or bool
				if ( typeof( System.DateTime ) == this.DataType )
					return Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownCalendar;

				if ( typeof( System.Boolean ) == this.DataType )    				
					return Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
			}
	            
			//if no style is set and underlying data in *not* date, bool, or list, then return edit
			return Infragistics.Win.UltraWinGrid.ColumnStyle.Edit;
		}
		

		// SSP 1/16/02 
		// Since we are only looking at the SortDirection, we don't
		// need to have a version. We just verify against the sort direction.
		//
		
		internal void BumpGroupByHierarchyVersion( )
		{
			// SSP 10/9/01
			
			// for reconstructing group by hierarchy at individual
			// group by level.
			this.Band.BumpGroupByHierarchyVersion( );

			this.groupByHierarchyVersion++;

			// SSP 8/12/03 - Optimizations - Grand Verify Version Number
			//
			if ( null != this.Layout )
				this.Layout.BumpGrandVerifyVersion( );
		}

		internal int GroupByHierarchyVersion 
		{
			get
			{
				return this.groupByHierarchyVersion;
			}
		}

		internal int GetDisplayLevel() 
		{			
			if ( this.band.ActualColDisplayLevels > 1) 
				return System.Math.Min( this.Level, this.band.ActualColDisplayLevels - 1 );

			return 0;
		}

		// JJD 1/15/02
		// Use the first item indicator on the header
		//
		internal bool FirstItem
		{
			get
			{
				return this.Header.FirstItem;
			}
			set
			{
				this.Header.FirstItem = value;
			}
		}

		// JJD 1/15/02
		// Use the last item indicator on the header
		//
		internal bool LastItem
		{
			get
			{
				return this.Header.LastItem;
			}
			set
			{
				this.Header.LastItem = value;
			}
		}

		internal bool FirstItemInLevel
		{
			get
			{
				return this.firstItemInLevel;
			}
			set
			{
				this.firstItemInLevel = value;
			}
		}


		internal bool LastItemInLevel
		{
			get
			{
				return this.lastItemInLevel;
			}
			set
			{
				this.lastItemInLevel = value;
			}
		}

		internal int RelativeOrigin
		{
			get
			{
				return this.relativeOrigin;
			}
			set
			{
				this.relativeOrigin = value;
			}
		}

		internal void SetWidthDuringDrag( int nNewWidth )
		{
			this.widthDuringDrag = nNewWidth;
		}
		internal int GetActualWidth ( )
		{
			return this.GetActualWidth( false );
		}
		internal int GetActualWidth ( bool bIgnoreDraggingWidth ) //false
		{
			// if the column is hidden return 0
			//
            // MRS 7/30/2009 - TFS20043
            // Un-did this change. When getting the width, we don't care if the group is hidden. 
            //
            // MRS 2/23/2009 - Found while fixing TFS14354
			//if ( this.Hidden )
            //if (this.HiddenResolved)
            //
            if (this.Hidden)
				return 0;

			// if minimized return the min col width
			//
			if ( this.widthDuringDrag > 0 && !bIgnoreDraggingWidth )
				return this.widthDuringDrag;

			return this.Width;
		}


		internal int OverallWidth
		{
			get
			{
				int nOverallWidth = this.GetActualWidth( false );
	    
				if ( this.FirstItem )
				{
					nOverallWidth +=  this.band.RowSelectorExtent;
				}

				if ( nOverallWidth < 1 )
					return 0;

				// If this column is part of a group then get the groups overall origin
				// and add the column's releative origin
				//
				//if ( m_pGroup && IsLastItem() && m_Band.GetLevelCount() > 1 )
				// SSP 1/14/04
				// What we meant to do here is that the last item in the group level should extend to 
				// the right edge of the group. This situation should only arise when there are more 
				// than 1 levels.
				//
				//if ( this.group != null && this.LastItem && this.band.LevelCount > 1 )
				// MD 1/21/09 - Groups in RowLayouts
				// Use LevelCountResolved because it checks the band's row layout style
				//if ( this.group != null && this.LastItemInLevel && this.band.LevelCount > 1 )
				if ( this.group != null && this.LastItemInLevel && this.band.LevelCountResolved > 1 )
				{
					nOverallWidth = this.group.OverallWidth - this.RelativeOrigin;
				}

				return nOverallWidth; 
			}
		}

		internal void InternalAdjustLevel ( short nLevel )
		{ 
			this.Level = nLevel; 
		}

		internal bool CanInitializeFrom( UltraGridColumn source )
		{
			if ( source == null ) return false;

			// AS - 10/30/01
			// Make sure not trying to compare bound and unbound column.
			//if (source.IsBound != this.IsBound)
			//	return false;

			// AS - 10/30/01
			// If both keys are blank, compare their unbound relative positions
			// AS 1/8/03 - fxcop
			// Do not compare against string.empty - test the length instead
			// SSP 1/14/03
			// I think the first if line was accidently left uncommented so I commented it out.
			//
			//if (!this.IsBound && this.Key == string.Empty && source.Key == string.Empty)
			if (!this.IsBound && (this.Key == null || this.Key.Length == 0) && (source.Key == null || source.Key.Length == 0))
			{
				return source.UnboundRelativePosition == this.UnboundRelativePosition;
			}

			// if the keys match return true
			//
			// AS 1/8/03 - fxcop
			// Must supply an explicit CultureInfo to the String.Compare method.
			//if ( String.Compare(this.Key, source.Key, true) == 0 )
			if ( String.Compare(this.Key, source.Key, true, System.Globalization.CultureInfo.CurrentCulture) == 0 )
				return true;

			return false;
		}

		internal UltraGridColumn Clone ( Infragistics.Win.UltraWinGrid.PosChanged posChanged )
		{
			if ( null != this.cloneData )
				throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_89"));

			UltraGridColumn clone = (UltraGridColumn)this.MemberwiseClone();

			// SSP 8/27/02 UWG1611
			// Null out the embeddable editor of the clone because we don't want both
			// columns to be referencing the same embeddable editor owner info.
			//
			clone.editorOwnerInfo = null;

			// SSP 4/18/05 - NAS 5.2 Filter Row
			// Also null out filter row owners.
			//
			clone.filterOperandEditorOwnerInfo = null;
			clone.filterOperatorEditorOwnerInfo = null;

			// SSP 9/27/01 UWG347
			// we shouldn't assume a header is already created. So
			// use Header property to enforce creation if not already
			// created
			// 
			//clone.header = this.header.Clone();
			//
			clone.header = this.Header.Clone();

			// SSP 12/10/03 UWG2784
			// Make sure the cloned header's Column property points to the cloned column and not
			// this column.
			//
			clone.header.InitColumn( clone );

			clone.cloneData = new ColumnCloneData();

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//clone.CloneData.posChanged = posChanged;

			clone.CloneData.clonedFromCol = this;
			clone.CloneData.width  = this.Width;
			clone.CloneData.visiblePosition = this.Header.VisiblePosition;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//clone.CloneData.group = this.Group;
			//clone.CloneData.level = this.Level;

			// SSP 8/2/05 - NAS 5.3 Column Swapping in Row-Layout
			// 
			if ( null != this.rowLayoutColumnInfo )
				clone.rowLayoutColumnInfo = this.rowLayoutColumnInfo.Clone( );
			    
			return clone;
		}	

		// SSP 5/2/03
		// Commented below method out because it's not being used anywhere.
		//
		

		// SSP 7/18/03 - Cell level Style property
		// Added Style property to the cell object.
		//
		//internal bool IsColumnStyleEditButton		// AS 9/24/01 This method should be internal, not public
		internal bool IsColumnStyleEditButton( UltraGridRow row )
		{
			// SSP 7/18/03 - Cell level Style property
			// Added Style property to the cell object.
			// Commented out the original code and added the new code.
			//
			
			return ColumnStyle.EditButton == this.GetStyleResolved( row );
		}


		// SSP 5/2/03
		// Commented below method out because it's not being used anywhere.
		//
		

		// SSP 7/18/03 - Cell level Style property
		// Added Style property to the cell object.
		//
		//internal bool IsColumnStyleButton			// AS 9/24/01 Changed from public to internal
		internal bool IsColumnStyleButton( UltraGridRow row )
		{	
			// SSP 7/18/03 - Cell level Style property
			// Added Style property to the cell object.
			// Commented out the original code and added the new code.
			//
			

			return ColumnStyle.Button == this.GetStyleResolved( row );
		}




		internal bool IsCellMultiLine			// AS 9/24/01 Changed from public to internal
		{
			get
			{
				//ROBA UWG208 8/21/01 this wasn't implemented
				if ( this.cellMultiLine != DefaultableBoolean.Default )
					return ( this.cellMultiLine == DefaultableBoolean.True );
				else
					return ( this.band.CellMultiLineResolved == DefaultableBoolean.True );				
			}
		}


		

		internal void CalculateHeaderRect ( ref System.Drawing.Rectangle rcHeader, ref System.Drawing.Rectangle prcFixedHeaders )	//NULL
		{
			// It is possible to have a zero header height if headers are marked hidden for this 
			// band and we are in horizontal view style. So assert >= 0 instead of just > 0
			//
			System.Diagnostics.Debug.Assert( this.Band.GroupHeaderHeight + this.Band.GetTotalHeaderHeight() >= 0,
				"Can't calculate a meaningful header height from band metrics" );

			rcHeader.X = 0;
			rcHeader.Y = 0;
			rcHeader.Height =  this.Band.GetColumnHeaderHeight();

			// Use IsLastItemInLevel method instead of IsLastItem 
			//
			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
			//if ( this.Group != null && this.LastItemInLevel )
			UltraGridGroup group = this.GroupResolved;
			if ( group != null && this.LastItemInLevel )
			{
				Rectangle rcGroupHeader = new Rectangle( 0, 0, 0, 0 );

				// MD 1/21/09 - Groups in RowLayouts
				// Use the cached group.
				//this.Group.CalculateHeaderRect( ref rcGroupHeader, ref prcFixedHeaders );
				group.CalculateHeaderRect( ref rcGroupHeader, ref prcFixedHeaders );
				// SSP 10/20/03 UWG2707 UWG2617
				// Commented out the original code and added the new code below. Below
				// code doesn't make any sense. Search for all the changes with above
				// bug numbers to see more info. I don't know why we are offsetting the 
				// last item by PreRowAreaExtent.
				//
				//int AdjustedWidth = this.Group.FirstItem && this.RelativeOrigin >= this.Band.PreRowAreaExtent
				//	? this.RelativeOrigin - this.Band.PreRowAreaExtent : this.RelativeOrigin;
				int AdjustedWidth = this.RelativeOrigin;

				rcHeader.Width = (rcGroupHeader.Width - AdjustedWidth );
			}
			else
			{
				rcHeader.Width  = this.OverallWidth;
			}

			// MD 1/20/09 - Groups in RowLayout
			// More checks were added to ActualColDisplayLevels which should also be added here, so just use the property instead.
			//short nLevelCount =	(short)this.Band.LevelCount;
			short nLevelCount = (short)this.Band.ActualColDisplayLevels;

			// Adjust rect top and botom based on the column's level number if we
			// have multiple levels and groups are displayed
			//
			// JJD 11/28/01
			// Check if groups are displayed (not if the headers are visible)
			// since the column headers could be visible and the group headers not
			//
			//if ( nLevelCount > 1  && this.Band.GroupHeadersVisibleResolved )	//m_Band.AreGroupsDisplayed() )
			// MD 1/20/09 - Groups in RowLayout
			// The check for GroupsDisplayed is no longer needed because now the variable is obtained from the ActualColDisplayLevels, which checks that.
			//if ( nLevelCount > 1 && this.Band.GroupsDisplayed && this.Band.ColHeadersVisibleResolved )
			if ( nLevelCount > 1 && this.Band.ColHeadersVisibleInSeparateAreaResolved )
			{
				// MD 1/21/09 - Groups in RowLayouts
				// Use LevelResolved because it checks the band's row layout style
				//int nLevel = this.Level;
				int nLevel = this.LevelResolved;

				System.Diagnostics.Debug.Assert( nLevel < nLevelCount, "Level >= nLevelCount in CColumn::CalculateHeaderRect" );

				// calc the height for each level (could round down)
				//
				int nHeightPerLevel = ( rcHeader.Bottom / nLevelCount );

				// adjust the top of the rect based on the level number
				//
				rcHeader.Y = (nHeightPerLevel * nLevel);
				rcHeader.Height -= (nHeightPerLevel * nLevel);

				// if this isn't the lowest level then adjust the bottom
				if ( nLevel < nLevelCount - 1 )
					rcHeader.Height = nHeightPerLevel;
			}

			// SSP 10/20/03 UWG2707 UWG2617
			// Although above bugs are not caused by the following line of code, I noticed it 
			// when I was fixing above two bugs. Commented out the code. This code doesn't
			// make sense at all. Why are we offsetting by PreRowAreaExtent. Either we should
			// be offsetting every header or no headers.
			// Commented out below code.
			//
			

			// If the fixed header height is greater than the band's header height offset the 
			// rect downward so that it is displayed butted up against the row
			//
			//RobA 10/26/01 implemented AlignHeaderInFixedHeaderArea
			this.AlignHeaderInFixedHeaderArea( ref rcHeader, ref prcFixedHeaders );
			
		}

		// Shifts the header rect down so that headers in the fixed header
		// area always but the top row even if bands have different header heights
		//
		void AlignHeaderInFixedHeaderArea (	ref System.Drawing.Rectangle headerRect, 
			ref System.Drawing.Rectangle fixedHeaders )
		{
			// If the fixed header height is greater than the band's header height offset 
			// the rect downward so that it is displayed butted up against the row
			//
			int bandHeaderHeight = this.Band.GetTotalHeaderHeight();
			if ( fixedHeaders.Height > bandHeaderHeight )
			{
				headerRect.Offset( 0, fixedHeaders.Height - bandHeaderHeight );
			}
		}

		internal Activation ActivationResolved 
		{
			get
			{
				// SSP 8/24/01 UWG240
				// If the grid is disabled in run mode then all of the columns should appear
				// disabled
				//
				UltraGridBase grid = this.Band.Layout.Grid;
				Infragistics.Win.UltraWinGrid.Activation maxActivation = 
					grid.Enabled || grid.DesignMode
					? ( DefaultableBoolean.True == this.Band.AllowUpdateResolved  )
					? Activation.AllowEdit
					: Activation.ActivateOnly
					: Activation.Disabled;

				// SSP 5/2/05 BR04321
				// After discussing with Joe, the behavior was changed so Disabled takes precedence 
				// over the NoEdit. Use the new MaxActivation method instead.
				// 
				//return (Activation)Math.Max( (int)this.CellActivation, (int)maxActivation );
				return GridUtils.MaxActivation( this.CellActivation, maxActivation );
				
				//return this.Activation;
			}

		}

		internal bool IsWidthFixed( bool byMouse, bool duringGroupResize, UltraGridColumn fixedColumn ) 
		{
			//	Always allow resizing @ design time			
			// AS - 11/5/01
			// We must have a grid reference to see if this is design time or not.
			//
			if ( this.Band.Layout.Grid != null && this.Band.Layout.Grid.DesignMode )
				return false;

            // MRS 2/23/2009 - Found while fixing TFS14354
			//if ( this.Hidden )
            if (this.HiddenResolved)
				return true;

			// when a column is being resized it is treated as if it were fixed
			// so that it won't be adjusted
			//
			if ( this == fixedColumn )
				return true;

			// If the band doesn't allow col sizing then always return true
			// when bByMouse is true
			//
			if ( byMouse &&
				this.Band.AllowColSizingResolved == Infragistics.Win.UltraWinGrid.AllowColSizing.None )
				return true;
			
			// If bByMouse is false, ignore the LockedWidth property.
			//
			if ( byMouse )
				return this.LockedWidth ? true : false ;				
			else
				return false;
		}					  
		

		internal void GetColumnWidthRange( bool byMouse, bool adjustGroupWidthAlso,
			ref int minWidth, ref int maxWidth )
		{
			if ( this.IsWidthFixed( byMouse, false, null ) )
			{
				minWidth = maxWidth = this.GetActualWidth();
			}
			else
			{
				// MD 1/21/09 - Groups in RowLayouts
				// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
				//if ( null != this.Group ) 
				UltraGridGroup group = this.GroupResolved;

				// MD 2/27/09 - TFS14482
				// We only want to ask the group for the width range in groups and levels mode.
				//if ( null != group ) 
				if ( group != null && 
					this.Band != null && 
					this.Band.RowLayoutStyle == RowLayoutStyle.None ) 
				{
					int minWidthL = minWidth;
					int maxWidthL = maxWidth;

					// MD 1/21/09 - Groups in RowLayouts
					// Use the cached group.
					//this.Group.GetColumnWidthRange( byMouse, adjustGroupWidthAlso , this, ref minWidthL, ref maxWidthL );
					group.GetColumnWidthRange( byMouse, adjustGroupWidthAlso, this, ref minWidthL, ref maxWidthL );

					minWidth = minWidthL;
					maxWidth = maxWidthL;
				}			
				else
				{
					minWidth   = this.MinWidthActual; 
					maxWidth   = this.MaxWidthActual; 
				}	
			}
		}

		internal void SynchColsOnWidthChange( int deltaWidth, bool includeSameBandColumns )
		{
			// If the width is smaller we need to try to adjust the current widths
			// of any synched columns (down to their minimum widths
			//
			if ( 0 != deltaWidth )
			{
				// Get the head of the synchronized columns linked list
				//
				UltraGridColumn firstColInSyncList = this.GetSynchronizedCol( NavigateType.First );

				Debug.Assert( null != firstColInSyncList, "GetSynchronizedCol returned null in Column.SynchColsOnWidthChange");

				UltraGridColumn column = firstColInSyncList;

				// walk the linked list until the end adjusting each width value
				//
				while (  null != column  )
				{
					if ( this != column )
					{
						// if we aren't including same band columns then make
						// sure the linked column is from another band
						//
						if ( includeSameBandColumns || this.Band != column.Band )
						{
							// SSP 12/15/04 BR00870 BR00016
							// Don't use the width member variable because it is not necessarily the actual 
							// width of the column as is evident if you look in the get of the column's Width
							// property. Instead use GetActualWidth method.
							// 
							// ----------------------------------------------------------------------------
							//column.width += deltaWidth;
							column.width = column.GetActualWidth( ) + deltaWidth;
							// ----------------------------------------------------------------------------

							column.width = System.Math.Max( column.width, column.MinWidthActual );

							column.hasWidthSet = true;

							if ( 0 != column.synchronizedWidth )
							{
								column.synchronizedWidth = column.width;
							}
						}
					}

					// get the next column in the linked list
					//
					column = column.GetSynchronizedCol( NavigateType.Next );
				}
			}
		}
		

		internal int Origin
		{
			get
			{
				int c = 0;
				if ( this.Header.FirstItem )
					c = this.Header.Band.RowSelectorExtent;
				return this.header.Origin - c;
			}
		}
		internal void Synchronize()
		{
            // MRS 2/23/2009 - Found while fixing TFS14354
			//if ( this.isSynchronized || this.Hidden )
            if (this.isSynchronized || this.HiddenResolved)
				return;

			// Adjust this column's origin based on previous sync col adjustments
			// 
			this.AdjustOriginDuringColSynch();

			// if their are no columns to be synchronized with this column
			// then return
			//
			if ( null == this.NextLinkedListItem &&
				null == this.PrevLinkedListItem )
			{
		
				// Even though we are not adjusting this column we still need 
				// to call AdjustWidthDuringColSynch with zero so that the
				// group totals can be maintained
				//
		
				// We are postponing col syncing within groups since it adds alot
				// of conplexity and we weren't able to get it to work 
				//
				return;
			}

			int rightMostCurr  = 0;
			int rightMostMin   = 0;
			int leftMostMax    = 0;

			// call GetSynchedColsRightCoordRange to find out where
			// we need to align the columns
			//
			this.GetSynchedColsRightCoordRange( ref rightMostCurr, ref rightMostMin, ref leftMostMax );

			// Get the head of the synchronized columns linked list
			//
			UltraGridColumn firstColInSyncList = this.GetSynchronizedCol( Infragistics.Win.UltraWinGrid.NavigateType.First );

			Debug.Assert( null != firstColInSyncList, "GetSynchronizedCol returned null in ColumnHeader.Synchronize()");

			// set the new right to the rightmost value of the columns based on
			// their current widths
			//
			int syncRight = rightMostCurr;

			// make sure the new right values falls between every row's
			// min and max values.
			//
			// Note: we apply the minimum width adjustment after
			//       the max width adjustment so that the minimum
			//       settings take precedence.
			//
			if ( leftMostMax > 0 )
				syncRight = System.Math.Min ( syncRight, leftMostMax );

			syncRight = System.Math.Max ( syncRight, rightMostMin );

			UltraGridColumn column = firstColInSyncList;

			// walk the linked list until the end and adjust the widths so
			// that all columns in the linked list align their right edges
			//
			while ( null != column )
			{
				// SSP 8/5/04 UWG3453
				// Since we are using the header's origin also use header's extent.
				// If the header happens to be the first header that's extended left
				// to occupy space over the row selectors then it's origin will be off
				// by the row selector width. This requires us to use the header's
				// extent instead of column's extent in order to properly compensate
				// for this.
				//
				//int currRight  = column.Header.OverallOrigin + column.Extent;
				int currRight  = column.Header.OverallOrigin + column.Header.Extent;
 
				column.AdjustWidthDuringColSynch( syncRight - currRight );				

				// get the next column in the linked list
				//
				column = column.GetSynchronizedCol( Infragistics.Win.UltraWinGrid.NavigateType.Next );
			}
		}
		internal void AdjustOriginDuringColSynch()
		{
			// set the synchronized flag to true so we don't do this more
			// than once for a column
			// 		
			this.isSynchronized = true;

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//HeadersCollection headers = this.Band.OrderedHeaders;

			ColumnHeader thisHeader = this.Header;

            // MRS 3/25/2008 - BR31396
            UltraGridBand band = this.Band;
            if (band != null &&
                band.AllowColSizingResolved == AllowColSizing.Synchronized)
            {
                UltraGridLayout layout = band.Layout;
                if (layout != null)
                {
                    if (layout.HasColSpan)
                    {
                        // MRS 3/25/2008 - BR31396
                        // Sandip commented this block of code out as an optimization. 
                        // I put this code back in and enclosed it in some conditionals. This code is very 
                        // inefficient, but it needs to be called when there are ColSpans in the grid. 
                        //
                        ////~ SSP 4/29/06 - Optimizations - 4000 cols
                        ////~ Commented out the following code.
                        ////~    
                        HeadersCollection headers = band.OrderedHeaders;
                        if (null != headers)
                        {
							// MD 1/21/09 - Groups in RowLayouts
							// Cache the resolved group to be used below.
							// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
							UltraGridGroup group = this.GroupResolved;

                            // iterate over the ordered headers synchronizing any columns that
                            // haven't already been synchronized that are before this column
                            //				
                            for (int i = 0, count = headers.Count; i < count; i++)
                            {
                                HeaderBase header = headers[i];

                                // Skip over hidden position items
                                //
                                if (header.Hidden)
                                    continue;

                                // when we reach this column (or its group) stop
                                //
                                if (header.IsGroup)
                                {
									// MD 1/21/09 - Groups in RowLayouts
									// Use the cahed group from above.
                                    //if (header == this.Group.Header)
									if ( header == group.Header )
                                        break;
                                }
                                else
                                {
                                    if (header == thisHeader)
                                        break;
                                }

                                header.Synchronize();
                            }
                        }
                    }
                }
            }

			// If this column is part of a group then get the groups overall origin
			// and add the column's releative origin
			//
			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
			//if ( null != this.Group )
			if ( null != this.GroupResolved )
			{
				// We are postponing col syncing within groups since it adds alot
				// of conplexity and we weren't able to get it to work 
				//
				
			}
			else
			{
				if ( null == thisHeader.ExclusiveColScrollRegion )
					thisHeader.SetOrigin(this.relativeOrigin + this.Band.OriginAdjustmentDuringColSynch);
			}
		}

		internal void GetResizeRange( ref int minWidth, ref int maxWidth)
		{
			minWidth = this.MinWidthActual;
			maxWidth = this.MaxWidthActual;

			// see if we are synched up with any other columns
			//
			if ( null == this.NextLinkedListItem &&
				null == this.PrevLinkedListItem )
				return;			

			int rightMostCurr  = 0;
			int rightMostMin   = 0;
			int leftMostMax    = 0;

			// call GetSynchedColsRightCoordRange to find out where
			// we need to align the columns
			//
			this.GetSynchedColsRightCoordRange( ref rightMostCurr, ref rightMostMin, ref leftMostMax );

			// SSP 12/11/01 UWG820
			// Use the header's OverallWidth and OverallOrigin to calculate the currWidth
			// and currRight
			//
			//int currWidth = this.Extent;
			//int currRight = this.Origin + this.Extent;
			int currWidth = this.Header.OverallExtent;
			int currRight = this.Header.OverallOrigin + currWidth;


			// If this is the first item adjust the width to account for
			// the pre row selector area
			//
			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
			//if ( this.FirstItem && null == this.Group  )
			if ( this.FirstItem && null == this.GroupResolved )
				currWidth -= this.Band.PreRowAreaExtent; 

			minWidth = currWidth - ( currRight - rightMostMin );

			if ( leftMostMax > 0 )
			{
				maxWidth = currWidth + leftMostMax - currRight;

				maxWidth = System.Math.Max( currWidth, maxWidth );
			}
		}
		

		void GetSynchedColsRightCoordRange( 
			ref int rightMostCurr, 
			ref int rightMostMin, 
			ref int leftMostMax ) 
		{
			// Get the head of the synchronized columns linked list
			//
			UltraGridColumn firstColInSyncList = this.GetSynchronizedCol( Infragistics.Win.UltraWinGrid.NavigateType.First );

			Debug.Assert( null != firstColInSyncList, "GetSynchronizedCol returned null in Column.GetSynchedColsRightCoordRange");

			int syncPosition = this.Band.GetSyncPosition( this );

			Debug.Assert( syncPosition >= 0, "Sync Position invalid");

			bool isMaxWidthSet;

			int clientOrigin = this.Header.OverallOrigin;

			// SSP 8/5/04 UWG3453
			// We need to work with the column's origin becase below we are using
			// column's Extent. Therefore adjust the clientOrigin to be the 
			// column's origin rather than the header's origin.
			//
			// --------------------------------------------------------------------
			int rowSelectorDelta = this.Header.RowSelectorExtent;
			clientOrigin += rowSelectorDelta;
			// --------------------------------------------------------------------
    
			rightMostCurr    = clientOrigin + this.Extent;

			//	Adding "and not design time" to this conditional
			if ( this.LockedWidth && !this.Layout.Grid.DesignMode)
			{
				rightMostMin   = rightMostCurr;
				leftMostMax    = rightMostCurr;
				isMaxWidthSet  = true;
			}
			else
			{
				rightMostMin   = clientOrigin + this.MinWidthActual;
				leftMostMax    = clientOrigin + this.MaxWidthActual;
				isMaxWidthSet  = 0 != this.MaxWidthActual;
			}

			UltraGridColumn column = firstColInSyncList;

			// SSP 8/15/05 BR04633 BR04666
			// 
			bool autoFitColumns = null != this.Layout && this.Layout.AutoFitAllColumns;
			UltraGridBand bandWithMostSyncPositions = this.Band;
			int bandWithMostSyncPositions_currRight = rightMostCurr;

			// walk the linked list until the end and keep track of the rightmost
			// values for the min, max and current settings
			//
			while ( null != column )
			{
				if ( this != column )
				{
					// first adjust the origin to allow for any previous column
					// adjustments
					//
					column.AdjustOriginDuringColSynch();
					
					clientOrigin = column.Header.OverallOrigin;

					// SSP 8/5/04 UWG3453
					// We need to work with the column's origin becase below we are using
					// column's Extent. Therefore adjust the clientOrigin to be the 
					// column's origin rather than the header's origin.
					//
					// --------------------------------------------------------------------
					clientOrigin += column.Header.RowSelectorExtent;
					// --------------------------------------------------------------------

					int minRight   = clientOrigin + column.MinWidthActual;
					int currRight  = clientOrigin + column.Extent;

					// Check IsLockedWidth off pColumn instead of this column
					//
					if ( column.LockedWidth )
					{
						minRight    = currRight;
						if ( isMaxWidthSet )
							leftMostMax = System.Math.Min( leftMostMax, currRight );
						else
						{
							leftMostMax   = currRight;
							isMaxWidthSet = true;
						}
					}
					else
						if ( 0 != column.MaxWidthActual )
					{
						int maxRight   = clientOrigin + column.MaxWidthActual;

						if ( isMaxWidthSet )
							leftMostMax    = System.Math.Min( leftMostMax, maxRight );
						else
						{
							leftMostMax = maxRight;
							isMaxWidthSet = true;
						}
					}

					rightMostMin   = System.Math.Max( rightMostMin, minRight );
					rightMostCurr  = System.Math.Max( rightMostCurr, currRight );

					// SSP 8/15/05 BR04633 BR04666
					// 
					if ( autoFitColumns
						&& column.Band.GetTotalColSyncPositions( null ) > bandWithMostSyncPositions.GetTotalColSyncPositions( null ) )
					{
						bandWithMostSyncPositions = column.Band;
						bandWithMostSyncPositions_currRight = currRight;
					}
				}

				// get the next column in the linked list
				//
				column  = column.GetSynchronizedCol( Infragistics.Win.UltraWinGrid.NavigateType.Next );
			}

			if ( !isMaxWidthSet )
				leftMostMax = 0;			

			// SSP 8/15/05 BR04633 BR04666
			// 
			if ( autoFitColumns )
				rightMostCurr = bandWithMostSyncPositions_currRight;
		}	


		
		
		// **** Looks like this is not used for columns

		//		internal void InsertBefore( Column existing ) 
		//		{
		//			// first remove us from any other linked list we're in
		//			//
		//			this.RemoveFromLinkedList();
		//
		//			// set our next linked list forward ptr to point to the Existing
		//			// object
		//			//			
		//			this.nextLinkedColumn = existing;
		//
		//			// set our backward ptr to the Existing object's back ptr
		//			//
		//			this.prevLinkedColumn = existing.prevLinkedColumn;
		//
		//			// Set the existing object's back ptr to us
		//			//
		//			existing.prevLinkedColumn = this;
		//
		//			// set the prev object's (if there is one) forward ptr to us
		//			//
		//			if ( null != this.prevLinkedColumn )
		//				this.prevLinkedColumn.nextLinkedColumn = this;
		//		}

		internal void InsertAfterLinkedList( UltraGridColumn existing )
		{
			// first remove us from any other linked list we're in
			//
			this.RemoveFromLinkedList();

			// set our next linked list back ptr to point to the Existing
			// object
			//
			this.prevLinkedColumn = existing;

			// set our forward ptr to the Existing object's forward ptr
			//
			this.nextLinkedColumn = existing.nextLinkedColumn;

			// Set the existing object's forward ptr to us
			//
			existing.nextLinkedColumn = this;

			// set the next object's (if there is one) backward ptr to us
			//
			if ( null != this.nextLinkedColumn )
				this.nextLinkedColumn.prevLinkedColumn = this;
		}
		internal void RemoveFromSynchronizedColList()
		{
			this.RemoveFromLinkedList();
			this.synchronizedWidth = 0;
		}		

		/// <summary>
		/// used for keeping track of synced list
		/// removes this from the ColumnHeader liked lists
		/// </summary>
		void RemoveFromLinkedList() 
		{
			// remove us from the double link list while preserving
			// the rest of the forward and back chains
			//
			if ( null != this.nextLinkedColumn )
				this.nextLinkedColumn.prevLinkedColumn = this.prevLinkedColumn;
    
			if ( null != this.prevLinkedColumn )
				this.prevLinkedColumn.nextLinkedColumn = this.nextLinkedColumn;

			this.nextLinkedColumn = null;
			this.prevLinkedColumn = null;
		}

		internal UltraGridColumn NextLinkedListItem
		{
			get
			{
				return this.nextLinkedColumn;
			}
		}
		internal UltraGridColumn PrevLinkedListItem
		{
			get
			{
				return this.prevLinkedColumn;
			}
		}		


		internal void AdjustWidthDuringColSynch( int adjustment )
		{
			//Debug.Assert(0 == this.synchronizedWidth, "this.synchronizedWidth already set in Column.AdjustWidthDuringColSynch");

			if ( 0 != adjustment )
			{
				this.synchronizedWidth = (int)this.GetActualWidth() + adjustment;
			}

			// Maintain the approriate adjustment totals 
			//
			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
			//if ( null != this.Group )
			if ( null != this.GroupResolved )
			{
				// We are postponing col syncing within groups since it adds alot
				// of conplexity and we weren't able to get it to work 
				//
				//m_pGroup->AdjustWidthDuringColSynch( *this, nAdjustment );
			}
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//else
			//    if ( null != this.Header.ExclusiveColScrollRegion )
			//    this.Header.ExclusiveColScrollRegion.AdjustWidthDuringColSynch( this.Header, adjustment );
			//else
			//    this.Band.AdjustWidthDuringColSynch( adjustment );
			else
			{
				ColScrollRegion exclusiveColScrollRegion = this.Header.ExclusiveColScrollRegion;

				if ( null != exclusiveColScrollRegion )
					exclusiveColScrollRegion.AdjustWidthDuringColSynch( this.Header, adjustment );
				else
					this.Band.AdjustWidthDuringColSynch( adjustment );
			}
		}

		internal int MaxWidthActual
		{
			get
			{
				return this.MaxWidth;
			}
		}
		internal int MinWidthActual
		{
			get
			{
				// SSP 4/7/03 UWG2092
				// There is no reason for us to restrict the min width to a minimum of 10.
				// So if the user sets the min width to something less than 10, honor it.
				//
				// MD 8/15/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.MinWidth > 0 )
				//    return this.MinWidth;
				int cachedMinWidth = this.MinWidth;

				if ( cachedMinWidth > 0 )
					return cachedMinWidth;

				int minWidth;
				// Minimum width should be based on column style
				// Casting is needed due to routine being const				
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//switch ( this.StyleResolved )
				switch ( this.GetStyleResolved( null ) )
				{
					case Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton:
					case Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox:
					case Infragistics.Win.UltraWinGrid.ColumnStyle.TriStateCheckBox:
					case Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown:
					case Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList:
					case Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate:
					case Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownCalendar:
						minWidth = SystemInformation.VerticalScrollBarWidth; 
						break;
					default:
						minWidth = Infragistics.Win.UltraWinGrid.UltraGridColumn.MinColWidthSetting;
						break;
				}

				// SSP 3/03/03 - Row Layout Functionality
				// Take into account the cell spacing as well. 
				//
				minWidth += 2 * Math.Max( 0, this.Band.CellSpacingResolved );

				// MD 8/15/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//return System.Math.Max( this.MinWidth, minWidth); 
				return System.Math.Max( cachedMinWidth, minWidth ); 
			}
		}

		// JJD 1/21/02 - UWG526 - Added
		/// <summary>
		/// Get the next/previous/first or last column in this band based on each column header's <see cref="Infragistics.Win.UltraWinGrid.ColumnHeader.VisiblePosition"/>.
		/// </summary>
		/// <param name="relation">The relation the requested column has to this colum.</param>
		/// <returns>The related column.</returns>
		public UltraGridColumn GetRelatedVisibleColumn( VisibleRelation relation )
		{
			if ( this.band == null )
				return null;
			
			return this.band.GetRelatedVisibleCol( this, UltraGridBand.MapToNavigateType( relation ), true );			
		}

		internal UltraGridColumn GetSynchronizedCol( )
		{
			return this.GetSynchronizedCol( Infragistics.Win.UltraWinGrid.NavigateType.Next );
		}
		internal UltraGridColumn GetSynchronizedCol( Infragistics.Win.UltraWinGrid.NavigateType navType  )
		{			
			// SSP 5/7/03 - Export Functionality
			//
			//if ( !this.Layout.IsDisplayLayout && !this.Layout.IsPrintLayout )
			if ( !this.Layout.IsDisplayLayout && !this.Layout.IsPrintLayout && !this.Layout.IsExportLayout )
				return null;

			this.Layout.ValidateSynchronizedColLists();

			UltraGridColumn synchedCol = null;

			switch ( navType )
			{
				case Infragistics.Win.UltraWinGrid.NavigateType.First:
				case Infragistics.Win.UltraWinGrid.NavigateType.Topmost:
        
					// get the prev column in the list
					//
					synchedCol = this.PrevLinkedListItem;

					// If there is one then call the method on it (which
					// will in effect walk the list to the beginning).
					// Otherwise, return this column.
					//
					if ( null != synchedCol )
						synchedCol = synchedCol.GetSynchronizedCol( navType );
					else
						synchedCol = this;
					break;

				case Infragistics.Win.UltraWinGrid.NavigateType.Last:
				case Infragistics.Win.UltraWinGrid.NavigateType.Bottommost:
        
					// get the next column in the list
					//
					synchedCol = this.NextLinkedListItem;

					// If there is one then call the method on it (which
					// will in effect walk the list to the end)
					// Otherwise, return this column.
					//
					if ( null != synchedCol )
						synchedCol = synchedCol.GetSynchronizedCol( navType );
					else
						synchedCol = this;
					break;

				case Infragistics.Win.UltraWinGrid.NavigateType.Next:
				case Infragistics.Win.UltraWinGrid.NavigateType.Below:
				
					// get the next column in the list
					//
					synchedCol = this.NextLinkedListItem;
					break;

				case Infragistics.Win.UltraWinGrid.NavigateType.Prev:
				case Infragistics.Win.UltraWinGrid.NavigateType.Above:
        
					// get the prev column in the list
					//
					synchedCol = this.PrevLinkedListItem;
					break;

				default:
					Debug.Fail("Invalid nav type in ColumnHeader.GetSynchronizedCol");
					break;
			}

			return synchedCol;
		}

		internal void SetWidth ( 
			int newWidth, 
			bool byMouse,
			bool fireEvents, 
			bool adjustAllColsInGroup, 
			bool adjustGroupWidthAlso,
			bool syncOtherColumns )
		{
			this.SetWidth( 
				newWidth, 
				byMouse, 
				fireEvents, 
				adjustAllColsInGroup, 
				adjustGroupWidthAlso, 
				syncOtherColumns, 
				false );
		}

		// SSP 12/10/02 UWG1768
		// Added an overload to above SetWidth method.
		//
		internal void SetWidth ( 
			int newWidth, 
			bool byMouse,
			bool fireEvents, 
			bool adjustAllColsInGroup, 
			bool adjustGroupWidthAlso,
			bool syncOtherColumns,
			bool enforceColumnMinWidthMaxWidth )
		{
			// SSP 5/16/07 BR22393
			// Added an overload that takes in autoFit parameter.
			// 
			this.SetWidth(
				newWidth,
				byMouse,
				fireEvents,
				adjustAllColsInGroup,
				adjustGroupWidthAlso,
				syncOtherColumns,
				enforceColumnMinWidthMaxWidth,
				false );
		}

		// SSP 5/16/07 BR22393
		// Added an overload that takes in autoFit parameter.
		// 
		internal void SetWidth(
			int newWidth,
			bool byMouse,
			bool fireEvents,
			bool adjustAllColsInGroup,
			bool adjustGroupWidthAlso,
			bool syncOtherColumns,
			bool enforceColumnMinWidthMaxWidth,
			bool autoFit )
		{
			int minWidth = 0;
			int maxWidth = 0;

			// SSP 12/10/02 UWG1768
			// Added enforceColumnMinWidthMaxWidth parameter added below if block. Put the already existing
			// code in the else block.
			//
			if ( enforceColumnMinWidthMaxWidth )
			{
				minWidth = this.MinWidthActual;
				maxWidth = this.MaxWidthActual;
			}
			else
			{
				// get the valid min/max range for this column in this context
				//
				this.GetColumnWidthRange( byMouse, 
					adjustGroupWidthAlso,
					ref minWidth, 
					ref maxWidth );
			}

			// adjust the width to fit within the min/max range
			//
			newWidth = System.Math.Max( newWidth, minWidth );

			if ( maxWidth > minWidth  )
				newWidth = System.Math.Min( newWidth, maxWidth );

			// if the width hasn't changed then just return
			//
			if ( newWidth == this.Width )
				return;

			int origWidth = (int)this.GetActualWidth();

			UltraGridBase grid = this.Band.Layout.Grid;

			if ( fireEvents && null != grid )
			{
				ColumnHeader[] selCols = new ColumnHeader[1];
				UltraGridColumn clone = null;

				if (null == selCols)
					throw new OutOfMemoryException(Shared.SR.GetString("LE_Exception_90"));

				// make a cloned copy of this column
				//
				clone = this.Clone( Infragistics.Win.UltraWinGrid.PosChanged.Sized );
			
				if ( null == clone )
				{
					throw new OutOfMemoryException(Shared.SR.GetString("LE_Exception_91"));
				}

				if ( null == clone.CloneData )
				{
					throw new OutOfMemoryException(Shared.SR.GetString("LE_Exception_92"));
				}

				// set the cloned copies width to the new width
				//				
				clone.CloneData.width = newWidth;

                // MBS 6/9/08 - BR33607
                // Set the actual width of the column as well so that the user will be able to check
                // the new width of the column in the BeforeColPosChanged event.
                clone.width = newWidth;
			

				// insert it into the selected cols collection passing in
				// false as the third param so that the clone doesn't get 
				// addrefed in the insert
				//
				selCols[0] = clone.Header;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//// set a flag so that we know we are in the event
				////
				////RobA 10/23/01 implemented inBeforePosChanged flag
				//this.Header.SetInBeforePosChanged(true);
				        
				//fire the BeforeColPosChanged event
				//
				bool cancel;
				BeforeColPosChangedEventArgs e = new BeforeColPosChangedEventArgs( PosChanged.Sized, selCols );
				
				// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
				//grid.FireBeforeColPosChanged( e );
				grid.FireCommonEvent(CommonEventIds.BeforeColPosChanged, e);
				
				cancel = e.Cancel;
				
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//// reset the flag
				////
				////RobA 10/23/01 implemented inBeforePosChanged flag
				//this.Header.SetInBeforePosChanged(false);
				
				// if it was canceled clean up and exit
				//
				if (cancel)
				{
					return;
				}

				// update the columns state with the new width
				//				
				this.width =  clone.CloneData.width;

				// set a flag so we know that the width has been set explicitly
				//
				this.hasWidthSet = true;

				// If the width is smaller we need to try to adjust the current widths
				// of any synched columns (down to their minimum widths
				//
				if ( syncOtherColumns )
				{
					if ( null != this.NextLinkedListItem ||
						null != this.PrevLinkedListItem )
					{
						this.SynchColsOnWidthChange( this.width - origWidth, true );
						

						if ( 0 != this.synchronizedWidth )
						{
							this.synchronizedWidth = this.width;
						}
					}
					else
					{
						// MD 1/21/09 - Groups in RowLayouts
						// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
						//if ( null != this.Group && adjustAllColsInGroup )
						//    this.Group.AdjustAllColumnWidths( true, false, false, false, false, this );
						UltraGridGroup group = this.GroupResolved;
						if ( null != group && adjustAllColsInGroup )
							group.AdjustAllColumnWidths( true, false, false, false, false, this );
					}

				}

				// Moved notification before firing after event so that the uielements will
				// be dirtied and will return the new values if asked for in the event
				//
				// notify any interested parties
				//
				this.NotifyPropChange( PropertyIds.Width );

				// Clear the collection of cloned columns and add 
				// this column into the collection so that the after event 
				// gets the actual column passed in instead of the clone
				//				
				selCols[0] = this.Header;

				// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
//				// fire the after event and clean up
//				//
//				grid.FireAfterColPosChanged( new AfterColPosChangedEventArgs( PosChanged.Sized, selCols ) );
				grid.FireCommonEvent(CommonEventIds.AfterColPosChanged, new AfterColPosChangedEventArgs( PosChanged.Sized, selCols ));
				return;
			}

			// we aren't firing events so just set the newvalue in the state
			// structure 
			//
			this.width = newWidth;

			// set a flag so we know that the width has been set explicitly
			//
			this.hasWidthSet = true;

			if ( 0 != this.synchronizedWidth )
			{
				this.synchronizedWidth = this.width;
			}

			// If the width is smaller we need to try to adjust the current widths
			// of any synched columns (down to their minimum widths
			//
			if ( syncOtherColumns && 
				( null != this.NextLinkedListItem ||
				null != this.PrevLinkedListItem ) )
			{
				this.SynchColsOnWidthChange( this.width - origWidth, false );
			}
			else
			{
				// MD 1/21/09 - Groups in RowLayouts
				// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
				//if ( null != this.Group && adjustAllColsInGroup )
				//    this.Group.AdjustAllColumnWidths( true, false, false, false, false, this );
				UltraGridGroup group = this.GroupResolved;
				if ( null != group && adjustAllColsInGroup )
					group.AdjustAllColumnWidths( true, false, false, false, false, this );
			}

			// notify any interested parties
			// 
			// SSP 5/16/07 BR22393
			// When auto-fitting operation is begin performed at design-time, do not
			// raise any designer notifications. When the grid loads, it may perform
			// auto-fit operation and cause designer change notifications causing the
			// form to be dirtied. This should not happen since no modifications have
			// been made to the grid - it's simply performing an auto-fit operation.
			// 
			//this.NotifyPropChange( PropertyIds.Width );
			if ( !autoFit || null == grid || !grid.DesignMode )
				this.NotifyPropChange( PropertyIds.Width );

			return;
		}

		[ Browsable(false)]
		internal bool HasWidthSet
		{
			get{ return hasWidthSet; }
			set{ hasWidthSet = value; }
		}

		internal PositionInfo PositionInfo
		{			
			get
			{
				PositionInfo posInfo = new PositionInfo();
				posInfo.Level = (short)this.Level;
				posInfo.Group = this.Group;

				posInfo.VisiblePos = (short)this.InternalGetVisiblePosition();
				return posInfo;
			}			
		}
	

		internal bool SetPositionInfo( PositionInfo posInfo)
		{
			if ( null == this.CloneData )
				return false;

			this.CloneData.visiblePosition = posInfo.VisiblePos;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			// Set the Level Property of the CloneData. The column
			// might be swapped with a column on a different level.
			//this.CloneData.level = posInfo.Level;
			//
			// Set the Group property of the CloneData. The column
			// might be swapped with a column in a different group.
			//this.cloneData.group = posInfo.Group;    

			return true;
		}	

		internal void SetGroup( UltraGridGroup group, int visiblePosition, int level )
		{
			bool levelSpecified = false;

			// make sure the new group is from the same band as the column
			//		
			
			if ( null != group )
			{
				if ( group.Band != this.Band )					
					throw new Exception(Shared.SR.GetString("LE_Exception_93") );
			}            

			// if a level was specified make sure that it is in range
			//
			if ( level < 0 || level >= this.Band.LevelCount )
			{
				//If level wasn't specified, simply set it 
				if ( levelSpecified )
					throw new Exception(Shared.SR.GetString("LE_Exception_94"));					
				else
					level = this.Band.LevelCount - 1;
			}

			// if the group hasn't changed then just return
			//
			if ( this.Group == group )
				return;

			// get the columns collection from the group
			//
			GroupColumnsCollection groupColsColl = 
				null != group ? group.Columns : this.Group.Columns;

			if ( null != group )
			{		
				// MD 2/25/09 - TFS14495
				// This has been moved below.
				// The reference to the group needs to be set before we add the collumn to the group or the 
				// column's HiddenResolved value will return True and the widht of the group will be incorrect.
				//// add this column to the new group's columns collection
				////				
				//groupColsColl.InternalAdd( this, visiblePosition, level );

				// remove this column from its old group (if exists)
				//				
				// Check to make sure the group is still valid before
				// calling remove
				//
				if ( null != this.group )
					//this.group.GetGroupColsColl( true ).InternalRemove( this );
					this.group.Columns.InternalRemove( this );

				// update our group ptr meber variable
				//
				this.group = group;

				// MD 2/25/09 - TFS14495
				// This has been moved from above.
				// The reference to the group needs to be set before we add the collumn to the group or the 
				// column's HiddenResolved value will return True and the width of the group will be incorrect.
				//
				// add this column to the new group's columns collection			
				groupColsColl.InternalAdd( this, visiblePosition, level );
			}
			else
			{
				// remove us from the current group
				//
				groupColsColl.InternalRemove( this );

				this.level = 0;

				// NULL out our group ptr meber variable
				//
				this.group = null;
			}

			// update the level count (since that might have been passed in)
			//
			this.level = level;

            // MRS 3/25/2008 - BR31396
            this.DirtyHasColSpan();

			// notify any interested parties
			//
			this.NotifyPropChange( PropertyIds.Group );
		}

		internal void InitSortIndicator(Infragistics.Win.UltraWinGrid.SortIndicator sortIndicator)
		{
			this.sortIndicator = sortIndicator;
		}


		internal void InitBand( Infragistics.Win.UltraWinGrid.UltraGridBand band, bool calledFromEndInit )
		{
			this.band = band;

			if ( calledFromEndInit )
			{
				// JJD 11/21/01
				// Get the group reference from its internal id
				//
				if ( this.band != null &&
					this.groupId != 0 )
					this.group = this.band.Groups.GetGroupFromInternalID( this.groupId );                                    
			}
			else
			{
				// JJD 11/21/01
				// Only need to hook everything up if we are not called from inside EndInit
				//

				// JJD 10/24/01
				// Init the cellappearance with the appearances collection
				// if necessary
				//
				if ( this.band != null &&
					this.cellAppearanceHolder != null )
				{
					this.cellAppearanceHolder.Collection = this.band.Layout.Appearances;
				}

				//RobA 9/14/01 UWG298
				if ( this.header != null )
				{
					// JJD 10/23/01
					// Init the column (instead of just the band)
					//
					this.header.InitColumn( this );
				}
			}

			// SSP 1/31/03 - Row Layout Functionality
			// Hook up the back ref on the row layout info object.
			//
			if ( null != this.rowLayoutColumnInfo )
				this.rowLayoutColumnInfo.InitColumn( this );
		}

		internal void InternalSetVisiblePosition( int newVisPos )
		{
			bool tmp;
			this.InternalSetVisiblePosition( newVisPos, out tmp );
		}

		
		internal void InternalSetVisiblePosition( int newVisPos, out bool posChanged)
		{						 
			if ( null != this.cloneData )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_95"));

			posChanged = false;

			// If we are in a group set the visible position from within the 
			// group
			//
			// MD 1/20/09 - Groups in RowLayout
			// We should not affect the group's Columns collections when in GroupLayout style.
			//if ( null != this.Group  )
			if ( null != this.Group && 
			    this.Band != null && 
			    this.Band.RowLayoutStyle != RowLayoutStyle.GroupLayout )
			{
				// get the columns collection from the group
				//
				GroupColumnsCollection groupColsColl = this.Group.Columns;	//this.Group.GetGroupColsColl( true );

				if ( null == groupColsColl )
					throw new OutOfMemoryException( );

				groupColsColl.SetVisiblePosition( this, (short)newVisPos, ref posChanged);
			}
			else
			{
				// Otherwise, set the visible position with the band
				//
				this.Band.SetVisiblePosition( this, newVisPos, ref posChanged );
			}

			if ( posChanged )
			{
				// notify any interested parties
				//
				this.NotifyPropChange( PropertyIds.VisiblePosition );
			}
		}
	
		
		internal void InternalSetLevel( int level, bool notify )
		{
			if ( this.level == level ) return;

			if ( !notify )
			{
				this.level = level;
			}
			else
			{
				UltraGridGroup group = this.Group;

				if ( null != group )
				{
					GroupColumnsCollection groupCols = group.Columns;
        
					if ( null != groupCols )
					{
						bool tmp = true;
						groupCols.SetLevel( this, (short)level, ref tmp );
            
						this.level = level;
					}
				}
			}

			// notify any interested parties
			//
			this.NotifyPropChange( PropertyIds.Level );

		}



		internal void InternalSetGroup( UltraGridGroup group )
		{
			this.group = group;
		}



		/// <summary>
		/// 
		/// </summary>
		/// <param name="swapColumn"></param>
		/// <param name="thisColInfo"></param>
		/// <param name="swapColInfo"></param>
		internal void InternalSwapPosition( UltraGridColumn swapColumn, PositionInfo  thisColInfo, PositionInfo  swapColInfo )
		{
			// MD 1/20/09 - Groups in RowLayout
			if ( this.Band != null && this.Band.RowLayoutStyle == RowLayoutStyle.GroupLayout )
			{
				this.InternalSetVisiblePosition( swapColInfo.VisiblePos );
				swapColumn.InternalSetVisiblePosition( thisColInfo.VisiblePos );
				return;
			}

			if ( thisColInfo.Group != swapColInfo.Group )
			{
				swapColumn.SetGroup( thisColInfo.Group, thisColInfo.VisiblePos, thisColInfo.Level );
				this.SetGroup( swapColInfo.Group, swapColInfo.VisiblePos, swapColInfo.Level );
			}
			else
			{
				// If the columns are both in the same Group, then
				// let the CGroupColsColl perform the swap
				if ( null != thisColInfo.Group || null != swapColInfo.Group )
				{
					// get the columns collection from the group
					//
					GroupColumnsCollection groupColsColl = this.Group.Columns;
            
					if ( null != groupColsColl )
					{
						groupColsColl.Swap( this, swapColumn );
					}
				}
				else
				{
					// Changed to Internal version because now the automation
					// methods fire events.
					try 
					{
						this.InternalSetVisiblePosition( swapColInfo.VisiblePos );
						this.InternalSetLevel( swapColInfo.Level, true );
					} 
					catch ( Exception )
					{
						Debug.Fail( "this.InternalSetLevel failed in Column.InternalSwapPosition");
					}        

					try
					{
						swapColumn.InternalSetVisiblePosition( thisColInfo.VisiblePos );
						swapColumn.InternalSetLevel( thisColInfo.Level, true );
					} 
					catch ( Exception )
					{
						Debug.Fail( "swapColumn.InternalSetLevel failed in Column.InternalSwapPosition");
					}
				}        
			}
		}

		
		internal int InternalGetVisiblePosition()
		{
			// if we are a cloned column then get the value out
			// of the cloned data structure
			//
			if ( null != this.cloneData )
			{
				return this.cloneData.visiblePosition;
			}

			// If we are in a group get the visible position from within the 
			// group
			//
			// MD 1/20/09 - Groups in RowLayout
			// We should only consult the group's Columns collection when the row layout style is None.
            //if ( null != this.group )
			if ( null != this.group 
				&& this.Band.RowLayoutStyle == RowLayoutStyle.None )
			{
				// get the columns collection from the group
				//
				GroupColumnsCollection gcc = this.group.Columns;

				if ( null == gcc )
					throw new OutOfMemoryException();

				int visiblePos = -1;

				gcc.GetVisiblePosition( this, ref visiblePos );
			
				return visiblePos;
			}

			return this.Band.GetVisiblePosition( this );
		}
        
		
		/// <summary>
		/// Swaps the location of this column with the specified swapColumn.
		/// </summary>
		/// <param name="swapColumn">The column with which to swap.</param>
		// SSP 9/19/05 BR06292
		// Made public.
		// 
		//internal void Swap( UltraGridColumn swapColumn )
		public void Swap( UltraGridColumn swapColumn )
		{
			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			if ( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_96"));
    		
			// Allow swapping at design time
			if(null != swapColumn && null != grid 
				 )
			{
				// allocate columns array object to pass into
				// the Before and AfterColPosChanged events	
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList swappedColumns = new ArrayList(2);
				List<UltraGridColumn> swappedColumns = new List<UltraGridColumn>( 2 );
        
				if (null == swappedColumns)
					return;
        
				// make a cloned copy of the swap columns
				UltraGridColumn cloneThis = this.Clone( PosChanged.Swapped );
				UltraGridColumn cloneSwap = swapColumn.Clone( PosChanged.Swapped );
        
				if ( null == cloneThis || 
					null == cloneSwap || 
					null == cloneThis.CloneData ||
					null == cloneSwap.CloneData )
				{
					return;
				}

				// SSP 7/25/03 - Fixed headers
				// When a fixed header is swapped with a non-fixed header and vice versa,
				// make sure that we changed their fixed states.
				//
				// ------------------------------------------------------------------------------
				bool fixedStatesChanged = false;

				ColScrollRegion activeCSR = null != this.Band && null != this.Band.Layout
					? this.Band.Layout.ActiveColScrollRegion : null;
				bool thisHeaderFixed = false;
				bool swapHeaderFixed = false;
				int thisHeaderVisiblePos = this.Header.VisiblePosition;
				int swapHeaderVisiblePos = swapColumn.Header.VisiblePosition;

				if ( null != activeCSR && this.Band.AreFixedHeadersAllowed( activeCSR )
					// The reason for checking for groups is that when we have groups, we
					// don't need to do anything special regarding fixed header functionality
					// when swapping columns.
					//
					&& ! this.Band.HasGroups )
				{
					thisHeaderFixed = cloneThis.Header.FixedResolved;
					swapHeaderFixed = cloneSwap.Header.FixedResolved;
 
					if ( thisHeaderFixed != swapHeaderFixed )
					{
						cloneThis.Header.InternalSetFixed( swapHeaderFixed );
						cloneSwap.Header.InternalSetFixed( thisHeaderFixed );
						
						fixedStatesChanged = true;
					}
				}
				// ------------------------------------------------------------------------------
        
				// get the visible positions for the 
				// columns to be swapped
				PositionInfo thisCol  = this.PositionInfo;
				PositionInfo swapCol = swapColumn.PositionInfo;
        
				// set the cloned copies visible pos
				// if the swap operation succeeds
				cloneThis.SetPositionInfo( swapCol );
				cloneSwap.SetPositionInfo( thisCol );
				
        
				// insert it into the selected cols collection passing in
				// false as the third param so that the clone doesn't get 
				// addrefed in the insert				
				swappedColumns.Insert( 0, cloneThis );				
				swappedColumns.Insert( 0, cloneSwap );

				// SSP 8/2/05 - NAS 5.3 Column Swapping in Row-Layout
				// In row-layout mode swap the row-layout column infos.
				// 
				if ( this.Band.UseRowLayoutResolved )
				{
					cloneThis.RowLayoutColumnInfo.InitializeFrom( cloneSwap.RowLayoutColumnInfo );
					cloneSwap.RowLayoutColumnInfo.InitializeFrom( this.RowLayoutColumnInfo );
				}
        
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//// set a flag so that we know we are in the event
				////RobA 10/23/01 implemented inBeforePosChanged flag
				//this.Header.SetInBeforePosChanged(true);
				//swapColumn.Header.SetInBeforePosChanged(true);
				
        
				//fire the BeforeColPosChanged event
				BeforeColPosChangedEventArgs e = new BeforeColPosChangedEventArgs( PosChanged.Swapped, DragStrategy.ToColumnHeaderArray( swappedColumns.ToArray() ) );
				
				grid.FireBeforeColPosChanged( e );
        
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//// reset the flag
				////RobA 10/23/01 implemented inBeforePosChanged flag
				//this.Header.SetInBeforePosChanged(false);
				//swapColumn.Header.SetInBeforePosChanged(false);
				        
				// if it was canceled then exit
				if ( e.Cancel )				
					return;			
	
				// SSP 7/24/03 - Fixed headers
				// If we are swapping a fixed and non-fixed header, then exchange their fixed states.
				// Added the if block and enclosed the already existing code into the else block.
				//
				if ( fixedStatesChanged )
				{
					this.Header.InternalSetFixed( swapHeaderFixed );
					swapColumn.Header.InternalSetFixed( thisHeaderFixed );

					this.Header.InternalSetVisiblePosValue( swapHeaderVisiblePos );
					swapColumn.Header.InternalSetVisiblePosValue( thisHeaderVisiblePos );

					this.Band.BumpFixedHeadersVerifyVersion( true );

					swapColumn.Header.NotifyFixedChanged( );
					this.Header.NotifyFixedChanged( );
				}
				else
				{
					// Perform the Actual swap if columns are being 
					// swapped between groups.
					this.InternalSwapPosition( swapColumn, thisCol, swapCol );
				}

				// SSP 8/2/05 - NAS 5.3 Column Swapping in Row-Layout
				// In row-layout mode swap the row-layout column infos.
				// 
				if ( this.Band.UseRowLayoutResolved )
				{
					this.RowLayoutColumnInfo.InitializeFrom( cloneThis.RowLayoutColumnInfo );
					swapColumn.RowLayoutColumnInfo.InitializeFrom( cloneSwap.RowLayoutColumnInfo );
				}

				// Clear the collection of cloned columns and add 
				// this column into the collection so that the after event 
				// gets the actual column passed in instead of the clone
				//
				swappedColumns.Clear();

				swappedColumns.Insert( 0, this );
				swappedColumns.Insert( 0, swapColumn );

				// fire the after event and clean up
				grid.FireAfterColPosChanged( new AfterColPosChangedEventArgs( PosChanged.Swapped, DragStrategy.ToColumnHeaderArray( swappedColumns.ToArray() ) ) );
				
				swappedColumns.Clear( );		
			}
		}

		internal bool AllowSwapping
		{
			get
			{
				AllowColSwapping allowSwapping = this.Band.AllowColSwappingResolved;    
				Debug.Assert( allowSwapping != AllowColSwapping.Default, "AllowColSwapping set to Default!");    
				return ( allowSwapping != AllowColSwapping.NotAllowed );
			}
		}	



		// SSP 3/21/02
		// Version 2 Row Filter Implementation
		//
		#region "Filter code"

		/// <summary>
		/// Set this to <b>False</b> to explicitly disable row filtering on this column.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridOverride.AllowRowFiltering"/>
		/// <seealso cref="UltraGridOverride.FilterUIType"/>
		/// </remarks>
		[ LocalizedDescription( "LDR_Column_AllowRowFiltering" ) ]
		public DefaultableBoolean AllowRowFiltering
		{
			get
			{
				return this.allowRowFiltering;
			}
			set
			{
				if ( this.allowRowFiltering != value )
				{
					if ( !Enum.IsDefined( typeof( DefaultableBoolean ), value ) )
						throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_104"), (int)value, typeof( DefaultableBoolean ) );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.AllowRowFiltering, null );

					this.allowRowFiltering = value;
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAllowRowFiltering( ) 
		{
			return DefaultableBoolean.Default != this.allowRowFiltering;
		}
 
		/// <summary>
		/// Resets AllowRowFiltering to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetAllowRowFiltering() 
		{
			this.AllowRowFiltering = DefaultableBoolean.Default;
		}



		internal DefaultableBoolean AllowRowFilteringResolved
		{
			get
			{
				if ( this.ShouldSerializeAllowRowFiltering( ) )
					return this.AllowRowFiltering;

				return this.Band.AllowRowFilteringResolved;
			}
		}

		#endregion // End of filter code

		#region AllowRowSummaries

		/// <summary>
		/// Indicates whether to allow the user to be able to specify a summary for this column.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridOverride.AllowRowSummaries"/>
		/// <seealso cref="UltraGridBand.Summaries"/>
        /// <seealso cref="SummarySettingsCollection.Add(string, SummaryType, UltraGridColumn, SummaryPosition)"/>
		/// <seealso cref="UltraGridOverride.SummaryFooterAppearance"/>
		/// </remarks>
		[ LocalizedDescription( "LDR_Column_AllowRowSummaries" ) ]
		public AllowRowSummaries AllowRowSummaries
		{
			get
			{
				return this.allowRowSummaries;
			}
			set
			{
				if ( this.AllowRowSummaries != value )
				{
					if ( !Enum.IsDefined( typeof( AllowRowSummaries ), value ) )
						throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_314"), (int)value, typeof( AllowRowSummaries ) );

					this.allowRowSummaries = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.AllowRowSummaries, null );
				}
			}
		}

		#endregion //AllowRowSummaries
		
		#region IsColumnNumeric

		// SSP 5/24/02
		// Added IsColumnNumeric property for use by summary rows feature (to decide
		// what summaries are enabled in summary dialog.
		//
		internal bool IsColumnNumeric
		{
			get
			{
				System.Type type = this.DataType;

				// SSP 10/2/06 BR16309
				// 
				// --------------------------------------------------------------------
				Type underlyingType = System.Nullable.GetUnderlyingType( type );
				if ( null != underlyingType )
					type = underlyingType;
				// --------------------------------------------------------------------

				return 
					typeof( int )		== type || 
					typeof( uint )		== type || 
					typeof( short )		== type ||
					typeof( ushort )	== type ||
					typeof( long )		== type ||
					typeof( ulong )		== type ||
					typeof( float )		== type ||
					typeof( double )	== type ||
					typeof( decimal )	== type ||
					typeof( byte )		== type ||
					typeof( sbyte )		== type;
			}
		}

		#endregion // IsColumnNumeric

		#region SingleSummariesOnly

		// SSP 8/8/03 - Row Layout Functionality - Multiple Summaries
		//
		internal bool SingleSummariesOnly
		{
			get
			{
				Infragistics.Win.UltraWinGrid.AllowRowSummaries allowSummaries = this.AllowRowSummaries;
				if ( Infragistics.Win.UltraWinGrid.AllowRowSummaries.Default == allowSummaries )
					allowSummaries = this.Band.AllowRowSummariesResolved;

				return AllowRowSummaries.SingleSummary == allowSummaries ||
					AllowRowSummaries.SingleSummaryBasedOnDataType == allowSummaries;
			}
		}

		#endregion // SingleSummariesOnly

		#region AllowRowSummariesResolved

		internal bool AllowRowSummariesResolved
		{
			get
			{
				// SSP 5/12/05 BR03958
				// If the user explicitly enables the AllowSummaries on an UltraCombo then
				// still resolve it to false since summaries user interface is not supported
				// in UltraCombo.
				//
				if ( null == this.Layout || ! ( this.Layout.Grid is UltraGrid ) )
					return false;

				// SSP 8/8/03 - Row Layout Functionality - Multiple Summaries
				//
				// ------------------------------------------------------------------------------------
				Infragistics.Win.UltraWinGrid.AllowRowSummaries allowSummaries = this.AllowRowSummaries;
				if ( Infragistics.Win.UltraWinGrid.AllowRowSummaries.Default == allowSummaries )
					allowSummaries = this.Band.AllowRowSummariesResolved;

				switch ( allowSummaries )
				{
					case Infragistics.Win.UltraWinGrid.AllowRowSummaries.True:
						return true;
					case Infragistics.Win.UltraWinGrid.AllowRowSummaries.False:
						return false;
					case Infragistics.Win.UltraWinGrid.AllowRowSummaries.BasedOnDataType:
						return this.IsColumnNumeric;						
					case Infragistics.Win.UltraWinGrid.AllowRowSummaries.SingleSummary:
						return true;
					case Infragistics.Win.UltraWinGrid.AllowRowSummaries.SingleSummaryBasedOnDataType:
						return this.IsColumnNumeric;
				}

				
				// ------------------------------------------------------------------------------------

				return false;
			}
		}

		#endregion // AllowRowSummariesResolved

		#region ShouldSerializeAllowRowSummaries

		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAllowRowSummaries() 
		{
			return AllowRowSummaries.Default != this.allowRowSummaries;
		}

		#endregion // ShouldSerializeAllowRowSummaries
 
		#region ResetAllowRowSummaries

		/// <summary>
		/// Resets AllowRowSummaries to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetAllowRowSummaries() 
		{
			this.AllowRowSummaries = AllowRowSummaries.Default;
		}

		#endregion // ResetAllowRowSummaries

		#region AllowGroupByResolved

		/// <summary>
		/// Determines whether columns from this band can be dragged into
		/// the GroupByBox to become GroupBy columns. If resolved to default
		/// will assume 'Yes'.
		/// </summary>
		internal DefaultableBoolean AllowGroupByResolved
		{
			get
			{
				// SSP 9/28/07 BR26796
				// If the SortIndicator is set to Disabled then we cannot sort the column 
				// and thus cannot group by the column as well. Attempting to add it to
				// SortedColumns collection, which is necessary for grouping by the column, 
				// will result in an exception. Therefore disable grouping when the 
				// column's SortIndicator is set to Disabled.
				// 
				if ( SortIndicator.Disabled == this.SortIndicator )
					return DefaultableBoolean.False;
				
				if ( DefaultableBoolean.Default == this.AllowGroupBy )				
					return this.Band.AllowGroupByResolved;				
				
				if ( DefaultableBoolean.False == this.Band.AllowGroupByResolved )
					return DefaultableBoolean.False;

				return this.AllowGroupBy;
			}
		}

		#endregion // AllowGroupByResolved

		#region AllowGroupBy

		/// <summary>
		/// Determines whether this column can be dragged into
		/// the GroupByBox to become a GroupBy column. 
		/// The <b>Default</b> is resolved to <b>True</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>AllowGroupBy</b> property determines whether the user is allowed
		/// to group-by the column. If set the <b>False</b> the user won't be allowed
		/// to add or remove this column from the group-by box. This property does
		/// not prevent you from grouping the rows by the column in code.
		/// </p>
		/// <p class="body">
		/// To enable the funtionality for grouping rows, set the Layout's
		/// <see cref="UltraGridLayout.ViewStyleBand"/> property to 
		/// <b>OutlookGroupBy</b>. This will display a group-by box on the top
		/// of the grid where the user can drag and drop a column to group rows
		/// by that column. To group rows by a column in code, add the column to
		/// the <see cref="UltraGridBand.SortedColumns"/> collection and specify
		/// true for the groupBy parameter of the <b>Add</b> method.
		/// </p>
		/// <para class="body">
		/// Also note that you can set the <see cref="UltraGridColumn.SortIndicator"/> 
		/// property of a column to <b>Disabled</b> to disable sorting as well. When 
		/// SortIndicator is set to Disabled, grouping by the column will be disabled
		/// as well.
		/// </para>
		/// <seealso cref="UltraGridOverride.AllowGroupBy"/>
		/// <seealso cref="UltraGridColumn.SortIndicator"/>
		/// <seealso cref="UltraGridLayout.ViewStyleBand"/>
		/// <seealso cref="UltraGridLayout.GroupByBox"/>
		/// <seealso cref="UltraGridOverride.GroupByColumnsHidden"/>
		/// </remarks>
		[LocalizedDescription("LD_AppearanceCache_P_AllowGroupBy")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean AllowGroupBy
		{
			get
			{
				if ( DefaultableBoolean.Default == this.allowGroupBy )
					return this.Band.AllowGroupByResolved;
					
				return this.allowGroupBy;
			}
			set
			{
				if ( value != this.allowGroupBy )
				{

                    // MRS NAS v8.3 - Unit Testing                    
                    //// test that the value is in range
                    ////
                    //if ( !Enum.IsDefined( typeof( DefaultableBoolean ), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_98") );
                    if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
                        throw new InvalidEnumArgumentException("AllowGroupBy", (int)value, typeof(DefaultableBoolean));

					this.allowGroupBy = value;
					this.NotifyPropChange( PropertyIds.AllowGroupBy, null );
				}
			}
		}

		#endregion // AllowGroupBy

		#region IsGroupByColumn

		// SSP 1/3/02 UWG346
		// UWG346 wants this to be a read-only property because
		// 'Is' in IsGroupByColumn is indicative of a read-only
		// property
		//
		

		/// <summary>
		/// Indicates whether rows are grouped by this column or not.
		/// (Whether this is a group by column or not).
		/// </summary>
		[Browsable(false)]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]	
		public bool IsGroupByColumn
		{
			get
			{
				// SSP 6/17/04 UWG3381
				// Return false if the view style is not an outlook group-by view style.
				//
				//return this.isGroupByColumn;
				return this.isGroupByColumn 
					&& ( null == this.Layout || null == this.Layout.ViewStyleImpl 
					|| this.Layout.ViewStyleImpl.SupportsGroupByRows );
			}
			// SSP 1/3/02 UWG346
			// UWG346 wants this to be read-only
			//
			//
			
		}

		#endregion // IsGroupByColumn

		#region SetGroupByColumnStatus

		internal void SetGroupByColumnStatus( bool groupedBy )
		{
			if ( groupedBy != this.isGroupByColumn )
			{
				// Band.SetSortedColumn will set isGroupByColumn member to true
				// by calling InternalSetIsGroupByColumn( ) function

				// Default the sorting to ascending
				SortIndicator sortIndicator = SortIndicator.Ascending;

				if ( SortIndicator.None != this.SortIndicator &&
					SortIndicator.Disabled != this.SortIndicator )
					sortIndicator = this.SortIndicator;

				// Make sure that the sortIndicator is not disabled.
				// Or else SetSortedColumn will fail
				if ( SortIndicator.Disabled == this.sortIndicator )
					this.sortIndicator = SortIndicator.None;
                    
				if ( groupedBy )
				{
					this.Band.SetSortedColumn( this, sortIndicator, true, false );						
				}
				else 
				{
					// remove it from the sorted cols if groupedBy is false
					if ( this.Band.HasSortedColumns && this.Band.SortedColumns.Count > 0 )
					{
						try
						{
							// SSP 8/6/02 UWG1480
							// If the key is null or "", then this will not work. Use the new
							// GetColumnIndex helper method off the SortedColumnsCollection and
							// use the returned index to remove from the collection.
							// 
                            //if ( this.Band.SortedColumns.Exists( this.Key ) )
							//	this.Band.SortedColumns.Remove( this );
							int index = this.Band.SortedColumns.GetColumnIndex( this );
							if ( index >= 0 )
								this.Band.SortedColumns.Remove( index );

						}
						catch ( Exception )
						{
							Debug.Assert( false, "Exception thrown while removing group " +
								"by column from the sorted columns collection" );
						}
					}
				}

				// SSP 6/30/05 - NAS 5.3 Column Chooser
				// Reset the hiddenWhenGroupByOverride when the column is ungrouped.
				// 
				if ( ! this.IsGroupByColumn )
					this.hiddenWhenGroupByOverride = DefaultableBoolean.Default;

				this.NotifyPropChange( PropertyIds.IsGroupByColumn, null );
			}
		}

		#endregion // SetGroupByColumnStatus

		#region ClickGroupByButton

		internal void ClickGroupByButton( )
		{
			// SSP 3/4/02 UWG1032
			// Fire the BeforeSortChange before actually changing the
			// sort indicator.
			//
			UltraGrid ultraGrid = this.Band.Layout.Grid as UltraGrid;

			Debug.Assert( null != ultraGrid, "ClickGroupByButton called when not ultragrid !" );

            // JDN 11/16/04 Added Synchronous Sorting and Filtering
            ProcessMode sortProcessMode = ProcessMode.Lazy;

			if ( null != ultraGrid )
			{
				// Store the original sort indicator value so that we can
				// restore it in case the user cancels the sort change in
				// BeforeSortChange event.
				//
				Infragistics.Win.UltraWinGrid.SortIndicator originalSortIndicatorVal = this.sortIndicator;


				if ( SortIndicator.Ascending == this.SortIndicator )
					this.sortIndicator = SortIndicator.Descending;
				else
					this.sortIndicator = SortIndicator.Ascending;

				BeforeSortChangeEventArgs e = 
					new BeforeSortChangeEventArgs( this.Band, this.Band.SortedColumns );

				ultraGrid.FireEvent( Infragistics.Win.UltraWinGrid.GridEventIds.BeforeSortChange, e );

				// Restore to the original value, allowing for below logic to
				// work.
				//
				this.sortIndicator = originalSortIndicatorVal;

				// If the sort changed was canceled, then return
				//
				if ( e.Cancel )
				{					
					return;
				}

                // JDN 11/16/04 Added Synchronous Sorting and Filtering
                sortProcessMode = e.ProcessMode;
			}

			if ( SortIndicator.Ascending == this.SortIndicator )
			{
				// SSP 10/1/01 UWG388
				// Changes made for UWG388 required below change as well
				// which is to call the InternalSetSortIndicator method
				// instead of SortIndicator property
				//
				//this.SortIndicator = SortIndicator.Descending;
				this.InternalSetSortIndicator( SortIndicator.Descending );			
			}
			else
			{
				// SSP 10/1/01 UWG388
				// Changes made for UWG388 required below change as well
				// which is to call the InternalSetSortIndicator method
				// instead of SortIndicator property
				//
				//this.SortIndicator = SortIndicator.Ascending;
				this.InternalSetSortIndicator( SortIndicator.Ascending );
			}

			// JJD 1/8/02
			// Just dirty the visible rows but don't set the first
			// row back to the beginning
			//
			// SSP 1/23/02 UWG963
			// Use the new method to dirty the visible rows
			//
			//this.band.Layout.RowScrollRegions.DirtyAllVisibleRows();
			this.Band.SortedColumns.OnSortChanged( );

			this.band.Layout.DirtyGridElement( );

			// SSP 3/4/02 UWG1032
			// Fire the AfterSortChange once the sort indicator has been changed
			//
			if ( null != ultraGrid )
			{
                // JDN 11/16/04 Added Synchronous Sorting and Filtering
                // Ensure sorting is synchronously processed for either all rows or all expanded rows
                // if the BeforeSortChangeEventArgs.ProcessMode is set to a synchronous value
				// SSP 2/4/05
				// Use the new EnsureSortedAndFilteredHelper method instead of duplicating the code
				// in a few places. Also make sure to pass in the lowest level band otherwise it
				// will sort and filter lower level bands than this band.
				//
				ultraGrid.Rows.EnsureSortedAndFilteredHelper( sortProcessMode, this.Band );
				

				BandEventArgs e = new BandEventArgs( this.Band );
				ultraGrid.FireEvent( GridEventIds.AfterSortChange, e );
			}
		}

		#endregion // ClickGroupByButton

		internal bool TempIsGroupByColumn
		{
			get
			{
				return this.tempIsGroupByColumn;
			}
			set
			{
				this.tempIsGroupByColumn = value;
			}
		}

		/// <summary>
		/// Returns true if property needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeIsGroupByColumn() 
		{
			return this.IsGroupByColumn;
		}
 
		/// <summary>
		/// Reset property to its default value
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetIsGroupByColumn() 
		{
			// SSP 1/3/02
			// Changed the property into an internal method prompting this
			// change
			//
			//this.IsGroupByColumn = false;
			this.SetGroupByColumnStatus( false );
		}

		internal void InternalSetIsGroupByColumn( bool val )
		{
			if ( this.isGroupByColumn != val )
			{
				this.isGroupByColumn = val;

				// SSP 7/24/03 - Fixed headers
				// If a header is grouped by, it can potentially get hidden (since we by default
				// don't show the group by columns). So bump the fixed headers version number
				// so the fixed headers cache gets recalculated.
				//
				if ( null != this.Band )
					this.Band.BumpFixedHeadersVerifyVersion( );

				// SSP 10/19/05 BR06245
				// Notify prop change so if this column belongs to an UltraGridGroup, the
				// UltraGridGroup can re-adjust it's width in case the column's Hidden state
				// is going to change as a result of change in it's group-by state.
				// 
				this.NotifyPropChange( PropertyIds.IsGroupByColumn );
			}
		}
		
		// SSP 9/14/04
		// Added InternalGetIsGroupByColumn method.
		//
		internal bool InternalGetIsGroupByColumn( )
		{
			return this.isGroupByColumn;
		}

		/// <summary>
		/// Returns true if property needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAllowGroupBy() 
		{
			return ( this.allowGroupBy != DefaultableBoolean.Default );
		}
 
		/// <summary>
		/// Reset property to its default value
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetAllowGroupBy() 
		{
			this.AllowGroupBy = DefaultableBoolean.Default;
		}
	




		internal bool IsSwappingAllowed		// AS 9/24/01 This method should be internal, not public
		{
			get
			{				
				AllowColSwapping allowSwapping = this.Band.AllowColSwappingResolved;
				
				return allowSwapping != AllowColSwapping.NotAllowed;
			}
		}

	
		internal void CreateSwapList( Infragistics.Win.ValueList swapDropDown )
		{
			ColumnsCollection columns = this.Band.Columns;
    
			if ( null != columns )
			{				
				// SSP 7/25/03 - Fixed headers
				// If the FixedHeaderIndicator type is InSwapDropDown, then add Fix Header or Unfix Header
				// item to the swap drop down list depending on whether the header is currently not fixed
				// or fixed.
				//
				// --------------------------------------------------------------------------------------
				ColScrollRegion activeCSR = null != this.Band && null != this.Band.Layout 
					? this.Band.Layout.ActiveColScrollRegion : null;
				this.Band.AddSwapDropdownFixUnfixItem( swapDropDown, this.Header );
				// --------------------------------------------------------------------------------------

				// MD 1/21/09 - Groups in RowLayouts
				// Cache the resolved group because in GroupLayout style, it will return the parent group of the layout item.
				UltraGridGroup thisGroup = this.GroupResolved;

				// loop thru columns collection looking for a match
				//
				for ( int i = 0; i < columns.Count; i++ )
				{
					UltraGridColumn column = columns[i];
					// Only check the iterator item if it 
					// meets these conditions, otherwise there's
					// no reason to perform more detailed analysis.
					if ( column != this  &&
						!column.IsChaptered &&
                        // MRS 2/23/2009 - TFS14354
						//!column.Hidden)					
                        !column.HiddenResolved)
					{
						// MD 1/21/09 - Groups in RowLayouts
						// Cache the resolved group because in GroupLayout style, it will return the parent group of the layout item.
						UltraGridGroup columnGroup = column.GroupResolved;

						AllowColSwapping allowSwapping = this.Band.AllowColSwappingResolved;

						Debug.Assert( allowSwapping != AllowColSwapping.Default, "AllowColSwapping is set to Default!");
						Debug.Assert( allowSwapping != AllowColSwapping.NotAllowed, "CreateSwapList Called when swapping not allowed!");

						// SSP 7/25/03 - Fixed headers
						// Skip the headers that can't be swapped because doing so would cause the Fixed
						// state of a header to change in violation of FixedHeaderIndicator settings.
						//
						// ------------------------------------------------------------------------------
						if ( null != activeCSR && this.Band.AreFixedHeadersAllowed( activeCSR ) )
						{
							// MD 1/21/09 - Groups in RowLayouts
							// Use the cached groups.
							//if ( null != this.Group || null != column.Group )
							//{
							//    if ( null != this.Group && null != column.Group )
							if ( null != thisGroup || null != columnGroup )
							{
								if ( null != thisGroup && null != columnGroup )
								{
									if ( this.Band.IsHeaderFixed( activeCSR, this.Header ) != this.Band.IsHeaderFixed( activeCSR, column.Header ) )
									{
										// MD 1/21/09 - Groups in RowLayouts
										// Use the cached groups.
										//if ( FixedHeaderIndicator.None == this.Group.Header.FixedHeaderIndicatorResolved
										//    || FixedHeaderIndicator.None == column.Group.Header.FixedHeaderIndicatorResolved )
										if ( FixedHeaderIndicator.None == thisGroup.Header.FixedHeaderIndicatorResolved
											|| FixedHeaderIndicator.None == columnGroup.Header.FixedHeaderIndicatorResolved )
											continue;
									}
										// If one group's FixedHeaderIndicator is None and other's not, don't allow the user to
										// swap columns between these groups.
										//
									// MD 1/21/09 - Groups in RowLayouts
									// Use the cached groups.
									//else if ( ( FixedHeaderIndicator.None == this.Group.Header.FixedHeaderIndicatorResolved )
									//    != ( FixedHeaderIndicator.None == column.Group.Header.FixedHeaderIndicatorResolved ) )
									else if ( ( FixedHeaderIndicator.None == thisGroup.Header.FixedHeaderIndicatorResolved )
										!= ( FixedHeaderIndicator.None == columnGroup.Header.FixedHeaderIndicatorResolved ) )
									{
										continue;
									}
								}
								else
									continue;
							}
							else
							{

								if ( this.Band.IsHeaderFixed( activeCSR, this.Header ) != this.Band.IsHeaderFixed( activeCSR, column.Header ) )
								{
									if ( FixedHeaderIndicator.None == this.Header.FixedHeaderIndicatorResolved
										|| FixedHeaderIndicator.None == column.Header.FixedHeaderIndicatorResolved )
										continue;
								}
							}
						}
						// ------------------------------------------------------------------------------
                        
						if ( allowSwapping == AllowColSwapping.WithinGroup )
						{
							
							// Only Add Column to the SwapList if they are in the same
							// groups.
							// MD 1/21/09 - Groups in RowLayouts
							// Use the cached groups.
							//if( this.Group == column.Group )
							if ( thisGroup == columnGroup )
							{
								swapDropDown.ValueListItems.Add( column, column.Header.Caption );
							}
								
						}
						else
						{   
							swapDropDown.ValueListItems.Add( column, column.Header.Caption );
						}
					}
				}
			}

		}	

		internal void AdjustForRowBorders( ref System.Drawing.Rectangle rect )
		{
			UIElementBorderStyle borderStyle = this.Band.BorderStyleRowResolved;
			int nBorderThickness =	this.Layout.GetBorderThickness( borderStyle );

			if ( nBorderThickness < 1 )
				return;

			int nMaxSpacingW = rect.Width/2 - ( 1 + nBorderThickness );
			int nMaxSpacingH = rect.Height/2 - ( 1 + nBorderThickness );

			int nSpacingH = Math.Max ( 0, Math.Min ( nBorderThickness, nMaxSpacingH ));
			int nSpacingW = Math.Max ( 0, Math.Min ( nBorderThickness, nMaxSpacingW ));

			// Adjust for top and bottom border
			//
			rect.Offset( 0, nSpacingH );
			rect.Height -= nSpacingH;

			if ( nSpacingW < 1 )
				return;


			// If this is marked as the first position item then adjust for
			// the row's left border
			//
			if ( this.FirstItem )
			{
				// Only adjust the left for the row border if the thickness of the
				// border is > 1 or if we aren't drawing row selectors for this band
				//
				// JJD 1/15/02
				// Use the band's BorderStyleRowResolved to do the test
				//
				// SSP 6/18/04 UWG3216
				// Why should existence of row selector matter?
				//
				// SSP 4/20/05 BR03223
				// Uncommented out the following two lines. This is necessary because we 
				// reverted the check for mergeability of the borders below (for BR03223).
				//
				if ( !Layout.CanMergeAdjacentBorders( this.band.BorderStyleRowResolved ) ||
					this.band.RowSelectorExtent < 1 )
				// SSP 2/16/05 BR02415
				// Also take into account the cell's border style.
				//
				//if ( !Layout.CanMergeAdjacentBorders( this.band.BorderStyleRowResolved ) )
				// SSP 4/20/05 BR03223
				// If the row and cell borders are dotted then in order for them to be drawn correctly we need
				// to not merge the borders. Instead we need to simply not draw the cell border that overlaps
				// with the row border. However we can not have the cell span over the row border otherwise
				// it will draw over the row border. So we always need to shrink the cell by the thickness
				// of the row borders regardless of where the row and cell borders are mergeable. Commented 
				// out the following condition that checks to see if the row and cell borders are mergeable. 
				// Always substract the row border width.
				//
				//if ( !Layout.CanMergeAdjacentBorders( this.band.BorderStyleRowResolved, this.band.BorderStyleCellResolved ) )
				{
					rect.X += nSpacingW;
					
					// SSP 8/2/02 UWG1464
					// If the Left coordinate is adjusted, then compensate the width 
					// so the Right stays the same. When we are merging borders, we only 
					// want to adjust a dimension that's involved in merging the borders
					// while keep the rest of the dimensions the same.
					//
					rect.Width -= nSpacingW;
				}
			}

			if ( this.LastItem )
			{
				// adjust the right for the row border since this is the 
				// last visible item
				//
				// SSP 6/18/04 UWG3216
				// Only substract the row border width if the borders of the row are not mergeable.
				//
				// SSP 2/16/05 BR02415
				// Also take into account the cell's border style.
				//
				//if ( ! this.Layout.CanMergeAdjacentBorders( this.band.BorderStyleRowResolved ) )
				// SSP 4/20/05 BR03223
				// If the row and cell borders are dotted then in order for them to be drawn correctly we need
				// to not merge the borders. Instead we need to simply not draw the cell border that overlaps
				// with the row border. However we can not have the cell span over the row border otherwise
				// it will draw over the row border. So we always need to shrink the cell by the thickness
				// of the row borders regardless of where the row and cell borders are mergeable. Commented 
				// out the following condition that checks to see if the row and cell borders are mergeable. 
				// Always substract the row border width.
				//
				//if ( ! this.Layout.CanMergeAdjacentBorders( this.band.BorderStyleRowResolved, this.band.BorderStyleCellResolved ) )
					rect.Width  -= nSpacingW;
			}
		}

		internal void AdjustForCellLevel ( ref System.Drawing.Rectangle rect, Infragistics.Win.UltraWinGrid.UltraGridRow Row)
		{
			// Save the original bottom of the rect
			//
			int   nOrigBottom   = rect.Bottom;
			int nLevel        = this.GetDisplayLevel();

			rect.Offset( 0, Row.GetLevelOffset( nLevel, true ) );

			// make sure the bottom doesn't extend below its initial passed in value
			//
			rect.Height =  Math.Min( nOrigBottom - rect.Top, Row.GetLevelHeight( nLevel ) );
		}





		internal void AdjustForCellSpacing( ref System.Drawing.Rectangle rect )
		{
			int nCellSpacing =	this.band.CellSpacingResolved;

			if ( nCellSpacing < 1 )
				return;

			int nBorderThickness = this.Layout.GetBorderThickness(this.band.Layout.BorderStyle);

			int nMaxSpacingW =	rect.Width/2 - ( 1 + nBorderThickness );
			int nMaxSpacingH =	rect.Height/2 - ( 1 + nBorderThickness );

			int nSpacingH = Math.Max ( 0, Math.Min ( nCellSpacing, nMaxSpacingH ));
			int nSpacingW = Math.Max ( 0, Math.Min ( nCellSpacing, nMaxSpacingW ));
    
			rect.Inflate( -nSpacingW, -nSpacingH );
		}

		// SSP 5/01/03
		// This is not used anywhere. Commenting it out.
		//
		
		
		// SSP 8/2/02
		// EM Embeddable editors.
		// Not being used anywhere. So commented it out.
		// 
		

		
		/// <summary>
		/// Returns the minwidth value. If minwidth is zero it returns the default minimum
		/// </summary>
		/// <returns></returns>
		protected int GetMinWidth( )
		{ 
			// SSP 4/7/03 UWG2092
			// There is no reason for us to restrict the min width to a minimum of 10.
			// So if the user sets the min width to something less than 10, honor it.
			//
			if ( this.minWidth > 0 )
				return this.minWidth;

			int minimumWidth=0;

			// Minimum width should be based on column style
			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//switch ( this.StyleResolved )
			switch ( this.GetStyleResolved( null ) )
			{
				case ColumnStyle.EditButton:
				case ColumnStyle.CheckBox:
				case ColumnStyle.TriStateCheckBox:
				case ColumnStyle.DropDown:
				case ColumnStyle.DropDownList:
				case ColumnStyle.DropDownValidate:
				case ColumnStyle.DropDownCalendar:
					minimumWidth = SystemInformation.VerticalScrollBarWidth;
					break;
				default:
					minimumWidth = MIN_COL_WIDTH_SETTING;
					break;
			}

			// JAS v5.2 Wrapped Header Text 4/25/05
			// Account for the borders of the WindowsXPCommand header style.
			//
			if( this.Band != null && this.Band.HeaderStyleResolved == HeaderStyle.WindowsXPCommand )
				minimumWidth += 6;

			return Math.Max( this.minWidth, minimumWidth); 
		}

		// SSP 5/1/03 - Cell Level Editor
		// Added Editor and ValueList properties off the cell so the user can set the editor
		// and value list on a per cell basis.
		// Commented out HasDropDown since cell is the one that needs to have it. 
		//
		

		// SSP 4/14/03
		// Added an overload that takes an int specifying the number of rows to base the auto-resize calculations on.
		// By default, we take into account only the visible rows. The new overload will allow the user to
		// specify the number of rows and also predefined constants that will mean visible rows, and all rows.
		//
		/// <summary>
		/// Resizes the column based on the cell values in first nRows rows in the band.
		/// </summary>
        /// <param name="nRows">The number of rows on which to base the sizing of the column.</param>
		public void PerformAutoResize( int nRows )
		{
			if ( nRows <= 0 )
				// SSP 6/23/03 - Localization
				//
				//throw new ArgumentOutOfRangeException( "nRows", nRows, "nRows must be greater than 0." );
				throw new ArgumentOutOfRangeException( "nRows", nRows, SR.GetString( "LER_ArgumentException_3" ) );

			// SSP 12/8/05 BR07665
			// Added this overload of PerformAutoResize that takes in maxRowCollectionsToTraverse parameter.
			// 
			//this.PerformAutoResizeHelper( null, nRows );
			this.PerformAutoResizeHelper( null, nRows, 0 );
		}

		// SSP 12/8/05 BR07665
		// Added this overload of PerformAutoResize that takes in maxRowCollectionsToTraverse parameter.
		// 
		/// <summary>
		/// Resizes the column based on the cell values in first nRows rows in the band.
		/// </summary>
		/// <param name="nRows">Specifies the maximum number of rows to base the auto-sizing on.</param>
		/// <param name="maxRowCollectionsToTraverse">Specifies the maximum number of rows collections to base the auto-sizing on.</param>
		public void PerformAutoResize( int nRows, int maxRowCollectionsToTraverse )
		{
			if ( nRows <= 0 )
				// SSP 6/23/03 - Localization
				//
				//throw new ArgumentOutOfRangeException( "nRows", nRows, "nRows must be greater than 0." );
				throw new ArgumentOutOfRangeException( "nRows", nRows, SR.GetString( "LER_ArgumentException_3" ) );

			if ( maxRowCollectionsToTraverse <= 0 )
				throw new ArgumentOutOfRangeException( "maxRowCollectionsToTraverse", maxRowCollectionsToTraverse, SR.GetString( "LER_ArgumentException_3" ) );

			this.PerformAutoResizeHelper( null, nRows, maxRowCollectionsToTraverse );
		}

		
		
		
		
		

		/// <summary>
		/// Resizes the column based on cell values in either currently visible rows or all rows depending on the value of autoSizeType argument.
		/// </summary>
		/// <param name="autoSizeType"></param>
		public void PerformAutoResize( PerformAutoSizeType autoSizeType )
		{
			// SSP 8/16/05 BR05387
			// 
			//this.PerformAutoResizeHelper( null, PerformAutoSizeType.AllRowsInBand == autoSizeType ? 0 : -1 );
			this.PerformAutoResize( autoSizeType, true );
		}

		// SSP 8/16/05 BR05387
		// Added this overload of PerformAutoResize.
		// 
		/// <summary>
		/// Resizes the column based on cell values in either currently visible rows or all rows depending on the value of autoSizeType argument.
		/// </summary>
		/// <param name="autoSizeType">Specifies if and which rows to base the auto-sizing of the column.</param>
		/// <param name="includeHeader">Specifies whether to include header caption width into the auto-size.</param>
		public void PerformAutoResize( PerformAutoSizeType autoSizeType, bool includeHeader )
		{
			this.PerformAutoResizeHelper( null, PerformAutoSizeType.AllRowsInBand == autoSizeType ? 0 : -1, true, includeHeader, true );
		}

		/// <summary>
		/// Resizes the column based on cell values in currently visible rows.
		/// </summary>
		public void PerformAutoResize( )
		{
			this.PerformAutoResize( PerformAutoSizeType.VisibleRows );
		}

		// SSP 6/24/03 UWG2385
		// Added CalculateAutoResizeWidth method.
		//
		/// <summary>
		/// Calculates the autoresize width.
		/// </summary>
		/// <param name="autoSizeType">Auto-size type.</param>
		/// <param name="includeHeader">Whether to include the header width.</param>
		/// <returns></returns>
		public int CalculateAutoResizeWidth( PerformAutoSizeType autoSizeType, bool includeHeader )
		{
			if ( PerformAutoSizeType.None == autoSizeType )
			{
				if ( includeHeader )
					return this.PerformAutoResizeHelper( null, 0, false, true, false );
				else
					return this.Width;
			}

			return this.PerformAutoResizeHelper( null, 
				PerformAutoSizeType.AllRowsInBand == autoSizeType ? 0 : -1, false,
				includeHeader, true );
		}

		// SSP 6/24/03 UWG2385
		// Added CalculateAutoResizeWidth method.
		//
		/// <summary>
		/// Calculates the auto-size width of the column based on the cell values in first nRows rows in the band.
		/// </summary>
		/// <param name="nRows">Number of rows to base the auto-size calculations on.</param>
		/// <param name="includeHeader">Whether to include the header width.</param>
		/// <returns></returns>
		public int CalculateAutoResizeWidth( int nRows, bool includeHeader )
		{			
			if ( nRows <= 0 )
				// SSP 6/23/03 - Localization
				//
				//throw new ArgumentOutOfRangeException( "nRows", nRows, "nRows must be greater than 0." );
				throw new ArgumentOutOfRangeException( "nRows", nRows, SR.GetString( "LER_ArgumentException_3" ) );

			return this.PerformAutoResizeHelper( null, nRows, false, includeHeader, false );
		}

		internal void PerformAutoResizeHelper( RowsCollection rows, int nRows )
		{
			this.PerformAutoResizeHelper( rows, nRows, 0 );
		}
		
		// SSP 12/8/05 BR07665
		// Added this overload of PerformAutoResizeHelper that takes in maxRowsCollections parameter.
		// 
		internal void PerformAutoResizeHelper( RowsCollection rows, int nRows, int maxRowsCollections )
		{
			this.PerformAutoResizeHelper( rows, nRows, true, true, true, maxRowsCollections );
		}
		
		// SSP 6/24/03 UWG2385
		// Changed the return type from void to int so the method can return the calculated with and added
		// applyWidth and headerOnly parameters so we can calculate the width without applying the width.
		//
		//internal void PerformAutoResizeHelper( RowsCollection rows, int nRows )
		// SSP 5/19/04 UWG2919
		// Added includeSummaryValues parameter.
		//
		//internal int PerformAutoResizeHelper( RowsCollection rows, int nRows, bool applyWidth, bool includeHeader, bool includeCells )
		internal int PerformAutoResizeHelper( RowsCollection rows, int nRows, bool applyWidth, 
			bool includeHeader, bool includeCells )
		{
			return this.PerformAutoResizeHelper( rows, nRows, applyWidth, includeHeader, includeCells, 0 );
		}

		// SSP 12/8/05 BR07665
		// Added an overload with maxRowsCollections parameter. This specifies the max number 
		// of data row collections from this band to traverse.
		// 
		internal int PerformAutoResizeHelper( RowsCollection rows, int nRows, bool applyWidth, 
			bool includeHeader, bool includeCells, int maxRowsCollections )
		{
			// JJD 1/16/02
			// Make sure we are vali and hooked up to a grid
			//
			if ( this.Disposed				||
				this.band == null			||
				this.Layout == null		|| 
				this.Layout.Disposed		||
				this.Layout.Grid == null	
				// SSP 7/15/03 UWG2354
				//
				//|| !this.Layout.Grid.Created )
				)
				return 0;

			// SSP 12/4/02 UWG1856
			// First, we should be using the ColScrollRegion's extent and not the RowScrollRegion's
			// Extent because RowScrollRegion.Extent retruns the height of the row scroll region. Here
			// we are trying to limit the column width to the col scroll region's width. Second. I
			// don't think it's necessary to have limit the column widths to the width of the row
			// scroll region. So just use some abtritrarily large number for the limit of the column's
			// width.
			//
			// use the ActiveRowScrollRegion's ClippedExtent as the max col width
			// allowed
			//
			//int maxColWidth = this.Layout.ActiveRowScrollRegion.ClippedExtent;	
			int maxColWidth = Math.Max( 10000, this.Layout.ActiveColScrollRegion.ClippedExtent );
	
			// Calculate the header's text width
			//
			// SSP 6/24/03 UWG2385
			// Added includeHeader parameter.
			//
			//int headerWidth = this.CalculateHeaderTextWidth( maxColWidth );			
			// MRS 4/14/05 - BR03343
			// If the header is not visible, don't do this
			//int headerWidth = ! includeHeader ? 0 : this.CalculateHeaderTextWidth( maxColWidth );			
			int headerWidth = 0;			
			if (includeHeader &&
				!this.Header.HiddenResolved)
			{
				headerWidth = this.CalculateHeaderTextWidth( maxColWidth );						
			}

			// Find the cell with the widest text
			//
			// SSP 4/14/03
			// Added overloads of PerformAutoResize that let the user specify the rows to base
			// the column resizing on.
			//
			//int maxWidth = this.CalculateMaxCellTextWidth( maxColWidth );		
			// SSP 6/24/03 UWG2385
			// Added includeCells parameter.
			//
			//int maxWidth = this.CalculateMaxCellTextWidth( maxColWidth, rows, nRows );
			// SSP 12/8/05 BR07665
			// Added an overload with maxRowsCollections parameter. This specifies the max number 
			// of data row collections from this band to traverse.
			// 
			//int maxWidth = ! includeCells ? 0 : this.CalculateMaxCellTextWidth( maxColWidth, rows, nRows );
			int maxWidth = ! includeCells ? 0 : this.CalculateMaxCellTextWidth( maxColWidth, rows, nRows, maxRowsCollections );

			// SSP 8/11/03 UWG2236
			// If the displat style of the button is to display an edit button, then add the width
			// of the edit button to the auto resize width.
			//
			if ( ColumnStyle.EditButton == this.Style )
				maxWidth += SystemInformation.VerticalScrollBarWidth;
			
			// Determine which is greater
			//
			maxWidth = Math.Max ( headerWidth, maxWidth );

			// SSP 10/28/03 UWG2309
			// I think we are adding 8 because of the how sometimes measurestring returns the incorrect
			// size. This was meant to compensate for that. However, with the new embeddable editors
			// architecture, the responsiblity falls upon the editors to do such padding (not all
			// editors deal with text). So changed the padding from 8 to 0.
			//
			//const int EXTRA_PADDING = 8;
			const int EXTRA_PADDING = 0;

			// SSP 6/18/04 UWG3216
			// What happens is that the row cell area ui element shrinks the first and the last
			// cell elements by the width of the row border if the row border is not mergeable.
			// This ends up causing the embeddable editor element's widths to be smaller.
			// Ideally we would like the row cell element to not shrink the cell elements
			// however fixing that requires a lot of changes. Safer thing is to simply add the
			// row border width if the column is the first or the last. Since we are adding the
			// row border width only to first and last column, if the user were to move an
			// auto-resized column that isn't the first or last column and make it the first or
			// last column then the row cell area element will end up shrinking that column's
			// cells by the border width even though the perform auto-resize didn't take into
			// account the row borders since when the auto-size was done the column wasn't the
			// first or the last. If this issue comes up then take out the checks for FirstItem
			// and LastItem below.
			//
			// ----------------------------------------------------------------------------------
			if ( ! this.Band.UseRowLayoutResolved && null != this.Band.Layout
				// SSP 2/16/05 BR02415
				// Also take into account the cell's border style.
				//
				//&& ! this.Band.Layout.CanMergeAdjacentBorders( this.Band.BorderStyleRowResolved ) 
				// SSP 4/20/05 BR03223
				// If the row and cell borders are dotted then in order for them to be drawn correctly we need
				// to not merge the borders. Instead we need to simply not draw the cell border that overlaps
				// with the row border. However we can not have the cell span over the row border otherwise
				// it will draw over the row border. So we always need to shrink the cell by the thickness
				// of the row borders regardless of where the row and cell borders are mergeable. Commented 
				// out the following condition that checks to see if the row and cell borders are mergeable. 
				//
				//&& ! this.Band.Layout.CanMergeAdjacentBorders( this.Band.BorderStyleRowResolved, this.Band.BorderStyleCellResolved )
				)
			{
				int rowBorderWidth = this.Band.Layout.GetBorderThickness( this.Band.BorderStyleRowResolved );

                // MRS 5/13/2008 - BR32739
                //if ( this.FirstItem )
                //    maxWidth += rowBorderWidth;

                //if ( this.LastItem )
                //    maxWidth += rowBorderWidth;

                bool accountForLeftBorder = this.FirstItem || this.Band.CardView;
                bool accountForRightBorder = this.LastItem || this.Band.CardView;

                maxWidth += accountForLeftBorder ? rowBorderWidth : 0;
                maxWidth += accountForRightBorder ? rowBorderWidth : 0;
			}
			// ----------------------------------------------------------------------------------

			// SSP 6/24/03 UWG2385
			// Changed the return type from void to int so the method can return the calculated with and added
			// applyWidth parameter so we can calculate the width without applying the width.
			//
			if ( ! applyWidth )
				return maxWidth + EXTRA_PADDING;

			// Set the width to the new value + 8 for padding
			// 
			this.Width = maxWidth + EXTRA_PADDING;

			// SSP 6/24/03 UWG2385
			// In row layout functionality, since we use the PreferredCellSize and the PreferredLabelSize
			// for the size of the headers and cells, also modify them. Also NOTE: We will make the width
			// and heights bigger but not smaller.
			//
			// ------------------------------------------------------------------------------------------
			if ( this.Band.UseRowLayoutResolved )
			{
				int newWidth = this.Width;

				Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutColumnInfo;
				// SSP 8/16/05 BR05387
				// Make it smaller if we have to.
				// 
				//if ( includeCells && ci.PreferredCellSize.Width < newWidth )
				if ( includeCells )
					ci.PreferredCellSize = new Size( newWidth, ci.PreferredCellSize.Height );

				// SSP 8/16/05 BR05387
				// Make it smaller if we have to.
				// 
				//if ( includeHeader && ci.PreferredLabelSize.Width < newWidth )
				if ( includeHeader )
					ci.PreferredLabelSize = new Size( newWidth, ci.PreferredLabelSize.Height );
			}
			// ------------------------------------------------------------------------------------------

			return maxWidth + EXTRA_PADDING;
		}

		// SSP 4/14/03
		// Added new implementation of PerformAutoResize above. Here is the old implementation commented out.
		//
		

		internal int CalculateHeaderTextWidth( int maxColWidth )
		{
			// MD 9/9/08 - TFS6590
			// Moved all code to the new overload
			return this.CalculateHeaderTextWidth( maxColWidth, false );
		}

		// MD 9/9/08 - TFS6590
		// Added a new overload to specify what gets measured.
		internal int CalculateHeaderTextWidth( int maxColWidth, bool includeOnlyText )
		{
			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			// create a graphics object
			//
			//Graphics gr = DrawUtility.CreateReferenceGraphics( this.Layout.Grid );
			// MRS 6/1/04 - UWG2915
			//Graphics gr = DrawUtility.GetCachedGraphics( this.Layout.Grid );
			Graphics gr = this.Layout.GetCachedGraphics();

			// Resolve the header to get to the font
			//
			AppearanceData appHeader = new AppearanceData();
			this.Header.ResolveAppearance( ref appHeader );

			// get the control's font
			//
			UltraGridLayout layout = this.Layout;
			Font font = layout.Grid.Font;	

			Font createdFont = null;

			if ( appHeader.HasFontData )
			{
				createdFont = appHeader.CreateFont( font );

				if ( createdFont != null )
					font = createdFont;
			}

			// MD 9/9/08 - TFS6590
			// Wrapped in a try...finally so we can be sure resources get cleaned up just in case we return early.
			try
			{

			// JAS 4/27/05 - Use the HeaderBorderThickness property in case the HeaderStyle does not use
			// the conventional UIElementBorderStyle settings.
			//
			//int borderThickness = this.Layout.GetBorderThickness( this.band.BorderStyleHeaderResolved );
			int borderThickness = this.Band.HeaderBorderThickness;
			
			// measure the header's caption
			//
            //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
			//int headerWidth = System.Drawing.Size.Ceiling(gr.MeasureString( this.Header.Caption, font, maxColWidth )).Width;
			// SSP 11/4/05 BR07555
			// Need to pass printing param when measuring in Whidbey. When printing,
			// in Whidbey we need use the GDI+ measuring.
			// 
			//int headerWidth = System.Drawing.Size.Ceiling(DrawUtility.MeasureString( gr, this.Header.Caption, font, maxColWidth )).Width;
			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// The header width measurement depends on the text rotation of the header.
			//int headerWidth = layout.MeasureString( gr, this.Header.Caption, font, maxColWidth ).Width;
			int headerWidth = layout.MeasureString( gr, this.Header.Caption, font, maxColWidth, null, this.Header.TextOrientationResolved ).Width;

			// MD 9/9/08 - TFS6590
			// If the text was the only thing that had to get measured, just bail out here.
			if ( includeOnlyText )
				return headerWidth;

            headerWidth += (2 * borderThickness);

			//if this column has a sort indicator include it in the width
			//
			// MD 9/23/08 - TFS6601
			// We might also need to leave room for the sort indicator if the column is sortable but not sorted.
			//if ( this.band.HasSortedColumns &&
			//     this.band.SortedColumns.Exists( this.Key ) )
			bool reserveSpaceForSortIndicator = false;
			switch ( this.ReserveSortIndicatorSpaceWhenAutoSizingResolved )
			{
				case ReserveSortIndicatorSpaceWhenAutoSizing.WhenVisible:
					reserveSpaceForSortIndicator = this.band.HasSortedColumns && this.band.SortedColumns.Exists( this.Key );
					break;

				case ReserveSortIndicatorSpaceWhenAutoSizing.WhenColumnIsSortable:
					reserveSpaceForSortIndicator = this.IsSortable;
					break;

				default:
					Debug.Fail( "Unknown ReserveSortIndicatorSpaceWhenAutoSizing value: " + this.ReserveSortIndicatorSpaceWhenAutoSizingResolved );
					break;
			}

			if ( reserveSpaceForSortIndicator )
			{
				headerWidth += Infragistics.Win.UltraWinGrid.SortIndicatorUIElement.SORTIND_WIDTH + 2;
			}

			if ( null != this.Header )
			{
				// SSP 3/29/02
				// If we are going to have a swap drop down button on the header, then
				// take into account that when calculating the required width of the
				// column header.
				//
				if ( this.Header.AllowSwapping )
					headerWidth += Infragistics.Win.UltraWinGrid.SwapButtonUIElement.SWAP_DROPDOWN_BUTTON_WIDTH;

				// If we are going to have a filter drop down button on the header, then
				// take into account that when calcuating the required width of the
				// column header.
				//
				// SSP 4/16/05 - NAS 5.2 Filter Row
				// Changed the name from AllowRowFiltering to HasFilterIcons.
				//
				//if ( null != this.Header && this.Header.AllowRowFiltering )
				if ( null != this.Header && this.Header.HasFilterIcons )
					headerWidth += Infragistics.Win.UltraWinGrid.FilterDropDownButtonUIElement.FILTER_DROPDOWN_BUTTON_WIDTH;

				// SSP 7/30/03 UWG2304
				// Take into account the row summaries.
				//
				if ( this.Header.AllowRowSummaries )
					headerWidth += RowSummariesButtonUIElement.ROW_SUMMARIES_BUTTON_WIDTH;
				
				// SSP 7/21/03
				// Add the width for the fixed header indicator.
				//
				if ( this.Band.UseFixedHeaders && FixedHeaderIndicator.None != this.Header.FixedHeaderIndicatorResolved )
					headerWidth += FixedHeaderIndicatorUIElement.FIXED_HEADER_INDICATOR_BUTTON_WIDTH;
			
				// MRS 10/19/04 - UWG2938
				// Account for the image in the header
				Image headerImage = appHeader.GetImage(this.Layout.Grid.ImageList);
				if (headerImage != null)
				{
					Size imageSize = headerImage.Size;

					// shrink the image size if necessary
					//
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( this.Header.Height > 2 && imageSize.Height > this.Header.Height - 2 )
					int headerHeight = this.Header.Height;

					if ( headerHeight > 2 && imageSize.Height > headerHeight - 2 )
					{
						// proportinally shrink the width
						//
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//imageSize.Width  = (int)((double)imageSize.Width * ( (double)(this.Header.Height - 2) / (double)imageSize.Height ));
						imageSize.Width = (int)( (double)imageSize.Width * ( (double)( headerHeight - 2 ) / (double)imageSize.Height ) );
					}

					// The HeaderUIElement positions the header one pixel to the right, so adjust.
					imageSize.Width += 1;

					headerWidth += imageSize.Width;

				}

                // CDS NAS v9.1 Header CheckBox
                if (this.ContainsVisibleHeaderCheckBoxOnLeftOrRight)
                    headerWidth += UIElementDrawParams.GetGlyphSize(GlyphType.CheckBox, this.Layout.UIElement).Width + HeaderCheckBoxUIElement.HEADERCHECKBOXPADDING * 2;

			}

			// MD 9/9/08 - TFS6590
			// Wrapped in a try...finally so we can be sure resources get cleaned up just in case we return early.
			// Also, moved the return statement from below into the try block.
			return headerWidth;
			}
			finally
			{
			// dispose of created font
			//
			if ( createdFont != null )
				createdFont.Dispose();

			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			//gr.Dispose();
			// MRS 6/1/04 - UWG2915
			//DrawUtility.ReleaseCachedGraphics(gr);
			this.Layout.ReleaseCachedGraphics(gr);
			}

			// MD 9/9/08 - TFS6590
			// Moved to the try block above.
			//return headerWidth;
		}


		#region CalculateCellTextWidth

		// SSP 4/14/03 
		// Added a helper method for calculating the cell text width.
		//
		private int CalculateCellTextWidth( UltraGridRow row, Graphics gr, StringFormat stringFormat, int maxColWidth )
		{
			// SSP 3/29/02 UWG1059
			// Skip the group by rows.
			//
			if ( row is UltraGridGroupByRow )
				return 0;

			// we are only interested in rows in this band
			//
			if ( row.Band != this.Band )
				return 0;
					
			int width = 0;

			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			EmbeddableEditorBase editor = this.GetEditor( row );
			//if ( null == this.Editor )
			if ( null == editor )
			{
				// if we have a drop down button we need to add that to the width
				//
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				// Use 0 for the button width because if there isn't an editor, the cell won't have
				// any drop down buttons.
				//
				//int buttonWidth = this.HasDropDown ? SystemInformation.VerticalScrollBarWidth : 0;
				int cellPadding = this.band.CellPaddingResolved;

				// get the control's font
				//
				Font font = this.Layout.Grid.Font;
				Font createdFont = null;

				// get the cell's text
				//
				string cellText = this.GetCellText( row );					
					
				// Resolve the cell to get the font and image
				//
				AppearanceData appCell = new AppearanceData();
				row.ResolveCellAppearance( this, ref appCell );

				if ( appCell.HasFontData )
				{
					createdFont = appCell.CreateFont( font );

					if ( createdFont != null )
						font = createdFont;
				}					
										
				// Measure the cell's text
				//
                //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
				//width = 2 * cellPadding + System.Drawing.Size.Ceiling(gr.MeasureString( cellText, font, maxColWidth, stringFormat )).Width;
				// SSP 11/4/05 BR07555
				// Need to pass printing param when measuring in Whidbey. When printing,
				// in Whidbey we need use the GDI+ measuring.
				// 
				//width = 2 * cellPadding + System.Drawing.Size.Ceiling(DrawUtility.MeasureString( gr, cellText, font, maxColWidth, stringFormat )).Width;
				width = 2 * cellPadding + this.Layout.MeasureString( gr, cellText, font, maxColWidth, stringFormat ).Width;

				// If we have an image, the widest it can be is the height of
				// the cell so just add the height to the width
				//
				if ( appCell.Image != null && 
					appCell.ImageHAlign != Infragistics.Win.HAlign.Center )
				{
					Rectangle imageRect = new Rectangle();
					row.GetCellRect( this, ref imageRect, ref appCell );
					width += imageRect.Height;
				}

				if ( null != createdFont )
					createdFont.Dispose( );
				createdFont = null;
			}
			else
			{
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//width = this.Editor.GetSize( this.EditorOwnerInfo, row, false, true, false ).Width;

				//	BF 10.21.04	UWG3454
				//
				//	Call the new overload which takes a height, so that editors that need to
				//	know the height (i.e., OptionSetEditor) will be able to return the correct
				//	size.
				//
				//width = editor.GetSize( this.EditorOwnerInfo, row, false, true, false ).Width;
				int height = row.HeightInsideRowBorders - row.AutoPreviewHeight;

				// SSP 4/18/05 - NAS 5.2 Filter Row
				// Use the new EditorOwnerInfo property of the cell because filter row cells 
				// use different owners.
				//
				//width = editor.GetSize( this.EditorOwnerInfo, row, false, true, false, 0, height ).Width;
				width = editor.GetSize( row.GetEditorOwnerInfo( this ), row, false, true, false, 0, height ).Width;
			}

			return width;
		}

		#endregion // CalculateCellTextWidth

		#region TraverseRowsCallback class definitition

		// MD 1/24/08
		// Made changes to allow for VS2008 style unit test accessors
		//private class TraverseRowsCallback : RowsCollection.IRowCallback
		private class TraverseRowsCallback : IRowCallback
			// SSP 12/8/05 BR07665
			// 
			, IRowsCollectionCallback
		{
			private UltraGridBand band = null;
			private UltraGridColumn column = null;
			private int nRows = 0;
			private int calculatedRowCount = 0;
			internal int width = 0;
			private int maxWidth = 0;
			private Graphics gr = null;
			private StringFormat stringFormat = null;
			// MRS 2/15/05 BR00166
			private Hashtable rowsCollectionSummaryWidthCalculated = new Hashtable();

			// SSP 12/8/05 BR07665
			// Added maxRowsCollections parameter. This specifies the max number of data row collections 
			// from this band to traverse.
			// 
			private int maxRowsCollections = 0;
			private int traversedRowsCollections = 0;
			private RowsCollection lastRowsCollection = null;

			internal TraverseRowsCallback( UltraGridColumn column, Graphics gr, StringFormat stringFormat, int nRows, int maxWidth 
				// SSP 12/8/05 BR07665
				// Added maxRowsCollections parameter. This specifies the max number of data row collections 
				// from this band to traverse.
				// 
				, int maxRowsCollections
				) 
			{
				this.nRows = nRows;
				this.calculatedRowCount = 0;
				this.band = column.Band;
				this.column = column;
				this.width = 0;
				this.maxWidth = maxWidth;
				this.gr = gr;
				this.stringFormat = stringFormat;

				// SSP 12/8/05 BR07665
				// Added maxRowsCollections parameter. This specifies the max number of data row collections 
				// from this band to traverse.
				// 
				this.maxRowsCollections = maxRowsCollections;
			}

			// SSP 12/8/05 BR07665
			// 
			bool IRowsCollectionCallback.ProcessRowsCollection( RowsCollection rows )
			{
				if ( null == this.lastRowsCollection || ! rows.IsTrivialDescendantOf( this.lastRowsCollection ) )
					this.traversedRowsCollections++;

				this.lastRowsCollection = rows;

				if ( 0 != this.maxRowsCollections && this.traversedRowsCollections > this.maxRowsCollections )
					return false;

				return true;
			}

			// MD 1/24/08
			// Made changes to allow for VS2008 style unit test accessors
			//bool RowsCollection.IRowCallback.ProcessRow( UltraGridRow row )
			bool IRowCallback.ProcessRow( UltraGridRow row )
			{
				Debug.Assert( row.Band == this.band );

				// skip the group by rows
				//
				if ( row is UltraGridGroupByRow )
					return true;

				this.calculatedRowCount++;

				this.width = Math.Max( this.width, column.CalculateCellTextWidth( row, gr, stringFormat, maxWidth ) );

				// MRS 6/2/04 UWG2919
				// If we are sizing all rows, then include summaries, too. 
				// Only calculate this for the first row in the rows collection. 
				// MRS 2/15/05 BR00166
				//if ( this.nRows == 0 && row == row.ParentCollection[0] )					
				if ( this.nRows == 0 && 
					!this.rowsCollectionSummaryWidthCalculated.ContainsKey(row.ParentCollection) )
				{
					this.rowsCollectionSummaryWidthCalculated.Add(row.ParentCollection, DBNull.Value);					

					this.width = Math.Max( this.width, column.CalculateMaxSummaryTextWidth( maxWidth, row ));
				}

				if ( this.maxWidth > 0 && this.width > this.maxWidth )
				{
					this.width = this.maxWidth;
					return false;
				}

				// Remember 0 means all the rows.
				//
				if ( 0 != this.nRows && this.calculatedRowCount >= this.nRows )
					return false;

				return true;
			}
		}

		#endregion // End of TraverseRowsCallback calss definitition

		// MRS 6/2/04 UWG2919
		#region CalculateSummaryTextWidth		
		internal int CalculateSummaryTextWidth( int maxColWidth, SummaryValue summaryValue )
		{
			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			// create a graphics object
			//
			//Graphics gr = DrawUtility.CreateReferenceGraphics( this.Layout.Grid );
			// SSP 11/20/06 BR15797
			// When printing get the print graphics.
			// 
			//Graphics gr = DrawUtility.GetCachedGraphics( this.Layout.Grid );
			Graphics gr = this.Layout.GetCachedGraphics( );

			// Resolve the summaryValue to get to the font
			//
			AppearanceData appSummary = new AppearanceData();
			AppearancePropFlags flags = AppearancePropFlags.FontData;
			summaryValue.ResolveAppearance( ref appSummary, ref flags);

			// get the control's font
			//
			Font font = this.Layout.Grid.Font;	

			Font createdFont = null;

			if ( appSummary.HasFontData )
			{
				createdFont = appSummary.CreateFont( font );

				if ( createdFont != null )
					font = createdFont;
			}

			int borderThickness = this.Layout.GetBorderThickness( summaryValue.BorderStyleResolved );
			
			// measure the SummaryValue's caption
			//
            //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
			//int summaryValueWidth = System.Drawing.Size.Ceiling(gr.MeasureString( summaryValue.SummaryText , font, maxColWidth )).Width;
			// SSP 11/4/05 BR07555
			// Need to pass printing param when measuring in Whidbey. When printing,
			// in Whidbey we need use the GDI+ measuring.
			// 
			//int summaryValueWidth = System.Drawing.Size.Ceiling(DrawUtility.MeasureString( gr, summaryValue.SummaryText , font, maxColWidth )).Width;
			int summaryValueWidth = this.Layout.MeasureString( gr, summaryValue.SummaryText , font, maxColWidth ).Width;

			summaryValueWidth += (2 * borderThickness);
						
			// dispose of created font
			//
			if ( createdFont != null )
				createdFont.Dispose();

			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			//gr.Dispose();
			// SSP 11/20/06 BR15797
			// When printing get the print graphics.
			// 
			//DrawUtility.ReleaseCachedGraphics(gr);
			this.Layout.ReleaseCachedGraphics( gr );

			return summaryValueWidth;
		}


		#endregion CalculateSummaryTextWidth

		// MRS 6/2/04 UWG2919
		#region CalculateMaxSummaryTextWidth
	
		internal int CalculateMaxSummaryTextWidth( int maxColWidth , UltraGridRow row)
		{
			int i = 0;
			int summaryValueWidth = 0;       
            // MRS 3/20/2009 - TFS15313          
			//SummaryValue summaryValue = row.ParentCollection.SummaryValues.GetSummaryValueFromPosition(SummaryPosition.UseSummaryPositionColumn, this, i);           
            SummaryValue summaryValue = row.ParentCollection.SummaryValues.GetSummaryValueFromPosition(SummaryPosition.UseSummaryPositionColumn, this, i, SummaryDisplayAreas.Bottom | SummaryDisplayAreas.BottomFixed | SummaryDisplayAreas.GroupByRowsFooter | SummaryDisplayAreas.InGroupByRows | SummaryDisplayAreas.RootRowsFootersOnly | SummaryDisplayAreas.Top | SummaryDisplayAreas.TopFixed);

			while ( summaryValue != null )
			{
				summaryValueWidth = Math.Max(summaryValueWidth, this.CalculateSummaryTextWidth(maxColWidth, summaryValue));
				i++;
				summaryValue = row.ParentCollection.SummaryValues.GetSummaryValueFromPosition(SummaryPosition.UseSummaryPositionColumn, this, i);
			}

			return summaryValueWidth;
		}


		#endregion // CalculateMaxSummaryTextWidth

		#region CalculateMaxCellTextWidth

		// SSP 4/14/03
		// Added an overload of PerformResize that takes an int specifying the number of rows to base the auto-resize calculations on.
		// Added nRows parameter to the CalculateMaxCellTextWidth method as a result
		//
		// SSP 12/8/05 BR07665
		// Added maxRowsCollections parameter. This specifies the max number of data row collections 
		// from this band to traverse.
		// 
		//internal int CalculateMaxCellTextWidth( int maxColWidth, RowsCollection rows, int nRows )
		internal int CalculateMaxCellTextWidth( int maxColWidth, RowsCollection rows, int nRows, int maxRowsCollections )
		{
			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			DrawUtility.BeginGraphicsCaching();
			Graphics gr = null;

			// AS 8/12/03
			// Moved from inside the try finally to here.
			int width = 0;

			try
			{
				// AS 8/12/03 optimization - Use the new caching mechanism.
				//
				// create a graphics object
				//
				//Graphics gr = DrawUtility.CreateReferenceGraphics( this.Layout.Grid );	
				// MRS 6/1/04 - UWG2915
				//gr = DrawUtility.GetCachedGraphics( this.Layout.Grid );	
				gr = this.Layout.GetCachedGraphics();

				// get the control's font
				//
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//Font font = this.Layout.Grid.Font;	
		
				StringFormatFlags strfmtflag = 0;

				//if not multiLine set format flag to 'NoWrap'
				//
				if( !this.IsCellMultiLine ) 
					strfmtflag = StringFormatFlags.NoWrap;

				//initialize StringFormat object with flag
				//
				StringFormat stringformat = new StringFormat(strfmtflag);	
		
				// SSP 7/1/05 BR04690
				// Apparently the code in this method assumes cellSpacing var comprises the left and
				// right cell spacing.
				// 
				//int cellSpacing = this.band.CellSpacingResolved;
				int cellSpacing = 2 * this.band.CellSpacingResolved;

				int borderThickness = this.Layout.GetBorderThickness( this.band.BorderStyleCellResolved );

				// if we have a drop down button we need to add that to
				// the width
				//
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//int buttonWidth = this.HasDropDown ? SystemInformation.VerticalScrollBarWidth : 0;
			
				// SSP 4/14/03
				// Commented out the old code here and added the new code that calcualtes the column width
				// based on the number of rows specified by the nRows parameter.
				//
				// loop over each rowscrollRegion's visible rows
				// and find the widest string
				//
				int maxWidth = maxColWidth - cellSpacing + ( 2 * borderThickness );

				// AS 8/12/03 optimization
				// Since we need a try finally and the last thing we do is
				// return the width, I need to move this outside the try/finally.
				// Moved to the beginning of the routine.
				//
				//int width = 0;

				if ( nRows < 0 )
				{				
					for ( int i = 0; i < this.Layout.RowScrollRegions.Count; ++i)
					{
						RowScrollRegion rsr = this.Layout.RowScrollRegions[i];

						VisibleRowsCollection visibleRows = rsr.VisibleRows;
						for ( int j = 0; j < visibleRows.Count; ++j )
						{
							//MRS 6/2/04 UWG2919 - BEGIN
							//width = Math.Max( width, this.CalculateCellTextWidth( rsr.VisibleRows[j].Row, gr, stringformat, maxWidth ) );
							UltraGridRow row = visibleRows[j].Row;

							// SSP 4/16/05 - NAS 5.2 Summaries Extention
							// Skip summary rows.
							//
							if ( row.IsSummaryRow )
								continue;

							width = Math.Max( width, this.CalculateCellTextWidth( row, gr, stringformat, maxWidth ) );

							// MRS 2/15/05 BR00166
							//if ( row == row.ParentCollection[row.ParentCollection.Count -1] )
							if ( row == row.ParentCollection.GetLastVisibleRow() )
							{
								width = Math.Max(width, this.CalculateMaxSummaryTextWidth ( maxWidth, row ) );
							}
							//MRS 6/2/04 UWG2919 - END
							
							if ( width > maxWidth )
							{
								width = maxWidth;
								break;
							}
						}
					}

					width = Math.Min( maxColWidth, width + cellSpacing + ( 2 * borderThickness ) );
				}
				else 
				{
					UltraGridColumn.TraverseRowsCallback c = new UltraGridColumn.TraverseRowsCallback( 
						this, gr, stringformat, nRows, maxWidth 
						// SSP 12/8/05 BR07665
						// Added maxRowsCollections parameter. This specifies the max number of data row collections 
						// from this band to traverse.
						// 
						, maxRowsCollections
						);

					// SSP 12/8/05 BR07665
					// Pass in the rowsCollectionCallback parameter as well.
					// 
					// --------------------------------------------------------------------------
					
					RowsCollection tmpRows = null != rows ? rows : this.Layout.Rows;
					tmpRows.InternalTraverseRowsHelper( this.Band, c, c, true, true );
					// --------------------------------------------------------------------------

					width = Math.Min( maxColWidth, c.width + cellSpacing + ( 2 * borderThickness ) );
				}

				if ( null != stringformat )
					stringformat.Dispose( );
				stringformat = null;
			}
			finally
			{
				if ( null != gr )
				{
					// AS 8/12/03 optimization - Use the new caching mechanism.
					//
					//gr.Dispose();
					// MRS 6/1/04 - UWG2915
					//DrawUtility.ReleaseCachedGraphics(gr);
					this.Layout.ReleaseCachedGraphics( gr );
				}

				gr = null;

				// AS 8/12/03 optimization - Use the new caching mechanism.
				//
				DrawUtility.EndGraphicsCaching();
			}


			return width;
		}

		#endregion // CalculateMaxCellTextWidth

		// SSP 4/14/03
		// Added new implementation of CalculateMaxCellTextWidth. Here is the old implementation
		// commented out.
		//
		

		internal bool ShouldCellValueBeDisplayedInCard( Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			// SSP 6/27/02
			// Compressed card view.
			// In the case of compressed view style, if the card is compressed, then
			// return false so the cells don't get displayed. Othwerise return true.
			//
			if ( null != row && row.IsCardStyleCompressed )
				return !row.IsCardCompressed;

			// SSP 2/28/03 - Row Layout Functionality
			// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
			// to StandardLabels card style so use the StyleResolved instead of the Style property.
			//
			if (this.Band.CardSettings.StyleResolved != CardStyle.VariableHeight)
				return true;

			// JM 01-25-02
			Debug.Assert(row != null, "Null row passed to ShouldCellValueBeDisplayedInCard");
			if (row == null)
				return false;

			// SSP 8/29/04 UWG3610
			// Don't hide cells in an add-row because otherwise the user can't enter any data
			// into new rows since typically cells in new rows are empty.
			//
			if ( row.IsAddRow || row.IsAddRowFromTemplate || row.IsTemplateAddRow )
				return true;

			// SSP 7/10/03
			// Commented out UltraGridColumn.GetCellValue because it does the same thing
			// as UltraGridRow.GetCellValue. We don't need both of these.
			//
			//object dataValue = this.GetCellValue( row );
			object dataValue = row.GetCellValue( this );

			if ( dataValue == null || dataValue is DBNull )
				return false;

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( dataValue is string )
			//    return ((string)dataValue).Trim().Length > 0;
			string strDataValue = dataValue as string;

			if ( strDataValue != null )
				return strDataValue.Trim().Length > 0;

			if ( dataValue is int )
				return ((int)dataValue)!= 0;

			if ( dataValue is uint )
				return ((uint)dataValue)!= 0;

			if ( dataValue is short )
				return ((short)dataValue)!= 0;

			if ( dataValue is ushort )
				return ((ushort)dataValue)!= 0;

			if ( dataValue is long )
				return ((long)dataValue)!= 0;

			if ( dataValue is ulong )
				return ((ulong)dataValue)!= 0;

			if ( dataValue is decimal )
				return ((decimal)dataValue)!= 0.0m;

			if ( dataValue is float )
				return ((float)dataValue)!= 0.0f;

			if ( dataValue is double )
				return ((double)dataValue)!= 0.0d;

			if ( dataValue is byte )
				return ((byte)dataValue)!= 0;

			if ( dataValue is sbyte )
				return ((sbyte)dataValue)!= 0;

			if ( dataValue is char )
				return ((char)dataValue)!= 0;

			try
			{
				return dataValue.ToString().Trim().Length > 0;
			}
			catch
			{
				return false;
			}
		}

		// SSP 7/10/03
		// Commented out UltraGridColumn.GetCellValue because it does the same thing
		// as UltraGridRow.GetCellValue. We don't need both of these.
		//
		

		// SSP 5/1/03
		// Not needed any more. Commenting it out.
		//
		

		#region ApplyDataFilter

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations Support
		// 
		internal object ApplyDataFilter( UltraGridRow row, object value, 
			ConversionDirection direction, out string error )
		{
			error = null;

			// Get the editor and the owner.
			//
			EmbeddableEditorBase editor = row.GetEditorResolved( this );
			EmbeddableEditorOwnerBase owner = row.GetEditorOwnerInfo( this );

			// Use row as ownerContext.
			// 
			object ownerContext = row;

			return null != editor 
				? this.ApplyDataFilter( editor, owner, ownerContext, value, direction, out error )
				: value;
		}

		internal object ApplyDataFilter( EmbeddableEditorBase editor, EmbeddableEditorOwnerBase owner, 
			object ownerContext, object value, ConversionDirection direction, out string error )
		{
			error = null;
			bool isValidConversion;

			object convertedVal = editor.GetDataFilteredDestinationValue( value, direction, 
				out isValidConversion, owner, ownerContext, out error );

			if ( ! isValidConversion )
			{
				if ( null == error || 0 == error.Length )
					error = SR.GetString( "DataErrorCellUpdateUnableToConvert", 
						( null != value ? value.GetType( ).Name : "null" ), this.DataType );

				if ( null == error )
					error = "Error converting.";

				convertedVal = null;
			}

			return convertedVal;
		}

		#endregion // ApplyDataFilter

		#region ConvertDisplayTextToDataValue

		// SSP 11/22/05 - NAS 6.1 Multi-cell Operations
		// Added ConvertDisplayTextToDataValue method.
		// 
		internal object ConvertDisplayTextToDataValue( UltraGridRow row, string displayText, out string error )
		{
			// First apply the DisplayToEditor conversion.
			// 
			object val = this.ApplyDataFilter( row, displayText, ConversionDirection.DisplayToEditor, out error );

			// Then apply the EditorToOwner conversion.
			// 
			if ( GridUtils.IsEmpty( error ) )
				val = this.ApplyDataFilter( row, val, ConversionDirection.EditorToOwner, out error );

			// Then convert the value to the data type of the column.
			// 
			if ( null != val && GridUtils.IsEmpty( error ) )
			{
				val = this.ConvertValueToDataType( val );

				// If the conversion fails, return error.
				// 
				if ( null == val )
					error = SR.GetString( "DataErrorCellUpdateUnableToConvert", "String", this.DataType );
			}

			if ( GridUtils.IsEmpty( error ) )
				error = null;
			else
				val = null;

			return val;
		}

		#endregion // ConvertDisplayTextToDataValue

		// MRS 1/4/2008 - BR29253
        // I removed this method, because it was only being used from one place (GroupByRow.GetDisplaytext) and 
        // this method does not account for the Format property of the column, which is wrong, since
        // the GroupByRow SHOULD be accounting for the format. 
        // Use GetCellText on the row, instead - it accounts for Format. 
		#region ConvertDataValueToDisplayText (Commented Out)

        

        #endregion // ConvertDataValueToDisplayText  (Commented Out)

        // SSP 4/20/05 - NAS 5.2 Filter Row
		// Added GetCellText method.
		//
		/// <summary>
		/// Returns the text that would be displayed by the editor in the cell if it were
		/// provided the specified cellVal. This method handles null as well as DBNull values.
		/// </summary>
		/// <param name="row">The UltraGridRow used to determine the editor for the cell.</param>
		/// <param name="cellVal">The cell value to be converted into a display text.</param>
		/// <returns></returns>
        // MRS v7.2 - Document Exporter
		//internal string GetCellText( UltraGridRow row, object cellVal )
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string GetCellText(UltraGridRow row, object cellVal)
		{
			GridEmbeddableEditorOwnerInfoBase cellEditorOwnerInfo = row.GetEditorOwnerInfo( this );
			string displayText = null;

			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//if ( null != this.Editor )
			//	displayText = this.Editor.DataValueToText( cellVal, this.EditorOwnerInfo, row );
			EmbeddableEditorBase editor = this.GetEditor( row );
			if ( null != editor )
			{
				// ZS 2/13/04 - UWG2959 
				// displayText = editor.DataValueToText( cellVal, this.EditorOwnerInfo, row );

				object filteredEV = editor.GetDataFilteredDestinationValue(cellVal, 
					// SSP 4/18/05 - NAS 5.2 Filter Row
					// Use the new EditorOwnerInfo property of the cell because filter row cells 
					// use different owners.
					//
					//ConversionDirection.OwnerToEditor, this.EditorOwnerInfo, row);
					ConversionDirection.OwnerToEditor, cellEditorOwnerInfo, row);

				//	BF 10.22.04	UWG3731
				//	This is unnecessary and does not affect UWG2959
				//	object filteredDV = editor.GetDataFilteredDestinationValue(filteredEV, 
				//		ConversionDirection.EditorToDisplay, this.EditorOwnerInfo, row);
				//
				//	if(filteredDV is string)
				//		// If filtered value is of type string, this is obviously the text which represents
				//		// cell value.
				//		displayText = (string) filteredDV;
				//	else
				//		// If filtered value is not of type string, it must be the special case of editor 
				//		// which doesn't have Editor to Display conversion at all (ProgressBar, CheckEditor,
				//		// EditorWithMask etc.) so it returns editor value back (without any filtering). In 
				//		// that case we will use default DataValueToText(...) conversion.

				// SSP 10/19/05 BR06887
				// Unlike comments above, it IS necessary to perform EditorToDisplay conversion.
				// The DataValueToText doesn't do EditorToDisplay conversions. Someone needs to
				// perform that conversion.
				// 
				// ------------------------------------------------------------------------------
				if ( null != editor.DataFilter )
				{
					bool isValidConversion;
					string validMessage;
					object filterDV = editor.GetDataFilteredDestinationValue( 
						filteredEV,
						ConversionDirection.EditorToDisplay,
						out isValidConversion,
						cellEditorOwnerInfo,
						row,
						out validMessage,
                        // MRS 4/9/2008 - BR31859
                        // I'm not sure why we are passing in False here. There is no other place I can find anywhere that we call this method and pass in false to not perform the automatic conversion. If the DataFilter doesn't do anything, then we want to autoconvert. 
                        //false,
                        true,
						false );

					// MD 8/7/07 - 7.3 Performance
					// FxCop - Do not cast unnecessarily
					//if ( isValidConversion && filterDV is string )
					//    return (string)filterDV;
					if ( isValidConversion )
					{
						string filterDVStr = filterDV as string;

						if ( filterDVStr != null )
							return filterDVStr;
					}
				}
				// ------------------------------------------------------------------------------

				// SSP 4/18/05 - NAS 5.2 Filter Row
				// Use the new EditorOwnerInfo property of the cell because filter row cells 
				// use different owners.
				//
				//displayText = editor.DataValueToText( filteredEV, this.EditorOwnerInfo, row );
				displayText = editor.DataValueToText( filteredEV, cellEditorOwnerInfo, row );

                // MRS 8/14/2009 - TFS20291
                // The cell text should return what is displayed on the screen. This includes the 
                // passwordchar or any character casing.                 
                //
                if (!EditorWithText.ApplyPasswordChar(cellEditorOwnerInfo, row, ref displayText))
                    EditorWithText.ApplyCharacterCasing(cellEditorOwnerInfo, row, ref displayText);
			}

			if ( null == displayText )
			{
				Debug.Assert( false, "Editor returned null display text !" );

				displayText = null != cellVal ? this.FormatDisplayText( cellVal ) : null;
			}		

			return null != displayText ? displayText : this.NullTextResolved;
		}

		internal string GetCellText( Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			Debug.Assert( null != row, "passed in row parameter null in Column.GetCellText" );
			
			if ( null == row )
				return string.Empty;

			// SSP 4/18/05 - NAS 5.2 Filter Row
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//GridEmbeddableEditorOwnerInfoBase cellEditorOwnerInfo = row.GetEditorOwnerInfo( this );

			// SSP 7/14/04 - UltraCalc
			// If the column has a formula then return the formula evaluation result. Following
			// code also takes care of the formula errors.
			//
			// --------------------------------------------------------------------------------
			// SSP 4/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			// Use the new IsDataRow which checks for IsTemplateAddRow as well as other special
			// rows like the filter row.
			//
			//if ( this.HasActiveFormula && ! row.IsTemplateAddRow )
			if ( this.HasActiveFormula && row.IsDataRow )
			{
				UltraGridCell cell = row.Cells[ this ];
				bool isError;
				object val = RefUtils.GetFormulaCalcValue( cell.CalcReference, out isError, this.FormatInfo );

				// SSP 8/23/04
				// Apply the format settings to the value.
				//
				if ( ! isError )
				{
					// SSP 4/20/05 - NAS 5.2 Filter Row
					// Use the new GetCellText overload that takes in the cell value. That also
					// takes into account any data filter on the editor which the following code
					// doesn't take into account.
					//
					
					return this.GetCellText( row, val );
				}

				return null == val || DBNull.Value == val ? this.NullTextResolved : val.ToString( );
			}
			// --------------------------------------------------------------------------------

			// SSP 7/10/03
			// Commented out UltraGridColumn.GetCellValue because it does the same thing
			// as UltraGridRow.GetCellValue. We don't need both of these.
			//
			//object cellVal = this.GetCellValue( row );
			object cellVal = row.GetCellValue( this );

			// SSP 4/20/05 - NAS 5.2 Filter Row
			// Moved the following code into the new GetCellText overload that takes in the
			// cell value. Commented out the original code.
			//
			// --------------------------------------------------------------------------------
			return this.GetCellText( row, cellVal );
			
			// --------------------------------------------------------------------------------
		}

		// SSP 8/2/02 
		// EM Embeddable editors.
		//
		

		private bool IsDataFormattingStipulated
		{
			get
			{
				// JJD 1/10/02
				// If formatinfo was set then return true
				//
				if ( this.formatInfo != null )
					return true;

				return null != this.format && this.format.Length > 0;
			}
		}

		private string FormatDisplayText( object dataValue )
		{

			// AS - 11/7/01
			// Wrapped this series of ToString's in a try/catch since the use
			// could accidentally provide an invalid format and we should not
			// be blowing. We'll fall back to the ToString of the datavalue
			// if an exception occurs.
			//
			try
			{
				// JJD 1/10/02
				// if a format string or info wasn't supplied and this is not
				// a date then just return ToString()
				//
				if ((this.format == null || this.format.Length < 1) && 
					this.formatInfo == null &&
					!Object.ReferenceEquals( dataValue.GetType(), typeof(System.DateTime) ) )
				{
					return dataValue.ToString();
				}

				// format a string
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(string) ) )
				{
					if ( this.formatInfo != null )
						return ((string)dataValue).ToString( this.formatInfo );

					return dataValue as string;
				}

				// format an int
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(int) ) )
					return ((int)dataValue).ToString( this.format, this.formatInfo );

				// format a datetime
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(System.DateTime) ) )
				{
					// JJD 1/10/02
					// If a format string wasn't supplied then use the short date pattern
					// by default. 
					//
					if (this.format == null || this.format.Length < 1)
					{
						// Try to get the CultureInfo from the formatinfo
						//
						System.Globalization.CultureInfo ci = this.formatInfo as System.Globalization.CultureInfo;

						// If not use the current culture
						//
						if ( ci == null )
							// SSP 4/17/02
							// Use CurrentCulture instead of CurrentUICulture.
							//
							//ci = System.Globalization.CultureInfo.CurrentUICulture;
							ci = System.Globalization.CultureInfo.CurrentCulture;

						return ((System.DateTime)dataValue).ToString(ci.DateTimeFormat.ShortDatePattern, this.formatInfo);
					}

					return ((System.DateTime)dataValue).ToString(this.format, this.formatInfo);
				}

				// format a day of week
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(System.DayOfWeek) ) )				
                    return ((System.DayOfWeek)dataValue).ToString(this.format);

                // format an unsigned int
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(uint) ) )				
					return ((uint)dataValue).ToString(this.format, this.formatInfo);

				// format a long
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(long) ) )				
					return ((long)dataValue).ToString(this.format, this.formatInfo);

				// format an unsigned long
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(ulong) ) )				
					return ((ulong)dataValue).ToString(this.format, this.formatInfo);

				// format a short
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(short) ) )				
					return ((short)dataValue).ToString(this.format, this.formatInfo);

				// format an unsigned short
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(ushort) ) )				
					return ((ushort)dataValue).ToString(this.format, this.formatInfo);

				// format a double
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(double) ) )				
					return ((double)dataValue).ToString(this.format, this.formatInfo);

				// format a float
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(float) ) )				
					return ((float)dataValue).ToString(this.format, this.formatInfo);

				// format a decimal
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(decimal) ) )				
					return ((decimal)dataValue).ToString(this.format, this.formatInfo);

				// format a byte
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(byte) ) )				
					return ((byte)dataValue).ToString(this.format, this.formatInfo);

				// format a sbyte
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(sbyte) ) )				
					return ((sbyte)dataValue).ToString(this.format, this.formatInfo);

				// format a Guid
				//
				if ( Object.ReferenceEquals( dataValue.GetType(), typeof(System.Guid) ) )				
					return ((System.Guid)dataValue).ToString(this.format, this.formatInfo);

				// SSP 5/7/02
				// Added support for System.Drawing.Color
				//
				if ( typeof( System.Drawing.Color ) == dataValue.GetType( ) )
					return ((System.Drawing.Color)dataValue).ToString( );

				// SSP 7/9/02
				// Since the bool doesn't have an overload that takes in 
				// a format string, we had skipped it here. But I think
				// we should still call it and use the overload that takes
				// in a format provider.
				//
				if ( typeof( bool ) == dataValue.GetType( ) )
					return ((bool)dataValue).ToString( this.formatInfo );
			}
			catch (Exception)
			{
			}

			// default to calling tostring without a format
			//
			return dataValue.ToString();
		}

		// SSP 5/28/02
		// EM Embeddable editors. Not needed anymore.
		//
		
		// SSP 4/26/02
		// EM Embeddable editors.
		// Commented out the ShouldDropDownBtnBeDisplayed because
		// embeddable editors should take care of displaying any 
		// drop down buttons.
		//
		

		// SSP 5/1/03 - Cell Level Editor
		// Added Editor and ValueList properties off the cell so the user can set the editor
		// and value list on a per cell basis.
		// Changed DefaultHorizontalTextAlignment to a method.
		//
		//internal HAlign DefaultHorizontalTextAlignment
		internal HAlign GetDefaultHorizontalTextAlignment( UltraGridRow row )
		{ 
			// If column has a valuelist then don't right align by default
			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//if ( !this.HasValueList )
			IValueList valueList = this.GetValueList( row );				
			if ( null == valueList )
			{
				if ( 
					typeof( System.Decimal   )  ==  this.DataType  ||
					typeof( System.Double    )  ==  this.DataType  ||
					typeof( System.Int16     )  ==  this.DataType  ||
					typeof( System.Int32     )  ==  this.DataType  ||
					typeof( System.Int64     )  ==  this.DataType  ||
					typeof( System.Single    )  ==  this.DataType  ||
					typeof( System.Single    )  ==  this.DataType  ||
					typeof( System.SByte     )  ==  this.DataType    
					)
					return HAlign.Right;					
					
			}
		
			return HAlign.Left; 
		}

		internal bool ShouldBtnElementSpaceBeReserved(
			UltraGridRow row,
			ref AppearanceData appResolved,
			RowScrollRegion rowScrollRegion, 
			ColScrollRegion colScrollRegion )
		{
			// If the column is not a edit button or a dropdown then return false
			//
			// We never want to reserve space for an edit btn if the style 
			// is ssStyleButton
			//
			// SSP 7/18/03 - Cell level style property
			//
			//if ( !this.IsColumnStyleEditButton )
			if ( !this.IsColumnStyleEditButton( row ) )
				return false;

			// If we are going to create the element we need to reserve space
			// for it
			//
			if ( this.ShouldBtnElementBeCreated( row, rowScrollRegion, colScrollRegion ) )
			{
				return true;
			}


			HAlign horizAlign = appResolved.TextHAlign;

			if ( HAlign.Default == horizAlign )
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//horizAlign = this.DefaultHorizontalTextAlignment;
				horizAlign = this.GetDefaultHorizontalTextAlignment( row );

			// We also need to reserve space for right and center aligned text
			// so it doesn't shift when the button evetually gets displayed
			//
			switch ( horizAlign )
			{
				case HAlign.Center:
				case HAlign.Right:
					return true;
			}
		
			// We also need to reserve space for multi line text
			// so it doesn't shift when the button evetually gets displayed
			//
			return this.IsCellMultiLine;
		}


		// SSP 8/15/02 UWG1561
		// Moved IsCursorOverCell to here from UltraGridCell because we don't always have a 
		// cell (since we create cells lazily we don't want to create a cell just to check if 
		// the mouse is over the cell).
		//
		internal bool IsCursorOverCell( UltraGridRow row ) 
		{
			return this.IsCursorOverCell( row, null, null );
		}

		internal bool IsCursorOverCell( 
			UltraGridRow row,
			RowScrollRegion rowScrollRegion,
			ColScrollRegion colScrollRegion ) 
		{
			Point point = System.Windows.Forms.Cursor.Position;	
			
			point = this.Layout.Grid.PointToClient( point );

			if ( null == rowScrollRegion )
				rowScrollRegion = this.Layout.ActiveRowScrollRegion;

			if ( null == colScrollRegion )
				colScrollRegion = this.Layout.ActiveColScrollRegion;
			
			// Use passed in scrolling regions 
			//
			UIElement mainElem = this.Layout.GetUIElement( false, false );

			if ( null != mainElem )
			{	
				UIElement elementFromPoint = mainElem.ElementFromPoint( point );

				if ( null != elementFromPoint && ! ( elementFromPoint is CellUIElementBase ) )
					elementFromPoint = elementFromPoint.GetAncestor( typeof( CellUIElementBase ) );
				
				CellUIElementBase cellElem = elementFromPoint as CellUIElementBase;

				if ( null != cellElem && 
					this == cellElem.Column && row == cellElem.Row &&
					cellElem.ClipRect.Contains( point ) )
				{
					// If scroll regions were specified, then make sure they match.
					//
					RowColRegionIntersectionUIElement rsrCsrIntersectionElem = (RowColRegionIntersectionUIElement)cellElem.GetAncestor( typeof( RowColRegionIntersectionUIElement ) );

					if ( null != rsrCsrIntersectionElem && 
						colScrollRegion == rsrCsrIntersectionElem.ColScrollRegion &&
						rowScrollRegion == rsrCsrIntersectionElem.RowScrollRegion )
						return true;
				}

				// SSP 8/15/02
				// This doesn't seem to get the ancestore so if the mouse is over for
				// example the text element in the cell, then it would not work 
				// because the subElement would not be a cell element.
				// So commented this out and added above code.
				//
				
			}

			return false;
		}
		
		internal bool ShouldBtnElementBeCreated(
			UltraGridRow row,
			RowScrollRegion rowScrollRegion,
			ColScrollRegion colScrollRegion )
		{
			// AS - 11/7/01
			// Why shouldn't we display buttons at design time? The user
			// should see the grid as close to how it will appear at runtime
			// as possible.
			//
			// If we are in design mode we never want to show the btn elements
			//
			//if ( this.Layout.Grid.DesignMode )
			//	return false;

			// If the column is not a edit button or a dropdown then return false
			//
			// SSP 7/18/03 - Cell level style property
			//
			//if ( !this.IsColumnStyleButton && !this.IsColumnStyleEditButton )
			if ( !this.IsColumnStyleButton( row ) && !this.IsColumnStyleEditButton( row ) )
				return false;

			// Set a flag if both regions are active
			//
			bool activeRegion = ( rowScrollRegion == this.Layout.ActiveRowScrollRegion &&
				colScrollRegion == this.Layout.GetActiveColScrollRegion(false) );

			// If this is the active cell then always return true
			//
			if ( activeRegion && row.Cells.HasCell( this ) && row.Cells[ this ] == this.Layout.ActiveCell )
				return true;

			bool visible = false;

			switch ( this.ButtonDisplayStyle )
			{
				case Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.OnMouseEnter:
					
					// SSP 8/15/02 UWG1561
					// What we want to check here is if the mouse is over the cell.
					//
					
					if ( this.IsCursorOverCell( row, rowScrollRegion, colScrollRegion ) )
					{
						// SSP 9/4/03 UWG2622
						// Only display the button if the activation is not disabled.
						//
						if ( Activation.Disabled != row.GetCellActivationResolved( this ) )
							visible = true;
					}
					
					break;
    
				case Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always:

					visible = true;
					break;
    
				case Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.OnCellActivate:
    
					break;
    
				case Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.OnRowActivate:
        
					visible = ( activeRegion && row == this.Layout.ActiveRow );

					// SSP 9/4/03 UWG2622
					// Only display the button if the activation is not disabled.
					//
					visible = visible && Activation.Disabled != row.GetCellActivationResolved( this );

					break;
			}

			return visible;
		}


		// SSP 4/26/02
		// EM Embeddable editors.
		// Look at the modified version above
		

		// SSP 7/29/02 UWG1457
		// Added CanCellEnterEditMode method.
		//
		internal bool CanCellEnterEditMode( UltraGridRow row )
		{
            return this.CanCellEnterEditMode(row, false);
        }

        // MBS 3/21/08 - RowEditTemplate NA2008 V2
        // Added additional overload since we want to prevent a grid cell from entering
        // edit mode while the template is shown.
        internal bool CanCellEnterEditMode( UltraGridRow row, bool isEnteringFromTemplate)
        {
            // MBS 3/21/08 - RowEditTemplate NA2008 V2
            // If a template is currently shown and is modeless, we don't want the user to be able
            // to edit simultaneously in both the template and the grid, as there isn't a good
            // use case for this, not to mention it causing several issues with UIElement synchronization
            if (isEnteringFromTemplate == false)
            {
                UltraGridRowEditTemplate template = row.RowEditTemplateResolved;
                if (template != null && template.DisplayMode == RowEditTemplateDisplayMode.Modeless && template.IsShown)
                    return false;
            }

			// SSP 4/6/05 - NAS 5.2 Filter Row
			// Enclosed the existing code into the if block.
			//
			if ( ! row.IsFilterRow )
			{
				// SSP 6/5/02
				// EM Emdeddable editors.
				// If the editor can't edit the column's data type, then return false.
				//
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				EmbeddableEditorBase editor = this.GetEditor( row );
				// SSP 9/16/03 UWG2512
				// Added support for object column types. Use the EmbeddableEditorOwnerInfo.GetDataType
				// to get the data type. It will return column's data type if it's not an object type
				// otherwise it will return the type provided by the base implementation.
				// 
				//if ( null != editor && ! editor.CanEditType( this.DataType ) )
				Type resolvedDataType = this.EditorOwnerInfo.GetDataType( row );
				if ( null != editor && ! editor.CanEditType( resolvedDataType ) )
					return false;
			}
            else
            {
                // MRS 5/21/2009 - TFS17747
                // There's no way we can tell if an editor is just a renderer (and therefore cannot
                // enter edit mode regardless of the type). So just trap for EmbeddableImageRenderer
                // here. 
                //
                EmbeddableEditorBase editor = this.GetEditor(row);
                if (editor is EmbeddableImageRenderer)
                    return false;
            }

			UltraGridCell cell = row.HasCell( this ) ? row.Cells[ this ] : null;

			Activation activation =  
				null != cell ? cell.ActivationResolved : row.GetResolvedCellActivation( this );

			if ( Activation.Disabled == activation || Activation.NoEdit == activation )
				return false;

			return true;
		}

		internal bool CanBeModified( UltraGridRow row ) 
		{
			// SSP 7/13/04 - UltraCalc
			// If the column has a formula then make the cells read-only.
			//
			// ----------------------------------------------------------
			if ( this.HasActiveFormula )
				return false;
			// ----------------------------------------------------------

			// if the cell has be created, then return delegate the call to 
			// cell's CanBeModified function
			// SSP 12/24/02
			// Optimizations.
			// Use HasCell off the row which checks for cells collection being allocated or
			// not. Directly accessing the cells property will cause the cells collection to
			// be created which we don't want here.
			//			
			//if ( row.Cells.HasCell( this ) )
			if ( row.HasCell( this ) )
				return row.Cells[ this ].CanBeModified;

			// first check the editmode enumerator to see if this cell
			// can be edited
			//
			if ( Activation.AllowEdit != row.GetResolvedCellActivation( this ) )
				return false;

			// Only allow going into edit mode if this is an html cell or
			// the band is not read only
			//
			//
			// All unbound columns can be modified.
			//
			//
			// SSP 12/3/04 BR00550
			// When the underlying DataColumn is read-only we still want to allow editing that
			// column cell in an add-row.
			//
			//return !this.IsReadOnly;
			// SSP 8/26/06 - NAS 6.3
			// Added ReadOnlyAppearance on the override.
			// In addition to IsAddRow, we should also be checking for IsTemplateAddRow.
			// 
			//return ! this.IsReadOnly || row.IsAddRow
			return ! this.IsReadOnly || ( row.IsAddRow || row.IsTemplateAddRow )
				// SSP 11/10/05 BR07684
				// If the data column is computed, it should be read-only regardless of whether
				// the cell is from an add-row.
				// 
				&& ! this.DataColumnHasExpression;
		}

		#region DataColumnHasExpression

		// SSP 11/10/05 BR07684
		// 
		internal bool DataColumnHasExpression
		{
			get
			{
				System.Data.DataColumn dataColumn = this.GetDataColumn( );
				string str = null != dataColumn ? dataColumn.Expression: null;
				return null != str && str.Length > 0;
			}
		}

		#endregion // DataColumnHasExpression

		// SSP 5/1/03 - Cell Level Editor
		// Commented below method out. Not needed anymore.
		//
		

		// SSP 4/26/02
		// EM Embeddable editor.
		// Look at the one on cell because that was modified.
		//
		

		// SSP 4/26/02
		// EM Embeddable editor.
		// Editors take care of min-max constraints.
		

		// SSP 4/23/02
		// Embeddable editors lead to heavy modifications so commented this whole method out
		// and added the new one in Cell.
		//



		// SSP 4/23/02
		// Embeddable editors.
		// Commented out the ShowDropDown since it's not being used anywhere
		//
		

		// SSP 5/2/03
		// Commented below method out because it's not being used anywhere.
		//
		

		// MD 8/2/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Returns the position item's overall origin (relative only to the
		//        /// appropriate col scrolling regions)
		//        /// </summary>
		//#endif
		//        internal int OverallOrigin
		//        {
		//            get
		//            {
		//                if ( this.Hidden )
		//                    return 0;

		//                int nOverallOrigin = this.relativeOrigin;

		//                if ( this.Header.ExclusiveColScrollRegion == null )
		//                {
		//                    // if this is not a ExclusiveColScrollRegion then
		//                    // add the band's origin in
		//                    nOverallOrigin += this.band.GetOrigin();
		//                }

		//                return nOverallOrigin; 
		//            }
		//        }

		#endregion Not Used


		// JJD 9/21/01 - UWG276
		/// <summary>
		/// Returns the key of the column
		/// </summary>
        /// <returns>The key of the column.</returns>
		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();

			// AS 1/8/03 - fxcop
			// Do not compare against string.empty - test the length instead
			//if (this.Key == null || this.Key == string.Empty)
			if (this.Key == null || this.Key.Length == 0)
			{
				sb.Append( "Column#" );
				sb.Append( this.Index );
			}
			else
			{
				sb.Append( this.Key );

			}

			if (this.Header.Hidden)
				sb.Append( " [hidden]" );

			return sb.ToString();
		}

		internal SortIndicator TempSortIndicator
		{
			get
			{
				return this.tempSortIndicator;
			}
			set
			{
				this.tempSortIndicator = value;
			}
		}
		internal bool ClickSortIndicator()
		{
			// SSP 1/23/02
			// If this is a group by column, then treat clicking on the 
			// column header as if they clicked on the group by button
			// of the associated column. So call ClickGroupByButton
			// which just toggles the sort indicator instead of manipulating
			// the sorted columns collection.
			//
			if ( SortIndicator.None != this.sortIndicator &&
				this.IsGroupByColumn )
			{
				this.ClickGroupByButton( );
				return true;
			}


			bool clearExisting = true;

			// First check the new HeaderClickAction property
			//
			switch ( this.band.HeaderClickActionResolved )
			{
				case HeaderClickAction.SortMulti:
			{
					// if the shift key is down then we don't want to clear the
					// existing sorted columns first
					//
					if ( (Control.ModifierKeys & Keys.Shift)  != 0 ) 
						clearExisting = false;

					// If the sort indicator for this column is disabled
					// then just return.
					if ( this.Header.SortIndicator == SortIndicator.Disabled )
						return true;
					break;
				}
				case HeaderClickAction.SortSingle:
			{
					// If the sort indicator for this column is disabled
					// then just return.
					if ( this.Header.SortIndicator == SortIndicator.Disabled )
						return true;
					break;
				}

				default:
					return false;
			}

			SortIndicator newSortIndicator = SortIndicator.None;

			// JJD 1/10/02
			// Changed logic so that at design time they can remove
			// a sort indicator by clicking on it when it is descending
			//
			// Determine the new state of the SortIndicator if the operation
			// is successful.
			switch( this.header.SortIndicator)
			{
				case SortIndicator.Ascending:
					newSortIndicator = SortIndicator.Descending;
					break;
				
				case SortIndicator.None:
					newSortIndicator = SortIndicator.Ascending;
					break;

				case SortIndicator.Descending:
					if ( this.Layout.Grid.DesignMode )
						newSortIndicator = SortIndicator.None;
					else
						newSortIndicator = SortIndicator.Ascending;
					break;
			}

			this.band.SetSortedColumn( this, newSortIndicator, clearExisting );
			
			return true;
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info,
			StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			
			if ( this.ShouldSerializeCellActivation() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CellActivation", this.CellActivation );
				//info.AddValue("CellActivation", (int)this.CellActivation );
			}

            //  BF 3/7/08   FR09238 - AutoCompleteMode
            #region Obsolete code
            //if ( this.ShouldSerializeAutoEdit() )
            //{
            //    // JJD 8/20/02
            //    // Use SerializeProperty static method to serialize properties instead.
            //    Utils.SerializeProperty( info, "AutoEdit", this.AutoEdit );
            //    //info.AddValue("AutoEdit", this.AutoEdit );
            //}
            #endregion Obsolete code

            //  BF 3/7/08   FR09238 - AutoCompleteMode
            if ( this.ShouldSerializeAutoCompleteMode() )
				Utils.SerializeProperty( info, "AutoCompleteMode", this.AutoCompleteMode );

			if ( this.ShouldSerializeAutoSizeEdit() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AutoSizeEdit", this.AutoSizeEdit );
				//info.AddValue("AutoSizeEdit", (int)this.AutoSizeEdit );
			}

			if ( this.ShouldSerializeButtonDisplayStyle() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ButtonDisplayStyle", this.ButtonDisplayStyle );
				//info.AddValue("ButtonDisplayStyle", (int)this.ButtonDisplayStyle );
			}

			// MRS 4/7/05 - Replaced Case with CharacterCasing
//			if ( this.ShouldSerializeCase() )
//			{
//				// JJD 8/20/02
//				// Use SerializeProperty static method to serialize properties instead.
//				Utils.SerializeProperty( info, "Case", this.Case );
//				//info.AddValue("Case", (int)this.Case );
//			}
			if ( this.ShouldSerializeCharacterCasing() )
			{
				Utils.SerializeProperty( info, "CharacterCasing", this.CharacterCasing );
			}

			if ( this.ShouldSerializeCellAppearance() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CellAppearanceHolder", this.cellAppearanceHolder );
				//info.AddValue("CellAppearanceHolder", this.cellAppearanceHolder );
			}

			if (this.ShouldSerializeCellMultiLine())
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CellMultiLine", this.CellMultiLine );
				//info.AddValue("CellMultiLine", (int)this.CellMultiLine );
			}

			if ( this.ShouldSerializeColSpan() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ColSpan", this.ColSpan );
				//info.AddValue("ColSpan", this.ColSpan );
			}

			// we want to serialize the DataType which is of System.Type as
			// a string
			if ( this.ShouldSerializeDataType() )			
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				// SSP 12/20/04 BR00921
				// Serialize the type as a Type instance rather than the full name of the type because
				// for some reason Color, Bitmap and Image types return null from System.Type.GetType
				// call.
				//
				//Utils.SerializeProperty( info, "DataType", this.DataType.FullName );
				// SSP 1/21/05
				// Revert back to serializing the type as the full name of the type because apparently
				// serializing out a type also serializes the assembly info and when deserializing that
				// exact version of assembly is required. As a result we can't serialize type instances.
				//
				//Utils.SerializeProperty( info, "DataType2", this.DataType );
				Utils.SerializeProperty( info, "DataType", this.DataType.FullName );
				//info.AddValue("DataType", this.DataType.FullName );			
			}

			// JAS 2005 v2 XSD Support - FieldLen was deprecated and replaced with MaxLength.
//			if ( this.ShouldSerializeFieldLen() )
			if( this.ShouldSerializeMaxLength() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
//				Utils.SerializeProperty( info, "FieldLen", this.FieldLen );
				//info.AddValue("FieldLen", this.FieldLen );

				// JAS 2005 v2 XSD Support - FieldLen was deprecated and replaced with MaxLength.
				Utils.SerializeProperty( info, "MaxLength", this.MaxLength );
			}

			if ( this.ShouldSerializeHeader() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Header", this.Header );
				//info.AddValue("Header", this.Header );
			}

			if ( this.ShouldSerializeHidden() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				// SSP 9/3/02 UWG1619
				// Use the member variable rather than the property because the Hidden
				// property will return true if the column is a group by column even
				// if the hidden state variable is false.
				//
				//Utils.SerializeProperty( info, "Hidden", this.Hidden );
				Utils.SerializeProperty( info, "Hidden", this.hidden );
				//info.AddValue("Hidden", this.Hidden );
			}

			// JJD 1/17/02 Added HiddenWhenGroupBy property
			//
			if ( this.ShouldSerializeHiddenWhenGroupBy() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "HiddenWhenGroupBy", this.HiddenWhenGroupBy );
				//info.AddValue("HiddenWhenGroupBy", this.HiddenWhenGroupBy );
			}

			// JJD 10/26/01
			// We always want to serialize the key for bound columns
			//
			if ( this.IsBound || this.ShouldSerializeKey() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Key", this.Key );
				//info.AddValue("Key", this.Key );
			}

			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			if ( this.ShouldSerializeLevel() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Level", this.Level );
				//info.AddValue( "Level", this.Level );
			}

			if ( this.ShouldSerializeLockedWidth() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "LockedWidth", this.LockedWidth );
				//info.AddValue("LockedWidth", this.LockedWidth );
			}

			if ( this.ShouldSerializeMaskClipMode() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaskClipMode", this.MaskClipMode );
				//info.AddValue("MaskClipMode", (int)this.MaskClipMode );
			}

			if ( this.ShouldSerializeMaskDataMode() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaskDataMode", this.MaskDataMode );
				//info.AddValue("MaskDataMode", (int)this.MaskDataMode );
			}

			if ( this.ShouldSerializeMaskDisplayMode() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaskDisplayMode", this.MaskDisplayMode );
				//info.AddValue("MaskDisplayMode", (int)this.MaskDisplayMode );
			}

			if ( this.ShouldSerializeMaskInput() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaskInput", this.MaskInput );
				//info.AddValue("MaskInput", this.MaskInput );
			}

			// JJD 8/19/02
			// Don't serialize MinDate or MaxDate anymore. MinValue, MaxValue 
			// replaced them.
			//if ( this.ShouldSerializeMaxDate() )
			//	info.AddValue("MaxDate", this.MaxDate );

			if ( this.ShouldSerializeMinWidth() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MinWidth", this.MinWidth );
				//info.AddValue("MinWidth", this.MinWidth );
			}

			if ( this.ShouldSerializeMaxWidth() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaxWidth", this.MaxWidth );
				//info.AddValue("MaxWidth", this.MaxWidth );
			}

			// JJD 8/19/02
			// Don't serialize MinDate or MaxDate anymore. MinValue, MaxValue 
			// replaced them.
			//if ( this.ShouldSerializeMinDate() )
			//	//info.AddValue("MinDate", this.MinDate );

			if ( this.ShouldSerializeNullable() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Nullable", this.Nullable  );
				//info.AddValue("Nullable", (int)this.Nullable );
			}

			if ( this.ShouldSerializePromptChar() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "PromptChar", this.PromptChar );
				//info.AddValue("PromptChar", this.PromptChar );
			}

			//	BF 10.25.04	UWG3518
			if ( this.ShouldSerializePadChar() )
			{
				Utils.SerializeProperty( info, "PadChar", this.PadChar );
			}

			if ( this.ShouldSerializeProportionalResize() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ProportionalResize", this.ProportionalResize );
				//info.AddValue("ProportionalResize", this.ProportionalResize );
			}

			if ( this.ShouldSerializeStyle() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Style", this.Style );
				//info.AddValue("Style", (int)this.Style );
			}

			if ( this.ShouldSerializeTabStop() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "TabStop", this.TabStop );
				//info.AddValue("TabStop", (bool)this.TabStop );
			}

			// SSP 8/13/02 UWG1550 UWG1551
			// Serialize AllowRowFiltering and AllowRowSummaries as well.
			//
			// ---------------------------------------------------------------
			if ( this.ShouldSerializeAllowRowFiltering( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowRowFiltering", this.AllowRowFiltering );
				//info.AddValue( "AllowRowFiltering", (int)this.AllowRowFiltering );
			}

			if ( this.ShouldSerializeAllowRowSummaries( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowRowSummaries", this.AllowRowSummaries );
				//info.AddValue( "AllowRowSummaries", (int)this.AllowRowSummaries );
			}
			// ---------------------------------------------------------------

			// SSP 4/14/03
			// Added AutoSizeMode property.
			//

			if ( this.ShouldSerializeAutoSizeMode( ) )
			{
				Utils.SerializeProperty( info, "AutoSizeMode", this.AutoSizeMode );
			}

			// SSP 4/28/03 UWG2206
			// Serialize the NullText as well.
			//
			if ( this.ShouldSerializeNullText( ) )
			{
				Utils.SerializeProperty( info, "NullText", this.NullText );
			}

			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			//
			if ( this.ShouldSerializeCellDisplayStyle( ) )
			{
				Utils.SerializeProperty( info, "CellDisplayStyle", this.CellDisplayStyle );
			}

			// AS - 10/30/01
			// We want to serialize just the id, if the valuelist is a
			// member of the control's valuelists collection. If its a dropdown,
			// we want to serialize the control name.
			//if ( this.ShouldSerializeValueList() )
			//	//info.AddValue("ValueList", this.ValueList );
			if ( this.ShouldSerializeValueList() )
			{
				// JAS 2005 v2 XSD Support
				//
//				if (this.valuelist is Infragistics.Win.UltraWinGrid.UltraDropDown)
				if (this.ValueList is Infragistics.Win.UltraWinGrid.UltraDropDown)
				{
					// JAS 2005 v2 XSD Support
					//
					// serialize the control name
//					Infragistics.Win.UltraWinGrid.UltraDropDown udd = (Infragistics.Win.UltraWinGrid.UltraDropDown)this.valuelist;
					Infragistics.Win.UltraWinGrid.UltraDropDown udd = (Infragistics.Win.UltraWinGrid.UltraDropDown)this.ValueList;

					// JJD 8/20/02
					// Use SerializeProperty static method to serialize properties instead.
					Utils.SerializeProperty( info, "ValueListControl", udd.Name );
					//info.AddValue( "ValueListControl", udd.Name );
				}
				// JAS 2005 v2 XSD Support
				//
//				else if ( this.valuelist is Infragistics.Win.ValueList)
				else if ( this.ValueList is Infragistics.Win.ValueList)
				{
					// JAS 2005 v2 XSD Support
					//
//					Infragistics.Win.ValueList vl = (Infragistics.Win.ValueList)this.valuelist;
					Infragistics.Win.ValueList vl = (Infragistics.Win.ValueList)this.ValueList;

					// serialize either the valuelist object
					// or the internal id.
					if (vl.InternalID == 0 || this.band == null || this.band.Layout == null || !this.band.Layout.ValueLists.Contains(vl) )
					{
						// JJD 8/20/02
						// Use SerializeProperty static method to serialize properties instead.
						Utils.SerializeProperty( info, "ValueList", vl );
						//info.AddValue( "ValueList", vl );
					}
					else
					{
						// JJD 8/20/02
						// Use SerializeProperty static method to serialize properties instead.
						Utils.SerializeProperty( info, "ValueListId", vl.InternalID );
						//info.AddValue( "ValueListId", vl.InternalID );
					}
				}
				// JAS 2005 v2 XSD Support
				//
//				else if (this.valuelist == null)
				else if (this.ValueList == null)
				{
					// if we have either the control name or value list id, stored but
					// we do not have an instance of the valuelist yet, serialize it.
					if (this.valueListId != 0)
					{
						// JJD 8/20/02
						// Use SerializeProperty static method to serialize properties instead.
						Utils.SerializeProperty( info, "ValueListId", this.valueListId );
						//info.AddValue( "ValueListId", this.valueListId );
					}
					else if (this.valueListControlName != null)
					{
						// JJD 8/20/02
						// Use SerializeProperty static method to serialize properties instead.
						Utils.SerializeProperty( info, "ValueListControl", this.valueListControlName );
						//info.AddValue( "ValueListControl", this.valueListControlName );
					}
				}
			}

			if ( this.ShouldSerializeVertScrollBar() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "VertScrollBar", this.VertScrollBar );
				//info.AddValue("VertScrollBar", this.VertScrollBar );
			}

			if ( this.ShouldSerializeWidth() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Width", this.Width );
				//info.AddValue("Width", this.Width );
			}

			// SSP 10/26/01
			// SortIndicator and IsGroupByColumn should not be serialized off the
			// column. They should be serialized in the SortedColumns collection.
			// So commented below code out
			//
			// SSP 12/20/04 BR01017
			// Uncommented the following two lines. We need to serialize the SortIndicator 
			// if it's set to Disabled.
			//
			if ( this.ShouldSerializeSortIndicator() )
				info.AddValue("SortIndicator", (int)this.sortIndicator );

			if ( this.ShouldSerializeFormat() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Format", this.format );
				//info.AddValue( "Format", this.format );
			}

			if ( this.ShouldSerializeAllowGroupBy( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowGroupBy", this.allowGroupBy );
				//info.AddValue( "AllowGroupBy", (int)this.allowGroupBy );
			}

			// SSP 10/26/01
			// SortIndicator and IsGroupByColumn should not be serialized off the
			// column. They should be serialized in the SortedColumns collection.
			// So commented below code out
			//
			//if ( this.ShouldSerializeIsGroupByColumn( ) )
			//	//info.AddValue( "IsGroupByColumn", this.isGroupByColumn );

			// AS - 10/30/01
			if ( !this.IsBound )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "UnboundRelativePosition", this.UnboundRelativePosition );
				//info.AddValue( "UnboundRelativePosition", this.UnboundRelativePosition );
			}

			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "IsBound", this.IsBound );
			//info.AddValue( "IsBound", this.IsBound );


			// SSP 11/13/01
			if ( this.ShouldSerializeMaskLiteralsAppearance( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaskListeralsAppearanceHolder", this.maskLiteralsAppearanceHolder );
				//info.AddValue( "MaskListeralsAppearanceHolder", this.maskLiteralsAppearanceHolder );
			}

			// SSP 2/1/02 UWG1016
			// Serialize the cell button appearance as well.
			//
			if ( this.ShouldSerializeCellButtonAppearance( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CellButtonAppearanceHolder", this.cellButtonAppearanceHolder );
				//info.AddValue( "CellButtonAppearanceHolder", this.cellButtonAppearanceHolder );
			}

			// SSP 6/5/02
			// EM Embeddable editors
			// Added MinValue and MaxValue propertries.
			//
			if ( this.ShouldSerializeMinValue( ) )
			{
				// AS 8/15/02
				// Use our object serialization wrapper
				//info.AddValue( "MinValue", this.minValue );
				//
				// JAS 2005 v2 XSD Support - 'minValue' is stored in the Constraint property now.
				//
//				Utils.SerializeObjectProperty(info, "MinValue", this.minValue);
				Utils.SerializeObjectProperty(info, "MinValue", this.MinValue);
			}

			if ( this.ShouldSerializeMaxValue( ) )
			{
				// AS 8/15/02
				// Use our object serialization wrapper
				//info.AddValue( "MaxValue", this.maxValue );
				//
				// JAS 2005 v2 XSD Support - 'minValue' is stored in the Constraint property now.
				//
//				Utils.SerializeObjectProperty(info, "MaxValue", this.maxValue);
				Utils.SerializeObjectProperty(info, "MaxValue", this.MaxValue);
			}

			// SSP 8/20/02
			// Serialize the EditorControl property.
			//
			// JAS 3/17/05 BR02741 - If the control is in the UltraGrid Designer, then we want to save
			// the name of the editor controls.
			//
			//if ( this.ShouldSerializeEditorControl( ) )
			if ( this.ShouldSerializeEditorControl( ) || this.IsInDesigner )
            {
#pragma warning disable 0618
                Control control = this.EditorControl; 
#pragma warning restore 0618                

				// JAS 3/17/05 BR02741
				//
				// Utils.SerializeProperty( info, "EditorControlName", control.Name );
				//
				if( control != null )
					Utils.SerializeProperty( info, "EditorControlName", control.Name );
				else if( this.editorControlName != null )
					Utils.SerializeProperty( info, "EditorControlName", this.editorControlName );					
			}

            // MRS - NAS 9.2 - Control Container Editor
            //
            // MBS 6/19/09 - TFS18459
            // Removed this check, since we should really be serializing 
            // the value regardless of whether we're in the grid designer or not
            //if (this.IsInDesigner)
            {
                Component component = this.EditorComponent;
                Infragistics.Win.INamedComponent namedComponent = component as Infragistics.Win.INamedComponent;                
                Control control = component as Control;

                if (control != null)
                    Utils.SerializeProperty(info, "EditorComponentName", control.Name);
                else if (namedComponent != null)
                    Utils.SerializeProperty(info, "EditorComponentName", namedComponent.Name);
                else if (this.editorComponentName != null)
                    Utils.SerializeProperty(info, "EditorComponentName", this.editorComponentName);
            }

			// SSP 12/11/03 UWG2781
			// Added UseEditorMaskSettings property.
			//
			if ( this.ShouldSerializeUseEditorMaskSettings( ) )
			{
                // MRS NAS v8.3 - Unit Testing
				//Utils.SerializeObjectProperty(info, "UseEditorMaskSettings", this.UseEditorMaskSettings );
                Utils.SerializeProperty(info, "UseEditorMaskSettings", this.UseEditorMaskSettings);
			}

			// SSP 1/31/03 - Row Layout Functionality
			// Serialize the row layout info object.
			//
			if ( this.ShouldSerializeRowLayoutColumnInfo( ) )
			{
				Utils.SerializeProperty( info, "RowLayoutColumnInfo", this.rowLayoutColumnInfo );
			}

			// SSP 11/13/03 Add Row Feature
			// 
			if ( this.ShouldSerializeDefaultCellValue( ) )
			{
				Utils.SerializeObjectProperty( info, "DefaultCellValue", this.defaultAddRowCellValue );
			}

			// SSP 12/12/03 DNF135
			// Added a way to control whether ink buttons get shown.
			// Added ShowInkButton property.
			//
			if ( this.ShouldSerializeShowInkButton( ) )
			{
                // MRS NAS v8.3 - Unit Testing
				//Utils.SerializeObjectProperty( info, "ShowInkButton", this.ShowInkButton );
                Utils.SerializeProperty(info, "ShowInkButton", this.ShowInkButton);
			}

			// MRS 9/14/04
			// Added a property to control whether or not the grid displays 
			// a string in a cell while calculating
			//
			if ( this.ShouldSerializeShowCalculatingText( ) )
			{
                // MRS NAS v8.3 - Unit Testing
				//Utils.SerializeObjectProperty( info, "ShowCalculatingText", this.ShowCalculatingText );
                Utils.SerializeProperty(info, "ShowCalculatingText", this.ShowCalculatingText);
			}


			// SSP 7/6/04 - UltraCalc
			// Added FormulaErrorAppearance.
			//
			if ( this.ShouldSerializeFormulaErrorAppearance( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "FormulaErrorAppearance", this.formulaErrorAppearanceHolder );
				//info.AddValue("CellAppearanceHolder", this.cellAppearanceHolder );
			}

			// SSP 7/13/04 - UltraCalc
			// Added Formula property.
			//
			if ( this.ShouldSerializeFormula( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Formula", this.Formula );
				//info.AddValue( "Format", this.format );
			}

			// SSP 7/13/04 - UltraCalc
			// Added FormulaErrorValue property.
			//
			if ( this.ShouldSerializeFormulaErrorValue( ) )
			{
				Utils.SerializeObjectProperty( info, "FormulaErrorValue", this.formulaErrorValue );
			}

			// SSP 11/3/04 - Merged Cell Feature
			//
			// ----------------------------------------------------------------------------------------
			if ( this.ShouldSerializeMergedCellContentArea( ) )
			{
				Utils.SerializeProperty( info, "MergedCellContentArea", this.MergedCellContentArea );
			}

			if ( this.ShouldSerializeMergedCellStyle( ) )
			{
				Utils.SerializeProperty( info, "MergedCellStyle", this.MergedCellStyle );
			}

			if ( this.ShouldSerializeMergedCellEvaluationType( ) )
			{
				Utils.SerializeProperty( info, "MergedCellEvaluationType", this.MergedCellEvaluationType );
			}

			if ( this.ShouldSerializeMergedCellAppearance( ) )
			{
				Utils.SerializeProperty( info, "MergedCellAppearance", this.mergedCellAppearanceHolder );
			}
			// ----------------------------------------------------------------------------------------

			// SSP 12/14/04 - IDataErrorInfo Support
			//
			if ( this.ShouldSerializeSupportDataErrorInfo( ) )
			{
				Utils.SerializeProperty( info, "SupportDataErrorInfo", this.supportDataErrorInfo );
			}

			// SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			//
			// ----------------------------------------------------------------------------------------
			if ( this.ShouldSerializeFilterEvaluationTrigger( ) )
			{
				Utils.SerializeProperty( info, "FilterEvaluationTrigger", this.FilterEvaluationTrigger );
			}

			if ( this.ShouldSerializeFilterOperandStyle( ) )
			{
				Utils.SerializeProperty( info, "FilterOperandStyle", this.FilterOperandStyle );
			}

			if ( this.ShouldSerializeFilterOperatorDefaultValue( ) )
			{
				Utils.SerializeProperty( info, "FilterOperatorDefaultValue", this.FilterOperatorDefaultValue );
			}

			if ( this.ShouldSerializeFilterOperatorDropDownItems( ) )
			{
				Utils.SerializeProperty( info, "FilterOperatorDropDownItems", this.FilterOperatorDropDownItems );
			}

			if ( this.ShouldSerializeFilterOperatorLocation( ) )
			{
				Utils.SerializeProperty( info, "FilterOperatorLocation", this.FilterOperatorLocation );
			}

			if ( this.ShouldSerializeFilterCellAppearance( ) )
			{
				Utils.SerializeProperty( info, "FilterCellAppearance", this.filterCellAppearanceHolder );
			}

			if ( this.ShouldSerializeFilterOperatorAppearance( ) )
			{
				Utils.SerializeProperty( info, "FilterOperatorAppearance", this.filterOperatorAppearanceHolder );
			}

			if ( this.ShouldSerializeFilterComparisonType( ) )
			{
				Utils.SerializeProperty( info, "FilterComparisonType", this.filterComparisonType );
			}

			// SSP 6/8/05 - NAS 5.2 Filter Row
			// Added FilterClearButtonVisible on the column as well for greater flexibility.
			//
			if ( this.ShouldSerializeFilterClearButtonVisible( ) )
			{
				Utils.SerializeProperty( info, "FilterClearButtonVisible", this.filterClearButtonVisible );
			}
			// ----------------------------------------------------------------------------------------

			// JAS 2005 v2 XSD Support
			//
			// ----------------------------------------------------------------------------------------
			if( this.ShouldSerializeMinLength() )
			{
				Utils.SerializeProperty( info, "MinLength", this.MinLength );
			}

			if( this.ShouldSerializeMaxValueExclusive() )
			{
				Utils.SerializeObjectProperty( info, "MaxValueExclusive", this.MaxValueExclusive );
			}

			if( this.ShouldSerializeMinValueExclusive() )
			{
				Utils.SerializeObjectProperty( info, "MinValueExclusive", this.MinValueExclusive );
			}

			if ( this.ShouldSerializeRegexPattern() )
			{
				Utils.SerializeProperty( info, "RegexPattern", this.RegexPattern );
			}

			if( this.XsdSuppliedConstraints != 0 )
			{
				Utils.SerializeProperty( info, "XsdSuppliedConstraints", this.XsdSuppliedConstraints );
			}
			// ----------------------------------------------------------------------------------------

			// JAS v5.2 GroupBy Break Behavior 5/3/05
			//
			if( this.ShouldSerializeGroupByMode() )
			{
				Utils.SerializeProperty( info, "GroupByMode", this.GroupByMode );
			}

			// JAS v5.2 GroupBy Break Behavior 5/4/05
			//
			if( this.ShouldSerializeSortComparisonType() )
			{
				Utils.SerializeProperty( info, "SortComparisonType", this.SortComparisonType );
			}

			// SSP 6/17/05 - NAS 5.3 Column Chooser
			// 
			if ( this.ShouldSerializeColumnChooserCaption( ) )
			{
				Utils.SerializeProperty( info, "ColumnChooserCaption", this.columnChooserCaption );
			}

			// SSP 6/17/05 - NAS 5.3 Column Chooser
			// 
			if ( this.ShouldSerializeExcludeFromColumnChooser( ) )
			{
				Utils.SerializeProperty( info, "ExcludeFromColumnChooser", this.excludeFromColumnChooser );
			}

			// SSP 6/17/05 - NAS 5.3 Column Chooser
			// 
			if ( this.ShouldSerializeHiddenWhenGroupByOverride( ) )
			{
				Utils.SerializeProperty( info, "HiddenWhenGroupByOverride", this.hiddenWhenGroupByOverride );
			}

			// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
			//
			if(	this.ShouldSerializeCellClickAction() )
			{
				Utils.SerializeProperty( info,  "CellClickAction", this.cellClickAction);
			}

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			if ( this.ShouldSerializeTabIndex( ) )
			{
				Utils.SerializeProperty( info, "TabIndex", this.tabIndex );
			}

			// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
			// 
			if ( this.ShouldSerializeInvalidValueBehavior( ) )
			{
				Utils.SerializeProperty( info, "InvalidValueBehavior", this.InvalidValueBehavior );
			}

			// SSP 2/17/06 BR08864
			// Added IgnoreMultiCellOperation property on the column.
			// 
			if ( this.ShouldSerializeIgnoreMultiCellOperation( ) )
			{
				Utils.SerializeProperty( info, "IgnoreMultiCellOperation", this.IgnoreMultiCellOperation );
			}

            // MBS 11/29/06 - NAS 7.1 - Conditional Formatting
            //
            if( this.ShouldSerializeValueBasedAppearance() )
            {
                // MRS NAS v8.3 - Unit Testing
                //Utils.SerializeObjectProperty(info, "ValueBasedAppearance", this.ValueBasedAppearance );
                Utils.SerializeProperty(info, "ValueBasedAppearance", this.ValueBasedAppearance);
            }

            // CDS 04/14/08 NA 8.2 ImeMode
            if (this.ShouldSerializeImeMode())
            {
                Utils.SerializeProperty(info, "ImeMode", this.ImeMode);
            }

			// MD 9/23/08 - TFS6601
			if ( this.ShouldSerializeReserveSortIndicatorSpaceWhenAutoSizing() )
			{
				Utils.SerializeProperty( info, "ReserveSortIndicatorSpaceWhenAutoSizing", this.reserveSortIndicatorSpaceWhenAutoSizing );
			}

            // MBS 5/11/09 - NA9.2 GroupByRowConnector Appearance
            if (this.ShouldSerializeGroupByRowConnectorAppearance())
            {
                Utils.SerializeProperty(info, "GroupByRowConnectorAppearance", this.groupByRowConnectorAppearanceHolder);
            }
            //
            if (this.ShouldSerializeGroupByRowAppearance())
            {
                Utils.SerializeProperty(info, "GroupByRowAppearance", this.groupByRowAppearanceHolder);
            }

            // MBS 6/23/09 - TFS18639
            if (this.ShouldSerializeFilterOperandDropDownItems())
            {
                Utils.SerializeProperty(info, "FilterOperandDropDownItems", this.FilterOperandDropDownItems);
            }

            // MRS 7/30/2009 - TFS20044
            // We can serialize the 'interface' properties if they are SafelyInfragisticsSerializable
            // ---------------------------------------------------------------------
            if (this.SortComparer != null && Utils.IsSafelyInfragisticsSerializable(this.SortComparer.GetType()))
            {
                Utils.SerializeProperty(info, "SortComparer", this.SortComparer);
            }

            if (this.GroupByComparer != null && Utils.IsSafelyInfragisticsSerializable(this.GroupByComparer.GetType()))
            {
                Utils.SerializeProperty(info, "GroupByComparer", this.GroupByComparer);
            }

            if (this.GroupByEvaluator != null && Utils.IsSafelyInfragisticsSerializable(this.GroupByEvaluator.GetType()))
            {
                Utils.SerializeProperty(info, "GroupByEvaluator", this.GroupByEvaluator);
            }

            if (this.RowFilterComparer != null && Utils.IsSafelyInfragisticsSerializable(this.RowFilterComparer.GetType()))
            {
                Utils.SerializeProperty(info, "RowFilterComparer", this.RowFilterComparer);
            }  

            if (this.MergedCellEvaluator != null && Utils.IsSafelyInfragisticsSerializable(this.MergedCellEvaluator.GetType()))
            {
                Utils.SerializeProperty(info, "MergedCellEvaluator", this.MergedCellEvaluator);
            }        
            // ---------------------------------------------------------------------
		}

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected UltraGridColumn(SerializationInfo info, StreamingContext context)
		{
			//since we're not saving properties with default values, we must set the default here
			this.Reset();

			
			//Needs to be set
			this.band = null;

			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
						// SSP 10/26/01
						// SortIndicator and IsGroupByColumn should not be serialized off the
						// column. They should be serialized in the SortedColumns collection.
						// So commented below code out
						//
						// SSP 12/20/04 BR01017
						// Uncommented the case for SortIndicator. We need to serialize the 
						// SortIndicator if it's set to Disabled.
						//
					case "SortIndicator":
						this.sortIndicator = (SortIndicator)Utils.DeserializeProperty( entry, typeof( SortIndicator ), this.sortIndicator );
						break;

					case "CellAppearanceHolder":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.cellAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						//this.cellAppearanceHolder = (Infragistics.Win.AppearanceHolder)entry.Value;
						break;

						// SSP 2/1/02 UWG1026
						// Deserialize the appearance as well.
						//
					case "CellButtonAppearanceHolder":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.cellButtonAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						//this.cellButtonAppearanceHolder = (Infragistics.Win.AppearanceHolder)entry.Value;
						break;

					case "Format":
						//this.format = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.format = (string)Utils.DeserializeProperty( entry, typeof(string), this.format );
						//this.format = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;

					case "CellActivation":
					{
						//this.CellActivation = (Activation)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.CellActivation = (Activation)Utils.DeserializeProperty( entry, typeof(Activation), this.CellActivation );
						//this.CellActivation = (Activation)Utils.ConvertEnum(entry.Value, this.CellActivation);
						break;
					}

					case "AutoEdit":
                    {
                        //  BF 3/7/08   FR09238 - AutoCompleteMode
                        #region Obsolete code
                        ////this.AutoEdit = (bool)entry.Value;
                        //// AS 8/14/02
                        //// Use the disposable object method to allow deserialization from xml, etc.
                        ////
                        //// JJD 8/19/02
                        //// Use the DeserializeProperty static method to de-serialize properties instead.
                        //this.AutoEdit = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.AutoEdit );
                        ////this.AutoEdit = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
                        //break;
                        #endregion Obsolete code

                        //  BF 3/7/08   FR09238 - AutoCompleteMode
                        //  Set the 'autoCompleteMode' member accordingly, unless it is already set.
                        bool autoEdit = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
                        if ( autoEdit == false &&
                             this.autoCompleteMode == Infragistics.Win.AutoCompleteMode.Default )
                            this.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
                        
                        break;
					}

                    //  BF 3/7/08   FR09238 - AutoCompleteMode
                    case "AutoCompleteMode":
                    {
                        this.AutoCompleteMode = (Infragistics.Win.AutoCompleteMode)Utils.DeserializeProperty( entry, typeof(Infragistics.Win.AutoCompleteMode), this.AutoCompleteMode );
                        break;
                    }

					case "AutoSizeEdit":
					{
						//this.AutoSizeEdit = (DefaultableBoolean)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.AutoSizeEdit = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.AutoSizeEdit );
						//this.AutoSizeEdit = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.AutoSizeEdit);
						break;
					}

					case "ButtonDisplayStyle":
					{
						//this.ButtonDisplayStyle = (ButtonDisplayStyle)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.ButtonDisplayStyle = (ButtonDisplayStyle)Utils.DeserializeProperty( entry, typeof(ButtonDisplayStyle), this.ButtonDisplayStyle );
						//this.ButtonDisplayStyle = (ButtonDisplayStyle)Utils.ConvertEnum(entry.Value, this.ButtonDisplayStyle);
						break;
					}

					case "Case":
					{	
						//this.Case = (Case)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						
						// MRS 4/7/05 - Replaced Case with CharacterCasing
						//this.Case = (Case)Utils.DeserializeProperty( entry, typeof(Case), this.Case );
						Case newCase = (Case)Utils.DeserializeProperty( entry, typeof(Case), GridUtils.CharacterCasingToCase(this.characterCasing) );
						this.characterCasing = GridUtils.CaseToCharacterCasing(newCase);

						//this.Case = (Case)Utils.ConvertEnum(entry.Value, this.Case);
						break;
					}

					// MRS 4/7/05 - Replaced Case with CharacterCasing
					case "CharacterCasing":
					{							
						this.characterCasing = (CharacterCasing)Utils.DeserializeProperty( entry, typeof(CharacterCasing), this.characterCasing );
						break;
					}

					case "CellMultiLine":
					{
						//this.CellMultiLine = (Infragistics.Win.DefaultableBoolean)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.CellMultiLine = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.CellMultiLine );
						//this.CellMultiLine = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.CellMultiLine);
						break;
					}

					case "ColSpan":
					{
						//this.ColSpan = (short)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.ColSpan = (short)Utils.DeserializeProperty( entry, typeof(short), this.ColSpan );
						//this.ColSpan = (short)DisposableObject.ConvertValue( entry.Value, typeof(short) );
						break;
					}

					case "DataType":
					{
						//String dataTypeName = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						//String dataTypeName = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						String dataTypeName = (string)Utils.DeserializeProperty( entry, typeof(string), null );
						if ( null != dataTypeName && !this.IsBound )
						{
							this.dataType = System.Type.GetType( dataTypeName );

							// SSP 12/20/04 BR00921
							// Serialize the type as a Type instance rather than the full name of the type because
							// for some reason Color, Bitmap and Image types return null from System.Type.GetType
							// call.
							//
							// ----------------------------------------------------------------------------------
							if ( null == this.dataType )
							{
								if ( "System.Drawing.Color" == dataTypeName )
									this.dataType = typeof( System.Drawing.Color );
								else if ( "System.Drawing.Image" == dataTypeName )
									this.dataType = typeof( System.Drawing.Image );
								else if ( "System.Drawing.Bitmap" == dataTypeName )
									this.dataType = typeof( System.Drawing.Bitmap );
							}
							// ----------------------------------------------------------------------------------
						}						
						break;
					}
					// SSP 12/20/04 BR00921
					// Serialize the type as a Type instance rather than the full name of the type because
					// for some reason Color, Bitmap and Image types return null from System.Type.GetType
					// call.
					//
					case "DataType2":
					{
						System.Type dataType = (System.Type)Utils.DeserializeProperty( entry, typeof( System.Type ), this.dataType );
						if ( null != dataType && !this.IsBound )
						{
							this.dataType = dataType;
						}						
						break;
					}

					case "FieldLen":
					{
						//this.FieldLen = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
//						this.FieldLen = (int)Utils.DeserializeProperty( entry, typeof(int), this.FieldLen );
						//this.FieldLen = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );

						// JAS 2005 v2 XSD Support - FieldLen was deprecated and replaced with MaxLength.
						//
						this.MaxLength = (int)Utils.DeserializeProperty( entry, typeof(int), this.MaxLength );
						break;
					}

					// JAS 2005 v2 XSD Support - FieldLen was deprecated and replaced with MaxLength.
					case "MaxLength":
					{
						this.MaxLength = (int)Utils.DeserializeProperty( entry, typeof(int), this.MaxLength );
						break;
					}

					case "Header":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.header = (ColumnHeader)Utils.DeserializeProperty( entry, typeof(ColumnHeader), null );
						//this.header = (ColumnHeader)entry.Value;
						break;
					}

					case "Hidden":
					{
						//this.Hidden = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Hidden = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.Hidden );
						//this.Hidden = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
					}

					// JJD 1/17/02 Added HiddenWhenGroupBy property
					//
					case "HiddenWhenGroupBy":
					{
						//this.HiddenWhenGroupBy = (DefaultableBoolean)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.HiddenWhenGroupBy = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.HiddenWhenGroupBy );
						//this.HiddenWhenGroupBy = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.HiddenWhenGroupBy);
						break;
					}

                    // CDS 04/14/08 NA 8.2 ImeMode
                    case "ImeMode":
                    {
                        this.imeMode = (System.Windows.Forms.ImeMode)Utils.DeserializeProperty(entry, typeof(System.Windows.Forms.ImeMode), this.imeMode);
                        break;
                    }

					case "Key":
					{
						//this.Key = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Key = (string)Utils.DeserializeProperty( entry, typeof(string), this.Key );
						//this.Key = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;
					}

					case "Level":
					{
						//this.level = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.level = (int)Utils.DeserializeProperty( entry, typeof(int), this.level );
						//this.level = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;
					}

					case "LockedWidth":
					{
						//this.LockedWidth = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.LockedWidth = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.LockedWidth );
                        //this.LockedWidth = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
					}

					case "MaskClipMode":
					{
						//this.MaskClipMode = (MaskMode)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.MaskClipMode = (MaskMode)Utils.DeserializeProperty( entry, typeof(MaskMode), this.MaskClipMode );
						//this.MaskClipMode = (MaskMode)Utils.ConvertEnum(entry.Value, this.MaskClipMode);
						break;
					}

					case "MaskDataMode":
					{
						//this.MaskDataMode = (MaskMode)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.MaskDataMode = (MaskMode)Utils.DeserializeProperty( entry, typeof(MaskMode), this.MaskDataMode );
						//this.MaskDataMode = (MaskMode)Utils.ConvertEnum(entry.Value, this.MaskDataMode);
						break;
					}

					case "MaskDisplayMode":
					{
						//this.MaskDisplayMode = (MaskMode)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.MaskDisplayMode = (MaskMode)Utils.DeserializeProperty( entry, typeof(MaskMode), this.MaskDisplayMode );
						//this.MaskDisplayMode = (MaskMode)Utils.ConvertEnum(entry.Value, this.MaskDisplayMode);
						break;
					}

					case "MaskInput":
					{
						//this.MaskInput = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.MaskInput = (string)Utils.DeserializeProperty( entry, typeof(string), this.MaskInput );
						//this.MaskInput = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;
					}

					case "MaxDate":
					{
						//this.MaxDate = (System.DateTime)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/17/02
						// Use the DeserializeProperty static method to de-serialize properties instead.

						// JAS 2005 v2 XSD Support
						//
//						this.maxValue = Utils.DeserializeObjectProperty( entry );
						this.MaxValue = Utils.DeserializeObjectProperty( entry );
						//this.MaxDate = (System.DateTime)DisposableObject.ConvertValue( entry.Value, typeof(System.DateTime) );
						break;
					}

					case "MinWidth":
					{
						//this.MinWidth = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.MinWidth = (int)Utils.DeserializeProperty( entry, typeof(int), this.MinWidth );
						//this.MinWidth = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;
					}

					case "MaxWidth":
					{
						//this.MaxWidth = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.MaxWidth = (int)Utils.DeserializeProperty( entry, typeof(int), this.MaxWidth );
						//this.MaxWidth = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;
					}

					case "MinDate":
					{
						//this.MinDate = (System.DateTime)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/17/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						//
						// JAS 2005 v2 XSD Support
						//
						this.MinValue = Utils.DeserializeObjectProperty( entry );
//						this.minValue = Utils.DeserializeObjectProperty( entry );
						//this.MinDate = (System.DateTime)DisposableObject.ConvertValue( entry.Value, typeof(System.DateTime) );
						break;
					}

					case "Nullable":
					{
						//this.Nullable = (Infragistics.Win.UltraWinGrid.Nullable)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Nullable = (Nullable)Utils.DeserializeProperty( entry, typeof(Nullable), this.Nullable );
						//this.Nullable = (Nullable)Utils.ConvertEnum(entry.Value, this.Nullable);
						break;
					}

					case "PromptChar":
					{
						//this.PromptChar = (char)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.PromptChar = (char)Utils.DeserializeProperty( entry, typeof(char), this.PromptChar );
						//this.PromptChar = (char)DisposableObject.ConvertValue( entry.Value, typeof(char) );
						break;
					}

					//	BF 10.25.04	UWG3518
					case "PadChar":
					{
						this.PadChar = (char)Utils.DeserializeProperty( entry, typeof(char), this.PadChar );
						break;
					}

					case "ProportionalResize":
					{
						//this.ProportionalResize = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.ProportionalResize = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.ProportionalResize );
						//this.ProportionalResize = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
					}

					case "Style":
					{
						//this.Style = (Infragistics.Win.UltraWinGrid.ColumnStyle)(int) entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Style = (ColumnStyle)Utils.DeserializeProperty( entry, typeof(ColumnStyle), this.Style );
						//this.Style = (ColumnStyle)Utils.ConvertEnum(entry.Value, this.Style);
						break;
					}

					case "TabStop":
					{
						//this.tabStop = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.tabStop = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.tabStop );
						//this.tabStop = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
					}

					case "Tag":
						// AS 8/15/02
						// Use our tag deserializer.
						//
						//this.tagValue = entry.Value;
						this.DeserializeTag(entry);
						break;


					case "ValueList":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.ValueList = (ValueList)Utils.DeserializeProperty( entry, typeof(ValueList), null );
						//this.ValueList = (Infragistics.Win.ValueList)entry.Value;
						break;
					}

					// AS - 10/30/01
					case "ValueListControl":
					{
						//this.valueListControlName = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.valueListControlName = (string)Utils.DeserializeProperty( entry, typeof(string), this.valueListControlName );
						//this.valueListControlName = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;
					}

					// AS - 10/30/01
					case "ValueListId":
					{
						//this.valueListId = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.valueListId = (int)Utils.DeserializeProperty( entry, typeof(int), this.valueListId );
						//this.valueListId = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;
					}

					case "VertScrollBar":
					{
						//this.VertScrollBar = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.VertScrollBar = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.VertScrollBar );
						//this.VertScrollBar = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
					}

					case "Width":
					{
						//this.Width = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Width = (int)Utils.DeserializeProperty( entry, typeof(int), this.Width );
						//this.Width = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;
					}

					case "AllowGroupBy":
					{
						//this.allowGroupBy = (DefaultableBoolean)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.allowGroupBy = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.allowGroupBy );
						//this.allowGroupBy = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.allowGroupBy);
						break;
					}

						// SSP 10/26/01
						// SortIndicator and IsGroupByColumn should not be serialized off the
						// column. They should be serialized in the SortedColumns collection.
						// So commented below code out
						//
					

					// AS - 10/30/01
					case "UnboundRelativePosition":
					{
						//this.unboundRelativePosition = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.unboundRelativePosition = (int)Utils.DeserializeProperty( entry, typeof(int), this.unboundRelativePosition );
						//this.unboundRelativePosition = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;
					}

					// AS - 10/30/01
					case "IsBound":
					{
						//this.bound = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.bound = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.bound );
						//this.bound = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
					}

					// SSP 11/13/01
					case "MaskListeralsAppearanceHolder":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.maskLiteralsAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						//this.maskLiteralsAppearanceHolder = (Infragistics.Win.AppearanceHolder)entry.Value;
						break;
					}

					// SSP 6/5/02
					// Added MinValue and MaxValue properties.
					//
					case "MinValue":
					{
						// AS 8/15/02
						// Use our object deserialization wrapper
						//this.minValue = entry.Value;
						//
						// JAS 2005 v2 XSD Support
						// 
//						this.minValue = Utils.DeserializeObjectProperty(entry);
						this.MinValue = Utils.DeserializeObjectProperty(entry);
						break;
					}
					case "MaxValue":
					{
						// AS 8/15/02
						// Use our object deserialization wrapper
						//this.maxValue = entry.Value;
						//
						// JAS 2005 v2 XSD Support
						//
//						this.maxValue = Utils.DeserializeObjectProperty(entry);
						this.MaxValue = Utils.DeserializeObjectProperty(entry);
						break;
					}
						// SSP 8/13/02 UWG1550 UWG1551
						// Serialize AllowRowFiltering and AllowRowSummaries as well.
						//
					case "AllowRowFiltering":
					{
						//this.allowRowFiltering = (DefaultableBoolean)(int)entry.Value;						
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.allowRowFiltering = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.allowRowFiltering );
						//this.allowRowFiltering = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.allowRowFiltering);
						break;
					}
					case "AllowRowSummaries":
					{
						//this.allowRowSummaries = (AllowRowSummaries)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.allowRowSummaries = (AllowRowSummaries)Utils.DeserializeProperty( entry, typeof(AllowRowSummaries), this.allowRowSummaries );
						//this.allowRowSummaries = (AllowRowSummaries)Utils.ConvertEnum(entry.Value, this.allowRowSummaries);
						break;
					}
						// SSP 8/20/02
						// Serialize the EditorControl property.
						//
					case "EditorControlName":
					{
						this.editorControlName = (string)Utils.DeserializeProperty( entry, typeof(string), this.editorControlName );
						break;
					}

                    // MRS - NAS 9.2 - Control Container Editor
                    case "EditorComponentName":
                    {
                        this.editorComponentName = (string)Utils.DeserializeProperty(entry, typeof(string), this.editorComponentName);
                        break;
                    }

					// SSP 1/31/03 - Row Layout Functionality
					// Deserialize the row layout info object.
					//
					case "RowLayoutColumnInfo":
					{
						this.rowLayoutColumnInfo = (RowLayoutColumnInfo)Utils.DeserializeProperty( entry, typeof( RowLayoutColumnInfo ), null );
						break;
					}
					// SSP 4/14/03
					// Added AutoSizeMode property.
					//
					case "AutoSizeMode":
					{
						this.autoSizeMode = (ColumnAutoSizeMode)Utils.DeserializeProperty( entry, typeof( ColumnAutoSizeMode ), this.autoSizeMode );
						break;
					}
						// SSP 4/28/03 UWG2206
						// Serialize the NullText as well.
						//
					case "NullText":
					{
						this.nullText = (string)Utils.DeserializeProperty( entry, typeof( string ), this.nullText );
						break;
					}
					case "CellDisplayStyle":
					{
						this.cellDisplayStyle = (CellDisplayStyle)Utils.DeserializeProperty( entry, typeof( CellDisplayStyle ), this.cellDisplayStyle );
						break;
					}
					// SSP 11/13/03 Add Row Feature
					//
					case "DefaultCellValue":
					{
						this.defaultAddRowCellValue = Utils.DeserializeObjectProperty( entry );
						break;
					}
					// SSP 12/11/03 UWG2781
					// Added UseEditorMaskSettings property.
					//
					case "UseEditorMaskSettings":
					{
						this.useEditorMaskSettings = (bool)Utils.DeserializeProperty( entry, typeof( bool ), this.useEditorMaskSettings );
						break;
					}
						
					// SSP 12/12/03 DNF135
					// Added a way to control whether ink buttons get shown.
					// Added ShowInkButton property.
					//
					case "ShowInkButton":
						this.ShowInkButton = (ShowInkButton)Utils.DeserializeProperty( entry, typeof( ShowInkButton ), this.showInkButton );
						break;

					// MRS 9/14/04
					// Added a property to control whether or not the grid displays 
					// a string in a cell while calculating
					//
					case "ShowCalculatingText":
						this.ShowCalculatingText = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), this.showCalculatingText );
						break;

					// SSP 7/6/04 - UltraCalc
					// Added FormulaErrorAppearance property.
					//
					case "FormulaErrorAppearance":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.formulaErrorAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						//this.cellAppearanceHolder = (Infragistics.Win.AppearanceHolder)entry.Value;
						break;
					// SSP 7/13/04 - UltraCalc
					// Added Formula property.
					//
					case "Formula":
						this.Formula = (string)Utils.DeserializeProperty( entry, typeof( string ), this.Formula );
						break;
					// SSP 7/13/04 - UltraCalc
					// Added FormulaErrorValue property.
					//
					case "FormulaErrorValue":
						this.formulaErrorValue = Utils.DeserializeObjectProperty( entry );
						break;
					// SSP 11/3/04 - Merged Cell Feature
					//
					case "MergedCellContentArea":
						this.mergedCellContentArea = (MergedCellContentArea)Utils.DeserializeProperty( entry, typeof( MergedCellContentArea ), this.mergedCellContentArea );
						break;

					case "MergedCellStyle":
						this.mergedCellStyle = (MergedCellStyle)Utils.DeserializeProperty( entry, typeof( MergedCellStyle ), this.mergedCellStyle );
						break;

					case "MergedCellEvaluationType":
						this.mergedCellEvaluationType = (MergedCellEvaluationType)Utils.DeserializeProperty( entry, typeof( MergedCellEvaluationType ), this.mergedCellEvaluationType );
						break;

					case "MergedCellAppearance":
						this.mergedCellAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof( AppearanceHolder ), null );
						break;
					// SSP 12/14/04 - IDataErrorInfo Support
					//
					case "SupportDataErrorInfo":
						this.supportDataErrorInfo = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), this.supportDataErrorInfo );
						break;
					// SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					//
					// ----------------------------------------------------------------------------------------
					case "FilterEvaluationTrigger": 
						this.filterEvaluationTrigger = (FilterEvaluationTrigger)Utils.DeserializeProperty( entry, typeof( FilterEvaluationTrigger ), this.filterEvaluationTrigger );
						break;
					case "FilterOperandStyle": 
						this.filterOperandStyle = (FilterOperandStyle)Utils.DeserializeProperty( entry, typeof( FilterOperandStyle ), this.filterOperandStyle );
						break;
					case "FilterOperatorDefaultValue": 
						this.filterOperatorDefaultValue = (FilterOperatorDefaultValue)Utils.DeserializeProperty( entry, typeof( FilterOperatorDefaultValue ), this.filterOperatorDefaultValue );
						break;
					case "FilterOperatorDropDownItems": 
						this.filterOperatorDropDownItems = (FilterOperatorDropDownItems)Utils.DeserializeProperty( entry, typeof( FilterOperatorDropDownItems ), this.filterOperatorDropDownItems );
						break;
					case "FilterOperatorLocation": 
						this.filterOperatorLocation = (FilterOperatorLocation)Utils.DeserializeProperty( entry, typeof( FilterOperatorLocation ), this.filterOperatorLocation );
						break;
					case "FilterCellAppearance":
						this.filterCellAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof( AppearanceHolder ), null );
						break;
					case "FilterOperatorAppearance":
						this.filterOperatorAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof( AppearanceHolder ), null );
						break;
					case "FilterComparisonType":
						this.filterComparisonType = (FilterComparisonType)Utils.DeserializeProperty( entry, typeof( FilterComparisonType ), this.filterComparisonType );
						break;

						// SSP 6/8/05 - NAS 5.2 Filter Row
						// Added FilterClearButtonLocation on the column as well for greater flexibility.
						//
					case "FilterClearButtonVisible":
						this.filterClearButtonVisible = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), this.filterClearButtonVisible );
						break;
					// ----------------------------------------------------------------------------------------

					// JAS 2005 v2 XSD Support 
					//
					// ----------------------------------------------------------------------------------------
					case "MinLength":
						this.MinLength = (int)Utils.DeserializeProperty( entry, typeof(int), -1 );
						break;
					case "MaxValueExclusive":
						this.MaxValueExclusive = Utils.DeserializeObjectProperty( entry );
						break;
					case "MinValueExclusive":
						this.MinValueExclusive = Utils.DeserializeObjectProperty( entry );
						break;
					case "RegexPattern":
						this.RegexPattern = Utils.DeserializeProperty( entry, typeof(string), null ) as string;
						break;
					case "XsdSuppliedConstraints":
						this.XsdSuppliedConstraints = (XsdConstraintFlags)Utils.DeserializeProperty( entry, typeof(XsdConstraintFlags), this.xsdSuppliedConstraints );
						break;
					// ----------------------------------------------------------------------------------------

					// JAS v5.2 GroupBy Break Behavior 5/3/05
					//
					case "GroupByMode":
						this.GroupByMode = (GroupByMode)Utils.DeserializeProperty( entry, typeof(GroupByMode), this.groupByMode );
						break;

					// JAS v5.2 GroupBy Break Behavior 5/4/05
					//
					case "SortComparisonType":
						this.SortComparisonType = (SortComparisonType)Utils.DeserializeProperty( entry, typeof(SortComparisonType), this.sortComparisonType );
						break;

					// SSP 6/17/05 - NAS 5.3 Column Chooser
					// 
					case "ColumnChooserCaption":
						this.columnChooserCaption = (string)Utils.DeserializeProperty( entry, typeof( string ), this.columnChooserCaption );
						break;

					// SSP 6/17/05 - NAS 5.3 Column Chooser
					// 
					case "ExcludeFromColumnChooser":
						this.excludeFromColumnChooser = (ExcludeFromColumnChooser)Utils.DeserializeProperty( entry, typeof( ExcludeFromColumnChooser ), this.excludeFromColumnChooser );
						break;

					// SSP 6/17/05 - NAS 5.3 Column Chooser
					// 
					case "HiddenWhenGroupByOverride":
						this.hiddenWhenGroupByOverride = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), this.hiddenWhenGroupByOverride );
						break;

					// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
					//
					case "CellClickAction": 
						this.CellClickAction = (CellClickAction)Utils.DeserializeProperty( entry, typeof(CellClickAction), this.CellClickAction );
						break;

					// SSP 7/29/05 - NAS 5.3 Tab Index
					// 
					case "TabIndex":
						this.tabIndex = (int)Utils.DeserializeProperty( entry, typeof( int ), this.tabIndex );
						break;

					// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
					// 
					case "InvalidValueBehavior":
						this.invalidValueBehavior = (InvalidValueBehavior)Utils.DeserializeProperty( entry, typeof( InvalidValueBehavior ), this.invalidValueBehavior );
						break;

						// SSP 2/17/06 BR08864
						// Added IgnoreMultiCellOperation property on the column.
						// 
					case "IgnoreMultiCellOperation":
						this.IgnoreMultiCellOperation = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), this.ignoreMultiCellOperation );
						break;

                    // MBS 12/4/06 NAS7.1 - Conditional Formatting
                    case "ValueBasedAppearance":
                        this.ValueBasedAppearance = (IValueAppearance)Utils.DeserializeObjectProperty(entry);

                        break;

					// MD 9/23/08 - TFS6601
					case "ReserveSortIndicatorSpaceWhenAutoSizing":
						this.reserveSortIndicatorSpaceWhenAutoSizing =
							(ReserveSortIndicatorSpaceWhenAutoSizing)Utils.DeserializeProperty( entry, typeof( ReserveSortIndicatorSpaceWhenAutoSizing ), this.reserveSortIndicatorSpaceWhenAutoSizing );
						break;

                    // MBS 5/11/09 - NA9.2 GroupByRowConnector Appearance
                    case "GroupByRowConnectorAppearance":
                        this.groupByRowConnectorAppearanceHolder =
                            (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.groupByRowConnectorAppearanceHolder);
                        break;
                    //
                    case "GroupByRowAppearance":
                        this.groupByRowAppearanceHolder =
                            (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.groupByRowAppearanceHolder);
                        break;

                    // MBS 6/23/09 - TFS18639
                    case "FilterOperandDropDownItems":
                        this.filterOperandDropDownItems = (FilterOperandDropDownItems)Utils.DeserializeProperty(entry, typeof(FilterOperandDropDownItems), this.filterOperandDropDownItems);
                        break;


                    // MRS 7/30/2009 - TFS20044
                    // We can serialize the 'interface' properties if they are SafelyInfragisticsSerializable
                    // ---------------------------------------------------------------------
                    case "SortComparer":
                        this.comparer = (IComparer)Utils.DeserializeProperty(entry, typeof(IComparer), this.comparer);
                        break;

                    case "GroupByComparer":
                        this.groupByComparer = (IComparer)Utils.DeserializeProperty(entry, typeof(IComparer), this.groupByComparer);
                        break;

                    case "GroupByEvaluator":
                        this.groupByEvaluator = (IGroupByEvaluator)Utils.DeserializeProperty(entry, typeof(IGroupByEvaluator), this.groupByEvaluator);
                        break;

                    case "RowFilterComparer":
                        this.rowFilterComparer = (IComparer)Utils.DeserializeProperty(entry, typeof(IComparer), this.rowFilterComparer);
                        break;

                    case "MergedCellEvaluator":
                        this.mergedCellEvaluator = (IMergedCellEvaluator)Utils.DeserializeProperty(entry, typeof(IMergedCellEvaluator), this.mergedCellEvaluator);
                        break;
                    // ---------------------------------------------------------------------
					
					default:
						Debug.Assert( false, "Invalid entry in Override de-serialization ctor" );
						break;					
				}
			}

            // MRS - NAS 9.2 - Control Container Editor
            if (this.editorComponentName == null || this.editorComponentName.Length == 0)
            {
                this.editorComponentName = this.editorControlName;
                this.editorControlName = null;
            }
		}

		internal void InitializeFrom( UltraGridColumn source, PropertyCategories propCat )
		{
			// Save the datatype if we are boound
			//
			System.Type currentType = this.dataType;
            
			bool bound = this.bound;
			// SSP 4/22/05 BR03492
			// Also do the same for isChaptered as we do for the bound.
			//
			bool isChaptered = this.cachedIsChaptered;
            
            
			//Copy state
			this.CopyState( source );

			// restore the saved bound state when this column is hooked up
			// otherwise, keep the state from the source column

			// JJD 11/26/01
			// Restore the bound state if this is the display or print layout
			// instead of not.
			//
			//if (this.Band != null && this.Band.Layout != null && !(this.Band.Layout.IsDisplayLayout || this.Band.Layout.IsPrintLayout))
			if ( this.Band != null && 
				this.Band.Layout != null && 
				// SSP 5/7/03 - Export Functionality
				//
				//(this.Band.Layout.IsDisplayLayout || this.Band.Layout.IsPrintLayout))
				(this.Band.Layout.IsDisplayLayout || this.Band.Layout.IsPrintLayout || this.Band.Layout.IsExportLayout ))
			{
				// restore the bound state and the datatype if we are bound
				//
				this.bound = bound;

				// SSP 4/22/05 BR03492
				// Also do the same for isChaptered as we do for the bound.
				//
				this.cachedIsChaptered = isChaptered;
			}
    
			if ( bound )
				this.dataType = currentType;

			this.hasWidthSet		= source.HasWidthSet;
			this.width				= source.width;
			this.relativeOrigin		= source.RelativeOrigin;
			this.Format				= source.format;

			// SSP 10/20/04 UWG3709
			// Copy over the format info as well. When cloning a layout the format info should
			// be copied to the cloned layout.
			//
			this.formatInfo			= source.formatInfo;

			// SSP 7/13/04 - UltraCalc
			//
			this.Formula = source.Formula;
			this.formulaErrorValue = source.formulaErrorValue;

			// JJd 10/24/01
			// Zero out synchronizedWidth so it will get recalculated
			//
			this.synchronizedWidth	= 0;

			//RobA 10/22/01 implemented	

			// JJD 10/22/01
			// Check to see if the source has an appearance first
			//
			if ( source.cellAppearanceHolder != null &&
				source.cellAppearanceHolder.HasAppearance )
			{
				// Get CellAppearance to trigger allocation of holder
				//
				Infragistics.Win.AppearanceBase app = this.CellAppearance;
			
				// init the holder from the source
				//
				this.cellAppearanceHolder.InitializeFrom( source.cellAppearanceHolder );
			}
			else
			{
				// reset the appearance holder
				//
				if ( this.cellAppearanceHolder != null &&
					this.cellAppearanceHolder.HasAppearance )
				{
					this.cellAppearanceHolder.Reset();
				}
			}

			
			// Check to see if the source has an appearance first
			//
			if ( source.cellButtonAppearanceHolder != null &&
				source.cellButtonAppearanceHolder.HasAppearance )
			{
				// Get CellAppearance to trigger allocation of holder
				//
				Infragistics.Win.AppearanceBase app = this.CellButtonAppearance;
			
				// init the holder from the source
				//
				this.cellButtonAppearanceHolder.InitializeFrom( source.cellButtonAppearanceHolder );
			}
			else
			{
				// reset the appearance holder
				//
				if ( this.cellButtonAppearanceHolder != null &&
					this.cellButtonAppearanceHolder.HasAppearance )
				{
					this.cellButtonAppearanceHolder.Reset();
				}
			}

			// Check to see if the source has an appearance first
			//
			if ( source.maskLiteralsAppearanceHolder != null &&
				source.maskLiteralsAppearanceHolder.HasAppearance )
			{
				// Get CellAppearance to trigger allocation of holder
				//
				Infragistics.Win.AppearanceBase app = this.MaskLiteralsAppearance;
			
				// init the holder from the source
				//
				this.maskLiteralsAppearanceHolder.InitializeFrom( source.maskLiteralsAppearanceHolder );
			}
			else
			{
				// reset the appearance holder
				//
				if ( this.maskLiteralsAppearanceHolder != null &&
					this.maskLiteralsAppearanceHolder.HasAppearance )
				{
					this.maskLiteralsAppearanceHolder.Reset();
				}
			}

			// SSP 7/6/04 - UltraCalc
			// Check to see if the source has an appearance first
			//
			if ( source.formulaErrorAppearanceHolder != null &&
				source.formulaErrorAppearanceHolder.HasAppearance )
			{
				// Get CellAppearance to trigger allocation of holder
				//
				Infragistics.Win.AppearanceBase app = this.FormulaErrorAppearance;
			
				// init the holder from the source
				//
				this.formulaErrorAppearanceHolder.InitializeFrom( source.formulaErrorAppearanceHolder );
			}
			else
			{
				// reset the appearance holder
				//
				if ( this.formulaErrorAppearanceHolder != null &&
					this.formulaErrorAppearanceHolder.HasAppearance )
				{
					this.formulaErrorAppearanceHolder.Reset();
				}
			}

			// SSP 11/29/04 - Merged Cell Feature
			//
			if ( source.mergedCellAppearanceHolder != null &&
				source.mergedCellAppearanceHolder.HasAppearance )
			{
				// Get CellAppearance to trigger allocation of holder
				//
				Infragistics.Win.AppearanceBase app = this.MergedCellAppearance;
			
				// init the holder from the source
				//
				this.mergedCellAppearanceHolder.InitializeFrom( source.mergedCellAppearanceHolder );
			}
			else
			{
				// reset the appearance holder
				//
				if ( this.mergedCellAppearanceHolder != null &&
					this.mergedCellAppearanceHolder.HasAppearance )
				{
					this.mergedCellAppearanceHolder.Reset( );
				}
			}

			// SSP 3/23/05 - NAS 5.2 Filter Row
			// Added FilterCellAppearance and FilterOperatorAppearance.
			//
			if ( source.filterCellAppearanceHolder != null &&
				source.filterCellAppearanceHolder.HasAppearance )
			{
				// Get CellAppearance to trigger allocation of holder
				//
				Infragistics.Win.AppearanceBase app = this.FilterCellAppearance;
			
				// init the holder from the source
				//
				this.filterCellAppearanceHolder.InitializeFrom( source.filterCellAppearanceHolder );
			}
			else
			{
				// reset the appearance holder
				//
				if ( this.filterCellAppearanceHolder != null &&
					this.filterCellAppearanceHolder.HasAppearance )
				{
					this.filterCellAppearanceHolder.Reset( );
				}
			}

			// SSP 3/23/05 - NAS 5.2 Filter Row
			// Added FilterCellAppearance and FilterOperatorAppearance.
			//
			if ( source.filterOperatorAppearanceHolder != null &&
				source.filterOperatorAppearanceHolder.HasAppearance )
			{
				// Get CellAppearance to trigger allocation of holder
				//
				Infragistics.Win.AppearanceBase app = this.FilterOperatorAppearance;
			
				// init the holder from the source
				//
				this.filterOperatorAppearanceHolder.InitializeFrom( source.filterOperatorAppearanceHolder );
			}
			else
			{
				// reset the appearance holder
				//
				if ( this.filterOperatorAppearanceHolder!= null &&
					this.filterOperatorAppearanceHolder.HasAppearance )
				{
					this.filterOperatorAppearanceHolder.Reset( );
				}
			}

			//this.AttachedToCol = source.AttachedToCol;

			this.Header.InitializeFrom ( source.Header, propCat );

			// Only update value lists if that category is set
			//
			if ( ( propCat & PropertyCategories.ValueLists ) != 0 )
			{
				// AS - 10/30/01
				// We really should be checking if the valuelist is part of this objects
				// collection and not part of the source, which in some cases, may not even
				// be hooked up enough to get to the control's valuelist. Also, we should
				// allow carrying over the valuelist if it is a dropdown control.
				//
				//				if ( source.ValueList != null && source.ValueList is Infragistics.Win.UltraWinGrid.ValueList )
				//				{
				//					Infragistics.Win.UltraWinGrid.ValueList sourceValueList = source.ValueList as Infragistics.Win.UltraWinGrid.ValueList;
				//
				//					//
				//					// get the valuelists collection from the layout
				//					//
				//					//ValueListsCollection valueListCol = this.band.Layout.ValueLists;
				//					ValueListsCollection valueListCol = source.band.Layout.ValueLists;
				//
				//					if ( valueListCol != null)
				//					{
				//						// get the key from the source value list
				//						//
				//						string key = sourceValueList.Key;
				//        
				//						if ( key != null )
				//						{
				//							// call the item method to return the new valuelist ptr
				//							//
				//							Infragistics.Win.UltraWinGrid.ValueList valueList = valueListCol[key];
				//
				//							// set the new value list
				//							//
				//							// Note: We don't need to addref the new value list since
				//							// that was done in the 'Item" method above
				//							//
				//							if ( valueList != null )
				//								this.valuelist   = valueList;
				//						}
				//					}
				//				}
				//				return;

				// JAS 2005 v2 XSD Support
				//
//				if (source.valuelist == null)
				if (source.ValueList == null)
				{
					// if there is no valuelist object yet, just hold onto the valuelistid
					// and control name from the source. we'll try to get the valuelist object
					// later.
					this.valueListId = source.valueListId;
					this.valueListControlName = source.valueListControlName;

					// SSP 5/15/02 UWG1115
					// Also set the valuelist to null becuase if we don't and if this.valuelist
					// is non-null, then in the get of ValueList property we will return that
					// without looking at the valueListId (look at ValueList's get).
					//
					//
					// JAS 2005 v2 XSD Support
					//
//					this.valuelist = null;
					this.ValueList = null;
				}
					// JAS 2005 v2 XSD Support
					//
//				else if ( source.valuelist is Infragistics.Win.UltraWinGrid.UltraDropDown)
				else if ( source.ValueList is Infragistics.Win.UltraWinGrid.UltraDropDown)
				{
					// JAS 2005 v2 XSD Support
					//
					// We should take references to a valuelist that are from a dropdown control.
//					this.valuelist = source.valuelist;
					this.ValueList = source.ValueList;
				}
					// SSP 6/25/02 UWG1261
					// Added following else if block for UltraCombo.
					//
					// JAS 2005 v2 XSD Support
					//
//				else if ( source.valuelist is Infragistics.Win.UltraWinGrid.UltraCombo )
				else if ( source.ValueList is Infragistics.Win.UltraWinGrid.UltraCombo )
				{
					// JAS 2005 v2 XSD Support
					//
//					this.valuelist = source.valuelist;
					this.ValueList = source.ValueList;
				}
					// JAS 2005 v2 XSD Support
					//
//				else if ( source.valuelist is Infragistics.Win.ValueList )
				else if ( source.ValueList is Infragistics.Win.ValueList )
				{
					// JAS 2005 v2 XSD Support
					//
//					Infragistics.Win.ValueList vl = (Infragistics.Win.ValueList)source.valuelist;
					Infragistics.Win.ValueList vl = (Infragistics.Win.ValueList)source.ValueList;

					if (vl.InternalID == 0 || this.band == null || this.band.Layout == null || !this.band.Layout.ValueLists.Contains(vl) )
					{
						try
						{
							// SSP 6/14/02 UWG1228
							// We want to use the property's set method to set the
							// value list which also unhooks from the old value list and
							// rehooks into the new one. Also it calls InitAppearances 
							// on the valueList which sets the backward pointers on the
							// value list items in the value list.
							//
							//this.valuelist = (Infragistics.Win.ValueList)DisposableObject.CloneSerializableObject(vl);
							//this.ValueList = (Infragistics.Win.ValueList)DisposableObject.CloneSerializableObject(vl);
							// JJD 8/14/02
							// Pass in the correct binder
							// SSP 10/2/06 BR15248
							// Use the new Clone method instead. If data values of value list items are custom objects
							// that can not be serialized, the CloneSerializableObject will result in a value list with
							// no data values. Instead use the new Clone method which copies over the references to the
							// data values.
							// 
							// ------------------------------------------------------------------------------
							this.ValueList = ((ValueList)source.ValueList).Clone( );
							
							// ------------------------------------------------------------------------------
						}
						catch (Exception)
						{
							// SSP 6/14/02 UWG1228
							// We want to use the property's set method to set the
							// value list which also unhooks from the old value list and
							// rehooks into the new one. Also it calls InitAppearances 
							// on the valueList which sets the backward pointers on the
							// value list items in the value list.
							//
							//this.valuelist = vl;
							this.ValueList = vl;
						}
					}
					else
						// SSP 6/14/02 UWG1228
						// We want to use the property's set method to set the
						// value list which also unhooks from the old value list and
						// rehooks into the new one. Also it calls InitAppearances 
						// on the valueList which sets the backward pointers on the
						// value list items in the value list.
						//
						//this.valuelist = vl;
						this.ValueList = vl;
				}
			}

            // MBS 11/30/06 - NAS7.1 - Conditional Formatting
            if (source.ValueBasedAppearance != null)
            {
                this.ValueBasedAppearance = source.ValueBasedAppearance.Clone() as IValueAppearance;

                // MBS 12/7/06
                // If the ConditionValueAppearance has any FormulaConditions, then we will need to loop through them 
                // and update them to use this column as the IFormulaProvider.
                if (this.ValueBasedAppearance is ConditionValueAppearance)
                {
                    ConditionValueAppearance cvAppearance = (ConditionValueAppearance)this.ValueBasedAppearance;
                    cvAppearance.ApplyFormulaProvider(this);
                }
            }
            else
                this.ValueBasedAppearance = null;

			this.maskInputString = source.MaskInput;

			// SSP 10/24/01 Implemented
			// Not needed to implement since masking works a bit differently
			// in the UltraWinGrid than the ActiveX Grid. In UltraWinGrid
			// We are using separate UltraMaskedEdit control for masked editing.
			// so no need to reset masking. The only thing that needs to be
			// done though is to reset the parsedMask to null so that it
			// get's recreated.
			//
			this.ClearCachedParsedMask( );

			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;
			
			// SSP 5/6/02
			// Copy the Editor and EditControl
			//
			// SSP 10/24/02 UWG1779
			// We shouldn't be checking if the source has an embeddable editor
			// set or not. We should just copy it. Otherwise if you save
			// a layout with no editor set on a column, and then set and editor
			// and then load that saved layout, the column will retain the new
			// set editor. We don't want to do that. Look at UWG1779.
			//
			//if ( null != source.embeddableEditor )
			this.embeddableEditor = source.embeddableEditor;

			// SSP 10/24/02 UWG1779
			// We shouldn't be checking if the source has an embeddable editor
			// set or not. We should just copy it. Otherwise if you save
			// a layout with no editor set on a column, and then set and editor
			// and then load that saved layout, the column will retain the new
			// set editor. We don't want to do that. Look at UWG1779.
			//
			//if ( null != source.editorControl )
			// SSP 1/28/05
			// We need to hook into the editor control's Disposed so we can reset the 
			// EditorControl property when the editor control gets disposed.
			//
			//this.editorControl = source.editorControl;
			this.InternalSetEditorControl( source.editorControl );

			// SSP 8/20/02
			// else copy the editorControlName if it exits.
			//
			// SSP 10/24/02 UWG1779
			// We shouldn't be checking if the source has an embeddable editor
			// set or not. We should just copy it. Otherwise if you save
			// a layout with no editor set on a column, and then set and editor
			// and then load that saved layout, the column will retain the new
			// set editor. We don't want to do that. Look at UWG1779.
			//
			//else if ( null != source.editorControlName )
			this.editorControlName = source.editorControlName;

            // MRS - NAS 9.2 - Control Container Editor
            this.InternalSetEditorComponent(source.editorComponent);
            this.editorComponentName = source.editorComponentName;

			// SSP 6/5/02
			// Added MinValue and MaxValue properties.
			//
			// SSP 10/24/02 UWG1779
			// Don't check if the source has the property set or not.
			//
			//if ( null != source.minValue )
			//
			// JAS 2005 v2 XSD Support
			// MinValue is copied over in the ValueConstraint.InitializeFrom method.
			//
//			this.minValue = source.minValue;

			// SSP 10/24/02 UWG1779
			// Don't check if the source has the property set or not.
			//			
			//if ( null != source.maxValue )
			//
			// JAS 2005 v2 XSD Support
			// MaxValue is copied over in the ValueConstraint.InitializeFrom method.
			//
//			this.maxValue = source.maxValue;

			// SSP 11/13/03 Add Row Feature
			//
			this.defaultAddRowCellValue = source.defaultAddRowCellValue;

			// SSP 12/11/03 UWG2781
			// Added UseEditorMaskSettings property.
			//
			this.useEditorMaskSettings = source.useEditorMaskSettings;

			// JAS 2005 v2 XSD Support
			//
			// ------------------------------------------------------------------------------
			// We need to pass 'false' into InitializeFrom so that it will not copy over the
			// Enumeration property (i.e. the ValueList).  This is necessary because the ValueList
			// is configured in the code above by setting the column's ValueList property.
			//
			this.Constraint.InitializeFrom( source.Constraint, false );
			this.xsdSuppliedConstraints = source.xsdSuppliedConstraints;
			// ------------------------------------------------------------------------------

			// SSP 8/13/02 UWG1567
			// Dirty the default editor flag.
			//
			this.defaultEditorDirtyFlag = true;
			// SSP 7/18/03 - Cell Level Editor/EditorControl/Style properties
			// Bump the version number that the cell verifies against.
			//
			this.verifyEditorVersionNumber++;

			// SSP 2/28/03 - Row Layout Functionality
			//
			if ( null != source.rowLayoutColumnInfo )
				this.RowLayoutColumnInfo.InitializeFrom( source.rowLayoutColumnInfo );
			else if ( null != this.rowLayoutColumnInfo )
				this.rowLayoutColumnInfo.Reset( );

            // MBS 5/18/09 - TFS17243
            if (source.groupByRowConnectorAppearanceHolder != null &&
                source.groupByRowConnectorAppearanceHolder.HasAppearance)
            {
                // Get CellAppearance to trigger allocation of holder
                //
                Infragistics.Win.AppearanceBase app = this.GroupByRowConnectorAppearance;

                // init the holder from the source
                //
                this.groupByRowConnectorAppearanceHolder.InitializeFrom(source.groupByRowConnectorAppearanceHolder);
            }
            else
            {
                // reset the appearance holder
                //
                if (this.groupByRowConnectorAppearanceHolder != null &&
                    this.groupByRowConnectorAppearanceHolder.HasAppearance)
                {
                    this.groupByRowConnectorAppearanceHolder.Reset();
                }
            }
            //
            if (source.groupByRowAppearanceHolder != null &&
                source.groupByRowAppearanceHolder.HasAppearance)
            {
                // Get CellAppearance to trigger allocation of holder
                //
                Infragistics.Win.AppearanceBase app = this.GroupByRowAppearance;

                // init the holder from the source
                //
                this.groupByRowAppearanceHolder.InitializeFrom(source.groupByRowAppearanceHolder);
            }
            else
            {
                // reset the appearance holder
                //
                if (this.groupByRowAppearanceHolder != null &&
                    this.groupByRowAppearanceHolder.HasAppearance)
                {
                    this.groupByRowAppearanceHolder.Reset();
                }
            }
		}

		internal object Clone()
		{
		//	Column col = new Column( this.band );
		//	col.InitializeFrom( this, PropertyCategories.All );
		//	return col;

			if ( null != this.cloneData )
				throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_101"));

			UltraGridColumn clone = (UltraGridColumn)this.MemberwiseClone();

			// SSP 8/27/02 UWG1611
			// Null out the embeddable editor of the clone because we don't want both
			// columns to be referencing the same embeddable editor owner info.
			//
			clone.editorOwnerInfo = null;

			// SSP 9/27/01 UWG347
			// we shouldn't assume a header is already created. So
			// use Header property to enforce creation if not already
			// created
			// 
			//clone.header = this.header.Clone();
			//
			clone.header = this.Header.Clone();

            // SSP 12/10/03 UWG2784
			// Make sure the cloned header's Column property points to the cloned column and not
			// this column.
			//
			clone.header.InitColumn( clone );

			clone.cloneData = new ColumnCloneData();

			//clone.CloneData.posChanged = this.cloneData.posChanged;
			clone.CloneData.clonedFromCol = this;
			clone.CloneData.width  = this.Width;
			clone.CloneData.visiblePosition = this.Header.VisiblePosition;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//clone.CloneData.group = this.Group;
			//clone.CloneData.level = this.Level;
			    
			return clone;


		}

		private void CopyState( UltraGridColumn source )
		{
			// JAS 2005 v2 XSD Support
			// FieldLen was replaced by MaxLength, which is copied in the ValueConstraint's InitializeFrom method.
			// That method is called in the column's InitializeFrom.
			//
//			this.fieldLen = source.fieldLen;

			this.minWidth = source.minWidth;
			this.maxWidth = source.maxWidth;

			if( this.level != source.Level )
			{
				this.level = source.Level;
				this.NotifyPropChange( PropertyIds.Level );
			}

			this.colSpan = source.ColSpan;
			this.promptChar = source.PromptChar;

			//	BF 10.25.04	UWG3518
			this.padChar = source.PadChar;

			this.dataType = source.DataType;

			this.style = source.Style;
			this.autoSizeEdit = source.AutoSizeEdit;
			this.activation = source.CellActivation;
			// MRS 4/7/05
			//this.Case = source.Case;
			this.CharacterCasing = source.CharacterCasing;

			this.maskClipMode = source.MaskClipMode;
			this.maskDataMode = source.MaskDataMode;
			this.maskDisplayMode = source.MaskDisplayMode;
			this.nullable = source.Nullable;
			//RobA UWG427 10/2/01 
			//this.SortIndicator = source.SortIndicator;
			this.InternalSetSortIndicator( source.SortIndicator );
			this.tabStop = source.TabStop;
			this.cellMultiLine = source.CellMultiLine;
			this.buttonDisplayStyle = source.ButtonDisplayStyle;

			// Storage for Get/Set properties

            //  BF 3/7/08   FR09238 - AutoCompleteMode
            //
            //  If the new AutoCompleteMode property is explicitly set, use that value.
            //  Use 'Append' if the obsolete AutoEdit property was set to true.
            //  Use 'None' if AutoEdit was set to false.
            //
            //this.autoEdit = source.AutoEdit;
            Infragistics.Win.AutoCompleteMode autoCompleteMode = source.AutoCompleteMode;
            this.autoCompleteMode = autoCompleteMode != Infragistics.Win.AutoCompleteMode.Default ?
                autoCompleteMode :
                // MBS 4/9/09 - TFS16326
                // Since we resolve 'Default' to 'Append' anyway, we shouldn't explicitly select 'Append' here since
                // a user might have actually set the value to Default in the designer (which will resolve to AutoEdit_Legacy
                // being True).
                //
			    //source.AutoEdit_Legacy ? Infragistics.Win.AutoCompleteMode.Append :
                source.AutoEdit_Legacy ? Infragistics.Win.AutoCompleteMode.Default :

                Infragistics.Win.AutoCompleteMode.None;

			this.proportionalResize = source.ProportionalResize;
			
			// SSP 9/3/02 UWG1619
			// Use the member variable rather than the property because the Hidden
			// property will return true if the column is a group by column even
			// if the hidden state variable is false.
			//
			//this.hidden = source.Hidden;
			this.hidden = source.hidden;
			
			// JJD 1/17/02 Added HiddenWhenGroupBy property
			//
			this.hiddenWhenGroupBy = source.HiddenWhenGroupBy;

			this.LockedWidth = source.LockedWidth;

			this.vertScrollBar = source.VertScrollBar;
			this.bound = source.IsBound;

			// SSP 4/22/05 BR03492
			// Also copy over the cachedIsChaptered.
			//
			this.cachedIsChaptered = source.cachedIsChaptered;

			// SSP 10/26/01 UWG589
			// Need to set the flag indicating whether the column
			// is a group by column.
			//
			// SSP 3/16/04 UWG3091
			// Bump the group-by hierarchy version number if the group-by status of the column
			// is going to change.
			//
			if ( this.isGroupByColumn != source.isGroupByColumn && null != this.Band )
				this.BumpGroupByHierarchyVersion( );
			this.InternalSetIsGroupByColumn( source.isGroupByColumn );
			this.allowGroupBy = source.allowGroupBy;

			// SSP 9/13/02 UWG1657
			// Also copy the group by evaluater when initializing from another column.
			//
			this.groupByEvaluator = source.groupByEvaluator;

			// SSP 1/20/04 UWG2905
			// Also copy the sort comparer when initializing from another column.
			//
			this.comparer = source.comparer;

			// SSP 11/10/05 BR07680
			// Added RowFilterComparer property.
			// 
			this.rowFilterComparer = source.rowFilterComparer;

			// SSP 10/20/04 UWG3747
			// Added GroupByComparer property to allow the user to sort group-by rows 
			// using a custom comparer.
			//
			this.groupByComparer = source.groupByComparer;

			// SSP 12/4/01 UWG804
			// Copy MinDate and MaxDate as well.
			//
			// JJD 8/19/02
			// Use minValue and MaxValue instead
			//this.minDate = source.minDate;
			//this.maxDate = source.maxDate;

			// JJD 1/21/02 - UWG851 Moved from layout
			//
			this.nullText = source.nullText;

			// SSP 8/13/02 UWG1550 UWG1551
			// Serialize AllowRowFiltering and AllowRowSummaries as well.
			//
			this.allowRowFiltering = source.allowRowFiltering;
			this.allowRowSummaries = source.allowRowSummaries;

			// SSP 4/14/03
			// Added AutoSizeMode property.
			//
			this.autoSizeMode = source.autoSizeMode;

			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			//
			this.cellDisplayStyle = source.cellDisplayStyle;

			// SSP 12/12/03 DNF135
			// Added a way to control whether ink buttons get shown.
			// Added ShowInkButton property.
			//
			this.showInkButton = source.showInkButton;
			
			// MRS 9/14/04
			// Added a property to control whether or not the grid displays 
			// a string in a cell while calculating
			//
			this.showCalculatingText = source.showCalculatingText;

			// SSP 11/29/04 - Merged Cell Feature
			//
			this.mergedCellContentArea = source.mergedCellContentArea;
			this.mergedCellEvaluator = source.mergedCellEvaluator;
			this.mergedCellStyle = source.mergedCellStyle;
			this.mergedCellEvaluationType = source.mergedCellEvaluationType;

			// SSP 12/14/04 - IDataErrorInfo Support
			//
			this.supportDataErrorInfo = source.supportDataErrorInfo;

			// SSP 3/14/05 - Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			//
			// ------------------------------------------------------------------------------
			this.filterEvaluationTrigger = source.filterEvaluationTrigger;
			this.filterOperandStyle = source.filterOperandStyle;
			this.filterOperatorDefaultValue = source.filterOperatorDefaultValue;
			this.filterOperatorDropDownItems = source.filterOperatorDropDownItems;
			this.filterOperatorLocation = source.filterOperatorLocation;
			this.filterComparisonType = source.filterComparisonType;
			// SSP 6/8/05 - NAS 5.2 Filter Row
			// Added FilterClearButtonLocation on the column as well for greater flexibility.
			//
			this.filterClearButtonVisible = source.filterClearButtonVisible;
			// ------------------------------------------------------------------------------

			// JAS v5.2 GroupBy Break Behavior 5/3/05
			//
			this.groupByMode = source.groupByMode;

			// JAS v5.2 GroupBy Break Behavior 5/4/05
			//
			this.sortComparisonType = source.sortComparisonType;

			// SSP 6/17/05 - NAS 5.3 Column Chooser
			// 
			this.columnChooserCaption = source.columnChooserCaption;
			this.excludeFromColumnChooser = source.excludeFromColumnChooser;
			// SSP 12/13/06 BR18313
			// 
			this.hiddenWhenGroupByOverride = source.hiddenWhenGroupByOverride;

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			this.tabIndex = source.tabIndex;

			// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
			// 
			this.invalidValueBehavior = source.invalidValueBehavior;

			// SSP 2/17/06 BR08864
			// Added IgnoreMultiCellOperation property on the column.
			// 
			this.ignoreMultiCellOperation = source.ignoreMultiCellOperation;

			// SSP 4/5/06 - NAS 6.2 - Support for New SpellCheck functionality 
			// Added SpellChecker property. Only copy from source if the source has a spell checker.
			// Otherwise leave the spell checker intact.
			// 
			if ( null != source.spellChecker )
				this.spellChecker = source.spellChecker;

			// SSP 1/15/07 BR19156
			// We need to copy over cellClickAction as well.
			// 
			this.cellClickAction = source.cellClickAction;

            // CDS 04/14/08 NA 8.2 ImeMode
            this.ImeMode = source.ImeMode;
        }

		private Infragistics.Win.UltraWinGrid.UltraDropDown FindDropDownFromName( string ctrlName )
		{
			Infragistics.Win.UltraWinGrid.UltraDropDown dropdown = null;

			try
			{
                // MRS 9/12/06 - BR14893
                // Before we search from the form down, First search from the control up.                 
                UltraGridBase gridBase = null != this.Layout ? this.Layout.Grid : null;
                if (gridBase != null)
                {
                    Control control = gridBase.Parent;
                    while (control != null)
                    {
                        if (control.Controls != null)
                            dropdown = this.SearchForDropDownWithName(control.Controls, ctrlName, false);

                        if (dropdown != null)
                            return dropdown;

                        control = control.Parent;
                    }
                }

				// SSP 12/28/05 BR08374
				// If the grid is part of a user control at design time then there won't be any form.
				// Search the user control instead.
				// 
				// --------------------------------------------------------------------------------------
				//System.Windows.Forms.Form form = this.band.Layout.Grid.FindForm();
				//dropdown = this.SearchForDropDownWithName( form.Controls, ctrlName );
				Control rootCtrl = GridUtils.GetRootControlForSearching( null != this.Layout ? this.Layout.Grid : null );
				if ( null != rootCtrl )
                    // MRS 9/12/06 - Added recursize param
                    //dropdown = this.SearchForDropDownWithName( rootCtrl.Controls, ctrlName );
                    dropdown = this.SearchForDropDownWithName(rootCtrl.Controls, ctrlName, true);
				// --------------------------------------------------------------------------------------
			}
			catch (Exception)
			{
			}

			return dropdown;
		}

        // MRS 9/12/06 - Added recursize param
        //private Infragistics.Win.UltraWinGrid.UltraDropDown SearchForDropDownWithName( System.Windows.Forms.Control.ControlCollection controls, string name)
        private Infragistics.Win.UltraWinGrid.UltraDropDown SearchForDropDownWithName(System.Windows.Forms.Control.ControlCollection controls, string name, bool recursive)
        {
			if (controls == null)
				return null;

			Infragistics.Win.UltraWinGrid.UltraDropDown dropdown = null;

			foreach (System.Windows.Forms.Control ctrl in controls)
			{
				if ( ctrl is Infragistics.Win.UltraWinGrid.UltraDropDown && ctrl.Name.Equals(name) )
					return (Infragistics.Win.UltraWinGrid.UltraDropDown)ctrl;
                else if (ctrl.Controls.Count > 0
                    // MRS 9/12/06 - Added recursize param
                    && recursive)
                {
                    // MRS 9/12/06 - Added recursize param
                    //dropdown = SearchForDropDownWithName( ctrl.Controls, name );
                    dropdown = SearchForDropDownWithName(ctrl.Controls, name, true);

					if (dropdown != null)
						break;
				}
			}

			return dropdown;
		}

		internal int UnboundRelativePosition
		{
			get
			{
				if (this.IsBound)
					return -1;

				// if one of the columns does not have its band hooked up, 
				// just use the position
				if (this.band == null)
					return this.unboundRelativePosition;
				else
				{
					// otherwise get, store and return the column's position
					// in relation to the other unbound columns.

					// JJD 11/26/01
					// Subtract the bound columns count instead of the unbound column's count
					//
					//this.unboundRelativePosition = (this.Index - this.band.Columns.UnboundColumnsCount);
					this.unboundRelativePosition = (this.Index - this.band.Columns.BoundColumnsCount);
					return this.unboundRelativePosition;
				}
			}
		}

		#region UltraGridColumnTypeConverter

		/// <summary>
		/// UltraGridColumn type converter.
		/// </summary>
		public sealed class UltraGridColumnTypeConverter : ExpandableObjectConverter 
		{
            /// <summary>
            /// Returns whether this converter can convert the object to the specified type, using the specified context.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
            /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) 
			{
				if (destinationType == typeof(InstanceDescriptor)) 
				{
					return true;
				}
				
				return base.CanConvertTo(context, destinationType);
			}

            /// <summary>
            /// Converts the given value object to the specified type, using the specified
            /// context and culture information.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
            /// <param name="value">The System.Object to convert.</param>
            /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
            /// <returns>An System.Object that represents the converted value.</returns>
            public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) 
			{
				if (destinationType == null) 
				{
					throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_20"));
				}

				if ( destinationType == typeof(InstanceDescriptor) && 
					value is UltraGridColumn ) 
				{
					UltraGridColumn item = (UltraGridColumn)value;
					
					ConstructorInfo ctor;

					int sortIndex = ( item.Band != null )
										? item.Band.SortedColumns.IndexOf( item )
										: -1;
					

					int indexInGroup = ( item.Group != null )
										? item.Group.Columns.IndexOf( item )
										: -1;

					int unboundRelativePosition = item.UnboundRelativePosition;

					object valueListLink = null;

					IValueList vList = item.ValueList;

					if ( vList is ValueList )
						valueListLink = ((ValueList)vList).InternalID;
					else
					if ( vList is UltraDropDown )
						valueListLink = ((UltraDropDown)vList).Name;
					// SSP 8/19/05 BR05725
					// 
					// ----------------------------------------------------------------------------
					else if ( null != item.valueListControlName && item.valueListControlName.Length > 0 )
						valueListLink = item.valueListControlName;
					else if ( 0 != item.valueListId )
						valueListLink = item.valueListId;
					// ----------------------------------------------------------------------------

					if ( indexInGroup >= 0		&&
						 sortIndex >= 0 )
					{
						ctor = typeof(UltraGridColumn).GetConstructor(new Type[] { typeof( string ), 
																				typeof ( int ) ,
																				typeof ( object ) ,
																				typeof ( int ) ,
																				typeof ( int ) ,
																				typeof ( int ) ,
																				typeof ( int ) ,
																				typeof ( SortIndicator ) ,
																				typeof ( bool ) } );
					}
					else
					if ( indexInGroup >= 0 )
					{
						ctor = typeof(UltraGridColumn).GetConstructor(new Type[] { typeof( string ), 
																				typeof ( int ) ,
																				typeof ( object ) ,
																				typeof ( int ) ,
																				typeof ( int ) ,
																				typeof ( int ) } );
					}
					else
					if ( sortIndex >= 0 )
					{
						ctor = typeof(UltraGridColumn).GetConstructor(new Type[] { typeof( string ), 
																				typeof ( int ) ,
																				typeof ( object ) ,
																				typeof ( int ) ,
																				typeof ( SortIndicator ) ,
																				typeof ( bool ) } );
					}
					else
					if ( valueListLink != null )
					{
						ctor = typeof(UltraGridColumn).GetConstructor(new Type[] { typeof( string ), 
																				typeof ( int ) ,
																				typeof ( object ) } );
					}
					else
					if ( unboundRelativePosition >= 0 )
					{
						ctor = typeof(UltraGridColumn).GetConstructor(new Type[] { typeof( string ), 
																					typeof ( int ) } );
					}
					else
					{
						ctor = typeof(UltraGridColumn).GetConstructor(new Type[] { typeof( string ) } );
					}

					if (ctor != null) 
					{
						//false as the last parameter here causes generation of a local variable for the type 

						if ( indexInGroup >= 0		&&
							sortIndex >= 0 )
						{
							return new InstanceDescriptor(ctor, new object[] {  item.Key, 
																				unboundRelativePosition,
																				valueListLink,
																				item.Group.InternalID,
																				indexInGroup,
																				item.Level,
																				sortIndex,
																				item.SortIndicator,
																				 // SSP 12/11/01
																				 // Use isGroupByColumn instead of isGroupBy.
																				 // We don't need isGroupBy.
																				 //item.isGroupBy 
																				 item.isGroupByColumn  }, false );
						}
						else
						if ( indexInGroup >= 0 )
						{
							return new InstanceDescriptor(ctor, new object[] {  item.Key, 
																				unboundRelativePosition,
																				valueListLink,
																				item.Group.InternalID,
																				indexInGroup,
																				item.Level }, false );
						}
						else
						if ( sortIndex >= 0 )
						{
							return new InstanceDescriptor(ctor, new object[] {  item.Key, 
																				unboundRelativePosition,
																				valueListLink,
																				sortIndex,
																				item.SortIndicator,
																				 // SSP 12/11/01
																				 // Use isGroupByColumn instead of isGroupBy.
																				 // We don't need isGroupBy.
																				//item.isGroupBy 
																				item.isGroupByColumn }, false );
						}
						else
						if ( valueListLink != null )
						{
							return new InstanceDescriptor(ctor, new object[] {  item.Key, 
																				unboundRelativePosition,
																				valueListLink }, false );
						}
						else
						if ( unboundRelativePosition >= 0 )
						{
							return new InstanceDescriptor(ctor, new object[] {  item.Key, 
																				unboundRelativePosition }, false );
						}
						else
						{
							return new InstanceDescriptor(ctor, new object[] {  item.Key }, false );
						}
					}
				}

				return base.ConvertTo(context, culture, value, destinationType);
			}
		
					/// <summary>
        /// Returns a collection of properties for the type of array specified by the
        /// value parameter, using the specified context and attributes.
		/// </summary>
        /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
        /// <param name="value">An <see cref="System.Object"/> that specifies the type of array for which to get properties.</param>
        /// <param name="attributes">An array of type <see cref="System.Attribute"/> that is used as a filter.</param>
        /// <returns>A <see cref="System.ComponentModel.PropertyDescriptorCollection"/> with the properties that are exposed for this data type, or null if there are no properties.</returns>
        public override PropertyDescriptorCollection GetProperties(
						ITypeDescriptorContext context,
						object value,
						Attribute[] attributes )
			{
				PropertyDescriptorCollection props = base.GetProperties( context, value, attributes );

				UltraGridColumn column = value as UltraGridColumn;

				// JJD 2/4/02 - UWG578
				// If this is a dropdown or combo filter out properties
				// that don't make sense
				//
				if ( props != null	&&
					 column != null &&
					 column.band != null	&&
					 column.band.Layout != null &&
					 column.band.Layout.Grid is UltraDropDownBase )
				{
					int count = 0;

					// count up all the properties that won't be filtered out
					//
					for ( int i = 0; i < props.Count; i++ )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( !this.FilterOutProperty( props[i] ) )
						if ( !UltraGridColumnTypeConverter.FilterOutProperty( props[i] ) )
							count++;
					}

					// allocate an array of the proper size
					//
					PropertyDescriptor [] propArray = new PropertyDescriptor[count];

					int current = 0;

					// copy the unfiltered properties into the array
					//
					for ( int i = 0; i < props.Count; i++ )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( !this.FilterOutProperty( props[i] ) )
						if ( !UltraGridColumnTypeConverter.FilterOutProperty( props[ i ] ) )
						{
							propArray[current] = props[i];
							current++;
						}
					}

					// return the filtered collection
					//
					return new PropertyDescriptorCollection( propArray );
				}

				return props;
			}

			// JJD 2/4/02 - UWG578
			// Filter out properties not meaningful to combos and dropdowns
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//private bool FilterOutProperty( PropertyDescriptor pd )
			private static bool FilterOutProperty( PropertyDescriptor pd )
			{
				switch ( pd.DisplayName )
				{
					case "AllowGroupBy":
					case "AutoEdit":
					case "AutoSizeEdit":
					case "ButtonDisplayStyle":
					case "CellActivation":
					case "CellButtonAppearance":
					case "HiddenWhenGroupBy":
					case "MaskClipMode":
					case "MaskDataMode":
					case "MaskDisplayMode":
					case "MaskInput":
					case "MaskLiteralsAppearance":
					case "MaxDate":
					case "MinDate":
					case "MaxValue":
					case "MinValue":
					case "Nullable":
					case "PromptChar":
					case "TabStop":
					case "ValueList":
					case "VertScrollBar":
					// JAS 2005 v2 XSD Support
					// -----------------------
					case "FieldLenMin":
					case "MaxValueExclusive":
					case "MinValueExclusive":
					case "RegexPattern":
					// -----------------------
					// SSP 5/16/05 - Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					// Added following entries.
					//
					// ------------------------------------------------------------------------
					case "FilterCellAppearance":
					case "FilterCellAppearanceActive":
					case "FilterClearButtonAppearance":
					case "FilterClearButtonLocation":
					case "FilterEvaluationTrigger":
					case "FilterOperandStyle":
					case "FilterOperatorAppearance":
					case "FilterOperatorDefaultValue":
					case "FilterOperatorDropDownItems":
					case "FilterOperatorLocation":
					case "AllowRowSummaries":
					// ------------------------------------------------------------------------
					// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
					case "CellClickAction":
					{
						return true;
					}
				}

				return false;
			}
		}
		
		#endregion UltraGridColumnTypeConverter

		#region OwnerInfo

		/// <summary>
		/// Returns an object that can be used to information about a value from the owner.
		/// </summary>
		/// <remarks>This is an instance of a class that the owner derives from EditorOwnerInfoBase.</remarks>
		internal Infragistics.Win.UltraWinGrid.EmbeddableEditorOwnerInfo EditorOwnerInfo 
		{ 
			get
			{
				if ( null == this.editorOwnerInfo )
				{
					this.editorOwnerInfo = new Infragistics.Win.UltraWinGrid.EmbeddableEditorOwnerInfo( this );
				}

				return this.editorOwnerInfo;
			}
		}

		#endregion //OwnerInfo
					
		#region GetDefaultEditorWithCombo

		internal EditorWithCombo GetDefaultEditorWithCombo( )
		{
			if ( null == this.defaultEditorWithCombo )
			{				
				this.defaultEditorWithCombo = new EditorWithCombo( );
			}

			return this.defaultEditorWithCombo;
		}

		#endregion // GetDefaultEditorWithCombo

		#region GetCachedEditor

		// SSP 8/8/05 - NAS 5.3 New Column Styles
		// 
		private EmbeddableEditorBase GetCachedEditor( Type desiredEditorType, ColumnStyle style,
			out Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings defOwnerSettings )
		{
			EmbeddableEditorBase recycleEditor = this.lastCachedStyleEditor;

			if ( null != recycleEditor && style == this.lastCachedStyle 
				&& recycleEditor.GetType( ) == desiredEditorType )
			{
				Infragistics.Win.UltraWinEditors.DefaultEditorOwner tmpDefOwner = recycleEditor.DefaultOwner as Infragistics.Win.UltraWinEditors.DefaultEditorOwner;
				if ( null != tmpDefOwner && null != tmpDefOwner.Settings )
				{
					defOwnerSettings = tmpDefOwner.Settings;
					return recycleEditor;
				}
			}

			EmbeddableEditorBase editor = null;
			defOwnerSettings = new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings( );
			Infragistics.Win.UltraWinEditors.DefaultEditorOwner defOwner = 
				new Infragistics.Win.UltraWinEditors.DefaultEditorOwner( defOwnerSettings );

			if ( typeof( Infragistics.Win.DateTimeEditor ) == desiredEditorType )
				editor = new Infragistics.Win.DateTimeEditor( defOwner );

			else if ( typeof( Infragistics.Win.EditorWithMask ) == desiredEditorType )
				editor = new Infragistics.Win.EditorWithMask( defOwner );

			else if ( typeof( Infragistics.Win.FontNameEditor ) == desiredEditorType )
				editor = new Infragistics.Win.FontNameEditor( defOwner );

			else if ( typeof( Infragistics.Win.ColorPickerEditor ) == desiredEditorType )
				editor = new Infragistics.Win.ColorPickerEditor( defOwner );

			else if ( typeof( Infragistics.Win.EmbeddableImageRenderer ) == desiredEditorType )
				editor = new Infragistics.Win.EmbeddableImageRenderer( defOwner );

			else if ( typeof( Infragistics.Win.TimeZoneEditor ) == desiredEditorType )
				editor = new Infragistics.Win.TimeZoneEditor( defOwner );

			else if ( typeof( Infragistics.Win.EditorWithCombo ) == desiredEditorType )
				editor = new Infragistics.Win.EditorWithCombo( defOwner );
            
			// MRS 1/11/06 - BR08740 NAS 2006 Vol. 1
			// Added two new styles to use the FormattedLinkRenderer
			//
			else if ( typeof( Infragistics.Win.FormattedLinkLabel.FormattedLinkEditor ) == desiredEditorType )
				// SSP 7/31/06 - NAS 6.3
				// Obsoleted old overloads and added new ones that take supportsEditing parameter explicitly.
				// 
				//editor = new Infragistics.Win.FormattedLinkLabel.FormattedLinkEditor( defOwner );
				editor = new Infragistics.Win.FormattedLinkLabel.FormattedLinkEditor( ColumnStyle.FormattedTextEditor == style, defOwner );
            // MRS 12/3/2007 - NAS v8.1
            else if (typeof(Infragistics.Win.TrackBarEditor) == desiredEditorType)
                editor = new Infragistics.Win.TrackBarEditor(defOwner);

			this.lastCachedStyleEditor = editor;
			this.lastCachedStyle = style;

			Debug.Assert( null != editor );

			return editor;
		}

		#endregion // GetCachedEditor

		#region GetDefaultEditor

		// SSP 5/1/03 - Cell Level Editor
		// Added Editor and ValueList properties off the cell so the user can set the editor
		// and value list on a per cell basis.
		// Added row parameter to the GetDefaultEditor method so we can take into
		// account the valuelist property off the row if any.
		internal EmbeddableEditorBase GetDefaultEditor( UltraGridRow row )
		{
			// SSP 1/27/05 BR01987
			// Honor style on formula columns. Moved this below.
			//
			

            // MBS 4/8/09 - TFS16454
            // We should be looking at the underlying type when determining which editor to use
            //
			//Type dataType = this.DataType;
            Type dataType = Utilities.GetUnderlyingType(this.DataType);

			Debug.Assert( null != this.Layout, "Null layout !" );

			// SSP 7/17/02 UWG1401
			// Take into account column style settings to determine the default editor.
			//
			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//ColumnStyle style = this.StyleResolved;
			ColumnStyle style = this.GetStyleResolved( row );

			// SSP 12/11/02
			// If the column style is Default, then don't base the editor on the column style.
			//
			if ( ColumnStyle.Default == this.style && ColumnStyle.Edit == style )
				style = ColumnStyle.Default;

			EmbeddableEditorBase editor = null;

			switch ( style )
			{
				case ColumnStyle.EditButton:
				case ColumnStyle.Edit:
					// SSP 10/8/02 UWG1739
					// Only use masked edit if the cell is not multiline.
					// Added !this.IsCellMultiLine clause.
					//
					//editor = this.HasMask 
					editor = this.HasMask && !this.IsCellMultiLine 
						? (EmbeddableEditorBase)this.Layout.EditorWithMask 
						: (EmbeddableEditorBase)this.Layout.EditorWithText;
					break;
				case ColumnStyle.Button:
					break;
				case ColumnStyle.TriStateCheckBox:					
					editor = this.Layout.CheckEditorTriState;
					break;
				case ColumnStyle.CheckBox:
					editor = this.Layout.CheckEditor;
					break;
				case ColumnStyle.DropDownCalendar:
					editor = this.Layout.DateTimeEditor;
					break;
					// For below styles fall through
					//
				case ColumnStyle.Default:
				case ColumnStyle.DropDown:
				case ColumnStyle.DropDownList:
				case ColumnStyle.DropDownValidate:
					break;

				// SSP 8/8/05 - NAS 5.3 New Column Styles
				// 
				// ------------------------------------------------------------------------
				default:
				{
					Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings editorSettings;

					switch ( style )
					{
						case ColumnStyle.Date:
						case ColumnStyle.DateWithoutDropDown:
						case ColumnStyle.DateWithSpin:
						case ColumnStyle.Time:
						case ColumnStyle.TimeWithSpin:
						case ColumnStyle.DateTime:
						case ColumnStyle.DateTimeWithoutDropDown:
						case ColumnStyle.DateTimeWithSpin:
						{
							editor = this.GetCachedEditor( typeof( Infragistics.Win.DateTimeEditor ), style, out editorSettings );
							Infragistics.Win.DateTimeEditor de = (Infragistics.Win.DateTimeEditor)editor;

							// Reset the properties in case we are reusing the editor.
							// 
							de.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
							de.SetSpinButtonDisplayStyle( Infragistics.Win.ButtonDisplayStyle.Never );
							de.SpinButtonAlignment = ButtonAlignment.Right;
							editorSettings.MaskInput = null;

							if ( ColumnStyle.DateTime == style
								|| ColumnStyle.DateTimeWithoutDropDown == style
								|| ColumnStyle.DateTimeWithSpin == style )
								editorSettings.MaskInput = "{date} {time}";

							if ( ColumnStyle.Time == style || ColumnStyle.TimeWithSpin == style )
								editorSettings.MaskInput = "{time}";

							if ( ColumnStyle.DateWithoutDropDown == style
								|| ColumnStyle.DateTimeWithoutDropDown == style 
								|| ColumnStyle.Time == style
								|| ColumnStyle.TimeWithSpin == style )
								de.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;

							if ( ColumnStyle.DateWithSpin == style || ColumnStyle.TimeWithSpin == style
								|| ColumnStyle.DateTimeWithSpin == style )
							{
								de.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;

								de.SetSpinButtonDisplayStyle( Infragistics.Win.ButtonDisplayStyle.Always );
								de.SpinButtonAlignment = ButtonAlignment.Right;
							}

							break;
						}
						case ColumnStyle.Double:
						case ColumnStyle.DoublePositive:
						case ColumnStyle.DoubleNonNegative:
						case ColumnStyle.DoubleWithSpin:
						case ColumnStyle.DoublePositiveWithSpin:
						case ColumnStyle.DoubleNonNegativeWithSpin:
						{
							editor = this.GetCachedEditor( typeof( Infragistics.Win.EditorWithMask ), style, out editorSettings );
							Infragistics.Win.EditorWithMask me = (Infragistics.Win.EditorWithMask)editor;

							bool allowNegatives = ColumnStyle.Double == style || ColumnStyle.DoubleWithSpin == style;
							editorSettings.MaskInput = "{double:" + ( allowNegatives ? "-" : "" ) + "9}";

							// Reset the editor properties in case we are reusing the editor.
							// 
							me.SpinButtonDisplayStyle = SpinButtonDisplayStyle.None;
							editorSettings.ValueConstraint = null;

							if ( ColumnStyle.DoublePositive == style || ColumnStyle.DoublePositiveWithSpin == style )
								editorSettings.ValueConstraint.MinExclusive = 0.0;
							else if ( ColumnStyle.DoubleNonNegative == style || ColumnStyle.DoubleNonNegativeWithSpin == style )
								editorSettings.ValueConstraint.MinInclusive = 0.0;

							if ( ColumnStyle.DoubleWithSpin == style 
								|| ColumnStyle.DoublePositiveWithSpin == style 
								|| ColumnStyle.DoubleNonNegativeWithSpin == style )
								me.SpinButtonDisplayStyle = SpinButtonDisplayStyle.OnRight;

							break;
						}
						case ColumnStyle.Integer:
						case ColumnStyle.IntegerPositive:
						case ColumnStyle.IntegerNonNegative:
						case ColumnStyle.IntegerWithSpin:
						case ColumnStyle.IntegerPositiveWithSpin:
						case ColumnStyle.IntegerNonNegativeWithSpin:
						{
							editor = this.GetCachedEditor( typeof( Infragistics.Win.EditorWithMask ), style, out editorSettings );
							Infragistics.Win.EditorWithMask me = (Infragistics.Win.EditorWithMask)editor;

							bool allowNegatives = ColumnStyle.Integer == style || ColumnStyle.IntegerWithSpin == style;
							editorSettings.MaskInput = ( allowNegatives ? "-" : "" ) + "nnnnnnnnnn";

							// Reset the editor properties in case we are reusing the editor.
							// 
							me.SpinButtonDisplayStyle = SpinButtonDisplayStyle.None;
							editorSettings.ValueConstraint = null;

							if ( ColumnStyle.IntegerPositive == style || ColumnStyle.IntegerPositiveWithSpin == style )
								editorSettings.ValueConstraint.MinExclusive = 0;
							else if ( ColumnStyle.IntegerNonNegative == style || ColumnStyle.IntegerNonNegativeWithSpin == style )
								editorSettings.ValueConstraint.MinInclusive = 0;

							if ( ColumnStyle.IntegerWithSpin == style 
								|| ColumnStyle.IntegerPositiveWithSpin == style 
								|| ColumnStyle.IntegerNonNegativeWithSpin == style )
								me.SpinButtonDisplayStyle = SpinButtonDisplayStyle.OnRight;

							break;
						}
						case ColumnStyle.Currency:
						case ColumnStyle.CurrencyPositive:
						case ColumnStyle.CurrencyNonNegative:
						{
							editor = this.GetCachedEditor( typeof( Infragistics.Win.EditorWithMask ), style, out editorSettings );
							Infragistics.Win.EditorWithMask me = (Infragistics.Win.EditorWithMask)editor;

							bool allowNegatives = ColumnStyle.Currency == style;
							editorSettings.MaskInput = "{currency:" + ( allowNegatives ? "-" : "" ) + "9}";

							// Reset the editor properties in case we are reusing the editor.
							// 
							me.SpinButtonDisplayStyle = SpinButtonDisplayStyle.None;
							editorSettings.ValueConstraint = null;

							if ( ColumnStyle.CurrencyPositive == style )
								editorSettings.ValueConstraint.MinExclusive = 0.0m;
							else if ( ColumnStyle.CurrencyNonNegative == style )
								editorSettings.ValueConstraint.MinInclusive = 0.0m;

							break;
						}
						case ColumnStyle.Color:
						{
							editor = this.GetCachedEditor( typeof( Infragistics.Win.ColorPickerEditor ), style, out editorSettings );
							break;
						}
						case ColumnStyle.Image:
						{
							editor = this.GetCachedEditor( typeof( Infragistics.Win.EmbeddableImageRenderer ), style, out editorSettings );
							EmbeddableImageRenderer imageRenderer = (EmbeddableImageRenderer)editor;
							imageRenderer.DrawBorderShadow = false;
							break;
						}
						case ColumnStyle.ImageWithShadow:
						{
							editor = this.GetCachedEditor( typeof( Infragistics.Win.EmbeddableImageRenderer ), style, out editorSettings );
							break;
						}
						case ColumnStyle.TimeZone:
						{
							editor = this.GetCachedEditor( typeof( Infragistics.Win.TimeZoneEditor ), style, out editorSettings );
							break;
						}
						case ColumnStyle.Font:
						{
							editor = this.GetCachedEditor( typeof( Infragistics.Win.FontNameEditor ), style, out editorSettings );
							break;
						}

						// MRS 1/11/06 - BR08740 NAS 2006 Vol. 1
						// Added two new styles to use the FormattedLinkRenderer
						//
						case ColumnStyle.FormattedText:
						case ColumnStyle.URL:
						// SSP 9/7/06 - NAS 6.3
						// Added FormattedTextEditor style.
						// 
						case ColumnStyle.FormattedTextEditor:
						{
							editor = this.GetCachedEditor( typeof( FormattedLinkLabel.FormattedLinkEditor ), style, out editorSettings );

							bool isURL = ColumnStyle.URL == style;
							FormattedLinkLabel.FormattedLinkEditor fle = (FormattedLinkLabel.FormattedLinkEditor)editor;
							
							if (isURL)
								fle.TreatValueAs = FormattedLinkLabel.TreatValueAs.URL;
							else
								fle.TreatValueAs = FormattedLinkLabel.TreatValueAs.FormattedText;

							break;
						}
                        // MRS 12/3/2007 - NAS v8.1
                        case ColumnStyle.TrackBar:
                        {
                            editor = this.GetCachedEditor(typeof(Infragistics.Win.TrackBarEditor), style, out editorSettings);
                            break;
                        }
					}

					break;
				}
				// ------------------------------------------------------------------------
			}

			// If the editor was assigned in above switch statemenet
			// then return that editor.
			//
			if ( null != editor )
				return editor;

			// SSP 1/27/05 BR01987
			// Honor style on formula columns. Moved this here from above.
			// If the column has a formula then make use the EditorWithText to display
			// the results of the formula evaluations because the results of the formula
			// evaluations most likely double or strings.
			//
			// --------------------------------------------------------------------------
			if ( this.HasActiveFormula )
				return this.Layout.EditorWithText;
			// --------------------------------------------------------------------------

			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//if ( null != this.ValueList )
			if ( null != this.GetValueList( row ) )
			{
				editor = this.GetDefaultEditorWithCombo( );
			} 
				// SSP 4/25/03 UWG1739
				// Don't use the masked editor if the cell is multiline.
				//
				//else if ( this.HasMask )
			else if ( this.HasMask && ! this.IsCellMultiLine )
			{
				editor = this.Layout.EditorWithMask;
			}
			else if ( typeof( string ) == dataType )
			{
				editor = this.Layout.EditorWithText;
			}
			else if ( typeof( bool ) == dataType )
			{
				editor = this.Layout.CheckEditor;
			}
			else if ( typeof( DateTime ) == dataType )
			{
				editor = this.Layout.DateTimeEditor;
			}
			else if ( typeof( System.Drawing.Color ) == dataType )
			{
				editor = this.Layout.ColorPickerEditor;
			}
				// SSP 7/17/02 UWG1361
				// By default for rest of the column data types to use the editor with text
				// including numeric columns.
				//
				
				// SSP 8/11/03 UWG2477
				// If the data type is an image, then retrun an image renderer.
				//
			// MD 8/3/07 - 7.3 Performance
			// IsAssignableFrom is faster than IsSubclassOf
			//else if ( typeof( System.Drawing.Image ) == dataType || dataType.IsSubclassOf( typeof( System.Drawing.Image ) ) )
			else if ( typeof( Image ) == dataType || typeof( Image ).IsAssignableFrom( dataType ) )
			{
				return this.Layout.ImageRenderer;
			}
			else
			{
				// If there is no editor for the dataType, then return an editor with text.
				//
				editor = this.Layout.EditorWithText;
			}

			return editor;
		}

		#endregion // GetDefaultEditor

		#region DirtyDefaultEditorSettings

		internal void DirtyDefaultEditor( )
		{
			this.defaultEditorDirtyFlag = true;
			this.verifyEditorVersionNumber++;

			// SSP 1/31/05
			// Release the reference to the editor instance.
			//
			this.lastEmbeddableEditor = null;

			if ( null != this.Layout )
			{
				// Bump the cell child elements cache version and dirty
				// the grid so the new editor is reflected in cells.
				//
				this.Layout.BumpCellChildElementsCacheVersion( );
				this.Layout.DirtyGridElement( );
			}
		}

		#endregion

		#region IsEditorAppropriate

		internal bool IsEditorAppropriate( EmbeddableEditorBase editor )
		{
			// SSP 10/9/02 UWG1744
			// What happens is that the designer writes out code in such a way
			// that a column's EditorControl or Editor property is assigned before
			// the column's DataType is initialized in the grid. This causes a
			// problem because the EditorControl or Editor properties will
			// throw an exception if the editor doesn't support the column's
			// data type (since it hasn't been initialized, it would be string
			// by default). To prevent that, we have to not throw the exception if
			// we are in such a state.
			//
			if ( null == this.Band || null == this.Band.Layout )
				return true;

			if ( null != editor )
			{
				// SSP 9/16/03 UWG2512
				// Added support for object column types. Use the EmbeddableEditorOwnerInfo.GetDataType
				// to get the data type. It will return column's data type if it's not an object type
				// otherwise it will return the type provided by the base implementation.
				// 
				// --------------------------------------------------------------------------------
				//if ( editor.CanRenderType( this.DataType ) )
				//	return true;
				Type dataType = this.DataType;
				
				if ( typeof( object ) == dataType && null != editor.DefaultOwner )
				{
					dataType = editor.DefaultOwner.GetDataType( null );

					if ( null == dataType )
						dataType = this.DataType;
				}
				
                if ( editor.CanRenderType( dataType ) )
					return true;
				// --------------------------------------------------------------------------------
			}

			return false;
		}

		#endregion // IsEditorAppropriate

		#region Editor
		
		// SSP 4/2/02
		// Added Editor property for embeddable editors

		/// <summary>
		/// Gets or sets the editor that this column will use for editing and
		/// displaying cells. By default or when it's assigned null it will get 
		/// the editor from the EditorControl property.
		/// </summary>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		  Browsable( false ) ]
		public EmbeddableEditorBase Editor
		{
			get
			{
				// If the user has assigned an embeddable editor, then return that.
				//
				if ( null != this.embeddableEditor )
					return this.embeddableEditor;

				// If the user has not assigned any embeddable editor, use a default 
				// one appropriate for this column.
				//
				if ( null == this.lastEmbeddableEditor || this.defaultEditorDirtyFlag )
				{
					// If Editor property is not set, then get the editor from
					// EditorControl property.
					//
                    // MRS - NAS 9.2 - Control Container Editor
					//IProvidesEmbeddableEditor editorProvider = this.EditorControl as IProvidesEmbeddableEditor;
                    IProvidesEmbeddableEditor editorProvider = this.EditorComponentResolved as IProvidesEmbeddableEditor;

					if ( null != editorProvider && this.IsEditorAppropriate( editorProvider.Editor ) )
						this.lastEmbeddableEditor = editorProvider.Editor;
					else
						// SSP 5/1/03 - Cell Level Editor
						// Added Editor and ValueList properties off the cell so the user can set the editor
						// and value list on a per cell basis.
						// Pass in null for the new row parameter.
						//
						//this.lastEmbeddableEditor = this.GetDefaultEditor( );
						this.lastEmbeddableEditor = this.GetDefaultEditor( null );

					this.defaultEditorDirtyFlag = false;
				}

				return this.lastEmbeddableEditor;
			}
			set
			{
				if ( this.embeddableEditor != value )
				{
					if ( null != value && !this.IsEditorAppropriate( value ) )
						throw new ArgumentException( SR.GetString("LER_Exception_336", value.GetType( ).Name), SR.GetString("LER_Exception_337") );

					EmbeddableEditorBase currentEditor = this.Editor;
					bool editorChanged = true;

					if ( null != value && currentEditor == value )
						editorChanged = false;
					if ( null == value && currentEditor == this.GetDefaultEditor( null ) )
						editorChanged = false;

					// If the editor has changed, then exit the dit mode first if a 
					// cell from this column is in edit mode. 
					//
					UltraGrid grid = this.Layout.Grid as UltraGrid;

					if ( editorChanged && null != grid )
					{
						UltraGridCell activeCell = grid.ActiveCell;
						
						if ( null != activeCell && activeCell.IsInEditMode &&
							activeCell.Column == this )
						{
							activeCell.ExitEditMode( false, true );
						}
					}

					this.embeddableEditor = value;

					// SSP 4/22/05
					// Use the DirtyDefaultEditor instead of duplicating the code. Also that method
					// resets couple of member vars.
					//
					this.DirtyDefaultEditor( );
					

                    // MRS NAS v8.3 - Unit Testing
                    this.NotifyPropChange(PropertyIds.Editor);
				}
			}
		}

		#endregion // Editor

		#region ResetEditor

		internal void ResetEditor( )
		{
			this.Editor = null;
		}

		#endregion // ResetEditor

		#region EditorControl
		
		/// <summary>
		/// Control that implements IProvidesEmbeddableEditor. Attempt to set a control that
		/// does not implement IProvidesEmbeddableEditor interface will cause an exception.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridColumnTypeConverter_P_EditorControl") ]
		//[ Editor( typeof( Infragistics.Win.UltraWinGrid.Design.ColumnEditorControlUIEditor ), typeof( UITypeEditor ) ) ]
		//[TypeConverter(typeof(NonExpandableControlConverter))]
		[ TypeConverter( typeof( Infragistics.Win.UltraWinGrid.Design.ColumnEditorControlReferenceConverter ) ) ]
        [Obsolete("This property has been deprecated; use the EditorComponent property instead.", false)] // MRS - NAS 9.2 - Control Container Editor
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public Control EditorControl
		{
			get
			{
                // MRS - NAS 9.2 - Control Container Editor
                //
                #region Old Code
                //if ( null == this.editorControl && null != this.editorControlName )
                //{
                //    // JAS 3/17/05 BR02741 - If the control exists in the UltraGrid Designer, then
                //    // we will never be able to find one of the editor controls, so just return null.
                //    //
                //    if( this.IsInDesigner )
                //        return null;

                //    Control control = this.FindEditorControlOnForm( this.editorControlName, null );

                //    Debug.Assert( null == control || 
                //        ( control is IProvidesEmbeddableEditor &&
                //        this.IsEditorAppropriate( ((IProvidesEmbeddableEditor)control).Editor ) ), 
                //                    "Returned control is not an IProvidesEmbeddableEditor !" );

                //    if ( control is IProvidesEmbeddableEditor && 
                //        this.IsEditorAppropriate( ((IProvidesEmbeddableEditor)control).Editor ) )
                //        // SSP 1/28/05
                //        // We need to hook into the editor control's Disposed so we can reset the 
                //        // EditorControl property when the editor control gets disposed.
                //        //
                //        //this.editorControl = control;
                //        this.InternalSetEditorControl( control );

                //    // Set it back to null so that we don't attempt to find the control on
                //    // the form every time this property is accessed (in case we failed).
                //    //
                //    this.editorControlName = null;
                //}

                //return this.editorControl;
                #endregion //Old Code
                //
                return this.EditorComponent as Control;
            }
			set
            {
                // MRS - NAS 9.2 - Control Container Editor
                //
                #region Old Code
                //if ( this.editorControl != value )
                //{
                //    if ( null != value && ! ( value is  IProvidesEmbeddableEditor ) )
                //        throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_316"), Shared.SR.GetString("LE_ArgumentException_317") );

                //    // SSP 7/23/03 - Don't access the editor when we are in the midst of initializing because
                //    // accessing the Editor property could cause premature cloning of the internal editor
                //    // which means that any properties set on the internal editor after the cloning won't get
                //    // copied over to the editor.
                //    // Enclosed the code in the if block.
                //    //
                //    if ( null != this.Layout && null != this.Layout.Grid &&	! this.Layout.Grid.Initializing )
                //    {
                //        IProvidesEmbeddableEditor editorProvider = value as IProvidesEmbeddableEditor;

                //        if ( null != editorProvider )
                //        {
                //            EmbeddableEditorBase editor = editorProvider.Editor;

                //            if ( !this.IsEditorAppropriate( editor ) )
                //                throw new ArgumentException( SR.GetString("LER_Exception_338", editorProvider.GetType( ).Name ), SR.GetString("LER_ArguementException_317") );
                //        }
                //    }

                //    // SSP 1/28/05
                //    // We need to hook into the editor control's Disposed so we can reset the 
                //    // EditorControl property when the editor control gets disposed.
                //    //
                //    // ----------------------------------------------------------------------
                //    /*
                //    this.editorControl = value;

                //    // Dirty the default editor so Editor property regets the editor
                //    // from newly assigned editor control.
                //    // 
                //    this.DirtyDefaultEditor( );
                //    */
                //    this.InternalSetEditorControl( value );
                //    // ----------------------------------------------------------------------
                //}
                #endregion //Old Code
                //
                this.EditorComponent = value;
            }            
        }
		
		#endregion // EditorControl

		#region InternalSetEditorControl

		// SSP 1/28/05
		// We need to hook into the editor control's Disposed so we can reset the 
		// EditorControl property when the editor control gets disposed.
		//
		private void InternalSetEditorControl( Control control )
		{
			if ( null != this.editorControl )
				this.editorControl.Disposed -= new System.EventHandler( this.OnEditorControlDisposed );

			this.editorControl = control;

			if ( null != this.editorControl )
				this.editorControl.Disposed += new System.EventHandler( this.OnEditorControlDisposed );

			this.DirtyDefaultEditor( );
		}

		#endregion // InternalSetEditorControl

		#region OnEditorControlDisposed

		// SSP 1/28/05
		// We need to hook into the editor control's Disposed so we can reset the 
		// EditorControl property when the editor control gets disposed.
		//
		private void OnEditorControlDisposed( object sender, System.EventArgs e )
		{
			this.ResetEditorControl( );
		}

		#endregion // OnEditorControlDisposed

		#region FindEditorControlOnForm

		// SSP 8/20/02
		// Added FindEditorControlOnForm method for use with serializing EditorControl property.
		//
		private Control FindEditorControlOnForm( string name, System.Windows.Forms.Control.ControlCollection controls )
		{
			try
			{
				// If no controls collection was passed in then get the form's controls child controls.
				//
				if ( null == controls )
				{				
					// SSP 12/28/05 BR08374
					// If the grid is part of a user control at design time then there won't be any form.
					// Search the user control instead.
					// 
					// --------------------------------------------------------------------------------------
					
					Control rootCtrl = GridUtils.GetRootControlForSearching( null != this.Layout ? this.Layout.Grid : null );
					if ( null != rootCtrl )
						controls = rootCtrl.Controls;
					// --------------------------------------------------------------------------------------
				}

				if ( null != controls )
				{
					foreach ( Control control in controls )
					{
						if ( control is IProvidesEmbeddableEditor && control.Name.Equals( name ) )
						{
							return control;
						}
						else
						{
							// Traverse through the hierarchy.
							//
							// SSP 12/28/05 BR08374
							// Added a check for control.Controls being null.
							// 
							System.Windows.Forms.Control.ControlCollection childControls = control.Controls;
							if ( null != childControls )
							{
								Control tmpControl = this.FindEditorControlOnForm( name, childControls );

								if ( null != tmpControl )
									return tmpControl;
							}
						}
					}
				}
			}
			catch (Exception)
			{
			}

			return null;
		}

		#endregion // FindEditorControlOnForm

		#region ShouldSerializeEditorControl
	
		/// <summary>
		/// Returns true if property needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeEditorControl( ) 
		{
            return null != this.editorControl || 
                // MBS 6/15/09 - TFS18459
                // If we never did anything that forced us to recreate the EditorControl
                // from the editorControlName, then if we try to save the layout, we'll
                // lose the editorControlName
                !String.IsNullOrEmpty(this.editorControlName);
		}

		#endregion // ShouldSerializeEditorControl

		#region ResetEditorControl
	
		/// <summary>
		/// Returns true if property needs to be serialized
		/// </summary>
		/// <returns></returns>
		protected void ResetEditorControl( ) 
		{
			// SSP 1/28/05
			// We need to hook into the editor control's Disposed so we can reset the 
			// EditorControl property when the editor control gets disposed.
			//
			//this.editorControl = null;
			this.InternalSetEditorControl( null );
		}

		#endregion // ResetEditorControl

		#region Implementation of IValueListsCollectionProvider
	
		// SSP 8/19/02
		// Implemented IValueListsCollectionProvider interface.
		//

		/// <summary>
		/// Returns the grid control.
		/// </summary>
		System.Windows.Forms.Control IValueListsCollectionProvider.Control
		{
			get
			{
				return null != this.Band && null != this.Band.Layout ? this.Band.Layout.Grid : null;
			}
		}

		/// <summary>
		/// Returns the value lists off the display layout if one has been created. Null otherwise.
		/// </summary>
		Infragistics.Win.ValueListsCollection IValueListsCollectionProvider.ValueLists
		{
			get
			{
				return null != this.Band && null != this.Band.Layout && this.Band.Layout.HasValueLists ? this.Band.Layout.ValueLists : null;
			}
		}

		#endregion // IValueListsCollectionProvider

		#region CellChildElementsCacheVersion

		internal int CellChildElementsCacheVersion
		{
			get
			{
				return this.cellChildElementsCacheVersion + this.band.Layout.CellChildElementsCacheVersion;
			}
		}

		#endregion // CellChildElementsCacheVersion

		#region BumpCellChildElementsCacheVersion

		internal void BumpCellChildElementsCacheVersion( )
		{
			this.cellChildElementsCacheVersion++;
		}

		#endregion // BumpCellChildElementsCacheVersion

		
		#region AutoSizeMode

		// SSP 4/14/03
		// Added ColumnAutoSizeMode property.
		//
		/// <summary>
		/// Specifies column autosizing mode. If <see cref="UltraGridOverride.AllowColSizing"/> is set to None, then this is ignored.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// When set the None, the user is not allowed to auto resize a column. If set to VisibleRows, then the column is resized based on the data in the visible rows. If set to SiblingRowsOnly, then the column is autoresized based on data in all the sibling rows. If set to AllRowsInBand, then the column is auto resized based on data in all rows in the band.
		/// </p>
		/// <seealso cref="UltraGridOverride.AllowColSizing"/>
		/// <seealso cref="UltraGridColumn.AutoSizeMode"/>
		/// </remarks>
		public ColumnAutoSizeMode AutoSizeMode
		{
			get
			{
				return this.autoSizeMode;			
			}
			set
			{
				if ( this.autoSizeMode != value )
				{
					if ( ! Enum.IsDefined( typeof( ColumnAutoSizeMode ), value ) )
						throw new InvalidEnumArgumentException( "AutoSizeMode", (int)value, typeof( ColumnAutoSizeMode ) );

					this.autoSizeMode = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ColumnAutoSizeMode );
				}
			}
		}

		#endregion // AutoSizeMode
		
		#region ResetAutoSizeMode

		// SSP 4/14/03
		// Added ColumnAutoSizeMode property.
		//
		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetAutoSizeMode( )
		{
			this.AutoSizeMode = ColumnAutoSizeMode.Default;
		}

		#endregion // ResetAutoSizeMode

		#region ShouldSerializeAutoSizeMode

		// SSP 4/14/03
		// Added ColumnAutoSizeMode property.
		//
		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAutoSizeMode( )
		{
			return ColumnAutoSizeMode.Default != this.AutoSizeMode;
		}

		#endregion // ShouldSerializeAutoSizeMode

		#region AutoSizeModeResolved
		
		// SSP 4/14/03
		// Added AutoSizeMode property.
		//		
		internal ColumnAutoSizeMode AutoSizeModeResolved
		{
			get
			{
				// If the column can't be resized, then auto-resizing the column is out of
				// the question.
				//
				if ( ! this.Header.AllowSizing )
					return ColumnAutoSizeMode.None;

				if ( this.ShouldSerializeAutoSizeMode( ) )
					return this.AutoSizeMode;

				return this.Band.ColumnAutoSizeModeResolved;			
			}
		}

		#endregion // AutoSizeModeResolved
		
		// SSP 1/30/03 - Row Layout Functionality
		//
		#region Row Layout Code

		#region RowLayoutColumnInfo

		/// <summary>
		/// Gets the instance of RowLayoutColumnInfo associated with this column.
		/// </summary>
		/// <remarks>
		/// <p>You can set various properties in the returned object to customize where the cell associated with this column shows up.</p>
		/// <p>By default all the columns are visible. Set <see cref="Hidden"/> to true to hide the column.</p>
		/// <p>Set <see cref="UltraGridBand.UseRowLayout"/> property to true to turn on the row-layout functionality.</p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ]
		public Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo RowLayoutColumnInfo
		{
			get
			{
				if ( null == this.rowLayoutColumnInfo )
				{
					this.rowLayoutColumnInfo = new ColumnLayoutInfo( this );

					this.rowLayoutColumnInfo.SubObjectPropChanged += this.SubObjectPropChangeHandler;

					Debug.WriteLineIf( ! this.IsChaptered, "RowLayoutColumnInfo accessed on a chaptered column." );
				}

				return this.rowLayoutColumnInfo;
			}
		}

		#endregion // RowLayoutColumnInfo

		#region HasRowLayoutColumnInfo

		internal bool HasRowLayoutColumnInfo
		{
			get
			{
				return null != this.rowLayoutColumnInfo;
			}
		}

		#endregion // HasRowLayoutColumnInfo

		#region ShouldSerializeRowLayoutColumnInfo

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowLayoutColumnInfo( )
		{
			return null != this.rowLayoutColumnInfo && this.rowLayoutColumnInfo.ShouldSerialize( );
		}

		#endregion // ShouldSerializeRowLayoutColumnInfo

		#region ResetRowLayoutColumnInfo
		
		/// <summary>
		/// Resets the RowLayoutColumnInfo property to its default value.
		/// </summary>
		public void ResetRowLayoutColumnInfo( )
		{
			if ( null != this.rowLayoutColumnInfo )
				this.rowLayoutColumnInfo.Reset( );
		}

		#endregion // ResetRowLayoutColumnInfo

		#region IsVisibleInLayout
		
		/// <summary>
		/// Indicates whether this column is visible in row-layout mode.
		/// </summary>
		/// <remarks>
		/// <p class="body">Set <see cref="Hidden"/> to true to hide a column.</p>
		/// </remarks>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]	
		public bool IsVisibleInLayout
		{
			get
			{
				// SSP 7/25/03
				// Use the Hidden property rather than the hidden variable because the Hidden
				// property also looks at the GroupByColumnsHidden settings and return true if
				// the column is a group-by column. Also Hidden property checks for IsChaptered
				// so we don't need to check it here.
				//
				//return ! this.IsChaptered && ! this.hidden;
				// MD 1/15/09 - Groups in RowLayouts
				// Use the resolved value, which also checks an owning group's hidden value.
				//return ! this.Hidden;
				return ( this.HiddenResolved == false );
			}
		}

		#endregion // IsVisibleInLayout

		#region MinimumSize
		
		internal Size MinimumSize
		{
			get
			{
				Size size = this.RowLayoutColumnInfo.MinimumCellSize;

				if ( size.Width <= 0 )
					size.Width = this.MinWidthActual;

				if ( size.Height <= 0 )
				{
					size.Height = this.CardCellHeight;

					if ( this.Band.CardView && this.Band.AreColumnHeadersInSeparateLayoutArea )
						size.Height = Math.Max( size.Height, this.CardLabelSize.Height );
				}

				return size;
			}
		}

		#endregion // MinimumSize

		#region MaximumSize

		// SSP 10/26/04 UWG3729
		// Restrict the user from resizing layout items greater than the max width. Added 
		// MaximumSize property.
		//
		internal Size MaximumSize
		{
			get
			{
				return new Size( this.MaxWidth, 0 );
			}
		}

		#endregion // MaximumSize

		#region PreferredSize

		internal Size PreferredSize
		{
			get
			{
				Size size = this.RowLayoutColumnInfo.PreferredCellSize;

				if ( size.Width <= 0 )
					size.Width = this.Width;

				if ( size.Height <= 0 )
				{
					size.Height = this.CardCellHeight;

					if ( this.Band.CardView && this.Band.AreColumnHeadersInSeparateLayoutArea )
						size.Height = Math.Max( size.Height, this.CardLabelSize.Height );
				}

				// SSP 6/30/03 UWG2375
				// Return the greater of preferred size and the minimum size.
				//
				Size minSize = this.RowLayoutColumnInfo.MinimumCellSize;
				size.Width = Math.Max( size.Width, minSize.Width );
				size.Height = Math.Max( size.Height, minSize.Height );

				return size;
			}
		}

		#endregion // PreferredSize

		#region Implementation of ILayoutItem

		#region ILayoutItem.MinimumSize
		
		Size ILayoutItem.MinimumSize
		{
			get
			{
				// SSP 11/17/04
				// Added a way to hide the header or the cell of a column in the row layout mode.
				// This way you can do things like hide a row of headers or add unbound columns
				// and hide their cells to show grouping headers etc...
				//
				if ( LabelPosition.LabelOnly == this.RowLayoutColumnInfo.LabelPositionResolved )
				{
					// SSP 8/16/05 BR05387
					// In order to make sure that the widths are distributed among logical columns
					// in the same manner in both the header layout manager and the cell layout
					// manager when LabelPosition is as such that either the cells or the headers
					// are hidden, then make sure one of the width or height dimensions are the
					// same depending on card-view or non-card-view.
					// 
					//return new Size( 0, 0 );
					return LayoutUtility.AccountForLabelPosition( this.Band, ((ILayoutItem)this.Header).MinimumSize );
				}

				return this.MinimumSize;
			}
		}

		#endregion // ILayoutItem.MinimumSize

		#region ILayoutItem.PreferredSize

		Size ILayoutItem.PreferredSize
		{
			get
			{
				// SSP 11/17/04
				// Added a way to hide the header or the cell of a column in the row layout mode.
				// This way you can do things like hide a row of headers or add unbound columns
				// and hide their cells to show grouping headers etc...
				//
				if ( LabelPosition.LabelOnly == this.RowLayoutColumnInfo.LabelPositionResolved )
				{
					// SSP 8/16/05 BR05387
					// In order to make sure that the widths are distributed among logical columns
					// in the same manner in both the header layout manager and the cell layout
					// manager when LabelPosition is as such that either the cells or the headers
					// are hidden, then make sure one of the width or height dimensions are the
					// same depending on card-view or non-card-view.
					// 
					//return new Size( 0, 0 );
					return LayoutUtility.AccountForLabelPosition( this.Band, ((ILayoutItem)this.Header).PreferredSize );
				}

				return this.PreferredSize;
			}
		}

		#endregion // ILayoutItem.PreferredSize

		#region ILayoutItem.IsVisible

		/// <summary>
		/// Indicates whether this column is visible.
		/// </summary>
		bool ILayoutItem.IsVisible
		{
			get
			{
				if ( ! this.IsVisibleInLayout )
					return false;
				
				Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutColumnInfo;

				if ( ! this.Band.AreColumnHeadersInSeparateLayoutArea && ci.LabelSpanResolved > 0 )
				{
					LabelPosition labelPosition = ci.LabelPositionResolved;
					if ( LabelPosition.Left == labelPosition || LabelPosition.Right == labelPosition )
					{
						if ( ci.SpanXResolved - ci.LabelSpanResolved <= 0 )
							return false;
					}
					else if ( LabelPosition.Top == labelPosition || LabelPosition.Bottom == labelPosition )
					{
						if ( ci.SpanYResolved - ci.LabelSpanResolved <= 0 )
							return false;
					}
				}

				return true;
			}
		}

		#endregion // ILayoutItem.IsVisible
		
		#endregion

		#region IGridBagConstraint interface implementation

		#region Anchor

		/// <summary>
		/// If the display area of the item is larger than the item, this property indicates where to anchor the item.
		/// </summary>
		AnchorType IGridBagConstraint.Anchor 
		{ 
			get
			{
				return AnchorType.Center;
			}
		}

		#endregion // Anchor

		#region Fill

		/// <summary>
		/// <p>Fill indicates whether to resize the item to fill the extra spance if the layout item's display area is larger than its size,</p> 
		/// </summary>
		FillType IGridBagConstraint.Fill 
		{ 
			get
			{
				return Infragistics.Win.Layout.FillType.Both;
			}
		}

		#endregion // Fill

		#region Insets

		/// <summary>
		/// Indicates the padding around the layout item.
		/// </summary>
		Insets IGridBagConstraint.Insets 
		{ 
			get
			{
				// Return an empty inset which is what the column's RowLayoutColumnInfo retruns.
				//
				// SSP 12/1/03 UWG2641
				// Added CellInsets and LabelInsets properties.
				//
				//return this.RowLayoutColumnInfo.Insets;
				return this.RowLayoutColumnInfo.CellInsets;
			}
		}

		#endregion // Insets

		#region OriginX

		/// <summary>
		/// <p>OriginX and OriginY define where the layout item will be placed in the virtual grid of the grid-bag layout. OriginX specifies the location horizontally while specifies the location vertically. These locations are the coordinates of the cells in the virtual grid that the grid-bag layout represents.</p>
		/// <p>The leftmost cell has OriginX of 0. The constant <see cref="GridBagConstraintConstants.Relative"/> specifies that the item be placed just to the right of the item that was added to the layout manager just before this item was added. </p>
		/// <p>The default value is <see cref="GridBagConstraintConstants.Relative"/>. OriginX should be a non-negative value.</p>
		/// </summary>
		int IGridBagConstraint.OriginX
		{ 
			get
			{
				Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutColumnInfo;

				if ( ! this.Band.AreColumnHeadersInSeparateLayoutArea )
				{
					if ( LabelPosition.Left == ci.LabelPositionResolved )
						return ci.OriginXResolved + ci.LabelSpanResolved;
				}

				return ci.OriginXResolved;
			}
		}

		#endregion // OriginX

		#region OriginY

		/// <summary>
		/// <p>OriginX and OriginY define where the layout item will be placed in the virtual grid of the grid-bag layout. OriginX specifies the location horizontally while specifies the location vertically. These locations are the coordinates of the cells in the virtual grid that the grid-bag layout represents.</p>
		/// <p>The topmost cell has OriginY of 0. The constant <see cref="GridBagConstraintConstants.Relative"/> specifies that the item be placed just below the item that was added to the layout manager just before this item was added.</p>
		/// <p>The default value is <see cref="GridBagConstraintConstants.Relative"/>. OriginY should be a non-negative value.</p>
		/// </summary>
		int IGridBagConstraint.OriginY 
		{ 
			get
			{
				Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutColumnInfo;

				if ( ! this.Band.AreColumnHeadersInSeparateLayoutArea )
				{
					if ( LabelPosition.Top == ci.LabelPositionResolved )
						return ci.OriginYResolved + ci.LabelSpanResolved;
				}

				return ci.OriginYResolved;
			}
		}

		#endregion // OriginY

		#region SpanX

		/// <summary>
		/// <p>Specifies the number of cells this item will span horizontally. The constant <see cref="GridBagConstraintConstants.Remainder"/> specifies that this item be the last one in the row and thus occupy remaining space.</p>
		/// </summary>
		int IGridBagConstraint.SpanX 
		{ 
			get
			{
				Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutColumnInfo;

				if ( ! this.Band.AreColumnHeadersInSeparateLayoutArea )
				{
					if ( LabelPosition.Left == ci.LabelPositionResolved ||
						LabelPosition.Right == ci.LabelPositionResolved  )
					{
						return ci.SpanXResolved - ci.LabelSpanResolved;
					}
				}

				return ci.SpanXResolved;
			}
		}

		#endregion // SpanX

		#region SpanY

		/// <summary>
		/// <p>Specifies the number of cells this item will span vertically. The constant <see cref="GridBagConstraintConstants.Remainder"/> specifies that this item be the last one in the column and thus occupy remaining space.</p>
		/// </summary>
		int IGridBagConstraint.SpanY 
		{ 
			get
			{
				Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutColumnInfo;

				if ( ! this.Band.AreColumnHeadersInSeparateLayoutArea )
				{
					if ( LabelPosition.Top == ci.LabelPositionResolved ||
						LabelPosition.Bottom == ci.LabelPositionResolved  )
					{
						return ci.SpanYResolved - ci.LabelSpanResolved;
					}
				}

				return ci.SpanYResolved;
			}
		}

		#endregion // SpanY

		#region WeightX

		/// <summary>
		/// Indicates how the extra horizontal space will be distributed among items. Default value is 0.0. Higher values give higher priority. The weight of the column in the virtual grid the grid-bag layout represents is the maximum WeightX of all the items in the row.
		/// </summary>
		float IGridBagConstraint.WeightX 
		{ 
			get
			{
				return this.RowLayoutColumnInfo.WeightX;
			}
		}

		#endregion // WeightX

		#region WeightY

		/// <summary>
		/// Indicates how the extra vertical space will be distributed among items. Default value is 0.0. Higher values give higher priority. The weight of the column in the virtual grid the grid-bag layout represents is the maximum WeightY of all the items in the column.
		/// </summary>
		float IGridBagConstraint.WeightY 
		{ 
			get
			{
				return this.RowLayoutColumnInfo.WeightY;
			}
		}

		#endregion // WeightY

		#endregion // IGridBagConstraint interface implementation

		#region CellSizeResolved

		// SSP 6/25/03 - Row Layout Functionality
		// Added CellSizeResolved method.
		//
		/// <summary>
		/// Resolved cell size. This property returns the actual width and the height of the cells associated with the column.
		/// </summary>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Size CellSizeResolved
		{
			get
			{
				// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				//
				UltraGridBand band = this.Band;

				Debug.Assert( null != band, "No band !" );
				if ( null == band )
					return Size.Empty;

				Size size;
				if ( band.UseRowLayoutResolved )
				{
					size = this.RowLayoutColumnInfo.CachedItemRectCell.Size;
				}
				else
				{
					// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
					// 
					//int height = band.RowHeightResolved
					//	- ( 2 * ( band.CellSpacingResolved + band.CellPaddingResolved ) );
					int height = band.RowHeightResolved;

					int width = this.Width;

					// SSP 11/10/05 BR07605
					// When auto fit style is extend last column, adjust the last column's 
					// width to account for it being extended.
					// 
					// SSP 7/2/07 BR24380
					// Refactored the code into a separate AutoFitExtendLastColumn_WidthDelta 
					// property.
					// 
					// ------------------------------------------------------------------------------
					width += this.AutoFitExtendLastColumn_WidthDelta;
					
					// ------------------------------------------------------------------------------

					size = new Size( width, height );
				}

				// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// Since the CachedItemRectCell, band.RowHeightResolved and this.Width all include
				// cell spacing we need to take it out since this method is supposed to return the
				// size of the cell.
				// 
				int delta = 2 * band.CellSpacingResolved;			

				GridUtils.Deflate( ref size, delta );

				return size;
			}
		}

		#endregion // CellSizeResolved

		#region AutoFitExtendLastColumn_WidthDelta

		// SSP 7/2/07 BR24380
		// Added AutoFitExtendLastColumn_WidthDelta. Code in there was moved from CellSizeResolved
		// above so it can be used some other place.
		// 
		internal int AutoFitExtendLastColumn_WidthDelta
		{
			get
			{
				UltraGridBand band = this.Band;

				if ( band.Layout.AutoFitExtendLastColumn )
				{
					// MD 1/20/09 - Groups in RowLayout
					//bool lastItem = !band.GroupsDisplayed && this.LastItem
					//    || band.GroupsDisplayed && null != this.Group && this.Group.LastItem && this.LastItemInLevel;
					bool lastItem;
					if ( band.GroupsDisplayed )
					{
						UltraGridGroup group = this.GroupResolved;

						if ( group == null )
						{
							if ( this.band.RowLayoutStyle == RowLayoutStyle.None )
								lastItem = false;
							else
								lastItem = this.LastItem;
						}
						else
						{
							lastItem = group.LastItem && this.LastItemInLevel;
						}
					}
					else
					{
						lastItem = this.LastItem;
					}

					if ( lastItem )
					{
						int bandExtent = band.GetExtent( );
						if ( band.bandExtent_BeforeAutoFitExtendLastColumn < bandExtent )
							return bandExtent - band.bandExtent_BeforeAutoFitExtendLastColumn;
					}
				}

				return 0;
			}
		}

		#endregion // AutoFitExtendLastColumn_WidthDelta

		#endregion // Row Layout Code

		// SSP 5/1/03 - Cell Level Editor
		// Added Editor and ValueList properties off the cell so the user can set the editor
		// and value list on a per cell basis.
		//
		#region Cell Level Editor/ValueList Related Code

		internal EmbeddableEditorBase GetEditor( UltraGridRow row )
		{
			// SSP 5/20/05 - NAS 5.2 Filter Row
			// Moved the following code into the new GetEditorResolved methods off the row.
			//
			
			return null != row ? row.GetEditorResolved( this ) : this.Editor;
		}

		internal IValueList GetValueList( UltraGridRow row )
		{
			// SSP 5/20/05 - NAS 5.2 Filter Row
			// Moved the following code into the new GetValueListResolved methods off the row.
			//
			
			return null != row ? row.GetValueListResolved( this ) : this.ValueList;
		}

		#endregion // Cell Level Editor/ValueList Related Code

		// SSP 5/12/03 - Optimizations
		// Added a way to just draw the text without having to embedd an embeddable ui element in
		// cells to speed up rendering.
		//
		#region CellDisplayStyle

		/// <summary>
		/// CellDisplayStyle specifies how the cells get rendered. You can use this property to speed up rendering of cells by setting it to <b>FormattedText</b> or <b>PlainText</b>. Default is resolved to <b>FullEditorDisplay</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>CellDisplayStyle</b> specifies how the cells get rendered. You can use this property to speed up rendering of cells by setting it to <b>FormattedText</b> or <b>PlainText</b>. Default is resolved to <b>FullEditorDisplay</b>.</p>
		/// <p class="body"><b>FormattedText</b> draws the formatted cell value in the cells. <b>PlainText</b> draws the cell value converted to text withought applying any formatting. It simply calls ToString on the cell value and draws the resulting text in the cell.<b>FullEditorDisplay</b> embedds an embeddable ui element which draws the cell value as well as draws any edit elements like buttons. This is a bit more performance intensive than <b>PlainText</b> and <b>FormattedText</b>. The advantage of using <b>FullEditorDisplay</b> which is the default is that any edit elements that the editor may render in cells do not get rendered until the cell enters the edit mode.</p>
		/// <seealso cref="UltraGridOverride.CellDisplayStyle"/>
		/// <seealso cref="UltraGridColumn.Style"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.CellDisplayStyle"/>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.CellDisplayStyle CellDisplayStyle
		{
			get
			{
				return this.cellDisplayStyle;
			}
			set
			{
				if ( this.cellDisplayStyle != value )
				{
					if ( ! Enum.IsDefined( typeof( CellDisplayStyle ), value ) )
						throw new InvalidEnumArgumentException( "CellDisplayStyle", (int)value, typeof( CellDisplayStyle ) );

					this.cellDisplayStyle = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.CellDisplayStyle );
				}
			}
		}

		#endregion // CellDisplayStyle

		#region ResetCellDisplayStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetCellDisplayStyle( )
		{
			this.CellDisplayStyle = Infragistics.Win.UltraWinGrid.CellDisplayStyle.Default;
		}

		#endregion // ResetCellDisplayStyle

		#region ShouldSerializeCellDisplayStyle

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeCellDisplayStyle( )
		{
			return Infragistics.Win.UltraWinGrid.CellDisplayStyle.Default != this.CellDisplayStyle;
		}

		#endregion // ShouldSerializeCellDisplayStyle

		#region GetCellDisplayStyleResolved
		
		// SSP 8/12/03 - Optimizations
		//
		private int verifiedGrandVersion_CellDisplayStyle = 0;
		private CellDisplayStyle cachedColumnCellDisplayStyle = CellDisplayStyle.Default;
		internal Infragistics.Win.UltraWinGrid.CellDisplayStyle GetCellDisplayStyleResolved( UltraGridRow row )
		{
			return this.GetCellDisplayStyleResolved( row.GetCellIfAllocated( this ) );

			// SSP 8/12/03 - Optimizations
			//
			
		}

		internal Infragistics.Win.UltraWinGrid.CellDisplayStyle GetCellDisplayStyleResolved( UltraGridCell cell )
		{
			if ( null != cell && Infragistics.Win.UltraWinGrid.CellDisplayStyle.Default != cell.CellDisplayStyle )
				return cell.CellDisplayStyle;

			// SSP 8/12/03 - Optimizations
			//
			// ------------------------------------------------------------------------------------
			if ( this.Layout.GrandVerifyVersion == this.verifiedGrandVersion_CellDisplayStyle )
				return this.cachedColumnCellDisplayStyle;				
			this.verifiedGrandVersion_CellDisplayStyle = this.Layout.GrandVerifyVersion;
			// ------------------------------------------------------------------------------------

			if ( Infragistics.Win.UltraWinGrid.CellDisplayStyle.Default != this.cellDisplayStyle )
				return this.cachedColumnCellDisplayStyle = this.cellDisplayStyle;

			UltraGridOverride ovrd = this.band.HasOverride ? this.band.Override : null;				
			if ( null != ovrd && ovrd.ShouldSerializeCellDisplayStyle( ) )
				return this.cachedColumnCellDisplayStyle = ovrd.CellDisplayStyle;

			ovrd = this.band.Layout.HasOverride ? this.band.Layout.Override : null;
			if ( null != ovrd && ovrd.ShouldSerializeCellDisplayStyle( ) )
				return this.cachedColumnCellDisplayStyle = ovrd.CellDisplayStyle;

			return this.cachedColumnCellDisplayStyle = CellDisplayStyle.FullEditorDisplay;
		}

		#endregion // GetCellDisplayStyleResolved

		#region InternalEditor

		// SSP 5/13/03
		// Added InternalEditor property.
		//
		internal EmbeddableEditorBase InternalEditor
		{
			get
			{
				return this.embeddableEditor;
			}
		}

		#endregion // InternalEditor

		// SSP 11/10/03 Add Row Feature
		// Added #region DefaultCellValue property.
		//
		#region DefaultCellValue

		/// <summary>
		/// Gets or sets the default cell value that will be assigned when new rows are added via the add-row feature or the add-new box.
		/// </summary>
		[ System.ComponentModel.Editor( typeof( PrimitiveTypeUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		[ TypeConverter( typeof( PrimitiveTypeConverter ) ) ]
		public object DefaultCellValue
		{
			get
			{
				return this.defaultAddRowCellValue;
			}
			set
			{
				if ( this.defaultAddRowCellValue != value )
                {
                    // MRS 8/7/2008 - BR35263
                    // We should never have been doing explicit type comparisons here. We should be 
                    // using IsAssignableFrom. This also means we don't need to bother with getting 
                    // the underlying type.
                    //
                    #region Old Code
                    // SSP 8/11/05 BR05504
                    // Allow DBNull values.
                    // 
                    // ----------------------------------------------------------------------------
                    //if ( null != value && value.GetType( ) != this.DataType )
                    //	throw new ArgumentException( SR.GetString( "LER_ArgumentException_16" ), "DefaultCellValue" );
                    //
                    // MBS 9/28/07 - BR26908
                    //if ( null != value && DBNull.Value != value && value.GetType( ) != this.DataType )
                    //if (null != value && DBNull.Value != value && value.GetType() != Utilities.GetUnderlyingType(this.DataType))
                    //{
                    //    // SSP 8/11/05 BR05504
                    //    // Try to convert the value to the column's DataType. Visual Studio designer
                    //    // writes out 1.0 double value as literal 1 in the InitializeComponent. Therefore
                    //    // without us converting to the column's DataType it's not possible to set whole
                    //    // values at design time on double, float etc... columns.
                    //    // 
                    //    object convertedVal = this.ConvertValueToDataType(value);

                    //    // MBS 9/28/07 - BR26908
                    //    //if ( null != convertedVal && convertedVal.GetType( ) == this.DataType )
                    //    if (null != convertedVal && convertedVal.GetType() == Utilities.GetUnderlyingType(this.DataType))
                    //        value = convertedVal;
                    //    else
                    //        throw new ArgumentException(SR.GetString("LER_ArgumentException_16"), "DefaultCellValue");
                    //}
                    // ----------------------------------------------------------------------------
                    #endregion // Old Code
                    //
                    if (null != value && DBNull.Value != value
                        && false == this.DataType.IsAssignableFrom(value.GetType()))
                    {
                        object convertedVal = this.ConvertValueToDataType(value);

                        // MBS 9/26/08 - TFS6924
                        // We should be checking the converted value and not the original
                        //
                        //if (null != convertedVal && this.DataType.IsAssignableFrom(value.GetType()))
                        if (null != convertedVal && this.DataType.IsAssignableFrom(convertedVal.GetType()))
                            value = convertedVal;
                        else
                            throw new ArgumentException(SR.GetString("LER_ArgumentException_16"), "DefaultCellValue");
                    }

                    this.defaultAddRowCellValue = value;

                    // JAS 2005 v2 XSD Support
                    // If this property had been set by the EnforceXsdConstraints method, then
                    // clear the bit which represents this property so that the ClearXsdConstraints
                    // method will know not to reset this value.  Both the DefaultValue and FixedValue
                    // properties of the ValueConstraint set this property, so both flags need to be reset.
                    //
                    if ((this.XsdSuppliedConstraints & XsdConstraintFlags.DefaultValue) != 0)
                        this.XsdSuppliedConstraints &= ~XsdConstraintFlags.DefaultValue;

                    if ((this.XsdSuppliedConstraints & XsdConstraintFlags.FixedValue) != 0)
                        this.XsdSuppliedConstraints &= ~XsdConstraintFlags.FixedValue;

                    // The FixedValue constraint sets this property, so if this property is being set to null
                    // then we must inform the constraint that there is no fixed value anymore.
                    //
                    if (value == null)
                        this.Constraint.ResetFixedValue();

                    this.NotifyPropChange(PropertyIds.DefaultCellValue);
                }
			}
		}

		#endregion // DefaultCellValue

		#region ShouldSerializeDefaultCellValue
		
		/// <summary>
		/// Returns true of the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDefaultCellValue( )
		{
			return null != this.defaultAddRowCellValue;
		}

		#endregion // ShouldSerializeDefaultCellValue

		#region ResetDefaultCellValue
		
		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetDefaultCellValue( )
		{
			this.DefaultCellValue = null;
		}

		#endregion // ResetDefaultCellValue

		// SSP 12/12/03 DNF135
		// Added a way to control whether ink buttons get shown.
		// Added ShowInkButton property.
		//
		#region ShowInkButton

		/// <summary>
		/// Specifies whether ink buttons get shown in cells.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use <b>ShowInkButton</b> property to explicitly turn on or turn off inck buttons.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridColumn_P_ShowInkButton")]
		[ LocalizedCategory("LC_Appearance") ]
		public ShowInkButton ShowInkButton
		{
			get
			{
				return this.showInkButton;
			}
			set
			{
				if ( this.showInkButton != value )
				{
					if ( ! Enum.IsDefined( typeof( ShowInkButton ), value ) )
						throw new InvalidEnumArgumentException( "ShowInkButton", (int)value, typeof( ShowInkButton ) );

					this.showInkButton = value;

					this.NotifyPropChange( PropertyIds.ShowInkButton );
				}
			}
		}

		#endregion // ShowInkButton

		#region ShouldSerializeShowInkButton

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeShowInkButton( )
		{
			return ShowInkButton.Default != this.showInkButton;
		}

		#endregion // ShouldSerializeShowInkButton

		#region ResetShowInkButton

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetShowInkButton( )
		{
			this.ShowInkButton = ShowInkButton.Default;
		}

		#endregion // ResetShowInkButton

		// MRS 9/14/04
		// Added a property to control whether or not the grid displays 
		// a string in a cell while calculating
		//
		#region ShowCalculatingText

		/// <summary>
		/// Specifies whether cells or summaries in the column will display "#Calculating" in when calculating with an UltraCalcManager.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use <b>ShowCalculatingText</b> property to explicitly turn on or turn off the display of the text during calculations.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridColumn_P_ShowCalculatingText")]
		[ LocalizedCategory("LC_Appearance") ]
		public DefaultableBoolean ShowCalculatingText
		{
			get
			{
				return this.showCalculatingText;
			}
			set
			{
				if ( this.showCalculatingText != value )
				{
					if ( ! Enum.IsDefined( typeof( DefaultableBoolean ), value ) )
						throw new InvalidEnumArgumentException( "ShowCalculatingText", (int)value, typeof( DefaultableBoolean ) );

					this.showCalculatingText = value;

					this.NotifyPropChange( PropertyIds.ShowCalculatingText );
				}
			}
		}

		#endregion // ShowCalculatingText

		#region ShouldSerializeShowCalculatingText

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeShowCalculatingText( )
		{
			return DefaultableBoolean.Default != this.showCalculatingText;
		}

		#endregion // ShouldSerializeShowCalculatingText

		#region ResetShowCalculatingText

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetShowCalculatingText( )
		{
			this.ShowCalculatingText = DefaultableBoolean.Default;
		}

		#endregion // ResetShowCalculatingText

		#region ShowInkButtonResolved

		// SSP 12/12/03 DNF135
		// Added a way to control whether ink buttons get shown.
		// Added ShowInkButton property.
		//
		internal ShowInkButton ShowInkButtonResolved
		{
			get
			{
				ShowInkButton showInkButton = this.showInkButton;

				if ( ShowInkButton.Default == showInkButton && band.HasOverride )
					showInkButton = band.Override.ShowInkButton;

				if ( ShowInkButton.Default == showInkButton && band.Layout.HasOverride )
					showInkButton = band.Layout.Override.ShowInkButton;
				
				return showInkButton;
			}
		}

		#endregion // ShowInkButtonResolved

		#region ShowCalculatingTextResolved

		// MRS 9/14/04
		// Added a property to control whether or not the grid displays 
		// a string in a cell while calculating
		//
		internal bool ShowCalculatingTextResolved
		{
			get
			{
				DefaultableBoolean showCalculatingText = this.showCalculatingText;

				if ( DefaultableBoolean.Default == showCalculatingText && band.HasOverride )
					showCalculatingText = band.Override.ShowCalculatingText;

				if ( DefaultableBoolean.Default == showCalculatingText && band.Layout.HasOverride )
					showCalculatingText = band.Layout.Override.ShowCalculatingText;
				
				return (showCalculatingText == DefaultableBoolean.True);					
			}
		}

		#endregion // ShowCalculatingTextResolved

		#region IEditType Interface Implementation

		// SSP 5/3/04 - Designer Enhancements
		// Added mask input standard values. To do that we need to implement IEditType
		// interface and add a MaskTypeConverter attribute to the mask properties.
		// 
		Type IEditType.EditType
		{
			get
			{
				return this.DataType;
			}
		}

		#endregion // IEditType Interface Implementation

		// SSP 6/29/04 - UltraCalc
		//
		#region UltraCalc Related

		#region FormulaHolder

		internal FormulaHolder FormulaHolder
		{
			get
			{
				if ( null == this.formulaHolder )
					this.formulaHolder = new FormulaHolder( this );

				return this.formulaHolder;
			}
		}

		// MD 5/22/09 - TFS16348
		internal bool HasFormulaHolder
		{
			get { return this.formulaHolder != null; }
		}

		#endregion // FormulaHolder

		#region Formula

		/// <summary>
		/// Specifies the formula. <see cref="UltraGridBase.CalcManager"/> property must be set to a valid instance of <see cref="IUltraCalcManager"/> for this property to have any affect.
		/// </summary>
		/// <remarks>
		/// <p class="body">Note: <see cref="UltraGridBase.CalcManager"/> property must be set to a valid instance of <see cref="IUltraCalcManager"/> for this property to have any affect.</p>
		/// <p><seealso cref="SummarySettings.Formula"/> <seealso cref="UltraGridBase.CalcManager"/> <seealso cref="UltraGridBase.CalcManager"/></p>
		/// </remarks>
		[ Editor( "Infragistics.Win.UltraWinCalcManager.FormulaBuilder.FormulaUITypeEditor, " + AssemblyRef.FormulaBuilderDesign, typeof(UITypeEditor) ) ]
		[ MergableProperty( false ) ]
		public string Formula
		{
			get
			{
				return this.FormulaHolder.Formula;
			}
			set
			{
				// SSP 11/8/04 UWC91
				// Trim spaces from the formula string. This is so the property grid doesn't
				// accept space as a formula.
				//
				if ( null != value )
					value = value.Trim( );

				if ( this.FormulaHolder.Formula != value )
				{
					this.FormulaHolder.Formula = value;

					this.NotifyPropChange( PropertyIds.Formula );
				}
			}
		}

		#endregion // Formula

		#region FormulaValueConverter 

		// SSP 11/9/04
		// Added FormulaValueConverter property.
		//
		/// <summary>
		/// Interface for providing custom logic for converting formula results and the formula source values.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>FormulaValueConverter</b> allows you to write custom logic for converting cell
		/// values to values that the formulas will use for calculations. It also allows you
		/// to write custom logic for converting results of formula calculations to cell values.
		/// </p>
		/// <seealso cref="IFormulaValueConverter"/>
		/// <seealso cref="SummarySettings.Formula"/>
		/// <seealso cref="UltraGridBase.CalcManager"/>
		/// </remarks>
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public IFormulaValueConverter FormulaValueConverter 
		{
			get
			{
				return this.FormulaHolder.FormulaValueConverter;
			}
			set
            {
                // MRS NAS v8.3 - Unit Testing
				//this.FormulaHolder.FormulaValueConverter = value;
                if (this.FormulaHolder.FormulaValueConverter != value)
                {
                    this.FormulaHolder.FormulaValueConverter = value;

                    this.NotifyPropChange(PropertyIds.FormulaValueConverter);
                }
			}
		}

		#endregion // FormulaValueConverter 
		
		#region ShouldSerializeFormula

		/// <summary>
		/// Returns true if this property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFormula( )
		{
			return null != this.Formula && this.Formula.Length > 0;
		}

		#endregion // ShouldSerializeFormula

		#region ResetFormula

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFormula( )
		{
			this.Formula = null;
		}

		#endregion // ResetFormula

		#region HasActiveFormula

		internal bool HasActiveFormula
		{
			get
			{
				return this.FormulaHolder.HasActiveFormula;
			}
		}

		#endregion // HasActiveFormula

		#region FormulaErrorValue

		/// <summary>
		/// Specifies the value to assign to the data source if the column�s formula evaluates to an error. If the column doesn�t have Formula set, this property is ignored.
		/// </summary>
		[ System.ComponentModel.Editor( typeof( PrimitiveTypeUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		[ TypeConverter( typeof( PrimitiveTypeConverter ) ) ]
		public object FormulaErrorValue
		{
			get
			{
				return this.formulaErrorValue;
			}
			set
			{
				if ( this.formulaErrorValue != value )
				{
					this.formulaErrorValue = value;

					this.NotifyPropChange( PropertyIds.Formula );
				}
			}
		}

		#endregion // FormulaErrorValue

		#region ShouldSerializeFormulaErrorValue

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFormulaErrorValue( )
		{
			return null != this.formulaErrorValue;
		}

		#endregion // ShouldSerializeFormulaErrorValue

		#region ResetFormulaErrorValue

		/// <summary>
		/// Resets the <see cref="FormulaErrorValue"/> property to its default value of <b>null</b> (<b>Nothing</b> in VB).
		/// </summary>
		public void ResetFormulaErrorValue( )
		{
			this.FormulaErrorValue = null;
		}

		#endregion // ResetFormulaErrorValue

		#region FormulaErrorValueResolved
		
		internal object FormulaErrorValueResolved
		{
			get
			{
				return null != this.formulaErrorValue ? this.formulaErrorValue : DBNull.Value;
			}
		}

		#endregion // FormulaErrorValueResolved

		#region CalcReference

		internal ColumnReference CalcReference
		{
			get
			{
				return (ColumnReference)this.FormulaHolder.CalcReference;
			}
		}

		#endregion // CalcReference

		#region HasCalcReference
		
		internal bool HasCalcReference
		{
			get
			{
				return this.FormulaHolder.HasCalcReference;
			}
		}

		#endregion // HasCalcReference

		#region IFormulaProvider.Participant

		IUltraCalcParticipant IFormulaProvider.Participant
		{
			get
			{
				return this.Layout.Grid;
			}
		}

		#endregion // IFormulaProvider.Participant

		#region IFormulaProvider.Reference

		IUltraCalcReference IFormulaProvider.Reference
		{
			get
			{
				return this.CalcReference;
			}
		}

		#endregion // IFormulaProvider.Reference

		#region IFormulaProvider.CalcManager

		IUltraCalcManager IFormulaProvider.CalcManager
		{
			get
			{
				return null != this.Layout ? this.Layout.CalcManager : null;
			}
		}

		#endregion // IFormulaProvider.CalcManager

		#region FormulaAbsoluteName

		// SSP 8/31/04 UWG3673
		// Added FormulaAbsoluteName.
		//
		/// <summary>
		/// Returns the absolute name of the column that you can use to refer to it in formulas.
		/// </summary>
		[ Browsable( false ) ]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
		public string FormulaAbsoluteName
		{
			get
			{
				return null != this.CalcReference ? this.CalcReference.AbsoluteName : null;
			}
		}

		#endregion // FormulaAbsoluteName

		#region FormulaErrorAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the cells containing formula errors.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>FormulaErrorAppearance</b> property is used to specify the appearance of the cells with formula errors.</p> 
		///	<p><seealso cref="UltraGridOverride.FormulaErrorAppearance"/></p>
		///	</remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FormulaErrorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FormulaErrorAppearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.formulaErrorAppearanceHolder )
				{
					this.formulaErrorAppearanceHolder = new Infragistics.Win.AppearanceHolder( );
                    
					// hook up the prop change notifications
					//
					this.formulaErrorAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if ( this.Layout != null )
					this.formulaErrorAppearanceHolder.Collection = this.Layout.Appearances;

				return this.formulaErrorAppearanceHolder.Appearance;
			}
			set
			{
				if ( this.formulaErrorAppearanceHolder == null				|| 
					! this.formulaErrorAppearanceHolder.HasAppearance		|| 
					value != this.formulaErrorAppearanceHolder.Appearance )
				{
					if ( null == this.formulaErrorAppearanceHolder )
					{
						this.formulaErrorAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.formulaErrorAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Layout != null)
						this.formulaErrorAppearanceHolder.Collection = this.Layout.Appearances;

					this.formulaErrorAppearanceHolder.Appearance = value;

					// notify listeners
					//
					this.NotifyPropChange( PropertyIds.FormulaErrorAppearance );
				}
			}
		}

		#endregion // FormulaErrorAppearance

		#region HasFormulaErrorAppearance
		
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal bool HasFormulaErrorAppearance
		{
			get
			{
				return null != this.formulaErrorAppearanceHolder 
					&& this.formulaErrorAppearanceHolder.HasAppearance;
			}
		}

		#endregion // HasFormulaErrorAppearance

		#region ShouldSerializeFormulaErrorAppearance

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFormulaErrorAppearance( )
		{
			return this.HasFormulaErrorAppearance 
				&& this.formulaErrorAppearanceHolder.ShouldSerialize( );
		}

		#endregion // ShouldSerializeFormulaErrorAppearance

		#region ResetFormulaErrorAppearance

		/// <summary>
		/// Resets the <see cref="FormulaErrorAppearance"/> property to its default value.
		/// </summary>
		public void ResetFormulaErrorAppearance( )
		{
			if ( this.HasFormulaErrorAppearance )
			{

				//				// remove the prop change notifications
				//				//
				//				this.cellAppearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				//                    
				//				this.cellAppearanceHolder.Reset();
				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//this.cellAppearanceHolder.Appearance.Reset();

				// Call reset on the holder instead of the appearance
				//
				this.formulaErrorAppearanceHolder.Reset();

				// Notify listeners that the appearance has changed. 
				this.NotifyPropChange( PropertyIds.FormulaErrorAppearance );
			}
		}

		#endregion // ResetFormulaErrorAppearance

		#region IsStillValid
        
		// SSP 9/1/04
		// Added IsStillValid property.
		//
		internal bool IsStillValid
		{
			get
			{
				return ! this.Disposed && null != this.band && this.Index >= 0 && this.band.IsStillValid;
			}
		}

		#endregion // IsStillValid

		#endregion // UltraCalc Related

		#region Merged Cell Feature

		// SSP 11/3/04 - Merged Cell Feature
		//
		#region MergedCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the merged cells.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridOverride.MergedCellAppearance"/> <seealso cref="UltraGridColumn.MergedCellContentArea"/> <seealso cref="UltraGridColumn.MergedCellStyle"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_MergedCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase MergedCellAppearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.mergedCellAppearanceHolder )
				{
					this.mergedCellAppearanceHolder = new Infragistics.Win.AppearanceHolder();
                    
					// hook up the prop change notifications
					//
					this.mergedCellAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.Layout != null)
					this.mergedCellAppearanceHolder.Collection = this.Layout.Appearances;

				return this.mergedCellAppearanceHolder.Appearance;
			}
			set
			{
				if ( this.mergedCellAppearanceHolder == null				|| 
					!this.mergedCellAppearanceHolder.HasAppearance		|| 
					value != this.mergedCellAppearanceHolder.Appearance )
				{
					if ( null == this.mergedCellAppearanceHolder )
					{
						this.mergedCellAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.mergedCellAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Layout != null)
						this.mergedCellAppearanceHolder.Collection = this.Layout.Appearances;

					this.mergedCellAppearanceHolder.Appearance = value;

					// notify listeners
					//
					this.NotifyPropChange( PropertyIds.MergedCellAppearance );
				}
			}
		}

		#endregion // MergedCellAppearance

		#region HasMergedCellAppearance

		internal bool HasMergedCellAppearance
		{
			get
			{
				return null != this.mergedCellAppearanceHolder 
					&& this.mergedCellAppearanceHolder.HasAppearance;
			}
		}

		#endregion // HasMergedCellAppearance

		#region ShouldSerializeMergedCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMergedCellAppearance( ) 
		{
			return this.HasMergedCellAppearance && this.mergedCellAppearanceHolder.ShouldSerialize( );
		}

		#endregion // ShouldSerializeMergedCellAppearance
 
		#region ResetMergedCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetMergedCellAppearance( ) 
		{
            if (null != this.mergedCellAppearanceHolder)
            {
                this.mergedCellAppearanceHolder.Reset();

                // MRS NAS v8.3 - Unit Testing
                this.NotifyPropChange(PropertyIds.MergedCellAppearance);
            }
		}

		#endregion // ResetMergedCellAppearance

		#region MergedCellContentArea

		/// <summary>
		/// Specifies whether to position the contents of a merged cell in the entire area of the merged cell or just the visible area of the merged cell. Default is resolved to <b>VirtualRect</b>.
		/// </summary>
		/// <remarks>
		/// <b class="body">Specifies whether to position the contents of a merged cell in the entire area of the merged cell or just the visible area of the merged cell. Default is resolved to <b>VirtualRect</b>.</b>
		/// <p><seealso cref="UltraGridOverride.MergedCellStyle"/> <seealso cref="UltraGridOverride.MergedCellAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_MergedCellContentArea")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public MergedCellContentArea MergedCellContentArea
		{
			get
			{
				return this.mergedCellContentArea;
			}
			set
			{
				if ( this.mergedCellContentArea != value )
				{
					if ( ! Enum.IsDefined( typeof( MergedCellContentArea ), value ) )
						throw new InvalidEnumArgumentException( "MergedCellContentArea", (int)value, typeof( MergedCellContentArea ) );

					this.mergedCellContentArea = value;

					this.NotifyPropChange( PropertyIds.MergedCellContentArea );
				}
			}
		}

		#endregion // MergedCellContentArea

		#region ShouldSerializeMergedCellContentArea

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMergedCellContentArea( )
		{
			return MergedCellContentArea.Default != this.mergedCellContentArea;
		}

		#endregion // ShouldSerializeMergedCellContentArea

		#region ResetMergedCellContentArea

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetMergedCellContentArea( )
		{
			this.MergedCellContentArea = MergedCellContentArea.Default;
		}

		#endregion // ResetMergedCellContentArea

		#region MergedCellStyle

		/// <summary>
		/// Specifies if and how cell merging is performed. Default is resolved to Never.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridOverride.MergedCellContentArea"/> <seealso cref="UltraGridOverride.MergedCellAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_MergedCellStyle")]
		[ LocalizedCategory("LC_Behavior") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public MergedCellStyle MergedCellStyle
		{
			get
			{
				return this.mergedCellStyle;
			}
			set
			{
				if ( this.mergedCellStyle != value )
				{
					if ( ! Enum.IsDefined( typeof( MergedCellStyle ), value ) )
						throw new InvalidEnumArgumentException( "MergedCellStyle", (int)value, typeof( MergedCellStyle ) );

					this.mergedCellStyle = value;

					this.NotifyPropChange( PropertyIds.MergedCellStyle );
				}
			}
		}

		#endregion // MergedCellStyle

		#region ShouldSerializeMergedCellStyle

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMergedCellStyle( )
		{
			return MergedCellStyle.Default != this.mergedCellStyle;
		}

		#endregion // ShouldSerializeMergedCellStyle

		#region ResetMergedCellStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetMergedCellStyle( )
		{
			this.MergedCellStyle = MergedCellStyle.Default;
		}

		#endregion // ResetMergedCellStyle

		#region MergedCellEvaluationType

		/// <summary>
		/// Specifies whether to merge cells based on their values or display text.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridColumn.MergedCellStyle"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_MergedCellEvaluationType")]
		[ LocalizedCategory("LC_Behavior") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public MergedCellEvaluationType MergedCellEvaluationType
		{
			get
			{
				return this.mergedCellEvaluationType;
			}
			set
			{
				if ( this.mergedCellEvaluationType != value )
				{
					if ( ! Enum.IsDefined( typeof( MergedCellEvaluationType ), value ) )
						throw new InvalidEnumArgumentException( "MergedCellEvaluationType", (int)value, typeof( MergedCellEvaluationType ) );

					this.mergedCellEvaluationType = value;

					this.NotifyPropChange( PropertyIds.MergedCellEvaluationType );
				}
			}
		}

		#endregion // MergedCellEvaluationType

		#region ShouldSerializeMergedCellEvaluationType

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMergedCellEvaluationType( )
		{
			return MergedCellEvaluationType.Default != this.mergedCellEvaluationType;
		}

		#endregion // ShouldSerializeMergedCellEvaluationType

		#region ResetMergedCellEvaluationType

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetMergedCellEvaluationType( )
		{
			this.MergedCellEvaluationType = MergedCellEvaluationType.Default;
		}

		#endregion // ResetMergedCellEvaluationType

		#region MergedCellEvaluator

		/// <summary>
		/// Used for specifying custom logic for determining which cells should merge.
		/// </summary>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false) ]
		public IMergedCellEvaluator MergedCellEvaluator
		{
			get
			{
				return this.mergedCellEvaluator;
			}
			set
			{
				if ( this.mergedCellEvaluator != value )
				{
					this.mergedCellEvaluator = value;

					this.NotifyPropChange( PropertyIds.MergedCellEvaluator );
				}
			}
		}

		#endregion // MergedCellEvaluator

		#region IsSorted
		
		internal bool IsSorted
		{
			get
			{
				return SortIndicator.Ascending == this.sortIndicator 
					|| SortIndicator.Descending == this.sortIndicator;
			}
		}

		#endregion // IsSorted

		#region MergedCellEnabledResolved
		
		internal bool MergedCellEnabledResolved
		{
			get
			{
				Infragistics.Win.UltraWinGrid.MergedCellStyle val = this.MergedCellStyleResolved;
				return MergedCellStyle.Never != val 					
					&& ! (
							// Cell merging will be disabled in the following situations.
							//

							// If merge style is OnlyWhenSorted and this column is not sorted.
							//
							MergedCellStyle.OnlyWhenSorted == val && ! this.IsSorted 
							// If there are multiple levels in groups.
							//
							// MD 1/20/09 - Groups in RowLayout
							// More checks were added to ActualColDisplayLevels which should also be added here, so just use the property instead.
							//|| this.Band.GroupsDisplayed && this.Band.LevelCount > 1 
							|| this.Band.ActualColDisplayLevels > 1
							// If the column is hidden which includes chaptered columns as well.
							//
                            // MRS 2/23/2009 - Found while fixing TFS14354
							//|| this.Hidden
                            || this.HiddenResolved
							// In card-view.
							//
							|| this.Band.CardView 
							// If the column doesn't span the entire row height in the row-layout mode.
							//
							|| this.Band.UseRowLayoutResolved && ! this.RowLayoutColumnInfo.SpansEntireLayoutHeight
					);
			}
		}

		#endregion // MergedCellEnabledResolved

		#region MergedCellStyleResolved
		
		internal MergedCellStyle MergedCellStyleResolved
		{
			get
			{
				MergedCellStyle val = this.mergedCellStyle;

				if ( MergedCellStyle.Default == val )
				{
					if ( this.Band.HasOverride )
						val = this.Band.Override.MergedCellStyle;

					if ( MergedCellStyle.Default == val )
					{
						if ( this.Layout.HasOverride )
							val = this.Layout.Override.MergedCellStyle;

						if ( MergedCellStyle.Default == val )
							val = MergedCellStyle.Never;
					}
				}

				return val;
			}
		}

		#endregion // MergedCellStyleResolved

		#region MergedCellEvaluationTypeResolved

		internal MergedCellEvaluationType MergedCellEvaluationTypeResolved
		{
			get
			{
				if ( MergedCellEvaluationType.Default != this.mergedCellEvaluationType )
					return this.mergedCellEvaluationType;

				// Default for date time columns to display text. For everything else 
				// default to cell values.
				//
				return typeof( DateTime ) == this.DataType 
					? MergedCellEvaluationType.MergeSameText
					: MergedCellEvaluationType.MergeSameValue;
			}
		}

		#endregion // MergedCellEvaluationTypeResolved

		#region MergedCellContentAreaResolved
		
		internal MergedCellContentArea MergedCellContentAreaResolved
		{
			get
			{
				MergedCellContentArea val = this.mergedCellContentArea;

				if ( MergedCellContentArea.Default == val )
				{
					if ( this.Band.HasOverride )
						val = this.Band.Override.MergedCellContentArea;

					if ( MergedCellContentArea.Default == val )
					{
						if ( this.Layout.HasOverride )
							val = this.Layout.Override.MergedCellContentArea;

						if ( MergedCellContentArea.Default == val )
						{
							// Since merged cells could span multiple pages re-display the 
							// content on every page.
							//
							val = this.Layout.IsPrintLayout 
								? MergedCellContentArea.VisibleRect
								: MergedCellContentArea.VirtualRect;
						}
					}
				}

				return val;
			}
		}

		#endregion // MergedCellContentAreaResolved

		#region GetMergedCell
		
		internal MergedCell GetMergedCell( UltraGridRow row )
		{
			return this.MergedCellEnabledResolved 
				? row.ParentCollection.MergedCellCache.GetMergedCell( row, this, true )
				: null;
		}

		#endregion // GetMergedCell

		#region AreCellsMerged

		internal bool AreCellsMerged( UltraGridRow row1, UltraGridRow row2 )
		{
			MergedCell mergedCell = this.GetMergedCell( row1 );
			return null != mergedCell && mergedCell.ContainsCell( row2 );
		}

		#endregion // AreCellsMerged

		#endregion // Merged Cell Feature

		// SSP 12/14/04 - IDataErrorInfo Support
		//
		#region SupportDataErrorInfo

		/// <summary>
		/// Specifies whether to make use of IDataErrorInfo interface implemented on the underlying row objects to display error info in the associated cells.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies whether to make use of IDataErrorInfo interface implemented on the underlying row objects to display error info in the associated cells.</p>
		/// <p><seealso cref="UltraGridOverride.SupportDataErrorInfo"/> <seealso cref="UltraGridOverride.DataErrorRowAppearance"/> <seealso cref="UltraGridOverride.DataErrorCellAppearance"/> <seealso cref="UltraGridOverride.DataErrorRowSelectorAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_SupportDataErrorInfo")]
		[ LocalizedCategory("LC_Behavior") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public DefaultableBoolean SupportDataErrorInfo
		{
			get
			{
				return this.supportDataErrorInfo;
			}
			set
			{
				if ( this.supportDataErrorInfo != value )
				{
					if ( ! Enum.IsDefined( typeof( DefaultableBoolean ), value ) )
						throw new InvalidEnumArgumentException( "SupportDataErrorInfo", (int)value, typeof( DefaultableBoolean ) );

					this.supportDataErrorInfo = value;

					this.NotifyPropChange( PropertyIds.SupportDataErrorInfo );
				}
			}
		}

		#endregion // SupportDataErrorInfo

		#region ShouldSerializeSupportDataErrorInfo

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSupportDataErrorInfo( )
		{
			return DefaultableBoolean.Default != this.supportDataErrorInfo;
		}

		#endregion // ShouldSerializeSupportDataErrorInfo

		#region ResetSupportDataErrorInfo

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		/// <returns></returns>
		public void ResetSupportDataErrorInfo( )
		{
			this.SupportDataErrorInfo = DefaultableBoolean.Default;
		}

		#endregion // ResetSupportDataErrorInfo

		#region SupportsDataErrorInfoResolved

		internal bool SupportsDataErrorInfoResolved
		{
			get
			{
				if ( this.ShouldSerializeSupportDataErrorInfo( ) )
					return DefaultableBoolean.True == this.supportDataErrorInfo;

				return this.Band.SupportsDataErrorInfoOnCellsResolved;
			}
		}

		#endregion // SupportsDataErrorInfoResolved

		// SSP 3/14/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		#region Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		#region FilterEvaluationTrigger

		/// <summary>
		/// Specifies when the filter input into filter row cells is applied.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can set this property to change the default behavior of when the UltraGrid applies the filter that the user types into the cells of a filter row. Default behavior is to apply as soon as the user changes the value of a filter cell.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterEvaluationTrigger"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridOverride.FilterOperandStyle"/> <seealso cref="UltraGridOverride.FilterOperatorLocation"/> <seealso cref="UltraGridColumn.FilterOperandStyle"/> <seealso cref="UltraGridColumn.FilterOperatorLocation"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterEvaluationTrigger")]
		[ LocalizedCategory("LC_Appearance") ]
		public FilterEvaluationTrigger FilterEvaluationTrigger
		{
			get
			{
				return this.filterEvaluationTrigger;
			}
			set
			{
				if ( value != this.filterEvaluationTrigger )
				{
					GridUtils.ValidateEnum( "FilterEvaluationTrigger", typeof( FilterEvaluationTrigger ), value );
					this.filterEvaluationTrigger = value;
					this.NotifyPropChange( PropertyIds.FilterEvaluationTrigger, null );
				}
			}
		}

		#endregion // FilterEvaluationTrigger

		#region ShouldSerializeFilterEvaluationTrigger

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterEvaluationTrigger( )
		{
			return FilterEvaluationTrigger.Default != this.filterEvaluationTrigger;
		}

		#endregion // ShouldSerializeFilterEvaluationTrigger

		#region ResetFilterEvaluationTrigger

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterEvaluationTrigger( )
		{
			this.FilterEvaluationTrigger = FilterEvaluationTrigger.Default;
		}

		#endregion // ResetFilterEvaluationTrigger
		
		#region FilterOperandStyle

		/// <summary>
		/// Specifies the style of operand input in the filter row cells. Default is be resolved to <b>UseColumnEditor</b> for DateTime and Boolean columns and <b>Combo</b> for other types of columns. This can be overridden on each column using the column's <see cref="UltraGridColumn.FilterOperandStyle"/> property.
		/// </summary>
		///	<remarks>
		///	<p class="body">Specifies the style of operand input in the filter row cells. Default is be resolved to <b>UseColumnEditor</b> for DateTime and Boolean columns and <b>Combo</b> for other types of columns. This can be overridden on each column using the column's <see cref="UltraGridColumn.FilterOperandStyle"/> property.</p>
		///	<p class="body">Filter rows by default display user interface for selecting filter operators in addition to the filter operands. <b>FilterOperatorLocation</b> property can be used to hide this user interface so the user can't change the filter operator. You can specify what items are available in the filter operators drop down list using the <see cref="UltraGridColumn.FilterOperatorDropDownItems"/> property. You can specify the default filter operator to use using the <see cref="UltraGridColumn.FilterOperatorDefaultValue"/> property. If the filter operator ui is enabled then it's initialized to the value of the <b>FilterOperatorDefaultValue</b> property.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterOperandStyle"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridOverride.FilterOperatorLocation"/> <seealso cref="UltraGridColumn.FilterOperandStyle"/> <seealso cref="UltraGridColumn.FilterOperatorLocation"/> <seealso cref="UltraGridColumn.FilterOperatorDefaultValue"/> <seealso cref="UltraGridColumn.FilterOperatorDropDownItems"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterOperandStyle")]
		[ LocalizedCategory("LC_Appearance") ]
		public FilterOperandStyle FilterOperandStyle
		{
			get
			{
				return this.filterOperandStyle;
			}
			set
			{
				if ( value != this.filterOperandStyle )
				{
					GridUtils.ValidateEnum( "FilterOperandStyle", typeof( FilterOperandStyle ), value );
					this.filterOperandStyle = value;
					this.DirtyDefaultEditor( );
					this.NotifyPropChange( PropertyIds.FilterOperandStyle, null );
				}
			}
		}

		#endregion // FilterOperandStyle

		#region ShouldSerializeFilterOperandStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterOperandStyle( )
		{
			return FilterOperandStyle.Default != this.filterOperandStyle;
		}

		#endregion // ShouldSerializeFilterOperandStyle

		#region ResetFilterOperandStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperandStyle( )
		{
			this.FilterOperandStyle = FilterOperandStyle.Default;
		}

		#endregion // ResetFilterOperandStyle

		#region FilterOperatorDropDownItems

		/// <summary>
		/// Specifies which operators to list in the operator drop down in filter rows. Default is resolved to a value that is based on the column's data type.
		/// </summary>
		///	<remarks>
		///	<p class="body">Filter rows by default display user interface for selecting filter operators in addition to the filter operands. <see cref="UltraGridColumn.FilterOperatorLocation"/> property can be used to hide or disable this user interface so the user can't change the filter operator. You can specify the default filter operator to use using the <see cref="UltraGridColumn.FilterOperatorDefaultValue"/> property. Default is resolved to a value that is based on the column's data type.</p>
		///	<p class="body"><b>FilterOperatorDropDownItems</b> property can be used to specify which operators to include in the operator drop down list.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridColumn.FilterOperandStyle"/> <seealso cref="UltraGridColumn.FilterOperatorLocation"/> <seealso cref="UltraGridColumn.FilterOperatorDefaultValue"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterOperatorDropDownItems")]
		[ LocalizedCategory("LC_Behavior") ]
		[ Editor( typeof( Infragistics.Win.UltraWinGrid.DefaultableFlagsEnumUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		public FilterOperatorDropDownItems FilterOperatorDropDownItems
		{
			get
			{
				return this.filterOperatorDropDownItems;
			}
			set
			{
				if ( value != this.filterOperatorDropDownItems )
				{
					GridUtils.ValidateEnum( "FilterOperatorDropDownItems", typeof( FilterOperatorDropDownItems ), value );
					this.filterOperatorDropDownItems = value;
					this.NotifyPropChange( PropertyIds.FilterOperatorDropDownItems, null );
				}
			}
		}

		#endregion // FilterOperatorDropDownItems

		#region ShouldSerializeFilterOperatorDropDownItems

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterOperatorDropDownItems( )
		{
			return FilterOperatorDropDownItems.Default != this.filterOperatorDropDownItems;
		}

		#endregion // ShouldSerializeFilterOperatorDropDownItems

		#region ResetFilterOperatorDropDownItems

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperatorDropDownItems( )
		{
			this.FilterOperatorDropDownItems = FilterOperatorDropDownItems.Default;
		}

		#endregion // ResetFilterOperatorDropDownItems

		#region FilterOperatorLocation

		/// <summary>
		/// Specifies the style of operator input in the filter row cells. <b>Default</b> is resolved to <b>WithOperand</b>.
		/// </summary>
		///	<remarks>
		///	<p class="body">Specifies the style of operand input in the filter row cells. Default will be resolved to Combo. This can be overridden on each column using the <see cref="UltraGridColumn.FilterOperatorLocation"/> property.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterOperatorLocation"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridOverride.FilterOperandStyle"/> <seealso cref="UltraGridOverride.FilterOperatorDefaultValue"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterOperatorLocation")]
		[ LocalizedCategory("LC_Appearance") ]
		public FilterOperatorLocation FilterOperatorLocation
		{
			get
			{
				return this.filterOperatorLocation;
			}
			set
			{
				if ( value != this.filterOperatorLocation )
				{
					GridUtils.ValidateEnum( "FilterOperatorLocation", typeof( FilterOperatorLocation ), value );
					this.filterOperatorLocation = value;
					this.NotifyPropChange( PropertyIds.FilterOperatorLocation, null );
				}
			}
		}

		#endregion // FilterOperatorLocation

		#region ShouldSerializeFilterOperatorLocation

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterOperatorLocation( )
		{
			return FilterOperatorLocation.Default != this.filterOperatorLocation;
		}

		#endregion // ShouldSerializeFilterOperatorLocation

		#region ResetFilterOperatorLocation

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperatorLocation( )
		{
			this.FilterOperatorLocation = FilterOperatorLocation.Default;
		}

		#endregion // ResetFilterOperatorLocation

		#region FilterOperatorDefaultValue

		/// <summary>
        /// Specifies the default value of the operator cells in the filter row. If operator cells are hidden, this is used as the filter operator for values entered in the associated filter operand cells. This can be overridden on each column. Default is resolved to <b>Equals</b> for DateTime and Boolean columns and <b>StartsWith</b> for other column types.
		/// </summary>
		///	<remarks>
        ///	<p class="body">Specifies the default value of the operator cells in the filter row. If operator cells are hidden, this is used as the filter operator for values entered in the associated filter operand cells. This can be overridden on each column. Default is resolved to <b>StartsWith</b>.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridOverride.FilterOperandStyle"/> <seealso cref="UltraGridOverride.FilterOperatorLocation"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterOperatorDefaultValue")]
		[ LocalizedCategory("LC_Appearance") ]
		public FilterOperatorDefaultValue FilterOperatorDefaultValue
		{
			get
			{
				return this.filterOperatorDefaultValue;
			}
			set
			{
				if ( value != this.filterOperatorDefaultValue )
				{
					GridUtils.ValidateEnum( "FilterOperatorDefaultValue", typeof( FilterOperatorDefaultValue ), value );
					this.filterOperatorDefaultValue = value;
					this.NotifyPropChange( PropertyIds.FilterOperatorDefaultValue, null );
				}
			}
		}

		#endregion // FilterOperatorDefaultValue

		#region ShouldSerializeFilterOperatorDefaultValue

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterOperatorDefaultValue( )
		{
			return FilterOperatorDefaultValue.Default != this.filterOperatorDefaultValue;
		}

		#endregion // ShouldSerializeFilterOperatorDefaultValue

		#region ResetFilterOperatorDefaultValue

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperatorDefaultValue( )
		{
			this.FilterOperatorDefaultValue = FilterOperatorDefaultValue.Default;
		}

		#endregion // ResetFilterOperatorDefaultValue

		#region FilterCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the associated cells in filter rows.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridOverride.FilterCellAppearance"/> <seealso cref="UltraGridColumn.FilterOperatorAppearance"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilterCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilterCellAppearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.filterCellAppearanceHolder )
				{
					this.filterCellAppearanceHolder = new Infragistics.Win.AppearanceHolder();
                    
					// hook up the prop change notifications
					//
					this.filterCellAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.Layout != null)
					this.filterCellAppearanceHolder.Collection = this.Layout.Appearances;

				return this.filterCellAppearanceHolder.Appearance;
			}
			set
			{
				if ( this.filterCellAppearanceHolder == null				|| 
					!this.filterCellAppearanceHolder.HasAppearance		|| 
					value != this.filterCellAppearanceHolder.Appearance )
				{
					if ( null == this.filterCellAppearanceHolder )
					{
						this.filterCellAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.filterCellAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Layout != null)
						this.filterCellAppearanceHolder.Collection = this.Layout.Appearances;

					this.filterCellAppearanceHolder.Appearance = value;

					// notify listeners
					//
					this.NotifyPropChange( PropertyIds.FilterCellAppearance );
				}
			}
		}

		#endregion // FilterCellAppearance

		#region HasFilterCellAppearance

		internal bool HasFilterCellAppearance
		{
			get
			{
				return null != this.filterCellAppearanceHolder 
					&& this.filterCellAppearanceHolder.HasAppearance;
			}
		}

		#endregion // HasFilterCellAppearance

		#region ShouldSerializeFilterCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterCellAppearance( ) 
		{
			return this.HasFilterCellAppearance && this.filterCellAppearanceHolder.ShouldSerialize( );
		}

		#endregion // ShouldSerializeFilterCellAppearance

		#region ResetFilterCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterCellAppearance( ) 
		{
            if (null != this.filterCellAppearanceHolder)
            {
                this.filterCellAppearanceHolder.Reset();

                // MRS NAS v8.3 - Unit Testing
                this.NotifyPropChange(PropertyIds.FilterCellAppearance);
            }
		}

		#endregion // ResetFilterCellAppearance

		#region FilterOperatorAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the associated operator indicators in filter rows.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridOverride.FilterOperatorAppearance"/> <seealso cref="UltraGridColumn.FilterCellAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilterOperatorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilterOperatorAppearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.filterOperatorAppearanceHolder )
				{
					this.filterOperatorAppearanceHolder = new Infragistics.Win.AppearanceHolder();
                    
					// hook up the prop change notifications
					//
					this.filterOperatorAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.Layout != null)
					this.filterOperatorAppearanceHolder.Collection = this.Layout.Appearances;

				return this.filterOperatorAppearanceHolder.Appearance;
			}
			set
			{
				if ( this.filterOperatorAppearanceHolder == null				|| 
					!this.filterOperatorAppearanceHolder.HasAppearance		|| 
					value != this.filterOperatorAppearanceHolder.Appearance )
				{
					if ( null == this.filterOperatorAppearanceHolder )
					{
						this.filterOperatorAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.filterOperatorAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Layout != null)
						this.filterOperatorAppearanceHolder.Collection = this.Layout.Appearances;

					this.filterOperatorAppearanceHolder.Appearance = value;

					// notify listeners
					//
					this.NotifyPropChange( PropertyIds.FilterOperatorAppearance );
				}
			}
		}

		#endregion // FilterOperatorAppearance

		#region HasFilterOperatorAppearance

		internal bool HasFilterOperatorAppearance
		{
			get
			{
				return null != this.filterOperatorAppearanceHolder 
					&& this.filterOperatorAppearanceHolder.HasAppearance;
			}
		}

		#endregion // HasFilterOperatorAppearance

		#region ShouldSerializeFilterOperatorAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterOperatorAppearance( ) 
		{
			return this.HasFilterOperatorAppearance && this.filterOperatorAppearanceHolder.ShouldSerialize( );
		}

		#endregion // ShouldSerializeFilterOperatorAppearance

		#region ResetFilterOperatorAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperatorAppearance( ) 
		{
            if (null != this.filterOperatorAppearanceHolder)
            {
                this.filterOperatorAppearanceHolder.Reset();

                // MRS NAS v8.3 - Unit Testing
                this.NotifyPropChange(PropertyIds.FilterOperatorAppearance);
            }
		}

		#endregion // ResetFilterOperatorAppearance

		#region FilterEvaluationTriggerResolved
		
		internal FilterEvaluationTrigger FilterEvaluationTriggerResolved
		{
			get
			{
				if ( this.ShouldSerializeFilterEvaluationTrigger( ) )
					return this.FilterEvaluationTrigger;

				if ( this.Band.HasOverride &&
					this.Band.Override.ShouldSerializeFilterEvaluationTrigger( ) )
					return this.Band.Override.FilterEvaluationTrigger;

				if ( this.Layout.HasOverride 
					&& this.Layout.Override.ShouldSerializeFilterEvaluationTrigger( ) )
					return this.Layout.Override.FilterEvaluationTrigger;

				return FilterEvaluationTrigger.OnCellValueChange;
			}
		}

		#endregion // FilterEvaluationTriggerResolved

		#region FilterOperandStyleResolved
		
		internal FilterOperandStyle FilterOperandStyleResolved
		{
			get
			{
				if ( DefaultableBoolean.False == this.AllowRowFilteringResolved )
					return FilterOperandStyle.Disabled;

                // MBS 1/9/09 - TFS12268
                // We shouldn't use a style of FilterUIProvider if no editor is actually
                // being provided, so if this is the case we'll treat this as the default case
                //
                #region Old Code
                //if ( this.ShouldSerializeFilterOperandStyle( ) )
                //    return this.FilterOperandStyle;

                //if ( this.Band.HasOverride &&
                //    this.Band.Override.ShouldSerializeFilterOperandStyle( ) )
                //    return this.Band.Override.FilterOperandStyle;

                //if ( this.Layout.HasOverride 
                //    && this.Layout.Override.ShouldSerializeFilterOperandStyle( ) )
                //    return this.Layout.Override.FilterOperandStyle;
                #endregion //Old Code
                //
                bool hasSetStyle = false;
                FilterOperandStyle resolvedStyle = FilterOperandStyle.Default;
                if (this.ShouldSerializeFilterOperandStyle())
                {
                    resolvedStyle = this.FilterOperandStyle;
                    hasSetStyle = true;
                }
                else if (this.Band.HasOverride &&
                    this.Band.Override.ShouldSerializeFilterOperandStyle())
                {
                    resolvedStyle = this.Band.Override.FilterOperandStyle;
                    hasSetStyle = true;
                }
                else if (this.Layout.HasOverride
                    && this.Layout.Override.ShouldSerializeFilterOperandStyle())
                {
                    resolvedStyle = this.Layout.Override.FilterOperandStyle;
                    hasSetStyle = true;
                }
                //
                if (hasSetStyle && (resolvedStyle != FilterOperandStyle.FilterUIProvider ||
                    (this.Band.FilterUIProviderResolved != null &&
                    this.Band.FilterUIProviderResolved.GetFilterCellEditor(this) != null)))
                {
                    return resolvedStyle;
                }

                this.VerifyFilterRowRelatedCache( );
				return this.cachedDefaultFilterOperandStyle;
			}
		}

		#endregion // FilterOperandStyleResolved

		#region FilterOperatorLocationResolved
		
		internal FilterOperatorLocation FilterOperatorLocationResolved
		{
			get
			{
				if ( this.ShouldSerializeFilterOperatorLocation( ) )
					return this.FilterOperatorLocation;

				if ( this.Band.HasOverride &&
					this.Band.Override.ShouldSerializeFilterOperatorLocation( ) )
					return this.Band.Override.FilterOperatorLocation;

				if ( this.Layout.HasOverride 
					&& this.Layout.Override.ShouldSerializeFilterOperatorLocation( ) )
					return this.Layout.Override.FilterOperatorLocation;

				this.VerifyFilterRowRelatedCache( );
				return this.cachedDefaultFilterOperatorLocation;
			}
		}

		#endregion // FilterOperatorLocationResolved

		#region FilterOperatorEditor

		internal EmbeddableEditorBase FilterOperatorEditor
		{
			get
			{
				return this.band.Layout.GetFilterOperatorEditor( this );
			}
		}

		#endregion // FilterOperatorEditor

		#region FilterOperandEditor

		internal EmbeddableEditorBase FilterOperandEditor
		{
			get
			{
				return this.band.Layout.GetFilterOperandEditor( this );
			}
		}

		#endregion // FilterOperandEditor

		#region FilterOperatorDropDownItemsResolved
		
		internal FilterOperatorDropDownItems FilterOperatorDropDownItemsResolved
		{
			get
			{
				if ( this.ShouldSerializeFilterOperatorDropDownItems( ) )
					return this.FilterOperatorDropDownItems;

				if ( this.Band.HasOverride &&
					this.Band.Override.ShouldSerializeFilterOperatorDropDownItems( ) )
					return this.Band.Override.FilterOperatorDropDownItems;

				if ( this.Layout.HasOverride 
					&& this.Layout.Override.ShouldSerializeFilterOperatorDropDownItems( ) )
					return this.Layout.Override.FilterOperatorDropDownItems;

				this.VerifyFilterRowRelatedCache( );
				return this.cachedDefaultFilterOperatorDropDownItems;
			}
		}

		#endregion // FilterFilterOperatorDropDownItemsStyleResolved

		#region FilterOperatorDefaultValueResolved
		
		internal FilterOperatorDefaultValue FilterOperatorDefaultValueResolved
		{
			get
			{
				if ( this.ShouldSerializeFilterOperatorDefaultValue( ) )
					return this.FilterOperatorDefaultValue;

				if ( this.Band.HasOverride &&
					this.Band.Override.ShouldSerializeFilterOperatorDefaultValue( ) )
					return this.Band.Override.FilterOperatorDefaultValue;

				if ( this.Layout.HasOverride 
					&& this.Layout.Override.ShouldSerializeFilterOperatorDefaultValue( ) )
					return this.Layout.Override.FilterOperatorDefaultValue;

				this.VerifyFilterRowRelatedCache( );
				return this.cachedDefaultFilterOperatorDefaultValue;
			}
		}

		#endregion // FilterFilterOperatorDefaultValueStyleResolved

		#region VerifyFilterRowRelatedCache

		private void VerifyFilterRowRelatedCache( )
		{
			if ( this.verifiedFilterRowCacheVersion != this.verifyEditorVersionNumber )
			{
				this.verifiedFilterRowCacheVersion = this.verifyEditorVersionNumber;

				// By default display the operator with the operand.
				//
				this.cachedDefaultFilterOperatorLocation = FilterOperatorLocation.WithOperand;

                // MBS 12/4/08 - NA9.1 Excel Style Filtering
                //
                // MBS 1/9/09 - TFS12268
                // Reconsidering this, we should ask for the editor of the column at this point when
                // determining what the default operand style is.  It is noted that the implementer
                // of the IFilterUIProvider interface is responsible for caching the editor, so 
                // this shouldn't be a big deal to check this here
                //
                //bool hasFilterUIProvider = this.Band != null && this.Band.FilterUIProviderResolved != null;
                bool hasFilterUIProvider = this.Band != null && this.Band.FilterUIProviderResolved != null &&
                    this.Band.FilterUIProviderResolved.GetFilterCellEditor(this) != null;

                // MBS 4/8/09 - TFS16454
                // We should be looking at the underlying type when determining which editor to use
                //
				//Type dataType = this.DataType;
                Type dataType = Utilities.GetUnderlyingType(this.DataType);

				if ( ( this.IsColumnNumeric || typeof( DateTime ) == dataType )
					// SSP 9/11/06 BR14332
					// If there is a value list then treat the column as string column.
					// 
					&& ! GridUtils.DisplaysTextValues( this ) )
				{
					this.cachedDefaultFilterOperatorDefaultValue = FilterOperatorDefaultValue.Equals;

                    // MBS 12/4/08 - NA9.1 Excel Style Filtering
                    //this.cachedDefaultFilterOperandStyle = typeof(DateTime) == dataType
                    //    ? FilterOperandStyle.UseColumnEditor : FilterOperandStyle.Combo;
                    //
                    if (hasFilterUIProvider)
                        this.cachedDefaultFilterOperandStyle = FilterOperandStyle.FilterUIProvider;
                    else
                    {
                        this.cachedDefaultFilterOperandStyle = typeof(DateTime) == dataType
                            ? FilterOperandStyle.UseColumnEditor : FilterOperandStyle.Combo;
                    }

					this.cachedDefaultFilterOperatorDropDownItems = 
						FilterOperatorDropDownItems.Equals
						| FilterOperatorDropDownItems.NotEquals
						| FilterOperatorDropDownItems.GreaterThan
						| FilterOperatorDropDownItems.GreaterThanOrEqualTo
						| FilterOperatorDropDownItems.LessThan
						| FilterOperatorDropDownItems.LessThanOrEqualTo;
				}
				else if ( typeof( bool ) == dataType )
				{
					this.cachedDefaultFilterOperatorDefaultValue = FilterOperatorDefaultValue.Equals;

                    // MBS 12/4/08 - NA9.1 Excel Style Filtering
					//this.cachedDefaultFilterOperandStyle = FilterOperandStyle.UseColumnEditor;
                    this.cachedDefaultFilterOperandStyle = hasFilterUIProvider 
                        ? FilterOperandStyle.FilterUIProvider : FilterOperandStyle.UseColumnEditor;
					
                    this.cachedDefaultFilterOperatorDropDownItems = FilterOperatorDropDownItems.Equals;
				}
				else 
				{
					this.cachedDefaultFilterOperatorDefaultValue = FilterOperatorDefaultValue.StartsWith;

                    // MBS 12/4/08 - NA9.1 Excel Style Filtering
					//this.cachedDefaultFilterOperandStyle = FilterOperandStyle.Combo;
                    this.cachedDefaultFilterOperandStyle = hasFilterUIProvider
                        ? FilterOperandStyle.FilterUIProvider : FilterOperandStyle.Combo;

					this.cachedDefaultFilterOperatorDropDownItems = FilterOperatorDropDownItems.All;
				}

				// SSP 5/12/05 BR03904
				// If the user explicitly set the default operator then always include it in the filter
				// operator drop down list even if it's not appropriate for the column's data type.
				//
				FilterOperatorDropDownItems defaultOperator = this.Layout.GetAssociatedFilterDropDownItem( this.FilterOperatorDefaultValueResolved );
				this.cachedDefaultFilterOperatorDropDownItems |= defaultOperator;

                // MBS 1/9/09 - TFS12272
                // By default don't show the filter operator when we're using a filter provider
                if (hasFilterUIProvider)
                    this.cachedDefaultFilterOperatorLocation = FilterOperatorLocation.Hidden;
                else
                {
                    // If number of items in the filter operator drop down is 1 then hide the filter
                    // operator button.
                    //
                    int operatorOptions = (int)this.FilterOperatorDropDownItemsResolved;
                    int numOfOperatorOptions = 0;
                    while (0 != operatorOptions)
                    {
                        operatorOptions >>= 1;
                        if (0 != (1 & operatorOptions))
                            numOfOperatorOptions++;
                    }

                    if (numOfOperatorOptions <= 1)
                        this.cachedDefaultFilterOperatorLocation = FilterOperatorLocation.Hidden;
                }
			}
		}

		#endregion // VerifyFilterRowRelatedCache

		#region ResolveFilterOperatorAppearance

		internal void ResolveFilterOperatorAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			// SSP 3/12/06 - App Styling
			// 
			AppStyling.ResolutionOrderInfo order;
			AppStyling.UIRole role = StyleUtils.GetRole( this, StyleUtils.Role.FilterOperator, out order );

			if ( this.HasFilterOperatorAppearance )
			{
				// SSP 3/8/06 - App Styling
				// Added if condition to the existing code.
				// 
				if ( order.UseControlInfo )
					this.FilterOperatorAppearance.MergeData( ref appData, ref flags );
			}

			this.Band.MergeBLOverrideAppearances(
				ref appData, ref flags, UltraGridOverride.OverrideAppearanceIndex.FilterOperatorAppearance
				// SSP 3/13/06 - App Styling
				// Use the new overload that takes in the app styling related params.
				// 
				, role, order, AppStyling.RoleState.Normal );

			if ( 0 != ( AppearancePropFlags.BorderColor & flags ) )
			{
				appData.BorderColor = SystemColors.ControlDark;
				flags ^= AppearancePropFlags.BorderColor;
			}

			// Align the filter operator symbols in the middle.
			//
			if ( 0 != ( AppearancePropFlags.ImageVAlign & flags ) )
			{
				appData.ImageVAlign = VAlign.Middle;
				flags ^= AppearancePropFlags.ImageVAlign;
			}
		}

		#endregion // ResolveFilterOperatorAppearance

		#region FilterOperandEditorOwnerInfo

		internal FilterOperandEmbeddableEditorOwnerInfo FilterOperandEditorOwnerInfo
		{
			get
			{
				if ( null == this.filterOperandEditorOwnerInfo )
					this.filterOperandEditorOwnerInfo = new FilterOperandEmbeddableEditorOwnerInfo( this );

				return this.filterOperandEditorOwnerInfo;
			}
		}

		#endregion // FilterOperandEditorOwnerInfo

		#region FilterOperatorEditorOwnerInfo
		
		internal FilterOperatorEmbeddableEditorOwnerInfo FilterOperatorEditorOwnerInfo
		{
			get
			{
				if ( null == this.filterOperatorEditorOwnerInfo )
					this.filterOperatorEditorOwnerInfo = new FilterOperatorEmbeddableEditorOwnerInfo( this );

				return this.filterOperatorEditorOwnerInfo;
			}
		}

		#endregion // FilterOperatorEditorOwnerInfo

		#region FilterComparisonType

		/// <summary>
		/// Specifies whether the filtering is performed case-sensitive. Default is to perform filtering case-insensitive.
		/// </summary>
		///	<remarks>
		///	<p class="body">By default the UltraGrid performs filtering using case-insensitive string comparison. You can set the <b>FilterComparisonType</b> property to <b>CaseSensitive</b> to perform filtering using case sensitve string comparisons.</p>
		///	<seealso cref="UltraGridColumn.FilterComparisonType"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterComparisonType") ]
		[ LocalizedCategory("LC_Behavior") ]
		public FilterComparisonType FilterComparisonType
		{
			get
			{
				return this.filterComparisonType;
			}
			set
			{
				if ( value != this.filterComparisonType )
				{
					GridUtils.ValidateEnum( "FilterComparisonType", typeof( FilterComparisonType ), value );
					this.filterComparisonType = value;
					this.NotifyPropChange( PropertyIds.FilterComparisonType, null );
				}
			}
		}

		#endregion // FilterComparisonType

		#region ShouldSerializeFilterComparisonType

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterComparisonType( )
		{
			return FilterComparisonType.Default != this.filterComparisonType;
		}

		#endregion // ShouldSerializeFilterComparisonType

		#region ResetFilterComparisonType

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterComparisonType( )
		{
			this.FilterComparisonType = FilterComparisonType.Default;
		}

		#endregion // ResetFilterComparisonType

		#region FilterComparisonTypeResolved

		internal FilterComparisonType FilterComparisonTypeResolved
		{
			get
			{
				FilterComparisonType ret = this.FilterComparisonType;
				if ( FilterComparisonType.Default == ret )
				{
					if ( this.band.HasOverride )
						ret = this.band.Override.FilterComparisonType;

					if ( FilterComparisonType.Default == ret && this.band.Layout.HasOverride )
						ret = this.band.Layout.Override.FilterComparisonType;
				}

				// By default perform case insensitive filter comparisons. Note that this
				// changes the default behavior in v5.2. v5.1 and older versions performed
				// case sensitive comparisons. It's imperative that now we perform case
				// insensitive comparisons by default because of the filter row.
				//
				return FilterComparisonType.Default != ret ? ret : FilterComparisonType.CaseInsensitive;
			}
		}

		#endregion // FilterComparisonTypeResolved

		#region FilterClearButtonVisible

		// SSP 6/8/05 - NAS 5.2 Filter Row
		// Added FilterClearButtonLocation on the column as well for greater flexibility.
		//
		/// <summary>
		/// Specifies whether to display the filter clear button in the filter cells fo this column. <b>Default</b> is resolved to <b>True</b>.
		/// </summary>
		///	<remarks>
		///	<p class="body">Specifies whether to display the filter clear button in the filter cells fo this column. <b>Default</b> is resolved to <b>True</b>.</p>
		///	<p class="body">You can use the <see cref="UltraGridOverride.FilterClearButtonLocation"/> property to hide or display filter clear buttons on all the columns of a particular band or the whole grid.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterClearButtonLocation"/> <seealso cref="UltraGridOverride.FilterClearButtonLocation"/> <seealso cref="UltraGridOverride.FilterClearButtonAppearance"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridColumn_P_FilterClearButtonVisible")]
		[ LocalizedCategory("LC_Behavior") ]
		public DefaultableBoolean FilterClearButtonVisible
		{
			get
			{
				return this.filterClearButtonVisible;
			}
			set
			{
				if ( value != this.filterClearButtonVisible )
				{
					GridUtils.ValidateEnum( "FilterClearButtonVisible", typeof( DefaultableBoolean ), value );
					this.filterClearButtonVisible = value;
					this.NotifyPropChange( PropertyIds.FilterClearButtonVisible, null );
				}
			}
		}

		#endregion // FilterClearButtonVisible

		#region ShouldSerializeFilterClearButtonVisible

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterClearButtonVisible( )
		{
			return DefaultableBoolean.Default != this.FilterClearButtonVisible;
		}

		#endregion // ShouldSerializeFilterClearButtonVisible

		#region ResetFilterClearButtonVisible

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterClearButtonVisible( )
		{
			this.FilterClearButtonVisible = DefaultableBoolean.Default;
		}

		#endregion // ResetFilterClearButtonVisible

		#region FilterClearButtonVisibleResolved
		
		internal bool FilterClearButtonVisibleResolved
		{
			get
			{
				if ( DefaultableBoolean.Default != this.filterClearButtonVisible )
					return DefaultableBoolean.True == this.filterClearButtonVisible;

				FilterClearButtonLocation loc = this.band.FilterClearButtonLocationResolved;
				return FilterClearButtonLocation.Cell == loc || FilterClearButtonLocation.RowAndCell == loc;
			}
		}

		#endregion // FilterClearButtonVisibleResolved

		#endregion // Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		// JAS 2005 v2 XSD Support
		//
		#region XSD Support

			#region Constraint [internal property]

		internal Infragistics.Win.ValueConstraint Constraint
		{
			get
			{
				if( this.valueConstraint == null )
					this.valueConstraint = new Infragistics.Win.ValueConstraint();

				return this.valueConstraint;
			}
		}

			#endregion // Constraint [internal property]

			#region MaxLength

		// This property replaces the old FieldLen property.
		//
		/// <summary>
		/// Returns or sets the maximum length of the text that can be entered into a cell. Zero indicates that there is no limit.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MaxLength</b> property gives you the ability to limit the amount of text that can be entered in column cells. You can use this property to enforce database-specific or application specific limitations.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridColumn_P_MaxLength")]
		[LocalizedCategory("LC_Behavior")]
		public int MaxLength
		{
			get 
			{
				//check for bound property
				if(!this.bound)
				{
					return this.Constraint.MaxLength;
				}
				else
				{
					if ( this.ShouldSerializeMaxLength() )
						return this.Constraint.MaxLength;

					// If the column has an underlying data column associated with it, then get the
					// MaxLength off of it.
					//
					System.Data.DataColumn dataColumn = this.GetDataColumn( );

                    if (null != dataColumn)
                    {
                        // MRS NAS v8.3 - Unit Testing
                        // The DataColumn uses -1 as the default (no MaxLength).
                        // But we use 0 and -1 will raise an exception. 
                        //return dataColumn.MaxLength;
                        if (dataColumn.MaxLength > 0)
                            return dataColumn.MaxLength;
                    }

					// Return 0 otherwise which means that there are no restrictions on the max length.
					//
					return 0;
				}				
			}
			set
			{
				if ( value < 0 )
					throw new ArgumentOutOfRangeException( "MaxLength", value, SR.GetString( "LER_ArgumentException_15" ) );

                // MRS NAS v8.3 - Unit Testing
                if (this.Constraint.MaxLength == value)
                    return;

				this.Constraint.MaxLength = value;

				// If this property had been set by the EnforceXsdConstraints method, then
				// clear the bit which represents this property so that the ClearXsdConstraints
				// method will know not to reset this value.
				//
				if( (this.XsdSuppliedConstraints & XsdConstraintFlags.MaxLength) != 0 )
					this.XsdSuppliedConstraints &= ~XsdConstraintFlags.MaxLength;

				this.NotifyPropChange( PropertyIds.MaxLengthColumn );
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeMaxLength() 
		{
			return this.Constraint.HasMaxLength;
		}
 
		/// <summary>
		/// Reset MaxLength to its default value (0).
		/// </summary>
		public void ResetMaxLength() 
		{
			// JAS 5/11/05 BR03915 - The check for IsBound is not necessary any more since
			// we will use the data source's MaxLength if this the column's MaxLength is 0.
			//
			// AS - 10/29/01
			// We are throwing an exception when we are invoking this on a bound column
			// since its field length cannot be set. We should skip bound columns.
			//if( ! this.IsBound )
				this.MaxLength = 0;
		}

			#endregion // MaxLength

			#region MaxValueExclusive

		/// <summary>
		/// Gets/sets the first value greater than the maximum value that can be entered in the cell. 
		/// Default value is null (Nothing) meaning no maximum constraint. 
		/// The object assigned to this property should be the same type as the column's <see cref="DataType"/> 
		/// or compatible otherwise it won't be honored.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridColumn_P_MaxValueExclusive")]
		[ LocalizedCategory("LC_Behavior") ]
		[ System.ComponentModel.Editor( typeof( PrimitiveTypeUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		[ TypeConverter(typeof(PrimitiveTypeConverter))]
		[ Localizable( true ) ]
		public object MaxValueExclusive
		{
			get
			{
				return this.Constraint.MaxExclusive;
			}
			set
			{
				if( this.Constraint.MaxExclusive != value )
				{
					this.Constraint.MaxExclusive = value;

					// If this property had been set by the EnforceXsdConstraints method, then
					// clear the bit which represents this property so that the ClearXsdConstraints
					// method will know not to reset this value.
					//
					if( (this.XsdSuppliedConstraints & XsdConstraintFlags.MaxExclusive) != 0 )
						this.XsdSuppliedConstraints &= ~XsdConstraintFlags.MaxExclusive;

					this.NotifyPropChange( PropertyIds.MaxValueExclusive );
				}
			}
		}

		/// <summary>
		/// Resets MaxValueExclusive to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMaxValueExclusive( ) 
		{
			this.MaxValueExclusive = null;
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeMaxValueExclusive()
		{
			return Utils.IsSafelySerializable( this.MaxValueExclusive );
		}

			#endregion // MaxValueExclusive

			#region MinLength

		/// <summary>
		/// Gets/sets the minimum length of the text that can be entered into a cell. The default value is 0.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MinLength</b> property gives you the ability to require a certain amount of text that must be entered in column cells. You can use this property to enforce database-specific or application specific limitations.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridColumn_P_MinLength")]
		[LocalizedCategory("LC_Behavior")]
		public int MinLength
		{
			get
			{
				return this.Constraint.MinLength;
			}
			set
			{
				if( this.Constraint.MinLength != value )
				{
					this.Constraint.MinLength = value;

					// If this property had been set by the EnforceXsdConstraints method, then
					// clear the bit which represents this property so that the ClearXsdConstraints
					// method will know not to reset this value.
					//
					if( (this.XsdSuppliedConstraints & XsdConstraintFlags.MinLength) != 0 )
						this.XsdSuppliedConstraints &= ~XsdConstraintFlags.MinLength;

					this.NotifyPropChange( PropertyIds.MinLengthColumn );
				}
			}
		}

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMinLength()
		{
			return this.Constraint.HasMinLength;
		}

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetMinLength()
		{
			this.MinLength = 0;
		}

			#endregion // MinLength

			#region MinValueExclusive

		/// <summary>
		/// Gets/sets the first value less than the minimum value that can be entered in the cell. 
		/// Default value is null (Nothing) meaning no maximum constraint. 
		/// The object assigned to this property should be the same type as the column's <see cref="DataType"/> 
		/// or compatible otherwise it won't be honored.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridColumn_P_MinValueExclusive")]
		[ LocalizedCategory("LC_Behavior") ]
		[ System.ComponentModel.Editor( typeof( PrimitiveTypeUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		[ TypeConverter(typeof(PrimitiveTypeConverter))]
		[ Localizable( true ) ]
		public object MinValueExclusive
		{
			get
			{
				return this.Constraint.MinExclusive;
			}
			set
			{
				if( this.Constraint.MinExclusive != value )
				{
					this.Constraint.MinExclusive = value;

					// If this property had been set by the EnforceXsdConstraints method, then
					// clear the bit which represents this property so that the ClearXsdConstraints
					// method will know not to reset this value.
					//
					if( (this.XsdSuppliedConstraints & XsdConstraintFlags.MinExclusive) != 0 )
						this.XsdSuppliedConstraints &= ~XsdConstraintFlags.MinExclusive;

					this.NotifyPropChange( PropertyIds.MinValueExclusive );
				}
			}
		}

		/// <summary>
		/// Resets MinValueExclusive to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetMinValueExclusive( ) 
		{
			this.MinValueExclusive = null;
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeMinValueExclusive()
		{
			return Utils.IsSafelySerializable( this.MinValueExclusive );
		}

			#endregion // MinValueExclusive

			#region RegexPattern

		/// <summary>
		/// Gets/sets a regular expression to which the cell values must conform.  All of the characters in the cell's text must be a part of the match for validation to succeed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The regular expression given to this property will be used during validation of the cell values in the column.  As the user attempts to move the input focus out of the cell, the text in the cell will be validated against the regular expression.  For the validation to succeed, all of the text in the cell must constitute a single match.  If the cell contains a match <b>and</b> other characters then the validation will fail.  For example, if the regular expression is "\d{2}" (meaning, two consecutive digit characters) and the cell's text is "12" then the validation will succeed.  However, if the cell's text is "123" then the validation will fail because of the extra digit ("3") that follows the match ("12").  This behavior can be altered by appending or prepending ".*" to the regular expression, meaning that any number of characters can precede or follow the target match.</p>
		/// </remarks>
		[LocalizedCategory("LC_Behavior")]
		[LocalizedDescription("LD_UltraGridColumn_P_RegexPattern")]
		[Editor( typeof(RegexUITypeEditor), typeof(UITypeEditor) )]
		public string RegexPattern
		{
			get
			{
				return this.Constraint.RegexPattern;
			}
			set
			{
				if( this.Constraint.RegexPattern != value )
				{
					this.Constraint.RegexPattern = value;

					// If this property had been set by the EnforceXsdConstraints method, then
					// clear the bit which represents this property so that the ClearXsdConstraints
					// method will know not to reset this value.
					//
					if( (this.XsdSuppliedConstraints & XsdConstraintFlags.RegexPattern) != 0 )
						this.XsdSuppliedConstraints &= ~XsdConstraintFlags.RegexPattern;

					this.NotifyPropChange( PropertyIds.RegexPattern );
				}
			}
		}

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRegexPattern( )
		{
			return this.Constraint.HasRegexPattern;
		}

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetRegexPattern( )
		{
			this.RegexPattern = null;
		}

			#endregion // RegexPattern

			#region XsdSuppliedConstraints

		internal XsdConstraintFlags XsdSuppliedConstraints
		{
			get { return this.xsdSuppliedConstraints;  }
			set { this.xsdSuppliedConstraints = value; }
		}

			#endregion // XsdSuppliedConstraints

		#endregion // XSD Support

		// JAS v5.2 GroupBy Break Behavior 5/3/05
		//
		#region GroupByMode Logic

			#region GroupByMode Property

				#region GroupByMode

		/// <summary>
		/// Gets/sets the setting which determines how the values in this column will be grouped when using the 'OutlookGroupBy' setting for <see cref="UltraGridLayout.ViewStyleBand"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property is of type <see cref="Infragistics.Win.UltraWinGrid.GroupByMode"/>.  Not all of the available settings make sense for all columns.  The settings that relate to dates and times should only be used with columns whose DataType is <see cref="System.DateTime"/>.  If this property is set to a value which does not make sense for the DataType of the column, the groupings will based on a "dummy" default value whenever possible.</p>
		/// <p class="body">
		/// To enable the funtionality for grouping rows, set the Layout's
		/// <see cref="UltraGridLayout.ViewStyleBand"/> property to 
		/// <b>OutlookGroupBy</b>. This will display a group-by box on the top
		/// of the grid where the user can drag and drop a column to group rows
		/// by that column. To group rows by a column in code, add the column to
		/// the <see cref="UltraGridBand.SortedColumns"/> collection and specify
		/// true for the groupBy parameter of the <b>Add</b> method.
		/// </p>
		/// <seealso cref="UltraGridOverride.AllowGroupBy"/>
		/// <seealso cref="UltraGridLayout.ViewStyleBand"/>
		/// <seealso cref="UltraGridLayout.GroupByBox"/>
		/// <seealso cref="UltraGridOverride.GroupByColumnsHidden"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridColumn_P_GroupByMode")]
		[LocalizedCategory("LC_Behavior")]
		public GroupByMode GroupByMode
		{
			get
			{
				return this.groupByMode;
			}
			set
			{
				if( this.GroupByMode != value )
				{
					GridUtils.ValidateEnum( typeof(GroupByMode), value );

					this.groupByMode = value;

					this.NotifyPropChange( PropertyIds.GroupByMode );
				}
			}
		}

				#endregion // GroupByMode

				#region ShouldSerializeGroupByMode

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeGroupByMode( )
		{
			return GroupByMode.Default != this.groupByMode;
		}

				#endregion // ShouldSerializeGroupByMode

				#region ResetGroupByMode

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetGroupByMode( )
		{
			this.GroupByMode = GroupByMode.Default;
		}

				#endregion // ResetGroupByMode

			#endregion // GroupByMode Property

			#region GroupByModeResolved

		internal GroupByMode GroupByModeResolved
		{
			get
			{
				if( this.GroupByMode != GroupByMode.Default )
					return this.GroupByMode;

				return GroupByMode.Value;
			}
		}

			#endregion // GroupByModeResolved

			#region GroupByEvaluatorResolved

		internal IGroupByEvaluator GroupByEvaluatorResolved
		{
			get
			{
				if( this.GroupByEvaluator != null )
					return this.GroupByEvaluator;

				return GroupByEvaluatorFactory.CreateEvaluator( this );
			}
		}

			#endregion // GroupByEvaluatorResolved

		#endregion // GroupByMode Logic

		// JAS 3/17/05 BR02741
		//
		#region IsInDesigner

		private bool IsInDesigner
		{
			get
			{
				if( this.band != null && this.band.Layout != null && this.band.Layout.Grid != null )
				{
					Win.UltraWinGrid.Design.IGridDesignInfo designInfo = this.Band.Layout.Grid as Win.UltraWinGrid.Design.IGridDesignInfo;
					if( designInfo != null && designInfo.DesignMode )
						return true;
				}

				return false;
			}
		}

		#endregion // IsInDesigner

		// JAS v5.2 GroupBy Break Behavior 5/4/05
		//
		#region SortComparisonType Logic

			#region SortComparisonType

		/// <summary>
		/// Gets/sets the type of sorting which will be performed the column.
		/// Note, this setting can be set for all columns via the <b>SortComparisonType</b> property on the Override object.
		/// </summary>
		[LocalizedDescription("LD_UltraGridColumn_P_SortComparisonType")]
		[LocalizedCategory("LC_Behavior")]
		public SortComparisonType SortComparisonType
		{
			get
			{
				return this.sortComparisonType;
			}
			set
			{
				if( this.SortComparisonType != value )
				{
					GridUtils.ValidateEnum( typeof(SortComparisonType), value );

					this.sortComparisonType = value;

					this.NotifyPropChange( PropertyIds.SortComparisonType );
				}
			}
		}

			#endregion // SortComparisonType

			#region ShouldSerializeSortComparisonType

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSortComparisonType()
		{
			return SortComparisonType.Default != this.sortComparisonType;
		}

			#endregion // ShouldSerializeSortComparisonType

			#region ResetSortComparisonType

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSortComparisonType()
		{
			this.SortComparisonType = SortComparisonType.Default;
		}

			#endregion // ResetSortComparisonType

			#region SortComparisonTypeResolved

		internal SortComparisonType SortComparisonTypeResolved
		{
			get
			{
				if( this.SortComparisonType != SortComparisonType.Default )
					return this.SortComparisonType;

				if( this.Band != null		&& 
					this.Band.HasOverride	&& 
					this.Band.Override.SortComparisonType != SortComparisonType.Default )
					return this.Band.Override.SortComparisonType;

				if( this.Band != null				&&
					this.Band.Layout != null		&&
					this.Band.Layout.HasOverride	&&
					this.Band.Layout.Override.SortComparisonType != SortComparisonType.Default )
					return this.Band.Layout.Override.SortComparisonType;

				return SortComparisonType.CaseSensitive;
			}
		}

			#endregion // SortComparisonTypeResolved

		#endregion // SortComparisonType Logic

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		// 
		#region Column Chooser Functionality

		#region ColumnChooserCaption

		/// <summary>
		/// The caption displayed in the column�s header in the column chooser. 
		/// Default is resolved to the column header�s Caption.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridColumn_P_ColumnChooserCaption")]
		[ LocalizedCategory("LC_Appearance") ]
		public string ColumnChooserCaption
		{
			get
			{
				return this.columnChooserCaption;
			}
			set
			{
				if ( value != this.columnChooserCaption )
				{
					this.columnChooserCaption = value;

					this.NotifyPropChange( PropertyIds.ColumnChooserCaption );
				}
			}
		}

		#endregion // ColumnChooserCaption

		#region ColumnChooserCaption

		internal string ColumnChooserCaptionResolved
		{
			get
			{
				if ( null != this.ColumnChooserCaption && this.ColumnChooserCaption.Length > 0 )
					return this.ColumnChooserCaption;

				return this.Header.Caption;
			}
		}

		#endregion // ColumnChooserCaption

		#region ShouldSerializeColumnChooserCaption

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeColumnChooserCaption( )
		{
			return null != this.columnChooserCaption && this.columnChooserCaption.Length > 0;
		}

		#endregion // ShouldSerializeColumnChooserCaption

		#region ResetColumnChooserCaption

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetColumnChooserCaption( )
		{
			this.ColumnChooserCaption = null;
		}

		#endregion // ResetColumnChooserCaption

		#region ExcludeFromColumnChooser

		/// <summary>
		/// Forces the column to be excluded from the column chooser control. This also 
		/// specifies whether the user can unhide the column.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Set the <b>ExcludeFromColumnChooser</b> property of a column to <b>True</b> if you want to
		/// exlude it from the column chooser. Excluding a column from the column chooser will cause
		/// the <see cref="UltraGridColumnChooser"/> control to not display the column.
		/// This effectively prevents the user from hiding or unhding the column via
		/// the column chooser.
		/// </p>
		/// <p class="body">
		/// The Band object also exposes <see cref="UltraGridBand.ExcludeFromColumnChooser"/> property that lets
		/// you exlcude entire band its columns.
		/// </p>
		/// <seealso cref="UltraGridBand.ExcludeFromColumnChooser"/>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// <seealso cref="ColumnChooserDialog"/>
		/// <seealso cref="UltraGridBase.ShowColumnChooser()"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridColumn_P_ExcludeFromColumnChooser")]
		[ LocalizedCategory("LC_Behavior") ]
		public ExcludeFromColumnChooser ExcludeFromColumnChooser
		{
			get
			{
				return this.excludeFromColumnChooser;
			}
			set
			{
				if ( value != this.excludeFromColumnChooser )
				{
					GridUtils.ValidateEnum( typeof( ExcludeFromColumnChooser ), value );

					this.excludeFromColumnChooser = value;

					this.NotifyPropChange( PropertyIds.ExcludeFromColumnChooser );
				}
			}
		}

		#endregion // ExcludeFromColumnChooser

		#region ExcludeFromColumnChooserResolved

		internal ExcludeFromColumnChooser ExcludeFromColumnChooserResolved
		{
			get
			{
				// ExcludeFromColumnChooser has reverse precedence order where the band has a higher
				// precedence than the column.
				// 
				if ( ExcludeFromColumnChooser.Default != this.Band.ExcludeFromColumnChooser )
					return this.Band.ExcludeFromColumnChooser;

				if ( ExcludeFromColumnChooser.Default != this.ExcludeFromColumnChooser )
					return this.ExcludeFromColumnChooser;

				// Default to false
				// 
				return ExcludeFromColumnChooser.False;
			}
		}

		#endregion // ExcludeFromColumnChooserResolved
		
		#region ColumnChooserEnabledResolved

		internal bool ColumnChooserEnabledResolved
		{
			get
			{
				if ( ExcludeFromColumnChooser.False == this.ExcludeFromColumnChooserResolved )
				{
					UltraGridLayout layout = this.Layout;
					UltraGridBase grid = null != layout ? layout.Grid : null;

					if ( null != layout && DefaultableBoolean.Default != layout.ColumnChooserEnabled )
						return DefaultableBoolean.True == layout.ColumnChooserEnabled;

					if ( null != grid && grid.columnChooserHasBeenDispalyedAtLeastOnce )
						return true;

					if ( null != this.Band 
						&& ( RowSelectorHeaderStyle.ColumnChooserButton == this.Band.RowSelectorHeaderStyleResolved 
							// SSP 12/8/05 BR06669
							// Added row selector header style of ColumnChooserButtonFixedSize.
							// 
							|| RowSelectorHeaderStyle.ColumnChooserButtonFixedSize == this.Band.RowSelectorHeaderStyleResolved )
						&& this.Band.RowSelectorExtent > 0 )
						return true;					
				}

				return false;
			}
		}

		#endregion // ColumnChooserEnabledResolved

		#region ShouldSerializeExcludeFromColumnChooser

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeExcludeFromColumnChooser( )
		{
			return ExcludeFromColumnChooser.Default != this.excludeFromColumnChooser;
		}

		#endregion // ShouldSerializeExcludeFromColumnChooser

		#region ResetExcludeFromColumnChooser

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetExcludeFromColumnChooser( )
		{
			this.ExcludeFromColumnChooser = ExcludeFromColumnChooser.Default;
		}

		#endregion // ResetExcludeFromColumnChooser

		#region HiddenWhenGroupByOverride

		internal DefaultableBoolean HiddenWhenGroupByOverride
		{
			get
			{
				return this.hiddenWhenGroupByOverride;
			}
			set
			{
				if ( value != this.hiddenWhenGroupByOverride )
				{
					GridUtils.ValidateEnum( typeof( DefaultableBoolean ), value );

					this.hiddenWhenGroupByOverride = value;

					this.NotifyPropChange( PropertyIds.HiddenWhenGroupBy );
				}
			}
		}

		#endregion // HiddenWhenGroupByOverride

		#region ShouldSerializeHiddenWhenGroupByOverride

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeHiddenWhenGroupByOverride( )
		{
			return DefaultableBoolean.Default != this.hiddenWhenGroupByOverride;
		}

		#endregion // ShouldSerializeHiddenWhenGroupByOverride

		#region ResetHiddenWhenGroupByOverride

		internal void ResetHiddenWhenGroupByOverride( )
		{
			this.HiddenWhenGroupByOverride = DefaultableBoolean.Default;
		}

		#endregion // ResetHiddenWhenGroupByOverride

		#region ResetColumnChooserStateCache

		/// <summary>
		/// Resets any column chooser related information that's cached by the column chooser
		/// related column drag-and-drop operations.
		/// </summary>
		public void ResetColumnChooserStateCache( )
		{
			this.ResetHiddenWhenGroupByOverride( );
		}

		#endregion // ResetColumnChooserStateCache

		#region HiddenWhenGroupByResolved

		// SSP 6/30/05 - NAS 5.3 Column Chooser
		// Added HiddenWhenGroupByResolved on the column.
		// 
		internal bool HiddenWhenGroupByResolved
		{
			get
			{
				DefaultableBoolean ret = this.HiddenWhenGroupByOverride;

				if ( DefaultableBoolean.Default == ret )
					ret = this.HiddenWhenGroupBy;

				UltraGridBand band = this.Band;
				if ( DefaultableBoolean.Default == ret && band.HasOverride )
					ret = band.Override.GroupByColumnsHidden;

				UltraGridLayout layout = band.Layout;
				if ( DefaultableBoolean.Default == ret && layout.HasOverride )
					ret = layout.Override.GroupByColumnsHidden;
						
				if ( DefaultableBoolean.Default == ret )
					ret = band.UseRowLayoutResolved 
						? DefaultableBoolean.False : DefaultableBoolean.True;

				return DefaultableBoolean.True == ret;
			}
		}

		#endregion // HiddenWhenGroupByResolved

		#region HiddenInternal
		
		internal bool HiddenInternal
		{
			get
			{
				return this.hidden;
			}
		}

		#endregion // HiddenInternal

		#endregion // Column Chooser Functionality

		// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
		// 
		#region NAS 2005 vol 3. - CellClickAction on Column

		#region CellClickAction
		/// <summary>
		/// Returns or sets a value that indicates what will occur when a cell is clicked.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>CellClickAction</b> property specifies what will occur when the user navigates through the grid by clicking on cells in the band or the grid controlled by the specified override. You can choose whether cells that are clicked will put the cell into edit mode or select the cell or its row. Depending on your application, you may want to enable the user to edit any cell just by clicking on it, or you may want to require a separate action to trigger cell editing, such as double-clicking or a keystroke combination. Similarly, you can choose whether cells should be individually selectable, or if selecting the row is a sufficient response to clicking on a cell.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_CellClickAction")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.CellClickAction CellClickAction
		{
			get
			{
				return this.cellClickAction;
			}

			set
			{
				if ( value != this.cellClickAction )
				{

                    // MRS NAS v8.3 - Unit Testing
                    //// test that the value is in range
                    ////
                    //if ( !Enum.IsDefined( typeof(CellClickAction), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_208") );
                    if (!Enum.IsDefined(typeof(CellClickAction), value))
                        throw new InvalidEnumArgumentException("CellClickAction", (int)value, typeof(CellClickAction));

					this.cellClickAction = value;
					this.NotifyPropChange( PropertyIds.CellClickAction, null );
				}
			}
		}

		#region ShouldSerializeCellClickAction
		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCellClickAction() 
		{
			return ( this.cellClickAction != CellClickAction.Default );
		}
		#endregion ShouldSerializeCellClickAction
 
		#region ResetCellClickAction
		/// <summary>
		/// Resets CellClickAction to its default value.
		/// </summary>
		public void ResetCellClickAction() 
		{
			this.CellClickAction = CellClickAction.Default;
		}
		#endregion ResetCellClickAction

		#endregion CellClickAction

		#region CellClickActionResolved
		internal CellClickAction CellClickActionResolved
		{
			get
			{
				if (this.cellClickAction != CellClickAction.Default)
					return this.cellClickAction;

				return this.band.CellClickActionResolved;
			}
		}
		#endregion CellClickActionResolved

	#endregion NAS 2005 vol 3. - CellClickAction on Column

		// SSP 7/29/05 - NAS 5.3 Tab Index
		// 
		#region NAS 5.3 Tab Index

		#region TabIndex

		/// <summary>
		/// Used for specifying the order the UltraGrid traverses the cells when tabbing. Default value is <b>-1</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// By default the UltraGrid determines the tab order of the cells based on their locations.
		/// You can use the <b>TabIndex</b> property to control the order in which the UltraGrid
		/// traverses cells of a row when tab key is used. The default value of the property is <b>-1</b> which 
		/// means the column will follow the default tab order. You can set the TabIndex of two or more columns
		/// to the same value in which case the tab order of those columns will follow the default tab 
		/// order. You can also specify the TabIndex only on some columns and leave the TabIndex to 
		/// the default value on other columns. In such a scenario the columns that have their 
		/// TabIndex set to non-default values take precedence over the columns that do not have their
		/// TabIndex set.
		/// </p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridColumn_P_TabIndex")]
		[ LocalizedCategory("LC_Behavior") ]
		public int TabIndex
		{
			get
			{
				return this.tabIndex;
			}
			set
			{
				if ( value != this.tabIndex )
				{
					this.tabIndex = value;

					if ( null != this.Band )
						this.Band.DirtyTabOrderedColumns( );

					this.NotifyPropChange( PropertyIds.TabIndex );
				}
			}
		}

		#endregion // TabIndex

		#region ShouldSerializeTabIndex

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeTabIndex( )
		{
			return this.tabIndex >= 0;
		}

		#endregion // ShouldSerializeTabIndex

		#region ResetTabIndex

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetTabIndex( )
		{
			this.TabIndex = -1;
		}

		#endregion // ResetTabIndex

		#endregion // NAS 5.3 Tab Index

		// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
		// 
		#region NAS 5.3 Invalid Value Behavior

		#region InvalidValueBehavior

		/// <summary>
		/// Specifies behavior when the user attempts to leave a cell after entering an invalid value.
		/// </summary>
		///	<remarks>
		///	<p class="body">
		///	<b>InvalidValueBehavior</b> property is used to specify what actions are taken
		///	by the UltraGrid when the user attempts to leave a cell after entering an invalid
		///	value. This property provides the default values for the properties on
		///	<see cref="CellDataErrorEventArgs"/> when it fires the <see cref="UltraGrid.CellDataError"/> 
		///	event. Therefore the same functionality can be achieved by hooking into that event
		///	and setting the properties on the event args. Also the event args
		///	exposes other properties that control other aspects of the behavior.
		///	</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.InvalidValueBehavior"/> 
		///	<seealso cref="UltraGrid.CellDataError"/> 
		///	<seealso cref="CellDataErrorEventArgs"/> 
		///	<seealso cref="UltraGrid.Error"/> 
		///	<seealso cref="UltraGridOverride.InvalidValueBehavior"/>
		///	</remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_InvalidValueBehavior")]
		[ LocalizedCategory("LC_Display") ]
		public InvalidValueBehavior InvalidValueBehavior
		{
			get
			{
				return this.invalidValueBehavior;
			}
			set
			{
				if ( value != this.invalidValueBehavior )
				{
					GridUtils.ValidateEnum( "InvalidValueBehavior", typeof( InvalidValueBehavior ), value );
					this.invalidValueBehavior = value;
					this.NotifyPropChange( PropertyIds.InvalidValueBehavior, null );
				}
			}
		}

		#endregion // InvalidValueBehavior

		#region InvalidValueBehaviorResolved
		
		internal InvalidValueBehavior InvalidValueBehaviorResolved
		{
			get
			{
				InvalidValueBehavior ret = this.InvalidValueBehavior;
				if ( InvalidValueBehavior.Default == ret )
				{
					if ( this.band.HasOverride )
						ret = this.band.Override.InvalidValueBehavior;

					if ( InvalidValueBehavior.Default == ret && this.Layout.HasOverride )
						ret = this.Layout.Override.InvalidValueBehavior;

					if ( InvalidValueBehavior.Default == ret )
						ret = InvalidValueBehavior.RetainValueAndFocus;
				}

                return ret;				
			}
		}

		#endregion // InvalidValueBehaviorResolved

		#region ShouldSerializeInvalidValueBehavior

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeInvalidValueBehavior( )
		{
			return InvalidValueBehavior.Default != this.invalidValueBehavior;
		}

		#endregion // ShouldSerializeInvalidValueBehavior

		#region ResetInvalidValueBehavior

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetInvalidValueBehavior( )
		{
			this.InvalidValueBehavior = InvalidValueBehavior.Default;
		}

		#endregion // ResetInvalidValueBehavior

		#endregion // NAS 5.3 Invalid Value Behavior

		// SSP 2/17/06 - NAS 6.1 Multi-cell Operations
		// 
		#region NAS 6.1 Multi-cell Operations

		#region IgnoreMultiCellOperation

		// SSP 2/17/06 BR08864
		// Added IgnoreMultiCellOperation property on the column.
		// 
		/// <summary>
		/// Specifies whether to ignore multi-cell operations on cells of this column.
		/// Default value of the property is Default which is resolved to False. If set to True,
		/// any multi cell operations that will end up modifying the cell values will be ignored 
		/// for the cells associated with this column.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>IgnoreMultiCellOperation</b> specifies whether to ignore multi-cell operations on 
		/// cells of this column. Default value of the property is Default which is resolved to False. 
		/// If set to True, any multi cell operations that will end up modifying the cell values will 
		/// be ignored for the cells associated with this column. This is useful for read-only or
		/// ID columns when you would typically not want to allow cutting/pasting on cells of
		/// such columns. <b>Note</b> that operations that do not modify the values, such as Copy
		/// operation, will still be allowed on the associated cells.
		/// </p>
		/// <p class="body">
		/// </p>
		/// <seealso cref="UltraGridOverride.AllowMultiCellOperations"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridColumn_P_IgnoreMultiCellOperation")]
		[ LocalizedCategory("LC_Behavior") ]
		public DefaultableBoolean IgnoreMultiCellOperation
		{
			get
			{
				return this.ignoreMultiCellOperation;
			}
			set
			{
				if ( value != this.ignoreMultiCellOperation )
				{

                    // MRS NAS v8.3 - Unit Testing
                    if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
                        throw new InvalidEnumArgumentException("IgnoreMultiCellOperation", (int)value, typeof(DefaultableBoolean));

					this.ignoreMultiCellOperation = value;

					this.NotifyPropChange( PropertyIds.IgnoreMultiCellOperation );
				}
			}
		}

		#endregion // IgnoreMultiCellOperation

		#region ResetIgnoreMultiCellOperation

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetIgnoreMultiCellOperation( )
		{
			this.IgnoreMultiCellOperation = DefaultableBoolean.Default;
		}

		#endregion // ResetIgnoreMultiCellOperation

		#region ShouldSerializeIgnoreMultiCellOperation

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeIgnoreMultiCellOperation( )
		{
			return DefaultableBoolean.Default != this.ignoreMultiCellOperation;
		}

		#endregion // ShouldSerializeIgnoreMultiCellOperation
        
		#region IgnoreMultiCellOperationResolved
		
		internal bool IgnoreMultiCellOperationResolved
		{
			get
			{
				return DefaultableBoolean.True == this.ignoreMultiCellOperation;
			}
		}

		#endregion // IgnoreMultiCellOperationResolved

		#endregion // NAS 6.1 Multi-cell Operations

		#region SpellChecker

		// SSP 4/5/06 - NAS 6.2 - Support for New SpellCheck functionality 
		// Added SpellChecker property.
		// 
		/// <summary>
		/// Gets/sets the component that will perform spell checking on this control.
		/// </summary>
		[ LocalizedDescription( "LDR_UltraGridColumn_P_SpellChecker" ) ]
		[ LocalizedCategory( "LC_Behavior" ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		public Infragistics.Win.UltraWinSpellChecker.IUltraSpellChecker SpellChecker
		{
			get 
			{ 
				return this.spellChecker;
			}
			set
			{ 
				if ( value != this.spellChecker )
				{
					this.spellChecker = value;

					//	Notify listeners of the property change
					this.NotifyPropChange( PropertyIds.SpellChecker );
				}
				
			}
		}

		#endregion // SpellChecker

		#region ResetSpellChecker

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSpellChecker( )
		{
			this.SpellChecker = null;
		}

		#endregion // ResetSpellChecker

		#region ShouldSerializeSpellChecker

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSpellChecker( )
		{
			return null != this.spellChecker;
		}

		#endregion // ShouldSerializeSpellChecker

        // MBS 11/15/06 - NAS 7.1 Conditional Formatting
        #region ValueBasedAppearance

        /// <summary>
        /// Gets/sets the <see cref="IValueAppearance"/> object that will be used to apply condition-based appearance resolution.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [TypeConverter(typeof(TypeConverter))]
        [Editor("Infragistics.Win.Design.Grid_Designer.ValueAppearanceUITypeEditor, " + AssemblyRef.Design, typeof(UITypeEditor))] 
        public IValueAppearance ValueBasedAppearance
        {
            get 
            {
                return this.valueBasedAppearance; 
            }
            set
            {
                if (this.valueBasedAppearance != value)
                {
                    // MBS 1/25/07                     
                    if (this.valueBasedAppearance != null)
                        this.valueBasedAppearance.PropertyChanged -= new EventHandler(this.OnValueBasedAppearancePropertyChanged);

                    this.valueBasedAppearance = value;

                    // MBS 1/25/07
                    if(this.valueBasedAppearance != null)
                        this.valueBasedAppearance.PropertyChanged += new EventHandler(this.OnValueBasedAppearancePropertyChanged);

					if(this.Layout != null)
					{
						// MBS 1/12/07 - BR19118
						this.Layout.BumpCellChildElementsCacheVersion();

						this.Layout.DirtyGridElement();
					}

                    // Notify listeners of the property change
                    this.NotifyPropChange( PropertyIds.ValueBasedAppearance );
                }
            }
        }

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeValueBasedAppearance()
        {
            return this.valueBasedAppearance != null;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetValueBasedAppearance()
        {
            this.ValueBasedAppearance = null;
        }

        #endregion //ValueBasedAppearance

        // MBS 12/21/06 - Conditional Formatting
        #region IFormulaProviderEx Implementation

        /// <summary>
        /// Returns a <see cref="IUltraCalcReference"/> based on the context, or null if none are found.
        /// </summary>
        /// <param name="context">The context from which to pull the reference.  In the case of the grid, generally is an
        /// <see cref="UltraGridCell"/> or <see cref="UltraGridRow"/>.</param>
        public IUltraCalcReference GetReferenceFromContext(object context)
        {
			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if (context is UltraGridCell)
			//{
			//    return ((UltraGridCell)context).CalcReference;
			//}
			//else if (context is UltraGridRow)
			//{
			//    return ((UltraGridRow)context).CalcReference;
			//}
			UltraGridCell cell = context as UltraGridCell;

			if ( cell != null )
				return cell.CalcReference;

			UltraGridRow row = context as UltraGridRow;

			if ( row != null )
				return row.CalcReference;

            return null;
        }

        /// <summary>
        /// Returns null.
        /// </summary>
        // MBS 7/27/07 - BR25195
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
        public ReferenceNode FormulaDialogReferenceNode
        {
            get { return null; }
        }
        #endregion //IFormulaProviderEx Implementation

        // MBS 1/25/07
        #region OnValueBasedAppearancePropertyChanged

        private void OnValueBasedAppearancePropertyChanged(object sender, EventArgs e)
        {
            if (this.Layout != null)
            {
                // MBS 1/12/07 - BR19118
                this.Layout.BumpCellChildElementsCacheVersion();

                this.Layout.DirtyGridElement();
            }
        }

        #endregion //OnValueBasedAppearancePropertyChanged

        // MRS 12/11/2007 - BR28989
        #region IsColumnBorderMerged
        internal bool IsColumnBorderMerged
        {
            get
            {
                UltraGridBand band = this.Band;
                if (band == null)
                    return false;

                UltraGridLayout layout = this.Layout;
                if (this.Layout == null)
                    return false;

                if (band.UseRowLayoutResolved == false &&
                    layout.CanMergeAdjacentBorders(band.BorderStyleCellResolved))
                {
                    ColumnHeader header = this.Header;
                    if (header == null)
                        return false;

                    bool isFirstColumn = header.FirstItem;
                    bool isLastColumn = header.LastItem;
                    bool rowSelectors = band.RowSelectorsResolved == DefaultableBoolean.True;
                    bool rowBordersMergable = layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved);

                    if (isFirstColumn)
                    {
                        if (rowSelectors && rowBordersMergable)
                            return true;
                    }
                    else if (isLastColumn)
                    {
                        if (rowBordersMergable)
                            return true;
                    }
                    else
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        #endregion //IsColumnBorderMerged

        // MRS 1/16/2008 - BR29722
        #region HiddenResolved
		/// <summary>
        /// Unlike the Hidden property, this property takes into account whether the column is in a hidden group, or if the band is displaying groups and the column is not in any group. 
        /// </summary>
		// MD 1/19/09 - Groups in RowLayout
		// Made public so this could be used in the design assembly.
        //internal bool HiddenResolved
		[Browsable( false )]
		[EditorBrowsable( EditorBrowsableState.Advanced )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public bool HiddenResolved
        {
            get
            {
				// MD 1/19/09 - Groups in RowLayout
				// If the band is null, the column is technically hidden.
				if ( this.band == null )
					return true;

                if (this.band.GroupsDisplayed)
                {
					// MD 1/19/09 - Groups in RowLayout
					// This logic is slightly different with different row layout styles.
					//if (this.Group == null ||
					//    this.Group.Hidden)
					//{
					//    return true;
					//}
					UltraGridGroup group = this.GroupResolved;

					if ( group == null )
					{
						if ( this.band.RowLayoutStyle == RowLayoutStyle.None )
							return true;
					}
					else if ( group.HiddenResolved )
					{
						return true;
					}
                }

                return this.Hidden;
            }
        }
        #endregion //HiddenResolved

        // MRS 3/25/2008 - BR31396
        #region DirtyHasColSpan
        private void DirtyHasColSpan()
        {
            UltraGridBand band = this.Band;
            if (band != null)
            {
                UltraGridLayout layout = band.Layout;
                if (layout != null)
                    layout.needToVerifyColSpan = true;
            }
        }
        #endregion //DirtyHasColSpan

        // CDS 04/14/08 NA 8.2 ImeMode
        #region ImeMode

        /// <summary>
        /// Specifies the IME mode for the column.
        /// </summary>
        /// <remarks>
        /// <p class="body">
        /// When set to ImeMode.Inherit, the ImeMode of the owning UltraGrid will be used.
        /// </p>
        /// </remarks>
        [LocalizedDescription("LD_UltraGridColumn_P_ImeMode")]
        //[DefaultValue(System.Windows.Forms.ImeMode.Inherit)]
        [LocalizedCategory("LC_Behavior")]
        public System.Windows.Forms.ImeMode ImeMode
        {
            get { return this.imeMode; }
            set
            {
                if (value != this.imeMode)
                {
                    GridUtils.ValidateEnum(typeof(System.Windows.Forms.ImeMode), value);
                    this.imeMode = value;
                    this.NotifyPropChange(PropertyIds.ImeMode);
                }
            }
        }

        #endregion ImeMode

        // CDS 04/14/08 NA 8.2 ImeMode
		#region ShouldSerializeImeMode

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeImeMode( )
		{
			return System.Windows.Forms.ImeMode.Inherit != this.imeMode;
		}

		#endregion ShouldSerializeImeMode

        #region ResetImeMode

        /// <summary>
        /// Reset property to its default value
        /// </summary>
        public void ResetImeMode()
        {
            this.ImeMode = System.Windows.Forms.ImeMode.Inherit;
        }

        #endregion ResetImeMode

		// MD 9/23/08 - TFS6601
		#region IsSortable

		internal bool IsSortable
		{
			get
			{
				if ( this.Band != null && this.Band.HeaderClickActionResolved == HeaderClickAction.Select )
					return false;

				if ( this.Header.SortIndicator == SortIndicator.Disabled )
					return false;

				return true;
			}
		} 

		#endregion IsSortable

		#region ReserveSortIndicatorSpaceWhenAutoSizing

		/// <summary>
		/// Gets or sets the value indicating when to reserve space for the sort indicator during an auto-size operation.
		/// </summary>
		/// <seealso cref="UltraGridOverride.ReserveSortIndicatorSpaceWhenAutoSizing"/>
		public ReserveSortIndicatorSpaceWhenAutoSizing ReserveSortIndicatorSpaceWhenAutoSizing
		{
			get { return this.reserveSortIndicatorSpaceWhenAutoSizing; }
			set
			{
				if ( this.ReserveSortIndicatorSpaceWhenAutoSizing == value )
					return;

				GridUtils.ValidateEnum( typeof( ReserveSortIndicatorSpaceWhenAutoSizing ), value );
				this.reserveSortIndicatorSpaceWhenAutoSizing = value;
				this.NotifyPropChange( PropertyIds.ReserveSortIndicatorSpaceWhenAutoSizing );
			}
		}

		/// <summary>
		/// Returns true if <see cref="ReserveSortIndicatorSpaceWhenAutoSizing"/> is not set to its default value.
		/// </summary>
		/// <returns>Returns true if ReserveSortIndicatorSpaceWhenAutoSizing is not set to its default value.</returns>
		protected bool ShouldSerializeReserveSortIndicatorSpaceWhenAutoSizing()
		{
			return this.reserveSortIndicatorSpaceWhenAutoSizing != ReserveSortIndicatorSpaceWhenAutoSizing.Default;
		}

		/// <summary>
		/// Resets <see cref="ReserveSortIndicatorSpaceWhenAutoSizing"/> to its default value.
		/// </summary>
		[EditorBrowsable( EditorBrowsableState.Advanced )]
		public void ResetReserveSortIndicatorSpaceWhenAutoSizing()
		{
			this.ReserveSortIndicatorSpaceWhenAutoSizing = ReserveSortIndicatorSpaceWhenAutoSizing.Default;
		} 

		#endregion ReserveSortIndicatorSpaceWhenAutoSizing

		#region ReserveSortIndicatorSpaceWhenAutoSizingResolved

		/// <summary>
		/// Gets the resolved value of <see cref="ReserveSortIndicatorSpaceWhenAutoSizing"/>.
		/// </summary>
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public ReserveSortIndicatorSpaceWhenAutoSizing ReserveSortIndicatorSpaceWhenAutoSizingResolved
		{
			get
			{
				if ( this.ShouldSerializeReserveSortIndicatorSpaceWhenAutoSizing() )
					return this.ReserveSortIndicatorSpaceWhenAutoSizing;

				if ( this.Band != null &&
					this.Band.HasOverride &&
					this.Band.Override.ShouldSerializeReserveSortIndicatorSpaceWhenAutoSizing() )
					return this.Band.Override.ReserveSortIndicatorSpaceWhenAutoSizing;

				if ( this.Band != null &&
					this.Band.Layout != null &&
					this.Band.Layout.HasOverride &&
					this.Band.Layout.Override.ShouldSerializeReserveSortIndicatorSpaceWhenAutoSizing() )
					return this.Band.Layout.Override.ReserveSortIndicatorSpaceWhenAutoSizing;

				return ReserveSortIndicatorSpaceWhenAutoSizing.WhenVisible;
			}
		} 

		#endregion ReserveSortIndicatorSpaceWhenAutoSizingResolved

        #region NAS v9.1 Header CheckBox

        // CDS NAS v9.1 Header CheckBox
        #region HeaderCheckBoxVisibilityResolved

        internal HeaderCheckBoxVisibility HeaderCheckBoxVisibilityResolved
        {
            get
            {
                return this.Header.CheckBoxVisibilityResolved;
            }
        }

        #endregion HeaderCheckBoxVisibilityResolved

        // CDS NAS v9.1 Header CheckBox
        #region HeaderCheckBoxAlignmentResolved

        internal HeaderCheckBoxAlignment HeaderCheckBoxAlignmentResolved
        {
            get
            {
                return this.Header.CheckBoxAlignmentResolved;
            }
        }

        #endregion HeaderCheckBoxAlignmentResolved

        // CDS NAS v9.1 Header CheckBox
        #region HeaderCheckBoxSynchronizationResolved

        internal HeaderCheckBoxSynchronization HeaderCheckBoxSynchronizationResolved
        {
            get
            {
                return this.Header.CheckBoxSynchronizationResolved;
            }
        }

        #endregion HeaderCheckBoxSynchronizationResolved

        // CDS NAS v9.1 Header CheckBox
        #region GetHeaderCheckedState

        /// <summary>
        /// Returns the current CheckState for the Header
        /// </summary>
        /// <param name="rows">The RowsCollection used to find the associated Header.</param>
        /// <returns>Returns CheckState based on the Header CheckBox's Checked property</returns>
        public CheckState GetHeaderCheckedState(RowsCollection rows)
        {
            switch (this.HeaderCheckBoxSynchronizationResolved)
            {
                case HeaderCheckBoxSynchronization.Band:
                    // 1/14/08 CDS TFS12429 Added a null check.
                    //return this.Band.GetHeaderCheckState(this);
                    return (this.Band != null) ? this.Band.GetHeaderCheckState(this): CheckState.Unchecked;
                case HeaderCheckBoxSynchronization.RowsCollection:
                case HeaderCheckBoxSynchronization.None:
                default:
                    if (null == rows)
                        throw new NotSupportedException(Shared.SR.GetString("LE_UltraGridColumn_GetHeaderCheckStateWithNullParameter"));
                    return rows.GetHeaderCheckState(this);
            }
        }

        #endregion GetHeaderCheckedState

        // CDS 02/04/09 TFS 12493 - NAS v9.1 Header CheckBox - Added an overload so the publicly visible method accept a boolean instead of CheckState
        #region SetHeaderCheckedState

        /// <summary>
        /// Sets the CheckState of the Header of the provided RowsCollection.
        /// </summary>
        /// <param name="rows">RowsCollection containing the header to be changed</param>
        /// <param name="check">Indicates if the header checkbox should be checked</param>
        public void SetHeaderCheckedState(RowsCollection rows, bool check)
        {
            this.SetHeaderCheckedState(rows, ((check)? CheckState.Checked: CheckState.Unchecked));
        }

        // CDS 02/04/09 TFS 12493 - NAS v9.1 Header CheckBox - Added an overload so the publicly visible method accept a boolean instead of CheckState       
        //public void SetHeaderCheckedState(RowsCollection rows, CheckState state)
        internal void SetHeaderCheckedState(RowsCollection rows, CheckState state)
        {
            this.SetHeaderCheckedStateInternal(rows, state, true);
        }

        #endregion SetHeaderCheckedState

        // CDS NAS v9.1 Header CheckBox
        #region SetHeaderCheckedStateInternal

        internal void SetHeaderCheckedStateInternal(RowsCollection rows, CheckState state, bool isCancelable)
        {
            if (state == CheckState.Indeterminate)
                throw new NotSupportedException("CheckState.Indeterminate is not supported for the SetHeaderCheckedState() method.");

            switch (this.HeaderCheckBoxSynchronizationResolved)
            {

                case HeaderCheckBoxSynchronization.Band:
                    // 1/14/08 CDS TFS12429 Added a null check.
                    if (this.Band != null)
                    {
                        // CDS 2/26/09 Allow it to synchronize
                        //this.Band.SetCheckStateCache(this, state, isCancelable);
                        this.Band.SetCheckStateCache(this, state, isCancelable, true);
                        this.DirtyHeaderUIElements(null);
                    }
                    break;
                case HeaderCheckBoxSynchronization.RowsCollection:
                case HeaderCheckBoxSynchronization.None:
                default:
                    if (rows != null)
                    {
                        // CDS 2/26/09 Allow it to synchronize
                        //rows.SetCheckStateCache(this, state, isCancelable);
                        rows.SetCheckStateCache(this, state, isCancelable, true);
                        this.DirtyHeaderUIElements(rows);
                    }
                    break;
            }
        }

        #endregion SetHeaderCheckedStateInternal

        // CDS NAS v9.1 Header CheckBox
        #region SetCellValuesBasedOnCheckState

        internal void SetCellValuesBasedOnCheckState(RowsCollection rows, CheckState state)
        {
            // 2/24/09 CDS TFS12452 Use CheckState instead of Boolean, 
            //object cellVal = (state == CheckState.Checked) ? true : false;
            object cellVal = state;
            IEnumerator enumerator = null;

            if (rows == null)
                enumerator = this.Band.GetRowEnumerator(GridRowType.DataRow).GetEnumerator();
            else
                enumerator = rows.GetEnumerator();
        
            //check the value for each row
            while (enumerator.MoveNext())
            {
                UltraGridRow row = (UltraGridRow)enumerator.Current;

                EmbeddableEditorBase editor = row.GetEditorResolved(this);
                if (editor != null)
                {
                    GridEmbeddableEditorOwnerInfoBase cellEditorOwnerInfo = row.GetEditorOwnerInfo(this);
                    bool isValid = false;
                    object filteredVal = editor.GetDataFilteredDestinationValue(cellVal,
                        ConversionDirection.EditorToOwner, out isValid, cellEditorOwnerInfo, this);
                    if (isValid)
                        row.SetCellValueConditionally(this, filteredVal);
                }
                else
                    row.SetCellValueConditionally(this, cellVal);
            }
        }

        #endregion SetCellValuesBasedOnCheckState

        // 2/24/09 CDS TFS12452 - No longer used as we now check to see if the Editor is a CheckEditor
		#region Removed

		//// CDS NAS v9.1 Header CheckBox
		//#region IsColumnDataTypeRepresentableByCheckBox
		//
		///// <summary>
		///// Determines via the DataType if the value of cells within the column are represented using a checkbox 
		///// </summary>
		//internal bool IsColumnDataTypeRepresentableByCheckBox
		//{
		//    get
		//    {
		//        return (this.dataType == typeof(bool) ||
		//            this.dataType == typeof(Nullable<bool>) ||
		//            this.dataType == typeof(DefaultableBoolean));
		//    }
		//}
		//
		//#endregion IsColumnDataTypeRepresentableByCheckBox 

		#endregion Removed

        // CDS NAS v9.1 Header CheckBox
        #region GetHeaderCheckedStateBasedOnCellValues

        internal CheckState GetHeaderCheckedStateBasedOnCellValues(RowsCollection rows)
        {
            // Column is not boolean so just get out
            // CDS 2/24/09 TFS12452 - Check if the checkbox is visible, not the datatype
            //if (!this.IsColumnDataTypeRepresentableByCheckBox)
            if (!this.IsHeaderCheckBoxVisible)
                return CheckState.Indeterminate;

            bool allFalse = true;
            bool allTrue = true;

            IEnumerator enumerator = null;

            if (rows == null)
                enumerator = this.Band.GetRowEnumerator(GridRowType.DataRow).GetEnumerator();
            else
                enumerator = rows.GetEnumerator();

            //enumerate through each row
            while (enumerator.MoveNext())
            {
                UltraGridRow row = (UltraGridRow)enumerator.Current;
                Nullable<bool> boolValue = ConvertObjectToBoolean(row.GetCurrentDataFilteredCellValue(this));

                if (boolValue == null ||
                    !boolValue.HasValue)
                    return CheckState.Indeterminate;

                if (boolValue.Value)
                    allFalse = false;
                else
                    allTrue = false;

                // already found a false and a true, so return Indeterminate without continuing the processing
                if (!allTrue && !allFalse)
                    return CheckState.Indeterminate;
            }

            if (allTrue && !allFalse)
                return CheckState.Checked;
            else if (allFalse && !allTrue)
                return CheckState.Unchecked;

            //either allTrue and allFalse were both True or both False.
            return CheckState.Indeterminate;
        }

        #endregion GetHeaderCheckedStateBasedOnCellValues

        // CDS NAS v9.1 Header CheckBox
        #region ContainsVisibleHeaderCheckBoxOnLeftOrRight

        internal bool ContainsVisibleHeaderCheckBoxOnLeftOrRight
        {
            get
            {
                return this.Header.ContainsVisibleHeaderCheckBoxOnLeftOrRight;
            }
        }

        #endregion ContainsVisibleHeaderCheckBoxOnLeftOrRight

        // CDS NAS v9.1 Header CheckBox
        #region IsHeaderCHeckBoxVisible

        internal bool IsHeaderCheckBoxVisible
        {
            get
            {
                return this.Header.IsHeaderCheckBoxVisible;
            }
        }

        #endregion IsHeaderCHeckBoxVisible

        // MBS 7/2/09
        // Commented this out since it's not being used anywhere and there's a method on the header
        // to do the exact same thign
        //
        // CDS NAS v9.1 Header CheckBox
        #region GetHeaderUIElements - Commented OUt

//#if DEBUG
//        /// <summary>
//        /// Retrieve all the HeaderUIElements for the provided RowsCollection
//        /// </summary>
//        /// <param name="rows">RowsCollection context used to find particular HeaderUIElements. If null, all HeaderUIElements for the Column are returned.</param>
//        /// <returns>Typed List of HeaderUIElements for the column</returns>
//#endif
//        internal List<HeaderUIElement> GetHeaderUIElements(RowsCollection rows)
//        {
//            return this.Header.GetHeaderUIElements(rows);
//        }

        #endregion GetHeaderUIElements

        // CDS NAS v9.1 Header CheckBox
        #region DirtyHeaderUIElements

        internal void DirtyHeaderUIElements(RowsCollection rows)
        {
            this.Header.DirtyHeaderUIElements(rows);
        }

        #endregion DirtyHeaderUIElements

        // CDS NAS v9.1 Header CheckBox
        #region ConvertObjectToBoolean

        private Nullable<bool> ConvertObjectToBoolean(object objValue)
        {
            //automatically return null for DBNull or null
            if (objValue is DBNull ||
                null == objValue)
                return null;

            // if boolean or nullable boolean, simply return the objValue;
            if (objValue is bool ||
                objValue is Nullable<bool>)
                return (Nullable<bool>)objValue;

            // CDS 2/24/09 TFS12452 Added CheckState
            // if CheckState, assume Checked = true, Unchecked = false, Indeterminate = null
            if (objValue is CheckState)
            {
                switch ((CheckState)objValue)
                {
                    case CheckState.Checked:
                        return true;
                    case CheckState.Unchecked:
                        return false;
                    case CheckState.Indeterminate:
                        return null;
                }
            }

            //if DefaultableBoolean, then convert to true/false
			// MD 2/24/09
			// Found while fixing TFS12452
			// We should not assume that int values should be DefaultableBoolean.
			//if ((objValue is Int32 ||
			//    objValue is DefaultableBoolean) &&
			//    Enum.IsDefined(typeof(DefaultableBoolean), objValue))
			if ( objValue is DefaultableBoolean )
            {
                DefaultableBoolean defaultableValue = (DefaultableBoolean)objValue;
                if (defaultableValue == DefaultableBoolean.True)
                    return true;
                else if (defaultableValue == DefaultableBoolean.False)
                    return false;
            }
            
            //either the DataType is not convertable, or its set to DefaultableBoolean.Default.
            return null;
        }

        #endregion ConvertObjectToBoolean

        // CDS NAS v9.1 Header CheckBox
        // CDS 02/02/09 TFS12512 Moved from the Cell to the Column.
        #region UpdateHeaderCheckBox

        internal void UpdateHeaderCheckBox(RowsCollection rows)
        {
            // CDS 2/24/09 TFS12452 Check for checkbox being visible, not the column's DataType
            //if (this.IsColumnDataTypeRepresentableByCheckBox)
            if (this.IsHeaderCheckBoxVisible)
            {
                switch (this.HeaderCheckBoxSynchronizationResolved)
                {
                    case HeaderCheckBoxSynchronization.Band:
                        this.Band.DirtyCheckStateCacheValidEntry(this);
                        this.DirtyHeaderUIElements(null);
                        break;
                    case HeaderCheckBoxSynchronization.RowsCollection:
                        rows.DirtyCheckStateCacheValidEntry(this);
                        this.DirtyHeaderUIElements(rows);
                        break;
                    case HeaderCheckBoxSynchronization.None:
                        return;
                    default:
                        Debug.Fail("Invalid HeaderCheckBoxSynchronization returned from the Resolved property.");
                        break;
                }
            }
        }

        #endregion UpdateHeaderCheckBox

        #endregion NAS v9.1 Header CheckBox

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout

		#region LevelResolved

		internal int LevelResolved
		{
			get
			{
				if ( this.Band != null && this.Band.RowLayoutStyle == RowLayoutStyle.GroupLayout )
					return 0;

				return this.Level;
			}
		} 

		#endregion LevelResolved

		#region IsSelectable

		internal bool IsSelectable
		{
			get
			{
				return
					this.HiddenResolved == false &&
					this.Header.Enabled;
			}
		}

		#endregion IsSelectable
        

        #region IProvideRowLayoutColumnInfo Members

        #region Band
        UltraGridBand IProvideRowLayoutColumnInfo.Band
        {
            get { return this.Band; }
        }
        #endregion //Band

        #region ContentLayoutItem
        ILayoutItem IProvideRowLayoutColumnInfo.ContentLayoutItem
        {
            get { return this; }
        }
        #endregion //ContentLayoutItem

        #region Header
        HeaderBase IProvideRowLayoutColumnInfo.Header
        {
            get { return this.Header; }
        }
        #endregion //Header

        #region HeaderConstraint
        IGridBagConstraint IProvideRowLayoutColumnInfo.HeaderConstraint
        {
            get { return this; }
        }
        #endregion //HeaderConstraint

        #region HeaderLayoutItem
        ILayoutItem IProvideRowLayoutColumnInfo.HeaderLayoutItem
        {
            get { return this.Header; }
        }
        #endregion //HeaderLayoutItem        

        #region MinimumHeaderSize
        Size IProvideRowLayoutColumnInfo.MinimumHeaderSize
        {
            get { return this.MinimumSize; }
        }
        #endregion //MinimumHeaderSize

        #region MinimumSize
        Size IProvideRowLayoutColumnInfo.MinimumSize
        {
            get { return this.MinimumSize; }
        }
        #endregion //MinimumSize

        #region PreferredHeaderSize
        Size IProvideRowLayoutColumnInfo.PreferredHeaderSize
        {
            get { return this.CardLabelSize; }
        }
        #endregion //PreferredHeaderSize

        #region PreferredSize
        Size IProvideRowLayoutColumnInfo.PreferredSize
        {
            get { return this.PreferredSize; }
        }
        #endregion //PreferredSize

        #region ResizeLayoutItem
        ILayoutItem IProvideRowLayoutColumnInfo.ResizeLayoutItem
        {
            get { return this; }
        }
        #endregion //ResizeLayoutItem

        #region RowLayoutColumnInfo
        RowLayoutColumnInfo IProvideRowLayoutColumnInfo.RowLayoutColumnInfo
        {
            get { return this.RowLayoutColumnInfo;  }
        }
        #endregion //RowLayoutColumnInfo

        #endregion // IProvideRowLayoutColumnInfo Members

        #region ILayoutChildItem Members

		// MD 2/17/09 - TFS14116
		// With the original fix for TFS13834, columns cannot be moved to new groups by default at design-time. The has been changed 
		// to allow for different behavior at run-time and design-time.
		//// MRS 2/13/2009 - TFS13834
		//bool ILayoutChildItem.AllowParentGroupChange
		//{
		//    get
		//    {
		//        switch (this.Band.AllowColMovingResolved)
		//        {
		//            case AllowColMoving.WithinBand:
		//                return true;
		//            case AllowColMoving.NotAllowed:
		//            case AllowColMoving.WithinGroup:
		//                return false;
		//            case AllowColMoving.Default:
		//            default:
		//                Debug.Fail("Unknown or invalid AllowColMovingResolved");
		//                return false;
		//        }                
		//    }
		//}

        /// <summary>
        /// Returns the group associated with this LayoutItem, or null, if the layoutitem does not represent a group. 
        /// </summary>
        ILayoutGroup ILayoutChildItem.AssociatedGroup
        {
            get { return null; }
        }

        ILayoutGroup ILayoutChildItem.ParentGroup
        {
            get 
            {
                return UltraGridGroup.GetLayoutGroup(this.Band, this);
            }
        }

        /// <summary>
        /// Returns the group into which an item will be dropped relative to this element.
        /// </summary>
        /// <param name="dropLocation"></param>
        /// <returns></returns>
        /// <remarks>
        /// This methods takes a DropLocation and determines what group an item dropped at that location 
        /// should be dropped into. If the item is a content, then the group is the parent group
        /// of that content. If the item is a group header, then the item may be dropped inside the group
        /// or into it's parent, depending on the DropLocation relative to the position of the header. 
        /// For example, if the group header is on top, dropping an item on the bottom of the header 
        /// should place that item inside the group while dropping the item to the left, right, or top
        /// of the header should place the item as a sibling of the group. 
        /// </remarks>
        ILayoutGroup ILayoutChildItem.GetDropGroup(GridBagLayoutDragStrategy.DropLocation dropLocation)
        {
            return ((ILayoutChildItem)this).ParentGroup;
        }

        #endregion // ILayoutChildItem Members

        #endregion // NAS 9.1 - Groups in RowLayout	

        // MRS 8/25/2009 - TFS20507
        // Re-factored from IsNullable so we get the correct Null value for cells that support
        // null, but not DBNull. 
        //
        #region GetNullValue
        internal object GetNullValue(out string message, ref bool canAcceptNull)
        {
            message = String.Empty;

            switch (this.Nullable)
            {                
                case Nullable.Disallow:
                    {
                        message = SR.GetString("DataErrorCellUpdateEmptyValueNotAllowed", this.Header.Caption);
                        canAcceptNull = false;
                        return null;
                    }
                case Nullable.EmptyString:
                    {
                        canAcceptNull = true;
                        return String.Empty;
                    }
                case Nullable.Nothing:
                    {
                        canAcceptNull = true;
                        return null;
                    }
                case Nullable.Null:
                    {
                        canAcceptNull = true;
                        return DBNull.Value;
                    }

                case Nullable.Automatic:
                default:
                    {
                        Debug.Assert(this.Nullable == Nullable.Automatic, "Unknown Nullable setting");

                        // Check if the column is an UltraDataColumn from UltraDataSource.
                        //
                        Infragistics.Win.UltraWinDataSource.IUltraDataColumn ultraDataColumn = this.PropertyDescriptor as Infragistics.Win.UltraWinDataSource.IUltraDataColumn;
                        if (ultraDataColumn != null)
                        {
                            canAcceptNull = ultraDataColumn.AllowDBNull;
                            return DBNull.Value;
                        }

                        // Check for a DataColumn
                        System.Data.DataColumn dataColumn = this.GetDataColumn();

                        if (null != dataColumn)
                        {
                            canAcceptNull = dataColumn.AllowDBNull;
                            return DBNull.Value;
                        }

                        // If it's a nullable type, it can accept Null. 
                        if (Infragistics.Win.Utilities.IsNullableType(this.DataType))
                        {
                            canAcceptNull = true;
                            return null;
                        }

                        // Default to true.
                        canAcceptNull = true;
                        return DBNull.Value;
                    }
            }
        }
        #endregion // GetNullValue

        // MRS - NAS 9.2 - Control Container Editor
        #region NAS 9.2 - Control Container Editor

        #region EditorComponent

        /// <summary>
        /// Component that implements IProvidesEmbeddableEditor. Attempt to set a component that
        /// does not implement IProvidesEmbeddableEditor interface will cause an exception.
        /// </summary>
        [LocalizedDescription("LD_UltraGridColumnTypeConverter_P_EditorComponent")]
        [TypeConverter(typeof(Infragistics.Win.EmbeddableEditorBase.EmbeddableEditorComponentConverter))]
        [InfragisticsFeature(
            Version = FeatureInfo.Version_9_2,
            FeatureName = FeatureInfo.FeatureName_ControlContainerEditor)]
        public Component EditorComponent
        {
            get
            {
                if (null == this.editorComponent && this.editorComponentName != null)
                {                    
                    // JAS 3/17/05 BR02741 - If the component exists in the UltraGrid Designer, then
                    // we will never be able to find one of the editor components, so just return null.
                    //
                    if (this.IsInDesigner)
                        return null;

                    ContainerControl rootCtrl = GridUtils.GetRootControlForSearching(null != this.Layout ? this.Layout.Grid : null) as ContainerControl;
                    Component component = Utilities.FindContainedComponentByName(rootCtrl, this.editorComponentName);

                    Debug.Assert(null == component ||
                        (component is IProvidesEmbeddableEditor &&
                        this.IsEditorAppropriate(((IProvidesEmbeddableEditor)component).Editor)),
                                    "Returned component is not an IProvidesEmbeddableEditor !");

                    if (component is IProvidesEmbeddableEditor &&
                        this.IsEditorAppropriate(((IProvidesEmbeddableEditor)component).Editor))
                        // SSP 1/28/05
                        // We need to hook into the editor component's Disposed so we can reset the 
                        // EditorComponent property when the editor component gets disposed.
                        //
                        //this.editorComponent = component;
                        this.InternalSetEditorComponent(component);

                    // Set it back to null so that we don't attempt to find the component on
                    // the form every time this property is accessed (in case we failed).
                    //
                    this.editorComponentName = null;
                }

                return this.editorComponent;
            }
            set
            {
                if (this.editorComponent != value)
                {
                    if (null != value && !(value is IProvidesEmbeddableEditor))
                        throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_316"), Shared.SR.GetString("LE_ArgumentException_317"));

                    // SSP 7/23/03 - Don't access the editor when we are in the midst of initializing because
                    // accessing the Editor property could cause premature cloning of the internal editor
                    // which means that any properties set on the internal editor after the cloning won't get
                    // copied over to the editor.
                    // Enclosed the code in the if block.
                    //
                    if (null != this.Layout && null != this.Layout.Grid && !this.Layout.Grid.Initializing)
                    {
                        IProvidesEmbeddableEditor editorProvider = value as IProvidesEmbeddableEditor;

                        if (null != editorProvider)
                        {
                            EmbeddableEditorBase editor = editorProvider.Editor;

                            if (!this.IsEditorAppropriate(editor))
                                throw new ArgumentException(SR.GetString("LER_Exception_338", editorProvider.GetType().Name), SR.GetString("LER_ArguementException_317"));
                        }
                    }

                    // SSP 1/28/05
                    // We need to hook into the editor component's Disposed so we can reset the 
                    // EditorComponent property when the editor component gets disposed.
                    //
                    // ----------------------------------------------------------------------
                    
                    this.InternalSetEditorComponent(value);
                    // ----------------------------------------------------------------------
                }
            }
        }

        #region ShouldSerializeEditorComponent

        /// <summary>
        /// Returns true if property needs to be serialized
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeEditorComponent()
        {
            return null != this.editorComponent ||
                // MBS 6/19/09 - TFS18459
                !String.IsNullOrEmpty(this.editorComponentName);
        }

        #endregion // ShouldSerializeEditorComponent

        #region ResetEditorComponent

        /// <summary>
        /// Returns true if property needs to be serialized
        /// </summary>
        /// <returns></returns>
        protected void ResetEditorComponent()
        {
            // SSP 1/28/05
            // We need to hook into the editor component's Disposed so we can reset the 
            // EditorComponent property when the editor component gets disposed.
            //
            //this.editorComponent = null;
            this.InternalSetEditorComponent(null);
        }

        #endregion // ResetEditorComponent

        #endregion // EditorComponent

        #region EditorComponentResolved
        internal Component EditorComponentResolved
        {
            get
            {
                if (this.EditorComponent != null)
                    return this.EditorComponent;

#pragma warning disable 0618
                return this.EditorControl;
#pragma warning restore 0618                
            }
        }
        #endregion //EditorComponentResolved

        #region InternalSetEditorComponent

        // SSP 1/28/05
        // We need to hook into the editor component's Disposed so we can reset the 
        // EditorComponent property when the editor component gets disposed.
        //
        private void InternalSetEditorComponent(Component component)
        {
            if (null != this.editorComponent)
                this.editorComponent.Disposed -= new System.EventHandler(this.OnEditorComponentDisposed);

            this.editorComponent = component;

            if (null != this.editorComponent)
                this.editorComponent.Disposed += new System.EventHandler(this.OnEditorComponentDisposed);

            this.DirtyDefaultEditor();
        }

        #endregion // InternalSetEditorComponent

        #region OnEditorComponentDisposed

        // SSP 1/28/05
        // We need to hook into the editor component's Disposed so we can reset the 
        // EditorComponent property when the editor component gets disposed.
        //
        private void OnEditorComponentDisposed(object sender, System.EventArgs e)
        {
            this.ResetEditorComponent();
        }

        #endregion // OnEditorComponentDisposed               

        #endregion //NAS 9.2 - Control Container Editor

        // MBS 5/8/09 - NA9.2 GroupByRowConnector Appearance
        #region GroupByRowConnectorAppearance

        /// <summary>
        /// Determines the appearance of the area that exists to the left of the child rows of a GroupByRow.
        /// </summary>
        [LocalizedDescription("LD_UltraGridOverride_P_GroupByRowConnectorAppearance")]
        [LocalizedCategory("LC_Appearance")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Infragistics.Win.AppearanceBase GroupByRowConnectorAppearance
        {
            get
            {
                // if we don't already have an appearance object then create it now
                //
                if (null == this.groupByRowConnectorAppearanceHolder)
                {
                    this.groupByRowConnectorAppearanceHolder = new Infragistics.Win.AppearanceHolder();

                    // hook up the prop change notifications
                    //
                    this.groupByRowConnectorAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }

                // Initialize the collection
                //
                if (this.Layout != null)
                    this.groupByRowConnectorAppearanceHolder.Collection = this.Layout.Appearances;

                return this.groupByRowConnectorAppearanceHolder.Appearance;
            }
            set
            {
                if (this.groupByRowConnectorAppearanceHolder == null ||
                    !this.groupByRowConnectorAppearanceHolder.HasAppearance ||
                    value != this.groupByRowConnectorAppearanceHolder.Appearance)
                {
                    if (null == this.groupByRowConnectorAppearanceHolder)
                    {
                        this.groupByRowConnectorAppearanceHolder = new Infragistics.Win.AppearanceHolder();

                        // hook up the new prop change notifications
                        //
                        this.groupByRowConnectorAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                    }

                    if (this.Layout != null)
                        this.groupByRowConnectorAppearanceHolder.Collection = this.Layout.Appearances;

                    this.groupByRowConnectorAppearanceHolder.Appearance = value;

                    // notify listeners
                    //
                    this.NotifyPropChange(PropertyIds.GroupByRowConnectorAppearance);
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal bool HasGroupByRowConnectorAppearance
        {
            get
            {
                return null != this.groupByRowConnectorAppearanceHolder
                    && this.groupByRowConnectorAppearanceHolder.HasAppearance;
            }

        }

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeGroupByRowConnectorAppearance()
        {
            return this.HasGroupByRowConnectorAppearance
                && this.groupByRowConnectorAppearanceHolder.ShouldSerialize();
        }

        /// <summary>
        /// Resets the <see cref="GroupByRowConnectorAppearance"/> property to its default value.
        /// </summary>
        public void ResetGroupByRowConnectorAppearance()
        {
            if (this.HasGroupByRowConnectorAppearance)
            {
                this.groupByRowConnectorAppearanceHolder.Reset();

                // Notify listeners that the appearance has changed. 
                this.NotifyPropChange(PropertyIds.GroupByRowConnectorAppearance);
            }
        }
        #endregion 
        //
        #region GroupByRowAppearance

        /// <summary>
        /// Gets or sets the default appearance of all the GroupByRows associated with this column.
        /// </summary>
        [LocalizedDescription("LD_UltraGridColumn_P_GroupByRowAppearance")]
        [LocalizedCategory("LC_Appearance")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Infragistics.Win.AppearanceBase GroupByRowAppearance
        {
            get
            {
                // if we don't already have an appearance object then create it now
                //
                if (null == this.groupByRowAppearanceHolder)
                {
                    this.groupByRowAppearanceHolder = new Infragistics.Win.AppearanceHolder();

                    // hook up the prop change notifications
                    //
                    this.groupByRowAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }

                // Initialize the collection
                //
                if (this.Layout != null)
                    this.groupByRowAppearanceHolder.Collection = this.Layout.Appearances;

                return this.groupByRowAppearanceHolder.Appearance;
            }
            set
            {
                if (this.groupByRowAppearanceHolder == null ||
                    !this.groupByRowAppearanceHolder.HasAppearance ||
                    value != this.groupByRowAppearanceHolder.Appearance)
                {
                    if (null == this.groupByRowAppearanceHolder)
                    {
                        this.groupByRowAppearanceHolder = new Infragistics.Win.AppearanceHolder();

                        // hook up the new prop change notifications
                        //
                        this.groupByRowAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                    }

                    if (this.Layout != null)
                        this.groupByRowAppearanceHolder.Collection = this.Layout.Appearances;

                    this.groupByRowAppearanceHolder.Appearance = value;

                    // notify listeners
                    //
                    this.NotifyPropChange(PropertyIds.GroupByRowAppearance);
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal bool HasGroupByRowAppearance
        {
            get
            {
                return null != this.groupByRowAppearanceHolder
                    && this.groupByRowAppearanceHolder.HasAppearance;
            }

        }

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeGroupByRowAppearance()
        {
            return this.HasGroupByRowAppearance
                && this.groupByRowAppearanceHolder.ShouldSerialize();
        }

        /// <summary>
        /// Resets the <see cref="GroupByRowAppearance"/> property to its default value.
        /// </summary>
        public void ResetGroupByRowAppearance()
        {
            if (this.HasGroupByRowAppearance)
            {
                this.groupByRowAppearanceHolder.Reset();

                // Notify listeners that the appearance has changed. 
                this.NotifyPropChange(PropertyIds.GroupByRowAppearance);
            }
        }
        #endregion 

        // MBS 6/19/09 - TFS18639
        #region FilterDropDownOperands

        /// <summary>
        /// Gets or sets the items that will be displayed by the grid when showing a dropdown filter list.
        /// </summary>        
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b>The <b>Errors</b> and <b>NonErrors</b> options will only be shown in the list if the
        /// <see cref="SupportDataErrorInfo"/> property enables the error info on cells.
        /// </p>        
        /// </remarks>
        /// <seealso cref="FilterUIType"/>
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridOverride_P_FilterDropDownOperands")]
        [Editor(typeof(Infragistics.Win.UltraWinGrid.DefaultableFlagsEnumUITypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public FilterOperandDropDownItems FilterOperandDropDownItems
        {
            get { return this.filterOperandDropDownItems; }
            set
            {
                if (this.filterOperandDropDownItems == value)
                    return;

                GridUtils.ValidateEnum("FilterDropDownOperands", typeof(FilterOperandDropDownItems), value);
                this.filterOperandDropDownItems = value;
                this.NotifyPropChange(PropertyIds.FilterOperandDropDownItems);
            }
        }

        internal FilterOperandDropDownItems FilterOperandDropDownItemsResolved
        {
            get
            {
                if (this.ShouldSerializeFilterOperandDropDownItems())
                    return this.FilterOperandDropDownItems;

                if (this.Band.HasOverride &&
                    this.Band.Override.ShouldSerializeFilterOperandDropDownItems())
                    return this.Band.Override.FilterOperandDropDownItems;

                if (this.Layout.HasOverride
                    && this.Layout.Override.ShouldSerializeFilterOperandDropDownItems())
                    return this.Layout.Override.FilterOperandDropDownItems;

                return UltraGridColumn.DefaultFilterOperandDropDownItems;
            }
        }

        /// <summary>
        /// Returns true if the FilterDropDownOperands property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeFilterOperandDropDownItems()
        {
            return this.filterOperandDropDownItems != FilterOperandDropDownItems.Default;
        }

        /// <summary>
        /// Resets the <see cref="UltraGridColumn.FilterOperandDropDownItems"/> property to its default value.
        /// </summary>
        public void ResetFilterOperandDropDownItems()
        {
            this.FilterOperandDropDownItems = FilterOperandDropDownItems.Default;
        }
        #endregion //FilterDropDownOperands

    }//end of column class

		#region IGroupByEvaluator

		/// <summary>
		/// Interface to be used for determining how rows are grouped.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>IGroupByEvaluator</b> interface is used to supply custom logic for grouping rows. 
		/// If none of the <see cref="GroupByMode"/> settings meet your needs, you can supply 
		/// a GroupByEvaluator to perform custom grouping. If your grouping criteria is not
		/// consistent with the sorting criteria, then you may need to implement the 
		/// <see cref="IGroupByEvaluatorEx"/> interface. <b>IGroupByEvaluatorEx</b> derives
		/// from this and exposes additional Compare method for sorting rows.
		/// </p>
		/// <p class="body">The following example shows how to implement a IGroupByEvaluator so that rows are grouped by the 1st 2 characters of a string field:</p>
		/// <p></p>
		/// <pre>
		/// private void ultraGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
		/// {
		///		// set the view style to outlook group by
		///		e.Layout.ViewStyleBand = ViewStyleBand.OutlookGroupBy;
		/// 	
		/// 	Infragistics.Win.UltraWinGrid.UltraGridColumn column;
		/// 	
		/// 	// get the CompanyName column
		/// 	column = e.Layout.Bands[0].Columns["CompanyName"];
		/// 
		/// 	// set the GroupByEvaluator on the column to an instance of MyGroupByEvaluator 
		/// 	column.GroupByEvaluator = new MyGroupByEvaluator();
		/// 
		/// 	// set the column's HiddenWhenGroupBy property to false since we are
		/// 	// grouping by the 1st 2 characters of each string we want the full
		/// 	// company name to show in each row
		/// 	//
		/// 	column.HiddenWhenGroupBy = DefaultableBoolean.False;
		/// }
		/// 
		/// public class MyGroupByEvaluator : Infragistics.Win.UltraWinGrid.IGroupByEvaluator
		/// {
		///		public object GetGroupByValue( UltraGridGroupByRow groupbyRow, UltraGridRow row )
		///		{
		///			string val;
		///	
		///			// get the default value from the groupbyRow
		///			if (groupbyRow.Value == null )
		///				val = "";
		///			else
		///				val = groupbyRow.Value.ToString();
		///
		///			// if it is longer than 2 characters truncate it
		///			if ( val.Length > 2 )
		///				val = val.Substring( 0, 2 );
		///
		///			// Convert the string to uppercase for display 
		///			// in the groupbyrow description.
		///			return val.ToUpper();
		///		}
		///
		///		public bool DoesGroupContainRow( UltraGridGroupByRow groupbyRow, UltraGridRow row )
		///		{
		///			// get the related cell's value as a string
		///			string cellValue = row.Cells[groupbyRow.Column].Value.ToString();
		///
		///			// if it is longer than 2 characters truncate it
		///			if ( cellValue.Length > 2 )
		///				cellValue = cellValue.Substring(0, 2);
		///
		///			// Do a case insensitive compare
		///			return string.Compare(groupbyRow.Value.ToString(), cellValue, true) == 0;
		///		}
		/// }
		/// </pre>
		/// </remarks>
		public interface IGroupByEvaluator
		{
			/// <summary>
			/// Returns the value that will be used to initialize a <see cref="Infragistics.Win.UltraWinGrid.UltraGridGroupByRow"/>
			/// </summary>
			/// <param name="groupByRow">The new group by row.</param>
			/// <param name="row">The first row to be included in a new group by row.</param>
			/// <returns>The value to use as the new group by row's <see cref="Infragistics.Win.UltraWinGrid.UltraGridGroupByRow.Value"/>.</returns>
			/// <remarks>
			/// <p>In order to keep the default value use the following code:</p>
			/// <p></p>
			/// <pre>
			/// public object GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			/// {
			///		return groupByRow.Value;
			/// }
			/// </pre>
			/// </remarks>
			object GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row );

			/// <summary>
			/// Method for determining if a row should be included in a group by row.
			/// </summary>
			/// <param name="groupByRow">The group by row that contains other rows.</param>
			/// <param name="row">The row to be tested</param>
			/// <returns>True if the row should be considered part of this group.</returns>
			bool DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row );
		}
		
		#endregion IGroupByEvaluator

		#region IGroupByEvaluatorEx

	// SSP 11/11/05 BR07685
	// Added IGroupByEvaluatorEx interface.
	// 
	/// <summary>
	/// Interface to be used for determining how rows are grouped.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// This interface is extended from the <see cref="IGroupByEvaluator"/> interface. Typically
	/// you only need to implement the <b>IGroupByEvaluator</b> interface for custom grouping. You
	/// need to implement this interface if sorting of the rows is not consistent with the grouping
	/// criteria. For example, if you wanted to sort by first character of the values however group
	/// by last character of the values.
	/// </p>
	/// <seealso cref="UltraGridColumn.GroupByEvaluator"/>
	/// <seealso cref="IGroupByEvaluator"/>
	/// </remarks>
	public interface IGroupByEvaluatorEx : IGroupByEvaluator
	{
		/// <summary>
		/// Compares the cell values of the specified cells.
		/// </summary>
		/// <param name="cell1">An UltraGridCell</param>
        /// <param name="cell2">An UltraGridCell</param>
		/// <returns></returns>
		int Compare( UltraGridCell cell1, UltraGridCell cell2 );
	}
		
		#endregion IGroupByEvaluatorEx

		#region ColumnCloneData
	internal class ColumnCloneData
	{
		internal ColumnCloneData()
		{
			this.clonedFromCol = null;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.group = null;

			this.width = 0;
			this.visiblePosition = 0;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.level = 0;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal Infragistics.Win.UltraWinGrid.PosChanged	posChanged;

		internal UltraGridColumn								clonedFromCol;
		internal int								width;
		internal int								visiblePosition;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal UltraGridGroup								group;
		//internal int								level;
	}


		#endregion ColumnCloneData

	internal class ChapteredColumnType
	{
	}

	#region ColumnUITypeEditor Class
	/// <summary>
	/// UITypeEditor for the UltraGridColumn objects.
	/// </summary>
    // MRS 12/19/06 - fxCop
    //// AS 1/8/03 - FxCop
    //// Added attribute to prevent FxCop violations
    //	
    
    
    public class ColumnUITypeEditor : UITypeEditor
	{

		/// <summary>
		/// Used to determine the type of UIEditor to display.
		/// </summary>
		/// <param name="context">ITypeDescriptorContext</param>
		/// <returns>Enum specifying the type of editor to display.</returns>
		public override System.Drawing.Design.UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.Modal;
		}

		/// <summary>
		/// Used to edit and convert the value as needed.
		/// </summary>
		/// <param name="context">ITypeDescriptorContext</param>
		/// <param name="provider">IServiceProvider</param>
		/// <param name="value">Current value</param>
		/// <returns>Edited value</returns>
		public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			if (context != null && context.Instance != null && provider != null) 
			{
				IWindowsFormsEditorService edSvc = null;

				edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

				if (edSvc != null && value != null && value is UltraWinGrid.ColumnsCollection)
				{
					try
					{
						edSvc.ShowDialog( new ColumnEditorDialog(value as UltraWinGrid.ColumnsCollection) );
					}
					catch (Exception err )
					{
						System.Diagnostics.Debug.WriteLine(err.ToString());
					}
				}
			}

			return value;
		}
	}
	#endregion ColumnUITypeEditor Class

	//	MRS 3/5/04
	//	Changed this to derive from DataTypeUITypeEditor up in Win
	// AS - 1/3/02 UWG884
	#region ColumnDataTypeUITypeEditor class
	/// <summary>
	/// UITypeEditor for selecting the data type of unbound columns.
	/// </summary>
    // MRS 12/19/06 - fxCop
    //// AS 1/8/03 - FxCop
    //// Added attribute to prevent FxCop violations
    //    
    
    
    public class ColumnDataTypeUITypeEditor : DataTypeUITypeEditor
	{
		// SSP 3/8/04
		// Not used anymore, so commented it out.
		//
		//private IWindowsFormsEditorService edSvc = null;

//		/// <summary>
//		/// Used to determine the type of UIEditor that will be displayed.
//		/// </summary>
//		/// <param name="context">ITypeDescriptorContext</param>
//		/// <returns>UITypeEditorEditStyle specifying the type of UIEditor.</returns>
//		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context) 
//		{
//			return UITypeEditorEditStyle.DropDown;
//		}

		/// <summary>
		/// Used to edit the value and convert the value as needed.
		/// </summary>
		/// <param name="context">ITypeDescriptorContext</param>
		/// <param name="provider">IServiceProvider</param>
		/// <param name="value">Current value</param>
		/// <returns>Edited value.</returns>
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value) 
		{
			
//			if (context != null && context.Instance != null && provider != null) 
//			{
//				edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
//
//				if (edSvc != null)
//				{
					UltraGridColumn column = context.Instance as UltraGridColumn;

					if ( column == null )
						return value;

					if ( column.IsBound )
						throw new NotSupportedException( Shared.SR.GetString("LE_NotSupportedException_102") );

					return base.EditValue(context, provider, value);

//					//[DA 8.13.02] UWG1484
//					//Added bool type
//					object[] items = new object[] { typeof( string ),
//												    typeof( bool ),
//													typeof( int ),
//													typeof( double ),
//													typeof( float ),
//													typeof( DateTime ),
//													typeof( uint ),
//													typeof( long ),
//													typeof( short ),
//													typeof( ulong ),
//													typeof( ushort),
//													typeof( decimal ),
//													typeof( byte ),
//													typeof( sbyte ),
//													typeof( System.Guid ),
//					
//													// SSP 5/7/02
//													// Added support for System.Drawing.Color
//													//
//													typeof( System.Drawing.Color )
//												};
//
//					object defaultItem = typeof(string).GetType();
//
//					DropDownListBox listBox = new DropDownListBox(items, defaultItem);
//
//					// hook up the list click event
//					listBox.Click += new EventHandler(this.OnListClick);
//
//					// adjust the listbox size if there are less items visible
//					// then control can display
//					if (listBox.PreferredHeight < listBox.Height)
//						listBox.Height = listBox.PreferredHeight;
//
//					edSvc.DropDownControl(listBox);
//
//					if (listBox.SelectedItem != null && listBox.WasItemClicked)
//					{
//						value = listBox.SelectedItem as System.Type;
//					}
//
//				}
//			}
//
//			return value;
		}
	}
	#endregion ColumnDataTypeUITypeEditor class

	// SSP 11/3/04 - Merged Cell Feature
	//
	#region IMergedCellEvaluator Interface

	/// <summary>
	/// Interface that can be implemented to specify custom logic for determining which cells get merged.
	/// </summary>
	/// <remarks>
	/// <p><seealso cref="UltraGridColumn.MergedCellEvaluator"/></p>
	/// </remarks>
	public interface IMergedCellEvaluator
	{
		/// <summary>
		/// Returns true if the cells of row1 and row2 associated with the column should be merged.
		/// </summary>
		/// <param name="row1">An UltraGridRow</param>
        /// <param name="row2">An UltraGridRow</param>
		/// <param name="column">The UltraGridColumn</param>
		/// <returns></returns>
		bool ShouldCellsBeMerged( UltraGridRow row1, UltraGridRow row2, UltraGridColumn column );
	}

	#endregion // IMergedCellEvaluator Interface

    // MRS - NAS 9.1 - Groups in RowLayout
    #region IProvideRowLayoutColumnInfo interface    
    
    /// <summary>
    /// For internal use only.
    /// </summary>
    public interface IProvideRowLayoutColumnInfo
    {
        /// <summary>
        /// The band.
        /// </summary>
        UltraGridBand Band { get; }

        /// <summary>
        /// The RowLayoutColumnInfo.
        /// </summary>
        RowLayoutColumnInfo RowLayoutColumnInfo { get; }

        /// <summary>
        /// MinWidth
        /// </summary>
        int MinWidth { get; }

        /// <summary>
        /// MinimumSize
        /// </summary>
        Size MinimumSize { get; }

        /// <summary>
        /// MinimumHeaderSize
        /// </summary>
        Size MinimumHeaderSize { get; }

        /// <summary>
        /// PreferredHeaderSize 
        /// </summary>
        Size PreferredHeaderSize { get; }

        /// <summary>
        /// PreferredSize
        /// </summary>
        Size PreferredSize { get; }

        /// <summary>
        /// HeaderConstraint
        /// </summary>
        IGridBagConstraint HeaderConstraint { get; }

        /// <summary>
        /// HeaderLayoutItem
        /// </summary>
        ILayoutItem HeaderLayoutItem { get; }

        /// <summary>
        /// ContentLayoutItem
        /// </summary>
        ILayoutItem ContentLayoutItem { get; }

        /// <summary>
        /// Header
        /// </summary>
        HeaderBase Header { get; }

        /// <summary>
        /// The LayoutItem to be used when resizing.
        /// </summary>
        ILayoutItem ResizeLayoutItem { get; }
    }
    #endregion //IProvideRowLayoutColumnInfo interface
}
