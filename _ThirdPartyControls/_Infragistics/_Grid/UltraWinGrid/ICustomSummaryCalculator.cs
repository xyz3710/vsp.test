#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{
	#region ICustomSummaryCalculator

	/// <summary>
	/// Interface user would implement to calculate custom summaries. <see cref="Infragistics.Win.UltraWinGrid.SummarySettings.CustomSummaryCalculator"/> for more information.
	/// </summary>
	public interface ICustomSummaryCalculator
	{
		// SSP 7/15/02 UWG1372
		// Added new rows parameter to the BeginCustomSummary and EndCustomSummary (and
		// made the necessary changes to the implementing interfaces).
		//

		/// <summary>
		/// Begins a custom summary for the SummarySettings object passed in. Implementation of this method would clear up and reset any state variables used for calculating the summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		void BeginCustomSummary( SummarySettings summarySettings, RowsCollection rows );

		/// <summary>
		/// Implementing code processes the value of the cell associated with passed in row and the SourceColumn of the passed in SummarySettings parameter.
		/// </summary>
		/// <param name="summarySettings">The SummarySettings</param>
		/// <param name="row">The UltraGridRow</param>
		void AggregateCustomSummary( SummarySettings summarySettings, UltraGridRow row );

		/// <summary>
		/// Ends previously begun summary and returns the calculated summary value. This is called after AggregateCustomSummary is called for all the rows to be summarized in a summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		/// <returns>Returns the summary value.</returns>
		object EndCustomSummary( SummarySettings summarySettings, RowsCollection rows );
	}

	#endregion // ICustomSummaryCalculator

	#region AverageSummaryCalculator

	internal class AverageSummaryCalculator : ICustomSummaryCalculator
	{
		// Decimal because it has the highest precision.
		//
		private decimal sum = 0;
		private int numberOfItems  = 0;

		internal AverageSummaryCalculator( )
		{
		}

		/// <summary>
		/// Begins a custom summary for the SummarySettings object passed in. Implementation of this method would clear up and reset any state variables used for calculating the summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		public void BeginCustomSummary( SummarySettings summarySettings, RowsCollection rows )
		{
			this.sum = 0;
			this.numberOfItems = 0;
		}

		/// <summary>
		/// Implementing code processes the value of the cell associated with passed in row and the SourceColumn off the passed in SummarySettings parameter.
		/// </summary>
		/// <param name="summarySettings"></param>
		/// <param name="row"></param>
		public void AggregateCustomSummary( Infragistics.Win.UltraWinGrid.SummarySettings summarySettings,
														Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			Debug.Assert( null != row && null != summarySettings, "Null row or null summarySettings object passed in!" );

			if ( null == row || null == summarySettings )
				return;

			Debug.Assert( null != summarySettings.SourceColumn, "Null source colmn !" );

			if ( null == summarySettings.SourceColumn )
				return;

			Debug.Assert( row.Band == summarySettings.SourceColumn.Band, "SourceColumn's band and row's band are different !" );

			if ( row.Band != summarySettings.SourceColumn.Band )
				return;

			object val = row.GetCellValue( summarySettings.SourceColumn );

			// Skip nulls.
			//
			if ( null == val || val is DBNull )
			{
				// If summary settings says not to ignore the nulls, then take nulls as 0 and
				// average them with rest of the values.
				//
				if ( !summarySettings.IgnoreNulls )
					this.numberOfItems++;

				return;
			}

			// Convert to decimal. Decimal because it has the highest precision.
			//
			try
			{
				// SSP 10/19/05 BR06540
				// Skip double.NaN. Convert.ToDecimal will throw exception if double.NaN is passed in.
				// 
				//val = Convert.ToDecimal( val, summarySettings.SourceColumn.FormatInfo );
				if ( ! ( val is double ) || ! double.IsNaN( (double)val ) )
					val = Convert.ToDecimal( val, summarySettings.SourceColumn.FormatInfo );
			}
			catch ( Exception )
			{
				//Debug.Assert( false, "Exception thrown while trying to convert cell's value to decimal !\n" + e.Message );
			}

			// If conversion was successful aggregate the value, otherwise just ignore the value.
			//
			if ( val is decimal )
			{
				
				//
				this.sum += (decimal)val;
				this.numberOfItems++;
			}
		}

		/// <summary>
		/// Ends previously begun summary and returns the calculated summary value. This is called after AggregateCustomSummary is called for all the rows to be summarized in a summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		/// <returns>Returns the summary value.</returns>
		public object EndCustomSummary( SummarySettings summarySettings, RowsCollection rows )		
		{
			if ( this.numberOfItems <= 0 )
				return 0m;

			decimal average = this.sum / this.numberOfItems;

			return average;
		}	
	}

	#endregion // AverageSummaryCalculator
	
	#region SumSummaryCalculator

	internal class SumSummaryCalculator : ICustomSummaryCalculator
	{
		// Decimal because it has the highest precision.
		//
		private decimal sum = 0;

		internal SumSummaryCalculator( )
		{
		}

		/// <summary>
		/// Begins a custom summary for the SummarySettings object passed in. Implementation of this method would clear up and reset any state variables used for calculating the summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		public void BeginCustomSummary( SummarySettings summarySettings, RowsCollection rows )		
		{
			this.sum = 0;
		}

		/// <summary>
		/// Implementing code processes the value of the cell associated with passed in row and the SourceColumn off the passed in SummarySettings parameter.
		/// </summary>
		/// <param name="summarySettings"></param>
		/// <param name="row"></param>
		public void AggregateCustomSummary( Infragistics.Win.UltraWinGrid.SummarySettings summarySettings,
			Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			Debug.Assert( null != row && null != summarySettings, "Null row or null summarySettings object passed in!" );

			if ( null == row || null == summarySettings )
				return;

			Debug.Assert( null != summarySettings.SourceColumn, "Null source colmn !" );

			if ( null == summarySettings.SourceColumn )
				return;

			Debug.Assert( row.Band == summarySettings.SourceColumn.Band, "SourceColumn's band and row's band are different !" );

			if ( row.Band != summarySettings.SourceColumn.Band )
				return;

			object val = row.GetCellValue( summarySettings.SourceColumn );

			// Skip nulls because with sum they won't make any difference even if they are
			// taken as 0.
			//
			if ( null == val || val is DBNull )
				return;

			// Convert to decimal. Decimal because it has the highest precision.
			//
			try
			{
				// SSP 10/19/05 BR06540
				// Skip double.NaN. Convert.ToDecimal will throw exception if double.NaN is passed in.
				// 
				//val = Convert.ToDecimal( val, summarySettings.SourceColumn.FormatInfo );
				if ( ! ( val is double ) || ! double.IsNaN( (double)val ) )
					val = Convert.ToDecimal( val, summarySettings.SourceColumn.FormatInfo );
			}
			catch ( Exception )
			{
				//Debug.Assert( false, "Exception thrown while trying to convert cell's value to decimal !\n" + e.Message );
			}

			// If conversion was successful aggregate the value, otherwise ignore the value.
			//
			if ( val is decimal )
			{
				this.sum += (decimal)val;
			}
		}

		/// <summary>
		/// Ends previously begun summary and returns the calculated summary value. This is called after AggregateCustomSummary is called for all the rows to be summarized in a summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		/// <returns>Returns the summary value.</returns>
		public object EndCustomSummary( SummarySettings summarySettings, RowsCollection rows )		
		{            
			return this.sum;
		}	
	}

	#endregion // AverageSummaryCalculator

	#region MinimumSummaryCalculator

	internal class MinimumSummaryCalculator : ICustomSummaryCalculator
	{
		// Decimal because it has the highest precision.
		//
		private object min = null;
		private bool firstTime = true;

		internal MinimumSummaryCalculator( )
		{
		}

		/// <summary>
		/// Begins a custom summary for the SummarySettings object passed in. Implementation of this method would clear up and reset any state variables used for calculating the summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		public void BeginCustomSummary( SummarySettings summarySettings, RowsCollection rows )		
		{
			this.min = null;
			this.firstTime = true;
		}


		/// <summary>
		/// Implementing code processes the value of the cell associated with passed in row and the SourceColumn off the passed in SummarySettings parameter.
		/// </summary>
		/// <param name="summarySettings"></param>
		/// <param name="row"></param>
		public void AggregateCustomSummary( Infragistics.Win.UltraWinGrid.SummarySettings summarySettings,
			Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			Debug.Assert( null != row && null != summarySettings, "Null row or null summarySettings object passed in!" );

			if ( null == row || null == summarySettings )
				return;

			Debug.Assert( null != summarySettings.SourceColumn, "Null source colmn !" );

			if ( null == summarySettings.SourceColumn )
				return;

			Debug.Assert( row.Band == summarySettings.SourceColumn.Band, "SourceColumn's band and row's band are different !" );

			if ( row.Band != summarySettings.SourceColumn.Band )
				return;

			object val = row.GetCellValue( summarySettings.SourceColumn );

			// Skip nulls.
			//
			if ( null == val || val is DBNull 
				// SSP 7/18/02 UWG1391
				// Ignore empty strings.
				//
				|| ( val is string && ((string)val).Length <= 0 ) )
			{
				// If summary settings says to ignore the nulls, then return.
				//
				if ( summarySettings.IgnoreNulls )
					return;
			}

			if ( this.firstTime )
			{
				// If this is the first value, then set the min to it and retrun.
				//
				this.min = val;
				this.firstTime = false;
				return;
			}
			else
			{
				// If it's not the first time, and if the min is already null or DBNull, 
				// then return because we already have gotten our min.
				//
				if ( null == this.min || this.min is DBNull )
					return;

				// If val is null, then assign that to min and return because null is always
				// less than everything.
				//
				if ( null == val || val is DBNull )
				{
					this.min = val;
					return;
				}
			}

			// If the val is not the same type as the column's data type, then convert.
			// This may be the case with unbound columns.
			//
			if ( val.GetType( ) != summarySettings.SourceColumn.DataType )
			{
				try
				{
					val = Convert.ChangeType( val, summarySettings.SourceColumn.DataType, summarySettings.SourceColumn.FormatInfo );
				}
				catch ( Exception )
				{
					//Debug.Assert( false, "Exception thrown while trying to convert cell's value to column's data type !\n" + e.Message );
				}
			}

			// Now try to compare the value with our current minumum.
			// If val is IComparable, then use that to compare, otherwise convert them to strings
			// and then try to compare them.
			//
			Debug.Assert( val.GetType( ) == this.min.GetType( ), "Min and val differ in types !" );

			if ( val is IComparable && val.GetType( ) == this.min.GetType( ) )
			{
				if ( ((IComparable)val).CompareTo( this.min ) < 0 )
					this.min = val;
			}
			else
			{
				string minString = this.min.ToString( );
				string valString = val.ToString( );

				// AS 1/8/03 - fxcop
				// Must supply an explicit CultureInfo to the String.Compare method.
				//if ( String.Compare( valString, minString ) < 0 )
				if ( String.Compare( valString, minString, false, System.Globalization.CultureInfo.CurrentCulture ) < 0 )
					this.min = val;
			}
		}

		/// <summary>
		/// Ends previously begun summary and returns the calculated summary value. This is called after AggregateCustomSummary is called for all the rows to be summarized in a summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		/// <returns>Returns the summary value.</returns>
		public object EndCustomSummary( SummarySettings summarySettings, RowsCollection rows )		
		{
			return this.min;
		}	
	}

	#endregion // MinimumSummaryCalculator

	#region MaximumSummaryCalculator

	internal class MaximumSummaryCalculator : ICustomSummaryCalculator
	{
		// Decimal because it has the highest precision.
		//
		private object max = null;

		internal MaximumSummaryCalculator( )
		{
		}

		/// <summary>
		/// Begins a custom summary for the SummarySettings object passed in. Implementation of this method would clear up and reset any state variables used for calculating the summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		public void BeginCustomSummary( SummarySettings summarySettings, RowsCollection rows )		
		{
			this.max = null;
		}

		/// <summary>
		/// Implementing code processes the value of the cell associated with passed in row and the SourceColumn off the passed in SummarySettings parameter.
		/// </summary>
		/// <param name="summarySettings"></param>
		/// <param name="row"></param>
		public void AggregateCustomSummary( Infragistics.Win.UltraWinGrid.SummarySettings summarySettings,
			Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			Debug.Assert( null != row && null != summarySettings, "Null row or null summarySettings object passed in!" );

			if ( null == row || null == summarySettings )
				return;

			Debug.Assert( null != summarySettings.SourceColumn, "Null source colmn !" );

			if ( null == summarySettings.SourceColumn )
				return;

			Debug.Assert( row.Band == summarySettings.SourceColumn.Band, "SourceColumn's band and row's band are different !" );

			if ( row.Band != summarySettings.SourceColumn.Band )
				return;

			object val = row.GetCellValue( summarySettings.SourceColumn );

			// Anything is greater than null.
			//
			if ( null == this.max || this.max is DBNull )
			{
				this.max = val;
				return;
			}

			// Nulls are always less than anything else.
			//
			if ( null == val || val is DBNull )
				return;

			// If the val is not the same type as the column's data type, then convert.
			// This may be the case with unbound columns.
			//
			if ( val.GetType( ) != summarySettings.SourceColumn.DataType )
			{
				try
				{
					val = Convert.ChangeType( val, summarySettings.SourceColumn.DataType, summarySettings.SourceColumn.FormatInfo );
				}
				catch ( Exception )
				{
					//Debug.Assert( false, "Exception thrown while trying to convert cell's value to column's data type !\n" + e.Message );
				}
			}

			// Now try to compare the value with our current minumum.
			// If val is IComparable, then use that to compare, otherwise convert them to strings
			// and then try to compare them.
			//
			Debug.Assert( val.GetType( ) == this.max.GetType( ), "Max and val differ in types !" );

			if ( val is IComparable && val.GetType( ) == this.max.GetType( ) )
			{
				if ( ((IComparable)val).CompareTo( this.max ) > 0 )
					this.max = val;
			}
			else
			{
				string maxString = this.max.ToString( );
				string valString = val.ToString( );

				// AS 1/8/03 - fxcop
				// Must supply an explicit CultureInfo to the String.Compare method.
				//if ( String.Compare( valString, maxString ) > 0 )
				if ( String.Compare( valString, maxString, false, System.Globalization.CultureInfo.CurrentCulture ) > 0 )
					this.max = val;
			}
		}

		/// <summary>
		/// Ends previously begun summary and returns the calculated summary value. This is called after AggregateCustomSummary is called for all the rows to be summarized in a summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		/// <returns>Returns the summary value.</returns>
		public object EndCustomSummary( SummarySettings summarySettings, RowsCollection rows )		
		{            
			return this.max;
		}	
	}

	#endregion // MaximumSummaryCalculator
	
	#region CountSummaryCalculator

	internal class CountSummaryCalculator : ICustomSummaryCalculator
	{
		// Decimal because it has the highest precision.
		//
		private int count = 0;

		internal CountSummaryCalculator( )
		{
		}

		/// <summary>
		/// Begins a custom summary for the SummarySettings object passed in. Implementation of this method would clear up and reset any state variables used for calculating the summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		public void BeginCustomSummary( SummarySettings summarySettings, RowsCollection rows )		
		{
			this.count = 0;
		}

		/// <summary>
		/// Implementing code processes the value of the cell associated with passed in row and the SourceColumn off the passed in SummarySettings parameter.
		/// </summary>
		/// <param name="summarySettings"></param>
		/// <param name="row"></param>
		public void AggregateCustomSummary( Infragistics.Win.UltraWinGrid.SummarySettings summarySettings,
			Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			Debug.Assert( null != row && null != summarySettings, "Null row or null summarySettings object passed in!" );

			if ( null == row || null == summarySettings )
				return;

			Debug.Assert( null != summarySettings.SourceColumn, "Null source column !" );

			if ( null == summarySettings.SourceColumn )
				return;

			Debug.Assert( row.Band == summarySettings.SourceColumn.Band, "SourceColumn's band and row's band are different !" );

			if ( row.Band != summarySettings.SourceColumn.Band )
				return;

			// If summarySettings says to ignore nulls, then don't incremenet the 
			// count.
			//
			if ( summarySettings.IgnoreNulls )
			{
				object val = row.GetCellValue( summarySettings.SourceColumn );

				if ( null == val || val is DBNull )
					return;
			}

			this.count++;
		}

		/// <summary>
		/// Ends previously begun summary and returns the calculated summary value. This is called after AggregateCustomSummary is called for all the rows to be summarized in a summary.
		/// </summary>
		/// <param name="summarySettings">SummarySettings object associated with the summary being calcualted.</param>
		/// <param name="rows">RowsCollection for which the summary is being calculated for.</param>
		/// <remarks>
		/// <p class="body"><code>rows</code> argument is the rows collection from the band that the <code>summarySettings</code> object is assigned to.</p>
		/// </remarks>
		/// <returns>Returns the summary value.</returns>
		public object EndCustomSummary( SummarySettings summarySettings, RowsCollection rows )		
		{            
			return this.count;
		}	
	}

	#endregion // CountSummaryCalculator
}
