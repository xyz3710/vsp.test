#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using Infragistics.Win;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.ComponentModel.Design;
using System.Reflection;
using System.Drawing.Design;
using System.Collections.Generic;

// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
// Added ability to display summaries in group-by rows aligned with columns, 
// summary footers for group-by row collections, fixed summary footers and
// being able to display the summary on top of the row collection.
// Added UltraGridSummaryRow class.
//

namespace Infragistics.Win.UltraWinGrid
{
	#region SummaryDisplayAreaContext Class

    // MRS 5/20/2009 - TFS17800
    // Made this class public so it can be used by the DocumentExporter. 
    //
	//internal class SummaryDisplayAreaContext
    /// <summary>
    /// Provides context information for the UltraGridSummaryRow. 
    /// </summary>
    public class SummaryDisplayAreaContext
	{
		internal const SummaryDisplayAreas ALL_EXCEPT_GROUPBY_SUMMARIES =
			SummaryDisplayAreas.Bottom | SummaryDisplayAreas.BottomFixed 
			| SummaryDisplayAreas.Top | SummaryDisplayAreas.TopFixed;

		private SummaryDisplayAreas matchAny;
		private SummaryDisplayAreas matchAll;

		// SSP 7/15/05 FR01423
		// Added the ability to show only the group-by row footers and only the top level footers.
		// A summary settings is said to match the context if it's resolved summary display 
		// are doesn't have any bit from the matchNone.
		// 
		private SummaryDisplayAreas matchNone;
		private SummaryDisplayAreas displayArea;

		private UltraGridGroupByRow groupByRowContext = null;
		
		internal SummaryDisplayAreaContext( SummaryDisplayAreas matchAny ) 
			: this( matchAny, SummaryDisplayAreas.None, 
			// SSP 7/15/05 FR01423
			// added matchNone param.
			// 
			SummaryDisplayAreas.None, matchAny )
		{
		}
		
		internal SummaryDisplayAreaContext( SummaryDisplayAreas matchAny, SummaryDisplayAreas matchAll        
			// SSP 7/15/05 FR01423
			// Added the ability to show only the group-by row footers and only the top level footers.
			// Added matchNone and displayArea parameters. A summary settings is said to match the 
			// context if it's resolved summary display are doesn't have any bit from the matchNone.
			// 
			, SummaryDisplayAreas matchNone, SummaryDisplayAreas displayArea )
		{
			this.matchAny = matchAny;
			this.matchAll = matchAll;

			// SSP 7/15/05 FR01423
			// 
			this.matchNone = matchNone;
			this.displayArea = displayArea;
		}

		internal bool IsGroupByRowSummaries
		{
			get
			{
				return SummaryDisplayAreas.InGroupByRows == this.matchAny;
			}
		}

		internal bool IsGroupByRowsCollectionSummaryFooter
		{
			get
			{
				// SSP 7/15/05 FR01423
				// Added the ability to show only the group-by row footers and only the top level footers.
				// 
				//return 0 != ( SummaryDisplayAreas.GroupByRowsFooter & this.matchAll );
				return 0 != ( SummaryDisplayAreas.GroupByRowsFooter & this.matchAny );
			}
		}

		internal SummaryDisplayAreas DisplayArea
		{
			get
			{
				// SSP 7/15/05 FR01423
				// 
				//return this.matchAny;
				return this.displayArea;
			}
		}

		internal bool IsDisplayAreaTop
		{
			get
			{
				return SummaryDisplayAreas.Top == this.DisplayArea
					|| SummaryDisplayAreas.TopFixed == this.DisplayArea;
			}
		}
		
		internal bool IsDisplayAreaBottom
		{
			get
			{
				return SummaryDisplayAreas.Bottom == this.DisplayArea
					|| SummaryDisplayAreas.BottomFixed == this.DisplayArea;
			}
		}

		internal bool DoesSummaryMatch( SummarySettings summary )
		{
			SummaryDisplayAreas area = summary.SummaryDisplayAreaResolved;
			return ( 0 == this.matchAny ||  0 != ( this.matchAny & area ) ) 
				&& this.matchAll == ( this.matchAll & area )
				// SSP 7/15/05 FR01423
				// Added the ability to show only the group-by row footers and only the top level footers.
				// Added matchNone member var. A summary settings is said to match the context if it's 
				// resolved summary display are doesn't have any bit from the matchNone.
				// 
                && 0 == ( this.matchNone & area )
				&& ! summary.HiddenResolvedHelper;
		}

		internal bool DoesSummaryMatch( SummaryValue summaryValue )
		{
			return this.DoesSummaryMatch( summaryValue.SummarySettings );
		}

		internal static SummaryDisplayAreaContext AllExceptGroupbySummaries
		{
			get
			{
				return new SummaryDisplayAreaContext( SummaryDisplayAreaContext.ALL_EXCEPT_GROUPBY_SUMMARIES );
			}
		}

		internal static SummaryDisplayAreaContext BottomScrollingSummariesContext
		{
			get
			{
				return new SummaryDisplayAreaContext( SummaryDisplayAreas.Bottom );
			}
		}

		internal static SummaryDisplayAreaContext NoneSummariesContext
		{
			get
			{
				return new SummaryDisplayAreaContext( SummaryDisplayAreas.None );
			}
		}

		internal static SummaryDisplayAreaContext InGroupByRowsSummariesContext
		{
			get
			{
				return new SummaryDisplayAreaContext( SummaryDisplayAreas.InGroupByRows );
			}
		}

		internal UltraGridGroupByRow GroupByRowContext
		{
			get
			{
				return this.groupByRowContext;
			}
		}

		internal static SummaryDisplayAreaContext GetInGroupByRowsSummariesContext( UltraGridGroupByRow groupByRowContext )
		{
			SummaryDisplayAreaContext context = new SummaryDisplayAreaContext( SummaryDisplayAreas.InGroupByRows );
			context.groupByRowContext = groupByRowContext;
			return context;
		}



        // MRS 5/20/2009 - TFS17800
        /// <summary>
        /// Determines whether the specified SummaryDisplayAreaContext is equal to the current SummaryDisplayAreaContext.
        /// </summary>
        /// <param name="obj">The SummaryDisplayAreaContext to compare with the current SummaryDisplayAreaContext.</param>
        /// <returns>true if the specified SummaryDisplayAreaContext is equal to the current SummaryDisplayAreaContext; otherwise, false.</returns>        
        public override bool Equals(object obj)
        {       
            SummaryDisplayAreaContext summaryDisplayAreaContext = obj as SummaryDisplayAreaContext;
            if (summaryDisplayAreaContext == null)
                return false;

            return this.displayArea == summaryDisplayAreaContext.displayArea &&
                this.matchAll == summaryDisplayAreaContext.matchAll &&
                this.matchAny == summaryDisplayAreaContext.matchAny &&
                this.matchNone == summaryDisplayAreaContext.matchNone &&
                this.groupByRowContext == summaryDisplayAreaContext.groupByRowContext;
        }

        // MRS 5/20/2009 - TFS17800
        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>A hash code for the current System.Object.</returns>
        public override int GetHashCode()
        {       
            int hashCode = this.displayArea.GetHashCode() ^
                this.matchAll.GetHashCode() ^
                this.matchAny.GetHashCode() ^
                this.matchNone.GetHashCode();

            if (this.groupByRowContext != null)
                hashCode = hashCode ^ this.groupByRowContext.GetHashCode();

            return hashCode;
        }

        /// <summary>
        /// Compares the SummaryDisplayAreaContexts accounting for all properties except for the GroupByRowContext.
        /// </summary>
        public class EqualityComparerWithoutGroupByRowContext : IEqualityComparer<SummaryDisplayAreaContext>
        {
            #region IEqualityComparer<SummaryDisplayAreaContext> Members

            bool IEqualityComparer<SummaryDisplayAreaContext>.Equals(SummaryDisplayAreaContext x, SummaryDisplayAreaContext y)
            {
                if (x == null && y != null)
                    return false;

                if (x != null && y == null)
                    return false;

                return x.displayArea == y.displayArea &&
                    x.matchAll == y.matchAll &&
                    x.matchAny == y.matchAny &&
                    x.matchNone == y.matchNone;
            }

            int IEqualityComparer<SummaryDisplayAreaContext>.GetHashCode(SummaryDisplayAreaContext obj)
            {
                return obj.displayArea.GetHashCode() ^
                    obj.matchAll.GetHashCode() ^
                    obj.matchAny.GetHashCode() ^
                    obj.matchNone.GetHashCode();
            }

            #endregion
        }

	}

	#endregion // SummaryDisplayAreaContext Class

	#region UltraGridSummaryRow Class

	/// <summary>
	/// A class to represent a summary row.
	/// </summary>
	public class UltraGridSummaryRow : UltraGridSpecialRowBase
	{
		#region Private Vars

		private SummaryDisplayAreaContext summaryDisplayAreaContext = null;

		#endregion // Private Vars

		#region Constructor
		
		internal UltraGridSummaryRow( RowsCollection parentCollection, SummaryDisplayAreaContext summaryDisplayAreaContext ) 
			: base( parentCollection )
		{
			GridUtils.ValidateNull( summaryDisplayAreaContext );

			// SSP 6/23/05 BR04635 BR04640
			// 
			//this.summaryDisplayAreaContext = summaryDisplayAreaContext;
			this.Initialize( summaryDisplayAreaContext );
		}

		#endregion // Constructor

		#region Private/Internal Properties/Methods

		#region Initialize

		// SSP 6/23/05 BR04635 BR04640
		// 
		internal void Initialize( SummaryDisplayAreaContext summaryDisplayAreaContext )
		{
			this.summaryDisplayAreaContext = summaryDisplayAreaContext;

			// SSP 7/18/05
			// 
			this.fixedResolvedLocationOverride = FixedRowStyle.Default;
		}

		#endregion // Initialize

		#region SummaryDisplayAreaContext

        // MRS 5/20/2009 - TFS17800
        /// <summary>
        /// Provides context information for the summary row. 
        /// </summary>
		public SummaryDisplayAreaContext SummaryDisplayAreaContext 
		{
			get
			{
				return this.summaryDisplayAreaContext;
			}
		}

		#endregion // SummaryDisplayAreaContext

		#endregion // Private/Internal Properties/Methods

		#region Public Methods

		#region IsSummaryVisible

		/// <summary>
		/// Returns true if the specified summary value would be visible in this row.
		/// </summary>
		/// <param name="summary">The associated summary settings.</param>
        /// <returns>true if the specified summary value would be visible in this row.</returns>
		public bool IsSummaryVisible( SummarySettings summary )
		{
			return this.SummaryDisplayAreaContext.DoesSummaryMatch( summary );
		}

		#endregion // IsSummaryVisible

		#region GetInGroupByRowSummaryContext

		// SSP 10/26/06 BR16917
		// Added GetInGroupByRowSummaryContext for use in the excel exported component.
		// 
		/// <summary>
		/// Returns an UltraGridSummaryRow instance for displaying summaries inside a group-by row.
		/// Various methods off the SummaryValuesCollection require a context to be passed in 
		/// that identifies the area in which the summaries are being displayed. The returned value
		/// is for that use. Note that this method will return null if there are no summaries or
		/// if GroupBySummaryDisplayStyle is not set to one of the SummaryCells options.
		/// </summary>
        /// <returns>An UltraGridSummaryRow instance for displaying summaries inside a group-by row.</returns>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public static UltraGridSummaryRow GetInGroupByRowSummaryContext( UltraGridGroupByRow groupByRow )
		{
			if ( null != groupByRow )
			{
				UltraGridBand band = groupByRow.Band;
				GroupBySummaryDisplayStyle style = band.GroupBySummaryDisplayStyleResolved;
				if ( band.HasSummaries && ( GroupBySummaryDisplayStyle.SummaryCells == style 
					|| GroupBySummaryDisplayStyle.SummaryCellsAlwaysBelowDescription == style ) )
				{
					SummaryDisplayAreaContext context = SummaryDisplayAreaContext.GetInGroupByRowsSummariesContext( groupByRow );
					if ( null != context )
						return new UltraGridSummaryRow( groupByRow.Rows, context );
				}
			}

			return null;
		}

		#endregion // GetInGroupByRowSummaryContext

		#region ShouldDisplaySummaryFooterCaption

		// SSP 10/26/06 BR16917
		// Added ShouldDisplaySummaryFooterCaption for use in the excel exported component.
		// 
		/// <summary>
		/// Specifies whether summaries associated with this summary row should display a
		/// summary footer caption.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public bool ShouldDisplaySummaryFooterCaption
		{
			get
			{
				return null != this.ParentCollection 
					&& this.ParentCollection.SummaryValues.SummaryFooterCaptionVisibleResolved 
					&& ! this.SummaryDisplayAreaContext.IsGroupByRowSummaries;
			}
		}

		#endregion // ShouldDisplaySummaryFooterCaption

		#endregion // Public Methods

		#region Base Overrides

		#region IsSummaryRow
		
		/// <summary>
		/// Indicates whether the row is a summary row.
		/// </summary>
		public override bool IsSummaryRow
		{
			get
			{
				return true;
			}
		}

		#endregion // IsSummaryRow

		#region ActivationResolved
		
		internal override Activation ActivationResolved
		{
			get
			{
				return Activation.Disabled;
			}
		}

		#endregion // ActivationResolved

		#region IsUIElementCompatible
		
		internal override bool IsUIElementCompatible( UIElement parent, UIElement elem )
		{
			return elem is SummaryFooterUIElement;
		}

		#endregion // IsUIElementCompatible

		#region HasSameContext

		internal override bool HasSameContext( UIElement parent, UIElement elem )
		{
			SummaryFooterUIElement summaryFooterElem = elem as SummaryFooterUIElement;
			return null != summaryFooterElem && this.ParentCollection == summaryFooterElem.Rows;
		}

		#endregion // HasSameContext

		#region CreateRowUIElement
		
		internal override UIElement CreateRowUIElement( UIElement parent )
		{
			return new SummaryFooterUIElement( parent, this.ParentCollection );
		}

		#endregion // CreateRowUIElement

		#region InitializeUIElement

		internal override void InitializeUIElement( UIElement elem )
		{
			SummaryFooterUIElement summaryFooterElem = (SummaryFooterUIElement)elem;
			summaryFooterElem.Initialize( this.ParentCollection, this.SummaryDisplayAreaContext );
		}

		#endregion // InitializeUIElement

		#region ShouldLeftAlignGroupByRowsFooter

		internal static bool ShouldLeftAlignGroupByRowsFooter( SummaryDisplayAreaContext context, RowsCollection rows )
		{
			return context.IsGroupByRowsCollectionSummaryFooter 
				&& ( context.IsDisplayAreaBottom 
					 || ! rows.IsTopLevel
					 || ! rows.Layout.ViewStyleImpl.IsMultiBandDisplay );
		}

		internal bool ShouldLeftAlignGroupByRowsFooter( )
		{
			return ShouldLeftAlignGroupByRowsFooter( this.SummaryDisplayAreaContext, this.ParentCollection );
		}

		#endregion // ShouldLeftAlignGroupByRowsFooter

		#region AdjustUIElementRect
		
		internal override void AdjustUIElementRect( ref Rectangle elemRect )
		{
			// Take out the pre row area from the summary footer element. Typically 
			// row ui elements include pre row area. However sumary footers do not 
			// include pre row area. Only do so for summary footers of non-group-by
			// rows. Also In multiband group-by scenario do not extend the top group-by 
			// row collection summaries to the left. They should be aligned with the
			// column headers.
			//
			if ( this.ShouldLeftAlignGroupByRowsFooter( ) )
			{
				int indent = this.BandInternal.CalcGroupByRowIndent( this.ParentCollection );
				elemRect.X += indent;
				elemRect.Width -= indent;
			}
			else
			{
				int preRowAreaExtent = this.BandInternal.PreRowAreaExtent;
				elemRect.X += preRowAreaExtent;
				elemRect.Width -= preRowAreaExtent;
			}
		}

		#endregion // AdjustUIElementRect

		#region SetElementClipSelfRect
		
		internal override void SetElementClipSelfRect( UIElement elem, Rectangle clipRect )
		{
			SummaryFooterUIElement summaryFooterElem = elem as SummaryFooterUIElement;
			if ( null != summaryFooterElem )
				summaryFooterElem.InternalSetClipSelfRegion( clipRect );
		}

		#endregion // SetElementClipSelfRect

		#region GetElementLastClipRect

		internal override Rectangle GetElementLastClipRect( UIElement elem )
		{
			SummaryFooterUIElement summaryFooterElem = elem as SummaryFooterUIElement;
			if ( null != summaryFooterElem )
				return summaryFooterElem.lastClipRect;

			return Rectangle.Empty;
		}

		#endregion // GetElementLastClipRect

		#region SetElementLastClipRect

		internal override void SetElementLastClipRect( UIElement elem, Rectangle clipRect )
		{
			SummaryFooterUIElement summaryFooterElem = elem as SummaryFooterUIElement;
			if ( null != summaryFooterElem )
				summaryFooterElem.lastClipRect = clipRect;
		}

		#endregion // SetElementLastClipRect

		#region HasHeadersOverride
		
		// SSP 7/6/05 - NAS 5.3 Empty Rows
		// Moved this into the base class since the UltraGridEmptyRow needs to override this too.
		// 
		//internal DefaultableBoolean HasHeadersOverride
		internal override DefaultableBoolean HasHeadersOverride
		{
			get
			{
				if ( this.SummaryDisplayAreaContext.IsGroupByRowsCollectionSummaryFooter
					&& ! this.ParentCollection.IsTopLevel )
					return DefaultableBoolean.False;

				// To maintain the behavior of 5.1 and older UltraGrid versions in regards
				// to summary footers, never show column headers above a summary footer if
				// there is a previous sibling row.
				//
				UltraGridRow prevRow = this.ParentCollection.GetPrevVisibleRow( this );
				if ( null != prevRow )
					return DefaultableBoolean.False;

				return DefaultableBoolean.Default;
			}
		}

		#endregion // HasHeadersOverride

		#region RowSpacingBeforeResolvedBase

		internal override int RowSpacingBeforeResolvedBase
		{
			get
			{
				UltraGridBand band = this.BandInternal;
				int ret = this.RowSpacingBefore;
				
				if ( ret < 0 && band.HasOverride )
					ret = band.Override.SummaryFooterSpacingBefore;

				if ( ret < 0 && band.Layout.HasOverride )
					ret = band.Layout.Override.SummaryFooterSpacingBefore;

				// For summary rows to maintain the old behavior ignore the row spacing
				// settings on the override. Only honor the one directly set on the summary
				// row itself. Also to maintain the old behavior have 1 pixel spacing
				// beween the rows of a row collection and the summary footer.
				//
				if ( ret < 0 && ! this.HasSpecialRowSeparatorBefore )
				{
					const int ROW_SPACING_BETWEEN_ROWS_AND_SUMMARY_FOOTER = 1;
					UltraGridRow prevRow = this.ParentCollection.GetPrevVisibleRow( this );
					if ( null != prevRow && 1 == prevRow.ScrollCountInternal )
						ret = ROW_SPACING_BETWEEN_ROWS_AND_SUMMARY_FOOTER;
				}

				return Math.Max( 0, ret );
			}
		}

		#endregion // RowSpacingBeforeResolvedBase

		#region RowSpacingAfterResolvedBase

		internal override int RowSpacingAfterResolvedBase
		{
			get
			{
				UltraGridBand band = this.BandInternal;
				int ret = this.RowSpacingAfter;
				
				if ( ret < 0 && band.HasOverride )
					ret = band.Override.SummaryFooterSpacingAfter;

				if ( ret < 0 && band.Layout.HasOverride )
					ret = band.Layout.Override.SummaryFooterSpacingAfter;

				// For summary rows to maintain the old behavior ignore the row spacing
				// settings on the override. Only honor the one directly set on the sumary
				// row itself.
				//
				return Math.Max( 0, ret );
			}
		}

		#endregion // RowSpacingAfterResolvedBase

		#region FixedResolvedLocation

		internal override FixedRowStyle FixedResolvedLocation
		{
			get
			{
				if ( this.ParentCollection.SupportsFixedRows )
				{
					if ( FixedRowStyle.Default != this.fixedResolvedLocationOverride )
						return this.fixedResolvedLocationOverride;

					SummaryDisplayAreas area = this.SummaryDisplayAreaContext.DisplayArea;
					if ( SummaryDisplayAreas.TopFixed == area )
						return FixedRowStyle.Top;
					else if ( SummaryDisplayAreas.BottomFixed == area )
						return FixedRowStyle.Bottom;
				}

				return FixedRowStyle.Default;
			}
		}

		#endregion // FixedResolvedLocation
		
		#region HasSpecialRowSeparatorBefore

		internal override bool HasSpecialRowSeparatorBefore
		{
			get
			{
				return 0 != ( SpecialRowSeparator.SummaryRow & this.BandInternal.GetSpecialRowSeparatorResolved( this ) )
					&& this.SummaryDisplayAreaContext.IsDisplayAreaBottom;
			}
		}

		#endregion // HasSpecialRowSeparatorBefore

		#region HasSpecialRowSeparatorAfter

		internal override bool HasSpecialRowSeparatorAfter
		{
			get
			{
				return 0 != ( SpecialRowSeparator.SummaryRow & this.BandInternal.GetSpecialRowSeparatorResolved( this ) )
					&& this.SummaryDisplayAreaContext.IsDisplayAreaTop;
			}
		}

		#endregion // HasSpecialRowSeparatorAfter
		
		#region AfterElementAdded

		internal override void AfterElementAdded( UIElement addedRowElem, UIElementsCollection oldElems, ref Rectangle actualRectOccupiedByRowRelatedElements )
		{
			// Added ability to display row connectors connecting the rows to the summary footer element.
			//
			if ( ! this.IsFromGroupByRowsParentCollection )
			{
				int preRowAreaExtent = this.BandInternal.PreRowAreaExtent;
				if ( preRowAreaExtent > 0 )
				{
					PreRowAreaUIElement preRowAreaElem = (PreRowAreaUIElement)GridUtils.ExtractExistingElement( oldElems, typeof( PreRowAreaUIElement ), true );
					if ( null == preRowAreaElem )
						preRowAreaElem = new PreRowAreaUIElement( addedRowElem.Parent );

					preRowAreaElem.Initialize( this );
					Rectangle rect = addedRowElem.Rect;
					rect.Width = preRowAreaExtent;
					rect.X -= preRowAreaExtent;
					actualRectOccupiedByRowRelatedElements = Rectangle.Union( actualRectOccupiedByRowRelatedElements, rect );
					preRowAreaElem.Rect = rect;

					addedRowElem.Parent.ChildElements.Add( preRowAreaElem );
				}
			}

			// Call base implementation to add the row separators.
			//
			base.AfterElementAdded( addedRowElem, oldElems, ref actualRectOccupiedByRowRelatedElements );
		}

		#endregion // AfterElementAdded
		
		internal override int BaseHeight
		{
			get
			{
				return this.ParentCollection.SummaryValues.GetSummaryFooterHeight( this.SummaryDisplayAreaContext );
			}
		}

		#region LayoutManager
		
		internal override Infragistics.Win.Layout.LayoutManagerBase LayoutManager
		{
			get
			{
				return this.BandInternal.GetSummaryLayoutManager( this.SummaryDisplayAreaContext );
			}
		}

		#endregion // LayoutManager

		#endregion // Base Overrides
	}

	#endregion // UltraGridSummaryRow Class

}
