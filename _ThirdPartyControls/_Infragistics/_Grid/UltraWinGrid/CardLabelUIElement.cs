#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Summary description for CardLabelUIElement.
	/// </summary>
	public class CardLabelUIElement : HeaderUIElement
	{
		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="parent">The parent element</param>
		public CardLabelUIElement(UIElement parent) : base(parent)
		{
		}

		#endregion Constructor

		#region Base Class Overrides

			#region DrawBorders

		// JM 01-16-02 Allow the borders to draw.
		// /// <summary>
		// /// Does nothing
		// /// </summary>
		//protected override void DrawBorders(ref UIElementDrawParams drawParams) {}

			#endregion DrawBorders

			#region DrawTheme

		/// <summary>
		/// Used by an element to render using the system theme. This method will not
		/// be invoked if the themes are not supported.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		/// <returns>Returning true will prevent the invocation of the DrawBackColor,
		/// DrawImageBackground, DrawBorders, DrawImage and DrawForeground. Return
		/// false when themes are not supported by the element or if unable to 
		/// render using the system theme.</returns>
		protected override bool DrawTheme (ref UIElementDrawParams drawParams)
		{
			// JAS 5/6/05 BR03741 - If the HeaderStyle is 'XPThemed' then we need to 
			// attempt a themed rendering.
			//
			if( this.HeaderStyle == HeaderStyle.XPThemed )
				return base.DrawTheme( ref drawParams );

			// Don't use themes.
			return false;
		}

			#endregion DrawTheme

		// MD 8/3/07 - 7.3 Performance
		// Removed - The logic in here is not needed and the same result occurs by using 
		// just the base implementation
		#region Removed

		//    #region GetContext
		//
		///// <summary>
		///// Returns an object of requested type that relates to the
		///// element or null. 
		///// </summary> 
		///// <param name="type">The requested type or null to pick up default context object. </param>
		///// <param name="checkParentElementContexts"></param>
		//public override object GetContext(Type type, bool checkParentElementContexts)
		//{
		//
		//    // Since the type may be the derived class of HeaderBase or
		//    // Headerbase, we need to check both
		//    // AS 10/16/02
		//    // Make sure the type is not null.
		//    //
		//    if (type != null &&
		//        (typeof(HeaderBase) == type ||
		//        type.IsSubclassOf(typeof(HeaderBase))) )
		//    {
		//        // If the context is of the passed in type return it
		//        if (this.PrimaryContext != null)
		//        {
		//            if (this.PrimaryContext.GetType() == type ||
		//                this.PrimaryContext.GetType().IsSubclassOf(type))
		//                return this.PrimaryContext;
		//        }
		//    }
		//
		//    return base.GetContext(type, checkParentElementContexts);
		//}
		//
		//    #endregion GetContext

		#endregion Removed

		// SSP 3/14/03 - Row Layout Functionality
		// Commented out SupportsLeftRightAdjustmentsFromPoint method. The base HeaderUIElement will
		// check to see if the card view is turned on and take appropriate actions. This change was
		// made because in row-layout mode, card labels can be resized.
		//
		

			#region SelectableItem

		// SSP 3/11/03 - Row Layout Functionality
		// When the cells and headers are displayed together in the row area, then
		// select the associated cell when the header is clicked upon.
		// Overrode SelectableItem property.
		//
		/// <summary>
		/// If the context for this element is a selectable item
		/// (e.g. a grid row, cell or header) it is returned. The
		/// default implementation walks up the parent chain calling
		/// this method recursively until a selectable item is found
		/// or the control element is reached
		/// </summary>
		public override ISelectableItem SelectableItem
		{
			get
			{
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//UltraGridBand band = 
				//    null != this.Header && null != this.Header.Column 
				//    ? this.Header.Column.Band 
				//    : null;
				UltraGridBand band = null;
				UltraGridColumn headerColumn = null;

				if ( this.Header != null )
				{
					headerColumn = this.Header.Column;

					if ( headerColumn != null )
						band = headerColumn.Band;
				}

				if ( null != band && ! band.AreColumnHeadersInSeparateLayoutArea )
				{
					UltraGridRow row = (UltraGridRow)this.GetContext( typeof( UltraGridRow ), true );

					if ( null != row )
					{
						// MD 8/2/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//return row.Cells[ this.Header.Column ];
						return row.Cells[ headerColumn ];
					}
				}

				return base.SelectableItem;
			}
		}

			#endregion // SelectableItem

		#endregion Base Class Overrides

		#region PositionChildElements

		// SSP 8/1/05 - NAS 5.3 Header Tool Tips/Wrap Header Text for Row-Layout
		// Instead of duplicating the code here, let the base class implementation handle the
		// positioning of the child elements. There should be no difference in the way the child
		// elements are positioned in card-view and non-card-view.
		// 
		

		#endregion PositionChildElements

		#region Internal Properties

			#region Band

		internal UltraGridBand Band
		{
			get
			{
				CardAreaUIElement cardAreaElement = this.GetAncestor(typeof(CardAreaUIElement)) as CardAreaUIElement;

				if (cardAreaElement != null)
					return cardAreaElement.Band;
				else
					return null;
			}
		}

			#endregion Band

		#endregion Internal Properties
	}
}
