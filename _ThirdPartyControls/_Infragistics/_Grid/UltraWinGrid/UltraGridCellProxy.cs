#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using Infragistics.Shared;
using System.Windows.Forms;
using Infragistics.Win.AppStyling;
using System.Drawing;

namespace Infragistics.Win.UltraWinGrid
{
    #region GridCellProxyPropertyIds

    /// <summary>
    /// Uniquely identifies properties of the UltraGridCellProxy class.
    /// </summary>
    public enum GridCellProxyPropertyIds
    {
        /// <summary>
        /// Identifies the <see cref="UltraGridCellProxy.Appearance"/> property.
        /// </summary>
        Appearance,

        /// <summary>
        /// Identifies the <see cref="UltraGridCellProxy.BorderStyle"/> property.
        /// </summary>
        BorderStyle,

        /// <summary>
        /// Identifies the <see cref="UltraGridCellProxy.ButtonStyle"/> property.
        /// </summary>
        ButtonStyle,

        /// <summary>
        /// Identifies the <see cref="UltraGridCellProxy.ColumnKey"/> property.
        /// </summary>
        ColumnKey,

        /// <summary>
        /// Identifies the <see cref="UltraGridCellProxy.EditAppearance"/> property.
        /// </summary>
        EditAppearance,

        /// <summary>
        /// Identifies the <see cref="UltraGridCellProxy.HotTrackAppearance"/> property.
        /// </summary>
        HotTrackAppearance,

        /// <summary>
        /// Identifies the <see cref="UltraGridCellProxy.UseGridAppearances"/> property.
        /// </summary>
        UseGridAppearances,
    }
    #endregion //GridCellProxyPropertyIds

    #region UltraGridCellProxy

    /// <summary>
    /// A class used as a proxy for the editor of an UltraGridCell when used on a RowEditTemplate.
    /// </summary>
    /// <remarks>
    /// <p class="body">
    /// A proxy functions by hosting the editor that the associated <see cref="UltraGridCell"/> would use within its
    /// bounds.  This control can be thought of as taking the cell and moving it onto the template, but
    /// otherwise behaving as if the actual cell were being edited, including firing the events associated
    /// with editing or activating the cell.
    /// </p>
    /// <p class="note">
    /// <b>Note: </b>The proxy will only function when placed on a RowEditTemplate or within a child control of a RowEditTemplate.
    /// </p>
    /// </remarks>    
    /// <seealso cref="UltraGridRowEditTemplate"/>
    [ToolboxBitmap(typeof(UltraGridRowEditTemplate), AssemblyVersion.ToolBoxBitmapFolder + "UltraGridCellProxy.bmp")]
    
    [ToolboxItem(true)]
    
    [Designer(typeof(UltraControlDesigner))]
    // MBS 6/24/08 - BR34194
    [LocalizedDescription("LD_UltraGridCellProxy")]
    public class UltraGridCellProxy : UltraEditorProxyBase,
        Infragistics.Shared.IUltraLicensedComponent
    {
        #region Members

        private UltraLicense license;
        private string columnKey;                   
        private UltraGridRowEditTemplate rowEditTemplate;
        private UltraGridCellProxyOwnerInfo owner;
        private AppearanceHolder appearanceHolder;
        private AppearanceHolder editAppearanceHolder;
        private AppearanceHolder hotTrackAppearanceHolder;
        private SubObjectPropChangeEventHandler subObjectPropChangeHandler;
        private RowEditTemplateRowChangedEventHandler rowChangedHandler;
        private KeyEventHandler editorKeyDownHandler;
        private bool useGridAppearances = true;        
        private EmbeddableEditorBase cachedEditor;
        private bool haveHookedEditor;
        private EditorWithText proxyEditorWithText;
        private bool didProxyEnterFail;
        private bool isInOnEnter;
        private UltraGridCellProxyUIElement mainElement;
        private Color backColorInternal;
        private UIElementBorderStyle borderStyle = UIElementBorderStyle.Default;
        private UIElementButtonStyle buttonStyle = UIElementButtonStyle.Default;
        private bool isSelecting;

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public UltraGridCellProxy() 
            : base() 
        {
            try
            {
                this.license = LicenseManager.Validate(typeof(UltraGridCellProxy), this) as UltraLicense;
            }
            catch (System.IO.FileNotFoundException) { }  
        }

        #endregion //Constructor      

        #region About Dialog and Licensing Interface

        /// <summary>
        /// Display the about dialog
        /// </summary>
        [DesignOnly(true)]
        [LocalizedDescription("LD_UltraGrid_P_About")]
        [LocalizedCategory("LC_Design")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [ParenthesizePropertyName(true)]
        [Editor(typeof(AboutDialogEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public object About { get { return null; } }

        UltraLicense IUltraLicensedComponent.License { get { return this.license; } }

        #endregion //About Dialog and Licensing Interface

        #region Private/Internal Properties

        #region Band

        internal UltraGridBand Band
        {
            get
            {
                if (this.RowEditTemplate == null)
                    return null;

                return this.RowEditTemplate.Band;
            }
        }
        #endregion //Band

        #region Column

        // MBS 3/24/09 - TFS15730
        // Changed from private to internal
        internal UltraGridColumn Column
        {
            get
            {
                if (this.Band == null || this.Band.Columns.Exists(this.columnKey) == false)
                    return null;

                // If we've had to create an editor to display an error message in the designer,
                // now that we have a column to return, we should use the appropriate editor,
                // so 
                if (this.DesignMode && this.cachedEditor != null)
                    this.cachedEditor = null;

                return this.Band.Columns[this.columnKey];
            }
        }
        #endregion //Column

        #region EditorKeyDownHandler

        private KeyEventHandler EditorKeyDownHandler
        {
            get
            {
                if (this.editorKeyDownHandler == null)
                    this.editorKeyDownHandler = new KeyEventHandler(this.OnEditorKeyDown);

                return this.editorKeyDownHandler;
            }
        }
        #endregion //EditorKeyDownHandler

        #region InvalidProxyEditor

        private EditorWithText ProxyEditorWithText
        {
            get
            {
                if (this.proxyEditorWithText == null)
                    this.proxyEditorWithText = new EditorWithText(this.Owner);

                return this.proxyEditorWithText;
            }
        }
        #endregion //InvalidProxyEditor

        #region IsHotTracking

        // MBS 3/24/09 - TFS15730
        // Changed from private to internal
        internal bool IsHotTracking
        {
            get
            {
                if (this.ControlUIElement == null)
                    return false;

                Point cursorPos = this.PointToClient(Cursor.Position);
                return this.ControlUIElement.Rect.Contains(cursorPos);
            }
        }
        #endregion //IsHotTracking

        #region Row

        private UltraGridRow Row
        {
            get { return this.rowEditTemplate != null ? this.rowEditTemplate.Row : null; }
        }
        #endregion //Row

        #region RowChangedHandler

        private RowEditTemplateRowChangedEventHandler RowChangedHandler
        {
            get
            {
                if (this.rowChangedHandler == null)
                    this.rowChangedHandler = new RowEditTemplateRowChangedEventHandler(this.OnRowChanged);

                return this.rowChangedHandler;
            }
        }
        #endregion //RowChangedHandler

        #region RowEditTemplate

        internal UltraGridRowEditTemplate RowEditTemplate
        {
            get { return this.rowEditTemplate; }
            set
            {
                if (this.rowEditTemplate == value)
                    return;

                // We need to unhook from the previous template
                if (this.rowEditTemplate != null)
                    this.rowEditTemplate.RowChanged -= this.RowChangedHandler;

                this.rowEditTemplate = value;

                // Hook the event so that we can take appropriate action when the row changes
                if (this.rowEditTemplate != null)
                    this.rowEditTemplate.RowChanged += this.RowChangedHandler;
            }
        }
        #endregion //RowEditTemplate

        #region SubObjectPropChangeHandler

        private SubObjectPropChangeEventHandler SubObjectPropChangeHandler
        {
            get
            {
                if (this.subObjectPropChangeHandler == null)
                    this.subObjectPropChangeHandler = new SubObjectPropChangeEventHandler(this.OnSubObjectPropChanged);

                return this.subObjectPropChangeHandler;
            }
        }
        #endregion SubObjectPropChangeHandler

        #endregion //Private/Internal Properties

        #region Public Properties

        #region Appearance

        /// <summary>
        /// Gets or sets the appearance of the control.
        /// </summary>
        /// <remarks>
        /// <p class="note"><b>Note: </b>These settings take precedence over appearances provided by the grid.</p>
        /// </remarks>
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridCellProxy_P_Appearance")]
        public AppearanceBase Appearance
        {
            get
            {
                if (this.appearanceHolder == null)
                {
                    this.appearanceHolder = new AppearanceHolder();
                    this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }
                return this.appearanceHolder.Appearance;
            }
            set
            {
                if (this.appearanceHolder == null ||
                    !this.appearanceHolder.HasAppearance ||
                    value != this.appearanceHolder.Appearance)
                {
                    if (null == this.appearanceHolder)
                    {
                        this.appearanceHolder = new AppearanceHolder();
                        this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                    }

                    this.appearanceHolder.Appearance = value;
                    this.NotifyPropChange(GridCellProxyPropertyIds.Appearance);
                }
            }
        }

        /// <summary>
        /// Returns true if an appearance has been defined.
        /// </summary>        
        [Browsable(false)]
        public bool HasAppearance
        {
            get
            {
                return this.appearanceHolder != null && this.appearanceHolder.HasAppearance;
            }
        }

        /// <summary>
        /// Returns a Boolean value that determines whether the Appearance property is set to its default value.
        /// </summary>
        /// <returns>True if the value should be serialized.</returns>
        protected bool ShouldSerializeAppearance()
        {
            return this.HasAppearance && this.appearanceHolder.ShouldSerialize();
        }

        /// <summary>
        /// Resets the Appearance property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetAppearance()
        {
            if (this.HasAppearance)
            {
                this.appearanceHolder.Reset();
                this.SyncBackColor();
                this.NotifyPropChange(GridCellProxyPropertyIds.Appearance);
            }
        }
        #endregion //Appearance

        #region BackColorInternal

        /// <summary>
        /// For internal use only. Used for serializing out the BackColor property value. This
        /// is strictly for backward compatibility.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [DefaultValue(typeof(Color), "")]
        public Color BackColorInternal
        {
            get
            {
                return this.backColorInternal;
            }
            set
            {
                if (value != this.backColorInternal)
                {
                    this.backColorInternal = value;

                    this.SyncBackColor();
                }
            }
        }
        #endregion //BackColorInternal

        #region BorderStyle

        /// <summary>
        /// Gets or sets the style of the borders for the proxy.
        /// </summary>        
        [LocalizedDescription("LD_UltraGridCellProxy_P_BorderStyle")]
        [LocalizedCategory("LC_Appearance")]
        [DefaultValue(UIElementBorderStyle.Default)]
        public UIElementBorderStyle BorderStyle
        {
            get { return this.borderStyle; }
            set
            {
                if (this.borderStyle == value)
                    return;

                if (!Enum.IsDefined(typeof(UIElementBorderStyle), value))
                    throw new InvalidEnumArgumentException("value", (int)value, typeof(UIElementBorderStyle));

                this.borderStyle = value;

                this.ComponentRole.ClearCachedPropertyValues();

                if(this.IsHandleCreated && this.ControlUIElement != null)
                    this.ControlUIElement.DirtyChildElements();

                this.NotifyPropChange(GridCellProxyPropertyIds.BorderStyle);
            }
        }

        /// <summary>
        /// Returns the resolved <see cref="BorderStyle"/> for the control.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public UIElementBorderStyle BorderStyleResolved
        {
            get
            {
                object val;

                if (false == this.ComponentRole.GetCachedProperty(UltraGridCellProxyRole.PropertyBorderStyle, out val))
                {
                    AppStyling.UIRole mainRole = StyleUtils.GetRole(this, StyleUtils.CellProxyRole.CellProxy);
                    UIElementBorderStyle fallBackValue;

                    if (this.UseOsThemesResolved &&
                        XPThemes.ThemesSupported &&
                        XPThemes.IsThemeActive &&
                        XPThemes.AppControlsThemeThemed)
                    {
                        fallBackValue = UIElementBorderStyle.Solid;
                    }
                    else
                        fallBackValue = UIElementBorderStyle.Inset;

                    val = this.ComponentRole.CachePropertyValue(
                        mainRole.BorderStyle,
                        this.borderStyle,
                        UIElementBorderStyle.Default,
                        fallBackValue,
                        UltraGridCellProxyRole.PropertyBorderStyle);
                }

                return (UIElementBorderStyle)val;
            }
        }
        #endregion BorderStyle

        #region ButtonStyle

        /// <summary>
        /// Gets or sets the style of the buttons on the proxy.
        /// </summary>
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridCellProxy_P_ButtonStyle")]
        [DefaultValue(UIElementButtonStyle.Default)]
        public UIElementButtonStyle ButtonStyle
        {
            get { return this.buttonStyle; }
            set
            {
                if (this.buttonStyle != value)
                {
                    if (!Enum.IsDefined(typeof(UIElementButtonStyle), value))
                        throw new InvalidEnumArgumentException("value", (int)value, typeof(UIElementButtonStyle));

                    this.buttonStyle = value;

                    this.ComponentRole.ClearCachedPropertyValues();

                    if (this.IsHandleCreated && this.ControlUIElement != null)
                        this.ControlUIElement.DirtyChildElements();                    

                    this.NotifyPropChange(GridCellProxyPropertyIds.ButtonStyle);
                }
            }
        }

        /// <summary>
        /// Returns the resolved style of buttons used on the proxy.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public UIElementButtonStyle ButtonStyleResolved
        {
            get
            {
                object val;

                if (false == this.ComponentRole.GetCachedProperty(UltraGridCellProxyRole.PropertyButtonStyle, out val))
                {
                    AppStyling.UIRole mainRole = StyleUtils.GetRole(this, StyleUtils.CellProxyRole.CellProxy);
                    UIElementButtonStyle fallBackValue;
                    ((UltraGridCellProxyOwnerInfo)this.Owner).GetBaseButtonStyle(this.OwnerContext, out fallBackValue);

                    val = this.ComponentRole.CachePropertyValue(
                        mainRole.GetButtonStyle(this.ComponentRole),
                        this.buttonStyle,
                        UIElementButtonStyle.Default,
                        fallBackValue,
                        UltraGridCellProxyRole.PropertyButtonStyle);
                }
                return (UIElementButtonStyle)val;
            }
        }
        #endregion //ButtonStyle

        #region ColumnKey

        /// <summary>
        /// Gets or sets the key of the column of the grid with which the editor should be associated.
        /// </summary>
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b>If the specified column does not exist in the band associated with
        /// the RowEditTemplate, or the template is not currently associated with a band,  the
        /// proxy will be disabled, showing a message as its value at design-time.
        /// </p>
        /// </remarks>
        [DefaultValue(null)]
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridCellProxy_P_ColumnKey")]
        public string ColumnKey
        {
            get { return this.columnKey; }
            set
            {
                if (this.columnKey == value)
                    return;

                if (this.RowEditTemplate != null)
                {
                    if (this.RowEditTemplate.ColumnProxyMap.ContainsKey(value))
                        throw new InvalidOperationException(SR.GetString("LE_UltraGridRowEditTemplate_MultipleProxiesOnColumn"));

                    // Remove the current key from the collection so that another proxy could act for the column
                    if(this.columnKey != null && this.RowEditTemplate.ColumnProxyMap.ContainsKey(this.columnKey))
                        this.RowEditTemplate.ColumnProxyMap.Remove(this.columnKey);

                    // Add the new value to the map
                    if(value != null)
                        this.RowEditTemplate.ColumnProxyMap.Add(value, this);
                }

                this.columnKey = value;
                this.ControlUIElement.DirtyChildElements();
                this.NotifyPropChange(GridCellProxyPropertyIds.ColumnKey);
            }
        }
        #endregion //ColumnKey

        #region EditAppearance

        /// <summary>
        /// Gets or sets the appearance of the control while in edit mode.
        /// </summary>
        /// <remarks>
        /// <p class="note"><b>Note: </b>These settings take precedence over appearances provided by the grid.</p>
        /// </remarks>
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridCellProxy_P_EditAppearance")]
        public AppearanceBase EditAppearance
        {
            get
            {
                if (this.editAppearanceHolder == null)
                {
                    this.editAppearanceHolder = new AppearanceHolder();
                    this.editAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }
                return this.editAppearanceHolder.Appearance;
            }
            set
            {
                if (this.editAppearanceHolder == null ||
                    !this.editAppearanceHolder.HasAppearance ||
                    value != this.editAppearanceHolder.Appearance)
                {
                    if (null == this.editAppearanceHolder)
                    {
                        this.editAppearanceHolder = new AppearanceHolder();
                        this.editAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                    }

                    this.editAppearanceHolder.Appearance = value;                    
                    this.NotifyPropChange(GridCellProxyPropertyIds.EditAppearance);
                }
            }
        }

        /// <summary>
        /// Returns true if an edit appearance has been defined.
        /// </summary>        
        [Browsable(false)]
        public bool HasEditAppearance
        {
            get
            {
                return this.editAppearanceHolder != null && this.editAppearanceHolder.HasAppearance;
            }
        }

        /// <summary>
        /// Returns a Boolean value that determines whether the EditAppearance property is set to its default value.
        /// </summary>
        /// <returns>True if the value should be serialized.</returns>
        protected bool ShouldSerializeEditAppearance()
        {
            return this.HasEditAppearance && this.editAppearanceHolder.ShouldSerialize();
        }

        /// <summary>
        /// Resets the EditAppearance property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetEditAppearance()
        {
            if (this.HasEditAppearance)
            {
                this.editAppearanceHolder.Reset();
                this.NotifyPropChange(GridCellProxyPropertyIds.EditAppearance);
            }
        }
        #endregion //EditAppearance

        #region HotTrackAppearance

        /// <summary>
        /// Gets or sets the hot-tracked appearance of the control.
        /// </summary>
        /// <remarks>
        /// <p class="note"><b>Note: </b>These settings take precedence over appearances provided by the grid.</p>
        /// </remarks>
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridCellProxy_P_HotTrackAppearance")]
        public AppearanceBase HotTrackAppearance
        {
            get
            {
                if (this.hotTrackAppearanceHolder == null)
                {
                    this.hotTrackAppearanceHolder = new AppearanceHolder();
                    this.hotTrackAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }
                return this.hotTrackAppearanceHolder.Appearance;
            }
            set
            {
                if (this.hotTrackAppearanceHolder == null ||
                    !this.hotTrackAppearanceHolder.HasAppearance ||
                    value != this.hotTrackAppearanceHolder.Appearance)
                {
                    if (null == this.hotTrackAppearanceHolder)
                    {
                        this.hotTrackAppearanceHolder = new AppearanceHolder();
                        this.hotTrackAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                    }

                    this.hotTrackAppearanceHolder.Appearance = value;
                    this.NotifyPropChange(GridCellProxyPropertyIds.HotTrackAppearance);
                }
            }
        }

        /// <summary>
        /// Returns true if an appearance has been defined.
        /// </summary>        
        [Browsable(false)]
        public bool HasHotTrackAppearance
        {
            get
            {
                return this.hotTrackAppearanceHolder != null && this.hotTrackAppearanceHolder.HasAppearance;
            }
        }

        /// <summary>
        /// Returns a Boolean value that determines whether the HotTrackAppearance property is set to its default value.
        /// </summary>
        /// <returns>True if the value should be serialized.</returns>
        protected bool ShouldSerializeHotTrackAppearance()
        {
            return this.HasHotTrackAppearance && this.hotTrackAppearanceHolder.ShouldSerialize();
        }

        /// <summary>
        /// Resets the HotTrackAppearance property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetHotTrackAppearance()
        {
            if (this.HasHotTrackAppearance)
            {
                this.hotTrackAppearanceHolder.Reset();
                this.NotifyPropChange(GridCellProxyPropertyIds.HotTrackAppearance);
            }
        }
        #endregion //HotTrackAppearance

        #region IsActiveProxy

        /// <summary>
        /// Returns whether the proxy is currently being used to edit a cell in the grid.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public bool IsActiveProxy
        {
            get
            {
                return this.RowEditTemplate != null && this.RowEditTemplate.Band != null && 
                    this.RowEditTemplate.Band.Layout != null && this.RowEditTemplate.Band.Layout.ActiveProxy == this;
            }
        }
        #endregion //IsActiveProxy

        #region UseGridAppearances

        /// <summary>
        /// Gets or sets whether the underlying grid should resolve the appearance of the control.
        /// </summary>
        /// <remarks>
        /// <p class="note">
        /// <b>Note:</b> the <see cref="Appearance"/>, <see cref="EditAppearance"/>, and <see cref="HotTrackAppearance"/> 
        /// take precedence over the resolution of the underlying grid.
        /// </p>
        /// <p class="note"><b>Note: </b>This property will not change the appearance of the control at design-time.</p>
        /// </remarks>
        [DefaultValue(true)]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridCellProxy_P_UseGridAppearances")]
        public bool UseGridAppearances
        {
            get { return this.useGridAppearances; }
            set
            {
                if (this.useGridAppearances == value)
                    return;

                this.useGridAppearances = value;

                // MBS 5/19/08 - BR33114
                this.ComponentRole.ClearCachedPropertyValues();

                if (this.ControlUIElement != null)
                    this.ControlUIElement.DirtyChildElements();

                this.NotifyPropChange(GridCellProxyPropertyIds.UseGridAppearances);
            }
        }

        private bool UseGridAppearancesResolved
        {
            get
            {
                if (this.DesignMode)
                    return false;

                object val;

                if (false == this.ComponentRole.GetCachedProperty(UltraGridCellProxyRole.UseGridAppearences, out val))
                {
                    val = this.ComponentRole.CachePropertyValue(
                        UltraGridCellProxyRole.UseGridAppearences,
                        "UseGridAppearances",
                        this.UseGridAppearances,
                        true,
                        true);
                }
                return (bool)val;
            }
        }
        #endregion //UseGridAppearances

        #endregion //Public Properties

        #region Private/Internal Methods

        #region ApplyTextBoxAppearance

        private void ApplyTextBoxAppearance()
        {
            EditorWithText textEditor = this.Editor as EditorWithText;
            if (textEditor == null || !textEditor.IsInEditMode || !this.IsActiveProxy)
                return;

            AppearanceData appData = new AppearanceData();
            AppearancePropFlags flags = AppearancePropFlags.ForeColor |
                        AppearancePropFlags.BackColor |
                        AppearancePropFlags.Cursor |
                        AppearancePropFlags.TextHAlign |
                        AppearancePropFlags.TextVAlign |
                        AppearancePropFlags.FontData;

            this.ResolveAppearance(ref appData, ref flags);
            textEditor.TextBox.ApplyAppearance(appData);
        }

        private void ApplyTextBoxAppearance(PropChangeInfo propChangeInfo)
        {
            EditorWithText textEditor = this.Editor as EditorWithText;
            if (textEditor == null || !textEditor.IsInEditMode || !this.IsActiveProxy)
                return;

            AppearanceData appData = new AppearanceData();
            AppearancePropFlags flags = (AppearancePropFlags)0;

            if (propChangeInfo.FindPropId(AppearancePropIds.FontData) != null ||
                 propChangeInfo.FindPropId(AppearancePropIds.BackColor) != null ||
                 propChangeInfo.FindPropId(AppearancePropIds.ForeColor) != null ||
                 propChangeInfo.FindPropId(AppearancePropIds.Cursor) != null ||
                 propChangeInfo.FindPropId(AppearancePropIds.TextHAlign) != null ||
                 propChangeInfo.FindPropId(AppearancePropIds.TextVAlign) != null ||
                 propChangeInfo.FindPropId(Infragistics.Win.UltraWinEditors.PropertyID.Appearance) != null)
            {
                flags = AppearancePropFlags.ForeColor |
                        AppearancePropFlags.BackColor |
                        AppearancePropFlags.Cursor |
                        AppearancePropFlags.TextHAlign |
                        AppearancePropFlags.TextVAlign |
                        AppearancePropFlags.FontData;

                this.ResolveAppearance(ref appData, ref flags);
                textEditor.TextBox.ApplyAppearance(appData);
            }
        }
        #endregion //ApplyTextBoxAppearance

        #region Dirty

        internal void Dirty()
        {
            if (this.ControlUIElement != null)
                this.ControlUIElement.DirtyChildElements(true);
        }
        #endregion //Dirty

        #region OnEditorKeyDown

        private void OnEditorKeyDown(object sender, KeyEventArgs e)
        {
            this.OnKeyDown(e);
            if (e.Handled)
                return;

            if (e.KeyData == Keys.Tab ||
                e.KeyData == (Keys.Tab | Keys.Shift))
            {
                if (this.Editor.IsInEditMode)
                {
                    if (!this.Editor.ExitEditMode(false, true))
                        return;
                }

                bool forward = true;
                if (((Control.ModifierKeys & Keys.Shift) == Keys.Shift))
                    forward = false;

                this.SetFocusToNextControl(forward);
                return;
            }

            IEmbeddableTextBoxListener textBox = this.Editor as IEmbeddableTextBoxListener;
            if (textBox != null)
                textBox.OnKeyDown(e);
        }
        #endregion //OnEditorKeyDown

        #region OnRowChanged

        private void OnRowChanged(object sender, RowEditTemplateRowChangedEventArgs e)
        {
            // Unhook the previous editor
            if (this.cachedEditor != null)
            {
                if (this.haveHookedEditor)
                {
                    this.cachedEditor.KeyDown -= this.EditorKeyDownHandler;
                    this.haveHookedEditor = false;
                }
                this.cachedEditor = null;
            }
            else
                // Make sure that this flag is false so that we can hook the editor later.
                this.haveHookedEditor = false;

            if (this.ControlUIElement != null)
            {
                this.ControlUIElement.VerifyChildElements(true);
                this.ControlUIElement.DirtyChildElements(true);
            }
        }
        #endregion //OnRowChanged

        #region OnSubObjectPropChanged

        private void OnSubObjectPropChanged(PropChangeInfo propChange)
        {
            this.ApplyTextBoxAppearance(propChange);

            if (this.HasAppearance && propChange.Source == this.Appearance)
            {
                this.SyncBackColor();

                if(this.ControlUIElement != null)
                    this.ControlUIElement.DirtyChildElements(true);                

                this.NotifyPropChange(GridCellProxyPropertyIds.Appearance, propChange);
            }
            else if (this.HasEditAppearance && propChange.Source == this.EditAppearance)
            {                
                if (this.ControlUIElement != null)
                    this.ControlUIElement.DirtyChildElements();

                this.NotifyPropChange(GridCellProxyPropertyIds.EditAppearance, propChange);
            }
        }
        #endregion //OnSubObjectPropChanged

        #region SyncBackColor

        private void SyncBackColor()
        {
            base.BackColor = Infragistics.Win.Utilities.ResolveControlBackColor(
                StyleUtils.GetRole(this, StyleUtils.CellProxyRole.CellProxy),
                ComponentRole.GetResolutionOrderInfo(this.ComponentRole),
                this.HasAppearance ? this.Appearance : null,
                this.backColorInternal);
        }
        #endregion //SyncBackColor

        #endregion //Private/Internal Methods

        #region Protected/Public Methods

        #region OnAppStyleChanged

        /// <summary>
        /// Fired when a property changes on the associated role.
        /// </summary>
        internal protected void OnAppStyleChanged()
        {
            this.SyncBackColor();
            this.ApplyTextBoxAppearance();
            
            if (this.IsHandleCreated && this.ControlUIElement != null)
                this.ControlUIElement.DirtyChildElements();
        }
        #endregion //OnAppStyleChanged

        #region ResolveAppearance

        /// <summary>
        /// Resolves the control's appearance.
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize.</param>
        /// <param name="requestedProps">The appearance properties to resolve.</param>
        public void ResolveAppearance(ref AppearanceData appearance, ref AppearancePropFlags requestedProps)
        {
            this.ResolveAppearance(this.OwnerContext, ref appearance, ref requestedProps, EmbeddableEditorArea.Default, this.IsHotTracking, String.Empty); 
        }
        
        internal bool ResolveAppearance(object ownerContext, ref AppearanceData appearance, ref AppearancePropFlags requestedProps, EmbeddableEditorArea area, bool hotTracking, string customArea)
        {
            if(area == EmbeddableEditorArea.Button)
                return false;

            bool isInEditMode = this.Editor != null && this.Editor.IsInEditMode && this.Row != null && this.Row.Layout.ActiveProxy == this;
            AppStyling.UIRole role = this.ControlUIElement.UIRoleResolved;
            AppStyling.ResolutionOrderInfo order = ComponentRole.GetResolutionOrderInfo(this.ComponentRole);

            if (order.UseStyleBefore && role != null)
            {
                if (hotTracking)
                    role.ResolveAppearance(ref appearance, ref requestedProps, RoleState.HotTracked);

                if (isInEditMode)
                    role.ResolveAppearance(ref appearance, ref requestedProps, RoleState.EditMode);

                role.ResolveAppearance(ref appearance, ref requestedProps, RoleState.Normal);
            }

            if (order.UseControlInfo)
            {
                if (hotTracking && this.HasHotTrackAppearance)
                    this.HotTrackAppearance.MergeData(ref appearance, ref requestedProps);

                if (isInEditMode && this.HasEditAppearance)
                    this.EditAppearance.MergeData(ref appearance, ref requestedProps);

                if (this.HasAppearance)
                    this.Appearance.MergeData(ref appearance, ref requestedProps);
            }

            if (order.UseStyleAfter && role != null)
            {
                if (hotTracking)
                    role.ResolveAppearance(ref appearance, ref requestedProps, RoleState.HotTracked);

                if (isInEditMode)
                    role.ResolveAppearance(ref appearance, ref requestedProps, RoleState.EditMode);

                role.ResolveAppearance(ref appearance, ref requestedProps, RoleState.Normal);
            }

            // Have the base owner resolve the appearance, but only if we have an owner context, since if we don't
            // that means that we're not currently associated with a row anyway and the owner will not have any
            // releavant information to provide.
            if (ownerContext != null && this.BaseOwner != null && this.UseGridAppearancesResolved)
            {
                // If we can get to the grid owner, then we should make sure that it's aware that we're resolving
                // a proxy's appearance so that we can ignore certain states, like ActiveRow and Selected
                GridEmbeddableEditorOwnerInfoBase gridOwner = this.BaseOwner as GridEmbeddableEditorOwnerInfoBase;
                if(gridOwner != null)
                    gridOwner.ResolveAppearance(ownerContext, ref appearance, ref requestedProps, area, hotTracking, customArea, true);
                else
                    this.BaseOwner.ResolveAppearance(ownerContext, ref appearance, ref requestedProps, area, hotTracking, customArea);
            }

            return true;
        }
        #endregion //ResolveAppearance

        #endregion //Protected/Public Methods

        #region Base Class Overrides

        #region BackColor

        /// <summary>
        /// BackColor property - use the BackColor property of the control's <see cref="Appearance"/> instead.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set
            {
                base.BackColor = value;
                this.Appearance.BackColor = value;
            }
        }
        #endregion //BackColor

        #region BackgroundImage

        /// <summary>
        /// This property is not used by the UltraGridCellProxy
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Image BackgroundImage
        {
            get
            {
                return base.BackgroundImage;
            }
            set
            {
                base.BackgroundImage = value;
            }
        }
        #endregion //BackgroundImage

        #region BaseOwner

        /// <summary>
        /// Returns the owner of the underlying column.
        /// </summary>        
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override EmbeddableEditorOwnerBase BaseOwner
        {
            get { return this.Column != null ? this.Column.EditorOwnerInfo : null; }
        }
        #endregion //BaseOwner        

        #region ControlUIElement

        /// <summary>
        /// Returns the main control UIElement.
        /// </summary>
        protected override ControlUIElementBase ControlUIElement
        {
            get
            {
                if (this.mainElement == null)
                    this.mainElement = new UltraGridCellProxyUIElement(this);

                return this.mainElement;
            }
        }
        #endregion //ControlUIElement

        #region CreateComponentRole

        /// <summary>
        /// Factory method used to create the component role that provides the style information for the control.
        /// </summary>
        /// <returns>The <see cref="AppStyling.ComponentRole"/> created.</returns>
        protected override ComponentRole CreateComponentRole()
        {
            return new UltraGridCellProxyRole(this);
        }
        #endregion //CreateComponentRole

        #region Editor

        /// <summary>
        /// Returns the editor that the control is hosting as a proxy.
        /// </summary>
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b>If the proxy is unable to access an underlying row, such as if
        /// the template isn't shown or the RowEditTemplate isn't associated with a band,
        /// a default editor will be returned to display the disabled control and to 
        /// show a message at design-time.
        /// </p>
        /// </remarks>
        [Browsable(false)]
        public override EmbeddableEditorBase Editor
        {
            get
            {
                if (this.Column != null)
                {
                    if (this.cachedEditor != null)
                        return this.cachedEditor;

                    // At design-time we always want to show an EditorWithText
                    // because we want to provide the user with the ability 
                    // to see which proxy matches which control, for ease of layout
                    if (this.DesignMode)
                        return this.ProxyEditorWithText;

                    if (this.Row != null)
                    {
                        // Cache the editor so that when we get a RowChanged notification,
                        // we can unhook from the previous editor, since it could be different
                        this.cachedEditor = this.Row.Cells[this.Column].EditorResolved;
                        return this.cachedEditor;
                    }
                    else
                    {
                        // Return the column's editor, though in this case we will prevent the user
                        // from entering edit mode since we don't have an actual row (and therefore
                        // we don't have any data to edit)                        
                        return this.Column.Editor;
                    }
                }

                // At this point if we can't get a valid editor, we need to provide an editor
                // that is used solely to display a disabled control
                return this.ProxyEditorWithText;
            }
        }
        #endregion //Editor
        
        #region OnCreateControl

        /// <summary>
        /// Called after the control is created
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            this.SyncBackColor();
        }
        #endregion //OnCreateControl

        #region OnEnter
        
        /// <summary>
        /// Called after the control is entered
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected override void OnEnter(EventArgs e)
        {
            if (this.isInOnEnter || this.Row == null)
                return;

            // If there is already an active proxy, we should bail out of 
            // here.  The only case that this should really happen is if
            // an editor fails to go out of edit mode.  If we don't bail out
            // we will try to cause the editor to exit edit mode several times
            if (!this.IsActiveProxy && this.Row.Layout.ActiveProxy != null)
            {
                // In the case where the user sets the CausesValidation property
                // to false, we will not be able to maintain focus in the control,
                // so the user could click on another control, causing it to 
                // try to capture focus.  Even if we bail out of the enter/gotfocus
                // early, we'll still get the OnLeave event, so we need to keep 
                // a flag to tell us not to call the base OnLeave, which will try 
                // to cause the underlying editor to exit edit mode again.
                if (!this.CausesValidation || !this.Row.Layout.ActiveProxy.CausesValidation)
                {
                    this.isSelecting = true;
                    this.Row.Layout.ActiveProxy.Select();
                    this.isSelecting = false;
                }

                return;
            }

            try
            {
                this.isInOnEnter = true;

                if (this.RowEditTemplate != null)
                {
                    // If we couldn't process the required logic for putting the proxy
                    // into edit mode, we should bail out here
                    if (this.RowEditTemplate.OnProxyEnter(this) == false)
                    {
                        this.didProxyEnterFail = true;
                        return;
                    }
                    else
                        this.didProxyEnterFail = false;
                }

                // We need to keep track of whether we've already hooked the KeyDown
                // event of the editor, since we could get multiple OnEnter notifications
                // without an OnLeave notification between them (such as when using an
                // editor that positions a TextBox over itself).
                //
                // NOTE: We have to hook the editor's events here as opposed to when we
                // create/cache the editor because multiple grid cells could be using the
                // same editor, so we would receive multiple notifications of the same action.
                if (!this.haveHookedEditor && this.Editor != null)
                {
                    this.Editor.KeyDown += this.EditorKeyDownHandler;
                    this.haveHookedEditor = true;
                }

                base.OnEnter(e);
            }
            finally
            {
                this.isInOnEnter = false;
            }
        }
        #endregion //OnEnter

        #region OnGotFocus

        /// <summary>
        /// Called after the control receives focus
        /// </summary>
        protected override void OnGotFocus(EventArgs e)
        {
            // If the row is null, then we won't be able to edit anyway, so just do nothing.
            if (this.Row == null ||
                // If there is already an active proxy, we should bail out of 
                // here.  The only case that this should really happen is if
                // an editor fails to go out of edit mode.  If we don't bail out
                // we will try to cause the editor to exit edit mode several times
                (!this.IsActiveProxy && this.Row.Layout.ActiveProxy != null))
                return;

            UltraGridCell activeCell = this.RowEditTemplate.Grid.ActiveCell;
            if (activeCell != null && activeCell.IsInEditMode && 
                // The IsInEditMode property will return true even if we're editing with the
                // template, so we also need to check the layout.
                activeCell.Layout.EmbeddableElementBeingEdited != null)
            {
                Debug.Fail("A grid cell was in edit mode while the template is shown; the end is nigh!");
                activeCell.ExitEditMode();
                if (activeCell.IsInEditMode)
                    return;
            }

            // We could get focus without the OnEnter actually being called, such as in a modeless template
            // where the grid has focus, then the user clicks directly on the cell.  Call this method to make
            // sure that we update the active cell as well as hooking the editor.
            if (this.RowEditTemplate != null)
            {
                if (this.didProxyEnterFail)
                {
                    // Reset the flag since we could get an OnGotFocus again without getting an Enter
                    this.didProxyEnterFail = false;
                    return;
                }
                else
                {
                    // If we couldn't process the required logic for putting the proxy
                    // into edit mode, we should bail out here.  We don't reset the flag
                    // for didProxyEnterFail, since the conditions could change across when the 
                    // user clicks into the proxy
                    if (this.RowEditTemplate.OnProxyEnter(this) == false)
                        return;
                }
            }

            base.OnGotFocus(e);
        }
        #endregion //OnGotFocus

        #region OnLeave

        /// <summary>
        /// Called after the control loses focus
        /// </summary>
        /// <param name="e">EventArgs</param>
        protected override void OnLeave(EventArgs e)
        {
            if (this.isSelecting)
                return;

            base.OnLeave(e);

            // The base implementation will cause the editor to exit edit mode as necessary,
            // but there are cases where we'll get an OnLeave notification without exiting
            // edit mode, such as a MessageBox for an invalid entry.  If this is the case,
            // we shouldn't unhook the editor or reset the ActiveProxy since it's still being
            // used.
            if (this.Editor != null && this.Editor.IsInEditMode && this.IsActiveProxy)
                return;

            if (this.IsActiveProxy)
                this.Row.Layout.ActiveProxy = null;

            // Unhook the editor as necessary.  See the note in the OnEnter override
            // for why we need to hook/unhook in Enter/Leave, respectively
            if (this.haveHookedEditor)
            {
                this.Editor.KeyDown -= this.EditorKeyDownHandler;
                this.haveHookedEditor = false;
            }
        }
        #endregion //OnLeave

        #region OnMouseWheel

        /// <summary>
        /// Fires when the mouse wheel is used when the cursor is positioned over the control
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);
            
            // MBS 4/8/09 - TFS16447
            // We need to do the same thing that the grid does and forward the MouseWheel message
            // to any controls that need to listen to it
            if (this.RowEditTemplate != null && this.RowEditTemplate.Grid != null)
            {
                this.RowEditTemplate.Grid.OnMouseWheelInternal(e);
            }
        }
        #endregion //OnMouseWheel

        #region OnPaint

        /// <summary>
        /// Calls the ControlUIElement's draw method
        /// </summary>
        /// <param name="pe">The paint EventArgs.</param>
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            // We can't set focus to the control until we get a paint message, since the child elements
            // might not have been created yet
            if (this.RowEditTemplate != null && this.RowEditTemplate.activeCellOnDisplay != null &&
                this.RowEditTemplate.activeCellOnDisplay.Column.Key == this.ColumnKey)
            {
                if(this.Visible)
                    this.RowEditTemplate.FocusActiveProxy(this.RowEditTemplate.activeCellOnDisplay);

                this.RowEditTemplate.activeCellOnDisplay = null;
            }
        }
        #endregion //OnPaint

        #region Owner

        /// <summary>
        /// Returns the proxy owner used to communicate between the control and the underlying owner.
        /// </summary>
        protected override UltraEditorProxyOwnerBase Owner
        {
            get
            {
                if (this.owner == null)
                    this.owner = new UltraGridCellProxyOwnerInfo(this);

                return this.owner;
            }
        }
        #endregion //Owner

        #region OwnerContext

        /// <summary>
        /// Returns the row that is currently being edited by the template, or null if no row is being edited.
        /// </summary>
        protected override object OwnerContext
        {
            get
            {
                return this.Row;
            }
        }
        #endregion //OwnerContext                

        #region Text

        /// <summary>
        /// Overriden.  Returns the editor's value as a string.
        /// </summary>
        [Bindable(false)]
        [Browsable(false)]        
        public override string Text
        {
            get
            {
                object val = this.Owner.GetValue(this.OwnerContext);
                if (val != null)
                    return val.ToString();

                return string.Empty;
            }
        }
        #endregion //Text

        #endregion //Base Class Overrides
    }
    #endregion //UltraGridCellProxy

    // MBS 3/24/09 - TFS15730
    // Needed a derived error icon class so that we can resolve the appearance manually
    #region ProxyDataErrorIconUIElement

    /// <summary>
    /// A derived class for displaying the data error icon within an UltraGridCellProxy.
    /// </summary>
    public class ProxyDataErrorIconUIElement : DataErrorIconUIElement
    {        
        /// <summary>
        /// Initializes a new instance of the element.
        /// </summary>
        /// <param name="parent">The parent proxy element.</param>
        public ProxyDataErrorIconUIElement(UltraGridCellProxyUIElement parent)
            : base(parent)
        {
        }

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance, ref AppearancePropFlags requestedProps)
        {
            // Give the control itself the first crack at resolving the appearances
            ((UltraGridCellProxy)this.Control).ResolveAppearance(ref appearance, ref requestedProps);

            UltraGridRow row = this.GetContext(typeof(UltraGridRow)) as UltraGridRow;
            UltraGridColumn column = this.GetContext(typeof(UltraGridColumn)) as UltraGridColumn;
            if (row != null && column != null)
                // In a normal cell, the data error icon will receive the default appearance data that the cell has been
                // drawn with by default, since the cell is the parent of the icon.  In this case, the icon won't
                // get these default appearances, so we need to resolve them ourselves.
                row.Layout.ResolvedCachedCellAppearance(row, column, ref appearance, ref requestedProps, false, false);            

            base.InitAppearance(ref appearance, ref requestedProps);
        }
    }
    #endregion //ProxyDataErrorIconUIElement

    #region UltraGridCellProxyUIElement

    /// <summary>
    /// The main UIElement for the UltraGridCellProxy control.
    /// </summary>
    public class UltraGridCellProxyUIElement : UltraEditorProxyUIElement
    {
        #region Constructor

        /// <summary>
        /// Constructor, initializes a new instance of the element.
        /// </summary>
        /// <param name="proxy">Owning control</param>
        public UltraGridCellProxyUIElement(UltraGridCellProxy proxy)
            : base(proxy)
        {
        }
        #endregion //Constructor

        #region Base Class Overrides

        #region BorderStyle

        /// <summary>
        /// Returns the border style
        /// </summary>
        public override UIElementBorderStyle BorderStyle
        {
            get
            {
                return ((UltraGridCellProxy)this.Control).BorderStyleResolved;
            }
        }
        #endregion //BorderStyle

        // MBS 3/24/09 - TFS15730
        #region GetContext

        /// <summary>
        /// Returns an object of requested type that relates to the element or null.  
        /// </summary>
        /// <param name="type">The requested type or null to pick up default context object. </param>
        /// <param name="checkParentElementContexts">If true will walk up the parent chain looking for the context.</param>
        /// <returns><see cref="Object"/></returns>
        public override object GetContext(Type type, bool checkParentElementContexts)
        {
            UltraGridCellProxy proxy = this.Control as UltraGridCellProxy;
            if (type == typeof(UltraGridRow))
            {
                if (proxy != null && proxy.RowEditTemplate != null)
                    return proxy.RowEditTemplate.Row;
            }
            else if(type == typeof(UltraGridColumn))
                return proxy.Column;

            return base.GetContext(type, checkParentElementContexts);
        }
        #endregion //GetContext

        #region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance, ref AppearancePropFlags requestedProps)
        {
            ((UltraGridCellProxy)this.Control).ResolveAppearance(ref appearance, ref requestedProps);

            base.InitAppearance(ref appearance, ref requestedProps);
        }
        #endregion //InitAppearance

        // MBS 3/24/09 - TFS15730
        // Override to allow us to position a DataErrorIcon, if applicable
        #region PositionChildElementsHelper

        /// <summary>
        /// Positions any additional child elements after the main embeddable element has been positioned.
        /// </summary>
        /// <param name="oldChildElements">The old child elements collection from the previous positioning logic.</param>
        protected override void PositionChildElementsHelper(UIElementsCollection oldChildElements)
        {            
            UltraGridCellProxy proxy = this.Control as UltraGridCellProxy;
            UltraGridRow row = this.GetContext(typeof(UltraGridRow)) as UltraGridRow;
            UltraGridColumn column = this.GetContext(typeof(UltraGridColumn)) as UltraGridColumn;

            // Don't try to position anything if we failed to get a UIElement in the base
            if (this.lastElement == null || proxy == null || row == null)
                return;

            bool hasDataErrorIcon = row.HasDataError(column);
            if (hasDataErrorIcon)
            {
                ProxyDataErrorIconUIElement dataErrorIconElement =
                    (ProxyDataErrorIconUIElement)CellUIElement.ExtractExistingElement(oldChildElements, typeof(ProxyDataErrorIconUIElement), true);

                if (dataErrorIconElement == null)
                    dataErrorIconElement = new ProxyDataErrorIconUIElement(this);

                Rectangle textRect = this.lastElement.Rect;
                CellUIElement.PositionDataErrorIcon(column, row, this, this.ChildElements, dataErrorIconElement, ref textRect);

                // In case the rect has been adjusted for the data icon, reassign it to the element
                this.lastElement.Rect = textRect;
            }
        }
        #endregion //PositionChildElementsHelper

        #region UIRole

        /// <summary>
        /// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
        /// </summary>
        public override Infragistics.Win.AppStyling.UIRole UIRole
        {
            get
            {
                return StyleUtils.GetRole((UltraGridCellProxy)this.Control, StyleUtils.CellProxyRole.CellProxy);
            }
        }
        #endregion //UIRole

        #endregion //Base Class Overrides
    }
    #endregion //UltraGridCellProxyUIElement

    #region UltraGridCellProxyOwnerInfo

    /// <summary>
    /// A class that allows a proxy to delegate many owner-related tasks to the the proxy's underlying owner.
    /// </summary>
    public class UltraGridCellProxyOwnerInfo : UltraEditorProxyOwnerBase
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="editorProxy">The proxy editor with which this should be associated.</param>
        public UltraGridCellProxyOwnerInfo(UltraGridCellProxy editorProxy)
            : base(editorProxy)
        {
        }
        #endregion //Constructor

        #region Properties

        #region GridCellProxy

        private UltraGridCellProxy GridCellProxy
        {
            get { return this.EditorProxy as UltraGridCellProxy; }
        }
        #endregion //GridCellProxy

        #endregion //Properties

        #region Methods

        #region GetBaseButtonStyle

        internal void GetBaseButtonStyle(object ownerContext, out UIElementButtonStyle buttonStyle)
        {
            base.GetButtonStyle(ownerContext, out buttonStyle);
        }
        #endregion //GetBaseButtonStyle

        #endregion //Methods

        #region Base Class Overrides

        #region GetBorderStyle

        /// <summary>
        /// Returns the BorderStyle to be used by the editor's embeddable element
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <param name="borderStyle">The BorderStyle to be used by the editor's embeddable element</param>
        /// <returns>The border style to be used by the editor's embeddable element.</returns>
        public override bool GetBorderStyle(object ownerContext, out UIElementBorderStyle borderStyle)
        {
            // The proxy UIElement will draw the borders, not the editor
            borderStyle = UIElementBorderStyle.None;
            return true;
        }
        #endregion //GetBorderStyle

        #region GetButtonStyle

        /// <summary>
        /// Returns the ButtonStyle to be used by the embeddable element's button
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <param name="buttonStyle">The ButtonStyle to be used by the embeddable element's buttons</param>
        /// <returns>True if a non-default value is returned.</returns>
        public override bool GetButtonStyle(object ownerContext, out UIElementButtonStyle buttonStyle)
        {
            buttonStyle = this.GridCellProxy.ButtonStyleResolved;
            return true;
        }
        #endregion //GetButtonStyle

        #region DrawAsActive

        /// <summary>
        /// Returns whether the element should be drawn as if it were in its "active" state. Only applicable if the return from the DisplayStyle property is not Standard.
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>True if the element should be drawn as active.</returns>
        public override DefaultableBoolean DrawAsActive(object ownerContext)
        {
            ControlUIElementBase proxyElement = this.ProxyElement;
            if (proxyElement == null)
                return base.DrawAsActive(ownerContext);

            // We can't directly delegate to the grid's owner implementation because it
            // is expecting the owner context to be a UIElement (i.e. the cell or row element)
            // so that it can perform checks against the scroll regions and such.  Since we 
            // have an UltraGridRow as the owner context, instead we'll get the embeddable element
            // and pass that off to the grid's implementation.
            EmbeddableUIElementBase element = proxyElement.GetDescendant(typeof(EmbeddableUIElementBase)) as EmbeddableUIElementBase;
            if (element != null)
                return base.DrawAsActive(element);

            return base.DrawAsActive(ownerContext);
        }
        #endregion //DrawAsActive

        #region GetValue

        /// <summary>
        /// Returns the value that should be rendered.
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>The <see cref="Object"/> to be rendered.</returns>
        public override object GetValue(object ownerContext)
        {
            // Make sure that we can actually access the band and column that the proxy is assocaited with
            if (this.GridCellProxy.Band == null || this.GridCellProxy.Band.Columns.Exists(this.GridCellProxy.ColumnKey) == false)
            {
                // We only want to show an error message at design-time, since at run-time we just want to show a disabled editor
                if (this.DesignMode)
                    return SR.GetString("LE_UltraGridCellProxy_DesignTime_NoBandOrColumn");

                return String.Empty;
            }
            // If the proxy is hooked up correctly, we want to show the key of the column at design-time so that the 
            // user will have an easier time of positioning the controls
            else if (this.DesignMode)
                return this.GridCellProxy.ColumnKey;

            return base.GetValue(ownerContext);
        }
        #endregion //GetValue

        #region IsEnabled

        /// <summary>
        /// Returns whether the value is enabled for editing.
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>True if the value is enabled for editing.</returns>
        public override bool IsEnabled(object ownerContext)
        {
            if (this.GridCellProxy == null)
            {
                Debug.Fail("Expected an UltraGridCellProxy");
                return false;
            }
            
            if (this.GridCellProxy.Enabled == false ||
                this.GridCellProxy.RowEditTemplate == null || this.GridCellProxy.RowEditTemplate.Row == null ||
                this.GridCellProxy.Band == null || this.GridCellProxy.Band.Columns.Exists(this.GridCellProxy.ColumnKey) == false)
                return false;                

            // We can't directly delegate to the grid's owner implementation because it
            // is expecting the owner context to be a UIElement (i.e. the cell or row element)
            // so that it can perform checks against the scroll regions and such.  Since we 
            // have an UltraGridRow as the owner context, we will perform the relevant logic here.
            UltraGridColumn col = this.GridCellProxy.Band.Columns[this.GridCellProxy.ColumnKey];
            return this.GridCellProxy.RowEditTemplate.Row.GetCellActivationResolved(col) != Activation.Disabled;
        }
        #endregion //IsEnabled

        #region IsKeyMapped

        /// <summary>
        /// Returns whether the key is used by the owner.
        /// </summary>
        /// <param name="keyData"></param>
        /// <param name="element">The EmbeddableUIElementBase-derived element</param>
        /// <returns>True if the key is used by the owner (e.g. an arrow or tab key used for internal navigation).</returns>
        public override bool IsKeyMapped(Keys keyData, EmbeddableUIElementBase element)
        {
            // MBS 5/29/09 - TFS18031
            // We always want to ensure that pressing the Escape key will close the template, 
            // and this should override what the control/editor wants to do with the key.
            if (keyData == Keys.Escape)
                return false;

            return base.IsKeyMapped(keyData, element);
        }
        #endregion //IsKeyMapped

        #region IsHotTrackingEnabled

        /// <summary>
        /// Returns whether "hot tracking" effects should be enabled
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>The default implementation returns <b>true</b>.</returns>
        public override bool IsHotTrackingEnabled(object ownerContext)
        {
            return true;
        }
        #endregion //IsHotTrackingEnabled

        #region OnEditorBeforeEnterEditMode

        /// <summary>
        /// Called before edit mode has been entered. 
        /// </summary>
        /// <param name="editor">The editor about to enter edit mode.</param>
        /// <param name="element">The element entering edit mode.</param>
        /// <returns>False to cancel the operation.</returns>
        protected override bool OnEditorBeforeEnterEditMode(EmbeddableEditorBase editor, EmbeddableUIElementBase element)
        {
            // We can't directly delegate to the grid's owner implementation because it
            // is expecting the owner context to be a UIElement (i.e. the cell or row element)
            // so that it can perform checks against the scroll regions and such.  Since we 
            // have an UltraGridRow as the owner context, we will perform the relevant logic here.
            UltraGridCellProxy gridCellProxy = this.EditorProxy as UltraGridCellProxy;
            if (gridCellProxy == null)
            {
                Debug.Fail("Expected an UltraGridCellProxy");
                return base.OnEditorBeforeEnterEditMode(editor, element);
            }

            if (gridCellProxy.RowEditTemplate.Row.Cells.Exists(gridCellProxy.ColumnKey) == false)
                return false;

            UltraGridCell cell = gridCellProxy.RowEditTemplate.Row.Cells[gridCellProxy.ColumnKey];
            if (!cell.Column.CanCellEnterEditMode(cell.Row, true))
                return false;

            return true;
        }
        #endregion //OnEditorBeforeEnterEditMode

        #region ResolveAppearance

        /// <summary>
        /// Resolves the appearance for an element.
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <param name="appearance">The appearance structure to initialize.</param>
        /// <param name="requestedProps">The appearance properties to resolve.</param>
        /// <param name="area">Enumeration of type <see cref="EmbeddableEditorArea"/> describing the area of the embeddable element to which the appearance will be applied</param>
        /// <param name="hotTracking">Boolean indicating whether the owner should apply its 'HotTrackingAppearance'</param>
        /// <param name="customArea">A string that denotes which appearance to resolve. Applicable only when the 'area' parameter is set to Custom.</param>
        /// <returns>True if the owner recognizes and supports the named appearance.</returns>        
        public override bool ResolveAppearance(object ownerContext, ref AppearanceData appearance, ref AppearancePropFlags requestedProps, EmbeddableEditorArea area, bool hotTracking, string customArea)
        {            
            // MBS 3/24/09 - TFS15730
            // Instead of relying on the element's knowledge of whether it's hot-tracked, ask the control.  This is because we might have
            // elements, such as the data error icon, that need to be positioned as siblings of the embeddable editor's element out of 
            // the nature of an editor proxy (i.e. we can't make the icon a child of the element), so the embeddable element will
            // only know whether the mouse is over itself, which can lead to odd hot-tracking. 
            //
            if(!hotTracking)
                hotTracking = this.GridCellProxy.IsHotTracking;

            if (false == this.GridCellProxy.ResolveAppearance(ownerContext, ref appearance, ref requestedProps, area, hotTracking, customArea))            
                return base.ResolveAppearance(ownerContext, ref appearance, ref requestedProps, area, hotTracking, customArea);

            // Return false here so that remaining items can be resolved
            return false;
        }
        #endregion //ResolveAppearance

        #region SuppressEnterLeaveInvalidation

        /// <summary>
        /// By default the editors will invalidate the embeddable editor elements as the mouse enters and leaves
        /// them. This method can be used to suppress this behavior. Default implementation returns false.
        /// </summary>
        /// <param name="ownerContext">The owner context, used to provide additional information.</param>
        /// <returns>True if the editor's enter/leave notification should be suppressed.</returns>
        public override bool SuppressEnterLeaveInvalidation(object ownerContext)
        {
            

            // Since the control allows Hot-Tracking, we don't want to prevent this from happening just because
            // a grid cell doesn't allow hot-tracking
            return false;
        }
        #endregion //SuppressEnterLeaveInvalidation

        #endregion //Base Class Overrides
    }
    #endregion //UltraGridCellProxyOwnerInfo
}
