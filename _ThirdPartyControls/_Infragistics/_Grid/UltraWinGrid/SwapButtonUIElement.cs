#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

	
	
	/// <summary>
	/// A column or group swap button UI element
	/// </summary>
	public class SwapButtonUIElement : UIElement
	{
		// SSP 3/29/02
		// We were using 9 (literal 9) all over the place for swap drop down button
		// width. Made it a static readonly variable.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//internal static readonly int SWAP_DROPDOWN_BUTTON_WIDTH = 9;
		internal const int SWAP_DROPDOWN_BUTTON_WIDTH = 9;

		private bool droppedDown = false;
		private bool inMouseDown = false;

//#if DEBUG
//		/// <summary>
//		/// Constructor.
//		/// </summary>
//		/// <param name="parent">The parent element</param>
//#endif
		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>SwapButtonUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		//internal SwapButtonUIElement( UIElement parent ) : base( parent )
		public SwapButtonUIElement( UIElement parent ) : base( parent )
		{
		}

		/// <summary>
		/// Returns or sets a value that determines the border style of an object.
		/// </summary>
		/// <remarks>
		/// <para>The border style of cells, rows, and headers can be set by the BorderStyleCell, BorderStyleRow, and BorderStyleHeader properties respectively.</para>
		/// <para>The border style of the AddNew box buttons can be set by the ButtonBorderStyle property.</para>
		/// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
		/// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				HeaderBase header = (HeaderBase)this.Parent.GetContext( typeof( HeaderBase ) );
				UltraGrid grid = null;
				if ( null != header )
					grid = header.Band.Layout.Grid as UltraGrid;
				
				if ( null != grid )
				{					
					this.droppedDown = ((IValueList)grid.SwapDropDown).IsDroppedDown &&
						// RobA 1/2/02 added check to make sure the owner is this header so
						// that only the button that belongs to this owner is drawn InsetSoft
						//
						                 grid.SwapDropDown.Owner == header;
				}

				UIElementBorderStyle borderStyle = UIElementBorderStyle.None;

				bool mouseOverBtn = false;
								
				Point mousePosition = this.Control.PointToClient( Control.MousePosition );				
			
				if ( this.PointInElement( mousePosition ) )
					mouseOverBtn = true;

				if ( this.droppedDown  ||
					 this.inMouseDown )
				{		
					borderStyle = UIElementBorderStyle.InsetSoft;
				}
				else if ( mouseOverBtn )
				{
					borderStyle = UIElementBorderStyle.RaisedSoft;
				}
				return borderStyle;
			}
		}

		

		/// <summary>
		/// Draws small arrow in the foreground
		/// </summary>
		protected override void DrawForeground ( ref UIElementDrawParams drawParams ) 
		{
			
			// JJD 11/29/01
			// Moved this logic from DrawBackColor and fixed the width and
			// height of the arrow that is drawn.
			//
			Rectangle thisRect = this.rectValue;
			
			int w = 5;
			int h = 3; 

			Point[] points = new Point[3];
			points[0] = new Point( 0, 0 );
			points[1] = new Point( w, 0 );
			points[2] = new Point( w/2, h );
			
			int dx = thisRect.Left + ( ( thisRect.Width - w) / 2 );
			int dy = thisRect.Top + ( ( thisRect.Height + 1 - h ) ) / 2;
			points[0].Offset( dx, dy );
			points[1].Offset( dx, dy );
			points[2].Offset( dx, dy );
			
			// AS 12/19/02 UWG1831
			// Create a brush since the color may need to have the alpha channel set.
			//
			//System.Drawing.Brush brush = System.Drawing.SystemBrushes.FromSystemColor( 
			//	this.Control.Enabled ? SystemColors.ControlText : SystemColors.GrayText );
			// SSP 1/12/04 UWG2742
			// Use the fore color of the header instead of using a fixed color since if the 
			// back color of the header is set by the user to ControlText or GrayText, then that
			// will cause the arrow to not show up.
			//
			// ----------------------------------------------------------------------------------
			//Color color = this.Control.Enabled ? SystemColors.ControlText : SystemColors.GrayText;
			AppearanceData appData = new AppearanceData( );
			HeaderBase header = (HeaderBase)this.Parent.GetContext( typeof( HeaderBase ) );
			if ( null != header )
				header.ResolveAppearance( ref appData, AppearancePropFlags.ForeColor | AppearancePropFlags.ForeColorDisabled );

			if ( ! appData.HasPropertyBeenSet( AppearancePropFlags.ForeColor ) )
				appData.ForeColor = SystemColors.ControlText;
			if ( ! appData.HasPropertyBeenSet( AppearancePropFlags.ForeColorDisabled ) )
				appData.ForeColorDisabled = SystemColors.GrayText;

			Color color = this.Control.Enabled ? appData.ForeColor : appData.ForeColorDisabled;
			// ----------------------------------------------------------------------------------

			System.Drawing.Brush brush = drawParams.CreateSolidBrush(color);

			drawParams.Graphics.FillPolygon( brush, points );

			if (brush != null)
				brush.Dispose();
		}

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{			
		}


		/// <summary>
		/// this element doesn't draw an image background
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{			
		}



		/// <summary>
		/// does nothing as SwapButtonUIElement does not get a focus
		/// </summary>
		protected override void DrawFocus ( ref UIElementDrawParams drawParams )
		{			
			// doesn't need a focus
		}
		
		/// <summary>
		/// called when mouse enters this element
		/// </summary>
		protected override void OnMouseEnter( )
		{
			
			// SSP 8/16/01 UWG95
			// We need to call the base class so that mouseDown 
			// and mouseDownOverButton state vars in the base calss
			// get set to correct values properly
			//
			base.OnMouseEnter( );
			
			this.Invalidate();
		}

		/// <summary>
		/// called when the mouse leaves this element
		/// </summary>
		protected override void OnMouseLeave( )
		{
			// SSP 8/16/01 UWG95
			// We need to call the base class so that mouseDown 
			// and mouseDownOverButton state vars in the base calss
			// get set to correct values properly
			//
			base.OnMouseLeave( );

			this.Invalidate();			
		}


        /// <summary>
        /// Called when the mouse down message is received over the element. 
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        /// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
        /// <returns>If true then bypass default processing</returns>
        protected override bool OnMouseDown( MouseEventArgs e, 
										 bool adjustableArea,
										 ref UIElement captureMouseForElement )
		{		
			//bool ret = base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );

			this.inMouseDown = true;

			try
			{
				this.Invalidate( true );
				
				if ( this.Enabled )
				{
					HeaderBase header = (HeaderBase)this.Parent.GetContext( typeof( HeaderBase ) );
					Debug.Assert( null != header, "no header in SwapButtonUIElement.OnClick" );

					if ( null == header )
						return true;
				
					UIElement headerElement = this.GetAncestor( typeof ( HeaderUIElement ) );	
				 
					Debug.Assert( null != headerElement, "no HeaderUIElement in SwalButtonUIElement.OnClick()" );
				
					if ( null != headerElement )
					{
						Rectangle rect = headerElement.Rect;						
						header.OnClickSwapButton( rect );						
					}
				}				

				captureMouseForElement = null;
			}

			finally
			{
				this.inMouseDown = false;				
			}

			return true;
		}

		
	};

}
