#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using Infragistics.Win;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Used to hold card view setting properties for a band.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridBand.CardView"/>
	/// </remarks>
	[ 
	TypeConverter( typeof( System.ComponentModel.ExpandableObjectConverter ) ), 
	Serializable() 
	]
	public class UltraGridCardSettings:	SubObjectBase,				 
				 ISerializable
	{

		#region Member variables

		private Infragistics.Win.UltraWinGrid.UltraGridBand   band;
		private string										  captionField     = null;
		private bool										  allowLabelSizing = true;
		private bool										  allowSizing      = true;
		private bool										  autoFit	       = false;
		private int											  captionLines     = 1;
		private int											  labelWidth       = 0;
		private int											  maxCardAreaRows  = 0;
		private int											  maxCardAreaCols  = 0;
		private bool										  showCaption      = true;
		private CardStyle									  style            = CardStyle.MergedLabels;
		private int											  width            = 0;

		// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
		//
		Infragistics.Win.UltraWinGrid.CardScrollbars cardScrollbars = CardScrollbars.Horizontal;
        
		#endregion

		#region Constructors

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="band"></param>
		public UltraGridCardSettings(Infragistics.Win.UltraWinGrid.UltraGridBand band )
		{
			this.band = band;
			this.Reset();
		}

		#endregion

		#region Serialization Methods

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal UltraGridCardSettings( SerializationInfo info, StreamingContext context )
		protected UltraGridCardSettings( SerializationInfo info, StreamingContext context )
		{
			//since we're not saving properties with default values, we must set the default here
			//
			this.Reset();

			//this will have to be set in InitBand
			//
			this.band = null;

			//everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "AllowLabelSizing":
						//this.AllowLabelSizing = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.AllowLabelSizing = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.AllowLabelSizing );
						//this.AllowLabelSizing = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "AllowSizing":
						//this.AllowSizing = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.AllowSizing = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.AllowSizing );
						//this.AllowSizing = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "AutoFit":
						//this.AutoFit = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.AutoFit = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.AutoFit );
						//this.AutoFit = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "CaptionField":
						//this.CaptionField = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.CaptionField = (string)Utils.DeserializeProperty( entry, typeof(string), this.CaptionField );
						//this.CaptionField = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;

					case "CaptionLines":
						//this.CaptionLines = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.CaptionLines = (int)Utils.DeserializeProperty( entry, typeof(int), this.CaptionLines );
						//this.CaptionLines = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "LabelWidth":
						//this.LabelWidth = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.LabelWidth = (int)Utils.DeserializeProperty( entry, typeof(int), this.LabelWidth );
						//this.LabelWidth = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "MaxCardAreaCols":
						//this.MaxCardAreaCols = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.MaxCardAreaCols = (int)Utils.DeserializeProperty( entry, typeof(int), this.MaxCardAreaCols );
						//this.MaxCardAreaCols = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "MaxCardAreaRows":
						//this.MaxCardAreaRows = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.MaxCardAreaRows = (int)Utils.DeserializeProperty( entry, typeof(int), this.MaxCardAreaRows );
						//this.MaxCardAreaRows = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "ShowCaption":
						//this.ShowCaption = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.ShowCaption = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.ShowCaption );
						//this.ShowCaption = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "Style":
						//this.Style = (Infragistics.Win.UltraWinGrid.CardStyle)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Style = (CardStyle)Utils.DeserializeProperty( entry, typeof(CardStyle), this.Style );
						//this.Style = (CardStyle)Utils.ConvertEnum(entry.Value, this.Style);
						break;

					case "Width":
						//this.Width = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Width = (int)Utils.DeserializeProperty( entry, typeof(int), this.Width );
						//this.Width = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					// JJD 1/31/02
					// De-Serialize the tag property
					//
					case "Tag":
					{
						// AS 8/15/02
						// Use our tag deserializer.
						//
						//this.tagValue = entry.Value;
						this.DeserializeTag(entry);
						break;
					}
					// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
					//
					case "CardScrollbars":
						this.CardScrollbars = (Infragistics.Win.UltraWinGrid.CardScrollbars)Utils.DeserializeProperty( 
												entry, typeof( Infragistics.Win.UltraWinGrid.CardScrollbars ), this.cardScrollbars );
						break;
						
					default:
					{
						Debug.Assert( false, "Invalid entry in CardSettings de-serialization ctor" );
						break;
					}
				}
			}
		}

		

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";
			
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//PropertyCategories propCat = context.Context is PropertyCategories
			//    ? (PropertyCategories)context.Context	: PropertyCategories.All;

			if ( this.ShouldSerializeAllowLabelSizing() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowLabelSizing", this.allowLabelSizing );
				//info.AddValue("AllowLabelSizing", this.allowLabelSizing );
			}

			if ( this.ShouldSerializeAllowSizing() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowSizing", this.allowSizing );
				//info.AddValue( "AllowSizing", this.allowSizing );
			}

			if ( this.ShouldSerializeAutoFit() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AutoFit", this.autoFit );
				//info.AddValue( "AutoFit", this.autoFit );
			}

			if ( this.ShouldSerializeCaptionField() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CaptionField", this.captionField );
				//info.AddValue( "CaptionField", this.captionField );
			}

			if ( this.ShouldSerializeCaptionLines() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CaptionLines", this.captionLines );
				//info.AddValue( "CaptionLines", this.captionLines );
			}

			if ( this.ShouldSerializeLabelWidth() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "LabelWidth", this.labelWidth );
				//info.AddValue( "LabelWidth", this.labelWidth );
			}

			if ( this.ShouldSerializeMaxCardAreaCols() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaxCardAreaCols", this.maxCardAreaCols );
				//info.AddValue( "MaxCardAreaCols", this.maxCardAreaCols );
			}

			if ( this.ShouldSerializeMaxCardAreaRows() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaxCardAreaRows", this.maxCardAreaRows );
				//info.AddValue( "MaxCardAreaRows", this.maxCardAreaRows );
			}

			if ( this.ShouldSerializeShowCaption() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ShowCaption", this.showCaption );
				//info.AddValue( "ShowCaption", this.showCaption );
			}

			if  ( this.ShouldSerializeStyle() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Style", this.style );
				//info.AddValue( "Style", this.style );
			}

			if ( this.ShouldSerializeWidth() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Width", this.width );
				//info.AddValue( "Width", this.width );
			}

			// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
			//
			if ( this.ShouldSerializeCardScrollbars( ) )
			{
				Utils.SerializeProperty( info, "CardScrollbars", this.CardScrollbars );
			}


			// JJD 1/31/02
			// Serialize the tag property
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
		}
		#endregion

		#region Reset methods

		/// <summary>
		/// Resets all properties back to their default values
		/// </summary>
		public void Reset() 
		{
			this.ResetAllowLabelSizing();
			this.ResetAllowSizing();
			this.ResetAutoFit();
			this.ResetCaptionField();
			this.ResetCaptionLines();
			this.ResetLabelWidth();
			this.ResetMaxCardAreaCols();
			this.ResetMaxCardAreaRows();
			this.ResetShowCaption();
			this.ResetStyle();
			this.ResetWidth();
			// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
			//
			this.ResetCardScrollbars( );
		}

		// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
		//
		/// <summary>
		/// Resets the CardScrollbars property to its default value of Horizontal
		/// </summary>
		public void ResetCardScrollbars( )
		{
			this.CardScrollbars = Infragistics.Win.UltraWinGrid.CardScrollbars.Horizontal;
		}

		/// <summary>
		/// Resets Width to its default value (0).
		/// </summary>
		public void ResetWidth() 
		{
			this.Width = 0;
		}

		/// <summary>
		/// Resets Style to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetStyle() 
		{
			this.Style = CardStyle.MergedLabels;
		}

		/// <summary>
		/// Resets ShowCaption to its default value (true).
		/// </summary>
		public void ResetShowCaption() 
		{
			this.ShowCaption = true;
		}

		/// <summary>
		/// Resets MaxCardAreaRows to its default value (0).
		/// </summary>
		public void ResetMaxCardAreaRows() 
		{
			this.MaxCardAreaRows = 0;
		}

		/// <summary>
		/// Resets MaxCardAreaCols to its default value (0).
		/// </summary>
		public void ResetMaxCardAreaCols() 
		{
			this.MaxCardAreaCols = 0;
		}


		/// <summary>
		/// Resets LabelWidth to its default value (0).
		/// </summary>
		public void ResetLabelWidth() 
		{
			this.LabelWidth = 0;
		}


		/// <summary>
		/// Resets CaptionLines to its default value (1).
		/// </summary>
		public void ResetCaptionLines() 
		{
			this.CaptionLines = 1;
		}

		/// <summary>
		/// Resets CaptionField to its default value (null).
		/// </summary>
		public void ResetCaptionField() 
		{
			this.CaptionField = null;
		}

		/// <summary>
		/// Resets AutoFit to its default value (false).
		/// </summary>
		public void ResetAutoFit() 
		{
			this.AutoFit = false;
		}

		/// <summary>
		/// Resets AllowSizing to its default value (true).
		/// </summary>
		public void ResetAllowSizing() 
		{
			this.AllowSizing = true;
		}

		/// <summary>
		/// Resets AllowLabelSizing to its default value (true).
		/// </summary>
		public void ResetAllowLabelSizing() 
		{
			this.AllowLabelSizing = true;
		}

		#endregion

		#region ShouldSerialize methods

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerialize() 
		{
			return this.ShouldSerializeAllowLabelSizing() ||
				this.ShouldSerializeAllowSizing() ||
				this.ShouldSerializeAutoFit() ||
				this.ShouldSerializeCaptionField() ||
				this.ShouldSerializeCaptionLines() ||
				this.ShouldSerializeLabelWidth() ||
				this.ShouldSerializeMaxCardAreaCols() ||
				this.ShouldSerializeMaxCardAreaRows() ||
				this.ShouldSerializeShowCaption() ||
				this.ShouldSerializeStyle() ||
				this.ShouldSerializeTag() ||
				this.ShouldSerializeWidth() ||
				// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
				//
				this.ShouldSerializeCardScrollbars( );
		}

		// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
		//
		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeCardScrollbars()
		{
			return Infragistics.Win.UltraWinGrid.CardScrollbars.Horizontal != this.cardScrollbars;
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeWidth() 
		{
			// AS 2/28/02
			// The default width is 0.
			//
			//return ( this.width  != 150 );
			return ( this.width  != 0 );
		}

		/// <summary>
		///  Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeStyle() 
		{
			return this.style != CardStyle.MergedLabels;
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeShowCaption() 
		{
			return ( true != this.showCaption );
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeMaxCardAreaRows() 
		{
			return ( this.maxCardAreaRows > 0 );
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeMaxCardAreaCols() 
		{
			return ( this.maxCardAreaCols > 0 );
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeLabelWidth() 
		{
			return ( this.labelWidth != 0 );
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeCaptionLines() 
		{
			return ( this.captionLines > 1 );
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeAutoFit() 
		{
			return ( false != this.autoFit );
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeAllowSizing() 
		{
			return ( true != this.allowSizing );
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeAllowLabelSizing() 
		{
			return ( true != this.allowLabelSizing );
		}
 

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeCaptionField() 
		{
			return ( null != this.captionField &&
				this.captionField.Length > 0 );
		}

		#endregion

		#region Properties

		/// <summary>
		/// Returns or sets the name of the field used to supply the text for the caption area of a card.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a band is displayed in card view, each card can display a caption. You can use the <b>CaptionField</b> property to specify the data field that will be used to set the caption text of the card. You can then choose whether or not to display that data field on the card itself. (Data displayed in the card's caption cannot be edited.)</p>
		/// <p class="body">If you want to set the card's caption to something other than the value of one of the data fields, you can do so through code in the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeRow"/> event. Simply set the <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow.CardCaption"/> property of the UltraGridRow object to the text you want to appear in the card's caption.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_CaptionField")]
		[LocalizedCategory("LC_Appearance")]
		public string CaptionField
		{
			get
			{
				// SSP 6/27/02
				// In compressed card-view, if the caption field is not specified, then
				// by default use the scrolltip field.
				//
				if ( null == this.captionField && CardStyle.Compressed == this.Style &&	null != this.band )
				{
					UltraGridColumn scrollTipColumn = this.band.ScrollTipCol;

					if ( null != scrollTipColumn )
						return scrollTipColumn.Key;
				}

				return this.captionField;
			}


			set
			{
				if ( this.captionField != value )
				{
					// JJD 2/5/02
					// Check to make sure band is not null.
					//
					if ( this.band == null || this.band.Layout == null )
					{
						this.captionField   = value;
					}
					else
					{
						// this was being check in the get of
						// Row.Description but now we are not calling that all the time
						//
						try
						{
							
							// If the value is null don't bother looking for it
							//
							if ( value == null || 
								 value.Length < 1 ||
								 this.band.Columns.Exists( value ) )
							{
								this.captionField   = value;
								this.NotifyPropChange( PropertyIds.CaptionField );
							}
							else
								throw new ArgumentException( SR.GetString("LER_Exception_315", value) );
								
						}
						catch( Exception )
						{
							throw new ArgumentException( SR.GetString("LER_Exception_315", value));
						}
					}					
				}
			}
		}		

		/// <summary>
		/// Returns or sets whether card labels can be sized in the horizontal dimension.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can determine whether the width of the label area can be changed by using the <b>AllowLabelSizing</b> property. If this property is set to allow label sizing, the user can resize the area occupied by the labels in card view by positioning their mouse pointer over the right edge of the label area then clicking and dragging to the left or right.</p>
		/// <p class="body">All labels which are displayed shared the same width. In a card view with standard labels, sizing the label area on any card will resize the label area used by all cards. (In merged label card view, only one label area exists.) You can determine which view is in effect by setting the <see cref="Infragistics.Win.UltraWinGrid.UltraGridCardSettings.Style"/> property of the CardSettings object.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_AllowLabelSizing")]
		[LocalizedCategory("LC_Behavior")]
		public bool AllowLabelSizing
		{
			get
			{
				return this.allowLabelSizing;
			}

			set
			{
				if ( value != this.allowLabelSizing )
				{
					this.allowLabelSizing = value;
					this.NotifyPropChange( PropertyIds.AllowLabelSizing );
				}
			}
		}


		/// <summary>
		/// Returns or sets whether cards can be sized in the horizontal dimension.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>AllowResizing</b> property is used to determine whether the width of cards in the card view can be changed by the user. All cards in a band share the same width. Resizing a single card changes the with of all the cards.</p>
		/// <p class="body">If this property is set to allow card resizing, the user can resize a card's width by positioning the mouse pointer over the right edge of the card, then clicking and dragging to the left or right.</p>
		/// <p class="body">If this property is set to allow card resizing, the user can resize a card's width by positioning the mouse pointer over the right edge of the card, then clicking and dragging to the left or right.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_AllowSizing")]
		[LocalizedCategory("LC_Behavior")]
		public bool AllowSizing
		{
			get
			{
				return this.allowSizing;
			}

			set
			{
				if ( value != this.allowSizing )
				{
					this.allowSizing = value;
					this.NotifyPropChange( PropertyIds.AllowSizing );
				}
			}
		}

		/// <summary>
		/// Returns or sets whether cards will be automatically increased in size to use all the width available in the card area.
		/// </summary>
		/// <remarks>
		/// <p class="body">When card view is used for a band, the cards that correpond to data records appear in a rectangular section of the grid called the card area. The horizontal size of the card area is determined by the number of cards being displayed, but it is also dependent on the visible size of the grid. The card area will always extend horizontally from its left origin to the right edge of the control. If there are more cards than can fit within the available area, a horizontal scroll bar appears.</p>
		/// <p class="body">Cards are arranged in the card area in one of three ways, depending on the setting of the <b>UltraGridCardSettings</b> object's <see cref="Infragistics.Win.UltraWinGrid.UltraGridCardSettings.Style"/> property. However they are arranged, the number of cards that can fit within the visible card area is determined by the width of the cards themselves. (All cards share the same width.)</p>
		/// <p class="body">The <b>AutoFit</b> property specifies whether cards will expand to fill the card area based on their width. When <b>AutoFit</b> is True, the cards "snap" to specific size when being resized. The control calculates how many cards can fill the area and then divides their widths evenly, until the width is changed enough to alter the number of cards that will fit in the area.</p>
		/// <p class="body">For example, suppose you have a card view band that is displaying three "stacks" of cards, and has <b>AutoFit</b> set to True. The cards' width is set to take up one-third of the card area. Then, suppose click on the right edge of a card and begin reducing the width. At first, nothing will happen, as the width remains at one-third of the available card area width. However, as you pass a threshold point, the control will determine that four cards can be displayed in the card area, and the width of all cards will be changed to one-fourth of the card area's width. If you begin to widen the cards, four will be displayed horizontally until the threshold width is passed, when the display will snap to show three cards across.</p>
		/// <p class="body">If <b>AutoFit</b> is set to False, card width will not be automatically adjusted, and you will see blank parts of the card area where no cards are being displayed.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_AutoFit")]
		[LocalizedCategory("LC_Behavior")]
		public bool AutoFit
		{
			get
			{
				return this.autoFit;
			}

			set
			{
				if ( value != this.autoFit )
				{
					this.autoFit = value;
					this.NotifyPropChange( PropertyIds.AutoFit );
				}
			}
		}

		/// <summary>
		/// Returns or sets the number of lines in a card's caption.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>CaptionLines</b> property specifies the number of lines of text in the caption area of the cards in card view. The setting of this property applies to all cards in the band.</p> 
		/// <p class="body">The setting of this property allocates the space for the specified number of lines in the card caption. The caption area of the card will be enlarged vertically to accommodate the specified number of lines whether or not there is actually enough text to fill that many lines. Even cards with no text in their caption will display an area sufficient to show the specified number of lines.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_CaptionLines")]
		[LocalizedCategory("LC_Appearance")]
		public int CaptionLines
		{
			get
			{
				return this.captionLines;
			}

			set
			{
				if ( value != this.captionLines )
				{
					if ( value <= 0 )
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_24"), Shared.SR.GetString("LE_ArgumentOutOfRangeException_306"));

					this.captionLines = value;
					this.NotifyPropChange( PropertyIds.CaptionLines );
				}
			}
		}

		/// <summary>
		/// Returns or sets the width of the card label area.
		/// </summary>
		/// <remarks>
		/// <p class="body">In standard card view, this is the width of the label area for each card. In merged card view, this is the width of the common label area. The setting of this property will change when the label area is resized by the user.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_LabelWidth")]
		[LocalizedCategory("LC_Appearance")]
		public int LabelWidth
		{
			get
			{
				return this.labelWidth;
			}

			set
			{
				if ( value != this.labelWidth )
				{
					this.labelWidth = value;
					this.NotifyPropChange( PropertyIds.LabelWidth );
				}
			}
		}		

		/// <summary>
		/// Returns or sets the maximum number of cards displayed in the vertical dimension within the card area.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a band is in card view mode, the card area is the container for the cards that are displayed. The vertical size of the card area is fixed (the card area only scrolls horizontally) but it can be changed by the user. Clicking the bottom edge of the card area and dragging resizes the card area's vertical dimension.</p>
		/// <p class="body">The cards in the card area will re-arrange themselves when it is resized. By default, the control will display as many rows of cards as will fit in the space, based on the size of a card with all fields visible. Using the <b>MaxCardAreaRows</b> property, you can limit the number of rows of cards that will be displayed. Once the maximum number of rows are visible, further expanding the height of the card area will only reveal more blank space.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_MaxCardAreaRows")]
		[LocalizedCategory("LC_Appearance")]
		public int MaxCardAreaRows
		{
			get
			{
				return this.maxCardAreaRows;
			}

			set
			{
				if ( value != this.maxCardAreaRows )
				{
					// JM UWG1020
					if (value < 0)
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_26"), Shared.SR.GetString("LE_ArgumentOutOfRangeException_307"));

					this.maxCardAreaRows = value;
					this.NotifyPropChange( PropertyIds.MaxCardAreaRows );
				}
			}
		}


		/// <summary>
		/// Returns or sets the maximum number of cards displayed in the horizontal dimension within the card area.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a band is in card view mode, the <b>MaxCardAreaCols</b> property specifies the maximum number of "stacks" of cards that will be visible in the card area. This property applies to visibility only; it does not limit the number of cards that will be displayed.</p>
		/// <p class="body">The default behavior is to display as many cards as will fit within the width of the card area, based on the width of the cards. So for example, if you have a card area that is wide enough to display six card widths across, and you set <b>MaxCardAreaCols</b> to 4, only 4 cards across will be displayed at one time. You can then scroll horizontally to see more of the cards, but only four "stacks" of cards will be visible at once.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_MaxCardAreaCols")]
		[LocalizedCategory("LC_Appearance")]
		public int MaxCardAreaCols
		{
			get
			{
				return this.maxCardAreaCols;
			}

			set
			{
				if ( value != this.maxCardAreaCols )
				{
					// JM UWG1020
					if (value < 0)
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_28"), Shared.SR.GetString("LE_ArgumentOutOfRangeException_308"));

					this.maxCardAreaCols = value;
					this.NotifyPropChange( PropertyIds.MaxCardAreaCols );
				}
			}
		}
		

		/// <summary>
		/// Returns or sets whether card captions should be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The text of the card caption is determined by the <see cref="Infragistics.Win.UltraWinGrid.UltraGridCardSettings.CaptionField"/><b>CaptionField</b> property of the <b>CardSettings</b> object or by setting the value of the <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow.CardCaption"/> property of the UltraGridRow object to the text you want to appear in the card's caption. You can also specify the height of the caption using the <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridCardSettings.CaptionLines"/> property.</p>
		/// <p class="body"><b>Note </b>In compressed card view style captions are always displayed and this property has no effect.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_ShowCaption")]
		[LocalizedCategory("LC_Appearance")]
		public bool ShowCaption
		{
			get
			{
				// SSP 7/3/02
				// In compressed card view, always display the card captions because
				// that's the only thing that's visible when the card is compressed.
				//
				// SSP 2/28/03 - Row Layout Functionality
				// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
				// to StandardLabels card style so use the StyleResolved instead of the Style property.
				//
				if ( CardStyle.Compressed == this.StyleResolved )
					return true;

				return this.showCaption;
			}

			set
			{
				if ( value != this.showCaption )
				{
					this.showCaption = value;
					this.NotifyPropChange( PropertyIds.ShowCaption );
				}
			}
		}	


		/// <summary>
		/// Returns or sets the width of every card in the card area.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a band is in card view mode, all the cards share a common width. The width of the card will depend on whether each card is displaying labels (as determined by the <see cref="Infragistics.Win.UltraWinGrid.UltraGridCardSettings.Style"/> property) and may be affected by the setting of the <see cref="Infragistics.Win.UltraWinGrid.UltraGridCardSettings.AutoFit"/> property. By default, the user can resize the width of any card at run time, and all of the cards will be resized to use the new width.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_Width")]
		[LocalizedCategory("LC_Appearance")]
		public int Width
		{
			get
			{
				return this.width;
			}

			set
			{
				if ( value != this.width )
				{
					this.width = value;
					this.NotifyPropChange( PropertyIds.Width );
				}
			}
		}

		/// <summary>
		/// Returns or sets the style of the card.
		/// </summary>
		/// <remarks>
		/// <p class="body">When you place a band in card view mode, the data from each row from that band is displayed as a card. These cards are arranged in a rectangular card view area, which replaces the row in the visible grid. The column headers of the band become the labels for the cards.  The cards are arranged in "stacks" or columns of cards, with each stack consisting of one or more "layers" or rows of individual cards. The <b>Style</b> property specifies how the cards will be arranged within the card view area.</p>
		/// <p class="body">There are three styles of card display. Standard Labels style displays the data labels on each card. Cards are arranged into columns (or "stacks") each having an identical number of rows of cards (or "levels") and each card is the the same height. In Merged Labels style, there is one area for labels on the left side of the card area and the cards themselves contain only the data. Merged Labels style also displays symmetrical stacks of cards with a fixed number of levels that share a common height.</p>
		/// <p class="body">When using the Variable Height style, labels are displayed on each card, but the height of the individual cards changes based on the contents of the card. If a data field has an empty value (a data value of DBNULL or an empty string for string fields) that field does not appear on the card. Note that when using  Variable Height style there is no way to add values to empty fields, since empty fields are inaccessible.</p>
		/// <p class="body">Each stack of cards in the Variable Height view style may contain a different number of levels, and the levels may not line up into neat rows and columns as they do with the other styles. Because the number of levels per stack is variable, there is a difference in the way certain properties are applied in this mode. For example, the <see cref="Infragistics.Win.UltraWinGrid.UltraGridCardSettings.MaxCardAreaRows"/> property determines the maximum number of levels that will be displayed based on the height of a theoretical stack made up of cards with all their fields visible, rather than on the height of any actual stack that is being displayed.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_Style")]
		[LocalizedCategory("LC_Behavior")]
		public Infragistics.Win.UltraWinGrid.CardStyle Style
		{
			get 
			{
				return this.style;
			}
			set
			{
				if(this.style != value)
				{
					if ( !Enum.IsDefined( typeof(CardStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_86") );

					// JJD 1/17/02
					// Only adjust the width if everthing is hooked up properly
					//
					if ( this.band				!= null && 
						 this.band.Layout		!= null &&
						 this.width != 0 )
					{
						// SSP 2/28/03 - Row Layout Functionality
						// In row layout mode, don't adjust the width since in the row layout mode we
						// don't add any card label area inside of the row ui element.
						//
						if ( null == this.band || ! this.band.UseRowLayoutResolved )
						{
							// JM 01-09-02 If we are going to or from the MergedLabels style,
							//			   adjust the overall width of the card so the field
							//			   area remains the same size.
							if (this.style == CardStyle.MergedLabels)
								this.Width = this.band.CardWidthResolved + this.band.CardLabelWidthResolved;
							else if (value == CardStyle.MergedLabels)
								this.Width = this.band.CardWidthResolved - this.band.CardLabelWidthResolved;
						}
					}

					this.style = value;
					this.NotifyPropChange( PropertyIds.CardStyle, null );
				}
			}
		}	

		// SSP 2/28/03 - Row Layout Functionality
		// Added StyleResolved preoprty.
		//
		internal CardStyle StyleResolved
		{
			get
			{
				if ( CardStyle.VariableHeight == this.Style && null != this.band && this.band.UseRowLayoutResolved )
					return CardStyle.StandardLabels;

				return this.Style;
			}
		}

		// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
		// Added CardScrollbars property.
		//
		/// <summary>
		/// Specifies whether scrollbars in card-view are visible.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use <b>CardScrollbars</b> property to control the scrollbars in card area.</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.CardScrollbars"/>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.CardScrollbars CardScrollbars
		{
			get
			{
				return this.cardScrollbars;
			}
			set
			{
				if ( this.cardScrollbars != value )
				{
					if ( ! Enum.IsDefined( typeof( CardScrollbars ), value ) )
						throw new InvalidEnumArgumentException( "CardScrollbars", (int)value, typeof( CardScrollbars ) );

					this.cardScrollbars = value;

					this.NotifyPropChange( PropertyIds.CardScrollbars );
				}
			}
		}

		#endregion

		#region Methods

		internal void InitBand( Infragistics.Win.UltraWinGrid.UltraGridBand band)
		{
			this.band = band;
		}		

		internal void InitializeFrom( UltraGridCardSettings source, PropertyCategories propertyCategories )
		{
			this.allowLabelSizing = source.allowLabelSizing;
			this.allowSizing      = source.allowSizing;
			this.autoFit          = source.autoFit;
			this.captionField     = source.captionField;
			this.captionLines     = source.captionLines;
			this.labelWidth       = source.labelWidth;
			this.maxCardAreaCols  = source.maxCardAreaCols;
			this.maxCardAreaRows  = source.maxCardAreaRows;
			this.showCaption      = source.showCaption;
			this.style            = source.style;
			this.width            = source.width;

			// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
			//
            this.cardScrollbars   = source.cardScrollbars;

			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;
		}

		/// <summary>
		/// Returns an empty string
		/// </summary>
        /// <returns>An empty string.</returns>
		public override string ToString() { return string.Empty; }

		#endregion
	}
}
