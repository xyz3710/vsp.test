#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Diagnostics;
	using System.Collections;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Drawing.Printing;
	using Infragistics.Shared;

	/// <summary>
	/// delegate for handling event that occurs when the layout is initialized
	/// </summary>
	public delegate void InitializeLayoutEventHandler( object sender, InitializeLayoutEventArgs e ); 
    
	/// <summary>
	/// delegate for handling event that occurs when a row is initialized
	/// </summary>
	public delegate void InitializeRowEventHandler( object sender, InitializeRowEventArgs e ); 

	/// <summary>
	/// delegate for handling event that occurs when a group by row is initialized
	/// </summary>
	public delegate void InitializeGroupByRowEventHandler( object sender, InitializeGroupByRowEventArgs e ); 


	/// <summary>
	/// delegate for handling events that take a CancelableCellEventArgs as the event args
	/// </summary>
	public delegate void CancelableCellEventHandler( object sender, CancelableCellEventArgs e );     
	
	/// <summary>
	/// delegate for handling events that take a CancelableRowEventArgs as the event args
	/// </summary>
	public delegate void CancelableRowEventHandler( object sender, CancelableRowEventArgs e );
	
	/// <summary>
	/// delegate for handling events that take a RowEventArgs as the event args
	/// </summary>
	public delegate void RowEventHandler( object sender, RowEventArgs e );
    
	/// <summary>
	/// delegate for handling events that take a CellEventArgs as the event args
	/// </summary>
	public delegate void CellEventHandler( object sender, CellEventArgs e ); 
	
	/// <summary>
	/// delegate for handling events that take a ColScrollRegionEventArgs as the event args
	/// </summary>
	public delegate void ColScrollRegionEventHandler(object sender, ColScrollRegionEventArgs e);
	
	/// <summary>
	/// delegate for handling events that take a RowScrollRegionEventArgs as the event args
	/// </summary>
	public delegate void RowScrollRegionEventHandler(object sender, RowScrollRegionEventArgs e);
	
	/// <summary>
	/// delegate for handling events that take a BandEventArgs as the event args
	/// </summary>
	public delegate void BandEventHandler(object sender, BandEventArgs e);
	
	/// <summary>
	/// delegate for handling events that take a CancelableAutoSizeEditEventArgs as the event args
	/// </summary>
	public delegate void CancelableAutoSizeEditEventHandler(object sender, CancelableAutoSizeEditEventArgs e);
    
	/// <summary>
	/// delegate for after any kind of selection state changes
	/// </summary>
	public delegate void AfterSelectChangeEventHandler(object sender, AfterSelectChangeEventArgs e);
	/// <summary>
	/// delegate for before any kind of selection state changes
	/// </summary>
	public delegate void BeforeSelectChangeEventHandler(object sender, BeforeSelectChangeEventArgs e);
	/// <summary>
	/// delegate for after a column has been moved, sized or swapped
	/// </summary>
	public delegate void AfterColPosChangedEventHandler(object sender, AfterColPosChangedEventArgs e);
	/// <summary>
	/// delegate for after a group has been moved, sized or swapped
	/// </summary>
	public delegate void AfterGroupPosChangedEventHandler(object sender, AfterGroupPosChangedEventArgs e);
	
	/// <summary>
	/// delegate for before a group has been moved, sized or swapped
	/// </summary>
	public delegate void BeforeGroupPosChangedEventHandler(object sender, BeforeGroupPosChangedEventArgs e); 
	/// <summary>
	/// delegate for before a column has been moved, sized or swapped
	/// </summary>
	public delegate void BeforeColPosChangedEventHandler(object sender, BeforeColPosChangedEventArgs e);

	/// <summary>
	/// delegate for handling event that is fired when a row scroll region is removed
	/// </summary>
	public delegate void BeforeRowRegionRemovedEventHandler(object sender, BeforeRowRegionRemovedEventArgs e);
	/// <summary>
	/// delegate for before the sort indicator has changed
	/// </summary>
	public delegate void BeforeSortChangeEventHandler(object sender, BeforeSortChangeEventArgs e);

	/// <summary>
	/// delegate for handling event that is fired before a row is being inserted
	/// </summary>
	public delegate void BeforeRowInsertEventHandler(object sender, BeforeRowInsertEventArgs e);

	/// <summary>
	/// delegate for handling event that is fired before a row is resized
	/// </summary>
	public delegate void BeforeRowResizeEventHandler(object sender, BeforeRowResizeEventArgs e);	

	/// <summary>
	/// delegate for handling event that is fired before a cell is updated
	/// </summary>
	public delegate void BeforeCellUpdateEventHandler(object sender, BeforeCellUpdateEventArgs e);
	
	/// <summary>
	/// delegate for handling event that is fired before rows are deleted
	/// </summary>
	public delegate void BeforeRowsDeletedEventHandler(object sender, BeforeRowsDeletedEventArgs e);
	
	/// <summary>
	/// delegate for handling event that is fired before a row scroll region is scrolled
	/// </summary>	
	public delegate void BeforeRowRegionScrollEventHandler(object sender, BeforeRowRegionScrollEventArgs e);
	
	/// <summary>
	/// delegate for handling event that is fired before a row scroll region is resized
	/// </summary>
	public delegate void BeforeRowRegionSizeEventHandler(object sender, BeforeRowRegionSizeEventArgs e);
	
	/// <summary>
	/// delegate for handling event that is fired before a row scroll region is split
	/// </summary>
	public delegate void BeforeRowRegionSplitEventHandler(object sender, BeforeRowRegionSplitEventArgs e);
	
	/// <summary>
	/// delegate for handling event that is fired before a col scroll region is removed
	/// </summary>
	public delegate void BeforeColRegionRemovedEventHandler(object sender, BeforeColRegionRemovedEventArgs e);
	
	/// <summary>
	/// delegate for handling event that is fired before a col scroll region is split
	/// </summary>
	public delegate void BeforeColRegionSplitEventHandler(object sender, BeforeColRegionSplitEventArgs e);
	
	/// <summary>
	/// delegate for handling event that is fired before a col scroll region is scrolled
	/// </summary>
	public delegate void BeforeColRegionScrollEventHandler(object sender, BeforeColRegionScrollEventArgs e);
	
	/// <summary>
	/// delegate for handling event that is fired before a col scroll region is resized
	/// </summary>
	public delegate void BeforeColRegionSizeEventHandler(object sender, BeforeColRegionSizeEventArgs e);


	/// <summary>
	/// delegate for handling event that is fired when data update error occurs
	/// </summary>
	public delegate void ErrorEventHandler(object sender, ErrorEventArgs e);

	// SSP 5/2/02
	// Added CellDataErro event.
	//
	/// <summary>
	/// delegate for handling event that is fired when data update error occurs
	/// </summary>
	public delegate void CellDataErrorEventHandler(object sender, CellDataErrorEventArgs e);

	// SSP 5/2/02
	// Changed event args of BeforeExitEditMode from CancelEventArgs to 
	// BeforeExitEditModeEventArgs because we needed to add two paramters to
	// it ( ForceExit, CancallingEditOperation ), and we can't add them to
	// CancelEventArgs for obvious reasons.
	//
	/// <summary>
	/// delegate for handling BeforeExitEditMode event which is fired before exitting the edit mode.
	/// </summary>
	public delegate void BeforeExitEditModeEventHandler( object sender, BeforeExitEditModeEventArgs e );

	/// <summary>
	/// delegate for handling event that is fired when a Initialize Print Preview event occurs
	/// </summary>
	public delegate void InitializePrintPreviewEventHandler( object sender, CancelablePrintPreviewEventArgs e );

	/// <summary>
	/// delegate for handling event that is fired when a Initialize Logical Print Page event occurs
	/// </summary>
	public delegate void InitializeLogicalPrintPageEventHandler( object sender, CancelableLogicalPrintPageEventArgs e );


	/// <summary>
	/// delegate for handling event that is fired when a Initialize Print event occurs
	/// </summary>
	public delegate void InitializePrintEventHandler( object sender, CancelablePrintEventArgs e );

	/// <summary>
	/// delegate for handling event that is fired when a Before Print event occurs
	/// </summary>
	public delegate void BeforePrintEventHandler( object sender, CancelablePrintEventArgs e );

	

	/// <summary>
	/// delegate for handling event that is fired whe a new row is selected
	/// in the ultradropdown
	/// </summary>
	public delegate void RowSelectedEventHandler( object sender, RowSelectedEventArgs e );

	/// <summary>
	/// delegate for handling event that is fired when a new row is selected
	/// in the ultradropdown
	/// </summary>
	public delegate void DropDownEventHandler( object sender, DropDownEventArgs e );

	// JJD 11/21/02 
	// CancelableDropDownEventHandler was never used.
	//	/// <summary>
	//	/// delegate for handling event that is fired whe a new row is selected
	//	/// in the ultradropdown
	//	/// </summary>
	//	public delegate void CancelableDropDownEventHandler( object sender, CancelableDropDownEventArgs e );


	// SSP 3/21/02
	// Version 2 Row Filter Implementation
	//
	#region Row filtering event delegates

	/// <summary>
	/// delegate for handling event that is fired when a row is being filtered using row filters.
	/// </summary>
	public delegate void FilterRowEventHandler( object sender, FilterRowEventArgs e );

	/// <summary>
	/// delegate for handling event that is fired before row-filter dropdown is dropped down.
	/// </summary>
	public delegate void BeforeRowFilterDropDownEventHandler( object sender, BeforeRowFilterDropDownEventArgs e );

	/// <summary>
	/// delegate for handling event that is fired before custom row filters dialog is displayed.
	/// </summary>
	public delegate void BeforeCustomRowFilterDialogEventHandler( object sender, BeforeCustomRowFilterDialogEventArgs e );

	// SSP 8/1/03
	// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
	//
	/// <summary>
	/// Delegate for AfterRowFilterChanged event.
	/// </summary>
	public delegate void AfterRowFilterChangedEventHandler( object sender, AfterRowFilterChangedEventArgs e );

	/// <summary>
	/// Delegate for BeforeRowFilterChanged event.
	/// </summary>
	public delegate void BeforeRowFilterChangedEventHandler( object sender, BeforeRowFilterChangedEventArgs e );

	// SSP 4/15/04 - Virtual Binding
	// Added BeforeRowFilterDropDownPopulate to give the users a chance to prevent the ultragrid
	// from populating the filter drop down list. This can be useful if there are lot of rows
	// and it takes a long time for the ultragrid to populate the filter drop down list.
	// 
	/// <summary>
	/// Delegate for BeforeRowFilterDropDownPopulate event.
	/// </summary>
	public delegate void BeforeRowFilterDropDownPopulateEventHandler( object sender, BeforeRowFilterDropDownPopulateEventArgs e );

	#endregion //Row filtering event delegates

	#region Summaries event delegates

	// SSP 5/17/02
	// Added events for summary rows feature.
	//
	/// <summary>
	/// Delegate for BeforeSummaryDialog event.
	/// </summary>
	public delegate void BeforeSummaryDialogEventHandler( object sender, BeforeSummaryDialogEventArgs e );

	// SSP 12/5/03 UWG2295
	// Added AfterSummaryDialog event.
	// 
	/// <summary>
	/// Delegate for AfterSummaryDialog event.
	/// </summary>
	public delegate void AfterSummaryDialogEventHandler( object sender, AfterSummaryDialogEventArgs e );

	/// <summary>
	/// delegate for SummaryValueChanged event.
	/// </summary>
	public delegate void SummaryValueChangedEventHandler( object sender, SummaryValueChangedEventArgs e );

	#endregion // Summaries event delegates

	// SSP 6/25/03 UWG2413 - Row Layout Functionality
	// Added events that we need to fire when a cell or a header is resized in the row-layout mode.
	// Added delegates BeforeRowLayoutItemResizedEventHandler and AfterRowLayoutItemResizedEventHandler.
	//
	/// <summary>
	/// Delegate for BeforeRowLayoutItemResized event.
	/// </summary>
	public delegate void BeforeRowLayoutItemResizedEventHandler( object sender, BeforeRowLayoutItemResizedEventArgs e );

	/// <summary>
	/// Delegate for AfterRowLayoutItemResized event.
	/// </summary>
	public delegate void AfterRowLayoutItemResizedEventHandler( object sender, AfterRowLayoutItemResizedEventArgs e );
    
	// SSP 7/30/03 UWG2544
	//
	/// <summary>
	/// Delegate for AfterUltraGridPerformAction event.
	/// </summary>
	public delegate void AfterUltraGridPerformActionEventHandler( object sender, AfterUltraGridPerformActionEventArgs e );

	/// <summary>
	/// Delegate for BeforeUltraGridPerformAction event.
	/// </summary>
	public delegate void BeforeUltraGridPerformActionEventHandler( object sender, BeforeUltraGridPerformActionEventArgs e );

	// SSP 11/10/03 Add Row Feature
	//
	/// <summary>
	/// Delegate for InitializeTemplateAddRow event.
	/// </summary>
	public delegate void InitializeTemplateAddRowEventHandler( object sender, InitializeTemplateAddRowEventArgs e );


	// SSP 12/5/03 UWG2295
	// Added AfterCardsScroll event.
	//
	/// <summary>
	/// Delegate for AfterCardsScrollEventArgs event.
	/// </summary>
	public delegate void AfterCardsScrollEventHandler( object sender, AfterCardsScrollEventArgs e );

	//JDN 11/9/04 LimitToList
	/// <summary>
	/// Delegate for ItemNotInList event.
	/// </summary>
	public delegate void  ItemNotInListEventHandler( object sender, Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e );

	// JDN 11/17/04 CardCompressedStateChanged events
	/// <summary>
	/// Delegate for BeforeCardCompressedStateChanged event.
	/// </summary>
	public delegate void BeforeCardCompressedStateChangedEventHandler( object sender, BeforeCardCompressedStateChangedEventArgs e );

	// JDN 11/17/04 CardCompressedStateChanged events 
	/// <summary>
	/// Delegate for AfterCardCompressedStateChanged event.
	/// </summary>
	public delegate void AfterCardCompressedStateChangedEventHandler( object sender, AfterCardCompressedStateChangedEventArgs e );

	// SSP 12/15/04 - IDataErrorInfo Support
	//
	/// <summary>
	/// Delegate for BeforeDisplayDataErrorTooltip event.
	/// </summary>
	public delegate void BeforeDisplayDataErrorTooltipEventHandler( object sender, BeforeDisplayDataErrorTooltipEventArgs e );

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	//
	// ------------------------------------------------------------------------------
	/// <summary>
	/// Delegate for AfterRowFixedStateChanged event.
	/// </summary>
	public delegate void AfterRowFixedStateChangedEventHandler( object sender, AfterRowFixedStateChangedEventArgs e );

	/// <summary>
	/// Delegate for BeforeRowFixedStateChanged event.
	/// </summary>
	public delegate void BeforeRowFixedStateChangedEventHandler( object sender, BeforeRowFixedStateChangedEventArgs e );

	/// <summary>
	/// Delegate for FilterCellValueChanged event.
	/// </summary>
	public delegate void FilterCellValueChangedEventHandler( object sender, FilterCellValueChangedEventArgs e );

	/// <summary>
	/// Delegate for InitializeRowsCollection event.
	/// </summary>
	public delegate void InitializeRowsCollectionEventHandler( object sender, InitializeRowsCollectionEventArgs e );
	// ------------------------------------------------------------------------------

	// SSP 6/17/05 - NAS 5.3 Column Chooser
	//
	/// <summary>
	/// Delegate for BeforeColumnChooserDisplayed event.
	/// </summary>
	public delegate void BeforeColumnChooserDisplayedEventHandler( object sender, BeforeColumnChooserDisplayedEventArgs e );

	// SSP 10/18/05 - NAS 5.3 Column Chooser
	// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by TestAdvantage.
	// 
	/// <summary>
	/// Delegate for BeforeBandHiddenChanged event.
	/// </summary>
	public delegate void BeforeBandHiddenChangedEventHandler( object sender, BeforeBandHiddenChangedEventArgs e );

	/// <summary>
	/// Delegate for AfterBandHiddenChanged event.
	/// </summary>
	public delegate void AfterBandHiddenChangedEventHandler( object sender, AfterBandHiddenChangedEventArgs e );

	// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
	// 
	/// <summary>
	/// Delegate for BeforeMultiCellOperation event.
	/// </summary>
	public delegate void BeforeMultiCellOperationEventHandler( object sender, BeforeMultiCellOperationEventArgs e );

	
    
	

	/// <summary>
	/// Uniquely identifies each UltraCombo specific event
	/// </summary>
	public enum ComboEventIds
	{
		// leave MouseEnterElement and MouseLeaveElement as 0 and 1 since
		// these events will be exposed on all of our controls and
		// we might as well have consistency
		//

		/// <summary>
		/// event id that identifies MouseEnterElement event
		/// </summary>
		MouseEnterElement = 0,

		/// <summary>
		/// event id that identifies MouseLeaveElement event
		/// </summary>
		MouseLeaveElement  = 1,

		/// <summary>
		/// Fired after the list is dropped down
		/// </summary>
		AfterDropDown		= 2,

		/// <summary>
		/// Fired after the list is closed up
		/// </summary>
		AfterCloseUp		= 3,

		/// <summary>
		/// Fired before the list is dropped down
		/// </summary>
		BeforeDropDown		= 4,

		/// <summary>
		/// Fired when a layout is initialized
		/// </summary>
		InitializeLayout	= 5,	
		
		/// <summary>
		/// Fired when a row is initialized
		/// </summary>
		InitializeRow		= 6,

		/// <summary>
		/// fired when a new row is selected
		/// </summary>
		RowSelected			= 7,

		/// <summary>
		/// Fired when the value property has changed
		/// </summary>
		ValueChanged		= 8,

		//JDN 11/9/04 LimitToList
		/// <summary>
		/// Fired before the Validating event whenever the text value in the editor portion of the control
		/// is not a value in the control�s valuelist.
		/// </summary>
		ItemNotInList		= 9,

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies FilterRow event
		/// </summary>
		FilterRow = 10,

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies AfterSortChange event
		/// </summary>
		AfterSortChange             = 11,

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeSortChange event
		/// </summary>
		BeforeSortChange            = 12,

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeColPosChanged event
		/// </summary>
		BeforeColPosChanged         = 13,

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies AfterColPosChanged event
		/// </summary>
		AfterColPosChanged          = 14,

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeRowFilterDropDown event
		/// </summary>
		BeforeRowFilterDropDown = 15,

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeRowFilterChanged event
		/// </summary>
		BeforeRowFilterChanged = 16,

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies AfterRowFilterChanged event
		/// </summary>
		AfterRowFilterChanged = 17,

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeCustomRowFilterDialog event
		/// </summary>
		BeforeCustomRowFilterDialog = 18,

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeRowFilterDropDownPopulate event
		/// </summary>
		BeforeRowFilterDropDownPopulate = 19,	

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		/// <summary>
		/// event id that identifies AfterEditorButtonCloseUp event
		/// </summary>
		AfterEditorButtonCloseUp = 20,

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		/// <summary>
		/// event id that identifies BeforeEditorButtonDropDown event
		/// </summary>
		BeforeEditorButtonDropDown = 21,

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		/// <summary>
		/// event id that identifies EditorButtonClick event
		/// </summary>
		EditorButtonClick = 22,

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		/// <summary>
		/// event id that identifies EditorSpinButtonClick event
		/// </summary>
		EditorSpinButtonClick = 23,

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		/// <summary>
		/// event id that identifies InitializeEditorButtonCheckState event
		/// </summary>
		InitializeEditorButtonCheckState = 24,

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		/// <summary>
		/// event id that identifies BeforeEditorButtonCheckStateChanged event
		/// </summary>
		BeforeEditorButtonCheckStateChanged = 25,

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		/// <summary>
		/// event id that identifies AfterEditorButtonCheckStateChanged event
		/// </summary>
		AfterEditorButtonCheckStateChanged = 26,

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		// ------------------------------------------------------------------------------
		/// <summary>
		/// Event id that identifies <b>FilterCellValueChanged</b> event.
		/// </summary>
		FilterCellValueChanged			= 27,

		/// <summary>
		/// Event id that identifies <b>InitializeRowsCollection</b> event.
		/// </summary>
		InitializeRowsCollection		= 28,
		// ------------------------------------------------------------------------------

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		//
		/// <summary>
		/// Event id that identifies <b>BeforeColumnChooserDisplayed</b> event.
		/// </summary>
		BeforeColumnChooserDisplayed	= 29,

		// SSP 10/18/05 - NAS 5.3 Column Chooser
		// 
		/// <summary>
		/// Event id that identifies <b>BeforeBandHiddenChanged</b> event.
		/// </summary>
		BeforeBandHiddenChanged			= 30,

		/// <summary>
		/// Event id that identifies <b>AfterBandHiddenChanged</b> event.
		/// </summary>
		AfterBandHiddenChanged			= 31,

        // CDS 02/02/09 TFS12512 - NAS v9.1 Header CheckBox -Added support for firing the events on the UltraCombo
        /// <summary>
        /// Event id that identifies the <b>BeforeHeaderCheckStateChanged</b> event.
        /// </summary>
        BeforeHeaderCheckStateChanged = 32,

        // CDS 02/02/09 TFS12512 - NAS v9.1 Header CheckBox -Added support for firing the events on the UltraCombo
        /// <summary>
        /// Event id that identifies the <b>AfterHeaderCheckStateChanged</b> event.
        /// </summary>
        AfterHeaderCheckStateChanged = 33,

		/// <summary>
		/// keep the last event id used
		/// </summary>
		[EditorBrowsable( EditorBrowsableState.Never ) ]
		LastEventId = 33
	}

	/// <summary>
	/// Uniquely identifies each UltraDropDown specific event
	/// </summary>
	public enum DropDownEventIds
	{
		// leave MouseEnterElement and MouseLeaveElement as 0 and 1 since
		// these events will be exposed on all of our controls and
		// we might as well have consistency
		//

		/// <summary>
		/// event id that identifies MouseEnterElement event
		/// </summary>
		MouseEnterElement	= 0,

		/// <summary>
		/// event id that identifies MouseLeaveElement event
		/// </summary>
		MouseLeaveElement	= 1,

		/// <summary>
		/// Fired after the list is dropped down
		/// </summary>
		AfterDropDown		= 2,

		/// <summary>
		/// Fired after the list is closed up
		/// </summary>
		AfterCloseUp		= 3,

		/// <summary>
		/// Fired before the list is dropped down
		/// </summary>
		BeforeDropDown		= 4,

		/// <summary>
		/// Fired when a layout is initialized
		/// </summary>
		InitializeLayout	= 5,	
		
		/// <summary>
		/// Fired when a row is initialized
		/// </summary>
		InitializeRow		= 6,

		/// <summary>
		/// fired when a new row is selected
		/// </summary>
		RowSelected			= 7,

		/// <summary>
		/// Fired when the text in the edit portion has changed
		/// </summary>
		TextChanged			= 8,

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies FilterRow event
		/// </summary>
		FilterRow = 9,

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies AfterSortChange event
		/// </summary>
		AfterSortChange             = 10,

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeSortChange event
		/// </summary>
		BeforeSortChange            = 11,

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeColPosChanged event
		/// </summary>
		BeforeColPosChanged         = 12,

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies AfterColPosChanged event
		/// </summary>
		AfterColPosChanged          = 13,

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeRowFilterDropDown event
		/// </summary>
		BeforeRowFilterDropDown = 14,

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeRowFilterChanged event
		/// </summary>
		BeforeRowFilterChanged = 15,

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies AfterRowFilterChanged event
		/// </summary>
		AfterRowFilterChanged = 16,

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeCustomRowFilterDialog event
		/// </summary>
		BeforeCustomRowFilterDialog = 17,

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		/// <summary>
		/// event id that identifies BeforeRowFilterDropDownPopulate event
		/// </summary>
		BeforeRowFilterDropDownPopulate = 18,
		
		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		// ------------------------------------------------------------------------------
		/// <summary>
		/// Event id that identifies <b>FilterCellValueChanged</b> event.
		/// </summary>
		FilterCellValueChanged			= 19,

		/// <summary>
		/// Event id that identifies <b>InitializeRowsCollection</b> event.
		/// </summary>
		InitializeRowsCollection		= 20,
		// ------------------------------------------------------------------------------

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		//
		/// <summary>
		/// Event id that identifies <b>BeforeColumnChooserDisplayed</b> event.
		/// </summary>
		BeforeColumnChooserDisplayed	= 21,

		// SSP 10/18/05 - NAS 5.3 Column Chooser
		// 
		/// <summary>
		/// Event id that identifies <b>BeforeBandHiddenChanged</b> event.
		/// </summary>
		BeforeBandHiddenChanged			= 22,

		/// <summary>
		/// Event id that identifies <b>AfterBandHiddenChanged</b> event.
		/// </summary>
		AfterBandHiddenChanged			= 23,

		/// <summary>
		/// keep the last event id used
		/// </summary>
		[EditorBrowsable( EditorBrowsableState.Never ) ]
		LastEventId = 23
	}

	/// <summary>
	/// Uniquely identifies each UltraGrid specific event
	/// </summary>
	public enum GridEventIds
	{
		// leave MouseEnterElement and MouseLeaveElement as 0 and 1 since
		// these events will be exposed on all of our controls and
		// we might as well have consistency
		//

		/// <summary>
		/// event id that identifies MouseEnterElement event
		/// </summary>
		MouseEnterElement = 0,

		/// <summary>
		/// event id that identifies MouseLeaveElement event
		/// </summary>
		MouseLeaveElement  = 1,

		// the order of the other events doesn't matter but MUST not
		// be changed after we ship the first release of the grid
		//

		/// <summary>
		/// event id that identifies AfterCellActivate event
		/// </summary>
		AfterCellActivate           = 2,


		/// <summary>
		/// event id that identifies AfterCellListCloseUp event
		/// </summary>
		AfterCellListCloseUp        = 3,


		/// <summary>
		/// event id that identifies AfterCellUpdate event
		/// </summary>
		AfterCellUpdate             = 4,


		/// <summary>
		/// event id that identifies AfterColPosChanged event
		/// </summary>
		AfterColPosChanged          = 5,


		/// <summary>
		/// event id that identifies AfterColRegionScroll event
		/// </summary>
		AfterColRegionScroll        = 6,


		/// <summary>
		/// event id that identifies AfterColRegionSize event
		/// </summary>
		AfterColRegionSize          = 7,


		/// <summary>
		/// event id that identifies AfterEnterEditMode event
		/// </summary>
		AfterEnterEditMode          = 8,


		/// <summary>
		/// event id that identifies AfterExitEditMode event
		/// </summary>
		AfterExitEditMode           = 9,


		/// <summary>
		/// event id that identifies AfterGroupPosChanged event
		/// </summary>
		AfterGroupPosChanged        = 10,


		/// <summary>
		/// event id that identifies AfterRowActivate event
		/// </summary>
		AfterRowActivate            = 11,


		/// <summary>
		/// event id that identifies AfterRowCollapsed event
		/// </summary>
		AfterRowCollapsed           = 12,


		/// <summary>
		/// event id that identifies AfterRowExpanded event
		/// </summary>
		AfterRowExpanded            = 13,


		/// <summary>
		/// event id that identifies AfterRowsDeleted event
		/// </summary>
		AfterRowsDeleted            = 14,


		/// <summary>
		/// event id that identifies AfterRowInsert event
		/// </summary>
		AfterRowInsert              = 15,


		/// <summary>
		/// event id that identifies AfterRowRegionScroll event
		/// </summary>
		AfterRowRegionScroll        = 16,


		/// <summary>
		/// event id that identifies AfterRowRegionSize event
		/// </summary>
		AfterRowRegionSize          = 17,


		/// <summary>
		/// event id that identifies AfterRowResize event
		/// </summary>
		AfterRowResize              = 18,


		/// <summary>
		/// event id that identifies AfterRowUpdate event
		/// </summary>
		AfterRowUpdate              = 19,


		/// <summary>
		/// event id that identifies AfterSelectChange event
		/// </summary>
		AfterSelectChange           = 20,


		/// <summary>
		/// event id that identifies AfterSortChange event
		/// </summary>
		AfterSortChange             = 21,


		/// <summary>
		/// event id that identifies AfterCellCancelUpdate event
		/// </summary>
		AfterCellCancelUpdate       = 22,


		/// <summary>
		/// event id that identifies AfterRowCancelUpdate event
		/// </summary>
		AfterRowCancelUpdate        = 23,


		/// <summary>
		/// event id that identifies BeforeAutoSizeEdit event
		/// </summary>
		BeforeAutoSizeEdit          = 24,


		/// <summary>
		/// event id that identifies BeforeCellActivate event
		/// </summary>
		BeforeCellActivate          = 25,


		/// <summary>
		/// event id that identifies BeforeCellDeactivate event
		/// </summary>
		BeforeCellDeactivate        = 26,


		/// <summary>
		/// event id that identifies BeforeCellListDropDown event
		/// </summary>
		BeforeCellListDropDown      = 27,  


		/// <summary>
		/// event id that identifies BeforeCellUpdate event
		/// </summary>
		BeforeCellUpdate            = 28,


		/// <summary>
		/// event id that identifies BeforeColPosChanged event
		/// </summary>
		BeforeColPosChanged         = 29,


		/// <summary>
		/// event id that identifies BeforeColRegionScroll event
		/// </summary>
		BeforeColRegionScroll       = 30,


		/// <summary>
		/// event id that identifies BeforeColRegionSize event
		/// </summary>
		BeforeColRegionSize         = 31,


		/// <summary>
		/// event id that identifies BeforeColRegionSplit event
		/// </summary>
		BeforeColRegionSplit        = 32,


		/// <summary>
		/// event id that identifies BeforeColRegionRemoved event
		/// </summary>
		BeforeColRegionRemoved      = 33,


		/// <summary>
		/// event id that identifies BeforeEnterEditMode event
		/// </summary>
		BeforeEnterEditMode         = 34,


		/// <summary>
		/// event id that identifies BeforeExitEditMode event
		/// </summary>
		BeforeExitEditMode          = 35,


		/// <summary>
		/// event id that identifies BeforeGroupPosChanged event
		/// </summary>
		BeforeGroupPosChanged       = 36,


		/// <summary>
		/// event id that identifies BeforeRowActivate event
		/// </summary>
		BeforeRowActivate           = 37,


		/// <summary>
		/// event id that identifies BeforeRowDeactivate event
		/// </summary>
		BeforeRowDeactivate         = 38,


		/// <summary>
		/// event id that identifies BeforeRowCollapsed event
		/// </summary>
		BeforeRowCollapsed          = 39,


		/// <summary>
		/// event id that identifies BeforeRowsDeleted event
		/// </summary>
		BeforeRowsDeleted           = 40,


		/// <summary>
		/// event id that identifies BeforeRowExpanded event
		/// </summary>
		BeforeRowExpanded           = 41,


		/// <summary>
		/// event id that identifies BeforeRowInsert event
		/// </summary>
		BeforeRowInsert             = 42,


		/// <summary>
		/// event id that identifies BeforeRowRegionScroll event
		/// </summary>
		BeforeRowRegionScroll       = 43,


		/// <summary>
		/// event id that identifies BeforeRowRegionSize event
		/// </summary>
		BeforeRowRegionSize         = 44,


		/// <summary>
		/// event id that identifies BeforeRowRegionSplit event
		/// </summary>
		BeforeRowRegionSplit        = 45,


		/// <summary>
		/// event id that identifies BeforeRowRegionRemoved event
		/// </summary>
		BeforeRowRegionRemoved      = 46,


		/// <summary>
		/// event id that identifies BeforeRowResize event
		/// </summary>
		BeforeRowResize             = 47,


		/// <summary>
		/// event id that identifies BeforeRowUpdate event
		/// </summary>
		BeforeRowUpdate             = 48,


		/// <summary>
		/// event id that identifies BeforeSelectChange event
		/// </summary>
		BeforeSelectChange          = 49,


		/// <summary>
		/// event id that identifies BeforeSortChange event
		/// </summary>
		BeforeSortChange            = 50,


		/// <summary>
		/// event id that identifies BeforeCellCancelUpdate event
		/// </summary>
		BeforeCellCancelUpdate      = 51,


		/// <summary>
		/// event id that identifies BeforeRowCancelUpdate event
		/// </summary>
		BeforeRowCancelUpdate       = 52,


		/// <summary>
		/// event id that identifies CellChange event
		/// </summary>
		CellChange                  = 53,


		/// <summary>
		/// event id that identifies CellListSelect event
		/// </summary>
		CellListSelect              = 54,


		/// <summary>
		/// event id that identifies Click event
		/// </summary>
		Click                       = 55,


		/// <summary>
		/// event id that identifies ClickCellButton event
		/// </summary>
		ClickCellButton             = 56,


		/// <summary>
		/// event id that identifies Error event
		/// </summary>
		Error                       = 57,


		/// <summary>
		/// event id that identifies DblClick event
		/// </summary>
		DblClick                    = 58,


		/// <summary>
		/// event id that identifies InitializeLayout event
		/// </summary>
		InitializeLayout            = 59,


		/// <summary>
		/// event id that identifies InitializeRow event
		/// </summary>
		InitializeRow               = 60,


		/// <summary>
		/// event id that identifies OnSelectionDrag event
		/// </summary>
		OnSelectionDrag             = 61,


		/// <summary>
		/// event id that identifies PostMessageReceived event
		/// </summary>
		// SSP 9/19/03 UWG2398
		// Marked the PostMessageReceived as editor browsable never since this is not
		// being used anywhere. Seems like this was accidently carried over from ActiveX.
		//
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		PostMessageReceived         = 62, 

		/// <summary>
		/// event id that identifies InitializePrintPreview event
		/// </summary>
		InitializePrintPreview		= 63,

		/// <summary>
		/// event id that identifies InitializeLogicalPrintPage event
		/// </summary>
		InitializeLogicalPrintPage	= 64,

		/// <summary>
		/// event id that identifies InitializePrint event
		/// </summary>
		InitializePrint				= 65,

		/// <summary>
		/// event id that identifies BeforePrint event
		/// </summary>
		BeforePrint					= 66,

		/// <summary>
		/// event id that identifies InitializeGroupByRow event
		/// </summary>
		InitializeGroupByRow		= 67,


		// SSP 3/21/02
		// Adde event ids for row filtering
		//
		/// <summary>
		/// event id that identifies FilterRow event
		/// </summary>
		FilterRow					= 68,

		/// <summary>
		/// event id that identifies BeforeCustomRowFilterDialog event. 
		/// </summary>
		BeforeCustomRowFilterDialog	= 69,

		/// <summary>
		/// event id that identifies BeforeRowFilterDropDown event.
		/// </summary>
		BeforeRowFilterDropDown		= 70,

		/// <summary>
		/// Event id that identifies CellDataError event.
		/// </summary>
		CellDataError				= 71,

		// SSP 5/17/02
		// Added event ids for summary rows events.
		//
		/// <summary>
		/// Event id that identifies BeforeSummaryDialog event.
		/// </summary>
		BeforeSummaryDialog			= 72,

		/// <summary>
		/// Event id that identifies SummaryValueChanged event.
		/// </summary>
		SummaryValueChanged			= 73,

		// SSP 6/25/03 UWG2413 - Row Layout Functionality
		// Added BeforeRowLayoutItemResized	and AfterRowLayoutItemResized events.
		//
		/// <summary>
		/// Event id that identifies BeforeRowLayoutItemResized event.
		/// </summary>
		BeforeRowLayoutItemResized	= 74,

		// SSP 6/25/03 UWG2413 - Row Layout Functionality
		// Added BeforeRowLayoutItemResized	and AfterRowLayoutItemResized events.
		//
		/// <summary>
		/// Event id that identifies AfterRowLayoutItemResized event.
		/// </summary>
		AfterRowLayoutItemResized	= 75,

		// SSP 7/30/03 UWG2544
		// Added BeforePerformAction and AfterPerformAction events.
		//
		/// <summary>
		/// Event id that identifies BeforePerformAction event.
		/// </summary>
		BeforePerformAction			= 76,
		
		/// <summary>
		/// Event id that identifies AfterPerformAction event.
		/// </summary>
		AfterPerformAction			= 77,

		// SSP 8/1/03
		// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
		//
		/// <summary>
		/// Event id that identifies BeforeRowFilterChanged event.
		/// </summary>
		BeforeRowFilterChanged		= 78,

		/// <summary>
		/// Event id that identifies AfterRowFilterChanged event.
		/// </summary>
		AfterRowFilterChanged		= 79,

		// SSP 11/10/03 Add Row Feature
		//
		/// <summary>
		/// Event id that identifies InitializeTemplateAddRow event.
		/// </summary>
		InitializeTemplateAddRow	= 80,

		// SSP 12/5/03 UWG2295
		// Added AfterSummaryDialog event.
		//
		/// <summary>
		/// Event id that identifies AfterSummaryDialog event.
		/// </summary>
		AfterSummaryDialog			= 81,

		// SSP 12/5/03 UWG2295
		// Added AfterCardsScroll event.
		//
		/// <summary>
		/// Event id that identifies AfterCardsScroll event.
		/// </summary>
		AfterCardsScroll			= 82,

		// SSP 4/15/04 - Virtual Binding
		// Added BeforeRowFilterDropDownPopulate to give the users a chance to prevent the ultragrid
		// from populating the filter drop down list. This can be useful if there are lot of rows
		// and it takes a long time for the ultragrid to populate the filter drop down list.
		// 
		/// <summary>
		/// Event id that identifies BeforeRowFilterDropDownPopulate event.
		/// </summary>
		BeforeRowFilterDropDownPopulate	= 83,

		// JDN 11/17/04 CardCompressedStateChanged events
		/// <summary>
		/// Event id that identifies BeforeCardCompressedStateChanged event.
		/// </summary>
		BeforeCardCompressedStateChanged	= 84,

		// JDN 11/17/04 CardCompressedStateChanged events
		/// <summary>
		/// Event id that identifies AfterCardCompressedStateChanged event.
		/// </summary>
		AfterCardCompressedStateChanged	= 85,

		// SSP 12/15/04 - IDataErrorInfo Support
		//
		/// <summary>
		/// Event id that identifies BeforeDisplayDataErrorTooltip event.
		/// </summary>
		BeforeDisplayDataErrorTooltip	= 86,

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		// ------------------------------------------------------------------------------
		/// <summary>
		/// Event id that identifies <b>AfterRowFixedStateChanged</b> event.
		/// </summary>
		AfterRowFixedStateChanged		= 87,

		/// <summary>
		/// Event id that identifies <b>BeforeRowFixedStateChanged</b> event.
		/// </summary>
		BeforeRowFixedStateChanged		= 88,

		/// <summary>
		/// Event id that identifies <b>FilterCellValueChanged</b> event.
		/// </summary>
		FilterCellValueChanged			= 89,

		/// <summary>
		/// Event id that identifies <b>InitializeRowsCollection</b> event.
		/// </summary>
		InitializeRowsCollection		= 90,
		// ------------------------------------------------------------------------------

		// JAS v5.2 DoubleClick Events 4/27/05
		// ------------------------------------------------------------------------------
		/// <summary>
		/// Event id that identifies <b>DoubleClickRow</b> event.
		/// </summary>
		DoubleClickRow					= 91,

		/// <summary>
		/// Event id that identifies <b>DoubleClickCell</b> event.
		/// </summary>
		DoubleClickCell					= 92,

		/// <summary>
		/// Event id that identifies <b>DoubleClickHeader</b> event.
		/// </summary>
		DoubleClickHeader				= 93,
		// ------------------------------------------------------------------------------

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		//
		/// <summary>
		/// Event id that identifies <b>BeforeColumnChooserDisplayed</b> event.
		/// </summary>
		BeforeColumnChooserDisplayed	= 94,

		// SSP 10/18/05 - NAS 5.3 Column Chooser
		// 
		/// <summary>
		/// Event id that identifies <b>BeforeBandHiddenChanged</b> event.
		/// </summary>
		BeforeBandHiddenChanged			= 95,

		/// <summary>
		/// Event id that identifies <b>AfterBandHiddenChanged</b> event.
		/// </summary>
		AfterBandHiddenChanged			= 96,

		// SSP 11/22/05 - NAS 6.1 Multi-cell Operations
		// 
		/// <summary>
		/// Event id that identifies <b>BeforeMultiCellOperation</b> event.
		/// </summary>
		BeforeMultiCellOperation = 97,

        // MBS 2/29/08 - RowEditTemplate NA2008 V2
        //
        /// <summary>
        /// Event id that identifies the <b>AfterRowEditTemplateClosed</b> event.
        /// </summary>
        AfterRowEditTemplateClosed = 98,
        //
        /// <summary>
        /// Event id that identifies the <b>AfterRowEditTemplateDisplayed</b> event.
        /// </summary>
        AfterRowEditTemplateDisplayed = 99,
        //
        /// <summary>
        /// Event id that identifies the <b>BeforeRowEditTemplateClosed</b> event.
        /// </summary>
        BeforeRowEditTemplateClosed = 100,
        //
        /// <summary>
        /// Event id that identifies the <b>BeforeRowEditTemplateDisplayed</b> event.
        /// </summary>
        BeforeRowEditTemplateDisplayed = 101,
        //
        /// <summary>
        /// Event id that identifies the <b>RowEditTemplateRequested</b> event.
        /// </summary>
        RowEditTemplateRequested = 102,

        // MBS 7/1/08 - NA2008 V3
        /// <summary>
        /// Event id that identifies the <b>ClickCell</b> event.
        /// </summary>
        ClickCell = 103,

        // CDS NAS v9.1 Header CheckBox
        /// <summary>
        /// Event id that identifies the <b>BeforeHeaderCheckStateChanged</b> event.
        /// </summary>
        BeforeHeaderCheckStateChanged = 104,

        // CDS NAS v9.1 Header CheckBox
        /// <summary>
        /// Event id that identifies the <b>AfterHeaderCheckStateChanged</b> event.
        /// </summary>
        AfterHeaderCheckStateChanged = 105,

		/// <summary>
		/// keep the last event id used
		/// </summary>
		[EditorBrowsable( EditorBrowsableState.Never ) ]
		LastEventId = 105,
	}

	
	/// <summary>
	/// Identifies groups of UltraGrid specific events
	/// </summary>
	public enum EventGroups
	{
		/// <summary>
		/// All events 
		/// </summary>
		AllEvents    = 0,
		/// <summary>
		/// Before events
		/// </summary>
		BeforeEvents = 1,
		/// <summary>
		/// After events
		/// </summary>
		AfterEvents  = 2
	}
	/// <summary>
	/// Used to determine what type of selection change has occurred
	/// </summary>
	internal enum SelectChange
	{	
		/// <summary>
		/// Row Selection Change. One or more rows were selected/deselected
		/// </summary>
		Row	      = 0,
		/// <summary>
		/// Cell Selection Change. One or more cells were selected/deselected
		/// </summary>
		Cell      = 1,
		/// <summary>
		/// Column Selection Change. One or more columns were selected/deselected
		/// </summary>
		Col	      = 2,
		/// <summary>
		/// Group Selection Change. One or more groups were selected/deselected
		/// </summary>
		Group	  = 3,

		/// <summary>
		/// Band Selection Change. One or more bands were selected/deselected
		/// </summary>
		Band	  = 4,

		/// <summary>
		/// Clear All Selections. All rows, columns and cells were deselected
		/// </summary>
		ClearAll  = 5
	}

	/// <summary>
	/// Used to specify what type of position change has occurred
	/// </summary>
	public enum PosChanged
	{	
		/// <summary>
		/// Position Moved. The object's position changed because it was moved
		/// </summary>
		Moved	= 0,

		/// <summary>
		/// Position Swapped. The object's position changed because it was swapped
		/// </summary>
		Swapped	= 1,

		/// <summary>
		/// Position Sized. The object's position changed because it was resized
		/// </summary>
		Sized	= 2,

		// SSP 8/11/05 - NAS 5.3 Column Chooser - BR05521
		// 
		/// <summary>
		/// Hidden state of the column changed. This is fired when the end user changes
		/// the hidden state of a column, for example via the ColumnChooser functionality.
		/// </summary>
		HiddenStateChanged = 3
	}

    /// <summary>
    /// Used to specify how a RowEditTemplate is shown.
    /// </summary>
    public enum TemplateDisplaySource
    {
        /// <summary>
        /// The template is shown manually through code.
        /// </summary>
        Manual,

        /// <summary>
        /// The template is shown by clicking on the icon in a row selector.
        /// </summary>
        RowSelectorImage,

        /// <summary>
        /// The template is shown by entering edit mode of a cell.
        /// </summary>
        OnEnterEditMode,

        /// <summary>
        /// The template is shown by double-clicking the row.
        /// </summary>
        OnRowDoubleClick,
    }

	

	// SSP 5/2/02
	// Added BeforeExitEditModeEventArgs class because we had to pass in more
	// information to BeforeExitEditMode (like whether we are forcing the exit
	// or user has cancelled the edit operation (by pressing Esc for example)).
	// Previously it was just using CancelEvantArgs so added this class instead.
	//
	/// <summary>
	/// Event parameters used for BeforeExitEditMode event.
	/// </summary>
	public class BeforeExitEditModeEventArgs : CancelEventArgs
	{
		private bool forceExit = false;
		private bool cancelingEditOperation = false;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="forceExit">Indicates whether we are forcing the exit.</param>
		/// <param name="cancelingEditOperation">Indicates whether the edit operation is cancelled by the user (for example by pressing Escape key).</param>
		public BeforeExitEditModeEventArgs( bool forceExit, bool cancelingEditOperation )
		{
			this.forceExit = forceExit;
			this.cancelingEditOperation = cancelingEditOperation;
		}

		/// <summary>
		/// Indicates whether cell is being forced to exit the edit mode.
		/// </summary>
		/// <remarks>
		/// <p class="body">When ForceExit is true, value set to Cancel property will be ignored.</p>
		/// <p class="body">This happens when grid is in certain situations where exitting the edit mode is required (like when grid is being disposed). If it's one of those situations, then this property will be true.</p>
		/// </remarks>
		public bool ForceExit
		{
			get
			{
				return this.forceExit;
			}
		}

		/// <summary>
		/// Indicates whether the edit operation is being canceled.
		/// </summary>
		/// <remarks>
		/// <p class="body">CancellingEditOperation is true when the user has canceled the editing the cell, for example by pressing the Escape key.</p>
		/// </remarks>
		public bool CancellingEditOperation
		{
			get
			{
				return this.cancelingEditOperation;
			}
		}
	}

	// SSP 5/16/02
	// Added summary rows events and associated event arg classes namely
	// BeforeSummaryDialogEventArgs and SummaryValueChangedEventArgs
	//
	//
	
	/// <summary>
	/// Used for event parameter to BeforeSummaryDialog event.
	/// </summary>
	public class BeforeSummaryDialogEventArgs : CancelEventArgs
	{
		private UltraGridColumn column = null;

		// SSP 11/5/04 UWG3789
		// Added SummaryDialog property on the BeforeSummaryDialogEventArgs.
		//
		private SummaryDialog summaryDialog = null;

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="column">Column for which the summary dialog is being shown.</param>
        /// <param name="summaryDialog">Returns the summary dialog that will be shown.</param>
		// SSP 11/5/04 UWG3789
		// Added summaryDialog parameter.
		//
		//public BeforeSummaryDialogEventArgs( UltraGridColumn column ) : base( )
		public BeforeSummaryDialogEventArgs( UltraGridColumn column, SummaryDialog summaryDialog ) : base( )
		{
			this.column = column;
			this.summaryDialog = summaryDialog;
		}

		/// <summary>
		/// Column for which the summary dialog is being shown.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		// SSP 11/5/04 UWG3789
		// Added SummaryDialog property on the BeforeSummaryDialogEventArgs.
		//
		/// <summary>
		/// Returns the summary dialog that will be shown.
		/// </summary>
		public SummaryDialog SummaryDialog
		{
			get
			{
				return this.summaryDialog;
			}
		}
	}

	// SSP 12/5/03 UWG2295
	// Added AfterSummaryDialog event.
	// 
	/// <summary>
	/// Event args associated with <see cref="UltraGrid.AfterSummaryDialog"/> event.
	/// </summary>
	public class AfterSummaryDialogEventArgs : EventArgs
	{
		private UltraGridColumn column = null;
		private bool summariesChanged = false;

		/// <summary>
        /// Constructor.
		/// </summary>
        /// <param name="column">Column for which the summary dialog was shown.</param>
        /// <param name="summariesChanged">Indicates whether the user modified any summaries in the dialog.</param>
		public AfterSummaryDialogEventArgs( UltraGridColumn column, bool summariesChanged )
		{
			this.column = column;
			this.summariesChanged = summariesChanged;
		}

		/// <summary>
		/// Column for which the summary dialog was shown.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		/// <summary>
		/// Indicates whether the user modified any summaries in the dialog.
		/// </summary>
		public bool SummariesChanged
		{
			get
			{
				return this.summariesChanged;
			}
		}
	}

	/// <summary>
	/// Used for event parameter to SummaryValueChanged event.
	/// </summary>
	public class SummaryValueChangedEventArgs : EventArgs
	{
		private SummaryValue summaryValue = null;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="summaryValue">Object with information on summary value.</param>
		public SummaryValueChangedEventArgs( SummaryValue summaryValue )
		{
			this.summaryValue = summaryValue;
		}

		/// <summary>
		/// Object with information on summary value.
		/// </summary>
		public SummaryValue SummaryValue
		{
			get
			{
				return this.summaryValue;
			}
		}
	}


	// SSP 3/21/02
	// Added Row Filtering event arg class defs
	//
	#region Row Filtering event arg class defs
    
	#region BeforeRowFilterDropDownEventArgs Class

	/// <summary>
	/// Used for event parameter to BeforeRowFilterDropDown event.
	/// </summary>
	public class BeforeRowFilterDropDownEventArgs : CancelEventArgs
	{
		private UltraGridColumn				column		= null;
		private RowsCollection				rows		= null;
		private Infragistics.Win.ValueList	valueList	= null;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="column">Column associated with the column header flter drop down button was clicked on.</param>
		/// <param name="rows">If <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowFilterMode"/> resolves to SiblingRowsOnly, then the RowsCollection associated with the column header that the filter dropdown button is clicked on will be passed in. Otherwise it will be null.</param>
		/// <param name="filterDropDownContents">The contents of the filter drop down as a ValueList.</param>
		public BeforeRowFilterDropDownEventArgs( 
			UltraGridColumn column,
			RowsCollection rows,
			Infragistics.Win.ValueList filterDropDownContents )
		{
			this.column		= column;
			this.rows		= rows;

			// SSP 8/13/02 UWG1555
			// Typo.
			//
			//this.valueList	= valueList;
			this.valueList	= filterDropDownContents;
		}

		/// <summary>
		/// Column associated with the column header flter drop down button was clicked on.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		/// <summary>
		/// The contents of the filter drop down as a ValueList. You can add or remove items from this 
		/// ValueList.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The contents of the filter drop down as a ValueList. You can add or remove items from this 
		/// ValueList. When an item is added to the value list, it's DataValue will be used for comparision.
		/// DataValue can be a data value or it can be an instance of FilterCondition or ColumnFilter object.
		/// Setting the DataValue to a FilterCondition or ColumnFilter instance allows you to add items
		/// that have custom filter conditions.
		/// </p>
		/// <p><seealso cref="ColumnFilter"/> <seealso cref="FilterCondition"/></p>
		/// </remarks>
		public Infragistics.Win.ValueList ValueList
		{
			get
			{
				return this.valueList;				
			}
		}

		/// <summary>
		/// If <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowFilterMode"/> resolves to SiblingRowsOnly, then the RowsCollection associated with the column header that the filter dropdown button is clicked on will be passed in. Otherwise it will be null.
		/// </summary>
		public RowsCollection Rows
		{
			get
			{
				return this.rows;
			}
		}
	}

	#endregion // BeforeRowFilterDropDownEventArgs Class

	#region BeforeCustomRowFilterDialogEventArgs 

	/// <summary>
	/// Used for event parameter to BeforeCustomRowFilterDialog event.
	/// </summary>
	public class BeforeCustomRowFilterDialogEventArgs : CancelEventArgs
	{
		private UltraGridColumn column = null;
		// SSP 9/17/03 UWG2644
		// Added rows parameter.
		//
		private RowsCollection rows = null;

		// SSP 10/8/03 UWG2459
		// 
		private CustomRowFiltersDialog customRowFiltersDialog = null;
        		
        // SSP 8/1/03
		// Added a column parameter.
		//
		//public BeforeCustomRowFilterDialogEventArgs( ) : base( )
		// SSP 9/17/03 UWG2644
		// Added rows parameter.
		//
		//public BeforeCustomRowFilterDialogEventArgs( UltraGridColumn column ) : base( )
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="column">Column for which the custom filter dialog is being shown.</param>
        /// <param name="rows">The rows collection that's being filtered</param>
        /// <param name="customRowFiltersDialog">The custom row filter dialog.</param>
		public BeforeCustomRowFilterDialogEventArgs( UltraGridColumn column, RowsCollection rows, CustomRowFiltersDialog customRowFiltersDialog ) : base( )
		{
			this.column = column;
			this.rows = rows;
			this.customRowFiltersDialog = customRowFiltersDialog;
		}

		// SSP 8/1/03
		// Added Column property.
		//
		/// <summary>
		/// Column for which the custom filter dialog is being shown.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		/// <summary>
		/// Returns null if the <see cref="UltraGridOverride.RowFilterMode"/> resolves to <b>AllRowsInBand</b> otherwise it returns the rows collection that's being filtered.
		/// </summary>
		/// <remarks>
		/// <p class="body">There are two row filtering modes: <b>SiblingRowsOnly</b> and <b>AllRowsInBand</b>. When it's <b>AllRowsInBand</b> all the rows in a band are being filtered and thus there is no particular rows collection that's being filtered. As a result this property returns null. However if the row filter mode is <b>SiblingRowsOnly</b> where rows of a particular rows collection are being filtered, then this property will return that rows collection.</p>
		/// </remarks>
		public RowsCollection Rows
		{
			get
			{
				return this.rows;
			}
		}

		// SSP 10/8/03 UWG2459
		// Added CustomRowFiltersDialog so the user can change the appearance of the form.
		//
		/// <summary>
		/// Returns the custom row filter dialog.
		/// </summary>
		public CustomRowFiltersDialog CustomRowFiltersDialog
		{
			get
			{
				return this.customRowFiltersDialog;
			}
		}
	}

	#endregion // BeforeCustomRowFilterDialogEventArgs 

	#region FilterRowEventArgs 

	/// <summary>
	/// Used as event parameter for FilterRow event.
	/// </summary>
	public class FilterRowEventArgs : System.EventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridRow row = null;
		
		// SSP 8/1/03 UWG1654 - Filter Action
		// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
		// appearances.
		// Added RowFilteredOut property off the FilterRowEventArgs.
		//
		private bool rowFilteredOut = false;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="row">Row that's being filtered.</param>
		/// <param name="isRowFilteredOut">Whether the row passed filters.</param>
		public FilterRowEventArgs( UltraGridRow row, bool isRowFilteredOut )
		{
			this.row = row;

			// SSP 8/1/03 UWG1654 - Filter Action
			// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
			// appearances.
			// Added RowFilteredOut property off the FilterRowEventArgs.
			//
			// SSP 10/27/04 - Optimizations
			//
			//this.rowFilteredOut = row.IsFilteredOut;
			this.rowFilteredOut = isRowFilteredOut;
		}

		/// <summary>
		/// Row that's being filtered (read-only).
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}

		// SSP 8/1/03 UWG1654 - Filter Action
		// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
		// appearances.
		// Added RowFilteredOut property off the FilterRowEventArgs.
		//
		/// <summary>
		/// Specifies whether the row is filtered out. You can change the value of this property in which case the row's IsFilteredOut state will be changed to the new value.
		/// </summary>
		public bool RowFilteredOut
		{
			get
			{
				return this.rowFilteredOut;
			}
			set
			{
				this.rowFilteredOut = value;
			}
		}
	}

	#endregion // FilterRowEventArgs 

	// SSP 8/1/03
	// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
	// JDN 11/16/04 Added ProcessMode property - Support for Synchronous Sorting and Filtering
	#region BeforeRowFilterChangedEventArgs

	/// <summary>
	/// EventArgs for BeforeRowFilterChanged event.
	/// </summary>
	public class BeforeRowFilterChangedEventArgs : System.ComponentModel.CancelEventArgs
	{
		private ColumnFilter newColumnFilter = null;
		private Infragistics.Win.UltraWinGrid.ProcessMode processMode;

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="newColumnFilter">Changed column filter conditions.</param>
		public BeforeRowFilterChangedEventArgs( ColumnFilter newColumnFilter )
		{
			this.newColumnFilter = newColumnFilter;
			this.processMode = ProcessMode.Lazy;
		}

		/// <summary>
		/// Changed column filter conditions.
		/// </summary>
		public ColumnFilter NewColumnFilter
		{
			get
			{
				return this.newColumnFilter;
			}
		}

		// JDN 11/16/04 Added ProcessMode property
		// Support for Synchronous Sorting and Filtering
		/// <summary>
		/// The process mode for the filter
		/// </summary>
		public ProcessMode ProcessMode
		{
			get { return this.processMode; }
			set { this.processMode = value; }
		}
	}

	#endregion // BeforeRowFilterChangedEventArgs

	#region AfterRowFilterChangedEventArgs

	/// <summary>
	/// EventArgs for AfterRowFilterChanged event.
	/// </summary>
	public class AfterRowFilterChangedEventArgs : EventArgs
	{
		private UltraGridColumn column = null;

		// SSP 3/15/05 - NAS 5.2 Filter Row
		// Added newColumnFilter member variable.
		//
		private ColumnFilter newColumnFilter = null;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="column">The column for which the filters changed.</param>
        /// <param name="newColumnFilter">The new column filter conditions.</param>
		public AfterRowFilterChangedEventArgs( UltraGridColumn column, 
			// SSP 3/15/05 - NAS 5.2 Filter Row
			// Added newColumnFilter parameter.
			//
			ColumnFilter newColumnFilter )
		{
			this.column = column;
			this.newColumnFilter = newColumnFilter;
		}

		/// <summary>
		/// Returns the column for which the filters changed.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		// SSP 3/15/05 - NAS 5.2 Filter Row
		// Added NewColumnFilter property.
		//
		/// <summary>
		/// Returns the new column filter conditions.
		/// </summary>
		public ColumnFilter NewColumnFilter
		{
			get
			{
				return this.newColumnFilter;
			}
		}
	}

	#endregion // AfterRowFilterChangedEventArgs

	#region BeforeRowFilterDropDownPopulateEventArgs Class

	// SSP 4/15/04 - Virtual Binding
	// Added BeforeRowFilterDropDownPopulate to give the users a chance to prevent the ultragrid
	// from populating the filter drop down list. This can be useful if there are lot of rows
	// and it takes a long time for the ultragrid to populate the filter drop down list.
	// 
	/// <summary>
	/// EventArgs for BeforeRowFilterDropDownPopulate event.
	/// </summary>
	public class BeforeRowFilterDropDownPopulateEventArgs : EventArgs
	{
		private UltraGridColumn				column		= null;
		private RowsCollection				rows		= null;
		private Infragistics.Win.ValueList	valueList	= null;
		private bool						handled		= false;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="column">Column associated with the column header flter drop down button was clicked on.</param>
		/// <param name="rows">If <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowFilterMode"/> resolves to SiblingRowsOnly, then the RowsCollection associated with the column header that the filter dropdown button is clicked on will be passed in. Otherwise it will be null.</param>
		/// <param name="filterDropDown">The value list that will be used as the filter drop down.</param>
		public BeforeRowFilterDropDownPopulateEventArgs( 
			UltraGridColumn column,
			RowsCollection rows,
			Infragistics.Win.ValueList filterDropDown )
		{
			this.column		= column;
			this.rows		= rows;
			this.valueList	= filterDropDown;
		}

		/// <summary>
		/// Column associated with the column header flter drop down button was clicked on.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		/// <summary>
		/// The value list that will be used as the filter drop down. You can populate the value list and set Handled to true to prevent the UltraGrid from populating the value list.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// You can add items to this ValueList. When an item is added to the value list, it's DataValue will be used for comparision.
		/// DataValue can be a data value or it can be an instance of FilterCondition or ColumnFilter object.
		/// Setting the DataValue to a FilterCondition or ColumnFilter instance allows you to add items
		/// that have custom filter conditions.
		/// </p>
		/// <p><seealso cref="ColumnFilter"/> <seealso cref="FilterCondition"/></p>
		/// </remarks>
		public Infragistics.Win.ValueList ValueList
		{
			get
			{
				return this.valueList;				
			}
		}

		/// <summary>
		/// If <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowFilterMode"/> resolves to SiblingRowsOnly, then the RowsCollection associated with the column header that the filter dropdown button is clicked on will be passed in. Otherwise it will be null.
		/// </summary>
		public RowsCollection Rows
		{
			get
			{
				return this.rows;
			}
		}

		/// <summary>
		/// Indicates whether the value list was populated and that UltraGrid should not attempt to pupulate the value list. Default value is false.
		/// </summary>
		/// <remarks>
		/// <p class="body">If the value list is populated set <b>Handled</b> to true. Failing to do so will result in UltraGrid clearing the value list and populating it with the cell values.</p>
		/// </remarks>
		public bool Handled
		{
			get
			{
				return this.handled;
			}
			set
			{
				this.handled = value;
			}
		}
	}

	#endregion // BeforeRowFilterDropDownPopulateEventArgs Class

	#endregion // Row Filtering event arg class defs

	/// <summary>
	/// Event parameters used for events that take RowScrollRegion as its argument
	/// </summary>
	public class RowScrollRegionEventArgs : System.EventArgs
	{
		private Infragistics.Win.UltraWinGrid.RowScrollRegion rowScrollRegion;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="rowScrollRegion"></param>
		public RowScrollRegionEventArgs( Infragistics.Win.UltraWinGrid.RowScrollRegion rowScrollRegion )
		{
			this.rowScrollRegion    = rowScrollRegion; //RowScrollRegion instance
		}

		/// <summary>
		/// The row scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowScrollRegion RowScrollRegion
		{
			get
			{
				return this.rowScrollRegion;
			}
		}

	}

	
	
	/// <summary>
	/// Event parameters used for events that take ColScrollRegion as their argument
	/// </summary>
	public class ColScrollRegionEventArgs : System.EventArgs
	{
		private Infragistics.Win.UltraWinGrid.ColScrollRegion colScrollRegion;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="colScrollRegion">The ColScrollRegion</param>
		public ColScrollRegionEventArgs( Infragistics.Win.UltraWinGrid.ColScrollRegion colScrollRegion )
		{
			this.colScrollRegion    = colScrollRegion; //ColScrollRegion instance
		}

		/// <summary>
		/// The col scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColScrollRegion ColScrollRegion
		{
			get
			{
				return this.colScrollRegion;
			}
		}

	}


	/// <summary>
	/// Event parameters used for the BeforeDropDown event
	/// </summary>
	public class CancelableDropDownEventArgs : CancelEventArgs
	{
		private Control owner;

		/// <summary>
		/// Constructor
		/// </summary>
		public CancelableDropDownEventArgs( Control owner ): base()
		{
			this.owner = owner;
		}

		/// <summary>
		/// owner
		/// </summary>
		public Control Owner
		{
			get
			{
				return this.owner;
			}
		}

	}


	/// <summary>
	/// Event parameters used for the AfterDropDown and AfterCloseUp event
	/// </summary>
	public class DropDownEventArgs : System.EventArgs
	{
		private Control owner;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="owner">The owner control.</param>
		public DropDownEventArgs( Control owner )
		{
			this.owner = owner;
		}

		/// <summary>
		/// owner
		/// </summary>
		public Control Owner
		{
			get
			{
				return this.owner;
			}
		}

	}

	/// <summary>
	/// Event parameters used for the AfterDropDown and AfterCloseUp event
	/// </summary>
	public class RowSelectedEventArgs : System.EventArgs
	{
		private Control owner;
		private UltraGridRow		row;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="owner">The owning control.</param>
        /// <param name="row">The selected row.</param>
		public RowSelectedEventArgs( Control owner, UltraGridRow row )
		{
			this.owner = owner;
			this.row = row;
		}

		/// <summary>
		/// owner
		/// </summary>
		public Control Owner
		{
			get
			{
				return this.owner;
			}
		}

		/// <summary>
		/// selected row
		/// </summary>
		public UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}

	}




	
	/// <summary>
	/// Event parameters used for the AfterSelectChange event
	/// </summary>
	public class AfterSelectChangeEventArgs : System.EventArgs
	{
		private System.Type type;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="type"></param>
		public AfterSelectChangeEventArgs( System.Type type )
		{
			this.type = type;
		}

		/// <summary>
		/// The select change (read-only). Indicates whether the change is selection of
		/// cells, rows, group-by rows or columns took place.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>Type</b> property value depends on whether change in the selection of 
		/// cells (UltraGridCell), rows (UltraGridRow), group-by rows (UltraGridGroupByRow)
		/// or columns (ColumnHeader) took place.
		/// </p>
		/// </remarks>
		public System.Type Type
		{
			get
			{
				return this.type;
			}
		}
	}


	
	/// <summary>
	/// Event parameters used for the AfterGroupPosChangedEventArgs event
	/// </summary>
	public class AfterGroupPosChangedEventArgs : EventArgs 
	{		
		private Infragistics.Win.UltraWinGrid.PosChanged  posChanged;
		private GroupHeader[] groupHeaders;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="posChanged">position changed value</param>
        /// <param name="groupHeaders">groups</param>
		public AfterGroupPosChangedEventArgs( Infragistics.Win.UltraWinGrid.PosChanged posChanged,
			GroupHeader[] groupHeaders )
		{
			this.posChanged = posChanged;
			this.groupHeaders   = groupHeaders;
		}

		/// <summary>
		/// position changed value (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.PosChanged PosChanged
		{
			get
			{
				return this.posChanged;
			}
		}

		/// <summary>
		/// groups (read-only)
		/// </summary>
		public GroupHeader[] GroupHeaders 
		{
			get
			{
				return this.groupHeaders;
			}
		}

	}


	
	/// <summary>
	/// Event parameters used for the AfterColPosChanged event
	/// </summary>
	public class AfterColPosChangedEventArgs : EventArgs 
	{		
		private Infragistics.Win.UltraWinGrid.PosChanged  posChanged;
		private ColumnHeader[] columnHeaders;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="posChanged">postion changed value</param>
        /// <param name="columnHeaders">columns</param>
		public AfterColPosChangedEventArgs( Infragistics.Win.UltraWinGrid.PosChanged posChanged,
			ColumnHeader[] columnHeaders )
		{
			this.posChanged   = posChanged;
			this.columnHeaders   = columnHeaders;
		}

		/// <summary>
		/// postion changed value (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.PosChanged PosChanged
		{
			get
			{
				return this.posChanged;
			}
		}

		/// <summary>
		/// columns (read-only)
		/// </summary>
		public ColumnHeader[] ColumnHeaders
		{
			get
			{
				return this.columnHeaders;
			}
		}

	}


	
	/// <summary>
	/// Event parameters used for the BeforeSelectChange event
	/// </summary>
	public class BeforeSelectChangeEventArgs : CancelEventArgs
	{
		private System.Type type;
		private Selected newSelections;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="type">Select change value</param>
        /// <param name="newSelections">New selections</param>
		public BeforeSelectChangeEventArgs( System.Type type,
			Selected newSelections )
		{
			this.type    = type;
			this.newSelections   = newSelections;
		}

		/// <summary>
		/// select change value (read-only)
		/// </summary>
		public System.Type Type
		{
			get
			{
				return this.type;
			}
		}

		/// <summary>
		/// new selections (read-only)
		/// </summary>
		public Selected NewSelections 
		{
			get
			{
				return this.newSelections;
			}
		}

	}


	
	/// <summary>
	/// Event parameters used for the events that take Band object as its arguments
	/// </summary>
	public class BandEventArgs : System.EventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridBand band;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="band"></param>
		public BandEventArgs( Infragistics.Win.UltraWinGrid.UltraGridBand band )
		{
			this.band    = band;
		}

		/// <summary>
		/// The associated band (read-only).
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridBand Band
		{
			get
			{
				return this.band;
			}
		}
	}

	// SSP 10/18/05 - NAS 5.3 Column Chooser
	// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
	// TestAdvantage.
	// 
	/// <summary>
	/// Event parameters used for the events that take Band object as its arguments
	/// </summary>
	public class CancelableBandEventArgs : System.ComponentModel.CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridBand band;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="band">The associated band</param>
		public CancelableBandEventArgs( Infragistics.Win.UltraWinGrid.UltraGridBand band )
		{
			this.band    = band;
		}

		/// <summary>
		/// The associated band (read-only).
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridBand Band
		{
			get
			{
				return this.band;
			}
		}
	}

	// SSP 10/18/05 - NAS 5.3 Column Chooser
	// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
	// TestAdvantage.
	// 
	/// <summary>
	/// EventArgs class associated with <see cref="UltraGridBase.BeforeBandHiddenChanged"/> event.
	/// </summary>
	public class BeforeBandHiddenChangedEventArgs : CancelableBandEventArgs
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="band"></param>
		public BeforeBandHiddenChangedEventArgs( Infragistics.Win.UltraWinGrid.UltraGridBand band ) : base( band )
		{
		}
	}

	// SSP 10/18/05 - NAS 5.3 Column Chooser
	// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
	// TestAdvantage.
	// 
	/// <summary>
	/// EventArgs class associated with <see cref="UltraGridBase.AfterBandHiddenChanged"/> event.
	/// </summary>
	public class AfterBandHiddenChangedEventArgs : BandEventArgs
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="band"></param>
		public AfterBandHiddenChangedEventArgs( Infragistics.Win.UltraWinGrid.UltraGridBand band ) : base( band )
		{
		}
	}

	
	/// <summary>
	/// Event parameters used for events that take Row as its argument
	/// </summary>
	public class RowEventArgs : System.EventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridRow row;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="row"></param>
		public RowEventArgs( Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			this.row    = row;
		}

		/// <summary>
		/// The row (usually the row that the cell belongs to) (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}

	}

	
	/// <summary>
	/// Event parameters used for the BeforeGroupPosChanged event
	/// </summary>
	public class BeforeGroupPosChangedEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.PosChanged posChanged;
		private GroupHeader[] groupHeaders;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="posChanged">The position changed</param>
        /// <param name="groupHeaders">Groups</param>
		public BeforeGroupPosChangedEventArgs( Infragistics.Win.UltraWinGrid.PosChanged posChanged, 
			GroupHeader[] groupHeaders)
		{
			this.posChanged = posChanged;
			this.groupHeaders = groupHeaders;
		}

		/// <summary>
		/// The position changed (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.PosChanged PosChanged
		{
			get
			{
				return this.posChanged;
			}
		}

		/// <summary>
		/// groups (read-only)
		/// </summary>
		public GroupHeader[] GroupHeaders 
		{
			get
			{
				return this.groupHeaders;
			}
		}
	}


	
	/// <summary>
	/// Event parameters used for the BeforeColRegionRemoved event
	/// </summary>
	public class BeforeColRegionRemovedEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.ColScrollRegion colScrollRegion;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="colScrollRegion">The column scroll region</param>
		public BeforeColRegionRemovedEventArgs(Infragistics.Win.UltraWinGrid.ColScrollRegion colScrollRegion)
		{
			this.colScrollRegion    = colScrollRegion;
		}

		/// <summary>
		/// The column scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColScrollRegion ColScrollRegion
		{
			get
			{
				return this.colScrollRegion;
			}
		}

	}

	
	/// <summary>
	/// Event parameters used for the BeforeColRegionSplit event
	/// </summary>
	public class BeforeColRegionSplitEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.ColScrollRegion originalColScrollRegion;
		private Infragistics.Win.UltraWinGrid.ColScrollRegion newColScrollRegion;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="originalColScrollRegion">The original column scroll region</param>
        /// <param name="newColScrollRegion">The new column scroll region</param>
		public BeforeColRegionSplitEventArgs(
			Infragistics.Win.UltraWinGrid.ColScrollRegion originalColScrollRegion,
			Infragistics.Win.UltraWinGrid.ColScrollRegion newColScrollRegion)

		{
			this.originalColScrollRegion    = originalColScrollRegion;
			this.newColScrollRegion    = newColScrollRegion;
		}

		/// <summary>
		/// The new column scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColScrollRegion NewColScrollRegion
		{
			get
			{
				return this.newColScrollRegion;
			}
		}
		
		/// <summary>
		/// The original column scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColScrollRegion OriginalColScrollRegion
		{
			get
			{
				return this.originalColScrollRegion;
			}
		}
	}


	
	/// <summary>
	/// Event parameters used for the BeforeColRegionScroll event
	/// </summary>
	public class BeforeColRegionScrollEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.ColScrollRegion newState;
		private Infragistics.Win.UltraWinGrid.ColScrollRegion oldState;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="newState">The new state column scroll region</param>
        /// <param name="oldState">The old state column scroll region</param>
		public BeforeColRegionScrollEventArgs(
			Infragistics.Win.UltraWinGrid.ColScrollRegion newState,
			Infragistics.Win.UltraWinGrid.ColScrollRegion oldState)
		{
			this.newState    = newState;
			this.oldState    = oldState;
		}

		/// <summary>
		/// The new state column scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColScrollRegion NewState
		{
			get
			{
				return this.newState;
			}
		}
		
		/// <summary>
		/// The old state column scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColScrollRegion OldState
		{
			get
			{
				return this.oldState;
			}
		}
	}

	// SSP 6/25/03 UWG2413 - Row Layout Functionality
	// Added events that we need to fire when a cell or a header is resized in the row-layout mode.
	// Added BeforeRowLayoutItemResizedEventArgs class below.
	//
	/// <summary>
	/// Event parameters used for the BeforeRowLayoutItemResized event.
	/// </summary>
	public class BeforeRowLayoutItemResizedEventArgs : CancelEventArgs
	{
		#region Private Variables

		private Size oldSize = Size.Empty;
		private Size newSize = Size.Empty;
		private Infragistics.Win.Layout.ILayoutItem item = null;

		// MRS 5/24/05 - BR04224
		private int oldSpan = 0;
		private int newSpan = 0;
		private RowLayoutItemResizeType rowLayoutItemResizeType = RowLayoutItemResizeType.SizeChange;

		#endregion // Private Variables

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="item">Row-Layout item being resized.</param>
		/// <param name="oldSize">Old size.</param>
		/// <param name="newSize">New size.</param>
		public BeforeRowLayoutItemResizedEventArgs( 
			Infragistics.Win.Layout.ILayoutItem item,
			Size oldSize,
			Size newSize )
		{
			this.item = item;
			this.oldSize = oldSize;
			this.newSize = newSize;				
		}

		// MRS 5/24/05 - BR04224
		// Added a new Constructor which takes span information. 

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="item">Row-Layout item being resized.</param>
		/// <param name="oldSize">Old size.</param>
		/// <param name="newSize">New size.</param>
		/// <param name="resizeType">The type of resize operation taking place.</param>
		/// <param name="oldSpan">Old Span</param>
		/// <param name="newSpan">New Span</param>
		public BeforeRowLayoutItemResizedEventArgs( 
			Infragistics.Win.Layout.ILayoutItem item,
			Size oldSize,
			Size newSize, 
			RowLayoutItemResizeType resizeType,
			int oldSpan, 
			int newSpan)
		{
			this.item = item;
			this.oldSize = oldSize;
			this.newSize = newSize;	
	
			this.rowLayoutItemResizeType = resizeType;
			this.oldSpan = oldSpan;
			this.newSpan = newSpan;
		}

		#endregion // Constructor

		#region RowLayoutItem

		/// <summary>
		/// Row-Layout item being resized. This can be either an UltraGridColumn or a ColumnHeader. It will be UltraGridColumn if the item being resized is a cell. It will be ColumnHeader if the item being resized is a header.
		/// </summary>
		public Infragistics.Win.Layout.ILayoutItem RowLayoutItem
		{
			get
			{
				return this.item;
			}
		}

		#endregion // RowLayoutItem

		#region OldSize

		/// <summary>
		/// Gets the old size of the layout item.
		/// </summary>
		public Size OldSize
		{
			get
			{
				return this.oldSize;
			}
		}

		#endregion // OldSize

		#region NewSize

		/// <summary>
		/// Gets or sets the new size of the layout item.
		/// </summary>
		public Size NewSize
		{
			get
			{
				return this.newSize;
			}
			set
			{
				if ( value.Width < 0 || value.Width < 0 )
					throw new ArgumentOutOfRangeException( "NewSize", value, "Width or Height of the size can't be less than 0!" );

				this.newSize = value;
			}
		}

		#endregion // NewSize

		// MRS 5/24/05 - BR04224
		#region OldSpan
		/// <summary>
		/// Gets the old Span of the layout item.
		/// </summary>
		/// <remarks>
		/// <p class="body">To determine if this is a SpanX or SpanY, see the <see cref="ResizeType"/> property.</p>
		/// <p class="body">This is not used when when <see cref="ResizeType"/> is <b>SizeChange</b> or <b>SpanYChange</b>.</p>
		/// </remarks>
		public int OldSpan
		{
			get { return this.oldSpan; }
		}
		#endregion OldSpan

		// MRS 5/24/05 - BR04224
		#region NewSpan

		/// <summary>
		/// Gets or sets the new Span of the layout item.
		/// </summary>		
		/// <remarks>
		/// <p class="body">To determine if this is a SpanX or SpanY, see the <see cref="ResizeType"/> property.</p>
		/// <p class="body">This is not used when when <see cref="ResizeType"/> is <b>SizeChange</b> or <b>SpanYChange</b>.</p>
		/// </remarks>
		public int NewSpan
		{
			get { return this.newSpan; }
			set { this.newSpan = value; }
		}

		#endregion // NewSpan

		// MRS 5/24/05 - BR04224
		#region ResizeType
		/// <summary>
		/// Gets the type of Resize operation being performed.
		/// </summary>
		public RowLayoutItemResizeType ResizeType
		{
			get { return this.rowLayoutItemResizeType; }
		}
		#endregion ResizeType

	}

	// SSP 6/25/03 UWG2413 - Row Layout Functionality
	// Added events that we need to fire when a cell or a header is resized in the row-layout mode.
	// Added AfterRowLayoutItemResizedEventArgs class below.
	//
	/// <summary>
	/// Event parameters used for the AfterRowLayoutItemResized event.
	/// </summary>
	public class AfterRowLayoutItemResizedEventArgs : EventArgs
	{
		#region Private Variables

		private Infragistics.Win.Layout.ILayoutItem item = null;

		// MRS 5/24/05 - BR04224
		private RowLayoutItemResizeType rowLayoutItemResizeType = RowLayoutItemResizeType.SizeChange;

		#endregion // Private Variables

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="item">Row-Layout item being resized.</param>
		public AfterRowLayoutItemResizedEventArgs( Infragistics.Win.Layout.ILayoutItem item )
		{
			this.item = item;
		}

		// MRS 5/24/05 - BR04224
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="item">Row-Layout item being resized.</param>
		/// <param name="resizeType">The type of resize operation taking place.</param>
		public AfterRowLayoutItemResizedEventArgs( Infragistics.Win.Layout.ILayoutItem item, RowLayoutItemResizeType resizeType ) : this(item)
		{
			this.rowLayoutItemResizeType = resizeType;
		}

		#endregion // Constructor

		#region RowLayoutItem

		/// <summary>
		/// Row-Layout item being resized. This can be either an UltraGridColumn or a ColumnHeader. It will be UltraGridColumn if the item being resized is a cell. It will be ColumnHeader if the item being resized is a header.
		/// </summary>
		public Infragistics.Win.Layout.ILayoutItem RowLayoutItem
		{
			get
			{
				return this.item;
			}
		}

		#endregion // RowLayoutItem

		// MRS 5/24/05 - BR04224
		#region ResizeType
		/// <summary>
		/// Gets the type of Resize operation being performed.
		/// </summary>
		public RowLayoutItemResizeType ResizeType
		{
			get { return this.rowLayoutItemResizeType; }
		}
		#endregion ResizeType
	}

	
	/// <summary>
	/// Event parameters used for the BeforeRowRegionRemoved event
	/// </summary>
	public class BeforeRowRegionRemovedEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.RowScrollRegion rowScrollRegion;
		
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="rowScrollRegion"></param>
		public BeforeRowRegionRemovedEventArgs(Infragistics.Win.UltraWinGrid.RowScrollRegion rowScrollRegion)
		{
			this.rowScrollRegion    = rowScrollRegion;
		}

		/// <summary>
		/// The row scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowScrollRegion RowScrollRegion
		{
			get
			{
				return this.rowScrollRegion;
			}
		}
	}


	
	// JDN 11/16/04 Added ProcessMode property
	// Support for Synchronous Sorting and Filtering
	/// <summary>
	/// Event parameters used for the BeforeSortChange event
	/// </summary>
	public class BeforeSortChangeEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridBand band;
		private Infragistics.Win.UltraWinGrid.SortedColumnsCollection sortedColumns;
		private Infragistics.Win.UltraWinGrid.ProcessMode processMode;
		
		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="band">The band</param>
        /// <param name="sortedColumns">The sorted columns</param>
		public BeforeSortChangeEventArgs(
			Infragistics.Win.UltraWinGrid.UltraGridBand band,
			Infragistics.Win.UltraWinGrid.SortedColumnsCollection sortedColumns)
		{
			this.band           = band;
			this.sortedColumns  = sortedColumns;
			this.processMode    = ProcessMode.Lazy;
		}

		/// <summary>
		/// The band (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridBand Band
		{
			get
			{
				return this.band;
			}
		}
		/// <summary>
		/// The sorted columns (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.SortedColumnsCollection SortedColumns
		{
			get
			{
				return this.sortedColumns;
			}
		}

		// JDN 11/16/04 Added ProcessMode property
		// Support for Synchronous Sorting and Filtering
		/// <summary>
		/// The process mode for the sort
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ProcessMode ProcessMode
		{
			get { return this.processMode; }
			set { this.processMode = value; }
		}        
	}

	
	/// <summary>
	/// Event parameters used for the BeforeRowInsert event
	/// </summary>
	public class BeforeRowInsertEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridBand band;
		private Infragistics.Win.UltraWinGrid.UltraGridRow parentRow;		
		
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="band">The band</param>
        /// <param name="parentRow">The parent row</param>
		public BeforeRowInsertEventArgs(
			Infragistics.Win.UltraWinGrid.UltraGridBand band,
			Infragistics.Win.UltraWinGrid.UltraGridRow parentRow)
		{
			this.band    = band;
			this.parentRow = parentRow;
		}

		/// <summary>
		/// The band (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridBand Band
		{
			get
			{
				return this.band;
			}
		}
		/// <summary>
		/// The parent row (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow ParentRow
		{
			get
			{
				return this.parentRow;
			}
		}
	}

	
	/// <summary>
	/// Event parameters used for the BeforeRowResize event
	/// </summary>
	public class BeforeRowResizeEventArgs : CancelEventArgs
	{
		int newHeight;
		private Infragistics.Win.UltraWinGrid.UltraGridRow row;		
		
		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="row">The row</param>
        /// <param name="newHeight">The new height</param>
		public BeforeRowResizeEventArgs(			
			Infragistics.Win.UltraWinGrid.UltraGridRow row,
			int newHeight)
		{
			this.row    = row;
			this.newHeight = newHeight;
		}

		/// <summary>
		/// The new height (read-only)
		/// </summary>
		public int NewHeight
		{
			get
			{
				return this.newHeight;
			}
		}

		/// <summary>
		/// The row (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}
	}

	
	/// <summary>
	/// Event parameters used for the BeforeRowsDeleted event
	/// </summary>
	public class BeforeRowsDeletedEventArgs : CancelEventArgs
	{        
		private Infragistics.Win.UltraWinGrid.UltraGridRow[] rows;		
		private bool displayPromptMsg;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="rows">The rows</param>
        /// <param name="displayPromptMsg">Whether to display prompt message.</param>
		public BeforeRowsDeletedEventArgs(			
			Infragistics.Win.UltraWinGrid.UltraGridRow[] rows,
			bool displayPromptMsg)
		{
			this.rows    = rows;
			this.displayPromptMsg = displayPromptMsg;
		}

		/// <summary>
		/// The rows (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow[] Rows
		{
			get
			{
				return this.rows;
			}
		}

		/// <summary>
		/// Whether to display prompt message.
		/// </summary>
		public bool DisplayPromptMsg
		{
			get
			{
				return this.displayPromptMsg;
			}
			// SSP 11/7/01 UWG655
			// DisplayPromptMsg should be writable.
			//
			set
			{
				this.displayPromptMsg = value;
			}
		}
	}


	
	/// <summary>
	/// Event parameters used for the BeforeRowRegionScroll event
	/// </summary>
	public class BeforeRowRegionScrollEventArgs : CancelEventArgs
	{        
		private Infragistics.Win.UltraWinGrid.RowScrollRegion newState;		
		private Infragistics.Win.UltraWinGrid.RowScrollRegion oldState;		

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="newState">The new state row scroll region</param>
        /// <param name="oldState">The old state row scroll region</param>
		public BeforeRowRegionScrollEventArgs(		
			Infragistics.Win.UltraWinGrid.RowScrollRegion newState,
			Infragistics.Win.UltraWinGrid.RowScrollRegion oldState)
		{
			this.newState	= newState;
			this.oldState	= oldState;
		}

		/// <summary>
		/// The new state row scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowScrollRegion NewState
		{
			get
			{
				return this.newState;
			}
		}

		/// <summary>
		/// The old state row scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowScrollRegion OldState
		{
			get
			{
				return this.oldState;
			}
		}
	}


	
	/// <summary>
	/// Event parameters used for the BeforeRowRegionSize event
	/// </summary>
	public class BeforeRowRegionSizeEventArgs : CancelEventArgs
	{        
		private Infragistics.Win.UltraWinGrid.RowScrollRegion region1;		
		private Infragistics.Win.UltraWinGrid.RowScrollRegion region2;	
	
		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="region1">The row scroll region 1</param>
        /// <param name="region2">The row scroll region 2</param>
		public BeforeRowRegionSizeEventArgs(		
			Infragistics.Win.UltraWinGrid.RowScrollRegion region1,
			Infragistics.Win.UltraWinGrid.RowScrollRegion region2)
		{
			this.region1	= region1;
			this.region2	= region2;
		}

		/// <summary>
		/// The row scroll region 1 (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowScrollRegion Region1
		{
			get
			{
				return this.region1;
			}
		}

		/// <summary>
		/// The row scroll region 2 (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowScrollRegion Region2
		{
			get
			{
				return this.region2;
			}
		}
	}

	
	
	/// <summary>
	/// Event parameters used for the BeforeRowRegionSplit event
	/// </summary>
	public class BeforeRowRegionSplitEventArgs : CancelEventArgs
	{        
		private Infragistics.Win.UltraWinGrid.RowScrollRegion originalRowScrollRegion;		
		private Infragistics.Win.UltraWinGrid.RowScrollRegion newRowScrollRegion;
		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="originalRowScrollRegion">The original row scroll region</param>
        /// <param name="newRowScrollRegion">The new row scroll region </param>
		public BeforeRowRegionSplitEventArgs(		
			Infragistics.Win.UltraWinGrid.RowScrollRegion originalRowScrollRegion,
			Infragistics.Win.UltraWinGrid.RowScrollRegion newRowScrollRegion)
		{
			this.originalRowScrollRegion = originalRowScrollRegion;
			this.newRowScrollRegion	= newRowScrollRegion;
		}

		/// <summary>
		/// The new row scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowScrollRegion NewRowScrollRegion
		{
			get
			{
				return this.newRowScrollRegion;
			}
		}

		/// <summary>
		/// The original row scroll region (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowScrollRegion OriginalRowScrollRegion
		{
			get
			{
				return this.originalRowScrollRegion;
			}
		}
	}


	
	/// <summary>
	/// Event parameters used for the BeforeCellUpdate event
	/// </summary>
	public class BeforeCellUpdateEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridCell cell;		
		private object newValue;

        // CDS 6/18/09 TFS17819 Keep track of the row and column, so we can create the cell when needed
        private UltraGridColumn column;
        private UltraGridRow row;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="cell">The cell</param>
        /// <param name="newValue">The new value of the cell</param>
		public BeforeCellUpdateEventArgs(			
			Infragistics.Win.UltraWinGrid.UltraGridCell cell,
			object newValue)
		{
			this.cell    = cell;
			this.newValue = newValue;
		}

        // CDS 6/18/09 TFS17819 Added a new constructor that doesn't force the Cell object to be created
        internal BeforeCellUpdateEventArgs(
            Infragistics.Win.UltraWinGrid.UltraGridRow row,
            UltraGridColumn column,
            object newValue)
        {
            this.row = row;
            this.column = column;
            this.newValue = newValue;
        }

		/// <summary>
		/// The cell (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridCell Cell
		{
			get
			{
                // CDS 6/18/09 TFS17819 Create the cell from the row and column is necessary
                if (this.cell == null &&
                    this.row != null &&
                    this.column != null)
                    this.cell = this.row.Cells[column];

				return this.cell;
			}
		}
		/// <summary>
		/// The new value of the cell (read-only)
		/// </summary>
		public object NewValue
		{
			get
			{
				return this.newValue ;
			}
		}
	}


	
	/// <summary>
	/// Event parameters used for the BeforeColRegionSize event
	/// </summary>
	public class BeforeColRegionSizeEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.ColScrollRegion region1;
		private Infragistics.Win.UltraWinGrid.ColScrollRegion region2;
		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="region1">The column scroll region 1</param>
        /// <param name="region2">The column scroll region 2</param>
		public BeforeColRegionSizeEventArgs(
			Infragistics.Win.UltraWinGrid.ColScrollRegion region1,
			Infragistics.Win.UltraWinGrid.ColScrollRegion region2)
		{
			this.region1    = region1;
			this.region2    = region2;
		}

		/// <summary>
		/// The column scroll region 1 (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColScrollRegion Region1
		{
			get
			{
				return this.region1;
			}
		}
		
		/// <summary>
		/// The column scroll region 2 (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColScrollRegion Region2
		{
			get
			{
				return this.region2;
			}
		}
	}


	
	/// <summary>
	/// Event parameters used for the BeforeColPosChanged event
	/// </summary>
	public class BeforeColPosChangedEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.PosChanged posChanged;
		private ColumnHeader[] columnHeaders;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="posChanged">The position changed action</param>
        /// <param name="columnHeaders">The selected columns</param>
		public BeforeColPosChangedEventArgs(
			Infragistics.Win.UltraWinGrid.PosChanged posChanged,
			ColumnHeader[] columnHeaders) //selected columns
		{
			this.posChanged    = posChanged;
			this.columnHeaders  = columnHeaders;
		}

		/// <summary>
		/// The position changed action (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.PosChanged PosChanged
		{
			get
			{
				return this.posChanged;
			}
		}


		/// <summary>
		/// The selected columns (read-only)
		/// </summary>
		public ColumnHeader[] ColumnHeaders
		{
			get
			{
				return this.columnHeaders;
			}
		}
	}


	
	/// <summary>
	/// Event parameters used for the BeforeCellActivate event
	/// </summary>
	public class CancelableRowEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridRow row;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="row">The row that the cell belongs to.</param>
		public CancelableRowEventArgs( Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			this.row    = row;
		}

		/// <summary>
		/// The row that the cell belongs to (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}

	}

	
	/// <summary>
	/// Class for handling data error info. 
	/// </summary>
	public class DataErrorInfo
	{	
		private Exception	exception	= null;
		private UltraGridRow row			= null;
		private UltraGridCell		cell		= null;
		private string		errorText	= null;
		private object		invalidValue = null;
		private DataErrorSource source	= DataErrorSource.Unspecified;

		/// <summary>
		/// Constructor.
		/// </summary>
		internal DataErrorInfo( )
		{
		}

		/// <summary>
		/// Constructor..
		/// Pass in whatever applies and null for others
		/// </summary>
		/// <param name="exception"></param>
		/// <param name="invalidValue"></param>
		/// <param name="row"></param>
		/// <param name="cell"></param>
		/// <param name="source"></param>
		/// <param name="errorText"></param>
		internal DataErrorInfo( Exception exception, object invalidValue, UltraGridRow row, UltraGridCell cell, DataErrorSource source, 
			string errorText )
		{
			this.exception = exception;
			this.invalidValue = invalidValue;
			this.row = row;
			this.cell = cell;
			this.source = source;
			this.errorText = errorText;
		}

		/// <summary>
		/// If the data error is caused by an exception, then this property will
		/// return that exception object.
		/// </summary>
		public Exception Exception
		{
			get
			{
				return this.exception;
			}
		}		

		/// <summary>
		/// Row associated with the DataError. If no row was associated
		/// then this will return null.
		/// </summary>
		public UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}

		/// <summary>
		/// Cell associated with this DataError. If no cell was associated with
		/// this DataErorr, it will return null.
		/// </summary>
		public UltraGridCell Cell
		{
			get
			{
				return this.cell;
			}
		}

		/// <summary>
		/// Error message associated with this error. 
		/// </summary>
		public string ErrorText
		{
			get
			{
				return this.errorText;
			}
		}

		/// <summary>
		/// Returns the invalid value, if any, associated with this DataError
		/// event.
		/// </summary>
		public object InvalidValue
		{
			get
			{
				return this.invalidValue;
			}
		}

		/// <summary>
		/// Source of the DataError.
		/// </summary>
		public DataErrorSource Source
		{
			get
			{
				return this.source;
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//// SSP 6/13/05
		//// Typo. The parameter name should have been invalidvalue as the statement inside
		//// the constructor expects it to be.
		//// 
		////internal void SetInvalidValue( object invalidvalue )
		//internal void SetInvalidValue( object invalidValue )
		//{
		//    this.invalidValue = invalidValue;
		//}

		//internal void SetCell( UltraGridCell cell )
		//{
		//    this.cell = cell;
		//}

		//internal void SetRow( UltraGridRow row )
		//{
		//    this.row = row;
		//}

		//internal void SetException( Exception exception )
		//{
		//    this.exception = exception;
		//}

		//internal void SetSource( DataErrorSource source )
		//{
		//    this.source = source;
		//}

		#endregion Not Used
	}

	/// <summary>
	/// class for handling mask error info
	/// </summary>
	public class MaskErrorInfo
	{
		private string	invalidText = null;
		private int		startPos	= -1;
		private bool	cancelBeep	= false;

		internal MaskErrorInfo( string invalidText, int startPos)
		{
			this.invalidText = invalidText;
			this.startPos = startPos;
		}

		/// <summary>
		/// The text that did not match the mask
		/// </summary>
		public string InvalidText
		{
			get
			{
				return this.invalidText;
			}
		}

		/// <summary>
		/// Position where the invalid char is in InvalidText
		/// </summary>
		public int StartPos
		{
			get
			{
				return this.startPos;
			}
		}

		/// <summary>
		/// Set this property to false to prevent the control from beeping
		/// </summary>
		public bool CancelBeep
		{
			get
			{
				return this.cancelBeep;
			}
			set
			{
				this.cancelBeep = value;
			}
		}
	}

	#region MultiCellOperationErrorInfo Class

	// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
	// 
	/// <summary>
	/// Contains information on the error that occurred during a multi-cell operation.
	/// </summary>
	/// <remarks>
	/// <seealso cref="ErrorEventArgs.MultiCellOperationErrorInfo"/>
	/// </remarks>
	public class MultiCellOperationErrorInfo
	{
		#region ErrorAction Enum

		/// <summary>
		/// Used for specifying the <see cref="MultiCellOperationErrorInfo.Action"/> property.
		/// </summary>
		public enum ErrorAction
		{
			/// <summary>
			/// Continue the operation with remaining cells. This option leaves the value of the cell to
			/// its current value.
			/// </summary>
			Continue,

			/// <summary>
			/// Continue the operation with remaining cells. This option clears out the value of the cell.
			/// </summary>
			ClearCellAndContinue,

			/// <summary>
			/// Stop the operation.
			/// </summary>
			Stop,

			/// <summary>
			/// Stop the operation and revert the previously pasted cells.
			/// </summary>
			Revert
		}

        #endregion // ErrorAction Enum

		#region Private Vars

		private ErrorAction action = ErrorAction.ClearCellAndContinue;
		private UltraGridCell errorCell;
		private Exception exception;
		private MultiCellOperation operation;
		private DataErrorInfo dataErrorInfo = null;
		private bool canContinueWithRemainingCells = false;

		#endregion // Private Vars

		#region Constructor

		internal MultiCellOperationErrorInfo( 
			MultiCellOperation operation,
			UltraGridCell errorCell,
			Exception exception,
			DataErrorInfo dataErrorInfo,
			bool canContinueWithRemainingCells )
		{
			this.operation = operation;
			this.errorCell = errorCell;
			this.exception = exception;
			this.dataErrorInfo = dataErrorInfo;
			this.canContinueWithRemainingCells = canContinueWithRemainingCells;
		}

		#endregion // Constructor

		#region Action

		/// <summary>
		/// Specifies whether to continue with remaining cells, stop or revert the cells modified so far.
		/// Default is <b>ClearCellAndContinue</b>, unless explicitly stated otherwise in the remarks
		/// section below.
		/// <b>Note</b> that this property is honored only if the error occurs with a particular cell. 
		/// If the error is with the whole operation, then this property has no effect.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// During the process of performing multi-cell operations, a cell can have an error. For example,
		/// when pasting values to cells, a cell might not accept the value being pasted to it because it's
		/// the wrong data type, or it doesn't satisfy the column constraints. In that case the 
		/// <see cref="UltraGrid.Error"/> event will be generated. In such a case you can choose to take one 
		/// of the actions listed in the <see cref="ErrorAction"/>
		/// enum. You can get the cell that had the error using the <see cref="ErrorCell"/> property.
		/// Also note that you can use the <see cref="UltraGrid.BeforeMultiCellOperation"/> event to perform
		/// custom conversions of values being copied or pasted.
		/// </p>
		/// <p class="body">
		/// If however the <b>Error</b> event is fired in response to an error that is not associated with
		/// a specific cell, then this property has no effect. For example, when pasting, if the number of
		/// cells available for pasting is smaller than the number of cells being pasted, then the <i>Error</i>
		/// event will be fired. However such an error is not associated with a specific cell, but rather
		/// it applies to the whole operation. In such a case this property won't be honored since the whole
		/// operations can't be performed. <b>Note</b> that in such a scenario, none of the cells will be
		/// modified.
		/// </p>
		/// <p class="body">
		/// <b>Note:</b> If you want to prevent the UltraGrid from displaying an error message, set the
		/// <b>Cancel</b> to true on the ErrorEventArgs. Alternatively you can also set the 
		/// <see cref="ErrorEventArgs.ErrorText"/> to null or empty string to prevent the UltraGrid from
		/// displaying the error dialog. This lets you display a custom error message box.
		/// </p>
		/// <p class="body">
		/// The default value of this property is <b>ClearCellAndContinue</b> except for read-only cells
		/// in which case the default value is <b>Continue</b> since the values of read-only cells can not
		/// be cleared.
		/// </p>
		/// </remarks>
		public ErrorAction Action
		{
			get
			{
				return this.action;
			}
			set
			{
				if ( this.action != value )
				{
					GridUtils.ValidateEnum( typeof( ErrorAction ), value );

					this.action = value;
				}
			}
		}

		#endregion // Action

		#region ErrorCell

		/// <summary>
		/// The cell where the operation being performed failed. Note that this property will
		/// return null if the failure is not associated with a specific cell.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// See remarks section of <see cref="Action"/> property for more information.
		/// </p>
		/// <see cref="Action"/>
		/// </remarks>
		public UltraGridCell ErrorCell
		{
			get
			{
				return this.errorCell;
			}
		}

		#endregion // ErrorCell

		#region Exception

		/// <summary>
		/// The exception, if any, that lead to this error event.
		/// </summary>
		public Exception Exception
		{
			get
			{
				return this.exception;
			}
		}

		#endregion // Exception

		#region Operation

		/// <summary>
		/// Specifies the multi-cell operation that's being performed.
		/// </summary>
		public MultiCellOperation Operation
		{
			get
			{
				return this.operation;
			}
		}

		#endregion // Operation

		#region DataErrorInfo

		/// <summary>
		/// If the error is data source related (for example, error updating the row), then
		/// this will return an object that contains further information on the error. 
		/// Otherwise it will be null.
		/// </summary>
		public DataErrorInfo DataErrorInfo
		{
			get
			{
				return this.dataErrorInfo;
			}
		}

		#endregion // DataErrorInfo

		#region GetErrorMsgBoxTitle

		internal string GetErrorMsgBoxTitle( )
		{
			string sr = null;

			switch ( this.Operation )
			{
				case MultiCellOperation.Copy:
					sr = "MultiCell_Copy_Error_MsgBox_Title";
					break;
				case MultiCellOperation.Cut:
					sr = "MultiCell_Cut_Error_MsgBox_Title";
					break;
				case MultiCellOperation.Delete:
					sr = "MultiCell_Delete_Error_MsgBox_Title";
					break;
				case MultiCellOperation.Paste:
					sr = "MultiCell_Paste_Error_MsgBox_Title";
					break;
				case MultiCellOperation.Redo:
					sr = "MultiCell_Redo_Error_MsgBox_Title";
					break;
				case MultiCellOperation.Undo:
					sr = "MultiCell_Undo_Error_MsgBox_Title";
					break;
			}

			return null != sr ? SR.GetString( sr ) : null;
		}

		#endregion // GetErrorMsgBoxTitle

		#region GetErrorMsgBoxMessage

		internal string GetErrorMsgBoxMessage( )
		{
			string furtherInformationStr = null != this.Exception ? this.Exception.Message : string.Empty;
			if ( furtherInformationStr.Length > 0 )
				furtherInformationStr = SR.GetString( "MultiCellOperation_Error_MsgBox_FurtherInformation", furtherInformationStr );

			string continueQuestion = this.CanContinueWithRemainingCells
				? SR.GetString( "MultiCellOperation_Error_MsgBox_ContinueQuestion" )
				: string.Empty;

			string sr = null;

			switch ( this.Operation )
			{
				case MultiCellOperation.Copy:
					sr = "MultiCell_Copy_Error_MsgBox_Message";
					break;
				case MultiCellOperation.Cut:
					sr = "MultiCell_Cut_Error_MsgBox_Message";
					break;
				case MultiCellOperation.Delete:
					sr = "MultiCell_Delete_Error_MsgBox_Message";
					break;
				case MultiCellOperation.Paste:
					sr = "MultiCell_Paste_Error_MsgBox_Message";
					break;
				case MultiCellOperation.Redo:
					sr = "MultiCell_Redo_Error_MsgBox_Message";
					break;
				case MultiCellOperation.Undo:
					sr = "MultiCell_Undo_Error_MsgBox_Message";
					break;
			}

			return null != sr ? SR.GetString( sr, furtherInformationStr, continueQuestion ) : null;
		}

		#endregion // GetErrorMsgBoxMessage

		#region CanContinueWithRemainingCells

		/// <summary>
		/// Specifies whether the operation can continue with remaining cells.
		/// </summary>
		public bool CanContinueWithRemainingCells
		{
			get
			{
				return this.canContinueWithRemainingCells;
			}
		}

		#endregion // CanContinueWithRemainingCells
	}

	#endregion // MultiCellOperationErrorInfo Class

	#region ErrorEventArgs Class

	/// <summary>
	/// Event parameters used for handling data error events
	/// </summary>
	public class ErrorEventArgs : CancelEventArgs
	{
		#region Private Vars

		private DataErrorInfo		dataErrorInfo	= null;
		private MaskErrorInfo		maskErrorInfo	= null;
		private ErrorType			errorType		= ErrorType.Generic;
		private string				errorDescription = null;

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
		// 
		private MultiCellOperationErrorInfo multiCellOperationErrorInfo = null;

		#endregion // Private Vars

		#region Constructor

		internal ErrorEventArgs( DataErrorInfo dataErrorInfo )
		{
			this.errorType = ErrorType.Data;
			this.dataErrorInfo = dataErrorInfo;
			this.errorDescription = dataErrorInfo.ErrorText;
		}

		internal ErrorEventArgs( MaskErrorInfo maskErrorInfo, string errorDescription )
		{
			this.errorType = ErrorType.Mask;
			this.maskErrorInfo = maskErrorInfo;
			this.errorDescription = errorDescription;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal ErrorEventArgs( string description )
		//{
		//    this.errorType = ErrorType.Generic;
		//    this.errorDescription = description;
		//}

		#endregion Not Used

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
		// 
		internal ErrorEventArgs( MultiCellOperationErrorInfo multiCellOperationErrorInfo )
		{
			GridUtils.ValidateNull( multiCellOperationErrorInfo );

			this.errorType = ErrorType.MultiCellOperation;
			this.multiCellOperationErrorInfo = multiCellOperationErrorInfo;

			this.errorDescription = this.multiCellOperationErrorInfo.GetErrorMsgBoxMessage( );
		}

        // MRS 4/27/2009 - TFS16619
        // Added a new constructor for Generic and Printer errors. 
        //
        internal ErrorEventArgs(ErrorType errorType, string errorDescription)
        {
            this.errorType = errorType;
            this.errorDescription = errorDescription;
        }

		#endregion // Constructor

        

		#region ErrorText

		/// <summary>
		/// Gets or sets the error text. This is the text that will be displayed in the error dialog box.
		/// You can modify this property to change what is displayed in the error dialog box. To prevent
		/// the error dialog box from displaying, set the Cancel to true.
		/// </summary>
		public String ErrorText
		{
			get
			{
				return this.errorDescription;
			}
			set
			{
				if ( null == value )

					throw new ArgumentNullException( SR.GetString("LER_Exception_309"), 
						SR.GetString("LER_Exception_333") );

				this.errorDescription = value;
			}
		}

		#endregion // ErrorText

		#region DataErrorInfo 

		/// <summary>
		/// If this error event was fired due to a data error, this will return
		/// the DataErrorInfo instance associated with it. Otherwise it will
		/// return null.
		/// </summary>
		public DataErrorInfo DataErrorInfo 
		{
			get
			{
				return this.dataErrorInfo;
			}
		}

		#endregion // DataErrorInfo 

		#region MaskErrorInfo

		/// <summary>
		/// If this error event was fired due to a mask error, this will return
		/// the MaskErrorInfo instance associated with it. Otherwise it will
		/// return null.
		/// </summary>
		public MaskErrorInfo MaskErrorInfo	
		{
			get
			{
				return this.maskErrorInfo;
			}
		}

		#endregion // MaskErrorInfo

		#region ErrorType

		// SSP 6/14/02 UWG1178
		// Made this property a public property.
		/// <summary>
		/// Indicates what type of error occurred that prompted this Error event to be fired.
		/// </summary>
		/// <remarks>
		/// <p class="body">Depending on what type of error lead to firing of this event, all or all except one of DataErrorInfo, MaskErrorInfo and MultiCellOperationErrorInfo will be null. If the ErrorType is Generic, all of DataErrorInfo, MaskErroInfo and MultiCellOperationErrorInfo will be null. If error type is Data, then DataErrorInfo will contain a valid DataErrorInfo object. If it's a Mask error then MaskErrorInfo will return a valid MaskErroInfo object.</p>
		/// </remarks>
		//internal ErrorType ErrorType
		public ErrorType ErrorType
		{
			get
			{
				return this.errorType;
			}
		}

		#endregion // ErrorType

		#region MultiCellOperationErrorInfo

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
		// 
		/// <summary>
		/// This object contains information on the error that occurred during a multi-cell operation.
		/// </summary>
		/// <remarks>
		/// <seealso cref="MultiCellOperationErrorInfo"/>
		/// </remarks>
		public MultiCellOperationErrorInfo MultiCellOperationErrorInfo
		{
			get
			{
				return this.multiCellOperationErrorInfo;
			}
		}

		#endregion // MultiCellOperationErrorInfo
	}

	#endregion // ErrorEventArgs Class

	#region BeforeMultiCellOperationEventArgs Class

	// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
	// 
	/// <summary>
	/// Event args associated with <see cref="UltraGrid.BeforeMultiCellOperation"/> event.
	/// </summary>
	public class BeforeMultiCellOperationEventArgs : CancelEventArgs
	{
		private MultiCellOperation operation;
		private MultiCellOperationInfo.CellValuesCollection newValues;

		/// <summary>
		/// Constructor. Initializes a new instance of <see cref="BeforeMultiCellOperationEventArgs"/> class.
		/// </summary>
        /// <param name="operation">The operation being performed.</param>
        /// <param name="newValues">New values of cells.</param>
		public BeforeMultiCellOperationEventArgs( 
			MultiCellOperation operation,
			MultiCellOperationInfo.CellValuesCollection newValues )
		{
			this.operation = operation;
			this.newValues = newValues;
		}

		/// <summary>
		/// The operation being performed.
		/// </summary>
		public MultiCellOperation Operation
		{
			get
			{
				return this.operation;
			}
		}

		/// <summary>
		/// Cells involved in the clipboard operation.
		/// </summary>
		public MultiCellOperationInfo.CellsCollection Cells
		{
			get
			{
				return this.newValues.Cells;
			}
		}

		/// <summary>
		/// New values of cells.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// New values of cells. This has different meaning depending on which operation is 
		/// being performed. When copy operation is being performed, this contains the text 
		/// representation of the cell values. You can write custom logic to convert the cell 
		/// values to text by getting the original cell value from the Cell object (from the 
		/// <b>Cells</b> collection) and perform a custom conversion. When pasting, this contains 
		/// the text representation of the cell value from the clipboard. You can convert the 
		/// value to the cell�s data type using custom logic. After converting, set the new value 
		/// back in this collection. When cutting, the new values 
		/// are all DBNull. When performing Undo or Redo, the raw cell values are the values that are 
		/// going to be set on the cells.
		/// </p>
		/// <p class="body">
		/// To traverse through the cell values, traverse through the <see cref="Cells"/> collection
		/// and index into the <b>CellValues</b> using the interating cell. See the following
		/// example.
		/// </p>
		/// </remarks>
		public MultiCellOperationInfo.CellValuesCollection NewValues
		{
			get
			{
				return this.newValues;
			}
		}
	}

	#endregion // BeforeMultiCellOperationEventArgs Class    
	
	/// <summary>
	/// Event parameters used for handling data error events
	/// </summary>
	public class CellDataErrorEventArgs : EventArgs
	{
		private bool forceExit;
		private bool stayInEditMode;
		private bool raiseErrorEvent;
		private bool restoreOriginalValue;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="forceExit"></param>
		internal CellDataErrorEventArgs( bool forceExit )
		{
			this.forceExit = forceExit;
			this.stayInEditMode = !forceExit ? true : false;
			this.raiseErrorEvent = true;
			this.restoreOriginalValue = false;
		}

		/// <summary>
		/// True if the cell is being forced to exit edit mode. If this is true then StayInEditMode will be ignored.
		/// </summary>
		public bool ForceExit
		{
			get
			{
				return this.forceExit;
			}
		}

		/// <summary>
		/// Indicates whether to stay in edit mode. Default value is true.
		/// </summary>
		/// <remarks>
		/// <p class="body">If StayInEditMode is true, then the cell won't exit the edit mode unless the ForceExit is true in which case the cell will exit the edit mode regardless.</p>
		/// </remarks>
		public bool StayInEditMode
		{
			get
			{
				return this.stayInEditMode;
			}
			set
			{
				this.stayInEditMode = value;
			}
		}

		/// <summary>
		/// Indicates whether to raise Error event. Default value is true.
		/// </summary>
		/// <remarks>
		/// <p class="body">If RaiseErrorEvent is true, then Error event will be fired. If this is false, then Error event will not be fired.</p>
		/// </remarks>
		public bool RaiseErrorEvent
		{
			get
			{
				return this.raiseErrorEvent;
			}
			set
			{
				this.raiseErrorEvent = value;
			}
		}

		/// <summary>
		/// Whether to restore the value in the cell to original value. Default value is true.
		/// </summary>
		/// <remarks>
		/// <p class="body">RestoreOriginalValue indicates whether to restore the original value in the cell.</p>
		/// </remarks>
		public bool RestoreOriginalValue
		{
			get
			{
				return this.restoreOriginalValue;
			}
			set
			{
				this.restoreOriginalValue = value;
			}
		}		
	}


	
	/// <summary>
	/// Event parameters used for events that take AutoSizeEdit as their arguments 
	/// and are cancelable
	/// </summary>
	public class CancelableAutoSizeEditEventArgs : CancelEventArgs
	{
		private Infragistics.Win.DefaultableBoolean autoSizeEdit;
		private int startHeight = 0;
		private int maxHeight	= 0;		
		private int startWidth	= 0;
		private int maxWidth	= 0;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="autoSizeEdit"></param>
		public CancelableAutoSizeEditEventArgs ( Infragistics.Win.DefaultableBoolean autoSizeEdit )
		{
			this.autoSizeEdit    = autoSizeEdit;
		}

		// SSP 9/3/02
		// Marked this property EditorBrowsable.Never since it's obsolete.
		//
		/// <summary>
		/// The AutoSizeEdit (read-only).
		/// </summary>
		/// <remarks>
		/// <p class="body">This property has been deprecated. Just set the Cancel property to true to prevent the cell from performing AutoSizeEdit.</p>
		/// </remarks>
		[ Obsolete( "EditControl property is obsolete. Set the Cancel property to true to prevent the cell from performing AutoSizeEdit.", false ) ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public Infragistics.Win.DefaultableBoolean AutoSizeEdit
		{
			get
			{
				return this.autoSizeEdit;
			}
		}

		/// <summary>
		/// Gets or sets the starting height of the edit control.
		/// </summary>
		public int StartHeight
		{
			get
			{
				return this.startHeight;
			}
			set
			{
				this.startHeight = value;
			}
		}

		/// <summary>
		/// Gets or sets the starting width of the edit control.
		/// </summary>
		public int StartWidth
		{
			get
			{
				return this.startWidth;
			}
			set
			{
				this.startWidth = value;
			}
		}

		/// <summary>
		/// Gets or sets the max height of the edit control.
		/// </summary>
		public int MaxHeight
		{
			get
			{
				return this.maxHeight;
			}
			set
			{
				this.maxHeight = value;
			}
		}

		/// <summary>
		/// Gets or sets the max wdith of the edit control.
		/// </summary>
		public int MaxWidth
		{
			get
			{
				return this.maxWidth;
			}
			set
			{
				this.maxWidth = value;
			}
		}
	}

	

	/// <summary>
	/// Event parameters used for Logical Print Page event
	/// </summary>
	public class CancelableLogicalPrintPageEventArgs : CancelEventArgs
	{
		private int logicalPageNumber;
		private PrintDocument printDocument;
		private LogicalPageLayoutInfo logicalPageLayoutInfo;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="printDocument">The PrintDocument</param>
        /// <param name="logicalPageLayoutInfo">Layout info of the current logical page</param>
        /// <param name="logicalPageNumber">Current logical page number</param>
		public CancelableLogicalPrintPageEventArgs( PrintDocument printDocument, LogicalPageLayoutInfo logicalPageLayoutInfo,
			int logicalPageNumber )
		{
			this.logicalPageLayoutInfo = logicalPageLayoutInfo;
			this.logicalPageNumber = logicalPageNumber;
			this.printDocument = printDocument;
			this.logicalPageLayoutInfo.InInitializePrintPageEvent = true;
		}

		/// <summary>
		/// Layout info of the current logical page
		/// </summary>
		public LogicalPageLayoutInfo LogicalPageLayoutInfo
		{
			get
			{
				return this.logicalPageLayoutInfo;
			}
		}

		/// <summary>
		/// Current logical page number (read-only)
		/// </summary>
		public int LogicalPageNumber
		{
			get
			{
				return this.logicalPageNumber;				
			}
			
		}

		/// <summary>
		/// Returns the PrintDocument's Collate value (read-only)
		/// </summary>
		public bool Collate
		{
			get
			{
				return this.printDocument.PrinterSettings.Collate;
			}
		}


		/// <summary>
		/// Returns the PrintDocument's Copies value (read-only)
		/// </summary>
		public short Copies
		{
			get
			{
				return this.printDocument.PrinterSettings.Copies;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's name (read-only)
		/// </summary>
		public string DocumentName
		{
			get
			{
				return this.printDocument.DocumentName;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's Margin Top value (read-only). 
		/// You can set the margins in the <see cref="UltraGrid.InitializePrint"/>
		/// and <see cref="UltraGrid.InitializePrintPreview"/> events.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This property is read-only. You can set the margins in the <see cref="UltraGrid.InitializePrint"/>
		/// and <see cref="UltraGrid.InitializePrintPreview"/> events using the associated event arg's 
		/// PrintDocument's DefaultPageSetting's Margins property. Note that this means that margins 
		/// can not be changed on a par page basis.
		/// </p>
		/// </remarks>
		public int MarginTop
		{
			get
			{
				return this.printDocument.DefaultPageSettings.Margins.Top;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's Margin Bottom value (read-only).
		/// You can set the margins in the <see cref="UltraGrid.InitializePrint"/>
		/// and <see cref="UltraGrid.InitializePrintPreview"/> events.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This property is read-only. You can set the margins in the <see cref="UltraGrid.InitializePrint"/>
		/// and <see cref="UltraGrid.InitializePrintPreview"/> events using the associated event arg's 
		/// PrintDocument's DefaultPageSetting's Margins property. Note that this means that margins 
		/// can not be changed on a par page basis.
		/// </p>
		/// </remarks>
		public int MarginBottom
		{
			get
			{
				return this.printDocument.DefaultPageSettings.Margins.Bottom;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's Margin Left value (read-only).
		/// You can set the margins in the <see cref="UltraGrid.InitializePrint"/>
		/// and <see cref="UltraGrid.InitializePrintPreview"/> events.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This property is read-only. You can set the margins in the <see cref="UltraGrid.InitializePrint"/>
		/// and <see cref="UltraGrid.InitializePrintPreview"/> events using the associated event arg's 
		/// PrintDocument's DefaultPageSetting's Margins property. Note that this means that margins 
		/// can not be changed on a par page basis.
		/// </p>
		/// </remarks>
		public int MarginLeft
		{
			get
			{
				return this.printDocument.DefaultPageSettings.Margins.Left;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's Margin Right value (read-only).
		/// You can set the margins in the <see cref="UltraGrid.InitializePrint"/>
		/// and <see cref="UltraGrid.InitializePrintPreview"/> events.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This property is read-only. You can set the margins in the <see cref="UltraGrid.InitializePrint"/>
		/// and <see cref="UltraGrid.InitializePrintPreview"/> events using the associated event arg's 
		/// PrintDocument's DefaultPageSetting's Margins property. Note that this means that margins 
		/// can not be changed on a par page basis.
		/// </p>
		/// </remarks>
		public int MarginRight
		{
			get
			{
				return this.printDocument.DefaultPageSettings.Margins.Right;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's Landscape value (read-only)
		/// </summary>
		public bool Landscape
		{
			get
			{
				return this.printDocument.DefaultPageSettings.Landscape;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's PrintRange value (read-only)
		/// </summary>
		public PrintRange PrintRange
		{
			get
			{
				return this.printDocument.PrinterSettings.PrintRange;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's PaperSize Width value (read-only)
		/// </summary>
		public int PaperWidth
		{
			get
			{
				return this.printDocument.DefaultPageSettings.PaperSize.Width;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's PaperSize Height value (read-only)
		/// </summary>
		public int PaperHeight
		{
			get
			{
				return this.printDocument.DefaultPageSettings.PaperSize.Height;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's SupportsColor value (read-only)
		/// </summary>
		public bool PrintColors
		{
			get
			{
				return this.printDocument.PrinterSettings.SupportsColor;
			}
		}

		/// <summary>
		/// Returns the PrintDocument's PrinterName value (read-only)
		/// </summary>
		public string PrinterName
		{
			get
			{
				return this.printDocument.PrinterSettings.PrinterName;
			}
		}

	}


	/// <summary>
	/// Event parameters used for initialize print event, and
	/// before print event
	/// </summary>
	public class CancelablePrintEventArgs : CancelEventArgs
	{
		private PrintDocument printDocument;
		private LogicalPageLayoutInfo logicalPageLayoutInfo;

		// SSP 12/15/03 UWG2560
		// Added PrintLayout property to InitializePrint and InitializePrintPreview event args.
		//
		private UltraGridLayout printLayout;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="printDocument">The PrintDocument</param>
        /// <param name="logicalPageLayoutInfo">The default layout info, for all logical pages.</param>
        /// <param name="printLayout">The print layout.</param>
        // SSP 12/15/03 UWG2560
		// Added PrintLayout property to InitializePrint and InitializePrintPreview event args.
		//
		//public CancelablePrintEventArgs( PrintDocument printDocument, LogicalPageLayoutInfo logicalPageLayoutInfo )
		public CancelablePrintEventArgs( PrintDocument printDocument, 
			LogicalPageLayoutInfo logicalPageLayoutInfo, UltraGridLayout printLayout )
		{
			this.printLayout = printLayout;

			this.printDocument = printDocument;
			this.logicalPageLayoutInfo = logicalPageLayoutInfo;
			this.logicalPageLayoutInfo.InInitializePrintPageEvent = false;
		}

		// SSP 12/15/03 UWG2560
		// Added PrintLayout property to InitializePrint and InitializePrintPreview event args.
		//
		/// <summary>
		/// Returns the print layout.
		/// </summary>
		public UltraGridLayout PrintLayout
		{
			get
			{
				return this.printLayout;
			}
		}

		/// <summary>
		/// The print document (read-only)
		/// </summary>
		public PrintDocument PrintDocument
		{
			get
			{
				return this.printDocument;
			}
		}

		/// <summary>
		/// The default layout info, for all logical pages
		/// </summary>
		public LogicalPageLayoutInfo DefaultLogicalPageLayoutInfo
		{
			get
			{
				return this.logicalPageLayoutInfo;
			}
		}

	}

	/// <summary>
	/// Event parameters used for Initialize Print Preview event
	/// </summary>
	public class CancelablePrintPreviewEventArgs : CancelablePrintEventArgs
	{
		private PrintPreviewSettings printPreviewSettings = null;


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="printDocument">The PrintDocument</param>
        /// <param name="printPreviewSettings">Settings used to alter the print preview dialog</param>
        /// <param name="logicalPageLayoutInfo">The default layout info, for all logical pages.</param>
        /// <param name="printLayout">The print layout.</param>
        // SSP 12/15/03 UWG2560
		// Added PrintLayout property to InitializePrint and InitializePrintPreview event args.
		// Added printLayout parameter.
		//
		public CancelablePrintPreviewEventArgs( PrintDocument printDocument, LogicalPageLayoutInfo logicalPageLayoutInfo, 
			PrintPreviewSettings printPreviewSettings, UltraGridLayout printLayout )
			:base( printDocument, logicalPageLayoutInfo, printLayout )
		{			
			this.printPreviewSettings = printPreviewSettings;
		}

		/// <summary>
		/// Settings used to alter the print preview dialog
		/// </summary>
		public PrintPreviewSettings PrintPreviewSettings
		{
			get
			{
				return this.printPreviewSettings;
			}			
		}
	}



	
	/// <summary>
	/// Event parameters used for events that take a row and column designating
	/// a cell (intersection of the row and the column)
	/// </summary>
	public class CellEventArgs : System.EventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridCell cell;

        // CDS 6/18/09 TFS17819 Keep track of the row and column, so we can create the cell when needed
        private UltraGridColumn column;
        private UltraGridRow row;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="cell">The UltraGridCell</param>
		public CellEventArgs( Infragistics.Win.UltraWinGrid.UltraGridCell cell )
		{
			this.cell = cell;
		}

        // CDS 6/18/09 TFS17819 Overload that doesnt force the Cell to be created
        internal CellEventArgs(UltraGridRow row, UltraGridColumn column)
        {
            this.row = row;
            this.column = column;
        }

		/// <summary>
		/// Returns a reference to the cell of interest.
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridCell Cell
		{
			get
            {
                // CDS 6/18/09 TFS17819 Create the cell from the row and column is necessary
                if (this.cell == null &&
                    this.row != null &&
                    this.column != null)
                    this.cell = this.row.Cells[column];
				return this.cell;
			}
		}
	}


	
	/// <summary>
	/// Event parameters used for events that take a row and a column
	/// designating a cell (the cell at the intersection of the row and the 
	/// column) as their argument and are cancelable
	/// </summary>
	public class CancelableCellEventArgs : CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridCell cell;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="cell"></param>
		public CancelableCellEventArgs( Infragistics.Win.UltraWinGrid.UltraGridCell cell )
		{
			this.cell = cell;
		}

		/// <summary>
		/// Returns a reference to the cell of interest.
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridCell Cell
		{
			get
			{
				return this.cell;
			}
		}
	}

	
	/// <summary>
	/// Event parameters used the InitializeLayout event.
	/// </summary>
	public class InitializeLayoutEventArgs : System.EventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridLayout layout;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="layout"></param>
		public InitializeLayoutEventArgs( Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		{
			if ( null == layout )
				throw new System.ArgumentNullException( Shared.SR.GetString("LE_System.ArgumentNullException_125") );

			this.layout            = layout;
		}

		/// <summary>
		/// <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout"/>
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridLayout Layout
		{
			get
			{
				return this.layout;
			}
		}

	}

	
	/// <summary>
	/// Event parameters used the InitializeRow event.
	/// </summary>
	public class InitializeRowEventArgs : System.EventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridRow row;
		private bool reInitialize;   

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="row">The row being initialized.</param>
        /// <param name="reInitialize">True if the row has already been initialized.</param>
		public InitializeRowEventArgs( Infragistics.Win.UltraWinGrid.UltraGridRow row, bool reInitialize )
		{
			if ( null == row )
				throw new System.ArgumentNullException( Shared.SR.GetString("LE_System.ArgumentNullException_126") );

			this.row            = row;
			this.reInitialize   = reInitialize;
		}

		/// <summary>
		/// The row being initialized (read-only) 
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}

		/// <summary>
		/// True if the row has already been initialized (read-only) 
		/// </summary>
		public bool ReInitialize
		{
			get
			{
				return this.reInitialize;
			}
		}

	}


	
	/// <summary>
	/// Event parameters used the InitializeRow event.
	/// </summary>
	public class InitializeGroupByRowEventArgs : System.EventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridGroupByRow row;
		private bool reInitialize;   

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="row">The group by row being initialized.</param>
        /// <param name="reInitialize">True if the row has already been initialized.</param>
		public InitializeGroupByRowEventArgs( Infragistics.Win.UltraWinGrid.UltraGridGroupByRow row, bool reInitialize )
		{
			if ( null == row )
				throw new System.ArgumentNullException( Shared.SR.GetString("LE_System.ArgumentNullException_126") );

			this.row            = row;
			this.reInitialize   = reInitialize;
		}

		/// <summary>
		/// The group by row being initialized (read-only) 
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridGroupByRow Row
		{
			get
			{
				return this.row;
			}
		}

		/// <summary>
		/// True if the row has already been initialized (read-only) 
		/// </summary>
		public bool ReInitialize
		{
			get
			{
				return this.reInitialize;
			}
		}
	}

	// SSP 7/30/03 UWG2544
	// Added BeforeUltraGridPerformActionEventArgs and AfterUltraGridPerformActionEventArgs 
	// classes.
	//
	/// <summary>
	/// EventArgs class used for BeforeUltraGridPerformAction event.
	/// </summary>
	public class BeforeUltraGridPerformActionEventArgs : System.ComponentModel.CancelEventArgs
	{
		private UltraGridAction ultraGridAction;

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="ultraGridAction">The action that's about to be performed.</param>
		public BeforeUltraGridPerformActionEventArgs( UltraGridAction ultraGridAction )
		{
			this.ultraGridAction = ultraGridAction;
		}

		/// <summary>
		/// The action that's about to be performed.
		/// </summary>
		public UltraGridAction UltraGridAction
		{
			get
			{
				return this.ultraGridAction;
			}
		}
	}

	/// <summary>
	/// EventArgs class used for AfterUltraGridPerformAction event.
	/// </summary>
	public class AfterUltraGridPerformActionEventArgs : System.EventArgs
	{
		private UltraGridAction ultraGridAction;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="ultraGridAction"></param>
		public AfterUltraGridPerformActionEventArgs( UltraGridAction ultraGridAction )
		{
			this.ultraGridAction = ultraGridAction;
		}

		/// <summary>
		/// The action that's been performed.
		/// </summary>
		public UltraGridAction UltraGridAction
		{
			get
			{
				return this.ultraGridAction;
			}
		}
	}

	#region InitializeTemplateAddRowEventArgs

	// SSP 11/10/03 Add Row Feature
	//
	/// <summary>
	/// Event args associated with <see cref="UltraGrid.InitializeTemplateAddRow"/> event.
	/// </summary>
	public class InitializeTemplateAddRowEventArgs : EventArgs 
	{
		private UltraGridRow templateAddRow;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="templateAddRow"></param>
		public InitializeTemplateAddRowEventArgs( UltraGridRow templateAddRow ) : base( )
		{
			this.templateAddRow = templateAddRow;
		}

		#endregion // Constructor

		#region TemplateAddRow

		/// <summary>
		/// Associated template add-row.
		/// </summary>
		public UltraGridRow TemplateAddRow
		{
			get
			{
				return this.templateAddRow;
			}
		}

		#endregion // TemplateAddRow
	}

	#endregion // InitializeTemplateAddRowEventArgs

	#region AfterCardsScrollEventArgs

	// SSP 12/5/03 UWG2295
	// Added AfterCardsScroll event.
	//
	/// <summary>
	/// Event args for <see cref="UltraGrid.AfterCardsScroll"/> event.
	/// </summary>
	public class AfterCardsScrollEventArgs : EventArgs
	{
		private RowsCollection rows = null;

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="rows">RowsCollection</param>
		public AfterCardsScrollEventArgs( RowsCollection rows )
		{
			this.rows = rows;
		}

		/// <summary>
		/// Rows collection where cards were scrolled.
		/// </summary>
		public RowsCollection Rows
		{
			get
			{
				return this.rows;
			}
		}
	}

	#endregion // AfterCardsScrollEventArgs

	// JDN 11/17/04 CardCompressedStateChanged events
    #region BeforeCardCompressedStateChangedEventArgs

	/// <summary>
	/// EventArgs for <see cref="UltraGrid.BeforeCardCompressedStateChanged"/> event.
	/// </summary>
	public class BeforeCardCompressedStateChangedEventArgs : System.ComponentModel.CancelEventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridRow row;
		private Infragistics.Win.UltraWinGrid.CardCompressedState newCompressedState;

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="row">The UltraGridRow undergoing a CardCompressedState change.</param>
        /// <param name="newCompressedState">The new CardCompressedState of the Row.</param>
		public BeforeCardCompressedStateChangedEventArgs( Infragistics.Win.UltraWinGrid.UltraGridRow row,
			Infragistics.Win.UltraWinGrid.CardCompressedState newCompressedState )
		{
			this.row = row;
			this.newCompressedState = newCompressedState;
		}

		/// <summary>
		/// The UltraGridRow undergoing a CardCompressedState change.
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get { return this.row; }
		}

		/// <summary>
		/// The new CardCompressedState of the Row.
		/// </summary>
		public Infragistics.Win.UltraWinGrid.CardCompressedState NewCompressedState
		{
			get { return this.newCompressedState; }
		}
	}

	#endregion // BeforeCardCompressedStateChangedEventArgs

	// JDN 11/17/04 CardCompressedStateChanged events
    #region AfterCardCompressedStateChangedEventArgs

	/// <summary>
	/// EventArgs for <see cref="UltraGrid.AfterCardCompressedStateChanged"/> event.
	/// </summary>
	public class AfterCardCompressedStateChangedEventArgs : System.EventArgs
	{
		private Infragistics.Win.UltraWinGrid.UltraGridRow row;
		private Infragistics.Win.UltraWinGrid.CardCompressedState newCompressedState;

		/// <summary>
		/// Constructor. Initializes new instance of <see cref="AfterCardCompressedStateChangedEventArgs"/>.
		/// </summary>
		/// <param name="row">The card row that was compressed or uncompressed.</param>
		/// <param name="newCompressedState">The new compressed sate of the card row.</param>
		public AfterCardCompressedStateChangedEventArgs( Infragistics.Win.UltraWinGrid.UltraGridRow row,
			Infragistics.Win.UltraWinGrid.CardCompressedState newCompressedState )
		{
			this.row = row;
			this.newCompressedState = newCompressedState;
		}

		/// <summary>
		/// The <see cref="UltraGridRow"/> undergoing a CardCompressedState change.
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get { return this.row; }
		}

		/// <summary>
		/// The new CardCompressedState of the Row.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridRow.IsCardCompressed"/>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.CardCompressedState NewCompressedState
		{
			get { return this.newCompressedState; }
		}
	}

	#endregion // AfterCardCompressedStateChangedEventArgs

	// SSP 12/15/04 - IDataErrorInfo Support
	//
	#region BeforeDisplayDataErrorTooltipEventArgs

	/// <summary>
	/// Event args associated with <see cref="UltraGrid.BeforeDisplayDataErrorTooltip"/> event.
	/// </summary>
	public class BeforeDisplayDataErrorTooltipEventArgs : System.ComponentModel.CancelEventArgs
	{
		private UltraGridRow row = null; 
		private UltraGridColumn column = null; 
		private IDataErrorInfo dataErrorInfo = null;
		private string tooltipText = null;

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="row">The row for which the data error tool tip is being displayed.</param>
        /// <param name="column">The associated column.</param>
        /// <param name="dataErrorInfo">The associated IDataErrorInfo instance.</param>
        /// <param name="tooltipText">The text that will be displayed in the data error tooltip.</param>
		public BeforeDisplayDataErrorTooltipEventArgs( UltraGridRow row, 
			UltraGridColumn column, IDataErrorInfo dataErrorInfo, string tooltipText )
		{
			this.row = row;
			this.column = column;
			this.dataErrorInfo = dataErrorInfo;
			this.tooltipText = tooltipText;
		}

		/// <summary>
		/// Returns the row for which the data error tool tip is being displayed.
		/// </summary>
		public UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}

		/// <summary>
		/// If the data error tooltip is being displayed for a row (when the user hovers over the
		/// the data error icon on the row selector), the <b>Column</b> will be null. If the
		/// data error tooltip is being displayed for a cell then this property will return the
		/// associated column.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		/// <summary>
		/// Associated IDataErrorInfo instance.
		/// </summary>
		public IDataErrorInfo DataErrorInfo
		{
			get
			{
				return this.dataErrorInfo;
			}
		}

		/// <summary>
		/// Gets or sets the text that will be displayed in the data error tooltip.
		/// </summary>
		public string TooltipText
		{
			get
			{
				return this.tooltipText;
			}
			set
			{
				this.tooltipText = value;
			}
		}
	}

	#endregion // BeforeDisplayDataErrorTooltipEventArgs

	#region AfterRowFixedStateChangedEventArgs

	/// <summary>
	/// EventArgs for AfterRowFixedStateChanged event.
	/// </summary>
	public class AfterRowFixedStateChangedEventArgs : System.EventArgs
	{
		private UltraGridRow row = null;

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="row">The row that was fixed or unfixed.</param>
		public AfterRowFixedStateChangedEventArgs( UltraGridRow row )
		{
			GridUtils.ValidateNull( "row", row );
			this.row = row;
		}

		/// <summary>
		/// Returns the row that was fixed or unfixed. The new fixed state of the row
		/// can be obtained using the <see cref="UltraGridRow.Fixed"/> property or
		/// querying the <see cref="RowsCollection.FixedRows"/> collection.
		/// </summary>
		public UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}
	}

	#endregion // AfterRowFixedStateChangedEventArgs

	#region BeforeRowFixedStateChangedEventArgs

	/// <summary>
	/// EventArgs for BeforeRowFixedStateChanged event.
	/// </summary>
	public class BeforeRowFixedStateChangedEventArgs : System.ComponentModel.CancelEventArgs
	{
		private UltraGridRow row = null;
		private bool newFixedState;

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="row">The row the user is attempting to fix or unfix</param>
        /// <param name="newFixedState">The new fixed state of the row.</param>
		public BeforeRowFixedStateChangedEventArgs( UltraGridRow row, bool newFixedState )
		{
			GridUtils.ValidateNull( "row", row );
			this.row = row;
			this.newFixedState = newFixedState;
		}

		/// <summary>
		/// Returns the row the user is attempting to fix or unfix. The origianl fixed state 
		/// of the row can be obtained using the <see cref="UltraGridRow.Fixed"/> property or
		/// querying the <see cref="RowsCollection.FixedRows"/> collection. The new state
		/// is provided by the <see cref="BeforeRowFixedStateChangedEventArgs.NewFixedState"/> 
		/// property of this event args. To cancel the operation of fixing or unfixing the row 
		/// set the <b>Cancel</b> property of the event args to true.
		/// </summary>
		public UltraGridRow Row
		{
			get
			{
				return this.row;
			}
		}

		/// <summary>
		/// Returns the new fixed state of the row. If this property returns true then the user
		/// is attempting to fix the row. If it returns false then the user is attempting to
		/// unfix the row. To cancel the operation of fixing or unfixing the row set the
		/// <b>Cancel</b> property of the event args to true.
		/// </summary>
		public bool NewFixedState
		{
			get
			{
				return this.newFixedState;
			}
		}
	}

	#endregion // BeforeRowFixedStateChangedEventArgs

	#region FilterCellValueChangedEventArgs

	/// <summary>
	/// EventArgs for FilterCellValueChanged event.
	/// </summary>
	public class FilterCellValueChangedEventArgs : System.EventArgs 
	{
		//private EmbeddableEditorBase operandEditor = null;
		//private FilterComparisionOperator selectedFilterOperator;
		private bool applyNewFilter;
		private UltraGridFilterCell filterCell = null;


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="filterCell">The UltraGridFilterCell whose value has changed.</param>
        /// <param name="applyNewFilter">A value indicating whether the new typed in filter should be applied.</param>
		public FilterCellValueChangedEventArgs( 
			UltraGridFilterCell filterCell,
			//EmbeddableEditorBase operandEditor,
			//FilterComparisionOperator selectedFilterOperator,			
			bool applyNewFilter )
		{
			//GridUtils.ValidateEnum( "selectedFilterOperator", typeof( FilterComparisionOperator ), selectedFilterOperator );
			//GridUtils.ValidateNull( "operandEditor", operandEditor );

			//this.operandEditor = operandEditor;
			//this.selectedFilterOperator = selectedFilterOperator;
			this.applyNewFilter = applyNewFilter;
			this.filterCell = filterCell;
		}

		/// <summary>
		/// The filter cell that the user modified.
		/// </summary>
		public UltraGridFilterCell FilterCell
		{
			get
			{
				return this.filterCell;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the new typed in filter should be applied.
		/// </summary>
		public bool ApplyNewFilter
		{
			get
			{
				return this.applyNewFilter;
			}
			set
			{
				this.applyNewFilter = value;
			}
		}
	}

	#endregion // FilterCellValueChangedEventArgs

	#region InitializeRowsCollectionEventArgs

	/// <summary>
	/// EventArgs for InitializeRowsCollection event.
	/// </summary>
	public class InitializeRowsCollectionEventArgs : System.EventArgs
	{
		private RowsCollection rows = null;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="rows"></param>
		public InitializeRowsCollectionEventArgs( RowsCollection rows )
		{
			GridUtils.ValidateNull( "rows", rows );
			this.rows = rows;
		}

		/// <summary>
		/// Gets the row collection that was created.
		/// </summary>
		public RowsCollection Rows
		{
			get
			{
				return this.rows;
			}
		}
	}

	#endregion // InitializeRowsCollectionEventArgs
	
	#region BeforeColumnChooserDisplayedEventArgs

	// SSP 6/17/05 - NAS 5.3 Column Chooser
	//
	/// <summary>
	/// EventArgs for BeforeColumnChooserDisplayed event.
	/// </summary>
	public class BeforeColumnChooserDisplayedEventArgs : System.ComponentModel.CancelEventArgs
	{
		private ColumnChooserDialog dialog = null;

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="dialog">The column chooser dialog that's about to be displayed.</param>
		public BeforeColumnChooserDisplayedEventArgs( ColumnChooserDialog dialog )
		{
			this.dialog = dialog;
		}

		/// <summary>
		/// Returns the column chooser dialog that's about to be displayed. You can cancel
		/// this event and display your own dialog.
		/// </summary>
		public ColumnChooserDialog Dialog
		{
			get
			{
				return this.dialog;
			}
		}
	}

	#endregion // BeforeColumnChooserDisplayedEventArgs

	// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
	#region CommonEventIds
	internal enum CommonEventIds
	{
		FilterRow, 
		AfterSortChange,
		BeforeSortChange,
		BeforeColPosChanged,
		AfterColPosChanged,
		BeforeRowFilterDropDown,
		BeforeRowFilterChanged,
		AfterRowFilterChanged,
		BeforeCustomRowFilterDialog,
		BeforeRowFilterDropDownPopulate,
		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Added FilterCellValueChanged and InitializeRowsCollection events.
		//
		FilterCellValueChanged,
		InitializeRowsCollection,
		// SSP 6/17/05 - NAS 5.3 Column Chooser
		//
		BeforeColumnChooserDisplayed,
		// SSP 10/18/05 - NAS 5.3 Column Chooser
		// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
		// TestAdvantage.
		// 
		BeforeBandHiddenChanged,
		AfterBandHiddenChanged,

        // CDS 02/02/09 TFS12512 - NAS v9.1 Header CheckBox -Added support for firing the events on the UltraCombo
        // Added the BeforeHeaderCheckStateChanged and AfterHeaderCheckStateChanged events.
        BeforeHeaderCheckStateChanged,
        AfterHeaderCheckStateChanged,
	}
	#endregion CommonEventIds

	// JAS v5.2 DoubleClick Events 4/27/05
	//
	#region DoubleClick Events Classes

		#region DoubleClickCellEventArgs

	/// <summary>
	/// The event argument type used by the <see cref="UltraGrid.DoubleClickCell"/> event.
	/// </summary>
	public class DoubleClickCellEventArgs : EventArgs
	{
		#region Data

		private UltraGridCell cell;

		#endregion // Data

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="cell">The cell which was double clicked on.</param>
		public DoubleClickCellEventArgs( UltraGridCell cell )
		{
			GridUtils.ValidateNull( "cell", cell );

			this.cell = cell;
		}

		#endregion // Constructor

		#region Cell

		/// <summary>
		/// Returns the cell in the grid which was double clicked on.
		/// </summary>
		public UltraGridCell Cell
		{
			get { return this.cell; }
		}

		#endregion // Cell
	}

		#endregion // DoubleClickCellEventArgs

		#region DoubleClickHeaderEventArgs

	/// <summary>
	/// The event argument type used by the <see cref="UltraGrid.DoubleClickHeader"/> event.
	/// </summary>
	public class DoubleClickHeaderEventArgs : EventArgs
	{
		#region Data

		private HeaderBase header;

		#endregion // Data

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="header">The header which was double clicked on.</param>
		public DoubleClickHeaderEventArgs( HeaderBase header )
		{
			GridUtils.ValidateNull( "header", header );

			this.header = header;
		}

		#endregion // Constructor

		#region Header

		/// <summary>
		/// Returns the header in the grid which was double clicked on.
		/// </summary>
		public HeaderBase Header
		{
			get { return this.header; }
		}

		#endregion // Header
	}

		#endregion // DoubleClickHeaderEventArgs

		#region DoubleClickRowEventArgs

	/// <summary>
	/// The event argument type used by the <see cref="UltraGrid.DoubleClickRow"/> event.
	/// </summary>
	public class DoubleClickRowEventArgs : EventArgs
	{
		#region Data

		private UltraGridRow row;
		private RowArea		 rowArea;

		#endregion // Data

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="row">The row which was double clicked on.</param>
		/// <param name="rowArea">The area of the row which was double clicked on.</param>
		public DoubleClickRowEventArgs( UltraGridRow row, RowArea rowArea )
		{
			GridUtils.ValidateNull( "row", row );
			GridUtils.ValidateEnum( "rowArea", typeof(RowArea), rowArea );

			this.row     = row;
			this.rowArea = rowArea;
		}

		#endregion // Constructor

		#region Row

		/// <summary>
		/// Returns the row in the grid which was double clicked on.
		/// </summary>
		public UltraGridRow Row
		{
			get { return this.row; }
		}

		#endregion // Row

		#region RowArea

		/// <summary>
		/// Returns the area of the row which was double clicked on.
		/// </summary>
		public RowArea RowArea
		{
			get { return this.rowArea; }
		}

		#endregion // RowArea
	}

		#endregion // DoubleClickRowEventArgs

		#region DoubleClickCellEventHandler

	/// <summary>
	/// The delegate type used by the <see cref="UltraGrid.DoubleClickCell"/> event.
	/// </summary>
	public delegate void DoubleClickCellEventHandler( object sender, DoubleClickCellEventArgs e );

		#endregion // DoubleClickCellEventHandler

		#region DoubleClickHeaderEventHandler

	/// <summary>
	/// The delegate type used by the <see cref="UltraGrid.DoubleClickHeader"/> event.
	/// </summary>
	public delegate void DoubleClickHeaderEventHandler( object sender, DoubleClickHeaderEventArgs e );

		#endregion // DoubleClickHeaderEventHandler

		#region DoubleClickRowEventHandler

	/// <summary>
	/// The delegate type used by the <see cref="UltraGrid.DoubleClickRow"/> event.
	/// </summary>
	public delegate void DoubleClickRowEventHandler( object sender, DoubleClickRowEventArgs e );

		#endregion // DoubleClickRowEventHandler

	#endregion // DoubleClick Events Classes

    // MBS 2/28/08 - RowEditTemplate NA2008 V2
    #region RowEditTemplate-Related Events

    #region AfterRowEditTemplateClosedEventArgs

    /// <summary>
    /// The event args used by the <see cref="UltraGrid.AfterRowEditTemplateClosed"/> event.
    /// </summary>
    public class AfterRowEditTemplateClosedEventArgs : EventArgs
    {
        #region Members

        private UltraGridRowEditTemplate template;

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="template">The template that has been closed.</param>
        public AfterRowEditTemplateClosedEventArgs(UltraGridRowEditTemplate template)
        {
            this.template = template;
        }
        #endregion //Constructor

        #region Properties

        #region Template

        /// <summary>
        /// Returns the template that has been closed.
        /// </summary>
        public UltraGridRowEditTemplate Template
        {
            get { return this.template; }
        }
        #endregion //Template

        #endregion //Properties
    }
    #endregion //AfterRowEditTemplateClosedEventArgs

    #region AfterRowEditTemplateClosedEventHandler

    /// <summary>
    /// The delegate type used by the <see cref="UltraGrid.AfterRowEditTemplateClosed"/> event.
    /// </summary>
    public delegate void AfterRowEditTemplateClosedEventHandler(object sender, AfterRowEditTemplateClosedEventArgs e);

    #endregion //AfterRowEditTemplateClosedEventHandler

    #region AfterRowEditTemplateDisplayedEventArgs

    /// <summary>
    /// The event args used by the <see cref="UltraGrid.AfterRowEditTemplateDisplayed"/> event.
    /// </summary>
    public class AfterRowEditTemplateDisplayedEventArgs : EventArgs
    {
        #region Members

        private UltraGridRowEditTemplate template;
        private TemplateDisplaySource source;

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="template">The template that has been shown.</param>
        /// <param name="source">The method used to display the template.</param>
        public AfterRowEditTemplateDisplayedEventArgs(UltraGridRowEditTemplate template, TemplateDisplaySource source)
        {
            this.template = template;
            this.source = source;
        }
        #endregion //Constructor

        #region Properties

        #region Source

        /// <summary>
        /// Returns the method that was used to display the RowEditTemplate.
        /// </summary>
        public TemplateDisplaySource Source
        {
            get { return this.source; }
        }
        #endregion //Source

        #region Template

        /// <summary>
        /// Returns the template that has been shown.
        /// </summary>
        public UltraGridRowEditTemplate Template
        {
            get { return this.template; }
        }
        #endregion //Template

        #endregion //Properties
    }

    #endregion AfterRowEditTemplateDisplayedEventArgs

    #region AfterRowEditTemplateDisplayedEventHandler

    /// <summary>
    /// The delegate type used by the <see cref="UltraGrid.BeforeRowEditTemplateDisplayed"/> event.
    /// </summary>
    public delegate void AfterRowEditTemplateDisplayedEventHandler(object sender, AfterRowEditTemplateDisplayedEventArgs e);

    #endregion //AfterRowEditTemplateDisplayedEventHandler

    #region BeforeRowEditTemplateClosedEventArgs

    /// <summary>
    /// The event args used by the <see cref="UltraGrid.BeforeRowEditTemplateClosed"/> event.
    /// </summary>
    public class BeforeRowEditTemplateClosedEventArgs : CancelEventArgs
    {
        #region Members

        private bool forceClosing;
        private UltraGridRowEditTemplate template;

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="template">The template that is about to be closed.</param>
        /// <param name="forceClosing">True if the template is going to be forced to be closed.</param>
        public BeforeRowEditTemplateClosedEventArgs(UltraGridRowEditTemplate template, bool forceClosing)
        {
            this.template = template;
            this.forceClosing = forceClosing;
        }
        #endregion //Constructor

        #region Properties

        #region ForceClosing

        /// <summary>
        /// Returns whether the template is going to be closed and cannot be cancelled,
        /// such as when the template is shown as a popup.
        /// </summary>
        public bool ForceClosing
        {
            get { return this.forceClosing; }
        }
        #endregion //ForceClosing

        #region Template

        /// <summary>
        /// Returns the template that is about to be closed.
        /// </summary>
        public UltraGridRowEditTemplate Template
        {
            get { return this.template; }
        }
        #endregion //Template

        #endregion //Properties
    }
    #endregion //BeforeRowEditTemplateClosedEventArgs

    #region BeforeRowEditTemplateClosedEventHandler

    /// <summary>
    /// The delegate type used by the <see cref="UltraGrid.BeforeRowEditTemplateClosed"/> event.
    /// </summary>
    public delegate void BeforeRowEditTemplateClosedEventHandler(object sender, BeforeRowEditTemplateClosedEventArgs e);

    #endregion //BeforeRowEditTemplateClosedEventHandler

    #region BeforeRowEditTemplateDisplayedEventArgs

    /// <summary>
    /// The event args used by the <see cref="UltraGrid.BeforeRowEditTemplateDisplayed"/> event.
    /// </summary>
    public class BeforeRowEditTemplateDisplayedEventArgs : CancelEventArgs
    {
        #region Members

        private UltraGridRowEditTemplate template;
        private Point location;
        private TemplateDisplaySource source;

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="template">The template that is about to be shown.</param>
        /// <param name="location">The starting location of the template.</param>
        /// <param name="source">The method used to display the template.</param>
        public BeforeRowEditTemplateDisplayedEventArgs(UltraGridRowEditTemplate template, Point location, TemplateDisplaySource source)
        {
            this.template = template;
            this.location = location;
            this.source = source;
        }
        #endregion //Constructor

        #region Properties

        #region Location

        /// <summary>
        /// Gets or sets the location, in screen coordinates, of where the template will be shown.
        /// </summary>
        public Point Location
        {
            get { return this.location; }
            set { this.location = value; }
        }
        #endregion //Location

        #region Source

        /// <summary>
        /// Returns the method that was used to display the RowEditTemplate.
        /// </summary>
        public TemplateDisplaySource Source
        {
            get { return this.source; }
        }
        #endregion //Source

        #region Template

        /// <summary>
        /// Returns the template that is about to be shown.
        /// </summary>
        public UltraGridRowEditTemplate Template
        {
            get { return this.template; }
        }
        #endregion //Template

        #endregion //Properties
    }
    #endregion //BeforeRowEditTemplateDisplayedEventArgs

    #region BeforeRowEditTemplateDisplayedEventHandler

    /// <summary>
    /// The delegate type used by the <see cref="UltraGrid.BeforeRowEditTemplateDisplayed"/> event.
    /// </summary>
    public delegate void BeforeRowEditTemplateDisplayedEventHandler(object sender, BeforeRowEditTemplateDisplayedEventArgs e);

    #endregion //BeforeRowEditTemplateDisplayedEventHandler

    #region RowEditTemplateRequestedEventArgs

    /// <summary>
    /// The event args used by the <see cref="UltraGrid.RowEditTemplateRequested"/> event.
    /// </summary>
    public class RowEditTemplateRequestedEventArgs : HandledEventArgs
    {
        #region Members

        private UltraGridRowEditTemplate template;

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="template">The resolved template of the current row about to be edited.</param>
        public RowEditTemplateRequestedEventArgs(UltraGridRowEditTemplate template)
            : base()
        {
            this.template = template;
        }
        #endregion //Constructor

        #region Properties

        #region Template

        /// <summary>
        /// Gets the resolved template of the row about to be edited.
        /// </summary>
        public UltraGridRowEditTemplate Template
        {
            get { return this.template; }
        }
        #endregion //Template

        #endregion //Properties
    }
    #endregion //RowEditTemplateRequestedEventArgs

    #region RowEditTemplateRequestedEventHandler

    /// <summary>
    /// The delegate type used by the <see cref="UltraGrid.RowEditTemplateRequested"/> event.
    /// </summary>
    public delegate void RowEditTemplateRequestedEventHandler(object sender, RowEditTemplateRequestedEventArgs e);

    #endregion //RowEditTemplateRequestedEventHandler

    #endregion //RowEditTemplate-Related Events

    // MBS 7/1/08 - NA2008 V3
    #region ClickCell Event Classes

    #region ClickCellEventArgs

    /// <summary>
    /// The event args used by the <see cref="UltraGrid.ClickCell"/> event.
    /// </summary>
    public class ClickCellEventArgs : EventArgs
    {
        #region Members

        private UltraGridCell cell;

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the event args.
        /// </summary>
        /// <param name="cell">The cell that was clicked.</param>
        public ClickCellEventArgs(UltraGridCell cell)
        {
            this.cell = cell;
        }
        #endregion //Constructor

        #region Properties

        #region Cell

        /// <summary>
        /// Returns the the cell the was clicked.
        /// </summary>
        public UltraGridCell Cell
        {
            get { return this.cell; }
        }
        #endregion //Cell

        #endregion //Properties
    }
    #endregion //ClickCellEventArgs

    #region ClickCellEventHandler

    /// <summary>
    /// The delegate type used by the <see cref="UltraGrid.ClickCell"/> event.
    /// </summary>
    public delegate void ClickCellEventHandler(object sender, ClickCellEventArgs e);

    #endregion //ClickCellEventHandler

    #endregion //ClickCell Event Classes

    // CDS NAS v9.1 Header CheckBox
    #region Header CheckBox Event Classes

    #region AfterHeaderCheckStateChangedEventArgs

    /// <summary>
    /// EventArgs class used to pass information during the AfterHeaderCheckStateChanged event.
    /// </summary>
    public class AfterHeaderCheckStateChangedEventArgs : EventArgs
    {

        #region Members

        private UltraGridColumn column = null;
        private RowsCollection rows = null;

        #endregion Members

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="column">UltraGridColumn containing the header checkbox whose state is being changed. </param>
        /// <param name="rows">RowsCollection which is affected the header checkbox's state change </param>
        public AfterHeaderCheckStateChangedEventArgs(UltraGridColumn column, RowsCollection rows)
        {
            this.column = column;
            this.rows = rows;
        }

        #endregion Constructors

        #region Properties

        #region Column

        /// <summary>
        /// Gets the UltraGridColumn containing the header checkbox whose state is being changed.
        /// </summary>
        public UltraGridColumn Column
        {
            get { return this.column; }
        }

        #endregion Column

        #region Rows

        /// <summary>
        /// Gets the RowsCollection which is affected the header checkbox's state change.
        /// </summary>
        public RowsCollection Rows
        {
            get { return this.rows; }
        }

        #endregion Rows

        #endregion Properties
    }

    #endregion AfterHeaderCheckStateChangedEventArgs

    #region BeforeHeaderCheckStateChangedEventArgs

    /// <summary>
    /// EventArgs class used to pass information during the BeforeHeaderCheckStateChanged event.
    /// </summary>
    public class BeforeHeaderCheckStateChangedEventArgs : CancelEventArgs
    {
        
        #region Members

        private UltraGridColumn column = null;
        private RowsCollection rows = null;
        private CheckState newState;
        private CheckState currentState;
        private bool isCancelable = false;


        #endregion Members

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="column">UltraGridColumn containing the header checkbox whose state is being changed</param>
        /// <param name="rows">RowsCollection which is affected the header checkbox's state change</param>
        /// <param name="newCheckState">The new CheckState value for the header's checkbox</param>
        /// <param name="currentCheckState">The current CheckState value for the header's checkbox</param>
        /// <param name="isCancelable">Indicates if the change of the CheckState can be cancelled</param>
        public BeforeHeaderCheckStateChangedEventArgs(UltraGridColumn column, RowsCollection rows, CheckState newCheckState, CheckState currentCheckState, bool isCancelable )
        {
            this.column = column;
            this.rows = rows;
            this.newState = newCheckState;
            this.currentState = currentCheckState;
            this.isCancelable = isCancelable;
        }

        #endregion Constructors

        #region Properties

        #region Column

        /// <summary>
        /// Gets the UltraGridColumn containing the header checkbox whose state is being changed
        /// </summary>
        public UltraGridColumn Column
        {
            get { return this.column; }
        }

        #endregion Column

        #region Rows

        /// <summary>
        /// Gets the RowsCollection which is affected the header checkbox's state change
        /// </summary>
        public RowsCollection Rows
        {
            get { return this.rows; }
        }

        #endregion Rows

        #region CurrentCheckState

        /// <summary>
        /// Gets the current CheckState value for the header's checkbox
        /// </summary>
        public CheckState CurrentCheckState
        {
            get { return this.currentState; }
        }

        #endregion CurrentCheckState

        #region NewCheckState

        /// <summary>
        /// Gets or sets the new CheckState value for the header's checkbox
        /// </summary>
        public CheckState NewCheckState
        {
            get { return this.newState; }
            set { this.newState = value; }
        }

        #endregion NewCheckState

        #region IsCancelable

        /// <summary>
        /// Indicates if the change of the CheckState can be cancelled
        /// </summary>
        public bool IsCancelable
        {
            get { return this.isCancelable; }
        }

        #endregion IsCancelable

        #endregion Properties
    }

    #endregion BeforeHeaderCheckStateChangedEventArgs

    #region AfterHeaderCheckStateChangedEventHandler
    
    /// <summary>
    /// The delegate type used by the AfterHeaderCheckStateChangedEvent event
    /// </summary>
    public delegate void AfterHeaderCheckStateChangedEventHandler(object sender, AfterHeaderCheckStateChangedEventArgs e);

    #endregion //AfterHeaderCheckStateChangedEventHandler

    #region BeforeHeaderCheckStateChangedEventHandler

    /// <summary>
    /// The delegate type used by the BeforeHeaderCheckStateChangedEvent event
    /// </summary>
    public delegate void BeforeHeaderCheckStateChangedEventHandler(object sender, BeforeHeaderCheckStateChangedEventArgs e);

    #endregion //BeforeHeaderCheckStateChangedEventHandler

    #endregion //Header CheckBox Event Classes
}
