#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using System.Diagnostics;

// SSP 3/29/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
// Added SpecialRowSeparatorUIElement class.
//

namespace Infragistics.Win.UltraWinGrid
{
	#region SpecialRowSeparatorUIElement Class

	/// <summary>
	/// UIElement used for displaying separation between special rows.
	/// </summary>
	public class SpecialRowSeparatorUIElement : UIElement
	{
		#region SpecialRowSeparatorUIElement

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		public SpecialRowSeparatorUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // SpecialRowSeparatorUIElement

		#region Initialize

		/// <summary>
		/// Sets the primary context to the specified row collection.
		/// </summary>
		/// <param name="rows">The rows collection associated with this element.</param>
		public void Initialize( RowsCollection rows )
		{
			this.PrimaryContext = rows;
		}

		#endregion // Initialize

		#region Rows

		/// <summary>
		/// Returns the associated row collection.
		/// </summary>
		public RowsCollection Rows
		{
			get
			{
				return this.PrimaryContext as RowsCollection;
			}
		}

		#endregion // Rows

		#region Band

		/// <summary>
		/// Returns the associated row collection.
		/// </summary>
		public UltraGridBand Band
		{
			get
			{
				RowsCollection rows = this.Rows;

                // MRS 2/23/2009 - Found while fixing TFS14184
                // The designer doesn't have a rows collection to work with. So it passes in null
                // and this causes:
                // 1) An annoying assert in InitAppearance
                // 2) The Designer does resolve the appearance for this element. 
                //
				//return null != rows ? rows.Band : null;
                return null != rows ? 
                    rows.Band : 
                    this.GetContext(typeof(UltraGridBand)) as UltraGridBand;
			}
		}

		#endregion // Rows

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData,
			ref AppearancePropFlags requestedProps )
		{
			UltraGridBand band = this.Band;
			Debug.Assert( null != band );
			if ( null != band )
				band.ResolveSpecialRowSeparatorAppearance( ref appData, ref requestedProps );
		}

		#endregion // InitAppearance

		#region BorderStyle

		/// <summary>
		/// Returns or sets a value that determines the border style of an object.
		/// </summary>
		/// <remarks>
		/// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
		/// </remarks>
		public override UIElementBorderStyle BorderStyle
		{
			get
			{
				UltraGridBand band = this.Band;
				return null != band
					? band.BorderStyleSpecialRowSeparatorResolved
					: UIElementBorderStyle.None;
			}
		}

		#endregion // BorderStyle

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Band, StyleUtils.Role.SpecialRowSeparator );
			}
		}

		#endregion // UIRole
	}

	#endregion // SpecialRowSeparatorUIElement Class

}
