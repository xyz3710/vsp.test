#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Security.Permissions;
	using System.Runtime.Serialization;
	using Infragistics.Shared.Serialization;

	/// <summary>
	/// Returns a reference to the SpecialBoxBase object. This property is read-only 
	/// at design-time and run-time.
	/// </summary><remarks><para>This property returns a reference to an SpecialBoxBase
	/// object that can be used to set properties of, and invoke methods on, 
	/// the AddNew box. You can use this reference to access any of the AddNew 
	/// box's properties or methods.</para><para>Use the returned reference to show or 
	/// hide the AddNew box or adjust its or its buttons' appearance.</para></remarks>
	[ TypeConverter( typeof( System.ComponentModel.ExpandableObjectConverter ) ), Serializable()  ]
	

	public abstract class SpecialBoxBase : SubObjectBase
	{
		private Infragistics.Win.UIElementBorderStyle	borderStyle;		
		private Infragistics.Win.UIElementBorderStyle	buttonConnectorStyle;
		private System.Drawing.Color					buttonConnectorColor;
		private bool									hidden	= true;
		private Infragistics.Win.AppearanceHolder		appearanceHolder = null;
		private Infragistics.Win.UltraWinGrid.UltraGridLayout	layout = null;
		private string									prompt = null;

	
	

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="layout">The layout of the ultragrid to which this box belongs.</param>
		// AS 1/8/03 - fxcop
		// abstract types should not have public constructors
		//public SpecialBoxBase( Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		protected SpecialBoxBase( Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		{
			this.layout = layout;
			this.Reset();
		}


		
		/// <summary>  
		/// Property: gets/sets SpecialBoxBase's appearance 
		/// </summary>  
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		[ LocalizedDescription("LD_SpecialBoxBase_P_Appearance") ]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.AppearanceBase Appearance
		{
			get
			{
				if ( null == this.appearanceHolder )
				{
				    
					this.appearanceHolder = new Infragistics.Win.AppearanceHolder();
				
					// hook up the prop change notifications
					//
					this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.Layout != null)
					this.appearanceHolder.Collection = this.layout.Appearances;

				return this.appearanceHolder.Appearance;
			}
			set
			{
				if( this.appearanceHolder == null  ||
					!this.appearanceHolder.HasAppearance ||
					this.appearanceHolder.Appearance != value )
				{
					// remove the old prop change notifications
					//
					// SSP 11/9/01 UWG722
					// if this.appearanceHolder is null, create a new one before going ahead 
					// with setting appearance on the appearance holder.
					//
					// if ( null != this.appearanceHolder.Appearance )
					if ( null == this.appearanceHolder )
					{
						// SSP 11/9/01 UWG722
                        //
						//this.appearanceHolder = new Infragistics.Win.AppearanceHolder( this.layout.Appearances );
						this.appearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

					}
					
				                    
					// Initialize the collection
					//
					if (this.Layout != null)
						this.appearanceHolder.Collection = this.layout.Appearances;

					this.appearanceHolder.Appearance = value;
            
					this.NotifyPropChange( PropertyIds.Appearance, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAppearance() 
		{
			return ( this.HasAppearance &&
				this.appearanceHolder.ShouldSerialize() );
		}

		/// <summary>
		/// Returns true if an appearance object has been created.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
		Browsable( false ) ]		
		public bool HasAppearance
		{
			get
			{
				return ( null != this.appearanceHolder &&
					this.appearanceHolder.HasAppearance );
			}
		}


		/// <summary>
		/// Reset button appearance
		/// </summary>
		public void ResetAppearance() 
		{
			if ( this.HasAppearance )
			{
//				// remove the prop change notifications
//				//
//				this.appearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
//                    
//				this.appearanceHolder.Reset();
				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//this.appearanceHolder.Appearance.Reset();

				// Call reset on the holder instead of the appearance
				//
				this.appearanceHolder.Reset();

				// Notify listeners that the appearance has changed. 
				this.NotifyPropChange(PropertyIds.Appearance);
			}
		}

		
		/// <summary>
		/// <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout"/>
		/// </summary>
		internal Infragistics.Win.UltraWinGrid.UltraGridLayout Layout 
		{
			get
			{
				return this.layout;	
			}
			
		}

		/// <summary>
		/// Determines whether the object will be displayed. This property is not 
		/// available at design-time.
		/// </summary><remarks><para>The Hidden property determines whether an object is 
		/// visible. Hiding an object may have have effects that go beyond simply 
		/// removing it from view. For example, hiding a band also hides all the rows 
		/// in that band. Also, changing the Hidden property of an object affects all 
		/// instances of that object. For example, a hidden column or row is hidden 
		/// in all scrolling regions.</para><para>There may be instances where the 
		/// Hidden property cannot be changed. For example, you cannot hide the 
		/// currently active rowscrollregion or colscrollregion. If you attempt to 
		/// set the Hidden property of the active rowscrollregion to True, 
		/// an error will occur: 'The following code will produce an error 
		/// UltraGrid1.ActiveRowScrollRegion.Hidden = True</para>
		/// <para>This property is ignored for chaptered columns; that is, columns 
		/// whose DataType property is set to 136 (DataTypeChapter).</para>
		/// </remarks>
		[ LocalizedDescription("LD_SpecialBoxBase_P_Hidden") ]
		[ LocalizedCategory("LC_Display") ]
		public bool Hidden
		{
			get 
			{
				return this.hidden;
			}
			set
			{
				if(this.hidden != value)
				{
					this.hidden = value;
					this.NotifyPropChange( PropertyIds.Hidden, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the hidden property needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeHidden() 
		{
			return this.hidden != this.DefaultHidden;
		}
 
		/// <summary>
		/// Set hidden back to true
		/// </summary>
		public void ResetHidden() 
		{
			this.Hidden = this.DefaultHidden;
		}


		/// <summary>
		/// <see cref="Infragistics.Win.UIElementBorderStyle"/>
		/// </summary>
		[ LocalizedDescription("LD_SpecialBoxBase_P_BorderStyle") ]
		[ LocalizedCategory("LC_Display") ]
		public Infragistics.Win.UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.borderStyle;
				
			}

			set
			{
				if ( value != this.borderStyle )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Infragistics.Win.UIElementBorderStyle), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_163") );

					this.borderStyle = value;
					this.NotifyPropChange( PropertyIds.BorderStyle, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the Border Style property needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeBorderStyle() 
		{ 
			return ( this.borderStyle != Infragistics.Win.UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets the BorderStyle to default
		/// </summary>
		public void ResetBorderStyle() 
		{
			this.BorderStyle = Infragistics.Win.UIElementBorderStyle.Default;
		}

		/// <summary>
		/// Returns or sets the connector style for buttons. 
		/// </summary>
		[ LocalizedDescription("LD_SpecialBoxBase_P_ButtonConnectorStyle") ]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.UIElementBorderStyle ButtonConnectorStyle
		{
			get
			{
				return this.buttonConnectorStyle;
			}
			
			set
			{
				if ( value != this.buttonConnectorStyle )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Infragistics.Win.UIElementBorderStyle), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_268") );

					this.buttonConnectorStyle = value;

					// SSP 3/20/06 - App Styling
					// 
					StyleUtils.DirtyCachedPropertyVals( this.Layout );

					this.NotifyPropChange( PropertyIds.ButtonConnectorStyle, null );
				}
			}
		}

		// SSP 3/15/06 - App Styling
		// Added ButtonConnectorStyleResolved since we need to now resolve between the style and the
		// control setting.
		// 
		internal UIElementBorderStyle ButtonConnectorStyleResolved
		{
			get
			{
				StyleUtils.CustomProperty prop = this is GroupByBox 
					? StyleUtils.CustomProperty.GroupByBoxButtonConnectorStyle
					: StyleUtils.CustomProperty.AddNewBoxButtonConnectorStyle;

				object val;
				if ( StyleUtils.GetCachedPropertyVal( this.Layout, prop, out val ) )
					return (UIElementBorderStyle)val;

				return (UIElementBorderStyle)StyleUtils.CachePropertyValue( this.Layout, prop,
					this.buttonConnectorStyle, UIElementBorderStyle.Default, UIElementBorderStyle.Solid );
			}
		}
	
		/// <summary>
		/// Returns true if the Button Connector Style property 
		/// needs to be serialized (not null)
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeButtonConnectorStyle() 
		{	
			// SSP 11/21/01 UWG669
			// Use the UIElementBorderStyle.Default instead of UIElementBorderStyle.Solid
			// because the ButtonConnectorUIElement will resolve to Solid
			// if ButtonConnectorStyle is set to Default. 
			//
			//return ( this.buttonConnectorStyle != Infragistics.Win.UIElementBorderStyle.Solid );
			return ( this.buttonConnectorStyle != Infragistics.Win.UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets the Button Connector Style to Solid
		/// </summary>
		public void ResetButtonConnectorStyle() 
		{
			// SSP 11/21/01 UWG669
			// Use the UIElementBorderStyle.Default instead of UIElementBorderStyle.Solid
			// because the ButtonConnectorUIElement will resolve to Solid
			// if ButtonConnectorStyle is set to Default. 
			//
			//this.buttonConnectorStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.buttonConnectorStyle = Infragistics.Win.UIElementBorderStyle.Default;
		}

		internal virtual void InitializeFrom( SpecialBoxBase source, PropertyCategories propertyCategories )
		{
			if ( (propertyCategories & PropertyCategories.General) == 0 )
				return;
			
			this.Hidden = source.hidden;
			this.BorderStyle = source.borderStyle;
			this.ButtonConnectorStyle = source.buttonConnectorStyle;
			this.ButtonConnectorColor = source.buttonConnectorColor;

			// SSP 11/4/05 BR07503
			// Clone copies over the AppearanceCollection as well. We don't want that.
			// 
			// ------------------------------------------------------------------------------
			//if (  source.appearanceHolder != null )
			//	this.appearanceHolder = source.appearanceHolder.Clone();

			if ( null != source.appearanceHolder &&
				source.appearanceHolder.HasAppearance )
			{
				// Get appearance to trigger allocation of holder
				//
				Infragistics.Win.AppearanceBase app = this.Appearance;
			
				// init the holder from the source
				//
				this.appearanceHolder.InitializeFrom( source.appearanceHolder );
			}
			else
			{
				// reset the appearance holder
				//
				if ( null != this.appearanceHolder &&
					this.appearanceHolder.HasAppearance )
				{
					this.appearanceHolder.Reset();
				}
			}
			// ------------------------------------------------------------------------------
			
			this.Prompt = source.prompt;

			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;
		}

			
			
		

		
		/// <summary>
		/// Determines the color of the lines that will be used to connect the 
		/// SpecialBoxBase object buttons.
		/// </summary><remarks>The ButtonConnectorColor property determines the 
		/// color of the lines used to connect the buttons in the AddNew box. 
		/// In addition to specifying the color of these lines, you can also set 
		/// their style using the ButtonConnectorStyle property.</remarks>
		[ LocalizedDescription("LD_SpecialBoxBase_P_ButtonConnectorColor") ]
		[ LocalizedCategory("LC_Appearance") ]
		public System.Drawing.Color ButtonConnectorColor
		{
			get
			{				
				return this.buttonConnectorColor;
			}
			set
			{
				if ( value != this.buttonConnectorColor )
				{
					this.buttonConnectorColor = value;

					// SSP 3/20/06 - App Styling
					// 
					StyleUtils.DirtyCachedPropertyVals( this.Layout );

					this.NotifyPropChange( PropertyIds.ButtonConnectorColor, null );
				}
			}
		}

		// SSP 3/15/06 - App Styling
		// Added ButtonConnectorColorResolved since we need to now resolve between the style and the
		// control setting.
		// 
		internal System.Drawing.Color ButtonConnectorColorResolved
		{
			get
			{
				StyleUtils.CustomProperty prop = this is GroupByBox 
					? StyleUtils.CustomProperty.GroupByBoxButtonConnectorColor 
					: StyleUtils.CustomProperty.AddNewBoxButtonConnectorColor;

				object val;
				if ( StyleUtils.GetCachedPropertyVal( this.Layout, prop, out val ) )
					return (Color)val;

				return (Color)StyleUtils.CachePropertyValue( this.Layout, prop, 
					this.buttonConnectorColor, Color.Empty, Color.DarkBlue );
			}
		}

		/// <summary>
		/// Returns true if the Button Connector Color property needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeButtonConnectorColor() 
		{
			// SSP 3/16/06 - App Styling
			// Changed the default from DarkBlue to Empty and added resolution mechanism.
			// 
			//return ( this.buttonConnectorColor != System.Drawing.Color.DarkBlue );
			return this.buttonConnectorColor != System.Drawing.Color.Empty;
		}
 
		/// <summary>
		/// Resets the Button Connector Color to DarkBlue
		/// </summary>
		public void ResetButtonConnectorColor() 
		{
			// SSP 3/16/06 - App Styling
			// Changed the default from DarkBlue to Empty and added resolution mechanism.
			// 
			//this.ButtonConnectorColor = System.Drawing.Color.DarkBlue;
			this.ButtonConnectorColor = System.Drawing.Color.Empty;
		}

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values
		/// </summary>
		public virtual bool ShouldSerialize() 
		{
			return	this.ShouldSerializeBorderStyle()			||				
				this.ShouldSerializeButtonConnectorColor()	||
				this.ShouldSerializeButtonConnectorStyle() ||
				this.ShouldSerializeHidden()					||
				this.ShouldSerializePrompt()					||	
				this.ShouldSerializeTag() ||
				this.ShouldSerializeAppearance();
			
		}

		/// <summary>
		/// Resets all properties back to their default values
		/// </summary>
		public virtual void Reset() 
		{
			this.ResetBorderStyle();			
			this.ResetButtonConnectorColor();
			this.ResetButtonConnectorStyle();
			this.ResetHidden();
			this.ResetPrompt();
			this.ResetAppearance();			
		}


		internal bool OnSubObjectPropChangedHelper( PropChangeInfo propChange )
		{			
			if ( this.HasAppearance &&
				this.appearanceHolder.RootAppearance == propChange.Source )

				this.NotifyPropChange( PropertyIds.Appearance, propChange );
			else
				return false;

			return true;
		}


		/// <summary>
		/// Called when the object is disposed of
		/// </summary>
		protected override void OnDispose()
		{
			
			// It is importantant to unhook the holder and call reset on it
			// when we are disposed
			//
			if ( this.appearanceHolder != null )
			{
				this.appearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.appearanceHolder.Reset();
				this.appearanceHolder = null;
			}


			base.OnDispose();
		}


		/// <summary>
		/// Resolves all of the add new box's appearance properties
		/// </summary>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		public void ResolveAppearance( ref AppearanceData appData )
		{
			this.ResolveAppearance( ref appData, AppearancePropFlags.AllRenderAndCursor );
		}

		/// <summary>
		/// Resolves selected properties of the add new box's appearance
		/// </summary>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		/// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
		public void ResolveAppearance( ref AppearanceData appData, AppearancePropFlags requestedProps )
		{
			this.ResolveAppearance( ref appData, ref requestedProps );
		}

		// SSP 3/16/06
		// Added an overload of ResolveAppearance that takes in requestedProps as ref param.
		// 
		internal void ResolveAppearance( ref AppearanceData appData, ref AppearancePropFlags requestedProps )
		{
			// Force text alignment to left middle
			// SSP 11/6/01 UWG678 
			// This is not a good way to force the alignment to be left and middle.
			// Look below for code that does that.
			//
			//this.Appearance.TextHAlign = HAlign.Left;
			//this.Appearance.TextVAlign = VAlign.Middle;

			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof (Infragistics.Win.UltraWinGrid.UltraGridLayout), requestedProps );

			// SSP 3/14/06 - App Styling
			// 
			context.Role = StyleUtils.GetRole( this.Layout, 
				this is GroupByBox ? StyleUtils.Role.GroupByBox : StyleUtils.Role.AddNewBox, out context.ResolutionOrder );

			// SSP 3/14/06 - App Styling
			// 
			if ( context.ResolutionOrder.UseStyleBefore )
				StyleUtils.ResolveAppearance( AppStyling.RoleState.Normal, ref appData, ref context );
            
			// SSP 3/14/06 - App Styling
			// Check for UseControlInfo. Also check for HasAppearance. Enclosed the existing code
			// into the if block.
			// 
			if ( this.HasAppearance && context.ResolutionOrder.UseControlInfo )
			{
				// first try to apply the addnew box's appearance
				//
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( ref appData, 
				//	this.Appearance.Data,
				//	ref context.UnresolvedProps );
				this.Appearance.MergeData( ref appData, ref context.UnresolvedProps );
			}

			// SSP 3/14/06 - App Styling
			// 
			if ( context.ResolutionOrder.UseStyleAfter )
				StyleUtils.ResolveAppearance( AppStyling.RoleState.Normal, ref appData, ref context );

			// default the back and fore color if not already set
			//
			if ( ( (requestedProps & AppearancePropFlags.BackColor) != 0) && 
				!appData.HasPropertyBeenSet(AppearancePropFlags.BackColor) )//&&
				//appData.BackColor == 0 )//TODO: find COLOR_USEDEFAULT
				appData.BackColor = SystemColors.ControlDark;//SS_SYSCOLOR_BTNSHADOW

			if ( ( (requestedProps & AppearancePropFlags.ForeColor) != 0) && 
				!appData.HasPropertyBeenSet(AppearancePropFlags.ForeColor)) //&&
				//appData.ForeColor == 0 )//TODO: find COLOR_USEDEFAULT
				appData.ForeColor = SystemColors.Highlight;//SS_SYSCOLOR_BTNHIGHLIGHT

			// SSP 11/6/01 UWG678 
			// Force the TextHAlign to be Left and TextVAlign to be Middle.
			//
			// SSP 8/3/06 BR14796
			// Only resolve if props are requested. This method needs to be called
			// from some other place and these props need to be resolved only if 
			// requested.
			// --------------------------------------------------------------------------
			//appData.TextHAlign = HAlign.Left;
			//appData.TextVAlign = VAlign.Middle;
			if ( 0 != ( AppearancePropFlags.TextHAlign & context.UnresolvedProps ) )
				appData.TextHAlign = HAlign.Left;

			if ( 0 != ( AppearancePropFlags.TextVAlign  & context.UnresolvedProps ) )
				appData.TextVAlign = VAlign.Middle;
			// --------------------------------------------------------------------------
			
			context.UnresolvedProps &= ~( AppearancePropFlags.TextHAlign | AppearancePropFlags.TextVAlign );

			if ( context.UnresolvedProps == 0 )
				return;

			// if font was requested but not resolved then set a font based on
			// the ctl's ambient font (just bigger)
			//
			if ( (context.UnresolvedProps & AppearancePropFlags.FontData )!= 0 )
			{
				// set the appearance object's font to the default prompt font
				//
				if ( appData.FontData.SizeInPoints == 0.0f )
				{
					// Increase the size (make it a larger font)
					//
					appData.FontData.SizeInPoints = this.Layout.Grid.Font.SizeInPoints * 110 / 100;

				}

				context.UnresolvedProps |= AppearancePropFlags.FontData;
			}

		}
		
		/// <summary>
		/// Refreshes (invalidates the area)
		/// </summary>
		public void Refresh()
		{
			// SSP 10/25/01
			// Implemented
			//
			UIElement elem = this.Layout.UIElement;

			if ( null != elem )
			{
				elem = elem.GetDescendant( this.GetType( ), this);
				
				if ( null != elem )
					elem.Invalidate( true );
			}
		}

		/// <summary>
		/// Gets the cursor to be displayed when the mouse is over the box.
		/// </summary>
        /// <returns>The cursor to be displayed when the mouse is over the box.</returns>
		public System.Windows.Forms.Cursor GetCursor( Infragistics.Win.UltraWinGrid.HeaderUIElement element )
		{
		
			// Call the parent element's GetCursor method so that we pick up
			// the mousepointer setting of the appearance object when we are
			// over the SpecialBoxBase caption
			//
			return element.Parent.Cursor;
			
		}



		// SSP 1/23/03 UWG981
		// Added this virtual method to resolve prompt appearance
		// so that it can be overridden by the GroupByBox
		//
		internal virtual void InternalResolvePromptAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			// SSP 3/16/06 - App Styling
			// Also resolve the AddNewBoxPrompt role settings.
			// 
			// ----------------------------------------------------------------------------------------------
			AppStyling.ResolutionOrderInfo order;
			AppStyling.UIRole role = StyleUtils.GetRole( this.Layout, StyleUtils.Role.AddNewBoxPrompt, out order );
			if ( order.UseStyleBefore || order.UseStyleAfter )
				StyleUtils.ResolveAppearance( role, AppStyling.RoleState.Normal, ref appData, ref flags );
			// ----------------------------------------------------------------------------------------------

			this.ResolveAppearance( ref appData, ref flags );
		}

		internal System.Drawing.Size PromptSize
		{
			get
			{
				Size size = Size.Empty;

				// get the extext of the prompt
				//
				// JJD 1/12/02
				// Make sure there are some characters after whitespace
				// has been trimmed.
				//
				if ( this.Prompt.Trim().Length > 0 )					
				{
					AppearanceData appData = new AppearanceData();
					
					// SSP 1/23/02 UWG981
					// Use the new InternalResolvePropmtAppearance virtual method that will
					// get the appropriate prompt appearance data.
					//
					//this.ResolveAppearance( ref appData, AppearancePropFlags.FontData );
					AppearancePropFlags flags = AppearancePropFlags.FontData;
					this.InternalResolvePromptAppearance( ref appData, ref flags );
				
					// JJD 12/14/01
					// Moved font height logic into CalculateFontHeight
					//
					size = this.Layout.CalculateFontSize( ref appData, this.Prompt );

					// SSP 1/20/04 UWG2911
					// Add couple of pixels to compensate for the MeasureString's misbehavior.
					//
					size.Width += 2;
				}
				
				return size;			
			}
		}



		internal virtual bool DefaultHidden
		{
			get
			{
				return true;
			}
		}	

		internal abstract string DefaultPrompt
		{
			get;
		}		

		/// <summary>
		/// Specifies a custom prompt string that will appear in the box.
		/// </summary><remarks>The AddNew box displays a text message that indicates 
		/// to the user how to add rows to the desired band. The Prompt property 
		/// determines the text that will be displayed. At run-time, you can change 
		/// this property through code to alter the message. This property is not 
		/// available at design-time.</remarks>
		[ LocalizedDescription("LD_SpecialBoxBase_P_Prompt") ]
		[ LocalizedCategory("LC_Display") ]
		[ Localizable( true ) ]
		public string Prompt
		{
			get
			{
				// JJD 1/12/02
				// Instead of setting this.prompt to the default prompt just
				// return the defalut prompt (in case the default prompt changes);
				//
				if ( null == this.prompt || this.prompt.Length < 1 )
					return this.DefaultPrompt;

				return this.prompt;
			}
			set
			{
				if (this.prompt != value )
				{
					// JJD 1/12/02
					// If they set the prompt to a null string then save
					// a string of 1 space so we won't try to use the default
					// prompt.
					//
					if ( value == null || value.Length < 1 )
						this.prompt = " ";
					else
						this.prompt = value;
					
					this.NotifyPropChange( PropertyIds.Prompt );
				}
			}
		}
		
		/// <summary>
		/// Returns true if the Prompt property needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializePrompt() 
		{
			// JJD 1/12/02
			// Only serialize the prompt if it is not a null string;
			//
			return this.prompt != null &&
				   this.prompt.Length > 0;
		}
 
		/// <summary>
		/// Resets the prompt property to this.DefaultPrompt
		/// </summary>
		public void ResetPrompt() 
		{
			// JJD 1/12/02
			// Set this.prompt which will then use the default prompt on the get.
			//
			this.prompt = null;

			this.NotifyPropChange( PropertyIds.Prompt );
		}

		/// <summary>
		/// Returns an empty string so that property window display nothing. 
		/// </summary>
        /// <returns>An empty string so that property window display nothing.</returns>
		public override string ToString() { return string.Empty; }

		internal void GetObjectData( SerializationInfo info,
			StreamingContext context )
		{
						
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			if ( this.ShouldSerializeBorderStyle() )	
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BorderStyle", this.BorderStyle );
				//info.AddValue("BorderStyle", (int)this.BorderStyle );
			}

			if ( this.ShouldSerializeAppearance() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AppearanceHolder", this.appearanceHolder );
				//info.AddValue("AppearanceHolder", this.appearanceHolder );
			}

			if ( this.ShouldSerializeButtonConnectorStyle() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ButtonConnectorStyle", this.ButtonConnectorStyle );
				//info.AddValue("ButtonConnectorStyle", (int)this.ButtonConnectorStyle );
			}

			if ( this.ShouldSerializeButtonConnectorColor() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ButtonConnectorColor", this.ButtonConnectorColor );
				//info.AddValue("ButtonConnectorColor", this.ButtonConnectorColor );
			}

			if ( this.ShouldSerializeHidden() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Hidden", this.hidden );
				//info.AddValue("Hidden", this.hidden );
			}

			if ( this.ShouldSerializePrompt() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Prompt", this.Prompt );
				//info.AddValue("Prompt", this.Prompt );
			}

			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
	
		}


        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected SpecialBoxBase(SerializationInfo info, StreamingContext context)
		{
						
			//since we're not saving properties with default values, we must set the default here
			this.Reset();

			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "Tag":
						// AS 8/15/02
						// Use our tag deserializer.
						//
						//this.tagValue = entry.Value;
						this.DeserializeTag(entry);
						break;

					case "AppearanceHolder":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.appearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						//this.appearanceHolder = (Infragistics.Win.AppearanceHolder)entry.Value;
						break;

					case "BorderStyle":
					{
						//this.BorderStyle = (Infragistics.Win.UIElementBorderStyle)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/20/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.BorderStyle = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.borderStyle );
						//this.BorderStyle = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.BorderStyle);
						break;
					}
				
					case "ButtonConnectorStyle":
					{
						//this.ButtonConnectorStyle = (Infragistics.Win.UIElementBorderStyle)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/20/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.ButtonConnectorStyle = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.buttonConnectorStyle );
						//this.ButtonConnectorStyle = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.ButtonConnectorStyle);
						break;
					}

					case "ButtonConnectorColor":
					{
						//this.ButtonConnectorColor = (Color)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.ButtonConnectorColor = (Color)Utils.DeserializeProperty( entry, typeof(Color), this.buttonConnectorColor );
						//this.ButtonConnectorColor = (System.Drawing.Color)DisposableObject.ConvertValue( entry.Value, typeof(System.Drawing.Color) );
						break;
					}

					case "Hidden":
					{
						//this.Hidden = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Hidden = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.hidden );
						//this.Hidden = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
					}

					case "Prompt":
					{
						//this.Prompt = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Prompt = (string)Utils.DeserializeProperty( entry, typeof(string), this.prompt );
						//this.Prompt = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;
					}
				}
			}

		}

		internal void InitLayout(UltraGridLayout layout)
		{
			this.layout = layout;

			// JJD 10/24/01
			// Call virtual method to do other initialization
			// chores
			//
			this.OnInitLayout();
		}


		internal virtual void OnInitLayout()
		{
			// JJD 10/24/01
			// Init the cellappearance with the appearances collection
			// if necessary
			//
			if ( this.layout != null )
			{
				if ( this.appearanceHolder != null )
					this.appearanceHolder.Collection = this.layout.Appearances;
			}
		}
		
	}

	
}
