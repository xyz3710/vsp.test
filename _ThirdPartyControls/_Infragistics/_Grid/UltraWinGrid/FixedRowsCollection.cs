#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;
using System.Collections.Generic;

// SSP 3/22/05 - NAS 5.2 Fixed Rows
// Added this file.
//

namespace Infragistics.Win.UltraWinGrid
{
	#region FixedRowsCollection

	/// <summary>
	/// Class that stores fixed rows.
	/// </summary>
	public class FixedRowsCollection : SubObjectsCollectionBase
	{
		#region Private Vars

		private RowsCollection parentRowCollection = null;

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private ArrayList rowAddOrder = null;
		private List<UltraGridRow> rowAddOrder = null;

		private int fixedRowsCollectionVersion = 0;
		private int verifiedFixedRowsCollectionVersion = 0;
		private int verifiedGrandVersion    = -1;
		private int verifiedRowCollectionVersion = -1;
		private int verifiedSortVersion = -1;

		// Rows before this index or after this index are not fixed depending on
		// the whether the rows are fixed at bottom or top respectively.
		//
		private int fixedResolvedBreakIndex = -1;
		private bool inVerifyFixedRows = false;

		#endregion // Private Vars

		#region Constructor

		internal FixedRowsCollection( RowsCollection parentRowCollection )
		{
			GridUtils.ValidateNull( "parentRowCollection", parentRowCollection );

			this.parentRowCollection = parentRowCollection;
		}

		#endregion // Constructor

		#region Public Properties

		#region InitialCapacity

		/// <summary>
		/// Abstract property that specifies the initial capacity of the collection.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		protected override int InitialCapacity
		{
			get
			{
				return 10;
			}        
		}

		#endregion // InitialCapacity

		#region IsReadOnly

		/// <summary>
		/// True if the collection is read only
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		#endregion // IsReadOnly

		#region ParentRows
		
		/// <summary>
		/// Returns the associated row collection.
		/// </summary>
		public RowsCollection ParentRows
		{
			get
			{
				return this.parentRowCollection;
			}
		}

		#endregion // ParentRows

		#region Count

		/// <summary>
		/// Overridden. Gets the number of items currently contained in the collection.
		/// </summary>
		public override int Count
		{
			get
			{
				this.VerifyFixedRows( );

				return base.Count;
			}
		}

		#endregion // Count

		#region Indexer

		/// <summary>
		/// Indexer.
		/// </summary>
		public UltraGridRow this[ int index ]
		{
			get
			{
				return (UltraGridRow)this.GetItem( index );
			}
		}

		#endregion // Indexer

		#endregion // Public Properties

		#region Protected Properties/Methods

		#region CreateArray

		/// <summary>
		/// Overridden. Creates an UltraGridRow array of the same length as the collection. It's initialized to null (Nothing in VB).
		/// </summary>
		/// <returns>The newly created object array</returns>
		/// <remarks>This is normally overridden in a derived class to allocate a type safe array.</remarks>
		protected override object[] CreateArray( )
		{
			return new UltraGridRow[ this.Count ];
		}

		#endregion // CreateArray

		#endregion // Protected Properties/Methods

		#region Public Methods

		#region GetItem

		/// <summary>
		/// Overridden. Returns the item at the specified index.
		/// </summary>
        /// <param name="index">Index of the object to retrieve.</param>
        /// <returns>The object at the index</returns>
        public override object GetItem(int index)
		{
			this.VerifyFixedRows( );
			return base.GetItem( index );
		}

		#endregion // GetItem

		#region Add

		/// <summary>
		/// Adds the specified row to the FixedRowsCollection. Throws an exception if the row already
		/// exists in the collection.
		/// </summary>
		/// <param name="row">The row to be fixed.</param>
		public void Add( UltraGridRow row )
		{
			this.Insert( this.Count, row );
		}

		internal void Add( UltraGridRow row, bool top )
		{
			this.Insert( top ? 0 : this.Count, row );
		}

		#endregion // Add

		#region Insert

		/// <summary>
		/// Inserts the specified row in the collection at the specified index. Throws an 
		/// exception if the row already exists in the collection.
		/// </summary>
		/// <param name="index">The index into which the row will be inserted.</param>
        /// <param name="row">The UltraGridRow to be inserted into the collection.</param>
		public void Insert( int index, UltraGridRow row )
		{
			this.ValidateRow( row );

			// Throw an exception if the row already exists in the collection.
			//
			if ( this.Contains( row ) )
				throw new InvalidOperationException( "Row already exists in the collection." );

			this.InternalInsert( index, row );
			this.RowAddOrder.Add( row );

			// Bump the collection version number.
			//
			this.BumpFixedRowsCollectionVersion( );

			// When a row gets fixed invalidate it.
			//
			row.InvalidateItemAllRegions( true, true );

			this.NotifyPropChange( PropertyIds.Add );
		}

		#endregion // Insert

		#region Remove

		/// <summary>
		/// Removes the speicifed row from the collection. If the row does not exist in the
		/// collection then does nothing.
		/// </summary>
		/// <param name="row"></param>
		public void Remove( UltraGridRow row )
		{
			int index = this.IndexOf( row );
			if ( index >= 0 )
				this.RemoveAt( index );
		}

		#endregion // Remove

		#region RemoveAt

		/// <summary>
		/// Removes the row at the specified index. Throws an exception if the specified index 
		/// is out of range.
		/// </summary>
		/// <param name="index"></param>
		public void RemoveAt( int index )
		{
			UltraGridRow row = this[ index ];
			this.InternalRemove( index );
			this.RowAddOrder.Remove( row );

			// Reposition the row in the proper sort order into the row collection.
			// Also check to make sure that the parent collection of the row hasn't 
			// changed since it was adde to this fixed rows collection (which could 
			// happen for example when rows are grouped.
			// 
			if ( row.ParentCollection == this.ParentRows )
				this.ParentRows.MoveRowToCorrectSortOrder( row );

			// Bump the collection version number.
			//
			this.BumpFixedRowsCollectionVersion( );

			// When a row gets unfixed invalidate it.
			//
			row.InvalidateItemAllRegions( true, true );

			this.NotifyPropChange( PropertyIds.Remove );
		}

		#endregion // RemoveAt

		#region Clear

		/// <summary>
		/// Clears the collection.
		/// </summary>
		public void Clear( )
		{
			this.InternalClear( );

			try
			{
				// Move the unfixed rows to their correct sort order.
				//
				foreach ( UltraGridRow row in this.RowAddOrder )
				{
					// Reposition the row in the proper sort order into the row collection.
					// Also check to make sure that the parent collection of the row hasn't 
					// changed since it was adde to this fixed rows collection (which could 
					// happen for example when rows are grouped.
					// 
					if ( row.ParentCollection == this.ParentRows )
						this.ParentRows.MoveRowToCorrectSortOrder( row );
				}
			}
			finally
			{
				this.RowAddOrder.Clear( );
				this.BumpFixedRowsCollectionVersion( );
			}			

			this.NotifyPropChange( PropertyIds.Clear );
		}

		#endregion // Clear

		#region RefreshSort

		/// <summary>
		/// Resorts the collection based on sort columns (band's SortedColumns).
		/// </summary>
		public void RefreshSort( )
		{
			if ( this.Band.HasSortedColumns )
			{
				this.verifiedSortVersion = this.Band.SortedColumns.SortVersion;

				if ( this.Count > 1 )
				{
					UltraGridRow[] rows = (UltraGridRow[])this.All;
					Infragistics.Win.Utilities.SortMerge( rows, new RowsCollection.RowsSortComparer( this.ParentRows ) );

					ArrayList list = this.List;
					list.Clear( );
					foreach ( UltraGridRow row in rows )
						list.Add( row );

					this.BumpFixedRowsCollectionVersion( );

					Debug.Assert( this.RowAddOrder.Count == this.List.Count );
				}
			}
		}

		#endregion // RefreshSort

		#region IndexOf

        /// <summary>
        /// Returns the index of the item in the collection that has the passed in key or -1 if key not found.
        /// </summary>
        /// <param name="obj">The object whose index in the collection will be determined.</param>
        /// <returns>The index of the item in the collection that has the passed in key, or -1
        /// if no item is found.</returns>
        public override int IndexOf(object obj) 
		{
			this.VerifyFixedRows( );
			return base.IndexOf( obj );
		}

		#endregion // IndexOf

		#endregion // Public Methods

		#region Private/Internal Properties/Methods

		#region Band

		internal UltraGridBand Band
		{
			get
			{
				return this.ParentRows.Band;
			}
		}

		#endregion // Band

		#region Layout

		internal UltraGridLayout Layout
		{
			get
			{
				return this.Band.Layout;
			}
		}

		#endregion // Layout

		#region TopFixed

		internal bool TopFixed
		{
			get
			{
				return FixedRowStyle.Bottom != this.Band.FixedRowStyleResolved;
			}
		}

		#endregion // TopFixed

		#region TopFixed

		internal bool BottomFixed
		{
			get
			{
				return ! this.TopFixed;
			}
		}

		#endregion // TopFixed
		
		#region RowAddOrder
		
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private ArrayList RowAddOrder
		private List<UltraGridRow> RowAddOrder
		{
			get
			{
				if ( null == this.rowAddOrder )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//this.rowAddOrder = new ArrayList( 4 );
					this.rowAddOrder = new List<UltraGridRow>( 4 );
				}

				return this.rowAddOrder;
			}
		}

		#endregion // RowAddOrder

		#region ValidateRow

		private void ValidateRow( UltraGridRow row )
		{
			if ( ! this.ParentRows.Contains( row ) )
				throw new ArgumentException( "Row must be from the associated RowsCollection.", "row" );

			if ( ! row.IsDataRow && ! row.IsGroupByRow )
				throw new ArgumentException( "Only data rows and group-by rows can be added to the fixed rows collection.", "row" );
		}

		#endregion // ValidateRow

		#region BumpFixedRowsCollectionVersion

		internal void BumpFixedRowsCollectionVersion( )
		{
			this.fixedRowsCollectionVersion++;

			// SSP 5/3/05
			//
			//this.verifiedGrandVersion--;
			this.Layout.DirtyGridElement( true );
			this.Layout.BumpGrandVerifyVersion( );
		}

		#endregion // BumpFixedRowsCollectionVersion

		#region VerifyFixedRows

		internal void VerifyFixedRows( )
		{
			if ( this.verifiedGrandVersion == this.Layout.GrandVerifyVersion )
				return;

			if ( this.inVerifyFixedRows )
				return;

			this.inVerifyFixedRows = true;			
			try
			{
				RowsCollection parentRows = this.ParentRows;
				UltraGridBand band = this.Band;
				UltraGridLayout layout = band.Layout;
				ArrayList list = this.List;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList rowsAddOrder = this.RowAddOrder;
				List<UltraGridRow> rowsAddOrder = this.RowAddOrder;

				Debug.Assert( rowsAddOrder.Count == list.Count );

				bool somethingChanged = false;

				// Remove rows that have been deleted from the main rows collection.
				//
				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.verifiedRowCollectionVersion != parentRows.VerifyVersion + parentRows.verifiedGroupByHierarchyBandsVersion )
				//{
				//    this.verifiedRowCollectionVersion = this.ParentRows.VerifyVersion + parentRows.verifiedGroupByHierarchyBandsVersion;
				int currentRowCollectionVersion = parentRows.VerifyVersion + parentRows.verifiedGroupByHierarchyBandsVersion;

				if ( this.verifiedRowCollectionVersion != currentRowCollectionVersion )
				{
					this.verifiedRowCollectionVersion = currentRowCollectionVersion;

					for ( int i = rowAddOrder.Count - 1; i >= 0; i-- )
					{
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//UltraGridRow row = (UltraGridRow)rowAddOrder[i];
						UltraGridRow row = rowAddOrder[ i ];

						if ( ! row.IsStillValid || row.ParentCollection != this.ParentRows )
						{
							this.Remove( row );
							somethingChanged = true;
						}
					}
				}

				// Ensure that the number of fixed rows do not violate the FixedRowsLimit settings.
				//
				int limit = band.FixedRowsLimitResolved;
				while ( limit > 0 && rowAddOrder.Count > limit )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//this.Remove( (UltraGridRow)rowAddOrder[0] );
					this.Remove( rowAddOrder[ 0 ] );

					somethingChanged = true;
				}

				// Ensure that the fixed rows are sorted, if FixedRowSortOrder setting dictates so.
				//
				SortedColumnsCollection sortedCols = band.InternalSortedColumns;
				if ( null != sortedCols && this.verifiedSortVersion != sortedCols.SortVersion 
					&& FixedRowSortOrder.Sorted == band.FixedRowSortOrderResolved )
				{
					this.RefreshSort( );
					somethingChanged = true;
				}

				// Figure out which rows are actually fixed and which ones are not. When rows are
				// fixed at the top, the rows after the first expanded row are not fixed. When rows
				// are fixed at the bottom it works in a similar fashion. Also following code moves
				// the fixed rows in the parent row collection to top or bottom depending on the
				// fixed rows style by calling MoveFixedRowsToProperLocation method.
				//
				int overallFixedRowsVersion = this.fixedRowsCollectionVersion + layout.FixedRowsCollectionsVersion;
				if ( overallFixedRowsVersion != this.verifiedFixedRowsCollectionVersion )
				{
					this.verifiedFixedRowsCollectionVersion = overallFixedRowsVersion;
					somethingChanged = true;

					// Make sure that the fixed rows in the parent row collection are in their proper 
					// location.
					//
					this.ParentRows.MoveFixedRowsToProperLocation( );

					if ( this.TopFixed )
					{
						// Rows are fixed at the top.
						//
						this.fixedResolvedBreakIndex = -1;
				
						for ( int i = 0; i < list.Count; i++ )
						{
							UltraGridRow row = (UltraGridRow)list[i];

							// Row scroll count of 0 means it's hidden, 1 means it's visible
							// but no children visible (not expanded or no children) and 
							// more than 1 means is expanded with visible children.
							//
							int rowScrollCount = row.ScrollCountInternal;
							this.fixedResolvedBreakIndex++;
							if ( rowScrollCount > 1 )
								break;
						}
					}
					else
					{
						// Rows are fixed at the bottom.
						//
						this.fixedResolvedBreakIndex = list.Count;

						for ( int i = list.Count - 1; i >= 0; i-- )
						{
							UltraGridRow row = (UltraGridRow)list[i];

							// Row scroll count of 0 means it's hidden, 1 means it's visible
							// but no children visible (not expanded or no children) and 
							// more than 1 means is expanded with visible children.
							//
							int rowScrollCount = row.ScrollCountInternal;
							if ( rowScrollCount > 1 )
								break;

							this.fixedResolvedBreakIndex--;
						}
					}
				}

				// Cause the rows collection to update the cached special rows array.
				//
				if ( somethingChanged )
				{
					this.ParentRows.verifiedSpecialRowsCacheVersion--;
					this.Layout.BumpGrandVerifyVersion( );
				}

				this.verifiedGrandVersion = layout.GrandVerifyVersion;
			}
			finally
			{
				this.inVerifyFixedRows = false;
			}
		}

		#endregion // VerifyFixedRows

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#region EnsureFixedRowsLimitConstraint

		//private void EnsureFixedRowsLimitConstraint( )
		//{
		//    this.VerifyFixedRows( );
		//}

		//#endregion // EnsureFixedRowsLimitConstraint

		#endregion Not Used

		#region IsFixedResolved

		internal bool IsFixedResolved( UltraGridRow row )
		{
			// Card-view doesn't support fixed rows.
			//
			if ( this.ParentRows.SupportsFixedRows )
			{
				this.VerifyFixedRows( );

				int index = this.IndexOf( row );
				return index >= 0 && 
					( this.TopFixed ? index <= this.fixedResolvedBreakIndex : index >= this.fixedResolvedBreakIndex )
					// SSP 5/15/06 BR12333
					// Check for the row being hidden or filtered out.
					// 
					&& ! row.HiddenInternal;
			}

			return false;
		}

		#endregion // IsFixedResolved

		#region Move
		
		internal void Move( UltraGridRow row, int newIndex )
		{
			ArrayList list = this.List;
			int index = list.IndexOf( row );
			if ( index >= 0 && newIndex >= 0 && newIndex < list.Count  )
			{
				list.RemoveAt( index );
				list.Insert( newIndex, row );
			}
			else
			{
				Debug.Assert( false );
			}
		}

		#endregion // Move

		#region HasFixedResolvedRow

		internal bool HasFixedResolvedRows
		{
			get
			{
				bool topFixed = this.TopFixed;
				int i = topFixed ? 0 : this.Count - 1;
				int step = topFixed ? 1 : -1;
				for ( ; i >= 0 && i < this.Count; i += step )
				{
					UltraGridRow row = this[ i ];
					if ( row.FixedResolved )
					{
						if ( ! row.HiddenInternal )
							return true;
					}
					else
						break;
				}

				return false;
			}
		}

		#endregion // HasFixedResolvedRow

		#endregion // Private/Internal Properties/Methods
	}

	#endregion // FixedRowsCollection

}
