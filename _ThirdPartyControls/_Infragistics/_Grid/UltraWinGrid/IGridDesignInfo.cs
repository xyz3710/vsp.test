#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.Layout;
using System.Diagnostics;

// SSP 2/11/03 - Row Layout Functionality
// Added IGridDesignInfo interface so the designers in design assembly can access
// internal properties from the grid via this interface without having to expose them 
// to the user.
//


namespace Infragistics.Win.UltraWinGrid.Design
{
	/// <summary>
	/// For internal use.
	/// </summary>
	public interface IGridDesignInfo
	{
		/// <summary>
		/// Gets the cell area layout manager for the passed in band.
		/// </summary>
		/// <param name="band"></param>
		/// <returns></returns>
		GridBagLayoutManager GetCellAreaGridBagLayoutManager( UltraGridBand band );

		/// <summary>
		/// Gets the header area layout manager for the passed in band.
		/// </summary>
		GridBagLayoutManager GetHeaderAreaGridBagLayoutManager( UltraGridBand band );

		/// <summary>
		/// Gets the summary area layout manager for the passed in band.
		/// </summary>
		GridBagLayoutManager GetSummaryAreaGridBagLayoutManager( UltraGridBand band );

		// SSP 8/6/03 UWG2567
		// Added EnsureColScrollRegionMetricsInitialized method to the interface.
		// The row layout designer needs a way to ensure that the col scroll regions'
		// metrics is initialized.
		//
		/// <summary>
		/// If the col scroll region metrics is dirty then initializes it.
		/// </summary>
		void EnsureColScrollRegionMetricsInitialized( );

		// SSP 3/10/04 - Designer Usability Improvements Related
		// Added ExtractDataStructure method to be able to extract the data structure from
		// a data source.
		//
		/// <summary>
		/// Extracts the data structure from the specified data source. This method will null out the existing data source on the UltraGrid.
		/// </summary>
		/// <param name="dataSource"></param>
		/// <param name="dataMember"></param>
		void ExtractDataStructure( object dataSource, string dataMember );

		// SSP 7/30/04 UWG3488
		// Added DesignMode property.
		//
		/// <summary>
		/// For internal use only. Set this to true to force the grid to behave as if it 
		/// were in design mode even though its Component.DesignMode returns false. This
		/// is meant to be used by the designer dialogs to show two dummy rows in the 
		/// preview grids. Note: This property returns whatever it was set to (in other
		/// words the value of the associated member variable).
		/// </summary>
		bool DesignMode { get; set; }

        // MRS v7.2 - Document Exporter
        // Added this method to get the GridBagLayoutManager for an individual row. 
        /// <summary>
        /// Gets the cell area layout manager for the passed in row.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        GridBagLayoutManager GetCellAreaGridBagLayoutManager(UltraGridRow row);

        // MRS v7.2 - Document Exporter
        /// <summary>
        /// Returns the Owner for the editor of the specified row and column.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        EmbeddableEditorOwnerBase GetEditorOwner(UltraGridRow row, UltraGridColumn column);

        // MRS v7.2 - Document Exporter
        /// <summary>
        /// Get the height of a Summary Value from it's LayoutItem. This only works for a Summary in 
        /// RowLayout mode and a summary that is positioned using a SummaryPositionColumn (not free-form).
        /// </summary>
        /// <param name="summaryLayoutItem">The SummaryLayoutItem of the SummaryPositionColumn for the summary.</param>
        /// <param name="level">The level of the summary.</param>
        /// <returns></returns>
        int GetSummaryHeight(ILayoutItem summaryLayoutItem, int level);

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout
        
        /// <summary>
        /// Gets the cell area layout manager for the specified ILayoutGroup.
        /// </summary>
        GridBagLayoutManager GetCellAreaHierarchicalGridBagLayoutManager(ILayoutGroup group);

        /// <summary>
        /// Gets the header area layout manager for the specified ILayoutGroup.
        /// </summary>
        GridBagLayoutManager GetHeaderAreaHierarchicalGridBagLayoutManager(ILayoutGroup group);

        /// <summary>
        /// Gets the summary area layout manager for the specified ILayoutGroup.
        /// </summary>
        GridBagLayoutManager GetSummaryAreaHierarchicalGridBagLayoutManager(ILayoutGroup group);

		// MD 1/16/09 - Groups in RowLayout
		// This may need to be set by the designer.
		/// <summary>
		/// Gets or sets a reference to the drag strategy used at design-time.
		/// </summary>
		GridBagLayoutDragStrategy GridBagLayoutDragStrategy { get; set; }

        #endregion // NAS 9.1 - Groups in RowLayout

        // MRS 5/20/2009 - TFS17800
        /// <summary>
        /// Returns the LayoutManager for the given SummaryRow based.
        /// </summary>
        /// <param name="summaryRow">An UltraGridSummaryRow</param>
        /// <returns></returns>
        GridBagLayoutManager GetSummaryRowLayoutManager(UltraGridSummaryRow summaryRow);
    }
}
