#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Diagnostics;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinScrollBar;

namespace Infragistics.Win.UltraWinGrid
{
	#region CardAreaUIElement class
	/// <summary>
	/// The CardAreaUIElement contains RowUIElements.
	/// </summary>
	public class CardAreaUIElement : AdjustableUIElement
	{
		//	BF 1.26.05	NAS2005 Vol1 - CardView vertical scrolling
		private bool hasVerticalScrollbar = false;

		#region Constructors
		internal CardAreaUIElement(UIElement parent) : base(parent, false, true)
		{
		}

		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Creates a new instance of the <see cref="CardAreaUIElement"/> class.
		/// </summary>
		/// <param name="parent">The UIElement of which this element is a child.</param>
		/// <param name="firstRow">A reference to the first <see cref="UltraGridRow"/> being displayed by this instance.</param>
		public CardAreaUIElement(UIElement parent, UltraGridRow firstRow) : this(parent)
		{
			this.InitializeRow(firstRow);
		}

		#endregion Constructors

		#region Member Variables

		// SSP 5/16/05 - Optimization
		// Row col region intersection element had an optimization for not causing the existing
		// row elements to reposition their descendants. It never worked so made it work.
		//
		//private	 Size				origParentSize;		
		internal Rectangle lastClipRect;

		//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling
		//
		//	We could potentially have more than one scrollbar (horizontal
		//	and vertical), so we need 2 separate members now.
		//
		//internal ScrollBarInfo		scrollBarInfo				= null;
		internal ScrollBarInfo		hScrollBarInfo				= null;
		internal ScrollBarInfo		vScrollBarInfo				= null;

		//	BF 11.9.04	NAS2005 Vol1 - CardView vertical scrolling	
		//
		//	Hold references to the scrollbar elements so we can simplify
		//	MouseWheel logic.
		//
		private	ScrollBarUIElement	hScrollbarElement			= null;
		private	ScrollBarUIElement	vScrollbarElement			= null;

		private	 int				lastProcessedResizeDelta	= 0;		
		private  int				heightAtStartOfResize		= 0;
		private	 UltraGridRow		firstVisibleRow				= null;

		// JM UWG977 01-23-02 Cache a reference to the band (set this in InitializeRow).
		private  UltraGridBand		band						= null;

		// SSP 7/1/02
		// Compressed card view.
		// A flag indicating we are currently scrolling. This is reset to false when
		// end scroll is received.
		// 
		private bool currentlyScrolling = false;
		
		// JJD 1/08/04 - Accessibility
		//
		private System.Windows.Forms.AccessibleObject scrollbarAccessibilityObject;

		// SSP 11/18/05 BR07794
		// We need to recalculate the formula cells in newly scrolled into view card rows.
		// 
		private UltraGridRow calcManager_lastFirstVisibleRow = null;

		#endregion Member Variables

		#region Base Class Overrides

			#region BorderStyle

		/// <summary>
		/// Returns or sets a value that determines the border style of an object.
		/// </summary>
		public override UIElementBorderStyle BorderStyle
		{
			get	{return this.Band.BorderStyleCardAreaResolved;}
		}

			#endregion BorderStyle

			#region BorderSides

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get	{return Border3DSide.All;}
		}

			#endregion BorderSides
				
			#region GetAdjustmentRange

		/// <summary>
		/// Returns the range limits for adjusting the element in either or both
		/// dimensions. It also returns the initial rects for the vertical and horizontal
		/// bars that will need to be inverted during the mouse drag operation.
		/// </summary>
		/// <param name="point">The point where the mouse is in client coordinates</param>
		/// <param name="range">Returned limits</param>
		public override void GetAdjustmentRange(System.Drawing.Point point,
												out UIElementAdjustmentRangeParams range)
		{
			// Initialize
			range = new UIElementAdjustmentRangeParams();

			if (this.SupportsUpDownAdjustmentsFromPoint(point))
			{
				range.maxDeltaUp      = - ( this.Rect.Height - 1 );

				RowColRegionIntersectionUIElement rowColRegionElement = this.GetAncestor(typeof(RowColRegionIntersectionUIElement)) as RowColRegionIntersectionUIElement;
				if (rowColRegionElement != null)
					range.maxDeltaDown    = rowColRegionElement.Rect.Bottom   - this.Rect.Bottom;
				else
					range.maxDeltaDown    = this.Parent.Rect.Bottom   - this.Rect.Bottom;

				range.upDownAdjustmentBar.Width  = this.Parent.Rect.Width;
				range.upDownAdjustmentBar.Height = 0;
				range.upDownAdjustmentBar.Offset( this.Parent.Rect.Left, this.Rect.Bottom );
			}
		}

			#endregion GetAdjustmentRange

			#region InitAppearance 

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
											   ref AppearancePropFlags	requestedProps)
		{
			// Resolve the appearance.
			// SSP 7/2/03 UWG2437
			// Don't use this.GetType. Use CardAreaUIElement because in the row-layout designer, we
			// derive from this class.
			//
			//ResolveAppearanceContext context = new ResolveAppearanceContext(this.GetType(), requestedProps);
			ResolveAppearanceContext context = new ResolveAppearanceContext(typeof( CardAreaUIElement ), requestedProps);

			// SSP 3/13/06 - App Styling
			// 
			context.Role = StyleUtils.GetRole( this.Band, StyleUtils.Role.CardArea, out context.ResolutionOrder );

			// JM UWG977 01-23-02  Access the band property directly which will return
			//					   a cached reference to our band in case FirstVisibleRow is null.
			//this.FirstVisibleRow.Band.ResolveAppearance(ref appearance, ref context);
			this.Band.ResolveAppearance(ref appearance, ref context);
		}

			#endregion InitAppearance 

			#region OnDispose

		/// <summary>
		/// Called when the object is being disposed.
		/// </summary>
		protected override void OnDispose()
		{
			if (this.hScrollBarInfo != null)
			{
				//	Unhook from the VScroll control's notifications
				//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling

				//	this.scrollBarInfo.Scroll		-= this.ScrollHandler;
				//	this.scrollBarInfo.ValueChanged -= this.ScrollValueChangedHandler;
				//
				//	this.scrollBarInfo.Dispose();
				//	this.scrollBarInfo = null;					

				this.hScrollBarInfo.Scroll		-= this.ScrollHandler;
				this.hScrollBarInfo.ValueChanged -= this.ScrollValueChangedHandler;

				this.hScrollBarInfo.Dispose();
				this.hScrollBarInfo = null;
			}

			if ( this.vScrollBarInfo != null )
			{
				this.vScrollBarInfo.Scroll		-= this.ScrollHandler;
				this.vScrollBarInfo.ValueChanged -= this.ScrollValueChangedHandler;

				this.vScrollBarInfo.Dispose();
				this.vScrollBarInfo = null;					
			}

			if (this.PrimaryContext != null)
				((RowsCollection)this.PrimaryContext).SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			// JJD 1/11/02
			// Call the base implementation
			//
			base.OnDispose();
		}

			#endregion OnDispose

			#region OnElementAdjustmentStart

		/// <summary>
        /// Called when a mousedown is received and a resize operation is started.
		/// </summary>
        /// <param name="isUpDownAdjustment">isUpDownAdjustment</param>
        /// <param name="initialUpDownAdjustmentPointInBottomBorder">initialUpDownAdjustmentPointInBottomBorder</param>
        /// <param name="isLeftRightAdjustment">isLeftRightAdjustment</param>
        /// <param name="initialLeftRightAdjustmentPointInRightBorder">initialLeftRightAdjustmentPointInRightBorder</param>
		protected override void OnElementAdjustmentStart(bool isUpDownAdjustment, 
														 bool initialUpDownAdjustmentPointInBottomBorder,
														 bool isLeftRightAdjustment,
														 bool initialLeftRightAdjustmentPointInRightBorder)
		{
			this.lastProcessedResizeDelta		= 0;
			this.heightAtStartOfResize	= this.Rect.Height;

			// Exit edit mode.
			if (this.Band.Layout.ActiveCell != null)
				this.Band.Layout.ActiveCell.ExitEditMode();
		}

			#endregion OnElementAdjustmentStart

			#region OnNewDeltaY

		/// <summary>
		/// Called when the the Y delta has changed.
		/// </summary>
		protected override void OnNewDeltaY(int newDeltaY)
		{
			base.OnNewDeltaY(newDeltaY);

			// If we're in variable height mode, don't process this delta change if
			// it does not exceed a certain threshold relative to the last delta change we
			// processed.  We do this to avoid generating a lot of repaints.
			if (this.VariableHeight)
			{
				if (Math.Abs(this.lastProcessedResizeDelta - newDeltaY) < this.Band.CardLabelHeightDefault)
					return;

				this.InvalidateAfterCardAreaResize();

				this.lastProcessedResizeDelta = newDeltaY;
			}

			// Calculate the new height.
			int newHeight = this.heightAtStartOfResize + newDeltaY;

			// Save the old area height
			int oldHeight = this.Rows.CardAreaHeight;

			// Set the card area height on our associated rows collection.
			this.Rows.CardAreaHeight = newHeight;

			// If the setting has changed, repaint.
			if (this.Rows.CardAreaHeight != oldHeight)
			{
				this.InvalidateAfterCardAreaResize();
			}
		}

			#endregion OnNewDeltaY

			#region SupportsUpDownAdjustmentsFromBottomBorder

		/// <summary>
		/// True if this element supports up and down adjustments by grabbing the bottom border.
		/// </summary>
		protected override bool SupportsUpDownAdjustmentsFromBottomBorder
		{
			get
			{ 
				if (this.Band.Index == 0 && this.Band.HasGroupBySortColumns == false)
					return false;

				if (this.VariableHeight)
					return true;

				if (this.Band.CardSettings.MaxCardAreaRows	== 1)
					return false;

				// JM UWG1019 - No sizing possible if only 1 row exists.
				if (this.Rows.VisibleRowCount < 2)
					return false;

				return true;
			}
		}

			#endregion SupportsUpDownAdjustmentsFromBottomBorder

			#region ClipChildren

		//	BF 11.8.04	NAS2005 Vol1 - CardView vertical scrolling
		//	Return true from ClipChildren now, so that the CardAreaScrollRegionUIElement
		//	is clipped by this element, since we now display a vertical scrollbar when necessary.

		/// <summary>
		/// Returns whether this element's child elements are clipped
		/// by its bounding rectangle.
		/// </summary>
		protected override bool ClipChildren { get{ return true; } }

			#endregion ClipChildren

			#region Mouse Panning
		//
		//	BF 11.8.04	NAS2005 Vol1 - CardView vertical scrolling
		//	Implemented mouse panning support for the CardAreaUIElement.
		//
			#region SupportsVerticalMousePanning
		/// <summary>
		/// Returns whether this element is supporting vertical mouse panning
		/// </summary>
		protected override bool SupportsVerticalMousePanning
		{
			//	Return true if there is a vertical scrollbar being displayed
			//	Note that there is no reason to check the enabled state of
			//	the vertical scrollbar, since it is only displayed when needed.
			//
			get{ return this.NeedsVerticalScrollbar( this.RectInsideBorders.Height - this.Band.CardAreaHorizontalScrollbarHeightResolved ); }
		}
			#endregion SupportsVerticalMousePanning

			#region SupportsHorizontalMousePanning
		/// <summary>
		/// Returns whether this element is supporting horizontal mouse panning
		/// </summary>
		protected override bool SupportsHorizontalMousePanning
		{
			get
			{ 
				//	Return true if there is a horizontal scrollbar being displayed,
				//	and it is enabled (can be disabled if there is nothing to scroll)
				return this.hScrollBarInfo != null && this.HScrollBarInfo.EnabledResolved;
			}
		}
			#endregion SupportsHorizontalMousePanning

			#region OnMousePanVertical
		/// <summary>
		/// Called when the element is panned in the up or down direction.
		/// </summary>
		/// <param name="delta">The amount by which the element was panned.</param>
		protected override void OnMousePanVertical( int delta )
		{
			//	Get the magnitude of the pan
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//int absoluteDelta = System.Math.Abs( delta );

			ScrollBarAction scrollbarAction = (ScrollBarAction)0;

			//	Note that we only use SmallIncrement / SmallDecrement here,
			//	because the viewable area of a card is relatively small.
			scrollbarAction = (delta < 0) ? ScrollBarAction.SmallDecrement : ScrollBarAction.SmallIncrement;

			this.VScrollBarInfo.PerformAction( scrollbarAction );
		}
			#endregion OnMousePanVertical

			#region OnMousePanHorizontal
		/// <summary>
		/// Called when the element is panned in the left or right direction.
		/// </summary>
		/// <param name="delta">The amount by which the element was panned.</param>
		protected override void OnMousePanHorizontal( int delta )
		{
			//	Get the magnitude of the pan; if it is greater than
			//	100 (arbitrary), we will perform a large increment
			//	rather than a small one.
			int absoluteDelta = System.Math.Abs( delta );

			ScrollBarAction scrollbarAction = (ScrollBarAction)0;

			if ( delta < 0 )
			{
				scrollbarAction =	absoluteDelta > 100 ?
									ScrollBarAction.LargeDecrement :
									ScrollBarAction.SmallDecrement;
			}
			else
			{
				scrollbarAction =	absoluteDelta > 100 ?
									ScrollBarAction.LargeIncrement :
									ScrollBarAction.SmallIncrement;
			}

			this.HScrollBarInfo.PerformAction( scrollbarAction );
		}
			#endregion OnMousePanHorizontal

			#endregion	Mouse Panning Support

			#region PositionChildElements

		//	BF 11.2.04	NAS2005 Vol1 - CardView vertical scrolling
		//
		//	Reimplemented PositionChildElements because this element
		//	now has only the scrollbars and the CardAreaScrollRegionUIElement
		//	as its children. The RowUIElements and CardLabelAreaUIElements are
		//	now children of the CardAreaScrollRegionUIElement.

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			// Save the current child elements collection so we can
			// potentially reuse some of the elements.
			UIElementsCollection oldElements = this.ChildElements;
	
			// Clear the current child elements collection.
			this.childElementsCollection = null;
	
			Rectangle rectInsideBorders = this.RectInsideBorders;
			Rectangle rectWork = rectInsideBorders;

			this.hScrollbarElement = null;
			this.vScrollbarElement = null;

			if ( ! this.RowLayoutDesignerElement )
			{
				//	Determine whether we need a vertical scrollbar, which will
				//	in turn factor into the width of the horizontal scrollbar
				int cardAreaHorizontalScrollbarHeightResolved = this.Band.CardAreaHorizontalScrollbarHeightResolved;
				int cardAreaHeight = rectInsideBorders.Height - cardAreaHorizontalScrollbarHeightResolved;
				int requiredHeight = 0;
				bool needsVerticalScrollbar = this.NeedsVerticalScrollbar( cardAreaHeight, out requiredHeight );

				//	BF 1.26.05	NAS2005 Vol1 - CardView vertical scrolling
				this.hasVerticalScrollbar = needsVerticalScrollbar;

				//	BF 1.19.05	BR01834
				//
				//	When this element is extracted, its width is calculated
				//	at a point when the height has not yet been updated.
				//	The result of this is that the vertical scrollbar might not
				//	accounted for in that width, but we do account for it here,
				//	resulting in one less card being displayed than should be
				//	displayed. Verify the width here so we account for the vertical
				//	scrollbar's width as appropriate.
				//
				if ( this.Band.Index != 0 && needsVerticalScrollbar )
				{
					int calculatedWidth = this.CalculateRequiredCardAreaWidth( rectInsideBorders.Width + SystemInformation.VerticalScrollBarWidth );
					if ( calculatedWidth > rectInsideBorders.Width )
					{
						Rectangle newRect = this.Rect;
						newRect.Width += SystemInformation.VerticalScrollBarWidth;
						this.Rect = newRect;
						rectInsideBorders = this.RectInsideBorders;
						rectWork = rectInsideBorders;
					}
				}

				#region Horizontal scrollbar

				// Set up the horizontal scrollbar...
				this.ResetHorizontalScrollBarMetricsAndValue();
	
				if ( null != this.Rows && this.Rows.IsCardAreaHorizontalScrollbarVisible )
				{
					// Try to reuse an element from the old elements list.
					ScrollBarUIElement hScrollbarElement = CardAreaUIElement.ExtractExistingElement(oldElements, typeof(ScrollBarUIElement), true) as ScrollBarUIElement;
					if (hScrollbarElement == null)
						hScrollbarElement = new ScrollBarUIElement(this, this.HScrollBarInfo);
	
					//	Hold a reference to the element
					this.hScrollbarElement = hScrollbarElement;

					//	Calculate the position and size of the horizontal scrollbar
					Rectangle hScrollRect	= rectInsideBorders;
					hScrollRect.Y		= hScrollRect.Bottom - SystemInformation.HorizontalScrollBarHeight;
					hScrollRect.Height	= SystemInformation.HorizontalScrollBarHeight;
	
					//	Reduce the width of the horizontal scrollbar if we are also
					//	displaying a vertical scrollbar
					if ( needsVerticalScrollbar )
						hScrollRect.Width -= SystemInformation.VerticalScrollBarWidth;

					//	Set the Rect, and add the element
					hScrollbarElement.Rect = hScrollRect;
					this.ChildElements.Add(hScrollbarElement);

					//	Reduce the height of the remaining area by the
					//	height of the horizontal scrollbar
					rectWork.Height -= hScrollRect.Height;
				}
				#endregion Horizontal scrollbar

				#region Vertical scrollbar
				if ( needsVerticalScrollbar )
				{
					this.ResetVerticalScrollBarMetricsAndValue( cardAreaHeight, requiredHeight );

                    // MRS NAS v8.2 - CardView Printing
                    // Enclosed this section in an 'if' block so we do not add the scrollbar
                    // element when printing. 
                    if (this.IsPrinting == false)
                    {
                        // Try to reuse an element from the old elements list.
                        ScrollBarUIElement vScrollbarElement = CardAreaUIElement.ExtractExistingElement(oldElements, typeof(ScrollBarUIElement), true) as ScrollBarUIElement;
                        if (vScrollbarElement == null)
                            vScrollbarElement = new ScrollBarUIElement(this, this.VScrollBarInfo);

                        //	Hold a reference to the element
                        this.vScrollbarElement = vScrollbarElement;

                        //	Calculate the position and size of the vertical scrollbar
                        Rectangle vScrollRect = rectInsideBorders;
                        vScrollRect.X = rectInsideBorders.Right - SystemInformation.VerticalScrollBarWidth;
                        vScrollRect.Width = SystemInformation.VerticalScrollBarWidth;
                        vScrollRect.Height -= cardAreaHorizontalScrollbarHeightResolved;

                        //	Set the Rect, and add the element
                        vScrollbarElement.Rect = vScrollRect;
                        this.ChildElements.Add(vScrollbarElement);

                        //	Reduce the height of the remaining area by the
                        //	height of the horizontal scrollbar
                        rectWork.Width -= vScrollRect.Width;
                    }
				}
				else
				{
					if ( this.vScrollBarInfo != null )
					{
						//	Unhook from events, dispose and nullify
						this.vScrollBarInfo.Scroll		-= this.ScrollHandler;
						this.vScrollBarInfo.ValueChanged -= this.ScrollValueChangedHandler;
						this.vScrollBarInfo.Dispose();
						this.vScrollBarInfo = null;
					}
				}
				#endregion Vertical scrollbar

			}
	
			#region CardAreaScrollRegionUIElement
			CardAreaScrollRegionUIElement scrollRegionElement = CardAreaUIElement.ExtractExistingElement(oldElements, typeof(CardAreaScrollRegionUIElement), true) as CardAreaScrollRegionUIElement;
			if (scrollRegionElement == null)
				scrollRegionElement = new CardAreaScrollRegionUIElement(this);

			scrollRegionElement.Rect = rectWork;

			this.ChildElements.Add(scrollRegionElement);
			#endregion CardAreaScrollRegionUIElement

			// SSP 11/18/05 BR07794
			// We need to recalculate the formula cells in newly scrolled into view card rows.
			// 
			this.NotifyCalcManagerHelper( true );
		}

		#region Obsolete code
		//
		//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling
		//	Relocated the contents of this method to the CardAreaScrollRegionUIElement class.
		//
		//	/// <summary>
		//	/// Makes sure that the child elements for this element are in the
		//	/// ChildElements array and that they are positioned properly.
		//	/// This needs to be overridden if the element has any child elements.
		//	/// </summary>
		//	protected override void PositionChildElements()
		//	{
		//		Rectangle	rectWork, rectCards;
		//
		//		// Save the current child elements collection so we can
		//		// potentially reuse some of the elements.
		//		UIElementsCollection oldElements = this.ChildElements;
		//
		//		// Clear the current child elements collection.
		//		this.childElementsCollection = null;
		//
		//		// Initialize the cards rect to the entire rect.
		//		rectCards = this.RectInsideBorders;
		//
		//		// SSP 3/19/03 - Row Layout Functionality
		//		// Don't show the scrollbars in the row layout designer.
		//		// Added the if condition to the block of code.
		//		//
		//		rectWork = Rectangle.Empty;
		//
		//		if ( ! this.RowLayoutDesignerElement )
		//		{
		//			// Setup the scrollbar.
		//			this.ResetScrollBarMetricsAndValue();
		//
		//			// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
		//			// Don't add a scroll-bar ui element if scrollbar isn't supposed to be visible.
		//			// Enclosed the exisiting code in the if block.
		//			//
		//			if ( null != this.Rows && this.Rows.IsCardAreaHorizontalScrollbarVisible )
		//			{
		//				// Try to reuse an element from the old elements list.
		//				ScrollBarUIElement scrollBarElement = (ScrollBarUIElement)CardAreaUIElement.ExtractExistingElement(oldElements, typeof(ScrollBarUIElement), true);
		//				if (scrollBarElement == null)
		//					scrollBarElement = new ScrollBarUIElement(this, this.HScrollBarInfo);
		//
		//				rectWork		= this.RectInsideBorders;
		//				rectWork.Y		= rectWork.Bottom - SystemInformation.HorizontalScrollBarHeight;
		//				rectWork.Height	= SystemInformation.HorizontalScrollBarHeight;
		//
		//				scrollBarElement.Rect = rectWork;
		//				this.ChildElements.Add(scrollBarElement);
		//			}
		//		}
		//
		//		// Adjust the rect available for the cards.
		//		rectCards.Height -= rectWork.Height;
		//
		//		// Subtract out the margins from the cards rect.
		//		rectCards.Inflate((-1 * this.Band.CardAreaMarginsResolved), (-1 * this.Band.CardAreaMarginsResolved));
		//
		//		// ---------------------------------------------------------------------
		//		// Handle the VariableHeight card style in a separate routine.
		//		// SSP 2/28/03 - Row Layout Functionality
		//		// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
		//		// to StandardLabels card style so use the StyleResolved instead of the Style property.
		//		//
		//		if ( this.Band.CardSettings.StyleResolved == CardStyle.VariableHeight ||
		//			// SSP 6/27/02
		//			// Do that for compressed card view as well.
		//			// Added below clause in the condition.
		//			//
		//			// SSP 2/28/03 - Row Layout Functionality
		//			// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
		//			// to StandardLabels card style so use the StyleResolved instead of the Style property.
		//			//
		//			CardStyle.Compressed == this.Band.CardSettings.StyleResolved
		//			)
		//		{
		//			// SSP 4/8/03 UWG2115
		//			// If all the rows are filtered out and there are no visible rows, then allow 
		//			// skip the logic for laying out compressed/variable-height rows and allow
		//			// the logic below to execute.
		//			// Enclosed the code in the if statement. If the number of visible cards is less
		//			// than 0, then fall through.
		//			//
		//			if ( this.TotalVisibleCards > 0 )
		//			{
		//				this.PositionElementsVariableHeight(rectCards, oldElements);
		//				return;
		//			}
		//		}
		//
		//		// ---------------------------------------------------------------------
		//		// Create a RowUIElement for each visible card.
		//		//
		//		// Setup some variables we'll need.
		//		int				currentCardIndex = this.FirstVisibleRowIndex;
		//		int				currentRow		 = 0;
		//		int				currentColumn	 = 0;
		//		int				i				 = 0;
		//		int				verticalOffset	 = 0;
		//		UltraGridRow	row				 = null;
		//		int				totalRows		 = this.Rows.VisibleRowCount;
		//		int				cumulativeVerticalOverlaps
		//											= 0;
		//
		//		// Keep track of the width used for each row as we process each card.
		//		int[]			widthUsed		 = new int[this.TotalRowsInCardArea];
		//		for (i = 0; i < widthUsed.Length; i++)	{ widthUsed[i] = 0; }
		//
		//		// SSP 6/7/02
		//		// CVMS Row filtering. Wrote code for situation where all the rows
		//		// are filtered out so there is no visible row. But we still want
		//		// to display the column lables so the user can unfilter.
		//		// Added if block below and put the already existing code into
		//		// the else block.
		//		//
		//		if ( this.TotalVisibleCards <= 0 )
		//		{
		//			// If there are not visible rows, then just show the column headers
		//			// without any row.
		//			//
		//
		//				// Try to reuse an element from the old elements list.
		//				CardLabelAreaUIElement cardLabelAreaElement = (CardLabelAreaUIElement)CardAreaUIElement.ExtractExistingElement(oldElements, typeof(CardLabelAreaUIElement), true);
		//				if (cardLabelAreaElement == null)
		//					cardLabelAreaElement = new CardLabelAreaUIElement(this);
		//
		//				// Initialize the element.
		//				cardLabelAreaElement.InFirstCardAreaRow = (currentRow == 0);
		//
		//				// Compute the vertical offset for this card.
		//				verticalOffset = (this.CardCaptionHeight) +
		//					(currentRow * (this.CardHeight + this.Band.CardSpacingVertical));
		//
		//				// Compute the effective border thickness.
		//				int	borderThickness = 0;
		//				if (this.Band.CardSettings.ShowCaption == true)
		//					borderThickness = this.Band.Layout.GetBorderThickness(this.Band.BorderStyleRowResolved);
		//
		//				// Compute additional adjustment.
		//				int additionalAdjustment = 0;
		//				if (this.MergedLabels && 
		//					this.Band.CardSettings.ShowCaption == false &&
		//					this.Band.CardSpacingVertical == 0 &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved)  &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved))
		//				{
		//					additionalAdjustment = 1;
		//				}
		//
		//				// Setup the element's rect.
		//				rectWork		 = rectCards;
		//				rectWork.Y		+= (verticalOffset + borderThickness + additionalAdjustment);
		//				rectWork.Height	 = (this.CardHeight - this.CardCaptionHeight - borderThickness - additionalAdjustment);
		//				rectWork.Width	 = this.LabelWidth;
		//
		//				// Make sure we haven't gone past the bottom of the card area.
		//				if (rectWork.Bottom > rectCards.Bottom)
		//					rectWork.Height = rectCards.Bottom - rectWork.Y;
		//
		//				// Overlap the left edge of the label area element with the cardarea
		//				// element if the conditions are right.
		//				bool overlappedLabelArea = false;
		//				if (this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleHeaderResolved) &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved) &&
		//					this.Band.CardSpacingHorizontal == 0)
		//				{
		//					rectWork.X--;
		//					rectWork.Width++;
		//					overlappedLabelArea = true;
		//				}
		//
		//				// Apply the cumulative vertical overlaps to the label
		//				// area's rect.
		//				rectWork.Y -= cumulativeVerticalOverlaps;
		//
		//				cardLabelAreaElement.Rect = rectWork;
		//				this.ChildElements.Add(cardLabelAreaElement);
		//
		//				// JM 01-16-02 Add cardSpacingHorizontal to the width used
		//				//widthUsed[currentRow]	 += rectWork.Width;
		//				widthUsed[currentRow]	 += rectWork.Width + this.Band.CardSpacingHorizontal;
		//
		//				// Subtract out the pixel we added to the width above in the case
		//				// where we overlapped the label area.
		//				if (overlappedLabelArea)
		//					widthUsed[currentRow]--;
		//		}
		//		else
		//		{
		//
		//			// Process each card, creating them in 'snaking' fashion
		//			//	i.e.	col1 row1
		//			//			col1 row2
		//			//			col1 row3
		//			//			col2 row1
		//			//			col2 row2	etc..
		//			for (i = 0; i < this.TotalVisibleCards; i++)
		//			{
		//				// Check to see if we've displayed all cards yet.
		//				if (currentCardIndex > (totalRows - 1))
		//					break;
		//
		//				// SSP 3/25/03 - Row Layout Functionality
		//				// In row layout designer, display a single row.
		//				//
		//				if ( 1 == i && this.RowLayoutDesignerElement )
		//					break;
		//
		//				// Get the row we're dealing with.
		//				row = this.Rows.GetRowAtVisibleIndex(currentCardIndex);
		//
		//				// Update the cumulative vertical overlap value to reflect
		//				// any vertical overlap in the current card.
		//				if ((currentRow == 0  &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved) &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//					this.Band.CardSpacingVertical == 0)
		//					||
		//					(currentRow == 0  &&
		//					this.Band.HeaderVisible &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleHeaderResolved) &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//					this.Band.CardSpacingVertical == 0))
		//				{
		//					cumulativeVerticalOverlaps++;
		//				}
		//
		//				if (currentRow != 0  &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//					this.Band.CardSpacingVertical == 0)
		//
		//				{
		//					cumulativeVerticalOverlaps++;
		//				}
		//
		//				// If the card style is MergeLabels, and we haven't created any
		//				// cards yet, create the label area.
		//				if (this.MergedLabels && widthUsed[currentRow] == 0)
		//				{
		//					// Try to reuse an element from the old elements list.
		//					CardLabelAreaUIElement cardLabelAreaElement = (CardLabelAreaUIElement)CardAreaUIElement.ExtractExistingElement(oldElements, typeof(CardLabelAreaUIElement), true);
		//					if (cardLabelAreaElement == null)
		//						cardLabelAreaElement = new CardLabelAreaUIElement(this);
		//
		//					// Initialize the element.
		//					cardLabelAreaElement.InFirstCardAreaRow = (currentRow == 0);
		//
		//					// Compute the vertical offset for this card.
		//					verticalOffset = (this.CardCaptionHeight) +
		//						(currentRow * (this.CardHeight + this.Band.CardSpacingVertical));
		//
		//					// Compute the effective border thickness.
		//					int	borderThickness = 0;
		//					if (this.Band.CardSettings.ShowCaption == true)
		//						borderThickness = this.Band.Layout.GetBorderThickness(this.Band.BorderStyleRowResolved);
		//
		//					// Compute additional adjustment.
		//					int additionalAdjustment = 0;
		//					if (this.MergedLabels && 
		//						this.Band.CardSettings.ShowCaption == false &&
		//						this.Band.CardSpacingVertical == 0 &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved)  &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved))
		//					{
		//						additionalAdjustment = 1;
		//					}
		//
		//					// Setup the element's rect.
		//					rectWork		 = rectCards;
		//					rectWork.Y		+= (verticalOffset + borderThickness + additionalAdjustment);
		//					rectWork.Height	 = (this.CardHeight - this.CardCaptionHeight - borderThickness - additionalAdjustment);
		//					rectWork.Width	 = this.LabelWidth;
		//
		//					// Make sure we haven't gone past the bottom of the card area.
		//					if (rectWork.Bottom > rectCards.Bottom)
		//						rectWork.Height = rectCards.Bottom - rectWork.Y;
		//
		//					// Overlap the left edge of the label area element with the cardarea
		//					// element if the conditions are right.
		//					bool overlappedLabelArea = false;
		//					if (this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleHeaderResolved) &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved) &&
		//						this.Band.CardSpacingHorizontal == 0)
		//					{
		//						rectWork.X--;
		//						rectWork.Width++;
		//						overlappedLabelArea = true;
		//					}
		//
		//					// Apply the cumulative vertical overlaps to the label
		//					// area's rect.
		//					rectWork.Y -= cumulativeVerticalOverlaps;
		//
		//					cardLabelAreaElement.Rect = rectWork;
		//					this.ChildElements.Add(cardLabelAreaElement);
		//
		//					// JM 01-16-02 Add cardSpacingHorizontal to the width used
		//					//widthUsed[currentRow]	 += rectWork.Width;
		//					widthUsed[currentRow]	 += rectWork.Width + this.Band.CardSpacingHorizontal;
		//
		//					// Subtract out the pixel we added to the width above in the case
		//					// where we overlapped the label area.
		//					if (overlappedLabelArea)
		//						widthUsed[currentRow]--;
		//				}
		//
		//				// Create the RowUIElement for the card.  This is the element that
		//				// actually contains the card contents (caption, cells and possibly labels).
		//				//
		//				// Try to reuse an element from the old elements list.
		//				RowUIElement rowElement = (RowUIElement)CardAreaUIElement.ExtractExistingElement(oldElements, typeof(RowUIElement), true);
		//				if (rowElement == null)
		//					rowElement = new RowUIElement(this);
		//
		//				// Initialize the element.
		//				rowElement.InitializeRow(row);
		//				rowElement.InFirstCardAreaCol	= (currentColumn == 0);
		//				rowElement.InFirstCardAreaRow	= (currentRow == 0);
		//				rowElement.InLastCardAreaCol	= (currentColumn == (this.TotalColsInCardArea - 1));
		//
		//				// Compute the vertical offset for this card.
		//				verticalOffset = (currentRow * (this.CardHeight + this.Band.CardSpacingVertical));
		//
		//				// Setup the rect.
		//				rectWork		 = rectCards;
		//				rectWork.X		+= widthUsed[currentRow];
		//				rectWork.Width	 = this.CardWidth;
		//				rectWork.Y		+= verticalOffset;
		//				rectWork.Height	 = this.CardHeight;
		//
		//				// Make sure we haven't gone past the bottom of the card area.
		//				if (rectWork.Bottom > rectCards.Bottom)
		//					rectWork.Height = rectCards.Bottom - rectWork.Y;
		//
		//				// Overlap the left edge of the leftmost card if the conditions are right.
		//				bool overlappedCardLeftEdge = false;
		//				if ( (currentColumn == 0  &&
		//					// SSP 2/28/03 - Row Layout Functionality
		//					// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
		//					// to StandardLabels card style so use the StyleResolved instead of the Style property.
		//					//
		//					this.Band.CardSettings.StyleResolved == CardStyle.MergedLabels  &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleHeaderResolved) &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//					this.Band.CardSpacingHorizontal == 0)
		//					||
		//					(currentColumn == 0  &&
		//					// SSP 2/28/03 - Row Layout Functionality
		//					// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
		//					// to StandardLabels card style so use the StyleResolved instead of the Style property.
		//					//
		//					this.Band.CardSettings.StyleResolved != CardStyle.MergedLabels  &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved) &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//					this.Band.CardSpacingHorizontal == 0)
		//					||
		//					(currentColumn != 0  &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//					this.Band.CardSpacingHorizontal == 0) )
		//				{
		//					rectWork.X--;
		//					rectWork.Width++;
		//					overlappedCardLeftEdge = true;
		//				}
		//
		//				// Overlap the right edge of the rightmost card with the card area if the 
		//				// conditions are right.
		//				if (currentColumn == (this.TotalColsInCardArea - 1)  &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved) &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//					this.Band.CardSpacingHorizontal == 0)
		//
		//				{
		//					rectWork.Width++;
		//				}
		//
		//				// Apply the cumulative vertical overlaps to the card's
		//				// rect.
		//				rectWork.Y		-= cumulativeVerticalOverlaps;
		//
		//				rowElement.Rect	 = rectWork;
		//				this.ChildElements.Add(rowElement);
		//
		//				// Update some variables.
		//				currentCardIndex++;
		//				widthUsed[currentRow]	+= (rectWork.Width + this.Band.CardSpacingHorizontal);
		//
		//				// Subtract out the pixel we added to the width above in the case
		//				// where we overlapped the card's left edge since we really didn't 
		//				// use an extra pixel of width (because we also adjusted the X value).
		//				if (overlappedCardLeftEdge)
		//					widthUsed[currentRow]--;
		//
		//				// Bump the row number.  (Reset the row number to zero if we've exceeded the
		//				// total rows in the card area.  This will force a 'snake' to the top of the
		//				// next column)
		//				currentRow++;
		//				if (currentRow > (this.TotalRowsInCardArea - 1))
		//				{
		//					currentRow = 0;
		//					currentColumn++;
		//					cumulativeVerticalOverlaps	= 0;
		//				}
		//			}
		//		}
		//	}
		#endregion Obsolete code

			#endregion PositionChildElements

			#region GetContext
			// MRS 4/5/05 - Override GetContext to give a context of the band
            /// <summary>
            /// Returns an object of requested type that relates to the element or null.
            /// </summary>
            /// <param name="type">The requested type or null to pick up default context object.</param>
            /// <param name="checkParentElementContexts">If true will walk up the parent chain looking for the context.</param>
            /// <returns>Returns null or an object of requested type that relates to the element.</returns>
            /// <remarks> Classes that override this method normally need to override the <see cref="Infragistics.Win.UIElement.ContinueDescendantSearch(System.Type,System.Object[])"/> method as well.</remarks>
			public override object GetContext(System.Type type, bool checkParentElementContexts)
			{
				// if the context is of the passed in type return it
				//
				if ( null != type  )
				{
					if ( null != this.PrimaryContext )
					{
						if ( typeof( RowsCollection ) == type )
						{
							return this.PrimaryContext;
						}
					}

					if ( null != this.Band )
					{
						if ( typeof( UltraGridBand ) == type )
						{
							return this.Band;
						}
					}
				}

				return base.GetContext( type, checkParentElementContexts );
			}
			#endregion GetContext

			#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Band, StyleUtils.Role.CardArea );
			}
		}

			#endregion // UIRole

		#endregion Base Class Overrides

		#region Public properties

			#region Rows
		/// <summary>
		/// Returns the rows collection that conatins all sibling rows
		/// even rows that are not visible (read-only).
		/// </summary>
		public RowsCollection Rows
		{
			get	{ return this.PrimaryContext as RowsCollection; }
		}
			#endregion Rows

		#endregion Public Properties

		#region Internal Properties

			#region RowLayoutDesignerElement

		// SSP 3/19/03 - Row Layout Functionality
		// Added a property to indicate whether we are in row layout designer mode.
		//

		internal bool RowLayoutDesignerElement
		{
			get
			{
				DataAreaUIElement dataAreaElem = (DataAreaUIElement)this.GetAncestor( typeof( DataAreaUIElement ) );

				return null != dataAreaElem && dataAreaElem.RowLayoutDesignerElement;
			}
		}

			#endregion	RowLayoutDesignerElement

			#region AutoFit

		internal bool AutoFit
		{
			get { return this.Band.CardSettings.AutoFit; }
		}

			#endregion AutoFit

			#region Band

		internal UltraWinGrid.UltraGridBand Band
		{
			get
			{
				// JM UWG977 01-23-02 Return the cached band reference.
				//return this.FirstVisibleRow.Band;
				return this.band;
			}
		}

			#endregion Band

			#region CardCaptionHeight

		internal int CardCaptionHeight
		{
			get { return this.Band.CardCaptionHeight; }
		}

			#endregion CardCaptionHeight

			#region CardHeight

		internal int CardHeight
		{
			get { return this.Band.CardHeight; }
		}

			#endregion CardHeight

			#region CardSpacing

		internal int CardSpacing
		{
			get { return this.Band.CardSpacingResolved; }
		}

			#endregion CardSpacing

			#region CardWidth

		internal int CardWidth
		{
			get
			{
				if (!this.AutoFit)
					return this.Band.CardWidthResolved; 
				
				// AutoFit is active so compute the size of each card based on the
				// total area available for all cards minus the intercard spacing.
				int widthAvailableForCardsAdjusted	 = this.WidthAvailableForCards;
				widthAvailableForCardsAdjusted		-= (this.TotalColsInCardArea - 1) * this.Band.CardSpacingHorizontal;

				return widthAvailableForCardsAdjusted / this.TotalColsInCardArea;
			}
		}

			#endregion CardWidth

			#region FirstVisibleRow

		internal UltraWinGrid.UltraGridRow FirstVisibleRow
		{
			// JM 01-22-02 - Deal with the situation where our cached row
			//				 may be invalid.
			//get	{ return this.firstVisibleRow; }
			get 
			{
				// If the visible row we have cached is valid return it.
				if (this.firstVisibleRow != null &&
					this.firstVisibleRow.VisibleIndex > -1)
					return this.firstVisibleRow;
				else
				{
					// Otherwise check the FirstVisibleRowIndex property. The 
					// property Get for this property will attempt to re-establish
					// a new first visible row.  If it cannot, we have no choice but
					// to return null.
					if (this.FirstVisibleRowIndex < 0)
						return null;
					else
						return this.firstVisibleRow;
				}
			}
		}
	
			#endregion FirstVisibleRow

			#region FirstVisibleRowIndex

		internal int FirstVisibleRowIndex
		{
			get
			{ 
				// If the visible row we have cached has a valid visible row index,
				// return it.
				if (this.firstVisibleRow != null &&
					this.firstVisibleRow.VisibleIndex > -1)
					return this.firstVisibleRow.VisibleIndex;

				UltraGridRow newRow = null;

				// Need to establish another row as the first visible row.  If our old
				// first visible row has a bad index (i.e., it's been removed from the
				// collection) then use the first visible row in the rows collection.
				// Otherwise, try to find an adjacent visible row first, and if not found
				// use the first visible in the rows collection.
				//
				// JM 01-22-02 Check for null
				if (this.firstVisibleRow == null)
					newRow = this.Rows.GetFirstVisibleRow();
				else if (this.firstVisibleRow.Index < 0)
					newRow = this.Rows.GetFirstVisibleRow();
				else
				{
					// SSP 7/15/03 UWG2240
					// If all the visible rows can fit in the card area, then make the first visible
					// row the first row.
					// Added if block and enclosed the existing code into the else block.
					//
					if ( this.CanFitAllVisibleRows( ) )
					{
						newRow = this.Rows.GetFirstVisibleRow( );
					}
					else
					{
						newRow = this.Rows.GetRowAtVisibleIndexOffset(this.firstVisibleRow, 1);
						if (newRow == null)
						{
							newRow = this.Rows.GetRowAtVisibleIndexOffset(this.firstVisibleRow, -1);
							if (newRow == null)
								newRow = this.Rows.GetFirstVisibleRow();
						}
					}
				}

				if (newRow == null)
					return -1;
				else
				{
					this.firstVisibleRow		= newRow;
					this.Rows.FirstVisibleCard	= newRow;

					return newRow.VisibleIndex;
				}
			}
		}
	
			#endregion FirstVisibleRowIndex

			#region FixedHeight

		internal bool FixedHeight
		{
			get 
			{ 
				// SSP 2/28/03 - Row Layout Functionality
				// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
				// to StandardLabels card style so use the StyleResolved instead of the Style property.
				//
				return this.Band.CardSettings.StyleResolved != CardStyle.VariableHeight 
					// SSP 6/27/02
					// Compressed card view.
					// It's not fixed height when the card view style is compressed 
					// as well.
					// Added below clause to the condition.
					//
					// SSP 2/28/03 - Row Layout Functionality
					// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
					// to StandardLabels card style so use the StyleResolved instead of the Style property.
					//
					&& CardStyle.Compressed != this.Band.CardSettings.StyleResolved;
			}
		}

			#endregion FixedHeight

			#region Grid

		internal UltraGrid Grid
		{
			get	{return (UltraGrid)(this.Control);}
		}

			#endregion Grid

			#region HeightAvailableForCards

		internal int HeightAvailableForCards
		{
			get
			{
				return this.Band.GetHeightAvailableForCards(this.Rect.Height);
			}
		}

			#endregion HeightAvailableForCards

			#region LabelWidth

		internal int LabelWidth
		{
			get { return this.Band.CardLabelWidthResolved; }
		}

			#endregion LabelWidth

			#region MergedLabels

		internal bool MergedLabels
		{
			// SSP 2/28/03 - Row Layout Functionality
			// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
			// to StandardLabels card style so use the StyleResolved instead of the Style property.
			//
			get { return this.Band.CardSettings.StyleResolved == CardStyle.MergedLabels; }
		}

			#endregion MergedLabels

			#region OrigParentSize

		// SSP 5/16/05 - Optimization
		// Row col region intersection element had an optimization for not causing the existing
		// row elements to reposition their descendants. It never worked so made it work.
		//
				

			#endregion origParentSize

			// JJD 1/08/04 - Accessibility
			//
			#region ScrollbarAccessibilityObject

		internal System.Windows.Forms.AccessibleObject ScrollbarAccessibilityObject
		{
			get 
			{
				if ( this.scrollbarAccessibilityObject == null )
				{
					if ( this.Rows.ParentRow == null )
						this.scrollbarAccessibilityObject = this.HScrollBarInfo.CreateAccessibilityInstance( this.band.Layout.Grid.AccessibilityObject );
					else
						this.scrollbarAccessibilityObject = this.HScrollBarInfo.CreateAccessibilityInstance( this.Rows.ParentRow.AccessibilityObject );
				}

				return this.scrollbarAccessibilityObject;
			}
		}

			#endregion ScrollbarAccessibilityObject

			#region HScrollBarInfo / VScrollBarInfo

		//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling
		//	Changed the ScrollBarInfo property to HScrollBarInfo, and added
		//	a VScrollBarInfo property.

		internal ScrollBarInfo HScrollBarInfo
		{
			get
			{
				if (this.hScrollBarInfo == null)
				{
					this.hScrollBarInfo = new ScrollBarInfo(this.Grid, Orientation.Horizontal, this.Grid.DisplayLayout.Appearances);

					//	Hook into the scrollbar's notifications
					this.hScrollBarInfo.Scroll		+= this.ScrollHandler;
					this.hScrollBarInfo.ValueChanged += this.ScrollValueChangedHandler;

					// SSP 6/11/02
					// Added PriorityScrolling property to grid.
					//
					this.hScrollBarInfo.PriorityScrolling = this.Grid.DisplayLayout.PriorityScrolling;
				}

				return this.hScrollBarInfo;
			}
		}

		#region MaintainScrollPositionHelper

		// SSP 4/21/05 BR03382
		// Added MaintainScrollPositionHelper. Maintain the position of the horizontal 
		// scrollbar whenever the rows in card area are sorted.
		//
		private int scrollBarPositionToMaintain = -1;

		internal void MaintainScrollPositionHelper( )
		{
			if ( null != this.hScrollBarInfo && ! this.hScrollBarInfo.Disposed )
				this.scrollBarPositionToMaintain = this.hScrollBarInfo.Value;
		}

		#endregion // MaintainScrollPositionHelper

		internal ScrollBarInfo VScrollBarInfo
		{
			get
			{
				if (this.vScrollBarInfo == null)
				{
					this.vScrollBarInfo = new ScrollBarInfo(this.Grid, Orientation.Vertical, this.Grid.DisplayLayout.Appearances);

					//	Hook into the scrollbar's notifications
					this.vScrollBarInfo.Scroll		+= this.ScrollHandler;
					this.vScrollBarInfo.ValueChanged += this.ScrollValueChangedHandler;
				}

				return this.vScrollBarInfo;
			}
		}

			#endregion HScrollBarInfo / VScrollBarInfo

			#region TotalColsInCardArea

		//	BF 11.8.04	NAS2005 Vol1 - CardView vertical scrolling
		//
		//	Reimplemented; this now requires that we know whether there
		//	is a vertical scrollbar present, and to determine that, we
		//	need to know the number of columns. To prevent circular referencing,
		//	this method does not call NeedsVerticalScrollbar to determine whether
		//	the available height is sufficient to display the tallest card, because
		//	NeedsVerticalScrollbar uses this property to make that determination.

		#region Obsolete code
		//#if DEBUG
		//		/// <summary>
		//		/// Returns the number of display columns in the card area.
		//		/// </summary>
		//#endif
		//		internal int TotalColsInCardArea
		//		{
		//			get
		//			{
		//				if (this.FixedHeight)
		//				{
		//					// Calculate how many columns we would need to show all the
		//					// visible rows.  
		//					int maxColumnsNeeded = this.Rows.VisibleRowCount / this.TotalRowsInCardArea;
		//					if ((this.Rows.VisibleRowCount % this.TotalRowsInCardArea) != 0)
		//						maxColumnsNeeded++;
		//
		//					// Calculate how many columns are available in the card area
		//					// given the current width.
		//					//
		//					int columnsAvailable = this.Band.GetTotalColsInCardArea(this.Rect.Width);
		//
		//					// Return the min of the above two values.
		//					return Math.Min(columnsAvailable, maxColumnsNeeded); 
		//				}
		//				else
		//				{
		//					return this.GetTotalColumnsNeededWithAdjustment(this.FirstVisibleRow,
		//						this.Band.GetTotalColsInCardArea(this.Rect.Width));
		//				}
		//			}
		//		}
		//
		#endregion Obsolete code


		internal int TotalColsInCardArea
		{
			get
			{
				Rectangle rectInsideBorders = this.RectInsideBorders;

				//	If there is more than one rows of cards being displayed, there
				//	will not be a vertical scrollbar.
				if ( this.TotalRowsInCardArea > 1 )
					return this.GetTotalColsInCardArea( rectInsideBorders.Width );
				else
				{
					int heightRequired = 0;
					int cardAreaHeight = rectInsideBorders.Height - this.Band.CardAreaHorizontalScrollbarHeightResolved;
		
					//	Get the total number of columns with and without the vertical scrollbar
					int totalColsWithoutVScrollbar = this.GetTotalColsInCardArea( rectInsideBorders.Width );
					int totalColsWithVScrollbar = this.GetTotalColsInCardArea( rectInsideBorders.Width - SystemInformation.VerticalScrollBarWidth );

					//	If the total was the same with or without the vertical scrollbar,
					//	the scrollbar is not a factor, so return the value. Note that this
					//	rule applies to all styles.
					if ( totalColsWithVScrollbar == totalColsWithoutVScrollbar )
						return totalColsWithVScrollbar;
					else
					{
						//	For the VariableHeight and Compressed styles, we iterate the
						//	visible rows (cards) and use the height of the tallest card...
						if ( this.VariableHeight || this.Compressed )
						{
							//	Get the total number of cards without the vertical scrollbar,
							//	so that we are sure to iterate through all the visible cards
							//	that could potentially be displayed.
							int totalCards = this.GetTotalVisibleCards( totalColsWithoutVScrollbar );

							//	Iterate the potentially visible cards and get the height
							//	of the tallest one.
							int	firstVisibleRowIndex = this.FirstVisibleRowIndex;							
							for ( int i = firstVisibleRowIndex; i < (firstVisibleRowIndex + totalCards); i ++ )
							{
								UltraGridRow row = this.Rows.GetRowAtVisibleIndex( i );
								heightRequired = Math.Max( heightRequired, row.CardHeight );
							}
						}
						else
							heightRequired = band.CardHeight;

						//	Add in the margins (actually vertical spacing)
						heightRequired += band.CardAreaMarginsResolved;

						//	If the height of the tallest card is greater than the
						//	available height, return the value that assumes a vertical
						//	scrollbar is present; otherwise, return the value that assumes
						//	there is no vertical scrollbar.
						if ( heightRequired > cardAreaHeight )
							return totalColsWithVScrollbar;
						else
							return totalColsWithoutVScrollbar;

					}
				}

			}
		}

		private int GetTotalColsInCardArea( int cardAreaWidth )
		{
			if (this.FixedHeight)
			{
				// Calculate how many columns we would need to show all the
				// visible rows.  
				int maxColumnsNeeded = this.Rows.VisibleRowCount / this.TotalRowsInCardArea;
				if ((this.Rows.VisibleRowCount % this.TotalRowsInCardArea) != 0)
					maxColumnsNeeded++;

				// Calculate how many columns are available in the card area
				// given the current width.
				//
				int columnsAvailable = this.Band.GetTotalColsInCardArea(cardAreaWidth);

				// Return the min of the above two values.
				return Math.Min(columnsAvailable, maxColumnsNeeded); 
			}
			else
			{
				return this.GetTotalColumnsNeededWithAdjustment(this.FirstVisibleRow,
					this.Band.GetTotalColsInCardArea(cardAreaWidth));
			}
		}

			#endregion TotalColsInCardArea

			#region TotalRowsInCardArea

		internal int TotalRowsInCardArea
		{
			get
			{
				// JM UWG1019
				//return this.Band.GetTotalRowsInCardArea(this.Rect.Height); 
				return this.Rows.GetTotalActualRowsInCardArea(this.Rect.Height); 
			}
		}

			#endregion TotalRowsInCardArea

			#region TotalVisibleCards

		internal int TotalVisibleCards
		{
			get
			{
				//	BF 11.8.04	NAS2005 Vol1 - CardView vertical scrolling	BF 11.8.04
				//	Externalized this to prevent circular referencing

				#region Obsolete code
				//	if (this.FixedHeight)
				//	{
				//		return Math.Min(this.Rows.VisibleRowCount - this.FirstVisibleRowIndex,
				//						this.TotalRowsInCardArea * this.TotalColsInCardArea); 
				//	}
				//	else
				//	{
				//		return this.NumberOfVariableHeightCardsDisplayableInColumns(this.TotalColsInCardArea);
				//	}
				#endregion Obsolete code

				return this.GetTotalVisibleCards( this.TotalColsInCardArea );
			}
		}

		//	BF 11.8.04	NAS2005 Vol1 - CardView vertical scrolling
		private int GetTotalVisibleCards( int totalColsInCardArea )
		{
			if (this.FixedHeight)
			{
				return Math.Min(this.Rows.VisibleRowCount - this.FirstVisibleRowIndex,
								this.TotalRowsInCardArea * totalColsInCardArea); 
			}
			else
			{
				return this.NumberOfVariableHeightCardsDisplayableInColumns(totalColsInCardArea);
			}
		}

			#endregion TotalVisibleCards

			#region VariableHeight

		internal bool VariableHeight
		{
			get 
			{
				// SSP 2/28/03 - Row Layout Functionality
				// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
				// to StandardLabels card style so use the StyleResolved instead of the Style property.
				//
				return this.Band.CardSettings.StyleResolved == CardStyle.VariableHeight;					
			}
		}

			#endregion VariableHeight

			#region Compressed

		//	BF 11.3.04	NAS2005 Vol1 - CardView vertical scrolling
		//	Added this helper property
		internal bool Compressed
		{
			get{ return this.Band.CardSettings.StyleResolved == CardStyle.Compressed; }
		}

			#endregion Compressed
	
			#region WidthAvailableForCards

		internal int WidthAvailableForCards
		{
			get
			{
				//	BF 11.9.04	NAS2005 Vol1 - CardView vertical scrolling
				//
				//	We need to account for the vertical scrollbar if there
				//	is one being displayed.
				//
				//return this.Band.GetWidthAvailableForCards(this.Rect.Width);
				Rectangle rectInsideBorders = this.RectInsideBorders;
				int cardAreaHeight = rectInsideBorders.Height - this.Band.CardAreaHorizontalScrollbarHeightResolved;
				int cardAreaWidth = rectInsideBorders.Width;

				if ( this.NeedsVerticalScrollbar(cardAreaHeight) )
					cardAreaWidth -= SystemInformation.VerticalScrollBarWidth;
				
				return this.Band.GetWidthAvailableForCards( cardAreaWidth );
			}
		}

			#endregion WidthAvailableForCards

			#region CardAreaScrollRegionVerticalOffset
		//
		//	BF 11.3.04	NAS2005 Vol1 - CardView vertical scrolling
		//	
		internal int CardAreaScrollRegionVerticalOffset
		{
			get
			{
				if ( this.vScrollBarInfo == null )
					return 0;

				return this.VScrollBarInfo.Value;
			}

			set
			{
				if ( value >= this.VScrollBarInfo.Minimum &&
					 value <= this.VScrollBarInfo.Maximum )
				{
					this.VScrollBarInfo.Value = value;

					if ( this.Parent != null )
						this.Parent.DirtyChildElements( true );
				}
				else
					Debug.Assert( false,  "Caller attempted to set CardAreaUIElement.CardAreaScrollRegionVerticalOffset property to a value that is outside the vertical scroll bounds." );
			}
		}
			#endregion CardAreaScrollRegionVerticalOffset

		//	BF 1.26.05	NAS2005 Vol1 - CardView vertical scrolling
			#region HasVerticalScrollbar
		internal bool HasVerticalScrollbar
		{
			get{ return this.hasVerticalScrollbar; }
		}
			#endregion HasVerticalScrollbar

            // MRS NAS v8.2 - CardView Printing
            #region IsPrinting
            private bool IsPrinting
            {
                get
                {
                    UltraGridBand band = this.Band;
                    if (band == null)
                        return false;

                    UltraGridLayout layout = band.Layout;
                    if (layout == null)
                        return false;

                    return layout.IsPrintLayout;
                }
            }
            #endregion //IsPrinting

        #endregion Internal Properties

        #region Internal Methods

        #region CalculateRequiredCardAreaWidth

		internal int CalculateRequiredCardAreaWidth(int availableWidth)
		{
			// If AutoFit is active, return the full available width.
			if (this.AutoFit)
				return availableWidth; 

			// Calculate the maximum number of cards per row we can fit horizontally.  Assume
			// integral width and honor the MaxCardAreaCols setting.
			int cardColumnsAvailable = (availableWidth - this.Band.CardMergedLabelAreaWidth - (2 * this.Band.CardAreaMarginsResolved))
													/ this.Band.CardWidthResolved;
			if (this.Band.CardSettings.MaxCardAreaCols > 0)
				cardColumnsAvailable = Math.Min(this.Band.CardSettings.MaxCardAreaCols,
									 			cardColumnsAvailable);

			// Calculate how many columns we actually need out of the cardColumnsAvailable. Call the
			// version of GetTotalColumnsNeeded that adjusts the firstVisibleRow backward to use
			// up as many of the cardColumnsAvailable as possible.
			int cardColumnsNeeded = this.GetTotalColumnsNeededWithAdjustment(this.FirstVisibleRow, cardColumnsAvailable);

			// Just in case.
			cardColumnsNeeded = Math.Max(cardColumnsNeeded, 1);

			// Calculate the width needed for the given number of columns.
			int widthNeeded = this.Band.CalculateRequiredCardAreaWidth(cardColumnsNeeded);

			// If the width needed is > the available width 
			// decrement the # of columns and recalculate the
			// width needed until it is not > the available width
			// or the columns = 1.
			while (widthNeeded > availableWidth && cardColumnsNeeded > 1)
			{
				cardColumnsNeeded--;
				widthNeeded = this.Band.CalculateRequiredCardAreaWidth(cardColumnsNeeded);
			}

			//	BF 11.9.04	NAS2005 Vol1 - CardView vertical scrolling
			//	We must now account for the vertical scrollbar, if one is being displayed
			int cardAreaHeight = this.RectInsideBorders.Height - this.Band.CardAreaHorizontalScrollbarHeightResolved;
			if ( this.NeedsVerticalScrollbar(cardAreaHeight) )
				widthNeeded += SystemInformation.VerticalScrollBarWidth;

			return widthNeeded;
		}

			#endregion CalculateRequiredCardAreaWidth

			#region DoesDragNeedScroll
		
		// SSP 10/28/05 BR06561
		// Implemented scrolling vertically in card-view when dragging and dropping columns.
		// Commented out the original code and added new code.
		// 
				#region Original Commented Out Code

		

				#endregion // Original Commented Out Code

				// SSP 10/28/05 BR06561
				// 
				#region BR06561 fix
		
		internal bool DoesDragNeedScrollHorizontal( Point mouseLoc, ref int timerInterval )
		{
			bool needsToScrollHorizontally, needsToScrollVertically;
			this.DoesDragNeedScroll( mouseLoc, ref timerInterval, out needsToScrollHorizontally, out needsToScrollVertically );
			return needsToScrollHorizontally;
		}
		
		internal bool DoesDragNeedScrollVertical( Point mouseLoc, ref int timerInterval )
		{
			bool needsToScrollHorizontally, needsToScrollVertically;
			this.DoesDragNeedScroll( mouseLoc, ref timerInterval, out needsToScrollHorizontally, out needsToScrollVertically );
			return needsToScrollVertically;
		}

		// SSP 10/28/05 BR06561
		// Implemented scrolling vertically in card-view when dragging and dropping columns.
		// 
		internal void ScrollCardVertically( Point mouseLoc )
		{
			if ( ! this.HasVerticalScrollbar || null == this.VScrollBarInfo )
				return;

			Rectangle cardArea = this.RectInsideBorders;

			ScrollBarInfo scrollBarInfo = this.VScrollBarInfo;
			int min = scrollBarInfo.Minimum;
			int max = scrollBarInfo.Maximum;
			if ( scrollBarInfo.LargeChange > 0 )
				max -= scrollBarInfo.LargeChange - 1;

			if ( min >= max )
				return;

			int delta = 0;
			if ( mouseLoc.Y < cardArea.Y )
				delta = -10;
			else if ( mouseLoc.Y > cardArea.Bottom )
				delta = 10;

			int newOffset = Math.Min( max, Math.Max( min, this.CardAreaScrollRegionVerticalOffset + delta  ) );
			this.CardAreaScrollRegionVerticalOffset = newOffset;
		}

		private void DoesDragNeedScroll( Point mouseLoc, ref int timerInterval, 
			out bool needsToScrollHorizontally, out bool needsToScrollVertically )
		{
			needsToScrollHorizontally = false;
			needsToScrollVertically = false;

			Rectangle cardArea = this.RectInsideBorders;

			UltraGridBand band = this.Band;
			UltraGridBase grid = band.Layout.Grid;

			// SSP 6/22/05 - NAS 5.3 Column Chooser
			// Don't scroll the grid if the mouse is over the column chooser.
			// 
			if ( null != grid && ! grid.ShouldScrollGridOnDragMoveHelper( mouseLoc ) )
				return;

			int mergedLabelsWidth = this.Band.CardMergedLabelAreaWidth;

			// subract the merged label area
			//
			if ( mergedLabelsWidth  > 0)
			{
				cardArea.Width	-= mergedLabelsWidth;
				cardArea.X		+= mergedLabelsWidth;
			}

			// SSP 10/28/05 BR06561
			// Implemented scrolling vertically in card-view when dragging and dropping columns.
			// 
			

			// if the point is in the rect then return false
			//
			if ( cardArea.Contains( mouseLoc ) )
				return;

			int distanceOutsideRgnHorizontal = 0;
			int distanceOutsideRgnVertical = 0;

			// calculate the distance from the card area's borders to where
			// the cursor is
			//
			if ( mouseLoc.X < cardArea.Left )
				distanceOutsideRgnHorizontal = cardArea.Left - mouseLoc.X;
			else if ( cardArea.Right < mouseLoc.X )
				distanceOutsideRgnHorizontal = mouseLoc.X - cardArea.Right;

			// SSP 10/28/05 BR06561
			// Implemented scrolling vertically in card-view when dragging and dropping columns.
			// 
            if ( mouseLoc.Y < cardArea.Top )
				distanceOutsideRgnVertical = cardArea.Top - mouseLoc.Y;
			else if ( cardArea.Bottom < mouseLoc.Y )
				distanceOutsideRgnVertical = mouseLoc.Y - cardArea.Bottom;

			int distanceOutsideRgn = Math.Max( distanceOutsideRgnHorizontal, distanceOutsideRgnVertical );

			// return faster timer intervals for longer distances
			//
			if ( distanceOutsideRgn > 100 )
				timerInterval =    40;
			else
				if ( distanceOutsideRgn > 80 )
				timerInterval =    65;
			else
				if ( distanceOutsideRgn > 60 )
				timerInterval =    80;
			else
				if ( distanceOutsideRgn > 50 )
				timerInterval =    100;
			else
				if ( distanceOutsideRgn > 40 )
				timerInterval =    150;
			else
				if ( distanceOutsideRgn > 20 )
				timerInterval =    200;
			else
				if ( distanceOutsideRgn > 10 )
				timerInterval =    300;
			else
				if ( distanceOutsideRgn > 6 )
				timerInterval =    400;
			else
				timerInterval =    500;

			needsToScrollHorizontally = distanceOutsideRgnHorizontal > 0;
			needsToScrollVertically = distanceOutsideRgnVertical > 0;
		}

				#endregion // BR06561 fix

			#endregion DoesDragNeedScroll

			#region GetColumnRelativeToColumn

		internal UltraGridRow GetColumnRelativeToColumn(UltraGridRow firstRowInReferenceColumn, int columnsToOffset)
		{
			// Can't continue if the target is not part of our card island.
			if (firstRowInReferenceColumn.ParentCollection != this.Rows)
				return null;

			// Just in case.
			if (columnsToOffset == 0)
				return firstRowInReferenceColumn;

			// Deal with the simple fixed height case.
			if (this.FixedHeight)
			{
				// SSP 7/15/03 UWG2240
				// If columnsToOffset * this.TotalRowsInCardArea happens to point to a row with negative visible indexes
				// then GetRowAtVisibleIndexOffset will return null. For example if the firstRowInReferenceColumn's
				// visible index is 2 and columnsToOffset * this.TotalRowsInCardArea happens to be -4, then the
				// method will return null. In the case of the card area however, we want to always return a row
				// so just return the first row.
				//
				// ----------------------------------------------------------------------------------------
				//return this.Rows.GetRowAtVisibleIndexOffset(firstRowInReferenceColumn, (columnsToOffset * this.TotalRowsInCardArea));
				int totalRowsInCardArea = this.TotalRowsInCardArea;
				UltraGridRow tmpRow = this.Rows.GetRowAtVisibleIndexOffset(
						firstRowInReferenceColumn, (columnsToOffset * totalRowsInCardArea ) );

				if ( null == tmpRow )
				{
					if ( columnsToOffset * totalRowsInCardArea < 0 )
						tmpRow = this.Rows.GetFirstVisibleRow( );
					else
						tmpRow = this.Rows.GetLastVisibleRow( );
				}
				
				return tmpRow;
				// ----------------------------------------------------------------------------------------
			}


			// For variable height we need to search columnsToOffset columns for the desired
			// column.
			bool		 searchForward	= columnsToOffset > 0;
			UltraGridRow currentCard	= null;
			if (searchForward)
			{
				currentCard = firstRowInReferenceColumn;
			}
			else
			{
				// Get the card immediately preceeding the first in the reference column.
				currentCard = this.Rows.GetRowAtVisibleIndexOffset(firstRowInReferenceColumn, -1);
			}


			// Loop thru columnsToOffset columns and return the first card in the final column.
			// Search forward or backward depending on 'searchForward'.
			int				columnsRemainingToSearch	= Math.Abs(columnsToOffset);
			UltraGridRow	firstCardInFinalColumm		= null;
			while (columnsRemainingToSearch > 0  &&
				   currentCard				!= null)
			{
				// Process the current column's cards.
				UltraGridRow	firstCardInCurrentColumn	= null;
				bool			spaceAvailable				= true;
				int				spaceRemaining				= this.HeightAvailableForCards;
				int				cardsCreatedCurrentCol		= 0;
				while (spaceAvailable	== true		&&
					   currentCard		!= null)
				{
					// Check to see if we can fit the next card.
					if (currentCard.CardHeight > spaceRemaining)
					{
						if (cardsCreatedCurrentCol < 1)
						{
							// We haven't created any cards in this column yet -
							// allow this one card to be created even though it won't
							// be totally visible.
						}
						else
						{
							// Not enough room - need to start a new column.
							spaceAvailable = false;
							break;
						}
					}

					// Update some variables.
					cardsCreatedCurrentCol++;
					spaceRemaining -= currentCard.CardHeight + this.Band.CardSpacingVertical;
					if (spaceRemaining < 1)
						spaceAvailable = false;

					// If we're searching backward, update firstCardInCurrentColumn.
					if (!searchForward)
						firstCardInCurrentColumn = currentCard;

					// Get the next card.
					currentCard = this.Rows.GetRowAtVisibleIndexOffset(currentCard, searchForward ? 1 : -1);
				}

				if (firstCardInCurrentColumn != null)
					firstCardInFinalColumm = firstCardInCurrentColumn;

				columnsRemainingToSearch--;
			}

			// If we're searching forward we want to return the current
			// card (which is really the next card that wouldn't fit)
			if (searchForward && currentCard != null)
				return currentCard;
			else
				return firstCardInFinalColumm;
		}

			#endregion GetColumnRelativeToColumn

			#region GetFirstCardInColumnContainingRow

		internal UltraGridRow GetFirstCardInColumnContainingRow(UltraGridRow targetRow)
		{
			// Can't continue if the target is not part of our card island.
			if (targetRow.ParentCollection != this.Rows)
				return null;


			// Deal with the simple fixed height case.
			if (this.FixedHeight)
			{
				int verticalPositionInCol = targetRow.VisibleIndex % this.TotalRowsInCardArea;
				
				return this.Rows.GetRowAtVisibleIndexOffset(targetRow, (-1 * verticalPositionInCol));
			}


			// For variable height we need to search for the column containing the
			// target row.
			//
			// Get the card we want to start searching at.  If we're searching backwards,
			// start with the card immediately preceeding the first visible card, otherwise
			// start with the first visible card.
			// SSP 11/5/04 UWG3784
			// Search forward if the target row happens to be the same as the first visible row.
			//
			//bool			searchForward	= targetRow.Index > this.firstVisibleRow.Index;
			bool			searchForward	= targetRow.Index >= this.firstVisibleRow.Index;

			UltraGridRow	searchStartCard = null;
			if (searchForward)
				searchStartCard = this.firstVisibleRow;
			else
			{
				// Get the card immediately preceeding the current first visible card.
				searchStartCard = this.Rows.GetRowAtVisibleIndexOffset(this.firstVisibleRow, -1);
				if (searchStartCard == null)
					return null;
			}

			if (searchStartCard == null)
				return null;


			// Loop thru each card starting at searchStartCard, until we find the
			// target row.  Search forward or backward depending on 'searchForward'.
			UltraGridRow currentCard	= searchStartCard;
			bool		 foundTargetRow = false;

			// SSP 11/5/04 UWG3784
			// Break out of the loop if currentCard gets set to null in the loop otherwise we
			// will end up in an infinite loop.
			//
			//while (foundTargetRow == false)
			while ( null != currentCard && foundTargetRow == false )
			{
				// Process the current column's cards.
				UltraGridRow	firstCardInColumn	= null;
				bool			spaceAvailable		= true;
				int				spaceRemaining		= this.HeightAvailableForCards;
				int				cardsCreatedCurrentCol = 0;

				while (					
					// SSP 11/5/04 UWG3784
					// If we've found the target row only break out if we are searching forward 
					// because we would have already gotton the first card in the column. However 
					// when searching backward we wouldn't have encountered the first card in 
					// the column yet because we are looping through cards from bottom to top. 
					// We need to process all the cards in the column even when we find the 
					// target row in order to get the first card in the column. NOTE: This 
					// particular change had nothing to do with the bug UWG3784. It's something I 
					// noticed while debugging. If you take this out you will see that the 
					// arrowing up to scroll into view a scrolled out card behaves differently in 
					// variable and fixed height modes. Commented out the following condition that 
					// breaks out when foundTargetRow is true.
					//
					//foundTargetRow	== false	&&
					   spaceAvailable	== true		&&
					   currentCard		!= null)
				{
					// Check to see if we can fit the next card.
					if (currentCard.CardHeight > spaceRemaining)
					{
						if (cardsCreatedCurrentCol < 1)
						{
							// We haven't created any cards in this column yet -
							// allow this one card to be created even though it won't
							// be totally visible.
						}
						else
						{
							// Not enough room - need to start a new column.
							spaceAvailable = false;
							break;
						}
					}

					// Update some variables.
					cardsCreatedCurrentCol++;
					spaceRemaining -= currentCard.CardHeight + this.Band.CardSpacingVertical;
					if (spaceRemaining < 1)
						spaceAvailable = false;

					// Update firstCardInColumn
					if (searchForward)
					{
						if (firstCardInColumn == null)
							firstCardInColumn = currentCard;
					}
					else
						firstCardInColumn = currentCard;

					// Check to see if the row is the target row.
					if (currentCard == targetRow)
					{
						foundTargetRow = true;

						// SSP 11/5/04 UWG3784
						// Only break out if we are searching forward because we would have already
						// gotton the first card in the column. However when searching backward we 
						// wouldn't have encountered the first card in the column yet because we are
						// looping through cards from bottom to top. We need to process all the cards
						// in the column even when we find the target row in order to get the first
						// card in the column. NOTE: This particular change had nothing to do with
						// the bug UWG3784. It's something I noticed while debugging. If you take this
						// out you will see that the arrowing up to scroll into view a scrolled out 
						// card behaves differently in variable and fixed height modes.
						//
						//break;
						if ( searchForward )
							break;
					}

					// Get the next card.
					currentCard = this.Rows.GetRowAtVisibleIndexOffset(currentCard, searchForward ? 1 : -1);
				}

				if (foundTargetRow)
					return firstCardInColumn;
			}

			return null;
		}

			#endregion GetFirstCardInColumnContainingRow

			#region GetItemNearestPoint
			internal ISelectableItem GetItemNearestPoint ( Point point, ISelectableItem lastItem )
			{
				RowUIElementBase rowElement;
				RowUIElementBase nearestRowElement = null;
				int nearestRowOffset = 0;
				int rowOffset = 0;

				// loop over the child row elements looking for the
				// one nearest the point
				//
				for ( int i = 0; i < this.ChildElements.Count; i++ )
				{
					rowElement = this.ChildElements[i] as RowUIElementBase;

					if ( rowElement == null )
						continue;

					// SSP 11/17/03 Add Row Feature
					// We don't allow selection of template rows since they are not real rows in the
					// sense that they don't have any data row associated with them. So return false
					// if any of the two rows is a template add-row.
					// 
					// ----------------------------------------------------------------------------------
					if ( null == rowElement.Row )
						continue;

					// MD 8/7/07 - 7.3 Performance
					// FxCop - Do not cast unnecessarily
					//UltraGridRow lastRow = lastItem is UltraGridCell ? ((UltraGridCell)lastItem).Row : lastItem as UltraGridRow;
					UltraGridRow lastRow;
					UltraGridCell cell = lastItem as UltraGridCell;

					if ( cell != null )
						lastRow = cell.Row;
					else
						lastRow = lastItem as UltraGridRow;

					if ( null != lastRow && lastRow != rowElement.Row )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//RowScrollRegion rsr = (RowScrollRegion)this.GetContext( typeof( RowScrollRegion ), true );
						//
						//if ( null != rsr && ! rsr.AreRowsSelectionCompatible( lastRow, rowElement.Row ) )
						if ( !RowScrollRegion.AreRowsSelectionCompatible( lastRow, rowElement.Row ) )
							continue;
					}
					// ----------------------------------------------------------------------------------

					Rectangle rect = rowElement.Rect;

					// if the point is contained in the row then return it
					//
					if ( rect.Contains( point ) )
						return rowElement.GetItemNearestPoint( point, lastItem );

					// calculate how far the rect is from the point in both the
					// x and y dimensions
					//
					rowOffset = 0;

					if ( point.X < rect.Left )
						rowOffset += rect.Left - point.X;
					else if ( point.X > rect.Right )
						rowOffset += point.X - rect.Right;

					if ( point.Y < rect.Top )
						rowOffset += rect.Top - point.Y;
					else if ( point.Y > rect.Bottom )
						rowOffset += point.Y - rect.Bottom;

					// if this element is the nearest then save it
					// and its offset
					// 
					if ( nearestRowElement == null ||
						rowOffset < nearestRowOffset )
					{
						nearestRowElement	= rowElement;
						nearestRowOffset	= rowOffset;
					}
				}

				if ( nearestRowElement != null )
					return nearestRowElement.GetItemNearestPoint( point, lastItem );

				return null;
			}
			#endregion GetItemNearestPoint

			#region GetLastCardInColumnContainingRow

		internal UltraGridRow GetLastCardInColumnContainingRow(UltraGridRow targetRow)
		{
			// Can't continue if the target is not part of our card island.
			if (targetRow.ParentCollection != this.Rows)
				return null;

			UltraGridRow firstCardInColumn = this.GetFirstCardInColumnContainingRow(targetRow);

			// Deal with the simple fixed height case.
			if (this.FixedHeight)
			{
				if (firstCardInColumn.VisibleIndex + this.TotalRowsInCardArea > this.Rows.VisibleRowCount)
					return this.Rows.GetLastVisibleRow();

				return this.Rows.GetRowAtVisibleIndexOffset(firstCardInColumn, (this.TotalRowsInCardArea - 1));
			}


			// For variable height we need to search for the last card in the column containing
			// the target row.
			//
			// Start searching with the first card in the column containing the target row.
			// Loop thru each card in the column, until we hit the last card.
			UltraGridRow	currentCard			= firstCardInColumn;
			UltraGridRow	lastCardInColumn	= null;
			bool			spaceAvailable		= true;
			int				spaceRemaining		= this.HeightAvailableForCards;
			int				cardsCreatedCurrentCol = 0;
			while (spaceAvailable	== true	&&
				   currentCard		!= null)
			{
				// Check to see if we can fit the next card.
				if (currentCard.CardHeight > spaceRemaining)
				{
					if (cardsCreatedCurrentCol < 1)
					{
						// We haven't created any cards in this column yet -
						// allow this one card to be created even though it won't
						// be totally visible.
					}
					else
					{
						// Not enough room - need to start a new column.
						spaceAvailable = false;
						break;
					}
				}

				// Update some variables.
				cardsCreatedCurrentCol++;
				spaceRemaining -= currentCard.CardHeight + this.Band.CardSpacingVertical;
				if (spaceRemaining < 1)
					spaceAvailable = false;

				// Update lastCardInColumn
				lastCardInColumn = currentCard;

				// Get the next card.
				currentCard = this.Rows.GetRowAtVisibleIndexOffset(currentCard, 1);
			}

			return lastCardInColumn;
		}

			#endregion GetLastCardInColumnContainingRow

			#region GetRelatedVisibleCard

		internal UltraGridRow GetRelatedVisibleCard(UltraGridRow row, NavigateType navType, bool includeNonActivateable)
		{
			switch (navType)
			{
				case NavigateType.Above:
					if (this.GetFirstCardInColumnContainingRow(row) == row)
						return null;

					return this.Rows.GetRowAtVisibleIndexOffset(row, -1);

				case NavigateType.Below:
					if (this.GetLastCardInColumnContainingRow(row) == row)
						return null;

					return this.Rows.GetRowAtVisibleIndexOffset(row, 1);

				case NavigateType.Bottommost:
					return this.GetLastCardInColumnContainingRow(row);

				case NavigateType.First:
					return this.Rows.GetFirstVisibleRow();

				case NavigateType.Last:
					return this.Rows.GetLastVisibleRow();

				case NavigateType.Next:
				{
					// JJD 1/14/02
					// For variable height cards we want to go to the next card.
					//
//					if ( this.VariableHeight )
						return this.Rows.GetRowAtVisibleIndexOffset(row, 1);

					// JJD 1/14/02
					// For fixed height cards we want to we want to go to card
					// in the next column.
					//
//					return this.Rows.GetRowAtVisibleIndexOffset(row, this.TotalRowsInCardArea );
				}

				case NavigateType.Prev:
				{
					// JJD 1/14/02
					// For variable height cards we want to go to the previous card.
					//
//					if ( this.VariableHeight )
						return this.Rows.GetRowAtVisibleIndexOffset(row, -1);

					// JJD 1/14/02
					// For fixed height cards we want to we want to go to card
					// in the previous column.
					//
//					return this.Rows.GetRowAtVisibleIndexOffset(row, - this.TotalRowsInCardArea );
				}

				case NavigateType.Topmost:
					return this.GetFirstCardInColumnContainingRow(row);
			}

			return null;
		}

			#endregion GetRelatedVisibleCard

			#region GetTotalColumnsNeeded

		internal int GetTotalColumnsNeeded(UltraGridRow firstVisibleRow, int maxAvailableColumns)
		{
			// Deal with the simple fixed height case.
			if (this.FixedHeight)
			{
				int tempColumns = maxAvailableColumns;
				while ((tempColumns * this.TotalRowsInCardArea) > this.Rows.VisibleRowCount + this.TotalRowsInCardArea - 1)
					{ tempColumns--; }

				return tempColumns;
			}

			// Variable height case.
			//
			// Loop thru maxAvailableColumns columns and and count how many columns
			// it takes to fit all the rows we have to display.
			int				columnsRemainingToSearch	= maxAvailableColumns;
			UltraGridRow	currentCard					= firstVisibleRow;
			while (columnsRemainingToSearch > 0  &&
				   currentCard				!= null)
			{
				// Process the current column's cards.
				bool			spaceAvailable				= true;
				int				spaceRemaining				= this.HeightAvailableForCards;
				int				cardsCreatedCurrentCol		= 0;
				while (spaceAvailable	== true		&&
					   currentCard		!= null)
				{
					// Check to see if we can fit the next card.
					if (currentCard.CardHeight > spaceRemaining)
					{
						if (cardsCreatedCurrentCol < 1)
						{
							// We haven't created any cards in this column yet -
							// allow this one card to be created even though it won't
							// be totally visible.
						}
						else
						{
							// Not enough room - need to start a new column.
							spaceAvailable = false;
							break;
						}
					}

					// Update some variables.
					cardsCreatedCurrentCol++;
					spaceRemaining -= currentCard.CardHeight + this.Band.CardSpacingVertical;
					if (spaceRemaining < 1)
						spaceAvailable = false;

					// Get the next card.
					currentCard = this.Rows.GetRowAtVisibleIndexOffset(currentCard, 1);
				}

				columnsRemainingToSearch--;
			}

			return maxAvailableColumns - columnsRemainingToSearch;
		}

			#endregion GetTotalColumnsNeeded

			#region GetTotalColumnsNeededWithAdjustment

		internal int GetTotalColumnsNeededWithAdjustment(UltraGridRow firstVisibleRow, int maxAvailableColumns)
		{
			// Adjust the first visible row backward if possible, to ensure that we
			// use as many of the available columns as possible.  We will do this
			// by continually adjusting the first visible row backwards until we find
			// the row with the lowest index that can fill all the available columns.  
			int			 neededCols			=  0;
			UltraGridRow currentFirstRow	= this.FirstVisibleRow;
			UltraGridRow lastGoodFirstRow	= this.FirstVisibleRow;
			while (currentFirstRow != null			 &&
				currentFirstRow.VisibleIndex >= 0 &&
				neededCols <= maxAvailableColumns)
			{
				// Get the number of columns needed to display rows starting at
				// currentFirstRow.  (notice that we pass maxAvailableRows + 1 as the
				// max rows available)
				neededCols = this.GetTotalColumnsNeeded(currentFirstRow, maxAvailableColumns + 1);

				// If the number of columns needed for the currentFirstRow is
				// less than or equal to the max available columns, save
				// the currentFirstRow as a potential first row, and set 
				// currentFirstRow to the row immediately preceeding it.
				if (neededCols <= maxAvailableColumns)
				{
					// Save the current first row as a potential final first row.
					lastGoodFirstRow = currentFirstRow;

					// Move the currentFirstRow pointer backwards differently depending
					// on whether we are variable or fixed height.
					int			 offset  = this.FixedHeight ? -this.TotalRowsInCardArea : -1;
					UltraGridRow tempRow = this.Rows.GetRowAtVisibleIndexOffset(currentFirstRow, offset);
					if (tempRow == currentFirstRow)
						currentFirstRow = null;
					else
						currentFirstRow = tempRow;
				}
			}

            // MRS 12/24/2008 - TFS11618
            // Added 'if' statement. We don't want to do this check when printing, because
            // we need to be able to scroll so that any card is first. 
            if (this.IsPrinting == false)
            {
                // Adjust the first visible row if necessary.
                if (this.firstVisibleRow != lastGoodFirstRow)
                    this.firstVisibleRow = lastGoodFirstRow;
            }

			return Math.Min(neededCols, maxAvailableColumns);
		}

			#endregion GetTotalColumnsNeededWithAdjustment

			#region CanFitAllVisibleRows

		// SSP 7/15/03 UWG2240
		// Added CanFitAllVisibleRows method.
		//
		private bool CanFitAllVisibleRows( )
		{
			return this.CanFitAllVisibleRows( this.Rows );
		}

		private bool CanFitAllVisibleRows( RowsCollection rows )
		{
			if ( null != rows )
			{
				UltraGridBand band = rows.Band;
			
				int availableColumnsInCardArea = band.GetTotalColsInCardArea( this.Rect.Width );
				int availableRowsInCardArea = band.GetTotalRowsInCardArea( this.Rect.Height );
				
				// If all the cards can fit in the card area, then make the first visible card the
				// first visible row.
				//
				if ( availableColumnsInCardArea * availableRowsInCardArea >= rows.VisibleRowCount )
					return true;
			}

			return false;
		}

			#endregion // CanFitAllVisibleRows

			#region InitializeRow

		/// <summary>
		/// Initializes our FirstVisibleRow with the UltraGridRow that represents the first row in the card area.
		/// </summary>
		/// <param name="row"></param>
		// SSP 3/19/03 - Row Layout Functionality
		// Made this method protected because we need to call it from the designer code.
		//
		//internal void InitializeRow(UltraGridRow row)
		internal protected void InitializeRow(UltraGridRow row)
		{
			// If we already have a row context we want to keep it, but we need to
			// make sure it's still valid.
			if (this.PrimaryContext != null)
			{
				RowsCollection currentRowsCollection = (RowsCollection)this.PrimaryContext;
				if (currentRowsCollection != row.ParentCollection)
				{
					currentRowsCollection.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					this.PrimaryContext	= null;
				}
			}

			// SSP 7/15/03 UWG2240
			// If the first visible card is hidden as a result of filtering operation and the
			// card area can fit all the visible rows, then make the first visible card the
			// first visible row so we display all the cards.
			// 
			// ------------------------------------------------------------------------------------
			UltraGridRow tmpRow = null != row ? row.ParentCollection.InternalFirstVisibleCard : null;
			if ( null != tmpRow && tmpRow.VisibleIndex < 0 )
			{
				if ( this.CanFitAllVisibleRows( row.ParentCollection ) )
					row.ParentCollection.FirstVisibleCard = row.ParentCollection.GetFirstVisibleRow( );

				this.firstVisibleRow = row.ParentCollection.FirstVisibleCard;
			}
				// SSP 7/29/03 UWG2356
				// Added the else if block.
				//
			else if ( null != row && this.CanFitAllVisibleRows( row.ParentCollection ) )
			{
				if ( this.CanFitAllVisibleRows( row.ParentCollection ) )
					row.ParentCollection.FirstVisibleCard = row.ParentCollection.GetFirstVisibleRow( );

				this.firstVisibleRow = row.ParentCollection.FirstVisibleCard;
			}
			// ------------------------------------------------------------------------------------

			if (this.PrimaryContext == null)
			{
				this.PrimaryContext	 = row.ParentCollection;

				this.firstVisibleRow = row.ParentCollection.FirstVisibleCard;

				// JM UWG977 01-23-02 Cache a reference to the band.
				this.band			 = row.Band;

				row.ParentCollection.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}
				// SSP 12/5/03 UWG2295
				// Make sure that the firstVisibleRow is assigned the FirstVisibleCard property
				// value. Added below else block. Exposed FirstVisibleCardRow property off
				// the rows collection which the user can set so we need to make sure the first
				// visible row set by the user is displayed by the card-area.
				//
			else
			{
				if ( null != row && null != row.ParentCollection )
					this.firstVisibleRow = row.ParentCollection.FirstVisibleCard;
			}

			// SSP 10/19/05 BR04664
			// In row-layout designer, make sure that we don't end up using a template add-row
			// or a fitler row for designing the row layout. Added the following if block.
			// 
			if ( this.RowLayoutDesignerElement && null != row 
				&& null != row.ParentCollection && row.ParentCollection.Count > 0 )
			{
				this.firstVisibleRow = row.ParentCollection[0];
			}

			// SSP 5/16/05 - Optimization
			// Row col region intersection element had an optimization for not causing the existing
			// row elements to reposition their descendants. It never worked so made it work.
			//
			//this.origParentSize = this.Parent.Rect.Size;
		}

			#endregion InitializeRow

			#region RefreshFirstVisibleCard

		internal void RefreshFirstVisibleCard()
		{
			if ( this.Rows == null )
			{
				Debug.Fail( "Rows collection not set in call to RefreshFirstVisibleCard" );
				return;
			}

			UltraGridRow newFirstRow = this.Rows.FirstVisibleCard;

			if ( newFirstRow != this.firstVisibleRow )
			{
				this.firstVisibleRow = newFirstRow;
				this.DirtyChildElements();
			}
		}

			#endregion RefreshFirstVisibleCard

			#region ResetHorizontalScrollBarMetricsAndValue

		//	BF 11.2.04	NAS2005 Vol1 - CardView vertical scrolling

		internal void ResetHorizontalScrollBarMetricsAndValue()
		{
			// SSP 3/19/03 - Row Layout Functionality
			// We are not displaying scrollbars in the crow layout designer.
			//
			if ( this.RowLayoutDesignerElement )
				return;

			// SSP 10/20/04 UWG3368
			// Apply the scroll bar look to the card area scrollbars.
			//
			this.HScrollBarInfo.ScrollBarLook = null != this.Band ? this.Band.Layout.ScrollBarLook : null;

			// SSP 7/2/02
			// Compressed card view. If card view style is compressed, then don't
			// reset these values while scroll operation is currently taking place.
			// We only want to do this when we receive an EndScroll message. Basically
			// we want to prevent changing the the thumb size while dragging the scroll 
			// thumb (without having released the thumb).
			//
			// --------------------------------------------------------------------
			// SSP 2/28/03 - Row Layout Functionality
			// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
			// to StandardLabels card style so use the StyleResolved instead of the Style property.
			//
			if ( CardStyle.Compressed == this.Band.CardSettings.StyleResolved )
			{
				// We don't want to reset the scrollbard metrics if the user
				// is in the midst of scrolling process (like dragging the thumb).
				//
				if ( !this.currentlyScrolling )
				{
					int totalVisibleCards = this.TotalVisibleCards;
				
					int visibleRowsCount = this.Rows.VisibleRowCount;
					int scrollBarVal = 0;

					int maximum = this.Rows.CalculateCardAreaScrollRange( 
						this.HeightAvailableForCards, this.FirstVisibleRowIndex, out scrollBarVal );

					this.HScrollBarInfo.Minimum = 0;
					this.HScrollBarInfo.SmallChange = 1;
					this.HScrollBarInfo.LargeChange = Math.Max( 1, this.TotalColsInCardArea );
					
					Debug.Assert( maximum < visibleRowsCount );
					this.HScrollBarInfo.Maximum	= Math.Max( this.HScrollBarInfo.Minimum,
						Math.Min( maximum, visibleRowsCount - 1 ) );

					// If the last visible row is being displayed, we need to make up for
					// the crudeness of the averageCardsPerCol calculation by manually forcing
					// the scrollbar thumb to be 'pinned' by setting the scrollbar Value to
					// the max possible value.
					bool lastVisibleRowDisplayed = false;
					bool firstVisibleRowDisplayed = false;

					// SSP 4/8/03 UWG2115
					// Check for FirstVisibleRow being null because if all the rows are filtered
					// out FirstVisibleRow will be null.
					//
					//int firstVisibleRowVisibleIndex = this.FirstVisibleRow.VisibleIndex;
					int firstVisibleRowVisibleIndex = 
						null != this.FirstVisibleRow 
						? this.FirstVisibleRow.VisibleIndex 
						: -1;					
					
					// SSP 4/8/03 UWG2115
					// Related to above change.
					//
					if ( firstVisibleRowVisibleIndex < 0 )
					{
						firstVisibleRowDisplayed = lastVisibleRowDisplayed = true;
					}
					else
					{
						if ( 0 == firstVisibleRowVisibleIndex )
							firstVisibleRowDisplayed = true;

						if ( ( firstVisibleRowVisibleIndex + totalVisibleCards ) >= visibleRowsCount )
							lastVisibleRowDisplayed = true;
					}
						

					// If first and the last visible rows are visible, then
					// disable the scrollbar. This could be the result of
					// the nature of approximating the total visible rows in
					// a column.
					//
					if ( lastVisibleRowDisplayed && firstVisibleRowDisplayed )
					{
						this.HScrollBarInfo.Enabled	= false;
					}
					else
					{
						this.HScrollBarInfo.Enabled	= true;

						if ( firstVisibleRowDisplayed )
						{
							// If the first visible row is displayed, then scrollbar's
							// Value should be set to 0.
							//
							this.HScrollBarInfo.Value = 0;
						}
						else if ( lastVisibleRowDisplayed )
						{
							this.HScrollBarInfo.Value	= this.HScrollBarInfo.Maximum - this.HScrollBarInfo.LargeChange + 1;
						}
						else 						
						{
							if ( scrollBarVal <= this.HScrollBarInfo.Minimum )
								scrollBarVal = 1 + this.HScrollBarInfo.Minimum;

							if ( scrollBarVal > this.HScrollBarInfo.Maximum )
								scrollBarVal = this.HScrollBarInfo.Maximum;

							this.HScrollBarInfo.Value = scrollBarVal;
						}
					}
				}

				// SSP 4/21/05 BR03382
				// Need to execute some code at the end so instead of returning here enclosed
				// the rest of the code into the else block (except for the new code at the
				// end of the method).
				//
				//return;
			} // End of compressed card view scrolling if block
				// --------------------------------------------------------------------
			else
			{
				this.HScrollBarInfo.SmallChange	= 1;
				this.HScrollBarInfo.LargeChange	= Math.Max(1, this.TotalColsInCardArea);
				this.HScrollBarInfo.Minimum		= 0;

				// JM 01-10-02 If there are no visible rows, we don't need the scroll bar
				if (this.Rows.VisibleRowCount < 1 ||
					this.FirstVisibleRowIndex < 0)
				{
					this.HScrollBarInfo.Maximum	= 0;
					this.HScrollBarInfo.Value	= 0;
					this.HScrollBarInfo.Enabled	= false;
					return;
				}

				if (this.FixedHeight)
				{
					this.HScrollBarInfo.Maximum	= ((this.Rows.VisibleRowCount + this.TotalRowsInCardArea - 1) / this.TotalRowsInCardArea) - 1;

                    
                    // JJD 12/20/01
                    // Make sure the first row isn't past one that would have
                    // been set using the scrollbar
                    //
                    int firstVisibleRowIndex = this.FirstVisibleRowIndex;

                    // MRS NAS v8.2 - CardView Printing
                    // Added 'if' statement. We don't want to do this check when printing, because
                    // we need to be able to scroll so that any card is first. 
                    if (this.IsPrinting == false)
                    {
                        while (firstVisibleRowIndex > 0 &&
                            firstVisibleRowIndex / this.TotalRowsInCardArea > this.HScrollBarInfo.Maximum + 1 - this.HScrollBarInfo.LargeChange)
                        {
                            firstVisibleRowIndex = Math.Max(0, firstVisibleRowIndex - this.TotalRowsInCardArea);

                            this.firstVisibleRow = this.Rows.GetRowAtVisibleIndex(firstVisibleRowIndex);
                        }
                    }

					// JJD 12/20/01
					// Set the value of the scrollbar based on its visible index
					//
					// SSP 7/15/03 UWG2240
					// If the first visible row in the card area is not the first absolute visible row
					// then make sure the user can scroll it into view by ensuring the ScrollBarInfo.Value
					// is greater than 0 so it can be scrolled left.
					// Added the if block and enclosed the already existing code into the else block
					// because if it goes into the if block, we always want to have the scroll bar enabled.
					//
					//this.HScrollBarInfo.Value = firstVisibleRowIndex / this.TotalRowsInCardArea;
					int totalRowsInCardArea = this.TotalRowsInCardArea;
					this.HScrollBarInfo.Value = firstVisibleRowIndex / totalRowsInCardArea;
					if ( firstVisibleRowIndex > 0 )
					{
						if ( this.HScrollBarInfo.Value <= this.HScrollBarInfo.Minimum )
						{
							int newScrollbarVal = 1 + this.HScrollBarInfo.Minimum;
							this.HScrollBarInfo.Maximum = Math.Max( 1 + newScrollbarVal, this.HScrollBarInfo.Maximum );
							this.HScrollBarInfo.Value = newScrollbarVal;
						}
						this.HScrollBarInfo.Enabled = true;
					}
					else
					{
						// JM 01-10-02 Disable the scrollbar if the minimum = (maximum - large change).
						if (this.HScrollBarInfo.Minimum == (this.HScrollBarInfo.Maximum + 1 - this.HScrollBarInfo.LargeChange))
							this.HScrollBarInfo.Enabled = false;
						else
							this.HScrollBarInfo.Enabled = true;
					}
				}
				else
				{
					int totalVisibleCards = this.TotalVisibleCards;

					// If we're able to display all the Visible Rows, disable the scrollbar.
					// otherwise enable it and calculate the scrollbar Maximum and Value.
					if (this.FirstVisibleRowIndex	== 0 &&
						totalVisibleCards			== this.Rows.VisibleRowCount)
					{
						this.HScrollBarInfo.Enabled = false;
					}
					else
					{
						// Enable the scollbar.
						this.HScrollBarInfo.Enabled	= true;

						// Estimate the scrollbar maximum based on the average cards per
						// column in the current display (icky)
						int averageCardsPerCol		= Math.Max(1, this.TotalVisibleCards / this.TotalColsInCardArea);
						this.HScrollBarInfo.Maximum	= ((this.Rows.VisibleRowCount + averageCardsPerCol - 1) / averageCardsPerCol) - 1;

						// If the last visible row is being displayed, we need to make up for
						// the crudeness of the averageCardsPerCol calculation by manually forcing
						// the scrollbar thumb to be 'pinned' by setting the scrollbar Value to
						// the max possible value.
						bool lastVisibleRowDisplayed = false;
						if ((this.FirstVisibleRow.VisibleIndex + this.TotalVisibleCards) >=
							this.Rows.VisibleRowCount)
							lastVisibleRowDisplayed = true;

						if (lastVisibleRowDisplayed)
							this.HScrollBarInfo.Value	= this.HScrollBarInfo.Maximum - this.HScrollBarInfo.LargeChange + 1;
						else
							this.HScrollBarInfo.Value	= this.FirstVisibleRowIndex / averageCardsPerCol;

						// SSP 7/15/03 UWG2240
						// If the first visible row in the card area is not the first absolute visible row
						// then make sure the user can scroll it into view by ensuring the ScrollBarInfo.Value
						// is greater than 0 so it can be scrolled left.
						//
						if ( null != this.FirstVisibleRow && this.FirstVisibleRow.VisibleIndex > 0 )
						{
							if ( this.HScrollBarInfo.Value <= this.HScrollBarInfo.Minimum )
							{
								int newScrollbarVal = 1 + this.HScrollBarInfo.Minimum;
								this.HScrollBarInfo.Maximum = Math.Max( 1 + newScrollbarVal, this.HScrollBarInfo.Maximum );
								this.HScrollBarInfo.Value = newScrollbarVal;
							}
						}
					}				
				}
			}
			
			// SSP 4/21/05 BR03382
			// Added MaintainScrollPositionHelper. Maintain the position of the horizontal 
			// scrollbar whenever the rows in card area are sorted.
			//
			// ----------------------------------------------------------------------------------
			if ( this.scrollBarPositionToMaintain >= 0 && null != this.hScrollBarInfo )
			{
				int pos = this.scrollBarPositionToMaintain;
				this.scrollBarPositionToMaintain = -1;

				if ( this.HScrollBarInfo.Minimum >= 0 && 0 < this.HScrollBarInfo.Maximum )
				{
					this.HScrollBarInfo.Value = 0;
					this.OnScroll( this.HScrollBarInfo, new ScrollEventArgs( ScrollEventType.ThumbPosition, pos ) );

					// Call this method again. This won't cause recursion since we are resetting
					// the this.scrollBarPositionToMaintain to -1 above.
					//
					this.ResetHorizontalScrollBarMetricsAndValue( );
				}
			}
			// ----------------------------------------------------------------------------------
		}

			#endregion ResetHorizontalScrollBarMetricsAndValue

			#region ScrollCardIntoView

		internal void ScrollCardIntoView(UltraGridRow rowToScroll, bool exitEditMode)
		{
			UltraGridRow oldFirstCard	= this.firstVisibleRow;
			RowUIElementBase rowElement		= null;
			int			 cardsCurrentlyDisplayed = 0;

			// Can't scroll row into view if it is not part of our card island
			if (rowToScroll.ParentCollection != this.Rows)
				return;

			//	BF 11.3.04	NAS2005 Vol1 - CardView vertical scrolling
			//	RowUIElements are now children of the CardAreaScrollRegionUIElement,
			//	so we have to plow thru that element's ChildElements, not this one's.
			
			#region Obsolete code
			//	// See if we're already displaying the requested row.
			//	for (int i = 0; i < this.ChildElements.Count; i++)
			//	{
			//		if (!(this.ChildElements[i] is RowUIElement))
			//			continue;
			//
			//		// If the card for this row is already visible then just return
			//		rowElement = this.ChildElements[i] as RowUIElement;
			//		if (rowElement.Row == rowToScroll  &&  !rowElement.ClipRect.IsEmpty)
			//			return;
			//
			//		cardsCurrentlyDisplayed++;
			//	}
			#endregion Obsolete code

			//	BF 11.3.04	NAS2005 Vol1 - CardView vertical scrolling (see above)
			// See if we're already displaying the requested row.
			CardAreaScrollRegionUIElement cardAreaScrollRegionUIElement = this.GetDescendant( typeof(CardAreaScrollRegionUIElement) ) as CardAreaScrollRegionUIElement;
			if ( cardAreaScrollRegionUIElement == null )
			{
				Debug.Assert( false, "Couldn't get a CardAreaScrollRegionUIElement in CardAreaUIElement.ScrollCardIntoView - no action will be taken." );
				return;
			}

			for (int i = 0; i < cardAreaScrollRegionUIElement.ChildElements.Count; i++)
			{
				if (!(cardAreaScrollRegionUIElement.ChildElements[i] is RowUIElementBase))
					continue;

				// If the card for this row is already visible then just return
				rowElement = cardAreaScrollRegionUIElement.ChildElements[i] as RowUIElementBase;
				if (rowElement.Row == rowToScroll  &&  !rowElement.ClipRect.IsEmpty)
					return;

				cardsCurrentlyDisplayed++;
			}

			// If asked, exit edit mode before doing the scroll.
			if (exitEditMode && this.Band.Layout.ActiveCell != null)
				this.Band.Layout.ActiveCell.ExitEditMode();

			// Get the first card in the column containing the row we are scrolling into view.
			UltraGridRow newFirstCard = this.GetFirstCardInColumnContainingRow(rowToScroll);
			if (newFirstCard == null)
				return;

			// If there is more than 1 card displayed in the existing area and
			// we are scrolling to the right, we want the column containing the row being
			// scrolled to be the last column displayed.  To do this we need to determine
			// the first card of the column that is (TotalCols - 1) columns to the left of 
			// the column containing the scrolled row.
			if (cardsCurrentlyDisplayed > 1 &&
				oldFirstCard.Index < newFirstCard.Index )
			{
				UltraGridRow tempCard = this.GetColumnRelativeToColumn(newFirstCard, (-1 * (this.TotalColsInCardArea - 1))); 
				if (tempCard != null)
					newFirstCard = tempCard;
			}

			// Set the default first card to the passed in row
			this.Rows.FirstVisibleCard = newFirstCard;

			// Call RefreshFirstVisibleCard which will pick up the new firstVisibleCard
			// set above.
			this.RefreshFirstVisibleCard();
		}

			#endregion ScrollCardIntoView

			#region ScrollCardArea

		/// <summary>
		/// Scrolls the card area 'columnsToScroll' columns.  Supply a positive number to scroll forward or a negative number to scroll backward.
		/// </summary>
		/// <param name="columnsToScroll"></param>
		/// <returns>True if the card area was scrolled</returns>
		internal bool ScrollCardArea(int columnsToScroll)
		{
			UltraGridRow newFirstRow = this.GetColumnRelativeToColumn(this.firstVisibleRow, columnsToScroll);
			if (newFirstRow != null)
			{
				// Set the first visible card in both the rows collection
				// and our primary context.
				this.Rows.FirstVisibleCard	= newFirstRow;
				this.firstVisibleRow		= newFirstRow;

				this.DirtyChildElements();

				return true;
			}
			else
				return false;
		}

			#endregion ScrollCardArea

			#region NeedsVerticalScrollbar

		//	BF 11.2.04	NAS2005 Vol1 - CardView vertical scrolling
		private bool NeedsVerticalScrollbar( int cardAreaHeight )
		{
			int heightRequired = 0;
			return this.NeedsVerticalScrollbar( cardAreaHeight, out heightRequired );
		}

		private bool NeedsVerticalScrollbar( int cardAreaHeight, out int heightRequired )
		{
			heightRequired = 0;

			//	If more than one row of cards is visible, or none at all,
			//	we don't require a scrollbar
			int totalRowsInCardArea = this.TotalRowsInCardArea;
			if ( totalRowsInCardArea != 1 )
				return false;

			UltraGridBand band = this.Band;

			//	For VariableHeight style, we iterate the
			//	visible rows (cards) and use the height of the tallest card
			if ( this.VariableHeight )
			{
				int	currentCardIndex = this.FirstVisibleRowIndex;
				int totalCols = this.TotalColsInCardArea;
				for ( int i = currentCardIndex; i < (currentCardIndex + totalCols); i ++ )
				{
					UltraGridRow row = this.Rows.GetRowAtVisibleIndex( i );

					if ( row == null )
						continue;

					heightRequired = Math.Max( heightRequired, row.CardHeight );
				}

				heightRequired += band.CardAreaMarginsResolved;
			}
			else
			if ( this.Compressed )
			{
				int	currentCardIndex = this.FirstVisibleRowIndex;
				int totalCards = this.TotalVisibleCards;
				for ( int i = currentCardIndex; i < (currentCardIndex + totalCards); i ++ )
				{
					UltraGridRow row = this.Rows.GetRowAtVisibleIndex( i );

					if ( row == null )
						continue;

					heightRequired = Math.Max( heightRequired, row.CardHeight );
				}

				heightRequired += band.CardAreaMarginsResolved;
			}
			else
				heightRequired = band.CardHeight + band.CardAreaMarginsResolved;

			return heightRequired > cardAreaHeight;
		}
			#endregion NeedsVerticalScrollbar

			#region ResetVerticalScrollBarMetricsAndValue

		//	BF 11.2.04	NAS2005 Vol1 - CardView vertical scrolling
		private void ResetVerticalScrollBarMetricsAndValue( int cardAreaHeight, int cardHeight )
		{
			if ( this.RowLayoutDesignerElement )
				return;

			UltraGridBand band = this.Band;
			int heightRequired = cardHeight + band.CardAreaMarginsResolved;

			if ( heightRequired <= cardAreaHeight )
			{
				Debug.Assert( false, "ResetVerticalScrollBarMetricsAndValue called when there is no need for a vertical scrollbar." );
				return;
			}

			//	Set the ScrollBarLook, based on that of the Layout
			this.VScrollBarInfo.ScrollBarLook = null != band ? band.Layout.ScrollBarLook : null;
			
			//	Minimum is always zero; Maximum is the number of pixels
			//	that comprises the difference between the card area height
			//	and the available height.
			this.VScrollBarInfo.Minimum = 0;

            // MRS NAS v8.2 - CardView Printing
            //int maximum =  heightRequired - cardAreaHeight;
            int maximum = heightRequired;
            bool isPrinting = this.IsPrinting;
            if (isPrinting == false)
                maximum -= cardAreaHeight;

			//	When SmallChange is set to 1, scrolling is a little too slow,
			//	so we'll make it (for now at least) whatever value produces 10
			//	logical "pages", but not allow it to be less than one.
			int smallChange = Math.Max( 1, ((maximum / 10) + 1) );

			//	LargeChange will be 5 times the size of SmallChange (but no
			//	smaller than 1), which makes only 2 clicks on the track necessary
			//	to scroll the entire card.
			this.VScrollBarInfo.LargeChange = Math.Max( 1, smallChange * 5 );
			this.VScrollBarInfo.SmallChange = smallChange;
			
			//	Increase the Maximum by the amount of LargeChange, to overcome
			//	the fact that when the thumb is all the way to the bottom, the
			//	value is [Maximum - LargeChange].
			maximum += this.VScrollBarInfo.LargeChange;
			this.VScrollBarInfo.Maximum = maximum;

			//	Clamp the value at the maximum.
			if ( this.VScrollBarInfo.Value > maximum )
				this.VScrollBarInfo.Value = maximum;

		}
			#endregion ResetVerticalScrollBarMetricsAndValue

			#region OnMouseWheel

		//	BF 11.9.04	NAS2005 Vol1 - CardView vertical scrolling
		//
		//	Added the OnMouseWheel method so we could support mouse wheel
		//	scrolling for the card area. This method is called by from the
		//	the UltraGridBase class' OnMouseWheel method.
		//
		internal bool OnMouseWheel( MouseEventArgs e )
		{
			//	Note that we only use the sign of the delta, not the magnitude

			//	Determine the scroll action based on the sign of the delta
			ScrollBarAction scrollbarAction =	e.Delta > 0 ?
												ScrollBarAction.SmallDecrement :
												ScrollBarAction.SmallIncrement;

			Point mouseLocation = new Point( e.X, e.Y );
			bool handled = false;

			//	If the cursor is positioned over the horizontal scrollbar, and
			//	the scrollbar is enabled, the wheel movement is used to scroll
			//	horizontally; if not, it is used to scroll vertically.
			if ( this.hScrollBarInfo != null && this.hScrollBarInfo.EnabledResolved &&
				 this.hScrollbarElement != null && this.hScrollbarElement.Rect.Contains(mouseLocation) )
			{
				this.HScrollBarInfo.PerformAction( scrollbarAction );
				handled = true;
			}
			else
			if ( this.vScrollbarElement != null && this.Rect.Contains(mouseLocation) )
			{
				this.VScrollBarInfo.PerformAction( scrollbarAction );
				handled = true;
			}
			
			return handled;				 
		}
			#endregion OnMouseWheel

		#endregion Internal Methods

		#region Private Methods

			#region InvalidateAfterCardAreaResize

		private void InvalidateAfterCardAreaResize()
		{

			RowColRegionIntersectionUIElement rowColRegionElement = this.GetAncestor(typeof(RowColRegionIntersectionUIElement)) as RowColRegionIntersectionUIElement;
			if (rowColRegionElement != null)
			{
				rowColRegionElement.RowScrollRegion.SetDestroyVisibleRows();

				DataAreaUIElement dataAreaElement = this.GetAncestor(typeof(DataAreaUIElement)) as DataAreaUIElement;
				if (dataAreaElement != null)
				{
					dataAreaElement.DirtyChildElements();
				}
			}
		}

			#endregion InvalidateAfterCardAreaResize

			#region PositionElementsVariableHeight (obsolete)
		//
		//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling
		//	Relocated this to the CardAreaScrollRegionUIElement class.
		//
		//	private void PositionElementsVariableHeight(Rectangle rectCards, UIElementsCollection oldElements)
		//	{
		//		Rectangle rectWork;
		//
		//		// Create a RowUIElement for each visible card.
		//		//
		//		// Setup some variables we'll need.
		//		int				currentCardIndex		= this.FirstVisibleRowIndex;
		//		int				heightUsedCurrentCol	= 0;
		//		int				cardsCreatedCurrentCol	= 0;
		//		int				totalRows				= this.Rows.VisibleRowCount;
		//		UltraGridRow	row						= null;
		//		int				cumulativeVerticalOverlaps
		//												= 0;
		//
		//		// Process each card by card area row within card area column.
		//		for (int cardAreaCol = 0; cardAreaCol < this.TotalColsInCardArea; cardAreaCol++)
		//		{
		//			heightUsedCurrentCol		= 0;
		//			cardsCreatedCurrentCol		= 0;
		//			cumulativeVerticalOverlaps	= 0;
		//			while (currentCardIndex < totalRows)
		//			{
		//				// Get the row we're working with.
		//				row = this.Rows.GetRowAtVisibleIndex(currentCardIndex);
		//
		//				// Check to see if we can fit the next card.
		//				if ((rectCards.Top + heightUsedCurrentCol + row.CardHeight) > rectCards.Bottom)
		//				{
		//					if (cardsCreatedCurrentCol < 1)
		//					{
		//						// We haven't created any cards in this column yet -
		//						// allow this one card to be created even though it won't
		//						// be totally visible.
		//					}
		//					else
		//					{
		//						// Not enough room - need to start a new column.
		//						break;
		//					}
		//				}
		//
		//				// Create the RowUIElement for the card.  This is the element that
		//				// actually contains the card contents (caption, cells and possibly labels).
		//				//
		//				// Try to reuse an element from the old elements list.
		//				RowUIElement rowElement = (RowUIElement)CardAreaUIElement.ExtractExistingElement(oldElements, typeof(RowUIElement), true);
		//				if (rowElement == null)
		//					rowElement = new RowUIElement(this);
		//
		//				// Initialize the element.
		//				rowElement.InitializeRow(row);
		//				rowElement.InFirstCardAreaCol = (cardAreaCol == 0);
		//				rowElement.InFirstCardAreaRow = (cardsCreatedCurrentCol == 0);
		//				rowElement.InLastCardAreaCol  = (cardAreaCol == (this.TotalColsInCardArea - 1));
		//
		//				rectWork		 = rectCards;
		//				rectWork.X		+= (cardAreaCol * (this.CardWidth + this.Band.CardSpacingHorizontal));
		//				rectWork.Width	 = this.CardWidth;
		//				rectWork.Y		+= heightUsedCurrentCol;
		//				rectWork.Height	 = row.CardHeight;
		//
		//				// Make sure we haven't gone past the bottom of the card area.
		//				if (rectWork.Bottom > rectCards.Bottom)
		//					rectWork.Height = rectCards.Bottom - rectWork.Y;
		//
		//				// Overlap the left edge of the card and the previous card (or the card
		//				// area if we're in the first column) if the conditions are right.
		//				if ( (cardAreaCol == 0  &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved) &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//						this.Band.CardSpacingHorizontal == 0)
		//											||
		//						(cardAreaCol != 0  &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//						this.Band.CardSpacingHorizontal == 0) )
		//				{
		//					rectWork.X--;
		//					rectWork.Width++;
		//				}
		//
		//				// Overlap the top edge of the card if the 
		//				// conditions are right.
		//				if ((cardsCreatedCurrentCol == 0  &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved) &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//						this.Band.CardSpacingVertical == 0)
		//										||
		//					(cardsCreatedCurrentCol == 0  &&
		//						this.Band.HeaderVisible  &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleHeaderResolved) &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//						this.Band.CardSpacingVertical == 0)
		//										||
		//					(cardsCreatedCurrentCol != 0  &&
		//						this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//						this.Band.CardSpacingVertical == 0))
		//				{
		//					cumulativeVerticalOverlaps++;
		//				}
		//
		//				// Overlap the right edge of the rightmost card with the card area if the 
		//				// conditions are right.
		//				if (cardAreaCol == (this.TotalColsInCardArea - 1)  &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved) &&
		//					this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleRowResolved) &&
		//					this.Band.CardSpacingHorizontal == 0)
		//
		//				{
		//					rectWork.Width++;
		//				}
		//
		//				// Apply the cumulative vertical overlaps to the card's
		//				// rect.
		//				rectWork.Y		-= cumulativeVerticalOverlaps;
		//
		//				rowElement.Rect	 = rectWork;
		//				this.ChildElements.Add(rowElement);
		//
		//				// Update some variables.
		//				currentCardIndex++;
		//				cardsCreatedCurrentCol++;
		//				heightUsedCurrentCol += rectWork.Height + this.Band.CardSpacingVertical;
		//			}
		//		}
		//	}

			#endregion PositionElementsVariableHeight (obsolete)

			#region NumberOfVariableHeightCardsDisplayableInColumns

		private int NumberOfVariableHeightCardsDisplayableInColumns(int availableColumns)
		{
			// Variable Height
			//
			// Count the number of cards we can display given the available number
			// of columns.
			int				totalCardsCreated			= 0;
			int				columnsRemainingToSearch	= availableColumns;
			UltraGridRow	currentCard					= this.firstVisibleRow;
			while (columnsRemainingToSearch > 0  &&
				   currentCard				!= null)
			{
				// Process the current column's cards.
				bool			spaceAvailable				= true;
				int				spaceRemaining				= this.HeightAvailableForCards;
				int				cardsCreatedCurrentCol		= 0;
				while (spaceAvailable	== true		&&
					   currentCard		!= null)
				{
					// Check to see if we can fit the next card.
					if (currentCard.CardHeight > spaceRemaining)
					{
						if (cardsCreatedCurrentCol < 1)
						{
							// We haven't created any cards in this column yet -
							// allow this one card to be created even though it won't
							// be totally visible.
						}
						else
						{
							// Not enough room - need to start a new column.
							spaceAvailable = false;
							break;
						}
					}

					// Update some variables.
					cardsCreatedCurrentCol++;
					totalCardsCreated++;
					spaceRemaining -= currentCard.CardHeight + this.Band.CardSpacingVertical;
					if (spaceRemaining < 1)
						spaceAvailable = false;

					// Get the next card.
					currentCard = this.Rows.GetRowAtVisibleIndexOffset(currentCard, 1);
				}

				columnsRemainingToSearch--;
			}

			return totalCardsCreated;
		}

			#endregion NumberOfVariableHeightCardsDisplayableInColumns

			#region NotifyCalcManagerHelper

		// SSP 11/18/05 BR07794
		// We need to recalculate the formula cells in newly scrolled into view card rows.
		// 
		private void NotifyCalcManagerHelper( bool onlyIfFirstCardChanged )
		{
			UltraGridBand band = this.Band;
			UltraGridLayout layout = null != band ? band.Layout : null;
			if ( null == layout )
				return;

			if ( ! onlyIfFirstCardChanged || this.calcManager_lastFirstVisibleRow != this.firstVisibleRow )
			{
				this.calcManager_lastFirstVisibleRow = this.firstVisibleRow;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//layout.ViewStyleImpl.OnAfterRowListCreated_ProcessBand( band );
				ViewStyleBase.OnAfterRowListCreated_ProcessBand( band );
			}
		}

			#endregion // NotifyCalcManagerHelper

		#endregion Private Methods

		#region ScrollBar Event Handlers

		//	Scroll events
		private System.Windows.Forms.ScrollEventHandler	scrollHandler = null;
		private System.EventHandler						scrollValueChangedHandler = null;
		
		/// <summary>
		/// Returns the event handler that notifies Scroll
		/// </summary>
		protected System.Windows.Forms.ScrollEventHandler ScrollHandler
		{
			get
			{
				if (this.scrollHandler == null)
					this.scrollHandler = new System.Windows.Forms.ScrollEventHandler(OnScroll);

				return this.scrollHandler;
			}
		}

		/// <summary>
		/// Returns the event handler that notifies ValueChanged
		/// </summary>
		protected System.EventHandler ScrollValueChangedHandler
		{
			get
			{
				if (this.scrollValueChangedHandler == null)
					this.scrollValueChangedHandler = new System.EventHandler(OnScrollValueChanged);

				return this.scrollValueChangedHandler;
			}
		}

		/// <summary>
		/// Handles the scrollbar control's OnValueChanged event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
        // MRS 12/19/06 - fxCop
        //protected void OnScrollValueChanged(object sender, EventArgs args)
        private void OnScrollValueChanged(object sender, EventArgs args)
        {
		}

		/// <summary>
		/// Handles user interactivity with the scrollbar
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
        // MRS 12/19/06 - fxCop
        //protected void OnScroll(object sender, ScrollEventArgs args)
        private void OnScroll(object sender, ScrollEventArgs args)
        {
			// Exit edit mode.
			if (this.Band.Layout.ActiveCell != null)
				this.Band.Layout.ActiveCell.ExitEditMode();

			//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling
			//
			//	Changed this method to handle both horizontal and vertical
			//	scrolling...note that the only significant change is that
			//	the existing logic is now only executed when the sender is
			//	the ScrollBarInfo for the horizontal scrollbar, but child
			//	elements are dirtied regardless of who the sender is.
			//
			if ( sender == this.hScrollBarInfo )
			{			
				try
				{
					if (args.Type != ScrollEventType.EndScroll)
					{
						// Set the first visible card in both the rows collection
						// and our primary context.
						UltraGridRow newFirstRow = this.GetColumnRelativeToColumn(this.firstVisibleRow, args.NewValue - this.HScrollBarInfo.Value);

						if (newFirstRow != null)
						{
							// SSP 12/5/03 UWG2295
							// We need to fire AfterCardsScroll event however we only should do that
							// if cards actually scrolled.
							//
							UltraGridRow originalFirstVisibleCard = this.Rows.FirstVisibleCard;

							this.Rows.FirstVisibleCard	= newFirstRow;
							this.firstVisibleRow		= newFirstRow;

							// SSP 12/5/03 UWG2295
							// Added AfterCardsScroll event. Fire AfterCardsScroll when the user scrolls
							// the cards.
							//
							// ------------------------------------------------------------------------------
							if ( originalFirstVisibleCard != this.Rows.FirstVisibleCard )
							{
								// SSP 12/21/04 BR01302
								// When the card area scrolls notify the calc manager of new rows getting scrolled
								// into view. This is for the deferred calculations mode.
								//
								// --------------------------------------------------------------------------------
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//if ( null != this.Band.Layout.ViewStyleImpl )
								//    this.Band.Layout.ViewStyleImpl.OnAfterRowListCreated_ProcessBand( this.Band );
								ViewStyleBase.OnAfterRowListCreated_ProcessBand( this.Band );
								// --------------------------------------------------------------------------------

								UltraGrid grid = this.Rows.Band.Layout.Grid as UltraGrid;
								grid.FireEvent( GridEventIds.AfterCardsScroll, new AfterCardsScrollEventArgs( this.Rows ) );
							}
							// ------------------------------------------------------------------------------
						}
					}
				}
				catch
				{
					return;
				}

			}

			this.DirtyChildElements();
		}

		#endregion Scrollbar Event Handlers

		#region PropChangeHandlers

		private SubObjectPropChangeEventHandler subObjectPropChangeHandler = null;

		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChangeInfo">A structure containing the property change information.</param>
		protected virtual void OnSubObjectPropChanged(PropChangeInfo propChangeInfo) 
		{
			this.DirtyChildElements();		
		}

		/// <summary>
		/// Returns the event handler that notifies OnSubObjectPropChanged
		/// </summary>
		protected SubObjectPropChangeEventHandler SubObjectPropChangeHandler
		{
			get
			{
				if (this.subObjectPropChangeHandler == null)
					this.subObjectPropChangeHandler = new SubObjectPropChangeEventHandler(OnSubObjectPropChanged);

				return this.subObjectPropChangeHandler;
			}
		}

		#endregion PropChangeHandlers		

	}
	#endregion CardAreaUIElement class

	#region CardAreaScrollRegionUIElement
	
	//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling
	//
	//	Added the CardAreaScrollRegionUIElement, which contains the
	//	RowUIElements and CardLabelAreaUIElement.

	/// <summary>
	/// UIElement that contains RowUIElements for the CardView style.
	/// </summary>
	public class CardAreaScrollRegionUIElement : UIElement
	{
		#region Constructor

		/// <summary>
		/// Creates a new instance of the <see cref="CardAreaScrollRegionUIElement"/> class.
		/// </summary>
		/// <param name="parent">The parent UIElement (see <see cref="CardAreaUIElement"/>)</param>
		public CardAreaScrollRegionUIElement( UIElement parent ) : base ( parent )
		{
		}

		#endregion Constructor

		#region ExtractRowElement

		// SSP 4/12/05 BR03282
		// Added ExtractRowElement method.
		//
		private RowUIElementBase ExtractRowElementHelper( UIElementsCollection elements, UltraGridRow row )
		{
			UIElement elem = RowColRegionIntersectionUIElement.ExtractRowElement( this, elements, row );

			RowUIElementBase rowElem = elem as RowUIElementBase;
			if ( null == rowElem )
			{
				elem = row.CreateRowUIElement( this );

				// MD 8/7/07 - 7.3 Performance
				// FxCop - Do not cast unnecessarily
				//Debug.Assert( null == elem || elem is RowUIElementBase, "Card area ui element currently does not support anything other than a RowUIElementBase derived row element." );
				//rowElem = (RowUIElementBase)elem;
				rowElem = elem as RowUIElementBase;
				Debug.Assert( null == elem || rowElem != null, "Card area ui element currently does not support anything other than a RowUIElementBase derived row element." );	
			}

			row.InitializeUIElement( rowElem );
			return rowElem;
		}

		#endregion // ExtractRowElement

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			Rectangle	rectWork, rectCards;

			// Save the current child elements collection so we can
			// potentially reuse some of the elements.
			UIElementsCollection oldElements = this.ChildElements;

			// Clear the current child elements collection.
			this.childElementsCollection = null;

			//	Get some of the more frequently used properties into stack variables
			Rectangle rectInsideBorders = this.RectInsideBorders;
			CardAreaUIElement cardAreaElement = this.CardAreaUIElement;
			UltraGridBand band = cardAreaElement.Band;

			// Initialize the cards rect to the entire rect.
			rectCards = rectInsideBorders;

			// Subtract out the margins from the cards rect.
			rectCards.Inflate((-1 * band.CardAreaMarginsResolved), (-1 * band.CardAreaMarginsResolved));

			// ---------------------------------------------------------------------
			// Handle the VariableHeight card style in a separate routine.
			// SSP 2/28/03 - Row Layout Functionality
			// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
			// to StandardLabels card style so use the StyleResolved instead of the Style property.
			//
			if ( band.CardSettings.StyleResolved == CardStyle.VariableHeight ||
				// SSP 6/27/02
				// Do that for compressed card view as well.
				// Added below clause in the condition.
				//
				// SSP 2/28/03 - Row Layout Functionality
				// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
				// to StandardLabels card style so use the StyleResolved instead of the Style property.
				//
				CardStyle.Compressed == cardAreaElement.Band.CardSettings.StyleResolved
				)
			{
				// SSP 4/8/03 UWG2115
				// If all the rows are filtered out and there are no visible rows, then allow 
				// skip the logic for laying out compressed/variable-height rows and allow
				// the logic below to execute.
				// Enclosed the code in the if statement. If the number of visible cards is less
				// than 0, then fall through.
				//
				if ( cardAreaElement.TotalVisibleCards > 0 )
				{
					this.PositionElementsVariableHeight(rectCards, oldElements);
					return;
				}
			}

			// ---------------------------------------------------------------------
			// Create a RowUIElement for each visible card.
			//
			// Setup some variables we'll need.
			int				currentCardIndex = cardAreaElement.FirstVisibleRowIndex;
			int				currentRow		 = 0;
			int				currentColumn	 = 0;
			int				i				 = 0;
			int				verticalOffset	 = 0;
			UltraGridRow	row				 = null;
			int				totalRows		 = cardAreaElement.Rows.VisibleRowCount;
			int				cumulativeVerticalOverlaps
											 = 0;

			// Keep track of the width used for each row as we process each card.
			int[]			widthUsed		 = new int[cardAreaElement.TotalRowsInCardArea];
			for (i = 0; i < widthUsed.Length; i++)	{ widthUsed[i] = 0; }

			// SSP 6/7/02
			// CVMS Row filtering. Wrote code for situation where all the rows
			// are filtered out so there is no visible row. But we still want
			// to display the column labels so the user can unfilter.
			// Added if block below and put the already existing code into
			// the else block.
			//
			if ( cardAreaElement.TotalVisibleCards <= 0 
				// SSP 12/16/04 BR00242
				// If the ColHeadersVisible property is set to false then don't display 
				// the card labels. Added below condition.
				//
				&& cardAreaElement.LabelWidth > 0 )
			{
				// If there are not visible rows, then just show the column headers
				// without any row.
				//

					// Try to reuse an element from the old elements list.
					CardLabelAreaUIElement cardLabelAreaElement = (CardLabelAreaUIElement)CardAreaUIElement.ExtractExistingElement(oldElements, typeof(CardLabelAreaUIElement), true);
					if (cardLabelAreaElement == null)
						cardLabelAreaElement = new CardLabelAreaUIElement(this);

					// Initialize the element.
					cardLabelAreaElement.InFirstCardAreaRow = (currentRow == 0);

					// Compute the vertical offset for this card.
					verticalOffset = (cardAreaElement.CardCaptionHeight) +
						(currentRow * (cardAreaElement.CardHeight + cardAreaElement.Band.CardSpacingVertical));

					//	BF 11.2.04	NAS2005 Vol1 - CardView vertical scrolling
					verticalOffset -= cardAreaElement.CardAreaScrollRegionVerticalOffset;

					// Compute the effective border thickness.
					int	borderThickness = 0;
					if (cardAreaElement.Band.CardSettings.ShowCaption == true)
						borderThickness = cardAreaElement.Band.Layout.GetBorderThickness(cardAreaElement.Band.BorderStyleRowResolved);

					// Compute additional adjustment.
					int additionalAdjustment = 0;
					if (cardAreaElement.MergedLabels && 
						cardAreaElement.Band.CardSettings.ShowCaption == false &&
						cardAreaElement.Band.CardSpacingVertical == 0 &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleCardAreaResolved)  &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleRowResolved))
					{
						additionalAdjustment = 1;
					}

					// Setup the element's rect.
					rectWork		 = rectCards;
					rectWork.Y		+= (verticalOffset + borderThickness + additionalAdjustment);
					rectWork.Height	 = (cardAreaElement.CardHeight - cardAreaElement.CardCaptionHeight - borderThickness - additionalAdjustment);
					rectWork.Width	 = cardAreaElement.LabelWidth;

					//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling
					//	We now allow the cards to extend past the bottom of the
					//	element, and display a vertical scrollbar so they can be accessed.
					//
					// Make sure we haven't gone past the bottom of the card area.
					//if (rectWork.Bottom > rectCards.Bottom)
					//	rectWork.Height = rectCards.Bottom - rectWork.Y;

					// Overlap the left edge of the label area element with the cardarea
					// element if the conditions are right.
					bool overlappedLabelArea = false;
					if (cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleHeaderResolved) &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleCardAreaResolved) &&
						cardAreaElement.Band.CardSpacingHorizontal == 0)
					{
						rectWork.X--;
						rectWork.Width++;
						overlappedLabelArea = true;
					}

					// Apply the cumulative vertical overlaps to the label
					// area's rect.
					rectWork.Y -= cumulativeVerticalOverlaps;

					cardLabelAreaElement.Rect = rectWork;
					this.ChildElements.Add(cardLabelAreaElement);

					// JM 01-16-02 Add cardSpacingHorizontal to the width used
					//widthUsed[currentRow]	 += rectWork.Width;
					widthUsed[currentRow]	 += rectWork.Width + cardAreaElement.Band.CardSpacingHorizontal;

					// Subtract out the pixel we added to the width above in the case
					// where we overlapped the label area.
					if (overlappedLabelArea)
						widthUsed[currentRow]--;
			}
			else
			{

				// Process each card, creating them in 'snaking' fashion
				//	i.e.	col1 row1
				//			col1 row2
				//			col1 row3
				//			col2 row1
				//			col2 row2	etc..
				for (i = 0; i < cardAreaElement.TotalVisibleCards; i++)
				{
					// Check to see if we've displayed all cards yet.
					if (currentCardIndex > (totalRows - 1))
						break;

					// SSP 3/25/03 - Row Layout Functionality
					// In row layout designer, display a single row.
					//
					if ( 1 == i && cardAreaElement.RowLayoutDesignerElement )
						break;

					// Get the row we're dealing with.
					row = cardAreaElement.Rows.GetRowAtVisibleIndex(currentCardIndex);

					// Update the cumulative vertical overlap value to reflect
					// any vertical overlap in the current card.
					if ((currentRow == 0  &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleCardAreaResolved) &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleRowResolved) &&
						cardAreaElement.Band.CardSpacingVertical == 0)
						||
						(currentRow == 0  &&
						cardAreaElement.Band.HeaderVisible &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleHeaderResolved) &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleRowResolved) &&
						cardAreaElement.Band.CardSpacingVertical == 0))
					{
						cumulativeVerticalOverlaps++;
					}

					if (currentRow != 0  &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleRowResolved) &&
						cardAreaElement.Band.CardSpacingVertical == 0)

					{
						cumulativeVerticalOverlaps++;
					}

					// If the card style is MergeLabels, and we haven't created any
					// cards yet, create the label area.
					if (cardAreaElement.MergedLabels && widthUsed[currentRow] == 0
						// SSP 12/16/04 BR00242
						// If the ColHeadersVisible property is set to false then don't display 
						// the card labels. Added below condition.
						//
						&& cardAreaElement.LabelWidth > 0 )
					{
						// Try to reuse an element from the old elements list.
						CardLabelAreaUIElement cardLabelAreaElement = (CardLabelAreaUIElement)CardAreaUIElement.ExtractExistingElement(oldElements, typeof(CardLabelAreaUIElement), true);
						if (cardLabelAreaElement == null)
							cardLabelAreaElement = new CardLabelAreaUIElement(this);

						// Initialize the element.
						cardLabelAreaElement.InFirstCardAreaRow = (currentRow == 0);

						// Compute the vertical offset for this card.
						verticalOffset = (cardAreaElement.CardCaptionHeight) +
							(currentRow * (cardAreaElement.CardHeight + cardAreaElement.Band.CardSpacingVertical));

						//	BF 11.2.04	NAS2005 Vol1 - CardView vertical scrolling
						verticalOffset -= cardAreaElement.CardAreaScrollRegionVerticalOffset;

						// Compute the effective border thickness.
						int	borderThickness = 0;
						if (cardAreaElement.Band.CardSettings.ShowCaption == true)
							borderThickness = cardAreaElement.Band.Layout.GetBorderThickness(cardAreaElement.Band.BorderStyleRowResolved);

						// Compute additional adjustment.
						int additionalAdjustment = 0;
						if (cardAreaElement.MergedLabels && 
							cardAreaElement.Band.CardSettings.ShowCaption == false &&
							cardAreaElement.Band.CardSpacingVertical == 0 &&
							cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleCardAreaResolved)  &&
							cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleRowResolved))
						{
							additionalAdjustment = 1;
						}

						// Setup the element's rect.
						rectWork		 = rectCards;
						rectWork.Y		+= (verticalOffset + borderThickness + additionalAdjustment);
						rectWork.Height	 = (cardAreaElement.CardHeight - cardAreaElement.CardCaptionHeight - borderThickness - additionalAdjustment);
						rectWork.Width	 = cardAreaElement.LabelWidth;

						//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling
						//	We now allow the cards to extend past the bottom of the
						//	element, and display a vertical scrollbar so they can be accessed.
						//
						// Make sure we haven't gone past the bottom of the card area.
						//	if (rectWork.Bottom > rectCards.Bottom)
						//		rectWork.Height = rectCards.Bottom - rectWork.Y;

						// Overlap the left edge of the label area element with the cardarea
						// element if the conditions are right.
						bool overlappedLabelArea = false;
						if (cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleHeaderResolved) &&
							cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleCardAreaResolved) &&
							cardAreaElement.Band.CardSpacingHorizontal == 0)
						{
							rectWork.X--;
							rectWork.Width++;
							overlappedLabelArea = true;
						}

						// Apply the cumulative vertical overlaps to the label
						// area's rect.
						rectWork.Y -= cumulativeVerticalOverlaps;

						cardLabelAreaElement.Rect = rectWork;
						this.ChildElements.Add(cardLabelAreaElement);

						// JM 01-16-02 Add cardSpacingHorizontal to the width used
						//widthUsed[currentRow]	 += rectWork.Width;
						widthUsed[currentRow]	 += rectWork.Width + cardAreaElement.Band.CardSpacingHorizontal;

						// Subtract out the pixel we added to the width above in the case
						// where we overlapped the label area.
						if (overlappedLabelArea)
							widthUsed[currentRow]--;
					}

					// Create the RowUIElement for the card.  This is the element that
					// actually contains the card contents (caption, cells and possibly labels).
					//
					// Try to reuse an element from the old elements list.
					// SSP 4/12/05 - BR03282 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// Use the new ExtractRowElementHelper.
					//
					// ------------------------------------------------------------------------------
					
					RowUIElementBase rowElement = this.ExtractRowElementHelper( oldElements, row );
					// ------------------------------------------------------------------------------

					rowElement.InFirstCardAreaCol	= (currentColumn == 0);
					rowElement.InFirstCardAreaRow	= (currentRow == 0);
					rowElement.InLastCardAreaCol	= (currentColumn == (cardAreaElement.TotalColsInCardArea - 1));

					// Compute the vertical offset for this card.
					verticalOffset = (currentRow * (cardAreaElement.CardHeight + cardAreaElement.Band.CardSpacingVertical));

					//	BF 11.2.04	NAS2005 Vol1 - CardView vertical scrolling
					verticalOffset -= cardAreaElement.CardAreaScrollRegionVerticalOffset;

					// Setup the rect.
					rectWork		 = rectCards;
					rectWork.X		+= widthUsed[currentRow];
					rectWork.Width	 = cardAreaElement.CardWidth;
					rectWork.Y		+= verticalOffset;
					rectWork.Height	 = cardAreaElement.CardHeight;

					//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling
					//	We now allow the cards to extend past the bottom of the
					//	element, and display a vertical scrollbar so they can be accessed.
					//
					// Make sure we haven't gone past the bottom of the card area.
					//	if (rectWork.Bottom > rectCards.Bottom)
					//		rectWork.Height = rectCards.Bottom - rectWork.Y;

					// Overlap the left edge of the leftmost card if the conditions are right.
					bool overlappedCardLeftEdge = false;
					if ( (currentColumn == 0  &&
						// SSP 2/28/03 - Row Layout Functionality
						// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
						// to StandardLabels card style so use the StyleResolved instead of the Style property.
						//
						cardAreaElement.Band.CardSettings.StyleResolved == CardStyle.MergedLabels  &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleHeaderResolved) &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleRowResolved) &&
						cardAreaElement.Band.CardSpacingHorizontal == 0)
						||
						(currentColumn == 0  &&
						// SSP 2/28/03 - Row Layout Functionality
						// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
						// to StandardLabels card style so use the StyleResolved instead of the Style property.
						//
						cardAreaElement.Band.CardSettings.StyleResolved != CardStyle.MergedLabels  &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleCardAreaResolved) &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleRowResolved) &&
						cardAreaElement.Band.CardSpacingHorizontal == 0)
						||
						(currentColumn != 0  &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleRowResolved) &&
						cardAreaElement.Band.CardSpacingHorizontal == 0) )
					{
						rectWork.X--;
						rectWork.Width++;
						overlappedCardLeftEdge = true;
					}

					// Overlap the right edge of the rightmost card with the card area if the 
					// conditions are right.
					if (currentColumn == (cardAreaElement.TotalColsInCardArea - 1)  &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleCardAreaResolved) &&
						cardAreaElement.Band.Layout.CanMergeAdjacentBorders(cardAreaElement.Band.BorderStyleRowResolved) &&
						cardAreaElement.Band.CardSpacingHorizontal == 0)

					{
						rectWork.Width++;
					}

					// Apply the cumulative vertical overlaps to the card's
					// rect.
					rectWork.Y		-= cumulativeVerticalOverlaps;

					rowElement.Rect	 = rectWork;
					this.ChildElements.Add(rowElement);

					// Update some variables.
					currentCardIndex++;
					widthUsed[currentRow]	+= (rectWork.Width + cardAreaElement.Band.CardSpacingHorizontal);

					// Subtract out the pixel we added to the width above in the case
					// where we overlapped the card's left edge since we really didn't 
					// use an extra pixel of width (because we also adjusted the X value).
					if (overlappedCardLeftEdge)
						widthUsed[currentRow]--;

					// Bump the row number.  (Reset the row number to zero if we've exceeded the
					// total rows in the card area.  This will force a 'snake' to the top of the
					// next column)
					currentRow++;
					if (currentRow > (cardAreaElement.TotalRowsInCardArea - 1))
					{
						currentRow = 0;
						currentColumn++;
						cumulativeVerticalOverlaps	= 0;
					}
				}
			}
		}

		#endregion PositionChildElements

		#region PositionElementsVariableHeight

		private void PositionElementsVariableHeight(Rectangle rectCards, UIElementsCollection oldElements)
		{
			Rectangle rectWork;

			//	Get some of the more frequently used properties into stack variables

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//Rectangle rectInsideBorders = this.RectInsideBorders;

			CardAreaUIElement cardAreaElement = this.CardAreaUIElement;
			UltraGridBand band = cardAreaElement.Band;

			// Create a RowUIElement for each visible card.
			//
			// Setup some variables we'll need.
			int				currentCardIndex		= cardAreaElement.FirstVisibleRowIndex;
			int				heightUsedCurrentCol	= 0;
			int				cardsCreatedCurrentCol	= 0;
			int				totalRows				= cardAreaElement.Rows.VisibleRowCount;
			UltraGridRow	row						= null;
			int				cumulativeVerticalOverlaps
													= 0;

			// Process each card by card area row within card area column.
			for (int cardAreaCol = 0; cardAreaCol < cardAreaElement.TotalColsInCardArea; cardAreaCol++)
			{
				heightUsedCurrentCol		= 0;
				cardsCreatedCurrentCol		= 0;
				cumulativeVerticalOverlaps	= 0;
				while (currentCardIndex < totalRows)
				{
					// Get the row we're working with.
					row = cardAreaElement.Rows.GetRowAtVisibleIndex(currentCardIndex);

					// Check to see if we can fit the next card.
					if ((rectCards.Top + heightUsedCurrentCol + row.CardHeight) > rectCards.Bottom)
					{
						if (cardsCreatedCurrentCol < 1)
						{
							// We haven't created any cards in this column yet -
							// allow this one card to be created even though it won't
							// be totally visible.
						}
						else
						{
							// Not enough room - need to start a new column.
							break;
						}
					}

					// Create the RowUIElement for the card.  This is the element that
					// actually contains the card contents (caption, cells and possibly labels).
					//
					// Try to reuse an element from the old elements list.
					// SSP 4/12/05 - BR03282 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// Use the new ExtractRowElementHelper.
					//
					// ------------------------------------------------------------------------------
					
					RowUIElementBase rowElement = this.ExtractRowElementHelper( oldElements, row );
					// ------------------------------------------------------------------------------
					
					rowElement.InFirstCardAreaCol = (cardAreaCol == 0);
					rowElement.InFirstCardAreaRow = (cardsCreatedCurrentCol == 0);
					rowElement.InLastCardAreaCol  = (cardAreaCol == (cardAreaElement.TotalColsInCardArea - 1));

					rectWork		 = rectCards;
					rectWork.X		+= (cardAreaCol * (cardAreaElement.CardWidth + band.CardSpacingHorizontal));
					rectWork.Width	 = cardAreaElement.CardWidth;
					rectWork.Y		+= heightUsedCurrentCol;
					rectWork.Height	 = row.CardHeight;

					//	BF 11.1.04	NAS2005 Vol1 - CardView vertical scrolling
					//	We now allow the cards to extend past the bottom of the
					//	element, and display a vertical scrollbar so they can be accessed.
					//
					// Make sure we haven't gone past the bottom of the card area.
					//	if (rectWork.Bottom > rectCards.Bottom)
					//		rectWork.Height = rectCards.Bottom - rectWork.Y;

					// Overlap the left edge of the card and the previous card (or the card
					// area if we're in the first column) if the conditions are right.
					if ( (cardAreaCol == 0  &&
						  band.Layout.CanMergeAdjacentBorders(band.BorderStyleCardAreaResolved) &&
						  band.Layout.CanMergeAdjacentBorders(band.BorderStyleRowResolved) &&
						  band.CardSpacingHorizontal == 0)
												||
						 (cardAreaCol != 0  &&
						  band.Layout.CanMergeAdjacentBorders(band.BorderStyleRowResolved) &&
						  band.CardSpacingHorizontal == 0) )
					{
						rectWork.X--;
						rectWork.Width++;
					}

					// Overlap the top edge of the card if the 
					// conditions are right.
					if ((cardsCreatedCurrentCol == 0  &&
						 band.Layout.CanMergeAdjacentBorders(band.BorderStyleCardAreaResolved) &&
						 band.Layout.CanMergeAdjacentBorders(band.BorderStyleRowResolved) &&
						 band.CardSpacingVertical == 0)
											||
						(cardsCreatedCurrentCol == 0  &&
						 band.HeaderVisible  &&
						 band.Layout.CanMergeAdjacentBorders(band.BorderStyleHeaderResolved) &&
						 band.Layout.CanMergeAdjacentBorders(band.BorderStyleRowResolved) &&
						 band.CardSpacingVertical == 0)
											||
						(cardsCreatedCurrentCol != 0  &&
						 band.Layout.CanMergeAdjacentBorders(band.BorderStyleRowResolved) &&
						 band.CardSpacingVertical == 0))
					{
						cumulativeVerticalOverlaps++;
					}

					// Overlap the right edge of the rightmost card with the card area if the 
					// conditions are right.
					if (cardAreaCol == (cardAreaElement.TotalColsInCardArea - 1)  &&
						band.Layout.CanMergeAdjacentBorders(band.BorderStyleCardAreaResolved) &&
						band.Layout.CanMergeAdjacentBorders(band.BorderStyleRowResolved) &&
						band.CardSpacingHorizontal == 0)

					{
						rectWork.Width++;
					}

					// Apply the cumulative vertical overlaps to the card's
					// rect.
					rectWork.Y		-= cumulativeVerticalOverlaps;

					//	BF 11.3.04	NAS2005 Vol1 - CardView vertical scrolling
					//	Apply the vertical offset
					rectWork.Y -= cardAreaElement.CardAreaScrollRegionVerticalOffset;

					rowElement.Rect	 = rectWork;
					this.ChildElements.Add(rowElement);

					// Update some variables.
					currentCardIndex++;
					cardsCreatedCurrentCol++;
					heightUsedCurrentCol += rectWork.Height + band.CardSpacingVertical;
				}
			}
		}

		#endregion PositionElementsVariableHeight

		#region Overrides

		/// <summary>
		/// Returns whether this element's child elements are clipped
		/// by its bounding rectangle.
		/// </summary>
		protected override bool ClipChildren { get{ return true; } }

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams ){}

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams ){}

		#endregion Overrides

		#region Public properties

			#region CardAreaUIElement
		/// <summary>
		/// Returns the <see cref="CardAreaUIElement"/> that contains this <see cref="CardAreaScrollRegionUIElement"/>.
		/// </summary>
		public CardAreaUIElement CardAreaUIElement
		{
			get{ return this.GetAncestor( typeof(CardAreaUIElement) ) as CardAreaUIElement; }
		}
			#endregion CardAreaUIElement

		#endregion Public properties

	}
	#endregion CardAreaScrollRegionUIElement
}
