#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Collections;
	using System.Collections.Generic;


	internal enum DropLocation
	{
		Before,
		After
	};

    internal class DropLocationInfo
	{		
		protected GridItemBase	gridItem = null;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//protected DropLocation	location;

		protected GroupHeader	groupHeader = null;
		protected int			level;
		protected DropLocation dropLocation;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//protected GroupByBox   groupByBox = null;

		protected UltraGridBand		   groupByBand = null;
		protected int		   groupBySortedColsInsertIndex = -1;
		private   Rectangle dropTargetItemBounds = Rectangle.Empty;
		

		internal DropLocationInfo()
		{
			this.gridItem = null;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.location = DropLocation.Before;

			this.groupHeader    = null;   
			this.level    = -1;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.groupByBox = null;

			this.groupByBand = null;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal DropLocationInfo( GridItemBase gridItem, DropLocation dropLoccation )
		//{
		//    this.gridItem = gridItem;
		//    this.location = dropLoccation;
		//    this.groupHeader    = null;   
		//    this.level    = -1;
		//}

		//internal DropLocationInfo( DropLocation dropLocation, UltraGridGroup group, int level )
		//{
		//    this.gridItem = null;
		//    this.location =  dropLocation;
		//    // SSP 6/13/05
		//    // Whidbey warning.
		//    // 
		//    //this.groupHeader  = groupHeader;   
		//    this.groupHeader = group.Header;

		//    this.level    = level;
		//}

		//internal DropLocationInfo( GroupByBox groupByBox )
		//{
		//    this.gridItem = null;
		//    this.groupByBox = groupByBox;			
		//    this.groupHeader    = null;   
		//    this.level    = -1;
		//}
		
		//internal GroupByBox GroupByBox
		//{
		//    get
		//    {
		//        return this.groupByBox;
		//    }
		//}

		#endregion Not Used

		internal GridItemBase GridItem
		{
			get
			{
				return this.gridItem;
			}
			set
			{
				this.gridItem = value;
			}
		}

		internal int Level
		{
			get
			{
				return this.level;
			}
			set
			{
				this.level = value;
			}
		}

		/// <summary>
		/// if dropping onto a group by band label in group by box,
		/// then this will return the band associated with the band label
		/// column is being dropped on.
		/// </summary>
		internal UltraGridBand GroupByBand
		{
			get
			{
				return this.groupByBand;
			}
			set
			{
				this.groupByBand = value;
			}
		}


		internal int GroupBySortedColsInsertIndex
		{
			get
			{
				return this.groupBySortedColsInsertIndex;
			}
			set
			{
				this.groupBySortedColsInsertIndex = value;
			}
		}

		internal ColumnHeader ColumnHeader
		{
			get
			{
				if ( this.IsDropTargetColumn )
					return (ColumnHeader)this.gridItem;
				else
					return null;
			}
		}

		internal Infragistics.Win.UltraWinGrid.GroupHeader GroupHeader
		{
			get
			{
				if ( this.IsDropTargetGroup )
					return (GroupHeader)this.gridItem;
				else
				{
					if ( null != this.groupHeader )
					{
						return this.groupHeader;
					}
					else if ( this.IsDropTargetColumn )
					{
						// MD 1/21/09 - Groups in RowLayouts
						// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
						//return this.ColumnHeader.Column.Group.Header;
						UltraGridGroup group = this.ColumnHeader.Column.GroupResolved;

						if ( group != null )
							return group.Header;
					}
				}

				return null;
			}
			set
			{
				this.groupHeader = value;
			}
		}

		// MD 1/21/09 - Groups in RowLayouts
		internal HeaderBase Header
		{
			get
			{
				return 
					(HeaderBase)this.ColumnHeader ?? 
					(HeaderBase)this.GroupHeader;
			}
		}

		internal DropLocation DropLocation
		{
			get
			{
				return this.dropLocation;
			}
			set
			{
				this.dropLocation = value;
			}
		}

		internal void SetDropTargetItemBounds( Rectangle rect )
		{
			this.dropTargetItemBounds = rect;
		}

		// SSP 12/21/01
		// If the drop is being made on a card label, this will return true
		// otherwise false.
		//
		internal bool IsDropTargetCardLabel
		{
			get
			{
				if ( !this.IsDropTargetColumn  ||  null == this.ColumnHeader )
					return false;

				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.ColumnHeader.Column.Band.CardView )
				//{
				//    GroupByBox groupByBox = 
				//        this.ColumnHeader.Column.Band.Layout.HasGroupByBox 
				//        ? this.ColumnHeader.Column.Band.Layout.GroupByBox 
				//        : null;
				UltraGridColumn column = this.ColumnHeader.Column;

				if ( column.Band.CardView )
				{
					GroupByBox groupByBox =
						column.Band.Layout.HasGroupByBox
						? column.Band.Layout.GroupByBox
						: null;

					if ( null != groupByBox )
					{
						UIElement groupByBoxElement = groupByBox.UIElement;
						
						if ( null != groupByBoxElement )
						{
							if ( groupByBoxElement.Rect.IntersectsWith( this.dropTargetItemBounds ) )
								return false;
						}
					}

					return true;
				}

				return false;
			}
		}

		internal bool IsDropTargetColumn
		{
			get
			{
				if ( null != this.gridItem )
				{
					if ( gridItem.SelectChangeType == SelectChange.Col )
						return true;
				}

				return false;
			}
		}


		internal bool IsDropTargetGroup
		{
			get
			{
				if ( null != gridItem )
				{
					if ( SelectChange.Group == gridItem.SelectChangeType )
						return true;
				}

				return false;
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal bool IsDropTargetGroupByBandLabel
		//{
		//    get
		//    {
		//        return null != this.GroupByBand;
		//    }
		//}

		#endregion Not Used
		

		internal bool IsDropTargetEmptyLevel
		{
			get
			{
				// SSP 10/10/01
				// We should return true if there is no gridItem not when
				// there is one.
				//
				
				return null == this.gridItem;
			}
		}
	}


	internal abstract class DragStrategy
	{
		internal const int INVALID_DROP_POS = -1;


		protected UltraGrid			grid = null;
		protected UltraGridBand				band = null;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//protected Selected			selected = null;

		protected ColScrollRegion	csr = null;
		protected bool				sameCSR;

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//protected ArrayList			dragItems = null; //array of header base
		protected List<HeaderBase> dragItems = null; //array of header base
							
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal DragStrategy( UltraGridBand band, UltraGrid grid, Selected selected )
		internal DragStrategy( UltraGridBand band, UltraGrid grid )
		{
			this.band = band;
			this.grid = grid;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.selected = selected;	
		
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//this.dragItems = new ArrayList( );
			this.dragItems = new List<HeaderBase>();
		}

		// SSP 7/24/03 - Fixed headers
		// Added below ToHeaderArray method.
		//
		internal static HeaderBase[] ToHeaderArray( SelectedColsCollection headers )
		{
		    // MD 8/10/07 - 7.3 Performance
		    // Use generics
		    //ArrayList list = new ArrayList( headers.Count );
		    List<HeaderBase> list = new List<HeaderBase>( headers.Count );
			
		    for ( int i = 0; i < headers.Count; i++ )
		    {
		        if ( null != headers[i] )
		            list.Add( headers[i] );
		    }

		    // MD 8/10/07 - 7.3 Performance
		    // Use generics
		    //return (HeaderBase[])list.ToArray( typeof( HeaderBase ) );
		    return list.ToArray();
		}

		// SSP 7/24/03 - Fixed headers
		// Added below ToHeaderArray method.
		//
		internal static HeaderBase[] ToHeaderArray( GroupsCollection groups )
		{
		    // MD 8/10/07 - 7.3 Performance
		    // Use generics
		    //ArrayList list = new ArrayList( groups.Count );
		    List<HeaderBase> list = new List<HeaderBase>( groups.Count );
			
		    for ( int i = 0; i < groups.Count; i++ )
		    {
		        if ( null != groups[i] && null != groups[i].Header )
		            list.Add( groups[i].Header );
		    }

		    // MD 8/10/07 - 7.3 Performance
		    // Use generics
		    //return (HeaderBase[])list.ToArray( typeof( HeaderBase ) );
		    return list.ToArray();
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal static ColumnHeader[] ToColumnHeaderArray( UltraGridColumn[] array )
		//{
		//    ColumnHeader[] columnHeaders = new ColumnHeader[ array.Length ];

		//    for ( int i = 0; i < array.Length; i++ )
		//    {
		//        columnHeaders[i] = array[i].Header;
		//    }

		//    return columnHeaders;
		//}

		#endregion Not Used

		internal static GroupHeader[] ToGroupHeaderArray( UltraGridGroup[] array )
		{
		    GroupHeader[] groupHeaders = new GroupHeader[ array.Length ];

		    for ( int i = 0; i < array.Length; i++ )
		    {
		        groupHeaders[i] = array[i].Header;
		    }


		    return groupHeaders;
		}


		internal static ColumnHeader[] ToColumnHeaderArray( object[] array )
		{
		    ColumnHeader[] columnHeaders = new ColumnHeader[ array.Length ];

		    int j = 0;
		    for ( int i = 0; i < array.Length; i++ )
		    {
		        ColumnHeader ch = array[i] as ColumnHeader;
		        UltraGridColumn c = array[i] as UltraGridColumn;

		        if ( null != ch )
		            columnHeaders[j++] = ch;
		        else if ( null != c )
		            columnHeaders[j++] = c.Header;
		        else
		        {
		            Debug.Assert( false, "array[i] is neither a Column or a ColumnHeader" );
		        }
		    }

		    return columnHeaders;
		}

		internal static GroupHeader[] ToGroupHeaderArray( object[] array )
		{
		    GroupHeader[] groupHeaders = new GroupHeader[ array.Length ];

		    int j = 0;
		    for ( int i = 0; i < array.Length; i++ )
		    {
		        GroupHeader gh = array[i] as GroupHeader;
		        UltraGridGroup g = array[i] as UltraGridGroup;

		        if ( null != gh )
		            groupHeaders[j++] = gh;
		        else if ( null != g )
		            groupHeaders[j++] = g.Header;
		        else
		        {
		            Debug.Assert( false, "array[i] is neither a Group or a GroupHeader" );
		        }
		    }

		    return groupHeaders;
		}


		internal abstract bool BeginDrag( HeaderBase headerItem );

		internal virtual bool IsLocationValid( Point point, DropLocationInfo dropLocationInfo, ref Rectangle itemBounds )
		{	
			ColScrollRegion csr = null;
			ColScrollRegionsCollection csrColl = null;

			//	compare the CSR we are dropping onto with the
			//	one the drag was initiated from, and make sure they are the same			
			csrColl = this.grid.DisplayLayout.GetColScrollRegions( false );
			csr = csrColl.GetRegionFromOrigin( point.X );
			
			//	If this is not the same CSR, set a flag that will disallow drop
			// SSP 6/29/05 - NAS 5.3 Column Chooser
			// If a column from a column chooser is being dragged over a grid then we should
			// skip the same col scroll region test.
			// 
			//this.sameCSR = csr == this.csr;
			this.sameCSR = csr == this.csr || null == csr || null == this.csr;

			bool valid = false;

			GridItemBase gridItem = this.GetGridItem( point,  ref itemBounds ) as GridItemBase;

			if ( null == gridItem )
			{
				// Added CDropLocationInfo class to handle Dropping Column's into empty groups
				// as well as empty levels. The IsExtendedLocationValid method is currently
				// only implemented for CDragStrategyColumn, to allow dropping on empty levels.
				
				if ( !this.IsExtendedLocationValid( point, dropLocationInfo, ref itemBounds ) )
					return false;
			}

			if ( null != gridItem )
			{
				if ( !this.IsGridItemValid( gridItem ) )
					return valid;

				dropLocationInfo.GridItem = gridItem;
			}
			

			// Iterate through all the DragItems
			for ( int i = 0; i < this.dragItems.Count; i++ )
			{
				HeaderBase header = this.dragItems[i] as HeaderBase;

				if ( null != header )
					valid =  header.IsValidDropArea( dropLocationInfo, point );
    
				// If the DropArea is not valid for 
				// any one of them the whole operation
				// is not valid.
				if ( !valid )
					break;
			}

			if ( !valid )
				dropLocationInfo.GridItem = null;

			//	Return false so the invalid drop cursor is displayed
			if(! this.sameCSR )
				valid = false;

			return valid;
		}

		internal virtual bool IsExtendedLocationValid( Point point, DropLocationInfo  dropLocationInfo, ref Rectangle itemBounds ) 
		{
			return false;
		}

		internal abstract bool IsDropValid( Point mouseLoc, DropLocationInfo dropLocationInfo, Rectangle itemBounds );
		internal abstract bool Drop( Point point, DropLocationInfo dropLocationInfo, Rectangle itemBounds );

		internal abstract bool CreateDragItemsList( HeaderBase headerItem );
		
		internal virtual void GetDropLocation( DropLocationInfo dropLocationInfo, Point point, Rectangle itemBounds )
		{		
			int midPoint = itemBounds.Left + ( itemBounds.Width / 2 );

			if ( point.X < midPoint )
				dropLocationInfo.DropLocation = DropLocation.Before;
			else
				dropLocationInfo.DropLocation = DropLocation.After;	

			// SSP 7/16/03 - Fixed headers
			// If the target item is scrolled under fixed headers, then set the drop location
			// After as the Before location is scrolled underneath the fixed headers.
			//
			// --------------------------------------------------------------------------------
			if ( dropLocationInfo.IsDropTargetColumn || dropLocationInfo.IsDropTargetGroup )
			{				
				HeaderBase header = dropLocationInfo.IsDropTargetColumn 
					? (HeaderBase)dropLocationInfo.ColumnHeader	: (HeaderBase)dropLocationInfo.GroupHeader;

				if ( null != header )
				{
					ColScrollRegion csr = header.Band.Layout.ActiveColScrollRegion;
					if ( header.Band.IsHeaderScrolledUnderFixedHeaders( csr, header ) )
						dropLocationInfo.DropLocation = DropLocation.After;
				}
			}
			// --------------------------------------------------------------------------------
		}

		// SSP 7/16/03 - Fixed headers
		// Added DragItems property.
		//
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList DragItems
		internal List<HeaderBase> DragItems
		{
			get
			{
				return this.dragItems;
			}
		}

		internal virtual bool IsMultiItemDrag()
		{
			return this.GetItemCount( ) > 1;
		}

		internal virtual HeaderBase GetDragBitmapItem()
		{
			HeaderBase dragBitmapItem = null;

			if ( null != this.dragItems && this.dragItems.Count > 0 )
			{
				

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//dragBitmapItem = this.dragItems[0] as HeaderBase;
				dragBitmapItem = this.dragItems[ 0 ];

				Debug.Assert( null != dragBitmapItem, "All elements of dragItems must be HeaderBase instances" );
			}
    
			return dragBitmapItem;
		}

		
		protected virtual Object GetGridItem( Point point, ref Rectangle bounds )
		{
			UltraGridLayout layout = this.grid.DisplayLayout;

			if ( null == layout )
				return null;

			// Get the main grid element from the layout
			//
			// Pass true into GetUIElement to trigger a verification of the 
			// sub element rects
			//
			UIElement element = layout.GetUIElement( true );			

			if ( null == element )
				return null;
			
			// get the grid item from element
			//
			element = element.ElementFromPoint( point );
			
			
			Object item = null;

			if ( null != element )
				item = ( element.SelectableItem as GridItemBase );

			// SSP 10/11/01 UWG470
			// Dropping a column on empty level will only work if the grid item is
			// set to null because the rest of the logic will try to drop on empty
			// level only if gridItem is null 
			//
			if ( !( item is HeaderBase ) || ( item is BandHeader ) )
				// MD 2/27/09 - TFS14678
				// This prevents groups from being dragged in groups and levels mode. Plus, the
				// original issue this fixed is no longer reproducible without this code, so it 
				// must have been fixed by some other change. This has been removed.
                //// MRS - NAS 9.1 - Groups in RowLayout
                //|| item is GroupHeader)
				item = null;
			
			// return the items rect (if requested)
			//
			if ( null != item )
			{	
				UIElement headerElement = element.GetAncestor( typeof( HeaderUIElement ) );

				if ( null != headerElement )
					element = headerElement;

				if ( null != element )
					bounds = element.Rect;
			}
			else
			{
				bounds = Rectangle.Empty;
			}

			return item;
		}

		protected abstract bool IsGridItemValid( GridItemBase gridItem );

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal int GetNewVisiblePos( HeaderBase headerItem, ArrayList orderedHeaders )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static int GetNewVisiblePos( HeaderBase headerItem, ArrayList orderedHeaders )
		internal static int GetNewVisiblePos( HeaderBase headerItem, List<HeaderBase> orderedHeaders )
		{
		    if ( null == headerItem || null == orderedHeaders )
		        return  INVALID_DROP_POS;

		    int count = 0;
		    int pos = INVALID_DROP_POS;

		    for ( int i = 0; i < orderedHeaders.Count; i++, count++)
		    {
		        if ( orderedHeaders[i] == headerItem )
		        {
		            pos = count;
		            break;
		        }
		    }

		    return pos;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool CompareVisiblePositions( ArrayList newHeaderList, ArrayList existingHeaderList )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static bool CompareVisiblePositions( ArrayList newHeaderList, ArrayList existingHeaderList )
		internal static bool CompareVisiblePositions( List<HeaderBase> newHeaderList, List<HeaderBase> existingHeaderList )
		{
		    if ( null == newHeaderList || null == existingHeaderList )
		        return false;

		    int existingItemIterator;
		    int newItemIterator;

		    // Loop through the Existing and New Items
		    // comparing the VisiblePositions

		    for ( existingItemIterator = 0; existingItemIterator < existingHeaderList.Count; existingItemIterator++ )
		    {
				// MD 8/10/07 - 7.3 Performance
				// Use generics
		        //HeaderBase existingItem = existingHeaderList[ existingItemIterator ] as HeaderBase;
				HeaderBase existingItem = existingHeaderList[ existingItemIterator ];

		        Debug.Assert( null != existingItem, "All elements of existingHeaderList must be HeaderBase instances" );

		        // Find the ExistingItem in New Item
		        for ( newItemIterator = 0; newItemIterator < newHeaderList.Count; newItemIterator++ )
		        {
					// MD 8/10/07 - 7.3 Performance
					// Use generics
		            //HeaderBase newItem = newHeaderList[ newItemIterator ] as HeaderBase;
					HeaderBase newItem = newHeaderList[ newItemIterator ];

		            Debug.Assert( null != newItem, "All elements of newHeaderList must be HeaderBase instances" );

		            // If the items match
		            if ( null != newItem && newItem == existingItem )
		            {
		                // Compare VisiblePositions
		                // MD 7/26/07 - 7.3 Performance
		                // FxCop - Mark members as static
		                //int newVisPos = this.GetNewVisiblePos( existingItem, newHeaderList );
		                int newVisPos = DragStrategy.GetNewVisiblePos( existingItem, newHeaderList );

		                int existingVisPos = existingItem.VisiblePosition;

		                // If they don't match there's no reason to go any further
		                // we are just trying to find if they are exactly the same
		                if ( newVisPos != existingVisPos )
		                {
		                    return false;
		                }
		                // Since we've already found this items match
		                // look at the new loop
		                break;
		            }
		        }

		    }

		    return true;
		}


		internal int GetItemCount()
		{			
			return this.dragItems.Count;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal bool SetInBeforePosChanged( bool newValue )
		//{
		//    // Reset all the InBeforePosChanged flags
		//    for ( int i = 0; i < this.dragItems.Count; i++ )
		//    {
		//        HeaderBase item = this.dragItems[i] as HeaderBase;

		//        Debug.Assert( null != item, "All elements in dragItems must be HeaderBase instances" );

		//        item.SetInBeforePosChanged( newValue );
		//    }
    
		//    return true;    
		//}
	};


	internal class DragStrategyColumn : DragStrategy
	{
		bool isDraggingGroupByButton = false;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal DragStrategyColumn( UltraGridBand band, UltraGrid grid, Selected selected,
		//    bool isDraggingGroupByButton ) 
		//    : base( band, grid, selected )
		internal DragStrategyColumn( UltraGridBand band, UltraGrid grid, bool isDraggingGroupByButton )
			: base( band, grid )
		{			
			this.isDraggingGroupByButton = isDraggingGroupByButton;
		}
	
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal DragStrategyColumn( UltraGridBand band, UltraGrid grid, Selected selected ) : base( band, grid, selected )
		internal DragStrategyColumn( UltraGridBand band, UltraGrid grid )
			: base( band, grid )
		{			
		}

		internal bool IsDraggingGroupByButton
		{
			get
			{
				return this.isDraggingGroupByButton;
			}
		}


		internal override bool BeginDrag( HeaderBase header )
		{
			bool valid = true;

			AllowColMoving allowMove = this.band.AllowColMovingResolved;
	
			//	Ignore the not allowed setting at design time			
			if ( AllowColMoving.NotAllowed == allowMove && 
				! this.band.Layout.Grid.DesignMode ) 				
			{
				valid = false;
			}
			else if ( AllowColMoving.WithinGroup == allowMove )
			{
				this.CreateDragItemsList( header );

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//if ( !this.AreAllColsInSameGroup( this.dragItems ) )
				if ( !DragStrategyColumn.AreAllColsInSameGroup( this.dragItems ) )
				{
					valid = false;
					this.dragItems.Clear();
				}        
			}
			else
			{

				this.CreateDragItemsList( header );
			}

			// SSP 10/8/01 UWG505
			// If dragging a group by button, then return true even if AllowColMoving
			// is set to NotAllowed.
			//
			// SSP 10/10/01 UWG349
			// If AllowGroupByResolved is not True, then don't allow the user
			// to ungroup a group by column.
			//
			//if ( ( header is ColumnHeader ) &&
			//	( this.IsDraggingGroupByButton ||
			//	( ( DefaultableBoolean.True == header.Column.AllowGroupByResolved ) &&
			//	( this.grid.Selected.Columns.Count <= 1 ) ) ) )
			if ( header is ColumnHeader )
			{
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.IsDraggingGroupByButton &&
				//    DefaultableBoolean.True != header.Column.AllowGroupByResolved )
				DefaultableBoolean allowGroupByResolved = header.Column.AllowGroupByResolved;

				if ( this.IsDraggingGroupByButton &&
					DefaultableBoolean.True != allowGroupByResolved )
				{
					return false;
				}

				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( DefaultableBoolean.True == header.Column.AllowGroupByResolved &&
				if ( DefaultableBoolean.True == allowGroupByResolved &&
					(  this.IsDraggingGroupByButton || this.grid.Selected.Columns.Count <= 1 ) )
				{
					this.CreateDragItemsList( header );

					valid = true;
				}
			}

			
			//	Get the CSR that the drag is beginning from, so we can
			//	disallow dropping onto a different CSR
			Point mouseLoc = this.grid.PointToClient( System.Windows.Forms.Cursor.Position );
			ColScrollRegionsCollection csrColl;
			csrColl = this.grid.DisplayLayout.GetColScrollRegions( false );
			csr = csrColl.GetRegionFromOrigin( mouseLoc.X );
	
			//	set flag to true, since we are in the same CSR at this point
			this.sameCSR = true;
	
			return valid;
		}


		private bool GetFirstLastButtonWithBand( 
			UltraGridBand band,
			GroupByBoxUIElement groupByBoxElem,
			out UIElement firstElem,
			out UIElement lastElem
			)
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//UIElement first = this.GetButtonWithBandHelper( typeof( GroupByButtonUIElement ),
			//    band, groupByBoxElem, false );
			//UIElement last = this.GetButtonWithBandHelper( typeof( GroupByButtonUIElement ),
			//    band, groupByBoxElem, true );
			//
			//UIElement firstLabel = this.GetButtonWithBandHelper( typeof( GroupByBandLabelUIElement ),
			//    band, groupByBoxElem, false );
			//UIElement lastLabel = this.GetButtonWithBandHelper( typeof( GroupByBandLabelUIElement ),
			//    band, groupByBoxElem, true );
			UIElement first = DragStrategyColumn.GetButtonWithBandHelper( typeof( GroupByButtonUIElement ),
				band, groupByBoxElem, false );
			UIElement last = DragStrategyColumn.GetButtonWithBandHelper( typeof( GroupByButtonUIElement ),
				band, groupByBoxElem, true );

			UIElement firstLabel = DragStrategyColumn.GetButtonWithBandHelper( typeof( GroupByBandLabelUIElement ),
				band, groupByBoxElem, false );
			UIElement lastLabel = DragStrategyColumn.GetButtonWithBandHelper( typeof( GroupByBandLabelUIElement ),
				band, groupByBoxElem, true );

			UIElement overallFirst = null;
			if ( null != first && null != firstLabel )
				overallFirst = first.Rect.Left < firstLabel.Rect.Left ? first : firstLabel;
			else if ( null != first )
				overallFirst = first;
			else 
				overallFirst = firstLabel;
			
			UIElement overallLast  = null;

			if ( null != last && null != lastLabel )
				overallLast = last.Rect.Right > lastLabel.Rect.Right ? last : lastLabel;
			else if ( null != last )
				overallLast = last;
			else 
				overallLast = firstLabel;

			firstElem = overallFirst;
			lastElem = overallLast;

			//something is wrong with the logic if one of these is wrong
			Debug.Assert( !( null != firstElem && null == lastElem ) );
			Debug.Assert( !( null == firstElem && null != lastElem ) );

			if ( null == firstElem  && null == lastElem && 
				null != band.ParentBand )
			{
				return this.GetFirstLastButtonWithBand( band.ParentBand, groupByBoxElem,
					out firstElem, out lastElem );
			}
			else 
				return null != firstElem && null != lastElem;
		}

		// type can only ba either  GroupByButtonUIElement or GroupByBandUIElement
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private UIElement GetButtonWithBandHelper( Type buttonType, UltraGridBand band, GroupByBoxUIElement groupByBoxElem, bool last )
		private static UIElement GetButtonWithBandHelper( Type buttonType, UltraGridBand band, GroupByBoxUIElement groupByBoxElem, bool last )
		{
			int startIndex = !last ? 0 : groupByBoxElem.ChildElements.Count - 1;
			int endIndex   = !last ? groupByBoxElem.ChildElements.Count - 1 : 0;
			int step	   = !last ? 1 : -1;

			for ( int i = startIndex; 
				i <= Math.Max( startIndex, endIndex ) && 
				i >= Math.Min( startIndex, endIndex ); 
				i += step )
			{
				UIElement elem = groupByBoxElem.ChildElements[i];

				if ( elem.GetType( ) == buttonType )
				{
					if ( elem is GroupByBandLabelUIElement )
					{
						if ( band == ((GroupByBandLabelUIElement)elem).Band )
							return elem;
					}
					else if ( elem is GroupByButtonUIElement )
					{
						if ( band == ((GroupByButtonUIElement)elem).Header.Band )
							return elem;
					}
					else
					{
						Debug.Assert( false, "buttonType can only be either GroupByButtonUIElement or GroupByBandUIElement" );
					}
				}
			}

			return null;
		}


		internal bool GetGridItemInGroupByBoxHelper(
			ColumnHeader dragColumnHeader, // column being dragged
			Point point,
			ref Rectangle bounds,
			out Object item,
			ref DropLocation dropLocation )
		{
			item = null;

			// Get the main grid element from the layout
			//
			// Pass true into GetUIElement to trigger a verification of the 
			// sub element rects
			//
			UIElement element = this.grid.DisplayLayout.GetUIElement( true );			

			if ( null == element )
				return false;
			
			// get the grid item from element
			//
			element = element.ElementFromPoint( point );

			if ( null == element )
				return false;

			element = element.GetAncestor( typeof( GroupByBoxUIElement ) );

			// if not over a group by box, return false
			//
			if ( null == element )
				return false;

			GroupByBoxUIElement groupByBoxElem = (GroupByBoxUIElement)element;


			for ( int i = 0; i < groupByBoxElem.ChildElements.Count; i++ )
			{
				UIElement elem = groupByBoxElem.ChildElements[i];

				if ( elem is GroupByButtonUIElement )
				{
					GroupByButtonUIElement groupByButtonElement = 
						(GroupByButtonUIElement)elem;

					if ( dragColumnHeader.Band !=
						groupByButtonElement.Header.Band )
						continue;

					if ( point.X <= groupByButtonElement.Rect.Right )
					{
						bounds = groupByButtonElement.Rect;
						item = groupByButtonElement.Header;

						if ( point.X < groupByButtonElement.Rect.Left + groupByButtonElement.Rect.Width / 2 )
							dropLocation = DropLocation.Before;
						else
							dropLocation = DropLocation.After;

						return true;
					}
				}
				else if ( elem is GroupByBandLabelUIElement )
				{
					GroupByBandLabelUIElement bandLabelElem = 
						(GroupByBandLabelUIElement)elem;

					if ( dragColumnHeader.Band !=
						bandLabelElem.Band )
						continue;

					if ( point.X <= bandLabelElem.Rect.Right )
					{
						bounds = bandLabelElem.Rect;
						item = bandLabelElem.Band;

						dropLocation = DropLocation.Before;

						return true;
					}
				}
			}

			UIElement soughtElement = null;

			UIElement first = null, last = null;

			this.GetFirstLastButtonWithBand( dragColumnHeader.Band, 
				groupByBoxElem, out first, out last );
		
			if ( null != first && null != last )
			{
				UltraGridBand band = null;

				UIElement tmp = first;

				if ( tmp is GroupByBandLabelUIElement )
				{
					band = 	((GroupByBandLabelUIElement)tmp).Band;
				}
				else if ( tmp is GroupByButtonUIElement )
				{
					band = ((GroupByButtonUIElement)tmp).Header.Band;
				}

				Debug.Assert( null != band,
					"The band must have been set since tmp moust have neen either a GroupByBandLabelUIElement or a GroupByButtonUIElement" );

				if ( band == dragColumnHeader.Band )
				{
					if ( point.X <= first.Rect.Left )
						soughtElement = first;
					else if ( point.X >= last.Rect.Right )
						soughtElement = last;

					if ( soughtElement is GroupByBandLabelUIElement )
					{
						item = ((GroupByBandLabelUIElement)soughtElement).Band;
						dropLocation = DropLocation.Before;
					}
					else if ( soughtElement is GroupByButtonUIElement )
					{
						item = ((GroupByButtonUIElement)soughtElement).Header;
						dropLocation = point.X < ( soughtElement.Rect.X + soughtElement.Rect.Width / 2 )
							? DropLocation.Before :
							DropLocation.After;
					}
				}
				else if ( dragColumnHeader.Band.IsDescendantOfBand( band ) )
				{
					soughtElement = last;
					if ( soughtElement is GroupByBandLabelUIElement )
					{
						item = ((GroupByBandLabelUIElement)soughtElement).Band;
						dropLocation = DropLocation.After;
					}
					else if ( soughtElement is GroupByButtonUIElement )
					{
						item = ((GroupByButtonUIElement)soughtElement).Header;
						dropLocation = DropLocation.After;
					}
				}
				else
				{
					soughtElement = first;
					if ( soughtElement is GroupByBandLabelUIElement )
					{
						item = ((GroupByBandLabelUIElement)soughtElement).Band;
						dropLocation = DropLocation.Before;
					}
					else if ( soughtElement is GroupByButtonUIElement )
					{
						item = ((GroupByButtonUIElement)soughtElement).Header;
						dropLocation = DropLocation.Before;
					}
				}
			}

			if ( null == soughtElement )
				return false;

			bounds = soughtElement.Rect;

			return null != soughtElement;
		}
		

		// SSP 9/24/01
		// overrode this function to take into account dragging column over the 
		// group by box
		protected override Object GetGridItem( Point point, ref Rectangle bounds )
		{
			if ( null != this.dragItems && 1 == this.dragItems.Count && this.dragItems[0] is ColumnHeader )
			{
				Object item = null;

				DropLocation discard = DropLocation.After;

				
				if ( this.GetGridItemInGroupByBoxHelper( (ColumnHeader)this.dragItems[0], point, ref bounds, out item, ref discard ) )
					return item;


				UIElement elem = this.GetGroupByBoxUIElement( );

				if ( null != elem && elem.Rect.Contains( point ) )
					return null;
			}

			return base.GetGridItem( point, ref bounds );			
		}


		private bool IsGroupByBoxLocationValid( Point point, DropLocationInfo dropLocationInfo, ref Rectangle itemBounds )
		{
			if ( null == this.dragItems || 
				 1 != this.dragItems.Count ||
				!( this.dragItems[0] is ColumnHeader ) )
				return false;

			ColumnHeader columnHeader = (ColumnHeader)this.dragItems[0];

			// Since the cursor is over the group by box element, see
			// if the column can be dropped over there for group by feature
			//
			// MD 8/3/07 - 7.3 Performance
			// Cached column - Prevent calling expensive getters multiple times
			//if ( DefaultableBoolean.True != columnHeader.Column.AllowGroupByResolved )
			UltraGridColumn columnHeaderColumn = columnHeader.Column;

			if ( DefaultableBoolean.True != columnHeaderColumn.AllowGroupByResolved )
				return false;


			if ( this.band.Layout.HasGroupByBox )
			{
				UIElement elem = this.band.Layout.GetUIElement( false );
				elem = elem.GetDescendant( typeof( GroupByBoxUIElement ), this.band.Layout.GroupByBox );

				// If the cursor is not over the group by box ui element,
				// then return false
				//
				if ( null == elem || !elem.Rect.Contains( point ) )
					return false;


				UltraGridBand band = columnHeader.Band;

				//object item = this.GetGridItem( point, ref itemBounds );
				object item = null;
				DropLocation dropLocation = DropLocation.After;
				
				if ( this.GetGridItemInGroupByBoxHelper( columnHeader,  point, ref itemBounds, out item, ref dropLocation ) )
				{
					dropLocationInfo.DropLocation = dropLocation;
				}

				UltraGridBand dropBand = item as UltraGridBand;
				ColumnHeader dropColumnHeader = item as ColumnHeader;

				// if header is being dragged over group by box, but it is not
				// over any group by button or group by band label
				//
				UIElement btnElement = null;    
				if ( null == dropBand && null == dropColumnHeader )
				{
					// do it for the previos band if no associated elements for the column's
					// band found because that's where the dragged column is going to go
					// if dropped.
					//
					while ( null != band )
					{								
						btnElement = elem.GetDescendant( typeof( GroupByButtonUIElement ), band );
								
						// If no button from the same band found, then get the band label 
						// 
						if ( null == btnElement && null != band.ParentBand )								
							btnElement = elem.GetDescendant( typeof( GroupByBandLabelUIElement ), band.ParentBand );

						if ( null != btnElement )
							break;						

						band = band.ParentBand;
					}

					if ( null == btnElement )
						btnElement = elem.GetDescendant( typeof( GroupByButtonUIElement ) );
					
					if ( null == btnElement )
						btnElement = elem.GetDescendant( typeof( GroupByBandLabelUIElement ) );

					if ( null != btnElement )
					{
						itemBounds = btnElement.Rect;

						dropLocationInfo.GridItem = null;
						dropLocationInfo.GroupByBand = null;

						dropLocationInfo.DropLocation = DropLocation.After;
							
						if ( btnElement is GroupByButtonUIElement )							
						{
							dropColumnHeader = (ColumnHeader)((GroupByButtonUIElement)btnElement).Header;
							if (dropColumnHeader.Band != columnHeader.Band && dropColumnHeader.Band.IsDescendantOfBand( columnHeader.Band ) )
								dropLocationInfo.DropLocation = DropLocation.Before;
							else
								dropLocationInfo.DropLocation = DropLocation.After;
						}
						else 
						{
							dropBand = ((GroupByBandLabelUIElement)btnElement).Band;
							if ( dropBand == columnHeader.Band || dropBand.IsDescendantOfBand( columnHeader.Band ) )
								dropLocationInfo.DropLocation = DropLocation.Before;
							else
								dropLocationInfo.DropLocation = DropLocation.After;
							dropLocationInfo.DropLocation = DropLocation.Before;
						}
					}
				}
				//else
				//	this.GetDropLocation( dropLocationInfo, point, itemBounds );

				if ( null != dropColumnHeader )
				{
					// if the drop header and the drag header are the same, return
					// false
					//
					if ( columnHeader == dropColumnHeader )
						return false;

					// as long as they are in the same band, retrun true
					//
					if ( columnHeader.Band == dropColumnHeader.Band )
					{
						// SSP 8/6/02 UWG1840
						// If the key is null or "", then IndexOf, Remove, Exists will not work. Instead
						// use the newly added helper method off the SortedColumnsCollection called 
						// GetColumnIndex which tries to search by the column.Index if the key fails.
						//
						// --------------------------------------------------------------------------------
						
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//int currentIndex = columnHeader.Band.SortedColumns.GetColumnIndex( columnHeader.Column );
						int currentIndex = columnHeader.Band.SortedColumns.GetColumnIndex( columnHeaderColumn );

						int newIndex = columnHeader.Band.SortedColumns.GetColumnIndex( dropColumnHeader.Column ) +
							( DropLocation.Before == dropLocationInfo.DropLocation ? 0 : 1 );
						// --------------------------------------------------------------------------------
						
						// if the new index of the column in sorted collection is going to
						// be the same, then return false;
						//
						// SSP 10/5/01
						// We want to not allow the drop if the column is already a group by
						// column and they are dropping at the sames spot it's already at.
						// HOWEVER, we do want to allow the drop if the column is not a group
						// by column ( since it could be in the sorted collection as a non group by
						// column ).
						//
						//if ( newIndex == currentIndex )						
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( newIndex == currentIndex && columnHeader.Column.IsGroupByColumn )
						if ( newIndex == currentIndex && columnHeaderColumn.IsGroupByColumn )
							return false;

						// if the column exists before the newIndex in the sorted cols collection
						// then make sure, it's not the column right before it because if it's
						// right before it, the index of the column won't be changed
						//
						if ( currentIndex >= 0 && currentIndex < newIndex )
							if ( currentIndex == newIndex - 1 )
								return false;

						dropLocationInfo.GroupBySortedColsInsertIndex = newIndex;
						
						dropLocationInfo.GridItem = dropColumnHeader;
						dropLocationInfo.GroupByBand = null;

						return true;
					}
					// if no existing buttons associated with the drag header's band
					// in the group by box, then insert the drag header at 0 index
					// in sorted cols collection
					else
					{
						dropLocationInfo.GroupBySortedColsInsertIndex = 0;
						
						dropLocationInfo.GridItem = dropColumnHeader;
						dropLocationInfo.GroupByBand = null;

						return true;
					}
				}

				if ( null != dropBand )
				{
					// if dropping the header right before the band it belongs
					// to, then return true
					//
					if ( dropBand == columnHeader.Band &&
						DropLocation.Before == dropLocationInfo.DropLocation )
					{
						int newIndex = 1 + dropBand.SortedColumns.LastGroupByColIndex;

						// if the new index of the column in sorted collection is going to
						// be the same, then return false;
						//
						// SSP 8/6/02 UWG1840
						// If the key is null or "", then IndexOf, Remove, Exists will not work. Instead
						// use the newly added helper method off the SortedColumnsCollection called 
						// GetColumnIndex which tries to search by the column.Index if the key fails.
						//
						// SSP 8/8/02 UWG1515
						// We want to not allow the drop if the column is already a group by
						// column and they are dropping at the sames spot it's already at.
						// HOWEVER, we do want to allow the drop if the column is not a group
						// by column ( since it could be in the sorted collection as a non group by
						// column ).
						// Check for column being a group by column. If its not a group by column, then
						// we do want to allow dropping it.
						//
						//if ( newIndex == columnHeader.Band.SortedColumns.IndexOf( columnHeader.Column.Key ) )
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//int currentIndex = columnHeader.Band.SortedColumns.GetColumnIndex( columnHeader.Column );
						//if ( newIndex == currentIndex && columnHeader.Column.IsGroupByColumn )
						int currentIndex = columnHeader.Band.SortedColumns.GetColumnIndex( columnHeaderColumn );
						if ( newIndex == currentIndex && columnHeaderColumn.IsGroupByColumn )
							return false;

						// SSP 8/8/02 UWG1515
						// if the column exists before the newIndex in the sorted cols collection
						// then make sure, it's not the column right before it because if it's
						// right before it, the index of the column won't be changed
						//
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( currentIndex >= 0 && currentIndex < newIndex && currentIndex == newIndex - 1 && 
						//    columnHeader.Column.IsGroupByColumn )
						if ( currentIndex >= 0 && currentIndex < newIndex && currentIndex == newIndex - 1 &&
							columnHeaderColumn.IsGroupByColumn )
							return false;

						dropLocationInfo.GroupBySortedColsInsertIndex = newIndex;

						dropLocationInfo.GridItem = null;
						dropLocationInfo.GroupByBand = dropBand;

						return true;
					}
						

					if ( DropLocation.After == dropLocationInfo.DropLocation &&
						columnHeader.Band.IsDescendantOfBand( dropBand ) )
					{
						// if the new index of the column in sorted collection is going to
						// be the same, then return false;
						//						
						// SSP 8/6/02 UWG1840
						// If the key is null or "", then IndexOf, Remove, Exists will not work. Instead
						// use the newly added helper method off the SortedColumnsCollection called 
						// GetColumnIndex which tries to search by the column.Index if the key fails.
						//
						// SSP 8/8/02 UWG1515
						// We want to not allow the drop if the column is already a group by
						// column and they are dropping at the sames spot it's already at.
						// HOWEVER, we do want to allow the drop if the column is not a group
						// by column ( since it could be in the sorted collection as a non group by
						// column ).
						// Check for column being a group by column. If its not a group by column, then
						// we do want to allow dropping it.
						//
						//if ( 0 == dropBand.SortedColumns.IndexOf( columnHeader.Column.Key ) )
						//if ( 0 == dropBand.SortedColumns.GetColumnIndex( columnHeader.Column ) )
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( 0 == dropBand.SortedColumns.GetColumnIndex( columnHeader.Column ) &&
						//     columnHeader.Column.IsGroupByColumn )								
						if ( 0 == dropBand.SortedColumns.GetColumnIndex( columnHeaderColumn ) &&
							 columnHeaderColumn.IsGroupByColumn )
							return false;

						// here we are droping the column right after the band
						// that is the parent of the dragged column
						//
						dropLocationInfo.GroupBySortedColsInsertIndex = 0;

						dropLocationInfo.GridItem = null;
						dropLocationInfo.GroupByBand = dropBand;
						return true;
					}
					else if ( DropLocation.Before == dropLocationInfo.DropLocation )
					{
						// if the new index of the column in sorted collection is going to
						// be the same, then return false;
						//
						// SSP 8/6/02 UWG1840
						// If the key is null or "", then IndexOf, Remove, Exists will not work. Instead
						// use the newly added helper method off the SortedColumnsCollection called 
						// GetColumnIndex which tries to search by the column.Index if the key fails.
						//
						// SSP 8/8/02 UWG1515
						// We want to not allow the drop if the column is already a group by
						// column and they are dropping at the sames spot it's already at.
						// HOWEVER, we do want to allow the drop if the column is not a group
						// by column ( since it could be in the sorted collection as a non group by
						// column ).
						// Check for column being a group by column. If its not a group by column, then
						// we do want to allow dropping it.
						//
						//if ( 0 == dropBand.SortedColumns.IndexOf( columnHeader.Column.Key ) )
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( 0 == dropBand.SortedColumns.GetColumnIndex( columnHeader.Column ) 
						//     && columnHeader.Column.IsGroupByColumn )
						if ( 0 == dropBand.SortedColumns.GetColumnIndex( columnHeaderColumn )
							 && columnHeaderColumn.IsGroupByColumn )
							return false;

						// here we are droping the column right after the band
						// that is the parent of the dragged column
						//
						dropLocationInfo.GroupBySortedColsInsertIndex = 0;

						dropLocationInfo.GridItem = null;
						dropLocationInfo.GroupByBand = dropBand;
						return true;
					}
				}

				
				// if no items found, then just use the bounds of the whole box
				//
				itemBounds = elem.Rect;		
				dropLocationInfo.GridItem = null;
				dropLocationInfo.GroupByBand = null;

				// if  dropping on the group by box ( and not on buttons or band labels )
				// then set the drop location to before so that the drop guides
				// are shown before the group by box
				//
				dropLocationInfo.DropLocation = DropLocation.Before;			

				return true;							
				
			}

			return false;
		}

		private bool IsGroupByBoxDropValid( Point mouseLoc, DropLocationInfo dropLocationInfo, Rectangle itemBounds )
		{
			if ( !this.IsGroupByBoxLocationValid( mouseLoc, dropLocationInfo, ref itemBounds ) )
				return false;

			// dropping the drag item on itself is not valid
			//
			if ( dropLocationInfo.GridItem == this.dragItems[0] )
				return false;
 
			return true;
		}

		internal GroupByBoxUIElement GetGroupByBoxUIElement( )
		{
			UIElement elem = this.grid.DisplayLayout.GetUIElement( false );

			elem = elem.GetDescendant( typeof( GroupByBoxUIElement ) );

			return elem as GroupByBoxUIElement;
		}
		
		internal override bool IsLocationValid( Point point, DropLocationInfo dropLocationInfo, ref Rectangle itemBounds )
		{
			// SSP 9/21/01
			// If the column is being dragged over the group by box, then check to see
			// if the drop is vaid there.
			//
			if ( this.IsGroupByBoxLocationValid( point, dropLocationInfo, ref itemBounds ) )
				return true;
			
			// SSP 9/27/01
			// Make sure that if the mouse is on the
			// group by box area and IsGroupByBoxLocationValid logic determines
			// that the drop is not valid, return false
			//
			GroupByBoxUIElement groupByBoxElem = this.GetGroupByBoxUIElement( );

			if ( null != groupByBoxElem && groupByBoxElem.Rect.Contains( point ) )
				return false;

			// SSP 10/11/01
			// Moved this up here.
			//
			if ( base.IsLocationValid( point, dropLocationInfo, ref itemBounds ) )
				return true;	

            
			// 10/3/01
			// if dragging a group by button, allow the drop as long as 
			// the drop is being made on a header belonging to the same band
			// as the header associated with the group by button
			//
			if ( this.IsDraggingGroupByButton && 1 == this.dragItems.Count &&
				 ( this.dragItems[0] is ColumnHeader ) )
			{
				ColumnHeader header = this.dragItems[0] as ColumnHeader;

				if ( null != header && null != dropLocationInfo.GridItem &&
					 dropLocationInfo.GridItem.Band == header.Band )
					return true; 
			}
			
			
			// SSP 10/11/01
			// Moved this down from above
			//
			// SSP 10/4/01 UWG488
			// Allow removing group by column by dropping it outside the 
			// grid 
			//
			// SSP 10/8/01
			// Commented out the code to allow removing of group by columns
			// by dragging anywhere outside of the group by box
			//
			//
			if ( this.IsDraggingGroupByButton && 1 == this.dragItems.Count &&
				( this.dragItems[0] is ColumnHeader ) )
			{
				// Set the grid item to null and the itemBounds to empty since
				// the drop is not going to be made on any particular grid item
				// but rather either outside the grid or inside the grid somewhere
				// other than column or group headers.
				//
				dropLocationInfo.GridItem = null;
				itemBounds = Rectangle.Empty;

				// SSP 10/8/01
				//
				// If dr
				//UIElement element = this.grid.DisplayLayout.UIElement;

				//if ( !element.Rect.Contains( point ) )
				//{
				return true;
				//}
			}

			// SSP 6/29/05 - NAS 5.3 Column Chooser
			// 
			// ------------------------------------------------------------------------------
            UltraGridColumnChooser cc = this.grid.GetDraggingColumnChooser( );
			if ( null != cc && this.grid.ClientRectangle.Contains( point ) )
				return true;
			// ------------------------------------------------------------------------------

			return false;
		}


		internal override bool IsDropValid( Point mouseLoc, DropLocationInfo dropLocationInfo, Rectangle itemBounds )
		{
			// SSP 9/21/01
			// if the drop is valid over the group by box, return true
			//
			if ( this.IsGroupByBoxDropValid( mouseLoc, dropLocationInfo, itemBounds ) )
				//&&dropLocationInfo.GridItem != this.dragItems[0] )
				return true;

			// SSP 9/27/01
			// Make sure that if the mouse is on the
			// group by box area and IsGroupByBoxLocationValid logic determines
			// that the drop is not valid, return false
			//
			GroupByBoxUIElement groupByBoxElem = this.GetGroupByBoxUIElement( );

			if ( null != groupByBoxElem && groupByBoxElem.Rect.Contains( mouseLoc ) )
				return false;

			
			// SSP 10/4/01 UWG488
			// Allow removing group by column by dropping it outside the 
			// grid
			//
			// SSP 10/8/01 
			// and also in the grid
			//
			if ( this.IsDraggingGroupByButton && 1 == this.dragItems.Count &&
				( this.dragItems[0] is ColumnHeader ) )
			{
				// SSP 10/8/01
				// Commented out the code to allow removing of group by columns
				// by dragging anywhere outside of the group by box
				//
				//UIElement element = this.grid.DisplayLayout.UIElement;

				//if ( !element.Rect.Contains( mouseLoc ) )
				//{
				return true;
				//}
			}


			// First loop through all the Dragged Columns to see if 
			// any of them are from different Groups. If so, then
			// the drop is always valid, because one of the columns
			// visiblepositions will change.

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//if ( !this.AreAllColsInSameGroup( this.dragItems ) )
			if ( !DragStrategyColumn.AreAllColsInSameGroup( this.dragItems ) )
				return true;

			bool valid = false;

			
			// 10/3/01
			// if dragging a group by button, allow the drop as long as 
			// the drop is being made on a header belonging to the same band
			// as the header associated with the group by button
			//
			if ( this.IsDraggingGroupByButton && 1 == this.dragItems.Count )
			{
				ColumnHeader header = this.dragItems[0] as ColumnHeader;

				if ( null != header && null != dropLocationInfo.GridItem &&
					dropLocationInfo.GridItem.Band == header.Band )
					return true;
			}

			// SSP 6/30/05 - NAS 5.3 Column Chooser
			// Show the drop indicators when dropping a hidden column even if the location 
			// where it's being dropped will cause all the columns to retain their visible 
			// positions.
			// 
			bool atLeastOneItemHidden = false;
			if ( null != this.dragItems )
			{
				foreach ( HeaderBase item in this.dragItems )
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( null != item.Column && ExcludeFromColumnChooser.False == item.Column.ExcludeFromColumnChooserResolved )
					//    atLeastOneItemHidden = item.Column.Hidden;
					UltraGridColumn itemColumn = item.Column;

					if ( null != itemColumn && ExcludeFromColumnChooser.False == itemColumn.ExcludeFromColumnChooserResolved )
                        // MRS 2/23/2009 - Found while fixing TFS14354
                        //atLeastOneItemHidden = itemColumn.Hidden;
                        atLeastOneItemHidden = itemColumn.HiddenResolved;

					if ( atLeastOneItemHidden )
						break;
				}
			}			

			//When the column(s) moving will not effect any column(s) visible position
			if ( dropLocationInfo.IsDropTargetColumn )
			{
				ColumnHeader dropColumnHeader = dropLocationInfo.GridItem as ColumnHeader;

				if ( null == dropColumnHeader )
					return false;

				// Create the OrderedColsClone that reflects the 
				// order of the Columns if the move operation
				// succeeded.
				this.GetDropLocation( dropLocationInfo, mouseLoc, itemBounds );

				bool same = false;

				// MD 1/21/09 - Groups in RowLayouts
				// This should only be done in groups and levels style, not GroupLayout style.
				//if ( null != dropColumnHeader.Column.Group )
				if ( null != dropColumnHeader.Column.Group && 
					this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout )
				{
					
					OrderedDropColumns orderedDropCols = new OrderedDropColumns( dropLocationInfo, this.dragItems );
                
					if ( null == orderedDropCols || !orderedDropCols.Valid )
					{
						if ( null != orderedDropCols )
						{
							orderedDropCols.Dispose( );
							orderedDropCols = null;
						}

						return false;
					}

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//same = this.CompareGroupColVisiblePositions( orderedDropCols, this.dragItems );
					same = DragStrategyColumn.CompareGroupColVisiblePositions( orderedDropCols, this.dragItems );
                        
					orderedDropCols.Dispose( );
					orderedDropCols = null;
				}
				else
				{
					// headers list
					// MD 1/21/09 - Groups in RowLayouts
					// This is no logner needed.
					//int tmp = -1;

					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//ArrayList orderedColsClone = this.CreateNewOrderedCols( dropLocationInfo.ColumnHeader, dropLocationInfo.DropLocation, out tmp );
					// MD 1/21/09 - Groups in RowLayouts
					// The ColumnHeader may be null of you drop in an empty space, so use the Header property, which will return the GroupHeader if the ColumnHeader is null.
					//List<HeaderBase> orderedColsClone = this.CreateNewOrderedCols( dropLocationInfo.ColumnHeader, dropLocationInfo.DropLocation, out tmp );
					List<HeaderBase> orderedColsClone = this.CreateNewOrderedCols( dropLocationInfo.Header, dropLocationInfo.DropLocation );

					if ( null == orderedColsClone )
						return false;

					//Compare pOrderedColsClone with m_DragItems PositionItems
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//same = this.CompareVisiblePositions( orderedColsClone,this.dragItems );
					same = DragStrategy.CompareVisiblePositions( orderedColsClone, this.dragItems );

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.CleanUp( orderedColsClone );
					DragStrategyColumn.CleanUp( orderedColsClone );
				}

				// SSP 6/30/05 - NAS 5.3 Column Chooser
				// Show the drop indicators when dropping a hidden column even if the location 
				// where it's being dropped will cause all the columns to retain their visible 
				// positions.
				// 
				//if ( !same )
				if ( !same || atLeastOneItemHidden )
					valid = true;
			}
			else if ( dropLocationInfo.IsDropTargetGroup ||
				      dropLocationInfo.IsDropTargetEmptyLevel )
			{
				// Added case for Group. So that when group is empty
				// columns can be added. Also added case for empty level.
				GroupHeader  dropGroupHeader = dropLocationInfo.GroupHeader;
        
				if ( null == dropGroupHeader )
					return false;

				// Added check to make sure all cols are in same band.
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//if ( !this.AreAllColsInSameBand( this.dragItems, dropGroupHeader.Band ) )
				if ( !DragStrategyColumn.AreAllColsInSameBand( this.dragItems, dropGroupHeader.Band ) )
					return false;

				this.GetDropLocation( dropLocationInfo, mouseLoc, itemBounds );
        
				bool same = false;

				OrderedDropColumns orderedDropCols = new OrderedDropColumns( dropLocationInfo, this.dragItems );
                
				if ( null == orderedDropCols || !orderedDropCols.Valid )
				{
					if ( null != orderedDropCols )
					{
						orderedDropCols.Dispose( );
						orderedDropCols = null;
					}

					return false;
				}

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//same = this.CompareGroupColVisiblePositions( orderedDropCols, this.dragItems );
				same = DragStrategyColumn.CompareGroupColVisiblePositions( orderedDropCols, this.dragItems );
                    
				orderedDropCols.Dispose( );
				orderedDropCols = null;

				// SSP 6/30/05 - NAS 5.3 Column Chooser
				// Show the drop indicators when dropping a hidden column even if the location 
				// where it's being dropped will cause all the columns to retain their visible 
				// positions.
				// 
				//if ( !same )
				if ( !same || atLeastOneItemHidden )
					valid = true;
			}

			return valid;
		}

		
		private bool DropHelper( Point point, DropLocationInfo dropLocationInfo, Rectangle itemBounds )
		{
			//	Don't allow drops on different CSRs than the one the drag started from
			if( !this.sameCSR )
				return false;

			// Updated Drop to allow dropping on Group Headers
			// as well as empty levels.

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridColumn droppedGroupByColumn = null;
			//
			//if ( null != this.dragItems && this.dragItems.Count > 0 )
			//{
			//    if ( this.dragItems[0] is ColumnHeader )
			//        droppedGroupByColumn = ((ColumnHeader)this.dragItems[0]).Column;
			//    else if ( this.dragItems[0] is UltraGridColumn )
			//        droppedGroupByColumn = (UltraGridColumn)this.dragItems[0];
			//}


			ColumnHeader dropColumnHeader = null;
			GroupHeader  dropGroupHeader  = null;
            
			if ( dropLocationInfo.IsDropTargetColumn )
			{
				dropColumnHeader = dropLocationInfo.ColumnHeader;
                
				if ( null != dropColumnHeader )
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//dropGroupHeader = null != dropColumnHeader.Group ? dropColumnHeader.Group.Header : null;
					UltraGridGroup dropColumnHeaderGroup = dropColumnHeader.Group;

					dropGroupHeader = null != dropColumnHeaderGroup ? dropColumnHeaderGroup.Header : null;
				}
			}
			else
			{
				dropGroupHeader = dropLocationInfo.GroupHeader;
			}

			// Only allow the Drop if the Item
			// that is the drop target is a column
			// or a group
			if (( dropLocationInfo.IsDropTargetColumn ||
				dropLocationInfo.IsDropTargetGroup  
				// SSP 1/28/05 BR02066
				// The drop target must be either a column or a group for this to work. 
				// This lead to an exception however that was caught by the caller.
				// Commented out below condition.
				//
				//|| dropLocationInfo.IsDropTargetEmptyLevel 
				// SSP 8/14/07 BR23568
				// Actually the drop is valid if dropped inside empty area of a group.
				// In such a case the drop target will be neither a column nor a group.
				// 
				// MD 1/21/09 - Groups in RowLayouts
				// We don't want to get in here for GroupLayout style
				//|| ( dropLocationInfo.IsDropTargetEmptyLevel && null != dropGroupHeader ))
				|| ( dropLocationInfo.IsDropTargetEmptyLevel && null != dropGroupHeader && this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout ) )
                // MRS 10/26/07 - BR27875
                && this.IsDropValid(point, dropLocationInfo, itemBounds)
				)
			{			

				// Determine if the Drop is Before or After Item
				this.GetDropLocation( dropLocationInfo, point, itemBounds );

				// Create the OrderedColsClone that reflects the 
				// order of the Columns if the move operation
				// succeeded.
				// MD 1/21/09 - Groups in RowLayouts
				// This is no longer needed.
				//int newDropPos		= INVALID_DROP_POS;

				OrderedDropColumns  orderedDropCols = null;
				SelectedColsCollection  movedColClones = null;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList  orderedColHeadersClone = null; // array list of HeaderBase instances
				List<HeaderBase> orderedColHeadersClone = null; // array list of HeaderBase instances
                                
				if ( null != dropGroupHeader 
                    // MRS - NAS 9.1 - Groups in RowLayout
                    && this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout )
				{
					orderedDropCols = new OrderedDropColumns( dropLocationInfo, this.dragItems );
                    
					if ( null == orderedDropCols || !orderedDropCols.Valid )
					{
						if ( null != orderedDropCols )
						{
							orderedDropCols.Dispose( );
							orderedDropCols = null;
						}
                        
						return false;
					}
                    
					movedColClones = this.CreateMovedColsClone( orderedDropCols, dropColumnHeader );  
                    
					if ( null == movedColClones )
					{
						orderedDropCols.Dispose( );
						orderedDropCols = null;

						return false;
					}
                    
				}
				else
				{
					// MD 1/21/09 - Groups in RowLayouts
					// The ColumnHeader may be null of you drop in an empty space, so use the Header property, which will return the GroupHeader if the ColumnHeader is null.
					//  Also, there is no longer an out parameter on the method.
					//orderedColHeadersClone = this.CreateNewOrderedCols( dropLocationInfo.ColumnHeader, dropLocationInfo.DropLocation, out newDropPos );
					orderedColHeadersClone = this.CreateNewOrderedCols( dropLocationInfo.Header, dropLocationInfo.DropLocation );

					if ( null == orderedColHeadersClone )
						return false;

					// Create the Collection that gets passed into the events.
					movedColClones = this.CreateMovedColsClone( orderedColHeadersClone, dropColumnHeader );

					if ( null == movedColClones )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//this.CleanUp( orderedColHeadersClone );
						DragStrategyColumn.CleanUp( orderedColHeadersClone );

						return false;
					}
				}

				BeforeColPosChangedEventArgs e = new BeforeColPosChangedEventArgs( PosChanged.Moved, DragStrategy.ToColumnHeaderArray( movedColClones.All ) );

				//fire the BeforeColPosChanged event
				this.grid.FireBeforeColPosChanged( e );	

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.SetInBeforePosChanged( false );

				if ( e.Cancel )
				{
					// If the operation was cancelled
					// release the resources
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.CleanUp( orderedColHeadersClone, movedColClones );
					// MD 8/10/07 - 7.3 Performance
					// CleanUp only needs orderedColHeadersClone
					//DragStrategyColumn.CleanUp( orderedColHeadersClone, movedColClones );
					DragStrategyColumn.CleanUp( orderedColHeadersClone );

					if ( null != orderedDropCols )
					{
						orderedDropCols.Dispose( );
						orderedDropCols = null;
					}

					return false;
				}

				if ( null != dropGroupHeader 
                    // MRS - NAS 9.1 - Groups in RowLayout
                    && this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout )
				{
					dropGroupHeader.Group.SetOrderedCols( orderedDropCols );
				}
				else
				{
					// SSP 7/23/03 - Fixed headers
					// Added movedHeaderClones parameter which contains the cloned headers that were moved
					// and the drag stratergy passed them in the event args of Before Col/Group Pos Changed
					// event. It contains the information on whether those headers should be fixed or unfixed.
					//
					//this.band.SetOrderedCols( orderedColHeadersClone );
					this.band.SetOrderedCols( orderedColHeadersClone, DragStrategy.ToHeaderArray( movedColClones ) );
				}

				// Create a new selected cols collection containing the 
				// actual (not cloned) columns that were moved for the 'after'
				// event
				//
				SelectedColsCollection movedCols = new SelectedColsCollection( );
					
				try
				{
					// copy all 'cloned from' (base) columns into the collection
					//
					for ( int i =  0; i < movedColClones.Count; i++ )
					{
						ColumnHeader header = movedColClones.GetItem( i ) as ColumnHeader;

						Debug.Assert( null != header, "All elements in movedColClones must be ColumnHeader instances" );

						if ( null == header ) 
							continue;

						// SSP 6/27/05 BR04687
						// As the above comment suggests, we need to get the original columns here.
						// 
						// ------------------------------------------------------------------------
						UltraGridColumn clonedFromCol = header.Column.ClonedFromColumn;
						if ( null != clonedFromCol && null != clonedFromCol.Header )
							header = clonedFromCol.Header;
						// ------------------------------------------------------------------------
							
						movedCols.InternalAdd( header );							
					}
				}
				catch( Exception )
				{
					Debug.Assert( false, "DragStrategyColumn.Drop failed" );
					movedCols = null;
				}

				AfterColPosChangedEventArgs eArgs = new AfterColPosChangedEventArgs( PosChanged.Moved, DragStrategy.ToColumnHeaderArray( null != movedCols.All ? movedCols.All : movedColClones.All ) );
				
				// fire the after event and clean up
				this.grid.FireAfterColPosChanged( eArgs );	

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.CleanUp( orderedColHeadersClone, movedColClones);
				// MD 8/10/07 - 7.3 Performance
				// CleanUp only needs orderedColHeadersClone
				//DragStrategyColumn.CleanUp( orderedColHeadersClone, movedColClones );
				DragStrategyColumn.CleanUp( orderedColHeadersClone );
                
				if ( null != orderedDropCols )
				{
					orderedDropCols.Dispose( );
					orderedDropCols = null;
				}

				// release the temp selected cols collection
				//
				if ( null != movedCols )
				{
					movedCols.Dispose( );							
				}

				// SSP 6/30/05 - NAS 5.3 Column Chooser
				// 
				return true;
			}

			// SSP 6/30/05 - NAS 5.3 Column Chooser
			// Only return true if successful.
			// 
			//return true;
			return false;
		}

		internal override bool Drop( Point point, DropLocationInfo dropLocationInfo, Rectangle itemBounds )
		{		
			ColumnHeader header = null;

			// SSP 10/4/01 UWG488
			// Allow removing group by column by dropping it outside the 
			// grid
			//
			if ( this.IsDraggingGroupByButton && 1 == this.dragItems.Count &&
				( this.dragItems[0] is ColumnHeader ) )
			{
				UIElement element = this.grid.DisplayLayout.UIElement;

				if ( !element.Rect.Contains( point ) )
				{
					header = this.dragItems[0] as ColumnHeader;

					// setting it to false will do the trick
					//
					// SSP 1/2/03 
					// Changed the property into an internal method prompting this
					// change
					//
					//header.Column.IsGroupByColumn = false;
					header.Column.SetGroupByColumnStatus( false );

					return true;
				}
			}




			// SSP 9/21/01
			// if the drop is valid over the group by box, return true
			//
			if ( this.IsGroupByBoxDropValid( point, dropLocationInfo, itemBounds ) &&
				dropLocationInfo.GridItem != this.dragItems[0] )				
			{
				ColumnHeader columnHeader = this.dragItems[0] as ColumnHeader;

				Debug.Assert( null != columnHeader, "IsGroupByBoxDropValid returned true, but there was " +
					"no column header being dragged !" );

				if ( null != columnHeader )
				{
					UltraGridColumn column = columnHeader.Column;

					int index = dropLocationInfo.GroupBySortedColsInsertIndex;

					if ( index < 0 )					
						index = Math.Max( 0, column.Band.SortedColumns.LastGroupByColIndex );
					
					column.Band.SortedColumns.Insert( index, column, true );

					// SSP 6/30/05 - NAS 5.3 Column Chooser
					// Store the necessary information about the drop so the column chooser drop logic
					// can make use of it.
					// 
					if ( null != this.grid.cachedColumnChooserDragInfo )
						this.grid.cachedColumnChooserDragInfo.droppedOverGroupByBox = DefaultableBoolean.True;

					return true;
				}
			}

			

			header = this.dragItems[0] as ColumnHeader;


			// SSP 10/8/01
			// Implemented code to allow grouping by a column even if the
			// AllowColMoving is set to NotAllowed
			//
			//if ( this.DropHelper( point, dropLocationInfo, itemBounds ) )
			bool ret = false;
			try
			{
				if ( AllowColMoving.NotAllowed != header.Band.AllowColMovingResolved )
					ret = this.DropHelper( point, dropLocationInfo, itemBounds );
			}
			catch ( Exception )
			{
			}

			// SSP 6/30/05 - NAS 5.3 Column Chooser
			// Store the necessary information about the drop so the column chooser drop logic
			// can make use of it.
			// 
			if ( null != this.grid.cachedColumnChooserDragInfo && this.IsDraggingGroupByButton && ret )
				this.grid.cachedColumnChooserDragInfo.groupByButtonDroppedOverColumnHeaders = DefaultableBoolean.True;

			if ( ( AllowColMoving.NotAllowed == header.Band.AllowColMovingResolved ) ||
				  ret )
			{
				// SSP 9/10/01
				// If dragging a group by button, set the IsGroupByColumn
				// property to false on the column
				//
				if ( this.IsDraggingGroupByButton )
				{
					if ( null != header )
					{
						// SSP 1/2/03 
						// Changed the property into an internal method prompting this
						// change
						//
						//header.Column.IsGroupByColumn = false;
						header.Column.SetGroupByColumnStatus( false );
					}
				}

				return true;
			}
	
			// SSP 10/3/01
			// If above drag was unsuccessfull and we are dragging a group by button
			// then allow the drop as long as 
			// the drop is being made on a header belonging to the same band
			// as the header associated with the group by button
			//
			if ( this.IsDraggingGroupByButton && 1 == this.dragItems.Count &&
				 ( this.dragItems[0] is ColumnHeader ) )
			{	
				// SSP 10/8/01
				// Commented below if condition out to allow for removing of
				// a group by column by dragging and dropping it anywhere in
				// the grid as long as it's not in the group by box area
				//
				//if ( null != header && null != dropLocationInfo.GridItem &&
				//	dropLocationInfo.GridItem.Band == header.Band )
				//{
					// setting it to false will do the trick
					//

				UIElement element = this.grid.DisplayLayout.UIElement;
				element = element.GetDescendant( typeof( GroupByBoxUIElement ) );

				if ( null == element || !element.Contains( point ) )
				{
					// SSP 1/3/02
					// Changed the property into an internal method prompting this
					// change
					//
					//header.Column.IsGroupByColumn = false;
					header.Column.SetGroupByColumnStatus( false );
					return true;
				}
				//}
			}

			return false;
		}

		internal override bool CreateDragItemsList( HeaderBase headerItem )
		{
			//Debug.WriteLine( "DragStrategyColumn.CreateDragItemsList called" );

			dragItems.Clear( );

			// If we are dragging a group by column header, then don't
			// add the selected columns below in the else statemenet
			if ( this.IsDraggingGroupByButton )
			{
				this.dragItems.Add( headerItem );
			}
			else
			{
				// drag the selected columns

				HeadersCollection headerList = this.band.OrderedHeaders;
    
				if ( null != headerList )
				{
					for ( int i = 0; i < headerList.Count; i++ )
					{
						HeaderBase header = headerList[i];

						if ( header.IsGroup )
						{
							GroupHeader groupHeader = header as GroupHeader;
						
							// Get the Groups Columns Collection
							// which is ordered
							//GroupColumnsCollection groupCols = groupHeader.Group.Columns; // ->GetGroupColsColl(false);
							IEnumerable<UltraGridColumn> groupCols = groupHeader.Group.ColumnsResolved;
                     
							if ( null != groupCols )
							{
	
								// Loop through and remove any of the Columns
								// that are in this Group
								// MD 1/21/09 - Groups in RowLayouts
								// The colList is now an IEnumarable<UltraGridColumn> , so we must use a foreach.
								//for ( int j = 0; j < groupCols.Count; j++ )
								//{
								//
								//    if ( groupCols[j].Header.Selected )
								//        this.dragItems.Add( groupCols[j].Header );                             
								//}
								foreach ( UltraGridColumn column in groupCols )
								{
									if ( column.Header.Selected )
										this.dragItems.Add( column.Header );
								}
                         
							}
                         
						}
						else
						{
							if ( header.Selected )
							{
								// MD 8/10/07 - 7.3 Performance
								// Use generics
								//this.dragItems.Add( header );
								ColumnHeader columnHeader = header as ColumnHeader;
								Debug.Assert( columnHeader != null );
								this.dragItems.Add( columnHeader );
							}
						}
					}
        
				}

				if ( 0 == this.dragItems.Count )
					this.dragItems.Add( headerItem );
			}

			return true;
		}

		// SSP 10/11/01 UWG470
		// We are supposed to be overridding the base class' IsExtendedLocationValid method
		//
		//internal virtual bool IsExtendedLocationValid( Point point, DropLocationInfo dropLocationInfo, ref Rectangle itemBounds )
		internal override bool IsExtendedLocationValid( Point point, DropLocationInfo dropLocationInfo, ref Rectangle itemBounds )
		{
			if ( dropLocationInfo.IsDropTargetColumn )
			{
				if ( null != dropLocationInfo.ColumnHeader )
				{
					UltraGridBand band = dropLocationInfo.ColumnHeader.Band;

					//  Added check to make sure all cols are in same band.
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//if ( !this.AreAllColsInSameBand( this.dragItems, band ) )
					if ( !DragStrategyColumn.AreAllColsInSameBand( this.dragItems, band ) )
						return false;
				}
			}
			else if ( dropLocationInfo.IsDropTargetGroup ||
				dropLocationInfo.IsDropTargetEmptyLevel )
			{
				if ( null != dropLocationInfo.GroupHeader )
				{
					UltraGridBand band = dropLocationInfo.GroupHeader.Band;

					// Added check to make sure all cols are in same band.
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//if ( !this.AreAllColsInSameBand( this.dragItems, band ) )
					if ( !DragStrategyColumn.AreAllColsInSameBand( this.dragItems, band ) )
						return false;
				}
     
			}    

			return this.GetExtendedDropLocation( point, dropLocationInfo, ref itemBounds );
		}

		
		protected override bool IsGridItemValid( GridItemBase gridItem )
		{
			// Added Group as being a valid drop area.
			if ( null != gridItem && ( SelectChange.Col == gridItem.SelectChangeType ||
				 SelectChange.Group == gridItem.SelectChangeType ) )
				return true;
			else
				return false;
		}

		internal override void GetDropLocation( DropLocationInfo dropLocationInfo, Point point, Rectangle itemBounds )
		{
			
			// see if the point is over the group by box and if so
			// then set appropriate drop location
			//
			if ( this.band.Layout.HasGroupByBox )
			{
				UIElement elem = this.band.Layout.GetUIElement( false );
				elem = elem.GetDescendant( typeof( GroupByBoxUIElement ), this.band.Layout.GroupByBox );

				// If the cursor is not over the group by box ui elemnt,
				// then do the default processing for column dragging
				if ( null != elem && elem.Rect.Contains( point ) )
				{
					// just use the previousely assigned DropLocation
					//					
					return;
				}
			}
		
			
			GridItemBase gridItem = dropLocationInfo.GridItem;

			if ( null != gridItem && SelectChange.Group == gridItem.SelectChangeType )
			{
				dropLocationInfo.DropLocation = DropLocation.Before;

				// SSP 7/16/03 - Fixed headers
				// If the target item is scrolled under fixed headers, then set the drop location
				// After as the Before location is scrolled underneath the fixed headers.
				//
				// --------------------------------------------------------------------------------
				HeaderBase header = dropLocationInfo.GroupHeader;

				if ( null != header )
				{
					ColScrollRegion csr = header.Band.Layout.ActiveColScrollRegion;
					if ( header.Band.IsHeaderScrolledUnderFixedHeaders( csr, header ) )
						dropLocationInfo.DropLocation = DropLocation.After;
				}
				// --------------------------------------------------------------------------------
			}
			else
			{
				// SSP 12/21/01
				// If dropping on a card label, the we are going to be showing the
				// drop guides horizontally.
				//
				if ( dropLocationInfo.IsDropTargetCardLabel )
				{
					int midPoint = itemBounds.Top + ( itemBounds.Height / 2 );

					if ( point.Y < midPoint )
						dropLocationInfo.DropLocation = DropLocation.Before;
					else
						dropLocationInfo.DropLocation = DropLocation.After;
				}
				else
				{
					int midPoint = itemBounds.Left + ( itemBounds.Width / 2 );

					if ( point.X < midPoint )
						dropLocationInfo.DropLocation = DropLocation.Before;
					else
						dropLocationInfo.DropLocation = DropLocation.After;				

					// SSP 7/16/03 - Fixed headers
					// If the target item is scrolled under fixed headers, then set the drop location
					// After as the Before location is scrolled underneath the fixed headers.
					//
					// --------------------------------------------------------------------------------
					if ( dropLocationInfo.IsDropTargetColumn || dropLocationInfo.IsDropTargetGroup )
					{				
						HeaderBase header = dropLocationInfo.IsDropTargetColumn 
							? (HeaderBase)dropLocationInfo.ColumnHeader	: (HeaderBase)dropLocationInfo.GroupHeader;

						if ( null != header )
						{
							ColScrollRegion csr = header.Band.Layout.ActiveColScrollRegion;
							if ( header.Band.IsHeaderScrolledUnderFixedHeaders( csr, header ) )
								dropLocationInfo.DropLocation = DropLocation.After;
						}
					}
					// --------------------------------------------------------------------------------
				}
			}
		}

		// returns array list of HeaderBase instances
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList CreateNewOrderedCols( ColumnHeader dropColumnHeader, DropLocation dropLocation, out int newDropPos )
		// MD 1/21/09 - Groups in RowLayouts
		// We may not always have a column header. Changed the ColumnHeader parameter to type HeaderBase and renmed it.
		// Also, the out param was never used by either caller, so it was removed.
		//internal List<HeaderBase> CreateNewOrderedCols( ColumnHeader dropColumnHeader, DropLocation dropLocation, out int newDropPos )
		internal List<HeaderBase> CreateNewOrderedCols( HeaderBase dropHeader, DropLocation dropLocation )
		{
			int i;

			// MD 1/21/09 - Groups in RowLayouts
			// This parameter has been removed.
			//newDropPos = INVALID_DROP_POS;

			// If we are in Group Mode
			// MD 1/21/09 - Groups in RowLayouts
			// The dropColumnHeader parameter was renamed.
			//if ( null != dropColumnHeader.Group 
			if ( null != dropHeader.Group 
                // MRS - NAS 9.1 - Groups in RowLayout
                && this.band.RowLayoutStyle == RowLayoutStyle.None)
			{
				Debug.Assert( false, "CreateNewOrderedCols should not be called when Groups are involved." );
				return null;
			}

			// This logic simulates what happens after the move
			// and the new VisiblePosition of the Column is set.
			// array list of HeaderBase instances
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList orderedColsClone = this.band.OrderedColsClone;
			List<HeaderBase> orderedColsClone = this.band.OrderedColsClone;

			// MD 1/21/09 - Groups in RowLayouts
			// We can't do anything when the columns were dropped on a group header or empty area, so just return the original collection.
			if ( this.band.RowLayoutStyle == RowLayoutStyle.GroupLayout && dropHeader is GroupHeader )
				return orderedColsClone;

			int dropPos = INVALID_DROP_POS;

			// First Remove the Cols that are moving
			if ( null != orderedColsClone )
			{				
				for ( i = 0; i < this.dragItems.Count; i++ )
				{
					try
					{
						// First Remove the Column Begin Moved
						orderedColsClone.Remove( this.dragItems[ i ] );
					}
					catch ( Exception )
					{
					}
				}

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//dropPos = this.GetNewVisiblePos( dropColumnHeader, orderedColsClone );
				// MD 1/21/09 - Groups in RowLayouts
				// The dropColumnHeader has been renamed.
				//dropPos = DragStrategy.GetNewVisiblePos( dropColumnHeader, orderedColsClone );
				dropPos = DragStrategy.GetNewVisiblePos( dropHeader, orderedColsClone );
         
				// If the DropPos is Not Valid, then add any columns that
				// were removed and release all resources.
				if ( INVALID_DROP_POS == dropPos )
				{
					for ( i = 0; i < this.dragItems.Count; i++ )
					{
						orderedColsClone.Add( this.dragItems[i] );
					}

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.CleanUp( orderedColsClone );
					DragStrategyColumn.CleanUp( orderedColsClone );

					return null;
				}
         
				// This should be after the check for INVALID_DROP_POS above.
				if ( DropLocation.After == dropLocation )
					++dropPos;

				i = dropPos;			

				for ( int itemInsertIterator = 0; itemInsertIterator < this.dragItems.Count; itemInsertIterator++ )
				{
					orderedColsClone.Insert( i++, this.dragItems[ itemInsertIterator ] );
				}

			}

			// MD 1/21/09 - Groups in RowLayouts
			// This parameter has been removed.
			//newDropPos = dropPos;

			return orderedColsClone;
		}

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal SelectedColsCollection CreateMovedColsClone( ArrayList orderedColHeadersClone, ColumnHeader dropColumnHeader )
		internal SelectedColsCollection CreateMovedColsClone( List<HeaderBase> orderedColHeadersClone, ColumnHeader dropColumnHeader )
		{
			if ( null == orderedColHeadersClone )
				return null;

			PositionInfo dropPosInfo = new PositionInfo( );

			// MD 1/21/09 - Groups in RowLayouts
			// We don't need to populate the dropPosInfo in GroupLayout style.
			//dropPosInfo = dropColumnHeader.Column.PositionInfo;
			if ( this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout )
				dropPosInfo = dropColumnHeader.Column.PositionInfo;

			SelectedColsCollection movedCols = new SelectedColsCollection(  );
    
			if ( null != movedCols )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.SetInBeforePosChanged( true );
        		
        
				// For Each column being dragged, set its
				// new position info and add to collection.      
				for ( int i = 0; i < this.dragItems.Count; i++ )
				{
					ColumnHeader columnHeader = this.dragItems[i] as ColumnHeader;

					Debug.Assert( null != columnHeader, "All elements in dragItems must be ColumnHeader instances" );

					if ( null != columnHeader )
					{
						PositionInfo newPosInfo = new PositionInfo( );

						// Get the Position Info from the Column object.
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//newPosInfo = columnHeader.Column.PositionInfo;
						UltraGridColumn columnHeaderColumn = columnHeader.Column;

						newPosInfo = columnHeaderColumn.PositionInfo;

						// Set the Position Info to reflect the columns
						// position if the move operation was successful
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//newPosInfo.VisiblePos = (short)this.GetNewVisiblePos( columnHeader, orderedColHeadersClone );
						newPosInfo.VisiblePos = (short)DragStrategy.GetNewVisiblePos( columnHeader, orderedColHeadersClone );

						// MD 1/21/09 - Groups in RowLayouts
						// We don't need to copy group and level info in GroupLayout style.
						//newPosInfo.Level = dropPosInfo.Level;
						//newPosInfo.Group = dropPosInfo.Group;
						if ( this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout )
						{
							newPosInfo.Level = dropPosInfo.Level;
							newPosInfo.Group = dropPosInfo.Group;
						}
                
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//UltraGridColumn cloneCol = columnHeader.Column.Clone( PosChanged.Moved );
						UltraGridColumn cloneCol = columnHeaderColumn.Clone( PosChanged.Moved );
        
						if ( null != cloneCol )
						{
							// Set the Position Info on the clone to
							// where it would be if the move succeeds.
							cloneCol.SetPositionInfo( newPosInfo );
							
							// Insert the Clone into the collection.
							movedCols.InternalAdd( cloneCol.Header ); 

							// SSP 7/24/03 - Fixed headers
							// If the headers are being dropped over fixed headers area, then set their
							// fixed property to true. The reason for doing this is that when the before
							// cols moved event gets fired, the fixed properties of the cloned guys will
							// be true.
							//
							// --------------------------------------------------------------------------
							if ( null != dropColumnHeader && null != cloneCol.Header )
							{
								if ( dropColumnHeader.FixedResolved )
								{
									cloneCol.Header.InternalSetFixed( true );
									
									// MD 12/9/08 - 9.1 - Column Pinning Right
									cloneCol.Header.InternalSetFixOnRight( dropColumnHeader.FixOnRight );
								}
								else
									cloneCol.Header.InternalSetFixed( false );
							}
							// --------------------------------------------------------------------------
						}
					}
				}
			}

			return movedCols;
		}

		internal SelectedColsCollection CreateMovedColsClone( OrderedDropColumns orderedDropCols, ColumnHeader dropColumn )
		{
			if ( null == orderedDropCols )
				return null;

			SelectedColsCollection movedCols = new SelectedColsCollection( );
    
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//// Set the BeforePoschanged flag.
			//this.SetInBeforePosChanged( true );

  
			// For Each column being dragged, set its
			// new position info and add to collection.
			for ( int i = 0; i < this.dragItems.Count; i++ )
			{
				ColumnHeader columnHeader = this.dragItems[i] as ColumnHeader;

				Debug.Assert( null != columnHeader, "All elements in dragItems must be ColumnHeader instances" );

				if ( null != columnHeader )
				{        
					// Get the Position Info from the OrderedDropCols object.
					// this gives us the columns new position if the drop
					// was successful.
					PositionInfo newPosInfo = orderedDropCols.GetPositionInfo( columnHeader );

					// Clone the column
					UltraGridColumn cloneCol = columnHeader.Column.Clone( PosChanged.Moved );

					if ( null != cloneCol )
					{
						// Set the Position Info on the clone to
						// where it would be if the move succeeds.
						cloneCol.SetPositionInfo( newPosInfo );
						// Insert the Clone into the collection.
						movedCols.InternalAdd( cloneCol.Header ); //-1, pCloneCol, true);

						// SSP 7/24/03 - Fixed headers
						// If the headers are being dropped over fixed headers area, then set their
						// fixed property to true. The reason for doing this is that when the before
						// cols moved event gets fired, the fixed properties of the cloned guys will
						// be true.
						//
						// --------------------------------------------------------------------------
						if ( null != dropColumn && null != cloneCol.Header )
						{
							if ( dropColumn.FixedResolved )
								cloneCol.Header.InternalSetFixed( true );
							else
								cloneCol.Header.InternalSetFixed( false );
						}
						// --------------------------------------------------------------------------
					}                       
				}
			}

			return movedCols;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal int GetNewVisiblePosInGroup( ColumnHeader columnHeader, ArrayList orderedColumnHeaders )
		//{
		//    if ( null == columnHeader || null == orderedColumnHeaders )
		//        return -1;

		//    int     visiblePosition    = -1; // init to -1 in case column isn't found
		//    int     itemLevel;
		//    int     currLevel          = -1; // init to -1 to trigger first time logic 
		//    int     levelPosition      = 0;


		//    // iterate over the columns collection looking for the passed in
		//    // column
		//    //
		//    for ( int i = 0; i < orderedColumnHeaders.Count; i++ )
		//    {
		//        ColumnHeader currentColHeader = orderedColumnHeaders[i] as ColumnHeader;

		//        Debug.Assert( null != currentColHeader, "All members of orderedColumnHeaders must be  instances" );

		//        if ( null == currentColHeader )
		//            return -1;

		//        // get the item's level number
		//        //
		//        itemLevel = currentColHeader.Column.Level;

		//        if ( currLevel < 0 )
		//        {
		//            // first time thru just initialize current level
		//            //
		//            currLevel      = itemLevel;
		//        }
		//        else if ( currLevel != itemLevel )
		//        {
		//            Debug.Assert( currLevel < itemLevel, "Level numbers should always go up in a group's columns");

		//            // The level has changed so keep track of the new level and
		//            // reset the visible position to 0
		//            //
		//            currLevel      = itemLevel;
		//            levelPosition  = 0;
		//        }
		//        else
		//        {
		//            // we are on the same level so just increment the visible 
		//            // position
		//            //
		//            levelPosition++;
		//        }

		//        if ( currentColHeader == columnHeader )
		//        {
		//            visiblePosition = levelPosition;
		//            break;
		//        }

		//    }

		//    return visiblePosition;
		//}

		#endregion Not Used

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool CompareGroupColVisiblePositions( OrderedDropColumns newPos, ArrayList existingPosHeadersList )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static bool CompareGroupColVisiblePositions( OrderedDropColumns newPos, ArrayList existingPosHeadersList )
		internal static bool CompareGroupColVisiblePositions( OrderedDropColumns newPos, List<HeaderBase> existingPosHeadersList )
		{
			if ( null == newPos || null == existingPosHeadersList )
				return false;

			// Loop through the Existing and New Items
			// comparing the VisiblePositions and Levels
			for ( int i = 0; i < existingPosHeadersList.Count; i++ )
			{
				ColumnHeader columnHeader =  existingPosHeadersList[i] as ColumnHeader;

				Debug.Assert( null != columnHeader, "All elements in existingPosHeadersList must be ColumnHeader instances" );

				if ( null == columnHeader )
					return false;

				PositionInfo newPosInfo = new PositionInfo( );
				newPosInfo = newPos.GetPositionInfo( columnHeader ); 
				PositionInfo existingPosInfo = columnHeader.Column.PositionInfo;

				newPosInfo = newPos.GetPositionInfo( columnHeader );

				if ( null ==  newPosInfo ||
					 newPosInfo.Group != existingPosInfo.Group ||
					 newPosInfo.Level != existingPosInfo.Level ||
					 newPosInfo.VisiblePos != existingPosInfo.VisiblePos )
				{
					return false;
				}        
			}
    
			return true;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void CleanUp( ArrayList orderedColHeadersClone )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static void CleanUp( ArrayList orderedColHeadersClone )
		internal static void CleanUp( List<HeaderBase> orderedColHeadersClone )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.CleanUp( orderedColHeadersClone, null );
			// MD 8/10/07 - 7.3 Performance
			// The CleanUp helper method was not neeed, moved all code into this method
			//DragStrategyColumn.CleanUp( orderedColHeadersClone, null );

			if ( null != orderedColHeadersClone )
				orderedColHeadersClone.Clear();
		}

		// MD 8/10/07 - 7.3 Performance
		// Not needed
		#region Removed

		//// MD 7/26/07 - 7.3 Performance
		//// FxCop - Mark members as static
		////internal void CleanUp( ArrayList orderedColHeadersClone, SelectedColsCollection movedCols )
		//internal static void CleanUp( ArrayList orderedColHeadersClone, SelectedColsCollection movedCols )
		//{
		//    // MD 7/26/07 - 7.3 Performance
		//    // FxCop - Mark members as static
		//    //this.CleanUp( orderedColHeadersClone, movedCols, null );
		//    DragStrategyColumn.CleanUp( orderedColHeadersClone, movedCols, null );
		//}
		//
		//// MD 7/26/07 - 7.3 Performance
		//// FxCop - Mark members as static
		////internal void CleanUp( ArrayList orderedColHeadersClone, SelectedColsCollection movedCols, ArrayList headerList )
		//internal static void CleanUp( ArrayList orderedColHeadersClone, SelectedColsCollection movedCols, ArrayList headerList )
		//{
		//    if ( null != orderedColHeadersClone )
		//    {
		//        orderedColHeadersClone.Clear( );
		//    }
		//
		//    if ( null != headerList )
		//    {
		//        headerList.Clear( );
		//    }
		//}

		#endregion Removed

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool AreAllColsInSameGroup( ArrayList colHeaderList )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static bool AreAllColsInSameGroup( ArrayList colHeaderList )
		internal static bool AreAllColsInSameGroup( List<HeaderBase> colHeaderList )
		{
			if ( null == colHeaderList )
				return false;

			bool inSameGroup = true;

			
			UltraGridGroup group = null;
    
			// Iterate through the Columns and
			// check if they all are in the same
			// group.
			for ( int i = 0; i < colHeaderList.Count; i++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//HeaderBase header = colHeaderList[i] as HeaderBase;
				HeaderBase header = colHeaderList[ i ];

				Debug.Assert( null != header, "colHeaderList must all contain column headers" );

				if ( null == header )
				{
					inSameGroup = false;
					break;
				}
                
				// If the Group hasn't been set yet
				// get the Columns Group
				if ( null == group )
				{
					group = header.Group;
				}
				else
				{
					// If one of the columns is in a different
					// group the whole move is not valid.
					if ( header.Group != group )
					{
						inSameGroup = false;
						break;
					}
				}
			}
    

			return inSameGroup;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool AreAllColsInSameBand( ArrayList colHeaderList, UltraGridBand band )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static bool AreAllColsInSameBand( ArrayList colHeaderList, UltraGridBand band )
		internal static bool AreAllColsInSameBand( List<HeaderBase> colHeaderList, UltraGridBand band )
		{
			if ( null == colHeaderList )
				return false;

			bool inSameBand = true;

			// Iterate through the Columns and
			// check if they all are in the same
			// band.
			for ( int i = 0; i < colHeaderList.Count; i++ )
			{
				ColumnHeader columnHeader = colHeaderList[i] as ColumnHeader;

				Debug.Assert( null != columnHeader, "All elements of colHeaderList must be ColumnHeader instances" );
        
				// If one of the Columns is in a different
				// band the whole move is not valid.
				if ( columnHeader.Band != band )
				{
					inSameBand = false;
					break;
				}
			}
    
			return inSameBand;
		}

		internal bool GetExtendedDropLocation( Point point, DropLocationInfo dropLocationInfo, ref Rectangle itemBounds )
		{
			bool valid = false;

			UltraGridLayout layout = this.grid.DisplayLayout;

			if ( null != layout )
			{
				if ( null != layout.GetUIElement( false ) )
				{
					UIElement element = layout.GetUIElement( false ).ElementFromPoint( point );

					// The BandHeaders Element contains all the Group/Column headers
					// for the band.
					if ( null != element && element.GetType( ) == typeof( BandHeadersUIElement ) )
					{
						UIElementsCollection childElements = element.ChildElements;
                
						if ( null != childElements )
						{
						   
							Rectangle groupHeaderRect = Rectangle.Empty;
							
							// First Get The Group
							//
							// iterator over the sub elements collection calling this
							// method on each
							//
							for ( int i = 0; i < childElements.Count; i++ )
							{
								UIElement element2 = childElements[ i ];

								// SSP 10/8/01
								// 
								//Rectangle childElementRect = element.Rect;
								Rectangle childElementRect = element2.Rect;

								// Get Group that point falls within Left and Right Boundaries.
								if ( point.X > childElementRect.Left && point.X < childElementRect.Right )
								{
									HeaderBase header =  element2.GetContext( typeof( ColumnHeader ) ) as HeaderBase;
									if ( null == header )
										header = element2.GetContext( typeof( GroupHeader ) ) as HeaderBase;
									
									if ( null != header )
									{

										// If this is a group then set the DropLocationInfo
										// to point to it.
										if ( header.IsGroup )
										{
											Debug.Assert( header is GroupHeader, "header has to be GroupHeader if header.IsGroup is true" );

											dropLocationInfo.GroupHeader = header as GroupHeader;

											groupHeaderRect = childElementRect;
											break;
										}
									}
								}
                        
							}
                    
							// Next get the Level

							// SSP 11/8/01 UWG686
							// I don't know how it worked in active x grid, but this calcualtion
							// does not make any sense to me. So wrote a new formula to figure out the level
							//							
							//int level = ( point.Y / ( ( groupHeaderRect.Bottom + band.GetColumnHeaderHeight( ) ) / this.band.LevelCount ) );
							// MRS 4/21/05 - BR03481
							// GetColumnHeaderHeight was returning 0 here. Call the overload
							// that gets a single header height even when the headers are not visible. 
							//int level = ( point.Y - groupHeaderRect.Bottom ) / ( band.GetColumnHeaderHeight( ) / this.band.LevelCount );
							// MD 1/21/09 - Groups in RowLayouts
							// Use LevelCountResolved because it checks the band's row layout style
							//int level = ( point.Y - groupHeaderRect.Bottom ) / ( band.GetColumnHeaderHeight(true, true, false ) / this.band.LevelCount );
							int level = ( point.Y - groupHeaderRect.Bottom ) / ( band.GetColumnHeaderHeight( true, true, false ) / this.band.LevelCountResolved );

							dropLocationInfo.Level = level;

							// MD 1/21/09 - Groups in RowLayouts
							// Use LevelCountResolved because it checks the band's row layout style
							//int levelHeight      = this.band.GetColumnHeaderHeight( ) / this.band.LevelCount;
							int levelHeight = this.band.GetColumnHeaderHeight() / this.band.LevelCountResolved;

							itemBounds.Y = groupHeaderRect.Bottom + ( level * levelHeight );
							itemBounds.Height  = levelHeight;
							itemBounds.X       = groupHeaderRect.Left; 
							itemBounds.Width   = groupHeaderRect.Width;
						
						}

					}

				}
        
			}

			// If the info we need to drop on an empty level was retrieved,
			// then this is a valid drop location.
			if ( -1 != dropLocationInfo.Level &&
				 null != dropLocationInfo.GroupHeader  )
			{
				valid = true;
			}


			return valid;
		}
	}



	internal class DragStrategyGroup : DragStrategy
	{
				
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields	
		//internal DragStrategyGroup( UltraGridBand band, UltraGrid grid, Selected selected )
		//    : base( band, grid, selected )
		internal DragStrategyGroup( UltraGridBand band, UltraGrid grid )
			: base( band, grid )
		{			
		}


		// Methods changed to take CDropLocationInfo class to handle drops on
		// group headers as well as on empty levels.
		internal override bool BeginDrag( HeaderBase headerItem )
		{
			bool valid = true;

			AllowGroupMoving allowMove = this.band.AllowGroupMovingResolved;
    
			//	Ignore ssAllowGroupMovingNotAllowed @ design time
			
			if ( AllowGroupMoving.NotAllowed == allowMove &&
				!this.band.Layout.Grid.DesignMode )
			{
				valid = false;
			}
    
			if ( valid )
			{
				this.CreateDragItemsList( headerItem );
			}

			//	Get the CSR that the drag is beginning from, so we can
			//	disallow dropping onto a different CSR
			Point mouseLoc = this.grid.PointToClient( System.Windows.Forms.Cursor.Position );			
			ColScrollRegionsCollection csrColl = 
				this.grid.DisplayLayout.GetColScrollRegions( false );
			this.csr = csrColl.GetRegionFromOrigin( mouseLoc.X );
	
			//	set flag to true, since we are in the same CSR at this point
			this.sameCSR = true;

			return valid;
		}

		internal override bool IsDropValid( Point mouseLoc, DropLocationInfo dropLocationInfo, Rectangle itemBounds )
		{
			// If we have more than one group begin
			// dragged, the drop is always valid
			if ( this.GetItemCount( )> 1)
				return true;

			GroupHeader dropGroupHeader = null;
        
			if ( dropLocationInfo.IsDropTargetColumn )
			{
				dropGroupHeader = dropLocationInfo.GroupHeader;
			}
			else if ( dropLocationInfo.IsDropTargetGroup )
			{
				dropGroupHeader = dropLocationInfo.GroupHeader;
			}
			else
			{
				return false;
			}

			if ( null == dropGroupHeader )
				return false;

			// if we are here then the DragGroups list
			// contains only one member, so compare it
			// against the DropGroup var. If they are
			// the same, then the Drag is not valid, 
			// because the drag and drop group is the same

			this.GetDropLocation( dropLocationInfo, mouseLoc, itemBounds );

			int tmp = -1;
			// array list of HeaderBase instanaces
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList newOrderedGroupHeaders = this.CreateNewOrderedGroupHeaders( dropLocationInfo.GroupHeader, dropLocationInfo.DropLocation, ref tmp );
			List<HeaderBase> newOrderedGroupHeaders = this.CreateNewOrderedGroupHeaders( dropLocationInfo.GroupHeader, dropLocationInfo.DropLocation, ref tmp );

			if ( null == newOrderedGroupHeaders )
				return false;

			//Compare pNewOrderedGroups with m_DragItems PositionItems
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//bool same = this.CompareVisiblePositions( newOrderedGroupHeaders, this.dragItems );
			bool same = DragStrategy.CompareVisiblePositions( newOrderedGroupHeaders, this.dragItems );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.CleanUp( newOrderedGroupHeaders, null );
			DragStrategyGroup.CleanUp( newOrderedGroupHeaders, null );

			return !same;
		}

		internal override bool Drop( Point point, DropLocationInfo dropLocationInfo, Rectangle itemBounds )
		{
			//	Don't allow drops on different CSRs than the one the drag started from
			if( !this.sameCSR )
				return false;

			GridItemBase gridItem = dropLocationInfo.GridItem;

			if ( null != gridItem )
			{
				SelectChange type = gridItem.SelectChangeType;
        
				// Only allow the Drop if the item
				// that is the drop target is a group
				if ( SelectChange.Group == type )
				{
					GroupHeader dropGroupHeader = gridItem as GroupHeader;

					Debug.Assert( null != dropGroupHeader, "if gridItem.SelectChangeType == Group, then gridItem must be a group" );

					// Allow swapping at design time
					if ( null != dropGroupHeader )
					{
						// Determine if the Drop is Before or After Item
						this.GetDropLocation( dropLocationInfo, point, itemBounds );

						// Create the OrderedGropsClone that reflects the 
						// order of the Groups if the move operation
						// succeeded.
						int dropPos = INVALID_DROP_POS;

						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//ArrayList orderedGroupHeadersClone = this.CreateNewOrderedGroupHeaders( dropLocationInfo.GroupHeader, dropLocationInfo.DropLocation, ref dropPos );
						List<HeaderBase> orderedGroupHeadersClone = this.CreateNewOrderedGroupHeaders( dropLocationInfo.GroupHeader, dropLocationInfo.DropLocation, ref dropPos );

						// Create the Collection that gets passed into the events.
						GroupsCollection movedGroupClones = this.CreateMovedGroupsClone( orderedGroupHeadersClone, dropGroupHeader );

						if ( null == orderedGroupHeadersClone ||
							null == movedGroupClones   ||
							INVALID_DROP_POS == dropPos )
						{
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//this.CleanUp( orderedGroupHeadersClone, movedGroupClones );
							DragStrategyGroup.CleanUp( orderedGroupHeadersClone, movedGroupClones );

							return false;
						}

						BeforeGroupPosChangedEventArgs e = new BeforeGroupPosChangedEventArgs( PosChanged.Moved, DragStrategy.ToGroupHeaderArray( movedGroupClones.All ) );
						
						//fire the BeforeGroupPosChanged event
						this.grid.FireBeforeGroupPosChanged( e );

						// MD 7/26/07 - 7.3 Performance
						// FxCop - Avoid unused private fields
						//this.SetInBeforePosChanged( false );

						if ( e.Cancel )
						{
							// If the operation was cancelled
							// release the resources
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//this.CleanUp( orderedGroupHeadersClone, movedGroupClones );
							DragStrategyGroup.CleanUp( orderedGroupHeadersClone, movedGroupClones );

							return false;
						}

						// SSP 7/23/03 - Fixed headers
						// Added movedHeaderClones parameter which contains the cloned headers that were moved
						// and the drag stratergy passed them in the event args of Before Col/Group Pos Changed
						// event. It contains the information on whether those headers should be fixed or unfixed.
						//
						//this.band.SetOrderedGroups( orderedGroupHeadersClone );
						this.band.SetOrderedGroups( orderedGroupHeadersClone, DragStrategy.ToHeaderArray( movedGroupClones ) );
						
						// Create a new groupss collection containing the 
						// actual (not cloned) groups that were moved for the 'after'
						// event
						//
						GroupsCollection movedGroups = new GroupsCollection( this.band  );
                				
						int i;

						try
						{
							// copy all 'cloned from' (base) columns into the collection
							//
							for ( i = 0; i < movedGroupClones.Count; i++ )
							{
								// AS - 12/14/01
								// At some point, we removed IList from the groups collection's
								// implementation. Use the add method instead.
								//((IList)movedGroups).Add( movedGroupClones[i].GetClonedFromGroup( ) );
								movedGroups.Add( movedGroupClones[i].GetClonedFromGroup( ) );
							}
						}
						catch( Exception e2 )
						{
							Debug.Assert( false, "DragStrategyGroup.Drop( ) failed: " + e2 );
						}

						AfterGroupPosChangedEventArgs e3 = new AfterGroupPosChangedEventArgs( PosChanged.Moved, null != DragStrategy.ToGroupHeaderArray( movedGroups.All ) ? DragStrategy.ToGroupHeaderArray( movedGroups.All ) : DragStrategy.ToGroupHeaderArray(  movedGroupClones.All ) );
							
						// fire the after event and clean up
						this.grid.FireAfterGroupPosChanged( e3 );

						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//this.CleanUp( orderedGroupHeadersClone, movedGroupClones );
						DragStrategyGroup.CleanUp( orderedGroupHeadersClone, movedGroupClones );

						// release the temp groups collection
						//
						if ( null != movedGroups )
						{
							movedGroups.ClearClonedGroups( );
						}
					}
				}
			}

			return true;
		}

		internal override bool CreateDragItemsList( HeaderBase headerItem )
		{
			Debug.WriteLine( "DragStrategyColumn.CreateDragItemsList called" );

			this.dragItems.Clear( );
    
			// array list of HeaderBase instances
			HeadersCollection headerList  = this.band.OrderedHeaders;
			    
			if ( null != headerList )
			{
				for ( int i = 0; i < headerList.Count; i++ )
				{
					HeaderBase header = headerList[i] as HeaderBase;

					Debug.Assert( null != header, "All elements of headerList must be HeaderBase instances" );

					if ( header.IsGroup )
					{
						GroupHeader groupHeader = header as GroupHeader;

						Debug.Assert( null != groupHeader, "If header.IsGroup returns true, that header must be a GroupHeader" );

						if ( null != groupHeader )
						{
							if ( groupHeader.Selected )
							{
								this.dragItems.Add( groupHeader );
							}
                     
						}
                     
					}
				}    
			}
		

			if ( 0 == this.dragItems.Count )
			{
				GroupHeader groupHeader = headerItem as GroupHeader;
				
				Debug.Assert( null != groupHeader );

				if ( null == groupHeader )
					return false;

				this.dragItems.Add( groupHeader );
			}

			return true;
		}

		
		protected override bool IsGridItemValid( GridItemBase gridItem )
		{
			return null != gridItem && SelectChange.Group == gridItem.SelectChangeType;
		}
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList CreateNewOrderedGroupHeaders( GroupHeader dropGroupHeader, DropLocation dropLocation, ref int newDropPos )
		internal List<HeaderBase> CreateNewOrderedGroupHeaders( GroupHeader dropGroupHeader, DropLocation dropLocation, ref int newDropPos )
		{
			// This logic simulates what happens after the move
			// and the new VisiblePosition of the Group is set.

			// array list of HeaderBase instances
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList orderedGroupHeadersClone = this.band.GetOrderedGroupHeadersClone( );
			List<HeaderBase> orderedGroupHeadersClone = this.band.GetOrderedGroupHeadersClone();
    
			if ( null != orderedGroupHeadersClone )
			{
				for ( int i = 0; i < this.dragItems.Count; i++ )
				{
					try
					{
						// First Remove the Group Begin Moved
						orderedGroupHeadersClone.Remove( this.dragItems[i] );            
					}
					catch ( Exception )
					{
					}
				}

				// Find the DropGroups new Visible Position
				// After any groups that are being moved
				// have been removed
				int dropPos = INVALID_DROP_POS;

				int count = 0;

				int newPosIterator;
        
				for ( newPosIterator = 0; newPosIterator < orderedGroupHeadersClone.Count;
					newPosIterator++, count++ )
				{
					if ( orderedGroupHeadersClone[ newPosIterator ] == dropGroupHeader )
					{
						dropPos = count;
						break;
					}
				}
        
				// If we are unable to find the DropGroups's
				// new VisiblePos, it was one of the Groups
				// being dragged, so bail
				if ( INVALID_DROP_POS == dropPos )
				{
					// If the DropPos is invalid
					// Add All items back to Clone list
					// and clean up any resources.
					for ( int k = 0; k < this.dragItems.Count; k++ )
					{
						orderedGroupHeadersClone.Add( this.dragItems[k] );
					}

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.CleanUp( orderedGroupHeadersClone, null );
					DragStrategyGroup.CleanUp( orderedGroupHeadersClone, null );

					return null;
				}
        
				// If the DropLocation is after the DropItem
				// then increment the position
				if ( DropLocation.After == dropLocation )
					++dropPos;

				// Set the Iterator to the new Visible Position (nDropPos)
				int j = dropPos;

				// Now add the Groups back to the collection
				// with the new VisiblePosition
				for ( int l = 0; l < this.dragItems.Count; l++ )
				{
					orderedGroupHeadersClone.Insert( j++, this.dragItems[l] );
				}

				newDropPos = dropPos;

			}
    
			return orderedGroupHeadersClone;
		}

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal GroupsCollection  CreateMovedGroupsClone( ArrayList orderedGroupHeadersClone, GroupHeader dropGroup )
		internal GroupsCollection CreateMovedGroupsClone( List<HeaderBase> orderedGroupHeadersClone, GroupHeader dropGroup )
		{
			if ( null == orderedGroupHeadersClone )
				return null;

			GroupsCollection movedGroups = new GroupsCollection( this.band  );
    
			if ( null != movedGroups )
			{
				// Clone the Groups that are moving
				UltraGridGroup cloneGroup = null;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.SetInBeforePosChanged( true );

				for ( int i = 0; i < this.dragItems.Count; i++ )
				{
					PositionInfo posInfo = new PositionInfo( );

					GroupHeader groupHeader = this.dragItems[i] as GroupHeader;

					Debug.Assert( null != groupHeader, "All elements in dragItems must be GroupHeader instances" );

					if ( null == groupHeader )
						continue;

					UltraGridGroup group = groupHeader.Group;

					cloneGroup = group.Clone( PosChanged.Moved, false );
        
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//posInfo.VisiblePos = (short)this.GetNewVisiblePos( groupHeader, orderedGroupHeadersClone );
					posInfo.VisiblePos = (short)DragStrategy.GetNewVisiblePos( groupHeader, orderedGroupHeadersClone );
					
					cloneGroup.SetPositionInfo( posInfo );
        
					if ( null != cloneGroup )
					{
						// AS - 12/14/01
						// At some point, we removed IList support from the groups collection.
						// Just use the add method instead.
						//((IList)movedGroups).Add( cloneGroup );
						movedGroups.Add( cloneGroup );

						// SSP 7/24/03 - Fixed headers
						// If the headers are being dropped over fixed headers area, then set their
						// fixed property to true. The reason for doing this is that when the before
						// cols moved event gets fired, the fixed properties of the cloned guys will
						// be true.
						//
						// --------------------------------------------------------------------------
						if ( null != dropGroup && null != cloneGroup.Header )
						{
							if ( dropGroup.FixedResolved )
							{
								cloneGroup.Header.InternalSetFixed( true );

								// MD 12/9/08 - 9.1 - Column Pinning Right
								cloneGroup.Header.InternalSetFixOnRight( dropGroup.FixOnRight );
							}
							else
								cloneGroup.Header.InternalSetFixed( false );
						}
						// --------------------------------------------------------------------------
					}
        
					cloneGroup = null;
				}
			}

			return movedGroups;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Remove unused locals
		//internal void CleanUp( ArrayList orderedGroupsClone, GroupsCollection movedGroups )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static void CleanUp( ArrayList orderedGroupsClone, GroupsCollection movedGroups )
		internal static void CleanUp( List<HeaderBase> orderedGroupsClone, GroupsCollection movedGroups )
		{
			if ( null != orderedGroupsClone )
			{    
				orderedGroupsClone.Clear( );
			}

			if ( null != movedGroups )
			{   
				movedGroups.ClearClonedGroups( );
			}
		}

	}

	//internal static class DragStrategyUtilities
	//{
	//    internal const int INVALID_DROP_POS = -1;

	//    // SSP 7/24/03 - Fixed headers
	//    // Added below ToHeaderArray method.
	//    //
	//    internal static HeaderBase[] ToHeaderArray( SelectedColsCollection headers )
	//    {
	//        // MD 8/10/07 - 7.3 Performance
	//        // Use generics
	//        //ArrayList list = new ArrayList( headers.Count );
	//        List<HeaderBase> list = new List<HeaderBase>( headers.Count );

	//        for ( int i = 0; i < headers.Count; i++ )
	//        {
	//            if ( null != headers[ i ] )
	//                list.Add( headers[ i ] );
	//        }

	//        // MD 8/10/07 - 7.3 Performance
	//        // Use generics
	//        //return (HeaderBase[])list.ToArray( typeof( HeaderBase ) );
	//        return list.ToArray();
	//    }

	//    // SSP 7/24/03 - Fixed headers
	//    // Added below ToHeaderArray method.
	//    //
	//    internal static HeaderBase[] ToHeaderArray( GroupsCollection groups )
	//    {
	//        // MD 8/10/07 - 7.3 Performance
	//        // Use generics
	//        //ArrayList list = new ArrayList( groups.Count );
	//        List<HeaderBase> list = new List<HeaderBase>( groups.Count );

	//        for ( int i = 0; i < groups.Count; i++ )
	//        {
	//            if ( null != groups[ i ] && null != groups[ i ].Header )
	//                list.Add( groups[ i ].Header );
	//        }

	//        // MD 8/10/07 - 7.3 Performance
	//        // Use generics
	//        //return (HeaderBase[])list.ToArray( typeof( HeaderBase ) );
	//        return list.ToArray();
	//    }

	//    internal static GroupHeader[] ToGroupHeaderArray( UltraGridGroup[] array )
	//    {
	//        GroupHeader[] groupHeaders = new GroupHeader[ array.Length ];

	//        for ( int i = 0; i < array.Length; i++ )
	//        {
	//            groupHeaders[ i ] = array[ i ].Header;
	//        }


	//        return groupHeaders;
	//    }


	//    internal static ColumnHeader[] ToColumnHeaderArray( object[] array )
	//    {
	//        ColumnHeader[] columnHeaders = new ColumnHeader[ array.Length ];

	//        int j = 0;
	//        for ( int i = 0; i < array.Length; i++ )
	//        {
	//            ColumnHeader ch = array[ i ] as ColumnHeader;
	//            UltraGridColumn c = array[ i ] as UltraGridColumn;

	//            if ( null != ch )
	//                columnHeaders[ j++ ] = ch;
	//            else if ( null != c )
	//                columnHeaders[ j++ ] = c.Header;
	//            else
	//            {
	//                Debug.Assert( false, "array[i] is neither a Column or a ColumnHeader" );
	//            }
	//        }

	//        return columnHeaders;
	//    }

	//    internal static GroupHeader[] ToGroupHeaderArray( object[] array )
	//    {
	//        GroupHeader[] groupHeaders = new GroupHeader[ array.Length ];

	//        int j = 0;
	//        for ( int i = 0; i < array.Length; i++ )
	//        {
	//            GroupHeader gh = array[ i ] as GroupHeader;
	//            UltraGridGroup g = array[ i ] as UltraGridGroup;

	//            if ( null != gh )
	//                groupHeaders[ j++ ] = gh;
	//            else if ( null != g )
	//                groupHeaders[ j++ ] = g.Header;
	//            else
	//            {
	//                Debug.Assert( false, "array[i] is neither a Group or a GroupHeader" );
	//            }
	//        }

	//        return groupHeaders;
	//    }

	//    // MD 7/26/07 - 7.3 Performance
	//    // FxCop - Mark members as static
	//    //internal int GetNewVisiblePos( HeaderBase headerItem, ArrayList orderedHeaders )
	//    // MD 8/10/07 - 7.3 Performance
	//    // Use generics
	//    //internal static int GetNewVisiblePos( HeaderBase headerItem, ArrayList orderedHeaders )
	//    internal static int GetNewVisiblePos<T>( HeaderBase headerItem, List<T> orderedHeaders ) where T : HeaderBase
	//    {
	//        if ( null == headerItem || null == orderedHeaders )
	//            return INVALID_DROP_POS;

	//        int count = 0;
	//        int pos = INVALID_DROP_POS;

	//        for ( int i = 0; i < orderedHeaders.Count; i++, count++ )
	//        {
	//            if ( orderedHeaders[ i ] == headerItem )
	//            {
	//                pos = count;
	//                break;
	//            }
	//        }

	//        return pos;
	//    }

	//    // MD 7/26/07 - 7.3 Performance
	//    // FxCop - Mark members as static
	//    //internal bool CompareVisiblePositions( ArrayList newHeaderList, ArrayList existingHeaderList )
	//    // MD 8/10/07 - 7.3 Performance
	//    // Use generics
	//    //internal static bool CompareVisiblePositions( ArrayList newHeaderList, ArrayList existingHeaderList )
	//    internal static bool CompareVisiblePositions( List<HeaderBase> newHeaderList, List<HeaderBase> existingHeaderList )
	//    {
	//        if ( null == newHeaderList || null == existingHeaderList )
	//            return false;

	//        int existingItemIterator;
	//        int newItemIterator;

	//        // Loop through the Existing and New Items
	//        // comparing the VisiblePositions

	//        for ( existingItemIterator = 0; existingItemIterator < existingHeaderList.Count; existingItemIterator++ )
	//        {
	//            // MD 8/10/07 - 7.3 Performance
	//            // Use generics
	//            //HeaderBase existingItem = existingHeaderList[ existingItemIterator ] as HeaderBase;
	//            HeaderBase existingItem = existingHeaderList[ existingItemIterator ];

	//            Debug.Assert( null != existingItem, "All elements of existingHeaderList must be HeaderBase instances" );

	//            // Find the ExistingItem in New Item
	//            for ( newItemIterator = 0; newItemIterator < newHeaderList.Count; newItemIterator++ )
	//            {
	//                // MD 8/10/07 - 7.3 Performance
	//                // Use generics
	//                //HeaderBase newItem = newHeaderList[ newItemIterator ] as HeaderBase;
	//                HeaderBase newItem = newHeaderList[ newItemIterator ];

	//                Debug.Assert( null != newItem, "All elements of newHeaderList must be HeaderBase instances" );

	//                // If the items match
	//                if ( null != newItem && newItem == existingItem )
	//                {
	//                    // Compare VisiblePositions
	//                    // MD 7/26/07 - 7.3 Performance
	//                    // FxCop - Mark members as static
	//                    //int newVisPos = this.GetNewVisiblePos( existingItem, newHeaderList );
	//                    // MD 8/10/07 - 7.3 Performance
	//                    // Method was moved to DragStrategyUtilities
	//                    //int newVisPos = DragStrategy.GetNewVisiblePos( existingItem, newHeaderList );
	//                    int newVisPos = DragStrategyUtilities.GetNewVisiblePos( existingItem, newHeaderList );

	//                    int existingVisPos = existingItem.VisiblePosition;

	//                    // If they don't match there's no reason to go any further
	//                    // we are just trying to find if they are exactly the same
	//                    if ( newVisPos != existingVisPos )
	//                    {
	//                        return false;
	//                    }
	//                    // Since we've already found this items match
	//                    // look at the new loop
	//                    break;
	//                }
	//            }

	//        }

	//        return true;
	//    }
	//}
}
