#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Collections;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Shared.Serialization;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Security.Permissions;


	/// <summary>
	/// Collection of all row scrolling regions in a layout
	/// </summary>
	[Serializable()]
	public sealed class RowScrollRegionsCollection : ScrollRegionsCollectionBase, 
											  ISerializable
	{
		private bool metricsDirty;
		private bool initializingMetrics;
		private int initialCapacity;

		/// <summary>
		/// contructor
		/// </summary>
		/// <param name="layout"></param>
		public RowScrollRegionsCollection( UltraGridLayout layout ) : base ( layout )
		{
		}

        /// <summary>
        /// Add method required during de-serialization. 
        /// </summary>
        /// <param name="obj">The object to add to the collection.</param>        
        /// <remarks>Should not be called outside InitializeComponent.</remarks>
        /// <returns>The position into which the new element was inserted.</returns>
		[ Browsable( false ) ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ] 		
        public int Add( object obj )
		{
			if ( obj == null )
				throw new ArgumentNullException();

			if ( ! ( obj is RowScrollRegion ) )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_254"));

			if ( ((RowScrollRegion)obj).Collection != null )
				throw new NotSupportedException();

			((RowScrollRegion)obj).InitCollection( this );

			return this.AddScrollRegionHelper( (RowScrollRegion)obj );
		}
		

		/// <summary>
		/// overridden method indicates that the collection can not
		/// be modified
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// indexer
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowScrollRegion this[ int index ]
		{
			get
			{
				return (RowScrollRegion)this.List[ index ];
			}
		}
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			//all values that were set are now save into SerializationInfo
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// first add the count so we can set the initial capacity
			// efficiently when we de-serialize
			//
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Count", this.Count );
			//info.AddValue( "Count", this.Count );

			// add each RowScrollRegion in the collection
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, i.ToString(), this[i] );
				//info.AddValue( i.ToString(), this[i] );
			}

			// JJD 1/31/02
			// Serialize the tag property
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
		}

		internal void InitializeFrom( RowScrollRegionsCollection source, PropertyCategories propertyCategories )
		{
			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			int countdiff = this.Count - source.Count;
			//if current collection is larger than source
			if ( countdiff > 0 )
			{
				for ( int i = this.Count; i > source.Count; i-- ) 
				{
					this[i-1].Dispose();
					this.InternalRemove(i-1);
				}
			}
			//if current collection is less than source
			if ( countdiff < 0 )
			{
				for ( int i = this.Count; i < source.Count; i++ )
				{
					RowScrollRegion rsr = new RowScrollRegion(this);
					this.AddScrollRegionHelper( rsr );
				}
			}

			for ( int index = 0; index < this.Count; index++ )
				this[index].InitializeFrom(source[index]);
		}


		internal RowScrollRegionsCollection Clone()
		{
			Infragistics.Win.UltraWinGrid.RowScrollRegionsCollection clone = new RowScrollRegionsCollection(this.Layout);

			for ( int i = 0; i < this.Count; i++ )
			{
				Infragistics.Win.UltraWinGrid.RowScrollRegion rsr = new RowScrollRegion( this );

				rsr.InitializeFrom( this[i] );
				clone.AddScrollRegionHelper( rsr );
			}

			return clone;
		}
		private int AddScrollRegionHelper( RowScrollRegion rsr )
		{
			// Add a listener for property changes.
			rsr.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			
			// Add the appearance and return the index
			int index = this.InternalAdd(rsr);			

			return index;
		}

//#if DEBUG
		/// <summary>
		/// Clears the collection
		/// </summary>
//#endif
		// SSP 5/19/04
		// Made Clear method public. We need this for design time serialization to work 
		// properly. More specifically, when a transaction gets canceled, the designer
		// looks for a Clear method to clear the collection and then add the items
		// it contained using Add method. However if Clear is not public, it simply
		// adds the items so items end up being duplicated every time a transaction
		// is canceled. Also exposing Clear method to the end users is not bad either.
		//
		//internal void Clear()
		public void Clear( )
		{
        
			foreach ( RowScrollRegion rsr in this )
			{
				// remove the prop change notifications
				//
				rsr.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				// call dispose on the band object
				//
				rsr.Dispose();
			}

			this.InternalClear();

			this.metricsDirty = true;
		}

		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal RowScrollRegionsCollection( SerializationInfo info, StreamingContext context )
		private RowScrollRegionsCollection( SerializationInfo info, StreamingContext context )
		{
			//this will have to be set in SetLayout()
			//this.Layout = null;
			

			//everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( RowScrollRegion ) )
				{
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					RowScrollRegion item = (RowScrollRegion)Utils.DeserializeProperty( entry, typeof(RowScrollRegion), null );

					// Add the item to the collection
					if ( item != null )
						this.InternalAdd(item);
				}
				else
				{
					switch ( entry.Name )
					{
						case "Count":
						{
							
							//this.initialCapacity = Math.Max( this.InitialCapacity, (int)entry.Value );
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/17/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.initialCapacity = Math.Max(this.initialCapacity, (int)Utils.DeserializeProperty( entry, typeof(int), this.initialCapacity ));
							//this.initialCapacity = Math.Max(this.initialCapacity, (int)DisposableObject.ConvertValue( entry.Value, typeof(int) ));
							break;
						}
						case "Tag":
							// AS 8/15/02
							// Use our tag deserializer.
							//
							this.DeserializeTag(entry);
							break;

						default:
						{
							Debug.Assert( false, "Invalid entry in RowScrollRegionsCollection de-serialization ctor" );
							break;
						}				
					}
				}
			}
		}
		
		internal void InitLayout( Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		{
			this.Layout =  layout;

			for(int i = 0; i < this.Count; i++)
			{
				this[i].InitCollection(this);

				this[i].SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}
		}

		/// <summary>
		/// Specifies the initial capacity of the collection
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return this.initialCapacity;
			}
		}
 
		internal bool AreCardsInView
		{
			get
			{
				bool hasAtLeastOneCardViewBand = false;
        
				// first check to see if any of the bands support
				// card view. If not we can return false
				//
				for ( int i = 0; i < this.Layout.SortedBands.Count; i++ )
				{
					if ( this.Layout.SortedBands[i].HiddenResolved )
						continue;

					if ( this.Layout.SortedBands[i].CardView )
					{
						hasAtLeastOneCardViewBand = true;
						break;
					}
				}
        
				if ( hasAtLeastOneCardViewBand )
				{
					for ( int i = 0; i < this.Count; i++ )
					{
						if ( this[i].AreCardsInView( false ) )
							return true;
					}
				}

				return false;
			}
		}
		
		/// <summary>
		/// Called when this object is disposed of
		/// </summary>
		protected override void OnDispose() 
		{
			this.Clear();			
		}

		/// <summary>
		/// True is the little split box above of the vertical
		/// scrollbar is visible. This property is read-only and is 
		/// determined based on the number of RowScrollRegions
		/// and the value of the layout's MaxRowScrollRegions
		/// property.
		/// </summary>
		public bool IsSplitBoxVisible
		{
			get
			{
				//RobA UWG494 10/4/01 Check so see if we have already reached our max
				//
				return this.Count < this.Layout.MaxRowScrollRegions;
				
				//return true;
			}
		}
		// SSP 11/4/04 UWG3593
		// Changed the parameter for assumeColScrollbarsVisible from boolean to an enum
		// that specifies whether to assume and if so to whether to assume the scrollbar
		// is hidden or visible. This is to fix UWG3593 where the scrollbars are displayed 
		// even when they are not needed because we assume the horizontal scrollbar is 
		// visible to find out if vertical scrollbar should be displayed or not.
		//
		//internal override bool AreScrollbarsVisible ( bool fAssumeColScrollbarsVisible ) 
		internal override bool AreScrollbarsVisible( ScrollbarVisibility assumeColScrollbarsVisible ) 
		{
			// iterate over the list and return true if any region
			// has a scrollbar
			//
			for ( int i=0; i < this.Count; ++i )
			{
				RowScrollRegion rsr = this[i];

				if ( rsr != null &&
					 rsr.WillScrollbarBeShown( assumeColScrollbarsVisible ) )
					return true;
			}
    
			return false;
		}
    
		internal void OnSplitBoxDrop( Point point )
		{
			// SSP 5/12/06 - App Styling
			// Added custom properties for controlling widths of splitter bars and split boxes.
			// 
			//int splitterBarWidth  = this.Layout.SplitterBarWidth;
			int splitterBarWidth  = this.Layout.RowScrollRegionSplitterBarHeightResolved;

			foreach ( RowScrollRegion rsr in this )
			{
				// if it is within an existing row scroll region
				// then split the region at that stop
				//
				if ( point.Y > rsr.Origin + splitterBarWidth && 
					point.Y < rsr.Origin + rsr.Extent - splitterBarWidth )
				{
					// AS - 2/20/02
					// We were showing a messagebox when the split box was dropped
					// on a fixed col scroll region. Instead, we are now throwing an
					// exception since the user may also call that routine from code.
					// We'll catch the exception and eat it, if needed.
					//
					try
					{
						rsr.Split( point.Y - rsr.Origin );	
					}
					catch {}

					break;
				}
			}
		}
		internal void OnResize( RowScrollRegion rsr, int deltaY )
		{            
			// SSP 5/12/06 - App Styling
			// Added custom properties for controlling widths of splitter bars and split boxes.
			// 
			//int splitterBarWidth            = this.Layout.SplitterBarWidth;
			int splitterBarWidth            = this.Layout.RowScrollRegionSplitterBarHeightResolved;

			int newExtent                   = rsr.Extent + deltaY;
			int newBottom                   = rsr.Origin + newExtent;
			RowScrollRegion nextRegion      = this.GetNextVisibleRegion ( rsr, false );
			RowScrollRegion regionToRemove  = null;
			RowScrollRegion regionToResize  = null;


			if ( newExtent < 2 )
			{
				// since it was dragged to the top of the range delete it
				// and adjust the next region to pick up its space
				//
				regionToRemove  = rsr;
				regionToResize  = nextRegion;
			}
			else
				if ( null != nextRegion &&
				newBottom >= nextRegion.Origin 
				+ nextRegion.Extent
				
				// + nextRegion.ClientWidth( NULL )
				- splitterBarWidth  )
			{
				// since it was dragged to the bottom of the range  
				// if the next region isn't fixed delete the next region
				// adjust this region to pick up its space
				//
				if ( SizingMode.Fixed == nextRegion.SizingMode )
				{
					// the next region is fixed so just delete
					// the passed in region  (don't attempt to
					// resize the next region)
					//
					regionToRemove  = rsr;
				}
				else
				{
					regionToRemove  = nextRegion;
					regionToResize  = rsr;
				}
			}
			else
			{
				// we are not deleting so just resize this region
				//
				regionToResize  = rsr;
			}

			if ( null != regionToRemove )
			{
				// Save the lower of the 2 origins as the
				// new origin
				//
				int newOrigin = regionToRemove.Origin;

				if ( null != regionToResize && 
					regionToResize.Origin < newOrigin )
					newOrigin = regionToResize.Origin;

				// recalce the extent so that the appropriate remaining region
				// will pick up the extent of the removed region
				//
				if ( null != regionToResize )
				{
					newExtent = regionToResize.Extent +
						regionToRemove.Extent + 
						splitterBarWidth;
				}

				this.Remove( regionToRemove );

				// check to make sure the above remove wasn't cancelled
				// by trying to find the region we attempted to remove
				// in the collection
				//
				int index = this.IndexOf( regionToRemove );
                
				// if the region is still there (remove was cancelled)
				// then just exit
				//
				if ( index >= 0 )
					return;

				// Call SetOriginAndExtent to ensure that the adjacent
				// regionToResize absorbs all of the freed up real estate
				//
				if ( null != regionToResize )
					regionToResize.SetOriginAndExtent( newOrigin, newExtent ); 

				// force an update of the UIElement which will cause
				// the metaregion rects to be re-calculated
				//
				this.Layout.UpdateUIElementRect();
			}
			else
				if ( null != regionToResize )
				this.SetExtent( regionToResize, newExtent );

		}

		internal bool MetricsDirty
		{
			get
			{
				return this.metricsDirty;
			}
		}
		internal bool InitializingMetrics
		{
			get
			{
				return this.initializingMetrics;
			}
		}
		internal void InitializeMetrics()
		{
			if ( !this.metricsDirty )
				return;

			this.metricsDirty = false;

			this.initializingMetrics = true;

			// JJD 12/28/01
			// Wrap logic in try/finally so that initializingMetrics flag
			// will be guaranteed to be reset
			//
			try
			{
				// tell each row scroll region to regenerate
				// its column hedaers based on the new metrics
				//
				for ( int i = 0; i < this.Count; i++ )
				{
					this[i].RegenerateVisibleRows();
				}
			}
			finally
			{
				this.initializingMetrics = false;
			}

			// SSP 10/25/01
			// Implemented repositioning the edit control
			//
			// JJD 3/31/00 - ult131
			// If we are in edit mode then reposition the edit ctl
			//
			// SSP 4/29/02
			// EM Embeddable editors.
			//
			//if ( null != this.Layout.EditControl && this.Layout.EditControl.Visible )
			if ( null != this.Layout.ActiveCell && this.Layout.ActiveCell.IsInEditMode )
			{
				try
				{
					Debug.Assert( null != this.Layout.ActiveCell,
						"No active cell but edit control visible" );

					if ( null != this.Layout.ActiveCell )
						this.Layout.ActiveCell.RepositionTextBoxCtl( );
				}
				catch( Exception )
				{
					Debug.Assert( false, "Failure in ActiveCell.RepositionEditCtl() call!" );
				}
			}
		}

		internal override void CheckIfSizeChanged()
		{	
			for ( int i=0; i < this.Count; ++i )
				this[i].CheckIfSizeChanged();
		}
		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// pass the notification along to our listeners
			//
			
			this.NotifyPropChange( PropertyIds.RowScrollRegion, propChange );

			if ( propChange.PropId is PropertyIds )
			{
				switch ( (PropertyIds)propChange.PropId )
				{
					case PropertyIds.Scrollbar:
					case PropertyIds.Hidden:
					case PropertyIds.Remove:
				{
						this.Layout.DirtyGridElement();
						return;
					}
				}
			}
		}
		internal override void InitFirstRegion() 
		{
			if ( Count < 1 )
			{
				RowScrollRegion rsr = new RowScrollRegion( this );

				this.List.Add( rsr );

				// hook up the prop change notifications
				//
				rsr.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}
		}
        
		internal void InternalInsert( RowScrollRegion rsr, int insertAt ) 
		{
			this.InternalInsert( insertAt, rsr );

			// hook up the prop change notifications
			//
			rsr.SubObjectPropChanged += new SubObjectPropChangeEventHandler( this.OnSubObjectPropChanged );

			this.metricsDirty = true;
		}
		internal RowScrollRegion GetNextVisibleRegion ( RowScrollRegion startRegion, 
			bool wrap )
		{

			if ( this.Count < 1 )
				return null;

			RowScrollRegion lastVisibleRegion = null;

			// iterate over the collection (with a reverse iterator)
			// looking for the next visible region to return
			//
			for ( int i = this.Count - 1; i >= 0 ; i-- )
			{
				// when we encounter the passed in start region we
				// want to return if we found a visible region
				// or if the fWrap flag is false 
				//
				if ( startRegion == this[i] )
				{
					if ( null != lastVisibleRegion || !wrap )
						return lastVisibleRegion;
				}

				// bypass hidden regions
				//      
				if ( this[i].Hidden )
					continue;

				// keep track of the last visible region
				//
				lastVisibleRegion = this[i];
			}

			// return the last visible region we found
			//
			return lastVisibleRegion;
		}

		internal RowScrollRegion GetPreviousVisibleRegion ( RowScrollRegion startRegion, 
			bool wrap )
		{
			// SSP 8/7/02 UWG1194
			// Logic for traversing the row scroll regions (through Shift-F6 keystrokes)
			// passes in null as the startRegion to get the first visible region.
			// So we don't need to throw an exception if startRegion is null. Below logic
			// does not access the startRegion in any way.
			//
			

			// SSP 8/7/02 UWG1194
			// This should be checking for no scroll regions, not less than 2.
			//
			//if ( this.Count < 2 )
			if ( this.Count <= 0 )
				return null;

			RowScrollRegion lastVisibleRegion = null;

			// iterate over the collection  looking for the previous 
			// visible region to return
			//
			// SSP 8/7/02 UWG1194
			// 
			//for ( int i = 0; i < this.Count - 1; i++ )
			for ( int i = 0; i < this.Count; i++ )
			{
				// when we encounter the passed in start region we
				// want to return if we found a visible region
				// or if the fWrap flag is false 
				//
				if ( startRegion == this[i] )
				{
					if ( null != lastVisibleRegion || !wrap )
						return lastVisibleRegion;
				}

				// bypass hidden regions
				//      
				if ( this[i].Hidden )
					continue;

				// keep track of the last visible region
				//
				lastVisibleRegion = this[i];
			}

			// return the last visible region we found
			//
			return lastVisibleRegion;
		}

		internal void SetExtent( RowScrollRegion rsr, int newExtent )
		{

			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if ( null != grid )
			{
				// if we are already in the BeforeColResionSize or
				// BeforeColRegionSplit events return an error
				//
				if ( grid.EventManager.InProgress( GridEventIds.BeforeRowRegionSize ) ||
					grid.EventManager.InProgress( GridEventIds.BeforeRowRegionSplit ) )
				{
					throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_256"));
				}
			}

			// get a pointer to next the visible region (if there is one)
			//
			RowScrollRegion nextVisibleRegion = GetNextVisibleRegion ( rsr, false );

			if ( null == nextVisibleRegion )
			{
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_61") );
			}

			int thisOrigin    = rsr.Origin;
			int thisExtent    = rsr.Extent;
			int nextOrigin    = nextVisibleRegion.Origin;
			int nextExtent    = nextVisibleRegion.Extent;

			// Check for valid value
			//
			// don't throw an error if new extent is too large. Instead, reduce it
			// to the largest allowable value
			//
			if ( newExtent < 0  ) 
				throw new ArgumentOutOfRangeException(Shared.SR.GetString("LE_ArgumentOutOfRangeException_256"));

			// Allow enough space for the next regions scrollbar and a splitter bar
			// between regions
			//
			int maxExtent = thisExtent + nextExtent - SystemInformation.HorizontalScrollBarHeight;

			if ( newExtent > maxExtent )
			{
				if ( maxExtent > 0 )
					newExtent = maxExtent;
			}

			if ( newExtent < 0 )
				newExtent = 0;

			// calculate the delta extent
			//
			int delta = newExtent - thisExtent;

			// set the adjusted origin and extent values
			//
			// Note: pass false in for the 3rd parameter so the scrollbar
			//       doesn't get repositioned immediately. This prevents
			//       a scrollbar flicker if the before event gets cancelled. 
			//
			rsr.SetOriginAndExtent( thisOrigin, newExtent );

			nextVisibleRegion.SetOriginAndExtent( nextOrigin + delta, 
				nextExtent - delta );

			// fire the before event - if cancelled return S_OK;
			//
			if ( null != grid )
			{
				BeforeRowRegionSizeEventArgs args = new BeforeRowRegionSizeEventArgs( rsr, nextVisibleRegion );
				grid.FireEvent( GridEventIds.BeforeRowRegionSize, args );

				if ( args.Cancel )
				{
					// reset the origin's and extents back to their original value
					//
					rsr.SetOriginAndExtent( thisOrigin, 
						thisExtent );

					nextVisibleRegion.SetOriginAndExtent( nextOrigin, 
						nextExtent );
				}
			}

			this.Layout.DirtyGridElement();

			// reposition the scrollbars now
			//
			rsr.PositionScrollbar( true );
			nextVisibleRegion.PositionScrollbar( true );

			// set the metrics dirty flag so we rebuild the visible headers collection
			// on the next paint operation
			//
			this.metricsDirty = true;

		}

		/// <summary>
		/// Remove a region from the collection
		/// </summary>
        /// <param name="rsr">The item to remove from the colletion.</param>
		public void Remove( RowScrollRegion rsr )
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if ( grid == null )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_257"));

			// make sure there is at least one other region
			//
			if ( this.Count < 2 )
				throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_258") );

			// make sure that the csr is in the collection
			//
			int index = this.IndexOf( rsr );

			if ( index >= 0 )
			{
				BeforeRowRegionRemovedEventArgs args = new BeforeRowRegionRemovedEventArgs( rsr );

				// fire the before event to see if they want to cancel it
				//
				grid.FireEvent( GridEventIds.BeforeRowRegionRemoved, args );

				// if cancel was set true in the event then
				// just return
				//
				if ( args.Cancel )
					return;

				// unhook the prop change notifications
				//
				rsr.SubObjectPropChanged -= new SubObjectPropChangeEventHandler( this.OnSubObjectPropChanged );

				rsr.Dispose();
			}

			// remove the csr from our internal list
			//
			this.InternalRemove( rsr );

			this.metricsDirty = true;

			// SSP 4/25/03 UWG2190
			// Also dirty the grid element so the elements get repositioned.
			//
			if ( null != this.Layout )
			{
				this.Layout.DirtyGridElement( true );
				this.Layout.UIElement.VerifyChildElements( true );
			}

			this.NotifyPropChange( PropertyIds.Remove );

		}

		// AS - 11/21/01
		// When resetting the collection, we were only resetting the
		// items in the collection and not clearing the collection.
		//

		/// <summary>
		/// Resets all properties back to their default values.
		/// </summary>
		public override void Reset()
		{
			// call the base implementation first
			base.Reset();

			// remove all items except the first
			for (int i = this.List.Count - 1; i > 0; i--)
			{
				RowScrollRegion rsr = (RowScrollRegion)this.List[i];

				rsr.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				rsr.Dispose();

				this.InternalRemove( rsr );
			}

			this.metricsDirty = true;

			this.NotifyPropChange( PropertyIds.Remove );
		}

		internal RowScrollRegion DefaultActiveRegion
		{
			get
			{
				if ( this.Count < 1 )
				{
					Debug.Fail("No Row scrolling regions in CRowScrollRegionsColl::GetDefaultActiveRegion()");
					return null;
				}
    
				// iterate over the collection looking for the first 
				// non-locked region to return
				//
				for ( int i = 0; i < this.Count; i++ )
				{
					RowScrollRegion rsr = this[i];
					if ( rsr.Hidden  )
						continue;

					return rsr;
				}

				return null;
			}
		}

		internal void OnRowDeleted( Infragistics.Win.UltraWinGrid.UltraGridRow deletedRow )
		{
			for ( int i=0; i < this.Count; ++i)
			{
				if ( this[i].FirstRow == deletedRow )
					this[i].SetDestroyVisibleRows( true, true );
			}
		}


		internal void ResetFirstRowIfDescendantOf( RowsCollection rows )
		{
			for ( int i=0; i < this.Count; ++i)
			{
				RowScrollRegion rsr = this[i];

				Infragistics.Win.UltraWinGrid.UltraGridRow row = rsr.FirstRow;

				if ( null == row )
					continue;

				while ( null != row )
				{
					if ( row.ParentCollection == rows )
					{
						// JJD 1/09/02
						// Instead of nulling out the first row set
						// it to the first visible row in the rows collection
						//
						rsr.SetFirstRow( rows.GetFirstVisibleRow() );
						break;
					}

					row = row.ParentRow;
				}
			}
		}


        internal void DirtyAllVisibleRows()
        {
            DirtyAllVisibleRows( false );
        }

		internal void DirtyAllVisibleRows( bool setScrollToBeginning )
        {
            for ( int i=0; i < this.Count; ++i)
            {
                this[i].SetDestroyVisibleRows( true, setScrollToBeginning );
            }
        }

		// Moved this into the SortedColumns class
		//
		


		internal void VerifyAllScrollbarPositions()
        {
            for ( int i = 0; i < this.Count; i++ )
            {
                this[i].PositionScrollbar( true );
            }
        }
     

		/// <summary>
		/// IEnumerable Interface Implementation
        /// </summary>
        /// <returns>A type safe enumerator</returns>
        public new RowScrollRegionEnumerator GetEnumerator() // non-IEnumerable version
        {
           return new RowScrollRegionEnumerator(this);
        }

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.RowScrollRegion[] array, int index)
		{
			base.CopyTo( (System.Array)array, index );
		}
		#endregion //CopyTo

		/// <summary>
		/// inner class implements IEnumerator interface
		/// </summary>
        public class RowScrollRegionEnumerator : DisposableObjectEnumeratorBase
        {   
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="rowScrollRegions">The row scroll regions collection to be enumerated.</param>
            public RowScrollRegionEnumerator(RowScrollRegionsCollection rowScrollRegions) : base( rowScrollRegions )
            {
            }

			/// <summary>
			/// non-IEnumerator version: type-safe
			/// </summary>
            public RowScrollRegion Current
            {
                get
                {
                    return (RowScrollRegion)((IEnumerator)this).Current;
                }
            }
        }
    }
}
