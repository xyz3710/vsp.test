#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Diagnostics;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

	/// <summary>
	///		Summary description for ButtonConnectorUIElement.
	/// </summary>
	public class ButtonConnectorUIElement : UIElement
	{
		
//		/// <summary>
//		/// Constructor.
//		/// </summary>
//		/// <param name="parent">parent UI element</param>
		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>ButtonConnectorUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		//internal ButtonConnectorUIElement( UIElement parent ) : base( parent )
		public ButtonConnectorUIElement( UIElement parent ) : base( parent )
		{
		}

		internal SpecialBoxBase SpecialBox
		{
			get
			{
				return (SpecialBoxBase)this.Parent.GetContext( typeof( SpecialBoxBase ), true );
			}
		}
		
		/// <summary>
		/// <see cref="Infragistics.Win.UIElement.BorderSides"/>
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return Border3DSide.Left | Border3DSide.Bottom;
			}
		}

		

		/// <summary>
		/// Returns or sets a value that determines the border style of an object.
		/// </summary>
		/// <remarks>
		/// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
		/// </remarks>
		public override Infragistics.Win.UIElementBorderStyle BorderStyle
		{
			get
			{
				// SSP 11/21/01 UWG669
				// If ButtonConnectorStyle is set to Default, then resolve to
				// Solid
				//
				//return this.SpecialBox.ButtonConnectorStyle;
				// SSP 3/15/06 - App Styling
				// Use the new ButtonConnectorStyleResolved.
				// 
				
				return this.SpecialBox.ButtonConnectorStyleResolved;
				
				
			}
		}

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{
			//ROBA UWG229 8/24/01
			// this element doesn't draw a background
		}

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// SSP 3/15/06 - App Styling
			// Use the new ButtonConnectorColorResolved.
			// 
			//appearance.BorderColor = this.SpecialBox.ButtonConnectorColor;
			appearance.BorderColor = this.SpecialBox.ButtonConnectorColorResolved;
		}
	}
}
