#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Drawing.Imaging;

	// SSP 3/21/02
	// Added FilterDropDownButtonUIElement class for row filter drop down
	// 

	
	/// <summary>
	/// A column or group swap button UI element
	/// </summary>
	public class FilterDropDownButtonUIElement : UIElement
	{
		#region Private/Internal variables

		internal static int FILTER_DROPDOWN_BUTTON_WIDTH = 13;

		#endregion // Private/Internal variables

		#region Constructor

//#if DEBUG
//		/// <summary>
//		/// Constructor.
//		/// </summary>
//		/// <param name="parent">The parent element</param>
//#endif
		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b></b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		//internal FilterDropDownButtonUIElement( UIElement parent ) : base( parent )
		public FilterDropDownButtonUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region Header

		private HeaderBase Header
		{
			get
			{
				return (HeaderBase)this.GetContext( typeof( HeaderBase ), true );
			}
		}

		#endregion // Header

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>The border style of cells, rows, and headers can be set by the BorderStyleCell, BorderStyleRow, and BorderStyleHeader properties respectively.</para>
        /// <para>The border style of the AddNew box buttons can be set by the ButtonBorderStyle property.</para>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
		public override UIElementBorderStyle BorderStyle
		{
			get
			{
				HeaderBase header = this.Header;

				UltraGrid grid = null;
				if ( null != header )
					grid = header.Band.Layout.Grid as UltraGrid;

				// During design mode, make the button look as if it's not pressable.
				//
				if ( null == grid || grid.DesignMode )
					return UIElementBorderStyle.None;
				
				bool mouseOverBtn = false;
				bool droppedDown = false;

				if ( grid.HasFilterDropDown )
				{
					Infragistics.Win.UltraWinGrid.ColumnHeader.RowFilterDropDownOwner filterOwner
						= grid.FilterDropDown.Owner as Infragistics.Win.UltraWinGrid.ColumnHeader.RowFilterDropDownOwner;

					droppedDown = 
						null != filterOwner &&
						((IValueList)grid.FilterDropDown).IsDroppedDown &&
						// RobA 1/2/02 added check to make sure the owner is this header so
						// that only the button that belongs to this owner is drawn InsetSoft
						//
						filterOwner.ColumnHeader == header;
				}			

				UIElementBorderStyle borderStyle = UIElementBorderStyle.None;
								
				Point mousePosition = this.Control.PointToClient( Control.MousePosition );				
			
				if ( this.PointInElement( mousePosition ) )
					mouseOverBtn = true;

				if ( droppedDown )
				{		
					borderStyle = UIElementBorderStyle.InsetSoft;
				}
				else if ( mouseOverBtn )
				{
					borderStyle = UIElementBorderStyle.RaisedSoft;
				}

				return borderStyle;
			}
		}

		#endregion // BorderStyle

		#region DrawForeground
		
		/// <summary>
		/// Draws small arrow in the foreground
		/// </summary>
		protected override void DrawForeground ( ref UIElementDrawParams drawParams ) 
		{
			Debug.Assert( null != this.Header, "No header context for filter drop down !" );

			if ( null == this.Header )
				return;

			UltraGridLayout layout = this.Header.Band.Layout;

			Debug.Assert( null != layout, "No layout !" );
			if ( null == layout )
				return;
			
			// SSP 7/19/02 UWG1386
			// Insted of calling RectInsideBorders which will change it's size when
			// the button has mouse over it (because then it would have borders).
			// So make the size static so the image doesn't shrink when the mouse
			// is moved over it.
			//
			//Rectangle imageRect = this.RectInsideBorders;
			Rectangle rect = this.Rect;
			int borderWidth = layout.GetBorderThickness( UIElementBorderStyle.RaisedSoft );
			rect.Inflate( -borderWidth, -borderWidth );			

			// SSP 7/19/02 UWG1364
			// Added ActiveFilterDropDownButtonImage property for filter drop down with
			// active filters.
			//
			//Image image = layout.FilterDropDownButtonImage;
			Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader = 
				this.Header as Infragistics.Win.UltraWinGrid.ColumnHeader;

			Debug.Assert( null != columnHeader, "Filter drop down button has no context of a column header! " );

			bool filtersActive = false;

			if ( null != columnHeader )
			{
				RowsCollection rows = (RowsCollection)this.GetContext( typeof( Infragistics.Win.UltraWinGrid.RowsCollection ), true );

				if ( columnHeader.AreFiltersActive( rows ) )
					filtersActive = true;
			}

			Image image = filtersActive
				? layout.FilterDropDownButtonImageActive
				: layout.FilterDropDownButtonImage;

			Debug.Assert( null != image, "No filter drop down button image !" );
			if ( null == image )
				return;

			// SSP 7/19/02 UWG1386
			// Scale the image if necessary because the user could set an image that's
			// larger than the size of the element, in which case we sould scale it.
			// Added below if block.
			//
			int imageWidth  = image.Width;
			int imageHeight = image.Height;
			if ( imageWidth > rect.Width || imageHeight > rect.Height )
			{
				// Scale since the image is bigger than the rect.
				//
				double scaleFactor = Math.Min ( (double)rect.Width / imageWidth, 
													(double)rect.Height / imageHeight );

				imageWidth = (int)( scaleFactor * imageWidth );
				imageHeight = (int)( scaleFactor * imageHeight );
			}

			// Center it horizontally and vertically
			//
			Rectangle imageRect = new Rectangle( 
				rect.X + ( rect.Width - imageWidth ) / 2, 
				rect.Y + ( rect.Height - imageHeight ) / 2, imageWidth, imageHeight );
			
			// Don't do the color maps if the user specified the image.
			//
			if ( image == layout.FilterDropDownButtonImageFromResource || 
				 image == layout.FilterDropDownButtonImageFromResourceActive )
			{
				AppearanceData appData = new AppearanceData( );
				AppearancePropFlags propFlags = 
					AppearancePropFlags.BackColor | 
					AppearancePropFlags.ForeColor | 
					AppearancePropFlags.BackColorDisabled |
					AppearancePropFlags.ForeColorDisabled;
			
				columnHeader.ResolveAppearance( ref appData, propFlags );

				if ( !appData.HasPropertyBeenSet( AppearancePropFlags.BackColor ) )
					appData.BackColor = SystemColors.Control;
				if ( !appData.HasPropertyBeenSet( AppearancePropFlags.ForeColor ) )
					appData.ForeColor = SystemColors.WindowText;
				if ( !appData.HasPropertyBeenSet( AppearancePropFlags.BackColorDisabled ) )
					appData.BackColorDisabled = SystemColors.Control;
				if ( !appData.HasPropertyBeenSet( AppearancePropFlags.ForeColorDisabled ) )
					appData.ForeColorDisabled = SystemColors.WindowText;

				// SSP 8/14/03 UWG2595
				// If the fore color of the appearance data is white which is also the
				// transparent back color of the filter image (later on we are setting
				// the color key to this color) then change it slightly so it doesn't
				// get treated as transparent back color by the SetColorKey(255,255,255)
				// below.
				//
				// ----------------------------------------------------------------------
				Color foreColor = appData.ForeColor;
				if ( 255 == foreColor.R && 255 == foreColor.G && 255 == foreColor.B )
					appData.ForeColor = Color.FromArgb( foreColor.A, 254, 255, 255 );
				// ----------------------------------------------------------------------

				ImageAttributes imageAttr = new ImageAttributes( );
		

				ColorMap cm;
				ColorMap[] cmaps = new ColorMap[ filtersActive ? 1 : 4 ];
				int cmapsIndex = 0;

				cm = new ColorMap( );
				cm.OldColor = Color.FromArgb( 0, 0, 0 );
				cm.NewColor = appData.ForeColor;
				cmaps[ cmapsIndex++ ] = cm;

				// SSP 7/19/02 UWG1364
				// Only do below color map for non-active filter image. A new property
				// for image for active filters was added and that's where this color
				// mapping is being done.
				//
				if ( !filtersActive )
				{
					cm = new ColorMap( );
					cm.OldColor = Color.FromArgb( 120, 120, 120 );
					cm.NewColor = UltraGridLayout.LightenColor( appData.ForeColor, 120 );
					cmaps[ cmapsIndex++ ] = cm;

					cm = new ColorMap( );
					cm.OldColor = Color.FromArgb( 220, 220, 220 );
					cm.NewColor = UltraGridLayout.LightenColor( appData.ForeColor, 220 );
					cmaps[ cmapsIndex++ ] = cm;

					cm = new ColorMap( );
					cm.OldColor = Color.FromArgb( 240, 240, 240 );
					cm.NewColor = UltraGridLayout.DarkenColor( appData.BackColor, 20 );
					cmaps[ cmapsIndex++ ] = cm;
				}

				// SSP 9/23/02 UWG1631
				// Instead of mapping to the back color of the header, set the color key
				// because in themed environment in winxp, the back color of the header
				// may be different than the back color in the appearance data. So make
				// it transparent.
				//
				
				imageAttr.SetColorKey( Color.FromArgb( 255, 255, 255 ), Color.FromArgb( 255, 255, 255 ) );

				imageAttr.SetRemapTable( cmaps );

				// AS 12/19/02 UWG1831
				// Use the DrawImage of the draw params so it can be alphablended if needed.
				//
				//drawParams.Graphics.DrawImage( image, imageRect, 0, 0, imageRect.Width, imageRect.Height, GraphicsUnit.Pixel, imageAttr );
				drawParams.DrawImage( image, imageRect, new Rectangle(Point.Empty, imageRect.Size), imageAttr );

				// SSP 9/23/02 UWG1631
				// Also dispose of the image attribute once finished.
				//
				if ( null != imageAttr )
					imageAttr.Dispose( );
				imageAttr = null;

				return;
			}
			
			// AS 12/19/02 UWG1831
			// Use the DrawImage of the draw params so it can be alphablended if needed.
			//
			//drawParams.Graphics.DrawImage( image, imageRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel );
			// SSP 3/6/03 UWG2051
			//
			//drawParams.DrawImage( image, imageRect, new Rectangle(Point.Empty, imageRect.Size), null );
			drawParams.DrawImage( image, imageRect, new Rectangle( Point.Empty, image.Size ), null );
		}

		#endregion // DrawForeground

		#region DrawBackColor

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{			
		}

		#endregion // DrawBackColor

		#region DrawImageBackground

		/// <summary>
		/// this element doesn't draw an image background
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{			
		}

		#endregion // DrawImageBackground

		#region DrawFocus

		/// <summary>
		/// does nothing as SwapButtonUIElement does not get a focus
		/// </summary>
		protected override void DrawFocus ( ref UIElementDrawParams drawParams )
		{			
			// doesn't need a focus
		}

		#endregion // DrawFocus
		
		#region OnMouseEnter

		/// <summary>
		/// called when mouse enters this element
		/// </summary>
		protected override void OnMouseEnter( )
		{
			
			// SSP 8/16/01 UWG95
			// We need to call the base class so that mouseDown 
			// and mouseDownOverButton state vars in the base calss
			// get set to correct values properly
			//
			base.OnMouseEnter( );
			
			this.Invalidate();
		}

		#endregion // OnMouseEnter

		#region OnMouseLeave

		/// <summary>
		/// called when the mouse leaves this element
		/// </summary>
		protected override void OnMouseLeave( )
		{
			// SSP 8/16/01 UWG95
			// We need to call the base class so that mouseDown 
			// and mouseDownOverButton state vars in the base calss
			// get set to correct values properly
			//
			base.OnMouseLeave( );

			this.Invalidate();			
		}

		#endregion // OnMouseLeave

		#region OnMouseDown

		/// <summary>
		/// Called when the mouse down message is received over the element. 
		/// </summary>
		/// <param name="e">Mouse event arguments</param>
		/// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
		/// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
		/// <returns>If true then bypass default processing</returns>
		protected override bool OnMouseDown( MouseEventArgs e, 
			bool adjustableArea,
			ref UIElement captureMouseForElement )
		{
			// Exit the edit mode before proceeding.
			//
			if ( null != this.Header &&
				null != this.Header.Band.Layout.ActiveCell &&
				this.Header.Band.Layout.ActiveCell.IsInEditMode )
			{
				// If exitting the edit mode is cancelled, then return without
				// proceeding.
				//
				if ( this.Header.Band.Layout.ActiveCell.ExitEditMode( ) )
					return true;
			}
				
			if ( this.Enabled )
			{
				HeaderBase header = this.Header;
				Debug.Assert( null != header, "no header in FilterDropDownButtonUIElement.OnClick" );

				if ( null == header )
					return true;
				
				HeaderUIElement headerElement = (HeaderUIElement)this.GetAncestor( typeof ( HeaderUIElement ) );	
				 
				Debug.Assert( null != headerElement, "no HeaderUIElement in FilterDropDownButtonUIElement.OnClick()" );
				
				if ( null != headerElement )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Remove unused locals
					//Rectangle rect = headerElement.Rect;
                        
					header.OnClickFilterDropDown( headerElement, Keys.Shift == ( Control.ModifierKeys & Keys.Shift )  );
				}
			}				

			captureMouseForElement = null;

			this.Invalidate( );
			return true;
		}

		#endregion // OnMouseDown
		
	}
}
