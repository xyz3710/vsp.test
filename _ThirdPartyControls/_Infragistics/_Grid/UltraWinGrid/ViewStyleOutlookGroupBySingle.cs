#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;

	internal class ViewStyleOutlookGroupBySingle : ViewStyleSingle
	{
		internal ViewStyleOutlookGroupBySingle( Infragistics.Win.UltraWinGrid.UltraGridLayout layout ) : base( layout )
		{
		}


		internal override bool IsOutlookGroupBy 
		{ 
			get
			{
				return true; 
			}
		}


		internal override bool IsLastRow ( UltraGridRow row )
		{
			// SSP 11/11/03 Add Row Feature
			//
			//if ( row.HasNextSibling(false, true) )
			if ( row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) )
				return false;

			// SSP 11/11/03 Add Row Feature
			//
			//if ( row.Expanded && row.HasChild(true) )
			if ( row.Expanded && row.HasChild( true, IncludeRowTypes.SpecialRows ) )
				return false;

			while ( null != row.ParentRow )
			{
				// SSP 11/11/03 Add Row Feature
				//
				//if ( row.ParentRow.HasNextSibling(false, true) )
				if ( row.ParentRow.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) )
					return false;

                row = row.ParentRow;
			}

			return true;
		}


		internal override void AdjustExclusiveMetrics( Infragistics.Win.UltraWinGrid.ColScrollRegion csr,
			                                            HeaderBase header,
			                                            HeaderBase priorHeader)
		{
			// JJD 1/23/02 - UWG971
			// Add groupby indent to first header origin
			//
			int nOrigin;
		    
			if ( null != priorHeader )
				nOrigin = priorHeader.Extent +  priorHeader.Origin;
			else
				nOrigin = this.CalculateGroupByIndent( header.Band );

			this.SetExclusiveMetrics(header, nOrigin);		    
		}


		internal override void AdjustBandOrigin( UltraGridBand band, 
			UltraGridBand priorBand,
			ref int origin )
		{	
			origin = 0;

			int groupByIndent = 0;

			if ( band.HasGroupBySortColumns )
			{
				// JJD 1/23/02 - UWG971
				// Moved groupby indent calculation logic into helper method
				//
				groupByIndent = this.CalculateGroupByIndent( band );
			}

			origin += groupByIndent;
		}

		internal override bool SupportsGroupByRows
		{
			get
			{
				return true;
			}
		}

	}
}
