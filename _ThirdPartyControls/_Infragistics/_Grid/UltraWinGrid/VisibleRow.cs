#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
	using System.Collections;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;


	/// <summary>
    /// This class reperesents an instance of a Row within
    /// a specific RowScrollRegion
    /// </summary>
    public class VisibleRow : SubObjectBase
    {
        private Infragistics.Win.UltraWinGrid.UltraGridRow row = null;
        private VisibleRow parentVisibleRow = null;
		private int tier = 0;
		private int origTop = 0;
		private int postRowSpacing;
		private Infragistics.Win.UltraWinGrid.ViewStyleSpecialFlags specialFlags = 0;

		private int top = 0;
		private bool hasHeader;
        private bool hookedIntoRowNotifications = false;
		private bool cardArea = false;

		private RowScrollRegion rowScrollRegion = null;
			        
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//VisibleRow nextCrossBandSibling = null;
		//VisibleRow prevCrossBandSibling = null;

		// SSP 4/29/05 - NAS 5.2 Fixed Rows_
		//
		private DefaultableBoolean isFullyVisibleOverride = DefaultableBoolean.Default;
		private int clipBottomTo = -1;
		
		/// <summary>
		/// contructor
		/// </summary>
		/// <param name="rowScrollRegion"></param>
		public VisibleRow( RowScrollRegion rowScrollRegion )
        {
			this.rowScrollRegion = rowScrollRegion;
        }

		/// <summary>
		/// True if this represents a cardarea instead of a row (read-only)
		/// </summary>
		public bool IsCardArea { get { return this.cardArea; } }
 
		internal void UnhookFromRowNotifications()
        {

            // unhook us form the old row's notifications
            //
            if ( this.hookedIntoRowNotifications )
            {
				if ( this.row != null )
				{
					this.row.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

					// SSP 11/4/03 UWG2736
					// Hook into the SubObjectDisposed of the row so if for some reason the row
					// gets disposed off, we can unhook from it.
					//
					this.row.SubObjectDisposed -= this.RowDisposedEventHandler;

					// SSP 8/16/04 - Optimizations
					// 
					this.row.DecrementVisibleRowCounter( );
				}

                this.hookedIntoRowNotifications = false;
            }

        }

		// SSP 4/12/04
		// Added InternalSetRowScrollRegion method.
		//
		internal void InternalSetRowScrollRegion( RowScrollRegion rowScrollRegion )
		{
			this.rowScrollRegion = rowScrollRegion;
		}

		internal void Initialize( VisibleRow parentVisibleRow,
                                  Infragistics.Win.UltraWinGrid.UltraGridRow row )
        {
			// SSP 4/29/05 - NAS 5.2 Fixed Rows_
			// Reset the isFullyVisibleOverride to Default. The ViewStyle visible row generation
			// logic will set it.
			//
			this.isFullyVisibleOverride = DefaultableBoolean.Default;

            if ( parentVisibleRow == null )
            {
                if ( row.BandInternal.ParentBand != null )
                    throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_303") );
            }
            else
            {
				// SSP 9/13/01 
				if ( !( parentVisibleRow.Row is UltraGridGroupByRow ) && parentVisibleRow.Band != row.BandInternal.ParentBand )
                //if ( parentVisibleRow.Band != row.Band.ParentBand )
                    throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_304") );
            }

            // unhook us form the old row's notifications
            //
			this.UnhookFromRowNotifications();

            this.parentVisibleRow = parentVisibleRow;

			// JJD 12/11/01
			// In cardview we only add the first row since the single VisibleRow
			// actually represents the entire cardview island of cards
			//
			if ( row != null && row.IsCard )
			{
				this.row		= row.ParentCollection.GetFirstVisibleRow();

				// SSP 6/7/02
				// CVMS Row filtering.
				// When all the rows are filtered out, they all get hidden. We
				// still add a row to show the headers in this situation so the
				// user can unfiler the rows. In such a situation where all
				// rows are filtered out, GetFirstVisibleRow will return null, 
				// since there are no visible rows. So in that case we want to 
				// use the row that was passed in. It will be the first row.
				// This should effectively solve the problem.
				//
				if ( null == this.row )
				{
					this.row = row;
				}

				this.cardArea	= true;
				
				// JJD 12/11/01
				// Assume that card areas have a band header
				//
				this.hasHeader	= true;
			}
			else
			{
				this.row		= row;
				this.cardArea	= false;
			}

            this.top    = 0;
            this.tier   = 0;

            // hook us into the new row's notifications
            //
            if ( this.row != null && !row.IsCard )
            {
                this.row.SubObjectPropChanged += this.SubObjectPropChangeHandler;

				// SSP 11/4/03 UWG2736
				// Hook into the SubObjectDisposed of the row so if for some reason the row
				// gets disposed off, we can unhook from it.
				//
				this.row.SubObjectDisposed += this.RowDisposedEventHandler;
                
				this.hookedIntoRowNotifications = true;

				// SSP 8/16/04 - Optimizations
				// 
				this.row.IncrementVisibleRowCounter( );
            }
            else
                this.hookedIntoRowNotifications = false;

			// SSP 5/2/05 - NAS 5.2 Fixed Rows_
			// 
			this.ResetClipBottomTo( );
        }

		// SSP 11/4/03 UWG2736
		// Hook into the SubObjectDisposed of the row so if for some reason the row
		// gets disposed off, we can unhook from it.
		//
		// ----------------------------------------------------------------------------
		private void OnRowDisposed( SubObjectBase obj )
		{
			this.UnhookFromRowNotifications( );
		}

		SubObjectDisposedEventHandler rowDisposedEventHandler = null;
		private SubObjectDisposedEventHandler RowDisposedEventHandler
		{
			get
			{
				if ( null == this.rowDisposedEventHandler )
					this.rowDisposedEventHandler = new SubObjectDisposedEventHandler( this.OnRowDisposed );
					
				return this.rowDisposedEventHandler;
			}
		}
		// ----------------------------------------------------------------------------

        /// <summary>
        /// The scrolling region containing this visible row (read-only).
        /// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a RowScrollRegion object that can be used to set properties of, and invoke methods on, the rowscrollregion to which the row belongs. You can use this reference to access any of the returned rowscrollregion's properties or methods.</p>
		/// </remarks>
        public RowScrollRegion RowScrollRegion
        {
            get
            {
                return this.rowScrollRegion;
            }
        }

        /// <summary>
        /// The parent UltraGridRow object of the visible row (read-only).
        /// </summary>
        public VisibleRow ParentVisibleRow
        {
            get
            {
                return this.parentVisibleRow;
            }
        }


        /// <summary>
        /// The UltraGridRow object associated with the visible row (read-only).
        /// </summary>
		/// <remarks>
		/// <p class="body">The <b>Row</b> property of an object refers to a specific row in the grid as defined by an UltraGridRow object. You use the <b>Row</b> property to access the properties of a specified UltraGridRow object, or to return a reference to the UltraGridRow object that is associated with the current object.</p>
		/// <p class="body">An UltraGridRow object represents a single row in the grid that displays the data from a single record in the underlying data source. The UltraGridRow object is one mechanism used to manage the updating of data records either singly or in batches (the other is the UltraGridCell object). When the user is interacting with the grid, one UltraGridRow object is always the active row, and determines both the input focus of the grid and the position context of the data source to which the grid is bound.</p>
		/// </remarks>
        public Infragistics.Win.UltraWinGrid.UltraGridRow Row
        {
            get
            {
                return this.row;
            }
        }

        /// <summary>
        /// The origin of this ScrollRegionBase in 
        /// coordinates relative to the row scroll region
        /// </summary>
        public int Origin
        {
            get
            {
                return this.Top;
            }
        }

		// JJD 10/04/01
		// No one was using Extent so commented it out
		//


		// SSP 4/28/05 - NAS 5.2 Fixed Rows_
		// VisibleRow.IsFirstRow and VisibleRow.IsAbsoluteFirstRow( ) and ViewStyleBase.IsFirstRow( )
		// all do the same. Just use the IsAbsoluteFirstRow instead.
		//
		

		internal bool HasHeader
		{
			get
			{
				if ( this.hasHeader )
				{
					// JJD 1/24/99 - ult30
					// check to make sure the band has a header height first
					//
					if ( this.row.BandInternal.GetTotalHeaderHeight() < 1 )
						return false;
				}

				return hasHeader; 
			}
			set
			{
				// JJD 12/11/01
				// Assume that card areas have a band header
				//
				if ( this.IsCardArea )
				{
					this.hasHeader = true;
				}
					// SSP 4/16/05 - NAS 5.2 Summaries Extention
					// Summary footer never has a header with it. Added following else-if block.
					//
					// SSP 7/6/05 - NAS 5.3 Empty Rows
					// Moved HasHeadersOverride from the SummaryRow to UltraGridRow since the 
					// UltraGridEmptyRow needs to override this too.
					// 
				//else if ( this.row is UltraGridSummaryRow )
				else if ( null != this.row )
				{
					// SSP 7/6/05 - NAS 5.3 Empty Rows
					// 
					//UltraGridSummaryRow summaryRow = (UltraGridSummaryRow)this.row;
					//switch ( summaryRow.HasHeadersOverride )
					switch ( this.row.HasHeadersOverride )
					{
						case DefaultableBoolean.Default:
							this.hasHeader = value;
							break;
						case DefaultableBoolean.False:
							this.hasHeader = false;
							break;
						case DefaultableBoolean.True:
							this.hasHeader = true;
							break;
					}
				}
				else
				{
					this.hasHeader = value;
				}
			}
		}
									  
		internal int GetTopOfRow ( )
		{
			return this.GetTopOfRow( true );
		}
		internal int GetTopOfRow ( bool adjustForOverlappedBorders )
		{				
			int topOfRow = this.top;

			if ( this.HasHeader )
			{
				UltraGridBand band = this.row.BandInternal;

				topOfRow += band.GetTotalHeaderHeight();
			}

			// If this is a double top border situation then adjust the top of row
			// so that the header's bottom border overlaps the top row border
			//
			if ( adjustForOverlappedBorders && this.SkipDblTopBorderDisplay() )
				topOfRow--;

			return topOfRow;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Sets the top of the row (after its header)
		//        /// </summary>
		//#endif
		//        internal void SetTopOfRow ( int top )
		//        {
		//            this.SetTopOfRow( top, true );
		//        }

		#endregion Not Used

		internal void SetTopOfRow ( int top, bool adjustForOverlappedBorders )
        {
            if ( this.HasHeader )
                this.top    = top - this.Band.GetTotalHeaderHeight();
            else
                this.top    = top;

            // If this is a double top border situation then adjust the top of row
            // so that the header's bottom border overlaps the top row border
            //
            if ( adjustForOverlappedBorders && this.SkipDblTopBorderDisplay() )
                this.top++;

        }
		internal bool SkipDblTopBorderDisplay() 
        {
            // JJD 4/24/01
            // Since RowSpacingBefore is now going to be between
            // the row and its header we never want to overlap the top
            // border if RowSpacingBefore is > zero
            //
            if ( this.row.RowSpacingBeforeResolved > 0 )
                return false;

			// SSP 10/12/01 UWG461
			// We want to return false for group by rows, since they don't share
			// borders
			//
			if ( this.Row is UltraGridGroupByRow )
				return false;

			// Evaluate the final set of conditions
			bool finalConditions = ( this.HasHeader || 
								   ( 0 == this.tier && this.Band.GetTotalHeaderHeight() > 0 ) ||
								   ( ViewStyleSpecialFlags.SkipTopBorder & this.specialFlags ) != 0 ) ;

			if (this.Row.IsCard)
				return finalConditions &&
					   this.Band.Layout.CanMergeAdjacentBorders(this.Band.BorderStyleCardAreaResolved);
			else
                return finalConditions && this.Band.ShareRowBorders;

        }

		internal bool IsAbsoluteFirstRow
		{
			get
			{
				// SSP 4/28/05 - NAS 5.2 Fixed Rows_
				// Use IsAbsoluteFirstRow off the row. It does the same thing.
				//
				return this.row.IsAbsoluteFirstRow( );
				
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Returns true if this row is the last row in the rowset.
		//        /// </summary>
		//        /// <param name="expandedOnly"></param>
		//        /// <returns></returns>
		//#endif
		//        internal bool IsAbsoluteLastRow( bool expandedOnly )
		//        {			
		//            // the last row cannot have any next siblings
		//            //
		//            // SSP 11/11/03 Add Row Feature
		//            //
		//            //if ( this.row.HasNextSibling( false, true ) ||
		//            if ( this.row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) ||
		//                 this.row.HasSiblingInNextSiblingBand(true) )
		//                return false;

		//            // if the row passed the above text and it's band
		//            // doesn't have any child bands then it is the last
		//            // row so return true
		//            //
		//            // SSP 5/17/04 UWG3330
		//            // If the row is a group-by row then it has child rows even though the associated
		//            // band may not have any child bands.
		//            //
		//            //if ( !this.Band.HasChildBands() )
		//            if ( !this.Band.HasChildBands() && ! ( this.row is UltraGridGroupByRow ) )
		//                return true;

		//            // we are interested in expanded rows only or the
		//            // row is expanded then return false if the row
		//            // has any child rows
		//            // 
		//            if ( !expandedOnly || this.row.Expanded )
		//                // SSP 11/11/03 Add Row Feature
		//                //
		//                //return !this.row.HasChild(true); /* .GetHasChildren(true); */
		//                return !this.row.HasChild( true, IncludeRowTypes.SpecialRows ); /* .GetHasChildren(true); */

		//            return true;
		//        }

		#endregion Not Used
		
		internal UltraGridBand Band
		{
			get
			{
				return this.row.BandInternal;
			}
		}
						 
		internal int Bottom
		{
			get
			{
				// JJD 12/11/01
				// The bottom is calculated differently for card areas 
				//
				if ( this.IsCardArea )
					return this.top + this.GetTotalHeight();

				return this.GetTopOfRow() + this.row.BaseHeight;
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Get bottom in client coords.
		//        /// </summary>
		//#endif
		//        internal int BottomInClient
		//        {
		//            get
		//            {
		//                // SSP 5/17/04 UWG3245
		//                // Why are we substracting 1 ? I don't think we should be substracting 1
		//                // here. It causes UWG3245 bug.
		//                //
		//                //return this.Bottom + this.rowScrollRegion.Origin - 1;				
		//                return this.Bottom + this.rowScrollRegion.Origin;
		//                //return  GetBottom() + m_ElemRowSink.GetOrigin() - 1; 				
		//            }
		//        }

		#endregion Not Used

		// SSP 11/5/04 UWG3771
		// Added BottomInClientOverall method.
		//
		internal int BottomInClientOverall
		{
			get
			{
				return this.GetTopInClient( ) + this.GetTotalHeight( );
			}
		}

		#region BottomOverall

		// SSP 4/29/05 - NAS 5.2 Fixed Rows_
		// 
		internal int BottomOverall
		{
			get
			{
				return this.GetTop( ) + this.GetTotalHeight( );
			}
		}

		#endregion // BottomOverall

		#region IsFullyVisibleOverride 

		// SSP 4/29/05 - NAS 5.2 Fixed Rows_
		// Added IsFullyVisibleOverride on the VisibleRow.
		//
		internal DefaultableBoolean IsFullyVisibleOverride 
		{
			get
			{
				return this.isFullyVisibleOverride;
			}
			set
			{
				this.isFullyVisibleOverride = value;
			}
		}

		#endregion // IsFullyVisibleOverride 

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        // SSP 12/10/03
		//        // Added BottomWithRowSpacingInClient property. Apparently Bottom property above does 
		//        // not take into account the row spacing before/after settings. We need to do that for 
		//        // RowScrollRegion.IsLastRowVisible method. Chose not to modify the existing Bottom 
		//        // property above in the interest of minimizing introducing any bugs since it's being 
		//        // used from quite a few different places, including the view styles logic.
		//        // 
		//#if DEBUG
		//        /// <summary>
		//        /// Returns the bottom of the row with row spacing before and row spacing after taken into account.
		//        /// </summary>
		//#endif
		//        internal int BottomWithRowSpacingInClient
		//        {
		//            get
		//            {
		//                int bottom = this.BottomInClient;

		//                if ( null != this.row && ! this.IsCardArea )
		//                    bottom += this.row.RowSpacingBeforeResolved + this.row.RowSpacingAfterResolved;

		//                return bottom;
		//            }
		//        }

		#endregion Not Used

        /// <summary>
        /// Returns the top coordinate of the row relative to the row scroll region.
        /// </summary>
        public int Top
        {
		    get
		    {
    			return this.top;
            }
		}

        internal int GetTop ( )
        {
            return this.top;
        }


		internal int SetTop ( int top ) 
		{
			int temp = this.top;
			this.top = this.origTop = top;
			return temp;
		}
		
		internal int GetTotalHeight ( )
		{
			return this.GetTotalHeight( true );
		}

		// MD 7/26/07 - 7.3 Performance
		// Logic in this method was changed and now it always returns null, it has been removed
		#region Removed

		//internal SummaryValuesCollection[] GetSummaryFootersToBeDisplayed( UltraGridRow row )
		//{
		//    // SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		//    // Added ability to display summaries in group-by rows aligned with columns, 
		//    // summary footers for group-by row collections, fixed summary footers and
		//    // being able to display the summary on top of the row collection.
		//    // Return null since now summary footers have their own row associated with
		//    // them.
		//    //
		//    return null;
		//    /*
		//    SummaryDisplayAreaContext bottomSummariesContext = SummaryDisplayAreaContext.BottomScrollingSummariesContext;

		//    // SSP 11/11/03 Add Row Feature
		//    //
		//    //if ( hasSummaryFooter && row.HasNextSibling( false, true ) )
		//    if ( row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) )
		//        return null;

		//    // SSP 7/10/03
		//    // Summaries are not supported in card-view.
		//    //
		//    if ( row.IsCard )
		//        return null;

		//    ViewStyleBase viewStyle = this.Band.Layout.ViewStyleImpl;
		//    bool hasSummaryFooter = true;

		//    // If the row has any children, then it shouldn't have the summary footer.
		//    //
		//    if ( hasSummaryFooter && row.Expanded && !row.IsCard )
		//    {
		//        bool hasVisibleChild = false;

		//        if ( null != row.ChildBands )
		//        {
		//            for ( int i = 0; i < row.ChildBands.Count; i++ )
		//            {
		//                UltraGridChildBand childBand = row.ChildBands[i];

		//                if ( childBand.Band.HiddenResolved )
		//                    continue;

		//                // SSP 11/11/03 Add Row Feature
		//                //
		//                //hasVisibleChild = row.HasChild( childBand.Band, true );
		//                hasVisibleChild = row.HasChild( childBand.Band, true, IncludeRowTypes.SpecialRows );

		//                // With filtering enabled, we allow displaying the column headers
		//                // even if all the rows are filtered out.
		//                //
		//                // SSP 9/2/03 UWG2330
		//                // Use the new helper method instead. Commented out the original code and added new one.
		//                //
		//                // ----------------------------------------------------------------------------------------
		//                //if ( !hasVisibleChild && childBand.Rows.Count > 0 && 
		//                //	viewStyle.ShouldApplySpecialRowFiltersCode( ) )
		//                //	hasVisibleChild = true;
		//                UltraGridRow discard;
		//                if ( ! hasVisibleChild && childBand.Rows.Band.ShouldAddFilteredOutFirstChildRow( 
		//                    viewStyle, this.RowScrollRegion, childBand.Rows, out discard ) )
		//                    hasVisibleChild = true;
		//                // ----------------------------------------------------------------------------------------

		//                // Once we know that there is a visible child then there is
		//                // no need to process further.
		//                //
		//                if ( hasVisibleChild )
		//                    break;
		//            }
		//        }

		//        if ( hasVisibleChild )
		//            hasSummaryFooter = false;
		//    }

		//    if ( hasSummaryFooter )
		//    {
		//        ArrayList list = new ArrayList( );

		//        // We don't add summary footers for group-by rows collections. So if
		//        // the parent collection is not group-by rows collection and it has
		//        // summary values, then add that collection to the list.
		//        //
		//        if ( !row.ParentCollection.IsGroupByRows && 
		//            // SSP 7/18/02 UWG1389
		//            // Use the HasVisibleSummaryValues instead of HasSummaryValues
		//            //
		//            //row.ParentCollection.HasSummaryValues 
		//            // SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		//            // Added ability to display summaries in group-by rows aligned with columns, 
		//            // summary footers for group-by row collections, fixed summary footers and
		//            // being able to display the summary on top of the row collection.
		//            //
		//            //row.ParentCollection.HasVisibleSummaryValues 
		//            row.ParentCollection.HasVisibleSummaryValues( bottomSummariesContext ) 
		//            )
		//            list.Add( (object)row.ParentCollection.SummaryValues );

		//        // Traverse through ancestor parent collections to see if we need to also
		//        // add their summary footers.
		//        // ____________________________________
		//        // |___Band 0 Row______________________|
		//        //	   ____________________________________
		//        //    |___Band 1 Row_______________________|     // Band 1 row has summary footer
		//        //	   ____________________________________      // for both band 1 and band 0 after 
		//        //    |___Band 1 Summary Footer____________|     // it
		//        // ____________________________________
		//        // |___Band 0 Summary Footer Row_______|
		//        //
		//        UltraGridRow currentRow = row;
		//        UltraGridRow parentRow = currentRow.ParentRow;
		//        while ( null != parentRow )
		//        {
		//            // If the parent row has a visible sibling, then it's parent row  collection's 
		//            // summaries will be displayed after the last sibling of parent row and we
		//            // don't have to worry about ancestor parent row collections so just break
		//            // out of the loop.
		//            // 
		//            // SSP 11/11/03 Add Row Feature
		//            //
		//            //if ( parentRow.HasNextSibling( false, true ) )
		//            if ( parentRow.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) )
		//                break;

		//            bool hasNextSiblingBandWithVisbleRows = false;

		//            Debug.Assert( null != parentRow.ChildBands, "No child bands when it has a child row !" );

		//            if ( null != parentRow.ChildBands )
		//            {
		//                for ( int i = 1 + currentRow.ParentChildBand.Index; i < parentRow.ChildBands.Count; i++ )
		//                {
		//                    UltraGridChildBand childBand = parentRow.ChildBands[i];

		//                    if ( childBand.Band.HiddenResolved )
		//                        continue;

		//                    // SSP 11/11/03 Add Row Feature
		//                    //
		//                    //hasNextSiblingBandWithVisbleRows = parentRow.HasChild( childBand.Band, true );
		//                    hasNextSiblingBandWithVisbleRows = parentRow.HasChild( childBand.Band, true, IncludeRowTypes.SpecialRows );

		//                    // With filtering enabled, we allow displaying the column headers
		//                    // even if all the rows are filtered out.
		//                    //
		//                    // SSP 9/2/03 UWG2330
		//                    // Use the new helper method instead. Commented out the original code and added new one.
		//                    //
		//                    // ----------------------------------------------------------------------------------------
		//                    //if ( !hasNextSiblingBandWithVisbleRows && childBand.Rows.Count > 0 && 
		//                    //	viewStyle.ShouldApplySpecialRowFiltersCode( ) )
		//                    //	hasNextSiblingBandWithVisbleRows = true;
		//                    UltraGridRow discard = null;
		//                    if ( ! hasNextSiblingBandWithVisbleRows && childBand.Rows.Band.ShouldAddFilteredOutFirstChildRow(
		//                        viewStyle, this.RowScrollRegion, childBand.Rows, out discard ) )
		//                        hasNextSiblingBandWithVisbleRows = true;
		//                    // ----------------------------------------------------------------------------------------

		//                    if ( hasNextSiblingBandWithVisbleRows )
		//                        break;
		//                }
		//            }

		//            if ( hasNextSiblingBandWithVisbleRows )
		//                break;

		//            // We don't add summaries footers for group-by rows collections. So if
		//            // the parent collection is not group-by rows collection and it has
		//            // summary values, then add that collection to the list.
		//            //
		//            if ( !parentRow.ParentCollection.IsGroupByRows &&
		//                // SSP 7/18/02 UWG1389
		//                // Use the HasVisibleSummaryValues instead of HasSummaryValues
		//                //
		//                //parentRow.ParentCollection.HasSummaryValues
		//                // SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		//                // Added ability to display summaries in group-by rows aligned with columns, 
		//                // summary footers for group-by row collections, fixed summary footers and
		//                // being able to display the summary on top of the row collection.
		//                //
		//                //parentRow.ParentCollection.HasVisibleSummaryValues 
		//                parentRow.ParentCollection.HasVisibleSummaryValues( bottomSummariesContext )
		//                )
		//                list.Add( (object)parentRow.ParentCollection.SummaryValues );

		//            currentRow = parentRow;
		//            parentRow  = currentRow.ParentRow;
		//        }

		//        if ( null != list && list.Count > 0 )
		//            return ( SummaryValuesCollection[] )list.ToArray( typeof( SummaryValuesCollection ) );
		//    }

		//    return null;
		//    */
		//}

		#endregion Removed

		// MD 7/26/07 - 7.3 Performance
		// GetSummaryFootersToBeDisplayed was changed and always returns null and consequently, this will always return 0
		#region Removed

		//internal int GetHeightForSummariesHelper( )
		//{
		//    SummaryValuesCollection[] summaryValuesArr = this.GetSummaryFootersToBeDisplayed( this.Row );

		//    int height = 0;
		//    if ( null != summaryValuesArr )
		//    {
		//        // SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		//        // Added ability to display summaries in group-by rows aligned with columns, 
		//        // summary footers for group-by row collections, fixed summary footers and
		//        // being able to display the summary on top of the row collection.
		//        //
		//        SummaryDisplayAreaContext bottomSummariesContext = SummaryDisplayAreaContext.BottomScrollingSummariesContext;

		//        for ( int i = 0; i < summaryValuesArr.Length; i++ )
		//        {					
		//            // SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		//            // Added ability to display summaries in group-by rows aligned with columns, 
		//            // summary footers for group-by row collections, fixed summary footers and
		//            // being able to display the summary on top of the row collection.
		//            //
		//            //int footerHeight = summaryValuesArr[i].GetSummaryFooterHeight( );
		//            int footerHeight = summaryValuesArr[i].GetSummaryFooterHeight( bottomSummariesContext );

		//            if ( footerHeight <= 0 )
		//                continue;

		//            // Allow for 1 pixel spacing between the rows and the summary footer.
		//            //
		//            if ( 0 == height )
		//                height++;

		//            height += footerHeight;

		//            // If we are going to have multiple summary footers, then allow
		//            // for spacing in-between.
		//            //
		//            if ( i > 0 || ( null != this.Row && this.Row.Band != summaryValuesArr[i].Band ) )
		//            {
		//                height += UltraGridBand.INTER_SUMMARY_FOOTER_SPACING;
		//            }
		//        }
		//    }

		//    return height;
		//}

		#endregion Removed


		internal int GetTotalHeight ( bool adjustForOverlappedBorders  )
		{
			// SSP 3/21/02
			// If the associated row is hidden, then return 0.
			// This is necessary for row filtering.
			//
			if ( !this.IsCardArea && null != this.row && this.row.HiddenResolved )
			{
				int height = this.HasHeader ? this.row.Band.GetTotalHeaderHeight() : 0;

				// SSP 9/2/03 UWG2330
				// Now the fix for UWG2330 involves filtering out group-by rows as well. However
				// if the user filters out all group-by rows then we have to display a dummy
				// group-by row that's hidden.
				//
				// ------------------------------------------------------------------------------
				if ( this.row is UltraGridGroupByRow )
					height += this.row.TotalHeight;
				// ------------------------------------------------------------------------------

				// MD 7/26/07 - 7.3 Performance
				// GetHeightForSummariesHelper was removed because it always returns 0 now
				//return height + this.GetHeightForSummariesHelper( );
			}

            int totalHeight = 0;

			// JJD 12/11/01
			// If this is a card area its height is calculated differently
			//
			if ( this.IsCardArea )
			{
				int availableHeight = this.RowScrollRegion.Extent - this.row.Band.GetTotalHeaderHeight();

				availableHeight -= this.Top;

				// JM 01-07-02 Subtract out the height of the horizontal scrollbar
				//			   if it's possible that we have one. (don't bother
				//			   checking to see if we really have one)
				if (this.RowScrollRegion.IsLastVisibleRegion	&&
					!this.Band.Layout.AutoFitAllColumns			&&
					(this.Band.Layout.Scrollbars == UltraWinGrid.Scrollbars.Both		||
					 this.Band.Layout.Scrollbars == UltraWinGrid.Scrollbars.Automatic	||
					 this.Band.Layout.Scrollbars == UltraWinGrid.Scrollbars.Horizontal))
				{
					availableHeight -= SystemInformation.HorizontalScrollBarHeight;
				}

				// if this is a band 0 row and there are no groupby rows 
				// in the band then make the row occupy the entire region
				//
				if ( this.row.Band.Index == 0 &&
					 !( this.row.Band.HasGroupBySortColumns &&
					    this.row.Layout.ViewStyleImpl.SupportsGroupByRows ) )
				{
					totalHeight = availableHeight;
					adjustForOverlappedBorders = false;
				}
				else
				{
					// SSP 7/20/06 BR11460
					// VisibleRow.GetTotalHeight sets the CardAreaHeight to available height. This overrides the
					// height that may have been explicitly set. We need to maintain the user set CardAreaHeight.
					// 
					this.row.ParentCollection.ResetCardAreaHeightToExplicitlySetValue( );

					totalHeight = this.row.ParentCollection.CardAreaHeight;

					// SSP 10/25/06 BR16003
					// Take into account the row spacing before and after settings.
					// 
					int rowSpacingBefore = this.row.RowSpacingBeforeResolved;
					int rowSpacingAfter = this.row.RowSpacingAfterResolved;
					totalHeight += rowSpacingBefore + rowSpacingAfter;

					// SSP 10/25/06 BR16003
					// Take into account the row spacing before and after settings.
					// 
					//if ( totalHeight > availableHeight )
					if ( totalHeight - rowSpacingAfter > availableHeight )
					{
						// For integral height card styles we should set the height
						// based on the the number cards that can fit. Otherwise,
						// for variable height use all available height.
						//
						// SSP 2/28/03 - Row Layout Functionality
						// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
						// to StandardLabels card style so use the StyleResolved instead of the Style property.
						//
						if ( this.row.Band.CardSettings.StyleResolved == CardStyle.VariableHeight
							// SSP 6/28/02
							// Compressed card view.
							// Same applies to compressed style as well.
							// Added below clause to the condition.
							//
							// SSP 2/28/03 - Row Layout Functionality
							// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
							// to StandardLabels card style so use the StyleResolved instead of the Style property.
							//
							|| CardStyle.Compressed == this.row.Band.CardSettings.StyleResolved )
						{
							totalHeight = availableHeight;
						}
						else
						{
							// get the rows that can fit in the available height
							//
							// JM UWG1019
							//int cardRows = this.row.Band.GetTotalRowsInCardArea( availableHeight );
							// SSP 10/25/06 BR16003
							// Take into account the row spacing before and after settings.
							// 
							//int cardRows = this.Row.ParentCollection.GetTotalActualRowsInCardArea( availableHeight );
							int cardRows = this.Row.ParentCollection.GetTotalActualRowsInCardArea( availableHeight - rowSpacingBefore );

							// calculate the height that that # of rows requires
							//
							totalHeight = this.Band.CalculateRequiredCardAreaHeight( cardRows );

							// JM 01-07-02 Reset the cached CardAreaHeight since we had to
							//			   adjust the total height to fit within the available
							//			   height.
							// SSP 7/20/06 BR11460
							// VisibleRow.GetTotalHeight sets the CardAreaHeight to available height. This overrides the
							// height that may have been explicitly set. We need to maintain the user set CardAreaHeight.
							// 
							//this.Row.ParentCollection.CardAreaHeight = totalHeight;
							this.Row.ParentCollection.InternalSetCardAreaHeight( totalHeight );

							// SSP 10/25/06 BR16003
							// Take into account the row spacing before and after settings.
							// 
							totalHeight += rowSpacingBefore + rowSpacingAfter;
						}
					}
				}
			}
			else
			{
				totalHeight = this.row.TotalHeight;
			}

			if ( this.HasHeader )
				totalHeight += this.row.Band.GetTotalHeaderHeight();

			// SSP 6/6/02
			// Summary rows feature. Take into account the summary footer.
			//
			// MD 7/26/07 - 7.3 Performance
			// GetHeightForSummariesHelper was removed because it always returns 0 now
			//totalHeight += this.GetHeightForSummariesHelper( );

			// If this is a double top border situation then adjust the total height
			// so that the header's bottom border overlaps the top row border
			//
			if ( adjustForOverlappedBorders && this.SkipDblTopBorderDisplay() )
				totalHeight--;

			return totalHeight;
		}						
   

		internal int Tier
		{
			get
			{
                return  this.tier;				
			}
			set
			{
				this.tier = value;
			}
		}

		internal int PostRowSpacing
		{
			get
			{
				return postRowSpacing;
			}
			set
			{
				postRowSpacing = value;
			}
		}			
		internal int OrigTop 
		{
			get
			{
				return this.origTop;
			}
		}

		internal bool HasPrevSibling
		{
			get
			{
				// SSP 11/11/03 Add Row Feature
				//
				//return this.row.HasPrevSibling( true );
				return this.row.HasPrevSibling( true, IncludeRowTypes.SpecialRows );
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		// The fields being set in these two methods are never used
//#if DEBUG
//        /// <summary>
//        /// Keep track of the next cross band sibling elem row at the same
//        /// band hierarchical level.
//        /// </summary>
//        /// <param name="nextCrossBandSibling"></param>
//#endif
//        internal void SetNextCrossBandSibling( VisibleRow nextCrossBandSibling )
//        {
//            this.nextCrossBandSibling = nextCrossBandSibling;
//        }
//#if DEBUG
//        /// <summary>
//        /// Keep track of the prev cross band sibling elem row at the same
//        /// band hierarchical level.
//        /// </summary>
//        /// <param name="prevCrossBandSibling"></param>
//#endif
//        internal void SetPrevCrossBandSibling( VisibleRow prevCrossBandSibling )
//        {
//            this.prevCrossBandSibling = prevCrossBandSibling;
//        }
		
		internal Infragistics.Win.UltraWinGrid.ViewStyleSpecialFlags SpecialFlags
		{
			get
			{
				return this.specialFlags;
			}
			set
			{
				this.specialFlags = value;
			}			
		}
		internal int GetTopInClient()
        { 
            return this.GetTop() + this.RowScrollRegion.Origin; 
        }

		internal int GetTopOfRowInClient()
        { 
            return this.GetTopOfRow() + this.RowScrollRegion.Origin; 
        }

		// MD 12/9/08 - 9.1 - Column Pinning Right
		// Added a parameter which indicates the on screen location of the row because we need that to determine the width of the row when 
		// there are right-fixed headers.
		//internal int GetRowBaseWidthHelper( ColScrollRegion csr )
		internal int GetRowBaseWidthHelper( ColScrollRegion csr, int rowLocationX )
		{
			// JJD 10/04/01
			// If this is a group by row then make the row the entire
			// width of the colscroll region
			//
			if ( this.Row is UltraGridGroupByRow )
			{
                // MBS 3/18/09 - TFS14061
                // When the extent is larger than the OverallWidth, we're not taking into account the fact that we may be showing
                // scrollbars, so we will position the GroupByRowUIElement to overlap the vertical scrollbar.  We can't change the
                // Extent itself, since this is determined by the DataAreaUIElement and the row's RowScrollRegion's vertical scrollbar,
                // so we need to check this here.
                //                          
                if (csr.Extent > csr.OverallWidth && this.RowScrollRegion.WillScrollbarBeShown())
                {
                    int proposedWidth = csr.Extent - csr.GetBandOrigin(this.Band) - this.Band.GetFixedHeaders_ExtentDelta(csr) - SystemInformation.VerticalScrollBarWidth;
                    int bandExtent = csr.GetBandExtent(this.Band);

                    // We shouldn't resize the with of a GroupByRow to be smaller than the width of its child rows
                    return Math.Max(proposedWidth, bandExtent);
                }

				// SSP 10/16/01 UWG532
				// Use the max of Extent and OverAllWidth instead of OverallWidth because
				//
				//return csr.OverallWidth - csr.GetBandOrigin( this.Band );
				// SSP 5/19/03 - Fixed headers
				// 
				//return Math.Max( csr.Extent, csr.OverallWidth ) - csr.GetBandOrigin( this.Band );                                
                return Math.Max(csr.Extent, csr.OverallWidth) - csr.GetBandOrigin(this.Band) - this.Band.GetFixedHeaders_ExtentDelta(csr);
			}
			else
			{
				// MD 12/9/08 - 9.1 - Column Pinning Right
				// If there are right-fixed headers, the row cannot extent past the reight edge of the last header.
				HeaderBase lastHeader = this.Band.OrderedHeaders.LastVisibleHeader;
				if ( lastHeader != null &&
					lastHeader.FixOnRightResolved &&
					this.Band.IsHeaderFixed( csr, lastHeader ) )
				{
					return lastHeader.GetOnScreenOrigin( csr, this.Band.HasRowSelectors ) + lastHeader.Extent - rowLocationX;
				}

				// SSP 5/19/03 - Fixed headers
				// 
	            //return csr.GetBandExtent( this.Band );
				return csr.GetBandExtent( this.Band ) - this.Band.GetFixedHeaders_ExtentDelta( csr );
			}
		}
		internal Rectangle GetDimensions( ColScrollRegion  csr,
                                          VisibleRowDimensions dimensions,
                                          DimOriginBase originBase )
        {
			int groupByDelta = 0;

			if ( this.Row is UltraGridGroupByRow )
			{
				// SSP 10/19/01
				// Created this helper function to calculate the indent
				// since it's also used from RowColRegionIntersectionUIELement's
				// PositionChildElement.
				//
				groupByDelta = this.Band.CalcGroupByRowIndent( (UltraGridGroupByRow)this.Row );
			}

			// SSP 4/1/05 - Optimizations
			// No need to check for valid enum values since this method is internally called.
			//
			

            Rectangle rcRow = new Rectangle( 0,0,0,0 );

            // The header area is calculated slightly differently
            //
            if ( VisibleRowDimensions.HeaderArea == dimensions )
            {
                if ( !this.HasHeader )
                    return rcRow;

                // JJD 7/5/00
                // Call GetBandOrigin of the ElemHeaderSink so that we get the
                // correct origin for exclusive col scroll region rows as well
                // as the normal rows
                //
                switch ( originBase )
                {
                case DimOriginBase.Normalized:
                    rcRow.X     = 0;
                    rcRow.Y     = 0;
                    break;
                case DimOriginBase.OnScreen:
					// SSP 5/19/03 - Fixed headers
					// 
                    //rcRow.X = csr.GetBandOrigin( this.Band ) - csr.Position;
					rcRow.X = csr.GetBandOrigin( this.Band ) - csr.Position + this.Band.GetFixedHeaders_OriginDelta( csr );
                    rcRow.Y = this.GetTop() + this.RowScrollRegion.Origin;
                    break;
                case DimOriginBase.Absolute:  
                    rcRow.X = csr.GetBandOrigin( this.Band );
                    rcRow.Y = this.GetTop();
                    break;
                case DimOriginBase.Relative:
					// SSP 5/19/03 - Fixed headers
					//
                    //rcRow.X = csr.GetBandOrigin( this.Band ) - csr.Position;
					rcRow.X = csr.GetBandOrigin( this.Band ) - csr.Position + this.Band.GetFixedHeaders_OriginDelta( csr );
                    rcRow.Y = GetTop();
                    break;
                }

                // set the right based on the left
                //
                // JJD 7/3/00
                // Call GetBandExtent of the ElemHeaderSink so that we get the
                // correct width for exclusive col scroll region rows as well
                // as the normal rows
                //
				// JJD 10/04/01
				// If this is a group by row then make the row the entire
				// width of the colscroll region
				//
				//	rcRow.Width = csr.GetBandExtent( this.Band );
				// MD 12/9/08 - 9.1 - Column Pinning Right
				// The GetRowBaseWidthHelper method now takes the X position of the row.
				//rcRow.Width = this.GetRowBaseWidthHelper( csr );
				rcRow.Width = this.GetRowBaseWidthHelper( csr, rcRow.X );
            
                // set the bottom based on the top
                //
                // NickC 10/4/00 : ult 1863 : Added GetRowSpacingBefore
                //rcRow.Height = this.row.BaseHeight + this.row.GetResolvedRowSpacingBefore();

                // Adjust for the pre row area
                //
                this.AdjustForPreRowArea( ref rcRow );

                // JJD 4/11/01
                // The height of the rect should be based on the header height
                //
                rcRow.Height = this.Band.GetTotalHeaderHeight();

				rcRow.X += groupByDelta; 
				rcRow.Width -= groupByDelta;

                return rcRow;
            }

			int bandOrigin = csr.GetBandOrigin( this.Band );

            // set the rcRow.left based on the originBase passed in
            //
            // JJD 7/5/00
            // Call GetBandOrigin of the ElemHeaderSink so that we get the
            // correct origin for exclusive col scroll region rows as well
            // as the normal rows
            //
            switch ( originBase )
            {
            case DimOriginBase.Normalized:
                rcRow.X = 0;
                rcRow.Y  = 0;
                break;
            case DimOriginBase.OnScreen:
				// SSP 5/19/03 - Fixed headers
				//
                //rcRow.X = bandOrigin + csr.Origin - csr.Position;
				rcRow.X = bandOrigin + csr.Origin - csr.Position + this.Band.GetFixedHeaders_OriginDelta( csr );
                rcRow.Y = this.GetTopOfRowInClient();
                break;
            case DimOriginBase.Absolute:  
                rcRow.X = bandOrigin;
                rcRow.Y = this.GetTopOfRow();
                break;
            case DimOriginBase.Relative:
				// SSP 5/19/03 - Fixed headers
				//
                //rcRow.X = bandOrigin - csr.Position;
				rcRow.X = bandOrigin - csr.Position + this.Band.GetFixedHeaders_OriginDelta( csr );
                rcRow.Y = this.GetTopOfRow();
                break;
            }

            // set the right based on the left
            //
            // JJD 7/3/00
            // Call GetBandExtent of the ElemHeaderSink so that we get the
            // correct width for exclusive col scroll region rows as well
            // as the normal rows
            //
			// JJD 10/04/01
			// If this is a group by row then make the row the entire
			// width of the colscroll region
			//
	        // rcRow.Width  = csr.GetBandExtent( this.Band );
            
			// JJD 12/11/01
			// The width is not used for card areas 
			//
			if ( this.IsCardArea )
			{
				// JJD 1/18/02 - UWG938
				// Offset the card are by the pre-row area extent so we
				// don't overlap sibling band row connector lines.
				//
				int preRowAreaextent = this.Band.PreRowAreaExtent;
				rcRow.X += preRowAreaextent;

				// JJD 12/14/01
				// Shift the left over to prevent it from being
				// scrolled out of view to the left.
				//
				// SSP 5/19/03 - Fixed headers
				// Added code for the fixed headers case and enclosed the already existing code
				// in the else block. In card view, if the parent band has fixed headers, then
				// don't shift the card row left to prevent it from overlapping with the row
				// connectors of the parent band rows.
				//
				if ( null != this.Band.ParentBand && this.Band.ParentBand.UseFixedHeaders )
				{
					if ( this.Band.Layout.ViewStyleImpl.HasMultiRowTiers )
					{
						int tmpScrollDelta = csr.Position, tmpOrigin, tmpExtent;
						UltraGridBand.GetBandOriginExtent_HorizontalViewStyle(
							this.Band.ParentBand, csr, ref tmpScrollDelta, out tmpOrigin, out tmpExtent );
						
						rcRow.X += tmpScrollDelta;
					}
					else
						rcRow.X += csr.Position;
				}
				else
				{
					if ( bandOrigin + preRowAreaextent < csr.Position )
						rcRow.X += csr.Position - bandOrigin - preRowAreaextent;
				}

				rcRow.Width = 1;
			}
			else
			{
				// MD 12/9/08 - 9.1 - Column Pinning Right
				// The GetRowBaseWidthHelper method now takes the X position of the row.
				//rcRow.Width = this.GetRowBaseWidthHelper( csr );
				rcRow.Width = this.GetRowBaseWidthHelper( csr, rcRow.X );
			}
            
            // set the bottom based on the top
            //
            // NickC 10/4/00 : ult 1863 : Added GetRowSpacingBefore
            
            // JJD 10/05/00 - ult1948
            // Adjust the row top to account for row spacing before
            //
			int rowSpacingBeforeResolved = this.row.RowSpacingBeforeResolved;
			rcRow.Y += rowSpacingBeforeResolved;
            
			// JJD 12/11/01
			// The height is calculated differently for card areas 
			//
			if ( this.IsCardArea )
			{
				// JM 01-17-02 
				//rcRow.Height = this.GetTotalHeight() - this.row.Band.GetTotalHeaderHeight();
				rcRow.Height = this.GetTotalHeight() - this.row.Band.GetTotalHeaderHeight() + 1;

				// SSP 10/25/06 BR16003
				// Take into account row spacing before and after settings.
				// 
				int rowSpacingAfterResolved = this.row.RowSpacingAfterResolved;
				rcRow.Height -= rowSpacingBeforeResolved + rowSpacingAfterResolved;
			}
			else
			{
				rcRow.Height = this.row.BaseHeight;

				if ( dimensions >= VisibleRowDimensions.InsideRowSelectors )
				{
					this.AdjustForPreRowArea( ref rcRow );
					this.AdjustForRowSelectorArea( ref rcRow );
	                
					if ( dimensions >= VisibleRowDimensions.InsideRowBorders )
					{
						this.AdjustForRowBorders( ref rcRow );
					}
				}
			}

			rcRow.X += groupByDelta; 
			rcRow.Width -= groupByDelta;

            return rcRow; 
        }

		internal void AdjustForPreRowArea( ref Rectangle rect )
        {
            int adjustment = this.Band.PreRowAreaExtent;
            
            rect.X      += adjustment;
            rect.Width  -= adjustment;
        }

		internal void AdjustForRowSelectorArea( ref Rectangle rect )
        {
            int adjustment = this.Band.RowSelectorExtent;
            
            rect.X      += adjustment;
            rect.Width  -= adjustment;
        }
		internal void AdjustForRowBorders( ref Rectangle rect )
        {
            UIElementBorderStyle borderStyle = this.Band.BorderStyleRowResolved;

            int borderThickness = this.Row.Layout.GetBorderThickness( borderStyle );

            if ( borderThickness < 1 )
                return;

            // Adjust for the top, bottom and right border
            //
            rect.Y      += borderThickness;
            rect.Height -= 2 * borderThickness;

            // Only adjust the left for the row border if we aren't merging
            // row borders or if we aren't drawing row selectors for this band
            //
            if ( !this.Row.Layout.CanMergeAdjacentBorders( borderStyle ) ||
                 this.Band.RowSelectorExtent < 1  )
            {
                rect.X      += borderThickness;
                rect.Width  -= 2 * borderThickness;
            }
            else
                rect.Width  -= borderThickness;
        }
 
	    /// <summary>
	    /// Called when a property has changed on a row
	    /// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
        protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
        {
            // pass this on to the Row scroll region's method
            //
            this.RowScrollRegion.OnRowPropChange( this, propChange ); 
        }

		#region NAS 5.2 Fixed Rows

		// SSP 5/3/05
		//
		internal int ClipBottomTo
		{
			get
			{
				return this.clipBottomTo;
			}
			set
			{
				this.clipBottomTo = value;
			}
		}

		internal bool IsClipBottomToSet
		{
			get
			{
				return this.clipBottomTo >= 0;
			}
		}

		internal void ResetClipBottomTo( )
		{
			this.clipBottomTo = -1;
		}

		#endregion // NAS 5.2 Fixed Rows

		#region AtachedHeadersRow

		// SSP 7/21/05 - NAS 5.3 Header Placement
		// 
		private VisibleRow attachedHeadersRow = null;
		internal VisibleRow AttachedHeadersRow
		{
			get
			{
				return this.attachedHeadersRow;
			}
			set
			{
				this.attachedHeadersRow = value;
			}
		}

		#endregion // AtachedHeadersRow

		#region OnDispose

		// SSP 7/6/05 - NAS 5.3 Empty Rows
		// Overrode OnDispose so we can unhook from the row notifications.
		// 
		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnDispose( )
		{
			this.UnhookFromRowNotifications( );

			base.OnDispose( );
		}

		#endregion // OnDispose
   }
}
