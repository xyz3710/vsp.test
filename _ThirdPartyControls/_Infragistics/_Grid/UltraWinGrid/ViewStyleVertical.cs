#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	internal class ViewStyleVertical : ViewStyleMultiBase
	{
		internal ViewStyleVertical( Infragistics.Win.UltraWinGrid.UltraGridLayout layout ) : base( layout )
		{
		}

		#region HasFixedHeaders
		
		// SSP 7/18/05 - NAS 5.3 Header Placement
		// Made HasFixedHeaders non-virtual. Instead added BandHasFixedHeaders method that takes in
		// the band context. Overrode that here instead of the HasFixedHeaders and commented that out.
		// 
		
		internal override bool BandHasFixedHeaders( UltraGridBand band )
		{ 
			if ( null != band )
			{
				if ( HeaderPlacement.FixedOnTop == band.HeaderPlacementResolved 
					&& band.HeadersAreaVisible && ! band.HiddenResolved )
					return true;
			}
			else
			{
				foreach ( UltraGridBand bandIterator in this.Layout.SortedBands )
				{
					if ( this.BandHasFixedHeaders( bandIterator ) )
						return true;
				}
			}

			return false; 
		}

		#endregion // HasFixedHeaders

		#region GetFixedHeaderBands
		
		// SSP 7/18/05 - NAS 5.3 Header Placement
		// 
		internal override void GetFixedHeaderBands( VisibleRowsCollection visibleRows,
			UltraGridBand [] headerBands,
			ref int headerBandCount )
		{
			// Init the return count.
			//
			headerBandCount = 0;

			foreach ( UltraGridBand bandIterator in this.Layout.SortedBands )
			{
				if ( this.BandHasFixedHeaders( bandIterator ) )
					headerBands[ headerBandCount++ ] = bandIterator;
			}
		}
		
		#endregion // GetFixedHeaderBands

		internal override bool RecreateHeaderList( ColScrollRegion csr )
        {
            csr.VisibleHeaders.InternalClear();

            if ( csr.Layout.SortedBands.Count < 1 )
                return false;
            
            // verify that the band origin and extents have been
            // calculated properly
            //
//            csr.Layout.Bands.VerifyBandOrigins();

            bool columnAdded = false;

            int extent          = csr.Extent;
            int scrollPosition  = csr.Position;
            int bandOrigin;
            int bandExtent;

            for ( int i = 0; i < csr.Layout.SortedBands.Count; i++ )
            {
                UltraGridBand band      = csr.Layout.SortedBands[i];

                // JJD 7/5/00
                // Use the new GetBandOrigin and GetBandExtent methods
                // off the header sink to allow for the different metrics
                // of exclusive col scroll regions
                //
                bandOrigin     = csr.GetBandOrigin( band );
                bandExtent     = csr.GetBandExtent( band );

                // if the band is to the left of the scroll region then just
                // continue
                //
				// SSP 5/23/03 - Fixed headers
				// If the band has fixed headers, then don't look at the scroll position
				// to decide whether to add the haders or not.
				// Added the existing code into the if block and added the new code
				// into the else block.
				//
				if ( ! band.UseFixedHeaders )
				{
					if ( band.HiddenResolved || 
						bandOrigin + bandExtent < 
						scrollPosition )
						continue;

					// if the band is to the right of the scroll region then just
					// continue
					//
					if ( bandOrigin > 
						scrollPosition + extent )
						break;
				}
				else
				{
					if ( band.HiddenResolved )
						continue;
				}

                columnAdded = this.AddBandHeaders( csr, band ) || columnAdded;
            }

			// SSP 8/17/04 - UltraCalc
			// Added OnAfterRowListCreated and OnAfterHeaderListCreated methods. Call
			// OnAfterHeaderListCreated after the visible headers are regenerated.
			//
			// ----------------------------------------------------------------------------
			// MD 7/26/07 - 7.3 Performance
			// OnAfterHeaderListCreated does nothing right now, uncomment if logic is added to OnAfterHeaderListCreated
			//this.OnAfterHeaderListCreated( csr );
			// ----------------------------------------------------------------------------

            return columnAdded;
        }

		internal override void AdjustBandOrigin( UltraGridBand band, 
			                                     UltraGridBand priorBand,
			                                     ref int origin )
		{

            if ( band.ParentBand != null )
                origin = band.ParentBand.GetOrigin() + 
						band.ParentBand.PreRowAreaExtent;

            else
			    origin = 0;
		}
		internal override void AdjustExclusiveMetrics( Infragistics.Win.UltraWinGrid.ColScrollRegion csr,
			                                            HeaderBase header,
			                                            HeaderBase priorHeader)
		{
			int nOrigin = 0;

            // on a band break recalc the origin 
            //
            if ( header.FirstItem )
            {
                // if there is a parent band indent so that this child band starts indented
                // by VSVERTICAL_BAND_OFFSET pixels
                // 
                if ( header.Band.ParentBand != null )
                    nOrigin = header.Band.ParentBand.GetOrigin() + 
						// SSP 6/13/05 BR04558
						// Seems like the intention here was to calculate the band origin. Changed the
						// following code.
						// 
						// ----------------------------------------------------------------------------
						header.Band.ParentBand.PreRowAreaExtent;
						
						// ----------------------------------------------------------------------------
				else
                    nOrigin = 0;
            
            }
            else
            {
                if ( priorHeader != null )
                    nOrigin = priorHeader.Extent +  //pPriorPositionItem->GetOverallWidth() +
					            priorHeader.Origin; //pPriorPositionItem->GetRelativeOrigin();
                else
                    nOrigin = 0;
            }

			this.SetExclusiveMetrics(header, nOrigin);		    
		}


		/// <summary>
		/// Sets the row's top, tier and has header flag when we are fetching
		/// forward. 
		/// </summary>
		protected override void OrientVisibleRowForward ( ref VisibleRowFetchRowContext context, 
                                                          VisibleRow visibleRow )
        {
            if ( context.lastRowInserted == null )
            {
                visibleRow.Tier = 0;

				// SSP 7/18/05 - NAS 5.3 Header Placement
				// Now the vertical view style can also have fixed headers.
				// 
				// --------------------------------------------------------------------------
				//visibleRow.SetTop( context.rowScrollRegion.ScrollOffset );
                //visibleRow.HasHeader = true;
				visibleRow.SetTop( this.GetFixedHeaderHeight() + context.rowScrollRegion.scrollOffset );
				this.SetHasHeaderHelper( ref context, visibleRow, true );
				// --------------------------------------------------------------------------

                visibleRow.PostRowSpacing = 0;

                // JJD 3/28/00
                // Set the special flags here
                //
                visibleRow.SpecialFlags = this.CalculateRowSpecialFlags ( visibleRow, context.lastRowInserted );
                return;
            }

            // bump the tier
            //
            visibleRow.Tier = context.lastRowInserted.Tier + 1;

            // check if this row is a sibling of the last row
            //
            bool sameBand = ( visibleRow.Band == context.lastRowInserted.Band );

            // set the has header flag 
            //
			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
            //visibleRow.HasHeader = !sameBand;
			this.SetHasHeaderHelper( ref context, visibleRow, ! sameBand );
            
            // set the PostRowSpacing 
            //
            context.lastRowInserted.PostRowSpacing = sameBand ? 0 : this.Layout.InterBandSpacing;

            // set the top 
            //
            visibleRow.SetTop( context.lastRowInserted.GetTop()            + 
                            context.lastRowInserted.GetTotalHeight()    + 
                            context.lastRowInserted.PostRowSpacing );

            SetNextCrossBandSibling( ref context, visibleRow );

            // JJD 3/28/00
            // Set the special flags here
            //
            visibleRow.SpecialFlags = this.CalculateRowSpecialFlags ( visibleRow, context.lastRowInserted );
        }

		/// <summary>
		/// Sets the row's top, tier and has header flag when we are fetching
		/// backward. 
		/// </summary>
		protected override void OrientVisibleRowBackward ( ref VisibleRowFetchRowContext context, 
                                                            VisibleRow visibleRow )
        {
            if ( context.lastRowInserted == null )
            {
                // set tier to arbitrary high number since after we fetch bacward we 
                // always do a fixup going forward that will set the tier back to zero
                //
                visibleRow.Tier = 5000;
                visibleRow.SetTop( context.rowScrollRegion.Extent - visibleRow.GetTotalHeight() );
                if ( visibleRow.Band.GetTotalHeaderHeight() >= visibleRow.GetTop() + context.rowScrollRegion.ScrollOffset )
                    visibleRow.SetTop( context.rowScrollRegion.ScrollOffset );

				// SSP 7/18/05 - NAS 5.3 Header Placement
				// 
                //visibleRow.HasHeader = ( 0 >= visibleRow.GetTop() );
				this.SetHasHeaderHelper( ref context, visibleRow, 0 >= visibleRow.GetTop() );

                visibleRow.PostRowSpacing = 0;
                
                return;
            }

            // decrement the tier
            //
            visibleRow.Tier = context.lastRowInserted.Tier - 1;

            // check if this row is a sibling of the last row
            //
            bool sameBand = ( visibleRow.Band == context.lastRowInserted.Band );

            // set the has header flag on the previously inserted row
            //
			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
            //context.lastRowInserted.HasHeader = !sameBand;
			this.SetHasHeaderHelper( ref context, context.lastRowInserted, ! sameBand );

            // if we set the has header to true on the previous row we need to
            // adjust its top value to allow for that header
            //
            if ( context.lastRowInserted.HasHeader )
                context.lastRowInserted.SetTop( context.lastRowInserted.GetTop() - context.lastRowInserted.Band.GetTotalHeaderHeight() );
            
            // set the PostRowSpacing 
            //
            visibleRow.PostRowSpacing = ( sameBand ? 0 : this.Layout.InterBandSpacing );

            // set the top 
            //
            visibleRow.SetTop( context.lastRowInserted.GetTop() - 
                            ( visibleRow.GetTotalHeight() + visibleRow.PostRowSpacing ) ) ;

            if ( 0 >= visibleRow.GetTop() + context.rowScrollRegion.ScrollOffset )
            {
                // The region has been filled so set this row's top to zero and
                // set its 'hasheader' flag to true
                //
                visibleRow.SetTop( context.rowScrollRegion.ScrollOffset );

				// SSP 7/18/05 - NAS 5.3 Header Placement
				// 
                //visibleRow.HasHeader = true;
				this.SetHasHeaderHelper( ref context, visibleRow, true );
            }
            else
            {
                // The region has not yet been filled so assume no header set this row's top to zero and
                // set its 'hasheader' flag to true
                //
				// SSP 7/18/05 - NAS 5.3 Header Placement
				// 
                //visibleRow.HasHeader = false;
				this.SetHasHeaderHelper( ref context, visibleRow, false );
            }
       }

        /// <summary>
		/// Calculates the special flags needed to draw the row and cells
		/// properly. These flags include whether the row is responsible for
		/// its top bnorder, et al.
		/// </summary>
		protected override Infragistics.Win.UltraWinGrid.ViewStyleSpecialFlags CalculateRowSpecialFlags ( VisibleRow visibleRow, VisibleRow lastVisibleRow )
		{
			Infragistics.Win.UltraWinGrid.ViewStyleSpecialFlags specialFlags = base.CalculateRowSpecialFlags( visibleRow, lastVisibleRow );
			
            specialFlags &= ~ViewStyleSpecialFlags.SkipTopLevelHierarchyLines;
            specialFlags |= ViewStyleSpecialFlags.DrawLongParentConnector;

			return specialFlags;
		}
	}
}
