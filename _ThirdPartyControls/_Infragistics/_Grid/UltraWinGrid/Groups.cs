#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Collections;
    using System.Data;
    using Infragistics.Shared;
    using Infragistics.Shared.Serialization;
    using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Diagnostics;
	using System.ComponentModel;
	using System.Security.Permissions;
    using System.Collections.Generic;


    /// <summary>
	/// Returns the collection of Group objects. This property is read-only at 
	/// run-time.
	/// </summary><remarks><para>The Groups property is used to access the 
	/// collection of Group objects associated with an Band object. An Group 
	/// is a group of columns that appear together in the grid, and can be 
	/// resized, moved or swapped together as a unit.</para><para>Each Group 
	/// object in the collection can be accessed by using its Key value. Group 
	/// objects in this collection do not support an Index value as the position 
	/// of the object within the collection is not significant.</para></remarks>
	[ Serializable()]
	[ ListBindable(false )]
	[ TypeConverter( typeof( GroupsCollectionConverter ) ) ]
	[ System.ComponentModel.Editor( typeof( GroupsCollectionUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
	public class GroupsCollection : KeyedSubObjectsCollectionBase,
//									IList,
									ISerializable
    {
        private UltraGridBand band;
		private int	initialCapacity = 10;
		private bool initialzingFrom = false;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="band">The UltraGridBand</param>
        public GroupsCollection( UltraGridBand band )
        {
            this.band = band;			
        }
 
        /// <summary>
        /// Called when this object is disposed of
        /// </summary>
        protected override void OnDispose() 
        {
        
            foreach ( Infragistics.Win.UltraWinGrid.UltraGridGroup group in this )
            {
                // remove the prop change notifications
                //
                group.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

                // call dispose on the band object
                //
                group.Dispose();
            }

            base.InternalClear();
        }

		/// <summary>
		/// Returns false
		/// </summary>
        public override bool IsReadOnly { get { return false; } }

		/// <summary>
		/// Associated band object read-only
		/// </summary>
        public UltraGridBand Band
        {
            get
            {
                return this.band;
            }
        }

        
		/// <summary>
		/// indexer 
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridGroup this[ int index ] 
        {
            get
            {
                return (UltraGridGroup)base.GetItem( index );
            }
        }

        
		/// <summary>
		/// indexer (by string key)
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridGroup this[ String key ] 
        {
            get
            {
                return (UltraGridGroup)base.GetItem( key );
            }
        }

        /// <summary>
        /// Abstract property that specifies the initial capacity
        /// of the collection
        /// </summary>
        protected override int InitialCapacity
        {
            get
            {
                return this.initialCapacity;
            }
        }
         /// <summary>
        /// The collection as an array of objects
        /// </summary>
        public override object[] All 
        {
			// AS 1/8/03 - fxcop
			// Properties should not be write only - even
			// though we know it wasn't anyway
			get { return base.All; }
			set 
            {
				throw new NotSupportedException();
            }
        }


        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
        protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
        {
            // pass the notification along to our listeners
            //
            this.NotifyPropChange( PropertyIds.Group, propChange );
        }

		internal int GetUniqueID()
		{
			// use the tick count to get a unique id
			//
			int id = System.Environment.TickCount;

			// keep bumping the id until we find one that
			// isn't already used
			//
			do
			{
				// the id must not be zero
				//
				if ( id == 0 ||
					id == int.MaxValue )
					id = 1;
				else
					id++;

			}
			while( this.GetGroupFromInternalID( id ) != null );

			return id;
		}
		
		internal UltraGridGroup GetGroupFromInternalID( int id )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				if ( id == this[ i ].InternalID )
					return this[ i ];
			}
			return null;
		}

		/// <summary>
		/// Appends a group to the collection
		/// </summary>
		/// <param name="group">Must be a group object</param>
		/// <returns>Index in collection</returns>
		// 
		// JJD 1/18/02- UWG943
		// Remove EditorBrowsableState.Never attribute and change
		// signature to take a group. The 'Never' prevented all
		// Adds form showing in statement completion.
		//
		//[EditorBrowsable(EditorBrowsableState.Never)]
		public int Add( UltraGridGroup group )
		{
            // MRS - NAS 9.1 - Groups in RowLayout
            this.BumpGroupsVersion();

			// JJD 10/19/01
			// Moved logic into VerifyAddGroupParameter method. This
			// will throw an error if the value is bogus
			//
			return this.InternalAdd( this.VerifyAddGroupParameter( group ) );            
		}

        /// <summary>
        /// Adds a <see cref='Infragistics.Win.UltraWinGrid.UltraGridGroup'/> to the collection.
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        ///    The <paramref name="Group"/>argument was <see langword='null'/>.
        /// </exception>
        /// <returns>The newly added group</returns>
        public Infragistics.Win.UltraWinGrid.UltraGridGroup Add() 
        {
			return this.AddGroup( null, false );
        }

        /// <summary>
        /// Adds a <see cref='Infragistics.Win.UltraWinGrid.UltraGridGroup'/> to the collection.
        /// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <exception cref='System.ArgumentNullException'>
        ///    The <paramref name="Group"/>argument was <see langword='null'/>.
        /// </exception>
        /// <returns>The newly added group</returns>
        public Infragistics.Win.UltraWinGrid.UltraGridGroup Add( String key ) 
        {
            this.ValidateKeyDoesNotExist( key );
			return this.AddGroup( key, false );
        }

        /// <summary>
        /// Adds a <see cref='Infragistics.Win.UltraWinGrid.UltraGridGroup'/> to the collection.
        /// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="caption">The <see cref="HeaderBase.Caption"/> of the new group.</param>
        /// <exception cref='System.ArgumentNullException'>
        ///    The <paramref name="Group"/>argument was <see langword='null'/>.
        /// </exception>
        /// <returns>The newly added group</returns>
		public Infragistics.Win.UltraWinGrid.UltraGridGroup Add( string key, string caption ) 
		{
			this.ValidateKeyDoesNotExist( key );
			UltraGridGroup group = this.AddGroup( key, false );
			group.Header.Caption = caption;
			return group;
		}

		// SSP 12/3/04 BR00883
		// We have multiple overloads of Add which ends up confusing Whidbey. Adding AddRange
		// method fixes that since the designer gives AddRange preference over Add.
		//
		/// <summary>
		/// For use in design time serialization/deserialization.
		/// </summary>
		/// <remarks></remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void AddRange( UltraGridGroup[] groups )
		{
			foreach ( UltraGridGroup group in groups )
				this.Add( group );
		}

        /// <summary>
        /// Adds a <see cref='Infragistics.Win.UltraWinGrid.UltraGridGroup'/> to the collection.
        /// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="cloned">Specifies whether the group is cloned.</param>
        /// <exception cref='System.ArgumentNullException'>
        ///    The <paramref name="Group"/>argument was <see langword='null'/>.
        /// </exception>
        /// <returns>The newly added group</returns>
		protected Infragistics.Win.UltraWinGrid.UltraGridGroup AddGroup( String key, bool cloned ) 
		{
			UltraGridGroup group = null;
			if( key == null )
				group = new UltraGridGroup( this.band );
			else
				group = new UltraGridGroup( this.band, key );

			// hook up the prop change notifications
			//
			group.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// if the band already has a groups collection, then add the newly created
			// group's header to OrderedGroupHeaders
			//
			// JJD 10/24/01
			// Only add to the ordered headers if we aren't in InitialzeFrom
			//
			if( !this.initialzingFrom &&
				this.band.HasOrderedGroups && !cloned ) 
				this.band.OrderedGroupHeaders.InternalAdd( group.Header );
            
			//Add this group to the collection
			//
			this.InternalAdd( group );

            // MRS - NAS 9.1 - Groups in RowLayout
            this.BumpGroupsVersion();

			// SSP 6/18/02 UWG1214
			// Notify the band and the layout which will eventually dirty the metrics.
			// 
			this.NotifyPropChange( PropertyIds.Add );

			return group;
		}

		internal Infragistics.Win.UltraWinGrid.UltraGridGroup AddClonedGroup( String key ) 
		{
			this.ValidateKeyDoesNotExist( key );
			return this.AddGroup( key, true );
		}

        /// <summary>
        /// Remove group from collection
        /// </summary>
        public void Remove( int index ) 
        {
            this.Remove ( this[ index ] );
        }

        /// <summary>
        /// Remove group from collection
        /// </summary>
        public void Remove( String key ) 
        {
            this.Remove ( this[ key ] );
        }

        /// <summary>
        /// Remove group from collection
        /// </summary>
        public void Remove( UltraGridGroup group ) 
        {
            // unhook prop change notifications
            //
            group.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			// JJD 10/03/01 - UWG249
			// Call Reset on the group so that each column's group reference
			// will get nulled out
			//
			group.Reset();

            // MRS - NAS 9.1 - Groups in RowLayout
            this.ClearGroupFromRowLayoutColumnInfos(group);

            // remove from collection
            //
            this.InternalRemove( group );

            // MRS - NAS 9.1 - Groups in RowLayout
            this.BumpGroupsVersion();

			// SSP 11/26/01 UWG764
			// Notify the removal of the group.
			//
			this.NotifyPropChange( PropertyIds.Remove );
        }

        /// <summary>
        /// Clears the collection
        /// </summary>
        public void Clear() 
        {			
            foreach ( Infragistics.Win.UltraWinGrid.UltraGridGroup group in this )
            {
                // remove the prop change notifications
                //
                group.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				// JJD 10/03/01 - UWG249
				// Call Reset on the group so that each column's group reference
				// will get nulled out
				//
				group.Reset();

                // MRS - NAS 9.1 - Groups in RowLayout
                if (this.band != null &&
                    this.band.Layout != null &&
                    this.band.Layout.IsApplyingLayout == false)
                {
                    this.ClearGroupFromRowLayoutColumnInfos(group);
                }
            }

			this.band.OrderedGroupHeaders.InternalClear();
			this.InternalClear();

            // MRS - NAS 9.1 - Groups in RowLayout
            this.BumpGroupsVersion();

			// SSP 2/1/02 UWG1026
			// Notify the band that we are cleared
			//
			this.NotifyPropChange( PropertyIds.Clear );

			// JJD 10/22/01
			// Dirty the metrics
			//
			// JM 04-09-04 Check for null band and layout before dirtying the metrics
			if (this.band != null  &&  this.band.Layout != null)
				this.band.Layout.ColScrollRegions.DirtyMetrics( true );
        }


		internal void ClearClonedGroups() 
		{
			foreach ( Infragistics.Win.UltraWinGrid.UltraGridGroup group in this )
			{
				// remove the prop change notifications
				//
				group.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
			}

			this.InternalClear();

            // MRS - NAS 9.1 - Groups in RowLayout
            this.BumpGroupsVersion();
		}


		/// <summary>
		///  Remove a group from the collection
		/// </summary>
		/// <param name="value"></param>
		public void	Remove( object value )
		{
			if ( value == null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_157") );
			
			Infragistics.Win.UltraWinGrid.UltraGridGroup group = value as Infragistics.Win.UltraWinGrid.UltraGridGroup;

			if ( group == null )
				throw new ArgumentException( SR.GetString("LER_Exception_340", value.GetType().Name ) );

			// Remove a listener for property changes.
			group.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			this.InternalRemove( group );
		}

		/// <summary>
		///  Remove a group from the collection
		/// </summary>
		/// <param name="index"></param>
		public void	RemoveAt( int index )
		{
			if ( index > this.Count || index < 0 )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_116") );
			
			Infragistics.Win.UltraWinGrid.UltraGridGroup group = this[ index ];
            
			this.InternalRemoveAt(index);

            // MRS - NAS 9.1 - Groups in RowLayout
            this.ClearGroupFromRowLayoutColumnInfos(group);

            // MRS - NAS 9.1 - Groups in RowLayout
            this.BumpGroupsVersion();

			if ( group != null )
			{
				// Remove a listener for property changes.
				group.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
			}
		}
       
		/// <summary>
		/// IEnumerable Interface Implementation
		/// </summary>
        /// <returns>A type safe enumerator</returns>
        public GroupEnumerator GetEnumerator() // non-IEnumerable version
        {
           return new GroupEnumerator(this);
        }	

	
		internal int MaxHeaderFontHeight
		{
			get
			{
				//RobA 10/23/01 implemented
				//
				int maxHeight = 0;

				// loop thru each column to find the one with the biggest font
				//
				for ( int i=0; i < this.Count; ++i )
				{
					int fontHeight = 0;

					AppearanceData appData = new AppearanceData();

					// get the resolved appearance for the column
					//
					ResolveAppearanceContext context = new ResolveAppearanceContext( typeof(Infragistics.Win.UltraWinGrid.HeaderBase), AppearancePropFlags.FontData ); 
					
					this[i].ResolveAppearance( ref appData, ref context );

					// JJD 12/14/01
					// Moved font height logic into CalculateFontHeight
					//
					fontHeight = this.Band.Layout.CalculateFontHeight( ref appData );

					maxHeight = Math.Max( fontHeight, maxHeight );
				}

				return maxHeight;
			}			
		}

		// MD 7/26/07 - 7.3 Performance
		// This property always returns 0
		#region Removed

		//internal int MaxHeaderPictureHeight
		//{
		//    get
		//    {
		//        //TODO: implement
		//        return 0;
		//    }
		//}

		#endregion Removed

		/// <summary>
		/// Returns false
		/// </summary>
		public bool IsFixedSize
		{
			get
			{
				return false;
			}
		}



		/// <summary>
		/// Inserts a group object to the collection at the given index
		/// </summary>
		/// <param name="index">The index into which the new group will be inserted.</param>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		public UltraGridGroup Insert ( int index, string key )
		{
			this.ValidateKeyDoesNotExist( key );

			UltraGridGroup group = null;
			if( key == null )
				group = new UltraGridGroup( this.band );
			else
				group = new UltraGridGroup( this.band, key );			
			
			// Add a listener for property changes.
			group.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			
			this.InternalInsert( index, group );

            // MRS - NAS 9.1 - Groups in RowLayout
            this.BumpGroupsVersion();

			// SSP 11/26/01 UWG764
			// Notify the addition of the group.
			//
			this.NotifyPropChange( PropertyIds.Add );

			return group;
		}


		private Infragistics.Win.UltraWinGrid.UltraGridGroup VerifyAddGroupParameter( object value )
		{
			if ( value == null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_158") );
			
			Infragistics.Win.UltraWinGrid.UltraGridGroup group = value as Infragistics.Win.UltraWinGrid.UltraGridGroup;

			if ( group == null )
				throw new ArgumentException( SR.GetString("LER_Exception_341", value.GetType().Name ) );

			// JJD 11/21/01
			// Only hook everything up if our band reference is not null
			//
			if ( this.band != null && this.band.Layout != null )
			{
				if ( group.Band == null )
					group.InitBand ( this.Band, false );
				else
				{
					if ( group.Band != this.Band )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_159") );
				}

				// hook up the prop change notifications
				//
				group.SubObjectPropChanged += this.SubObjectPropChangeHandler;

				// JJD 10/22/01
				// Dirty the metrics on the first add
				//
				if ( this.Count == 0 )
                    // MBS 10/30/07 - BR27382
                    // There is no reason to reset the scroll region at this point since the user
                    // will generally want to maintain the current state of the grid
                    //
                    //this.band.Layout.ColScrollRegions.DirtyMetrics( true );
                    this.band.Layout.ColScrollRegions.DirtyMetrics(false);
			}

			return group;

		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			//all values that were set are now save into SerializationInfo
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// first add the count so we can set the initial capacity
			// efficiently when we de-serialize
			//
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Count", this.Count  );
			//info.AddValue( "Count", this.Count );

			// add each layout in the collection
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, i.ToString(), this[i] );
				//info.AddValue( i.ToString(), this[i] );
			}

			// JJD 1/31/02
			// Serialize the tag property
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal GroupsCollection( SerializationInfo info, StreamingContext context )
		protected GroupsCollection( SerializationInfo info, StreamingContext context )
		{
			//this will have to be set in InitBand()
			this.band = null;

			//everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( Infragistics.Win.UltraWinGrid.UltraGridGroup ) )
				{
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					UltraGridGroup item = (UltraGridGroup)Utils.DeserializeProperty( entry, typeof(UltraGridGroup), null );

					// Add the item to the collection
					if ( item != null )
						this.InternalAdd( item );
				}
				else
				{
					switch ( entry.Name )
					{
						case "Count":
						{
							//this.initialCapacity = Math.Max( this.initialCapacity, (int)entry.Value );
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/17/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.initialCapacity = Math.Max(this.initialCapacity, (int)Utils.DeserializeProperty( entry, typeof(int), this.initialCapacity ));
							//this.initialCapacity = Math.Max(this.initialCapacity, (int)DisposableObject.ConvertValue( entry.Value, typeof(int) ));
							break;
						}

						// JJD 1/31/02
						// De-Serialize the tag property
						//
						case "Tag":
						{
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;
						}

						default:
						{
							Debug.Assert( false, "Invalid entry in BandsCollection de-serialization ctor" );
							break;
						}				
					}
				}
			}
		}

		internal void InitBand( UltraGridBand band, bool calledFromEndInit )
		{
			this.band = band;

			//iterate through collection to set bands
			for ( int i = 0; i < this.Count; i++ )
			{
				this[i].InitBand( band, calledFromEndInit );
			}
		}

		/// <summary>
		/// Returns a descriptive string at design time.
		/// </summary>
		public override string ToString() 
		{
			if ( this.band			== null		||
				 this.band.Layout	== null		||
				 this.band.Layout.Grid == null	||
				 !this.band.Layout.Grid.DesignMode )
				return base.ToString();
 
			return SR.GetString("LDR_Groups_AddViaCustomProps");
		}
		
		internal void InitializeFrom( GroupsCollection source, PropertyCategories categories  )
		{
			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			this.Clear();

			if( source.Count < 1 )
				return ;

			// Set flag so we know not to generate any notifications
			// during a load while adding the group
			//
			this.initialzingFrom = true;

			Infragistics.Win.UltraWinGrid.UltraGridGroup newGroup;
			
			// loop thru each of the source's groups and add
			// a corresponding group to our collection
			//
			for ( int index = 0; index < source.Count; index++ )
			{
				// get this group's key
				//
				string key = source[index].Key;
        
				// add a new group to our collection with the same key
				//
				newGroup = this.AddGroup( key, false ); 

				// call InitForm on the new group to copy
				// over the properties from the source group
				//
				// JJD 10/19/01
				// Instead of getting the group through its key (which may be null)
				// use the group returned from AddGroup above.
				//
				//this[key].InitializeFrom(source[index], categories );
				newGroup.InitializeFrom( source[index], categories );
			}

			// Reset the flag
			//
			this.initialzingFrom = false;
		}

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout
                
        #region BumpGroupsVersion
        internal void BumpGroupsVersion()
        {
            if (null != this.Band)
                this.Band.BumpGroupsVersion();
        }        
        #endregion //BumpGroupsVersion

        #region ClearGroupFromRowLayoutColumnInfos        
        private void ClearGroupFromRowLayoutColumnInfos(UltraGridGroup groupBeingRemoved)
        {
            List<IProvideRowLayoutColumnInfo> groupsAndColumns = UltraGridBand.GetGroupsAndColumns(this.Band);

            // Loop through all the groups and columns of this band, excluding the group being removed. 
            foreach (IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo in groupsAndColumns)
            {
                if (iProvideRowLayoutColumnInfo == groupBeingRemoved)
                    continue;

                // If the group or column is parented to the group being removed, then shift the item
                // into the grandparent group. 
                if (groupBeingRemoved == iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.ParentGroup)
                {
                    UltraGridGroup grandparentGroup = groupBeingRemoved.RowLayoutGroupInfo.ParentGroup;
                    if (grandparentGroup != null)
                        iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.ParentGroup = grandparentGroup;
                    else
                        iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.ResetParentGroup();

                    iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.ResetOriginX();
                    iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.ResetOriginY();
                }
            }            
        }
        
        #endregion //ClearGroupFromRowLayoutColumnInfos

        #endregion //NAS 9.1 - Groups in RowLayout
    }

    /// <summary>
    /// Enumerator for the GroupsCollection
    /// </summary>
    public class GroupEnumerator: DisposableObjectEnumeratorBase
    {   
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="groups"></param>
        public GroupEnumerator( GroupsCollection groups ) : base( groups )
        {
        }

      
		/// <summary>
		/// Type-safe version of Current
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridGroup Current 
        {
            get
            {
                return (UltraGridGroup)((IEnumerator)this).Current;
            }
        }
    }
}

