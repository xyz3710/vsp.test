#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved



namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Collections;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	
	/// <summary>
	/// Summary description for SelectedRowsCollection.
	/// </summary>
	// SSP 4/12/04 - Virtual Binding Related
	// Changed the base class from SubObjectsCollectionBase to SparseCollectionBase because we
	// want a fast Contains (as well as remove but to a lesser extent).
	//
	//public class SelectedRowsCollection : SubObjectsCollectionBase
	public class SelectedRowsCollection : SparseCollectionBase
	{
		// SSP 12/11/02 UWG1835
		// Added a back reference to the selected object.
		//
		private Selected selected = null;

		/// <summary>
		/// Constructor
		/// </summary>
		public SelectedRowsCollection()
		{
			
		}

		/// <summary>
		/// Indicates whether the collection is read-only
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				// SSP 12/11/02 UWG1835
				// Added methods for clearing the selected rows, cells or columns as well as adding a
				// range of objects at the same time. We want to return true only for the main selected
				// object and not for the onces that get passed in various selectchange events.
				//
				//return true;

				return null == this.Grid || this.Selected != this.Grid.Selected || this != this.Grid.Selected.Rows;
			}
		}

		// SSP 3/03/03 - Row Layout Functionality
		// This change doesn't have much to do with the row layout functionality, it is an 
		// optimization for selection. Checking to see if the item being added to the list
		// is already in the list is inefficient so added an overload with which you can
		// specify not to do that.
		// 
		internal int InternalAdd( UltraGridRow selectedRow ) 
		{
			return this.InternalAdd( selectedRow, true );
		}

		internal int InternalAdd( UltraGridRow selectedRow, bool validateAlreadyExists ) 
		{
			// SSP 7/8/02 UWG1328
			// If the item is already in the collection don't add it again.
			//
			// SSP 3/03/03 - Row Layout Functionality
			// This change doesn't have much to do with the row layout functionality, it is an 
			// optimization for selection. Checking to see if the item being added to the list
			// is already in the list is inefficient so added an overload with which you can
			// specify not to do that.
			// Commented out below code and added replacement below.
			// --------------------------------------------------------------------------------
			//			if ( this.Contains( selectedRow ) )
			//			{
			//				Debug.Assert( false, "Row is already in the selected rows collection. Can't added it again." );
			//				return this.IndexOf( selectedRow );
			//			}
			if ( validateAlreadyExists )
			{
				int index = this.IndexOf( selectedRow );

				if ( index >= 0 )
				{
					//Debug.Assert( false, "Row is already in the selected rows collection. Can't added it again." );
					return index;
				}
			}
			// --------------------------------------------------------------------------------

			return base.InternalAdd( selectedRow );
		}
		
		// SSP 3/12/04 UWG3072
		// Added RemoveAt.
		//
		internal void RemoveAt( int index )
		{
			this.InternalRemove( index );
		}

		internal void Remove( UltraGridRow selectedRow ) 
		{
			this.InternalRemove( selectedRow );
		}

		internal void InternalInsert( int index, UltraGridRow selectedRow )
		{
			// SSP 7/8/02 UWG1328
			// If the item is already in the collection don't add it again.
			//
			if ( this.Contains( selectedRow ) )
			{
				Debug.Assert( false, "Row is already in the selected rows collection. Can't added it again." );
				return;
			}

			base.InternalInsert( index, selectedRow );
		}
         
		// SSP 1/12/05 BR0155 UWG3736
		// Added InternalClear method.
		//
		internal new void InternalClear( )
		{
			base.InternalClear( );
		}

		/// <summary>
		/// Returns a type safe enumerator.
		/// </summary>
        /// <returns>A type safe enumerator</returns>
		public RowEnumerator GetEnumerator() // non-IEnumerable version
		{	
			return new RowEnumerator( this );
		}

		// JJD 10/04/01
		// Made the indexer public
		//
		/// <summary>
		/// Index into collection
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow this[ int index ] 
		{
			get
			{
				// SSP 2/20/07 BR19262
				// If the grid's row collection has been dirtied (because we recieved Reset notification etc...)
				// then we need to verify that before returning Count here otherwise the SelectedRowsCollection
				// could return rows that have been deleted.
				// 
				if ( null != this.selected )
					this.selected.VerifyRowsNotDirty( );

				return (UltraGridRow)base.GetItem(index);
			}
		}

		// SSP 12/11/02 UWG1835
		// Added methods for clearing the selected rows, cells or columns as well as adding a
		// range of objects at the same time.
		//
		#region Fix For UWG1835

		#region InternalSetList

		internal void InternalSetList( SelectedRowsCollection rows )
		{
			this.InternalClear( );
			this.InternalAddRange( rows );
		}

		#endregion // InternalSetList

		#region All

		// SSP 12/11/02 UWG1835
		// Added methods for clearing the selected rows, cells or columns as well as adding a
		// range of objects at the same time. We want to return true only for the main selected
		// object and not for the onces that get passed in various selectchange events.
		// Overrode All so we can prevent the user from calling the Set.
		//
		/// <summary>
		/// The collection as an array of objects
		/// </summary>
		[ Browsable( false ), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public override object[] All 
		{
			get 
			{
				return base.All;
			}
			set 
			{
				if ( this.IsReadOnly )
					throw new NotSupportedException( );

				// Clear existing selected rows and call AddRange to add the passed in rows.
				//
				this.Clear( );

				if ( value.Length > 0 )
				{
					UltraGridRow[] rows = new UltraGridRow[ value.Length ];

					for ( int i = 0; i < value.Length; i++ )
					{
						rows[i] = (UltraGridRow)value[i];
					}

					this.AddRange( rows );
				}
			}
		}

		#endregion // All

		#region Initialize

		internal void Initialize( Selected selected )
		{
			this.selected = selected;
		}

		#endregion // Initialize

		#region Selected

		internal Infragistics.Win.UltraWinGrid.Selected Selected
		{
			get
			{
				return this.selected;
			}
		}

		#endregion // Selected

		#region Grid

		internal UltraGrid Grid
		{
			get
			{
				return null != this.selected ? this.selected.Grid : null;
			}
		}

		#endregion // Grid

		#region Clear

		/// <summary>
		/// Clears the collection unselecting any selected rows.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="AddRange(UltraGridRow[])"/>, <seealso cref="UltraGridRow.Selected"/></p>
		/// </remarks>
		public void Clear( )
		{			
			UltraGrid grid = this.Grid;

			if ( this.IsReadOnly )
				// SSP 6/23/03 - Localization
				//
				//throw new NotSupportedException( "Clear operation is not supported." );
				throw new NotSupportedException( SR.GetString( "LER_NotSupportedException_346" ) );

			// IsReadOnly should ensure that following condition in Debug.Assert is met before
			// returning true.
			//
			Debug.Assert( this == grid.Selected.Rows, "Grid's selected and this are not the same !" );

			// Call ClearSelectedRows to clear the selected rows. It also fires the select change
			// events.
			//
			grid.ClearSelectedRows( );
		}

		#endregion // Clear

		#region AddRange

		/// <summary>
        /// Adds a range of rows to this SelectedRowsCollection, effectively selecting multiple rows in one operation.
		/// </summary>
		/// <param name="rows">An array of UltraGridRows to be added to the collection.</param>
		/// <remarks>
		/// <p>This method can be used to select a range of rows. It will keep the existing selected rows and select the passed in rows.</p>
		/// <p>You can use UltraGridRow.Selected property to select or unselect individual rows.</p>
		/// <p><seealso cref="Clear"/>, <seealso cref="UltraGridRow.Selected"/></p>
		/// </remarks>		
		public void AddRange( UltraGridRow[] rows )
		{
			this.AddRange( rows, true, true );
		}

		/// <summary>
		/// Selects rows contained in passed in rows array keeping the existing selected rows selected.
		/// </summary>
		/// <param name="rows">Rows to select.</param>
		/// <param name="ensureRowsOrdered">If true then sorts the rows in their visible order. For efficiency reason you may want to specify false for this parameter, if for example the order of rows in the selected rows collection doesn't matter to your logic that accesses and manipulates the selected rows collection or you know the rows to be in the correct order.</param>
		/// <param name="fireSlectionChangeEvent">Specifies whether to fire the <see cref="UltraGrid.BeforeSelectChange"/> and <see cref="UltraGrid.AfterSelectChange"/> events.</param>
		/// <remarks>
		/// <p>This method can be used to select a range of rows. It will keep the existing selected rows and select the passed in rows.</p>
		/// <p>You can use UltraGridRow.Selected property to select or unselect individual rows.</p>
		/// <p><seealso cref="Clear"/>, <seealso cref="UltraGridRow.Selected"/></p>
		/// </remarks>		
		public void AddRange( UltraGridRow[] rows, bool ensureRowsOrdered, bool fireSlectionChangeEvent )
		{
			// If the passed in array is of length 0, then do nothing.
			//
			if ( rows.Length <= 0 )
				return;

			int i;
			UltraGrid grid = this.Grid;

			// Ensure that all the rows are from the same band. We don't allow selecting rows from
			// different bands.
			//
			UltraGridBand band = rows[0].Band;

			for ( i = 1; i < rows.Length; i++ )
			{
				if ( rows[i].BandInternal != band )
					// SSP 6/23/03 - Localization
					//
					//throw new ArgumentException( "Rows must be from the same band.", "rows" );
					throw new ArgumentException( SR.GetString( "LER_ArgumentException_8" ), "rows" );
			}

			// Ensure that the rows are from this grid. We don't want to add rows from a different grid
			// or a different layout to the selected of this grid.
			//
			if ( null == band || band.Layout != grid.DisplayLayout )
				// SSP 6/23/03 - Localization
				//
				//throw new ArgumentException( "Rows must be valid rows from display layout of the same grid.", "rows" );
				throw new ArgumentException( SR.GetString( "LER_ArgumentException_9" ), "rows" );

			SelectType selectType = band.SelectTypeRowResolved;
			
			if ( SelectType.None == selectType )
				// SSP 6/23/03 - Localization
				//
				//throw new InvalidOperationException( "Can't select rows. SelectType is None." );
				throw new InvalidOperationException( SR.GetString( "LER_InvalidOperationException7" ) );

			if ( SelectType.Single == selectType || SelectType.SingleAutoDrag == selectType )
			{
				if ( rows.Length + this.Count > 1 )
					// SSP 6/23/03 - Localization
					//
					//throw new InvalidOperationException( "Can't select more than 1 row when SelectType is Single or SingleAutoDrag" );
					throw new InvalidOperationException( SR.GetString( "LER_InvalidOperationException8" ) );
			}
			// SSP 6/15/05
			// If select type is single and only a single row is being added to 0 count collection then
			// allow the operation. Made the if into else if block.
			// 
			//if ( SelectType.Extended != selectType &&  SelectType.ExtendedAutoDrag != selectType )
			else if ( SelectType.Extended != selectType &&  SelectType.ExtendedAutoDrag != selectType )
				// SSP 6/23/03 - Localization
				//
				//throw new InvalidOperationException( "Row selection not allowed. Invalid SelectType settings." );
				throw new InvalidOperationException( SR.GetString( "LER_InvalidOperationException9" ) );

			// Clear the selected cells since cells and rows can't be selected at the same time. If that gets 
			// cancelled, then return.
			//
			if ( grid.ClearSelectedCells( ) )
				return;

			// SSP 8/17/05 - Optimizations
			// If there are no previously selected rows and the new rows are in correct order
			// then skip sorting and merging.
			// 
			bool needsToMergeAndSort = ensureRowsOrdered || 0 != this.Count;
			if ( needsToMergeAndSort && 0 == this.Count )
			{
				needsToMergeAndSort = false;
				for ( i = 1; ! needsToMergeAndSort && i < rows.Length; i++ )
					needsToMergeAndSort = 0 < UltraWinGrid.Selected.SelectionPositionSortComparer.CompareRow( rows[ i - 1 ], rows[ i ] );
			}

			object[] arr;

			// SSP 8/17/05 - Optimizations
			// If there are no previously selected rows and the new rows are in correct order
			// then skip sorting and merging. Enclosed the existing code into the if block and
			// added the else block.
			// 
			if ( needsToMergeAndSort )
			{
				Hashtable newSelectedRows = new Hashtable( );
				object dummy = new object( );

				// Add the existing rows.
				//
				for ( i = 0; i < this.Count; i++ )
				{
					newSelectedRows[ this[i] ] = dummy;
				}

				// Add the passed in rows.
				//
				for ( i = 0; i < rows.Length; i++ )
				{
					newSelectedRows[ rows[i] ] = dummy;
				}

				arr = new object[ newSelectedRows.Count ];
				newSelectedRows.Keys.CopyTo( arr, 0 );
				newSelectedRows = null;
			
				// Sort the new rows by the selected position.
				//
				Infragistics.Win.Utilities.SortMerge( arr, new Infragistics.Win.UltraWinGrid.Selected.SelectionPositionSortComparer( ) );
			}
			else
			{
				arr = rows;
			}

			// SSP 8/17/05 - Optimizations
			// Added fireSlectionChangeEvent parameter. Added the if block and enclosed the existing
			// code into the else block.
			// 
			if ( ! fireSlectionChangeEvent )
			{
				this.InternalClear( );
				this.InternalAddRange( arr );

				// Set the Selected property to true on each item.
				// 
				foreach ( GridItemBase item in this )
					item.InternalSelect( true );
			}
			else
			{
				Infragistics.Win.UltraWinGrid.Selected newSelection = new Selected( );
				newSelection.Rows.InternalAddRange( arr );

				// Copy the columns since columns and rows can be selected at the same time. Rows and cells
				// can't be selected at the same time.
				//
				newSelection.Columns.InternalSetList( grid.Selected.Columns );

				// Finally select the new selection by calling SelectNewSelection which fires the 
				// select change events.
				//
				grid.SelectNewSelection( typeof( UltraGridRow ), newSelection );
			}
		}

		#endregion // AddRange

		#endregion // Fix For UWG1835

		#region Add

		// SSP 6/15/05
		// Added Add method. We had AddRange but not Add.
		// 
		/// <summary>
		/// Adds the specified row to this selected rows collection.
		/// </summary>
		/// <param name="row">Te row to be added to the collection.</param>
        /// <remarks>Adding a row to the SelectedRowCollection selects the row. This means the row's <see cref="UltraGridRow.Selected"/> property will be set to true.</remarks>
		public void Add( UltraGridRow row )
		{
			this.AddRange( new UltraGridRow[] {  row } );
		}

		#endregion // Add

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo

		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.UltraGridRow[] array, int index)
		{
			base.CopyTo( (System.Array)array, index );
		}

		#endregion //CopyTo

		#region Sort

		// SSP 2/22/06 BR09673
		// Added Sort method.
		// 
		/// <summary>
		/// Sorts the items in the collection according their visible order.
		/// </summary>
		public void Sort( )
		{
			base.Sort( new Infragistics.Win.UltraWinGrid.Selected.SelectionPositionSortComparer( ) );
		}

		#endregion // Sort

		#region Count

		// SSP 2/20/07 BR19262
		// If the grid's row collection has been dirtied (because we recieved Reset notification etc...)
		// then we need to verify that before returning Count here otherwise the SelectedRowsCollection
		// could return rows that have been deleted.
		// 
		/// <summary>
		/// Returns the number of items in the collection.
		/// </summary>
		public override int Count
		{
			get
			{
				int count = base.Count;

				if ( count > 0 && null != this.selected )
				{
					if ( this.selected.VerifyRowsNotDirty( ) )
						count = base.Count;
				}

				return count;
			}
		}

		#endregion // Count
	}


	
}
