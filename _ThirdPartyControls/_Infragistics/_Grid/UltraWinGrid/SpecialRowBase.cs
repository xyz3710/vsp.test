#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;

namespace Infragistics.Win.UltraWinGrid
{
	// SSP 4/14/05 - NAS 5.2 Fixed Rows
	// Added UltraGridSpecialRowBase class. The new UltraGridFilterRow and 
	// UltraGridSummaryRow classes derive from this class.
	//

	#region UltraGridSpecialRowBase Class

	/// <summary>
	/// Base class for non-data rows, like UltraGridFilterRow and UltraGridSummaryRow.
	/// </summary>
	public class UltraGridSpecialRowBase : UltraGridRow
	{
		#region Constructor
		
		internal UltraGridSpecialRowBase( RowsCollection parentCollection ) : base( parentCollection.Band, parentCollection )
		{
		}

		#endregion // Constructor

		#region Base Overrides

		#region GetCellValue

		/// <summary>
		/// Overridden. Returns the filter operand value.
		/// </summary>
        /// <param name="column">The column intersecting the row from which the value will be retrieved.</param>
        /// <returns>The filter operand value.</returns>
		public override object GetCellValue( Infragistics.Win.UltraWinGrid.UltraGridColumn column )
		{
			// AS 1/19/06
			// Setting row sizing to fixed with empty rows is getting into here.
			// and getting stuck in an endless loop of asserts.
			//
			//Debug.Assert( false, "GetCellValue called on a non-data row." );
			return null;
		}

		#endregion // GetCellValue

		#region SetCellValue

		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// Changed the return type from void to bool. We need to know if the action was canceled or not.
		// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
		// 
		//internal override void SetCellValue( 
		internal override bool SetCellValue( 
			Infragistics.Win.UltraWinGrid.UltraGridColumn column, 
			object val, bool suppressErrorMessagePrompt,
			// SSP 2/21/06 BR10249
			// Added an overload that takes in throwExceptionOnError parameter. Pass that along.
			// 
			bool throwExceptionOnError )
		{
			Debug.Assert( false, "SetCellValue called on a non-data row." );

			// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
			// Changed the return type from void to bool. We need to know if the action was canceled or not.
			// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
			// 
			return false;
		}

		#endregion // SetCellValue

		#region AutoPreviewHeight 

		/// <summary>
		/// Overridden. Always returns 0 as the filter row never displays an auto-preview.
		/// </summary>
		public override int AutoPreviewHeight 
		{
			get
			{
				// No auto previews for filter row.
				//
				return 0;
			}
		}

		#endregion // AutoPreviewHeight 

		#region IsDataRow

		/// <summary>
		/// Returns true if the row is associated with a row in the data source. This property returns
		/// false for group-by rows, filter rows, template add-rows and summary rows.
		/// </summary>
		public override bool IsDataRow
		{
			get
			{
				return false;
			}
		}

		#endregion // IsDataRow

		#region FireInitializeRow
		
		internal override void FireInitializeRow( )
		{
			// Do nothing since only the data rows should fire InitializeRow event.
		}

		#endregion // FireInitializeRow

		#region Update

		/// <summary>
		/// Overridden. <b>Update</b> does not do anything in this context because this is not a data row. It has no associated list object in the binding list.
		/// </summary>
		/// <returns>true</returns>
		public override bool Update( )
		{
			return true;
		}

		#endregion // Update

		#region IsSpecialRow
		
		internal override bool IsSpecialRow
		{
			get
			{
				return true;
			}
		}

		#endregion // IsSpecialRow

		#region InternalPositionToRow

		internal override void InternalPositionToRow( System.Collections.Stack oldPositions )
		{
			// call this method on the parent row first since that has to
			// be set first to give the proper context
			//
			UltraGridRow parentRow = this.ParentRow;
			if ( null != parentRow )
				parentRow.InternalPositionToRow( oldPositions );
		}

		#endregion // InternalPositionToRow

		#region RowScrollTip

		// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries
		// Added RowScrollTip property.
		// 
		internal override string RowScrollTip
		{
			get
			{
				return null;
			}
		}

		#endregion // RowScrollTip

		#endregion // Base Overrides
	}

	#endregion // UltraGridSpecialRowBase Class

	#region HeadersRow Class

	/// <summary>
	/// A row class that represents headers.
	/// </summary>
	public class HeadersRow : UltraGridSpecialRowBase
	{
		#region Private Vars

		// SSP 9/22/06 BR14784
		// 
		private bool isAttachedHeader;

		#endregion // Private Vars

		#region Constructor

		// SSP 9/22/06 BR14784
		// 
		//internal HeadersRow( RowsCollection parentCollection ) : base( parentCollection )
		internal HeadersRow( RowsCollection parentCollection, bool isAttachedHeader ) : base( parentCollection )
		{
			// SSP 9/22/06 BR14784
			// 
			this.isAttachedHeader = isAttachedHeader;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal static int c = 0;

		#endregion // Constructor

		#region Private/Internal Methods/Properties

		#region IsAttachedHeader

		// SSP 9/22/06 BR14784
		// 
		internal bool IsAttachedHeader
		{
			get
			{
				return this.isAttachedHeader;
			}
		}

		#endregion // IsAttachedHeader

		#endregion // Private/Internal Methods/Properties

		#region Base Overrides

		#region BaseHeight

		internal override int BaseHeight
		{
			get
			{
				return 0;
			}
		}

		#endregion // BaseHeight

		#region RowSpacingBeforeResolvedBase

		internal override int RowSpacingBeforeResolvedBase
		{
			get
			{
				return 0;
			}
		}

		#endregion // RowSpacingBeforeResolvedBase

		#region RowSpacingAfterResolvedBase

		internal override int RowSpacingAfterResolvedBase
		{
			get
			{
				return 0;
			}
		}

		#endregion // RowSpacingAfterResolvedBase

		#region HasHeadersOverride
		
		internal override DefaultableBoolean HasHeadersOverride
		{
			get
			{
				return DefaultableBoolean.True;
			}
		}

		#endregion // HasHeadersOverride

		#region IsStillValid 

		/// <summary>
		/// True only if this row and all of its ancestor rows are still valid (read-only).
		/// </summary>
		public override bool IsStillValid 
		{ 
			get
			{
				return null != this.ParentCollection;
			}
		}

		#endregion // IsStillValid 

		#region FixedResolvedLocation

		internal override FixedRowStyle FixedResolvedLocation
		{
			get
			{
				// SSP 9/22/06 BR14784
				// 
				//return FixedRowStyle.Top;
				return this.isAttachedHeader ? FixedRowStyle.Top : base.FixedResolvedLocation;
			}
		}

		#endregion // FixedResolvedLocation

		#endregion // Base Overrides
	}

	#endregion // HeadersRow Class

}
