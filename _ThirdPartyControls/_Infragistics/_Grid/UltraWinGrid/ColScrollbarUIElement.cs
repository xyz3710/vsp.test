#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Diagnostics;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

	using Infragistics.Win.UltraWinScrollBar;

	

	/// <summary>
	/// ColScrollbarUIElement
	/// </summary>
	public class ColScrollbarUIElement : Infragistics.Win.UltraWinScrollBar.ScrollBarUIElement
	{

		private ColScrollRegion colScrollRegion;

		internal ColScrollbarUIElement( UIElement parent, ScrollBarInfo scrollInfo )
			: base( parent, scrollInfo )
		{
		}
		internal void initialize( ColScrollRegion colScrollRegion, ScrollBarInfo scrollInfo )
		{
			this.colScrollRegion = colScrollRegion;
			this.PrimaryContext = colScrollRegion;

			// reset the scroll info internal member
			this.Reset( scrollInfo );
		}


		/// <summary>
		/// Returns the ColScrollRegion object to which a UIElement belongs. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the colscrollregion to which the UIElement belongs. You can use this reference to access any of the returned colscrollregion's  properties or methods.</p>
		/// <p class="body">If the UIElement does not belong to a colscrollregion, Nothing is returned.</p>
		/// <p class="body">The <b>RowScrollRegion</b> property can be used to return a reference to a RowScrollRegion object to which a UIElement belongs.</p>
		/// </remarks>
		public ColScrollRegion ColScrollRegion
		{
			get
			{
				return this.colScrollRegion;
			}
		}
	}
}
