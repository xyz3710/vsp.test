#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms.Design;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.InteropServices;
using System.Globalization;

// SSP 3/30/05 - NAS 5.2 Filter Row
// Split EmbeddableEditorOwnerInfo into GridEmbeddableEditorOwnerInfoBase,
// GridCellEmbeddableEditorOwnerInfoBase, GridCellEmbeddableEditorOwnerInfo,
// FilterOperandEmbeddableEditorOwnerInfo and FilterOperatorEmbeddableEditorOwnerInfo.
//

namespace Infragistics.Win.UltraWinGrid
{
	#region GridEmbeddableEditorOwnerInfoBase Class

	internal abstract class GridEmbeddableEditorOwnerInfoBase : Infragistics.Win.EmbeddableEditorOwnerBase
	{
		#region Veriables

		// Associated column and band and layout.
		protected UltraGridColumn column = null;
		protected UltraGridBand band = null;
		protected UltraGridLayout layout = null;

		// This flag is set to true if the grid initiates entering of the edit mode.
		internal bool dontHandleOnBeforeEnterEditMode = false;

		// SSP 11/4/05 BR07555
		// Need to pass printing param when measuring and rendering in Whidbey.
		// 
		private int cached_IsPrinting = -1;

		#endregion // Veriables

		#region Constructor

		internal GridEmbeddableEditorOwnerInfoBase( UltraGridColumn column ) : base( )
		{
			this.column = column;
			this.band = column.Band;
			this.layout = this.band.Layout;

			GridUtils.ValidateNull( layout );
		}

		#endregion    //Constructor

		#region Private/Internal properties

		#region Column

		internal UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		#endregion // Column

		#region Band

		internal UltraGridBand Band
		{
			get
			{
				return this.column.Band;
			}
		}

		#endregion // Band

		#region Layout

		internal UltraGridLayout Layout
		{
			get
			{
				return this.layout;
			}
		}

		#endregion // Layout

		#region Grid

		internal UltraGridBase Grid
		{
			get
			{
				return this.layout.Grid;
			}
		}

		#endregion // Grid

        // MBS 11/14/07 - BR28423
        #region GetCellIfAllocatedFromOwnerContext

        internal static UltraGridCell GetCellIfAllocatedFromOwnerContext(object owner, object ownerContext)
        {
            GridEmbeddableEditorOwnerInfoBase ownerInfo = owner as GridEmbeddableEditorOwnerInfoBase;
            if (ownerInfo == null || ownerInfo.Column == null)
                return null;

            UltraGridRow row = GetRowFromOwnerContext(ownerContext);
            if (row == null)
                return null;

            return row.GetCellIfAllocated(ownerInfo.Column);
        }
        #endregion //GetCellIfAllocatedFromOwnerContext

		#region GetOwnerContext

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Split the CellUIElement into CellUIElementBase class and CellUIElement class so 
		// the filter cell element can derive from CellUIElementBase.
		//
		//internal static object GetOwnerContext( CellUIElement cellElement )
		internal static object GetOwnerContext( CellUIElementBase cellElement )
		{
			return cellElement;
		}

		#endregion  // GetOwnerContext

		#region GetRowFromOwnerContext

		internal static UltraGridRow GetRowFromOwnerContext( object ownerContext )
		{
			// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			// Split the CellUIElement into CellUIElementBase class and CellUIElement class so 
			// the filter cell element can derive from CellUIElementBase.
			//
			//CellUIElement cellElem = ownerContext as CellUIElement;
			CellUIElementBase cellElem = ownerContext as CellUIElementBase;

			if ( null != cellElem )
				return cellElem.Row;

			// SSP 11/12/04 - Merged Cell Feature
			// Now the owner context could be a MergedCellUIElement as well.
			//
			//Debug.Assert( ownerContext is UltraGridRow, "Owner context something other than a CellUIElement or a UltraGridRow !" );
			//return ownerContext as UltraGridRow;
			UltraGridRow row = ownerContext as UltraGridRow;
			if ( null == row )
			{				
				// SSP 9/8/05 BR06252
				// 
				//MergedCellUIElement mergedCellElem = ownerContext as MergedCellUIElement;
				MergedCellUIElement mergedCellElem = GetWrappedElement( ownerContext ) as MergedCellUIElement;

				if ( null != mergedCellElem )
					row = mergedCellElem.Row;

				// SSP 4/6/05 - NAS 5.2 Filter Row
				//
				if ( null == row )
				{
					// SSP 9/8/05 BR06252
					// 
					//UIElement elem = ownerContext as UIElement;
					UIElement elem = GetWrappedElement( ownerContext );

					if ( null != elem )
						row = (UltraGridRow)elem.GetContext( typeof( UltraGridRow ), true );

					// SSP 5/19/05
					// Also allow for cell as the context for convenience.
					//
					if ( null == row )
					{
						UltraGridCell cell = ownerContext as UltraGridCell;
						if ( null != cell )
							row = cell.Row;
					}
				}
			}

			//Debug.Assert( null != row, "Owner context something other than a CellUIElement, an UltraGridRow or a MergedCellUIElement !" );
			return row;
		}
		
		#endregion // GetRowFromOwnerContext

		#region GetWrappedElemenet

		// SSP 9/8/05 BR06252
		// 
		internal static UIElement GetWrappedElement( object ownerContext )
		{
			UIElement elem = ownerContext as UIElement;
			if ( null == elem )
			{
				ToolTipOwnerContext toolTipOwnerContext = ownerContext as ToolTipOwnerContext;
				if ( null != toolTipOwnerContext )
					elem = GetWrappedElement( toolTipOwnerContext.ownerContext );
			}

			return elem;
		}

		#endregion // GetWrappedElemenet

		#region GetWrappedCellElement

		// SSP 9/8/05 BR06252
		// 
		internal static CellUIElementBase GetWrappedCellElement( object ownerContext )
		{
			CellUIElementBase cellElem = ownerContext as CellUIElementBase;
			if ( null == cellElem )
				cellElem = GetWrappedElement( ownerContext ) as CellUIElementBase;

			return cellElem;
		}

		#endregion // GetWrappedCellElement

		#region InternalGetRCRIntersectionElemHelper

		// SSP 9/8/05 BR06252
		// 
		private static RowColRegionIntersectionUIElement InternalGetRCRIntersectionElemHelper( object ownerContext )
		{
			UIElement element = GetWrappedElement( ownerContext );

			return null != element 
				? (RowColRegionIntersectionUIElement)element.GetAncestor( typeof( RowColRegionIntersectionUIElement ) )
				: null;
		}

		#endregion // InternalGetRCRIntersectionElemHelper

		#region GetRowScrollRegion

		internal static RowScrollRegion GetRowScrollRegion( object ownerContext )
		{
			// SSP 9/8/05 BR06252
			// 
			// ------------------------------------------------------------------------
			RowColRegionIntersectionUIElement rowColIntersectionElement = InternalGetRCRIntersectionElemHelper( ownerContext );
			
			// ------------------------------------------------------------------------

			Debug.Assert( null != rowColIntersectionElement, "Element has no ancestor of row col intersection ui element !" );
			
			if ( null != rowColIntersectionElement )
				return rowColIntersectionElement.RowScrollRegion;

			return null;
		}
		
		#endregion // GetRowScrollRegion

		#region GetColScrollRegion

		internal static ColScrollRegion GetColScrollRegion( object ownerContext )
		{
			// SSP 9/8/05 BR06252
			// 
			// ------------------------------------------------------------------------
			RowColRegionIntersectionUIElement rowColIntersectionElement = InternalGetRCRIntersectionElemHelper( ownerContext );
			
			// ------------------------------------------------------------------------

			Debug.Assert( null != rowColIntersectionElement, "Element has no ancestor of row col intersection ui element !" );

			if ( null != rowColIntersectionElement )
				return rowColIntersectionElement.ColScrollRegion;

			return null;
		}
		
		#endregion // GetColScrollRegion

		#region GetRCRIntersectionElem

		internal RowColRegionIntersectionUIElement GetRCRIntersectionElem( object ownerContext )
		{
			// if we got the row, get the associated scroll regions...
			RowScrollRegion rsr = EmbeddableEditorOwnerInfo.GetRowScrollRegion( ownerContext );
			ColScrollRegion csr = EmbeddableEditorOwnerInfo.GetColScrollRegion( ownerContext );

			if (rsr == null)
				rsr = this.Grid.ActiveRowScrollRegion;

			if (csr == null)
				csr = this.Grid.ActiveColScrollRegion;

			UIElement mainElem = this.Layout.UIElement;
			return null != mainElem 
				? (RowColRegionIntersectionUIElement)mainElem.GetDescendant( typeof( RowColRegionIntersectionUIElement ), new object[] { rsr, csr } )
				: null;
		}

		#endregion // GetRCRIntersectionElem

		#endregion  // Private/Internal properties

		#region Public Properties
		
		#region DesignMode
		
		/// <summary>
		/// Returns true if in design mode, false otherwise.
		/// </summary>
		public override bool DesignMode 
		{ 
			get
			{
				// SSP 3/30/05 - Optimizations
				//
				UltraGridBase grid = this.Grid;
				return null != grid && grid.DesignMode;
				
			}
		}
		
		#endregion //DesignMode

		#endregion    //Public Properties

		#region Implemented Abstract Methods Overridden

		#region GetControl	

		/// <summary>
		/// Returns the owner's control. 
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>The owner's control. This is used e.g. to re-parent the edit control.</returns>
		public override System.Windows.Forms.Control GetControl( object ownerContext )
		{
			Debug.Assert( null != this.Grid, "No grid associated with the layout !" );

			return this.Grid;
		}

		#endregion    //GetControl	

		#region GetPadding	

		/// <summary>
		/// The padding to place around the value to display.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="padding">(out) The padding to place around the value to display.</param>
		/// <returns>A boolean indicating whether a meaningful value was returned.</returns>
		public override bool GetPadding( object ownerContext, out System.Drawing.Size padding )
		{
			// SSP 5/13/03 - Optimizations
			//
			//padding = new Size( this.Column.Band.CellPaddingResolved, this.Column.Band.CellPaddingResolved );
			int cellPadding = this.band.CellPaddingResolved;
			padding = new Size( cellPadding, cellPadding );
			return true;
		}

		#endregion    //GetPadding	

		#region IsEnabled	

		/// <summary>
		/// Returns whether the value is enabled for editing.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>True if the value is enabled for editing.</returns>
		public override bool IsEnabled( object ownerContext )
		{
			// SSP 9/8/05 BR06252
			// 
			//UIElement element = ownerContext as UIElement;
			UIElement element = GetWrappedElement( ownerContext );

			return null != element && element.Enabled;
		}

		#endregion    //IsEnabled	

		#region IsReadOnly	

		/// <summary>
		/// Returns true is the value is read-only
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>A boolean indicating whether the value is read only</returns>
		/// <remarks>The default implementation returns false.</remarks>
		public override bool IsReadOnly( object ownerContext )
		{
			return ! this.IsEnabled( ownerContext );
		}

		#endregion    //IsReadOnly	

		#region IsInputKey

		/// <summary>
		/// Returns whether the key is used by the owner.
		/// </summary>
		/// <param name="keyDataArg"></param>
		/// <param name="element">The EmbeddableUIElementBase-derived element</param>
		/// <returns>True if the key is used by the owner (e.g. an arrorw or tab key used for internal navigation).</returns>
		public override bool IsKeyMapped( Keys keyDataArg, EmbeddableUIElementBase element )
		{
			Debug.Assert( null != this.Column, "Null column off the EmbeddableEditorOwnerInfo !" );

			if ( null == this.Column )
				return false;

			UltraGrid grid = this.Column.Layout.Grid as UltraGrid;
	
			Debug.Assert( null != grid, "Null grid !" );

			if ( null == grid )
				return false;

			// If Alt is down, then return false.
			//
			if ( Keys.Alt == ( Keys.Alt & keyDataArg ) )
				return false;

			//	BF 7.2.02
			//	Use the new IsKeyMapped method off KeyActionMappingsBase
			return grid.KeyActionMappings.IsKeyMapped( keyDataArg, grid.GetCurrentState() );

	#region Obsolete code (BF 7.2.02)
			
	#endregion //	Obsolete code (BF 7.2.02)
		}

		#endregion // IsInputKey

		#region OnEnterEditor

		/// <summary>
		/// Called when focus is entering the editor. 
		/// </summary>
		/// <param name="editor">The editor who is getting focus.</param>
		public override void OnEnterEditor( EmbeddableEditorBase editor )
		{
			
			//
		}

		#endregion    //OnEnterEditor

		#region OnLeaveEditor

		/// <summary>
		/// Called when focus is leaving the editor. 
		/// </summary>
		/// <param name="editor">The editor who is losing focus.</param>
		public override void OnLeaveEditor( EmbeddableEditorBase editor )
		{
			
			//
		}

		#endregion    //OnLeaveEditor

		#region OnEditorMouseDown

		// SSP 1/2/03
		// Optimizations.
		//
		/// <summary>
		/// The editor calls this method whenever any of its embeddable elements gets a mouse down.
		/// </summary>
		/// <param name="embeddableElem"></param>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		public override void OnEditorMouseDown( EmbeddableUIElementBase embeddableElem, EmbeddableMouseDownEventArgs e )
		{
			// SSP 11/12/04 - Merged Cell Feature
			//
			// --------------------------------------------------------------------------------
			object ownerContext = embeddableElem.OwnerContext;
			if ( ownerContext is MergedCellUIElement )
			{
				((MergedCellUIElement)ownerContext).OnEmbeddableElementMouseDown( this, e );
				return;
			}
			// --------------------------------------------------------------------------------

			// SSP 4/16/05 - NAS 5.2 Filter Row
			//
			//CellUIElementBase cellElem = ownerContext as CellUIElementBase;
			// SSP 9/8/05 BR06252
			// 
			//UIElement elem = ownerContext as UIElement;
			UIElement elem = GetWrappedElement( ownerContext );

			CellUIElementBase cellElem = null != elem 
				? (CellUIElementBase)elem.GetAncestor( typeof( CellUIElementBase ) ) : null;

            // MBS 2/19/08 - RowEditTemplate
            // Changed the assert since the proxy will be delegating here and the owner context
            // will be the row, not the cell.
            //
			//Debug.Assert( null != cellElem, "Owner context is not a cell ui element !" );
            Debug.Assert(null != cellElem || embeddableElem.Owner is UltraGridCellProxyOwnerInfo, "Owner context is not a cell ui element !");

			if ( null != cellElem )
			{
				cellElem.OnEmbeddableElementMouseDown( this, e );
			}
		}

		#endregion // OnEditorMouseDown

		// SSP 7/3/03 - Ink Editor Related
		// Overrode newly added OnEditorBeforeEnterEditMode method. The reason for adding
		// was so that if someone other than the grid calls EnterEditMode on the editor
		// (like the ink editor code which doesn't know anything about the grid )
		// then the grid won't get notified that the editor is going into edit mode becase
		// the grid isn't hooked into the editor's events yet.
		// 
		#region OnEditorBeforeEnterEditMode

		/// <summary>
		/// Called before edit mode has been entered. 
		/// </summary>
		/// <param name="editor">The editor about to enter edit mode.</param>
		/// <param name="embeddableElement">The element entering edit mode.</param>
		/// <returns>False to cancel the operation.</returns>
		protected override bool OnEditorBeforeEnterEditMode( EmbeddableEditorBase editor, EmbeddableUIElementBase embeddableElement )
		{
			// Added OnEditorBeforeEnterEditMode virtual method off the owner which gets
			// called when the EnterEditMode is called on the editor. However we don't
			// want to execute the code there if we are calling it ourselves because
			// we already have done all that.
			//
			if ( this.dontHandleOnBeforeEnterEditMode )
				return true;

			// Check if the editor and the embeddable element's editor match.
			//
			if ( editor != embeddableElement.Editor )
				throw new InvalidOperationException( "Embeddable element's Editor does not match the editor parameter of OnEditorBeforeEnterEditMode." );

			// Check if this is the emebeddable element's owner.
			//
			if ( this != embeddableElement.Owner )
				throw new InvalidOperationException( "Embeddable element's Owner does not match the owner OnEditorBeforeEnterEditMode method is called on." );

			UltraGridRow row = GetRowFromOwnerContext( embeddableElement.OwnerContext );
			if ( null == row )
				throw new InvalidOperationException( "Invalid onwer context on the embeddable element." );

			// Check if the bands match.
			//
			Debug.Assert( row.Band == this.Band, "Bands must match." );				
			if ( row.Band != this.Band )
				return false;

			UltraGridCell cell = row.Cells[ this.Column ];
	
			Debug.Assert( null != cell, "Null cell !" );
			if ( null == cell )
				return false;

			// Only UltraGrid supports entering edit mode.
			//
			UltraGrid grid = this.Grid as UltraGrid;
			if ( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_V2_NotSupportedException_309"));

			// Don't enter edit mode during design time.
			// 
			if ( grid.DesignMode )
				return false;

			// Clear the selected items. If that gets cancelled, then return false.
			// Note how the retrun value of true of ClearAllSelected means it's canceled.
			//
			bool cancel = grid.ClearAllSelected();
			if ( cancel )
				return false;

			if ( ! cell.CanEnterEditMode )
			{
				// SSP 4/16/05
				//
				//throw new InvalidOperationException( "Cell's Activation settings do not allow for it to enter edit mode or the editor doesn't support editing on the cell's data type." );
				Debug.Assert( false, "Cell's Activation settings do not allow for it to enter edit mode or the editor doesn't support editing on the cell's data type." );
				return false;
			}

			// If the row scroll region and the col scroll region the emebddable element is
			// in are not the active scroll regions, then make them
			//
			RowScrollRegion rsr = GetRowScrollRegion( embeddableElement.OwnerContext );
			ColScrollRegion csr = GetColScrollRegion( embeddableElement.OwnerContext );
			Debug.Assert( null != rsr && null != csr, "No rsr or csr !" );
			grid.ActiveRowScrollRegion = rsr;
			grid.ActiveColScrollRegion = csr;
			if ( grid.ActiveRowScrollRegion != rsr || grid.ActiveColScrollRegion != csr )
				return false;

			// SSP 4/16/05 - NAS 5.2 Filter Row
			// Added the following if block and enclosed the existing code into the else block.
			// Also the code enclosed in the else block was moved down here from above the 
			// above check for cell.CanEnterEditMode.
			//
			bool isFilterOperatorEdiorElem = null != embeddableElement.GetAncestor( typeof( FilterOperatorUIElement ) );
			if ( isFilterOperatorEdiorElem )
			{
				if ( editor != this.Column.FilterOperatorEditorOwnerInfo.GetEditor( embeddableElement.OwnerContext ) )
				{
					Debug.Assert( false, "Filter operator editor doesn't match the embeddable element's editor." );
					return false;
				}
			}
			else
			{
				// Editor has to match the resolved editor of the cell.
				//
				Debug.Assert( cell.EditorResolved == editor, "Wrong editor passed in !" );
				if ( cell.EditorResolved != editor )
				{
					// SSP 4/16/05
					//
					//throw new InvalidOperationException( "Cell's editor does not match the editor of the embeddable element attempting to enter the edit mode." );
					Debug.Assert( false, "Cell's editor does not match the editor of the embeddable element attempting to enter the edit mode." );
					return false;
				}
			}

			// Activate the cell if already not activated.
			//
			grid.ActiveCell = cell;

			// If the cell activation fails (because it's canceled or there is an error updating
			// the value of the cell previously in edit mode) then return false to indicate that
			// it's canceled.
			//
			if ( grid.ActiveCell != cell )
				return false;

			// Call HookIntoEditor to hook the editor into edit mode.
			//
			// SSP 4/16/05 - NAS 5.2 Filter Row
			// Added the if block and enclosed the existing code into the else block.
			//
			if ( isFilterOperatorEdiorElem )
			{
				// If operator editor is going into edit mode than also make the operand
				// go into edit mode.
				//
				bool failed = cell.EnterEditMode( false );
				if ( failed )
					return false;
			}
			else
			{
				this.Layout.HookIntoEditor( editor );
			}

			return true;
		}

		#endregion // OnEditorBeforeEnterEditMode

		#region GetImageList

		/// <summary>
		/// Returns the image list to be used by the editor's ValueList, or null
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>An ImageList, or null if not supported</returns>
		/// <remarks>The default implementation returns <b>null</b>.</remarks>
		public override ImageList GetImageList( object ownerContext )
		{
			UltraGridBase grid = this.Grid;
			return null != grid ? grid.ImageList : base.GetImageList( ownerContext );
		}

		#endregion GetImageList

		#region ShouldDrawFocus	

		/// <summary>
		/// Determines if a focus rect should be drawn.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>Returns true if a focus rect should be drawn.</returns>
		public override bool ShouldDrawFocus( object ownerContext )
		{
			// SSP 7/25/02 UWG1441
			// Cell elements draw their own focus rects. So the editors should never do that.
			// Commented out below code and returned false.
			//
			// -----------------------------------------------------------------------
			

			return false;
			// -----------------------------------------------------------------------
		}

		#endregion    //ShouldDrawFocus	

		#endregion // Implemented Abstract Methods Overridden

		#region Virtual Methods

		#region AccessibilityNotifyClients

		public override void AccessibilityNotifyClients(object ownerContext, System.Windows.Forms.AccessibleEvents eventId)
		{
			// JJD 01/08/04
			// Only pass the notification along if the grid has focus
			// AS 1/16/04
			// Why would we not want to pass along a selection notification
			// from a child accessible object? Anyway, only impose
			// this limitation, if we are sending a focus notification.
			//
			//if ( this.column.Layout.Grid.Focused )
			if (eventId != AccessibleEvents.Focus || this.Grid.Focused)
				this.Grid.AccessibilityNotifyClientsInternal( eventId, 0 );		
		}

		#endregion AccessibilityNotifyClients

		//	BF 7.29.02	(related to UWG1453)
		#region GetContextMenu
		/// <summary>
		/// Returns the ContextMenu that will be displayed by the editor
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>A ContextMenu</returns>
		/// <remarks>The default implementation returns <b>null</b></remarks>
		public override ContextMenu GetContextMenu( object ownerContext )
		{
			//	If the ContextMenu property is not null, return it
			if ( null != this.Grid && null != this.Grid.ContextMenu )
				return this.Grid.ContextMenu;
	
			//	Since the grid's ContextMenu property was null,
			//	we will call the base class implementation, which will
			//	try to get one from the default owner, if there is one
			return base.GetContextMenu( ownerContext );
		}
		#endregion //	GetContextMenu	

        // MRS 11/21/05 - BR07839
        #region GetContextMenuStrip
        /// <summary>
        /// Returns the ContextMenuStrip that will be displayed by the editor
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>A ContextMenuStrip</returns>
        /// <remarks>The default implementation returns <b>null</b></remarks>
        public override ContextMenuStrip GetContextMenuStrip(object ownerContext)
        {
            //	If the ContextMenuStrip property is not null, return it
            if (null != this.Grid && null != this.Grid.ContextMenuStrip)
                return this.Grid.ContextMenuStrip;

            //	Since the grid's ContextMenuStrip property was null,
            //	we will call the base class implementation, which will
            //	try to get one from the default owner, if there is one
            return base.GetContextMenuStrip(ownerContext);
        }
        #endregion //	GetContextMenuStrip	

		#region IsVertical

		/// <summary>
		/// Returns whether the element should display vertical.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>A boolean indicating whether the display is vertical</returns>
		/// <remarks>The default implementation returns false.</remarks>
		public override bool IsVertical( object ownerContext )
		{
			return false;
		}

		#endregion //IsVertical

		#region EnterEditModeOnClick

		/// <summary>
		/// If true is returned, the editor will enter edit mode on either
		/// MouseDown or MouseUp, depending on the nature of the editor
		/// </summary>
		public override bool EnterEditModeOnClick( object ownerContext )
		{
			return false;
		}

		#endregion // EnterEditModeOnClick 

		#region GetBorderStyle

		// SSP 7/26/02 UWG1449
		// Overrode GetBorderStyle method.
		//
		/// <summary>
		/// Returns the BorderStyle to be used by the editor's embeddable element
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="borderStyle">The BorderStyle to be used by the editor's embeddable element</param>
		/// <returns></returns>
		/// <remarks>The default implementation returns <b>false</b>, with borderStyle set to UIElementBorderStyle.Default.</remarks>
		public override bool GetBorderStyle( object ownerContext, out UIElementBorderStyle borderStyle )
		{
			// Since the cells draw their own borders, the border style for the embeddable
			// editor element should be none.
			// 
			borderStyle = UIElementBorderStyle.None;
			return true;
		}

		#endregion // GetBorderStyle

		//	BF 10.9.02	UWG1732
		//
		//	The grid does not want an I-Beam to be displayed
		//	by embeddable elements, so unconditionally return false
		//	from this method.
		//
		#region UseDefaultEditorCursor
		/// <summary>
		/// Returns whether the editor should use its default cursor
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override bool UseDefaultEditorCursor( object ownerContext )
		{
			return false;
		}
		#endregion UseDefaultEditorCursor

		// MRS 5/21/04 - UWG2915
		#region GetCachedGraphics
		/// <summary>
		/// Called to get a graphics object suitable for doing metrics calculations only.
		/// </summary>
		/// <returns>A graphics object suitable for doing metrics calculations only. This graphics object shouldn't be rendered on.</returns>
		/// <param name="ownerContext">The owner context which will be used to get the Control this owner is associated with.</param>
		/// <remarks>
		/// <p class="body">Do NOT call the <b>Dispose</b> method on the graphics object returned from this method.</p>
		/// <p class="body">Instead, each call to this method should be paired with a call to <see cref="ReleaseCachedGraphics"/>.</p>
		/// <p class="body">During graphics caching calls to <see cref="GetCachedGraphics"/> will return a single cached graphics object and calls to <see cref="ReleaseCachedGraphics"/> will be ignored.</p>
		/// </remarks>
		public override Graphics GetCachedGraphics( object ownerContext )
		{	
			// MRS 6/1/05 - UWG2915
			// Call the method on the Layout, instead
			//			if ( !this.column.Layout.IsPrintLayout )
			//				return base.GetCachedGraphics(ownerContext);
			//			else
			//				return ((UltraGrid)this.column.Layout.Grid).PrintManager.PrinterMetricsGraphics;
			return this.Layout.GetCachedGraphics();
    
		}
#endregion    //GetCachedGraphics	

		#region ReleaseCachedGraphics
		/// <summary>
		/// Called to release a graphics object that was returned from a prior call to <see cref="GetCachedGraphics"/>.
		/// </summary>
		/// <param name="gr">The graphics object to release.</param>
		/// <remarks>
		/// <p class="body">Do NOT call the <b>Dispose</b> method on the graphics object returned from <see cref="GetCachedGraphics"/>. Use this method instead.</p>
		/// <p class="body">During graphics caching calls to <see cref="GetCachedGraphics"/> will return a single cached graphics object and calls to <see cref="ReleaseCachedGraphics"/> will be ignored.</p>
		/// </remarks>
		public override void ReleaseCachedGraphics( Graphics gr )
		{			
			// MRS 6/1/05 - UWG2915
			// Call the method on the Layout, instead
			//			if ( !this.column.Layout.IsPrintLayout )
			//				base.ReleaseCachedGraphics( gr );
			this.Layout.ReleaseCachedGraphics( gr );
		}
	#endregion    //ReleaseCachedGraphics	

		// JAS 1/13/05 BR01677
		#region ShouldShowOverflowIndicator

		/// <summary>
		/// Sets the out parameters with values relating to whether the editor's overflow indicator should be shown
		/// and if it should show a tooltip when the cursor enters the overflow indicator.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="showOverflowIndicator">Specifies whether to display the overflow indicator.</param>
		/// <param name="showToolTip">Specifies whether to display the overflow tooltip.</param>
		/// <param name="indicatorImage">The image to be shown in the overflow indicator, or null.</param>
		/// <remarks>'showToolTip' is always set to false.</remarks>
		public override void ShouldShowOverflowIndicator( object ownerContext, out bool showOverflowIndicator, out bool showToolTip, out Image indicatorImage )
		{
			base.ShouldShowOverflowIndicator( ownerContext, out showOverflowIndicator, out showToolTip, out indicatorImage );
	
			showToolTip = false;
		}

		#endregion // ShouldShowOverflowIndicator

		#region ResolveAppearance	

		/// <summary>
		/// Resolves the appearance for an element.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="appearance">The appearance structure to initialize.</param>
		/// <param name="requestedProps">The appearance properties to resolve.</param>
		/// <param name="area">Enumeration of type <see cref="EmbeddableEditorArea"/> describing the area of the embeddable element to which the appearance will be applied</param>
		/// <param name="hotTracking">Boolean indicating whether the owner should apply its 'HotTrackingAppearance'</param>
		/// <param name="customArea">A string that denotes which appearance to resolve. Applicable only when the 'area' parameter is set to Custom.</param>
		/// <returns>True if the owner recognizes and supports the named appearance.</returns>
		public override bool ResolveAppearance ( object ownerContext,
			ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps,
			EmbeddableEditorArea area,
			bool hotTracking,
			string customArea )
		{
            return this.ResolveAppearance(ownerContext, ref appearance, ref requestedProps, area, hotTracking, customArea, false);
        }

        // MBS 5/5/08 - RowEditTemplate NA2008 V2
        // Added an additional overload so that we can distinguish between resolving the proxies vs other things
        internal bool ResolveAppearance ( object ownerContext,
			ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps,
			EmbeddableEditorArea area,
			bool hotTracking,
			string customArea,
            bool isProxyResolution)
        {
			// SSP 4/23/03 - Optimizations
			// Moved this inside the if block.
			//
			//UltraGridRow row = GetRowFromOwnerContext( ownerContext );

			if ( EmbeddableEditorArea.Default == area || EmbeddableEditorArea.Text == area )
			{
				UltraGridRow row = GetRowFromOwnerContext( ownerContext );

				bool resolvingCursor = 0 != ( AppearancePropFlags.Cursor & requestedProps );

				// SSP 12/23/02
				// Optimizations.
				// Use the new ResolvedCachedCellAppearance method.
				//
				//row.ResolveCellAppearance( this.Column, ref appearance, ref requestedProps, false, false );
				//				if ( AppearancePropFlags.AllRender == requestedProps )
				//				{
				//					if ( this.lastRow != null && this.lastColumn != null )
				//					{
				//						appearance = this.appData;
				////						appearance.BackColor = Color.White;
				////						appearance.ForeColor = Color.Black;
				////						appearance.ImageHAlign = HAlign.Left;
				////						requestedProps ^= ( AppearancePropFlags.BackColor | AppearancePropFlags.ForeColor | AppearancePropFlags.ImageHAlign );
				//						requestedProps = this.lastFlags;
				//					} 
				//					else 
				//					{
				//						//this.Column.Layout.ResolvedCachedCellAppearance( row, this.Column, ref appearance, ref requestedProps );
				//						row.ResolveCellAppearance( this.column, ref appearance, ref requestedProps, false, false );
				//						this.appData = appearance;
				//						this.lastFlags = requestedProps;
				//						this.lastColumn = column;
				//						this.lastRow = row;
				//					}
				//				}
				//				else
				//				{
				// SSP 11/12/04 - Merged Cell Feature
				//

				// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
				// 
				//if ( ownerContext is MergedCellUIElement )
				// SSP 9/8/05 BR06252
				// 
				//CellUIElementBase cellElem = ownerContext as CellUIElementBase;
				CellUIElementBase cellElem = GetWrappedCellElement( ownerContext );

				// MD 8/7/07 - 7.3 Performance
				// FxCop - Do not cast unnecessarily
				//if ( null == cellElem && ownerContext is MergedCellUIElement )
				//    ((MergedCellUIElement)ownerContext).ResolveAppearance( ref appearance, ref requestedProps );
				MergedCellUIElement mergedCellElement = ownerContext as MergedCellUIElement;

				if ( null == cellElem && mergedCellElement != null )
					mergedCellElement.ResolveAppearance( ref appearance, ref requestedProps );
				else
				{
                    // MBS 5/8/08 - BR32611
                    // We decided not to cache the proxy appearences, so we don't need to go through the whole
                    // process of the ResolvedCachedCellApppearance, nor do we want to resolve based on
                    // whether the the row or cell itself is being hot-tracked, so just have the 
                    // row resolve the appearance here.
                    if (isProxyResolution)
                    {
                        row.ResolveCellAppearance(this.column, ref appearance, ref requestedProps, 
                            true, 
                            false, 
                            ForceActive.None, 
                            false, 
                            false, 
                            false, 
                            true);
                    }
                    else
                    {
                        // SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
                        // Added hotTrackingCell and hotTrackingRow parameters.
                        // 
                        bool hotTrackingRow = null != cellElem && GridUtils.IsRowHotTracking(cellElem);
                        bool hotTrackingCell = hotTrackingRow && GridUtils.IsHotTracking(cellElem, cellElem.isMouseOverCell);

                        // SSP 9/8/05 BR06252
                        // Don't apply the hot-tracking appearances to the tooltip.
                        // 
                        if (hotTrackingRow && ownerContext is ToolTipOwnerContext)
                            hotTrackingCell = hotTrackingRow = false;

                        this.Layout.ResolvedCachedCellAppearance(row, this.column, ref appearance, ref requestedProps
                            // SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
                            // Added hotTrackingCell and hotTrackingRow parameters.
                            //
                            , hotTrackingCell, hotTrackingRow);
                    }
				}
				//				}

				// SSP 8/2/02 UWG1476
				// If the grid is a drop down or a combo, then on the drop down don't show
				// the cursor of IBeam. Show the default cursor.
				//
				if ( resolvingCursor && !( null != this.Column.Layout && this.Column.Layout.Grid is UltraGrid  ) 
					&& 0 != ( AppearancePropFlags.Cursor & requestedProps ) )
				{
					appearance.Cursor = Cursors.Default;
					requestedProps ^= AppearancePropFlags.Cursor;
				}

				return true;
			}
			else if ( EmbeddableEditorArea.MaskLiterals == area )
			{
				// SSP 11/7/02
				// Use the ResolveMaskLiteralsAppearance method to resolve mask literals appearance
				// which also takes into account the override's mask literal appearance settings.
				//
				
				this.Column.ResolveMaskLiteralsAppearance( ref appearance, ref requestedProps );

				return true;
			}
			else if ( EmbeddableEditorArea.Button == area )
			{
				// SSP 12/1/03 UWG2085
				// Added button appearance property to row object.
				//
				// ----------------------------------------------------------------------------
				//this.Column.ResolveCellButtonAppearance( ref appearance, ref requestedProps );
				UltraGridRow row = GetRowFromOwnerContext( ownerContext );
				row.ResolveCellButtonAppearance( this.column, ref appearance, ref requestedProps );
				// ----------------------------------------------------------------------------

				return true;
			}

			return base.ResolveAppearance( 
				ownerContext, 
				ref appearance, 
				ref requestedProps, 
				area, 
				hotTracking, 
				customArea );
		}

		#endregion // ResolveAppearance	

		#region DrawAsActive

		// SSP 7/26/02 UWG1449
		// Overrode DrawAsActive method.
		//
		/// <summary>
		/// Returns whether the element should be drawn as if it were in its "active" state. Only applicable if the return from the DisplayStyle property is not Standard.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>Whether the element should be drawn as if it is in its "active" state.</returns>
		/// <remarks>
		/// The default implementation returns <b>DefaultableBoolean.Default</b>, for which the element should follow the cursor position.
		/// </remarks>
		public override DefaultableBoolean DrawAsActive( object ownerContext )
		{
			// SSP 9/8/05 BR06252
			// 
			//UIElement elem = ownerContext as UIElement;
			UIElement elem = GetWrappedElement( ownerContext );

			// SSP 5/3/06 - App Styling
			// The elem will be cell element. We need to get the embeddable element of the cell.
			// 
			elem = null != elem ? GridUtils.GetChild( elem, typeof( EmbeddableUIElementBase ) ) : null;

			EmbeddableUIElementBase elemBeingEdited = this.Layout.EmbeddableElementBeingEdited;

			if ( null != elem && elem == elemBeingEdited )
				return DefaultableBoolean.True;
	
			if ( null != elemBeingEdited )
				return DefaultableBoolean.False;

			return DefaultableBoolean.Default;
		}

		#endregion // DrawAsActive

		#region DropDownListOffset

		//	BF 3.26.02
		/// <summary>
		/// Returns the horizontal and vertical offset to apply to the DropDown's list
		/// </summary>
		/// <remarks>
		/// Since it is implied that a ValueList's DropDown is displayed at the bottom left<br></br>
		/// corner of the element that displays it, this property provides a way to compensate for<br></br>
		/// control borders, etc.
		/// </remarks>
		public override Size GetDropDownListOffset( object ownerContext )
		{
			return base.GetDropDownListOffset( ownerContext );
		}

		#endregion DropDownListOffset

		#region WrapText

		/// <summary>
		/// Returns whether the text should be wrapped if it doesn't fit.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>A boolean indicating whether the text should be wrapped</returns>
		public override bool WrapText( object ownerContext )
		{
			return this.IsMultiLine( ownerContext );
		}

		#endregion // WrapText

		#region IsPrinting

		// SSP 11/4/05 BR07555
		// Added TextRenderingMode property to UltraGrid and also the EmbeddableEditorOwnerBase.
		// Also added IsPirnting on the EmbeddableEditorOwnerBase.
		// 
		/// <summary>
		/// Returns true if the editor is being used in printing. The default is false.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override bool IsPrinting( object ownerContext )
		{
			if ( -1 == this.cached_IsPrinting )
				this.cached_IsPrinting = this.Layout.IsPrintLayout ? 1 : 0;

			return 1 == this.cached_IsPrinting;
		}

		#endregion // IsPrinting

		#region GetTextRenderingMode

		// SSP 11/4/05 BR07555
		// Added TextRenderingMode property to UltraGrid and also the EmbeddableEditorOwnerBase.
		// Also added IsPirnting on the EmbeddableEditorOwnerBase.
		// 
		/// <summary>
		/// Returns the text rendering mode to use. Default implementation returns <b>Default</b> value.
		/// </summary>
		/// <returns></returns>
		public override TextRenderingMode GetTextRenderingMode( object ownerContext )
		{
            // MRS 7/17/2008 - BR34498
            // Excel and PDF always use GDI 
            if (this.Layout.IsExportLayout)
                return TextRenderingMode.GDI;

			return this.Grid.TextRenderingMode;
		}

		#endregion // GetTextRenderingMode

		// AS 1/27/06 BR09428
		// While the ultradropdown and ultracombo can display editor elements in the 
		// dropdown area they should not be interactive.
		//
		#region IsActionableAreaSupported
		/// <summary>
		/// Invoked by an editor to determine if an area of the element that performs an action is supported.
		/// </summary>
		/// <param name="ownerContext">Context used to identify the object to reference</param>
		/// <param name="area">Enum indicating the type of actionable area</param>
		/// <returns>Returns true by default.</returns>
		public override bool IsActionableAreaSupported(object ownerContext, ActionableArea area)
		{
			if (this.Grid is UltraDropDownBase)
				return false;

			// SSP 10/4/07 BR25629
			// If the cell element is underneath a merged cell and thus completely hidden out of view,
			// it should not support links. Otherwise clicking on a merged cell on an area that 
			// otherwise seems blank however happens to have a cell underneath with a link where 
			// the mouse is will end up activating that link.
			// 
			// --------------------------------------------------------------------------------------
			CellUIElement cellElem = ownerContext as CellUIElement;
			MergedCellUIElement mergedCell = null != cellElem ? cellElem.MergedCellElement : null;
			if ( null != mergedCell && ! cellElem.IsInEditMode )
				return false;
			// --------------------------------------------------------------------------------------

			return base.IsActionableAreaSupported(ownerContext, area);
		}
		#endregion //IsActionableAreaSupported

		#region GetComponentRole

		// SSP 3/6/06 - App Styling
		// Overrode GetComponentRole.
		// 
		/// <summary>
		/// Invoked by an editor to obtain the application style information.
		/// </summary>
		/// <param name="ownerContext">Context used to identify the object to reference</param>
		/// <returns>An <see cref="Infragistics.Win.AppStyling.ComponentRole"/> instance that should be used by the editor.</returns>
		public override AppStyling.ComponentRole GetComponentRole( object ownerContext )
		{
			return this.Grid.ComponentRole;
		}

		#endregion // GetComponentRole

		//	BF 3/21/06	NAS2006 Vol2 - ValueListDropDown
		#region GetScrollBarLook
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.UltraWinScrollBar.ScrollBarLook"/> to be used by
		/// the scrollbars displayed by the embeddable editor.
		/// </summary>
		/// <param name="ownerContext">Context used to identify the object to reference</param>
		/// <returns>A <see cref="Infragistics.Win.UltraWinScrollBar.ScrollBarLook"/> instance.</returns>
		public override Infragistics.Win.UltraWinScrollBar.ScrollBarLook GetScrollBarLook( object ownerContext )
		{
			UltraGridBase grid = this.Grid;
			return grid != null ? grid.DisplayLayout.ScrollBarLook : null;
		}
		#endregion //GetScrollBarLook

		#region SuppressEnterLeaveInvalidation

		// SSP 9/15/06 - NAS 6.3
		// Added SuppressEnterLeaveInvalidation on the owner. This is to implement the 
		// CellHottrackInvalidationStyle property of the grid.
		// 
		/// <summary>
		/// Overridden.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override bool SuppressEnterLeaveInvalidation( object ownerContext )
		{
			CellHottrackInvalidationStyle style = this.Layout.CellHottrackInvalidationStyleResolved;
			if ( CellHottrackInvalidationStyle.Never == style )
				return true;

			if ( CellHottrackInvalidationStyle.OnlyIfHottrackAppearance == style )
			{
				EmbeddableElementDisplayStyle elemStyle = this.GetDisplayStyle( ownerContext );
				if ( EmbeddableElementDisplayStyle.Default == elemStyle
					|| EmbeddableElementDisplayStyle.Standard == elemStyle )
					return true;
			}

			return false;
		}

		#endregion // SuppressEnterLeaveInvalidation

        // MRS 11/22/06 - BR17614
        // I added this method so that controls like the grid can override default appearance for things
        // like BackColorDisabled. The grid needs to show Disabled cells with a Window backcolor instead of
        // Control color like the editor does. 
        #region ResolveDefaultAppearance

        /// <summary>
        /// Resolves the default appearance for an element.
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <param name="appearance">The appearance structure to initialize.</param>
        /// <param name="requestedProps">The appearance properties to resolve.</param>
        /// <param name="area">Enumeration of type <see cref="EmbeddableEditorArea"/> describing the area of the embeddable element to which the appearance will be applied</param>
        /// <param name="hotTracking">Boolean indicating whether the owner should apply its 'HotTrackingAppearance'</param>
        /// <param name="customArea">A string that denotes which appearance to resolve. Applicable only when the 'area' parameter is set to Custom.</param>
        /// <returns>True if the owner recognizes and supports the named appearance.</returns>
        /// <remarks>The default implementation returns <b>false</b></remarks>
        public override bool ResolveDefaultAppearance(object ownerContext,
            ref AppearanceData appearance,
            ref AppearancePropFlags requestedProps,
            EmbeddableEditorArea area,
            bool hotTracking,
            string customArea)
        {

            if (area == EmbeddableEditorArea.Default || area == EmbeddableEditorArea.Text)
            {
                #region BackColorDisabled
                if ((requestedProps & AppearancePropFlags.BackColorDisabled) != 0 &&
                    !appearance.HasPropertyBeenSet(AppearancePropFlags.BackColorDisabled))
                {
                    // MRS 2/27/07 - BR20599
                    // Setting the BackColorDisabled here causes cells in the grid not to show
                    // the ActiveRow, HotTracking, or selected appearances. 
                    // So don't set the BackColorDisabled, just clear the 
                    // flag so that the normal cell BackColor is used. 
                    //
                    //appearance.BackColorDisabled = SystemColors.Window;
                    
                    requestedProps &= ~AppearancePropFlags.BackColorDisabled;
                }
                #endregion BackColorDisabled

                return true;
            }

            return base.ResolveDefaultAppearance(
                ownerContext,
                ref appearance,
                ref requestedProps,
                area,
                hotTracking,
                customArea);
        }

        #endregion ResolveDefaultAppearance

        // MBS 7/9/08 - BR33993
        // Need this to notify us that we need to release any resources being used
        #region ReleaseResources

        internal virtual void ReleaseResources()
        {
        }
        #endregion //ReleaseResources

		#endregion // Virtual Methods
	}

	#endregion // GridEmbeddableEditorOwnerInfoBase Class

	#region GridCellEmbeddableEditorOwnerInfoBase Class

	internal abstract class GridCellEmbeddableEditorOwnerInfoBase : Infragistics.Win.UltraWinGrid.GridEmbeddableEditorOwnerInfoBase
	{
		#region Veriables

		#endregion // Veriables

		#region Constructor

		internal GridCellEmbeddableEditorOwnerInfoBase( UltraGridColumn column ) : base( column )
		{
		}

		#endregion    //Constructor
			
		#region Virtual Methods

		#region GetShowInkButton

		// SSP 12/12/03 DNF135
		// Added a way to control whether ink buttons get shown.
		//
		/// <summary>
		/// Determines when to show the Ink Button on the editor.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>A ShowInkButton value indicating when the InkButton will be shown.</returns>
		/// <remarks><p class="body">The default implementation returns <b>ShowInkButton.Default</b>.</p></remarks>
		public override ShowInkButton GetShowInkButton( object ownerContext )
		{
			ShowInkButton showInkButton = this.Column.ShowInkButtonResolved;
			if ( ShowInkButton.Default != showInkButton )
				return showInkButton;
	
			return base.GetShowInkButton( ownerContext );
		}

		#endregion // GetShowInkButton

		// SSP 12/16/02
		// Optimizations. Added GetEditorContext and SetEditorContext methods for
		// caching things like index in the value list items collection of the last
		// matching item.
		//
		#region GetEditorContext

		/// <summary>
		/// Gets the editor context that was set with SetEditorContext method.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>Editor context that was last set with SetEditorContext for the passed in ownerContext.</returns>
		/// <remarks>
		/// <p>GetEditorContext and <seealso cref="SetEditorContext"/> can be used to cache objects per owner context.</p>
		/// <p>Implementing owner will return the object that was last cached using SetEditorContext method.</p>
		/// </remarks>
		public override object GetEditorContext( object ownerContext )
		{
			UltraGridRow row = GetRowFromOwnerContext( ownerContext );

			// SSP 5/1/03
			// Now we are using the editor to convert the group-by-row value to the display
			// text. So there is no need for storing the last value list index. However do
			// store the editor context because that's what the editor uses to cache the
			// index of last matched item in the value list.
			//
			if ( null != row )
			{
				UltraGridGroupByRow groupByRow = row as UltraGridGroupByRow;
				if ( null != groupByRow )
				{
					return groupByRow.EditorContext;
				}
				else
				{
					UltraGridCell cell = row.GetCellIfAllocated( this.column );
					if ( null != cell )
						return cell.EmbeddableEditorContext;
				}
			}

			return null;
		}

		#endregion // GetEditorContext

		#region SetEditorContext

		/// <summary>
		/// Sets the editor context for the passed in ownerContext.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="editorContext"></param>
		/// <remarks>
		/// <p class="body"><seealso cref="GetEditorContext"/> and SetEditorContext can be used to cache objects per owner context.</p>
		/// <p class="body">Implementing owner will return the object that was last cached using SetEditorContext method.</p>
		/// </remarks>
		public override void SetEditorContext( object ownerContext, object editorContext )
		{
			UltraGridRow row = GetRowFromOwnerContext( ownerContext );

			// SSP 5/1/03
			// Now we are using the editor to convert the group-by-row value to the display
			// text. So there is no need for storing the last value list index. However do
			// store the editor context because that's what the editor uses to cache the
			// index of last matched item in the value list.
			//
			if ( null != row )
			{
				UltraGridGroupByRow groupByRow = row as UltraGridGroupByRow;
				if ( null != groupByRow )
				{
					groupByRow.EditorContext = editorContext;
				}
				else
				{
					UltraGridCell cell = row.Cells[ this.column ];
					cell.EmbeddableEditorContext = editorContext;
				}
			}
		}

		#endregion // SetEditorContext

		#region GetNullText	

		/// <summary>
		/// The string to display for a null value.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="nullText">(out) The string that should be used if the value is null or DB_Null.</param>
		/// <returns>A boolean indicating whether a meaningful value was returned.</returns>
		/// <remarks>The default implementation returns an empty string.</remarks>
		public override bool GetNullText( object ownerContext, out string nullText )
		{
			nullText = this.Column.NullTextResolved;
			return true;
		}

		#endregion    //GetNullText	

		// AS 11/19/03 Accessibility
		#region GetEditorElement

		/// <summary>
		/// Returns the embeddable uielement associated with a particular object or null if none is available.
		/// </summary>
		/// <param name="ownerContext">Context used to identify the object to reference</param>
		/// <returns>The embeddable uielement representing the specified owner context.</returns>
		public override EmbeddableUIElementBase GetEditorElement( object ownerContext )
		{
			// first see if the context is an element
			// SSP 9/8/05 BR06252
			// 
			//CellUIElementBase cellElement = ownerContext as CellUIElementBase;
			CellUIElementBase cellElement = GetWrappedCellElement( ownerContext );

			if ( null == cellElement )
			{
				// if its not, get the row
				UltraGridRow row = EmbeddableEditorOwnerInfo.GetRowFromOwnerContext(ownerContext);

				if (row != null)
				{
					UIElement rcrElem = GetRCRIntersectionElem( ownerContext );
					cellElement = null != rcrElem ?
						(CellUIElementBase)rcrElem.GetDescendant( typeof( CellUIElementBase ), new object[] { row, this.Column } )
						: null;
				}
			}

			return null != cellElement ? cellElement.FindEmbeddableUIElement(false) : null;
		}

		#endregion //GetEditorElement

		#region GetFormatInfo

		/// <summary>
		/// Returns information needed to format a string.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="format">Returns the format string or null.</param>
		/// <param name="provider">Returns the IFormatProvider or null.</param>
		public override void GetFormatInfo( object ownerContext, out string format, out IFormatProvider provider )
		{
			//	BF 8.29.02	UWG1616
			//	If both the Format and FormatInfo properties are non-null,
			//	we will return them without looking at the default owner.
			if ( null != this.column.Format && null != this.column.FormatInfo )
			{
				format = this.column.Format;
				provider = this.column.FormatInfo;
				return;
			}

			//	Now we will look at each relevant property individually; if either is set
			//	on the column, we will use it. If it isn't, and the default owner provides
			//	a value , we will use it
			//
			string defaultOwnerFormat = null;
			IFormatProvider defaultOwnerProvider = null;
			base.GetFormatInfo( ownerContext, out defaultOwnerFormat, out defaultOwnerProvider );

			if ( this.column.Format != null )
				format = this.column.Format;
			else
				format = defaultOwnerFormat;

			if ( this.column.FormatInfo != null )
				provider = this.column.FormatInfo;
			else
				provider = defaultOwnerProvider;

	#region Obsolete code (BF 8.29.02, bug UWG1616)
						
	#endregion //	Obsolete code (BF 8.29.02, bug UWG1616)
		}

		#endregion // GetFormatInfo

		#region GetMaskInfo	

		/// <summary>
		/// Returns masking information. This is only of ibterest to a editor that supports masking.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="mask">The string used for masking the value or null if there is no mask.</param>
		/// <param name="dataMode">Determines whether literals and prompt characters are included in Value property of the Editor.</param>
		/// <param name="displayMode">Determines whether literals and prompt characters are displayed when not in edit mode.</param>
		/// <param name="clipMode">Determines whether literals and prompt characters are included when the editor copyies text to the clipboard.</param>
		/// <returns>True only if masking info is available.</returns>
		/// <remarks>The default implementation returns false.</remarks>
		public override bool GetMaskInfo( object ownerContext,
			out string mask,
			out UltraWinMaskedEdit.MaskMode dataMode,
			out UltraWinMaskedEdit.MaskMode displayMode,
			out UltraWinMaskedEdit.MaskMode clipMode )
		{
			// SSP 12/11/03 UWG2781
			// Since the MaskMode enum doesn't have Default as its member, we need to let
			// the user be able to specify that we pickup properties from the owner rather
			// than use the column's settings. Added UseEditorMaskSettings property.
			//
			// ----------------------------------------------------------------------------
			if ( this.column.UseEditorMaskSettings )
			{
				base.GetMaskInfo( ownerContext, out mask, out dataMode, out displayMode, out clipMode );

				if ( null == mask )
					mask = this.column.MaskInput;

				return true;
			}
			// ----------------------------------------------------------------------------

			mask		= this.Column.MaskInput;

			// SSP 7/29/02 UWG1447
			// If column doesn't have any mask set, then get the mask from the default owner
			// by calling the base class' implementation. This will cause the embeddable editor to
			// pickup the mask setting on the associated standalone control.
			//
			if ( null == mask )
				base.GetMaskInfo( ownerContext, out mask, out dataMode, out displayMode, out clipMode );

			dataMode	= this.Column.MaskDataMode;
			displayMode = this.Column.MaskDisplayMode;
			clipMode	= this.Column.MaskClipMode;

			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			// If the editor being used by the cell is date time editor and the display mode hasn't
			// been set, then resolved it to IncludeLiterals.
			//
			// --------------------------------------------------------------------------------------
			if ( UltraWinMaskedEdit.MaskMode.Raw == displayMode )
			{
				EmbeddableEditorBase editor = this.GetEditor( ownerContext );

				if ( editor is DateTimeEditor )
					displayMode = UltraWinMaskedEdit.MaskMode.IncludeLiterals;
			}
			// --------------------------------------------------------------------------------------

			// SSP 7/9/02
			// For date times, we want to by default show the literals.
			//
			if ( UltraWinMaskedEdit.MaskMode.Raw == dataMode && 
				typeof( DateTime ) == this.Column.DataType )
				displayMode = UltraWinMaskedEdit.MaskMode.IncludeLiterals;

			return true;
		}

		#endregion    //GetMaskInfo

		#region GetPadChar

		/// <summary>
		/// Returns character used as a substitute for spaces.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="padChar">Pad character.</param>
		/// <returns>True only if pad character info is available.</returns>
		public override bool GetPadChar( object ownerContext, out char padChar )
		{
			// SSP 3/21/06 BR10991
			// If UseEditorMaskSettings is set to true then use the mask editor setting.
			// 
			if ( this.column.UseEditorMaskSettings && base.GetPadChar( ownerContext, out padChar ) )
				return true;

			padChar = this.Column.PadChar;
			return true;
		}
		#endregion    //GetPadChar

		#region GetPromptChar

		/// <summary>
		/// Returns the character used as prompt during editing (e.g. '_').
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="promptChar">Prompt character.</param>
		/// <returns>True only if prompt character info is available.</returns>
		public override bool GetPromptChar( object ownerContext, out char promptChar )
		{
			// SSP 3/21/06 BR10991
			// If UseEditorMaskSettings is set to true then use the mask editor setting.
			// 
			if ( this.column.UseEditorMaskSettings && base.GetPromptChar( ownerContext, out promptChar ) )
				return true;

			promptChar = this.Column.PromptChar;
			return true;
		}
		#endregion    //GetPromptChar

		#region GetAutoEdit

		/// <summary>
		/// Indicates whether AutoEdit should enabled.
		/// </summary>
		/// <param name="ownerCotext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>
		/// Indicates whether AutoEdit should enabled. Default implementation returns false.
		/// </returns>
		public override bool GetAutoEdit( object ownerCotext )
		{
            //  BF 3/7/08   FR09238 - AutoCompleteMode
			//return this.Column.AutoEdit;
            return this.Column.AutoCompleteModeResolved == AutoCompleteMode.Append ? true : false;
		}

        //  BF 3/7/08   FR09238 - AutoCompleteMode
        /// <summary>
        /// Returns the <see cref="Infragistics.Win.AutoCompleteMode"/> constant which
        /// determines the automatic completion mode for the <see cref="Infragistics.Win.EditorWithCombo"/> editor.
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>
        /// <see cref="UltraGridColumn.AutoCompleteMode"/>
        /// </returns>
        /// <remarks>
        /// <p class="note"><b>Note: </b>This method only has significance to the <see cref="Infragistics.Win.EditorWithCombo"/> and derived editors.</p>
        /// </remarks>
        public override AutoCompleteMode GetAutoCompleteMode( object ownerContext )
        {
            return this.Column.AutoCompleteModeResolved;
        }

		#endregion // GetAutoEdit

		#region GetAutoSizeEditInfo

		/// <summary>
		/// Returns the AutoSizeEdit information to be used by editors that support AutoSizing
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="autoSizeEdit">(out) Indicates whether the owner supports AutoSizeEditing.</param>
		/// <param name="startSize">(out) A Size struct containing the starting width and height to which the editor should be set.</param>
		/// <param name="maxSize">(out) A Size struct containing the maximum width and height to which the editor should be allowed to grow.</param>
		/// <returns>A boolean indicating whether AutoSizeEdit functionality is supported by the owner.</returns>
		/// <remarks>The default implementation returns <b>false</b>.</remarks>
		public override bool GetAutoSizeEditInfo( object ownerContext,
			out bool autoSizeEdit, 
			out Size startSize,
			out Size maxSize )
		{
			if ( DefaultableBoolean.True != this.Column.AutoSizeEdit || 
				null == this.Column.Layout || 
				null == this.Column.Layout.LastAutoSizeEditEventArgs ||
				this.Column.Layout.LastAutoSizeEditEventArgs.Cancel )
			{
				autoSizeEdit = false;
				startSize = Size.Empty;
				maxSize = Size.Empty;
			}
			else
			{
				autoSizeEdit = true;
				startSize = new Size( this.Column.Layout.LastAutoSizeEditEventArgs.StartWidth,
					this.Column.Layout.LastAutoSizeEditEventArgs.StartHeight );
				maxSize = new Size(  this.Column.Layout.LastAutoSizeEditEventArgs.MaxWidth,
					this.Column.Layout.LastAutoSizeEditEventArgs.MaxHeight );
			}

			return true;
		}
		#endregion //	GetAutoSizeEditInfo

		#region IsMultiLine	

		/// <summary>
		/// Returns whether the text is multiline.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>A boolean indicating whether the text is multiline</returns>
		/// <remarks>The default implementation returns false.</remarks>
		public override bool IsMultiLine( object ownerContext )
		{
			return this.column.IsCellMultiLine;
		}

		#endregion    //IsMultiLine

		// JAS v5.2 DoubleClick Events 4/27/05
		//
		#region OnEditorDoubleClick

		/// <summary>
		/// Overrides OnEditorDoubleClick.
		/// </summary>
		/// <param name="embeddableElem"></param>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		public override void OnEditorDoubleClick(Infragistics.Win.EmbeddableUIElementBase embeddableElem, Infragistics.Win.EmbeddableClickEventArgs e)
		{
			// SSP 9/22/06 BR15924
			// Raise double click for check editor element. Technically it's a button however doesn't appear as a button.
			// 
			//if( e != null && ! e.IsButton )
			if ( e != null && ( ! e.IsButton || embeddableElem is EmbeddableCheckUIElement ) )
			{
				UltraGrid grid = this.Grid as UltraGrid;
				if( grid != null )
				{				
					if( embeddableElem.OwnerContext is CellUIElementBase )
					{
						CellUIElementBase cellElemBase = embeddableElem.OwnerContext as CellUIElementBase;

						if( cellElemBase.Cell != null )
							grid.FireEvent( GridEventIds.DoubleClickCell, new DoubleClickCellEventArgs( cellElemBase.Cell ) );

                        if (cellElemBase.Row != null)
                        {
                            grid.FireEvent(GridEventIds.DoubleClickRow, new DoubleClickRowEventArgs(cellElemBase.Row, RowArea.Cell));

                            // MBS 5/6/08 - RowEditTemplate NA2008 V2
                            RowEditTemplateUIType templateType = cellElemBase.Row.Band.RowEditTemplateUITypeResolved;
                            if ((templateType & RowEditTemplateUIType.OnDoubleClickRow) != 0)
                                cellElemBase.Row.ShowEditTemplate(true, TemplateDisplaySource.OnRowDoubleClick);
                        }
					}
				}
			}

			base.OnEditorDoubleClick( embeddableElem, e );
		}

		#endregion // OnEditorDoubleClick

		
		
		
		
		
		
		
		

		// AS 1/30/06
		// We previously added the concept of a button style for the grid but
		// the editors are not making use of it as they should. This came up
		// when we were creating a custom preset and the editor buttons were
		// not honoring the grid's buttonstyle property.
		//
		#region GetButtonStyle

		/// <summary>
		/// Returns the ButtonStyle to be used by the embeddable element's button
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="buttonStyle">The ButtonStyle to be used by the embeddable element's buttons</param>
		/// <returns></returns>
		/// <remarks>The default implementation returns <b>false</b>, with buttonStyle set to UIElementButtonStyle.Default.</remarks>
		public override bool GetButtonStyle( object ownerContext, out UIElementButtonStyle buttonStyle )
		{
			// just get the explicit setting first
			// SSP 3/27/06 - App Styling
			// Pass in the role related information in order to get the button style setting off the
			// currect role.
			// 
			//UIElementButtonStyle cellButtonStyle = this.Band.GetButtonStyleResolved(false);
			UIElementButtonStyle cellButtonStyle = this.Band.GetButtonStyleResolved( 
				StyleUtils.GetControlAreaRole( this.Band.Layout ), StyleUtils.CachedProperty.ButtonStyleLayout, false);

			if ( cellButtonStyle != UIElementButtonStyle.Default )
			{
				buttonStyle = cellButtonStyle;
				return true;
			}

			return base.GetButtonStyle(ownerContext, out buttonStyle);
		}

		#endregion GetButtonStyle

        // MBS 7/1/08 - NA2008 V2 ClickCellEvent
        #region OnEditorClick

        /// <summary>
        /// The editor calls this method whenever any of its embeddable elements gets a click.
        /// </summary>
        /// <param name="embeddableElem">The <see cref="EmbeddableUIElementBase"/> that was clicked.</param>
        /// <param name="e">The <see cref="EmbeddableClickEventArgs"/>.</param>
		/// <returns>True if the click event was handled; False if the default click processing should be done.</returns>
		// MD 9/19/08 - TFS7822
        //public override void OnEditorClick(EmbeddableUIElementBase embeddableElem, EmbeddableClickEventArgs e)
		public override bool OnEditorClick( EmbeddableUIElementBase embeddableElem, EmbeddableClickEventArgs e )
        {
            if (e != null && (!e.IsButton || embeddableElem is EmbeddableCheckUIElement))
            {
                UltraGrid grid = this.Grid as UltraGrid;
                if (grid != null)
                {
                    if (embeddableElem.OwnerContext is CellUIElementBase)
                    {
                        CellUIElementBase cellElemBase = embeddableElem.OwnerContext as CellUIElementBase;

                        if (cellElemBase.Cell != null)
						{
                            // MBS 3/22/09 - TFS14728
                            // Check to see if we've fire the event already
                            if (!cellElemBase.hasFiredCellClick)
                            {
                                cellElemBase.hasFiredCellClick = true;
                                grid.FireEvent(GridEventIds.ClickCell, new ClickCellEventArgs(cellElemBase.Cell));
                            }

							// MD 9/19/08 - TFS7822
							// Return true because the click has been handled.
							return true;
						}
                    }
                }
            }

			// MD 9/19/08 - TFS7822
			// This method needs to return a value now.
            //base.OnEditorClick(embeddableElem, e);
			return base.OnEditorClick( embeddableElem, e );
        }
        #endregion //OnEditorClick

        #endregion // Virtual Methods
    }

	#endregion // GridEmbeddableEditorOwnerInfoBase Class

	#region EmbeddableEditorOwnerInfo Class

	/// <summary>
	/// Embeddable editor owner info class for grid cells.
	/// </summary>
	internal class EmbeddableEditorOwnerInfo : GridCellEmbeddableEditorOwnerInfoBase
	{
		#region Private Members

		#endregion    //Private Members

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="column"></param>
		internal EmbeddableEditorOwnerInfo( UltraGridColumn column ) : base( column )
		{
		}

		#endregion    //Constructor

		#region Implemented Abstract Methods Overridden

		#region GetEditor

		/// <summary>
		/// Returns the editor for the passed in ownerContext. This is used by the base implementation of EmbeddableOwnerBase.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override EmbeddableEditorBase GetEditor( object ownerContext )
		{
			// SSP 5/1/03 - Cell Level Editor / Optimizations
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			// Return the editor directly from the cell element rather than going through the normal
			// editor resolution mechanism which involves checking if the cell has been created or not
			// since now we support cell level value lists and editors. EmbeddableEditorOwnerInfo.GetEditor
			// method gets called quite a lot of times.
			//
			//return this.Column.Editor;
			// SSP 9/8/05 BR06252
			// 
			//CellUIElementBase cellElem = ownerContext as CellUIElementBase;
			CellUIElementBase cellElem = GetWrappedCellElement( ownerContext );

			if ( null != cellElem && null != cellElem.embeddableEditorLastUsed )
				return cellElem.embeddableEditorLastUsed;

			return this.column.GetEditor( EmbeddableEditorOwnerInfo.GetRowFromOwnerContext( ownerContext ) );
		}

		#endregion // GetEditor

		#region GetDataType	

		/// <summary>
		/// Returns the data type.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override System.Type GetDataType( object ownerContext )
		{
			// SSP 9/16/03 UWG2512
			//
			// ------------------------------------------------------
			//return this.Column.DataType;
			Type type = this.column.DataType;
			if ( null == type || typeof( object ) == type )
			{
				
				
				
				
				// Call the base implementation and if it doesn't return string type then use that.
				// The base implementation as a last resort returns string type however in that case
				// we do not want to make use of that - instead in that case we want to make use of
				// editor specific data type.
				// 
				Type baseType = base.GetDataType( ownerContext );
				if ( null != baseType && typeof( string ) != baseType )
				{
					type = baseType;
				}
				else
				{
					// If the editor has default owner set with a specific data type then use that
					// data type.
					// 
					EmbeddableEditorBase editor = this.GetEditor( ownerContext );
					UltraWinEditors.DefaultEditorOwner defOwner = null != editor ? editor.DefaultOwner as UltraWinEditors.DefaultEditorOwner : null;
					if ( null != defOwner && null != defOwner.Settings )
					{
						Type tmp = defOwner.Settings.DataType;
						if ( null != tmp )
							type = tmp;
					}

					// If data type isn't explicitly set however the column style is set, then
					// use data type associated with the column style (like PositiveInteger column
					// style would use Int32 as the data type.
					// 
					if ( null == type || typeof( object ) == type )
					{
						UltraGridRow row = GetRowFromOwnerContext( ownerContext );
						if ( null != row && row.IsDataRow )
						{
							Type tmp = GridUtils.GetDefaultDataType( this.column.GetStyleResolved( row ) );
							if ( null != tmp )
								type = tmp;
						}
					}

					if ( null == type )
						type = baseType;
				}
				
			}
			return type;
			// ------------------------------------------------------
		}

		#endregion    //GetDataType	

        // CDS 04/14/08 NA 8.2 ImeMode
        #region GetImeMode

        /// <summary>
        /// Returns the ImeMode for owning UltraGridColumn. This is used by the base implementation of EmbeddableOwnerBase.
        /// </summary>
        /// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns></returns>
        public override ImeMode GetImeMode(object ownerContext)
        {
            return this.column.ImeMode;
        }

        #endregion GetImeMode

		#region GetValue	

		/// <summary>
		/// Returns the value that should be rendered.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override object GetValue( object ownerContext )
		{
			UltraGridRow row = GetRowFromOwnerContext( ownerContext );

			Debug.Assert( row.Band == this.column.Band, "Row and column bands don't match !" );

			// SSP 7/14/04 - UltraCalc
			// If the column has a formula, then return the text instead of the underlying
			// value. GetCellText takes care of the formula errors etc.
			//
			// --------------------------------------------------------------------------------
			if ( this.column.HasActiveFormula )
				return this.column.GetCellText( row );
			// --------------------------------------------------------------------------------

			return row.GetCellValue( this.column );
		}

		#endregion    //GetValue	

		#region IsEnabled	

		/// <summary>
		/// Returns whether the value is enabled for editing.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>True if the value is enabled for editing.</returns>
		public override bool IsEnabled( object ownerContext )
		{
			if ( ! base.IsEnabled( ownerContext ) )
				return false;

			UltraGridRow row = EmbeddableEditorOwnerInfo.GetRowFromOwnerContext(ownerContext);
			return null != row && row.GetCellActivationResolved(this.Column) != Activation.Disabled;
		}

		#endregion    //IsEnabled	

		#region IsNullable	

		/// <summary>
		/// Returns whether the value can be set to null.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>True if the value can be set to null.</returns>
		public override bool IsNullable( object ownerContext )
		{
			return this.Column.IsNullable;
		}

		#endregion    //IsNullable	

		#endregion // Implemented Abstract Methods Overridden

		#region Virtual Methods

		#region IsReadOnly	

		/// <summary>
		/// Returns true is the value is read-only
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>A boolean indicating whether the value is read only</returns>
		/// <remarks>The default implementation returns false.</remarks>
		public override bool IsReadOnly( object ownerContext )
		{
			UltraGridRow row = GetRowFromOwnerContext( ownerContext );			 
	
			return null == row || !this.Column.CanBeModified( row );
			// SSP 7/29/02
			// Not necessary as CanBeModified method checks for this.
			//|| this.Column.Band.IsReadOnly;
		}

		#endregion    //IsReadOnly	

		#region GetCharacterCasing	

		/// <summary>
		/// Determines how the text will be cased. 
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>The CharacterCasing to be applied to the text</returns>
		public override CharacterCasing GetCharacterCasing( object ownerContext )
		{
			// MRS 4/7/05 - Replaced Case with CharacterCasing			
//			CharacterCasing casing;
//			switch ( this.Column.Case )
//			{
//				case Infragistics.Win.UltraWinGrid.Case.Lower:
//					casing = CharacterCasing.Lower;
//					break;
//				case Infragistics.Win.UltraWinGrid.Case.Upper:
//					casing = CharacterCasing.Upper;
//					break;
//				case Infragistics.Win.UltraWinGrid.Case.Unchanged:
//					casing = CharacterCasing.Normal;
//					break;
//				default:
//					casing = CharacterCasing.Normal;
//					break;
//			}
//			return casing;
			return this.Column.CharacterCasing;
		}

		#endregion    //GetCharacterCasing	

		#region GetMaxLength	

		/// <summary>
		/// Returns the maximum length for a string.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="maxLength">(out) The maximum value or 0 if there is no maximum.</param>
		/// <returns>A boolean indicating whether a meaningful value was returned.</returns>
		/// <remarks>The default implementation returns 0.</remarks>
		public override bool GetMaxLength( object ownerContext, out int maxLength )
		{
			// SSP 1/24/03 UWG1944
			// Added code in the FieldLen property to get the underlying DataColumn if there is one
			// and return the MaxLength off of it.
			//
			// ------------------------------------------------------------
			
			// JAS 2005 v2 XSD Support
			// FieldLen was deprecated and replaced by MaxLength.
			//
			//			int fieldLen = this.Column.FieldLen;
			int fieldLen = this.column.MaxLength;

			// If the value return by FieldLen property is greater than 0, then use that.
			//
			if ( fieldLen > 0 )
			{
				maxLength = fieldLen;
				return true;
			}
			// ------------------------------------------------------------

			return base.GetMaxLength( ownerContext, out maxLength );
		}

		#endregion    //GetMaxLength	

		#region GetMaxValue	

		/// <summary>
		/// Returns the maximum allowable value.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>The maximum value or null if there is no maximum.</returns>
		/// <remarks>The default implementation returns 0.</remarks>
		public override object GetMaxValue( object ownerContext )
		{
			// SSP 8/19/02
			// Moved this from below the so that we give higher precedence
			// to MaxValue as MaxDate is obsoleted.
			//
			if ( this.Column.ShouldSerializeMaxValue( ) )
				return this.Column.MaxValue;

			return base.GetMaxValue( ownerContext );
		}

		#endregion    //GetMaxValue	

		#region GetMinValue	

		/// <summary>
		/// Returns the minimum allowable value.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>The minimum value or null if there is no minimum.</returns>
		/// <remarks>The default implementation returns 0.</remarks>
		public override object GetMinValue( object ownerContext )
		{
			// SSP 8/19/02
			// Moved this from below the so that we give higher precedence
			// to MinValue as MinDate is obsoleted.
			//
			if ( this.Column.ShouldSerializeMinValue( ) )
				return this.Column.MinValue;

			return base.GetMinValue( ownerContext );
		}

		#endregion    //GetMinValue	

		#region GetValueList	

		/// <summary>
		/// Returns a list of predefined values.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>Returns a list of predefined values or null.</returns>
		/// <remarks>The default implementation returns null.</remarks>
		public override IValueList GetValueList( object ownerContext )
		{
			// SSP 5/1/03 - Optimizations
			//
			//if ( null != this.Column.ValueList )
			//	return this.Column.ValueList;
			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//IValueList valueList = this.Column.ValueList;
			IValueList valueList = this.column.GetValueList( EmbeddableEditorOwnerInfo.GetRowFromOwnerContext( ownerContext ) );

			if ( null != valueList )
				return valueList;

			return base.GetValueList( ownerContext );
		}

		#endregion    //GetValueList	

		#region MustSelectFromList	

		/// <summary>
		/// Returns whether a selection can only be made from the value list.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>If true will act as a combo with a style of DropDownList.</returns>
		/// <remarks>The default implementation returns false.</remarks>
		public override bool MustSelectFromList( object ownerContext )
		{
			// SSP 7/18/03 - Cell level Style property
			//
			//return ColumnStyle.DropDownList == this.Column.Style;
			return ColumnStyle.DropDownList == this.column.GetStyleResolved( GetRowFromOwnerContext( ownerContext ) );
		}

		#endregion    //MustSelectFromList	

		#region MustDisplayFromList	

		// SSP 8/2/02
		// Overrode newly added MustDisplayFromList method to return true. This is for
		// consistency with version 1.
		//
		/// <summary>
		/// Returns whether the text an element displays must correspond to the text of an item in a list
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>If true and the editor's Value does not match an item in the list, an empty string will be displayed.</returns>
		/// <remarks>The default implementation returns <b>true</b>, meaning that if an editor's Value does not correspond to an item in the list, and <see cref="MustSelectFromList"/> returns true, an empty string will be displayed.</remarks>
		/// <remarks><b>Note:</b>The base class implementation will <b>always</b> return false when <see cref="MustSelectFromList"/> returns false.</remarks>
		public override bool MustDisplayFromList( object ownerContext )
		{
			//	BF 8.19.02
			//	I believe we want to return false here to maintain version 1 compatibility.
			//	Returning true means that dropdown lists will display an empty string
			//	if the current value does not match an item in the list; v1 of the UltraGrid
			//	would display the text even if it doesn't match an item.
			//
			//return true;
			return false;
		}

		#endregion //	MustDisplayFromList	

		#region EnsureValueIsInList

		/// <summary>
		/// Returns whether an editor's value must correspond to an item in its ValueList
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <remarks>The default implementation returns false.</remarks>
		/// <remarks>When an instance of an EmbeddableEditorOwnerBase-derived class returns true from this method, the editor will validate its text against the items in its ValueList and return false from the IsValid property if the text does not match any items.</remarks>
		public override bool EnsureValueIsInList( object ownerContext )
		{
			// SSP 7/18/03 - Cell level Style property
			//
			//if ( ColumnStyle.DropDownValidate == this.Column.Style )
			if ( ColumnStyle.DropDownValidate == this.column.GetStyleResolved( GetRowFromOwnerContext( ownerContext ) ) )
				return true;

			return base.EnsureValueIsInList( ownerContext );
		}

		#endregion //	EnsureValueIsInList

		#region DrawAsActive

		// SSP 7/26/02 UWG1449
		// Overrode DrawAsActive method.
		//
		/// <summary>
		/// Returns whether the element should be drawn as if it were in its "active" state. Only applicable if the return from the DisplayStyle property is not Standard.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        /// <returns>Whether the element should be drawn as if it is in its "active" state.</returns>
		/// <remarks>
		/// The default implementation returns <b>DefaultableBoolean.Default</b>, for which the element should follow the cursor position.
		/// </remarks>
		public override DefaultableBoolean DrawAsActive( object ownerContext )
		{
	#region Obsolete code (UWG1449)
			
	#endregion //	Obsolete code (UWG1449)

			//	BF 8.5.02	UWG1449
			//
			//	I revised this implementation to fix bug UWG1449

			// SSP 8/6/02
			// Since cells are lazily created, we don't want to create a cell just 
			// to check if we are in edit mode. Accessing cellUI.Cell will cause the
			// cell to be created. So I just rewrote the code that will not cause the
			// cell to be created if not already created.
			// Commented out below block of code and added code below.
			//
			
			UltraGridRow row = EmbeddableEditorOwnerInfo.GetRowFromOwnerContext( ownerContext );
			UltraGridColumn column = this.Column;
	
			if ( null == row )
				return DefaultableBoolean.Default;

			// SSP 12/24/02
			// Optimizations.
			// Use HasCell off the row which checks for cells collection being allocated or
			// not. Directly accessing the cells property will cause the cells collection to
			// be created which we don't want here.
			//	
			//UltraGridCell cell = row.Cells.HasCell( this.Column ) ? row.Cells[ this.Column ] : null;
			UltraGridCell cell = row.GetCellIfAllocated( column );

			// SSP 4/17/03 - Optimizations
			//
			// SSP 4/22/05 - Optimizations
			//
			//UltraGridCell activeCell = column.Layout.ActiveCell;
			UltraGridCell activeCell = this.Layout.ActiveCellInternal;

			if ( cell == activeCell && null != cell && cell.IsInEditMode &&
				column.Layout.ActiveColScrollRegion == EmbeddableEditorOwnerInfo.GetColScrollRegion( ownerContext ) &&
				column.Layout.ActiveRowScrollRegion == EmbeddableEditorOwnerInfo.GetRowScrollRegion( ownerContext ) )
				return DefaultableBoolean.True;
			else
			{
				//	If there is a cell in edit mode, but it is not this cell, we want to return false
				//	so that only the edit mode cell is drawn as active. If no cells are in edit mode,
				//	then we want to return DefaultableBoolean.Default; the editor will then handle
				//	active drawing based on whether the cursor is positioned over the element.
				//
				// SSP 4/17/03 - Optimizations
				//
				if ( null == activeCell || ! activeCell.IsInEditMode )
					//				if ( this.Column.Layout.ActiveCell == null ||
					//						! this.Column.Layout.ActiveCell.IsInEditMode )
					return DefaultableBoolean.Default;

				return DefaultableBoolean.False;
			}
	
		}

		#endregion // DrawAsActive

		//	BF 11.7.02
		#region GetTextBoxScrollBars
		/// <summary>
		/// Returns which scrollbars should appear in an editor's multiline TextBox.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>A boolean indicating which scrollbars should appear in an editor's multiline TextBox.</returns>
		public override System.Windows.Forms.ScrollBars GetTextBoxScrollBars( object ownerContext )
		{
			//	If the column's VertScrollBar is true, return ScrollBars.Vertical;
			//	otherwise, return None. Note that there is no property of the
			//	Column object that determines whether horizontal scrollbars
			//	should be displayed, so we don't ever return 'Both' or 'Horizontal'.
			if ( this.Column != null &&
				this.Column.VertScrollBar )
				return System.Windows.Forms.ScrollBars.Vertical;
			else
				//	Call the base class implementation, which will return
				//	the default owner's value, if they supply one. If they don't,
				//	neither horizontal nor vertical scrollbars wll be displayed.
				return base.GetTextBoxScrollBars( ownerContext );

		}
		#endregion //	GetTextBoxScrollBars

		// JAS 2005 v2 XSD Support
		//
		#region GetConstraints

		/// <summary>
		/// Returns the <see cref="ValueConstraint"/> object associated with the column.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		public override ValueConstraint GetConstraints( object ownerContext )
		{
			// Create a new ValueConstraint.
			ValueConstraint mergedConstraint = new ValueConstraint();

			bool setValidateAsType = false;

			// Merge the temp object with the column's ValueConstraint object, if available.
			if( this.Column != null && this.Column.Constraint != null )
			{
				// If the column's ValueList was not extracted from an XSD schema
				// then do not copy over the ValueConstraint's reference to it.
				// This prevents the ValueList from being used during validation.
				ValueConstraintFlags propsToCopy = ValueConstraintFlags.All;
				if( (this.Column.XsdSuppliedConstraints & XsdConstraintFlags.Enumeration) == 0 )
					propsToCopy &= ~ValueConstraintFlags.Enumeration;

				mergedConstraint.Merge( this.Column.Constraint, propsToCopy  );
				mergedConstraint.ValidateAsType = this.Column.Constraint.ValidateAsType;
				setValidateAsType = true;
			}

			// Call base's GetConstraints.
			ValueConstraint baseConstraint = base.GetConstraints( ownerContext );

			// Merge the base class's constraint with the temp constraint.
			if( baseConstraint != null )
			{
				mergedConstraint.Merge( baseConstraint );

				if( ! setValidateAsType )
					mergedConstraint.ValidateAsType = baseConstraint.ValidateAsType;
			}			

			return mergedConstraint;
		}

		#endregion // GetConstraints

		// AS 7/24/03
		// We need a way for the editor buttons to get some context relative to
		// the object it is dealing with that the programmer can use without relying
		// upon the contents/structure of the OwnerContext that the owner provides to
		// the editor.
		//
		#region GetExternalContext

		/// <summary>
		/// Returns an object that may be provided to the programmer using an editor.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>An object that the programmer may use to determine which object was associated with an action.</returns>
		/// <remarks>
		/// <p class="body">The <see cref="EmbeddableUIElementBase.OwnerContext"/> is provided to the editor when it creates or reinitializes an embeddable uielement. 
		/// The embeddable element in turn uses that information when making requests to the owner for information but the editor. That object is opaque to the editor but provides context to the owner 
		/// so that it knows which object the editor is dealing with. An owner may change the contents of the <b>OwnerContext</b> or the object may not be useful to the programmer using an editor. For example, a CellUIElement 
		/// may be the <b>OwnerContext</b> for a grid's cell editor but that isn't normally the level of information that the programmer would need. The programmer needs a 
		/// Cell object and shouldn't be expected to extract that from the OwnerContext. Instead, an editor will ask the owner via this method to translate the <b>OwnerContext</b> to 
		/// something that the programmer can use.</p>
		/// </remarks>
		public override object GetExternalContext( object ownerContext )
		{
			UltraGridRow row = GetRowFromOwnerContext(ownerContext);

            if (row != null)
            {
                // MRS 1/4/2008 - BR29336
                if (row.IsGroupByRow)
                    return row;

                return row.Cells[this.Column];
            }

			return null;
		}

		#endregion //GetExternalContext

        // MRS 4/13/06 - BR08446 & BR11407
        // I took this code out. We decided that the grid should support
        // the DisplayStyle. BR08446 was fixed in Win by checking the 
        // DrawOuterBorders property on Embeddable. 
        #region BR08446 - Old Code
        //// MRS 12/28/05 - BR08446
        //#region GetDisplayStyle

        ///// <summary>
        ///// Returns the DisplayStyle to be used by the embeddable element
        ///// </summary>
        ///// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
        ///// <returns>The DisplayStyle to be used by the embeddable element</returns>
        ///// <remarks>// The grid does not make use of the DisplayStle of the editor within a cell. So this method always returns <b>EmbeddableElementDisplayStyle.Standard</b>.</remarks>
        //public override EmbeddableElementDisplayStyle GetDisplayStyle(object ownerContext)
        //{
        //    // The grid does not make use of the DisplayStle of the editor within a cell. 
        //    // So always return Standard. 
        //    return EmbeddableElementDisplayStyle.Standard;
        //}

        //#endregion GetDisplayStyle
        #endregion BR08446 - Old Code

        #region GetSpellChecker

        // SSP 4/5/06 - NAS 6.2 - Support for New SpellCheck functionality 
		// Added SpellChecker property.
		// 
		/// <summary>
		/// Gets the spell checker
		/// </summary>
		/// <returns>The <see cref="UltraGridColumn.SpellChecker"/></returns>
		public override Infragistics.Win.UltraWinSpellChecker.IUltraSpellChecker GetSpellChecker( object ownerContext )
		{
			UltraWinSpellChecker.IUltraSpellChecker spellChecker = this.Column.SpellChecker;
			if ( null != spellChecker )
				return spellChecker;

			return base.GetSpellChecker( ownerContext );
		}

		#endregion //GetSpellChecker

		#region GetTypeConverter

		// SSP 4/30/07 BR22014
		// 
		/// <summary>
		/// Returns the type converter to use to convert values. The default implementation
		/// returns the type converter associated with the data type of the owner.
		/// </summary>
		/// <param name="ownerContext">The owner context</param>
		/// <param name="ignoreTypeConverters">Specifies whether to ignore type converters associated with the data types and
		/// only use the one, if any, returned by this method.</param>
		/// <returns></returns>
		public override TypeConverter GetTypeConverter( object ownerContext, out bool ignoreTypeConverters )
		{
			ignoreTypeConverters = false;
			PropertyDescriptor pd = this.column.PropertyDescriptor;
			if ( null != pd )
			{
				TypeConverter cc = pd.Converter;
				if ( null != cc )
					return cc;
			}

			return base.GetTypeConverter( ownerContext, out ignoreTypeConverters );
		}

		#endregion // GetTypeConverter

		#endregion    //Virtual Methods

    }

	#endregion // EmbeddableEditorOwnerInfo Class

	#region FilterOperandEmbeddableEditorOwnerInfo Class
	
	internal class FilterOperandEmbeddableEditorOwnerInfo : GridCellEmbeddableEditorOwnerInfoBase
	{
		#region Private Members

		#endregion    //Private Members

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="column"></param>
		internal FilterOperandEmbeddableEditorOwnerInfo( UltraGridColumn column ) : base( column )
		{
		}

		#endregion    //Constructor

		#region Private/Internal Properties/Methods

		#region UsingColumnEditor

		private bool UsingColumnEditor
		{
			get
			{
				return FilterOperandStyle.UseColumnEditor == this.column.FilterOperandStyleResolved;
			}
		}

		#endregion // UsingColumnEditor

		#region GetFilterRowFromOwnerContext

		internal static UltraGridFilterRow GetFilterRowFromOwnerContext( object ownerContext )
		{
			UltraGridFilterRow filterRow = GridEmbeddableEditorOwnerInfoBase.GetRowFromOwnerContext( ownerContext ) as UltraGridFilterRow;
			Debug.Assert( null != filterRow );
			return filterRow;
		}

		#endregion // GetFilterRowFromOwnerContext

		#endregion // Private/Internal Properties/Methods

		#region Implemented Abstract Methods Overridden

				#region GetEditor

		/// <summary>
		/// Returns the editor for the passed in ownerContext. This is used by the base implementation of EmbeddableOwnerBase.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override EmbeddableEditorBase GetEditor( object ownerContext )
		{
			return this.column.FilterOperandEditor;
		}

				#endregion // GetEditor

				#region GetDataType	

		/// <summary>
		/// Returns the data type.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override System.Type GetDataType( object ownerContext )
		{
			// SSP 9/16/03 UWG2512
			//
			// ------------------------------------------------------
			//return this.Column.DataType;
			Type type = this.column.DataType;

			// SSP 5/24/06 BR12854
			// For combo and drop down list styles, always return string as we will 
			// be adding Blank, NonBlank, Custom etc... string items and also see the
			// BR12854 bug sample for more info.
			// 
			// ------------------------------------------------------------------------
			// SSP 9/7/07 BR25866
			// Commented out the following code. This was an optimization originally 
			// implemented before making the change below for BR18566. However with
			// the BR18566 fix, this is not valid anymore and causes BR25866.
			// 
			//if ( typeof( string ) == type )
			//	return type;
			
			FilterOperandStyle operandStyle = this.Column.FilterOperandStyleResolved;
			if ( FilterOperandStyle.DropDownList == operandStyle || FilterOperandStyle.Combo == operandStyle )
				// SSP 12/13/06 BR18566
				// 
				//type = typeof( string );
				return typeof( object );
			// ------------------------------------------------------------------------

			if ( null == type || typeof( object ) == type )
			{
				type = base.GetDataType( ownerContext );

				if ( null == type )
					type = this.Column.DataType;
			}
			return type;
			// ------------------------------------------------------
		}

				#endregion    //GetDataType	

				#region GetValue	

		/// <summary>
		/// Returns the value that should be rendered.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override object GetValue( object ownerContext )
		{
			UltraGridRow row = GetRowFromOwnerContext( ownerContext );
			return row.GetCellValue( this.column );
		}

				#endregion    //GetValue	

				#region IsNullable	

		/// <summary>
		/// Returns whether the value can be set to null.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>True if the value can be set to null.</returns>
		public override bool IsNullable( object ownerContext )
		{
			return true;
		}

				#endregion    //IsNullable	

				#region GetNullText	

		/// <summary>
		/// The string to display for a null value.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="nullText">(out) The string that should be used if the value is null or DB_Null.</param>
		/// <returns>A boolean indicating whether a meaningful value was returned.</returns>
		/// <remarks>The default implementation returns an empty string.</remarks>
		public override bool GetNullText( object ownerContext, out string nullText )
		{
			nullText = string.Empty;
			return true;
		}

				#endregion    //GetNullText	

		#endregion // Implemented Abstract Methods Overridden

		#region Public Methods

		#region GetAutoEdit

		/// <summary>
		/// Indicates whether AutoEdit should enabled.
		/// </summary>
		/// <param name="ownerCotext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>
		/// Indicates whether AutoEdit should enabled. Default implementation returns false.
		/// </returns>
		public override bool GetAutoEdit( object ownerCotext )
		{
			return false;
		}

		#endregion // GetAutoEdit

		#region GetCharacterCasing	

		/// <summary>
		/// Determines how the text will be cased. 
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>The CharacterCasing to be applied to the text</returns>
		public override CharacterCasing GetCharacterCasing( object ownerContext )
		{
			return this.UsingColumnEditor 
				? this.column.EditorOwnerInfo.GetCharacterCasing( ownerContext )
				: base.GetCharacterCasing( ownerContext );
		}

		#endregion    //GetCharacterCasing	

		#region GetValueList	

		/// <summary>
		/// Returns a list of predefined values.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>Returns a list of predefined values or null.</returns>
		/// <remarks>The default implementation returns null.</remarks>
		public override IValueList GetValueList( object ownerContext )
		{
			UltraGridRow filterRow = GetFilterRowFromOwnerContext( ownerContext );
			IValueList valueList = null != filterRow
				? filterRow.GetValueListResolved( this.column )
				: null;

			// SSP 7/1/05 BR04783
			// If column or cell doesn't have a value list assigned then fallback to the
			// default owner by calling the base implementation.
			// 
			if ( null == valueList )
				valueList = base.GetValueList( ownerContext );

			return valueList;
		}

		#endregion    // GetValueList

		#region MustSelectFromList	

		/// <summary>
		/// Returns whether a selection can only be made from the value list.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>If true will act as a combo with a style of DropDownList.</returns>
		/// <remarks>The default implementation returns false.</remarks>
		public override bool MustSelectFromList( object ownerContext )
		{
			FilterOperandStyle operandStyle = this.column.FilterOperandStyleResolved;
			if ( FilterOperandStyle.UseColumnEditor == operandStyle )
				return this.column.EditorOwnerInfo.MustSelectFromList( ownerContext );

			if ( FilterOperandStyle.DropDownList == operandStyle )
				return true;

			return base.MustSelectFromList( ownerContext );
		}

		#endregion    //MustSelectFromList	

		#region MustDisplayFromList	

		// SSP 8/2/02
		// Overrode newly added MustDisplayFromList method to return true. This is for
		// consistency with version 1.
		//
		/// <summary>
		/// Returns whether the text an element displays must correspond to the text of an item in a list
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>If true and the editor's Value does not match an item in the list, an empty string will be displayed.</returns>
		/// <remarks>The default implementation returns <b>true</b>, meaning that if an editor's Value does not correspond to an item in the list, and <see cref="MustSelectFromList"/> returns true, an empty string will be displayed.</remarks>
		/// <remarks><b>Note:</b>The base class implementation will <b>always</b> return false when <see cref="MustSelectFromList"/> returns false.</remarks>
		public override bool MustDisplayFromList( object ownerContext )
		{
			//	BF 8.19.02
			//	I believe we want to return false here to maintain version 1 compatibility.
			//	Returning true means that dropdown lists will display an empty string
			//	if the current value does not match an item in the list; v1 of the UltraGrid
			//	would display the text even if it doesn't match an item.
			//
			//return true;
			return false;
		}

		#endregion //	MustDisplayFromList	

		#region EnsureValueIsInList

		/// <summary>
		/// Returns whether an editor's value must correspond to an item in its ValueList
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <remarks>The default implementation returns false.</remarks>
		/// <remarks>When an instance of an EmbeddableEditorOwnerBase-derived class returns true from this method, the editor will validate its text against the items in its ValueList and return false from the IsValid property if the text does not match any items.</remarks>
		public override bool EnsureValueIsInList( object ownerContext )
		{
			return this.UsingColumnEditor 
				? this.column.EditorOwnerInfo.EnsureValueIsInList( ownerContext )
				: base.EnsureValueIsInList( ownerContext );
		}

		#endregion //	EnsureValueIsInList

		#endregion // Public Methods
	}

	#endregion // FilterOperandEmbeddableEditorOwnerInfo Class

	#region FilterOperatorEmbeddableEditorOwnerInfo Class

	internal class FilterOperatorEmbeddableEditorOwnerInfo : GridEmbeddableEditorOwnerInfoBase
	{
		#region Private Members

		private ValueList cachedFilterOperatorsValueList = null;
		private FilterOperatorDropDownItems cachedFilterOperatorsValueListItems = (FilterOperatorDropDownItems)0;
		private int verifiedGrandVersion = -1;

		// SSP 10/31/05 BR07291
		// 
		private int verifiedCustomizedStringsVersion = -1;

		#endregion    //Private Members

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="column"></param>
		internal FilterOperatorEmbeddableEditorOwnerInfo( UltraGridColumn column ) : base( column )
		{
		}

		#endregion    //Constructor

		#region Private/Internal Properties

		#region FilterOperatorsValueList

		internal ValueList FilterOperatorsValueList
		{
			get
			{
				if ( null == this.cachedFilterOperatorsValueList
					|| this.verifiedGrandVersion != this.Layout.GrandVerifyVersion 
					// SSP 10/31/05 BR07291
					// 
                    || this.verifiedCustomizedStringsVersion != Resources.Customizer.CustomizedStringsVersion )
				{
					this.verifiedGrandVersion = this.Layout.GrandVerifyVersion;

					if ( null == this.cachedFilterOperatorsValueList
						|| this.cachedFilterOperatorsValueListItems != this.Column.FilterOperatorDropDownItemsResolved 
						// SSP 10/31/05 BR07291
						// 
                        || this.verifiedCustomizedStringsVersion != Resources.Customizer.CustomizedStringsVersion)
					{
						// SSP 10/31/05 BR07291
						// 
                        this.verifiedCustomizedStringsVersion = Resources.Customizer.CustomizedStringsVersion;

						this.cachedFilterOperatorsValueList = new ValueList( );
						this.cachedFilterOperatorsValueListItems = this.Column.FilterOperatorDropDownItemsResolved;
						this.Layout.LoadFilterOperatorValueList( this.cachedFilterOperatorsValueList, this.cachedFilterOperatorsValueListItems );
					}
				}

				return this.cachedFilterOperatorsValueList;
			}
		}

		#endregion // FilterOperatorsValueList

		#region GetFilterRowFromOwnerContext

		internal static UltraGridFilterRow GetFilterRowFromOwnerContext( object ownerContext )
		{
			return FilterOperandEmbeddableEditorOwnerInfo.GetFilterRowFromOwnerContext( ownerContext );
		}

		#endregion // GetFilterRowFromOwnerContext

		#endregion // Private/Internal Properties

		#region Implemented Abstract Methods Overridden

				#region GetEditor

		/// <summary>
		/// Returns the editor for the passed in ownerContext. This is used by the base implementation of EmbeddableOwnerBase.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override EmbeddableEditorBase GetEditor( object ownerContext )
		{
			return this.Column.FilterOperatorEditor;
		}

				#endregion // GetEditor

				#region GetDataType	

		/// <summary>
		/// Returns the data type.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override System.Type GetDataType( object ownerContext )
		{
			return typeof( FilterComparisionOperator );
		}

				#endregion    //GetDataType	

				#region GetValue	

		/// <summary>
		/// Returns the value that should be rendered.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override object GetValue( object ownerContext )
		{
			UltraGridFilterRow filterRow = GetRowFromOwnerContext( ownerContext ) as UltraGridFilterRow;
			Debug.Assert( null != filterRow );
			return null != filterRow
				? filterRow.GetOperatorValue( this.column )
				: null;
		}

				#endregion    //GetValue	

				#region IsNullable	

		/// <summary>
		/// Returns whether the value can be set to null.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>True if the value can be set to null.</returns>
		public override bool IsNullable( object ownerContext )
		{
			return false;
		}

				#endregion    //IsNullable	

				#region ResolveAppearance	
		
		/// <summary>
		/// Resolves the appearance for an element.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="appearance">The appearance structure to initialize.</param>
		/// <param name="requestedProps">The appearance properties to resolve.</param>
		/// <param name="area">Enumeration of type <see cref="EmbeddableEditorArea"/> describing the area of the embeddable element to which the appearance will be applied</param>
		/// <param name="hotTracking">Boolean indicating whether the owner should apply its 'HotTrackingAppearance'</param>
		/// <param name="customArea">A string that denotes which appearance to resolve. Applicable only when the 'area' parameter is set to Custom.</param>
		/// <returns>True if the owner recognizes and supports the named appearance.</returns>
		public override bool ResolveAppearance ( object ownerContext,
			ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps,
			EmbeddableEditorArea area,
			bool hotTracking,
			string customArea )
		{
			if ( EmbeddableEditorArea.Default == area || EmbeddableEditorArea.Text == area )
				this.Column.ResolveFilterOperatorAppearance( ref appearance, ref requestedProps );

			bool retVal = base.ResolveAppearance( 
				ownerContext, 
				ref appearance, 
				ref requestedProps, 
				area, 
				hotTracking, 
				customArea );

			return retVal;
		}

				#endregion    //ResolveAppearance	

		#endregion // Implemented Abstract Methods Overridden

		#region Virtual Methods

		// JAS BR04593 7/8/05 - Needed a way to tell the EditorWithCombo
		// that it should not respond to mouse wheel messages.
		#region CanProcessMouseWheel

		/// <summary>
		/// Returns true if the editor is dropped down.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns></returns>
		public override bool CanProcessMouseWheel( object ownerContext )
		{
			// SSP 7/8/05 BR03866
			// Since in a filter cell the operator and operand editors both are in edit mode
			// at the same time we need to prevent the filter operator editor from processing 
			// the mouse wheel unless it's dropped down. For that we added CanProcessMouseWheel
			// on the owner which the editor with combo calls.
			// 
			EmbeddableEditorBase editor = this.GetEditor( ownerContext );
			EmbeddableUIElementBase elemInEditMode = null != editor && editor.IsInEditMode ? editor.ElementBeingEdited : null;

			// Make the filter operator editor process the mouse wheel only when the 
			// drop down is dropped down.
			// 
			return null != elemInEditMode && ownerContext == elemInEditMode.OwnerContext 
				&& editor.SupportsDropDown && editor.IsDroppedDown;
		}

		#endregion // CanProcessMouseWheel

		#region GetValueList	

		/// <summary>
		/// Returns a list of predefined values.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>Returns a list of predefined values or null.</returns>
		/// <remarks>The default implementation returns null.</remarks>
		public override IValueList GetValueList( object ownerContext )
		{
			return this.FilterOperatorsValueList;
		}

		#endregion    //GetValueList	

		#region MustDisplayFromList	

		// SSP 8/2/02
		// Overrode newly added MustDisplayFromList method to return true. This is for
		// consistency with version 1.
		//
		/// <summary>
		/// Returns whether the text an element displays must correspond to the text of an item in a list
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>If true and the editor's Value does not match an item in the list, an empty string will be displayed.</returns>
		/// <remarks>The default implementation returns <b>true</b>, meaning that if an editor's Value does not correspond to an item in the list, and <see cref="Infragistics.Win.EmbeddableEditorOwnerBase.MustSelectFromList"/> returns true, an empty string will be displayed.</remarks>
		/// <remarks><b>Note:</b>The base class implementation will <b>always</b> return false when <see cref="Infragistics.Win.EmbeddableEditorOwnerBase.MustSelectFromList"/> returns false.</remarks>
		public override bool MustDisplayFromList( object ownerContext )
		{
			//	BF 8.19.02
			//	I believe we want to return false here to maintain version 1 compatibility.
			//	Returning true means that dropdown lists will display an empty string
			//	if the current value does not match an item in the list; v1 of the UltraGrid
			//	would display the text even if it doesn't match an item.
			//
			//return true;
			return false;
		}

		#endregion //	MustDisplayFromList	

		#region MustSelectFromList	

		/// <summary>
		/// Returns whether a selection can only be made from the value list.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>If true will act as a combo with a style of DropDownList.</returns>
		/// <remarks>The default implementation returns false.</remarks>
		public override bool MustSelectFromList( object ownerContext )
		{
			return true;
		}

		#endregion    //MustSelectFromList

		#region EnsureValueIsInList

		/// <summary>
		/// Returns whether an editor's value must correspond to an item in its ValueList
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <remarks>The default implementation returns false.</remarks>
		/// <remarks>When an instance of an EmbeddableEditorOwnerBase-derived class returns true from this method, the editor will validate its text against the items in its ValueList and return false from the IsValid property if the text does not match any items.</remarks>
		public override bool EnsureValueIsInList( object ownerContext )
		{
			return true;
		}

		#endregion //	EnsureValueIsInList

		#region GetPadding	

		/// <summary>
		/// The padding to place around the value to display.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="padding">(out) The padding to place around the value to display.</param>
		/// <returns>A boolean indicating whether a meaningful value was returned.</returns>
		public override bool GetPadding( object ownerContext, out System.Drawing.Size padding )
		{
			if ( FilterOperatorLocation.WithOperand == this.column.FilterOperatorLocationResolved )
			{
				padding = new Size( 0, 0 );
				return true;
			}

			return base.GetPadding( ownerContext, out padding );
		}

		#endregion // GetPadding

		// AS 11/19/03 Accessibility
		#region GetEditorElement

		/// <summary>
		/// Returns the embeddable uielement associated with a particular object or null if none is available.
		/// </summary>
		/// <param name="ownerContext">Context used to identify the object to reference</param>
		/// <returns>The embeddable uielement representing the specified owner context.</returns>
		public override EmbeddableUIElementBase GetEditorElement( object ownerContext )
		{
			// first see if the context is an element
			// SSP 9/8/05 BR06252
			// 
			//UIElement elem = ownerContext as CellUIElementBase;
			UIElement elem = GetWrappedCellElement( ownerContext );

			if ( null == elem )
			{
				elem = this.GetRCRIntersectionElem( ownerContext );

				// if its not, get the row
				UltraGridRow row = EmbeddableEditorOwnerInfo.GetRowFromOwnerContext(ownerContext);

				if ( null != elem && null != row )
					elem = elem.GetDescendant( typeof( FilterOperatorUIElement ), new object[] { row, this.column } );
			}

			return null != elem ? CellUIElementBase.FindEmbeddableUIElement( elem.ChildElements, false ) : null;
		}

		#endregion //GetEditorElement

		// SSP 12/16/02
		// Optimizations. Added GetEditorContext and SetEditorContext methods for
		// caching things like index in the value list items collection of the last
		// matching item.
		//
		#region GetEditorContext

		/// <summary>
		/// Gets the editor context that was set with SetEditorContext method.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <returns>Editor context that was last set with SetEditorContext for the passed in ownerContext.</returns>
		/// <remarks>
		/// <p>GetEditorContext and <seealso cref="SetEditorContext"/> can be used to cache objects per owner context.</p>
		/// <p>Implementing owner will return the object that was last cached using SetEditorContext method.</p>
		/// </remarks>
		public override object GetEditorContext( object ownerContext )
		{
			UltraGridFilterRow filterRow = GetFilterRowFromOwnerContext( ownerContext );
			ColumnFilter cf = null != filterRow ? filterRow.GetColumnFilter( this.Column ) : null;
			return null != cf ? cf.filterRowOperatorEditorContext : null;
		}

		#endregion // GetEditorContext

		#region SetEditorContext

		/// <summary>
		/// Sets the editor context for the passed in ownerContext.
		/// </summary>
		/// <param name="ownerContext">The context that was passed into the <see cref="Infragistics.Win.EmbeddableEditorBase.GetEmbeddableElement(UIElement, EmbeddableEditorOwnerBase, object, EmbeddableUIElementBase)"/> method.</param>
		/// <param name="editorContext"></param>
		/// <remarks>
		/// <p class="body"><seealso cref="GetEditorContext"/> and SetEditorContext can be used to cache objects per owner context.</p>
		/// <p class="body">Implementing owner will return the object that was last cached using SetEditorContext method.</p>
		/// </remarks>
		public override void SetEditorContext( object ownerContext, object editorContext )
		{
			UltraGridFilterRow filterRow = GetFilterRowFromOwnerContext( ownerContext );
			ColumnFilter cf = null != filterRow ? filterRow.GetColumnFilter( this.Column ) : null;
			if ( null != cf )
				cf.filterRowOperatorEditorContext = editorContext;
		}

		#endregion // SetEditorContext

        // MBS 7/9/08 - BR33993
        #region ReleaseResources

        internal override void ReleaseResources()
        {
            if (this.cachedFilterOperatorsValueList != null)
                this.cachedFilterOperatorsValueList.ReleaseResources();

            base.ReleaseResources();
        }
        #endregion //ReleaseResources

		#endregion    //Virtual Methods
	}

	#endregion // FilterOperatorEmbeddableEditorOwnerInfo Class

	#region ToolTipOwnerContext

	// SSP 9/8/05 BR06252
	// When a cell is being hot-tracked, we need to resolve the tool-tip appearance without
	// the hot-tracking appearances in it. ResolveAppearance of the embeddable owner needs to
	// know that it's a tool tip. The only way it can do so is using a owner context that
	// indicates that it's a tool tip embeddable element. That's what this class is for.
	// 
	internal class ToolTipOwnerContext
	{
		internal object ownerContext;

		internal ToolTipOwnerContext( object ownerContext )
		{
			this.ownerContext = ownerContext;
		}
	}

	#endregion // ToolTipOwnerContext
}
