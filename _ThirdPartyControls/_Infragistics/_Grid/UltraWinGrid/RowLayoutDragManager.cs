#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;

using System.Diagnostics;
using Infragistics.Win;
using Infragistics.Win.Layout;
using System.Collections.Generic;
using Infragistics.Win.UltraWinGrid.Design;


namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Helper class which provides information to the LayoutDragStrategy. For internal Infragistics use only. 
	/// </summary>
	public class GridBagLayoutDragManager : IGridBagLayoutDragManager
	{
		#region Enums
		/// <summary>
		/// Enum indicating the type of GridBagLayoutDrag.
		/// </summary>
		public enum GridBagLayoutDragType
		{
			/// <summary>
			/// A column header is being repositioned.
			/// </summary>
			HeaderReposition,

			/// <summary>
			/// A column header is being Span-Resized. 
			/// </summary>
			HeaderSpanResize,

			/// <summary>
			/// A cell is being Span-Resized. 
			/// </summary>
			CellSpanResize,
		}
		#endregion Enums

		#region Private Members

        // MRS - NAS 9.1 - Groups in RowLayout
        // Changed this member variable everywhere it appeared in this class.
        //private UltraGridColumn columnBeingDragged = null;
        private IProvideRowLayoutColumnInfo itemBeingDragged = null;
		
		private GridBagLayoutDragType dragType;
		
		// MRS 4/21/05 - BR03503
		internal bool isGroupByButtonBeingDragged = false;

		// MRS 5/24/05 - BR04224
		Hashtable cachedRowLayoutInfo = null;

		#endregion Private Members

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="columnBeingDragged">The UltraGridColumn being dragged.</param>
		public GridBagLayoutDragManager(UltraGridColumn columnBeingDragged)
			: this (columnBeingDragged, GridBagLayoutDragType.HeaderReposition)
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="columnBeingDragged">The UltraGridColumn being dragged.</param>
		/// <param name="dragType">Indicates the type of drag being performed.</param>        
		public GridBagLayoutDragManager(UltraGridColumn columnBeingDragged, GridBagLayoutDragType dragType)
            // MRS - NAS 9.1 - Groups in RowLayout
            : this((IProvideRowLayoutColumnInfo)columnBeingDragged, dragType) {}

        // MRS - NAS 9.1 - Groups in RowLayout
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="groupBeingDragged">The UltraGridGroup being dragged.</param>
        public GridBagLayoutDragManager(UltraGridGroup groupBeingDragged)
            : this(groupBeingDragged, GridBagLayoutDragType.HeaderReposition)
        {
        }

        // MRS - NAS 9.1 - Groups in RowLayout
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="groupBeingDragged">The UltraGridGroup being dragged.</param>
        /// <param name="dragType">Indicates the type of drag being performed.</param>        
        public GridBagLayoutDragManager(UltraGridGroup groupBeingDragged, GridBagLayoutDragType dragType) 
            : this ((IProvideRowLayoutColumnInfo)groupBeingDragged, dragType) {}

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="itemBeingDragged">The item being dragged.</param>
        /// <param name="dragType">Indicates the type of drag being performed.</param>        
        internal GridBagLayoutDragManager(IProvideRowLayoutColumnInfo itemBeingDragged, GridBagLayoutDragType dragType)
        {
            if (null == itemBeingDragged)
                throw new ArgumentNullException("itemBeingDragged");

            this.itemBeingDragged = itemBeingDragged;
            this.dragType = dragType;
        }

		#endregion Constructor

		#region IGridBagLayoutDragManager implementation

		#region IGridBagLayoutDragManager.Control
		/// <summary>
		/// The Control in which the drag starts
		/// </summary>
        // MRS v7.2 - UltraGridBagLayoutPanel
		//UltraControlBase IGridBagLayoutDragManager.Control
        Control IGridBagLayoutDragManager.Control
		{
			get 
			{
				// MD 1/22/09 - Groups in RowLayouts
				// The Control property has been renamed for clarity.
				//return this.Control;
				return this.Grid;
			}
		}
		#endregion IGridBagLayoutDragManager.Control

		#region IGridBagLayoutDragManager.GetContainerElementContext
		/// <summary>
		/// Returns the Context to use when trying to determine the Container Element of a layout item.
		/// </summary>
		/// <remarks>The Context is important so that when the DragManager attempt to find a Layout Container element at a certain point, it gets the correct one. For example, there may be many BandHeaderUIElements in the grid, but a drag operation is only valid within elements that have the same context (the same band).</remarks>
		/// <returns>The grid band.</returns>
		object IGridBagLayoutDragManager.GetContainerElementContext()
		{
			return this.Band;
		}
		#endregion IGridBagLayoutDragManager.GetContainerElementContext

		#region IGridBagLayoutDragManager.GetContainerElementType
		/// <summary>
		/// Returns the type of UIElement that contains the layout. 
		/// </summary>
		/// <remarks>
		/// This is the type of UIElement that contains the layout object. For example, in a grid this may be a BandHeaderUIElement (when dragging a Header that is separate from the cells) or a RowCellAreaUIElement (when the headers are with the cells).
		/// </remarks>
		Type IGridBagLayoutDragManager.GetContainerElementType()
		{
			// MD 1/22/09 - Groups in RowLayouts
			// Moved this implementation to a protected method so it can be overridden in derived types.
			return this.GetContainerElementType();
		}

		// MD 1/22/09 - Groups in RowLayouts
		/// <summary>
		/// Returns the type of UIElement that contains the layout. 
		/// </summary>
		/// <remarks>
		/// This is the type of UIElement that contains the layout object. For example, in a grid this may be a BandHeaderUIElement (when dragging a Header that is separate from the cells) or a RowCellAreaUIElement (when the headers are with the cells).
		/// </remarks>
		protected virtual Type GetContainerElementType()
		{
			return GridBagLayoutDragManager.GetContainerElementType(this.itemBeingDragged, this.dragType);
		}

		#endregion IGridBagLayoutDragManager.GetContainerElementType

		#region IGridBagLayoutDragManager.GetLayoutItemElementFromPoint
		/// <summary>
		/// Gets an an array of UIElements that represent a LayoutItem within the ContainingElement. This is for the purposes for determining the drop location. 
		/// </summary>
		/// <remarks>This method returns multiple elements and uses them to determine a drop rect. There may be multiple elements because when the headers and cells are together, both are treated as one.</remarks>		
		UIElement[] IGridBagLayoutDragManager.GetLayoutItemElementsFromPoint(Point point)
		{
			return this.GetDragElements(point);
		}
		#endregion IGridBagLayoutDragManager.GetLayoutItemElementFromPoint

		#region IGridBagLayoutDragManager.GetLayoutItemFromElement
		/// <summary>
		/// Returns an ILayoutItem that corresponds to a particulr UIElement
		/// </summary>
		ILayoutItem IGridBagLayoutDragManager.GetLayoutItemFromElement(UIElement element)
		{
            // MRS - NAS 9.1 - Groups in RowLayout
			//UltraGridColumn column = null;
            IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = null;            

			HeaderUIElement headerUIElement = element as HeaderUIElement;
			if (headerUIElement != null)
			{				
				ColumnHeader header = headerUIElement.GetContext() as ColumnHeader;
                if (header != null)
                    iProvideRowLayoutColumnInfo = header.GetColumn();
                else
                {
                    // MRS - NAS 9.1 - Groups in RowLayout
                    GroupHeader groupHeader = headerUIElement.GetContext(typeof(GroupHeader)) as GroupHeader;
                    if (groupHeader != null)
                        iProvideRowLayoutColumnInfo = groupHeader.Group;
                }                
			}			
			else
			{
				CellUIElementBase cellUIElement = element as CellUIElementBase;
				if (cellUIElement != null)
					iProvideRowLayoutColumnInfo = cellUIElement.GetContext(typeof(UltraGridColumn)) as UltraGridColumn;
			}      
			if (null == iProvideRowLayoutColumnInfo)
				return null;

			return this.GetLayoutItem(iProvideRowLayoutColumnInfo);
		}
		#endregion IGridBagLayoutDragManager.GetLayoutItemFromElement

		#region IGridBagLayoutDragManager.ItemBeingDragged
		/// <summary>
		/// Returns the ILayoutItem currently being dragged.
		/// </summary>
		ILayoutItem IGridBagLayoutDragManager.ItemBeingDragged
		{
			get
			{
				return this.GetLayoutItem(this.itemBeingDragged);
			}
		}
		#endregion IGridBagLayoutDragManager.ItemBeingDragged
		
		#region IGridBagLayoutDragManager.GetResolvedGCs
		/// <summary>
		/// Returns resolved GridBagConstraint objects for visible columns as a hashtable where the keys are the visible columns and the values are the GridBagConstraint objects.
		/// </summary>
		/// <returns>A hashTable where the keys are the ILayoutItems in the layout and the values are the GridBagConstraints.</returns>
		Hashtable IGridBagLayoutDragManager.GetResolvedGCs()
		{
			// MD 1/26/09 - Groups in RowLayouts
			// Moved code to a virtual method so it could be overriden.
			return this.GetResolvedGCs();
		}

		// MD 1/26/09 - Groups in RowLayouts
        /// <summary>
        /// Returns resolved GridBagConstraint objects for visible columns as a hashtable where the keys are the visible columns and the values are the GridBagConstraint objects.
        /// </summary>
        /// <returns>A hashTable where the keys are the ILayoutItems in the layout and the values are the GridBagConstraints.</returns>		
		protected virtual Hashtable GetResolvedGCs()
		{
			if (null == this.Band)
				return null;

			object key = null;
			RowLayoutColumnInfo ci = null;

			UltraGridColumn[] columnsArr = this.GetVisibleColumns( );
			Hashtable hash = new Hashtable( columnsArr.Length, 1.0f );

            // MRS - NAS 9.1 - Groups in RowLayout  
            if (this.Band.RowLayoutStyle == RowLayoutStyle.GroupLayout)
            {
                UltraGridGroup[] groupsArr = this.GetVisibleGroups();

				// MD 2/18/09
				// Found while fixing TFS14144
				// There is a better way to fix TFS13902 and we think it might also fix other issues because it should have been 
				// done all along. The fix for TFS13902 is now in RowLayoutColumnInfo.VerifyLayoutItemDimensions().
				//// MD 2/17/09 - TFS13902
				//// Verify the band's layout cache before getting the spans and origins of the groups because they might be stale.
				//if ( groupsArr.Length > 0 )
				//	groupsArr[ 0 ].Band.VerifyRowLayoutCache();

                for (int i = 0; i < groupsArr.Length; i++)
                {
                    UltraGridGroup group = groupsArr[i];
                    key = this.GetLayoutItem(group);

                    ci = group.RowLayoutGroupInfo;

                    hash[key] = new GridBagConstraint(
                        ci.OriginXResolved, ci.OriginYResolved, ci.SpanXResolved, ci.SpanYResolved);
                }
            }

			for ( int i = 0; i < columnsArr.Length; i++ )
			{
				UltraGridColumn column = columnsArr[i];
				key = this.GetLayoutItem(column);

				ci = column.RowLayoutColumnInfo;

				//hash[ column ] = new GridBagConstraint( 
				hash[ key ] = new GridBagConstraint( 
					ci.OriginXResolved, ci.OriginYResolved, ci.SpanXResolved, ci.SpanYResolved );
			}

			// MRS 4/29/05 - BR03503
			// Make sure the Item being dragged exists in the hash table, even
			// if it is not visible. 
			key = this.GetLayoutItem(this.itemBeingDragged);			
			if ( !(hash.ContainsKey(key)) )
			{
				ci = itemBeingDragged.RowLayoutColumnInfo;
				hash[ key ] = new GridBagConstraint( 
					ci.OriginXResolved, ci.OriginYResolved, ci.SpanXResolved, ci.SpanYResolved );
			}				

			return hash;
		}
		#endregion IGridBagLayoutDragManager.GetResolvedGCs
		
		#region IGridBagLayoutDragManager.SetResolvedGCs
		/// <summary>
		/// Sets the OriginX, OriginY, SpanX and SpanY of each layout item to respective property values in the GridBagConstraint object.
		/// </summary>
		/// <param name="hash">A hashtable where the keys are the visible ILayoutItems and the values are the GridBagConstraint objects.</param>
        /// <param name="newGroup">The new group into which the item being dragged was dropped.</param>
        // MRS - NAS 9.1 - Groups in RowLayout
		//void IGridBagLayoutDragManager.SetResolvedGCs(System.Collections.Hashtable hash)
        void IGridBagLayoutDragManager.SetResolvedGCs(System.Collections.Hashtable hash, ILayoutGroup newGroup)
		{

			// MRS 5/24/05 - BR04222
			// Created a helper method for this, so we can fire the
			// event here, but also call SetResolvedGCsInternal from
			// SetSpanGC without firing the ColPosChanged events. 
//			foreach ( object key in hash.Keys )
//			{
//				GridBagConstraint gc = (GridBagConstraint)hash[ key ];
//				RowLayoutColumnInfo ci = this.GetRowLayoutColumnInfo(key as ILayoutItem);
//				ci.Initialize( gc.OriginX, gc.OriginY, gc.SpanX, gc.SpanY );
//			}            

			// MD 1/22/09 - Groups in RowLayouts
			// The Control property has been renamed for clarity
			//UltraGrid grid = this.Control as UltraGrid;
			UltraGrid grid = this.Grid as UltraGrid;

            // MRS - NAS 9.1 - Groups in RowLayout
            UltraGridColumn columnBeingDragged = this.itemBeingDragged as UltraGridColumn;
            UltraGridGroup groupBeingDragged = this.itemBeingDragged as UltraGridGroup;

            // MRS 2/13/2009 - TFS13728
            if (columnBeingDragged == null &&
                groupBeingDragged == null)
            {
                ILayoutChildItem childItemBeingDragged = this.itemBeingDragged as ILayoutChildItem;
                groupBeingDragged = childItemBeingDragged != null
                    ? childItemBeingDragged.AssociatedGroup as UltraGridGroup
                    : null;
            }

            UltraGridGroup group = null;
            bool setGroup = false;

			if (this.itemBeingDragged != null &&
				grid != null)
			{
				// Store the original RowLayoutInfo in case the user
				// cancels BeforeColPosChanged			
                // MRS - NAS 9.1 - Groups in RowLayout
                //RowLayoutColumnInfo originalCI = new RowLayoutColumnInfo(this.iProvideRowLayoutColumnInfo);
                RowLayoutColumnInfo originalCI;                
                if (columnBeingDragged != null)
                    originalCI = new RowLayoutColumnInfo(columnBeingDragged);
                else
                    originalCI = new RowLayoutColumnInfo(groupBeingDragged);

				originalCI.InitializeFrom(this.itemBeingDragged.RowLayoutColumnInfo);
            
				// Set the RowLayoutColumnInfo on the column being dragged
				ILayoutItem layoutItem = this.GetLayoutItem(this.itemBeingDragged);
				GridBagConstraint gc = (GridBagConstraint)hash[ layoutItem ];
				this.itemBeingDragged.RowLayoutColumnInfo.Initialize( gc.OriginX, gc.OriginY, gc.SpanX, gc.SpanY );

                // MRS - NAS 9.1 - Groups in RowLayout
                // If the specified newGroup is null, it means we just want to leave the item in the
                // same group. So do nothing. 
                if (newGroup != null)
                {
                    // MRS 2/18/2009 - TFS14100
                    // Added 'if' block. We only want to do this if the group
                    // has actually changed. 
                    //
                    ILayoutChildItem childItemBeingDragged = ((IGridBagLayoutDragManager)this).ItemBeingDragged as ILayoutChildItem;
                    Debug.Assert(childItemBeingDragged != null, "Item being dragged is not an ILayoutChildItem; unexpected");
                    if (childItemBeingDragged != null &&
                        childItemBeingDragged.ParentGroup != newGroup)
                    {
                        // If it's not null, then the new group is either a real group or the Band. If it's
                        // the Band, then we need to explicitly set the ParentGroup to null. 
                        group = newGroup is UltraGridGroup
                            ? (UltraGridGroup)newGroup
                            : null;

                        setGroup = true;
                    }

                    // MRS 2/13/2009 - TFS13728
                    //originalCI.ParentGroup = group;                    
                }

                // MRS - NAS 9.1 - Groups in RowLayout
                // Surrounded the existing code in an 'if' block to see if we are dragging a column. 
                if (columnBeingDragged != null)
                {
                    // MRS - NAS 9.1 - Groups in RowLayout
                    //ColumnHeader[] colHeaders = new ColumnHeader[] { this.itemBeingDragged.Header };
                    ColumnHeader[] colHeaders = new ColumnHeader[] { columnBeingDragged.Header };

                    BeforeColPosChangedEventArgs e = new BeforeColPosChangedEventArgs(PosChanged.Moved, colHeaders);

                    //fire the BeforeColPosChanged event
                    grid.FireBeforeColPosChanged(e);

                    if (e.Cancel)
                    {
                        this.itemBeingDragged.RowLayoutColumnInfo.InitializeFrom(originalCI);
                        return;
                    }
                }
                else
                {
                    // MRS - NAS 9.1 - Groups in RowLayout
                    // Added this 'else' block for when we are dragging a group, not a column. 
                    GroupHeader[] groupHeaders = new GroupHeader[] { groupBeingDragged.Header };

                    BeforeGroupPosChangedEventArgs e = new BeforeGroupPosChangedEventArgs(PosChanged.Moved, groupHeaders);

                    //fire the BeforeColPosChanged event
                    grid.FireBeforeGroupPosChanged(e);

                    if (e.Cancel)
                    {
                        this.itemBeingDragged.RowLayoutColumnInfo.InitializeFrom(originalCI);
                        return;
                    }
                }
			}

            // MRS - NAS 9.1 - Groups in RowLayout
			//this.SetResolvedGCsInternal(hash);
            this.SetResolvedGCsInternal(hash, group, setGroup);

			if (this.itemBeingDragged != null &&
				grid != null)
			{
                // MRS - NAS 9.1 - Groups in RowLayout
                // Surrounded the existing code in an 'if' block to see if we are dragging a column. 
                if (columnBeingDragged != null)
                {
                    // MRS - NAS 9.1 - Groups in RowLayout
                    //ColumnHeader[] colHeaders = new ColumnHeader[] { this.itemBeingDragged.Header };
                    ColumnHeader[] colHeaders = new ColumnHeader[] { columnBeingDragged.Header };
                    AfterColPosChangedEventArgs e = new AfterColPosChangedEventArgs(PosChanged.Moved, colHeaders);

                    //fire the BeforeColPosChanged event
                    grid.FireAfterColPosChanged(e);
                }
                else
                {
                    // MRS - NAS 9.1 - Groups in RowLayout
                    // Added this 'else' block for when we are dragging a group, not a groupumn. 
                    GroupHeader[] groupHeaders = new GroupHeader[] { groupBeingDragged.Header };
                    AfterGroupPosChangedEventArgs e = new AfterGroupPosChangedEventArgs(PosChanged.Moved, groupHeaders);

                    //fire the BeforeGroupPosChanged event
                    grid.FireAfterGroupPosChanged(e);
                }
			}
			
		}
		#endregion IGridBagLayoutDragManager.SetResolvedGCs

		#region IGridBagLayoutDragManager.GetVisibleLayoutItems
		/// <summary>
		/// Returns the layout items that are visible in the row-layout in an array.
		/// </summary>
		/// <returns>An array of visible ILayoutItems</returns>
		ILayoutItem[] IGridBagLayoutDragManager.GetVisibleLayoutItems()
		{
            
            // MD 8/10/07 - 7.3 Performance
            // Use generics
            //ArrayList columnList = new ArrayList( this.Band.Columns.Count );
            List<ILayoutItem> columnList = new List<ILayoutItem>(this.Band.Columns.Count);

            // MRS - NAS 9.1 - Groups in RowLayout
            if (this.Band.RowLayoutStyle == RowLayoutStyle.GroupLayout)
            {
                for (int i = 0; i < this.Band.Groups.Count; i++)
                {
                    UltraGridGroup group = this.Band.Groups[i];

                    if (group != this.itemBeingDragged &&
                        !group.IsVisibleInLayout)
                    {
                        continue;
                    }

                    columnList.Add(this.GetLayoutItem(group));
                }
            }

            for (int i = 0; i < this.Band.Columns.Count; i++)
            {
                UltraGridColumn column = this.Band.Columns[i];

                // MRS 4/29/05 - BR03503
                // Make sure the Item being dragged exists in the list, even
                // if it is not visible. 
                //				if ( ! column.IsVisibleInLayout )
                if (column != this.itemBeingDragged &&
                    !column.IsVisibleInLayout)
                {
                    continue;
                }

                columnList.Add(this.GetLayoutItem(column));
            }            

            // MD 8/10/07 - 7.3 Performance
            // Use generics
            //return (ILayoutItem[])columnList.ToArray( typeof( ILayoutItem ) );
            return columnList.ToArray();           
		}
		#endregion IGridBagLayoutDragManager.GetVisibleLayoutItems

		#region IGridBagLayoutDragManager.GetDragItemSpans
		/// <summary>
		/// Returns the resolved spanX and spanY of the ILayoutItem being dragged. 
		/// </summary>
		/// <remarks>This is used when drawing the rectangle when an item is dragged over a hole in a layout.</remarks>
		void IGridBagLayoutDragManager.GetDragItemSpans(out int spanX, out int spanY)
		{
			RowLayoutColumnInfo ci = this.GetDragItemRowLayoutColumnInfo();

            // MRS - NAS 9.1 - Groups in RowLayout
            //spanX = ci.SpanX;
            //spanY = ci.SpanY;
            spanX = ci.SpanXResolved;
            spanY = ci.SpanYResolved;
		}
		#endregion IGridBagLayoutDragManager.GetDragItemSpans

		#region IGridBagLayoutDragManager.GetLayoutItemDimensions
		/// <summary>
		/// For internal use. Returns dimensions of layout items. It contains entries for only the visible items.
		/// </summary>
		GridBagLayoutItemDimensionsCollection IGridBagLayoutDragManager.GetLayoutItemDimensions( ILayoutContainer layoutContainer, object containerContext )
		{
			// MD 1/22/09 - Groups in RowLayouts
			// This implementation has been moved to a virtual so it can be overridden in derived classes.
			return this.GetLayoutItemDimensions( layoutContainer, containerContext );
		}

		// MD 1/22/09 - Groups in RowLayouts
		/// <summary>
		/// For internal use. Returns dimensions of layout items. It contains entries for only the visible items.
		/// </summary>
		protected virtual GridBagLayoutItemDimensionsCollection GetLayoutItemDimensions(ILayoutContainer layoutContainer, object containerContext)
		{
			if (null == this.Band )
				return null;

			GridBagLayoutManager layoutManager = null;

			if (this.AreColumnHeadersInSeparateArea && 
				this.dragType != GridBagLayoutDragType.CellSpanResize )				 
			{
				layoutManager = this.Band.HeaderLayoutManager as GridBagLayoutManager;
			}
			else
				layoutManager = this.CellAreaLayoutManager;
									
			return layoutManager.GetLayoutItemDimensions(layoutContainer, null);
		}
		#endregion IGridBagLayoutDragManager.GetLayoutItemDimensions

		#region IGridBagLayoutDragManager.GetLayoutContainerRect
		/// <summary>
		/// Returns the valid rect of the container element.
		/// </summary>
		/// <remarks>This is needed because the rect in which the Layout items are positioned isn't neccessarily the same as the rect of the UIElement. In the grid, for example, this rect should not include the area within the row taken up by the Row Selectors when they are present.</remarks>
		Rectangle IGridBagLayoutDragManager.GetLayoutContainerRect(UIElement layoutContainerElement)
		{
			if ( layoutContainerElement is RowCellAreaUIElement )
			{
				// MRS 4/12/05 - BR03337 
				//return layoutContainerElement.Rect;
				return ((RowCellAreaUIElement)layoutContainerElement).GetRowLayoutAreaRect();
			}
				// MRS 4/12/05 - BR03337 
				//else if ( layoutContainerElement is BandHeadersUIElement || layoutContainerElement is CardLabelAreaUIElement )
			else if ( layoutContainerElement is BandHeadersUIElement )
			{
				// MRS 4/12/05 - BR03337 
				//				// Take into account the fact that leftmost headers in the band headers area are
				//				// extended to occupy the space over the row selectors.
				//				//
				//				Rectangle containerRect = layoutContainerElement.RectInsideBorders;
				//				if ( layoutContainerElement is BandHeadersUIElement )
				//				{
				//					containerRect.X += this.Band.RowSelectorWidthResolved;
				//					containerRect.Width -= this.Band.RowSelectorWidthResolved;
				//				}
				//				return containerRect;
				return ((BandHeadersUIElement)layoutContainerElement).GetRowLayoutAreaRect();
			}
				// MRS 4/12/05 - BR03337 
			else if ( layoutContainerElement is CardLabelAreaUIElement )
				return layoutContainerElement.Rect;

			Debug.Assert(false, "GetLayoutContainerRect returned Rectangle.Empty");
			return Rectangle.Empty;
			
		}
		#endregion IGridBagLayoutDragManager.GetLayoutContainerRect

		#region IGridBagLayoutDragManager.GetPreferredSize
		/// <summary>
		/// Returns the resolved preferred size of the item. 
		/// </summary>
		/// <remarks>The resolved PreferredSize may differ from the PreferredSize property when dragging multiple objects at once, like a cell and a header when the headers are with the cells in the grid.</remarks>
		Size IGridBagLayoutDragManager.GetPreferredSize(ILayoutItem layoutItem)
		{		
			Size itemPreferredSize = layoutItem.PreferredSize;			

			if ( ! this.AreColumnHeadersInSeparateArea )
			{   
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//UltraGridColumn column = this.GetColumn(layoutItem);      
                // MRS - NAS 9.1 - Groups in RowLayout
				//UltraGridColumn column = GridBagLayoutDragManager.GetColumn( layoutItem );
                IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = layoutItem as IProvideRowLayoutColumnInfo;                

				// Since we are draggin the cell and the label togather, adjust the
				// preferred size to take into account the corresponding label or the column.
				//
				LabelPosition labelPosition = iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.LabelPositionResolved;

                // MRS - NAS 9.1 - Groups in RowLayout
                //Size tmpSize = 
                //    layoutItem is UltraGridColumn
                //    ? ((ILayoutItem)iProvideRowLayoutColumnInfo.Header).PreferredSize
                //    : ((ILayoutItem)iProvideRowLayoutColumnInfo).PreferredSize;
                Size tmpSize = layoutItem is UltraGridColumn || layoutItem is GroupLayoutItemBase
                    ? iProvideRowLayoutColumnInfo.PreferredHeaderSize
                    : iProvideRowLayoutColumnInfo.PreferredSize;

				if ( LabelPosition.Left == labelPosition || LabelPosition.Right == labelPosition )
				{
					itemPreferredSize.Width += tmpSize.Width;

					// Also use the max of the heights of the cell and the label if they are 
					// side by side horizontally.
					//
					itemPreferredSize.Height = Math.Max( itemPreferredSize.Height, tmpSize.Height );
				}
				else if ( LabelPosition.Top == labelPosition || LabelPosition.Bottom == labelPosition )
				{
					itemPreferredSize.Height += tmpSize.Height;

					// Also use the max of the heights of the cell and the label if they are 
					// side by side vertically.
					//
					itemPreferredSize.Width = Math.Max( itemPreferredSize.Width, tmpSize.Width );
				}
			}

			return itemPreferredSize;
		}
		#endregion IGridBagLayoutDragManager.GetPreferredSize

		#region IGridBagLayoutDragManager.SetContraintsOfDragItem
		/// <summary>
		/// Sets the Consraint information on the item being dropped.
		/// </summary>
		/// <param name="newConstraint">A GridBagConstraint with the new settings to apply to the item being dragged</param>
		/// <remarks>This is called when an item is dropped onto a hole in the layout.</remarks>
		void IGridBagLayoutDragManager.SetConstraintsOfDragItem(GridBagConstraint newConstraint)
		{
			RowLayoutColumnInfo ci = this.GetDragItemRowLayoutColumnInfo();
			if (null == ci )
				return;

			ci.OriginX = newConstraint.OriginX;
			ci.OriginY = newConstraint.OriginY;
			ci.SpanX = newConstraint.SpanX;
			ci.SpanY = newConstraint.SpanY;
		}
		#endregion IGridBagLayoutDragManager.SetContraintsOfDragItem

		#region IGridBagLayoutDragManager.SetSpanGC
		/// <summary>
		/// Sets the Span and/or labelSpan properties on an item after a Span Resize.
		/// </summary>
		/// <param name="itemBeingDragged"></param>
		/// <param name="spanX"></param>
		/// <param name="spanY"></param>
		/// <param name="labelSpanX"></param>
		/// <param name="labelSpanY"></param>
		/// <param name="preferredLabelWidth"></param>
		/// <param name="preferredLabelHeight"></param>
		/// <param name="preferredCellWidth"></param>
		/// <param name="preferredCellHeight"></param>
		/// <returns></returns>
		/// <remarks>This is called after a Span Resize operation to set the new constraints on the item.</remarks>
		bool IGridBagLayoutDragManager.SetSpanGC(ILayoutItem itemBeingDragged, int spanX, int spanY, int labelSpanX, int labelSpanY, int preferredLabelWidth, int preferredLabelHeight, int preferredCellWidth, int preferredCellHeight)
		{
			ILayoutItem[] layoutItems = ((IGridBagLayoutDragManager)this).GetVisibleLayoutItems( );
			Hashtable hash = ((IGridBagLayoutDragManager)this).GetResolvedGCs();
			GridBagConstraint gc = (GridBagConstraint)hash[ itemBeingDragged ];
				
			RowLayoutColumnInfo ci = GridBagLayoutDragManager.GetRowLayoutColumnInfo(itemBeingDragged);

			// MRS 5/24/05 - BR04224
			// cache all the original RowLayoutColumnInfo information
			// in case the user cancels BeforeRowLayoutItemResize. 
			this.CacheRowLayoutInfo();			

			bool spanXChanged = false;
				
			// SSP 7/23/03 UWG2478
			// If no changes are made in the spans when span resizing, then don't set the resolved
			// gc's if we had started out with a clean row layout.
			//
			int originalSpanX = gc.SpanX;
			int originalSpanY = gc.SpanY;
			int originalLabelSpan = ci.LabelSpan;

			// MRS 5/24/05 - BR04224
			// Store the original size, so we can use it in the 
			// BeforeRowLayoutItemResize event.
			// ------------------------------------------------------
			Size oldSize = Size.Empty;			
			if ( itemBeingDragged is UltraGridColumn )
				oldSize = ((UltraGridColumn)itemBeingDragged).CellSizeResolved;
			else if ( itemBeingDragged is HeaderBase )
				oldSize = ((HeaderBase)itemBeingDragged).SizeResolved;
			else
				Debug.Assert( false, "Unknown type of row-layout item." );
			// ------------------------------------------------------
				
			if ( labelSpanX > 0 )
			{
				gc.SpanX += labelSpanX - ci.LabelSpan;
				ci.LabelSpan = labelSpanX;
				spanXChanged = true;
			}
			else if ( labelSpanY > 0 )
			{					
				gc.SpanY += labelSpanY - ci.LabelSpan;
				ci.LabelSpan = labelSpanY;
			}
			else if ( spanX > 0 )
			{
				gc.SpanX = spanX;
				spanXChanged = true;
			}
			else if ( spanY > 0 )
			{
				gc.SpanY = spanY;
			}
				
			// SSP 7/23/03 UWG2478
			// If no changes are made in the spans when span resizing, then don't set the resolved
			// gc's if we had started out with a clean row layout.
			//
			if ( originalSpanX == gc.SpanX && originalSpanY == gc.SpanY && originalLabelSpan == ci.LabelSpan )
			{
				// Set the drop valid to false so we don't pack the layout later on.
				// 
				return false;
			}
				
			if ( preferredLabelWidth >= 0 )
				ci.PreferredLabelSize = new Size( preferredLabelWidth, ci.PreferredLabelSize.Height );
				
			if ( preferredLabelHeight >= 0 )
				ci.PreferredLabelSize = new Size( ci.PreferredLabelSize.Width, preferredLabelHeight );
				
			if ( preferredCellWidth >= 0 )
				ci.PreferredCellSize = new Size( preferredCellWidth, ci.PreferredCellSize.Height );
				
			if ( preferredCellHeight >= 0 )
				ci.PreferredCellSize = new Size( ci.PreferredCellSize.Width, preferredCellHeight );
				
			// Ensure no items right of the resized item or below the resized item overlap with
			// the drag item after resizing its span.			
			//
			// MD 1/22/09 - Groups in RowLayouts
			// We don;t need a GridBagLayoutDragStrategy instance anymore.
			//GridBagLayoutDragStrategy gridBagLayoutDragStrategy = this.GetGridBagLayoutDragStrategy();
			//
			//if (gridBagLayoutDragStrategy != null)
			{
                // MRS - NAS 9.1 - Groups in RowLayout
                //gridBagLayoutDragStrategy.EnsureItemDoesntOverlap( itemBeingDragged, layoutItems, hash, true, true, ! spanXChanged );
                //gridBagLayoutDragStrategy.PackLayout( hash );                
                GridBagLayoutDragStrategy.EnsureItemDoesntOverlap(itemBeingDragged, layoutItems, hash, spanXChanged, null);
			}

			// MRS 5/24/05 - BR04224
			// Fire the BeforeRowLayoutItemResized event
			// -------------------------------------------------------
			// MD 1/22/09 - Groups in RowLayouts
			// The Control property has been renamed for clarity
			//UltraGrid grid = this.Control as UltraGrid;
			UltraGrid grid = this.Grid as UltraGrid;

			RowLayoutItemResizeType resizeType = RowLayoutItemResizeType.SizeChange;
			if ( null != grid )
			{				
				Size newPreferredSize = (this.dragType == GridBagLayoutDragType.CellSpanResize) ? ci.PreferredCellSize : ci.PreferredLabelSize;
				Size newSize = new Size(
					(newPreferredSize.Width != 0) ? newPreferredSize.Width : oldSize.Width, 
					(newPreferredSize.Height != 0) ? newPreferredSize.Height : oldSize.Height );					
								
				int oldSpan;
				int newSpan;				
				if ( originalSpanX != gc.SpanX )
				{
					resizeType = RowLayoutItemResizeType.SpanXChange;
					oldSpan = originalSpanX;
					newSpan = gc.SpanX;
				}
				else if ( originalSpanY != gc.SpanY )
				{
					resizeType = RowLayoutItemResizeType.SpanYChange;
					oldSpan = originalSpanY;
					newSpan = gc.SpanY;
				}
				else 
				{
					// If it's not an X or Y change, it must be LabelSpan ( originalLabelSpan != ci.LabelSpan )
					resizeType = RowLayoutItemResizeType.SpanYChange;
					oldSpan = originalLabelSpan;
					newSpan = ci.LabelSpan;
				}

				int originalNewSpan = newSpan;
				BeforeRowLayoutItemResizedEventArgs e = new BeforeRowLayoutItemResizedEventArgs(
					itemBeingDragged, oldSize, newSize, resizeType, oldSpan, newSpan );
				
				grid.FireEvent(GridEventIds.BeforeRowLayoutItemResized, e);

				try
				{
					if (e.Cancel)
					{
						this.RestoreRowLayoutInfo();
						return false;
					}
				}
				finally
				{
					this.cachedRowLayoutInfo = null;
				}

				if (e.NewSpan != originalNewSpan)
				{
					switch (resizeType)
					{
						case RowLayoutItemResizeType.SpanXChange:
							gc.SpanX = e.NewSpan;
							break;
						case RowLayoutItemResizeType.SpanYChange:
							gc.SpanY = e.NewSpan;
							break;
						case RowLayoutItemResizeType.LabelSpanChange:						
							ci.LabelSpan = e.NewSpan;
							break;
					}

					// MD 1/22/09 - Groups in RowLayouts
					// We don;t need a GridBagLayoutDragStrategy instance anymore.
					//if (gridBagLayoutDragStrategy != null)
					{
                        // MRS - NAS 9.1 - Groups in RowLayout
                        //gridBagLayoutDragStrategy.EnsureItemDoesntOverlap(itemBeingDragged, layoutItems, hash, true, true, !spanXChanged);
                        //gridBagLayoutDragStrategy.PackLayout(hash);
						GridBagLayoutDragStrategy.EnsureItemDoesntOverlap( itemBeingDragged, layoutItems, hash, spanXChanged, null);
					}	
				}
			}
			// -------------------------------------------------------

			// MRS 5/24/05 - BR04222
			// Call the helper method, so the ColPosChanged events
			// don't fire. 
			//((IGridBagLayoutDragManager)this).SetResolvedGCs( hash );
            // MRS - NAS 9.1 - Groups in RowLayout
			//this.SetResolvedGCsInternal( hash );            
            this.SetResolvedGCsInternal(hash, null, false);

			// MRS 5/24/05 - BR04224
			// Fire the AfterRowLayoutItemResized event
			// -------------------------------------------------------
			if ( null != grid )
			{				
				AfterRowLayoutItemResizedEventArgs e = new AfterRowLayoutItemResizedEventArgs(
					itemBeingDragged, resizeType );

				grid.FireEvent(GridEventIds.AfterRowLayoutItemResized, e);
			}
			// -------------------------------------------------------

			return true;
		}        
		#endregion IGridBagLayoutDragManager.SetSpanGC

		#region IGridBagLayoutDragManager.GetAllowMovingResolved
		GridBagLayoutAllowMoving IGridBagLayoutDragManager.GetAllowMovingResolved(ILayoutItem layoutItem)
		{
            // MRS - NAS 9.1 - Groups in RowLayout
            #region Old Code
            //// MD 7/26/07 - 7.3 Performance
            //// FxCop - Mark members as static
            ////UltraGridColumn column = this.GetColumn(layoutItem);
            //UltraGridColumn column = GridBagLayoutDragManager.GetColumn(layoutItem);

            //if (null == column ||
            //    null == column.Band)
            //{
            //    return GridBagLayoutAllowMoving.None;
            //}

            //return column.Band.AllowRowLayoutColMovingResolved;
            #endregion //Old Code
            //  
            // MRS 2/23/2009 - TFS14401
            //if (this.itemBeingDragged is GroupHeader
            if (this.itemBeingDragged.RowLayoutColumnInfo.ContextType == RowLayoutColumnInfoContext.Group)
                return this.Band.AllowRowLayoutGroupMovingResolved;
            else
                return this.Band.AllowRowLayoutColMovingResolved;
        }
		#endregion IGridBagLayoutDragManager.GetAllowMovingResolved

		#region IGridBagLayoutDragManager.GetAllowSpanSizingResolved
		GridBagLayoutAllowSpanSizing IGridBagLayoutDragManager.GetAllowSpanSizingResolved(ILayoutItem layoutItem)
		{
            // MRS - NAS 9.1 - Groups in RowLayout
            //
            #region Old Code
            //// MD 7/26/07 - 7.3 Performance
            //// FxCop - Mark members as static
            ////UltraGridColumn column = this.GetColumn(layoutItem);
            //UltraGridColumn column = GridBagLayoutDragManager.GetColumn( layoutItem );

            //if (null == column ||
            //    null == column.Band)
            //{
            //    return GridBagLayoutAllowSpanSizing.None;
            //}

            //if (layoutItem is UltraGridColumn)
            //    return column.Band.AllowRowLayoutCellSpanSizingResolved;				
            //else
            //    return column.Band.AllowRowLayoutLabelSpanSizingResolved;
            #endregion //Old Code
            //
            HeaderBase header = layoutItem as HeaderBase;
            if (header != null)
                return header.Band.AllowRowLayoutLabelSpanSizingResolved;

            IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = layoutItem as IProvideRowLayoutColumnInfo;
            if (iProvideRowLayoutColumnInfo != null)
                return iProvideRowLayoutColumnInfo.Band.AllowRowLayoutCellSpanSizingResolved;

            Debug.Fail("Unknown Layout Item");
            return GridBagLayoutAllowSpanSizing.None;
        }
		#endregion IGridBagLayoutDragManager.GetAllowSpanSizingResolved

		#region IGridBagLayoutDragManager.IsDropAreaValid

		// SSP 6/28/05 - NAS 5.3 Column Chooser
		// Added IsDropAreaValid method. When the mouse over a column chooser the drag
		// strategy should consider the location invalid for dropping. UltraGrid handles the 
		// dropping of columns over the column chooser.
		// 
		/// <summary>
		/// Returns true if the drop area is valid. Returns false if a location should be 
		/// considered invalid drop area. Note that returning true will cause the drag strategy 
		/// to use the default logic to determine whether a drop location is valid.
		/// </summary>
		/// <param name="point">In control coordinates.</param>
		/// <returns></returns>
		bool IGridBagLayoutDragManager.IsDropAreaValid( Point point )
		{
			// If the mouse is over a column chooser then don't show any drop indicators.
			// 
			// MD 1/22/09 - Groups in RowLayouts
			// The Control property has been renamed for clarity
			//UltraGridBase grid = this.Control;
			UltraGridBase grid = this.Grid;

			if ( null != grid && grid.IsOverColumnChooser( point, false ) )
				return false;

			return true;
		}

		#endregion // IGridBagLayoutDragManager.IsDropAreaValid

        // MRS - NAS 9.1 - Groups in RowLayout
        #region IGridBagLayoutDragManager.GetGroupFromPoint
        /// <summary>
        /// Returns the ILayoutGroup which contains the specified point.
        /// </summary>
        /// <param name="point">A point in control coordinates.</param>
        /// <returns>An ILayoutGroup which contains the specified point or null if the point is outside any group or if groups are not supported.</returns>
        ILayoutGroup IGridBagLayoutDragManager.GetGroupFromPoint(Point point)
        {
            UltraGridBand band = this.Band;
            if (band.RowLayoutStyle != RowLayoutStyle.GroupLayout)
                return null;

            UltraGridBase grid = band.Layout.Grid;
			UIElement mainElement = this.UIElement;
			ILayoutGroup group = grid.GetLayoutGroupFromPointInternal( point, mainElement );
            if (group != null)
                return group;

			UIElement element = grid.GetLayoutGroupContainerElement( point, mainElement );
            if (element != null)
            {
                UltraGridBand layoutBand = element.GetContext(typeof(UltraGridBand)) as UltraGridBand;
                if (layoutBand != null && layoutBand == band)
                    return layoutBand;
            }

            return null;
        }
        #endregion //IGridBagLayoutDragManager.GetGroupFromPoint

		// MD 2/17/09 - TFS14116
		// With the original fix for TFS13834, columns cannot be moved to new groups by default at design-time. The has been changed 
		// to allow for different behavior at run-time and design-time.
		#region IGridBagLayoutDragManager.ShouldAllowParentGroupChange

		bool IGridBagLayoutDragManager.ShouldAllowParentGroupChange( ILayoutChildItem item )
		{
			return this.ShouldAllowParentGroupChange( item );
		}

		/// <summary>
		/// Gets the value which indicates whether the parent group of the specified item can be changed.
		/// </summary>
		/// <param name="item">The item to test.</param>
		/// <returns>True if the parent group of the specified item can be changed; False otehrwise.</returns>
		protected virtual bool ShouldAllowParentGroupChange( ILayoutChildItem item )
		{
			UltraGridGroup group = null;
			UltraGridColumn column = null;
			HeaderBase header = item as HeaderBase;

			if ( header != null )
			{
				group = header.Group;
				column = header.Column;
			}
			else
			{
				GroupLayoutItemBase layoutItemBase = item as GroupLayoutItemBase;

				if ( layoutItemBase != null )
					group = layoutItemBase.Group;
				else
					column = item as UltraGridColumn;
			}

			if ( column != null )
			{
				switch ( column.Band.AllowColMovingResolved )
				{
					case AllowColMoving.WithinBand:
						return true;
					case AllowColMoving.NotAllowed:
					case AllowColMoving.WithinGroup:
						return false;
					case AllowColMoving.Default:
					default:
						Debug.Fail( "Unknown or invalid AllowColMovingResolved" );
						return false;
				}
			}

			if ( group != null )
				return group.AllowParentGroupChangeResolved;

			Debug.Fail( "Unknown item type." );
			return false;
		} 

		#endregion IGridBagLayoutDragManager.ShouldAllowParentGroupChange

        #endregion IGridBagLayoutDragManager implementation

        #region Private / Internal Methods

        // MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#region GetMousePointInGridCoords
		//private Point GetMousePointInGridCoords()
		//{			
		//    Control control = ((IGridBagLayoutDragManager)this).Control;			
		//    Point controlPoint = control.PointToClient(System.Windows.Forms.Control.MousePosition);
		//    return controlPoint;
		//}
		//#endregion GetMousePointInGridCoords

		#endregion Not Used

		#region AreColumnHeadersInSeparateArea

		internal bool AreColumnHeadersInSeparateArea
		{
			get
			{
				return GridBagLayoutDragManager.GetAreColumnHeadersInSeparateArea(this.itemBeingDragged);
			}
		}

		#endregion // AreColumnHeadersInSeparateArea

		#region GetMouseOffSet
		/// <summary>
		/// Returns a point describing the distance between the mouse and the upper left corner of the image.
		/// As the drag takes place, the DragWindow will be positioned relative to the mouse based on this value.		
		/// </summary> 
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal Point GetMouseOffSet(UIElement[] dragElements)
		// MD 1/22/09 - Groups in RowLayouts
		// This has been made public so it could be used in the design assembly.
		//internal static Point GetMouseOffSet( UIElement[] dragElements )
		public static Point GetMouseOffSet( UIElement[] dragElements )
		{		
			// SSP 6/29/05 - NAS 5.3 Column Chooser
			// If the column from a column chooser is being dragged then the element rect is
			// relative to the column chooser control and not the UltraGrid.
			// 
			//Point controlPoint = this.GetMousePointInGridCoords();
			if (dragElements == null || dragElements.Length == 0)
				return new Point(0,0);

			// SSP 6/29/05 - NAS 5.3 Column Chooser
			// If the column from a column chooser is being dragged then the element rect is
			// relative to the column chooser control and not the UltraGrid.
			//
			// ------------------------------------------------------------------------------
			return GridUtils.GetDragIndicatorOffset( dragElements );
			
			// ------------------------------------------------------------------------------
		}
		#endregion GetMouseOffSet

		#region GetDragElements

		/// <summary>
		/// Gets the UIElements which will be drawn on the dragIndicator.
		/// </summary>
        /// <param name="itemBeingDragged">The column whose elements will be returned.</param>
		/// <param name="isGroupByButton">Pass in true to get the UIElement for the GroupByButton</param>
		/// <returns></returns>
        // MRS - NAS 9.1 - Groups in RowLayout
		//internal UIElement[] GetDragElements(UltraGridColumn column, bool isGroupByButton)
		// MD 1/22/09 - Groups in RowLayouts
		// This has been made public and virtual so it could be used and overridden in the design assembly.
        //internal UIElement[] GetDragElements(IProvideRowLayoutColumnInfo itemBeingDragged, bool isGroupByButton)
		public virtual UIElement[] GetDragElements( IProvideRowLayoutColumnInfo itemBeingDragged, bool isGroupByButton )
		{
			// SSP 6/28/05
			// ControlForGridDisplay property never returns an UltraGridBase in the case of the UltraCombo.
			// Tha will cause the whole expression to evaluate to null. 
			// If (((IGridBagLayoutDragManager)this).Control is UltraGridBase than that's what we need. No
			// need to go to the ControlForGridDisplay.
			//
			//UltraGridBase grid = (((IGridBagLayoutDragManager)this).Control as UltraGridBase).ControlForGridDisplay as UltraGrid;
			// MD 1/22/09 - Groups in RowLayouts
			// Use the new Grid property. The Control is not guaranteed to be a grid.
			//UltraGridBase grid = ((IGridBagLayoutDragManager)this).Control as UltraGridBase;
			UltraGridBase grid = this.Grid;

			// MD 1/22/09 - Groups in RowLayouts
			// The element might not necessarily be the grid's element. The design assembly uses its own element.
			//UIElement controlElement = grid.DisplayLayout.UIElement;
			UIElement controlElement = this.UIElement;

			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// If a column from a column chooser is being dragged then get the bitmap from the 
			// column chooser instead.
			// 
			// --------------------------------------------------------------------------------
			UltraGridColumnChooser cc = grid.GetDraggingColumnChooser( );
			if ( null != cc )
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//UIElement elem = cc.GetHeaderUIElement( itemBeingDragged );
                Debug.Assert(itemBeingDragged is UltraGridColumn, "Item from the ColumnChooser is not a column; unexpected");
                UIElement elem = cc.GetHeaderUIElement((UltraGridColumn)itemBeingDragged);
				if ( null != elem )
					return new UIElement[] { elem };
			}
			// --------------------------------------------------------------------------------

			if (isGroupByButton)
			{
				UIElement groupByButtonElement = controlElement.GetDescendant(typeof(GroupByButtonUIElement), itemBeingDragged.Header);
				if (null == groupByButtonElement)
					return null;

				return new UIElement[] {groupByButtonElement};
			}

			// MD 1/22/09 - Groups in RowLayouts
			// Use the interface implementations for this so we use any overridden interface members.
			//Point pointInControlCoords = this.Control.PointToClient(System.Windows.Forms.Control.MousePosition);
			//Type containerType = GridBagLayoutDragManager.GetContainerElementType(itemBeingDragged, this.dragType);
			Point pointInControlCoords = ( (IGridBagLayoutDragManager)this ).Control.PointToClient( System.Windows.Forms.Control.MousePosition );
			Type containerType = ( (IGridBagLayoutDragManager)this ).GetContainerElementType();

			UIElement element = controlElement.ElementFromPoint(pointInControlCoords);

			UIElement rowLayoutContainerElement = null;
			if (null != element)
				rowLayoutContainerElement = element.GetAncestor(containerType);
			else
				rowLayoutContainerElement = controlElement.GetDescendant(containerType);

			UIElement containerElement = null;
			if (rowLayoutContainerElement != null)
				containerElement = rowLayoutContainerElement;
			else
				containerElement = controlElement;

			if (null != containerElement)
			{                
				UIElement headerElement = containerElement.GetDescendant(typeof(HeaderUIElement), itemBeingDragged.Header);                

				if (headerElement != null)
				{
                    // MRS - NAS 9.1 - Groups in RowLayout
                    bool areColumnHeadersInSeparateArea = GridBagLayoutDragManager.GetAreColumnHeadersInSeparateArea(itemBeingDragged);
                    GroupHeader groupHeader = headerElement.GetContext(typeof(GroupHeader)) as GroupHeader;
                    UltraGridGroup group = groupHeader != null ? groupHeader.Group : null;
                    if (group != null)
                        return GridBagLayoutDragManager.GetDragElements(group, containerElement, areColumnHeadersInSeparateArea, null);

                    // MRS - NAS 9.1 - Groups in RowLayout
					//if (GridBagLayoutDragManager.GetAreColumnHeadersInSeparateArea(itemBeingDragged))
                    if (areColumnHeadersInSeparateArea)
						return new UIElement[] {headerElement};
					else
					{
						UIElement cellElement = containerElement.GetDescendant(typeof(CellUIElementBase), itemBeingDragged);
                        if (null != cellElement)
                            return new UIElement[] { headerElement, cellElement };
                        else
                        {
                            // MRS 2/24/2009 - TFS14486
                            // Added this 'else' block
                            // If the cell element is not found, just return the header element. 
                            //
                            return new UIElement[] { headerElement };
                        }
					}
				}
			}
		
			return null;
		}       

		/// <summary>
		/// Gets the UIElements for a layout item at the specific point within the control.
		/// </summary>
		/// <param name="pointInControlCoords">A Point in Control coords</param>
		/// <returns>An Array of UIElement which represent the layoutItem at a particular point.</returns>	
		// MD 1/22/09 - Groups in RowLayouts
		// Made protectred and virtual so this could be overridden in derived classes.
		//internal UIElement[] GetDragElements(Point pointInControlCoords)
		protected virtual UIElement[] GetDragElements( Point pointInControlCoords )
		{
			// MD 1/22/09 - Groups in RowLayouts
			// The element might not necessarily be the grid's element. The design assembly uses its own element.
			//UltraGridBase grid = (((IGridBagLayoutDragManager)this).Control as UltraGridBase).ControlForGridDisplay as UltraGrid;
			//
			//UIElement element = grid.DisplayLayout.UIElement;
			UIElement element = this.UIElement;

			element = element.ElementFromPoint(pointInControlCoords);
			if (element == null)
				return null;
			
			GroupByButtonUIElement groupByButtonUIElement = element.GetAncestor(typeof(GroupByButtonUIElement)) as GroupByButtonUIElement;
			if (null != groupByButtonUIElement)
				return new UIElement[] {groupByButtonUIElement};

            // MRS - NAS 9.1 - Groups in RowLayout
			//UIElement headerElement = null;			
            UIElement headerElement = element.GetAncestor(typeof(HeaderUIElement));

            // MRS - NAS 9.1 - Groups in RowLayout  
            if (headerElement != null)
            {
                GroupHeader groupHeader = headerElement.GetContext(typeof(GroupHeader)) as GroupHeader;
                if (groupHeader != null)
                {
                    // If we are dragging onto a group header, we need to determine the drop location
                    // based on the header so that we can tell if we are dropping inside the group
                    // or outside the group. 
                    GridBagLayoutDragStrategy.DropLocation dropLocation = GridBagLayoutDragStrategy.GetDropLocation(pointInControlCoords, headerElement.Rect);
                    UltraGridGroup dropGroup = ((ILayoutChildItem)groupHeader).GetDropGroup(dropLocation) as UltraGridGroup;

                    // If we are dropping inside the group, then just use the group header. 
                    if (dropGroup == groupHeader.Group)
                        return new UIElement[] { headerElement };

                    // If we are dropping as a sibling of the group, then use the entire group and it's contents
                    // as the drop elements.                     
                    return GridBagLayoutDragManager.GetDragElements(groupHeader.Group, this.GetLayoutContainingElement(element), this.AreColumnHeadersInSeparateArea, null);                    
                }                
            }

			if (this.AreColumnHeadersInSeparateArea)
			{
                // MRS - NAS 9.1 - Groups in RowLayout
                // Moved this up to avoid duplicate code
				//headerElement = element.GetAncestor(typeof(HeaderUIElement));
				if (null == headerElement)
					return null;

				return new UIElement[] {headerElement};
			}
			else
			{
                // MRS - NAS 9.1 - Groups in RowLayout
                // Moved this up to avoid duplicate code
				//headerElement = element.GetAncestor(typeof(HeaderUIElement));
				UIElement cellElement = element.GetAncestor(typeof(CellUIElementBase));

				if (null == headerElement && null == cellElement)
					return null;
                
				UIElement layoutContainingElement = this.GetLayoutContainingElement(element);
				if (layoutContainingElement ==  null)
					return null;

				if (null != headerElement)
				{
					ColumnHeader columnHeader = headerElement.GetContext(typeof(ColumnHeader)) as ColumnHeader;

					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if (null == columnHeader || null == columnHeader.Column)
					//    return null;
					//
					//cellElement = layoutContainingElement.GetDescendant(typeof(CellUIElementBase), columnHeader.Column); 
					UltraGridColumn headerColumn = columnHeader != null 
						? columnHeader.Column 
						: null;

					if ( null == headerColumn )
						return null;

					cellElement = layoutContainingElement.GetDescendant( typeof( CellUIElementBase ), headerColumn ); 

					if (null == cellElement)
						return null;
				}
				else
				{					
					UltraGridCell cell = cellElement.GetContext(typeof(UltraGridCell)) as UltraGridCell;
					if (null == cell ||
						null == cell.Column ||
						null == cell.Column.Header)
					{
						return null;
					}			
					
					headerElement = layoutContainingElement.GetDescendant(typeof(HeaderUIElement), cell.Column.Header);
					if (null == headerElement)
						return null;
				}

				return new UIElement[] {headerElement,cellElement};
			}
		}
		#endregion GetDragElements		

		#region GetLayoutContainingElement
		private UIElement GetLayoutContainingElement(UIElement element)
		{
			if (null == element)
				return null;

			return element.GetAncestor(((IGridBagLayoutDragManager)this).GetContainerElementType());
		}
		#endregion GetLayoutContainingElement

		// MD 1/22/09 - Groups in RowLayouts
		// This code is no longer used anywhere in code.
		#region Not Used

		

		#endregion Not Used

		// MRS 5/19/05 - BR03851
		#region IsPointInValidLayoutArea
		/// <summary>
		/// Returns true if the specified point is within a UIElement that contains the RowLayout for the current drag in progress. 
		/// </summary>
		/// <param name="pointInControlCoords"></param>
		/// <returns></returns>
		internal bool IsPointInValidLayoutArea(Point pointInControlCoords)
		{
			if (null == this.Band ||
				null == this.Band.Layout)
			{
				return false;
			}
				
			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;
			if (null == grid)
				return false;

			// MD 1/22/09 - Groups in RowLayouts
			// The element might not necessarily be the grid's element. The design assembly uses its own element.
			//UIElement layoutElement = grid.DisplayLayout.UIElement.ElementFromPoint(pointInControlCoords);
			UIElement layoutElement = this.UIElement.ElementFromPoint( pointInControlCoords );

			if (null == layoutElement)
				return false;

			Type layoutElementType = ((IGridBagLayoutDragManager)this).GetContainerElementType();
			layoutElement = layoutElement.GetAncestor(layoutElementType);
			if (null == layoutElement)
				return false;
			
			object layoutElementContext = ((IGridBagLayoutDragManager)this).GetContainerElementContext();
            // MRS - NAS 9.1 - Groups in RowLayout
			//if (layoutElement.HasContext(layoutElementContext, false) )
            if (layoutElement.HasContext(layoutElementContext, true))
				return true;
		
			return false;
		}
		#endregion IsPointInValidLayoutArea

		// MRS 5/24/05 - BR04224
		#region CacheRowLayoutInfo
		private void CacheRowLayoutInfo ()
		{			
			this.cachedRowLayoutInfo = new Hashtable();

            // MRS - NAS 9.1 - Groups in RowLayout
            foreach (UltraGridGroup group in this.Band.Groups)
            {
                RowLayoutColumnInfo ci = new RowLayoutColumnInfo(group);
                ci.InitializeFrom(group.RowLayoutGroupInfo);
                this.cachedRowLayoutInfo[group] = ci;
            }

			foreach (UltraGridColumn column in this.Band.columns)
			{
				RowLayoutColumnInfo ci = new RowLayoutColumnInfo( column );
				ci.InitializeFrom(column.RowLayoutColumnInfo);
				this.cachedRowLayoutInfo[column] = ci;
			}
		}
		#endregion CacheRowLayoutInfo

		// MRS 5/24/05 - BR04224
		#region RestoreRowLayoutInfo
		private void RestoreRowLayoutInfo ()
		{
            // MRS - NAS 9.1 - Groups in RowLayout
			foreach (UltraGridGroup group in this.Band.Groups)
			{
                RowLayoutColumnInfo ci = this.cachedRowLayoutInfo[group] as RowLayoutColumnInfo;
				group.RowLayoutGroupInfo.InitializeFrom(ci);				
			}

            foreach (UltraGridColumn column in this.Band.columns)
            {
                RowLayoutColumnInfo ci = this.cachedRowLayoutInfo[column] as RowLayoutColumnInfo;
                column.RowLayoutColumnInfo.InitializeFrom(ci);
            }
		}
		#endregion RestoreRowLayoutInfo

		// MRS 5/24/05 - BR04222
		#region SetResolvedGCsInternal
        // MRS - NAS 9.1 - Groups in RowLayout
		//private void SetResolvedGCsInternal(Hashtable hash)
        private void SetResolvedGCsInternal(Hashtable hash, UltraGridGroup newGroup, bool setGroup)
		{
			foreach ( object key in hash.Keys )
			{
				GridBagConstraint gc = (GridBagConstraint)hash[ key ];
                RowLayoutColumnInfo ci = GridBagLayoutDragManager.GetRowLayoutColumnInfo(key as ILayoutItem);
				ci.Initialize( gc.OriginX, gc.OriginY, gc.SpanX, gc.SpanY );

                if (setGroup)
                {
                    // MRS 2/13/2009 - TFS13728
                    //if (ci.Context == this.itemBeingDragged)                    
                    if (this.itemBeingDragged.RowLayoutColumnInfo == ci)
                        ci.ParentGroup = newGroup;
                }
			}

			// MD 1/27/09 - Groups in RowLayouts
			// If the grid bag constraints have changed, the row layouts should be reverified.
			this.Band.DirtyRowLayoutCachedInfo();
		}
		#endregion SetResolvedGCsInternal

		#endregion Private / Internal Methods

		#region Static methods

		#region GetAreColumnHeadersInSeparateArea

        // MRS - NAS 9.1 - Groups in RowLayout
		//internal static bool GetAreColumnHeadersInSeparateArea(UltraGridColumn column)
        internal static bool GetAreColumnHeadersInSeparateArea(IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo)
		{		
			if (null == iProvideRowLayoutColumnInfo.Band)
				return true;
            
			return iProvideRowLayoutColumnInfo.Band.AreColumnHeadersInSeparateLayoutArea;			
		}
		#endregion GetAreColumnHeadersInSeparateArea

        // MRS 2/24/2009 - TFS14427
        #region GetContainerElement
        /// <summary>
        /// Returns the Layout container UIElement that contains the specified element. 
        /// </summary>        
        internal static UIElement GetContainerElement(UIElement element)
        {
            UIElement containerElement = element.GetAncestor(typeof(BandHeadersUIElement)) as BandHeadersUIElement;
            if (containerElement != null)
                return containerElement;

            containerElement = element.GetAncestor(typeof(CardLabelAreaUIElement)) as CardLabelAreaUIElement;
            if (containerElement != null)
                return containerElement;

            containerElement = element.GetAncestor(typeof(RowCellAreaUIElement)) as RowCellAreaUIElement;
            if (containerElement != null)
                return containerElement;

            containerElement = element as BandHeadersUIElement;
            if (containerElement != null)
                return containerElement;

            containerElement = element as CardLabelAreaUIElement;
            if (containerElement != null)
                return containerElement;

            containerElement = element as RowCellAreaUIElement;
            if (containerElement != null)
                return containerElement;

            return null;
        }
        #endregion //GetContainerElement

        #region GetContainerElementType

        /// <summary>
		/// Returns the type of UIElement that contains the layout. 
		/// </summary>
        // MRS - NAS 9.1 - Groups in RowLayout
		//internal static Type GetContainerElementType(UltraGridColumn column, GridBagLayoutDragType dragType)
        internal static Type GetContainerElementType(IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo, GridBagLayoutDragType dragType)
		{
			bool isCellDrag = dragType == GridBagLayoutDragType.CellSpanResize;
			
			if (GridBagLayoutDragManager.GetAreColumnHeadersInSeparateArea(iProvideRowLayoutColumnInfo))
			{
				if (null != iProvideRowLayoutColumnInfo.Band && iProvideRowLayoutColumnInfo.Band.CardView )
					if (!isCellDrag)
						return typeof(CardLabelAreaUIElement);									
					else
						// MRS 4/21/05 - BR03504
						//return typeof(CardAreaUIElement);
						return typeof(RowCellAreaUIElement);
				else
					if (!isCellDrag)
						return typeof(BandHeadersUIElement);					
					else
						return typeof(RowCellAreaUIElement);
			}
			else
				return typeof(RowCellAreaUIElement);
		}        
		#endregion GetContainerElementType


		#endregion Static methods

		#region Private / Internal Properties

		#region Band
		private UltraGridBand Band
		{
			get
			{
				return this.itemBeingDragged.Band;
			}
		}
		#endregion Band

		#region Grid
		/// <summary>
		/// The Grid to which the drag relates.
		/// </summary>
		// MD 1/22/09 - Groups in RowLayouts
		// This property has been renamed for clarity.
		//internal UltraGridBase Control
		protected internal virtual UltraGridBase Grid
		{
			get 
			{
				if (this.Band != null &&
					this.Band.Layout != null )
				{
					return this.Band.Layout.Grid;
				}

				return null;
			}
		}
		#endregion Grid

		#region GetVisibleColumns
		
		private UltraGridColumn[] GetVisibleColumns( )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList columnList = new ArrayList( this.Band.Columns.Count );
			List<UltraGridColumn> columnList = new List<UltraGridColumn>( this.Band.Columns.Count );

			for ( int i = 0; i < this.Band.Columns.Count; i++ )
			{
				UltraGridColumn column = this.Band.Columns[i];

				if ( ! column.IsVisibleInLayout )
					continue;

				columnList.Add( column );
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (UltraGridColumn[])columnList.ToArray( typeof( UltraGridColumn ) );
			return columnList.ToArray();
		}

		#endregion // GetVisibleColumns

		#region CellAreaLayoutManager

		private GridBagLayoutManager CellAreaLayoutManager
		{
			get
			{
				if ( null != this.Band )
					return ((Design.IGridDesignInfo)this.Band.Layout.Grid).GetCellAreaGridBagLayoutManager( this.Band );

				return null;
			}
		}

		#endregion // CellAreaLayoutManager

		#region GetRowLayoutColumnInfo
		internal static RowLayoutColumnInfo GetRowLayoutColumnInfo(ILayoutItem layoutItem)
		{
            // MRS - NAS 9.1 - Groups in RowLayout
            //
            #region Old Code
            //if (this.dragType == GridBagLayoutDragType.CellSpanResize)
            //{
            //    UltraGridColumn column = layoutItem as UltraGridColumn;
            //    if (null == column)
            //        return null;
            //    return column.RowLayoutColumnInfo;
            //}
            //else
            //{
            //    Infragistics.Win.UltraWinGrid.ColumnHeader header = layoutItem as Infragistics.Win.UltraWinGrid.ColumnHeader;

            //    // MD 8/3/07 - 7.3 Performance
            //    // Refactored - Prevent calling expensive getters multiple times
            //    //if (null == header || 
            //    //    null == header.Column)
            //    //{
            //    //    return null;
            //    //}
            //    //return header.Column.RowLayoutColumnInfo;
            //    UltraGridColumn headerColumn = header != null 
            //        ? header.Column 
            //        : null;

            //    if ( headerColumn == null )
            //        return null;

            //    return headerColumn.RowLayoutColumnInfo;
            //}
            #endregion //Old Code
            //
            return UltraGridBand.GetRowLayoutColumnInfo(layoutItem);
		}
		#endregion GetRowLayoutColumnInfo

		#region GetRowLayoutColumnInfo

		// MD 1/15/09 - Groups in RowLayout
		//internal RowLayoutColumnInfo GetDragItemRowLayoutColumnInfo()
		/// <summary>
		/// Gets the <see cref="RowLayoutColumnInfo"/> for the associated drag item.
		/// </summary>
		protected internal RowLayoutColumnInfo GetDragItemRowLayoutColumnInfo()
		{
            return GridBagLayoutDragManager.GetRowLayoutColumnInfo(((IGridBagLayoutDragManager)this).ItemBeingDragged);
		}
		#endregion GetRowLayoutColumnInfo

		#region GetLayoutItem
        // MRS - NAS 9.1 - Groups in RowLayout
		//private ILayoutItem GetLayoutItem(UltraGridColumn column)
		// MD 1/22/09 - Groups in RowLayouts
		// Made protectred and virtual so this could be overridden in derived classes.
        //internal ILayoutItem GetLayoutItem(IProvideRowLayoutColumnInfo rowLayoutColumnInfoProvider)
        /// <summary>
        /// For Interal Use Only. 
        /// </summary>
        /// <param name="rowLayoutColumnInfoProvider"></param>
        /// <returns></returns>
		protected internal virtual ILayoutItem GetLayoutItem( IProvideRowLayoutColumnInfo rowLayoutColumnInfoProvider )
		{
            // MRS - NAS 9.1 - Groups in RowLayout
            //
            #region Old Code
            //if (this.AreColumnHeadersInSeparateArea)
            //    if (this.dragType == GridBagLayoutDragType.CellSpanResize)
            //        return column as ILayoutItem;
            //    else
            //        return column.Header as ILayoutItem;
            //else
            //    if (this.dragType == GridBagLayoutDragType.CellSpanResize)
            //        return column as ILayoutItem;
            //    else
            //        return column.Header as ILayoutItem;
            #endregion //Old Code
            //
            return GridBagLayoutDragManager.GetLayoutItem(rowLayoutColumnInfoProvider, this.dragType);
        }

        // MRS - NAS 9.1 - Groups in RowLayout
        internal static ILayoutItem GetLayoutItem(IProvideRowLayoutColumnInfo rowLayoutColumnInfoProvider, GridBagLayoutDragType dragType)
        {
            // MRS - NAS 9.1 - Groups in RowLayout
            //
            #region Old Code
            //if (this.AreColumnHeadersInSeparateArea)
            //    if (this.dragType == GridBagLayoutDragType.CellSpanResize)
            //        return column as ILayoutItem;
            //    else
            //        return column.Header as ILayoutItem;
            //else
            //    if (this.dragType == GridBagLayoutDragType.CellSpanResize)
            //        return column as ILayoutItem;
            //    else
            //        return column.Header as ILayoutItem;
            #endregion //Old Code
            //
            if (dragType == GridBagLayoutDragType.CellSpanResize)
                return rowLayoutColumnInfoProvider.ContentLayoutItem as ILayoutItem;
            else
                return rowLayoutColumnInfoProvider.HeaderLayoutItem as ILayoutItem;
        }
		#endregion GetLayoutItem

		#region GetColumn
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private UltraGridColumn GetColumn(ILayoutItem layoutItem)
		private static UltraGridColumn GetColumn( ILayoutItem layoutItem )
		{
			UltraGridColumn column = layoutItem as UltraGridColumn;
			if  (null != column)
				return column;

			ColumnHeader header = layoutItem as ColumnHeader;
			if (null != header)
				return header.Column;

			RowLayoutColumnInfo rowLayoutColumnInfo = layoutItem as RowLayoutColumnInfo;
			if (null != rowLayoutColumnInfo)
				return rowLayoutColumnInfo.Column;

			return null;			
		}

		#endregion GetColumn

		#endregion Private / Internal Properties

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uninstantiated internal classes
		#region Not Used

		//        #region DragElementContext Class
		//#if DEBUG
		//        /// <summary>
		//        /// This class is used to store information about what is being dragged
		//        /// for use in the GridBagLayoutDragManager
		//        /// </summary>
		//#endif
		//        internal class DragElementContext
		//        {
		//            #region Private Members

		//            private UltraGridRow row = null;
		//            private UltraGridColumn column = null;
		//            private bool isGroupByButton = false;

		//            #endregion Private Members

		//            #region Constructor

		//            internal DragElementContext( UltraGridRow row, UltraGridColumn column, bool isGroupByButton )
		//            {
		//                this.row = row;
		//                this.column = column;
		//                this.isGroupByButton = isGroupByButton;
		//            }

		//            #endregion Constructor

		//            #region Internal Properties

		//            internal UltraGridRow Row
		//            {
		//                get { return this.row; }
		//            }

		//            internal UltraGridColumn Column
		//            {
		//                get { return this.column; }
		//            }

		//            internal bool IsGroupByButton
		//            {
		//                get { return this.isGroupByButton; }
		//            }


		//            #endregion Internal Properties
		//        }
		//        #endregion DragElementContext Class

		#endregion Not Used

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout

		#region DragType

        /// <summary>
        /// For Internal Use Only
        /// </summary>
		protected GridBagLayoutDragType DragType
		{
			get { return this.dragType; }
		} 

		#endregion DragType

        #region GetDragElements
        private static UIElement[] GetDragElements(UltraGridGroup group, UIElement containerElement, bool areColumnHeadersInSeparateArea, List<UIElement> dragElements)
        {
            if (dragElements == null)
                dragElements = new List<UIElement>();

            UIElement element = containerElement.GetDescendant(typeof(HeaderUIElement), group.Header);
            if (element != null &&
                dragElements.Contains(element) == false)
            {
                dragElements.Add(element);
            }

            UltraGridColumn[] columns = group.GetLayoutVisibleColumns();
            foreach (UltraGridColumn column in columns)
            {
                element = containerElement.GetDescendant(typeof(HeaderUIElement), column.Header);
                if (element != null &&
                    dragElements.Contains(element) == false)
                {
                    dragElements.Add(element);
                }

                if (areColumnHeadersInSeparateArea == false)
                {
                    element = containerElement.GetDescendant(typeof(CellUIElementBase), column);
                    if (element != null &&
                        dragElements.Contains(element) == false)
                    {
                        dragElements.Add(element);
                    }
                }
            }

            UltraGridGroup[] groups = group.GetLayoutVisibleGroups();
            foreach (UltraGridGroup childGroup in groups)
            {
                if (areColumnHeadersInSeparateArea == false)
                {
                    element = containerElement.GetDescendant(typeof(HeaderUIElement), childGroup.Header);
                    if (element != null &&
                        dragElements.Contains(element) == false)
                    {
                        dragElements.Add(element);
                    }
                }

                GridBagLayoutDragManager.GetDragElements(childGroup, containerElement, areColumnHeadersInSeparateArea, dragElements);
            }

            return dragElements.ToArray();
        }
        #endregion //GetDragElements

        #region GetVisibleGroups

        private UltraGridGroup[] GetVisibleGroups()
        {
            List<UltraGridGroup> groupList = new List<UltraGridGroup>(this.Band.Groups.Count);

            for (int i = 0; i < this.Band.Groups.Count; i++)
            {
                UltraGridGroup group = this.Band.Groups[i];

                if (!group.IsVisibleInLayout)
                    continue;

                groupList.Add(group);
            }

            return groupList.ToArray();
        }

        #endregion // GetVisibleGroups

		#region UIElement

        /// <summary>
        /// For Internal Use Only
        /// </summary>
		protected virtual UIElement UIElement
		{
			get
			{
				UltraGridBase grid = this.Grid;
				IUltraControlElement control = grid.ControlForGridDisplay as IUltraControlElement;

				if ( control != null )
					return control.MainUIElement;

				return grid.DisplayLayout.UIElement;
			}
		} 

		#endregion UIElement

        #endregion // NAS 9.1 - Groups in RowLayout
    }
}
