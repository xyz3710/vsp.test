#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

// SSP 6/14/05 - NAS 5.3 Column Chooser Feature
// Added ColumnChooserDialog class.
// 

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Dialog used for the column chooser functionality.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// This dialog embeds a UltraGridColumnChooser which can be accessed via the 
	/// <see cref="ColumnChooserDialog.ColumnChooserControl"/> property. <b>UltraGridColumnChooser</b>
	/// exposes various properties for associating it with an UltraGrid, a band among other things.
	/// See <see cref="UltraGridColumnChooser"/> for more information.
	/// </p>
	/// <seealso cref="UltraGridColumnChooser"/> <seealso cref="UltraGridBase.ShowColumnChooser()"/>
	/// </remarks>
	public class ColumnChooserDialog : System.Windows.Forms.Form
	{
		private DefaultableBoolean disposeOnClose = DefaultableBoolean.Default;

		private Infragistics.Win.UltraWinGrid.UltraGridColumnChooser ultraGridColumnChooser;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Constructor.
		/// </summary>
		public ColumnChooserDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.ClientSize = this.ultraGridColumnChooser.GetIdealSize( false );
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ultraGridColumnChooser = new Infragistics.Win.UltraWinGrid.UltraGridColumnChooser();
			this.SuspendLayout();
			// 
			// ultraGridColumnChooser
			// 
			this.ultraGridColumnChooser.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ultraGridColumnChooser.Name = "ultraGridColumnChooser";
			this.ultraGridColumnChooser.Size = new System.Drawing.Size(240, 254);
			this.ultraGridColumnChooser.SourceGrid = null;
			this.ultraGridColumnChooser.TabIndex = 0;
			// 
			// ColumnChooserDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(240, 254);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.ultraGridColumnChooser});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "ColumnChooserDialog";
			this.ShowInTaskbar = false;
			this.Closing += new System.ComponentModel.CancelEventHandler(this.ColumnChooserDialog_Closing);
			this.Load += new System.EventHandler(this.ColumnChooserDialog_Load);
			this.ResumeLayout(false);

		}
		#endregion

		#region ColumnChooserDialog_Closing

		bool inColumnChooserDialog_Closing =  false;

		private void ColumnChooserDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if ( this.inColumnChooserDialog_Closing )
				return;

			this.inColumnChooserDialog_Closing = true;
			try
			{
				if ( DefaultableBoolean.True == this.DisposeOnClose )
				{
					e.Cancel = true;
					this.Dispose( );
				}
				else if ( DefaultableBoolean.False == this.DisposeOnClose )
				{
					e.Cancel = true;
					this.Visible = false;
				}
			}
			finally
			{
				this.inColumnChooserDialog_Closing = false;
			}
		}

		#endregion // ColumnChooserDialog_Closing

		#region ColumnChooserDialog_Load

		private void ColumnChooserDialog_Load(object sender, System.EventArgs e)
		{
		}

		#endregion // ColumnChooserDialog_Load

		#region UltraGridColumnChooser

		/// <summary>
		/// Returns the associated column chooser control. This is the control that displays
		/// the columns.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>UltraGridColumnChooser</b> displays the list of columns. It exposes various
		/// properties for controlling appearance and behavior related aspects of the column 
		/// chooser. For example it can be used to control which columns are dispalyed.
		/// </p>
		/// <p class="body">
		/// The UltraGridColumnChooser occupies the entire client area of the form. It's 
		/// <b>Dock</b> is set to <b>Fill</b> and therefore resizing this dialog will also 
		/// automatically resize the control.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumnChooser"/>
		/// </remarks>
		public UltraGridColumnChooser ColumnChooserControl
		{
			get
			{
				return this.ultraGridColumnChooser;
			}
		}

		#endregion // UltraGridColumnChooser

		#region DisposeOnClose

		/// <summary>
		/// Specifies whether to dispose of the dialog when its closed. Default value is <b>Default</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// You can set this property to <b>False</b> to prevent the dialog from being disposed when it's closed. 
		/// Setting the property to <b>True</b> will cause it to be disposed in the Closing event handler. 
		/// Leaving this property to <b>Default</b> will perform the default behavior as documented in the
		/// <see cref="Form.Closing"/>.
		/// </p>
		/// </remarks>
		public DefaultableBoolean DisposeOnClose
		{
			get
			{
				return this.disposeOnClose;
			}
			set
			{
				this.disposeOnClose = value;
			}
		}

		#endregion // DisposeOnClose
	}
}
