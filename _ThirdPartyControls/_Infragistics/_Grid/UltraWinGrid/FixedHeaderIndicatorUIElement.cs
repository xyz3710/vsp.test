#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using System.Diagnostics;
using System.Drawing.Imaging;


namespace Infragistics.Win.UltraWinGrid
{
	#region FixedHeaderIndicatorUIElement Class

	// SSP 3/14/05 - NAS 5.2 Fixed Rows
	// Separated FixedHeaderIndicatorUIElement into FixedIndicatorUIElementBase and
	// FixedHeaderIndicatorUIElement so the FixedRowIndicatorUIElement can derive from
	// FixedIndicatorUIElementBase.
	//

	/// <summary>
	/// UIElement used for fixed header indicator.
	/// </summary>
	public class FixedHeaderIndicatorUIElement : FixedIndicatorUIElementBase
	{
		#region Private/Internal variables

		internal const int FIXED_HEADER_INDICATOR_BUTTON_WIDTH = 11;

		#endregion // Private/Internal variables

		#region InitAppearance

		// SSP 8/12/05 BR05338
		// Overrode InitAppearance so we resolve the header appearance first. Apparently the
		// ButtonUIElementBase.InitAppearance resolves the default appearances based on the
		// style of the button, overridding the parent element (which can be a header elem or
		// row selector elem) appearances.
		// 
        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags flags)
		{
			Infragistics.Win.UltraWinGrid.HeaderUIElement headerElem = (Infragistics.Win.UltraWinGrid.HeaderUIElement)this.GetAncestor( typeof( Infragistics.Win.UltraWinGrid.HeaderUIElement ) );
			if ( null != headerElem )
			{
				AppearancePropFlags HEADER_APP_FLAGS = AppearancePropFlags.ForeColor | AppearancePropFlags.ForeColorDisabled;
				AppearancePropFlags tmpFlags = HEADER_APP_FLAGS & flags;
				AppearancePropFlags origTmpFlags = tmpFlags;

				headerElem.InternalInitAppearance( ref appData, ref tmpFlags );

				flags ^= origTmpFlags ^ tmpFlags;
			}

			base.InitAppearance( ref appData, ref flags );
		}

		#endregion // InitAppearance

		#region FixedHeaderIndicatorUIElement

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		public FixedHeaderIndicatorUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // FixedHeaderIndicatorUIElement

		#region Header

		/// <summary>
		/// Gets the associated Header object.
		/// </summary>
		public HeaderBase Header
		{
			get
			{
				return (HeaderBase)this.GetContext( typeof( HeaderBase ), true );
			}
		}

		#endregion // Header

		#region ButtonClick

		/// <summary>
		/// Called when the button is clicked.
		/// </summary>
		/// <returns></returns>
		protected override bool ButtonClick( )
		{
			// If the button is disabled, then return without doing anything.
			//
			if ( ! this.Enabled )
				return false;

			HeaderBase header = this.Header;
			ColScrollRegion csr = (ColScrollRegion)this.GetContext( typeof( ColScrollRegion ) );

			Debug.Assert( null != header && null != csr, "No header or csr context !" );

			if ( null != header && null != csr )
			{
				// Exit the edit mode before proceeding.
				//
				if ( null != header.Band.Layout.ActiveCell 
					&& header.Band.Layout.ActiveCell.IsInEditMode )
				{
					// If exitting the edit mode is cancelled, then return without
					// proceeding.
					//
					if ( header.Band.Layout.ActiveCell.ExitEditMode( ) )
						return true;
				}

				header.OnFixedHeaderIndicatorClicked( csr );
			}

			return true;
		}

		#endregion // ButtonClick

		#region GetButtonImage

		internal override System.Drawing.Image GetButtonImage( out bool setTransparencyKey, out UltraGridLayout layout )
		{
			HeaderBase header = this.Header;
			layout = null != header ? header.Band.Layout : null;
			RowColRegionIntersectionUIElement rcrElem = (RowColRegionIntersectionUIElement)this.Parent.GetAncestor( typeof( RowColRegionIntersectionUIElement ) );
			Debug.Assert( null != layout && null != rcrElem );

			System.Drawing.Image image = null;
			setTransparencyKey = false;
			if ( null != layout && null != rcrElem )
			{
				if ( header.Band.IsHeaderFixed( rcrElem.ColScrollRegion, header ) )
				{
					image = layout.FixedHeaderOnImage;
					setTransparencyKey = ! layout.ShouldSerializeFixedHeaderOnImage( );
				}
				else
				{
					image = layout.FixedHeaderOffImage;
					setTransparencyKey = ! layout.ShouldSerializeFixedHeaderOffImage( );
				}
			}

			return image;
		}

		#endregion // GetButtonImage
	}

	#endregion // FixedHeaderIndicatorUIElement Class
}

