#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Diagnostics;
	using System.Collections;

	// SSP 2/19/03 - Row Layout Functionality
	//
	using Infragistics.Win.Layout;
	using System.Collections.Generic;

	#region RowCellAreaUIElementBase Class

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Split the RowCellAreaUIElement into RowCellAreaUIElementBase class and 
	// RowCellAreaUIElement class so the filter cell area ui element can derive from 
	// RowCellAreaUIElementBase .
	//
    /// <summary>
    /// The base class for RowCellAreaUIElement.
    /// </summary>
	public abstract class RowCellAreaUIElementBase : AdjustableUIElement
	{

		private Infragistics.Win.UltraWinGrid.UltraGridRow previousRow = null;

		private bool wasActiveRow = false;
		// SSP 6/17/02 UWG1203
		// Just like wasActiveRow var above, added wasSelectedRow var for
		// selected rows. When the row's selected status changes, we need
		// to circumvent the cell element's caching logic so they reposition 
		// their children.
		//
		private bool wasSelectedRow = false;

		private bool inFirstCardAreaRow	= false;
		private bool inLastCardAreaCol	= false;

		// SSP 7/12/05 BR04897
		// 
		internal bool cellElemsDirty = false;

		// SSP 5/1/03 - Optimizations
		// Cache the border style and cell spacing.
		//
        private UIElementBorderStyle borderStyle = UIElementBorderStyle.None;
		internal UIElementBorderStyle cellBorderStyle = UIElementBorderStyle.None;
		// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
		// Added rowBorderStyle member var.
		//
		private UIElementBorderStyle rowBorderStyle = UIElementBorderStyle.None;
		// SSP 7/24/03 - Optimizations
		// Cache the cell spacing as well.
		//
		internal int cellSpacingResolved = 0;

		// SSP 4/2/05 - Optimizations
		//
		private UltraGridRow row = null;

		// SSP 5/19/03 - Fixed headers
		//
		internal bool usingFixedHeaders = false;

		// SSP 11/30/04 - Merged Cell Feature
		// Added mergeableCellBordersBetweenRows flag so the merged cell element can make use of it.
		//
		internal bool mergeableCellBordersBetweenRows = false;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		// SSP 11/18/05
		// Made the constructor public from internal. UI elements should to be public.
		// 
		public RowCellAreaUIElementBase( UIElement parent ) : base( parent, false, true )
		{
		}

		/// <summary>
		/// Returns the UltraGridRow object associated with the cell. This property is not available at design time. This property is read-only at run time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Row</b> property of an object refers to a specific row in the grid as defined by an UltraGridRow object. You use the <b>Row</b> property to access the properties of a specified UltraGridRow object, or to return a reference to the UltraGridRow object that is associated with the current object.</p>
		/// <p class="body">An UltraGridRow object represents a single row in the grid that displays the data from a single record in the underlying data source. The UltraGridRow object is one mechanism used to manage the updating of data records either singly or in batches (the other is the UltraGridCell object). When the user is interacting with the grid, one UltraGridRow object is always the active row, and determines both the input focus of the grid and the position context of the data source to which the grid is bound.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				// SSP 4/2/05 - Optimizations
				// Added row member var.
				//
				if ( null != this.row )
					return this.row;

				// SSP 4/22/03 - Optimizations
				//
				RowUIElementBase rowElem = this.parentElement as RowUIElementBase;
				if ( null != rowElem )
					return rowElem.Row;

				return (Infragistics.Win.UltraWinGrid.UltraGridRow)this.Parent.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ) );
			}
		}


		// SSP 5/19/03 - Fixed headers
		// Overrode ClipChildren so we can clip the cells in the row cell area element.
		//
		/// <summary>
		/// Overridden. Returning true causes all drawing of this element's child elements to be
		/// expicitly clipped to the area inside this elements borders
		/// </summary>
		protected override bool ClipChildren 
		{ 
			get 
			{ 
				if ( this.usingFixedHeaders )
					return true;
				else
					return base.ClipChildren;
			} 
		}

		// MD 12/9/08 - 9.1 - Column Pinning Right
		// The base implementation seems to work here for multiple border styles and when columns are pinned to the 
		// right and left.
		#region Removed

		

		#endregion Removed


		/// <summary>
		/// Sets the active row to this row
		/// </summary>
		protected override void OnClick( )
		{
			// SSP 11/28/01 UWG508
			// If the grid does not have the focus, then do not change the active
			// row because there could be a situation where some other control
			// is forcibly retaining the focus (like masked edit because it has
			// invalid input ) and the user clicks on the row cell area, we shouln't
			// change the active row.
			//
			UltraGrid grid = this.Row.Layout.Grid as UltraGrid;
			if ( null != grid && !grid.HasFocus )
			{
				return;
			}

			// set the active row to this row when they click in the row
			// the row cell area
			//
			this.Row.Band.Layout.Grid.ActiveRow = this.Row;
		}

		// SSP 2/19/03 - Row Layout Functionality
		// Added PositionLayoutElements method that lays out cells and headers according to the
		// row layout manager.
		//
		private void PositionLayoutElements( bool dirtyCellElems )
		{
			UltraGridRow row = this.Row;
			UltraGridBand band = row.BandInternal;

			UIElementsCollection oldElems = this.ChildElements;
			this.childElementsCollection = null;
			Rectangle rowAreaRect = this.Rect;

			RowAutoPreviewUIElement rowAutoPreviewElem = null;
			int rowPreviewHeight = row.AutoPreviewEnabled ? row.AutoPreviewHeight : 0;
			if ( rowPreviewHeight > 0 )
			{
				// SSP 8/6/03
				// In row layout designer, we don't show the auto preview area because it does not
				// participate in the grid bag layout. So don't add the auto preview in row layout
				// designer.
				//
				DataAreaUIElement dataAreaElem = (DataAreaUIElement)this.Parent.GetAncestor( typeof( DataAreaUIElement ) );

				if ( null == dataAreaElem || ! dataAreaElem.RowLayoutDesignerElement )
				{
					Rectangle rowPreviewRect = new Rectangle(
						rowAreaRect.X,
						rowAreaRect.Bottom - rowPreviewHeight,
						rowAreaRect.Width,
						rowPreviewHeight );
				
					// Adjust the cell area to account for the row preview element.
					//
					rowAreaRect.Height -= rowPreviewHeight;

					// If the cells can merge their borders with the row preview element, then
					// merge cells' bottom borders with the row preview area's top borders.
					//
					if ( band.Layout.CanMergeAdjacentBorders( this.BorderStyle ) )
						rowAreaRect.Height++;

					rowAutoPreviewElem = (RowAutoPreviewUIElement)RowCellAreaUIElement.ExtractExistingElement( oldElems, typeof( RowAutoPreviewUIElement ), true );
					if ( null == rowAutoPreviewElem )
						rowAutoPreviewElem = new RowAutoPreviewUIElement( this, rowPreviewRect );
					rowAutoPreviewElem.Rect = rowPreviewRect;
				}
			}

			// Layout container code assumes that the container rect (rowAreaRect) passed in includes
			// the row selectors. So add the row selector extent here and then the layout container
			// code will substract it. The reason for is that the headers and summaries include the
			// row selector extent. So to be uniform for all of these three layout areas, we are
			// passing in the area that includes the row selectors.
			//
			int rowSelectorWidth = band.RowSelectorExtent;
			if ( rowSelectorWidth > 0 )
			{
				// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
				// Use the borderStyle member var.
				//
				//if ( band.Layout.CanMergeAdjacentBorders( band.BorderStyleRowResolved ) )
				if ( band.Layout.CanMergeAdjacentBorders( this.borderStyle ) )
					rowSelectorWidth--;

				rowAreaRect.X -= rowSelectorWidth;
				rowAreaRect.Width += rowSelectorWidth;
			}

			// SSP 7/7/05 - NAS 5.3 Empty Rows
			// Empty rows may have extended their row cell area left depending on the Style
			// property setting. Take that into account here. The row layout container must be
			// passed in the same rect as the regular rows if the columns are to be properly 
			// aligned.
			// 
			// ----------------------------------------------------------------------------------
			if ( row.IsEmptyRow )
			{
				UltraGridEmptyRow emptyRow = (UltraGridEmptyRow)row;
				int delta = emptyRow.BeginCellsAt;

				switch ( emptyRow.EmptyRowSettings.Style )
				{
					case EmptyRowStyle.PrefixWithEmptyCell:
					case EmptyRowStyle.ExtendFirstCell:
						delta = band.PreRowAreaExtent + rowSelectorWidth + emptyRow.GroupByRowsHierarchyIndent;
						break;
					case EmptyRowStyle.HideRowSelector:
						delta = rowSelectorWidth;
						break;
				}

				rowAreaRect.X += delta;
				rowAreaRect.Width -= delta;
			}
			// ----------------------------------------------------------------------------------

			// SSP 5/23/03 - Fixed headers
			// Take into account the fact the row cell area element could have been shrinked in
			// the fixed headers mode. However we still need to pass in the whole row cell area
			// rect to the layout manager and not the shrunk rect.
			//
			if ( this.usingFixedHeaders )
			{
				ColScrollRegion csr = (ColScrollRegion)this.GetContext( typeof( ColScrollRegion ), true );

				if ( null != csr )
				{
					int delta = band.GetFixedHeaders_OriginDelta( csr );
					rowAreaRect.X -= delta;
					rowAreaRect.Width += delta;
				}
			}

			LayoutContainerCellArea lc = band.GetCachedLayoutContainerCellArea( );
			lc.Initialize( band, rowAreaRect, this, oldElems, this.previousRow == row, dirtyCellElems );

			// SSP 5/12/05 - NAS 5.2 Filter Row
			// Filter cells can have different sizes (for example if the FilterOperatorLocation
			// is AboveOperand). Therefore get the row specific layout manager.
			//
			//band.RowLayoutManager.LayoutContainer( lc, null );
			row.LayoutManager.LayoutContainer( lc, null );

			// Add the row auto preview element if we have it.
			//
			if ( null != rowAutoPreviewElem )
				this.ChildElements.Add( rowAutoPreviewElem );

			// SSP 3/30/05 - NAS 5.2
			// Added ability to display prompts in add-rows. Also the new filter row feature displays
			// prompts in filter rows as well. Added AddRowPromptElementHelper method.
			//
			RowPromptUIElement.AddRowPromptElementHelper( row, this, oldElems, null );

			// SSP 5/17/05 BR03705
			// Dispose of the elements that weren't reused.
			//
			if ( null != oldElems )
				oldElems.DisposeElements( );
		}

		#region AddFixedCellSeparatorUIElement

		// SSP 7/17/03 - Fixed headers
		// Added AddFixedCellSeparatorUIElement method.
		//
		private void AddFixedCellSeparatorUIElement( UIElementsCollection oldElems, int fixedHeadersAreaRight )
		{
			RowCellAreaUIElementBase.AddFixedCellSeparatorUIElement( this, this.Row.Band, oldElems, fixedHeadersAreaRight );
		}

		// SSP 5/6/05 - NAS 5.2 Extension of Summaries Functionality
		// Added static overload of AddFixedCellSeparatorUIElement.
		//
		internal static void AddFixedCellSeparatorUIElement( 
			UIElement parent, UltraGridBand band,
			UIElementsCollection oldElems, int fixedHeadersAreaRight )
		{
			FixedCellSeparatorUIElement separatorElem = 
				(FixedCellSeparatorUIElement)RowCellAreaUIElement.ExtractExistingElement( 
											oldElems, typeof( FixedCellSeparatorUIElement ), true );
									
			if ( null == separatorElem )
				separatorElem = new FixedCellSeparatorUIElement( parent );

			// SSP 8/29/03 UWG2586
			//
            separatorElem.Initialize( band );

			// Note how we are using fixedHeadersAreaRight - 1. This is to draw the borders
			// right on top of right border of the last fixed header.
			//
			Rectangle parentRect = parent.Rect;
			separatorElem.Rect = new Rectangle( fixedHeadersAreaRight - 1, parentRect.Y, 1, parentRect.Height );

			parent.ChildElements.Add( separatorElem );
		}

		#endregion // AddFixedCellSeparatorUIElement

		#region ChildElementArrayCapacity

		// SSP 8/12/03 - Optimizations
		// Overrode ChildElementArrayCapacity property.
		//
		private int childElementArrayCapacity = 5;
		/// <summary>
		/// Returns the expected number of child elements for this element
		/// </summary>
		protected override int ChildElementArrayCapacity
		{ 
			get 
			{ 
				return this.childElementArrayCapacity;
			} 
		}

		#endregion ChildElementArrayCapacity

		#region InitCachedInfo

		// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
		// Added InitCachedInfo method.
		//
		internal void InitCachedInfo( )
		{
			// SSP 4/2/05 - Optimizations
			// Added row member var. Reget the row.
			//
			this.row = null;
			UltraGridRow row = this.Row;
			this.row = row;
			if ( null == row )
				return;

			UltraGridBand band  = row.BandInternal;

			// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
			// Added rowBorderStyle member var.
			//
			this.rowBorderStyle = row.BorderStyleResolved;

			// SSP 5/1/03 - Optimizations
			// Cache the borderStyle.
			//
			this.borderStyle = row.IsCard ? UIElementBorderStyle.None 
				// SSP 11/11/03 Add Row Feature
				// 
				//: band.BorderStyleRowResolved;
				// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
				// Added rowBorderStyle member var which is initialized above.
				//
				//: ( ! row.IsTemplateAddRow ? band.BorderStyleRowResolved : band.BorderStyleTemplateAddRowResolved );
				: this.rowBorderStyle;

			this.cellBorderStyle = row.BorderStyleCellResolved;

			// SSP 7/24/03 - Optimizations
			// Cache the cell spacing as well.
			//
			this.cellSpacingResolved = band.CellSpacingResolved;

			// SSP 5/19/03 - Fixed headers
			// Set the flag indicating whether we are using fixed headers. We do this for optimization
			// purposes so rather than having to access this property through the band multiple times
			// we can just store it as a flag.
			//
			this.usingFixedHeaders = band.UseFixedHeaders;
		}

		#endregion // InitCachedInfo

		#region CreateCellUIElement

		// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
		//
		/// <summary>
		/// Method for creating CellUIElementBase derived class instances.
		/// </summary>
		/// <returns></returns>
		protected abstract CellUIElementBase CreateCellUIElement( UIElementsCollection oldElems );

		#endregion // CreateCellUIElement

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
			// Added InitCachedInfo method and moved the code there.
			//
			this.InitCachedInfo( );

			Infragistics.Win.UltraWinGrid.UltraGridRow row = this.Row;

			if( row == null || row.BandInternal == null )
				return;

			UltraGridBand band  = row.BandInternal;
			UltraGridLayout layout = band.Layout;

			// SSP 7/12/05 BR04897
			// 
			bool cellElemsDirty = this.cellElemsDirty;
			this.cellElemsDirty = false;

			// SSP 2/19/03 - Row Layout Functionality
			//
			// ------------------------------------------------------------------------------------
			if ( band.UseRowLayoutResolved )
			{
				// SSP 4/15/05 - Optimizations
				//
				//bool isRowActive = row.IsActiveRow;
				bool isRowActive = row.IsActiveRowInternal;
				bool isRowSelected = row.Selected;

				if ( ! row.IsCard )
					this.PositionLayoutElements( 
						// SSP 7/12/05 BR04897
						// Also check the new cellElemsDirty flag.
						// 
						cellElemsDirty ||
						this.previousRow != row ||	
						this.wasActiveRow != isRowActive ||	this.wasSelectedRow != isRowSelected );
				else
					this.PositionChildElementsForCard( );

				this.previousRow = row;
				this.wasActiveRow = isRowActive;
				this.wasSelectedRow = isRowSelected;

				return;
			}
			// ------------------------------------------------------------------------------------

			// SSP 7/24/03 - Optimizations
			//
			//if (this.Row.IsCard)
			if ( row.IsCard )
			{
				this.PositionChildElementsForCard();
				return;
			}

			ColScrollRegion colRegion = ((RowColRegionIntersectionUIElement)this.GetAncestor( typeof ( RowColRegionIntersectionUIElement ) ) ).ColScrollRegion;
			
			if ( colRegion == null ) 
				return;

			UIElementsCollection oldCellElements = this.ChildElements;

			this.childElementsCollection = null;

			// SSP 8/12/03 - Optimizations
			// Overrode ChildElementArrayCapacity property.
			//
			// ------------------------------------------------------------
			this.childElementArrayCapacity = 5 + oldCellElements.Count;
			// It shouldn't really be greater than 100
			if ( this.childElementArrayCapacity > 100 )
				this.childElementArrayCapacity = 100;
			// ------------------------------------------------------------

			// SSP 4/11/05
			//
			//Rectangle       rcWork = new System.Drawing.Rectangle( 0, 0, 0, 0 );
			Rectangle       rcWork = Rectangle.Empty;
			Rectangle       rcWork2;
			bool            foundBand = false;

			// SSP 3/19/03 - Row Layout Functionality
			// Moved this in the beggining of the method.
			//
			

			int rowSelectorWidth       = band.RowSelectorExtent;
			// SSP 1/26/04 UWG2707 UWG2617
			// preRowSelectorWidth is not being used anywhere. Commented it out.
			//
			//int preRowSelectorWidth    = band.PreRowAreaExtent;

			// JJD 10/30/01
			// Initialize the sameRow flag based on the row context set the 
			// last time this method was called. This is passed into the
			// AddElementHelper method to optimize looking for matching cells.
			// If we are on a different row entirely we can stop looking.
			// 
			bool sameRow = ( row == this.previousRow );

			// JJD 10/3/0/01
			// Keep track of the row so that if the parent element's
			// context changes we know to re-initialize the cell elements
			//
			this.previousRow = row;

			// SSP 11/13/01 UWG599
			// If the row was previously a non-active row and now it's an active row
			// or vice versa, then make sure the cell uielement is dirited so any
			// active row appearance settings on the cell will take effect.
			//
			// SSP 4/11/05 - Optimizations
			//
			//bool isActiveRow = row.IsActiveRow;
			bool isActiveRow = row.IsActiveRowInternal;
			bool activeRowStatusChanged = this.wasActiveRow != isActiveRow
				// SSP 7/12/05 BR04897
				// Also check the new cellElemsDirty flag.
				// 
				|| cellElemsDirty;

			// SSP 6/17/02 UWG1203
			// Also do the same for selected row status change.
			//
			if ( !activeRowStatusChanged )
			{
				if ( ( band.HasOverride && band.Override.HasSelectedRowAppearance ) ||
					( null != layout && layout.HasOverride && layout.Override.HasSelectedRowAppearance ) )
					activeRowStatusChanged = this.wasSelectedRow != row.Selected;
			}

			this.wasActiveRow = isActiveRow;
			// SSP 6/17/02 UWG1203
			// Do the same of selected row
			//
			this.wasSelectedRow = row.Selected;

			// JJD 1/15/02
			// Get the rect for this element that is clipped to all of its
			// parent elements
			//
			Rectangle clippedRect = this.ClipRect;

            // JJD 4/11/01 - ULT1953
            // If cellspacing is zero and cell borders can be merged set a flag
            // so we can adjust their widths to overlap each other
            //
			// SSP 7/24/03 - Optimizations
			// Use the cached cell spacing resolved.
			//
            //bool overlapCellBorders = ( band.CellSpacingResolved == 0   &&
			bool overlapCellBorders = ( this.cellSpacingResolved == 0   &&
				// SSP 6/5/03 - Optimizations
				// 
                //row.Layout.CanMergeAdjacentBorders( band.BorderStyleCellResolved ) );
				layout.CanMergeAdjacentBorders( this.cellBorderStyle ) );

			// JJD 1/24/02
			// Added flag to cover the case where there are no row borders and
			// the cell border can overlap without any spacing between them
			//
			// SSP 6/5/03 - Optimizations
			//
			
			// SSP 11/30/04 - Merged Cell Feature
			// Added mergeableCellBordersBetweenRows member variable so the merged cell element 
			// can make use of it.
			//
			//bool mergeableCellBordersBetweenRows = 
			this.mergeableCellBordersBetweenRows = 
				( this.borderStyle == UIElementBorderStyle.None &&
				// SSP 7/24/03 - Optimizations
				// Use the cached cell spacing resolved.
				//
				//row.Band.CellSpacingResolved < 1 &&
				this.cellSpacingResolved < 1 &&
				layout.CanMergeAdjacentBorders( this.cellBorderStyle ) );

			// SSP 5/19/03 - Fixed headers
			//
			// MD 12/9/08 - 9.1 - Column Pinning Right
			// Renamed for clarity
			//int fixedHeadersAreaRight = 0;
			int leftFixedHeadersAreaRight = 0;

			CellUIElementBase cellElem = null;
			bool isHeaderFixed;

			// SSP 7/17/03 - Fixed headers
			// Add a separator separating fixed and non-fixed headers.
			//
			// MD 12/24/08 - TFS11569
			// Renamed for clarity
			//bool fixedHeadersSeparatorAdded = false;
			bool leftFixedHeadersSeparatorAdded = false;

			// SSP 7/17/03 - Optimizations
			//
			Rectangle thisRect = this.Rect;

			// Iterate over the visible headers and create an element for each
			//
			// SSP 6/5/03 - Optimizations
			//
			//for ( int index = 0; index < colRegion.VisibleHeaders.Count; index++ )
			//{
			//	VisibleHeader vh = colRegion.VisibleHeaders[index]; 
			VisibleHeadersCollection visibleHeaders = colRegion.VisibleHeaders;
			int count = visibleHeaders.Count;
			for ( int index = 0; index < count; index++ )
			{
				VisibleHeader vh = visibleHeaders[index]; 
				HeaderBase header = vh.Header;

				if ( header == null )
					continue;

				// If the vh's band doesn't match our band then bypass this header
				//
				if ( header.Band != band )
				{
					// if we have already processed our band's headers then stop iterating
					//
					if ( foundBand )
						break;

					continue;
				}

				foundBand      = true;
        
				// SSP 7/17/03 - Optimizations
				//
				//rcWork.Y = this.Rect.Top;
				//rcWork.Height = this.Rect.Height;
				rcWork.Y = thisRect.Top;
				rcWork.Height = thisRect.Height;
				rcWork.X = header.GetOnScreenOrigin( colRegion, false );
				rcWork.Width = header.Extent - header.RowSelectorExtent;

				// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
				// In AutoFitStyle mode of ExtendLastColumn the band extents are adjusted to
				// occupy the extra visible space however header extents (column widths) aren't.
				// We need to do them here. If the column or group is the last in the row cell 
				// area then extend it so it occupies the rest of the space in the row cell area.
				//
				// ------------------------------------------------------------------------------
				if ( header.LastItem && layout.AutoFitExtendLastColumn )
				{
					if ( rcWork.Right < thisRect.Right )
						rcWork.Width += thisRect.Right - rcWork.Right;
				}
				// ------------------------------------------------------------------------------

				if ( row.AutoPreviewEnabled )
					rcWork.Height -= row.AutoPreviewHeight;

				// if this is a group we need to add all of its columns as well
				//
				if ( header.IsGroup )
				{
					// MD 1/21/09 - Groups in RowLayouts
					// Use the resolved column because in GroupLayout style, it will return a different enumarator which returns the columns 
					// actually visible in the group
					//GroupColumnsCollection colList = header.Group.Columns;
					IEnumerable<UltraGridColumn> colList = header.Group.ColumnsResolved;

					if ( colList != null )
					{
						// MD 1/21/09 - Groups in RowLayouts
						// The colList is now an IEnumarable<UltraGridColumn> , so we must use a foreach.
						//UltraGridColumn  column;
						//
						//// Iterate over the columns in this group and create an element for each
						////
						//for ( int i = 0; i < colList.Count; i++ )
						//{
						//    column = colList[i];
						// Iterate over the columns in this group and create an element for each
						//
						foreach ( UltraGridColumn column in colList )
						{
							// SSP 5/14/04 UWG3259
							// Skip hidden columns.
							//
                            // MRS 2/23/2009 - Found while fixing TFS14354
							//if ( null == column || column.Hidden )
                            if (null == column || column.HiddenResolved)
								continue;

							// SSP 5/14/03 - Hiding Individual Cells Feature
							// Added Hidden property off the cell to allow the user to be able to hide a cell.
							//
							// --------------------------------------------------------------------------------
							if ( LayoutContainerHelper.IsCellHidden( row, column ) )
							{
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Avoid unused private fields
								//// Set the previous cell's nextCellHidden to true to indicate that the next
								//// cell is hidden so not to clear the right border flag. See 
								//// CellUIEelement.CalcBorderSides for more info.
								////
								//if ( null != this.childElementsCollection && this.childElementsCollection.Count > 0 )
								//{
								//    cellElem = this.childElementsCollection[ this.childElementsCollection.Count - 1 ] as CellUIElementBase;
								//    if ( null != cellElem )
								//    {
								//        if ( cellElem.Column.Level == column.Level )
								//            cellElem.nextCellHidden = true;
								//    }
								//}

								continue;
							}
							// --------------------------------------------------------------------------------

							rcWork2         =  rcWork;
							rcWork2.X		+= column.RelativeOrigin;
							rcWork2.Width	-= column.RelativeOrigin;

							// SSP 10/20/03 UWG2707 UWG2617
							// Commented out the original code and added the new code below. Below
							// code doesn't make any sense. Search for all the changes with above
							// bug numbers to see more info.
							//
							// ------------------------------------------------------------------------
							
							if ( vh.Header.FirstItem && ! column.FirstItem )
								rcWork2.X -= rowSelectorWidth;
							// ------------------------------------------------------------------------

							// Use IsLastItemInLevel method instead of IsLastItem 
							//
							// JJD 1/15/02
							// For the last item in level use rcWork.Right
							//
							if ( column.LastItemInLevel )
								rcWork2.Width = rcWork.Right - rcWork2.Left;
							else
								rcWork2.Width = column.GetActualWidth();

                            // JJD 4/11/01 - ULT1953
                            // If cellspacing is zero and cell borders can be merged 
                            // and this is not the first cell in the row then
                            // adjust the left of the rect so it overlaps with the
                            // cell to its left
                            //
                            if ( overlapCellBorders && 
								( !vh.Header.FirstItem || !column.FirstItemInLevel ) )
                            {
                                rcWork2.X--;
                                rcWork2.Width++;
                            }

							// SSP 7/17/03 - Fixed headers
							// If the column header is completely scrolled underneath the fixed
							// headers, then don't add it.
							//
							// ----------------------------------------------------------------------
							isHeaderFixed = false;
							if ( usingFixedHeaders )
							{
								isHeaderFixed = band.IsHeaderFixed( colRegion, column.Header );
								if ( ! isHeaderFixed && rcWork2.Right <= leftFixedHeadersAreaRight )
									continue;
							}
							// ----------------------------------------------------------------------

							// add the column's element object
							//
							UIElement element = AddElementHelper( oldCellElements,
								typeof(CellUIElementBase),
								rcWork2, clippedRect, row, column, ref sameRow, 
								// SSP 11/13/01 UWG599
								// added activeRowStatusChanged
								activeRowStatusChanged,
								// JJD 1/24/02
								// Added flag to cover the case where there are no row borders and
								// the cell border can overlap without any spacing between them
								//
								// SSP 11/30/04 - Merged Cell Feature
								// Added mergeableCellBordersBetweenRows member variable so there is
								// no need to pass it around to AddElementHelper method.
								//
								//mergeableCellBordersBetweenRows, 
								// SSP 2/26/03 - Row Layout Functionality
								// Pass in false for the new rowLayoutMode parameter.
								//
								false
								);

							// SSP 5/19/03 - Fixed headers
							// 
							// ------------------------------------------------------------------------------------
							cellElem = element as CellUIElementBase;
							if ( this.usingFixedHeaders && null != cellElem )
							{
								// Add a separator element separating fixed and non-fixed cells.
								// 
								//if ( ( ! isHeaderFixed || 1 + index == count ) && ! fixedHeadersSeparatorAdded )
								if ( ! isHeaderFixed && ! leftFixedHeadersSeparatorAdded )
								{
									this.AddFixedCellSeparatorUIElement( oldCellElements, leftFixedHeadersAreaRight );
									leftFixedHeadersSeparatorAdded = true;
								}

								Rectangle clipSelfRect = Rectangle.Empty;
							
								// SSP 5/3/05 - NAS 5.2
								// AddElementHelper call above could modify the rect so reget the actual cell
								// element's rect.
								//
                                rcWork2 = element.Rect;

								if ( rcWork2.Left < leftFixedHeadersAreaRight && ! isHeaderFixed )
								{
									clipSelfRect = rcWork2;
									clipSelfRect.Width = clipSelfRect.Right - leftFixedHeadersAreaRight; 
									clipSelfRect.X = leftFixedHeadersAreaRight;

									if ( clipSelfRect.Width <= 0 )
									{
										// SSP 8/16/05 BR05458
										// This can happen if there is cell spacing or if the AddElementHelper
										// modifies the passed in elem rect so that it completely falls left of
										// the fixed headers are right. In such a case remove the header elem
										// and continue.
										// 
										//Debug.Assert( false, "This should not have happened !" );
										this.ChildElements.Remove( cellElem );

										continue;
									}
								}

								// MD 12/9/08 - 9.1 - Column Pinning Right
								// Only update the leftFixedHeadersAreaRight variable is the header is fixed on the left.
								//if ( isHeaderFixed )
								if ( isHeaderFixed && header.FixOnRightResolved == false )
									leftFixedHeadersAreaRight = Math.Max( leftFixedHeadersAreaRight, cellElem.Rect.Right );

								cellElem.InternalSetClipSelfRegion( clipSelfRect );	
							}
							// ------------------------------------------------------------------------------------
						}
					}

				}
				else
				{

					// SSP 5/14/03 - Hiding Individual Cells Feature
					// Added Hidden property off the cell to allow the user to be able to hide a cell.
					//
					// --------------------------------------------------------------------------------
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( LayoutContainerHelper.IsCellHidden( row, header.Column ) )
					UltraGridColumn headerColumn = header.Column;

					if ( LayoutContainerHelper.IsCellHidden( row, headerColumn ) )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Avoid unused private fields
						//// Set the previous cell's nextCellHidden to true to indicate that the next
						//// cell is hidden so not to clear the right border flag. See 
						//// CellUIEelement.CalcBorderSides for more info.
						////
						//if ( null != this.childElementsCollection && this.childElementsCollection.Count > 0 )
						//{
						//    cellElem = this.childElementsCollection[ this.childElementsCollection.Count - 1 ] as CellUIElementBase;
						//    if ( null != cellElem )
						//        cellElem.nextCellHidden = true;
						//}

						continue;
					}
					// --------------------------------------------------------------------------------

                    // JJD 4/11/01 - ULT1953
                    // If cellspacing is zero and cell borders can be merged 
                    // and this is not the first cell in the row then
                    // adjust the left of the rect so it overlaps with the
                    // cell to its left
                    //
                    if ( overlapCellBorders )
					{
						//if this is a UltraDropDown we want to merge the first cell
						//
						if ( !( layout.Grid is UltraGrid ) || ! header.FirstItem ) 
                        {
							rcWork.X--;
							rcWork.Width++;						
						}
					}

					// SSP 7/17/03 - Fixed headers
					// If the column header is completely scrolled underneath the fixed
					// headers, then don't add it.
					//
					// ----------------------------------------------------------------------
					isHeaderFixed = false;
					if ( usingFixedHeaders )
					{
						isHeaderFixed = band.IsHeaderFixed( colRegion, header );
						if ( ! isHeaderFixed && rcWork.Right <= leftFixedHeadersAreaRight )
							continue;
					}
					// ----------------------------------------------------------------------

					// add the column's header object
					//
					UIElement element = AddElementHelper( oldCellElements,
						typeof(CellUIElementBase),
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//rcWork, clippedRect, row, header.Column, ref sameRow,
						rcWork, clippedRect, row, headerColumn, ref sameRow,
						// SSP 11/13/01 UWG599
						// added activeRowStatusChanged
						activeRowStatusChanged,
						// JJD 1/24/02
						// Added flag to cover the case where there are no row borders and
						// the cell border can overlap without any spacing between them
						//
						// SSP 11/30/04 - Merged Cell Feature
						// Added mergeableCellBordersBetweenRows member variable so there is
						// no need to pass it around to AddElementHelper method.
						//
						//mergeableCellBordersBetweenRows,
						// SSP 2/26/03 - Row Layout Functionality
						// Pass in false for the new rowLayoutMode parameter.
						//
						false
						);

					// SSP 5/19/03 - Fixed headers
					// 
					// ------------------------------------------------------------------------------------
					cellElem = element as CellUIElementBase;
					if ( this.usingFixedHeaders && null != cellElem )
					{
						// Add a separator element separating fixed and non-fixed cells.
						// 
						//if ( ( ! isHeaderFixed || 1 + index == count ) && ! fixedHeadersSeparatorAdded )
						if ( ! isHeaderFixed && ! leftFixedHeadersSeparatorAdded )
						{
							this.AddFixedCellSeparatorUIElement( oldCellElements, leftFixedHeadersAreaRight );
							leftFixedHeadersSeparatorAdded = true;
						}

						Rectangle clipSelfRect = Rectangle.Empty;
							
						// SSP 5/3/05 - NAS 5.2
						// AddElementHelper call above could modify the rect so reget the actual cell
						// element's rect.
						//
						rcWork = element.Rect;

						if ( rcWork.Left < leftFixedHeadersAreaRight && ! isHeaderFixed )
						{
							clipSelfRect = rcWork;
							clipSelfRect.Width = clipSelfRect.Right - leftFixedHeadersAreaRight; 
							clipSelfRect.X = leftFixedHeadersAreaRight;

							if ( clipSelfRect.Width <= 0 )
							{
								// SSP 8/16/05 BR05458
								// This can happen if there is cell spacing or if the AddElementHelper
								// modifies the passed in elem rect so that it completely falls left of
								// the fixed headers are right. In such a case remove the header elem
								// and continue.
								// 
								//Debug.Assert( false, "This should not have happened !" );
								this.ChildElements.Remove( cellElem );

								continue;
							}
						}

						// MD 12/9/08 - 9.1 - Column Pinning Right
						// Only update the leftFixedHeadersAreaRight variable is the header is fixed on the left.
						//if ( isHeaderFixed )
						if ( isHeaderFixed && header.FixOnRightResolved == false )
							leftFixedHeadersAreaRight = Math.Max( leftFixedHeadersAreaRight, cellElem.Rect.Right );

						cellElem.InternalSetClipSelfRegion( clipSelfRect );	
					}
					// ------------------------------------------------------------------------------------
				}

			}

			// SSP 7/17/03 - Fixed headers
			// Add a separator element separating fixed and non-fixed cells.
			// 
			if ( usingFixedHeaders && ! leftFixedHeadersSeparatorAdded && leftFixedHeadersAreaRight > 0 )
			{
				this.AddFixedCellSeparatorUIElement( oldCellElements, leftFixedHeadersAreaRight );
				leftFixedHeadersSeparatorAdded = true;
			}

			// MD 12/24/08 - TFS11569
			// Add in the fixed cell separator for right-fixed columns.
			if ( usingFixedHeaders )
			{
				HeaderBase[] rightFixedHeaders = band.GetFixedHeaders( colRegion, true );

				if ( rightFixedHeaders != null && rightFixedHeaders.Length > 0 )
					this.AddFixedCellSeparatorUIElement( oldCellElements, rightFixedHeaders[ 0 ].GetOnScreenOrigin( colRegion, false ) );
			}

			if ( row.AutoPreviewEnabled )
			{
				Rectangle rcAutoPreviewArea = this.rectValue;

				// SSP 5/12/06 - Optimizations
				// 
				//if ( rcAutoPreviewArea.Height > 0 && row.AutoPreviewHeight > 0 )
				int autoPreviewHeight = row.AutoPreviewHeight;				
				if ( rcAutoPreviewArea.Height > 0 && autoPreviewHeight > 0 )
                {
					int temp = rcAutoPreviewArea.Bottom - autoPreviewHeight -1;
					rcAutoPreviewArea.Height = autoPreviewHeight +1;

					rcAutoPreviewArea.Y = temp;

					//RobA UWG685 11/7/01 
					//
					if ( layout.CanMergeAdjacentBorders( this.BorderStyle ) )
					{
						--rcAutoPreviewArea.Y;
						++rcAutoPreviewArea.Height;
					}

					// SSP 1/24/02 UWG980
					// We are supposed to be indenting the text within the auto preview area
					// not the whole auto preview area itself. So commented this out and
					// moved the logic for indenting the text into RowAutoPreviewUIElement
					//
                    
				   
					this.AddElementHelper( oldCellElements, 
											typeof(RowAutoPreviewUIElement), 
											rcAutoPreviewArea,
											clippedRect,
											row,
											null,
											ref sameRow,
											// SSP 11/13/01 UWG599
											// added activeRowStatusChanged
											activeRowStatusChanged,
											// JJD 1/24/02
											// Added flag to cover the case where there are no row borders and
											// the cell border can overlap without any spacing between them
											//
											// SSP 11/30/04 - Merged Cell Feature
											// Added mergeableCellBordersBetweenRows member variable so there is
											// no need to pass it around to AddElementHelper method.
											//
											//mergeableCellBordersBetweenRows,
											// SSP 2/26/03 - Row Layout Functionality
											// Pass in false for the new rowLayoutMode parameter.
											//
											false );
  
                }
			}

			// SSP 3/30/05 - NAS 5.2
			// Added ability to display prompts in add-rows. Also the new filter row feature displays
			// prompts in filter rows as well. Added AddRowPromptElementHelper method.
			//
			RowPromptUIElement.AddRowPromptElementHelper( row, this, oldCellElements, null );

			// SSP 5/17/05 BR03705
			// Dispose of the elements that weren't reused.
			//
			if ( null != oldCellElements )
				oldCellElements.DisposeElements( );
		}

			// JJD 1/24/02
			// Added flag to cover the case where there are no row borders and
			// the cell border can overlap without any spacing between them
			//
		// SSP 2/26/03 - Row Layout Functionality
		// Changed the access modifier from private (actually none) to internal.
		//
		// SSP 6/5/03 - Fixed headers
		// Changed the return type from void to UIElement. It will return the added element or null if
		// none was added.
		//
		internal UIElement AddElementHelper( UIElementsCollection oldList,
								Type type,
								Rectangle rcElement,
								Rectangle rcClip,
								Infragistics.Win.UltraWinGrid.UltraGridRow row,
								Infragistics.Win.UltraWinGrid.UltraGridColumn column,
								ref bool sameRow,
								bool activeRowStatusChanged,
								// SSP 11/30/04 - Merged Cell Feature
								// Added mergeableCellBordersBetweenRows member variable so there is
								// no need to pass it around to AddElementHelper method.
								//
 								//bool mergeableCellBordersBetweenRows,
								// SSP 2/26/03 - Row Layout Functionality
								// Added rowLayoutMode parameter to indicate that it's being called from
								// row layout logic.
								// 
								bool rowLayoutMode )
		{
			// SSP 7/8/05 - NAS 5.3 Empty Rows
			// Depending on the empty row style, the empty row may need to extend the first cell left.
			// Call the new AdjustCellUIElementRect method for that. In row layout mode, the row layout
			// container already does that so if rowLayoutMode is true then don't call 
			// AdjustCellUIElementRect.
			// 
			if ( ! rowLayoutMode )
				row.AdjustCellUIElementRect( this, column, ref rcElement );

			// if the passed in rect doesn't intersect at all with this element's
			// rect then just return
			//
			// JJD 1/15/02
			// Use the passed in clip rect so we don't add elements that are
			// scrolled out of view
			//
			// SSP 6/27/03 - Optimization
			// In row-layout mode, we are checking for this in the row layout container. So don't
			// check for it again here. Not only that, the code in the row layout container also
			// does another important check for the row layout designer.
			//
			//if( !rcElement.IntersectsWith( rcClip ) )
			if( ! rowLayoutMode && !rcElement.IntersectsWith( rcClip ) )
				return null;

			UIElement element = null;

			if ( typeof( CellUIElementBase ) == type )
			{
				if ( column == null )
				{
					System.Diagnostics.Debug.Assert( false, "CellUiement needs both row and column context");
					return null;
				}

				// SSP 2/26/03 - Row Layout Functionality
				// row layout code already takes care of overlapping borders so don't do it here.
				//
				if ( ! rowLayoutMode )
				{
					column.AdjustForRowBorders( ref rcElement );
					column.AdjustForCellLevel ( ref rcElement, row );
					column.AdjustForCellSpacing( ref rcElement );

					// JJD 1/24/02
					// Added flag to cover the case where there are no row borders and
					// the cell border can overlap without any spacing between them
					//
					// SSP 11/30/04 - Merged Cell Feature
					// Added mergeableCellBordersBetweenRows member variable so there is
					// no need to pass it around to AddElementHelper method.
					//
					//if ( mergeableCellBordersBetweenRows )
					if ( this.mergeableCellBordersBetweenRows )
					{
						// if the cell is on the first level adjust its
						// top up
						// MD 1/21/09 - Groups in RowLayouts
						// Use LevelResolved because it checks the band's row layout style
						//if ( column.Level == 0 &&
						if ( column.LevelResolved == 0 &&
							row.RowSpacingBeforeResolved < 1 &&
							row.RowSpacingAfterResolved < 1 )
						{
							rcElement.Y--;
							rcElement.Height++;
						}

						// If the cell is the first item in a level expand it
						// one pixel to the left if we have row selectors
						//
						if ( column.FirstItemInLevel &&
							column.Band.RowSelectorExtent > 0 )
						{
							// MD 1/21/09 - Groups in RowLayouts
							// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
							//if ( column.Group == null ||
							//    column.Group.FirstItem )
							if ( column.GroupResolved == null ||
								column.GroupResolved.FirstItem )
							{
								rcElement.X--;
								rcElement.Width++;
							}
						}
					}
				}

				// SSP 5/19/03 - Fixed headers
				// If the cell is right next to the row selector then merge the cell border with the
				// row selector border by stretching the cell 1 pixel left.
				//
				// ----------------------------------------------------------------------------------
				if ( this.usingFixedHeaders )
				{
					if ( this.Rect.X + 1 == rcElement.X )
					{
						// SSP 12/1/03 UWG2641
						// Added support for cell and label insets. If the cell has any insets, then don't
						// merge borders. Enclosed the existing code in the if block.
						//
						if ( ! rowLayoutMode || 0 == RowLayoutColumnInfo.GetItemInsetsHelper( column ).Left )
						{
							rcElement.X--;
							rcElement.Width++;
						}
					}
				}
				// ----------------------------------------------------------------------------------

				// JJD 10/30/01
				// Moved logic into GetCellElement which will try to find this
				// exact cell in the list of existing cells
				//
				
				CellUIElementBase cellElem = this.GetCellElement( oldList, rcElement, row, column, ref sameRow, activeRowStatusChanged );
				element = cellElem;

				//RobA UWG586 11/7/01
				//
				// SSP 8/4/03 - Optimizations
				// Commented out the original code and added the new code below it. Apparently
				// GetResolvedCellActivation method does not check the cell's Activation settings
				// (which goes against the convetions of GetResolved methods off the row taking
				// a column). We have to do it here ourselves.
				//
				// ----------------------------------------------------------------------------------
				
				UltraGridCell cell = row.GetCellIfAllocated( column );
				element.Enabled = Activation.Disabled != 
					( null != cell ? cell.ActivationResolved : row.GetResolvedCellActivation( column ) );
				// ----------------------------------------------------------------------------------

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//// SSP 5/14/03 - Hiding Individual Cells Feature
				//// A flag by the row cell area element's position child elements to indicate that the next
				//// cell is hidden.
				//// Reset the flag.
				//// 
				//// ----------------------------------------------------------------------------------------
				//if ( null != cellElem )
				//{
				//    cellElem.nextCellHidden = false;
				//}
				//// ----------------------------------------------------------------------------------------
			}
			
			else if ( typeof( RowAutoPreviewUIElement ) == type )
			{        
				if ( row == null)
					return null;

				// Make sure we did not adjust right past left and bottom past top
				//
				rcElement.Width = (System.Math.Max( rcElement.Right, rcElement.Left + 1 )) - rcElement.X ;
				rcElement.Height = (System.Math.Max( rcElement.Bottom, rcElement.Top+1 )) - rcElement.Y ;

				if ( element == null )
				{
					element = new RowAutoPreviewUIElement( this, rcElement );
				}					
				else
				{
					element.Rect = rcElement;
				}
			}	

			if ( element != null )
			{
				// insert the new element into the passed in list
				//
				this.ChildElements.Add( element );
			}
			
			return element;
		}

		private CellUIElementBase GetCellElement( UIElementsCollection oldList,
			Rectangle rcElement,
			Infragistics.Win.UltraWinGrid.UltraGridRow row,
			Infragistics.Win.UltraWinGrid.UltraGridColumn column,
			ref bool sameRow,
			// SSP 11/13/01 UWG599
			// If the row was previously a non-active row and now it's an active row
			// or vice versa, then make sure the cell uielement is dirited so any
			// active row appearance settings on the cell will take effect.
			//
			bool activeRowStatusChanged )
		{

			CellUIElementBase	element = null;
			CellUIElementBase	oldElement;

			// if we are on the same row look for an existing cell element
			// that matches the context
			//
			if ( sameRow )
			{
				// SSP 5/14/03 - Optimizations
				//
				// if ( oldList == null || oldList.Count < 1 )
				int oldListCount = oldList.Count;
				if ( oldList == null || oldListCount < 1 )
				{
					// since we don't have any old elements set the
					// sameRow flag to false so we can ignore this logic
					// on subsequent calls
					//
					sameRow = false;
				}
				else
				{
					// iterate over the old elements list looking for
					// a context match
					//
					// SSP 5/14/03 - Optimizations
					//	
					//for ( int i = 0; i < oldList.Count; i++ )
					for ( int i = 0; i < oldListCount; i++ )
					{
						oldElement = oldList[i] as CellUIElementBase;
  
						// if this slot is nulled out or
						// it is not a CellUIElement then continue
						//
						if ( oldElement == null )
							continue;

						// if the column context doesn't natch continue
						//
						if ( oldElement.Column != column )
							continue;

						// check the row context
						//
						if ( oldElement.Row != row )
						{
							// set the same row flag to false
							// so we stop looking thru the existing elements
							//
							sameRow = false;
							break;
						}

						// both contexts match so we can use the existing 
						// element
						//
						element = oldElement;
						
						// null out the slot in the old list so we don't
						// try using this element again.
						//
						oldList[i] = null;

						break;
					}
				}
			}

			if ( element != null )
			{
				Rectangle oldRect = element.Rect;

				// If the size of the element is the same as the new size
				// we just offset the element (which is more efficient).
				// Otherwise we set its rect which will dirty it
				//
				// SSP 11/13/01 UWG599
				// Look at the comment near the method signature regarding UWG599
				//
				//if ( oldRect.Size == rcElement.Size )
				// SSP 11/28/01 UWG769
				// Also check for the newly added CellChildElementsCacheVersion version
				// off the layout.
				//
				//if ( !activeRowStatusChanged && oldRect.Size == rcElement.Size )
				if ( !activeRowStatusChanged && 
					oldRect.Size == rcElement.Size &&
					// SSP 10/10/02 UWG1530
					// Take into account column.CellChildElementsCacheVersion as well.
					//
					//element.VerifiedCellChildElementsCacheVersion == row.Band.Layout.CellChildElementsCacheVersion )
					element.VerifiedCellChildElementsCacheVersion == column.CellChildElementsCacheVersion )
					element.Offset( rcElement.Left - oldRect.Left, rcElement.Top - oldRect.Top, true );
				else
					element.Rect = rcElement;
				
				return element;
			}

			// Since we didn't find an element that matched, iterate
			// thru the oldList backwards and use the first (last) cell
			// element we find.
			// Note: we look for the last cell element in the list 
			// since that will be the least likely to match the next
			// cell we go to add.
			//
			// SSP 4/23/02
			// Contrary to the comment above, we could have gotten here if sameRow was
			// false, bypassing the logic above that finds the element. If that is
			// the case, then we still should try to do the column matching.
			// 
			// If the sameRow is false, and we did not try to find any cell element
			// above, then do a column matching.
			//
			if ( sameRow )
			{
				for ( int i = oldList.Count - 1; i >= 0; i-- )
				{
					oldElement = oldList[i] as CellUIElementBase;

					if ( oldElement == null )
						continue;

					// use the element and null out the old list slot
					//
					element = oldElement;
					oldList[i] = null;

					break;
				}
			}
			else
			{
				int lastNonNullElementIndex = -1;

				int oldListCount = oldList.Count;
				for ( int i = 0; i < oldListCount; i++ )
				{
					oldElement = oldList[i] as CellUIElementBase;

					if ( oldElement == null )
						continue;

					lastNonNullElementIndex = i;

					if ( oldElement.Column != column )
						continue;

					// use the element and null out the old list slot
					//
					element = oldElement;
					oldList[i] = null;

					break;
				}

				// SSP 4/23/02
				//
				if ( null == element && lastNonNullElementIndex >= 0 )
				{
					element = oldList[ lastNonNullElementIndex ] as CellUIElementBase;
					oldList[ lastNonNullElementIndex ] = null;
				}
			}			

			// if we didn't find one in the old list then create a new item now.
			//
			if ( element == null )
				// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
				//
				//element = new CellUIElement( this );
				element = this.CreateCellUIElement( null );

			// SSP 11/4/04 - Optimizations
			// Added the row parameter to the InitializeCell method.
			//
			//element.InitializeCell( column );
			element.InitializeCell( column, row );
			
			element.Rect = rcElement;

			return element;
		}

		private void PositionChildElementsForCard()
		{
			// SSP 2/19/03 - Row Layout Functionality
			//
			UltraGridRow			row			= this.Row;
			UltraGridBand			band		= row.Band;

			// Save the current child elements collection so we can
			// potentially reuse some of the elements.
			UIElementsCollection	oldElements = this.childElementsCollection;
			this.childElementsCollection = null;

			if ( band.UseRowLayoutResolved )
			{
				LayoutContainerCardCellArea lc = band.GetCachedLayoutContainerCardCellArea( );
				lc.Initialize( band, this.Rect, this, oldElements );

				band.RowLayoutManager.LayoutContainer( lc, null );
			}
			else
			{
				Rectangle				rectWork;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//ColScrollRegion			colRegion	= ((RowColRegionIntersectionUIElement)this.GetAncestor(typeof(RowColRegionIntersectionUIElement))).ColScrollRegion;

				// If cellspacing is zero and the top cell border can be merged set a flag
				// so we can adjust their height to overlap each other.
				bool overlapTopCellBorder = band.CellSpacingResolved == 0   &&
					// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
					// Use the cellBorderStyle member var instead.
					//
					//row.Layout.CanMergeAdjacentBorders(band.BorderStyleCellResolved);
					band.Layout.CanMergeAdjacentBorders( this.cellBorderStyle );


				// If cellspacing is zero and left/right cell borders can be merged set a flag
				// so we can adjust their width to overlap each other.
				bool overlapLeftRightCellBorder;
				// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
				// Added rowBorderStyle member var.
				//
				//if (row.Layout.CanMergeAdjacentBorders(band.BorderStyleRowResolved)  == false)
				if (band.Layout.CanMergeAdjacentBorders( this.rowBorderStyle )  == false)
					if (row.Band.CardSpacingHorizontal != 0  &&
						// SSP 2/28/03 - Row Layout Functionality
						// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
						// to StandardLabels card style so use the StyleResolved instead of the Style property.
						//
						row.Band.CardSettings.StyleResolved == CardStyle.MergedLabels)
						overlapLeftRightCellBorder = false;
					else
						overlapLeftRightCellBorder = band.CellSpacingResolved == 0   &&
							// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
							// Use the cellBorderStyle member var instead.
							//
							//row.Layout.CanMergeAdjacentBorders(band.BorderStyleCellResolved);
							row.Layout.CanMergeAdjacentBorders( this.cellBorderStyle );
				else
					overlapLeftRightCellBorder = band.CellSpacingResolved == 0   &&
						// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
						// Added rowBorderStyle member var.
						//
						//row.Layout.CanMergeAdjacentBorders(band.BorderStyleRowResolved);
						row.Layout.CanMergeAdjacentBorders( this.rowBorderStyle );


				// If cellspacing is zero and bottom cell borders can be merged set a flag
				// so we can adjust their height to overlap each other.
				bool overlapBottomCellBorder = band.CellSpacingResolved == 0   &&
					// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
					// Added rowBorderStyle member var.
					//
					//row.Layout.CanMergeAdjacentBorders(band.BorderStyleRowResolved);
					row.Layout.CanMergeAdjacentBorders( this.rowBorderStyle );


				// JM 01-24-02
				// Make a pass thru the headers to find the last displayable header so we can pass
				// the correct 'isLastItem' value to AddLabelElementHelper.  We cannot always
				// rely on the header's LastItem property since this may not be accurate
				// if the CardStyle is variable.  The reason is that if the LastItem
				// property = true but the header has a cell value that would cause it to
				// not be displayed when CardStyle is variable, then it will not be the
				// last card.
				HeaderBase		lastDisplayableHeader = band.GetLastDisplayableCardLabel(this.Row);


				// JM 01-24-02
				// Need to keep track of the first displayable header.  Can't rely on
				// the header's FirstItem for the same reason outlined above.
				bool			isFirstItem = true;

				// Iterate over the ordered headers and add cell elements for each one.
				int				heightUsed	= 0;
				HeaderBase		header		= null;

				// MD 1/21/09 - Groups in RowLayouts
				// This is no logner needed.
				//UltraGridColumn	column		= null;

				for (int index = 0; index < band.OrderedHeaders.Count; index++)
				{
					header = band.OrderedHeaders[index]; 
					if (header == null)
						continue;

					// If hidden, skip it.
					if (header.Hidden)
						continue;

					// If it's a group, drill down for the headers.
					if (header.IsGroup)
					{
						// MD 1/21/09 - Groups in RowLayouts
						// Use the resolved column because in GroupLayout style, it will return a different enumarator which returns the columns 
						// actually visible in the group
						//GroupColumnsCollection colList = header.Group.Columns;
						IEnumerable<UltraGridColumn> colList = header.Group.ColumnsResolved;

						if (colList != null)
						{
							// MD 1/21/09 - Groups in RowLayouts
							// The colList is now an IEnumarable<UltraGridColumn> , so we must use a foreach.
							// Iterate over the columns in this group and create an element for each
							//for ( int i = 0; i < colList.Count; i++ )
							//{
							//    column = colList[i];
							foreach ( UltraGridColumn column in colList )
							{
								// If hidden, skip it.
								if (column.Header.Hidden)
									continue;

								// Make sure we should display this cell.
								if (column.ShouldCellValueBeDisplayedInCard(this.Row) == false)
									continue;

								// Initialize the cell's rect.
								rectWork		= this.RectInsideBorders;
								rectWork.Y		= this.Rect.Top + heightUsed;
								rectWork.Height	= column.CardCellHeight;
								heightUsed	   += rectWork.Height;

								// Create/find the cell and add it to our child elements collection.
								this.AddCardCellHelper(oldElements,
									rectWork,
									row,
									band,
									column,
									overlapTopCellBorder,
									overlapLeftRightCellBorder,
									overlapBottomCellBorder,
									// JM 01-24-02 Use isFirstItem
									//header.FirstItem && column.FirstItem,
									isFirstItem,
									// JM 01-24-02 Use lastDisplayableHeader 
									//header.LastItem  && column.LastItem);
									header.Equals(lastDisplayableHeader ), 
									// SSP 2/26/03 - Row Layout Functionality
									// Pass in false for the new rowLayoutMode parameter.
									//
									false );

								isFirstItem = false;
							}
						}
					}
					else
					{
						// Make sure we should display this cell.
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if (header.Column.ShouldCellValueBeDisplayedInCard(this.Row) == false)
						UltraGridColumn headerColumn = header.Column;

						if ( headerColumn.ShouldCellValueBeDisplayedInCard( this.Row ) == false )
							continue;

						// Initialize the cell's rect.
						rectWork		= this.RectInsideBorders;
						rectWork.Y		= this.Rect.Top + heightUsed;

						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//rectWork.Height	= header.Column.CardCellHeight;
						rectWork.Height = headerColumn.CardCellHeight;

						heightUsed	   += rectWork.Height;

						// Create/find the cell and add it to our child elements collection.
						this.AddCardCellHelper(oldElements,
							rectWork,
							row,
							band,

							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//header.Column,
							headerColumn,

							overlapTopCellBorder,
							overlapLeftRightCellBorder,
							overlapBottomCellBorder,
							// JM 01-24-02 Use isFirstItem
							//header.FirstItem,
							isFirstItem, 
							// JM 01-24-02 Use lastDisplayableHeader 
							//header.LastItem);
							header.Equals(lastDisplayableHeader),
							// SSP 2/26/03 - Row Layout Functionality
							// Pass in false for the new rowLayoutMode parameter.
							//
							false );

						isFirstItem = false;
					}
				}
			}

			// SSP 3/30/05 - NAS 5.2
			// Added ability to display prompts in add-rows. Also the new filter row feature displays
			// prompts in filter rows as well. Added AddRowPromptElementHelper method.
			//
			RowPromptUIElement.AddRowPromptElementHelper( row, this, oldElements, null );

			// SSP 5/17/05 BR03705
			// Dispose of the elements that weren't reused.
			//
			if ( null != oldElements )
				oldElements.DisposeElements( );
		}

		// SSP 2/26/03 - Row Layout Functionality
		// Changed the access modifier from private (actually none) to internal.
		//
		internal void AddCardCellHelper(UIElementsCollection oldElements,
							   Rectangle			rcElement,
							   UltraGridRow			row,
							   UltraGridBand		band,
							   UltraGridColumn		column,
							   bool					overlapTopCellBorder,
							   bool					overlapLeftRightCellBorder,
							   bool					overlapBottomCellBorder,
							   bool					isFirstItem,
							   bool					isLastItem,
							   // SSP 2/26/03 - Row Layout Functionality
							   // Added rowLayoutMode parameter to indicate that it's being called from
							   // row layout logic.
							   // 
							   bool rowLayoutMode )
		{
			// SSP 7/8/05 - NAS 5.3 Empty Rows
			// Depending on the empty row style, the empty row may need to extend the first cell left.
			// Call the new AdjustCellUIElementRect method for that. In row layout mode, the row layout
			// container already does that so if rowLayoutMode is true then don't call 
			// AdjustCellUIElementRect.
			// 
			if ( ! rowLayoutMode )
				row.AdjustCellUIElementRect( this, column, ref rcElement );

			// If the passed in rect doesn't intersect at all with this element's
			// rect then just return.
			if(!rcElement.IntersectsWith(this.rectValue))
				return;

			// SSP 2/26/03 - Row Layout Functionality
			// row layout code already takes care of overlapping borders so don't do it here.
			//
			if ( ! rowLayoutMode )
			{
				column.AdjustForCellSpacing(ref rcElement);

				// Adjust the element's rect for border overlap if necessary.
				this.AdjustElementRectForBorderOverlap(ref rcElement,
					row,
					band,
					overlapTopCellBorder,
					overlapLeftRightCellBorder,
					overlapBottomCellBorder,
					isFirstItem,
					isLastItem);
			}


			// Try to reuse an element from the old elements list.
			// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
			// Use the CreateCellUIElement method instead.
			//
			//CellUIElement cellElement = (CellUIElement)RowCellAreaUIElement.ExtractExistingElement(oldElements, typeof(CellUIElement), true);
			//if (cellElement == null)				
			//	cellElement = new CellUIElement(this);
			CellUIElementBase cellElement = this.CreateCellUIElement( oldElements );

			// SSP 11/4/04 - Optimizations
			// Added the row parameter to the InitializeCell method.
			//
			//cellElement.InitializeCell(column);
			cellElement.InitializeCell( column, row );
			cellElement.Rect = rcElement;

			// SSP 8/4/03 - Optimizations
			// Commented out the original code and added the new code below it. Apparently
			// GetResolvedCellActivation method does not check the cell's Activation settings
			// (which goes against the convetions of GetResolved methods off the row taking
			// a column). We have to do it here ourselves.
			//
			// ----------------------------------------------------------------------------------
			
			UltraGridCell cell = row.GetCellIfAllocated( column );
			cellElement.Enabled = Activation.Disabled != ( 
				null != cell ? cell.ActivationResolved 
				: ( null != row ? row.GetResolvedCellActivation( column ) : Activation.Disabled ) );
			// ----------------------------------------------------------------------------------

			// Add the new element.
			this.ChildElements.Add(cellElement);
		}

		private void AdjustElementRectForBorderOverlap(ref Rectangle	rcElement,
													   UltraGridRow		row,
													   UltraGridBand	band,
													   bool				overlapTopCellBorder,
													   bool				overlapLeftRightCellBorder,
													   bool				overlapBottomCellBorder,
													   bool				isFirstItem,
													   bool				isLastItem)
		{

			// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
			// Added rowBorderStyle member var.
			//
			//bool canMergeRow		= band.Layout.CanMergeAdjacentBorders(band.BorderStyleRowResolved);
			bool canMergeRow		= band.Layout.CanMergeAdjacentBorders( this.rowBorderStyle );

			bool canMergeCardArea	= band.Layout.CanMergeAdjacentBorders(band.BorderStyleCardAreaResolved);
			bool canMergeHeader		= band.Layout.CanMergeAdjacentBorders(band.BorderStyleHeaderResolved);
			bool showCaption		= band.CardSettings.ShowCaption;
			// SSP 2/28/03 - Row Layout Functionality
			// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
			// to StandardLabels card style so use the StyleResolved instead of the Style property.
			//
			bool mergedLabels		= band.CardSettings.StyleResolved == CardStyle.MergedLabels;
			bool inFirstCardAreaRow = this.InFirstCardAreaRow;
            // SSP 6/13/05
            // Whidbey gives warnings when assigning a var to itself so commented this out.
            // 
			//bool doNothing			= false;

			// Overlap top border if conditions are right.
			if (overlapTopCellBorder)
			{
				if (!isFirstItem)
				{
					rcElement.Y--;
					rcElement.Height++;						
				}
				else if (showCaption == false)
				{
					if (inFirstCardAreaRow	== true		&&
						canMergeRow			== false	&&
						canMergeCardArea	== false	&&
						(canMergeHeader		== false || band.HeaderVisible == false))
					{
                        // SSP 6/13/05
                        // Whidbey gives warnings when assigning a var to itself so commented this out.
                        // 
						//doNothing = doNothing;
					}
					else if (inFirstCardAreaRow == true	  &&
							 band.HeaderVisible == true	  &&
							 canMergeHeader		== true	  &&
							 canMergeRow		== false  &&
							 canMergeCardArea	== false  &&
							 band.CardSpacingVertical != 0)
					{
                        // SSP 6/13/05
                        // Whidbey gives warnings when assigning a var to itself so commented this out.
                        // 
						//doNothing = doNothing;
					}
					else if (showCaption		== false &&
							 canMergeRow		== false &&
							 band.CardSpacingVertical	!= 0)
					{
                        // SSP 6/13/05
                        // Whidbey gives warnings when assigning a var to itself so commented this out.
                        // 
						//doNothing = doNothing;
					}
					else
					{
						rcElement.Y--;
						rcElement.Height++;						
					}
				}
				else
				{
                    // SSP 6/13/05
                    // Whidbey gives warnings when assigning a var to itself so commented this out.
                    // 
					//doNothing = doNothing;
				}
			}

			// Overlap left & right borders if conditions are right.
			if (overlapLeftRightCellBorder)
			{
				if (mergedLabels	== false	&&
					canMergeHeader	== false)
				{
                    // SSP 6/13/05
                    // Whidbey gives warnings when assigning a var to itself so commented this out.
                    // 
					//doNothing = doNothing;
				}
				else
				{
					rcElement.X--;
					rcElement.Width++;
				}

				if (this.InLastCardAreaCol)
				{
					if (canMergeRow			== true ||
					   (canMergeCardArea	== true && band.CardSpacingHorizontal == 0))
					{
						rcElement.Width++;
					}
					else
					{
                        // SSP 6/13/05
                        // Whidbey gives warnings when assigning a var to itself so commented this out.
                        // 
						//doNothing = doNothing;
					}
				}
				else if (canMergeRow)
				{
					rcElement.Width++;
				}
				else if (mergedLabels		== false &&
						 canMergeCardArea	== false &&
						 canMergeHeader		== true	 &&
						 band.CardSpacingHorizontal == 0)	
				{
					rcElement.Width++;
				}
			}

			// Overlap bottom border if conditions are right.
			if (overlapBottomCellBorder)
			{
				if (isLastItem)
				{
					rcElement.Height++;
				}
			}
		}

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
                                                 ref AppearancePropFlags requestedProps )
		{
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// 
			//this.Row.ResolveAppearance( ref appearance, requestedProps );
			this.Row.ResolveAppearance( ref appearance, ref requestedProps, false, this.IsRowHotTracking );
		}


        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// SSP 5/1/03 - Optimizations
				// Cache the borderStyle.
				//
				
				return this.borderStyle;
			}
		}

		
		/// <summary>
		/// Returns true if this  element needs to draw a focus rect. This should 
		/// be overridden since the default implementation always returns false. 
		/// </summary>
		/// <remarks>Even if this property returns true the focus will not be drawn unless the control has focus.</remarks>
		protected override bool DrawsFocusRect
		{
			get
			{
				UltraGridRow row = this.Row;

				// JJD 9/26/01
				// Draw the focus rect for dropdowns as well
				//
				//	return ( this.Row.Layout.Grid is UltraGrid && 
				//		this.Row.IsActiveRow && this.Row.Band.Layout.ActiveCell == null );
				//				
				// SSP 4/1/05 - Optimizations
				// Use the IsActiveRowInternal which does not go through the process of verifying 
				// that the active row is valid.
				//
				//if ( !row.IsActiveRow || row.Band.Layout.ActiveCell != null )
				if ( ! row.IsActiveRowInternal || row.Band.Layout.ActiveCell != null )
					return false;

				RowScrollRegion rsr = this.GetContext( typeof( RowScrollRegion ) ) as RowScrollRegion;
				ColScrollRegion csr = this.GetContext( typeof( ColScrollRegion ) ) as ColScrollRegion;

				return ( rsr == row.Layout.Grid.ActiveRowScrollRegion &&
						 csr == row.Layout.Grid.ActiveColScrollRegion );
			}
		}
		

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return Border3DSide.All;
			}
		}

		/// <summary>
		/// Does nothing if the row is a Card.
		/// </summary>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{
			if (this.Row.IsCard)
			{
				// SSP 5/18/06 - App Styling
				// Before we were not drawing the back color and background image of the row cell area
				// in card-view. Therefore there was no way of specifying gradients or background image 
				// for cards. This change will let one specify these however we will only honor them if
				// BackColorAlpha is set to a non-default value.
				// 
				// --------------------------------------------------------------------------------------------
				Alpha alpha = drawParams.AppearanceData.BackColorAlpha;

                // MRS NAS v8.2 - CardView Printing
                // If we are printing in CardView and the Alpha is Default, resolve it to 
                // opaque so that we get borders in between cells in the printout.
                bool isPrint = this.ControlElement != null && this.ControlElement.IsPrint;
                if (isPrint && alpha == Alpha.Default)
                    alpha = Alpha.Opaque;

				if ( Alpha.Default != alpha && Alpha.Transparent != alpha )
				{
					AppearancePropFlags flags = AppearancePropFlags.BackColorAlpha;
					AppearanceData appData = new AppearanceData( );
					this.Row.ResolveAppearance( ref appData, ref flags, false, this.IsRowHotTracking, true );
					alpha = appData.BackColorAlpha;

                    // MRS NAS v8.2 - CardView Printing
                    // If we are printing in CardView and the Alpha is Default, resolve it to 
                    // opaque so that we get borders in between cells in the printout. 
                    if (isPrint && alpha == Alpha.Default)                    
                        alpha = Alpha.Opaque;

					if ( Alpha.Default != alpha && Alpha.Transparent != alpha )
						base.DrawBackColor( ref drawParams );
				}
				// --------------------------------------------------------------------------------------------
			}
			else
			{
				base.DrawBackColor(ref drawParams);
			}
		}

		#region IsRowHotTracking

		// SSP 5/18/06 - App Styling
		// 
		internal bool IsRowHotTracking
		{
			get
			{
				return GridUtils.IsRowHotTracking( this );
			}
		}

		#endregion // IsRowHotTracking

		/// <summary>
		/// Does nothing if the row is a Card.
		/// </summary>
		protected override void DrawImageBackground(ref UIElementDrawParams drawParams)
		{
			if (this.Row.IsCard)
			{
				// SSP 5/18/06 - App Styling
				// Before we were not drawing the back color and background image of the row cell area
				// in card-view. Therefore there was no way of specifying gradients or background image 
				// for cards. This change will let one specify these however we will only honor them if
				// BackColorAlpha is set to a non-default value.
				// 
				// --------------------------------------------------------------------------------------------
				Alpha alpha = drawParams.AppearanceData.ImageBackgroundAlpha;
				if ( Alpha.Default != alpha && Alpha.Transparent != alpha )
				{
					AppearancePropFlags flags = AppearancePropFlags.ImageBackgroundAlpha;
					AppearanceData appData = new AppearanceData( );
					this.Row.ResolveAppearance( ref appData, ref flags, false, this.IsRowHotTracking, true );
					alpha = appData.ImageBackgroundAlpha;

					if ( Alpha.Default != alpha && Alpha.Transparent != alpha )
						base.DrawImageBackground( ref drawParams );
				}
				// --------------------------------------------------------------------------------------------
			}
			else
			{
				base.DrawImageBackground(ref drawParams);
			}
		}

		// SSP 11/22/04 - Merged Cell Feature
		// Overrode DrawBorders so we can clip out the borders under merged cells.
		// This is necessary if the merged cells are semi or fully transparent.
		//
		/// <summary>
		/// Overridden.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBorders( ref UIElementDrawParams drawParams )
		{
			RowUIElementBase rowElem = this.Parent as RowUIElementBase;
			System.Drawing.Region origRegion = null;

			// SSP 12/2/05 BR07533
			// Commented out the original code and added the new code below.
			// 
			// ------------------------------------------------------------------------------
			

			// Clip out the areas under the merged cells if merged cells are semi or fully tansparent.
			// Otherwise the row borders will show through the merged cells.
			// 
			Border3DSide extraPass = 0;
			if ( null != rowElem && rowElem.hasMergedCells )
			{
				origRegion = drawParams.Graphics.Clip;

				// Loop through the child cell elements.
				//
				UIElementsCollection elems = this.ChildElements;
				for ( int i = 0, count = elems.Count; i < count; i++ )
				{
					CellUIElement cellElem = elems[ i ] as CellUIElement;
					MergedCellUIElement mergedCellElem = null != cellElem ? cellElem.MergedCellElement : null;

					// If the cell is under a merged cell.
					// 
					if ( null != mergedCellElem )
					{
						// Resolve the merged cell's appearance so we can check if it's using alpha blending.
						// 
						AppearanceData appData = new AppearanceData( );
						AppearancePropFlags flags = AppearancePropFlags.AllRender;
						mergedCellElem.ResolveAppearance( ref appData, ref flags );

						if ( ! GridUtils.IsBackgroundOpaque( ref appData ) )
						{
							// If the cell is in edit mode, we don't need clip out its borders since 
							// the cell will be visible, even when it's part of a merged cell.
							// 
							if ( cellElem.IsInEditMode )
								continue;

							Rectangle thisRect = this.Rect;

							// We are only interested in the intersection of the merged cell and the
							// row.
							// 
							Rectangle rect = Rectangle.Intersect( thisRect, mergedCellElem.Rect );
							
							// Always draw the left and the right borders of the row.
							// 
							if ( rect.X == thisRect.X )
							{
								rect.X++;
								rect.Width--;
							}

							// Always draw the left and the right borders of the row.
							// 
							if ( rect.Right == thisRect.Right )
								rect.Width--;

							// Get all the cells that make up the merged cells.
							// 
							// MD 8/10/07 - 7.3 Performance
							// Use generics
							//ArrayList mergedCellElems = mergedCellElem.CellElements;
							List<CellUIElement> mergedCellElems = mergedCellElem.CellElements;

							if ( rect.Y == thisRect.Y )
							{
								// MD 8/10/07 - 7.3 Performance
								// Use generics
								//CellUIElement aboveCellElem = (CellUIElement)GridUtils.GetPrevItem( mergedCellElems, cellElem );
								CellUIElement aboveCellElem = GridUtils.GetPrevItem( mergedCellElems, cellElem );

								if ( 
									// Draw the top border of the row if the row is the first of the 
									// rows that constitute the merged cell.
									// 
									cellElem == GridUtils.GetFirstItem( mergedCellElems ) 
									// Also if the cell above is in edit mode, which means that it will
									// be fully visible, make sure that it draws its bottom border properly.
									// What happens is that the background of this row overrides the
									// bottom border of the cell above. So if we don't draw our top border,
									// the cell above won't draw it's bottom border. To demonstrate this
									// issue, comment this condition out and enter edit mode in a merged
									// cell (ofcourse the merged cell has to be transparent). You will 
									// notice that the bottom border of the edit cell is not drawn without
									// this condition.
									// 
									|| null != aboveCellElem && aboveCellElem.IsInEditMode )
								{
									rect.Y++;
									rect.Height--;
								}
							}

							// Draw the bottom border of the row if the row is the last of the 
							// rows that constitute the merged cell.
							// 
							if ( rect.Bottom == thisRect.Bottom
								&& cellElem == GridUtils.GetLastItem( mergedCellElems ) )
								rect.Height--;

							if ( rect.Width > 0 && rect.Height > 0 )
							{
								// Exclude the constructed rect from the graphics cliprect so when the 
								// row borders are drawn below, they won't draw borders under the merged
								// cells.
								// 
								drawParams.Graphics.ExcludeClip( rect );

								// There is a bug in .NET line drawing code that will prevent the
								// right border from being drawn if there is a exclusion rect on 
								// the graphics clip region that's adjecent to the line being drawn.
								// Therefore we have to draw the right border after clearing the
								// clip rect. That's what the extraPass is for.
								// 
								if ( rect.Right == thisRect.Right || rect.Right == thisRect.Right - 1 )
									extraPass |= Border3DSide.Right;
							}
						}
					}
				}
			}

			base.DrawBorders( ref drawParams );

			if ( null != origRegion )
			{
				drawParams.Graphics.Clip = origRegion;
				origRegion.Dispose( );
			}

			if ( 0 != extraPass )
				drawParams.DrawBorders( this.BorderStyle, extraPass );
			// ------------------------------------------------------------------------------
		}

		/// <summary>
		/// Returns true if row can be resized (using row cell area)
		/// </summary>
		/// <returns></returns>
		public override bool Adjustable
		{
			get
			{
				return !this.Row.IsFixedHeight && 
					this.Row.Band.RowSizingAreaResolved != RowSizingArea.RowSelectorsOnly;
			}
		}

        /// <summary>
        /// Returns the cursor to use over the adjustable area of the element.
        /// </summary>
        /// <param name="point">The point that should be used to determine if the area is adjustable.</param>
        /// <returns>The cursor that should be used to represent an adjustable region.</returns>
        public override System.Windows.Forms.Cursor GetAdjustableCursor(System.Drawing.Point point)
		{
			if ( this.SupportsUpDownAdjustments )
			{
				// SSP 11/26/03 UWG2344
				// Added PerformAutoSize to the UltraGridRow and perform auto size functionality for rows.
				//
				// ----------------------------------------------------------------------
				if ( this.SupportsAutoSizing )
				{
					System.Windows.Forms.Cursor cursor = this.Row.Band.Layout.RowAutoSizeCursor;

					if ( null != cursor )
						return cursor;
				}
				// ----------------------------------------------------------------------

				return System.Windows.Forms.Cursors.SizeNS;
			}

			return null;
		}


		/// <summary>
		/// Returns the range limits for adjusting the element in either or both
		/// dimensions. It also returns the initial rects for the vertical and horizontal
		/// bars that will need to be inverted during the mouse drag operation.
		/// </summary>
		/// <param name="point">Point</param>
		/// <param name="range">Returned limits</param>
		public override void GetAdjustmentRange ( System.Drawing.Point point,
			out UIElementAdjustmentRangeParams range )
		{
			base.GetAdjustmentRange( point, out range );

			if ( !this.SupportsUpDownAdjustments )
				return;

			UltraGridRow  row  = this.Row;
			UltraGridBand band = this.Row.Band;

			Debug.Assert( null != row, "No band object in CUIElementRowSelector.GetAdjustmentRange");
			Debug.Assert( null != band, "No row object in CUIElementRowSelector.GetAdjustmentRange");

			if ( null != row && null != band )
			{
				if ( band.IsRowSizingFixed )
					return;

				Rectangle dataAreaRect   = this.GetAncestor( typeof ( DataAreaUIElement ) ).Rect;
				Rectangle metaRgionRect  = this.GetAncestor( typeof ( RowColRegionIntersectionUIElement ) ).Rect;
        
				// get the band's minimum row height
				//
				int minRowHeight = row.MinRowHeight;
 
				if ( this.Rect.Height > minRowHeight )
				{
					range.maxDeltaUp = - System.Math.Min( metaRgionRect.Bottom - this.Rect.Top,
						this.Rect.Height - minRowHeight );
				} 
				else
				{
					range.maxDeltaUp = 0;
				}

				range.maxDeltaDown = System.Math.Max( 0, metaRgionRect.Bottom - this.Rect.Bottom - 1 );	

				// set up the horz bar from left to right of the meta region
				range.upDownAdjustmentBar.X = dataAreaRect.Left;
				range.upDownAdjustmentBar.Width = dataAreaRect.Width;
			}
		}

        /// <summary>
        /// Called after a move/resize operation. 
        /// </summary>
        /// <param name="delta">The delta</param>
        public override void ApplyAdjustment(Point delta)
		{
			
			UltraGridRow row = this.Row;

			Debug.Assert( null != row, "Not row found in RowSelectorUIElement.ApplyAdjustment" );

			if ( 0 != delta.Y  )
			{
				int newHeight = row.BaseHeight + delta.Y;

				// SSP 8/23/01 UWG218
				// Before resizing the row, we have to exit the edit mode so the text box
				// gets hidden
				if ( null != this.Row.Layout.ActiveCell &&
					this.Row.Layout.ActiveCell.IsInEditMode )
				{
					this.Row.Layout.ActiveCell.ExitEditMode( );	
				}

				// tell the band that the row needs to be resized to the new height				
				row.Band.ResizeRow( row, newHeight, true );

				RowScrollRegion rsr = (RowScrollRegion)this.Parent.GetContext( typeof( RowScrollRegion ), true );
				Debug.Assert( null != rsr, "null RowScrollRegion" ); 

				if ( null != rsr )
					rsr.ScrollRowIntoView( row );
			}			
		}

		internal bool InFirstCardAreaRow
		{
			get	{ return this.inFirstCardAreaRow; }
			set { this.inFirstCardAreaRow = value; }
		}

		internal bool InLastCardAreaCol
		{
			get	{ return this.inLastCardAreaCol; }
			set { this.inLastCardAreaCol = value; }
		}

		// SSP 8/6/03 - Row Layout Functionality
		// 
		internal Size GetLayoutContainerSize( )
		{
			// SSP 8/6/03
			// Take into account the auto preview area if there is one. It's not part of the layout.
			// Commented out the original code and added the new code below it.
			//
			// --------------------------------------------------------------------------------------
			//return rowCellAreaElem.Rect.Size;
			Rectangle rect = this.Rect;

			UltraGridBand band = null != this.Row ? this.Row.Band : null;
			if ( null != band && band.AutoPreviewEnabled )
			{
				// NOTE: This is used only for resizing logic. Not when the row/cell elements are
				// positioned or painted. So calling GetDescendant here is not unjustified from
				// performance perspective.
				//
				RowAutoPreviewUIElement previewArea = (RowAutoPreviewUIElement)this.GetDescendant( typeof( RowAutoPreviewUIElement ) );
				if ( null != previewArea )
					rect.Height = previewArea.Rect.Y - rect.Y;
			}

			// SSP 8/12/03 - Fixed headers
			// Take into account the fact the row cell area element could have been shrinked in
			// the fixed headers mode. However we still need to pass in the whole row cell area
			// rect to the layout manager and not the shrunk rect.
			//
			// ----------------------------------------------------------------------------------
			if ( this.usingFixedHeaders )
			{
				ColScrollRegion csr = (ColScrollRegion)this.GetContext( typeof( ColScrollRegion ), true );

				if ( null != csr && null != band )
				{
					int delta = band.GetFixedHeaders_OriginDelta( csr );
					rect.X -= delta;
					rect.Width += delta;
				}
			}
			// ----------------------------------------------------------------------------------

			return rect.Size;
			// --------------------------------------------------------------------------------------
		}

		// SSP 11/26/03 UWG2344
		// Added PerformAutoSize to the UltraGridRow and perform auto size functionality for rows.
		//
		#region Code for Row Auto-Size functionality

		#region SupportsAutoSizing

		private bool SupportsAutoSizing
		{
			get
			{
				return null != this.Row && this.Adjustable &&
					( RowSizing.Free == this.Row.Band.RowSizingResolved ||
					RowSizing.AutoFree == this.Row.Band.RowSizingResolved );
			}
		}

		#endregion // SupportsAutoSizing

		#region OnDoubleClick

        /// <summary>
        /// Called when the mouse is double clicked on this element.
        /// </summary>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        protected override void OnDoubleClick(bool adjustableArea)
		{
			if ( adjustableArea && this.SupportsAutoSizing )
			{
				this.Row.PerformAutoSize( );
				return;
			}

			base.OnDoubleClick( adjustableArea );
		}

		#endregion // OnDoubleClick

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// Overrode OnMouseEnter and OnMouseLeave so we can invalidate the row if there
		// are hot tracking appearances.
		// 
		#region NAS 5.3 Row, Cell and Header Hot Tracking Appearances

		#region OnMouseEnterLeave

		internal void OnMouseEnterLeave( bool enter )
		{
			RowCellAreaUIElementBase.OnMouseEnterLeaveHelper( this, enter );
		}

		internal static void OnMouseEnterLeaveHelper( UIElement elem, bool enter )
		{
			RowUIElementBase rowElem = null != elem ? (RowUIElementBase)elem.GetAncestor( typeof( RowUIElementBase ) ) : null;
			if ( null != rowElem )
			{
				rowElem.isMouseOverSelectorOrCellArea = enter;

				UltraGridRow row = rowElem.Row;
				if ( null != row )
				{
					UIElement elemToInvalidate = null;
					if ( row.HasRowHotTrackingAppearances )
						elemToInvalidate = rowElem;
					else if ( row.HasRowSelectorHotTrackingAppearances )
						elemToInvalidate = rowElem.GetDescendant( typeof( RowSelectorUIElementBase ) );

					if ( null != elemToInvalidate )
					{
						// SSP 8/28/06 - NAS 6.3
						// Added CellHottrackInvalidationStyle property on the layout.
						// 
						//UltraGrid grid = row.Layout.Grid as UltraGrid;
						//if ( null == grid || grid.InvalidateCellOnMouseEnterLeave )
						CellHottrackInvalidationStyle invalidationStyle = row.Layout.CellHottrackInvalidationStyleResolved;
						if ( CellHottrackInvalidationStyle.Never != invalidationStyle )
						{
							// SSP 9/13/05 BR06344
							// Make sure that cell elems re-position the child elems in case
							// the hot track appearance has Image set on it. For that use
							// the InvalidateElem method of the row.
							// 
							if ( elemToInvalidate is RowUIElementBase )
								row.InvalidateElem( elemToInvalidate, true );
							else 
								elemToInvalidate.DirtyChildElements( );
						}
					}
				}
			}
		}

		#endregion // OnMouseEnterLeave

		#region OnMouseEnter

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseEnter( )
		{
			this.OnMouseEnterLeave( true );

			base.OnMouseEnter( );
		}

		#endregion // OnMouseEnter

		#region OnMouseLeave

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseLeave( )
		{
			this.OnMouseEnterLeave( false );

			base.OnMouseLeave( );
		}

		#endregion // OnMouseLeave

		#endregion // NAS 5.3 Row, Cell and Header Hot Tracking Appearances

		#endregion // Code for Row Auto-Size functionality

		// MRS 3/31/05 - SpanResizing support for RowLayouts
		#region SpanResizing support for RowLayouts

		#region Private / Internal Properties

		#region Grid
		private UltraGrid Grid
		{			
			get
			{
				if (null == this.Row ||
					null == this.Row.Layout ||
					null == this.Row.Layout.Grid )
				{
					return null;
				}

				return this.Row.Layout.Grid.ControlForGridDisplay as UltraGrid;
			}
		}
		#endregion Grid

		#endregion Private / Internal Properties

		#region Private / Internal Methods

		#region SupportsSpanSizing
		private bool SupportsSpanSizing
		{
			get
			{	
				if (!GridUtils.IsControlKeyDown)
					return false;

				UltraGrid grid = this.Grid;
				if (null == grid)
					return false;

				
				return true;
			}
		}
		#endregion SupportsSpanSizing

		#region GetRowLayoutAreaRect
		// MRS 4/11/05 - Calcalate the rect the layout is using
		internal Rectangle GetRowLayoutAreaRect()
		{			
			UltraGridRow row = this.Row;
			UltraGridBand band = row.BandInternal;
			
			Rectangle rowAreaRect = this.Rect;

			int rowPreviewHeight = row.AutoPreviewEnabled ? row.AutoPreviewHeight : 0;
			if ( rowPreviewHeight > 0 )
			{
				// SSP 8/6/03
				// In row layout designer, we don't show the auto preview area because it does not
				// participate in the grid bag layout. So don't add the auto preview in row layout
				// designer.
				//
				DataAreaUIElement dataAreaElem = (DataAreaUIElement)this.Parent.GetAncestor( typeof( DataAreaUIElement ) );

				if ( null == dataAreaElem || ! dataAreaElem.RowLayoutDesignerElement )
				{
					Rectangle rowPreviewRect = new Rectangle(
						rowAreaRect.X,
						rowAreaRect.Bottom - rowPreviewHeight,
						rowAreaRect.Width,
						rowPreviewHeight );
				
					// Adjust the cell area to account for the row preview element.
					//
					rowAreaRect.Height -= rowPreviewHeight;

					// If the cells can merge their borders with the row preview element, then
					// merge cells' bottom borders with the row preview area's top borders.
					//
					if ( band.Layout.CanMergeAdjacentBorders( this.BorderStyle ) )
						rowAreaRect.Height++;
				}
			}
			
			// Layout container code assumes that the container rect (rowAreaRect) passed in includes
			// the row selectors. So add the row selector extent here and then the layout container
			// code will substract it. The reason for is that the headers and summaries include the
			// row selector extent. So to be uniform for all of these three layout areas, we are
			// passing in the area that includes the row selectors.
			//
			int rowSelectorWidth = band.RowSelectorExtent;
			if ( rowSelectorWidth > 0 )
			{
				// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
				// Use the borderStyle member var.
				//
				//if ( band.Layout.CanMergeAdjacentBorders( band.BorderStyleRowResolved ) )
				if ( band.Layout.CanMergeAdjacentBorders( this.borderStyle ) )
					rowSelectorWidth--;

				rowAreaRect.X -= rowSelectorWidth;
				rowAreaRect.Width += rowSelectorWidth;
			}

			// SSP 5/23/03 - Fixed headers
			// Take into account the fact the row cell area element could have been shrinked in
			// the fixed headers mode. However we still need to pass in the whole row cell area
			// rect to the layout manager and not the shrunk rect.
			//
			if ( this.usingFixedHeaders )
			{
				ColScrollRegion csr = (ColScrollRegion)this.GetContext( typeof( ColScrollRegion ), true );

				if ( null != csr )
				{
					int delta = band.GetFixedHeaders_OriginDelta( csr );
					rowAreaRect.X -= delta;
					rowAreaRect.Width += delta;
				}
			}

			rowAreaRect = LayoutContainerHelper.GetContainerRectHelper(band, rowAreaRect, band.BorderStyleRowResolved, band.BorderStyleCellResolved);

			return rowAreaRect;
		}
		#endregion GetRowLayoutAreaRect

		#endregion Private / Internal Methods

		#endregion SpanResizing support for RowLayouts
	}

	#endregion // RowCellAreaUIElementBase Class

	#region RowCellAreaUIElement Class

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Split the RowCellAreaUIElement into RowCellAreaUIElementBase class and 
	// RowCellAreaUIElement class so the filter cell area ui element can derive from 
	// RowCellAreaUIElementBase .
	//
	/// <summary>
	/// The RowCellAreaUIElement contains the CellUIElements for a specific row.
	/// </summary>
	public class RowCellAreaUIElement : RowCellAreaUIElementBase
	{
		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		// SSP 11/18/05
		// Made the constructor public from internal. UI elements should to be public.
		// 
		public RowCellAreaUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region CreateCellUIElement

		/// <summary>
		/// Method for creating CellUIElementBase derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>The existing CellUIElementBase or a new one if there was no existing instance.</returns>
		protected override CellUIElementBase CreateCellUIElement( UIElementsCollection oldElems )
		{
			CellUIElement elem = (CellUIElement)RowCellAreaUIElement.ExtractExistingElement( oldElems, typeof( CellUIElement ), true );
			return null != elem ? elem : new CellUIElement( this );
		}

		#endregion // CreateCellUIElement

	}

	#endregion // RowCellAreaUIElement Class

	#region RowPromptUIElement Class

	// SSP 3/30/05 - NAS 5.2 Filter Row
	// Added ability to display prompts in add-rows. Also the new filter row feature supports 
	// prompts in filter rows as well.
	//
	/// <summary>
	/// UI element used for displaying a prompt in template add-row or a filter row.
	/// </summary>
	public class RowPromptUIElement : UIElement
	{
		#region Private Vars

		private string prompt = null;

		#endregion // Private Vars

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public RowPromptUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region InitAppearance 

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags flags)
		{
			this.Row.ResolveRowPromptAppearance( ref appData, ref flags );
		}

		#endregion // InitAppearance 

		#region AddRowPromptElementHelper

		// SSP 3/30/05 - NAS 5.2
		// Added ability to display prompts in add-rows. Also the new filter row feature displays
		// prompts in filter rows as well. Added AddRowPromptElementHelper method.
		//
		internal static void AddRowPromptElementHelper( 
			UltraGridRow row,
			UIElement parentElem,
			UIElementsCollection oldElems,
			RowPromptUIElement oldRowPromptElem )
		{
			string promptText = row.RowPromptResolved;
			if ( null != promptText && promptText.Length > 0 && ! row.IsActiveRowInternal )
			{
				UltraGridColumn promptCol = row.Band.SpecialRowPromptFieldResolved;
				if ( parentElem is CellUIElementBase )
				{
					// Only add the prompt to the cell specified by the SpecialRowPromptField.
					//
					if ( null == promptCol || promptCol != ((CellUIElementBase)parentElem).Column )
						return;
				}
					// If there is a prompt col specified by the SpecialRowPromptField property then 
					// don't add the prompt to the row.
					//
				else if ( null != promptCol )
					return;

				RowPromptUIElement promptElem = null != oldRowPromptElem 
					? oldRowPromptElem
					: (RowPromptUIElement)RowCellAreaUIElementBase.ExtractExistingElement( oldElems, typeof( RowPromptUIElement ), true );

				if ( null == promptElem )
					promptElem = new RowPromptUIElement( parentElem );

				promptElem.Prompt = promptText;

				Rectangle parentRect = parentElem.RectInsideBorders;
				Rectangle rect = parentRect;

				// If prompt is displayed in a cell then make it occupy the entire cell. If it's 
				// displayed in the row cell area then size it to the prompt text.
				//
				if ( ! ( parentElem is CellUIElementBase ) )
				{
					Size size = row.GetRowPromptSize( parentRect.Width );
					rect.Width = Math.Min( parentRect.Width, size.Width );
					rect.Height = Math.Min( parentRect.Height, size.Height );
				}
				else
				{
					// MRS 2/21/06 - BR09279
					// If this is a filter row cell, Position the text over the editor, 
					// so that the text does not cover the buttons. 
					if (row.IsFilterRow)
					{
						UIElement editElem = GridUtils.GetChild(parentElem, typeof(EmbeddableUIElementBase));
						if (editElem != null)
						{
							// Get the embeddable editor UIElement and draw the text inside of it. 
							rect = editElem.Rect;							

							// if the Editor is an EditorWithCombo, which is will be by default, 
							// account for the dropdown button. 
							EditorWithComboUIElement comboElem = editElem as EditorWithComboUIElement;
							if (comboElem != null)
								rect.Width -= SystemInformation.VerticalScrollBarWidth;
						}
					}
				}

				// In card-view make the prompt span the entire width of the card row
				// and in non-card-view make the prompt span the entire height of the row.
				//
				if ( row.IsCard )
					rect.Width = Math.Max( rect.Width, parentRect.Width );
				else
					rect.Height = Math.Max( rect.Height, parentRect.Height );

				promptElem.Rect = rect;
				parentElem.ChildElements.Add( promptElem );
			}
		}

		#endregion // AddRowPromptElementHelper

		#region Row

		/// <summary>
		/// Returns the associated row.
		/// </summary>
		public UltraGridRow Row
		{
			get
			{
				return (UltraGridRow)this.GetContext( typeof( UltraGridRow ) );
			}
		}

		#endregion // Row

		#region Prompt

		/// <summary>
		/// Gets or sets the text this element displays.
		/// </summary>
		public string Prompt
		{
			get
			{
				return null != this.prompt ? this.prompt : string.Empty;
			}
			set
			{
				this.prompt = value;
			}
		}

		#endregion // Prompt

		#region PositionChildElements

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void PositionChildElements( )
		{
			TextUIElement textElem = (TextUIElement)RowPromptUIElement.ExtractExistingElement( 
						this.childElementsCollection, typeof( TextUIElement ), true );
			this.ChildElements.Clear( );

			if ( null == textElem )
				textElem = new TextUIElement( this, this.Prompt );
			else
				textElem.Text = this.Prompt;

			textElem.Rect = this.RectInsideBorders;
            this.ChildElements.Add( textElem );
		}

		#endregion // PositionChildElements

		#region WantsInputNotification

        /// <summary>
        /// Returns true if this ui element is interested in getting notificaions of type inputType at the specified location. Default implementation always returns true.
        /// </summary>
        /// <param name="inputType">The type of notification.</param>
        /// <param name="point">Point of interest.</param>
        /// <returns>true if this ui element is interested in getting notificaions of type inputType at the specified location. Default implementation always returns true.</returns>
        protected override bool WantsInputNotification(UIElementInputType inputType, Point point)
		{
			// Let mouse messages fall through to the row underneath so the cell can go into 
			// edit mode, etc, except for the MouseHover which we use for the tooltip.
			//
			return UIElementInputType.MouseHover == inputType;
		}

		#endregion // WantsInputNotification

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				UltraGridRow row = this.Row;
				StyleUtils.Role eRole;
				if ( row.IsFilterRow )
					eRole = StyleUtils.Role.FilterRowPrompt;
				else if ( row.IsAddRow )
					eRole = StyleUtils.Role.TemplateAddRowPrompt;
				else
					return null;

				return StyleUtils.GetRole( row.BandInternal, eRole );
			}
		}

		#endregion // UIRole
	}

	#endregion // RowPromptUIElement Class
}
