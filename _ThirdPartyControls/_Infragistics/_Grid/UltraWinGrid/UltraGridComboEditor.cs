#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Editor for using an <see cref="UltraCombo"/>.
	/// </summary>
	public class UltraGridComboEditor : EditorWithCombo, IEmbeddableTextBoxListener
	{
		#region Member Variables

		private UltraCombo				control;
		private bool					createdCombo = true;

		// JAS 3/4/05 BR00644 - This is necessary because if the UltraGridComboEditor
		// is being used as an external editor in the grid, it needs to know that it is
		// being used externally.  It needs to know this because it has to reset its
		// selected index when exiting edit mode ONLY IF it is an external editor.
		//
		private readonly bool isExternalEditor = true;

		#endregion //Member Variables

		#region Constructor

		// JAS 3/4/05 BR00644 - Created a constructor which allows you to inform the editor if it is being used
		// internally or externally.  It needs to know this because it has to reset its selected index when
		// exiting edit mode ONLY IF it is an external editor.
		//
		internal UltraGridComboEditor(UltraCombo combo, bool isExternalEditor) : this(combo, null)
		{
			this.isExternalEditor = isExternalEditor;
		}


		/// <summary>
		/// Initializes a new <see cref="UltraGridComboEditor"/>
		/// </summary>
		public UltraGridComboEditor() : this(null, null)
		{
		}

		/// <summary>
		/// Initializes a new <see cref="UltraGridComboEditor"/>
		/// </summary>
		/// <param name="defaultOwner">An instance of the default EmbeddableEditorOwnerBase-derived class from which to obtain owner-specific information</param>
		public UltraGridComboEditor(EmbeddableEditorOwnerBase defaultOwner) : this(null, defaultOwner)
		{
		}

		/// <summary>
		/// Initializes a new <see cref="UltraGridComboEditor"/>
		/// </summary>
		/// <param name="combo">UltraCombo control that will provide the dropdown for the editor.</param>
		public UltraGridComboEditor(UltraCombo combo) : this(combo, null)
		{
		}

		/// <summary>
		/// Initializes a new <see cref="UltraGridComboEditor"/>
		/// </summary>
		/// <param name="combo">UltraCombo control that will provide the dropdown for the editor.</param>
		/// <param name="defaultOwner">An instance of the default EmbeddableEditorOwnerBase-derived class from which to obtain owner-specific information</param>
		public UltraGridComboEditor(UltraCombo combo, EmbeddableEditorOwnerBase defaultOwner) : base(defaultOwner)
		{
			this.control = combo;
			// AS 1/9/04 UWG2866
			//this.createdCombo = this.control != null;
			this.createdCombo = this.control == null;
		}
		#endregion //Constructor

		#region Properties

		#region Control
		/// <summary>
		/// Returns the associated <see cref="UltraCombo"/>
		/// </summary>
		public UltraCombo Control
		{
			get
			{
				if (this.control == null)
				{
					// AS 1/9/04 UWG2866
					// Use a special constructor
					// so the combo doesn't make its editor
					// the owner of the valuelist.
					//
					//this.control = new UltraCombo();
					this.control = new UltraCombo(this);
					this.control.Size = Size.Empty;
				}

				return this.control;
			}
		}
		#endregion //Control

		#region ValueList
		/// <summary>
		/// Returns the <see cref="UltraCombo"/> that provides the dropdown for the combo.
		/// </summary>
		public override IValueList ValueList
		{
			get
			{
				return this.Control;
			}
		}
		#endregion //ValueList

		#endregion //Properties

		#region Methods

		#region Clone
		/// <summary>
		/// Creates a copy of the embeddable editor with the specified default owner.
		/// </summary>
		/// <param name="defaultOwner">An instance of the default EmbeddableEditorOwnerBase-derived class from which to obtain owner-specific information</param>
		/// <returns>A copy of the editor</returns>
		public override EmbeddableEditorBase Clone(EmbeddableEditorOwnerBase defaultOwner)
		{
			UltraGridComboEditor editor = new UltraGridComboEditor(this.control, defaultOwner);

			editor.InitializeFrom( this );

			return editor;
		}
		#endregion //Clone

		// In the case of the UltraCombo, setting the SelectedIndex
		// is going to cause the Value to be updated, which we do
		// not want to do when we are exiting edit mode.
		//
		#region ResetValueListOnExitEditMode
		/// <summary>
		/// Determines whether the valuelist's selected item index should be reset to -1 when exiting edit mode.
		/// </summary>
		protected override bool ResetValueListOnExitEditMode
		{
			// JAS 3/4/05 BR00644 - It needs to reset its selected index when
			// exiting edit mode ONLY IF it is an external editor. If the editor
			// was being used in the grid, then it was impossible to give two cells
			// in the same column the same value via the AutoEdit functionality.  This
			// is because when the valuelist's selected index was being set, the UltraCombo
			// noticed that the new index and the current selected index were the same, so it
			// bypassed the rest of the method.
			//
			//get { return false; }
			get { return this.isExternalEditor; }
		}
		#endregion //ResetValueListOnExitEditMode

		// AS 11/18/03 [UltraCombo as Editor]
		// I need to get the MouseWheel notification from
		// the textbox but I still want the combo editor to handle it
		// for itself. Unfortunately I cannot reimplement this method
		// on the interface and call the base implementation since
		// its explicitly defined. Instead, I've made a protected virtual
		// method that this method will invoke and therefore I can
		// override in the editor for the UltraCombo control.
		//
		#region OnTextBoxMouseWheel
		/// <summary>
		/// Invoked when the MouseWheel event of the associated embedded 
		/// textbox is invoked.
		/// </summary>
		/// <param name="e">Mouse event arguments</param>
		protected override void OnTextBoxMouseWheel( MouseEventArgs e )
        {
            //  BF 11/13/08 TFS10297
            //
            //  1. We should not be calling the base class implementation first here,
            //  because that executes the mouse wheel logic, and we don't know at this
            //  point if that's what they want since they haven't had a chance to mark
            //  the MouseWheel event handled.
            //
            //  2. Since we only support CLR2 and up now, I removed the CLR1 code.
            //
            //  3. Why are we creating a new instance of the HandledMouseEventArgs?
            //  Unless there is a good reason, we probably shouldn't because then 
            //  we are creating a new instance each time the wheel is rolled, which
            //  fragments the heap.
            //
            #region Obsolete code
            //            // call the base first
            //            base.OnTextBoxMouseWheel(e);

            //            // now we can call off the combo's mousewheel

            //            // allow the ultracombo to pass along the event 
            //            // notification to maintain backward compatibility			
            //            Point pt = MapTextBoxPtToCombo( new Point(e.X, e.Y) );


            //#if CLR2
            //            // MRS 10/18/07 - BR27522
            //            // The code below which creates a new MouseEventArgs is dangerous, because the event args
            //            // might have been a HandledMouseEventArgs, in which case, the Handled flag would be lost. 
            //            // So check to see if we should created a HandledMouseEventArgs, instead. 
            //            HandledMouseEventArgs handledMouseEventArgs = e as HandledMouseEventArgs;
            //            if (handledMouseEventArgs != null)
            //            {
            //                HandledMouseEventArgs newHandledArgs = new HandledMouseEventArgs(e.Button, e.Clicks, pt.X, pt.Y, e.Delta, handledMouseEventArgs.Handled);
            //                this.control.InvokeTextBoxEvent(UltraCombo.TextBoxEvent.MouseWheel, newHandledArgs);
            //                return;
            //            }
            //#endif

            //            MouseEventArgs newArgs = new MouseEventArgs(e.Button, e.Clicks, pt.X, pt.Y, e.Delta);
            //            this.control.InvokeTextBoxEvent(UltraCombo.TextBoxEvent.MouseWheel, newArgs);
            #endregion Obsolete code


            // allow the ultracombo to pass along the event 
            // notification to maintain backward compatibility			
            Point pt = MapTextBoxPtToCombo( new Point(e.X, e.Y) );

            bool handled = false;

            // MRS 10/18/07 - BR27522
            // The code below which creates a new MouseEventArgs is dangerous, because the event args
            // might have been a HandledMouseEventArgs, in which case, the Handled flag would be lost. 
            // So check to see if we should created a HandledMouseEventArgs, instead. 
            HandledMouseEventArgs handledMouseEventArgs = e as HandledMouseEventArgs;
            if (handledMouseEventArgs != null)
            {
                HandledMouseEventArgs newHandledArgs = new HandledMouseEventArgs(e.Button, e.Clicks, pt.X, pt.Y, e.Delta, handledMouseEventArgs.Handled);
                this.control.InvokeTextBoxEvent(UltraCombo.TextBoxEvent.MouseWheel, newHandledArgs);

                handled = newHandledArgs.Handled;
            }

            //  Only call the base implementation (which is not an event-raising method)
            //  if the MouseWheel event was not marked handled, since this method actually
            //  executes our special mouse wheel logic.
            if ( handled == false )
                base.OnTextBoxMouseWheel(e);

		}
		#endregion //OnTextBoxMouseWheel

		// In order to maintain backward compatibility, we need
		// to invoke the methods of the UltraCombo when something happens
		// to the textbox it contains. For the mouse events,
		// we need to map coordinates from the textbox to the combo.
		//
		#region MapTextBoxPtToCombo
		private Point MapTextBoxPtToCombo( Point textboxPt )
		{
			System.Windows.Forms.Control source = this.TextBox;
			System.Windows.Forms.Control dest = this.control;

			if (source != null && dest != null &&
				!source.IsDisposed && !dest.IsDisposed)
			{
				Point pt = source.PointToScreen(textboxPt);
				return dest.PointToClient(pt);
			}
			else
				return textboxPt;
		}
		#endregion //MapTextBoxPtToCombo

		// AS 1/9/04 UWG2866
		// The control needs to be in a control's collection.
		//
		#region OnAfterEnterEditMode
		/// <summary>
		/// Invoked after the editor has entered edit mode
		/// </summary>
		protected override void OnAfterEnterEditMode()
		{
			this.VerifyNotDisposed();

			base.OnAfterEnterEditMode();

			// MRS 10/19/05 - BR06916
			// This code was originally intended to work around an issue where
			// the Control is created in code with no parent, and therefore no 
			// BindingContext. A better workaround has since been implemented
			// wherein the BindingContext Get just creates a new BindingContext
			// if there isn't one. So this code is no longer neccessary.
//			if (this.createdCombo)
//			{
//				Control ownerControl = this.ElementBeingEdited.Owner.GetControl(this.ElementBeingEdited.OwnerContext);
//
//				ownerControl.Controls.Add(this.Control);
//
//				if ( !this.Control.Created )
//					this.Control.CreateControl();
//
//				this.Control.VerifyDataSourceAttached();
//			}

		}
		#endregion //OnAfterEnterEditMode

		#region OnAfterExitEditMode
		/// <summary>
		/// Invoked after the editor has exited edit mode
		/// </summary>
		protected override void OnAfterExitEditMode()
		{
			base.OnAfterExitEditMode();

			// MRS 10/19/05 - BR06916
			// This code was originally intended to work around an issue where
			// the Control is created in code with no parent, and therefore no 
			// BindingContext. A better workaround has since been implemented
			// wherein the BindingContext Get just creates a new BindingContext
			// if there isn't one. So this code is no longer neccessary.
//			// unparent it
//			if (this.createdCombo)
//				this.Control.Parent = null;

		}
		#endregion //OnAfterExitEditMode

		// AS 1/9/04 UWG2866
		// Make sure we dispose the combo if we created it.
		//
		#region OnDispose
		/// <summary>
		/// Invoked when the editor is disposed.
		/// </summary>
		protected override void OnDispose()
		{
			if (this.createdCombo &&
				this.control != null &&
				!this.control.Disposing &&
				!this.control.IsDisposed)
			{
				this.control.Dispose();
				this.control = null;
			}

			base.OnDispose();
		}
		#endregion //OnDispose

		#region ProcessOnMouseWheel
		// MRS 2/17/06 - BR09325
		/// <summary>
		/// Allows the editor to handle the owner's mouse wheel event
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void ProcessOnMouseWheel(System.Windows.Forms.MouseEventArgs e)
		{
			this.Control.OnMouseWheelInternal(e);
		}
		#endregion ProcessOnMouseWheel

		#endregion //Methods

		// In order to maintain backward compatibility, we need
		// to invoke the methods of the UltraCombo when something happens
		// to the textbox it contains.
		//
		#region IEmbeddableTextBoxListener interface
		void IEmbeddableTextBoxListener.OnClick()
		{
			// allow the ultracombo to pass along the event 
			// notification to maintain backward compatibility
			this.control.InvokeTextBoxEvent(UltraCombo.TextBoxEvent.Click, EventArgs.Empty);
		}

		void IEmbeddableTextBoxListener.OnDoubleClick()
		{
			// allow the ultracombo to pass along the event 
			// notification to maintain backward compatibility
			this.control.InvokeTextBoxEvent(UltraCombo.TextBoxEvent.DoubleClick, EventArgs.Empty);
		}

		void IEmbeddableTextBoxListener.OnMouseHover()
		{
			// allow the ultracombo to pass along the event 
			// notification to maintain backward compatibility
			this.control.InvokeTextBoxEvent(UltraCombo.TextBoxEvent.MouseHover, EventArgs.Empty);
		}

		void IEmbeddableTextBoxListener.OnMouseMove( MouseEventArgs e )
		{
			// allow the ultracombo to pass along the event 
			// notification to maintain backward compatibility
			Point pt = MapTextBoxPtToCombo( new Point(e.X, e.Y) );
			MouseEventArgs newArgs = new MouseEventArgs(e.Button, e.Clicks, pt.X, pt.Y, e.Delta);

			this.control.InvokeTextBoxEvent(UltraCombo.TextBoxEvent.MouseMove, newArgs);
		}

		void IEmbeddableTextBoxListener.OnMouseDown( MouseEventArgs e )
		{
			// allow the ultracombo to pass along the event 
			// notification to maintain backward compatibility
			Point pt = MapTextBoxPtToCombo( new Point(e.X, e.Y) );
			MouseEventArgs newArgs = new MouseEventArgs(e.Button, e.Clicks, pt.X, pt.Y, e.Delta);

			this.control.InvokeTextBoxEvent(UltraCombo.TextBoxEvent.MouseDown, newArgs);
		}

		void IEmbeddableTextBoxListener.OnMouseUp( MouseEventArgs e )
		{
			// allow the ultracombo to pass along the event 
			// notification to maintain backward compatibility
			Point pt = MapTextBoxPtToCombo( new Point(e.X, e.Y) );
			MouseEventArgs newArgs = new MouseEventArgs(e.Button, e.Clicks, pt.X, pt.Y, e.Delta);

			this.control.InvokeTextBoxEvent(UltraCombo.TextBoxEvent.MouseUp, newArgs);
		}
		#endregion //IEmbeddableTextBoxListener interface

		#region Data Filter Related

		/// <summary>
		/// Automatic data conversions specific to this editor.
		/// </summary>
		/// <param name="direction">Type of conversion.</param>
		/// <param name="sourceVal">Value to convert.</param>
		/// <param name="destinationType">Type of converted value</param>
		/// <param name="isValidConversion">True if conversion was valid.</param>
		/// <param name="owner">Owner.</param>
		/// <param name="ownerContext">OwnerContext.</param>
		/// <returns>Converted value.</returns>
		protected override object GetAutoConvertedDestinationValue(ConversionDirection direction, object sourceVal, 
			Type destinationType, out bool isValidConversion, EmbeddableEditorOwnerBase owner, object ownerContext)
		{
			// ZS 2/2/2004 - UWG2939
			if(direction==ConversionDirection.EditorToOwner ||
				direction==ConversionDirection.OwnerToEditor)
			{
                //  BF 1/28/09  TFS12030
                if ( this.IsValueProvidedByCheckedItems(owner, ownerContext) )
                    return base.GetAutoConvertedDestinationValue(direction, sourceVal, destinationType, out isValidConversion, owner, ownerContext);

				isValidConversion = true;
				return sourceVal;
			}
			else
			{
				return base.GetAutoConvertedDestinationValue(direction, sourceVal, destinationType, 
					out isValidConversion, owner, ownerContext);
			}		
		}

		// MRS 6/8/05 - BR04469
		// This is just copying the code from the base class, so 
		// there's no reason to even bother overriding this method. 
//		/// <summary>
//		/// Checks validity of converted values.
//		/// </summary>
//		/// <param name="direction">Type of conversion.</param>
//		/// <param name="destinationVal">Value to check.</param>
//		/// <param name="owner">Owner.</param>
//		/// <param name="ownerContext">Owner context.</param>
//		/// <param name="message">Additional information in the case of failure.</param>
//		/// <returns>True if the destination value is valid.</returns>
//		protected override bool IsDestinationValueValid(ConversionDirection direction, object destinationVal, 
//			EmbeddableEditorOwnerBase owner, object ownerContext, out string message)
//		{
//			message = String.Empty;
//
//			// MRS 5/18/05 - BR03693
//			// Zeljko's fix here is a little too wide-reaching. 
//			// Replaced with code copied from UltraComboEditor which 
//			// checks if the item is on the list, etc. 
////			// ZS 2/2/2004 - UWG2939
////			if(direction==ConversionDirection.EditorToOwner ||
////				direction==ConversionDirection.OwnerToEditor)
////			{	
////				message = String.Empty;
////				return true;
////			}
//
//			if(direction==ConversionDirection.EditorToOwner)
//			{		
//				//	BF 6.12.02
//				//	If the editor's value is null, it is valid as long as the owner returns true from IsNullable
//				//	We need to do this so that we don't return false from this method when there is no
//				//	item selected, because that is a valid state to be in if the owner says the value is nullable
//				if ( destinationVal == null && owner.IsNullable( ownerContext ) )
//					return true;
//
//				//	BF 9.30.03	UWG2514
//				//	If there is no ValueList, delegate this to the base class
//				//	(EditorWithText) implementation.
//				if ( this.ValueList == null )
//					return base.IsDestinationValueValid(direction, destinationVal, owner, 
//						ownerContext, out message);
//
//				int lastIndex = this.ValueList.SelectedItemIndex;
//
//				//	If the owner returns true from MustSelectFromList, make sure
//				//	the editor's value matches an item in the ValueList
//				//	Note that with a DropDownList, we don't look at any other criteria,
//				//	so we can return immediately
//				if ( owner.MustSelectFromList( ownerContext ) )
//				{
//					string text = this.ValueList.GetText( destinationVal, ref lastIndex );
//					return( text != null );
//				}
//				//	If the owner returns true from EnsureValueIsInList, we must
//				//	verify that the text in the textbox matches an item in the value list
//				//	Note that if the text does correspond to an item in the list, we do not
//				//	return true yet, because the value could be invalid for other reasons.
//				//	That will be determined in EditorWithText's implementation
//				if ( owner.EnsureValueIsInList( ownerContext ) )
//				{
//					object val = this.ValueList.GetValue( this.GetDisplayValue(), ref lastIndex );
//					if ( val == null )
//						return false;
//				}
//
//				//	Call the base class implementation
//				return base.IsDestinationValueValid(direction, destinationVal, owner, ownerContext, out message);
//			}
//			else
//			{
//				return base.IsDestinationValueValid(direction, destinationVal, owner, ownerContext, out message);
//			}		
//		}

		#endregion

        #region BF NA 9.1 - UltraCombo MultiSelect

            #region InternalRaiseValueChangedEvent
        internal void InternalRaiseValueChangedEvent() { this.RaiseValueChangedEvent(); }
            #endregion InternalRaiseValueChangedEvent

            #region RaiseValueChangedWhenCheckStateChanged
        /// <summary>
        /// Returns whether the editor raises the ValueChanged event
        /// in response to the ICheckedItemList implementor's CheckStateChanged event.
        /// </summary>
        protected override bool RaiseValueChangedWhenCheckStateChanged
        {
            get { return false; }
        }
            #endregion RaiseValueChangedWhenCheckStateChanged

        //  BF 12/24/08
            #region IsValueProvidedByCheckedItems
        /// <summary>
        /// Returns whether the editor's value is determined by the
        /// <see cref="Infragistics.Win.ICheckedItemList">ICheckedItemList</see> implementation.
        /// </summary>
        internal bool IsValueProvidedByCheckedItems( EmbeddableUIElementBase embeddableElement )
        {
            EmbeddableEditorOwnerBase owner = embeddableElement != null ? embeddableElement.Owner : null;
            object ownerContext = embeddableElement != null ? embeddableElement.OwnerContext : null;
            return owner != null ? this.IsValueProvidedByCheckedItems( owner, ownerContext ) : false;
        }

        /// <summary>
        /// Returns whether the editor's value is determined by the
        /// <see cref="Infragistics.Win.ICheckedItemList">ICheckedItemList</see> implementation.
        /// </summary>
        protected override bool IsValueProvidedByCheckedItems( EmbeddableEditorOwnerBase owner, object ownerContext )
        {
            //  If there is no valid "check state" column, we can't
            //  provide a value from the checked item list, so return
            //  false right away if that is the case.
            UltraCombo ultraCombo = this.Control;
            if ( ultraCombo == null || ultraCombo.CheckStateColumn == null )
                return false;

            return base.IsValueProvidedByCheckedItems( owner, ownerContext );
        }

        internal bool InternalIsValueProvidedByCheckedItems{ get { return this.IsValueProvidedByCheckedItems( this.ElementBeingEdited ); } }

            #endregion IsValueProvidedByCheckedItems

        #endregion BF NA 9.1 - UltraCombo MultiSelect
	}


}
