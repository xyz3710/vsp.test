#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved


namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Collections;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Selected
	{
		#region Private fields

		private SelectedCellsCollection cells	= null;
		private SelectedRowsCollection  rows	= null;
		private SelectedColsCollection  columns	= null;

		// SSP 12/11/02 UWG1835
		// Added a backreference to the grid.
		//
		private UltraGrid grid = null;

		// SSP 2/20/07 BR19262
		// 
		private bool inVerifyRowsNotDirty = false;
		internal bool needsToVerifyRowCollections = false;
		
		#endregion

		/// <summary>
		/// Returns a reference to a Selected object containing collections of all the selected objects in the grid. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Selected</b> property of the UltraWinGrid is used to work with any of the currently selected items in the grid. It provides access to the Selected object, which contains three collection sub-objects. Each collection holds one type of selected object; there are collections for rows, columns and cells. Whenever an UltraGridRow, UltraGridColumn or UltraGridCell object in the grid is selected, it is added to the corresponding collection of the Selected object. Deselecting an object removes it from the collection.</p>
		/// <p class="body">You can use the <b>Selected</b> property to iterate through the selected items of any type, or to examine or change the properties of any selected item.</p>
		/// </remarks>
		public Selected()
		{
		}

		#region Initialize

		
		// SSP 12/11/02 UWG1835
		// Added backreference to the grid.
		//
		internal void Initialize( UltraGrid grid )
		{
			this.grid = grid;
		}

		#endregion // Initialize

		#region Grid

		// SSP 12/11/02 UWG1835
		// Added backreference to the grid for the purposes of accessing the main grid from
		// the Selected rows/columns/cells collection objects. I'm only initializing the
		// the main selected of the UltraGrid (ie. UltraGrid.Selected ) with the grid. Other
		// temporary selected objects are not being initialized as there is no need for them
		// to be initialized and there are 12 places where these things are being created.
		//
		internal UltraGrid Grid
		{
			get
			{
				return this.grid;
			}
		}

		#endregion // Grid

		/// <summary>
		/// Returns a reference to a collection of the selected UltraGridCell objects. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a collection of UltraGridCell objects that can be used to retrieve references to the UltraGridCell objects that are currently selected. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
		/// <p class="body">As cells are selected and deselected, their corresponding UltraGridCell objects are added to and removed from the SelectedCells collection returned by this property. When a cell is selected or deselected, the <b>BeforeSelectChange</b> event is generated.</p>
		/// <p class="body">The <b>Count</b> property of the returned SelectedCells collection can be used to determine the number of cells that either belong to a row or are currently selected.</p>
		/// </remarks>
		public SelectedCellsCollection Cells
		{
			get 
			{
				if ( this.cells == null )
				{
					this.cells = new SelectedCellsCollection();

					// SSP 12/11/02 UWG1835
					//
					this.cells.Initialize( this );
				}

				return this.cells;
			}
		}

		/// <summary>
		/// Returns a reference to a collection of the selected UltraGridRow objects. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a collection of UltraGridRow objects that can be used to retrieve references to the UltraGridRow objects that are currently selected. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
		/// <p class="body">As rows are selected and deselected, their corresponding UltraGridRow objects are added to and removed from the SelectedRows collection returned by this property.</p>
		/// <p class="body">When a row is selected or deselected, the <b>BeforeSelectChange</b> event is generated.</p>
		/// <p class="body">The <b>Count</b>property of the returned SelectedRows collection can be used to determine the number of rows currently selected.</p>
		/// </remarks>
		public SelectedRowsCollection  Rows
		{
			get
			{
				if ( this.rows == null )
				{
					this.rows = new SelectedRowsCollection();

					// SSP 12/11/02 UWG1835
					//
					this.rows.Initialize( this );
				}

				return this.rows;
			}
		}

		/// <summary>
		/// Returns a reference to a collection of the selected UltraGridColumn objects. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a collection of UltraGridColumn objects that can be used to retrieve references to the UltraGridColumn objects that are currently selected. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
		/// <p class="body">As columns are selected and deselected, their corresponding UltraGridColumn objects are added to and removed from the SelectedCols collection returned by this property.</p>
		/// <p class="body">When a column is selected or deselected, the <b>BeforeSelectChange</b> event is generated.</p>
		/// <p class="body">The <b>Count</b>property of the returned SelectedCols collection can be used to determine the number of columns currently selected.</p>
		/// </remarks>
		public SelectedColsCollection Columns
		{
			get
			{
				if ( this.columns == null )
				{
					this.columns = new SelectedColsCollection();

					// SSP 12/11/02 UWG1835
					//
					this.columns.Initialize( this );
				}

				return this.columns;
			}
		}

		// SSP 12/17/02
		// Optimizations.
		// Added HasCells, HasRows and HasColumns methods so we don't access the respective
		// properties and allocate cells when all we want to find out is whether we
		// have cells or not.
		//
		internal bool HasCells
		{
			get
			{
				return null != this.cells && this.cells.Count > 0;
			}
		}

		internal bool HasRows
		{
			get
			{
				return null != this.rows && this.rows.Count > 0;
			}
		}

		// AS 1/17/06 BR09041
		// When groupby rows are selected, we don't want to consider
		// the selected object as having rows that it can act upon since
		// there are no cells to cut/copy/paste.
		//
		internal bool HasGroupByRows
		{
			get
			{
				return null != this.rows && this.rows.Count > 0 && this.rows[0].IsGroupByRow;
			}
		}

		internal bool HasColumns
		{
			get
			{
				return null != this.columns && this.columns.Count > 0;
			}
		}

		internal void SetColumns( SelectedColsCollection newColumns )
		{
			this.columns = newColumns;

			// SSP 12/11/02 UWG1835
			// Initilize the columns with this selected if the columns has none set.
			//
			if ( null != this.columns && null == this.columns.Selected )
				this.columns.Initialize( this );
		}

		internal void SetCells( SelectedCellsCollection newCells )
		{
			this.cells = newCells;

			// SSP 12/11/02 UWG1835
			// Initilize the cells with this selected if the cells has none set.
			//
			if ( null != this.cells && null == this.cells.Selected )
				this.cells.Initialize( this );
		}

		internal void SetRows( SelectedRowsCollection newRows )
		{
			this.rows = newRows;

			// SSP 12/11/02 UWG1835
			// Initilize the rows with this selected if the rows has none set.
			//
			if ( null != this.rows && null == this.rows.Selected )
				this.rows.Initialize( this );
		}

		// SSP 7/8/02 UWG1328
		// Added a helper method to find out if the item is already in apporpriate
		// collection of selected items.
		//
		internal bool IsItemInCollection( ISelectableItem item )
		{
			if ( null == item )
				return false;

			bool selected = false;

			if ( null != this.rows && this.Rows.Contains( item ) )
				selected = true;
			else if ( null != this.columns && this.Columns.Contains( item ) )
				selected = true;
			else if ( null != this.cells && this.Cells.Contains( item ) )
				selected = true;

			return selected;
		}

		// SSP 1/12/05 BR0155 UWG3736
		// Added RemoveItemFromCollection method.
		//
		#region RemoveItemFromCollection

		internal void RemoveItemFromCollection( ISelectableItem item )
		{
			if ( item is UltraGridRow )
			{
				if ( null != this.rows )
					this.rows.Remove( (UltraGridRow)item );
			}
			else if ( item is UltraGridCell )
			{
				if ( null != this.cells )
					this.cells.Remove( (UltraGridCell)item );
			}
			else if ( item is ColumnHeader )
			{
				if ( null != this.columns )
					this.columns.Remove( (ColumnHeader)item );
			}
			else
				Debug.Assert( false, "Unknown type of item." );
		}

		private static UltraGridBand GetBandFromItem( ISelectableItem item )
		{
			UltraGridBand band = null;
			if ( item is UltraGridRow )
			{
				band = ((UltraGridRow)item).BandInternal;
			}
			else if ( item is UltraGridCell )
			{
				band = ((UltraGridCell)item).Band;
			}
			else if ( item is Infragistics.Win.UltraWinGrid.ColumnHeader )
			{
				band = ((Infragistics.Win.UltraWinGrid.ColumnHeader)item).Band;
			}
			else
				Debug.Assert( false, "Unknown type of item." );

			return band;
		}
        
		internal static void RemoveItemFromCollectionHelper( ISelectableItem item )
		{
			UltraGridBand band = Selected.GetBandFromItem( item );
			if ( null != band )
			{
				UltraGridLayout layout = band.Layout;
				if ( null != layout && ! layout.Disposed )
				{
					UltraGrid grid = layout.Grid as UltraGrid;
					if ( null != grid && grid.HasSelectedBeenAllocated )
					{
						Selected selection = grid.Selected;

						selection.RemoveItemFromCollection( item );
					}
				}
			}
		}

		#endregion // RemoveItemFromCollection

		// SSP 1/12/05 BR0155 UWG3736
		// Added ClearAllSelected method.
		//
		#region ClearAllSelected

		internal void ClearAllSelected( )
		{
			if ( null != this.rows )
			{
				// SSP 7/11/06 BR14276
				// Also set the Selected of all the columns to false.
				// 
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.ClearSelectedFlags( this.rows );
				Selected.ClearSelectedFlags( this.rows );

				this.rows.InternalClear( );
			}

			if ( null != this.columns )
			{
				// SSP 7/11/06 BR14276
				// Also set the Selected of all the columns to false.
				// 
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.ClearSelectedFlags( this.columns );
				Selected.ClearSelectedFlags( this.columns );

				this.columns.InternalClear( );
			}

			if ( null != this.cells )
			{
				// SSP 7/11/06 BR14276
				// Also set the Selected of all the columns to false.
				// 
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.ClearSelectedFlags( this.cells );
				Selected.ClearSelectedFlags( this.cells );

				this.cells.InternalClear( );
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void ClearSelectedFlags( IEnumerable gridItems )
		private static void ClearSelectedFlags( IEnumerable gridItems )
		{
			foreach ( GridItemBase item in gridItems )
				item.InternalSelect( false, false );
		}

		#endregion // ClearAllSelected

		// SSP 10/20/04 UWG3713
		// Added ClearItemsOfType method.
		//
		#region ClearItemsOfType
		
		internal void ClearItemsOfType( ISelectableItem item )
		{
			if ( item is UltraGridRow )
			{
				if ( this.HasRows )
					this.Rows.Clear( );
			}
			else if ( item is UltraGridCell )
			{
				if ( this.HasCells )
					this.Cells.Clear( );
			}
			else if ( item is HeaderBase )
			{
				if ( this.HasColumns )
					this.Columns.Clear( );
			}
			else
				Debug.Assert( false, "Unknown type of item type !" );
		}

		#endregion // ClearItemsOfType

		// SSP 10/27/03 UWG2399
		// Added CompareCollections.
		//
		#region CompareCollections
		
		// SSP 4/12/04 - Virtual Binding Related Optimizations
		//
		//private bool CompareCollections( SubObjectsCollectionBase coll1, SubObjectsCollectionBase coll2 )
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool CompareCollections( ICollection coll1, ICollection coll2 )
		private static bool CompareCollections( ICollection coll1, ICollection coll2 )
		{
			if ( coll1 == coll2 )
				return true;

			int count1 = null != coll1 ? coll1.Count : 0;
			int count2 = null != coll2 ? coll2.Count : 0;

			if ( count1 <= 0 && count2 <= 0 )
				return true;

			if ( count1 != count2 )
				return false;

			bool allSame = true;

			// SSP 4/12/04 - Virtual Binding Related Optimizations
			// Changed the signature of the method to take in two ICollection instances instead
			// if two SubObjectsCollectionBase instances. Commented out the original code and 
			// added the new one below it.
			//
			// ----------------------------------------------------------------------------------
			
			IEnumerator coll2Enumerator = coll2.GetEnumerator( );
			coll2Enumerator.Reset( );

			foreach ( object item in coll1 )
			{
				if ( ! coll2Enumerator.MoveNext( ) || item != coll2Enumerator.Current )
				{
					allSame = false;
					break;
				}
			}
			// ----------------------------------------------------------------------------------

			if ( allSame )
				return true;

			Hashtable hash1 = new Hashtable( coll1.Count );
			Hashtable hash2 = new Hashtable( coll2.Count );

			object dummy = new object( );

			// SSP 4/12/04 - Virtual Binding Related Optimizations
			// Changed the signature of the method to take in two ICollection instances instead
			// if two SubObjectsCollectionBase instances. Commented out the original code and 
			// added the new one below it.
			//
			// ----------------------------------------------------------------------------------
			
			
			foreach ( object item in coll1 )
				hash1[ item ] = dummy;

			foreach ( object item in coll2 )
				hash2[ item ] = dummy;

			if ( hash1.Count != hash2.Count )
				return false;

			foreach ( object key in hash1.Keys )
			{
				if ( ! hash2.ContainsKey( key ) )
					return false;
			}

			foreach ( object key in hash2.Keys )
			{
				if ( ! hash1.ContainsKey( key ) )
					return false;
			}
			// ----------------------------------------------------------------------------------

			return true;
		}

		#endregion // CompareCollections

		// SSP 10/27/03 UWG2399
		// Added IsSelectionSameForItemType;
		//
		#region IsSelectionSameForItemType
	
		internal bool IsSelectionSameForItemType( Type itemType, Selected selectionToCompareTo )
		{
			// MD 8/3/07 - 7.3 Performance
			// IsAssignableFrom is faster than IsSubclassOf
			//if ( typeof( UltraGridRow ) == itemType || itemType.IsSubclassOf( typeof( UltraGridRow ) ) )
			if ( typeof( UltraGridRow ) == itemType || typeof( UltraGridRow ).IsAssignableFrom( itemType ) )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//return this.CompareCollections( this.rows, selectionToCompareTo.rows );
				return Selected.CompareCollections( this.rows, selectionToCompareTo.rows );
			}
			// SSP 4/16/05 - NAS 5.2 Filter Row
			// Added UltraGridFilterCell class.
			//
			//else if ( typeof( UltraGridCell ) == itemType )
			// MD 8/3/07 - 7.3 Performance
			// IsAssignableFrom is faster than IsSubclassOf
			//else if ( typeof( UltraGridCell ) == itemType || itemType.IsSubclassOf( typeof( UltraGridCell ) ) )
			else if ( typeof( UltraGridCell ) == itemType || typeof( UltraGridCell ).IsAssignableFrom( itemType ) )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//return this.CompareCollections( this.cells, selectionToCompareTo.cells );
				return Selected.CompareCollections( this.cells, selectionToCompareTo.cells );
			}
			// MD 8/3/07 - 7.3 Performance
			// IsAssignableFrom is faster than IsSubclassOf
			//else if ( typeof( HeaderBase ) == itemType || itemType.IsSubclassOf( typeof( HeaderBase ) ) )
			else if ( typeof( HeaderBase ) == itemType || typeof( HeaderBase ).IsAssignableFrom( itemType ) )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//return this.CompareCollections( this.columns, selectionToCompareTo.columns );
				return Selected.CompareCollections( this.columns, selectionToCompareTo.columns );
			}
			else
				Debug.Assert( false, "Unknown type of item type !" );

			return false;
		}

		#endregion // IsSelectionSameForItemType
		
		#region SelectionPositionSortComparer

		// SSP 12/13/02 UWG1835
		// Added SelectionPositionSortComparer class to sort selectable objects (rows, cells, headers)
		// by their selection position.
		//
		internal class SelectionPositionSortComparer : IComparer
		{
			internal SelectionPositionSortComparer( )
			{
			}

			internal static int CompareRow( UltraGridRow rowX, UltraGridRow rowY )
			{
				UltraGridRow parentRowX = rowX.ParentRow;
				UltraGridRow parentRowY = rowY.ParentRow;

				if ( null != parentRowX )
				{
					Debug.Assert( null != parentRowY, "Rows are not at the same level !" );
					int r = CompareRow( parentRowX, parentRowY );

					if ( 0 != r )
						return r;
				}

				return rowX.Index.CompareTo( rowY.Index );
			}

			int IComparer.Compare( object x, object y )
			{
				if ( x == y )
					return 0;
				else if ( null == x )
					return 1;
				else if ( null == y )
					return -1;					

				if ( x is UltraGridRow && y is UltraGridRow )
				{
					UltraGridRow xRow = (UltraGridRow)x;
					UltraGridRow yRow = (UltraGridRow)y;

					if ( xRow.Band == yRow.Band )
					{
						return CompareRow( xRow, yRow );
					}

					return xRow.OverallSelectionPosition.CompareTo( yRow.OverallSelectionPosition );
				}
				else if ( x is UltraGridCell && y is UltraGridCell )
				{
					UltraGridCell xCell = (UltraGridCell)x;
					UltraGridCell yCell = (UltraGridCell)y;

					// SSP 1/24/03 UWG1862 Optimizations
					//
					// ----------------------------------------------------------------------------------
					
					int rowCompareVal = ((IComparer)this).Compare( xCell.Row, yCell.Row );

					if ( 0 == rowCompareVal )
						return xCell.Column.Header.VisiblePosition.CompareTo( yCell.Column.Header.VisiblePosition );
					else
						return rowCompareVal;
					// ----------------------------------------------------------------------------------
				}
				else if ( x is HeaderBase && y is HeaderBase )
				{
					HeaderBase xHeader = (HeaderBase)x;
					HeaderBase yHeader = (HeaderBase)y;

					return xHeader.VisiblePosition.CompareTo( yHeader.VisiblePosition );
				}
				else
				{
					throw new ArgumentException( "Passed in objects must be cells or rows." );
				}
			}
		}

		#endregion // SelectionPositionSortComparer

		#region RemoveDuplicateObjects

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        // SSP 1/24/03
		//        // Added RandomizeList method.
		//        //
		//#if DEBUG
		//        /// <summary>
		//        /// Randomizes the list.
		//        /// </summary>
		//        /// <param name="list"></param>
		//#endif
		//        internal static void RandomizeList( int[] list )
		//        {
		//            Random random = new Random( );

		//            for ( int i = list.Length - 1; i >= 0; i-- )
		//            {
		//                int r = random.Next( 0, i );
		//                int tmpObj = list[i];
		//                list[i] = list[r];
		//                list[r] = tmpObj;
		//            }
		//        }

		#endregion Not Used

		// SSP 12/13/02 UWG1835
		// Added RemoveDuplicateObjects method to remove duplicates selectable items from a list.
		// So if the user were to add same rows, cells or headers to the selected collections,
		// then we can merge them rather than have duplicates.
		//
		internal static void RemoveDuplicateObjects( ArrayList list )
		{
			// NOTE: Condition is i > 0 and not i >= 0
			//
			// SSP 1/24/03 UWG1862 - Optimizations
			// Rather than removing individual items, remove all of them at once since
			// removing an item at a time is lot slower since it probably involves shifting the
			// rest of the items left.
			// Commented out the original code and added new code below.
			//
			// -----------------------------------------------------------------------------
			
			int i = 0;
			int j = 0;
			int listCount = list.Count;
			while ( j < listCount )
			{
				while ( j + 1 < listCount && object.ReferenceEquals( list[j], list[j+1] ) )
				{
					j++;
				}

				if ( j >= listCount )
					break;

				list[i] = list[j];
				i++;
				j++;
			}

			if ( i < listCount )
				list.RemoveRange( i, listCount - i );
			// -----------------------------------------------------------------------------
		}

		#endregion // RemoveDuplicateObjects

		#region VerifyRowsNotDirty

		// SSP 2/20/07 BR19262
		// If the grid's row collection has been dirtied (because we recieved Reset notification etc...)
		// then we need to verify that before returning Count here otherwise the SelectedRowsCollection
		// could return rows that have been deleted.
		// 
		internal bool VerifyRowsNotDirty( )
		{
			if ( ! this.needsToVerifyRowCollections )
				return false;

			UltraGrid grid = this.grid;
			if ( this.inVerifyRowsNotDirty || null == grid || this != grid.SelectedIfAllocated )
				return false;

			this.needsToVerifyRowCollections = false;
			this.inVerifyRowsNotDirty = true;
			try
			{
				UltraGridLayout layout = null != grid ? grid.DisplayLayout : null;
				RowsCollection rows = null != layout ? layout.RowsIfAllocated : null;

				if ( null != rows && rows.IsRowsDirty )
				{
					rows.SyncRows( );
					return true;
				}
			}
			finally
			{
				this.inVerifyRowsNotDirty = false;
			}

			return false;
		}

		#endregion // VerifyRowsNotDirty
	}
}
