#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved


namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Collections;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

	/// <summary>
	/// Summary description for SelectedColsCollection.
	/// </summary>
	public class SelectedColsCollection : SubObjectsCollectionBase
	{
		// SSP 12/11/02 UWG1835
		// Added a back reference to the selected object.
		//
		private Selected selected = null;

		// SSP 12/13/02 UWG1835
		private ArrayList list = null;

		/// <summary>
		/// Constructor
		/// </summary>
		public SelectedColsCollection()
		{
			
		}

		/// <summary>
		/// Specifies the initial capacity of the collection
		/// </summary>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		protected override int InitialCapacity
		{
			get
			{
				return 20;
			}
		}

		/// <summary>
		/// returns whether the collection is read-only
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				// SSP 12/11/02 UWG1835
				// Added methods for clearing the selected rows, cells or columns as well as adding a
				// range of objects at the same time. We want to return true only for the main selected
				// object and not for the onces that get passed in various selectchange events.
				//
				//return true;

				return null == this.Grid || this.Selected != this.Grid.Selected || this != this.Grid.Selected.Columns;
			}
		}
		internal int InternalAdd( ColumnHeader selectedCol ) 
		{
			// SSP 7/8/02 UWG1328
			// If the item is already in the collection don't add it again.
			//
			if ( this.Contains( selectedCol ) )
			{
				Debug.Assert( false, "Column is already in the selected columns collection. Can't added it again." );
				return this.IndexOf( selectedCol );
			}

			return base.InternalAdd( selectedCol );
		}

		// SSP 1/12/05 BR0155 UWG3736
		// Added InternalClear method.
		//
		internal new void InternalClear( )
		{
			base.InternalClear( );
		}

		internal void Remove( ColumnHeader selectedCol ) 
		{
			base.InternalRemove( selectedCol );
		}
         
		/// <summary>
        /// returns a type safe enumerator
        /// </summary>
        /// <returns>A type safe enumerator</returns>
        public ColumnHeaderEnumerator GetEnumerator() // non-IEnumerable version
        {	
			return new ColumnHeaderEnumerator(this);
        }

		// JJD 10/04/01
		// Added the indexer
		//
		/// <summary>
		/// Index into collection
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColumnHeader this[ int index ] 
		{
			get
			{
				return (ColumnHeader)base.GetItem(index);
			}
		}

		// SSP 12/11/02 UWG1835
		// Added methods for clearing the selected rows, cells or columns as well as adding a
		// range of objects at the same time.
		//
		#region Fix For UWG1835

		#region List

		// SSP 12/11/02 UWG1835
		// Overrode List so that we can set the list to the list of new SelectedColsCollection
		// rather than having to copy.
		//
		/// <summary>
		/// The list that contains the item references 
		/// </summary>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		protected override ArrayList List 
		{
			get 
			{
				if ( null == this.list )
					this.list = new ArrayList( this.InitialCapacity );

				return this.list;
			}
		}

		#endregion // List

		#region InternalSetList

		internal void InternalSetList( SelectedColsCollection selectedCols )
		{
			// We are setting the list rather than copying for efficiency, but copying will work
			// just as well.
			//
			this.list = selectedCols.list;
		}

		#endregion // InternalSetList
		
		#region HasSameList

		internal bool HasSameList( SelectedColsCollection selectedCols )
		{
			return this.list == selectedCols.list;
		}

		#endregion // HasSameList

		#region All

		// SSP 12/11/02 UWG1835
		// Added methods for clearing the selected rows, cells or columns as well as adding a
		// range of objects at the same time. We want to return true only for the main selected
		// object and not for the onces that get passed in various selectchange events.
		// Overrode All so we can prevent the user from calling the Set.
		//
		/// <summary>
		/// The collection as an array of objects
		/// </summary>
		[ Browsable( false ), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public override object[] All 
		{
			get 
			{
				object[] array;

				array = this.CreateArray();
                
				if (Count > 0) 
					this.List.CopyTo(array, 0); 

				return array;
			}

			set 
			{
				if ( this.IsReadOnly )
					throw new NotSupportedException();

				// Clear existing selected cols and call AddRange to add the passed in cols.
				//
				this.Clear( );

				if ( value.Length > 0 )
				{
					ColumnHeader[] columnHeaders = new ColumnHeader[ value.Length ];

					for ( int i = 0; i < value.Length; i++ )
					{
						columnHeaders[i] = (ColumnHeader)value[i];
					}

					this.AddRange( columnHeaders );
				}
			}
		}

		#endregion // All

		#region Initialize

		internal void Initialize( Selected selected )
		{
			this.selected = selected;
		}

		#endregion // Initialize

		#region Selected

		internal Infragistics.Win.UltraWinGrid.Selected Selected
		{
			get
			{
				return this.selected;
			}
		}

		#endregion // Selected

		#region Grid

		internal UltraGrid Grid
		{
			get
			{
				return null != this.selected ? this.selected.Grid : null;
			}
		}

		#endregion // Grid

		#region Clear

		/// <summary>
		/// Clears the collection unselecting any selected cols.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="AddRange"/>, <seealso cref="HeaderBase.Selected"/></p>
		/// </remarks>
		public void Clear( )
		{			
			UltraGrid grid = this.Grid;

			if ( this.IsReadOnly )
				// SSP 6/23/03 - Localization
				//
				//throw new NotSupportedException( "Clear operation is not supported." );
				throw new NotSupportedException( SR.GetString( "LER_NotSupportedException_346" ) );

			// IsReadOnly should ensure that following condition in Debug.Assert is met before
			// returning true.
			//
			Debug.Assert( this == grid.Selected.Columns, "Grid's selected and this are not the same !" );

			// Call ClearSelectedColumns to clear the selected cols. It also fires the select change
			// events.
			//
			grid.ClearSelectedColumns( );
		}

		#endregion // Clear

		#region AddRange

		/// <summary>
		/// Selects column headers contained in the passed in array keeping the existing selected column headers selected.
		/// </summary>
		/// <param name="columnHeaders"></param>
		/// <remarks>
		/// <p>This method can be used to select a range of column headers. It will keep the existing selected column headers and select the passed in column headers.</p>
		/// <p>You can use HeaderBase.Selected property to select or unselect individual column headers.</p>
		/// <p><seealso cref="Clear"/>, <seealso cref="HeaderBase.Selected"/></p>
		/// </remarks>		
		public void AddRange( ColumnHeader[] columnHeaders )
		{
			// If the passed in array is of length 0, then do nothing.
			//
			if ( columnHeaders.Length <= 0 )
				return;

			int i;
			UltraGrid grid = this.Grid;

			// Ensure that all the column headers are from the same band. We don't allow selecting column 
			// headers from different bands.
			//
			UltraGridBand band = columnHeaders[0].Band;

			for ( i = 1; i < columnHeaders.Length; i++ )
			{
				if ( columnHeaders[i].Band != band )
					// SSP 6/23/03 - Localization
					//
					//throw new ArgumentException( "Column headers must be from the same band.", "columnHeaders" );
					throw new ArgumentException( SR.GetString( "LER_ArgumentException_6" ), "columnHeaders" );
			}

			// Ensure that the column headers are from this grid. We don't want to add column headers from a 
			// different grid or a different layout to the selected of this grid.
			//
			if ( null == band || band.Layout != grid.DisplayLayout )
				// SSP 6/23/03 - Localization
				//
				//throw new ArgumentException( "Column headers must be from display layout of the same grid.", "columnHeaders" );
				throw new ArgumentException( SR.GetString( "LER_ArgumentException_7" ), "columnHeaders" );

			SelectType selectType = band.SelectTypeColResolved;
			
			if ( SelectType.None == selectType )
				// SSP 6/23/03 - Localization
				//
				//throw new InvalidOperationException( "Can't select columns. SelectType is None." );
				throw new InvalidOperationException( SR.GetString( "LER_InvalidOperationException4" ) );

			if ( SelectType.Single == selectType || SelectType.SingleAutoDrag == selectType )
			{
				if ( columnHeaders.Length + this.Count > 1 )
					// SSP 6/23/03 - Localization
					//
					//throw new InvalidOperationException( "Can't select more than 1 column when SelectType is Single or SingleAutoDrag" );
					throw new InvalidOperationException( SR.GetString( "LER_InvalidOperationException5" ) );
			}
			// SSP 6/15/05
			// If select type is single and only a single row is being added to 0 count collection then
			// allow the operation. Made the if into else if block.
			// 
			//if ( SelectType.Extended != selectType &&  SelectType.ExtendedAutoDrag != selectType )
			else if ( SelectType.Extended != selectType &&  SelectType.ExtendedAutoDrag != selectType )
				// SSP 6/23/03 - Localization
				//
				//throw new InvalidOperationException( "Column selection not allowed. Invalid SelectType settings." );
				throw new InvalidOperationException( "LER_InvalidOperationException6" );

			Infragistics.Win.UltraWinGrid.Selected newSelection = new Selected( );

			// Add the existing column headers
			//
			for ( i = 0; i < this.Count; i++ )
			{
				newSelection.Columns.List.Add( this[i] );
			}

			// Add the passed in column headers.
			//
			for ( i = 0; i < columnHeaders.Length; i++ )
			{
				newSelection.Columns.List.Add( columnHeaders[i] ); 
			}

			// Sort the new column headers by the selected position.
			//
			newSelection.Columns.List.Sort( new Infragistics.Win.UltraWinGrid.Selected.SelectionPositionSortComparer( ) );

			// Remove duplicates since we merged the existing selected column headers to the new column headers
			// passed in. Any duplicate column headers should be removed.
			//
			Infragistics.Win.UltraWinGrid.Selected.RemoveDuplicateObjects( newSelection.Columns.List );

			// Copy the selected rows and cells since columns and rows/cells can be selected at the same time. 
			// Rows and cells can't be selected at the same time.
			//
			newSelection.Rows.InternalSetList( grid.Selected.Rows );
			newSelection.Cells.InternalSetList( grid.Selected.Cells );

			// Finally select the new selection by calling SelectNewSelection which fires the 
			// select change events.
			//
			grid.SelectNewSelection( typeof( ColumnHeader ), newSelection );
		}

		#endregion // AddRange

		#endregion // Fix For UWG1835

		#region Add

		// SSP 6/15/05
		// Added Add method. We had AddRange but not Add.
		// 
		/// <summary>
		/// Adds the specified column header to this selected cols collection.
		/// </summary>
		/// <param name="columnHeader"></param>
		public void Add( Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader )
		{
			this.AddRange( new Infragistics.Win.UltraWinGrid.ColumnHeader[] { columnHeader } );
		}

		#endregion // Add

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.ColumnHeader[] array, int index)
		{
			base.CopyTo( (System.Array)array, index );
		}
		#endregion //CopyTo

		#region Sort

		// SSP 2/22/06 BR09673
		// Added Sort method.
		// 
		/// <summary>
		/// Sorts the items in the collection according their visible order.
		/// </summary>
		public void Sort( )
		{
            object[] all = this.All;
			
			Array.Sort( all, new Infragistics.Win.UltraWinGrid.Selected.SelectionPositionSortComparer( ) );

			base.InternalClear( );

			foreach ( SubObjectBase o in all )
				base.InternalAdd( o );
		}

		#endregion // Sort
	}

	/// <summary>
	/// Enumerator for a collection of ColumnHeaders
	/// </summary>
    public class ColumnHeaderEnumerator : DisposableObjectEnumeratorBase
    {   
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="columnHeaders"></param>
        public ColumnHeaderEnumerator(SelectedColsCollection columnHeaders) : base( columnHeaders )
        {
        }

		/// <summary>
		/// non-IEnumerator version: type-safe
		/// </summary>
        public Infragistics.Win.UltraWinGrid.ColumnHeader Current 
        {
            get
            {
                return (ColumnHeader)((IEnumerator)this).Current;
            }
        }
    }
}
