#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Collections.Generic;

// SSP 4/23/02
// Added SummaryValue class for Summary Rows Feature
//


namespace Infragistics.Win.UltraWinGrid
{

	/// <summary>
	/// Class that holds information of a particular summary calculation.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// A <b>SummaryValue</b> instance contains the result of a summary calculation.
	/// This object is associated with the summary value ui element that's displayed
	/// for each summary calculation. You can access summary values of a row collection
	/// using the RowsCollection's <see cref="RowsCollection.SummaryValues"/> property.
	/// </p>
	/// <p class="body">
	/// A <see cref="Infragistics.Win.UltraWinGrid.SummarySettings"/> instance can
	/// have multiple <see cref="Infragistics.Win.UltraWinGrid.SummaryValue"/> instances
	/// associated with it. <b>SummarySettings</b> contains information on what formula
	/// to use for calculation. You can add summaries in code using the Band's 
	/// <see cref="UltraGridBand.Summaries"/> property.
	/// </p>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Summaries"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettingsCollection"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummaryValuesCollection"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.SummaryValues"/>
	/// </remarks>
	public class SummaryValue : SubObjectBase
	{
		#region Private Vars

		private SummaryValuesCollection	parentCollection = null;
		private Appearance			appearance				= null;
		private SummarySettings		summarySettings			= null;
		private object				calculatedSummaryValue	= null;
		private int					verifiedSummaryCalculationsVersion = -1;
		private int					verifiedDataChangedVersion = -1;
		private bool				inCalculateSummary	= false;

		// SSP 6/30/04 - UltraCalc
		//
		private SummaryValueReference calcReference = null;

		// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
		// Added GroupBySummaryValueAppearance.
		//
		private AppearanceBase groupBySummaryValueAppearance = null;

		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		private string toolTipText = null;

		#endregion // Private Vars

		#region Constructor

		internal SummaryValue( SummaryValuesCollection parentCollection, SummarySettings summarySettings )
		{
			if ( null == parentCollection )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_43"), Shared.SR.GetString("LE_ArgumentNullException_358") );

			if ( null == summarySettings )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_359"), Shared.SR.GetString("LE_ArgumentNullException_360") );

			this.parentCollection = parentCollection;
			this.summarySettings = summarySettings;
		}

		#endregion // Constructor

		#region Public Properties
		
		#region Appearance

		/// <summary>
		/// Appearance that will be applied to this summary value.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This property controls the appearance of this summary value instance. To set the
		/// appearance of all the summaries in the grid or a band, use the Override's 
		/// <see cref="UltraGridOverride.SummaryValueAppearance"/> property.
		/// </p>
		/// <p class="body"><b>Note:</b> The font settings of the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.SummaryValueAppearance"/> of the Override object are used to calculate the necessary height of the summary footer and the summary values. So if you are planning on modifying the size of font for individual SummarySettings or SummaryValue objects, make sure that a font just as big is assigned to SummaryValueAppearance of the Override object for the band or the layout.</p>
		/// <seealso cref="UltraGridOverride.SummaryValueAppearance"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings.Appearance"/>
		/// <seealso cref="UltraGridOverride.SummaryFooterAppearance"/>
		/// <seealso cref="UltraGridOverride.SummaryFooterAppearance"/>
		/// </remarks>
		public Appearance Appearance
		{
			get
			{
				if ( null == this.appearance )
				{				    
					this.appearance = new Infragistics.Win.Appearance();
				
					// hook up the prop change notifications
					//
					this.appearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.appearance;
			}
			set
			{
				if( this.appearance != value )
				{
					// SSP 12/3/01 UWG825
					// Unhook from the old appearance object
					//
					if ( null != this.appearance )
						this.appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

					this.appearance = value;

					// SSP 12/3/01 UWG825
					// hook up the prop change notifications
					//
					if ( null != this.appearance )
						this.appearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;

					this.NotifyPropChange( PropertyIds.Appearance, null );
				}
			}
		}

		#endregion //Appearance

		#region HasAppearance

		/// <summary>
		/// Returns true if appearance object for Appearance property has been allocated.
		/// </summary>
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public bool HasAppearance
		{
			get
			{
				return null != this.appearance;
			}
		}

		#endregion // HasAppearance

		#region GroupBySummaryValueAppearance

		// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
		// Added GroupBySummaryValueAppearance.
		//
		/// <summary>
		/// Appearance applied to summaries associated with this SummerySettings object that are displayed in the group-by rows.
		/// </summary>
		/// <remarks>
		/// <p class="body">Font settings off the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.GroupBySummaryValueAppearance"/> off the Override object are used to calculate the necessary height of the the summary values. So if you are planning on modifying the size of font for individual SummarySettings or SummaryValue objects, make sure that a font just as big is assigned to GroupBySummaryValueAppearance off the Override object for the band or the layout.</p>
		/// </remarks>
		public AppearanceBase GroupBySummaryValueAppearance
		{
			get
			{
				if ( null == this.groupBySummaryValueAppearance )
				{				    
					this.groupBySummaryValueAppearance = new Infragistics.Win.Appearance();
				
					// hook up the prop change notifications
					//
					this.groupBySummaryValueAppearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.groupBySummaryValueAppearance;
			}
			set
			{
				if( this.groupBySummaryValueAppearance != value )
				{
					// SSP 12/3/01 UWG825
					// Unhook from the old groupBySummaryValueAppearance object
					//
					if ( null != this.groupBySummaryValueAppearance )
						this.groupBySummaryValueAppearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

					this.groupBySummaryValueAppearance = value;

					// SSP 12/3/01 UWG825
					// hook up the prop change notifications
					//
					if ( null != this.groupBySummaryValueAppearance )
						this.groupBySummaryValueAppearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;

					this.NotifyPropChange( PropertyIds.GroupBySummaryValueAppearance, null );
				}
			}
		}

		#endregion // GroupBySummaryValueAppearance

		#region HasGroupBySummaryValueAppearance

		// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
		// Added GroupBySummaryValueAppearance.
		//
		/// <summary>
		/// Returns true if groupBySummaryValueAppearance object for GroupBySummaryValueAppearance property has been allocated.
		/// </summary>
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public bool HasGroupBySummaryValueAppearance
		{
			get
			{
				return null != this.groupBySummaryValueAppearance;
			}
		}

		#endregion // HasGroupBySummaryValueAppearance

		#region ParentRows

		/// <summary>
		/// RowsCollection object this SummaryValue corresponds to. 
		/// RowsCollection returned will be from the same band as the band
		/// the associated SummarySettings object is assigned to.
		/// </summary>
		public RowsCollection ParentRows
		{
			get
			{
				return this.ParentCollection.Rows;
			}
		}

		#endregion // ParentRows

		#region SummarySettings

		/// <summary>
		/// The associated <see cref="Infragistics.Win.UltraWinGrid.SummarySettings"/> object.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings"/>
		/// </remarks>
		public SummarySettings SummarySettings
		{
			get
			{
				return this.summarySettings;
			}
		}

		#endregion // SummarySettings

		#region Value

		/// <summary>
		/// Returns the calculated summary value for this summary.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>Value</b> property returns the calculated summary value for this summary.
		/// The type of object returned depends on the type of calculations performed.
		/// For example, Minimum or Maximum summary calculations simply return the 
		/// smallest or the largest cell value, without performing any conversion on 
		/// the value. For Sum or Average summary type it will be a decimal instance 
		/// and for Count summary type it will be an integer.
		/// </p>
		/// <p class="body">
		/// You can use the <b>SummaryText</b> property to retrive the text as it's
		/// displayed in summary.
		/// </p>
		/// <seealso cref="SummaryText"/>
		/// </remarks>
		public object Value
		{
			get
			{
				// SSP 7/13/04 - UltraCalc
				// 
				// ----------------------------------------------------------------------
				if ( this.SummarySettings.HasActiveFormula )
				{
					bool isError;
					return RefUtils.GetFormulaCalcValue( this.CalcReference, out isError, this.FormatProviderResolved );
				}
				// ----------------------------------------------------------------------

				// Lazily calculate the summary value.
				//
				this.CalculateSummaryValue( );

				return this.calculatedSummaryValue;
			}
		}

		#endregion // Value

		#region SummaryText

		/// <summary>
		/// Returns the text that will be displayed as the summary. This text is obtained by applying <see cref="Infragistics.Win.UltraWinGrid.SummarySettings.DisplayFormat"/> to the summary value. If there was no display format specified, a default display format is used.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>SummaryText</b> property returns the text that will be displayed in this summary. You can 
		/// change the format of the text using the <see cref="Infragistics.Win.UltraWinGrid.SummarySettings"/> object's 
		/// <see cref="Infragistics.Win.UltraWinGrid.SummarySettings.DisplayFormat"/> property.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings.DisplayFormat"/> 
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SummaryValue.SummarySettings"/>
		/// </remarks>
		public string SummaryText
		{
			get
			{
				// SSP 7/13/04 - UltraCalc
				// 
				// ----------------------------------------------------------------------
				if ( this.SummarySettings.HasActiveFormula )
				{
					bool isError;
					object calcValue = RefUtils.GetFormulaCalcValue( this.CalcReference, out isError, this.FormatProviderResolved );
					if ( isError )
						return calcValue.ToString( );
				}
				// ----------------------------------------------------------------------

				object val = this.Value;

				// SSP 8/10/04 - UltraCalc
				// Check for the source column being null because it can be null if the 
				// summary is a formula summary.
				//
				//if ( null == val || val is DBNull )
				if ( ( null == val || val is DBNull ) && null != this.SummarySettings.SourceColumn )
					val = this.SummarySettings.SourceColumn.NullTextResolved;

				// SSP 6/30/04 - UltraCalc
				// Use the new resolved properties.
				//
				//IFormatProvider formatProvider = this.SummarySettings.SourceColumn.FormatInfo;
				//string format = this.SummarySettings.DisplayFormatResolved;
				IFormatProvider formatProvider = this.FormatProviderResolved;
				string format = this.FormatResolved;

				// If the user has deliberately specified an empty string as
				// the format, then just return ToString( ) off the object.
				//
				// AS 1/8/03 - fxcop
				// Do not compare against string.empty - test the length instead
				//if ( null == format || "".Equals( format ) )
				if ( null == format || format.Length == 0 )
				{
					// MD 8/7/07 - 7.3 Performance
					// FxCop - Do not cast unnecessarily
					//if ( val is IFormattable )
					//{
					//    return ((IFormattable)val).ToString( null, formatProvider );
					//}
					IFormattable formattable = val as IFormattable;

					if ( formattable != null )
					{
						return formattable.ToString( null, formatProvider );
					}
					else
						return val.ToString( );
				}

				// If the user has specified a non-empty string for the format,
				// then call Format off the string to format the summary value.
				//
				// SSP 9/13/04 UWC119
				// Instead of crashing, display a error message in the summary. This has
				// nothing to do with the UltraCalc functionality. This issue existed before.
				// if the user sets the DisplayFormat property to an invalid value, the grid
				// simplay crashes. Instead display an error message in the summary value.
				//
				// --------------------------------------------------------------------------
				//return string.Format( formatProvider, format, val );
				try
				{
					return string.Format( formatProvider, format, val );
				}
				catch ( Exception e )
				{
					return SR.GetString( "SummaryValueInvalidDisplayFormat", format, e.Message );
				}
				// --------------------------------------------------------------------------
			}
		}

		#endregion // SummaryText

		#region Key

		/// <summary>
		/// Key associated with the associated SummarySettingsObject. (Read-only).
		/// </summary>
		/// <remarks>
		/// <p class="body">Key returned is the key of the SummarySettings object this SummaryValue object is associated with.</p>
		/// <seealso cref="SummarySettings"/>
		/// <seealso cref="SummarySettingsCollection.Add(SummaryType, UltraGridColumn)"/>
		/// </remarks>		
		public string Key
		{
			get
			{
				return null != this.SummarySettings ? this.SummarySettings.Key : "";
			}
		}

		#endregion // Key

		#region ToolTipText
		
		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		/// <summary>
		/// Specifies the tooltip to display when the user hovers the mouse over the header.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies the tooltip to display when the user hovers the mouse over the header. No tooltip is displayed if this property is set to null or empty string.</p>
		/// </remarks>
		public string ToolTipText
		{
			get
			{
				return this.toolTipText;
			}
			set
			{
				if ( this.toolTipText != value )
				{
					this.toolTipText = value;

					this.NotifyPropChange( PropertyIds.ToolTipText );
				}
			}
		}

		#endregion // ToolTipText

		#endregion // Public Properties

		#region ResetAppearance

		// SSP 5/4/05 - NAS 5.2 
		// Added ResetAppearance method for user's convenience.
		//
		/// <summary>
		/// Resets Appearance property to its default value.
		/// </summary>
		public void ResetAppearance( )
		{
			if ( this.HasAppearance )
			{
				// Call reset on the holder instead of the appearance
				//
				this.appearance.Reset( );

				// Notify listeners that the appearance has changed. 
				//
				this.NotifyPropChange(PropertyIds.Appearance);
			}
		}

		#endregion // ResetAppearance
		
		#region ResetGroupBySummaryValueAppearance

		// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
		// Added GroupBySummaryValueAppearance.
		//
		/// <summary>
		/// Resets GroupBySummaryValueAppearance property to its default value.
		/// </summary>
		public void ResetGroupBySummaryValueAppearance( )
		{
			if ( this.HasGroupBySummaryValueAppearance )
			{
				// Call reset on the holder instead of the appearance
				//
				this.groupBySummaryValueAppearance.Reset( );

				// Notify listeners that the appearance has changed. 
				//
				this.NotifyPropChange(PropertyIds.GroupBySummaryValueAppearance);
			}
		}

		#endregion // ResetGroupBySummaryValueAppearance

		#region ResetToolTipText

		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		/// <summary>
		/// Resets the property to its default value of false.
		/// </summary>
		public void ResetToolTipText( )
		{
			this.ToolTipText = null;
		}

		#endregion // ResetToolTipText

		#region Protected Methods

		#region OnSubObjectPropChanged

		// SSP 8/15/02
		// Overrode OnSubObjectPropChanged so we can refresh the display when any appearance
		// based settings are changed.
		//
		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			if ( this.HasAppearance && propChange.Source == this.Appearance 
				// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
				// Added GroupBySummaryValueAppearance.
				//
				|| this.HasGroupBySummaryValueAppearance && propChange.Source == this.GroupBySummaryValueAppearance )
			{
				this.NotifyPropChange( PropertyIds.SummaryValue, propChange );				
			}
		}

		#endregion // OnSubObjectPropChanged

		#endregion // Protected Methods

		#region Private/Internal Properties/Methods

		#region DirtySummaryValue

		internal void DirtySummaryValue( )
		{
			// Just decremenet one of the verified version numbers so that
			// we calculate the summary next time it's accessed.
			//
			this.verifiedSummaryCalculationsVersion--;
		}

		#endregion // DirtySummaryValue

		#region ParentCollection

		internal SummaryValuesCollection ParentCollection
		{
			get
			{
				return this.parentCollection;
			}
		}

		#endregion

		#region OnSummaryValueChanged

		// SSP 6/10/05 BR04553
		// Fire SummaryValueChanged notification for formula summaries as well. Added 
		// OnSummaryValueChanged helper method. Code in there was moved from the 
		// CalculateSummaryValue method.
		// 
		internal void OnSummaryValueChanged( object oldSummaryValue, object newSummaryValue )
		{
			// Fire the SummaryValueChanged event notifying the user that a summary value
			// has been calculated.
			//

			UltraGrid grid = null;
			if ( null != this.Layout )
			{
				// SSP 6/13/05 BR04539
				// Added RowChildElementsCacheVersion.
				// 
				this.Layout.BumpRowChildElementsCacheVersion( );

				grid = this.Layout.Grid as UltraGrid;
			}

			if ( null != grid )
			{
				// SSP 8/15/02 UWG1553
				// Behaviour of SummaryValueChanged event was changed so now we are only going to
				// fire the event if the summary value actually changes. Before we were firing whenever
				// the summary value is calculated.
				//
				bool summaryValueChanged = true;
					
				if ( oldSummaryValue == newSummaryValue )
				{
					// If both are null or reference the same object, then they are the same
					// values.
					//
					summaryValueChanged = false;
				}
				else if ( null != oldSummaryValue && null != newSummaryValue &&
					oldSummaryValue.GetType() == newSummaryValue.GetType( ) )
				{
					if ( oldSummaryValue.Equals( newSummaryValue ) )
						summaryValueChanged = false;
				}

				// SSP 8/15/02 UWG1553
				// Only fire if the summary value changed.
				//
				if ( summaryValueChanged )
				{
					// SSP 8/3/04 - UltraCalc
					// Notify the calc manager of the change in the value of the summary.
					//
					// ------------------------------------------------------------------
					// SSP 6/24/05 BR04577
					// Use the new NotifyCalcManager_ValueChanged method instead. It does
					// the same thing.
					// 
					this.NotifyCalcManager_ValueChanged( );
					
					// ------------------------------------------------------------------

					grid.FireEvent( GridEventIds.SummaryValueChanged, new SummaryValueChangedEventArgs( this ) );
				}
			}
		}

		#endregion // OnSummaryValueChanged

		#region CalculateSummaryValue

		internal void CalculateSummaryValue( )
		{
			RowsCollection rows = this.ParentRows;

			Debug.Assert( null != rows, "Null ParentRows !" );

			if ( null == rows )
				return;

			if ( this.SummarySettings.SummaryCalculationsVersion == this.verifiedSummaryCalculationsVersion &&				 
				 rows.DataChangedVersion == this.verifiedDataChangedVersion )
				return;

			// If we are already in calculate summary, then return to avoid
			// possible recursion.
			//
			if ( this.inCalculateSummary )
				return;

			this.inCalculateSummary = true;

			try
			{				
				this.verifiedSummaryCalculationsVersion = this.SummarySettings.SummaryCalculationsVersion;
				this.verifiedDataChangedVersion = rows.DataChangedVersion;

				// SSP 8/15/02 UWG1553
				// Behaviour of SummaryValueChanged event was changed so now we are only going to
				// fire the event if the summary value actually changes. Before we were firing whenever
				// the summary value is calculated.
				//
				object oldSummaryValue = this.calculatedSummaryValue;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.calculatedSummaryValue = 
				//    this.CalculateSummaryValue( rows, this.SummarySettings );
				this.calculatedSummaryValue =
					SummaryValue.CalculateSummaryValue( rows, this.SummarySettings );

				// SSP 6/10/05 BR04553
				// Moved code from here into the new FireSummaryValueChangedHelper method so the
				// summary value calc reference can make use of it to fire the SummaryValueChanged
				// event whenever the summary formula is recalculated.
				// 
				// --------------------------------------------------------------------------------
				this.OnSummaryValueChanged( oldSummaryValue, this.calculatedSummaryValue );
				
				// --------------------------------------------------------------------------------
			}
			finally
			{
				// Reset the anti-recursion flag.
				//
				this.inCalculateSummary = false;
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private object CalculateSummaryValue( RowsCollection rows, Infragistics.Win.UltraWinGrid.SummarySettings summarySettings )
		private static object CalculateSummaryValue( RowsCollection rows, Infragistics.Win.UltraWinGrid.SummarySettings summarySettings )
		{
			// If summary type is Custom and there is no custom summary calculator provied,
			// then return null.
			//
			if ( SummaryType.Custom == summarySettings.SummaryType &&
				 null == summarySettings.CustomSummaryCalculator )
				return null;

			ICustomSummaryCalculator summaryCalculator = null;

			switch ( summarySettings.SummaryType )
			{
				case SummaryType.Average:
					summaryCalculator = new AverageSummaryCalculator( );
					break;
				case SummaryType.Count:
					summaryCalculator = new CountSummaryCalculator( );
					break;
				case SummaryType.Maximum:
					summaryCalculator = new MaximumSummaryCalculator( );
					break;
				case SummaryType.Minimum:
					summaryCalculator = new MinimumSummaryCalculator( );
					break;
				case SummaryType.Sum:
					summaryCalculator = new SumSummaryCalculator( );
					break;
				case SummaryType.Custom:
					summaryCalculator = summarySettings.CustomSummaryCalculator;
					break;
				// SSP 8/2/04 - UltraCalc
				// If the the summary is a formula summary, then return because we
				// are directly accessing the summary value reference' calculated value.
				//
				case SummaryType.Formula:
					return null;
				default:
					Debug.Assert( false, "Unknown summary type !" );
					return null;
			}

			// SSP 8/10/04 - Design time serialization of summaries
			// Summaries can have null source columns so check for that.
			//
			// --------------------------------------------------------------
			if ( null == summarySettings.SourceColumn )
				return null;
			// --------------------------------------------------------------

			if ( summarySettings.SourceColumn.Band != summarySettings.ParentCollection.Band &&
				!summarySettings.SourceColumn.Band.IsDescendantOfBand( summarySettings.ParentCollection.Band ) )
			{
				Debug.Assert( false, "Source column must be from the same band as summary settings or from a descendant band !" );
				return null;
			}

			// SSP 7/15/02 UWG1372
			// Added new rows parameter to the BeginCustomSummary and EndCustomSummary methods
			// of the ICustomSummaryCalculator interface.
			//
			//summaryCalculator.BeginCustomSummary( summarySettings, );
			summaryCalculator.BeginCustomSummary( summarySettings, rows );

			rows.InternalTraverseRowsHelper( summarySettings.SourceColumn.Band, 
				new RowCallback( summaryCalculator, summarySettings ), true );

			// SSP 7/15/02 UWG1372
			// Added new rows parameter to the BeginCustomSummary and EndCustomSummary methods
			// of the ICustomSummaryCalculator interface.
			//
			//return summaryCalculator.EndCustomSummary( summarySettings );
			return summaryCalculator.EndCustomSummary( summarySettings, rows );
		}

		#region RowCallback Class

		// MD 1/24/08
		// Made changes to allow for VS2008 style unit test accessors
		//private class RowCallback : RowsCollection.IRowCallback 
		private class RowCallback : IRowCallback 
		{
			private SummarySettings summarySettings = null;
			private ICustomSummaryCalculator summaryCalculator = null;

			internal RowCallback( ICustomSummaryCalculator summaryCalculator, SummarySettings summarySettings )
			{
				this.summarySettings = summarySettings;
				this.summaryCalculator = summaryCalculator;
			}

			/// <summary>
			/// Return true to continue traversing, false to stop
			/// </summary>
			/// <param name="row"></param>
			/// <returns></returns>
			// MD 1/24/08
			// Made changes to allow for VS2008 style unit test accessors
			//bool RowsCollection.IRowCallback.ProcessRow( UltraGridRow row )
			bool IRowCallback.ProcessRow( UltraGridRow row )
			{
				// Skip group-by rows
				//
				if ( typeof( UltraGridRow ) == row.GetType( ) )
				{
					// SSP 8/1/03 UWG1654 - Filter Action
					// Use HiddenInternal instead of the public Hidden behavior of which we changed in regards to filtering.
					//
					//bool hidden = row.Hidden;
					bool hidden = row.HiddenInternal;

					// Also check for any ancestor rows being hidden. For example in
					// Customer-Order-OrderDetails hierarchy, if we are calculating
					// Average UnitPrice (a column in OrderDetails) for each Customer,
					// then if the row in the Order band is hidden, then we should not
					// take that into calculations.
					//
					UltraGridRow parentRow = row.ParentRow;
					while ( null != parentRow )
					{
						if ( parentRow.Band != this.summarySettings.Band &&
							!parentRow.Band.IsDescendantOfBand( this.summarySettings.Band ) )
							break;

						// SSP 8/1/03 UWG1654 - Filter Action
						// Use HiddenInternal instead of the public Hidden behavior of which we changed in regards to filtering.
						//
						//if ( parentRow.Hidden )
						if ( parentRow.HiddenInternal )
						{
							hidden = true;						
							break;
						}

						parentRow = parentRow.ParentRow;
					}

					// Only aggregate values for for rows that are not
					// hidden (either explicitly by the user or are filtered out). We
					// still calculate for rows whose parents are not expanded.
					//
					if ( !hidden )
						this.summaryCalculator.AggregateCustomSummary( this.summarySettings, row );
				}

				return true;
			}
		}

		#endregion // RowCallback Class

		#endregion // CalculateSummaryValue

		#region ResolveAppearance

		// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
		// Added an overload of ResolveAppearance that takes in summaryDisplayArea parameter.
		//
		/// <summary>
		/// Resolves the appearance for the summary value.
		/// </summary>
        /// <param name="appData">The structure to contain the resolved apperance.</param>
        /// <param name="propFlags">Bit flags indictaing which properties to resolve.</param>
        /// <param name="summaryDisplayArea">Summaries matching one of the specified display area will be displayed.</param>
		public void ResolveAppearance( ref AppearanceData appData, 
			ref AppearancePropFlags propFlags, SummaryDisplayAreas summaryDisplayArea )
		{
			this.ResolveAppearance( ref appData, ref propFlags, new SummaryDisplayAreaContext( summaryDisplayArea ) );
		}

		// SSP 11/22/02 
		// Made ResolveAppearance public. 
		//
		/// <summary>
		/// Resolves the appearance for the summary value.
		/// </summary>
        /// <param name="appData">The structure to contain the resolved apperance.</param>
        /// <param name="propFlags">Bit flags indictaing which properties to resolve.</param>
        //internal void ResolveAppearance( ref AppearanceData appData, ref AppearancePropFlags propFlags )
		public void ResolveAppearance( ref AppearanceData appData, ref AppearancePropFlags propFlags )
		{
			this.ResolveAppearance( ref appData, ref propFlags, SummaryDisplayAreaContext.BottomScrollingSummariesContext );
		}

        // MRS 5/9/2008 - BR32722
        // Resolves the appearance for the summary value.
        //
        /// <summary>
        /// Resolves the appearance used by the summary footer.
        /// </summary>
        /// <param name="appData"></param>
        /// <param name="propFlags"></param>        
        /// <param name="summaryRow"></param>
        public void ResolveAppearance(UltraGridSummaryRow summaryRow, ref AppearanceData appData, ref AppearancePropFlags propFlags)
        {
            this.ResolveAppearance(ref appData, ref propFlags, summaryRow.SummaryDisplayAreaContext);
        }

		// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
		// Added an overload of ResolveAppearance that takes in summaryDisplayAreaContext parameter.
		//
		internal void ResolveAppearance( ref AppearanceData appData, 
			ref AppearancePropFlags propFlags, SummaryDisplayAreaContext summaryDisplayAreaContext )
		{
			UltraGridBand band = this.Band;

			// SSP 3/13/06 - App Styling
			// 
			AppStyling.ResolutionOrderInfo order;
			AppStyling.UIRole role = StyleUtils.GetRole( band, StyleUtils.Role.SummaryValue, out order );

			// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
			// 
			if ( null != summaryDisplayAreaContext && summaryDisplayAreaContext.IsGroupByRowSummaries )
			{
				// If the group-by row is selected then resolve the back color and fore color to the
				// selected colors.
				//
				if ( null != summaryDisplayAreaContext.GroupByRowContext 
					&& summaryDisplayAreaContext.GroupByRowContext.Selected )
				{
					ResolveAppearanceContext selectedResolveContext = new ResolveAppearanceContext( typeof( UltraGridGroupByRow ), propFlags );

					// SSP 3/13/06 - App Styling
					// 
					selectedResolveContext.Role = StyleUtils.GetRole( band, StyleUtils.Role.GroupByRow, out selectedResolveContext.ResolutionOrder );

					selectedResolveContext.IsSelected = true;
					band.ResolveAppearance( ref appData, ref selectedResolveContext );
					propFlags = selectedResolveContext.UnresolvedProps;

					// Also resolve the border color to the when the group-by row is selected fore color.
					//
					if ( 0 != ( AppearancePropFlags.BorderColor & propFlags ) )
					{
						propFlags ^= AppearancePropFlags.BorderColor;
						appData.BorderColor = appData.ForeColor;
					}
				}

				// SSP 3/13/06 - App Styling
				// 
				//if ( this.HasGroupBySummaryValueAppearance )
				if ( this.HasGroupBySummaryValueAppearance && order.UseControlInfo )
					this.GroupBySummaryValueAppearance.MergeData( ref appData, ref propFlags );
			}

			// SSP 3/13/06 - App Styling
			// 
			//if ( this.HasAppearance )
			if ( this.HasAppearance && order.UseControlInfo )
			{
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( ref appData, this.Appearance.Data, ref propFlags ); 
				this.Appearance.MergeData( ref appData, ref propFlags );
			}

			Infragistics.Win.UltraWinGrid.SummarySettings summary = this.SummarySettings;

			Debug.Assert( null != summary, "No summary object associated with summary value !" );

			// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
			// Added the following if block and enclosed the existing code into the else block.
			//
			if ( null != summaryDisplayAreaContext && summaryDisplayAreaContext.IsGroupByRowSummaries )
			{
				// SSP 3/13/06 - App Styling
				// 
				//if ( summary.HasGroupBySummaryValueAppearance )
				if ( summary.HasGroupBySummaryValueAppearance && order.UseControlInfo )
					summary.GroupBySummaryValueAppearance.MergeData( ref appData, ref propFlags );

				band.MergeBLOverrideAppearances( ref appData, ref propFlags, UltraGridOverride.OverrideAppearanceIndex.GroupBySummaryValueAppearance 
					// SSP 3/13/06 - App Styling
					// 
					, role, order, AppStyling.RoleState.GroupByRow );

				// SSP 8/2/04 - UltraCalc
				// Resolve formula error appearance if the summary is a formula summary and has formula error.
				//
				if ( this.HasFormulaCalcError )
					band.MergeBLOverrideAppearances( ref appData, ref propFlags, UltraGridOverride.OverrideAppearanceIndex.FormulaErrorAppearance
						// SSP 3/13/06 - App Styling
						// 
						, role, order, AppStyling.RoleState.FormulaError );

				// Resolve default the back color of the summaries in group-by rows to transparent.
				//
				GridUtils.ResolveBackColorsToTransparent( ref appData, ref propFlags );
			}
			else
			{
				// SSP 3/13/06 - App Styling
				// 
				//if ( summary.HasAppearance )
				if ( summary.HasAppearance && order.UseControlInfo )
				{
					// JJD 12/12/02 - Optimization
					// Call the Appearance object's MergeData method instead so
					// we don't make unnecessary copies of the data structure
					//AppearanceData.MergeAppearance( ref appData, summary.Appearance.Data, ref propFlags ); 
					summary.Appearance.MergeData( ref appData, ref propFlags ); 
				}

				if ( 0 == propFlags )
					return;

				// SSP 8/2/04 - UltraCalc
				// Resolve formula error appearance if the summary is a formula summary and has formula error.
				//
				// ------------------------------------------------------------------------------------------------
				if ( this.HasFormulaCalcError )
				{
					// SSP 4/26/05 - NAS 5.2 Extension of Summaries Functionality
					// Optimization. Use the MergeBLOverrideAppearances method instead.
					//
					band.MergeBLOverrideAppearances( ref appData, ref propFlags, UltraGridOverride.OverrideAppearanceIndex.FormulaErrorAppearance 
						// SSP 3/13/06 - App Styling
						// 
						, role, order, AppStyling.RoleState.FormulaError );
					

					if ( 0 == propFlags )
						return;
				}
				// ------------------------------------------------------------------------------------------------
			}

			if ( null != this.ParentCollection )
				this.ParentCollection.ResolveSummaryValueAppearance( summaryDisplayAreaContext, ref appData, ref propFlags );
		}

		#endregion // ResolveAppearance

		#region BorderStyleResolved

		internal UIElementBorderStyle BorderStyleResolved
		{
			get
			{
				return this.ParentRows.Band.BorderStyleSummaryValueResolved;
			}
		}

		#endregion // BorderStyleResolved

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        #region GetRequiredSize

		//#if DEBUG
		//        /// <summary>
		//        /// Gets the required size.
		//        /// </summary>
		//        /// <returns></returns>
		//#endif
		//        internal Size GetRequiredSize( Graphics gr )
		//        {
		//            string str = this.SummaryText;

		//            UltraGridBase grid = 
		//                null != this.ParentCollection.Band && null != this.ParentCollection.Band.Layout.Grid
		//                ? this.ParentCollection.Band.Layout.Grid
		//                : null;

		//            Debug.Assert( null != grid, "No grid !" );

		//            if ( null == grid )
		//                return Size.Empty;

		//            Debug.Assert( null != gr, "Null graphics passed in." );

		//            if ( null == gr )
		//                return Size.Empty;

		//            int lines = this.SummarySettings.Lines;

		//            StringFormat sf = new StringFormat( );

		//            if ( lines > 1 )
		//                sf.FormatFlags &= ~StringFormatFlags.NoWrap;
		//            else
		//                sf.FormatFlags |= StringFormatFlags.NoWrap;

		//            sf.FormatFlags |= ( StringFormatFlags.MeasureTrailingSpaces | StringFormatFlags.FitBlackBox );

		//            AppearanceData appData = new AppearanceData( );
		//            AppearancePropFlags flags = AppearancePropFlags.FontData;
		//            this.ResolveAppearance( ref appData, ref flags );

		//            Font font = grid.Font;

		//            Font createdFont = appData.CreateFont( font );

		//            if ( null != createdFont )
		//                font = createdFont;

		//            //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
		//            //Size size = gr.MeasureString( str, font, PointF.Empty, sf ).ToSize( );
		//            // SSP 11/4/05 BR07555
		//            // Need to pass printing param when measuring in Whidbey. When printing,
		//            // in Whidbey we need use the GDI+ measuring.
		//            // 
		//            //Size size = DrawUtility.MeasureString( gr, str, font, PointF.Empty, sf ).ToSize( );
		//            Size size = this.Layout.MeasureString( gr, str, font, sf );

		//            size.Width++;

		//            if ( null != createdFont )
		//                createdFont.Dispose( );
		//            createdFont = null;

		//            if ( null != sf )
		//                sf.Dispose( );
		//            sf = null;

		//            return size;
		//        }

		//        #endregion // GetRequiredSize

		#endregion Not Used

		#region Band
		
		internal UltraGridBand Band
		{
			get
			{
				return null != this.SummarySettings 
					? this.SummarySettings.Band
					: null;
			}
		}

		#endregion // Band

		#region Layout
		
		internal UltraGridLayout Layout
		{
			get
			{
				return null != this.Band ? this.Band.Layout : null;
			}
		}

		#endregion // Layout

		// SSP 6/30/04 - UltraCalc
		// 
		#region UltraCalc Related Changes

		#region CalcReference

		internal SummaryValueReference CalcReference
		{
			get
			{
				if ( null == this.calcReference && null != this.Layout )
					this.calcReference = new SummaryValueReference( this );

				return this.calcReference;
			}
		}

		#endregion // CalcReference

		#region HasCalcReference
		
		internal bool HasCalcReference
		{
			get
			{
				return null != this.calcReference;
			}
		}

		#endregion // HasCalcReference

		#region HasFormulaCalcError

		internal bool HasFormulaCalcError
		{
			get
			{
				string errorInfoText;
				return this.GetFormulaCalcError( out errorInfoText );
			}
		}

		internal bool GetFormulaCalcError( out string errorInfoText )
		{
			errorInfoText = null;

			if ( this.SummarySettings.HasActiveFormula )
			{
				bool isError;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//object val = RefUtils.GetFormulaCalcValue( this.CalcReference, 
				RefUtils.GetFormulaCalcValue( this.CalcReference, 
							out isError, out errorInfoText, this.FormatProviderResolved );

				return isError;
			}

			return false;
		}

		#endregion // HasFormulaCalcError

		#region FormatResolved

		internal string FormatResolved
		{
			get
			{
				return this.SummarySettings.DisplayFormatResolved;
			}
		}

		#endregion // FormatResolved

		#region FormatProviderResolved
		
		internal IFormatProvider FormatProviderResolved
		{
			get
			{
				// SSP 1/3/06 BR08523
				// Use the new DisplayFormatProviderResolved of the SummarySettings instead.
				// 
				//return null != this.SummarySettings.SourceColumn
				//	? this.SummarySettings.SourceColumn.FormatInfo
				//	: null;
				return this.SummarySettings.DisplayFormatProviderResolved;
			}
		}

		#endregion // FormatProviderResolved

		#region ToolTipTextResolved

		// SSP 8/11/04 - UltraCalc
		// Added ToolTipTextResolved.
		//
		internal string ToolTipTextResolved
		{
			get
			{
				// SSP 7/27/05 - Header, Row, Summary Tooltips
				// 
				// --------------------------------------------------------------
				string tipText = this.ToolTipText;
				if ( null == tipText || 0 == tipText.Length )
					tipText = this.SummarySettings.ToolTipText;

				if ( null != tipText && tipText.Length > 0 )
					return tipText;
				// --------------------------------------------------------------

				string formulaErrorInfoText;
				if ( this.GetFormulaCalcError( out formulaErrorInfoText ) )
					return formulaErrorInfoText;

				return null;
			}
		}

		#endregion // ToolTipTextResolved

		#region InvalidateItemAllRegions

		// MD 2/10/09 - TFS13331
		// Changed this code to be optimaized for situations when mulitple summary values in the same rows collection have InvalidateItemAllRegions at the same time.
		#region Old Code

		

		#endregion Old Code
		internal void InvalidateItemAllRegions()
		{
			SummaryValue.InvalidateItemAllRegions( new SummaryValue[] { this }, this.Band, this.ParentRows );
		}

		internal static void InvalidateItemAllRegions( SummaryValue[] summaryValues, UltraGridBand band, RowsCollection parentRows )
		{
			if ( band == null )
				return;

			UltraGridLayout layout = band.Layout;

			if ( layout == null )
				return;

			// SSP 11/19/04 - Optimizations
			// If the parent rows is not visible then don't bother searching for the ui element.
			//
			bool isPotentiallyVisibleOnScreen =
				parentRows != null &&
				parentRows.TopLevelRowsCollection.IsPotentiallyVisibleOnScreen;

			if ( isPotentiallyVisibleOnScreen == false )
				return;

			Dictionary<SummaryValue, object> summaryValuesHash = new Dictionary<SummaryValue, object>();
			bool hasGroupBySortColumns = band.HasGroupBySortColumns;

			for ( int valueIndex = 0; valueIndex < summaryValues.Length; valueIndex++ )
			{
				SummaryValue summaryValue = summaryValues[ valueIndex ];
				summaryValuesHash[ summaryValue ] = null;

				if ( hasGroupBySortColumns
					&& isPotentiallyVisibleOnScreen
					&& null != summaryValue.SummarySettings
					// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
					// Obsoleted Hidden and DisplayInGroupBy. Instead added SummaryDisplayArea.
					//
					//&& this.SummarySettings.DisplayInGroupBy 
					&& SummaryDisplayAreaContext.InGroupByRowsSummariesContext.DoesSummaryMatch( summaryValue.SummarySettings ) )
				{
					layout.DirtyDataAreaElement();
					layout.BumpRowChildElementsCacheVersion();
					return;
				}
			}

			// If no summary values were displayed in group by rows, dirty all their elements.
			SummaryValue.InvalidateAllSummaryElements( layout.UIElement, summaryValuesHash );
		}

		#endregion InvalidateItemAllRegions

		// MD 2/10/09 - TFS13331
		#region InvalidateAllSummaryElements

		private static void InvalidateAllSummaryElements( UIElement element, Dictionary<SummaryValue, object> summaryValues )
		{
			for ( int i = 0; i < element.ChildElements.Count; i++ )
			{
				UIElement childElement = element.ChildElements[ i ];

				// If a SummaryValueUIElement is found, dirty the element if it's summary value is in the collection and move on to 
				// the next element. It will not contain other SummaryValueUIElement instances, so we do not have to do the descendant 
				// search on this element.
				if ( childElement is SummaryValueUIElement )
				{
					SummaryValue context = (SummaryValue)childElement.GetContext( typeof( SummaryValue ) );

					if ( context == null || summaryValues.ContainsKey( context ) == false )
						continue;

					childElement.DirtyChildElements();
					continue;
				}

				SummaryValue.InvalidateAllSummaryElements( childElement, summaryValues );
			}
		} 

		#endregion InvalidateAllSummaryElements

		#region IsStillValid
        
		// SSP 9/1/04
		// Added IsStillValid property.
		//
		internal bool IsStillValid
		{
			get
			{
				return !this.Disposed && this.SummarySettings.IsStillValid
					&& null != this.ParentCollection && this.ParentCollection.IndexOf( this ) >= 0
					&& null != this.ParentRows && this.ParentRows.IsStillValid;
			}
		}

		#endregion // IsStillValid

		#region NotifyCalcManager_ValueChanged

		// SSP 6/23/05 BR04577
		// 
		internal void NotifyCalcManager_ValueChanged( )
		{
			if ( this.HasCalcReference && ! this.Layout.CalcManagerNotificationsSuspended
				// SSP 6/23/05 - BR04577
				// Notify if dummy formula as well.
				// 
				//&& ! this.CalcReference.HasFormula  )
				&& ( ! this.CalcReference.HasFormula || this.CalcReference.ContainingFormulaRef.dummyFormula ) )
			{
				this.CalcReference.NotifyCalcEngineValueChanged( );
			}
		}

		#endregion // NotifyCalcManager_ValueChanged

		#endregion // UltraCalc Related Changes

		#endregion // Private/Internal Properties
	}

}
