#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;

namespace Infragistics.Win.UltraWinGrid
{
	// SSP 1/5/04 UWG1606
	// Added appearances for the splitter bars.
	// Added RowColSplitterIntersectionUIElement class.
	//

	/// <summary>
	/// UI element thats positioned at the intersection of row scroll region and col scroll region splitter bars.
	/// </summary>
	public class RowColSplitterIntersectionUIElement : SplitterIntersectionUIElement
	{
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private SplitterUIElement leftRightAdjustmentSplitter;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		/// <param name="leftRightAdjustmentSplitter">The splitter element for the left and right adjustment.</param>
        /// <param name="upDownAdjustmentSplitter">The splitter element for the up and down adjustment.</param>
		public RowColSplitterIntersectionUIElement( 
			UIElement parent,
			SplitterUIElement leftRightAdjustmentSplitter,
			SplitterUIElement upDownAdjustmentSplitter )
			: base( parent, leftRightAdjustmentSplitter, upDownAdjustmentSplitter )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.leftRightAdjustmentSplitter = leftRightAdjustmentSplitter;
		}

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			UltraGridLayout layout = this.Layout;

			// SSP 3/14/06 - App Styling
			// Use the new overload of MergeLayoutAppearances that also takes care of the app
			// stlyle settings.
			// 
			// --------------------------------------------------------------------------------------
			layout.MergeLayoutAppearances( ref appearance, ref requestedProps, 
				UltraGridLayout.LayoutAppearanceIndex.SplitterBarHorizontalAppearance,
				StyleUtils.Role.SplitterBarIntersection, AppStyling.RoleState.Normal );

			base.InitAppearance( ref appearance, ref requestedProps );
			
			// --------------------------------------------------------------------------------------
		}

		private UltraGridLayout Layout
		{
			get
			{
				return (UltraGridLayout)this.GetContext( typeof( UltraGridLayout ), true );
			}
		}

		private bool ShouldDrawBackColor( ref AppearanceData horizSplitterBarAppearance )
		{
			UltraGridLayout layout = this.Layout;

			if ( null != layout )
			{
				AppearanceData vertSplitterBarAppearance = new AppearanceData( );
				if ( layout.GetHasAppearance( UltraGridLayout.LayoutAppearanceIndex.SplitterBarVerticalAppearance ) )
				{
					AppearancePropFlags flags = AppearancePropFlags.AllRender;
					layout.SplitterBarVerticalAppearance.MergeData( ref vertSplitterBarAppearance, ref flags );
				}

				if ( ! vertSplitterBarAppearance.HasPropertyBeenSet( AppearancePropFlags.BackColor ) )
					vertSplitterBarAppearance.BackColor = SystemColors.Control;
				

				// If the back colors for the vertical splitter bar and the horizontal splitter
				// bars are different then don't draw the back color for this splitter area.
				//
				if ( vertSplitterBarAppearance.BackColor != horizSplitterBarAppearance.BackColor )
					return false;

				// If there are gradients then retrun false as well.
				//
				if ( Color.Empty != vertSplitterBarAppearance.BackColor2 
					 || Color.Empty != horizSplitterBarAppearance.BackColor2 )
					return false;

				// If the alpha levels are different, then return false as well.
				//
				if ( vertSplitterBarAppearance.BackColorAlpha != horizSplitterBarAppearance.BackColorAlpha )
					return false;
			}

			return true;
		}
			

		/// <summary>
		/// Overridden.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor( ref UIElementDrawParams drawParams )
		{
			if ( this.ShouldDrawBackColor( ref drawParams.AppearanceData )
				// SSP 4/29/06 - App Styling - BR11852
				// Also draw the back color if the SplitterBarIntersection ui role has back color 
				// appearance settings.
				// 
				|| GridUtils.ShouldDrawBackColor( this.Layout, StyleUtils.Role.SplitterBarIntersection ) )
				base.DrawBackColor( ref drawParams );
		}

		/// <summary>
		/// Overridden.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBorders( ref UIElementDrawParams drawParams )
		{
			if ( this.ShouldDrawBackColor( ref drawParams.AppearanceData )
				// SSP 4/29/06 - App Styling - BR11852
				// Also draw the back color if the SplitterBarIntersection ui role has back color 
				// appearance settings.
				// 
				|| GridUtils.ShouldDrawBackColor( this.Layout, StyleUtils.Role.SplitterBarIntersection ) )
				base.DrawBorders( ref drawParams );
		}

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Layout, StyleUtils.Role.SplitterBarIntersection );
			}
		}

		#endregion // UIRole
	}
}
