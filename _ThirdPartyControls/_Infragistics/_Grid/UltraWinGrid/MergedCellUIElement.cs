#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Collections.Generic;

// SSP 11/3/04 - Merged Cell Feature
// Added MergedCellUIElement class.
//


namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// The ui element class for representing a merged cell.
	/// </summary>
	public class MergedCellUIElement : AdjustableUIElement
	{
		#region Private Vars

		// Row elements will be initialized to row elements this merged cell spans over in 
		// top to bottom order. These row elements are the ones that are actually positioned 
		// in the scroll region element. The same with cellElements except it contains cell 
		// elements.
		//
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private ArrayList rowElements = null;
		//private ArrayList cellElements = null;
		private List<RowUIElement> rowElements = null;
		private List<CellUIElement> cellElements = null;

		private CellUIElement activeCellElem = null;
		private RowCellAreaUIElement activeRowElem = null;
		private bool isActiveCellElemInEditMode = false;
		private int selectedCellsCount = 0;

		private int elemTop = 0, elemHeight = 0;
		private Rectangle clipSelfRect = Rectangle.Empty;
		private UIElementBorderStyle borderStyle = UIElementBorderStyle.Default;
		private Border3DSide borderSides = Border3DSide.All;

		private AppearanceData cachedAppData = new AppearanceData( );
		private int verifiedCellChildElementsCacheVersion = -1;

		#endregion // Private Vars

		#region Constructor

		internal MergedCellUIElement( UIElement parent ) : base( parent, false, false )
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent ui element.</param>
		/// <param name="row">Row associated with the first cell in the group of merged cells.</param>
		/// <param name="column">The column associated with the merged cells.</param>
		public MergedCellUIElement( UIElement parent, UltraGridRow row, UltraGridColumn column ) : base( parent, true, true )
		{
			MergedCell mergedCell = column.GetMergedCell( row );
			if ( null == mergedCell )
				throw new InvalidOperationException( "Cell designated by the specified row and column is not merged with any other cell." );

			this.Initialize( mergedCell, 0, 0 );
		}

		#endregion // Constructor

		#region Public Properties

		#region Row

		// SSP 4/30/05
		// Made this public.
		//
		/// <summary>
		/// The row associated with the first cell in the group of merged cells.
		/// </summary>
		public UltraGridRow Row
		{
			get
			{
				return this.MergedCell.StartRow;
			}
		}

		#endregion // Row

		#region Column

		// SSP 4/30/05
		// Made this public.
		//
		/// <summary>
		/// The column associated with the merged cells.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return this.MergedCell.Column;
			}
		}

		#endregion // Column

		#region Cell

		// SSP 4/30/05
		// Made this public.
		//
		/// <summary>
		/// The first cell in the group of merged cells.
		/// </summary>
		public UltraGridCell Cell
		{
			get
			{
				return null != this.Row && null != this.Column ? this.Row.Cells[ this.Column ] : null;
			}
		}

		#endregion // Cell

		#endregion // Public Properties

		#region Private/Internal Properties

		#region MergedCell

		internal Infragistics.Win.UltraWinGrid.MergedCell MergedCell
		{
			get
			{
				return (MergedCell)this.PrimaryContext;
			}
		}

		#endregion // MergedCell

		#region RowElements

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList RowElements
		internal List<RowUIElement> RowElements
		{
			get
			{
				if ( null == this.rowElements )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//this.rowElements = new ArrayList( );
					this.rowElements = new List<RowUIElement>();
				}

				return this.rowElements;
			}
		}

		#endregion // RowElements

		#region CellElements

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList CellElements
		internal List<CellUIElement> CellElements
		{
			get
			{
				if ( null == this.cellElements )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//this.cellElements = new ArrayList( );
					this.cellElements = new List<CellUIElement>();
				}

				return this.cellElements;
			}
		}

		#endregion // CellElements

		#endregion // Private/Internal Properties

		#region Private/Internal Methods

		#region Initialize

		internal void Initialize( MergedCell mergedCell, int top, int height )
		{
			if ( null == mergedCell )
				throw new ArgumentNullException( "mergedCell" );

			this.elemTop = top;
			this.elemHeight = height;

			// If the merged cell context is different then force repositioning the child elements.
			// We use the following version number in the PositionSelf method.
			//
			if ( this.childElementsCollectionDirty || this.PrimaryContext != mergedCell )
				this.verifiedCellChildElementsCacheVersion--;
			this.childElementsCollectionDirty = true;

			this.PrimaryContext = mergedCell;
			this.ResetCachedInfo( );
		}

		#endregion // Initialize

		#region ResetCellElemsList

		private void ResetCellElemsList( )
		{
			this.activeCellElem = null;
			this.activeRowElem = null;
			this.selectedCellsCount = 0;
			this.isActiveCellElemInEditMode = false;

			if ( null != this.cellElements )
			{
				// Clear the merged cell elem pointers on the cell elements in case they get reused
				// for a different column.
				//
				for ( int i = 0, count = this.cellElements.Count; i < count; i++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//CellUIElement cellElem = (CellUIElement)this.cellElements[i];
					CellUIElement cellElem = this.cellElements[ i ];

					if ( this == cellElem.MergedCellElement )
						cellElem.SetMergedCellElement( null );
				}

				this.cellElements.Clear( );
			}
		}

		#endregion // ResetCellElemsList

		#region InitCellElemsList

		private void InitCellElemsList( )
		{
			this.ResetCellElemsList( );

			Infragistics.Win.UltraWinGrid.MergedCell mergedCell = this.MergedCell;
			UltraGridColumn column = mergedCell.Column;
			UltraGridLayout layout = column.Band.Layout;
			// SSP 4/15/05 - Optimizations
			//
			//UltraGridRow activeRow = column.Band.Layout.ActiveRow;
			//UltraGridCell activeCell = column.Band.Layout.ActiveCell;
			UltraGridCell activeCell = layout.ActiveCellInternal;
			UltraGridRow activeRow = layout.ActiveRowInternal;

			// LikelyCellElementIndex is just an optimization.
			//
			int likelyCellElemIndex = mergedCell.ParentCollection.likelyCellElementIndex;

			for ( int i = 0, count = this.RowElements.Count; i < count; i++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//RowCellAreaUIElement rowCellAreaElem = (RowCellAreaUIElement)((RowUIElementBase)this.rowElements[i]).GetDescendant( typeof( RowCellAreaUIElement ) );
				RowCellAreaUIElement rowCellAreaElem = (RowCellAreaUIElement)this.rowElements[ i ].GetDescendant( typeof( RowCellAreaUIElement ) );

				//Debug.Assert( null != rowCellAreaElem );
				if ( null != rowCellAreaElem )
				{					
					CellUIElement cellElem = null;

					// LikelyCellElementIndex is just an optimization.
					//
					UIElementsCollection rowChildElems = rowCellAreaElem.ChildElements;
					if ( likelyCellElemIndex >= 0 && likelyCellElemIndex < rowChildElems.Count )
					{
						CellUIElement tmpCellElem = rowChildElems[ likelyCellElemIndex ] as CellUIElement;
						if ( null != tmpCellElem && column == tmpCellElem.Column )
							cellElem = tmpCellElem;
					}

					if ( null == cellElem )
					{
						cellElem = (CellUIElement)rowCellAreaElem.GetDescendant( typeof( CellUIElement ), column );
						if ( null != cellElem )
							likelyCellElemIndex = rowChildElems.IndexOf( cellElem );
					}

					if ( null != cellElem )
					{
						// Set the merged cell pointer on the cell element so the cell element knows not to draw itself.
						//
						cellElem.SetMergedCellElement( this );

						// Store the cell elements that are underneath this merged cell.
						//
						this.CellElements.Add( cellElem );
						
						// Following block of code initializes the selectedCellsCount, activeCellElem and activeRowElem 
						// member variables.
						//
						UltraGridRow row = rowCellAreaElem.Row;
						Debug.Assert( null != row );
						if ( null != row )
						{
							UltraGridCell cell = row.GetCellIfAllocated( column );

							if ( row == activeRow )
								this.activeRowElem = rowCellAreaElem;

							if ( null != cell && cell == activeCell )
							{
								this.activeCellElem = cellElem;
								this.isActiveCellElemInEditMode = cell.IsInEditMode &&
									LayoutUtility.IsElementInActiveScrollRegion( this.activeCellElem );

								// Since we want the cell in edit mode to draw don't set it's merged cell pointer.
								// 
								// SSP 5/9/06 BR12064
								// Also display the active cell if its style is Button. Use the new IsActiveCellElemDrawn method.
								// 
								//if ( this.isActiveCellElemInEditMode )
								if ( this.IsActiveCellElemDrawn( ) )
									cellElem.SetMergedCellElement( null );
							}

							// Keep a count of selected cells.
							//
							if ( row.Selected || null != cell && cell.Selected )
								this.selectedCellsCount++;
						}						
					}
				}
			}

			mergedCell.ParentCollection.likelyCellElementIndex = likelyCellElemIndex;

            // MBS 7/6/07 - BR24110
            // We should ask for the cursor as well
            //AppearancePropFlags appearanceFlags = AppearancePropFlags.AllRender;
            AppearancePropFlags appearanceFlags = AppearancePropFlags.AllRenderAndCursor;

			this.cachedAppData = new AppearanceData( );
			mergedCell.ResolveAppearance( ref this.cachedAppData, ref appearanceFlags, 
				this.selectedCellsCount == this.CellElements.Count ? CellState.Selected : CellState.None );
		}

		#endregion // InitCellElemsList

		#region ResetCachedInfo
		
		private void ResetCachedInfo( )
		{
			if ( null != this.rowElements )
				this.rowElements.Clear( );

			this.ResetCellElemsList( );
		}

		#endregion // ClearElementLists

		#region OnEmbeddableElementMouseDown

		internal void OnEmbeddableElementMouseDown( object sender, Infragistics.Win.EmbeddableMouseDownEventArgs e )
		{
			// If the cell underneath is a checkbox then we have to mannually enter edit mode
			// since the check editor changes its value on mouse up. We don't want to change 
			// the value of the cell underneath as soon as the user clicks on the merged cell
			// to enter edit mode on the cell underneath.
			//
			Point point = new Point( e.MouseArgs.X, e.MouseArgs.Y );
			if ( this.ShouldMannuallyEnterEditMode( point ) )
			{
				CellUIElement cellElem = this.GetCellElemFromPoint( point );
				EmbeddableUIElementBase embeddableElem = null != cellElem 
					// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					// Use the FindEmbeddableUIElement instead which searches only the child elements.
					// In filter cells there can be two embeddable elements. One for the operand and 
					// the other for operator. Here we are interested in getting the operand editor 
					// element and since the operator editor element not an immediate child (unlike
					// the operand editor element which) using the FindEmbeddableUIElement gets us
					// the operand editor element.
					// 
					//? (EmbeddableUIElementBase)cellElem.GetDescendant( typeof( EmbeddableUIElementBase ) ) : null;
					? cellElem.FindEmbeddableUIElement( false ) : null;

				if ( null != embeddableElem && null != embeddableElem.Editor )
					embeddableElem.Editor.EnterEditMode( embeddableElem );
			}

			// For other editors nothing needs to be done here since mouse downs should not be 
			// delivered to the editor elements.
			//
		}

		#endregion // OnEmbeddableElementMouseDown

		#region ResolveAppearance
		
		internal void ResolveAppearance( ref AppearanceData appData, ref AppearancePropFlags requestedProps )
		{
			AppearanceData.Merge( ref appData, ref this.cachedAppData, ref requestedProps );
		}

		#endregion // ResolveAppearance

		#region PositionSelf
		
		private static void ExcludeHorizBorders( ref Rectangle mergedCellClipRect, 
			RowUIElementBase firstRowElem, RowUIElementBase lastRowElem )
		{
			if ( null != firstRowElem && mergedCellClipRect.Y == firstRowElem.Rect.Y )
			{
				mergedCellClipRect.Y++;
				mergedCellClipRect.Height--;
			}

			if ( null != lastRowElem && mergedCellClipRect.Bottom == lastRowElem.Rect.Bottom )
			{
				mergedCellClipRect.Height--;
			}
		}
	
		// SSP 6/27/05 BR04617
		// Added GetRowClipRect.
		// 
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private Rectangle GetRowClipRect( RowUIElementBase rowElem )
		private static Rectangle GetRowClipRect( RowUIElementBase rowElem )
		{
			Rectangle rect = rowElem.ClipSelfRect;
			if ( rect.IsEmpty )
				rect = rowElem.Rect;

			return rect;
		}
		
		private bool PositionSelf( )
		{
			// Resolve and cache the appearance.
			//
			UltraGridColumn column = this.Column;
			UltraGridBand band = column.Band;

			RowUIElementBase firstVisibleRowElem = null, lastVisibleRowElem = null;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//CellUIElement cellElem = null, firstVisibleCellElem = null, lastVisibleCellElem = null;
			CellUIElement cellElem = null;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//CellUIElement lastVisibleCellElem = null;

			// Init CellElements array among other things.
			//
			this.InitCellElemsList( );

			if ( this.RowElements.Count > 0 )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//firstVisibleRowElem = (RowUIElementBase)this.rowElements[ 0 ];
				//lastVisibleRowElem = (RowUIElementBase)this.rowElements[ this.rowElements.Count - 1 ];
				firstVisibleRowElem = this.rowElements[ 0 ];
				lastVisibleRowElem = this.rowElements[ this.rowElements.Count - 1 ];
			}

			if ( this.CellElements.Count > 0 )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//cellElem = firstVisibleCellElem = (CellUIElement)this.cellElements[ 0 ];
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//cellElem = (CellUIElement)this.cellElements[ 0 ];
				cellElem = this.cellElements[ 0 ];

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//lastVisibleCellElem = (CellUIElement)this.cellElements[ this.cellElements.Count - 1 ];
			}

			Debug.Assert( null != firstVisibleRowElem && null != lastVisibleRowElem );
			
			// Based on a cell element underneath this merged cell figure out what the left and
			// the width of the merged cell element should be. When we add merged cells in row col
			// region intersection element we don't set their rects' X and Width properties.
			//
			if ( null != cellElem && null != firstVisibleRowElem && null != lastVisibleRowElem )
			{
				this.borderStyle = cellElem.BorderStyle;
				Rectangle mergedCellRect = cellElem.Rect;
				mergedCellRect.Y = this.elemTop;
				mergedCellRect.Height = this.elemHeight;

				// Set the rect to the calculated merged cell rect.
				//
				Rectangle oldRect = this.Rect;
				if ( oldRect.Size == mergedCellRect.Size 
					&& this.verifiedCellChildElementsCacheVersion == column.CellChildElementsCacheVersion 
					// If the editor element is positioned based on the clip rect then don't use 
					// the optimization.
					//
					&& MergedCellContentArea.VisibleRect != column.MergedCellContentAreaResolved )
				{
					this.Offset( mergedCellRect.X - oldRect.X, mergedCellRect.Y - oldRect.Y, true );
					this.childElementsCollectionDirty = false;
				}
				else
				{
					this.verifiedCellChildElementsCacheVersion = column.CellChildElementsCacheVersion;
					this.Rect = mergedCellRect;
				}
				
				// Calculate the clip rect if any for self clipping this merged cell. We need to do
				// this because the merged cell could be partillay scrolled out of view in which
				// case we don't want the merged cell to paint over the headers and potentially other
				// elements. Note: the merged cell elements are positioned after header and row elems
				// in the row col region intersection element.
				//
				Rectangle mergedCellClipRect = mergedCellRect;

				// SSP 6/27/05 BR04617
				// 
				// ----------------------------------------------------------------------------
				//int newY = Math.Max( mergedCellClipRect.Y, firstVisibleRowElem.Rect.Y );
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//Rectangle firstRowClipSelfRect = this.GetRowClipRect( firstVisibleRowElem );
				Rectangle firstRowClipSelfRect = MergedCellUIElement.GetRowClipRect( firstVisibleRowElem );

				int newY = Math.Max( mergedCellClipRect.Y, firstRowClipSelfRect.Y );
				// ----------------------------------------------------------------------------

				mergedCellClipRect.Height += mergedCellClipRect.Y - newY;
				mergedCellClipRect.Y = newY;

				// Set the clip rect of the merged cell element to the visible portion. The
				// visible portion is the area from top of the first row element to the bottom
				// of the last row element. Clip rect is used by the PositionChildElements to
				// position the embeddable editor when the merged cell content area is set to
				// VisibleRect. The reason for using Rect instead of ClipRect if first and 
				// last row elems are the same is that we want to use at least one row's height 
				// for the merged cell contents.
				//
				// SSP 6/27/05 BR04617
				// 
				// ----------------------------------------------------------------------------
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//Rectangle lastRowClipSelfRect = this.GetRowClipRect( lastVisibleRowElem );
				Rectangle lastRowClipSelfRect = MergedCellUIElement.GetRowClipRect( lastVisibleRowElem );

				mergedCellClipRect.Height = Math.Min( mergedCellClipRect.Height,
					lastRowClipSelfRect.Bottom - mergedCellClipRect.Y );
				
				// ----------------------------------------------------------------------------

				Rectangle cellClipSelfRect = cellElem.ClipSelfRect;
				if ( ! cellClipSelfRect.IsEmpty )
				{
					mergedCellClipRect.X = cellClipSelfRect.X;
					mergedCellClipRect.Width = cellClipSelfRect.Width;
				}

				RowCellAreaUIElementBase rowCellArea = cellElem.RowCellAreaElement;
				if ( rowCellArea.usingFixedHeaders )
				{
					int deltaX = rowCellArea.RectInsideBorders.X - mergedCellClipRect.X;
					if ( deltaX > 0 )
					{
						mergedCellClipRect.X += deltaX;
						mergedCellClipRect.Width -= deltaX;
					}
				}

				// Figure out which borders the merged cell should draw. Following code also adjustes
				// the clip self rect to exclude some border areas in some situations so the merged 
				// cell doesn't overwrite the row borders underneath.
				//				
				this.borderSides = Border3DSide.All;
				if ( null != rowCellArea )
				{
					bool usingRowLayout = band.UseRowLayoutResolved;

					if ( band.Layout.CanMergeAdjacentBorders( rowCellArea.BorderStyle, this.borderStyle ) )
					{
						// In non-row-layout mode when the row border color and the cell border color is
						// different the vertical borders are drawn using the cell border color and the
						// horizontal borders are drawn with the row border color. To maintain adjust the
						// clip self rect to exlcude the row border area so the row border (horizontal 
						// borders) show up.
						//
						if ( ! usingRowLayout )
						{
							// Also don't draw the left border (just like cell elements) if this is the first
							// cell element otherwise you will see double left borders. One from the row and 
							// one from the cell. This only applies to non-row-layout mode.
							//
							Rectangle rowCellAreaRect = rowCellArea.Rect;
							if ( mergedCellRect.X == rowCellAreaRect.X + 1 )
								this.borderSides &= ~Border3DSide.Left;

							// SSP 4/26/05 BR03223
							// If the row and cell borders are dotted then in order for them to be drawn correctly we need
							// to not merge the borders. Instead we need to simply not draw the cell border that overlaps
							// with the row border. However we can not have the cell span over the row border otherwise
							// it will draw over the row border. So we always need to shrink the cell by the thickness
							// of the row borders regardless of where the row and cell borders are mergeable. Commented 
							// out the following condition that checks to see if the row and cell borders are mergeable. 
							// Always substract the row border width.
							//
							if ( mergedCellClipRect.Right + 1 == rowCellAreaRect.Right )
								this.borderSides &= ~Border3DSide.Right;

							MergedCellUIElement.ExcludeHorizBorders( 
								ref mergedCellClipRect, firstVisibleRowElem, lastVisibleRowElem );
						}
						else if ( firstVisibleRowElem.topBorderOverlappedWithHeaders )
						{
							MergedCellUIElement.ExcludeHorizBorders( 
								ref mergedCellClipRect, firstVisibleRowElem, null );
						}
					}
						// MergeableCellBordersBetweenRows flag off the RowCellAreaUIElement is set in
						// the PositionChildElements of the RowCellAreaUIElement. Look there for more
						// info. If the row border style is None and the cell border style is mergeable
						// then we have special logic in RowCellAreaUIElement to avoid showing double
						// borders. Apparently this logic is only turned on for non-row-layout more.
						//
					else if ( ! usingRowLayout && rowCellArea.mergeableCellBordersBetweenRows )
					{
						this.borderSides &= ~Border3DSide.Top;
					}
					else if ( UIElementBorderStyle.None == this.borderStyle
						&& UIElementBorderStyle.None != rowCellArea.BorderStyle )
					{
						MergedCellUIElement.ExcludeHorizBorders( 
							ref mergedCellClipRect, firstVisibleRowElem, lastVisibleRowElem );
					}
				}

				// Only clip self if the merged cell is partially scrolled out of view.
				//
				this.clipSelfRect = mergedCellRect != mergedCellClipRect ? mergedCellClipRect : Rectangle.Empty;

				return true;
			}
			else
			{
				// If there aren't any cell elements underneath this merged cell then the merged cell 
				// should not be visible. Dispose of this merged cell element. VerifyChildElements of
				// RowColRegionIntersectionUIElement will remove any disposed elements from its child
				// elements collection. This situation would arise when this element happens to be 
				// associated with a column that's completely scrolled underneath a fixed header.
				//
				if ( ! this.Disposed )
					this.Dispose( );

				return false;
			}
		}

		#endregion // PositionSelf

		#region GetCellRectHelper

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private Rectangle GetCellRectHelper( CellUIElement cellElem, bool includeEntireRowHeight )
		private static Rectangle GetCellRectHelper( CellUIElement cellElem, bool includeEntireRowHeight )
		{
			Rectangle rect = cellElem.Rect;
			Rectangle rowCellAreaRect = cellElem.RowCellAreaElement.Rect;
			Border3DSide cellBorders = cellElem.BorderSides;

			if ( ! includeEntireRowHeight )
			{
				// In non-row-layout mode the cells do not draw borders that are adjacent to the row borders.
				// If that's the case then include the row borders so they show up.
				//
				if ( 0 == ( Border3DSide.Top & cellBorders ) && rect.Y - 1 == rowCellAreaRect.Y )
				{
					rect.Y--;
					rect.Height++;
				}

				if ( 0 == ( Border3DSide.Bottom & cellBorders ) && rect.Bottom + 1 == rowCellAreaRect.Bottom )
				{
					rect.Height++;
				}
			}
			else
			{
				rect.Y = rowCellAreaRect.Y;
				rect.Height = rowCellAreaRect.Height;
			}

			return rect;
		}

		#endregion // GetCellRectHelper

		#region CreateRegion

		private System.Drawing.Region CreateRegion( bool forceCreation )
		{
			// The reason why we are creating a new instance of region is because the element's
			// Draw method disposes of this after accessing it.
			//
			System.Drawing.Region region = ! this.clipSelfRect.IsEmpty ? new Region( this.clipSelfRect ) : base.Region;

			// If a cell under this merged cell is in edit mode exclude that cell area from the 
			// region so the cell in edit mode from underneath shows up.
			//
			// SSP 5/9/06 BR12064
			// Also display the active cell if its style is Button. Use the new IsActiveCellElemDrawn method.
			// 
			//if ( null != this.activeCellElem && this.isActiveCellElemInEditMode )
			if ( this.IsActiveCellElemDrawn( ) )
			{
				if ( null == region )
					region = new System.Drawing.Region( this.ClipRect );

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//Rectangle rect = this.GetCellRectHelper( activeCellElem, false );
				Rectangle rect = MergedCellUIElement.GetCellRectHelper( activeCellElem, false );

				region.Exclude( rect );
			}
			else if ( null == region && forceCreation )
				region = new System.Drawing.Region( this.ClipRect );

			return region;
		}

		#endregion // CreateRegion

		#region IsActiveCellElemDrawn

		// SSP 5/9/06 BR12064
		// Added IsActiveCellElemDrawn and IsCellStyleButton methods.
		// 
		private bool IsActiveCellElemDrawn( )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//return null != this.activeCellElem 
			//    && ( this.isActiveCellElemInEditMode || this.IsCellStyleButton( this.activeCellElem ) );
			return null != this.activeCellElem
				&& ( this.isActiveCellElemInEditMode || MergedCellUIElement.IsCellStyleButton( this.activeCellElem ) );
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool IsCellStyleButton( CellUIElementBase cellElem )
		private static bool IsCellStyleButton( CellUIElementBase cellElem )
		{
			if ( null != cellElem )
			{
				CellButtonUIElement buttonElem = (CellButtonUIElement)GridUtils.GetChild( cellElem, typeof( CellButtonUIElement ) );
				if ( null != buttonElem )
					return true;
			}

			return false;
		}

		#endregion // IsActiveCellElemDrawn

		#region GetFocusRectHelper

		// SSP 3/25/05 BR03031
		// Added GetFocusRectHelper method. Code in this method was moved from OnAfterDraw
		// (slightly modified ofcourse) and removed OnAfterDraw since it's not needed any more.
		//
		private Rectangle GetFocusRectHelper( )
		{
			// Draw active row/cell focus rect.
			//
			UIElement activeElem = null;
			if ( null != this.activeCellElem )
			{
				activeElem = ! this.isActiveCellElemInEditMode ? this.activeCellElem : null;
			}
			else if ( null != this.activeRowElem )
			{
				UltraGridBand band = null != this.MergedCell ? this.MergedCell.Band : null;
				if ( null != band && null != band.Layout )
					activeElem = null == band.Layout.ActiveCell ? activeRowElem : null;
			}

			// Only draw the focus rect if in active scroll region.
			//
			if ( null != activeElem && LayoutUtility.IsElementInActiveScrollRegion( this ) )
			{
				return activeElem.RectInsideBorders;
			}

			return Rectangle.Empty;
		}

		#endregion // GetFocusRectHelper

		#region GetCellElemFromPoint

		private CellUIElement GetCellElemFromPoint( Point point )
		{
			if ( null != this.cellElements )
			{
				for ( int i = 0; i < this.cellElements.Count; i++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//CellUIElement cellElem = (CellUIElement)this.cellElements[i];
					CellUIElement cellElem = this.cellElements[ i ];

					if ( null != cellElem && cellElem.Rect.Contains( point ) )
						return cellElem;
				}
			}

			return null;
		}

		#endregion // GetCellElemFromPoint

		#region ShouldMannuallyEnterEditMode

		private bool ShouldMannuallyEnterEditMode( Point point )
		{
			// If the cell underneath is a checkbox then we have to mannually enter edit mode
			// since the check editor changes its value on mouse up. We don't want to change 
			// the value of the cell underneath as soon as the user clicks on the merged cell
			// to enter edit mode on the cell underneath.
			//
			CellUIElement cellElem = this.GetCellElemFromPoint( point );
			EmbeddableUIElementBase editorElem = null != cellElem 
				// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				// Use the FindEmbeddableUIElement instead which searches only the child elements.
				// In filter cells there can be two embeddable elements. One for the operand and 
				// the other for operator. Here we are interested in getting the operand editor 
				// element and since the operator editor element not an immediate child (unlike
				// the operand editor element which) using the FindEmbeddableUIElement gets us
				// the operand editor element.
				// 
				//? (EmbeddableUIElementBase)cellElem.GetDescendant( typeof( EmbeddableUIElementBase ) ) : null;
				? cellElem.FindEmbeddableUIElement( false ) : null;

			return null != editorElem && editorElem.Editor is CheckEditor 
				&& null != editorElem.Owner && ! editorElem.IsInEditMode
				&& ! editorElem.Owner.IsReadOnly( editorElem.OwnerContext )
				&& editorElem.Owner.IsEnabled( editorElem.OwnerContext );
		}

		#endregion // ShouldMannuallyEnterEditMode

		#region CalculateSpecialRegions

		// SSP 9/15/05 BR06301
		// Added CalculateSpecialRegions method.
		// 
		private void CalculateSpecialRegions( out Region selectedRegion, out CellUIElement activeCellElem, out CellUIElement hotTrackedCellElem )
		{
			selectedRegion = null;
			activeCellElem = null;
			hotTrackedCellElem = null;

			UltraGridColumn column = this.Column;

			for ( int pass = 0; pass < 3; pass++ )
			{
				for ( int i = 0, count = this.cellElements.Count; i < count; i++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//CellUIElement cellElem = (CellUIElement)this.cellElements[i];
					CellUIElement cellElem = this.cellElements[ i ];

					UltraGridRow row = cellElem.Row;

					if ( 0 == pass )
					{
						// Hot-tracked region pass.
						// 

						// SSP 7/2/07 BR24510
						// 
						//RowUIElementBase rowElem = (RowUIElementBase)cellElem.GetAncestor( typeof( RowUIElementBase ) );

						if (
							// SSP 7/2/07 BR24510
							// 
							//null != rowElem && rowElem.isMouseOverSelectorOrCellArea
							GridUtils.IsRowHotTracking( cellElem )
							&& ( row.HasRowHotTrackingAppearances || row.HasCellHotTrackingAppearances( column ) ) )
						{
							hotTrackedCellElem = cellElem;
							break;
						}
					}
					else if ( 1 == pass )
					{
						// Active region pass.
						// 

						if ( null != this.activeRowElem && row == this.activeRowElem.Row )
						{
							if ( cellElem != hotTrackedCellElem )
								activeCellElem = cellElem;

							break;
						}
					}
					else 
					{
						// Selected region pass.
						// 

						if ( cellElem == activeCellElem || cellElem == hotTrackedCellElem )
							continue;

						if ( null == selectedRegion )
							selectedRegion = new System.Drawing.Region( Rectangle.Empty );

						UltraGridCell cell = null != row ? row.GetCellIfAllocated( column ) : null;
						if ( null != cell && cell.Selected )
						{
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//selectedRegion.Union( this.GetCellRectHelper( cellElem, false ) );
							selectedRegion.Union( MergedCellUIElement.GetCellRectHelper( cellElem, false ) );
						}
						else if ( null != row && row.Selected )
						{
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//selectedRegion.Union( this.GetCellRectHelper( cellElem, true ) );
							selectedRegion.Union( MergedCellUIElement.GetCellRectHelper( cellElem, true ) );
						}
					}
				}
			}
		}

		#region Commented Out Code

		

		#endregion // Commented Out Code

		#endregion // CalculateSpecialRegions

		#endregion // Private/Internal Methods

		#region Overridden Properties

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.Column.Band.BorderStyleCellResolved;
			}
		}

		#endregion // BorderStyle

		#region BorderSides

		/// <summary>
		/// Overridden property that returns an appropriate Border3DSide
		/// structure for drawing the borders for the cell UI element
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return this.borderSides;
			}
		}

		#endregion // BorderSides
		
		#region ClipSelf

		/// <summary>
		/// Returning true causes all drawing of this element to be expicitly clipped
		/// to its region
		/// </summary>
		protected override bool ClipSelf 
		{ 
			get 		  
			{ 
				return ! this.clipSelfRect.IsEmpty 
					// SSP 5/9/06 BR12064
					// Also display the active cell if its style is Button. Use the new IsActiveCellElemDrawn method.
					// 
					//|| null != this.activeCellElem && this.isActiveCellElemInEditMode;
					|| this.IsActiveCellElemDrawn( );
			} 
		}
 
		#endregion // ClipSelf

		#region Region

		/// <summary>
		/// Returns the region of this element. The deafult returns the element's
		/// Rect as a region. This method can be overriden to supply an irregularly
		/// shaped region 
		/// </summary>
		public override System.Drawing.Region Region
		{
			get
			{
                return this.CreateRegion( true );
			}
		}
 
		#endregion // Region

		#region ThemedElementRegion

		/// <summary>
		/// Overridden.
		/// </summary>
		public override System.Drawing.Region ThemedElementRegion
		{
			get
			{
				return this.CreateRegion( false );
			}
		}

		#endregion // ThemedElementRegion

		#region DrawsFocusRect

		// SSP 3/25/05 BR03031
		// Before we were drawing focus rect of the active cell/row passing through the merged cell
		// in OnAfterDraw however that skips the draw filter logic. So instead use the DrawsFocusRect
		// and DrawFocus methods for that. Overrode those two methods.
		//
		/// <summary>
		/// Returns true if this  element needs to draw a focus rect. This should 
		/// be overridden since the default implementation always returns false. 
		/// </summary>
		/// <remarks>Even if this property returns true the focus will not be drawn unless the control has focus.</remarks>
		protected override bool DrawsFocusRect
		{
			get
			{
				return ! this.GetFocusRectHelper( ).IsEmpty;
			}
		}

		#endregion // DrawsFocusRect

		#region UIRole

		// SSP 4/18/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.MergedCell.Band, StyleUtils.Role.Cell );
			}
		}

		#endregion // UIRole

		#endregion // Overridden Properties

		#region Overridden Methods

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags requestedProps)
		{
			this.ResolveAppearance( ref appData, ref requestedProps );
		}

		#endregion // InitAppearance

		#region GetContext

        /// <summary>
        /// Returns an object of requested type that relates to the element or null.
        /// </summary>
        /// <param name="type">The requested type or null to pick up default context object.</param>
        /// <param name="checkParentElementContexts">If true will walk up the parent chain looking for the context.</param>
        /// <returns>Returns null or an object of requested type that relates to the element.</returns>
        /// <remarks> Classes that override this method normally need to override the <see cref="Infragistics.Win.UIElement.ContinueDescendantSearch(System.Type,System.Object[])"/> method as well.</remarks>
        public override object GetContext(Type type, bool checkParentElementContexts)
		{
			if ( typeof( UltraGridCell ) == type )
				return this.Cell;

			if ( typeof( UltraGridRow ) == type )
				return this.Row;

			return base.GetContext( type, checkParentElementContexts );
		}

		#endregion // GetContext

		#region GetDescendant

		/// <summary>
		/// Overridden. Returns the element of the requested type that satisfies all of the passed in contexts
		/// </summary>
		/// <param name="type"><see cref="Type"/>The type (or base class type) of the UIElement to look for.</param>
		/// <param name="contexts">The contexts that must all match with the contexts of the element.</param>
		/// <returns>Descendent element of this element that matches the specified type and contexts.</returns>
		public override UIElement GetDescendant( Type type, object[] contexts )
		{
			if ( null != contexts && UIElement.IsContextOfType( this, type ) )
			{
				bool allContextsMatched = true;
				for ( int i = 0; allContextsMatched && i < contexts.Length; i++ )
				{
					UltraGridRow row = contexts[i] is UltraGridRow ? (UltraGridRow)contexts[i] 
						: ( contexts[i] is UltraGridCell ? ((UltraGridCell)contexts[i]).Row : null );

					allContextsMatched = null != row && this.Column.AreCellsMerged( this.Row, row );
				}

				if ( allContextsMatched )
					return this;
			}

			return base.GetDescendant( type, contexts );
		}

		#endregion // GetDescendant

		#region WantsInputNotification

		/// <summary>
        ///  Returns true if this ui element is interested in getting notificaions of type inputType at the specified location.
		/// </summary>
        /// <param name="inputType">The type of notification.</param>
		/// <param name="point">Point of interest.</param>
        /// <returns>true if this ui element is interested in getting notificaions of type inputType at the specified location.</returns>
		protected override bool WantsInputNotification( UIElementInputType inputType, Point point )
		{
			// SSP 10/4/07 BR25629
			// Also make sure that the mouse is not over an actual cell that's in edit mode. In
			// that case that portion of the merged cell will be clipped out (to show the cell
			// being edited underneath) and thus should not process any mouse messages and let
			// all the mouse notifications go to the cell being edited.
			// 
			CellUIElement cellElemUnderPoint = this.GetCellElemFromPoint( point );
			if ( null != cellElemUnderPoint && cellElemUnderPoint.IsInEditMode )
				return false;

			// If the cell underneath is a checkbox then we have to mannually enter edit mode
			// since the check editor changes its value on mouse up. We don't want to change 
			// the value of the cell underneath as soon as the user clicks on the merged cell
			// to enter edit mode on the cell underneath.
			//
			if ( this.ShouldMannuallyEnterEditMode( point ) )
				return true;

			// SSP 10/4/07 BR25629
			// If the merged cell contains a link and the mouse is over the link then handle the mouse
			// cliks so the link gets activated.
			// 
			// --------------------------------------------------------------------------------------------
			EmbeddableUIElementBase editorElem = CellUIElementBase.FindEmbeddableUIElement( this.childElementsCollection, false );
			if ( editorElem is FormattedLinkLabel.FormattedLinkEmbeddableUIElement )
			{
				FormattedLinkLabel.FormattedTextUIElement formattedTextElem = (FormattedLinkLabel.FormattedTextUIElement)
					editorElem.GetDescendant( typeof( FormattedLinkLabel.FormattedTextUIElement ) );

				if ( null != formattedTextElem && formattedTextElem.HasMouseActionAt( point ) )
					return true;
			}
			// --------------------------------------------------------------------------------------------

			// Let mouse down, click and double click events go to cells underneath.
			//
			return UIElementInputType.MouseClick != inputType
				&& UIElementInputType.MouseDoubleClick != inputType
				&& UIElementInputType.MouseDownUp != inputType;
		}

		#endregion // WantsInputNotification

		#region Contains

        /// <summary>
        /// Checks if the point is over the element.
        /// </summary>
        /// <param name="point">In client coordinates.</param>
        /// <param name="ignoreClipping">Specifieds if we should ignore clipping or not</param>
        /// <returns>Returns true if the point is over the element.</returns>
        public override bool Contains(Point point, bool ignoreClipping)
		{
			// Since the merged cell element could be over headers and other elements
			// (because the merged cell elements are sized their virtual dimensions
			// rather than visible portions) return true only if the point is contained
			// in the visible portion otherwise the header and other elements underneath 
			// will never receive any mouse notifications.
			//
			return this.clipSelfRect.IsEmpty 
				? base.Contains( point, ignoreClipping ) 
				: this.clipSelfRect.Contains( point );
		}

		#endregion // Contains

		#region DrawBackColor

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void DrawBackColor( ref UIElementDrawParams drawParams )
		{
			// AS 1/27/03 UWG1968/UWG1966
			// When the element is within another element and alphablending is
			// occuring, having the parent and this element render the backcolor
			// ends up alphablending this backcolor onto that rendered by the parent
			// element. When the backcolor is white, it appears as if the cell/element
			// is lighter than others that do not have this layering.
			//
		}

		#endregion // DrawBackColor

		#region DrawChildElements

		/// <summary>
		/// Overridden.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawChildElements( ref UIElementDrawParams drawParams )
		{
			Infragistics.Win.UltraWinGrid.MergedCell mergedCell = this.MergedCell;

			System.Drawing.Region selectedRegion = null;
			CellUIElement activeCellElem = null;
			CellUIElement hotTrackedCellElem = null;
			if ( null != mergedCell )
				this.CalculateSpecialRegions( out selectedRegion, out activeCellElem, out hotTrackedCellElem );

			if ( null == selectedRegion && null == activeCellElem && null == hotTrackedCellElem )
			{
				base.DrawChildElements( ref drawParams );
			}
			else
			{
				System.Drawing.Region oldRegion = drawParams.Graphics.Clip;

				// We need to set the cachedAppData temporarily to the selected appearance so the 
				// descendant embeddable editor elements get the selected appearance. 
				// Embeddable owner info uses cachedAppData.
				//
				AppearanceData origCachedAppData = this.cachedAppData;

				System.Drawing.Region activeRegion = null, hotTrackingRegion = null;

				try
				{
					activeRegion = null != activeCellElem
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//? new Region( this.GetCellRectHelper( activeCellElem, activeCellElem != this.activeCellElem ) )
						? new Region( MergedCellUIElement.GetCellRectHelper( activeCellElem, activeCellElem != this.activeCellElem ) )
						: null;

					hotTrackingRegion = null != hotTrackedCellElem
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//? new Region( this.GetCellRectHelper( hotTrackedCellElem, ! hotTrackedCellElem.isMouseOverCell ) )
						? new Region( MergedCellUIElement.GetCellRectHelper( hotTrackedCellElem, !hotTrackedCellElem.isMouseOverCell ) )
						: null;

					// Draw the non-selected portion.
					//
					if ( null != selectedRegion )
						drawParams.Graphics.ExcludeClip( selectedRegion );

					// SSP 8/18/05 BR05562
					// 
					if ( null != activeRegion )
						drawParams.Graphics.ExcludeClip( activeRegion );

					// SSP 9/15/05 BR06301
					// 
					if ( null != hotTrackingRegion )
						drawParams.Graphics.ExcludeClip( hotTrackingRegion );

					base.DrawChildElements( ref drawParams );

					// Now draw the selected portion.
					//

					// SSP 8/18/05 BR05562
					// Enclosed the existing code into the for loop.
					// 
					for ( int phase = 0; phase <= 2; phase++ )
					{
						bool hotTrackingPhase	= 0 == phase;
						bool selectedPhase		= 1 == phase;
						bool activePhase		= 2 == phase;

						System.Drawing.Region phaseRegion = null;
						if ( selectedPhase )
							phaseRegion = selectedRegion;
						else if ( activePhase )
							phaseRegion = activeRegion;
						else if ( hotTrackingPhase )
							phaseRegion = hotTrackingRegion;
						
						if ( null == phaseRegion )
							continue;

						// First resolve the selected appearance and copy over the settings to the draw params app data.
						AppearanceData selectedAppData = new AppearanceData( );
						const AppearancePropFlags SELECTED_APP_FLAGS = AppearancePropFlags.BackColor 
								  | AppearancePropFlags.BackColor2 | AppearancePropFlags.BackColorAlpha 
								  | AppearancePropFlags.BackColorDisabled | AppearancePropFlags.BackColorDisabled2 
								  | AppearancePropFlags.BackGradientAlignment | AppearancePropFlags.ForeColor 
								  | AppearancePropFlags.ForeColorDisabled | AppearancePropFlags.ForegroundAlpha
								  | AppearancePropFlags.ThemedElementAlpha
								  // SSP 9/19/05 BR06535 
								  // 
								  | AppearancePropFlags.BackGradientStyle;

						AppearancePropFlags flags = SELECTED_APP_FLAGS;

						// SSP 8/18/05 BR05562
						// 
						//mergedCell.ResolveAppearance( ref selectedAppData, ref flags, true );
						CellState state = CellState.None;
						UltraGridRow resolveAppearanceRow = null;
						if ( selectedPhase )
						{
							state |= CellState.Selected;
						}
						else if ( activePhase )
						{
							state |= CellState.ActiveRow;
							if ( null != this.activeCellElem )
								state |=  CellState.ActiveCell;

							resolveAppearanceRow = null != activeCellElem ? activeCellElem.Row : null;
						}
						else if ( hotTrackingPhase )
						{
							state |= CellState.HotTrackedRow;
							if ( hotTrackedCellElem.isMouseOverCell )
								state |= CellState.HotTrackedCell;

							resolveAppearanceRow = null != hotTrackedCellElem ? hotTrackedCellElem.Row : null;
						}

						if ( null == resolveAppearanceRow )
							mergedCell.ResolveAppearance( ref selectedAppData, ref flags, state );
						else 
						{
							UltraGridCell cell = resolveAppearanceRow.GetCellIfAllocated( mergedCell.Column );
							if ( resolveAppearanceRow.Selected || null != cell && cell.Selected )
								state |= CellState.Selected;

							resolveAppearanceRow.ResolveMergedCellAppearance( 
								mergedCell.Column, ref selectedAppData, ref flags, state );
						}

						// This doesn't work because Merge won't copy props that are already set on the dest 
						// so take a long way around.
						//
						//flags ^= SELECTED_APP_FLAGS;
						//AppearanceData.Merge( ref drawParams.AppearanceData, ref selectedAppData, ref flags );
						flags ^= SELECTED_APP_FLAGS ^ AppearancePropFlags.AllRender;
						AppearanceData.Merge( ref selectedAppData, ref drawParams.AppearanceData, ref flags );
						drawParams.AppearanceData = selectedAppData;

						// Set the cachedAppData temporarily to the selected appearance so the descendant
						// embeddable editor elements get the selected appearance. Embeddable owner info
						// uses cachedAppData.
						//
						this.cachedAppData = selectedAppData;
                    
						// Invert the selected region to figure out the non-selected region.
						//
						if ( null != oldRegion )
						{
							phaseRegion.Intersect( oldRegion );
						}
						else
						{
							using ( System.Drawing.Region tmpRegion = this.CreateRegion( true ) )
								phaseRegion.Intersect( tmpRegion );
						}

						drawParams.Graphics.Clip = phaseRegion;
						base.DrawChildElements( ref drawParams );
					}
				}
				finally
				{
					this.cachedAppData = origCachedAppData;

					drawParams.Graphics.Clip = oldRegion;
					oldRegion.Dispose( );

					if ( null != selectedRegion )
						selectedRegion.Dispose( );

					if ( null != activeRegion )
						activeRegion.Dispose( );
				}
			}
		}

		#endregion // DrawChildElements

		#region DrawImageBackground 

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
			// AS 1/27/03 UWG1968/UWG1966
			// When the element is within another element and alphablending is
			// occuring, having the parent and this element render the backcolor
			// ends up alphablending this backcolor onto that rendered by the parent
			// element. When the backcolor is white, it appears as if the cell/element
			// is lighter than others that do not have this layering.
			//
		}

		#endregion // DrawImageBackground 

		#region IntersectInvalidRect

		// SSP 8/6/04 UWG3380
		// Overrode IntersectInvalidRect to fix the problem where a check indicator in 
		// a non-fixed cell that was scrolled under a fixed cell was not clipped and
		// appeared through the fixed cell. This is because the themes-drawing does not
		// honor UIElement.Region property that we are overriding.
		//
		/// <summary>
		/// Returns the intersection of the element's rect with the invalid rect for the
		/// current draw operation.
		/// </summary>
		/// <param name="invalidRect">Invalid rect</param>
		/// <returns>The intersection of the element's rect with the invalid rect.</returns>
		protected override Rectangle IntersectInvalidRect( Rectangle invalidRect )
		{
			return this.clipSelfRect.IsEmpty
				? base.IntersectInvalidRect( invalidRect )
				: Rectangle.Intersect( invalidRect, this.clipSelfRect );
		}

		#endregion IntersectInvalidRect 

		#region VerifyChildElements

        /// <summary>
        /// Called during a drawing operation to ensure that all child elements are created and positioned properly. If the ChildElementsDirty flag is true then the default implementation will call PositionChildElements and reset the flag.
        /// </summary>
        /// <param name="controlElement">The control's main UIElement</param>
        /// <param name="recursive">If true will call this method on all descendant elements</param>
        protected override void VerifyChildElements(ControlUIElementBase controlElement, bool recursive)
		{
			if ( this.childElementsCollectionDirty )
			{
				if ( ! this.PositionSelf( ) )
					return;
			}
			
			base.VerifyChildElements( controlElement, recursive );
		}

		#endregion // VerifyChildElements

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			Debug.Assert( ! this.Disposed && this.Rect.Width > 0 );

			EmbeddableUIElementBase embeddableElement = CellUIElement.FindEmbeddableUIElement( 
				this.childElementsCollection, true );

			UltraGridColumn column = this.Column;
			UltraGridRow row = this.Row;
			EmbeddableEditorBase editor = column.GetEditor( row );

			// If the underlying cell value is an image then use the EmbeddableImageRenderer 
			// instead of the editor returned by GetEditor.
			//
			object cellValue = ! column.ShouldCheckForImageCellValue ? null : row.GetCellValue( column );
			if ( cellValue is Image && ! ( editor is EmbeddableImageRenderer ) )
				editor = row.Layout.ImageRenderer;

			embeddableElement = editor.GetEmbeddableElement( 
				this, column.EditorOwnerInfo, this, false, false, false, false, embeddableElement );

			Rectangle contentRect = this.RectInsideBorders;
			if ( MergedCellContentArea.VisibleRect == column.MergedCellContentAreaResolved )
			{
				if ( ! this.clipSelfRect.IsEmpty )
				{
					// SSP 6/27/05 BR04617
					// We are constrainting the height of the contents in the merged cell to at least 
					// height of a single cell. Before the clipSelfRect was at least such a height, which
					// is not the case anymore.
					// 
					// ------------------------------------------------------------------------------------
					//int visibleAreaTop = Math.Max( contentRect.Y, this.clipSelfRect.Y );
					//int visibleAreaBottom = Math.Min( contentRect.Bottom, this.clipSelfRect.Bottom );

					Rectangle visibleRect = this.clipSelfRect;

					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//ArrayList mergedCells = this.CellElements;
					List<CellUIElement> mergedCells = this.CellElements;

					if ( null != mergedCells && mergedCells.Count > 0 )
					{
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//CellUIElementBase firstCellElem = mergedCells[0] as CellUIElementBase;
						CellUIElementBase firstCellElem = mergedCells[ 0 ];

						if ( null != firstCellElem )
							visibleRect.Height = Math.Max( visibleRect.Height, firstCellElem.Rect.Height );
					}

					int visibleAreaTop = Math.Max( contentRect.Y, visibleRect.Y );
					int visibleAreaBottom = Math.Min( contentRect.Bottom, visibleRect.Bottom );
					// ------------------------------------------------------------------------------------

					contentRect.Y = visibleAreaTop;
					contentRect.Height = visibleAreaBottom - visibleAreaTop;
				}

				DataAreaUIElement dataAreaElem = (DataAreaUIElement)this.GetAncestor( typeof( DataAreaUIElement ) );
				if ( null != dataAreaElem )
					dataAreaElem.dontUseUseScrollWindow = true;
			}

			embeddableElement.Rect = contentRect;

			this.ChildElements.Clear( );
			this.ChildElements.Add( embeddableElement );

			// SSP 3/31/06 BR11080
			// Added support for displaying tooltips in merged cells.
			// 
			this.ToolTipItem = MergedCellElementToolTipItem.VALUE;
		}

		#endregion // PositionChildElements

		#region DrawFocus

		// SSP 3/25/05 BR03031
		// Before we were drawing focus rect of the active cell/row passing through the merged cell
		// in OnAfterDraw however that skips the draw filter logic. So instead use the DrawsFocusRect
		// and DrawFocus methods for that. Overrode those two methods.
		//
		/// <summary>
		/// Default drawfocus method draws a focus rect inside the element's borders.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawFocus( ref UIElementDrawParams drawParams )
		{
			Rectangle focusRect = this.GetFocusRectHelper( );
			if ( ! focusRect.IsEmpty )
				drawParams.DrawFocusRectangle( focusRect );
		}

		#endregion DrawFocus 

		#region Offset

		// SSP 5/18/05 - Optimizations
		// Overrode Offset so we can offset the clipSelfRect as well.
		//
		/// <summary>
		/// Overridden. Offsets this element's rect and (optionally) all of its descendant elements.
		/// </summary>
		/// <param name="deltaX">The number of pixels to offset left/right</param>
		/// <param name="deltaY">The number of pixels to offset up/down </param>
		/// <param name="recursive">If true will offset all descendant elements as well</param>
		public override void Offset( int deltaX, int deltaY, bool recursive )
		{
			if ( ! this.clipSelfRect.IsEmpty )
				this.clipSelfRect.Offset( deltaX, deltaY );

			base.Offset( deltaX, deltaY, recursive );
		}

		#endregion // Offset

		// SSP 9/15/05 BR06301 - Hot tracking row/cell appearance
		// 
		#region NAS 5.3 Hot-tracking Row/Cell Feature

		#region OnMouseMove

		// SSP 9/15/05 BR06301 - Hot tracking row/cell appearance
		// 
		/// <summary>
        /// Called when the mouse is moved over the element or during capture
		/// </summary>
        /// <param name="e">MouseEventArgs</param>
		protected override void OnMouseMove( MouseEventArgs e )
		{
			base.OnMouseMove( e );

			Point mousePos = new Point( e.X, e.Y );
			this.VerifyHotTrackedCell( mousePos, 0 );
		}

		#endregion // OnMouseMove

		#region OnMouseEnter

		// SSP 9/15/05 BR06301 - Hot tracking row/cell appearance
		// 
		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseEnter( )
		{
			base.OnMouseEnter( );

			this.VerifyHotTrackedCell( 1 );
		}

		#endregion // OnMouseEnter

		#region OnMouseLeave

		// SSP 9/15/05 BR06301 - Hot tracking row/cell appearance
		// 
		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseLeave( )
		{
			base.OnMouseLeave( );

			this.VerifyHotTrackedCell( 2 );
		}

		#endregion // OnMouseLeave

		#region VerifyHotTrackedCell
		
		// SSP 9/15/05 BR06301 - Hot tracking row/cell appearance
		// 
		private void VerifyHotTrackedCell( int moveEnterLeave )
		{
			Point mousePos = this.Control.PointToClient( Control.MousePosition );
			this.VerifyHotTrackedCell( mousePos, moveEnterLeave );
		}
		
		private void VerifyHotTrackedCell( Point mousePos, int moveEnterLeave )
		{
			if ( null != this.cellElements )
			{
				RowUIElementBase hotTrackedRowElem = null;

				for ( int i = 0, count = this.cellElements.Count; i < count; i++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//CellUIElementBase cellElem = this.cellElements[ i ] as CellUIElementBase;
					CellUIElementBase cellElem = this.cellElements[ i ];

					if ( null == cellElem )
						continue;

					if ( 2 == moveEnterLeave || null != hotTrackedRowElem && ! cellElem.IsDescendantOf( hotTrackedRowElem ) )
					{
						cellElem.OnMouseEnterLeave( false );
						if ( null != cellElem.RowCellAreaElement )
							cellElem.RowCellAreaElement.OnMouseEnterLeave( false );
					}
					else
					{
						RowUIElementBase rowElem = (RowUIElementBase)cellElem.GetAncestor( typeof( RowUIElementBase ) );
						if ( null != rowElem )
						{

							bool mouseInsideCellElem = cellElem.ClipRect.Contains( mousePos );
							if ( mouseInsideCellElem != cellElem.isMouseOverCell )
								cellElem.OnMouseEnterLeave( mouseInsideCellElem );
						
							bool mouseInsideRowElem = rowElem.ClipRect.Contains( mousePos );
							if ( mouseInsideRowElem != rowElem.isMouseOverSelectorOrCellArea )
								RowCellAreaUIElementBase.OnMouseEnterLeaveHelper( rowElem, mouseInsideRowElem );

							if ( rowElem.isMouseOverSelectorOrCellArea )
								hotTrackedRowElem = rowElem;
						}
					}
				}
			}			
		}

		#endregion // VerifyHotTrackedCell

		#endregion // NAS 5.3 Hot-tracking Row/Cell Feature

		#endregion // Overridden Methods

		#region MergedCellElementToolTipItem Class

		// SSP 3/31/06 BR11080
		// Added support for displaying tooltips in merged cells.
		// Added MergedCellElementToolTipItem class.
		// 
		private class MergedCellElementToolTipItem : IToolTipItem
		{
			private static MergedCellElementToolTipItem val = null;

			internal static MergedCellElementToolTipItem VALUE 
			{
				get
				{
					if ( null == val )
						val = new MergedCellElementToolTipItem( );

					return val;
				}
			}

			ToolTipInfo Infragistics.Win.IToolTipItem.GetToolTipInfo( Point mousePosition, UIElement element, UIElement previousToolTipElement, ToolTipInfo toolTipInfoDefault )
			{
				MergedCellUIElement mergedCellElem = element as MergedCellUIElement;
				MergedCell mergedCell = null != mergedCellElem ? mergedCellElem.MergedCell : null;

				if ( null != mergedCell && ! mergedCellElem.Disposed && null != mergedCellElem.Control
					// SSP 7/21/06 BR14294
					// Tip style needs to be resolved differently based whether the ToolTipText has been explicitly set
					// or the cell's value (clipped) will be displayed.
					// This check is done below now.
					// 
					//&& TipStyle.Show == mergedCell.Band.TipStyleCellResolved 
					&& ( null == mergedCellElem.activeCellElem || ! mergedCellElem.activeCellElem.IsInEditMode ) )
				{
					// SSP 5/30/06 BR13090
					// If the first of the merged cells has ToolTipText set, then display that 
					// instead of the cell tooltip.
					// 
					// ------------------------------------------------------------------------------
					bool toolTipSet = false;

					if ( null != mergedCellElem.cellElements && mergedCellElem.cellElements.Count > 0 )
					{
						// MD 8/10/07 - 7.3 Performance
						// Use generics	
						//CellUIElementBase firstCellElem = mergedCellElem.cellElements[0] as CellUIElementBase;
						CellUIElementBase firstCellElem = mergedCellElem.cellElements[ 0 ];

						UltraGridCell firstCell = null != firstCellElem && firstCellElem.HasCell ? firstCellElem.Cell : null;
						string toolTipText = null != firstCell ? firstCell.ToolTipTextResolved : null;
						if ( null != toolTipText && toolTipText.Length > 0
							// SSP 7/21/06 BR14294
							// Tip style needs to be resolved differently based whether the ToolTipText has been explicitly set
							// or the cell's value (clipped) will be displayed.
							// 
							&& TipStyle.Show == firstCell.Band.TipStyleCellResolved( firstCell ) )
						{
							toolTipInfoDefault.ToolTipText = toolTipText;
							toolTipSet = true;
						}
					}
					// ------------------------------------------------------------------------------

					// SSP 5/30/06 BR13090
					// Enclosed the existing code into the if block. Don't set the tooltip if we set it above.
					// 
					// 
					if ( ! toolTipSet )
					{
						EmbeddableUIElementBase editorElem = CellUIElementBase.FindEmbeddableUIElement( mergedCellElem.childElementsCollection, false );
						if ( null != editorElem && ! editorElem.IsDataFullyVisible
							// SSP 7/21/06 BR14294
							// Tip style needs to be resolved differently based whether the ToolTipText has been explicitly set
							// or the cell's value (clipped) will be displayed.
							// 
							&& null != mergedCell.Band && TipStyle.Show == mergedCell.Band.TipStyleCellResolved( null ) )
						{
							toolTipInfoDefault.Editor = editorElem.Editor;
							toolTipInfoDefault.EditorOwner = editorElem.Owner;
							toolTipInfoDefault.EditorOwnerContext = new ToolTipOwnerContext( editorElem.OwnerContext );

							if ( toolTipInfoDefault.HasToolTip )
							{
								Rectangle rect = mergedCellElem.Control.RectangleToScreen( mergedCellElem.Rect );
								toolTipInfoDefault.Location = rect.Location;
								toolTipInfoDefault.Size = rect.Size;
							}
						}
					}
				}

				// By default the tool tip info mechanism has no auto pop delay. Meaning the
				// tool tip stays visible until the mouse leaves the element. To maintain
				// the previous UltraGrid behavior of auto-popping, set the AutoPopDelay to
				// -1 which means use the default auto pop delay.
				// 
				toolTipInfoDefault.AutoPopDelay = -1;

				return toolTipInfoDefault;
			}
		}

		#endregion // MergedCellElementToolTipItem Class
	}

	#region ForceActive Enum

	// SSP 8/18/05 BR05562
	// 
	[ Flags() ]
	internal enum ForceActive
	{
		None = 0,
		Row  = 1,
		Cell = 2
	}

	#endregion // ForceActive Enum

	#region CellState Enum

	// SSP 9/14/05 BR06301 BR05562
	// 
	[ Flags() ]
	internal enum CellState
	{
		None			= 0x0,
		Selected		= 0x1,
		ActiveRow		= 0x2,
		ActiveCell		= 0x4,
		HotTrackedRow	= 0x8,
		HotTrackedCell	= 0x10
	}

	#endregion // CellState Enum

}
