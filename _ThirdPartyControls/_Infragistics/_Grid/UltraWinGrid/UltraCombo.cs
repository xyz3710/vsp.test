#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

#region Old Code

#endregion //Old Code

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Collections;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Drawing.Printing;
using System.Security;
using System.Security.Permissions;

// JAS 12/21/04 ButtonsLeft & ButtonsRight
using Infragistics.Win.UltraWinEditors;

//  BF NA 9.1 - UltraCombo MultiSelect
using System.Collections.Generic;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// The UltraCombo control is like a ComboBox control, but the list is a multi-column, grid-like dropdown list.
	/// </summary>
	/// <remarks>
	/// <p class="body">The UltraCombo control can be used as the <see cref="UltraGridColumn.ValueList"/> of an <see cref="UltraGrid"/> column, but it is intended for use as a standalone control on a form. To provide a multi-column dropdown list in a grid cell, use the <see cref="Infragistics.Win.UltraWinGrid.UltraDropDown"/> control.</p>
	/// <p class="body">The UltraCombo is populated by binding it to a data source, just like the <see cref="UltraGrid"/>. So use the <see cref="UltraGridBase.SetDataBinding(object, string, bool, bool)"/> method, or the <see cref="UltraGridBase.DataSource"/> and <see cref="UltraGridBase.DataMember"/> properties.</p>
	/// <p class="body">Be sure to set the <see cref="UltraDropDownBase.ValueMember"/> property to determine which column in the dropdown is associated with the <see cref="Value"/> of the UltraCombo.</p>
	/// <p class="body">Optionally, you may also want to set the <see cref="UltraDropDownBase.DisplayMember"/> property to display more user-friendly text, instead of displaying the value.</p>
	/// <p class="note">Note that the UltraCombo control requires a container (such as a Form or UserControl) in order to function properly. Placing the control on a form (or UserControl) at design-time is sufficient for this, but if the control is created at run-time in code, be sure to add the UltraCombo to the form's (or UserControl's) Controls collection.</p>
	/// </remarks>
	
	// AS 4/8/05
	//
		
	[ToolboxBitmap(typeof(UltraGridBase), AssemblyVersion.ToolBoxBitmapFolder + "UltraCombo.bmp")] // MRS 11/13/05
	[ DefaultProperty( "DisplayLayout" ) ]
	// AS 11/11/03 [UltraCombo as Editor]
	// Somewhere along the way, we disassociated this control from
	// its designer - the UltraComboDesigner.
	//
	//[ Designer( "Infragistics.Win.UltraWinGrid.Design.UltraGridDesigner, " + AssemblyRef.Design ) ]
	[ Designer( "Infragistics.Win.UltraWinGrid.Design.UltraComboDesigner, " + AssemblyRef.Design ) ]
	[ DefaultEvent( "InitializeLayout" ) ]
	[ ValueList.ValueListBindable(false)]
	[ LocalizedDescription("LD_UltraCombo") ] // JAS BR06191 10/10/05
	[DefaultBindingProperty("Value")]	// AS 3/28/05 Drag-Once Binding
	[LookupBindingProperties("DataSource", "DisplayMember", "ValueMember", "Value")]
	public class UltraCombo : UltraDropDownBase,
		Infragistics.Shared.IUltraLicensedComponent,
		// AS 11/11/03 [UltraCombo as Editor]
		// The editor becomes the valuelist owner.
		//IValueListOwner,
		IProvidesEmbeddableEditor,	// AS 11/7/03
        ICheckedItemList    //  BF 11/25/08 NA 9.1 - UltraCombo MultiSelect
	{
		#region Private Fields	

		internal static int					DEFAULT_PADDING			= 1;

		private UltraLicense				license = null;

		private ComboKeyActionMappings		keyActionMappings = null;
		private ComboEventManager			eventManager = null;
		private UltraComboUIElement			element = null;

		// AS 11/11/03 [UltraCombo as Editor]
		// We no longer own the textbox. Its provided by the editor.
		//
		//private ComboTextBox				textbox = null;

		private ComboDropDownControl		dropDownControl = null;
		private AppearanceHolder			appearanceHolder = null;

		private bool						autoSize = true;
		private UltraComboStyle				dropDownStyle = UltraComboStyle.DropDown;		
		private UIElementBorderStyle 		borderStyle = UIElementBorderStyle.Default;

		private Size						padding	= new Size( DEFAULT_PADDING, DEFAULT_PADDING );
		private int							lastIndex = -1;
		private object						selectedValue = null;

		// SSP 8/1/03 UWG2549
		// Changed the default value for autoEdit from false to true.
		//
		//private bool						autoEdit = false;

        //  BF 2/28/08  FR09238 - AutoCompleteMode
        //  Replaced AutoEdit with AutoCompleteMode; member no longer necessary
		//private bool						autoEdit = true;

		// MRS 11/17/04 - UWG2996
		// No longer needed
//		// SSP 5/14/02 UWG1102
//		// A flag to prevent Value property from being called repeatedly causing
//		// an infinite loop.
//		//
//		private bool						inValueSet = false;

		// MRS 11/17/04 - UWG2996
		// No longer needed
//		// SSP 6/18/02 UWG1253
//		// Same as above but for Text property.
//		//
//		private bool						inSetText = false;

		// MRS 11/17/04 - UWG2996
		// No longer needed
//		// SSP 8/20/02 UWG1280
//		// Added a flag to prevent the set of the Value property to not to
//		// return when the old value and new value being assigned is the same.
//		// This is to fix the problem that happens when a new row is selected
//		// that happens to have the same value in the ValueMember field will
//		// cause the set of the Value to return without updating the display
//		// text.
//		//
//		private bool						dontCheckValueBeingOldValue = false;

		// SSP 8/21/02 UWG1853
		//
		private ComboKeyActionMappings		allActionsMappings = null;

		// AS 11/17/03 [UltraCombo as Editor]
		// Ok. Its still needed...
		//
		// AS 11/11/03 [UltraCombo as Editor]
		// No longer needed.
		//
		// SSP 10/28/02 UWG1468
		//
		private bool						hasOnEnterBeenCalled = false;

		// SSP 11/26/02 UWG1821
		// Added a flag that indicates whether we are in base.OnTextChanged method call which
		// fires TextChanged. This is to solve a problem where the user sets the text of the
		// combo to something else in TextChanged event and we igore it because of other 
		// flags to prevent recursion and auto edit feature related flags.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private bool						inTextChangedEvent = false;

		// SSP 12/6/02 UWG1666
		// Added a flag to prevent combo from giving focus to the text box.
		//
		internal bool						dontGiveFocusToTextBox = false;

		// SSP 6/02/03 UWG2036
		// Added DisplayStyle property.
		//
		// SSP 4/10/06 - App Styling
		// Added Default member to the enum.
		// 
		//private EmbeddableElementDisplayStyle displayStyle = EmbeddableElementDisplayStyle.Standard;
		private EmbeddableElementDisplayStyle displayStyle = EmbeddableElementDisplayStyle.Default;

		// MRS 11/17/04 - UWG2996
		// No longer needed
//		// SSP 7/15/03 UWG2315
//		// Added dontFireValueChanged to prevent set of the Value property from firing
//		// ValueChanged event.
//		//
//		private bool						dontFireValueChanged = false;

		// SSP 8/1/03 UWG1662
		// We need to expose a public ReadOnly property that the user can set to make 
		// the UltraCombo read-only.
		//
		private bool						isReadOnly = false;

		// SSP 10/13/03 UWG1953
		// Added ButtonAppearance property to UltraCombo.
		//
		private AppearanceHolder			buttonAppearanceHolder = null;

		// AS 11/11/03 [UltraCombo as Editor]
		private bool						inOnLeaveMethod = false;
		private bool						inOnEnterMethod = false;

		private bool						preventOnTextChanged = false;
		private bool						initializingText = false;

		// AS 1/9/04 UWG2866
		private IValueListOwner				valueListOwner;

		// SSP 4/2/04 UWG3046
		// Added CharacterCasing property.
		//
		private System.Windows.Forms.CharacterCasing characterCasing = System.Windows.Forms.CharacterCasing.Normal;

		// AS 5/21/04 UWG3238(aka UWE925)
		// Keep track of whether the last validating event
		// invocation was cancelled.
		//
		private bool						wasLastValidationCancelled = false;
		// AS 5/21/04 UWG3238(aka UWE925) - See UWE917
		private int							cachedSelStart  = 0;
		private int							cachedSelLength = 0;

		// MRS 10/22/04 - UWG3633
		private const int defaultMaxLength = 32767;
		private int maxLength = defaultMaxLength;

		//JDN 11/9/04 LimitToList
		private bool    limitToList     = false;
		private object  lastValidValue  = null;

		// SSP 1/21/05 BR01891 
		// Added SyncWithCurrencyManager property on UltraCombo. This will cause the UltraCombo
		// to synchronize the selected row with the currency manager's Position.
		//
		private bool syncWithCurrencyManager = false;

		//	BF 2/15/06	BR07876
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//private static readonly bool defaultAlwaysInEditMode = false;
		private const bool defaultAlwaysInEditMode = false;

		private bool alwaysInEditMode = UltraCombo.defaultAlwaysInEditMode;
		private bool hasPainted = false;

		// MRS 4/18/06 - BR11468
		private DefaultableBoolean allowNull = DefaultableBoolean.Default;
		private string nullTextValue = null;

        //  BF 2/28/08  FR09238 - AutoCompleteMode
        private FilterCondition     autoCompleteFilterCondition = null;
        private AutoCompleteMode    autoCompleteMode = AutoCompleteMode.Default;

        //  BF 11/25/08 NA 9.1 - UltraCombo MultiSelect
        private CheckedListSettings     checkedListSettings = null;
        private CheckedRowsCollection   checkedRows = null;

        // MBS 7/10/09 - TFS19046
        private Color backColorInternal = Color.Empty;

		#endregion //Private Fields

		#region Static Event Objects

		private static readonly object EVENT_INTIALIZELAYOUT = new object();
		private static readonly object EVENT_INTIALIZEROW = new object();
		private static readonly object EVENT_AFTERDROPDOWN = new object();
		private static readonly object EVENT_AFTERCLOSEUP = new object();
		private static readonly object EVENT_BEFOREDROPDOWN = new object();
		private static readonly object EVENT_ROWSELECTED = new object();
		private static readonly object EVENT_VALUECHANGED = new object();

		//JDN 11/9/04 LimitToList
		private static readonly object EVENT_ITEMNOTINLIST = new object();

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		private static object EVENT_EDITORBUTTONCLICK					= new object();
		private static object EVENT_AFTEREDITORBUTTONCLOSEUP			= new object();
		private static object EVENT_BEFOREEDITORBUTTONDROPDOWN			= new object();
		private static object EVENT_EDITORSPINBUTTONCLICK				= new object();
		private static object EVENT_INITIALIZEEDITORBUTTONCHECKSTATE	= new object();
		private static object EVENT_BEFORECHECKSTATECHANGED				= new object();
		private static object EVENT_AFTERCHECKSTATECHANGED				= new object();

		#endregion //Static Event Objects

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="UltraCombo"/>
		/// </summary>
		public UltraCombo() : this(null)
		{
		}

		// AS 1/9/04 UWG2866
		// I added a special constructor so the combo doesn't make its editor
		// the owner of the valuelist if the owner should be a standalone
		// ultragridcomboeditor.
		//
		internal UltraCombo(IValueListOwner valueListOwner)
		{
			// verify and cache the license
			//
			// AS 3/5/03 DNF37
			// Wrapped in a try/catch for a FileNotFoundException.
			// When the assembly is loaded dynamically, VS seems 
			// to be trying to reload a copy of Shared even though 
			// one is in memory. This generates a FileNotFoundException
			// when the dll is not in the gac and not in the AppBase
			// for the AppDomain.
			//
			try
			{
				this.license	= LicenseManager.Validate( typeof(UltraCombo), this ) as UltraLicense;
			}
			catch (System.IO.FileNotFoundException)	{}

            // MBS 7/10/09 - TFS19046
            // Support a transparent backcolor for the combo.           
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);        

			// AS 11/11/03 [UltraCombo as Editor]
			// Activate it with the editor.
			//
			// activate the combo through the the IValueList interface
			//
			//((IValueList)this).Activate( this );
			// AS 1/9/04 UWG2866
			if (valueListOwner == null)
				this.valueListOwner = this.InternalEditor;
			else
				this.valueListOwner = valueListOwner;

			((IValueList)this).Activate( this.valueListOwner );
		}
		#endregion //Constructor

		#region Base class overrides

		#region Control/Component overrides

		 // AS 11/11/03 [UltraCombo as Editor]

		#region Dispose
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose( bool disposing )
		{

			if ( disposing )
			{
				

				// AS 11/11/03 [UltraCombo as Editor]
				// We are no longer the owner of the valuelist.
				//
				// deactivate the drop down
				//
				//((IValueList)this).DeActivate( this );
				if (this.internalEditor != null)
					((IValueList)this).DeActivate( this.internalEditor );
				

				if ( this.license != null ) 
				{
					this.license.Dispose();
					this.license = null;
				}

				if ( this.dropDownControl != null )
				{
					this.dropDownControl.Dispose();
					this.dropDownControl = null;
				}

				// AS 11/11/03 [UltraCombo as Editor]
				// We don't own the text box.
				//
				//this.FreeTextBox();

				// It is importantant to unhook the holder and call reset on it
				// when we are disposed
				//
				if ( this.appearanceHolder != null )
				{
					this.appearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					this.appearanceHolder.Reset();
					this.appearanceHolder = null;
				}

				// SSP 10/13/03 UWG1953
				// Added ButtonAppearance property to UltraCombo.
				//
				if ( null != this.buttonAppearanceHolder )
				{
					this.buttonAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					this.buttonAppearanceHolder.Reset( );
					this.buttonAppearanceHolder = null;
				}

				// AS 11/11/03 [UltraCombo as Editor]
				if (this.externalEditor != null)
				{
					// JAS 12/21/04 ButtonsLeft & ButtonsRight
					this.DetachHandlersFromEditorButtonEvents( this.externalEditor, false );

					this.externalEditor.Dispose();
					this.externalEditor = null;
				}

				if (this.internalEditor != null)
				{
					// unhook from the editor
					this.internalEditor.KeyDown -= new KeyEventHandler(this.OnEditorKeyDown);
					this.internalEditor.KeyPress -= new KeyPressEventHandler(this.OnEditorKeyPress);
					this.internalEditor.KeyUp -= new KeyEventHandler(this.OnEditorKeyUp);

					this.internalEditor.ValueChanged -= new EventHandler(this.OnEditorValueChanged);

					// AS 1/16/04
					this.internalEditor.AfterEnterEditMode -= new EventHandler(this.OnAfterEditorEnterEditMode);
					this.internalEditor.AfterExitEditMode -= new EventHandler(this.OnAfterEditorExitEditMode);

					// AS 5/21/04 UWG3238(aka UWE925) - See UWE917
					this.internalEditor.BeforeExitEditMode -= new Infragistics.Win.BeforeExitEditModeEventHandler(this.OnBeforeEditorExitEditMode);

					// JAS 12/21/04 ButtonsLeft & ButtonsRight
					this.DetachHandlersFromEditorButtonEvents( this.internalEditor, true );

					this.internalEditor.Dispose();
					this.internalEditor = null;
				}

				if (this.editorOwner != null)
					this.editorOwner = null;

                //  BF 2/28/08  FR09238 - AutoCompleteMode
                this.ResetAutoCompleteFilterCondition();
			}

			base.Dispose( disposing );

			// SSP 10/28/03 UWG2573
			// Set the activeColScrollRegion and activeRowScrollRegion to null so that if
			// for some reason some external object is holding a reference to UltraGrid
			// and preventing it from being collected by the garbase collector, then at 
			// least row bands/scrollregions/rows/cells and other grid objects get collected.
			//
			// ------------------------------------------------------------------------------
			if ( disposing )
			{
				this.eventManager = null;
			}
			// ------------------------------------------------------------------------------
		}
		#endregion //Dispose

		#region IsInputKey
		/// <summary>
        /// Determines whether the specified key is a regular input key or a special key that requires preprocessing.
		/// </summary>
        /// <param name="keyData">One of the System.Windows.Forms.Keys values.</param>
        /// <returns>true if the specified key is a regular input key; otherwise, false.</returns>
		protected override bool IsInputKey( Keys keyData )
		{
			// SSP 7/31/02
			// 
			
			
			if ( this.DropDownStyle == UltraComboStyle.DropDown &&
				( keyData == Keys.Tab || keyData == (Keys.Tab | Keys.Shift) ) )
				return true;

			// MRS 9/28/06 - BR15851
			//if (this.InternalEditor.IsInputKey(keyData))
			if (this.InternalEditor != null &&
				this.InternalEditor.IsInputKey(keyData))
				return true;

			return this.KeyActionMappings.IsKeyMapped( keyData, (long)this.CurrentState );
		}
		#endregion //IsInputKey

		#region OnCreateControl
		/// <summary>
		/// Called when the control is first created
		/// </summary>
		protected override void OnCreateControl()
		{
			base.OnCreateControl();

			// SSP 10/13/03 UWG1878
			// Don't create the text box from OnCreateControl. Somehow creating a control
			// in OnCreateControl confuses the framework and causes UWG1878. So instead
			// syncing the text here, do so in the Textbox property's get where the text
			// box is created.
			//
			

			// AS 11/11/03 [UltraCombo as Editor]
			// Follow the UltraComboEditor's lead...
			//	BF 11.22.02
			//	Activate the ValueList now, in case any of its properties
			//	are accessed (e.g., SelectedIndex) before the dropdown list
			//	has been displayed for the first time.
			// AS 1/9/04 UWG2866
			// The valuelist owner may be a different editor.
			//
			//((IValueList)this).Activate( this.InternalEditor );
			// SSP 10/9/06 BR16577
			// Don't activate if already activated. If the UltraCombo which is a source of
			// value list to an external editor and that external editor is in the process
			// of performing an operation on the value list (the UltraCombo) like dropping
			// it down, the changing the owner during that process will cause problems.
			// 
			//((IValueList)this).Activate( this.valueListOwner );
            if ( null == this.Owner )
				((IValueList)this).Activate( this.valueListOwner );

			// AS 11/11/03 TODO
			// The EditorWithTextControlBase hooks the 
			// activated/deactivated of the form but that
			// doesn't cover if the parent changes (or something
			// in the parent chain changes) or if we're on an
			// mdi child, nested form, etc.
			//

			// MRS 8/31/06 - BR15457
			this.SetIdealHeight();

            // MBS 7/10/09 - TFS19046
            this.SyncBackColor();
		}
		#endregion //OnCreateControl
		
		// SSP 7/31/02 UWG1468
		// Moved this from OnGotFocus because setting the focus to the text box in
		// OnGotFocus causes validation event when cancelled to not work properly.
		// So commented out the OnGotFocus overridden method below.
		//
		

		#region OnEnabledChanged
		/// <summary>
		/// Overridden method.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnEnabledChanged( EventArgs e )
		{
			// SSP 11/16/01 UWG703
			// When Enabled changes, resync the properties of the text box by
			// calling SyncTextBoxProperties to ensure that text box is 
			// either hidden or made visible because now we are hiding the
			// text box when the combo is disabled.
			//
			base.OnEnabledChanged( e );

			// AS 11/11/03 [UltraCombo as Editor]
			// See if we need to do anything here?
			//
			// We no longer own the textbox.
			//
			//this.SynchTextBoxProperties( );
		}
		#endregion //OnEnabledChanged
 
		#region OnEnter
		 // AS 11/11/03 [UltraCombo as Editor]

		/// <summary>
		/// Called when the focus enters this control
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnEnter( EventArgs e )
		{
			//JDN 11/9/04 LimitToList
			if ( this.IsItemInList() )
				this.lastValidValue = this.Value;

			// AS 11/17/03 [UltraCombo as Editor]
			// Reintroduced since its needed for UWG1468
			//
			// SSP 10/28/02 UWG1790
			// Added this flag.
			//
			this.hasOnEnterBeenCalled = true;

			// follow the text editor's lead and do not continue
			// if we are in the enter event
			if (this.inOnEnterMethod)
				return;

			try
			{
				this.inOnEnterMethod = true;

				// AS 11/17/03 [UltraCombo as Editor]
				// To ensure that UWG1885 is not broken, we need
				// to enter edit mode before firing the base
				// Enter method.
				//
				// first call the base implementation
				//base.OnEnter(e);

				// follow the text editor and do not force focus
				// if the flag is set
				if (this.dontGiveFocusToTextBox == false)
				{
					// the text editor does this to ensure that the 
					// embeddable elements are created and positioned
					this.SafeDirtyChildElements();
					this.ControlUIElement.VerifyChildElements(true);

					// again, the text editor does this so here we go...
					if (this.ControlUIElement.ChildElements.Count == 0)
						this.Update();

					//	Find the embeddable UIElement, and enter edit mode on it
					EmbeddableUIElementBase  element = this.ControlUIElement.GetDescendant( typeof(EmbeddableUIElementBase) ) as EmbeddableUIElementBase;

					// lastly go into edit mode
					// AS 5/21/04 UWG3238(aka UWE925)
					//if ( element != null && !this.InternalEditor.IsInEditMode )
					//	this.InternalEditor.EnterEditMode( element );
					if ( element != null && !this.InternalEditor.IsInEditMode )
					{
						this.InternalEditor.EnterEditMode( element );

						// AS 5/14/04 UWG3238(aka UWE925) - See UWE917
						if (this.wasLastValidationCancelled && 
							this.InternalEditor.IsInEditMode	&&
							this.InternalEditor.SupportsSelectableText)
						{
							this.InternalEditor.SelectionStart = cachedSelStart;
							this.InternalEditor.SelectionLength = cachedSelLength;
						}
					}
					else
					{
						// AS 5/21/04 UWG3238(aka UWE925)
						// lastly, if validation was cancelled 
						// and we don't have focus, make a delay
						// call to change focus/put us back into
						// edit mode. We need this for vs 2002 usercontrols
						//
						if (this.wasLastValidationCancelled	&&
							this.IsHandleCreated			&&
							this.InternalEditor.CanFocus		&& 
							!this.InternalEditor.Focused)
							this.BeginInvoke( new MethodInvoker(this.DelayedInvokeEditorFocus) );
					}

					// AS 5/21/04 UWG3238(aka UWE925)
					// Reset the flag regardless.
					//
					this.wasLastValidationCancelled = false;
				}
			}
			finally
			{
				// AS 11/17/03 [UltraCombo as Editor]
				// To ensure that UWG1885 is not broken, we need
				// to enter edit mode before firing the base
				// Enter method.
				//
				base.OnEnter(e);

				this.inOnEnterMethod = false;
			}
		}

		#endregion //OnEnter
 
		#region OnFontChanged
		/// <summary>
		/// Overridden method. Sets the height of the masked edit
		/// according to the newly assigned font
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnFontChanged( EventArgs e )
		{
			base.OnFontChanged( e );

			// cause it to recalculate masked edit font height and
			// set the height of the masked edit appropriately
			this.SetIdealHeight( );

			this.UIElement.DirtyChildElements( false );
		}
		#endregion //OnFontChanged

		#region OnGotFocus
		 // AS 11/11/03 [UltraCombo as Editor]

		/// <summary>
		/// Used to invoke the GotFocus event.
		/// </summary>
		/// <param name="e">Event args</param>
		protected override void OnGotFocus( EventArgs e )
		{
			// follow what the combo/text editor does and call the
			// base impl first
			base.OnGotFocus(e);

			// if we set the flag to prevent us from putting focus,
			// then exit not
			if ( this.dontGiveFocusToTextBox )
				return;

			// AS 11/17/03 [UltraCombo as Editor]
			// This is to prevent us from breaking UWG1468 and UWG1666
			//
			if (!this.ShouldEnterEditModeOnFocus)
				return;

			// follow what the text editor does and don't continue if
			// we are in the leave event. this is somewhat confusing
			// because the ultracomboeditor that derives from that
			// texteditorcontrolbase then overrides ongotfocus
			// and does NOT check the flag
			if (this.inOnLeaveMethod)
				return;

			// if we are not in edit mode...
			if (!this.InternalEditor.IsInEditMode)
			{
				// get our editor element
				EmbeddableUIElementBase editorUI = this.ControlUIElement.GetDescendant( typeof(EmbeddableUIElementBase) ) as EmbeddableUIElementBase;

				// assuming we got one, try to go into edit mode
				if ( editorUI != null )
					this.InternalEditor.EnterEditMode( editorUI );
			}
			else
			{
				// MRS 1/13/05 - BR00169
				// Added this else clause. This is essentially what UltraComboEditor does. 
				if (this.InternalEditor.CanFocus && !this.InternalEditor.Focused)
					this.InternalEditor.Focus();
			}

			// i followed the combo editor here. the text editor does
			// different things depending upon whether it was in edit
			// mode. but the combo simply checks to see that if it can
			// get focus, that it has focus or else it pulls it out
			// of edit mode.
			if (this.InternalEditor.CanFocus && !this.InternalEditor.Focused)
				this.InternalEditor.ExitEditMode(true,true);

			// AS 11/17/03 [UltraCombo as Editor]
			// This still seems to be needed for UWG1403 and
			// probably for UWG2036 as well.
			//
			if ( null != this.element)
				this.element.DirtyChildElements( );

            // MRS 7/24/2008 - BR34638
            if (this.IsFocused && this.useBaseFocused == false)
                this.BeginInvoke(new MethodInvoker(this.DelayedInvokeEditorFocus));
		}

		#endregion //OnGotFocus

		#region OnKeyPress
		/// <summary>
		/// Overridden. Raises KeyPress events.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Control.OnKeyPress"/>
		/// </remarks>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnKeyPress( KeyPressEventArgs e )
		{			
			base.OnKeyPress( e );

			 // AS 11/11/03 [UltraCombo as Editor]

			// this is from the editors...
			if ( e.Handled )
				return;

			if ( this.DropDownStyle == UltraComboStyle.DropDownList )
				this.EditorOwner.RaiseKeyPress( e );
		}
		#endregion //OnKeyPress

		#region OnKeyDown
		/// <summary>
		/// Called when a key is pressed
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnKeyDown( KeyEventArgs e )
		{
			base.OnKeyDown( e );

			 // AS 11/11/03 [UltraCombo as Editor]

			// this is from the editors...
			if (e.Handled)
				return;

			if ( this.DropDownStyle == UltraComboStyle.DropDownList &&
				this.IsInputKey( e.KeyData ) )
				this.EditorOwner.RaiseKeyDown( e );
		}
		#endregion //OnKeyDown

		#region OnLocationChanged
		/// <summary>
		/// Invoked when the position of the control changes
		/// </summary>
		/// <param name="e">Event arguments</param>
		protected override void OnLocationChanged( EventArgs e )
		{
			// AS 11/17/03
			// I noticed while looking at UWG2751 that when the
			// combo is in a scrollable panel, the panel will
			// scroll when a contained control gets the mouse
			// wheel event. Anyway, the dropdown portion 
			// remains in place when this occurs. We should close
			// up when the control moves while we are dropped down.
			//
			this.CloseUpList();

			base.OnLocationChanged(e);
		}
		#endregion //OnLocationChanged

		#region OnLeave
		 // AS 11/11/03 [UltraCombo as Editor]

		/// <summary>
		/// Used to invoke the Leave event.
		/// </summary>
		/// <param name="e">Event arguments</param>
		protected override void OnLeave( EventArgs e )
		{
			// AS 11/17/03 [UltraCombo as Editor]
			// Reintroduced since its needed for UWG1468
			//
			// SSP 10/28/02 UWG1790
			// Added this flag.
			//
			this.hasOnEnterBeenCalled = false;

			// follow what the texteditor does and exit if
			// we are already in this method
			if (this.inOnLeaveMethod)
				return;

			this.inOnLeaveMethod = true;

			try
			{
				// exit edit when we leave the combo
				// SSP 4/23/04 UWG3189
				// Check for InternalEditor being null. It would be so if the control is 
				// disposed off.
				//
				//if (this.InternalEditor.IsInEditMode)
				if ( null != this.InternalEditor && this.InternalEditor.IsInEditMode )
				{
					//	BF 2/15/06	BR07876
					//
					//	Not sure if we should ever specify true for 'forceExit', but we
					//	certainly can't do it if we are going to support the AlwaysInEditMode
					//	property.
					//
					//this.InternalEditor.ExitEditMode(true, true);
					this.InternalEditor.ExitEditMode(this.AlwaysInEditMode == false, true);
				}

				// dirty the child elements
				this.SafeDirtyChildElements();

				// now we can call the base impl.
				base.OnLeave(e);
			}
			finally
			{
				this.inOnLeaveMethod = false;
			}
		}
		#endregion //OnLeave
		
		#region OnLostFocus
		// SSP 6/02/03 UWG2036
		// Overrode OnLostFocus so we can dirty the child elements when the control loses the focus.
		//
		/// <summary>
		/// Overridden. Called when the control loses the focus.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnLostFocus( EventArgs e )
		{
			// verify that we do not need to dirty the child elements
			if ( null != this.element )
				this.element.DirtyChildElements( );

			base.OnLostFocus( e );
		}
		#endregion //OnLostFocus1

		#region OnResize
		/// <summary>
		/// Called when grid itself resizes.
		/// </summary>
        /// <param name="eventArgs">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnResize(System.EventArgs eventArgs )
		{
			this.SetIdealHeight( );

			//call the base
			base.OnResize(eventArgs);

			// SSP 11/16/01 UWG703
			// When the edit portion of the combo box is resized, set the size
			// of the ui element to the new size.
			// And since we are verifying the rects right away, we don't need
			// to dirty the child elements. Then call the SyncTextBoxProperties
			// to resize the text box to new size.
			//
			this.UIElement.Rect = new Rectangle( new Point(0,0), this.Size );
			// SSP 927/07 BR26652
			// 
			//this.UIElement.VerifyChildElements( true );
			if ( this.HasUIElement )
			{
				if ( this.Initializing )
					this.UIElement.DirtyChildElements( );
				else
					this.UIElement.VerifyChildElements( true );
			}

			// AS 11/11/03 [UltraCombo as Editor]
			// We no longer own the textbox.
			//
			//this.SynchTextBoxProperties( );

			//this.UIElement.DirtyChildElements( false );
		}
		#endregion //OnResize

		#region OnTextChanged
		/// <summary>
		/// Called when the text changes
		/// </summary>
		protected override void OnTextChanged( EventArgs e )
		{
			// AS 11/11/03 [UltraCombo as Editor]
			if (this.preventOnTextChanged)
				return;

			// SSP 11/26/02 UWG1821
			// Added a flag that indicates whether we are in base.OnTextChanged method call which
			// fires TextChanged. This is to solve a problem where the user sets the text of the
			// combo to something else in TextChanged event and we igore it because of other 
			// flags to prevent recursion and auto edit feature related flags.
			//
			// -----------------------------------------------------------------------------------
			//base.OnTextChanged( e );
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			// FxCop - Avoid unused private fields
			//string oldText = this.Text;
			//string newText = "";
			//bool origInTextChangedEvent = this.inTextChangedEvent;
			//try
			//{
			//    this.inTextChangedEvent = true;
			//    base.OnTextChanged( e );
			//}
			//finally
			//{
			//    this.inTextChangedEvent = false;
			//    newText = this.Text;
			//}
			base.OnTextChanged( e );
			// -----------------------------------------------------------------------------------

			 // AS 11/11/03 [UltraCombo as Editor]
			// AS 11/17/03
			// We still need this to address UWG1854
			//
			this.UIElement.DirtyChildElements();
		}
		#endregion //OnTextChanged

		// MRS 11/17/04 - UWG2996
		// No longer needed
//		// AS 11/11/03 [UltraCombo as Editor]
//		private bool initializingEditorValue = false;

		#region Text

		// MRS 11/2/04 - UWG2996
		#region Obsolete code
		//		// SSP 5/12/04 UWG3165
		//		// Added SetText method. Moved the code from Text's set into this method.
		//		//
		//		private void SetText( string value, bool calledFromEditorValueChanged )
		//		{
		//			if ( value != base.Text )
		//			{
		//				// SSP 6/18/02 UWG1253
		//				// Added anti-recursion flag.
		//				//
		//				// SSP 11/26/02 UWG1821
		//				// Added a flag that indicates whether we are in base.OnTextChanged method call which
		//				// fires TextChanged. This is to solve a problem where the user sets the text of the
		//				// combo to something else in TextChanged event and we igore it because of other 
		//				// flags to prevent recursion and auto edit feature related flags.
		//				//
		//				//if ( !this.inSetText )
		//				if ( !this.inSetText || this.inTextChangedEvent )
		//				{
		//					this.inSetText = true;
		//
		//					// ZS 2/2/2004 - Data Filter: after discussion we decided that Text property represents 
		//					// display text so we need data filtered editor value.
		//					object edVal = this.InternalEditor.GetDataFilteredDestinationValue(value, 
		//						ConversionDirection.DisplayToEditor, this.EditorOwner, this);			
		//
		//					try
		//					{
		//						// SSP 7/15/03 UWG2315
		//						// Fire TextChanged event after updating the Value. Moved this below.
		//						//
		//						//base.Text = value;
		//					
		//					
		//						// SSP 5/13/02 UWG1102
		//						// Syncronize the Text and Value properties.
		//						//
		//						if ( !this.inValueSet )
		//						{
		//							// AS 11/11/03 [UltraCombo as Editor]
		//							// Follow the ultracomboeditor on this one since
		//							// i can see no other way of getting it to
		//							// update its edit text while in edit mode
		//							//
		//							// SSP 5/12/04 UWG3165
		//							// Only do this if not called from the editor's ValueChanged event handler.
		//							// Because if the editor is firing ValueChanged, we don't want to set the
		//							// value back on the editor. It has various side effects (as described in
		//							// the UWG3165 bug).
		//							//
		//							//if (this.InternalEditor.IsInEditMode)
		//							if ( ! calledFromEditorValueChanged && this.InternalEditor.IsInEditMode )
		//							{
		//								if (this.initializingEditorValue)
		//									return;
		//
		//								// AS 11/11/03
		//								// We need to take some extra steps here.
		//								//
		//								this.initializingEditorValue = true;
		//
		//								try
		//								{
		//									//	BF 1.9.04	[DataFilter_support]
		//									//
		//									//	Since we are setting a property of an explicit type here,
		//									//	we don't want the value to be data filtered, so use the
		//									//	SetEditorValueInternal method instead of setting the editor's
		//									//	Value property directly
		//									//
		//									//this.InternalEditor.Value = value;
		//									// ZS 2/2/2004 - Data Filter: after discussion we decided that Text property represents 
		//									// display text so we need data filtered editor value.
		//									//this.InternalEditor.SetEditorValueInternal( value );
		//									this.InternalEditor.SetEditorValueInternal( edVal );
		//
		//									if (this.InternalEditor.IsInEditMode)
		//										value = this.InternalEditor.CurrentEditText;
		//								}
		//								finally
		//								{
		//									this.initializingEditorValue = false;
		//								}
		//							}
		//
		//							// SSP 7/15/03 UWG2315
		//							// Why are we catching the exception. The set of the Value
		//							// property fires ValueChanged event which the user could've hooked
		//							// into. If the event handler for ValueChanged throws an exception
		//							// we shouldn't catch it.
		//							// Commented out the try-catch block.
		//							//
		//							//try
		//							//{
		//							int index = this.lastIndex;
		//
		//							// ZS 2/2/2004 - Data Filter: after discussion we decided that Text property represents 
		//							// display text so we need data filtered editor value.
		//							//val = ((IValueList)this).GetValue( value, ref index );
		//							object val;
		//
		//							if( this.DataFilter!=null &&
		//								this.InternalEditor.CanHandleConversionInternal(ConversionDirection.DisplayToEditor)==DefaultableBoolean.True )
		//								val = edVal;
		//							else
		//								val = ((IValueList)this).GetValue( value, ref index );
		//
		//							try
		//							{
		//								// SSP 7/15/03 UWG2315
		//								// Prevent the Value property from firing the ValueChanged 
		//								// because we don't want it to fire ValueChanged until we've
		//								// updated the Text property below.
		//								//
		//								this.dontFireValueChanged = true;
		//								// ZS 1/23/04 - Added data filter support.
		//								//this.Value = val;
		//								this.ValueSetInternal(val);
		//							}
		//							finally
		//							{
		//								this.dontFireValueChanged = false;
		//							}
		//							//}
		//							//catch ( Exception )
		//							//{
		//							//}
		//						}
		//
		//						// SSP 7/15/03 UWG2315
		//						// Fire TextChanged event after updating the Value. Moved this from above.
		//						// Also fire ValueChanged event since we are skipping above by setting 
		//						// dontFireValueChanged.
		//						//
		//						base.Text = value;							
		//
		//						// SSP 10/2/03 UWG2685
		//						// Only fire the ValueChanged if we aren't being called from Value's set
		//						// because if we are then it will fire the ValueChanged event itself.
		//						//
		//						// ----------------------------------------------------------------------
		//						//this.FireEvent( ComboEventIds.ValueChanged, new EventArgs( ) );
		//						if ( ! this.inValueSet )
		//							this.FireEvent( ComboEventIds.ValueChanged, new EventArgs( ) );
		//						// ----------------------------------------------------------------------
		//					}
		//					finally
		//					{
		//						// SSP 6/18/02 UWG1253
		//						// Reset the anti-recursion flag.
		//						//
		//						this.inSetText = false;
		//					}
		//				}
		//			}
		//		}
		//
		#endregion Obsolete code

		// RobA UWG415 11/27/01
		// 
		/// <summary>
		/// Overridden. The text displayed in the edit portion of the control.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the user selects an item from the list, the Text property is set based on the <see cref="UltraDropDownBase.DisplayMemberResolved"/> property.</p>
		/// </remarks>
		/// <seealso cref="UltraDropDownBase.DisplayMember"/>
		/// <seealso cref="Value"/>
		public override string Text
		{
			get
			{
                //  BF NA 9.1 - UltraCombo MultiSelect
                if ( this.checkedListSettings != null &&
                     this.checkedListSettings.IsValueProvidedByCheckedItemList )
                {
                    ICheckedItemList checkedItemList = this as ICheckedItemList;
                    return checkedItemList.GetText( this.Value as IList, this.checkedListSettings.ListSeparatorResolved );
                }

				// AS 11/11/03 [UltraCombo as Editor]
				if (this.initializingText)
					return base.Text;

				if ( this.DropDownStyle == UltraComboStyle.DropDownList )
				{
					// if we are in DesignMode always return null
					//
					if ( this.DesignMode )
						return string.Empty;

					else
					{
						// if nothing is selected return null
						//
						if ( ((IValueList)this).SelectedItemIndex < 0 )
							return string.Empty;
					}
				}

				// AS 11/11/03 [UltraCombo as Editor]
				if (this.InternalEditor.IsInEditMode)
					return this.InternalEditor.CurrentEditText;

				return base.Text;
			}
		
			set
			{
                //  BF NA 9.1 - UltraCombo MultiSelect
                if ( this.checkedListSettings != null &&
                     this.checkedListSettings.IsValueProvidedByCheckedItemList )
                {
                    this.checkedListSettings.SetControlTextHelper( value );
                    return;
                }

				// MRS 10/28/04 - UWG2996				
				// Funnel this and Text and IValueList.SelectedItemIndex through a common method
				this.SelectedItemChangeHelper(ComboSelectedItemChangeContext.Text, value);

				//				// SSP 5/12/04 UWG3165
				//				// Added SetText method. Moved the code from here to into the new method.
				//				//
				//				this.SetText( value, false );
			}
		}

		#endregion //Text

		#region OnValidating
		/// <summary>
		/// Overriden. Invoked when the control's Validating event is to be invoked.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnValidating(System.ComponentModel.CancelEventArgs e)
		{
            // MRS 6/5/06 - BR13222
            // Don't bother with any of this if we are disposing.
            if (this.Disposing || this.IsDisposed)
            {
                base.OnValidating(e);
                return;
            }

			//MRS 4/18/06 - BR11468
			// Store whether the control has a valid Null Value.			
			bool controlHasValidNullValue = false;
			if (this.AllowNull == DefaultableBoolean.True)
			{
				string displayText = this.Text;
				if (displayText == null || displayText.Length == 0 ||
					displayText == this.NullText)
				{
					controlHasValidNullValue =  true;
				}
			}

			//JDN 11/9/04 LimitToList
			//base.OnValidating( e );
			//MRS 4/18/06 - BR11468
			//if( !this.IsItemInList() )
			if( !controlHasValidNullValue && 
				!this.IsItemInList() )
			{
				UltraWinEditors.ValidationErrorEventArgs args = 
					new UltraWinEditors.ValidationErrorEventArgs( this.lastValidValue, this.Text );

				args.Beep = false;
               
				if ( this.LimitToList )
					args.RetainFocus = true;
				else
					args.RetainFocus = false;

				this.FireItemNotInList( args );

				// If the Beep property is set to true, make the sound
				if ( args.Beep )
					Microsoft.VisualBasic.Interaction.Beep();
            
				//	If the RetainFocus property is set to true, we don't call the
				//	base implementation so the control retains focus
				if ( args.RetainFocus )
				{
					e.Cancel = true;
				}  
			}

			if( !e.Cancel )
				base.OnValidating( e );

			// AS 5/21/04 UWG3238(aka UWE925)
			// Reset the flag based on the validating return value.
			this.wasLastValidationCancelled = e.Cancel;

			if ( e.Cancel )
			{
				// AS 5/21/04 UWG3238(aka UWE925)
				// When processing a tab key, we set this flag to 
				// false to prevent ourselves from trying to
				// go back into edit mode but if our 
				// validating event is cancelled, we should
				// clear that flag since we need to get
				// back into edit mode.
				//
				this.dontGiveFocusToTextBox = false;
			}
		}
		#endregion //OnValidating

		//	BF 2/15/06	BR07876
		#region OnPaint
		/// <summary>
		/// Invokes the control's Paint event.
		/// </summary>
		/// <param name="e">PaintEventArgs</param>
		protected override void OnPaint( PaintEventArgs e )
		{
			base.OnPaint( e );

			if ( this.hasPainted == false )
			{
				this.hasPainted = true;

				if ( this.AlwaysInEditMode &&
					 this.InternalEditor != null && this.InternalEditor.IsInEditMode == false )
				{
					this.EnterEditMode( false );
				}
			}
		}
		#endregion OnPaint

		#endregion //Control/Component overrides

		#region Infragistics overrides

		#region ControlUIElement
		/// <summary>
		/// Associated Control UIElement object
		/// </summary>
		protected override ControlUIElementBase ControlUIElement 
		{ 
			get
			{
				return this.UIElement; 
			}
		}
		#endregion //ControlUIElement

		#region GetCurrentState
		/// <summary>
		/// Returns the current state.
		/// </summary>
        /// <returns>The current state.</returns>
		internal protected override Int64 GetCurrentState()
		{
			return (Int64)this.CurrentState;
		}
		#endregion //GetCurrentState

		#region GetKeyActionMappings
		/// <summary>
		/// Returns the key action mappings collection.
		/// </summary>
        /// <returns>The key action mappings collection.</returns>
		internal protected override KeyActionMappingsBase GetKeyActionMappings()
		{
			return this.KeyActionMappings;
		}
		#endregion //GetKeyActionMappings

		#region InInitializeLayout
		/// <summary>
		/// Internal property to determine if we are in the InitializeLayout event. 
		/// </summary>
		internal protected override bool InInitializeLayout 
		{ 
			get
			{
				return this.EventManager.InProgress(Infragistics.Win.UltraWinGrid.ComboEventIds.InitializeLayout); 
			}
		}
		#endregion //InInitializeLayout

		#region IsEventInProgress
		/// <summary>
		/// Returns true if the event is in progress
		/// </summary>
        /// <param name="eventId">Indicates the event.</param>
        /// <returns>true if the event is in progress.</returns>
		internal protected override bool IsEventInProgress( Enum eventId )
		{
			ComboEventIds id = (Infragistics.Win.UltraWinGrid.ComboEventIds) eventId;

			return this.EventManager.InProgress( id );
		}
		#endregion //IsEventInProgress

		#region OnActiveRowChange
        /// <summary>
        /// Called when the active row changes
        /// </summary>
        /// <param name="newActiveRow">The new active row.</param>
        /// <param name="scrollIntoView">true to scroll the new row into view.</param>
        /// <returns>true if the row was successfully changes. false it it was cancelled.</returns>
        internal protected override bool OnActiveRowChange(Infragistics.Win.UltraWinGrid.UltraGridRow newActiveRow, bool scrollIntoView)
		{
			if ( newActiveRow == null )
			{
				// AS 11/11/03 [UltraCombo as Editor]
				// I don't think this is needed since the OnValueChanged
				// of the editor should give enough.
				//
				//this.Value = null;
				//this.UpdateDisplayText( string.Empty );
				return false;
			}

			// SSP 4/23/05 - NAS 5.2 Filter Row
			// Added OnActiveStateChanged on row and cell.
			//
			if ( null != this.currentActiveRow )
				this.currentActiveRow.OnActiveStateChanged( false );

			//set activerow to the newactiverow
			this.currentActiveRow = newActiveRow;

			// SSP 4/23/05 - NAS 5.2 Filter Row
			// Added OnActiveStateChanged on row and cell.
			//
			if ( null != this.currentActiveRow )
				this.currentActiveRow.OnActiveStateChanged( true );

			// cache the index
			//
			this.lastIndex = this.currentActiveRow.Index;
			
			// MRS 11/17/04 - UWG2996
			// dontCheckValueBeingOldValue is no longer needed
//			// JJD 10/01/01 - UWG406
//			// update the value property
//			//
//			// SSP 8/20/02 UWG1280
//			// Added a flag to prevent the set of the Value property to not to
//			// return when the old value and new value being assigned is the same.
//			// This is to fix the problem that happens when a new row is selected
//			// that happens to have the same value in the ValueMember field will
//			// cause the set of the Value to return without updating the display
//			// text.
//			//
//			this.dontCheckValueBeingOldValue = true;
//			try
//			{
//				// MRS 10/28/04 - UWG2996				
//				// Funnel this and Text and IValueList.SelectedItemIndex through a common method
//				this.SelectedItemChangeHelper(ComboSelectedItemChangeContext.SelectedIndex , this.lastIndex );
//
//				//				// ZS 1/23/04 - Added data filter support.
//				//				//this.Value = ((IValueList)this).GetValue( this.lastIndex );
//				//				this.ValueSetInternal( ((IValueList)this).GetValue( this.lastIndex ) );
//			}
//			finally
//			{
//				// SSP 8/20/02 UWG1280
//				// Reset the flag.
//				//
//				this.dontCheckValueBeingOldValue = false;
//			}
			this.SelectedItemChangeHelper(ComboSelectedItemChangeContext.SelectedIndex , this.lastIndex );

			return true;		
		}
		#endregion //OnActiveRowChange

		#region OnActiveRowCleared
        /// <summary>
        /// Called when the active row is cleared
        /// </summary>
        ///<param name="update">true to update the row.</param>
        ///<returns>true if the row was successfully cleared. false it it was cancelled.</returns>
        internal protected override bool OnActiveRowCleared(bool update)
		{
			// finally clear the active row
			//
			this.currentActiveRow = null;

			return true;
		}
		#endregion //OnActiveRowCleared

		#region OnBasePropChanged
		/// <summary>
		/// Called by UltraGridBase when a property has changed on a sub-object.
		/// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnBasePropChanged( PropChangeInfo propChange ) 
		{
			try
			{
				if ( this.HasAppearance && 
					propChange.Source == this.appearanceHolder.RootAppearance )
				{
					// SSP 1/10/02
					// Call SetIdealHeight after calling the DirtyChildElements
					// and making sure the elements are positioned properly
					// because it relies on the subelements being positioned at
					// right positions.
					//
					//this.SetIdealHeight();
					this.UIElement.DirtyChildElements( true );
					this.UIElement.VerifyChildElements( true );
					this.SetIdealHeight();
			
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( PropertyIds.Appearance, propChange );
				}
					// SSP 10/16/03 UWG1953
					// Added ButtonAppearance property.
					// Added below else-if block.
					//
				else if ( this.HasButtonAppearance && propChange.Source == this.buttonAppearanceHolder.RootAppearance )
				{
					this.UIElement.DirtyChildElements( true );
					this.UIElement.VerifyChildElements( true );			
					this.NotifyPropChange( PropertyIds.ButtonAppearance, propChange );
				}
				else
					if ( propChange.Source == this.DisplayLayout )
				{
					if ( null != propChange.FindPropId( AppearancePropIds.FontData ) )
					{
						// Clear cached metrics and dirty grid if font changes
						//				
						this.DisplayLayout.ClearCachedCaptionHeight();

						this.DisplayLayout.OnBaseFontChanged();

						this.DisplayLayout.DirtyGridElement();

						this.SetIdealHeight();

						return;
					}

					switch ( (PropertyIds)propChange.PropId )
					{

						case PropertyIds.Scrollbars:
						{
							//old code = m_pLayout->DirtyGridElement();
							//this code was already here
							this.DisplayLayout.UIElement.DirtyChildElements();
							break;
						}
						case PropertyIds.ColScrollRegions:
						case PropertyIds.RowScrollRegions:
						{
							if ( null != propChange.FindPropId( PropertyIds.Hidden ) ||
								null != propChange.FindPropId( PropertyIds.Scrollbar  ) ||
								null != propChange.FindPropId( PropertyIds.Remove  ) )
								this.DisplayLayout.DirtyGridElement();
							else
								this.Invalidate();
							break;
						}
						
							// Invalidate the grid when an appearance property is set
							//
						case PropertyIds.Appearance:
						case PropertyIds.CaptionAppearance:
						{
							bool dirtyGridElement = true;
							
							//ROBA UWG116 8/17/01 
							//assume that the fontdata has changed
							//
							if ( propChange.Trigger == null )
							{
								// Clear cached metrics and dirty grid if font changes
								//				
								this.DisplayLayout.ClearCachedCaptionHeight();

								this.DisplayLayout.OnBaseFontChanged();
							}

							if ( propChange.FindPropId( AppearancePropIds.AlphaLevel ) != null )
								dirtyGridElement = false;
							if ( dirtyGridElement )
								this.DisplayLayout.DirtyGridElement( false );
							else
								this.Invalidate();

							break;
						}

						case PropertyIds.AlphaBlendEnabled:
						case PropertyIds.RowConnectorStyle:
						case PropertyIds.RowConnectorColor:
						{
							bool dirtyGridElement = true;
							
							if ( propChange.FindPropId( AppearancePropIds.AlphaLevel ) != null )
								dirtyGridElement = false;
							if ( dirtyGridElement )
								this.DisplayLayout.DirtyGridElement( false );
							else
								this.Invalidate();

							break;
						}

						case PropertyIds.Caption:
						{
							this.DisplayLayout.DirtyGridElement();
							break;
						}
						
						case PropertyIds.BorderStyle:
							this.DisplayLayout.DirtyGridElement();
							break;
					}
				}
			}
			finally
			{
				// JJD 11/06/01
				// Call the base implementation which will fire
				// the PropertyChanged notification
				//  
				base.OnBasePropChanged( propChange );
			}

		}
		#endregion //OnBasePropChanged

		#region OnPropertyChanged
		// SSP 8/7/02 UWG1291
		// Overrode OnPropertyChanged so we can invalidate when a property's value
		// is changed.
		// 
		/// <summary>
		/// Called when a property has changed
		/// </summary>
		protected override void OnPropertyChanged( Infragistics.Win.PropertyChangedEventArgs e )
		{

            //  BF 9/18/08  TFS7564
            //  When the DisplayMember or ValueMember properties have changed,
            //  the AutoComplete filter condition must be reset so that the
            //  correct column is used to provide the filter criteria.
            PropChangeInfo pic = e.ChangeInfo;
            if ( pic != null && pic.PropId is PropertyIds )
            {
                PropertyIds propId = (PropertyIds)pic.PropId;

                switch ( propId )
                {
                    case PropertyIds.DisplayMember:
                    case PropertyIds.ValueMember:
                    {
                        this.ResetAutoCompleteFilterCondition();

                        //  BF 12/22/08 BF NA 9.1 - UltraCombo MultiSelect
                        
                        //  ValueMember resolves early on to the first column, so we get
                        //  in the CheckedRows verification routine before the ValueMember
                        //  property is set, and it thinks it is up to date when it isn't.
                        if ( this.checkedRows != null )
                            this.checkedRows.DirtyAll();
                    }
                    break;
                }

                //  BF 2/10/09  TFS13516
                //  Treat this as a potential ValueChanged
                PropChangeInfo nullTrigger = pic.FindTrigger( null );
                if ( nullTrigger != null && nullTrigger.PropId is PropertyIds &&
                     (PropertyIds)nullTrigger.PropId == PropertyIds.ColumnFilters &&
                     this.checkedListSettings != null && this.checkedListSettings.IsValueProvidedByCheckedItemList )
                {
                    RowsCollection rows = this.Rows;
                    CheckedListSettings.DataChangedEventArgs args = new CheckedListSettings.DataChangedEventArgs( null, this.checkedListSettings.CheckStateMember, null );
                    rows.FireDataChanged( args );

                    //  If the editor is in edit mode, we have to update the textbox
                    UltraGridComboEditor editor = this.EditorInEditMode;
                    if ( editor != null && editor.TextBox != null )
                        editor.TextBox.Text = this.CheckedRows.ToString( this.checkedListSettings.ListSeparatorResolved );
                }
            }

			// SSP 3/25/06 - App Styling
			// 
			if ( null != this.DisplayLayout )
				StyleUtils.DirtyCachedPropertyVals( this.DisplayLayout );

			// AS 3/11/04
			if (!this.Initializing)
			{
				if ( null != e.ChangeInfo && e.ChangeInfo.PropId is PropertyIds &&
					PropertyIds.Appearance == (PropertyIds)e.ChangeInfo.PropId )
				{
					// SSP 1/10/02
					// Call SetIdealHeight after calling the DirtyChildElements
					// and making sure the elements are positioned properly
					// because it relies on the subelements being positioned at
					// right positions.
					//
					//this.SetIdealHeight();
					this.UIElement.DirtyChildElements( true );
					this.UIElement.VerifyChildElements( true );
					this.SetIdealHeight();
				}

				this.UIElement.DirtyChildElements( );
			}

			base.OnPropertyChanged( e );
		}
		#endregion //OnPropertyChanged

		#region OnSelectingNextControl
		//	BF 8.7.02
		/// <summary>
		/// Called from OnKeyDownForwarded so that derived classes can
		/// perform control-specific actions, such as closing open dropdowns
		/// </summary>
		internal protected override void OnSelectingNextControl()
		{
			if ( this.IsDroppedDown )
				this.CloseUpList();
		}
		#endregion //OnSelectingNextControl

		#region PerformKeyAction
		/// <summary>
		/// Performs a specific key action
		/// </summary>
        ///	<param name="actionCode">An enumeration value that determines the user action to be performed.</param>
        ///	<param name="ctlKeyDown">A boolean specifies whether the action should be performed as if the control key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is maintained, as it is when the user holds down the control key and selects a row in a grid.</param>
        ///	<param name="shiftKeyDown">A boolean specifies whether the action should be performed as if the shift key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is extended, as it is when the user holds down the shift key and selects a range of rows in a grid.</param>                
        ///	<returns>true if the action completed successfully, false if the action failed.</returns>
        internal protected override bool PerformKeyAction(Enum actionCode, bool shiftKeyDown, bool ctlKeyDown)
		{
			// SSP 8/21/02
			//
			//return this.PerformAction( (UltraComboAction)actionCode, shiftKeyDown, ctlKeyDown );
			return this.PerformAction( (UltraComboAction)actionCode, shiftKeyDown, ctlKeyDown, true );
		}
		#endregion //PerformKeyAction
		
		#endregion //Infragistics overrides

		#endregion //Base class overrides

		#region Properties

		#region Public 

		#region Appearance
		/// <summary>
		/// Returns the Appearance for the edit portion of the control.
		/// </summary>
		/// <remarks>
		/// <p class="body">In the UltraCombo, the control-level <b>Appearance</b> property controls the appearance fo the edit portion of the combo. The drop-down (grid) portion of the control is formatted by using the <see cref="UltraGridLayout.Appearance"/> property of the <see cref="UltraGridBase.DisplayLayout"/> object. For more information on how to use the <b>Appearance</b> property, see the the DisplayLayout's <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.Appearance"/> property.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBase.DisplayLayout"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.Appearance"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.Appearances"/>
		[
		LocalizedDescription("LD_UltraCombo_P_Appearance"),
		LocalizedCategory("LC_Appearance"),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )
		]
		public Infragistics.Win.AppearanceBase Appearance
		{
			get
			{
				if (null == this.appearanceHolder )
				{
					this.appearanceHolder = new Infragistics.Win.AppearanceHolder();

					// hook into notifications
					//
					this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.DisplayLayout != null)
					this.appearanceHolder.Collection = this.DisplayLayout.Appearances;

				return this.appearanceHolder.Appearance;
			}

			set
			{
				if ( this.appearanceHolder == null			|| 
					!this.appearanceHolder.HasAppearance	||
					value != this.appearanceHolder.Appearance )
				{
					if ( null == this.appearanceHolder )
					{
						this.appearanceHolder = new Infragistics.Win.AppearanceHolder();
						
						// hook into notifications
						//
						this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;						
					}

					// Initialize the collection
					//
					if (this.DisplayLayout != null)
						this.appearanceHolder.Collection = this.DisplayLayout.Appearances;

					this.appearanceHolder.Appearance = value;

                    // MBS 7/10/09 - TFS19046
                    this.SyncBackColor();

					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( PropertyIds.Appearance );
                    
				}
			}
		}

		/// <summary>
		/// Returns true if the <see cref="Appearance"/> object has been created.
		/// </summary>
		[ Browsable(false),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )  ]
		public bool HasAppearance
		{
			get
			{
				return ( null != this.appearanceHolder &&
					this.appearanceHolder.HasAppearance );
			}
		}

		/// <summary>
		/// Returns true if the appearance has any non-default
		/// property values set
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		internal protected bool ShouldSerializeAppearance() 
		{
			// SSP 8/29/03 UWG2612
			// In addition to checking if the appearance has been allocated, also check
			// if any of its properties are set to a non-default value.
			//
			//return this.HasAppearance;
			return this.HasAppearance && this.Appearance.ShouldSerialize( );
		}
 
		/// <summary>
		/// Resets all <see cref="Appearance"/> property values to their defaults.
		/// </summary>
		// SSP 8/7/02 UWG1426
		// Reset methods should show up in intellisense. Uncommneted below attribute.
		//
		//[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		public void ResetAppearance() 
		{
			if ( this.HasAppearance )
			{
				//				// unhook notification
				//				//
				//				this.appearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				//
				//				this.appearanceHolder.Reset();
				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//
				//this.appearanceHolder.Appearance.Reset();
				// Call reset on the holder instead of the appearance
				//
				this.appearanceHolder.Reset();

				// SSP 8/7/02 UWG1291
				// Notify that the appearance has changed. We don't do this in the Reset
				// of the appearanceHolder however its too late now since all the controls
				// are doing it this ways. Changing the Reset on the appearanceHolder now
				// will cause them to fire twice.
				//
				this.NotifyPropChange( PropertyIds.Appearance );
			}
		}
		#endregion //Appearance
		
		#region AutoEdit

        //  BF 2/28/08  FR09238 - AutoCompleteMode
        #region Obsolete code
        ///// <summary>
        ///// Determines if the combo will support automatic value completion. (Default is true).
        ///// </summary>
        ///// <remarks>
        ///// <p class="body">Setting the <b>AutoEdit</b> property to True will enable automatic edit value completion for the text area of the combo, based on the values in the value list.</p>
        ///// <p class="body">When <b>AutoEdit</b> is True and the user types a character in the combo's text box, the control will search the contents of the <see cref="UltraDropDownBase.DisplayMember"/> column to see if it contains a value that begins with the same character. If it does, this value will appear in the text box, with all of its characters highlighted except the one that the user typed. If the user types a second character, the control will check to see if it is the next highlighted character is in the value that appeared. If it is, the value stays and the character typed becomes deselected. If the second character does not appear in the value, the control searches the list again for a value that begins with the first two characters that were typed. If one exists, it appears in the text box; otherwise the selected text is removed and no more searching takes place. This process continues until the user shifts the input focus away from the control.</p>
        ///// <p class="body">If the UltraCombo's <see cref="DropDownStyle"/> has been set to <b>DropDownList</b>, then setting the <b>AutoEdit</b> to true will enable multi-character auto-edit functionality for the drop down list style. In multi-character auto-edit mode, the control will select the item whose text starts with characters typed so far. After 500 milliseconds of time span since last keystroke, a new auto-edit session begins where the previously typed characters are discarded and new characters are used for multi-character matching.</p>		
        ///// </remarks>
        //[LocalizedDescription("LD_UltraCombo_P_AutoEdit")]
        //[LocalizedCategory("LC_Behavior")]
        //public bool AutoEdit
        //{
        //    get
        //    {
        //        return this.autoEdit;
        //    }

        //    set
        //    {
        //        if ( this.autoEdit != value )
        //        {
        //            this.autoEdit = value;
			
        //            // JJD 11/06/01
        //            // Notify listeners
        //            //  
        //            this.NotifyPropChange( PropertyIds.AutoEdit );
        //        }
        //    }
        //}

        //// SSP 8/1/03 UWG2549
        //// Added ShouldSerialize/Reset methods DisplayStyle property.
        ////
        //#region ShouldSerializeAutoEdit

        ///// <summary>
        ///// Returns true if the property has been set to a non-default value.
        ///// </summary>
        ///// <returns>Returns true if this property is not set to its default value</returns>
        //protected bool ShouldSerializeAutoEdit( )
        //{
        //    return ! this.autoEdit;
        //}

        //#endregion // ShouldSerializeAutoEdit

        //#region ResetAutoEdit

        ///// <summary>
        ///// Resets the <see cref="AutoEdit"/> property to its default value. (true).
        ///// </summary>
        //public void ResetAutoEdit( )
        //{
        //    this.AutoEdit = true;
        //}

        //#endregion // ResetAutoEdit
        #endregion Obsolete code

        //  BF 2/28/08  FR09238 - AutoCompleteMode
		/// <summary>
		/// Determines if the combo will support automatic value completion. (Default is true).
		/// </summary>
		/// <remarks>
		/// <p class="note">With the 2008 Volume 2 release of NetAdvantage, the AutoEdit property has been deprecated, and replaced by the <see cref="AutoCompleteMode"/> property.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraCombo_P_AutoEdit")]
		[LocalizedCategory("LC_Behavior")]
        [Obsolete("This property has been deprecated; use the AutoCompleteMode property instead.", false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
		public bool AutoEdit
		{
			get { return this.AutoEdit_Legacy; }
			set { this.AutoEdit_Legacy = value; }
		}

        internal bool AutoEdit_Legacy
        {
            get { return this.autoCompleteMode == AutoCompleteMode.Append; }
            
            set { this.AutoCompleteMode = value ? AutoCompleteMode.Append : AutoCompleteMode.None; }
        }

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
		/// <remarks>
		/// <p class="note">With the 2008 Volume 2 release of NetAdvantage, the AutoEdit property has been deprecated, and replaced by the <see cref="AutoCompleteMode"/> property.</p>
		/// </remarks>
        /// <returns>Returns false unconditionally since the property is obsolete.</returns>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
		protected bool ShouldSerializeAutoEdit( )
		{
			return false;
		}

		/// <summary>
		/// Resets the <see cref="AutoEdit"/> property to its default value. (true).
		/// </summary>
		/// <remarks>
		/// <p class="note">With the 2008 Volume 2 release of NetAdvantage, the AutoEdit property has been deprecated, and replaced by the <see cref="AutoCompleteMode"/> property.</p>
		/// </remarks>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
		public void ResetAutoEdit( )
		{
			this.AutoEdit_Legacy = true;
		}

		    #endregion //AutoEdit

        //  BF 2/28/08  FR09238 - AutoCompleteMode
		    #region AutoCompleteMode

		/// <summary>
		/// Gets/sets the mode for automatic completion of text typed in the edit portion.
		/// </summary>
		/// <remarks>
		/// <p class="body"></p>
		/// <p class="note">
        /// <b>Note: </b>This property replaces the <see cref="AutoEdit"/> property, which is now obsolete.
        /// Setting the AutoCompleteMode property to 'Append' is the functional equivalent of setting AutoEdit to true.
        /// </p>
		/// <p class="body">
        /// Prior to the addition of the AutoCompleteMode property, automatic value completion was supported in the form
        /// of the now obsolete AutoComplete property, which, when enabled, modified the control's text by appending characters
        /// to the string typed by the end user so as to match the text of the first item found in the list whose
        /// text begins with the typed characters. For example, given a list which contains an item whose text is "Apple",
        /// when the end user types the string "Ap", the edit portion would then be updated to include the remaining characters, "ple",
        /// so that the text of the first item that matches the typed string, "Apple", becomes selected. That same functionality is now
        /// enabled by setting AutoCompleteMode to 'Append'. The appended characters are selected so that continuing to type causes
        /// these characters to be removed.
        /// </p>
		/// <p class="body">
        /// The AutoCompleteMode property extends two additional modes for value completion, 'Suggest' and 'SuggestAppend'. When the
        /// property is set to 'Suggest', no characters are appended to the edit portion, but rather the dropdown is automatically
        /// displayed, showing only the items whose text begins with the string that was typed by the end user. For example, given a
        /// list containing the items, "Apple", "Application", and "Apprehend", upon typing the "A" character, the dropdown list will
        /// appear, displaying all of those items. The list is continually filtered as more characters are typed, eliminating the entries
        /// whose text no longer matches the typed string. For example, if the continues typing until the edit portion contains "Appl", the
        /// "Apprehend" item would be removed from the list, since the presence of the character "l" in the typed string now precludes that
        /// item.
        /// </p>
		/// <p class="body">
        /// The 'SuggestAppend' setting essentially combines the funtionality extended by the 'Append' and 'Suggest' settings. The dropdown
        /// list is automatically displayed and filtered as needed, and text is appended to the edit portion to complete the typed text to
        /// match the first item with matching text.
        /// </p>
		/// </remarks>
		[ LocalizedDescription("UltraCombo_P_AutoCompleteMode")]
        [LocalizedCategory("LC_Behavior")]
		public AutoCompleteMode AutoCompleteMode
		{
			get
			{ 
				return this.autoCompleteMode; 
			}
			set
			{ 
				if ( value != this.autoCompleteMode )
				{
                    // MRS NAS v8.3 - Unit Testing
                    GridUtils.ValidateEnum("AutoCompleteMode", typeof(AutoCompleteMode), value);

					this.autoCompleteMode = value;

					this.NotifyPropChange( PropertyIds.AutoCompleteMode );
				}
			}
		}

        /// <summary>
        /// Returns the resolved value of the <see cref="AutoCompleteMode"/> property.
        /// </summary>
		/// <remarks>
		/// <p class="body">In the absence of an explicit setting, the AutoCompleteMode property resolves to 'Append'.</p>
		/// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public AutoCompleteMode AutoCompleteModeResolved
        {
            get
            {
                return this.autoCompleteMode != AutoCompleteMode.Default ? this.autoCompleteMode : AutoCompleteMode.Append; }
        }

		/// <summary>
		/// Returns true if the <see cref="AutoCompleteMode"/> property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeAutoCompleteMode( )
		{
			return this.autoCompleteMode != AutoCompleteMode.Default;
		}


		/// <summary>
		/// Resets the <see cref="AutoCompleteMode"/> property to its default value.
		/// </summary>
		public void ResetAutoCompleteMode( )
		{
			this.AutoCompleteMode = AutoCompleteMode.Default;
		}
		
		    #endregion AutoCompleteMode

		#region AutoSize
		/// <summary>
		/// Determines whether the height of the control is automatically adjusted to the size of the font used for the text box.
		/// </summary>
		/// <remarks>
		/// If this property is set to True, the height of the control automatically adjusts to the appropriate height based on the font. When False, the control's height remains the same until explicitly changed. The default is True.
		/// </remarks>
		[LocalizedDescription("LD_UltraCombo_P_AutoSize")]
		[LocalizedCategory("LC_Behavior")]
		[Browsable(true)] // MRS 9/26/05 - BR06696
		[DefaultValue(true)] // MRS 9/26/05 - BR06696
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)] // MRS 12/27/05 - BR08493
        public override bool AutoSize
		{
			get
			{
				return this.autoSize;
			}
			set
			{
				if ( value != this.autoSize )
				{
					this.autoSize = value;

					// JJD 1/25/02
					// Adjust the height of the control to take
					// into account changed padding
					if ( this.autoSize )
						this.SetIdealHeight( );
			
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( PropertyIds.AutoSize );

                    this.OnAutoSizeChanged(EventArgs.Empty);
				}
			}
		}

		/// <summary>
		/// Checks to see if AutoSize needs serializing
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAutoSize( )
		{
			return !this.autoSize;
		}

		/// <summary>
		/// Resets the <see cref="AutoSize"/> property to its default value (true).
		/// </summary>
		/// <returns>true</returns>
		public bool ResetAutoSize( )
		{
			// SSP 8/7/02 UWG1292
			// Use the property rather than the variable. Why is thi method returning bool ?
			//
			//return this.autoSize = true;
			return this.AutoSize = true;
		}
		#endregion //AutoSize

        // MBS 7/10/09 - TFS19046
        #region BackColorInternal

        /// <summary>
        /// For internal use only. Used for serializing out the BackColor property value. This
        /// is strictly for backward compatibility.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [DefaultValue(typeof(Color), "")]
        public Color BackColorInternal
        {
            get
            {
                return this.backColorInternal;
            }
            set
            {
                if (value != this.backColorInternal)
                {
                    this.backColorInternal = value;

                    this.SyncBackColor();
                }
            }
        }
        #endregion // BackColorInternal

		#region BorderStyle
		/// <summary>
		/// Returns or sets the border style of the control.
		/// </summary>
		[ LocalizedDescription("LD_UltraCombo_P_BorderStyle") ]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.borderStyle;
			}
			set
			{
				if ( value != this.borderStyle )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UIElementBorderStyle ), value ) )
						throw new InvalidEnumArgumentException( SR.GetString("LER_Exception_307"),
							(int)value, typeof( Infragistics.Win.UIElementBorderStyle ) );

					this.borderStyle = value;

					// Adjust the height of the control to take
					// into account changed padding
					this.SetIdealHeight( );


					if ( null != this.UIElement )
						this.UIElement.DirtyChildElements( );
			
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( PropertyIds.BorderStyle );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyle() 
		{
			return UIElementBorderStyle.Default != this.borderStyle;
		}
 
		/// <summary>
		/// Resets the <see cref="BorderStyle"/> property to it's default (UIElementBorderStyle.Default).
		/// </summary>
		public void ResetBorderStyle() 
		{			
			// SSP 8/7/02 UWG1292
			// Use the property rather than the variable.
			//
			//this.borderStyle = UIElementBorderStyle.Default;
			this.BorderStyle = UIElementBorderStyle.Default;
		}

		// AS 11/11/03 [UltraCombo as Editor]
		internal UIElementBorderStyle BorderStyleResolved
		{
			get
			{
				

				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this.DisplayLayout, StyleUtils.CachedProperty.BorderStyleCombo, out val ) )
				{
					val = StyleUtils.CacheBorderStylePropertyValue( this.DisplayLayout, StyleUtils.CachedProperty.BorderStyleCombo,
						StyleUtils.Role.UltraComboEditPortion, this.BorderStyle, UIElementBorderStyle.Inset );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		#endregion //BorderStyle

		#region ButtonStyleResolved

		// SSP 3/21/06 - App Styling
		// Added ButtonStyleResolved. We need to make use of the ButtonStyle setting of the role.
		// 
		internal UIElementButtonStyle ButtonStyleResolved
		{
			get
			{
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this.DisplayLayout, StyleUtils.CachedProperty.ButtonStyleCombo, out val ) )
				{
					val = StyleUtils.CacheButtonStylePropertyValue( this.DisplayLayout, StyleUtils.CachedProperty.ButtonStyleCombo,
						UIElementButtonStyle.Default, UIElementButtonStyle.Default );
				}

				return (UIElementButtonStyle)val;
			}
		}

		#endregion // ButtonStyleResolved

		#region ButtonAppearance

		// SSP 10/13/03 UWG1953
		// Added ButtonAppearance property to UltraCombo.
		//
		/// <summary>
		/// Gets/sets the <see cref="Infragistics.Win.AppearanceBase"/> object used by the control's buttons.
		/// </summary>
		/// <remarks>
		/// <p class="body">The ButtonAppearance effects the default appearance of the dropdown button as well as any buttons in the <see cref="ButtonsRight"/> and <see cref="ButtonsLeft"/> collections.</p>
		/// </remarks>
		/// <seealso cref="Appearance"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_UltraCombo_P_ButtonAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase ButtonAppearance
		{
			get
			{
				if (null == this.buttonAppearanceHolder )
				{
					this.buttonAppearanceHolder = new Infragistics.Win.AppearanceHolder();

					// hook into notifications
					//
					this.buttonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.DisplayLayout != null)
					this.buttonAppearanceHolder.Collection = this.DisplayLayout.Appearances;

				return this.buttonAppearanceHolder.Appearance;
			}

			set
			{
				if ( this.buttonAppearanceHolder == null			|| 
					!this.buttonAppearanceHolder.HasAppearance	||
					value != this.buttonAppearanceHolder.Appearance )
				{
					if ( null == this.buttonAppearanceHolder )
					{
						this.buttonAppearanceHolder = new Infragistics.Win.AppearanceHolder();
						
						// hook into notifications
						//
						this.buttonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					// Initialize the collection
					//
					if (this.DisplayLayout != null)
						this.buttonAppearanceHolder.Collection = this.DisplayLayout.Appearances;

					this.buttonAppearanceHolder.Appearance = value;

			
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( PropertyIds.ButtonAppearance );
				}
			}
		}

		/// <summary>
		/// Returns true if a <see cref="ButtonAppearance"/> object has been created.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasButtonAppearance
		{
			get
			{
				return null != this.buttonAppearanceHolder && this.buttonAppearanceHolder.HasAppearance;
			}
		}

		/// <summary>
		/// Returns true if the <see cref="ButtonAppearance"/> property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		/// <seealso cref="ButtonAppearance"/>
		protected bool ShouldSerializeButtonAppearance() 
		{
			return this.HasButtonAppearance && this.ButtonAppearance.ShouldSerialize( );
		}
 
		/// <summary>
		/// Resets the <see cref="ButtonAppearance"/> properties to their default values.
		/// </summary>
		/// <seealso cref="ButtonAppearance"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetButtonAppearance() 
		{
			if ( this.HasButtonAppearance )
			{
				this.buttonAppearanceHolder.Reset();
				this.NotifyPropChange( PropertyIds.ButtonAppearance );
			}		
		}

		#endregion	//	ButtonAppearance

		#region CurrentState
		/// <summary>
		/// Returns bit flags that signify the current editing state of the control.
		/// </summary>
		/// <remarks>
		/// The <b>CurrentState</b> property is used primarily in conjunction with the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.KeyActionMappings"/> property and the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.PerformAction(UltraComboAction)"/> method to return information about the state of the control with regards to user interaction. The setting of the <b>CurrentState</b> property indicates which object has focus in the control, whether the user has placed the control into edit mode, and other information such as whether a combo box is dropped down or whether a row is expanded.
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)]
		public UltraComboState CurrentState
		{
			get
			{
				Int64 state = 0;

				// set the current state
				//
				if ( this.ActiveRow != null )
					state |= (Int64)UltraComboState.Row; 

				if ( this.IsDroppedDown )
					state |= (Int64)UltraComboState.IsDroppedDown; 

				if ( this.DropDownStyle != UltraComboStyle.DropDownList )
					state |= (Int64)UltraComboState.HasEdit;
 
                //  BF NA 9.1 - UltraCombo MultiSelect
                if ( this.CheckedListSettings.IsValueProvidedByCheckedItemList )
                    state |= (Int64)UltraComboState.ValueProvidedByCheckedItemList;

				return (UltraComboState) state;
			}
		}
		#endregion //CurrentState

		#region DisplayStyle

		// SSP 6/02/03 UWG2036
		// Added DisplayStyle property.
		//
		/// <summary>
		/// Gets/sets the display style.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can choose from a number of different display style that emulate the common applications like several versions of Microsoft Office and Microsoft Visual Studio.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraCombo_P_DisplayMode"), LocalizedCategory("LC_Appearance") ]
		public EmbeddableElementDisplayStyle DisplayStyle
		{
			get
			{ 
				return this.displayStyle; 
			}
			set
			{ 
				if ( value != this.displayStyle )
				{
                    // MRS NAS v8.3 - Unit Testing
                    GridUtils.ValidateEnum("DisplayStyle", typeof(EmbeddableElementDisplayStyle), value);
                    
					this.displayStyle = value;

					if ( null != this.UIElement )
					{						
						// SSP 9/23/02 UWM152
						// When the display style changes, the required height can also change
						// since the border style can change when display style is changed.
						// Call SetIdealHeight.
						//
						this.SetIdealHeight( );

						this.UIElement.DirtyChildElements( );
					}

					//	Notify listeners of the property change
					// 
					this.NotifyPropChange( Infragistics.Win.UltraWinMaskedEdit.PropertyIds.DisplayStyle );
				}
			}
		}

		#region DisplayStyleResolved

		// SSP 4/10/06 - App Styling
		// 
		internal EmbeddableElementDisplayStyle DisplayStyleResolved
		{
			get
			{
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this.DisplayLayout, StyleUtils.CachedProperty.DisplayStyle, out val ) )
				{
					AppStyling.ComponentRole componentRole = StyleUtils.GetComponentRole( this.DisplayLayout );
					EmbeddableElementDisplayStyle styleVal = null != componentRole 
						? StyleUtils.ViewStyleToDisplayStyle( componentRole.ViewStyle )
						: EmbeddableElementDisplayStyle.Default;

					val = StyleUtils.CachePropertyValue( this.DisplayLayout, StyleUtils.CachedProperty.DisplayStyle, 
						styleVal, this.DisplayStyle, EmbeddableElementDisplayStyle.Default, EmbeddableElementDisplayStyle.Standard );
				}

				return (EmbeddableElementDisplayStyle)val;
			}
		}

		#endregion // DisplayStyleResolved

		// SSP 8/1/03 UWG2549
		// Added ShouldSerialize/Reset methods DisplayStyle property.
		//
		#region ShouldSerializeDisplayStyle

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDisplayStyle( )
		{
            // MRS NAS v8.3 - Unit Testing
            //// SSP 8/5/03 UWG2549
            ////
            ////return EmbeddableElementDisplayStyle.Standard == this.displayStyle;
            //return EmbeddableElementDisplayStyle.Standard != this.displayStyle;
            return EmbeddableElementDisplayStyle.Default != this.displayStyle;
		}

		#endregion // ShouldSerializeDisplayStyle

		#region ResetDisplayStyle

		/// <summary>
		/// Resets the <see cref="DisplayStyle"/> property to its default value (EmbeddableElementDisplayStyle.Standard).
		/// </summary>
		/// <returns></returns>
		public void ResetDisplayStyle( )
		{
			this.DisplayStyle = EmbeddableElementDisplayStyle.Standard;
		}

		#endregion // ResetDisplayStyle
		
		#endregion // DisplayStyle

		#region DropDownStyle
		/// <summary>
		/// Determines the style of the combo.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can choose between dropdown and dropdown list styles for the UltraCombo. The default is to use the dropdown style, where the text portion of the control is editable. You can also choose a dropdown list style, where the text portion of the control is not editable and the user must choose an item from the list.</p>
		/// </remarks>
		/// <seealso cref="LimitToList"/>
		[ LocalizedDescription("LD_UltraCombo_P_DropDownStyle") ]
		[ LocalizedCategory("LC_Appearance") ]
		[ DefaultValue( UltraComboStyle.DropDown ) ] 
		public UltraComboStyle DropDownStyle
		{
			get 
			{
				return this.dropDownStyle;
			}
			set
			{
				if ( value != this.dropDownStyle )
				{
                    // MRS NAS v8.3 - Unit Testing
                    //// test that the value is in range
                    ////
                    //if ( !Enum.IsDefined( typeof(Infragistics.Win.UltraWinGrid.UltraComboStyle), value ) )
                    //    throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_270") );
                    GridUtils.ValidateEnum("DropDownStyle", typeof(UltraComboStyle), value);

					this.dropDownStyle = value;

					// AS 11/11/03 [UltraCombo as Editor]
					// No longer needed - just let the elements get
					// dirtied so the combo editor can synchronize with
					// the owner.
					//
					// if the style is dropdown list then
					// get rid of the textbox
					//
					//if ( this.dropDownStyle == UltraComboStyle.DropDownList )
					//	this.FreeTextBox();

					// RobA UWG829 12/3/01 
					// Dirty the child elements
					//												
					this.UIElement.DirtyChildElements();
								
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( PropertyIds.DropDownStyle );
				}
			}
		}
		#endregion //DropDownStyle

		#region EventManager
		/// <summary>
		/// The object that enables, disables and controls firing of specific control events.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>EventManager</b> gives you a high degree of control over how the component invokes event procedures. You can use it to selectively enable and disable event procedures depending on the context of your application. You can also use the event manager to return information about the state of the component's events.</p>
		/// <p class="body">The event manager's methods are used to determine the enabled state of an event (<see cref="Infragistics.Win.UltraWinGrid.ComboEventManager.IsEnabled(ComboEventIds)"/>), to selectively enable or disable events (<see cref="Infragistics.Win.UltraWinGrid.ComboEventManager.SetEnabled(ComboEventIds,bool)"/>), and to tell whether an event procedure is currently being processed (<see cref="Infragistics.Win.UltraWinGrid.ComboEventManager.InProgress"/>). There is also an <seealso cref="Infragistics.Shared.EventManagerBase.AllEventsEnabled"/> property that you can check to quickly determine whether any events have been disabled by the event manager.</p>
		/// </remarks>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.UltraWinGrid.ComboEventManager EventManager
		{
			get
			{
				if ( null == this.eventManager )
				{
					this.eventManager = new Infragistics.Win.UltraWinGrid.ComboEventManager( this );
				}

				return this.eventManager;
			}
		}
		#endregion //EventManager

		#region KeyActionMappings
		/// <summary>
		/// Gives you the ability to reconfigure the way the control responds to user keystrokes.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>KeyActionMappings</b> property provides access to the control's mechanism for handling keyboard input from users. All keystrokes for actions such as selection, navigation and editing are stored in a table-based system that you can examine and modify using this property. Through the <b>KeyActionsMappings</b> property, you can customize the keyboard layout of the control to match your own standards for application interactivity.</p>
		/// <p class="body">For example, if you wanted users to be able to navigate between cells by pressing the F8 key, you could add this behavior. You can specify the key code and any special modifier keys associated with an action, as well as determine whether a key mapping applies in a given context.</p>
		/// <p class="body">The following table lists the default key mappings for the UltraCombo control:</p>
		/// <p class="body">
		/// <table border="1" cellpadding="3" width="100%" class="FilteredItemListTable">
		/// <thead>
		/// <tr>
		/// <th>KeyCode</th>
		/// <th>ActionCode</th>
		/// <th>StateRequired</th>
		/// <th>StateDisallowed</th>
		/// <th>SpecialKeysRequired</th>
		/// <th>SpecialKeysDisallowed</th>
		/// </tr>
		/// </thead>
		/// <tbody>
		/// <tr><td class="body">Up</td><td class="body">PrevRow</td><td class="body">Row</td><td class="body"></td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Down</td><td class="body">NextRow</td><td class="body">Row</td><td class="body"></td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Up</td><td class="body">FirstRow</td><td class="body"></td><td class="body">Row</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Down</td><td class="body">FirstRow</td><td class="body"></td><td class="body">Row</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Right</td><td class="body">FirstRow</td><td class="body"></td><td class="body">Row, HasEdit</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Left</td><td class="body">FirstRow</td><td class="body"></td><td class="body">Row, HasEdit</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Home</td><td class="body">FirstRow</td><td class="body">IsDroppedDown</td><td class="body"></td><td class="body"></td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">End</td><td class="body">LastRow</td><td class="body">IsDroppedDown</td><td class="body"></td><td class="body"></td><td class="body">AltCtrl</td></tr>
		/// <tr><td class="body">Right</td><td class="body">NextRow</td><td class="body">Row</td><td class="body">HasEdit</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Left</td><td class="body">PrevRow</td><td class="body">Row</td><td class="body">HasEdit</td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Prior</td><td class="body">PageUp</td><td class="body">Row</td><td class="body"></td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Next</td><td class="body">PageDown</td><td class="body">Row</td><td class="body"></td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Escape</td><td class="body">CloseDropdown</td><td class="body">IsDroppedDown</td><td class="body"></td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Enter</td><td class="body">CloseDropdown</td><td class="body">IsDroppedDown</td><td class="body"></td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">F4</td><td class="body">ToggleDropdown</td><td class="body"></td><td class="body"></td><td class="body"></td><td class="body">Alt</td></tr>
		/// <tr><td class="body">Up</td><td class="body">ToggleDropdown</td><td class="body"></td><td class="body"></td><td class="body">Alt</td><td class="body"></td></tr>
		/// <tr><td class="body">Down</td><td class="body">ToggleDropdown</td><td class="body"></td><td class="body"></td><td class="body">Alt</td><td class="body"></td></tr>
		/// </tbody>
		/// </table>
		/// </p>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		public ComboKeyActionMappings  KeyActionMappings
		{
			get
			{
				if ( null == this.keyActionMappings )
					this.keyActionMappings = new ComboKeyActionMappings();

				return this.keyActionMappings;
			}
		}
		#endregion //KeyActionMappings

		#region Padding
		/// <summary>
		/// Returns or sets the amount of padding around the text, in pixels.
		/// </summary>
		/// <remarks>
		/// Padding is the space between the text and the border of the control.
		/// </remarks>
		[LocalizedDescription("LD_UltraCombo_P_Padding")]
		[LocalizedCategory("LC_Appearance")]
        public new Size Padding
		{
			get
			{
				return this.padding;
			}
			set
			{
				if ( value != this.padding )
				{
					if ( value.Width < 0 || value.Height < 0 )
						throw new ArgumentOutOfRangeException( SR.GetString("LER_Exception_326"),
							value,SR.GetString("LER_Exception_327" ));
				
					this.padding = value;

					// Adjust the height of the control to take
					// into account changed padding
					this.SetIdealHeight( );
			
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( PropertyIds.Padding );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializePadding() 
		{
			return ( DEFAULT_PADDING != this.padding.Width ||
				DEFAULT_PADDING != this.padding.Height );
		}
 
		/// <summary>
		/// Resets the <see cref="Padding"/> property to it's default (1,1).
		/// </summary>
		public void ResetPadding() 
		{
			// SSP 8/7/02 UWG1292
			// Use the property rather than the variable.
			//
			//this.padding.Width	= DEFAULT_PADDING;
			//this.padding.Height = DEFAULT_PADDING;
			this.Padding = new Size( DEFAULT_PADDING, DEFAULT_PADDING );
		}
		#endregion //Padding

		#region ReadOnly

		// SSP 8/1/03 UWG1662
		// Exposed ReadOnly property off the UltraCombo.
		// Added IsReadOnly read-only (only get) virtual method to the UltraDropDownBase so
		// the any deriving class with a ReadOnly or a similar property can override this
		// and specify that the drop down should be read-only.
		//
		#region IsReadOnly

		/// <summary>
		/// Specifies whether the ultra drop down is read-only. This property returns the value of ReadOnly property.
		/// </summary>
		protected override bool IsReadOnly
		{
			get
			{
				return this.ReadOnly;
			}
		}

		#endregion // IsReadOnly

		// SSP 8/1/03 UWG1662
		// We need to expose a public ReadOnly property that the user can set to make 
		// the UltraCombo read-only. We already had an internal ReadOnly property
		// which I commented out below and added a new public ReadOnly property.
		//
		// ------------------------------------------------------------------------------
		

		/// <summary>
		/// Specifies whether the UltraCombo is read-only.
		/// </summary>
		/// <remarks>
		/// <p class="body">When this property is set to <b>True</b> the user won't be allowed to change the value of the combo.</p>
		/// </remarks>
		public bool ReadOnly
		{
			get
			{
				return this.isReadOnly;
			}
			set
			{
				if ( value != this.isReadOnly )
				{
					// SSP 10/17/03 UWG2718
					// Typo. Use the value and not the literal true. 
					//
					//this.isReadOnly = true;
					this.isReadOnly = value;

					// AS 11/11/03 [UltraCombo as Editor]
					// We no longer own the textbox.
					//
					// SSP 8/1/03 UWG1662
					// Exposed ReadOnly property off the UltraCombo.
					// Added IsReadOnly read-only (only get) virtual method to the UltraDropDownBase so
					// the any deriving class with a ReadOnly or a similar property can override this
					// and specify that the drop down should be read-only.
					// Set the text box' ReadOnly property to the new value. NOTE; We are also doing
					// this when we create the text box.
					//
					// --------------------------------------------------------------------------------
					//if ( null != this.textbox )
					//	this.textbox.ReadOnly = this.IsReadOnly;
					// --------------------------------------------------------------------------------

					// AS 11/11/03 [UltraCombo as Editor]
					// We no longer own the textbox - just
					// dirty the element and let the combo
					// editor pick up the change.
					//
					//this.SynchTextBoxProperties( );
					this.SafeDirtyChildElements();

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ReadOnly );
				}
			}
		}
		// ------------------------------------------------------------------------------

		#region ShouldSerializeReadOnly

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeReadOnly( )
		{
			return this.isReadOnly;
		}

		#endregion // ShouldSerializeReadOnly

		#region ResetReadOnly

		/// <summary>
		/// Resets the <see cref="ReadOnly"/> property to it's default (false).
		/// </summary>
		public void ResetReadOnly( )
		{
			this.ReadOnly = false;
		}

		#endregion // ResetReadOnly

		#endregion // ReadOnly

		#region TextBox
		/// <summary>
		/// Returns the texbox control used for the edit portion of the combo. Thsi property is read-only.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ Browsable( false ) ]
			// AS 11/11/03 [UltraCombo as Editor]
			// We need to change the return type. This should
			// not really break backward compatibility since
			// there wasn't anything of value on the derived control.
			//
			//public ComboTextBox Textbox
		public TextBox Textbox
		{
			get
			{
				
				// AS 11/11/03 [UltraCombo as Editor]
				// Do what we did before when in dropdown list
				// and at design time...
				if ( this.dropDownStyle == UltraComboStyle.DropDownList ||
					this.DesignMode )
				{
					return null;
				}

				// make sure we're still around
				if (this.IsDisposed || this.Disposing)
					return null;

				// get it from the editor
				return this.InternalEditor.TextBox;
			}
		}
		#endregion //TextBox

		#region UIElement
		/// <summary>
		/// The main element for the combo's list portion (read-only)
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)]
		public UltraComboUIElement UIElement 
		{ 
			get
			{
				if ( this.element == null )
					this.element = new UltraComboUIElement( this );
				
				return this.element; 
			}
		}
		#endregion //UIElement

		#region HasUIElement

		// SSP 9/27/07 BR26652
		// 
		/// <summary>
		/// Returns true if the combo ui element has been allocated.
		/// </summary>
		internal bool HasUIElement
		{
			get
			{
				return null != this.element;
			}
		}

		#endregion // HasUIElement

		#region Value
		/// <summary>
		/// The value that has been selected from the dropdown list.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the user selects an item from, the value is set based on the <see cref="UltraDropDownBase.ValueMemberResolved"/> property.</p>
		/// <p class="body">When the user enters text into the combo, the list is searched based on the <see cref="UltraDropDownBase.DisplayMemberResolved"/> property. If a matching string is found in the <see cref="UltraDropDownBase.DisplayMemberResolved"/> column, the Value of the combo is the value of the <see cref="UltraDropDownBase.ValueMemberResolved"/> column in the same row. If the text entered in the combo does not match any item on the list, then the value will return the displayed Text.</p>
		/// </remarks>
		/// <seealso cref="UltraDropDownBase.ValueMember"/>
		/// <seealso cref="UltraDropDownBase.DisplayMember"/>
		/// <seealso cref="UltraDropDownBase.ValueMemberResolved"/>
		/// <seealso cref="UltraDropDownBase.DisplayMemberResolved"/>
		/// <seealso cref="Text"/>			
		[ Browsable( false ) ]
		[ Bindable( true ) ]
		[ DefaultValue( null ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public object Value 
		{
			get
			{
				if ( this.DesignMode )
				{
					// AS 11/11/03 [UltraCombo as Editor]
					// At design time, we need to return the underlying
					// text so it displays in the control.
					//
					//return null;
					return base.Text;
				}

                //  BF 12/1/08
                //  If we are in "multi-select" mode, whereby the value is determined
                //  by which rows are "checked", return the value of the ValueMember column
                //  for each row that is considered to be checked.
                #region NA 9.1 - UltraCombo MultiSelect
                if ( this.checkedListSettings != null &&
                     this.checkedListSettings.IsValueProvidedByCheckedItemList )
                {
                    return this.CheckedRows.Values;
                }
                #endregion NA 9.1 - UltraCombo MultiSelect

				

				// MRS 6/27/05 - BR04469, BR04792, BR04781 
				// The following code is copied from UltraComboEditor control to handle
				// when the value is set to something invalid and the combo is in 
				// DropDownList mode. This used to just return null. The fix for
				// BR04469 made it raise an exception. This was technically correct, 
				// but it broke existing functionality. So now we are back to 
				// returning null. 
				// --------------------------------------------------------
				string text =	(this.InternalEditor.IsInEditMode && ! this.InternalEditor.IsEnteringEditMode) ?
					this.InternalEditor.CurrentEditText :
					this.Text;
				
				if ( this.EditorOwner.IsNullable(this) && this.SelectedRow == null &&
					(text == null || text.Length == 0) )
					return null;
				// --------------------------------------------------------

				// ZS 1/23/04 - Added data filter support.
				//return this.selectedValue;
				return this.InternalEditor.GetDataFilteredDestinationValue(this.selectedValue, 
					ConversionDirection.EditorToOwner, this.EditorOwner, this);
			}
			set
			{
                //  BF 12/1/08
                //  If we are in "multi-select" mode, whereby the value is determined
                //  by which rows are "checked", return the value of the ValueMember column
                //  for each row that is considered to be checked.
                #region NA 9.1 - UltraCombo MultiSelect
                if ( this.checkedListSettings != null &&
                     this.checkedListSettings.IsValueProvidedByCheckedItemList )
                {
                    string listSeparator = this.checkedListSettings.ListSeparatorResolved;
                    ICheckedItemList checkedItemList = this as ICheckedItemList;

                    //  Set the Value on the ICheckedItemList implementation
                    checkedItemList.Value = value as IList;

                    return;
                }
                #endregion NA 9.1 - UltraCombo MultiSelect

				// MRS 10/28/04 - UWG2996				
				// Funnel this and Text and IValueList.SelectedItemIndex through a common method
				this.SelectedItemChangeHelper(ComboSelectedItemChangeContext.Value , value);

				//				// ZS 1/23/04 - Added data filter support.
				//				value = this.InternalEditor.GetDataFilteredDestinationValue(value, 
				//					ConversionDirection.OwnerToEditor, this.EditorOwner, this);
				//
				//				this.ValueSetInternal(value);
			}
		}
		#endregion //Value

		//	BF 1.7.04	DataFilter
		#region DataFilter

		// MRS 11/2/04 - UWG2996
		#region Obsolete code
		//		// ZS 1/23/04 - Added for data filter support.
		//		private void ValueSetInternal(object value)
		//		{
		//
		//			// AS - 4/16/02 UWG1093
		//			// When comparing boxed types, using the not equals operator
		//			// is doing a reference comparison. Use the Equals method
		//			// instead to see if the values are the same.
		//			//
		//			// SSP 8/20/02 UWG1280
		//			// Added a flag to prevent the set of the Value property to not to
		//			// return when the old value and new value being assigned is the same.
		//			// This is to fix the problem that happens when a new row is selected
		//			// that happens to have the same value in the ValueMember field will
		//			// cause the set of the Value to return without updating the display
		//			// text.
		//			//
		//			//if (value != null && this.selectedValue != null)
		//			if ( !this.dontCheckValueBeingOldValue && value != null && this.selectedValue != null)
		//			{
		//				if (value.Equals(this.selectedValue))
		//					return;
		//			}
		//
		//			/* AS 11/17/03 [UltraCombo as Editor]
		//				 * Should we do this? Probably not
		//				 * since we don't follow the UltraComboEditor
		//				 * for its get_Value logic.
		//				 * 
		//				// AS 11/11/03 [UltraCombo as Editor]
		//				// 
		//				//	If in edit mode, delegate to editor and return
		//				if ( this.InternalEditor.IsInEditMode )
		//				{
		//					this.InternalEditor.Value = value;
		//					return;
		//				}
		//				*/
		//
		//			// SSP 12/10/02 UWG1882
		//			// Check for dontCheckValueBeingOldValue flag to see if we should compare the old and the
		//			// new value.
		//			//
		//			//if ( value != this.selectedValue )
		//			if ( this.dontCheckValueBeingOldValue || value != this.selectedValue )
		//			{
		//				// SSP 5/14/02 UWG1102
		//				// A flag to prevent Value property from being called repeatedly causing
		//				// an infinite loop.
		//				//
		//				if ( !this.inValueSet )
		//				{
		//					// SSP 5/14/02 UWG1102
		//					// Set the anti-recursive flag.
		//					//
		//					// SSP 10/23/02
		//					// This should have been true.
		//					//this.inValueSet = false;
		//					this.inValueSet = true;
		//
		//					try
		//					{
		//						this.selectedValue = value;
		//
		//						if ( !this.Initializing )
		//						{
		//							/*
		//								if ( this.selectedValue == null ||
		//									 this.selectedValue is System.DBNull )
		//								{
		//									this.lastIndex = -1;
		//									this.Text = string.Empty;
		//								}
		//								else
		//								{
		//									((IValueList)this).GetText( this.selectedValue, ref this.lastIndex );
		//
		//									// set the selected item index
		//									//
		//									if ( this.lastIndex >= 0 )
		//										((IValueList)this).SelectedItemIndex = this.lastIndex;
		//				
		//									string displayString = ((IValueList)this).GetText( this.lastIndex );
		//
		//									if ( displayString != null )
		//									{
		//										//RobA 10/31/01 added check to see if we are in autoedit
		//										//mode so that we don't update the textbox's text.
		//										//
		//										bool inAutoEditMode = false;
		//
		//										//if inAutoEditMode is true do not update the display text
		//										//
		//										if ( this.Textbox != null )
		//											inAutoEditMode = this.Textbox.InAutoEditMode;
		//
		//										// see if data has changed
		//										if ( !inAutoEditMode && !this.Text.Equals( displayString ) )
		//										{
		//											// if so, update display text
		//											this.UpdateDisplayText( displayString );
		//										}
		//									}
		//									else
		//									{
		//										this.Text = string.Empty;
		//									}
		//								}
		//								*/
		//
		//							// SSP 5/13/02 UWG1128
		//							// When the selectedValue is DBNull, then still try to find a match because
		//							// there could be a mapping between DBNull and a display text. If no match is
		//							// found, then set the text to empty string.
		//							//
		//							//if ( this.selectedValue == null ||	this.selectedValue is System.DBNull )
		//							if ( this.selectedValue == null )
		//							{
		//								this.lastIndex = -1;
		//								this.Text = string.Empty;
		//								// SSP 5/13/02 UWG1128
		//								// Syncronize the SelectedItemIndex with this property.
		//								//
		//								((IValueList)this).SelectedItemIndex = this.lastIndex;
		//							}
		//							else
		//							{
		//								((IValueList)this).GetText( this.selectedValue, ref this.lastIndex );
		//
		//								// set the selected item index
		//								//
		//								// SSP 5/13/02 UWG1128
		//								// Regardless of this.lastIndex being -1 or a valid index, we still
		//								// need to keep the SelectedItemIndex and the Value property in sync.
		//								// So even if lastIndex is -1, assign that to SelectedItemIndex keeping
		//								// it in sync with this property.
		//								//
		//								// Took out the if condition.
		//								//if ( this.lastIndex >= 0 )
		//								((IValueList)this).SelectedItemIndex = this.lastIndex;
		//				
		//								string displayString = null;
		//							
		//								// SSP 5/13/02 UWG1128
		//								// Check for the lastIndex being -1 before calling GetText otherwise
		//								// GetText will throw an exception if passed in argument is -1.
		//								//
		//								// ZS 2/2/2004 - Data filter.
		////								if ( this.lastIndex >= 0 )
		////									displayString = ((IValueList)this).GetText( this.lastIndex );
		//								displayString = (string) this.InternalEditor.GetDataFilteredDestinationValue(value,
		//									ConversionDirection.EditorToDisplay, this.EditorOwner, this);
		//
		//								if ( displayString != null )
		//								{
		//									/* AS 11/14/03 [UltraCombo as Editor]
		//										 * We don't need to do this.
		//										 * 
		//										//RobA 10/31/01 added check to see if we are in autoedit
		//										//mode so that we don't update the textbox's text.
		//										//
		//										bool inAutoEditMode = false;
		//
		//										// AS 11/17/03 [UltraCombo as Editor]
		//										// Looks like the EditorWithCombo can handle this without us.
		//										//
		//										//if inAutoEditMode is true do not update the display text
		//										//
		//										//if ( this.Textbox != null )
		//										//	inAutoEditMode = this.Textbox.InAutoEditMode;
		//
		//										// see if data has changed
		//										if ( !inAutoEditMode && !this.Text.Equals( displayString ) )
		//										{
		//											// if so, update display text
		//											this.UpdateDisplayText( displayString );
		//										}
		//											// SSP 10/23/02
		//											// If we are in being called from within the OnTextChanged of the
		//											// text box, we still want to update the ultra combo's Text property.
		//											// 
		//										else if ( !this.Text.Equals( displayString ) )
		//										{
		//											this.Text = displayString;
		//										}
		//										*/
		//										
		//									// AS 11/14/03
		//									// Apparantly the text can be null.
		//									//
		//									//if ( !this.Text.Equals( displayString ) )
		//									if (string.Compare(this.Text, displayString) != 0)
		//									{
		//										this.Text = displayString;
		//									}
		//
		//								}
		//								else
		//								{
		//									// If no match was found for the value being assigned, then sett the
		//									// text to empty string and last index to -1.
		//									//
		//									this.Text = string.Empty;
		//									this.lastIndex = -1;
		//								
		//									// SSP 5/13/02 UWG1128
		//									// Syncronize the SelectedItemIndex with this property.
		//									//
		//									((IValueList)this).SelectedItemIndex = this.lastIndex;
		//								}
		//							}
		//
		//							// SSP 10/28/02 UWG1792
		//							// Invalidate the ui element when the value changes.
		//							//
		//							this.UIElement.Invalidate( );
		//
		//							// fire the change event
		//							//
		//							// SSP 7/15/03 UWG2315
		//							// Added dontFireValueChanged to prevent set of the Value property from firing
		//							// ValueChanged event.
		//							//
		//							if ( ! this.dontFireValueChanged )
		//								this.FireEvent( ComboEventIds.ValueChanged, new EventArgs() );
		//						}
		//					}
		//					finally
		//					{
		//						// SSP 5/14/02 UWG1102
		//						// Reset the anti-recursive flag.
		//						//
		//						this.inValueSet = false;
		//					}
		//				}
		//				
		//			}
		//		}
		//
		#endregion Obsolete code

		/// <summary>
		/// Specifies a custom data filter used by the control's editor.
		/// </summary>
		/// <remarks>
		/// <p class="body">A DataFilter allows you to intercept the value of the control as it passes between different states, such as the text displayed on screen, the editor, and the owner. This is similar to the Parse and Format events of a binding, in that you can manipulate the display without altering the underlying data.</p>
		/// </remarks>		
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IEditorDataFilter DataFilter
		{
			get
			{
				IEditorDataFilter dataFilter = this.InternalEditor.DataFilter;

				// MRS 7/2/04 - UWG978
				// There's no reason to throw an exception here. It's prefectly 
				// valid for the DataFilters to differ.
				//				if(dataFilter!=this.ExternalEditor.DataFilter)
				//					throw new Exception("Internal error: internal and external editors don't have same data filter. ");

				return dataFilter;
			}
			set
			{
                // MRS NAS v8.3 - Unit Testing
                if (this.InternalEditor.DataFilter == value &&
                    this.ExternalEditor.DataFilter == value)
                {
                    return;
                }
                
				this.InternalEditor.DataFilter = value;
				this.ExternalEditor.DataFilter = value;

                // MRS NAS v8.3 - Unit Testing
                this.NotifyPropChange(PropertyID.DataFilter);
			}
		}

		#endregion DataFilter

		#region CharacterCasing

		// SSP 4/2/04 UWG3046
		// Added CharacterCasing property.
		//
		/// <summary>
		/// Gets or sets whether the control modifies the case of characters as they are typed.
		/// </summary>
		/// <remarks>
		/// <p class="body">CharacterCasing is similar to the <b>Case</b> property on a TextBox control. Case allows you to specificy that all text in the Combo will be converted to upper or lower case automatically.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraCombo_P_CharacterCasing")]
		[LocalizedCategory("LC_Behavior")]
		[DefaultValue(CharacterCasing.Normal)] // MBS 2/9/09         
        public System.Windows.Forms.CharacterCasing CharacterCasing
		{
			get
			{
				return this.characterCasing;
			}
			set
			{
				if ( this.characterCasing != value )
				{
					if ( ! Enum.IsDefined( typeof( System.Windows.Forms.CharacterCasing ), value ) )
						throw new InvalidEnumArgumentException( "CharacterCasing" );

					this.characterCasing = value;

					this.NotifyPropChange( PropertyIds.CharacterCasing );				
				}
			}
		}

		#endregion // CharacterCasing

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		#region ButtonsLeft

		/// <summary>
		/// Returns a collection of editor buttons displayed on the left side of the edit area.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use the <see cref="ButtonsLeft"/> or <see cref="ButtonsRight"/> collection to add buttons to the left or right side of an editor, respectively. There are several types of buttons available including <see cref="Infragistics.Win.UltraWinEditors.EditorButton"/>, <see cref="Infragistics.Win.UltraWinEditors.DropDownEditorButton"/>, <see cref="Infragistics.Win.UltraWinEditors.StateEditorButton"/>, <see cref="Infragistics.Win.UltraWinEditors.AutoRepeatEditorButton"/>, <see cref="Infragistics.Win.UltraWinEditors.SpinEditorButton"/>, and <see cref="Infragistics.Win.UltraWinEditors.StateEditorButton"/>.</p>		
		/// </remarks>
		/// <seealso cref="EditorButtonClick"/>
		/// <seealso cref="EditorSpinButtonClick"/>
		/// <seealso cref="BeforeEditorButtonCheckStateChanged"/>
		/// <seealso cref="AfterEditorButtonCheckStateChanged"/>
		/// <seealso cref="BeforeEditorButtonDropDown"/>
		/// <seealso cref="AfterEditorButtonCloseUp"/>
		[LocalizedDescription("LD_UltraCombo_P_ButtonsLeft")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[MergableProperty(false)]
		public EditorButtonsCollection ButtonsLeft
		{
			get
			{
				EmbeddableEditorButtonBase editor = this.InternalEditor;

				if (editor == null)
					return null;

				return editor.ButtonsLeft;
			}
		}

		// JAS 3/11/05
		//
		/// <summary>
		/// Indicates if the <see cref="ButtonsLeft"/> should be serialized.
		/// </summary>
		/// <returns>True if the <see cref="ButtonsLeft"/> contains any items</returns>
		/// <seealso cref="ButtonsLeft"/>
		protected bool ShouldSerializeButtonsLeft()
		{
			return this.internalEditor != null && this.InternalEditor.ButtonsLeft.Count > 0;
		}

		// JAS 3/11/05
		//
		/// <summary>
		/// Clears the <see cref="ButtonsLeft"/> collection.
		/// </summary>
		/// <seealso cref="ButtonsLeft"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetButtonsLeft()
		{
			if (this.internalEditor != null)
				this.InternalEditor.ButtonsLeft.Clear();
		}

		#endregion // ButtonsLeft

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		#region ButtonsRight

		/// <summary>
		/// Returns a collection of editor buttons displayed on the left side of the edit area.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use the <see cref="ButtonsLeft"/> or <see cref="ButtonsRight"/> collection to add buttons to the left or right side of an editor, respectively. There are several types of buttons available including <see cref="Infragistics.Win.UltraWinEditors.EditorButton"/>, <see cref="Infragistics.Win.UltraWinEditors.DropDownEditorButton"/>, <see cref="Infragistics.Win.UltraWinEditors.StateEditorButton"/>, <see cref="Infragistics.Win.UltraWinEditors.AutoRepeatEditorButton"/>, <see cref="Infragistics.Win.UltraWinEditors.SpinEditorButton"/>, and <see cref="Infragistics.Win.UltraWinEditors.StateEditorButton"/>.</p>		
		/// </remarks>
		/// <seealso cref="EditorButtonClick"/>
		/// <seealso cref="EditorSpinButtonClick"/>
		/// <seealso cref="BeforeEditorButtonCheckStateChanged"/>
		/// <seealso cref="AfterEditorButtonCheckStateChanged"/>
		/// <seealso cref="BeforeEditorButtonDropDown"/>
		/// <seealso cref="AfterEditorButtonCloseUp"/>
		[LocalizedDescription("LD_UltraCombo_P_ButtonsRight")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[MergableProperty(false)]
		public EditorButtonsCollection ButtonsRight
		{
			get
			{
				EmbeddableEditorButtonBase editor = this.InternalEditor;

				if (editor == null)
					return null;

				return editor.ButtonsRight;
			}
		}

		// JAS 3/11/05
		//
		/// <summary>
		/// Indicates if the <see cref="ButtonsRight"/> should be serialized.
		/// </summary>
		/// <returns>True if the <see cref="ButtonsRight"/> contains any items</returns>
		/// <seealso cref="ButtonsRight"/>
		protected bool ShouldSerializeButtonsRight()
		{
			return this.internalEditor != null && this.InternalEditor.ButtonsRight.Count > 0;
		}

		// JAS 3/11/05
		//
		/// <summary>
		/// Clears the <see cref="ButtonsRight"/> collection.
		/// </summary>
		/// <seealso cref="ButtonsRight"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetButtonsRight()
		{
			if (this.internalEditor != null)
				this.InternalEditor.ButtonsRight.Clear();
		}

		#endregion // ButtonsRight

		//	BF 2/15/06	BR07876
			#region AlwaysInEditMode (and related)

		/// <summary>
		/// Gets/sets whether the control stays in edit mode when it loses the input focus.
		/// </summary>
		/// <remarks><p class="body">By default, the <see cref="UltraCombo"/> control exits edit mode when it loses the input focus. The <b>AlwaysInEditMode</b> property, when set to true, prevents the control from exiting edit mode when it loses the input focus.</p></remarks>
		[LocalizedDescription("LD_UltraCombo_P_AlwaysInEditMode")]
		[LocalizedCategory("LC_Behavior")]
		public bool AlwaysInEditMode
		{
			get{ return this.alwaysInEditMode; }

			set
			{ 
				if ( this.alwaysInEditMode != value )
				{
					this.alwaysInEditMode = value;

					//	Notify listeners of the property change
					this.NotifyPropChange( PropertyIds.AlwaysInEditMode );

					//	If the property is being set to true, enter edit mode now,
					//	but don't take the input focus
					if ( this.alwaysInEditMode )
						this.EnterEditMode( false );
					else
					{
						//	Since we enter edit mode when the property
						//	is set to true, we should exit edit mode when it is
						//	set to false - assuming we don't have the focus.
						if ( this.ContainsFocus == false )
							this.InternalEditor.ExitEditMode( false, true );
					}

				}
			}
		}

		/// <summary>
		/// <p class="body">Indicates whether the <see cref="AlwaysInEditMode"/> property requires serialization.</p>
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		/// <seealso cref="AlwaysInEditMode"/>
		protected bool ShouldSerializeAlwaysInEditMode()
		{
			return ( this.AlwaysInEditMode != UltraCombo.defaultAlwaysInEditMode );
		}
		/// <summary>
		/// Resets the <see cref="AlwaysInEditMode"/> property to its default value.
		/// </summary>
		/// <seealso cref="AlwaysInEditMode"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetAlwaysInEditMode()
		{
            this.AlwaysInEditMode = UltraCombo.defaultAlwaysInEditMode;
		}		

		private bool EnterEditMode( bool takeFocus )
		{
			//	Don't even try at design time
			if ( this.DesignMode )
				return false;

			//	Get a reference to the control's UIElement and the embeddable editor
			UIElement controlElement = this.ControlUIElement;
			EditorWithText editor = this.InternalEditor;

			//	Return if we could not get either
			if ( controlElement == null || editor == null )
				return false;

			//	Get the embeddable element
			EmbeddableUIElementBase embeddableElement = controlElement.GetDescendant( typeof(EmbeddableUIElementBase) ) as EmbeddableUIElementBase;
			if ( embeddableElement == null )
				return false;

			//	Attempt to enter edit mode on the embeddable element
			return editor.EnterEditMode( embeddableElement, takeFocus );
		}

			#endregion //	AlwaysInEditMode (and related)

        //  BF 11/25/08 NA 9.1 - UltraCombo MultiSelect
        #region CheckedListSettings
        /// <summary>
        /// Returns a <see cref="Infragistics.Win.UltraWinGrid.CheckedListSettings">CheckedListSettings</see> instance which determines
        /// whether an editable checkbox column appears in the dropdown grid, and whether the value of the control's
        /// <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.Value">Value</see> property is obtained from the
        /// <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.CheckedRows">CheckedRows</see> collection.
        /// </summary>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.CheckedListSettings">CheckedListSettings class</seealso>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public CheckedListSettings CheckedListSettings
        {
            get
            {
                if ( this.checkedListSettings == null )
                {
                    this.checkedListSettings = new CheckedListSettings( this );
                    this.checkedListSettings.SubObjectPropChanged += new SubObjectPropChangeEventHandler(this.OnCheckedListSettingsSubObjectPropChanged);                    
                }

                return this.checkedListSettings;
            }
        }

        internal bool HasCheckedListSettings { get { return this.checkedListSettings != null && this.checkedListSettings.ShouldSerialize(); } }

        /// <summary>
        /// Returns a collection which contains the members of the <see cref="Infragistics.Win.UltraWinGrid.UltraGridBase.Rows">Rows</see>
        /// collection whose <see cref="Infragistics.Win.UltraWinGrid.CheckedListSettings.CheckStateMember">CheckStateMember</see> cells
        /// equate to a value of true.
        /// </summary>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.CheckedRowsCollection">CheckedRowsCollection class</seealso>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.CheckedListSettings">CheckedListSettings class</seealso>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public CheckedRowsCollection CheckedRows
        {
            get
            {
                if ( this.checkedRows == null )
                    this.checkedRows = new CheckedRowsCollection( this );

                return this.checkedRows;
            }
        }

        internal UltraGridColumn CheckStateColumn
        {
            get
            {
                string columnKey = this.checkedListSettings != null ? this.checkedListSettings.CheckStateMember : string.Empty;
                if ( string.IsNullOrEmpty(columnKey) == false )
                {
                    UltraGridBand bandZero = this.DisplayLayout.BandZero;
                    return bandZero != null && bandZero.columns.Exists(columnKey) ? bandZero.Columns[columnKey] : null;
                }

                return null;
            }
        }

        private void OnCheckedListSettingsSubObjectPropChanged(PropChangeInfo propChange)
        {
            //  BF 1/20/09  TFS12422
            //  I never added code to dirty the element here.
            UIElement controlElement = this.UIElement;
            if ( controlElement != null )
                controlElement.DirtyChildElements( true );

            //  BF 2/10/09  TFS13731
            //  I also never added the code to raise PropertyChanged.
            this.NotifyPropChange( PropertyIds.CheckedListSettings, propChange );
        }

        #endregion CheckedListSettings

        #endregion //Public

        #region AllActionMappings (internal)
        // SSP 8/20/02 UWG1583
		// When the user calls this method to perform an action, we shouldn't check
		// if that action is allowed by looking at the current key action mappings
		// collection which he could have modified but rather the default
		// key action mappings collection. We don't want to restrict the user
		// from performing only the actions that are in current key action mappings
		// collection.
		//
		internal ComboKeyActionMappings AllActionsMappings
		{
			get
			{				
				if ( null == this.allActionsMappings )
				{
					this.allActionsMappings = new ComboKeyActionMappings( );
					this.allActionsMappings.InternalLoadAllActions( );
				}

				return this.allActionsMappings;
			}
		}
		#endregion //AllActionMappings

		#region ControlForGridDisplay (internal protected)
		/// <summary>
		/// Returns the control that will be used for the grid display.
		/// </summary>
		/// <remarks>The default is the return this. It is overridden by
		/// the UltraCombo class to return a different control (used for the 
		/// dropdown area).</remarks>
		internal protected override System.Windows.Forms.Control ControlForGridDisplay
		{ 
			get 
			{ 
				if ( this.dropDownControl == null )
				{
					this.dropDownControl = new ComboDropDownControl( this );
				}

				return this.dropDownControl; 
			} 
		}
		#endregion //ControlForGridDisplay

		#region IdealHeight (private)
		private int IdealHeight
		{
			get
			{
				 // AS 11/11/03 [UltraCombo as Editor]

				// AS 11/11/03
				// So much for getting it from
				// the editor. The editor will size
				// based on the current text or value
				// converted to text.
				// Just do what the combo does.
				//
				//				System.Drawing.Size size = this.InternalEditor.GetSize( 
				//					this.EditorOwner,		// owner
				//					this,					// context
				//					false,					// value only
				//					true,					// full
				//					true);					// include borders

				// get the control's font
				//
				Font font = this.Font;
				Font createdFont = null;
		
				// if there is a font override then create a temp font
				//
				// AS 1/15/03
				// Accessing the Data method will create a clone of the AppearanceData object.
				//if ( this.Appearance.Data.HasFontData )
				if ( this.HasAppearance && this.Appearance.HasFontData )
				{
					// AS 1/15/03
					// Accessing the Data method will create a clone of the AppearanceData object.
					createdFont = this.Appearance.CreateFont( font );

					if ( createdFont != null )
						font = createdFont;
				}			

				//	Get the image height, if any
				int imageHeight = 0;
				// AS 1/15/03
				// Accessing the Data method will create a clone of the AppearanceData object.
				// Also, we should just be using the GetImage method.
				//Image img = this.GetImageFromAppearanceData( this.Appearance.Data );
				Image img = null;
				
				if (this.HasAppearance)
					img = this.Appearance.GetImage(this.ImageList);

				if ( img != null )
					imageHeight = img.Height + 2;
			
				//	Get the font height
			
				// AS 8/12/03 optimization - Use the new caching mechanism.
				//
				//	BF 7.11.03
				//Graphics gr = this.CreateGraphics();
				//Graphics gr = DrawUtility.CreateReferenceGraphics( this );
				Graphics gr = DrawUtility.GetCachedGraphics( this );
			
				//  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
				//int height = gr.MeasureString( "Wj", font ).ToSize().Height;
				// SSP 11/4/05 BR07555
				// Need to pass printing param when measuring in Whidbey. When printing,
				// in Whidbey we need use the GDI+ measuring.
				// 
				//int height = DrawUtility.MeasureString( gr, "Wj", font ).ToSize().Height;
				int height = this.DisplayLayout.MeasureString( gr, "Wj", font ).Height;

				//	Pad it a little
				// AS 11/11/03
				// The combo editor uses a 'defaultTextPadding' of 4,
				// so we'll use that.
				//height += defaultTextPadding;
				// AS 11/14/03
				// On second thought, since we have a padding property
				// use that.
				//
				//height += 4;
				height += 2 + (this.padding.Height * 2);

				//	Use the greater of the two, font or image
				height = System.Math.Max( height, imageHeight );

				//	Adjust for borders...use the sum of the top and bottom borders
				UIElementBorderWidths borderWidths = DrawUtility.CalculateBorderWidths( this.BorderStyleResolved, Border3DSide.All, this.UIElement );
				height += (borderWidths.Bottom + borderWidths.Top);

				// Clean up the font created above
				if ( createdFont != null )
					createdFont.Dispose();

				// AS 8/12/03 optimization - Use the new caching mechanism.
				//
				//	Clean up the graphics object
				//gr.Dispose();
				DrawUtility.ReleaseCachedGraphics(gr);

				return height;
			}
		}
		#endregion //IdealHeight

		#region TextHeight (internal)
		internal int TextHeight
		{
			get
			{
				// SSP 11/16/01 UWG703
				// If the appearance has a font set, then use that to calculate the height
				// of the text box.
				//
				AppearanceData appData = new AppearanceData( );
				AppearancePropFlags flags = AppearancePropFlags.FontData;
				this.ResolveAppearance( ref appData, flags );

				Font font = this.Font;
				Font createdFont = null;
				createdFont = appData.CreateFont( this.Font );
				if ( null != createdFont )
					font = createdFont;
				
				int height = font.Height;

				if ( null != createdFont )
					createdFont.Dispose( );
				createdFont = null;

				return height;
			}
		}
		#endregion //TextHeight

		// AS 2/5/04 UWG2952
		// This was reported during install testing. Essentially
		// the combo would occasionally size to 0,0 when first
		// created because we didn't specify a default size.
		//
		#region DefaultSize
		/// <summary>
		/// Returns the default size of the control
		/// </summary>
		protected override Size DefaultSize
		{
			get
			{ 
				return new Size(100,21); 
			}
		}
		#endregion //DefaultSize

		// MRS 10/22/04 - UWG3633
		#region MaxLength
		/// <summary>
		/// Gets or sets the maximum number of characters the user can type into the control.
		/// </summary>
		/// <remarks>When the <b>MaxLength</b> property is set to <b>0</b>, the number of characters accepted by the control is limited only by available memory.</remarks>
		[LocalizedDescription("LD_UltraCombo_P_MaxLength")]
		[LocalizedCategory("LC_Behavior")]
		[DefaultValue(defaultMaxLength)]
		public int MaxLength
		{
			get { return this.maxLength; }
			set 
			{ 
				if ( value != this.maxLength )
				{
					//	BF 3.3.03	UWE469
					if ( value < 0 )
						throw new ArgumentException( "MaxLength", SR.GetString( "LER_UltraCombo_MaxLength_Exception" ) );
						
					this.maxLength = value;

					//	Notify listeners of the property change
					this.NotifyPropChange( PropertyIds.MaxLength );

					//	If we are in edit mode, set the EmbeddableTextBox's
					//	MaxLength property
					if ( this.internalEditor.IsInEditMode )
						this.internalEditor.TextBox.MaxLength = value;										}
			}
		}
		#endregion MaxLength

		//JDN 11/9/04 LimitToList
		#region LimitToList
		/// <summary>
		/// Gets/sets a value indicating whether the control will retain focus upon validation whenever
		/// the entered value is not a value in the control's list.
		/// </summary>
		/// <remarks> 
		/// <p class="body">An ItemNotInList event will fire before the Validating event of the control whenever the text value
		/// entered into the editor portion of the control is not a value in the control�s valuelist.
		/// If true The LimitToList property will cancel the validating event and return focus whenever the entered value
		/// is not in the list.</p>
		/// </remarks>
		[DefaultValue( false )]
		[LocalizedDescription("LD_UltraCombo_P_LimitToList")]
		[LocalizedCategory("LC_Behavior")]        
		public bool LimitToList
		{
			get { return this.limitToList; }
			set 
			{ 
				if ( value != this.limitToList )
				{
					this.limitToList = value;

					//	Notify listeners of the property change
					this.NotifyPropChange( PropertyIds.LimitToList );
				}
			}
		}
		#endregion // LimitToList

		#region SyncWithCurrencyManager

		// SSP 1/21/05 BR01891
		// Added SyncWithCurrencyManager property on UltraCombo. This will cause the UltraCombo
		// to synchronize the selected row with the currency manager's Position.
		//
		/// <summary>
		/// Specifies whether to synchronize the selected row of the UltraCombo with the associated currency manager's position. Default is <b>false</b>.
		/// </summary>
		[ DefaultValue( false ) ]
		public bool SyncWithCurrencyManager
		{
			get
			{
				return this.syncWithCurrencyManager;
			}
			set
			{
                // MRS NAS v8.3 - Unit Testing
                if (this.syncWithCurrencyManager == value)
                    return;

				this.syncWithCurrencyManager = value;

                // MRS NAS v8.3 - Unit Testing
                this.NotifyPropChange(Infragistics.Win.UltraWinGrid.PropertyIds.SyncWithCurrencyManager);
			}
		}

		#endregion // SyncWithCurrencyManager

		#endregion //Properties

		#region Methods

		#region FreeTextBox
		 // AS 11/11/03 [UltraCombo as Editor]
		#endregion //FreeTextBox

		#region UpdateDisplayText
		
		#endregion //UpdateDisplayText

		#region ToggleDropdown
		/// <summary>
		/// Changes the list portion of the control between dropped down and closed up.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use this method to switch the combo between its dropped down and closed up states. If the combo is closed up, it will drop down when you invoke this method. If the combo is dropped down, it will close up when you invoke this method.</p>
		/// <p class="body">To determine the current state of the control, use the <see cref="UltraDropDownBase.IsDroppedDown"/> property.</p>
		/// </remarks>
		public void ToggleDropdown()
		{

			//RobA UWG307 9/18/01 in old code we would close up the dropdown if 
			//drop down was called and it was already dropped down
			if ( this.IsDroppedDown )
				this.CloseUpList();
			else
			{
				// JJD 1/25/02 - UWG998
				// Moved this logic into IValueList.DropDown so we could fire
				// the BeforeDropDown event first.
				//
				//RobA UWG544 10/22/01
				//if ( this.ValueMember == string.Empty )
				//{
				//	throw new ArgumentNullException( "ValueMember", "UltraCombo's ValueMember was not set!" );					
				//}

				this.DropDownList();
			}
		}
		#endregion //ToggleDropdown
		
		#region CloseUpList
		internal void CloseUpList()
		{
			// AS 11/11/03 [UltraCombo as Editor]
			// Defer this to the editor.
			//
			//((IValueList)this).CloseUp();	
			if (!this.InternalEditor.IsInEditMode)
				return;

			this.InternalEditor.CloseUp();
		}
		#endregion //CloseUpList

		#region DropDownList
		internal void DropDownList()
		{
			 // AS 11/11/03 [UltraCombo as Editor]

			if (!this.InternalEditor.IsInEditMode)
				this.Focus();
			else if (!this.InternalEditor.Focused && this.InternalEditor.CanFocus)
				this.InternalEditor.Focus();

			if (!this.InternalEditor.IsInEditMode)
				return;

			this.InternalEditor.DropDown();
		}
		#endregion //DropDownList

		#region PerformAction
		/// <summary>
		/// Simulates user interaction with the control.
		/// </summary>
		///	<remarks>
		///	<p class="body">Invoke this method to simulate an action the user can perform.</p>
		///	<p class="body">Many actions are only appropriate in certain situations; if an action is inappropriate, it will not be performed. For example, attempting to delete rows by performing the DeleteRows action (37 - KeyActionDeleteRows) will have no effect if no rows are selected. Similarly, an attempt to toggle a cell's dropdown list by performing a droptown toggle action (14 - KeyActionToggleDropdown) will also be ignored if the column does not have a dropdown list associated with it.</p>
		///	<p class="body">You can use the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.CurrentState"/> property to determine the state of the control when the action is about to be performed.</p>
		///	</remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.CurrentState"/>
 		///	<param name="actionCode">An <see cref="UltraComboAction"/> enumeration value that determines the user action to be performed.</param>
        ///	<returns>true if the action completed successfully, false if the action failed.</returns>
		public virtual bool PerformAction( UltraComboAction actionCode ) 
		{
			return this.PerformAction( actionCode, false, false );
		}

		/// <summary>
		/// Simulates user interaction with the control.
		/// </summary>
		///	<remarks>
		///	<p class="body">Invoke this method to simulate an action the user can perform.</p>
		///	<p class="body">Many actions are only appropriate in certain situations; if an action is inappropriate, it will not be performed. For example, attempting to delete rows by performing the DeleteRows action (37 - KeyActionDeleteRows) will have no effect if no rows are selected. Similarly, an attempt to toggle a cell's dropdown list by performing a droptown toggle action (14 - KeyActionToggleDropdown) will also be ignored if the column does not have a dropdown list associated with it.</p>
		///	<p class="body">You can use the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.CurrentState"/> property to determine the state of the control when the action is about to be performed.</p>
		///	</remarks>
		///	<param name="actionCode">An <see cref="UltraComboAction"/> enumeration value that determines the user action to be performed.</param>
		///	<param name="control">A boolean specifies whether the action should be performed as if the control key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is maintained, as it is when the user holds down the control key and selects a row in a grid.</param>
		///	<param name="shift">A boolean specifies whether the action should be performed as if the shift key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is extended, as it is when the user holds down the shift key and selects a range of rows in a grid.</param>
        ///	<returns>true if the action completed successfully, false if the action failed.</returns>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.CurrentState"/>
		public virtual bool PerformAction( UltraComboAction actionCode, bool shift, bool control ) 
		{
			return this.PerformAction( actionCode, shift, control, false );
		}

		// SSP 8/21/02 UWG1583
		// Added an overload of PerformAction checkIfActionIsAllowed parameter
		internal bool PerformAction( UltraComboAction actionCode, bool shift, bool control, bool checkIfActionIsAllowed ) 
		{
            //  BF 3/5/08   FR09238 AutoCompleteMode
            EditorWithCombo.AutoCompleteInfo autoCompleteInfo = this.AutoCompleteInfo;
            bool isFilteredDropDownVisible = autoCompleteInfo != null && autoCompleteInfo.IsFilteredDropDownVisible;
            bool isRowSelected = this.SelectedRow != null;

			switch ( actionCode )
			{
				case UltraComboAction.ToggleDropdown:
				{
					this.ToggleDropdown();
					return true;
				}

				case UltraComboAction.Dropdown:
				{
					this.DropDownList();
					return true;
				}

				case UltraComboAction.CloseDropdown:
				{
					this.CloseUpList();
					return true;
				}

					// Other actions are intended for selection
				default:
				{
					// see if action is allowed
					// SSP 8/20/02 UWG1583
					// When the user calls this method to perform an action, we shouldn't check
					// if that action is allowed by looking at the current key action mappings
					// collection which he could have modified but rather the default
					// key action mappings collection. We don't want to restrict the user
					// from performing only the actions that are in current key action mappings
					// collection.
					//
					//if ( this.KeyActionMappings.IsActionAllowed( actionCode, this.CurrentState ) )
					bool actionAllowed = true;

					if ( checkIfActionIsAllowed )
						actionAllowed = this.KeyActionMappings.IsActionAllowed( actionCode, (long)this.CurrentState );					
					else
						actionAllowed = this.AllActionsMappings.IsActionAllowed( actionCode, (long)this.CurrentState );

					if ( actionAllowed )
					{
						//based on the action code get a selectable item
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//GridItemBase item = this.DisplayLayout.GetSelectableItem( this.MapToGridAction( actionCode ), ref shift, control );
						GridItemBase item = this.DisplayLayout.GetSelectableItem( UltraCombo.MapToGridAction( actionCode ), ref shift, control );
						
						if ( item != null )
						{						
							//get the items selection strategy
							ISelectionStrategy strategy = ((ISelectionManager)this).GetSelectionStrategy( item );
								
							if ( strategy != null )
							{
								//process that item inside the selection strategy
								//
								bool ret = strategy.ProcessKeyBoardItem ( item, shift, control );

								// JJD 8/21/01 - UWG141
								// Make sure any pending paint is processed
								//
								if ( ret )
								{
									// JJD 9/27/01
									// Call OnSelectedItemChanged so we can update
									// the edit area
									//
									// AS 11/11/03 [UltraCombo as Editor]
									//((IValueListOwner)this).OnSelectedItemChanged();

									// if the list is dropped down scroll the row
									// into view
									//
									if ( this.IsDroppedDown )
									{
										this.DisplayLayout.RowScrollRegions[0].ScrollRowIntoView( item as UltraGridRow );
									}

									this.Update();
								}

                                // MBS 10/13/06 BR15884
                                // We always want to return true here to signify that the key action has been handled
                                //return ret;
                                return true;
							}
						}
					}
					break;
				}
			}

			return false;
		}
		#endregion //PerformAction

		#region MapToGridAction
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private UltraGridAction MapToGridAction( UltraComboAction comboAction )
		private static UltraGridAction MapToGridAction( UltraComboAction comboAction )
		{
			UltraGridAction gridAction = UltraGridAction.NextRow;

			switch ( comboAction )
			{
				case UltraComboAction.FirstRow:
					gridAction = UltraGridAction.FirstRowInGrid;
					break;

				case UltraComboAction.LastRow:
					gridAction = UltraGridAction.LastRowInGrid;
					break;

				case UltraComboAction.NextRow:
					gridAction = UltraGridAction.NextRow;
					break;

				case UltraComboAction.PrevRow:
					gridAction = UltraGridAction.PrevRow;
					break;

				case UltraComboAction.PageDown:
					gridAction = UltraGridAction.PageDownRow;
					break;

				case UltraComboAction.PageUp:
					gridAction = UltraGridAction.PageUpRow;
					break;

				default:
					throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_271"), Shared.SR.GetString("LE_ArgumentException_362") );

			}

			return gridAction;
		}
		#endregion //MapToGridAction

		#region ResolveAppearance
		// SSP 11/22/02
		// Made ResolveAppearance public
		//
		/// <summary>
		/// Resolves the appearance of the edit portion of the combo. 
		/// </summary>
		/// <param name="appData">An <see cref="Infragistics.Win.AppearanceData"/> object whose properties will be set to the resolved values.</param>
		/// <param name="requestedProps">An <see cref="Infragistics.Win.AppearancePropFlags"/> enumeration value which determines which properties to resolve.</param>
		/// <remarks>
		/// <p class="body">The ResolveAppearance method will resolve all appearance properties of the combo control and set those properties on the <see cref="Infragistics.Win.AppearanceData"/> passed in. The properties of the AppearanceData will be set to actual literal values, there will be no defaults. You can use this method to determine the final, resolved colors, fonts, or other settings that the control will be displaying based on all of it's relevant properties.</p>
		/// <p class="body">The requestedProps parameter is a flags enumeration that allows you to specify which proeprties are returned. It is recommended that you only get the properties you need to acomplish what you want for maximum efficiency.</p>
		/// </remarks>
		//internal void ResolveAppearance( ref AppearanceData appData, AppearancePropFlags requestedProps ) 
		public void ResolveAppearance( ref AppearanceData appData, AppearancePropFlags requestedProps )
		{
			this.ResolveAppearance(ref appData, ref requestedProps);
		}

		internal void ResolveAppearance( ref AppearanceData appData, ref AppearancePropFlags requestedProps )
		{
			// SSP 3/14/06 - App Styling
			// 
			// ----------------------------------------------------------------------
			AppStyling.ResolutionOrderInfo order;
			AppStyling.UIRole role = StyleUtils.GetRole( this.DisplayLayout, StyleUtils.Role.UltraComboEditPortion, out order );

			bool isReadOnly = this.ReadOnly;
			if ( isReadOnly )
				StyleUtils.ResolveAppearance( role, AppStyling.RoleState.ReadOnly, ref appData, ref requestedProps );

			bool focused = this.ContainsFocus;
			if ( focused )
				StyleUtils.ResolveAppearance( role, AppStyling.RoleState.Focused, ref appData, ref requestedProps );

			bool isInEditMode = this.InternalEditor.IsInEditMode;
			if ( isInEditMode )
				StyleUtils.ResolveAppearance( role, AppStyling.RoleState.EditMode, ref appData, ref requestedProps );

			if ( order.UseStyleBefore )
				StyleUtils.ResolveAppearance( role, AppStyling.RoleState.Normal, ref appData, ref requestedProps );
			// ----------------------------------------------------------------------

			// merge in the appearance settings
			//
			// SSP 3/14/06 - App Styling
			// 
			//if ( this.HasAppearance )
			if ( this.HasAppearance && order.UseControlInfo )
			{
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( ref appData, 
				//	this.appearanceHolder.Appearance.Data,
				//	ref requestedProps );
				this.appearanceHolder.Appearance.MergeData( ref appData, ref requestedProps );
			}

			// SSP 3/14/06 - App Styling
			// 
			if ( order.UseStyleAfter )
				StyleUtils.ResolveAppearance( role, AppStyling.RoleState.Normal, ref appData, ref requestedProps );

			// MRS - NA v6.3 - Office2007
			// Can't resolve the ultimate default here. It's potentially dependant on the DisplayStyle,
			// so it will get resolved in the EmbeddableEditorBase.MergeDisplayStyleAppearance method.
			//
//			// SSP 8/1/03
//			// Added entries for BackColorDisabled and ForeColorDisabled properties.
//			//
//			// ------------------------------------------------------------------------
//			//	BackColorDisabled
//			//
//			if ( ( requestedProps & AppearancePropFlags.BackColorDisabled ) != 0 &&
//				!appData.HasPropertyBeenSet( AppearancePropFlags.BackColorDisabled ) )
//			{
//				appData.BackColorDisabled	= SystemColors.Control;
//				requestedProps	&= ~AppearancePropFlags.BackColorDisabled;
//			}
//
//			//	ForeColorDisabled
//			//
//			if ( ( requestedProps & AppearancePropFlags.ForeColorDisabled ) != 0 &&
//				!appData.HasPropertyBeenSet( AppearancePropFlags.ForeColorDisabled ) )
//			{
//				appData.ForeColorDisabled	= SystemColors.GrayText;
//				requestedProps	&= ~AppearancePropFlags.ForeColorDisabled;
//			}
//			// ------------------------------------------------------------------------
//			
//			// now set the other properties to their respective defaults
//			//
//			if ( ( requestedProps & AppearancePropFlags.BackColor ) != 0 &&
//				!appData.HasPropertyBeenSet( AppearancePropFlags.BackColor ) )
//			{
//				// SSP 8/1/03 UWG1662
//				// Exposed ReadOnly property off the UltraCombo.
//				// If the control is read-only, then set the back color to SystemColors.Control.
//				//
//				if ( this.IsReadOnly )
//					appData.BackColor			= SystemColors.Control;
//				else
//					appData.BackColor			= SystemColors.Window;
//				requestedProps	&= ~AppearancePropFlags.BackColor;
//			}

			

			// SSP 8/16/02 UWG1572
			// Also set resolve the border color 3d base to Control color otherwise
			// the border will look darker than the regular combo.
			//
			if ( ( requestedProps & AppearancePropFlags.BorderColor3DBase ) != 0 &&
				!appData.HasPropertyBeenSet( AppearancePropFlags.BorderColor3DBase ) )
			{
				appData.BorderColor3DBase			= SystemColors.Control;
				requestedProps	&= ~AppearancePropFlags.BorderColor3DBase;
			}

			// MRS - NA v6.3 - Office2007
			// Can't resolve the ultimate default here. It's potentially dependant on the DisplayStyle,
			// so it will get resolved in the EmbeddableEditorBase.MergeDisplayStyleAppearance method.
			//
//			// now set the other properties to their respective defaults
//			//
//			if ( ( requestedProps & AppearancePropFlags.ForeColor ) != 0 &&
//				!appData.HasPropertyBeenSet( AppearancePropFlags.ForeColor ) )
//			{
//				appData.ForeColor			= SystemColors.WindowText;
//				requestedProps	&= ~AppearancePropFlags.ForeColor;
//			}

			if ( requestedProps == 0 )
				return;

			if ( ( (requestedProps & AppearancePropFlags.ImageHAlign) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.ImageHAlign) )
			{
				appData.ImageHAlign = HAlign.Left;
				requestedProps ^= AppearancePropFlags.ImageHAlign;
			}

			if ( ( (requestedProps & AppearancePropFlags.ImageVAlign) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.ImageVAlign) )
			{
				appData.ImageVAlign = VAlign.Middle;
				requestedProps ^= AppearancePropFlags.ImageVAlign;
			}

			if ( ( (requestedProps & AppearancePropFlags.TextHAlign) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.TextHAlign) )
			{
				appData.TextHAlign = HAlign.Left;
				requestedProps ^= AppearancePropFlags.TextHAlign;
			}

			if ( ( (requestedProps & AppearancePropFlags.TextVAlign) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.TextVAlign) )
			{
				appData.TextVAlign = VAlign.Middle;
				requestedProps ^= AppearancePropFlags.TextVAlign;
			}

			if ( ( (requestedProps & AppearancePropFlags.ImageBackgroundStyle) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.ImageBackgroundStyle) )
			{
				appData.ImageBackgroundStyle = ImageBackgroundStyle.Centered;
				requestedProps ^= AppearancePropFlags.ImageBackgroundStyle;
			}

			// AS 11/14/03 [UltraCombo as Editor]
			// Try to make it look as close as possible to
			// the textbox when its not in edit mode.
			//
			if ( ( (requestedProps & AppearancePropFlags.TextTrimming) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.TextTrimming) )
			{
				appData.TextTrimming = TextTrimming.None;
				requestedProps ^= AppearancePropFlags.TextTrimming;
			}

		}
		#endregion //ResolveAppearance

		#region SynchTextBoxProperties
		 // AS 11/11/03 [UltraCombo as Editor]
		#endregion //SynchTextBoxProperties

		#region SelectDropDownItemFromText
		
		#endregion //SelectDropDownItemFromText

		#region InternalDataSourceInitialized
		// SSP 7/23/02 UWG1420
		//
		internal void InternalDataSourceInitialized( )
		{
			if ( null != this.selectedValue && 				
				( null == this.Text || this.Text.Length <= 0 ) )
			{
				((IValueList)this).GetText( this.selectedValue, ref this.lastIndex );
				((IValueList)this).SelectedItemIndex = this.lastIndex;				
				
				string displayString = "";

                // Check for the lastIndex being -1 before calling GetText otherwise
                // GetText will throw an exception if passed in argument is -1.

                //  BF 2/6/09   TFS13643
                #region Obsolete code
                //// ZS 2/2/2004 - Data filter.
                ////if ( this.lastIndex >= 0 )
                ////	displayString = ((IValueList)this).GetText( this.lastIndex );
                //displayString = (string) this.InternalEditor.GetDataFilteredDestinationValue(this.selectedValue,
                //    ConversionDirection.EditorToDisplay, this.EditorOwner, this);

                //this.Text = null != displayString ? displayString : null;
                #endregion Obsolete code

                //  BF 2/6/09   TFS13643
                //  Don't do this if the value is coming from the checked list
                if ( this.checkedListSettings == null ||
                     this.checkedListSettings.IsValueProvidedByCheckedItemList == false )
                {
				    displayString = (string) this.InternalEditor.GetDataFilteredDestinationValue(this.selectedValue,
					    ConversionDirection.EditorToDisplay, this.EditorOwner, this);

				    this.Text = null != displayString ? displayString : null;
                }
			}

            //  BF NA 9.1 - UltraCombo MultiSelect
            //  If anything about the underlying data source has changed,
            //  dirty up the CheckedRows collection.
            if ( this.checkedRows != null )
                this.checkedRows.DirtyAll();
		}
		#endregion //InternalDataSourceInitialized

		#region SetIdealHeight
		private void SetIdealHeight( )
		{
			// AS 11/11/03 [UltraCombo as Editor]
			// This is no longer needed.
			//
			//this.SynchTextBoxProperties();

			// Adjust the control size only if AutoSize is set to true
			if ( this.AutoSize )
				this.Height = this.IdealHeight;
		}
		#endregion //SetIdealHeight

		#region DelayedInvokeEditorFocus
		// AS 5/21/04 UWG3238(aka UWE925)
		// In VS 2002's usercontrol, after cancelling validation
		// you'll still get an enter but then you'll get a leave
		// which causes us to exit edit mode even though
		// we still have focus. The solution to that was to
		// delay putting us back into edit mode or focusing
		// the editor.
		//
		private void DelayedInvokeEditorFocus()
		{
			if (this.IsDisposed || this.Disposing)
				return;

			if (!this.ContainsFocus)
				return;

			if (!this.InternalEditor.IsInEditMode)
			{
				//	Find the embeddable UIElement, and enter edit mode on it
				EmbeddableUIElementBase  element = this.ControlUIElement.GetDescendant( typeof(EmbeddableUIElementBase) ) as EmbeddableUIElementBase;

				if (element != null)
					this.InternalEditor.EnterEditMode(element);

				// AS 5/14/04 UWE925
				// Moved from OnValidating and added check
				// for the wasLastValidationCancelled flag.
				//
				// MRS 4/30/04 - UWE917
				// If the validation was cancelled, 
				// set the SelStart and SelLength back to what they were
				if (//this.wasLastValidationCancelled && 
					this.InternalEditor.IsInEditMode	&&
					this.InternalEditor.SupportsSelectableText)
				{
					this.InternalEditor.SelectionStart = this.cachedSelStart;
					this.InternalEditor.SelectionLength = this.cachedSelLength;
				}
			}
			else if (this.InternalEditor.CanFocus && !this.InternalEditor.Focused)
				this.InternalEditor.Focus();
		}
		#endregion //DelayedInvokeEditorFocus

		//JDN 11/9/04 LimitToList
		#region IsItemInList  
      
		/// <summary>
		/// Determines if the text in the edit portion of the combo exists in the list.
		/// </summary>
		/// <remarks>
		/// <p class="body">This method searches a column in the list (determined by the <see cref="UltraDropDownBase.DisplayMemberResolved"/> property) to determine if the text current in the edit portion of the cbombo is on the list.</p>
		/// </remarks>		
		/// <returns>
		/// A boolean indicating whether the text in the edit portion of the combo is a value in the control's list.
		/// </returns>
		public bool IsItemInList()
		{
			return this.IsItemInList( this.Text );
		}
       
		/// <summary>
		/// Determines if the specified text exists in the list.
		/// </summary>
		/// <remarks>
		/// <p class="body">This method searches a column in the list (determined by the <see cref="UltraDropDownBase.DisplayMemberResolved"/> property) to determine if the specified text is on the list.</p>
		/// </remarks>		
		/// <returns>
		/// A boolean indicating whether the specified text is a value in the control's list.
		/// </returns>
		/// <param name="displayText">The text to be tested.</param>
		public bool IsItemInList( string displayText )
		{  
			int index = -1;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//object val = ((IValueList)this).GetValue( displayText, ref index );
			( (IValueList)this ).GetValue( displayText, ref index );

            // JDN 2/10/05 BR02183 - a ValueListItem can have a DisplayText and a null DataValue.
            // We should check to see if the by ref index is -1 instead
            //if ( val == null )
            if ( index == -1 )
				return false; 
			else
				return true;
		}

        #endregion // IsItemInList

        // MBS 7/10/09 - TFS19046
        #region OnAppStyleChanged

        internal void OnAppStyleChanged()
        {
            this.SyncBackColor();
        }
        #endregion //OnAppStyleChanged
        //
        #region SyncBackColor

        private void SyncBackColor()
        {
            AppStyling.ResolutionOrderInfo order;
            AppStyling.UIRole role = StyleUtils.GetRole(this.DisplayLayout, StyleUtils.Role.UltraComboEditPortion, out order);

            base.BackColor = Infragistics.Win.Utilities.ResolveControlBackColor(
                role,
                order,
                this.HasAppearance ? this.Appearance : null,
                this.BackColorInternal);
        }
        #endregion //SyncBackColor

        #endregion //Methods

        #region Events

        #region Event declarations
        /// <summary>
		/// Occurs when the display layout is initialized, such as when the control is loading data from the data source.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <see cref="InitializeLayoutEventArgs.Layout"/> argument returns a reference to a <see cref="UltraGridBase.DisplayLayout"/> object that can be used to set properties of, and invoke methods on, the layout of the control. You can use this reference to access any of the returned layout's properties or methods.</p>
		///	<p class="body">Like a form's <b>Load</b> event, this event provides an opportunity to configure the control before it is displayed. It is in this event procedure that actions such as creating appearances, valuelists, and unbound columns should take place.</p>
		///	<p class="body">This event is generated when the control is first preparing to display data from the data source. This may occur when the data source changes.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraCombo_E_InitializeLayout")]	
		public event InitializeLayoutEventHandler   InitializeLayout
		{
			add { this.EventsOptimized.AddHandler( EVENT_INTIALIZELAYOUT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INTIALIZELAYOUT, value ); }
		}

		/// <summary>
		/// Occurs when a row is initialized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <see cref="InitializeRowEventArgs.Row"/> argument returns a reference to an <see cref="UltraGridRow"/> object that can be used to set properties of, and invoke methods on, the row being displayed. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">The <see cref="InitializeRowEventArgs.ReInitialize"/> argument can be used to determine if the row is being initialized for the first time, such as when the <see cref="UltraDropDown"/> is initially loading data, or whether it is being reinitialized, such as when the <see cref="RowsCollection.Refresh(RefreshRow, bool, bool)"/> method is called.</p>
		///	<p class="body">This event is generated once for each row being displayed or printed and provides an opportunity to perform actions on the row before it is rendered, such as populating an unbound cell or changing a cell's color based on its value.</p>
		///	<p class="body">The <see cref="UltraGridLayout.ViewStyle"/> and <see cref="UltraGridLayout.ViewStyleBand"/> properties of the <see cref="UltraGridBase.DisplayLayout"/> object are read-only in this event procedure.</p>		
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeLayout"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraCombo_E_InitializeRow")]	
		public event InitializeRowEventHandler      InitializeRow
		{
			add { this.EventsOptimized.AddHandler( EVENT_INTIALIZEROW, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INTIALIZEROW, value ); }
		}

		/// <summary>
		/// Occurs after the list portion of the combo is dropped down.
		/// </summary>
		/// <remarks>
		/// <p class="note">Note that this event does not fire for editor button (buttons in the <see cref="ButtonsLeft"/> or <see cref="ButtonsRight"/> collection. To trap events for <see cref="DropDownEditorButton"/>s, use the <see cref="BeforeEditorButtonDropDown"/> event.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraCombo_E_AfterDropDown")]	
		public event EventHandler					AfterDropDown
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERDROPDOWN, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERDROPDOWN, value ); }
		}

		/// <summary>
		/// Occurs after the list portion for the combo is closed up.
		/// </summary>
		/// <remarks>
		/// <p class="note">Note that this event does not fire for editor button (buttons in the <see cref="ButtonsLeft"/> or <see cref="ButtonsRight"/> collection. To trap events for <see cref="DropDownEditorButton"/>s, use the <see cref="AfterEditorButtonCloseUp"/> event.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraCombo_E_AfterCloseUp")]	
		public event EventHandler					AfterCloseUp
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERCLOSEUP, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCLOSEUP, value ); }
		}

		/// <summary>
		/// Occurs before the list portion of the combo is dropped down.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use this event to examine or change attributes of the list before it is displayed to the user. You can also use this event to cancel the dropdown of the list before it occurs.</p>
		/// <p class="note">Note that this event does not fire for editor button (buttons in the <see cref="ButtonsLeft"/> or <see cref="ButtonsRight"/> collection. To trap events for <see cref="DropDownEditorButton"/>s, use the <see cref="BeforeEditorButtonDropDown"/> event.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraCombo_E_BeforeDropDown")]	
		public event CancelEventHandler			BeforeDropDown
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREDROPDOWN, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREDROPDOWN, value ); }
		}

		/// <summary>
		/// Occurs when the <see cref="UltraDropDownBase.SelectedRow"/> changes.
		/// </summary>
		/// <remarks>
		/// <p class="body">This event fires any time the selected row of the Combo changes. This will also change the <see cref="Value"/> and <see cref="Text"/> properties of the control, except in cases where the previously selected row has the same value or text as the new selected row.</p>
		/// <p class="note">It is generally undesirable to do anything in this event that will cause the Combo to lose focus, such as displaying a MessageBox. This is because the event will often fire when the user is in the process of (but has not yet completed) making a change, such as when the user is pressing the arrow keys to navigate through the list, or typing into the edit portion of the combo and using AutoEdit to find a particular item.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraCombo_E_RowSelected")]	
		public event RowSelectedEventHandler				RowSelected
		{
			add { this.EventsOptimized.AddHandler( EVENT_ROWSELECTED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_ROWSELECTED, value ); }
		}

		/// <summary>
		/// Occurs when the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.Value"/> property of the control has changed.
		/// </summary>		
		/// <remarks>
		/// <p class="body">This event fires any time the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.Value"/> property of the Combo changes. This will also change the <see cref="UltraDropDownBase.SelectedRow"/> and <see cref="Text"/> properties of the control, except in cases where the previously selected row has the same value or text as the new selected row.</p>
		/// <p class="note">It is generally undesirable to do anything in this event that will cause the Combo to lose focus, such as displaying a MessageBox. This is because the event will often fire when the user is in the process of (but has not yet completed) making a change, such as when the user is pressing the arrow keys to navigate through the list, or typing into the edit portion of the combo and using AutoEdit to find a particular item.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraCombo_E_ValueChanged")]	
		public event EventHandler					ValueChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_VALUECHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_VALUECHANGED, value ); }
		}

		//JDN 11/9/04 LimitToList
		/// <summary>
		/// Occurs before the Validating event of the control whenever the text value entered into the edit portion of the control is not a value in the control�s list.
		/// </summary>
		/// <remarks>
		/// <p class="body">This event fires any time the control is validated (such as when it loses focus) and the text in the edit portion of the Combo does not match a value on the list. The column of the list that is searched for a matching value is determined by the <see cref="UltraDropDownBase.DisplayMemberResolved"/>.</p>		
		/// <p class="body">The <see cref="LimitToList"/> property of the Combo determines the default value of the <see cref="Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs.RetainFocus"/> parameter. You can change this parameter in this event to change the default behavior.</p>
		/// </remarks>
		/// <seealso cref="LimitToList"/>
		/// <seealso cref="IsItemInList()"/>
		[LocalizedDescription("LD_UltraCombo_E_ItemNotInList")]	
		public event ItemNotInListEventHandler		ItemNotInList
		{			
			add { this.EventsOptimized.AddHandler( EVENT_ITEMNOTINLIST, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_ITEMNOTINLIST, value ); }
		}

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		#region Editor Button Events

			#region EditorButtonClick
		/// <summary>
		/// Invoked when an <see cref="Infragistics.Win.UltraWinEditors.EditorButton"/> is clicked.
		/// </summary>
		/// <remarks>
		/// <p class="body">Editor button events such as this one will fire any time the editor button is affected. This means that the event will fire, not only for the specific instance of the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo"/> but also for any object that is using the combo as an editor (an <see cref="UltraGridColumn"/>, for example). Use the <see cref="Infragistics.Win.UltraWinEditors.EditorButtonEventArgs.Context"/> property to determine the object the event applies to.</p>
		/// </remarks>
		/// <seealso cref="ButtonsLeft"/>
		/// <seealso cref="ButtonsRight"/>
		[LocalizedDescription("LD_UltraCombo_E_EditorButtonClick")]
		[LocalizedCategory("LC_Action")]
		public event EditorButtonEventHandler EditorButtonClick
		{
			add { this.EventsOptimized.AddHandler(EVENT_EDITORBUTTONCLICK, value); }
			remove {this.EventsOptimized.RemoveHandler(EVENT_EDITORBUTTONCLICK, value);}
		}
			#endregion //EditorButtonClick

			#region EditorSpinButtonClick
		/// <summary>
		/// Invoked when a <see cref="Infragistics.Win.UltraWinEditors.SpinEditorButton"/> is clicked.
		/// </summary>
		/// <remarks>
		/// <p class="body">Editor button events such as this one will fire any time the editor button is affected. This means that the event will fire, not only for the specific instance of the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo"/> but also for any object that is using the combo as an editor (an <see cref="UltraGridColumn"/>, for example). Use the <see cref="Infragistics.Win.UltraWinEditors.EditorButtonEventArgs.Context"/> property to determine the object the event applies to.</p>
		/// </remarks>
		/// <seealso cref="ButtonsLeft"/>
		/// <seealso cref="ButtonsRight"/>
		[LocalizedDescription("LD_UltraCombo_E_EditorSpinButtonClick")]
		[LocalizedCategory("LC_Action")]
		public event SpinButtonClickEventHandler EditorSpinButtonClick
		{
			add { this.EventsOptimized.AddHandler(EVENT_EDITORSPINBUTTONCLICK, value); }
			remove {this.EventsOptimized.RemoveHandler(EVENT_EDITORSPINBUTTONCLICK, value);}
		}
			#endregion //EditorSpinButtonClick

			#region AfterEditorButtonCloseUp
		/// <summary>
		/// Invoked when a <see cref="Infragistics.Win.UltraWinEditors.DropDownEditorButton"/> has closed.
		/// </summary>
		/// <remarks>
		/// <p class="body">Editor button events such as this one will fire any time the editor button is affected. This means that the event will fire, not only for the specific instance of the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo"/> but also for any object that is using the combo as an editor (an <see cref="UltraGridColumn"/>, for example). Use the <see cref="Infragistics.Win.UltraWinEditors.EditorButtonEventArgs.Context"/> property to determine the object the event applies to.</p>
		/// <p class="note">Note that this event does not apply to the DropDown button that is normally present in the combo. This event only fires for editor buttons in the <see cref="ButtonsLeft"/> or <see cref="ButtonsRight"/> collection. To determine when the standard dropdown is closed, use the <see cref="AfterCloseUp"/> event.</p>
		/// </remarks>
		/// <seealso cref="ButtonsLeft"/>
		/// <seealso cref="ButtonsRight"/>
		[LocalizedDescription("LD_UltraCombo_E_AfterEditorButtonCloseUp")]
		[LocalizedCategory("LC_Action")]
		public event EditorButtonEventHandler AfterEditorButtonCloseUp
		{
			add { this.EventsOptimized.AddHandler(EVENT_AFTEREDITORBUTTONCLOSEUP, value); }
			remove {this.EventsOptimized.RemoveHandler(EVENT_AFTEREDITORBUTTONCLOSEUP, value);}
		}
			#endregion //AfterEditorButtonCloseUp

			#region BeforeEditorButtonDropDown
		/// <summary>
		/// Invoked when a <see cref="Infragistics.Win.UltraWinEditors.DropDownEditorButton"/> is dropping down.
		/// </summary>
		/// <remarks>
		/// <p class="body">Editor button events such as this one will fire any time the editor button is affected. This means that the event will fire, not only for the specific instance of the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo"/> but also for any object that is using the combo as an editor (an <see cref="UltraGridColumn"/>, for example). Use the <see cref="Infragistics.Win.UltraWinEditors.EditorButtonEventArgs.Context"/> property to determine the object the event applies to.</p>
		/// <p class="note">Note that this event does not apply to the DropDown button that is normally present in the combo. This event only fires for editor buttons in the <see cref="ButtonsLeft"/> or <see cref="ButtonsRight"/> collection. To determine when the standard dropdown is dropping down, use the <see cref="BeforeDropDown"/> event.</p>
		/// </remarks>
		/// <seealso cref="ButtonsLeft"/>
		/// <seealso cref="ButtonsRight"/>
		[LocalizedDescription("LD_UltraCombo_E_BeforeEditorButtonDropDown")]
		[LocalizedCategory("LC_Action")]
		public event BeforeEditorButtonDropDownEventHandler BeforeEditorButtonDropDown
		{
			add { this.EventsOptimized.AddHandler(EVENT_BEFOREEDITORBUTTONDROPDOWN, value); }
			remove {this.EventsOptimized.RemoveHandler(EVENT_BEFOREEDITORBUTTONDROPDOWN, value);}
		}
			#endregion //BeforeEditorButtonDropDown

			#region InitializeEditorButtonCheckState
		/// <summary>
		/// Invoked when the check state of a state editor button for an element should be initialized.
		/// </summary>
		/// <remarks>		
		/// <p class="body">Editor button events such as this one will fire any time the editor button is affected. This means that the event will fire, not only for the specific instance of the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo"/> but also for any object that is using the combo as an editor (an <see cref="UltraGridColumn"/>, for example). Use the <see cref="Infragistics.Win.UltraWinEditors.EditorButtonEventArgs.Context"/> property to determine the object the event applies to. This particular event has no meaning for the UltraCombo control itself. It applies only to cases where the Combo is used as en editor in a grid. In such a case, the state information of the <see cref="Infragistics.Win.UltraWinEditors.StateEditorButton"/> is cannot be maintained by the editor. The programmer is responsible for maintaining the value and initializing it by responding to this method.</p>
		/// </remarks>
		/// <seealso cref="ButtonsLeft"/>
		/// <seealso cref="ButtonsRight"/>		
		[LocalizedDescription("LD_UltraCombo_E_InitializeEditorButtonCheckState")]
		[LocalizedCategory("LC_Action")]
		public event InitializeCheckStateEventHandler InitializeEditorButtonCheckState
		{
			add { this.EventsOptimized.AddHandler(EVENT_INITIALIZEEDITORBUTTONCHECKSTATE, value); }
			remove {this.EventsOptimized.RemoveHandler(EVENT_INITIALIZEEDITORBUTTONCHECKSTATE, value);}
		}
			#endregion //InitializeEditorButtonCheckState

			#region BeforeEditorButtonCheckStateChanged
		/// <summary>
		/// Invoked before the CheckState of a <see cref="Infragistics.Win.UltraWinEditors.StateEditorButton"/> has been changed.
		/// </summary>
		/// <remarks>
		/// <p class="body">Editor button events such as this one will fire any time the editor button is affected. This means that the event will fire, not only for the specific instance of the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo"/> but also for any object that is using the combo as an editor (an <see cref="UltraGridColumn"/>, for example). Use the <see cref="Infragistics.Win.UltraWinEditors.EditorButtonEventArgs.Context"/> property to determine the object the event applies to.</p>
		/// </remarks>
		/// <seealso cref="ButtonsLeft"/>
		/// <seealso cref="ButtonsRight"/>
		[LocalizedDescription("LD_UltraCombo_E_BeforeEditorButtonCheckStateChanged")]
		[LocalizedCategory("LC_Action")]
		public event BeforeCheckStateChangedEventHandler BeforeEditorButtonCheckStateChanged
		{
			add { this.EventsOptimized.AddHandler(EVENT_BEFORECHECKSTATECHANGED, value); }
			remove {this.EventsOptimized.RemoveHandler(EVENT_BEFORECHECKSTATECHANGED, value);}
		}
			#endregion //BeforeEditorButtonCheckStateChanged

			#region AfterEditorButtonCheckStateChanged
		/// <summary>
		/// Invoked after the CheckState of a <see cref="Infragistics.Win.UltraWinEditors.StateEditorButton"/> has been changed.
		/// </summary>
		/// <remarks>
		/// <p class="body">Editor button events such as this one will fire any time the editor button is affected. This means that the event will fire, not only for the specific instance of the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo"/> but also for any object that is using the combo as an editor (an <see cref="UltraGridColumn"/>, for example). Use the <see cref="Infragistics.Win.UltraWinEditors.EditorButtonEventArgs.Context"/> property to determine the object the event applies to.</p>
		/// </remarks>
		/// <seealso cref="ButtonsLeft"/>
		/// <seealso cref="ButtonsRight"/>
		[LocalizedDescription("LD_UltraCombo_E_AfterEditorButtonCheckStateChanged")]
		[LocalizedCategory("LC_Action")]
		public event EditorButtonEventHandler AfterEditorButtonCheckStateChanged
		{
			add { this.EventsOptimized.AddHandler(EVENT_AFTERCHECKSTATECHANGED, value); }
			remove {this.EventsOptimized.RemoveHandler(EVENT_AFTERCHECKSTATECHANGED, value);}
		}
			#endregion //AfterEditorButtonCheckStateChanged

		#endregion // Editor Button Events

		#endregion //Event declarations

		#region Fire XXX Methods

		#region FireEvent
		internal void FireEvent( ComboEventIds id, EventArgs e )
		{
            
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( id ) )
				return;

			if ( !Enum.IsDefined( typeof( ComboEventIds ), id ) )
			{
				Debug.Fail( "Invalid event id in UltraCombo.FireEvent, id: " + ((int)id).ToString() );
				return;
			}

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( id );

			try
			{
				switch ( id )
				{
					case ComboEventIds.AfterDropDown:
						this.OnAfterDropDown( e );
						break;

					case ComboEventIds.AfterCloseUp:
						this.OnAfterCloseUp( e );
						break;

					case ComboEventIds.BeforeDropDown:
						this.OnBeforeDropDown( e as CancelEventArgs );
						break;

					case ComboEventIds.InitializeLayout:
						this.OnInitializeLayout( e as InitializeLayoutEventArgs );
						break;

					case ComboEventIds.InitializeRow:
						this.OnInitializeRow( e as InitializeRowEventArgs );
						break;

					case ComboEventIds.RowSelected:
						this.OnRowSelected( e as RowSelectedEventArgs );
						break;

					case ComboEventIds.ValueChanged:
						this.OnValueChanged( e );
						break;

					//JDN 11/9/04 LimitToList
					case ComboEventIds.ItemNotInList:
						this.OnItemNotInList( e as UltraWinEditors.ValidationErrorEventArgs );
						break;

					// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case ComboEventIds.FilterRow:
						this.OnFilterRow( e as FilterRowEventArgs );
						break;
					
					// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case ComboEventIds.AfterSortChange:
						this.OnAfterSortChange( e as BandEventArgs );
						break;

					// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case ComboEventIds.BeforeSortChange:
						this.OnBeforeSortChange( e as BeforeSortChangeEventArgs );
						break;

					// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case ComboEventIds.AfterColPosChanged:
						this.OnAfterColPosChanged( e as AfterColPosChangedEventArgs);
						break;

					// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case ComboEventIds.BeforeColPosChanged:
						this.OnBeforeColPosChanged( e as BeforeColPosChangedEventArgs );
						break;

					// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case ComboEventIds.BeforeRowFilterDropDown:
						this.OnBeforeRowFilterDropDown( e as BeforeRowFilterDropDownEventArgs );
						break;

					// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case ComboEventIds.BeforeRowFilterChanged:
						this.OnBeforeRowFilterChanged( e as BeforeRowFilterChangedEventArgs );
						break;

					// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case ComboEventIds.AfterRowFilterChanged:
						this.OnAfterRowFilterChanged( e as AfterRowFilterChangedEventArgs );
						break;

					// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case ComboEventIds.BeforeCustomRowFilterDialog:
						this.OnBeforeCustomRowFilterDialog( e as BeforeCustomRowFilterDialogEventArgs );
						break;

					// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case ComboEventIds.BeforeRowFilterDropDownPopulate:
						this.OnBeforeRowFilterDropDownPopulate( e as BeforeRowFilterDropDownPopulateEventArgs );
						break;

					// JAS 12/21/04 ButtonsLeft & ButtonsRight 
					case ComboEventIds.InitializeEditorButtonCheckState:
						this.OnInitializeEditorButtonCheckState( e as InitializeCheckStateEventArgs );
						break;

					// JAS 12/21/04 ButtonsLeft & ButtonsRight 
					case ComboEventIds.EditorButtonClick:
						this.OnEditorButtonClick( e as EditorButtonEventArgs );
						break;

					// JAS 12/21/04 ButtonsLeft & ButtonsRight 
					case ComboEventIds.BeforeEditorButtonDropDown:
						this.OnBeforeEditorButtonDropDown( e as BeforeEditorButtonDropDownEventArgs );
						break;

					// JAS 12/21/04 ButtonsLeft & ButtonsRight 
					case ComboEventIds.AfterEditorButtonCloseUp:
						this.OnAfterEditorButtonCloseUp( e as EditorButtonEventArgs );
						break;

					// JAS 12/21/04 ButtonsLeft & ButtonsRight 
					case ComboEventIds.EditorSpinButtonClick:
						this.OnEditorSpinButtonClick( e as SpinButtonClickEventArgs );
						break;

					// JAS 12/21/04 ButtonsLeft & ButtonsRight 
					case ComboEventIds.BeforeEditorButtonCheckStateChanged:
						this.OnBeforeEditorButtonCheckStateChanged( e as BeforeCheckStateChangedEventArgs );
						break;

					// JAS 12/21/04 ButtonsLeft & ButtonsRight 
					case ComboEventIds.AfterEditorButtonCheckStateChanged:
						this.OnAfterEditorButtonCheckStateChanged( e as EditorButtonEventArgs );
						break;
					// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					//
					// --------------------------------------------------------------------------------
					case ComboEventIds.FilterCellValueChanged:
						this.OnFilterCellValueChanged( e as FilterCellValueChangedEventArgs );
						break;
					case ComboEventIds.InitializeRowsCollection:
						this.OnInitializeRowsCollection( e as InitializeRowsCollectionEventArgs );
						break;
					// SSP 6/17/05 - NAS 5.3 Column Chooser
					//
					case ComboEventIds.BeforeColumnChooserDisplayed:
						this.OnBeforeColumnChooserDisplayed( e as BeforeColumnChooserDisplayedEventArgs );
						break;
					// --------------------------------------------------------------------------------
						// SSP 10/18/05 - NAS 5.3 Column Chooser
						// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
						// TestAdvantage.
						// 
						// ----------------------------------------------------------------------------
					case ComboEventIds.BeforeBandHiddenChanged:
						this.OnBeforeBandHiddenChanged( e as BeforeBandHiddenChangedEventArgs );
						break;
					case ComboEventIds.AfterBandHiddenChanged:
						this.OnAfterBandHiddenChanged( e as AfterBandHiddenChangedEventArgs );
						break;
						// ----------------------------------------------------------------------------

                    // CDS 02/02/09 TFS12512 - NAS v9.1 Header CheckBox - Added UltraCombo support
                    case ComboEventIds.BeforeHeaderCheckStateChanged:
                        this.OnBeforeHeaderCheckBoxStateChanged(e as BeforeHeaderCheckStateChangedEventArgs);
                        break;

                    // CDS 02/02/09 TFS12512 - NAS v9.1 Header CheckBox - Added UltraCombo support
                    case ComboEventIds.AfterHeaderCheckStateChanged:
                        this.OnAfterHeaderCheckBoxStateChanged(e as AfterHeaderCheckStateChangedEventArgs);
                        break;

                    default:
						Debug.Assert(false, "Unhandled Id passed into UltraCombo.FireEvent");
						break;
				}
			}
			finally
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( id );
			}
		}
		#endregion //FireEvent

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		#region FireCommonEvent
		internal override bool FireCommonEvent( CommonEventIds id, EventArgs e, bool checkInProgress )
		{
			ComboEventIds resolvedEventID;
			
			switch (id)
			{
				case CommonEventIds.AfterSortChange:
					resolvedEventID = ComboEventIds.AfterSortChange;
					break;
				case CommonEventIds.BeforeSortChange:
					resolvedEventID = ComboEventIds.BeforeSortChange;
					break;
				case CommonEventIds.FilterRow:
					resolvedEventID = ComboEventIds.FilterRow;
					break;
				case CommonEventIds.BeforeColPosChanged:
					resolvedEventID = ComboEventIds.BeforeColPosChanged;
					break;
				case CommonEventIds.AfterColPosChanged:
					resolvedEventID = ComboEventIds.AfterColPosChanged;
					break;
				case CommonEventIds.BeforeRowFilterDropDown:
					resolvedEventID = ComboEventIds.BeforeRowFilterDropDown;
					break;
				case CommonEventIds.AfterRowFilterChanged:
					resolvedEventID = ComboEventIds.AfterRowFilterChanged;
					break;
				case CommonEventIds.BeforeRowFilterChanged:
					resolvedEventID = ComboEventIds.BeforeRowFilterChanged;
					break;
				case CommonEventIds.BeforeCustomRowFilterDialog:
					resolvedEventID = ComboEventIds.BeforeCustomRowFilterDialog;
					break;
				case CommonEventIds.BeforeRowFilterDropDownPopulate:
					resolvedEventID = ComboEventIds.BeforeRowFilterDropDownPopulate;
					break;
				// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				// Added FilterCellValueChanged and InitializeRowsCollection events.
				//
				// ----------------------------------------------------------------------------
				case CommonEventIds.FilterCellValueChanged:
					resolvedEventID = ComboEventIds.FilterCellValueChanged;
					break;
				case CommonEventIds.InitializeRowsCollection:
					resolvedEventID = ComboEventIds.InitializeRowsCollection;
					break;
				// ----------------------------------------------------------------------------
					// SSP 6/17/05 - NAS 5.3 Column Chooser
					//
				case CommonEventIds.BeforeColumnChooserDisplayed:
					resolvedEventID = ComboEventIds.BeforeColumnChooserDisplayed;
					break;
					// SSP 10/18/05 - NAS 5.3 Column Chooser
					// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
					// TestAdvantage.
					// 
					// ----------------------------------------------------------------------------
				case CommonEventIds.BeforeBandHiddenChanged:
					resolvedEventID = ComboEventIds.BeforeBandHiddenChanged;
					break;
				case CommonEventIds.AfterBandHiddenChanged:
					resolvedEventID = ComboEventIds.AfterBandHiddenChanged;
					break;
					// ----------------------------------------------------------------------------
                // CDS 02/02/09 TFS12512 - NAS v9.1 Header CheckBox - Added UltraCombo support
                case CommonEventIds.BeforeHeaderCheckStateChanged:
                    resolvedEventID = ComboEventIds.BeforeHeaderCheckStateChanged;
                    break;
                case CommonEventIds.AfterHeaderCheckStateChanged:
                    resolvedEventID = ComboEventIds.AfterHeaderCheckStateChanged;
                    break;
                default:
					Debug.Assert(false, "Unhandled Id passed into UltraCombo.FireCommonEvent");
					return false;			
			}

			if (checkInProgress && 
				this.EventManager.InProgress(resolvedEventID))
			{
				return false;
			}

			this.FireEvent(resolvedEventID, e);
			return true;
		}
		#endregion FireCommonEvent

        #region FireAfterCloseUp
        /// <summary>
        /// Called after list is closed up
        /// </summary>
        protected override void FireAfterCloseUp()
        {
            // MRS 11/2/06 - BR17309
            // We should not be checking for listeners here. This check is already done
            // in the appropriate OnXXX method.
            //EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERCLOSEUP];

            //if ( eDelegate != null )
            //    this.FireEvent(	ComboEventIds.AfterCloseUp, new EventArgs() );

            this.FireEvent(ComboEventIds.AfterCloseUp, new EventArgs());
        }
        #endregion //FireAfterCloseUp

        #region FireAfterDropDown
        /// <summary>
        /// Called after list is dropped down
        /// </summary>
        protected override void FireAfterDropDown()
        {
            // MRS 11/2/06 - BR17309
            // We should not be checking for listeners here. This check is already done
            // in the appropriate OnXXX method.
            //EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERDROPDOWN];

            //if ( eDelegate != null )
            //    this.FireEvent(	ComboEventIds.AfterDropDown, new EventArgs() );

            this.FireEvent(ComboEventIds.AfterDropDown, new EventArgs());
        }
        #endregion //FireAfterDropDown

        #region FireBeforeDropDown
        /// <summary>
        /// Called after list is dropped down
        /// </summary>
        /// <returns>True if cancelled.</returns>
        protected override bool FireBeforeDropDown()
        {
            // MRS 11/2/06 - BR17309
            // We should not be checking for listeners here. This check is already done
            // in the appropriate OnXXX method.
            //CancelEventHandler eDelegate = (CancelEventHandler)EventsOptimized[EVENT_BEFOREDROPDOWN];

            //if ( eDelegate != null )
            //{
            CancelEventArgs e = new CancelEventArgs();

            // fire the event
            //
            this.FireEvent(ComboEventIds.BeforeDropDown, e);


            bool cancelled = e.Cancel;
            //}

            // MRS 2/21/07 - This code was accidently deleted along with the fix for BR17309, 
            // So I put it back in
            //
            // SSP 10/11/06 - NAS 6.3
            // Added SetInitialValue on the UltraCombo.
            // 
            // ------------------------------------------------------
            if (!cancelled)
            {
                if (this.dontLoadDataSource)
                {
                    this.dontLoadDataSource = false;
                    if (null != this.DisplayLayout && !this.DisplayLayout.Disposed)
                        this.DisplayLayout.EnsureDataSourceAttached();
                }
            }
            // ------------------------------------------------------

            return cancelled;
        }
        #endregion //FireBeforeDropDown

		#region FireInitializeLayout
		/// <summary>
		/// Called when the layout is first initialized after the 
		/// datasource has been set 
		/// </summary>
		internal protected override void FireInitializeLayout( InitializeLayoutEventArgs e )
		{
         
			// JJD 12/03/01 - UWG813
			// Call base to maintain flag so we know if event was fired
			//
			base.FireInitializeLayout( e );

			// SSP 9/9/02 UWG1648
			// We want to call FireEvent even if no one has hooked into the event because
			// the FireEvent method actually calls the OnInitlaizeLayout method which we
			// need to call even if the user has not hooked into the InitializeLayout event.
			// In case someone derives a control from UltraCombo, he would expect 
			// OnInitializeLayout to get called even if no one has hooked into it.
			// This is what we seem to be doing in the ultra grid.
			//
			// -----------------------------------------------------------------------------
			
			this.FireEvent( ComboEventIds.InitializeLayout, e );
			// -----------------------------------------------------------------------------
		}
		#endregion //FireInitializeLayout

        #region FireRowSelected
        /// <summary>
        /// Called when a new row has be selected
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        protected override void FireRowSelected(RowSelectedEventArgs e)
        {
            // MRS 11/2/06 - BR17309
            // We should not be checking for listeners here. This check is already done
            // in the appropriate OnXXX method.
            //RowSelectedEventHandler eDelegate = (RowSelectedEventHandler)EventsOptimized[EVENT_ROWSELECTED];

            //if ( eDelegate != null )
            //    this.FireEvent( ComboEventIds.RowSelected, e );

            this.FireEvent(ComboEventIds.RowSelected, e);
        }
        #endregion //FireRowSelected

        #region FireInitializeRow
        /// <summary>
        /// Called when a row is initialized
        /// </summary>
        internal protected override void FireInitializeRow(InitializeRowEventArgs e)
        {
            // MRS 11/2/06 - BR17309
            // We should not be checking for listeners here. This check is already done
            // in the appropriate OnXXX method.
            //InitializeRowEventHandler eDelegate = (InitializeRowEventHandler)EventsOptimized[EVENT_INTIALIZEROW];

            //if ( eDelegate != null )
            //    this.FireEvent( ComboEventIds.InitializeRow, e );

            this.FireEvent(ComboEventIds.InitializeRow, e);
        }
        #endregion //FireInitializeRow

        //JDN 11/9/04 LimitToList
        #region FireItemNotInList
        /// <summary>
        /// Called before the Validating event of the control whenever the text value entered
        /// into the editor portion of the control is not a value in the control�s valuelist.
        /// </summary>
        internal protected virtual void FireItemNotInList(Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e)
        {
            // MRS 11/2/06 - BR17309
            // We should not be checking for listeners here. This check is already done
            // in the appropriate OnXXX method.
            //ItemNotInListEventHandler eDelegate = (ItemNotInListEventHandler)EventsOptimized[EVENT_ITEMNOTINLIST];

            //if ( eDelegate != null )
            //    this.FireEvent( ComboEventIds.ItemNotInList, e );
            this.FireEvent(ComboEventIds.ItemNotInList, e);
        }
        #endregion // FireItemNotInList

		#endregion //Fire XXX Methods

		#region OnXXX Methods

		#region OnMouse Enter/Leave Element
		/// <summary>
		/// Called when an element is entered (the mouse is moved
		/// over the element)
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnMouseEnterElement( UIElementEventArgs e )
		{
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( ComboEventIds.MouseEnterElement ) )
				return;

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( ComboEventIds.MouseEnterElement );

			try
			{
				// fire the event
				//
				base.OnMouseEnterElement( e );
			}
			catch(Exception)
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( ComboEventIds.MouseEnterElement );

				// re-throw the error
				//
				throw;
			}

			// decrement the inProgress count
			//
			this.EventManager.DecrementInProgress( ComboEventIds.MouseEnterElement );
		}

		/// <summary>
		/// Called when an element is left (the mouse is moved
		/// off the element)
		/// </summary>
		protected override void OnMouseLeaveElement( UIElementEventArgs e )
		{
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( ComboEventIds.MouseLeaveElement ) )
				return;

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( ComboEventIds.MouseLeaveElement );

			try
			{
				// fire the event
				//
				base.OnMouseLeaveElement( e );
			}
			catch(Exception)
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( ComboEventIds.MouseLeaveElement );

				// re-throw the error
				//
				throw;
			}

			// decrement the inProgress count
			//
			this.EventManager.DecrementInProgress( ComboEventIds.MouseLeaveElement );
		}
		#endregion //OnMouse Enter/Leave Element

		#region OnValueChanged
		/// <summary>
		/// Called when the Value property changes
		/// </summary>
		protected virtual void OnValueChanged( EventArgs e )
		{
			EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_VALUECHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnValueChanged

		#region OnInitializeLayout
		/// <summary>
		/// Called when the layout is first initialized after the 
		/// datasource has been set 
		/// </summary>
		protected virtual void OnInitializeLayout( InitializeLayoutEventArgs e )
		{
			InitializeLayoutEventHandler eDelegate = (InitializeLayoutEventHandler)EventsOptimized[EVENT_INTIALIZELAYOUT];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion //OnInitializeLayout

		#region OnAfterDropDown
		/// <summary>
		/// Called after list is dropped down
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnAfterDropDown( EventArgs e )
		{
			EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERDROPDOWN];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion //OnAfterDropDown

		#region OnAfterCloseUp
		/// <summary>
		/// Called after the list has closed
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnAfterCloseUp( EventArgs e )
		{
			EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERCLOSEUP];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion //OnAfterCloseUp

		#region OnBeforeDropDown
		/// <summary>
		/// Called before the list is dropped down
		/// </summary>
		protected virtual void OnBeforeDropDown( CancelEventArgs e )
		{
			CancelEventHandler eDelegate = (CancelEventHandler)EventsOptimized[EVENT_BEFOREDROPDOWN];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion //OnBeforeDropDown

		#region OnRowSelected
		/// <summary>
		/// Called when a new row has be selected
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnRowSelected( RowSelectedEventArgs e )
		{
			RowSelectedEventHandler eDelegate = (RowSelectedEventHandler)EventsOptimized[EVENT_ROWSELECTED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion //OnRowSelected

		#region OnInitializeRow
		/// <summary>
		/// Called when a row is initialized
		/// </summary>
		protected virtual void OnInitializeRow( InitializeRowEventArgs e )
		{
			InitializeRowEventHandler eDelegate = (InitializeRowEventHandler)EventsOptimized[EVENT_INTIALIZEROW];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion //OnInitializeRow

		//JDN 11/9/04 LimitToList
		#region OnItemNotInList
		/// <summary>
		/// Called before the Validating event of the control whenever the text value entered
		/// into the editor portion of the control is not a value in the control�s valuelist.
		/// </summary>
		protected virtual void OnItemNotInList( Infragistics.Win.UltraWinEditors.ValidationErrorEventArgs e )
		{
			ItemNotInListEventHandler eDelegate = (ItemNotInListEventHandler)EventsOptimized[EVENT_ITEMNOTINLIST];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion // OnItemNotInList

		// JAS 12/21/04 ButtonsLeft & ButtonsRight
		#region Onxxx Editor Button Event methods

			#region Editor Button Event Sinks

		private void internalOnInitializeCheckState(object sender, InitializeCheckStateEventArgs e)
		{
			this.FireEvent( ComboEventIds.InitializeEditorButtonCheckState, e );
		}

		private void internalOnEditorButtonClick(object sender, EditorButtonEventArgs e)
		{
			this.FireEvent( ComboEventIds.EditorButtonClick, e );
		}

		private void internalOnBeforeEditorButtonDropDown(object sender, BeforeEditorButtonDropDownEventArgs e)
		{
			this.FireEvent( ComboEventIds.BeforeEditorButtonDropDown, e );
		}

		private void internalOnAfterEditorButtonCloseUp(object sender, EditorButtonEventArgs e)
		{
			this.FireEvent( ComboEventIds.AfterEditorButtonCloseUp, e );
		}

		private void internalOnEditorButtonSpinClick(object sender, SpinButtonClickEventArgs e)
		{
			this.FireEvent( ComboEventIds.EditorSpinButtonClick, e );
		}

		private void internalOnBeforeEditorButtonCheckStateChanged(object sender, BeforeCheckStateChangedEventArgs e)
		{
			this.FireEvent( ComboEventIds.BeforeEditorButtonCheckStateChanged, e );
		}

		private void internalOnAfterEditorButtonCheckStateChanged(object sender, EditorButtonEventArgs e)
		{
			this.FireEvent( ComboEventIds.AfterEditorButtonCheckStateChanged, e );
		}

		private void internalOnEditorButtonAdded(object sender, EditorButtonEventArgs e)
		{
			// Transfer the layout's Appearances so that the button can use images from the
			// control's Imagelist at design-time.
			e.Button.Appearances = this.DisplayLayout.Appearances;
		}

				#endregion // Editor Button Event Sinks

			#region OnBeforeEditorButtonCheckStateChanged

		/// <summary>
		/// Used to invoke the <see cref="BeforeEditorButtonCheckStateChanged"/> event
		/// </summary>
		/// <param name="e">Event arguments</param>
		protected virtual void OnBeforeEditorButtonCheckStateChanged(BeforeCheckStateChangedEventArgs e)
		{
			BeforeCheckStateChangedEventHandler handler = (BeforeCheckStateChangedEventHandler)EventsOptimized[EVENT_BEFORECHECKSTATECHANGED];

			if ( handler != null ) 
				handler(this, e);
		}

			#endregion // OnBeforeEditorButtonCheckStateChanged

			#region OnAfterEditorButtonCheckStateChanged

		/// <summary>
		/// Used to invoke the <see cref="AfterEditorButtonCheckStateChanged"/> event
		/// </summary>
		/// <param name="e">Event arguments</param>
		protected virtual void OnAfterEditorButtonCheckStateChanged(EditorButtonEventArgs e)
		{
			EditorButtonEventHandler handler = (EditorButtonEventHandler)EventsOptimized[EVENT_AFTERCHECKSTATECHANGED];

			if ( handler != null ) 
				handler(this, e);
		}

			#endregion // OnAfterEditorButtonCheckStateChanged

			#region OnEditorButtonClick

		/// <summary>
		/// Used to invoke the <see cref="EditorButtonClick"/> event
		/// </summary>
		/// <param name="e">Event arguments</param>
		protected virtual void OnEditorButtonClick(EditorButtonEventArgs e)
		{
			EditorButtonEventHandler handler = (EditorButtonEventHandler)EventsOptimized[EVENT_EDITORBUTTONCLICK];

			if ( handler != null ) 
				handler(this, e);
		}

			#endregion // OnEditorButtonClick

			#region OnInitializeEditorButtonCheckState

		/// <summary>
		/// Used to invoke the <see cref="InitializeEditorButtonCheckState"/> event
		/// </summary>
		/// <param name="e">Event arguments</param>
		protected virtual void OnInitializeEditorButtonCheckState(InitializeCheckStateEventArgs e)
		{
			InitializeCheckStateEventHandler handler = (InitializeCheckStateEventHandler)EventsOptimized[EVENT_INITIALIZEEDITORBUTTONCHECKSTATE];

			if ( handler != null ) 
				handler(this, e);
		}

			#endregion // OnInitializeEditorButtonCheckState

			#region OnEditorSpinButtonClick

		/// <summary>
		/// Used to invoke the <see cref="EditorSpinButtonClick"/> event
		/// </summary>
		/// <param name="e">Event arguments</param>
		protected virtual void OnEditorSpinButtonClick(SpinButtonClickEventArgs e)
		{
			SpinButtonClickEventHandler handler = (SpinButtonClickEventHandler)EventsOptimized[EVENT_EDITORSPINBUTTONCLICK];

			if ( handler != null ) 
				handler(this, e);
		}

			#endregion // OnEditorSpinButtonClick

			#region OnBeforeEditorButtonDropDown

		/// <summary>
		/// Used to invoke the <see cref="BeforeEditorButtonDropDown"/> event
		/// </summary>
		/// <param name="e">Event arguments</param>
		protected virtual void OnBeforeEditorButtonDropDown(BeforeEditorButtonDropDownEventArgs e)
		{
			BeforeEditorButtonDropDownEventHandler handler = (BeforeEditorButtonDropDownEventHandler)EventsOptimized[EVENT_BEFOREEDITORBUTTONDROPDOWN];

			if ( handler != null ) 
				handler(this, e);
		}

			#endregion // OnBeforeEditorButtonDropDown

			#region OnAfterEditorButtonCloseUp

		/// <summary>
		/// Used to invoke the <see cref="AfterEditorButtonCloseUp"/> event
		/// </summary>
		/// <param name="e">Event arguments</param>
		protected virtual void OnAfterEditorButtonCloseUp(EditorButtonEventArgs e)
		{
			EditorButtonEventHandler handler = (EditorButtonEventHandler)EventsOptimized[EVENT_AFTEREDITORBUTTONCLOSEUP];

			if ( handler != null ) 
				handler(this, e);
		}

			#endregion // OnAfterEditorButtonCloseUp

			#region OnEditorPropertyChanged
		/// <summary>
		/// Invoked when a property on the editor has changed.
		/// </summary>
        /// <param name="propChangeInfo">A structure containing the property change information.</param>
		private void OnEditorPropertyChanged(Infragistics.Shared.PropChangeInfo propChangeInfo)
		{
			Infragistics.Shared.PropChangeInfo pci = propChangeInfo.FindTrigger(null);

			if (pci.PropId is EditorButtonPropertyId)
			{
				if (this.ControlUIElement != null)
					this.ControlUIElement.DirtyChildElements();
			}

			// JAS 3/10/05 BR02657 - Need to send property change notifications if buttons were added
			// to the ButtonsLeft or ButtonsRight collection.
			//
			PropChangeInfo pciTemp = propChangeInfo.Trigger; 

			if (pciTemp != null)
			{
				if (pciTemp.Source == this.InternalEditor.ButtonsLeft)
				{
					this.NotifyPropChange(EditorButtonPropertyId.ButtonsLeft, pciTemp);
				}
				else if (pciTemp.Source == this.InternalEditor.ButtonsRight)
				{
					this.NotifyPropChange(EditorButtonPropertyId.ButtonsRight, pciTemp);
				}
			}

		}
			#endregion //OnEditorPropertyChanged

		#endregion //Onxxx Editor Button Event methods

		#endregion //OnXXX Methods

		#endregion //Events
		
		#region Interfaces

		#region About Dialog and Licensing Interface

		/// <summary>
		/// Displays the about dialog
		/// </summary>
		/// <remarks>
		/// <p class="body">The About property is a design-time only property which exists to provide a way to show the About dialog.. The property has no real value, but it will display in the property grid in Visual Studio and show an ellipsis button which will show the About dialog when clicked.</p>
		/// </remarks>
		[ DesignOnly( true ) ]
		[ LocalizedDescription("LD_UltraCombo_P_About") ]
		[ LocalizedCategory("LC_Design") ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ ParenthesizePropertyName( true ) ]
		[ Editor(typeof(AboutDialogEditor), typeof(System.Drawing.Design.UITypeEditor) ) ]
		public object About { get { return null; } }

		/// <summary>
		/// Return the license we cached inside the constructor
		/// </summary>
		UltraLicense IUltraLicensedComponent.License { get { return this.license; } }
	
		#endregion

		#region IValueListOwner
		
		 // AS 11/11/03 [UltraCombo as Editor]
		#endregion // IValueListOwner

		#endregion //Interfaces

		#region DropDownList_AutoEditHelper

		// SPS 8/1/02 UWG1454
		// When the drop down style is drop down list, we don't show the text box.
		// So we have to find the matching item in the items collection for the any 
		// characters typed and make that the selected item.
		//
		// SSP 7/30/03 UWG2307
		// Added support for multi-character auto-edit in DropDownList style.
		//
		
		#endregion // DropDownList_AutoEditHelper

		#region TextBox related
		 // AS 11/11/03 [UltraCombo as Editor]
		#endregion //TextBox related

		// AS 11/11/03 [UltraCombo as Editor]
		// The editor owner needs to be able to check if the key is mapped.
		// 
		#region InternalIsInputKey
		internal bool InternalIsInputKey( Keys keyData )
		{
			return this.IsInputKey( keyData );
		}
		#endregion //InternalIsInputKey

		#region Editor related properties

		private UltraGridComboEditor		internalEditor = null;
		private UltraGridComboEditor		externalEditor = null;
		private UltraGridComboEditorOwner	editorOwner = null;

		internal UltraGridComboEditorOwner EditorOwner
		{
			get
			{
				if (this.editorOwner == null && !this.IsDisposed && !this.Disposing)
					this.editorOwner = new UltraGridComboEditorOwner(this);

				return this.editorOwner;
			}
		}

		internal UltraGridComboEditor InternalEditor
		{
			get
			{
				if (this.internalEditor == null && !this.IsDisposed && !this.Disposing)
				{
					// JAS 3/4/05 BR00644 - Added the 'false' argument so that the editor knows that it is
					// an 'internal' editor.  This is necessary because if the UltraGridComboEditor
					// is being used as an external editor in the grid, it needs to know that it is
					// being used externally.  It needs to know this because it has to reset its
					// selected index when exiting edit mode ONLY IF it is an external editor.
					//
					this.internalEditor = new UltraGridComboEditor(this, false);

					// catch the key events of the editor
					this.internalEditor.KeyDown += new KeyEventHandler(this.OnEditorKeyDown);
					this.internalEditor.KeyPress += new KeyPressEventHandler(this.OnEditorKeyPress);
					this.internalEditor.KeyUp += new KeyEventHandler(this.OnEditorKeyUp);

					this.internalEditor.ValueChanged += new EventHandler(this.OnEditorValueChanged);

					// JAS 12/21/04 ButtonsLeft & ButtonsRight
					this.AttachHandlersToEditorButtonEvents( internalEditor, true );

					// AS 1/16/04
					this.internalEditor.AfterEnterEditMode += new EventHandler(this.OnAfterEditorEnterEditMode);
					this.internalEditor.AfterExitEditMode += new EventHandler(this.OnAfterEditorExitEditMode);

					// AS 5/21/04 UWG3238(aka UWE925) - See UWE917
					this.internalEditor.BeforeExitEditMode += new Infragistics.Win.BeforeExitEditModeEventHandler(this.OnBeforeEditorExitEditMode);
				}

				return this.internalEditor;
			}
		}

		internal UltraGridComboEditor ExternalEditor
		{
			get
			{
				if (this.externalEditor == null && !this.IsDisposed && !this.Disposing)
				{
					this.externalEditor = (UltraGridComboEditor)this.InternalEditor.Clone(this.EditorOwner);
					// JAS 12/21/04 ButtonsLeft & ButtonsRight
					this.AttachHandlersToEditorButtonEvents( this.externalEditor, false );
				}

				return this.externalEditor;
			}
		}

			// JAS 12/21/04 ButtonsLeft & ButtonsRight
			#region AttachHandlersToEditorButtonEvents
		
		private void AttachHandlersToEditorButtonEvents( UltraGridComboEditor editor, bool isInternalEditor )
		{
			if( editor == null || editor.Disposed )
				return;

			if( isInternalEditor )
				editor.SubObjectPropChanged				+= new SubObjectPropChangeEventHandler(			this.OnEditorPropertyChanged						);
			editor.AfterEditorButtonCloseUp				+= new EditorButtonEventHandler(				this.internalOnAfterEditorButtonCloseUp				);
			editor.BeforeEditorButtonDropDown			+= new BeforeEditorButtonDropDownEventHandler(	this.internalOnBeforeEditorButtonDropDown			);
			editor.EditorButtonClick					+= new EditorButtonEventHandler(				this.internalOnEditorButtonClick					);
			editor.EditorSpinButtonClick				+= new SpinButtonClickEventHandler(				this.internalOnEditorButtonSpinClick				);
			editor.InitializeEditorButtonCheckState		+= new InitializeCheckStateEventHandler(		this.internalOnInitializeCheckState	 				);
			editor.BeforeEditorButtonCheckStateChanged	+= new BeforeCheckStateChangedEventHandler(		this.internalOnBeforeEditorButtonCheckStateChanged	);
			editor.AfterEditorButtonCheckStateChanged	+= new EditorButtonEventHandler(				this.internalOnAfterEditorButtonCheckStateChanged	);
			editor.EditorButtonAdded					+= new EditorButtonEventHandler(				this.internalOnEditorButtonAdded					);
		}

			#endregion // AttachHandlersToEditorButtonEvents

			// JAS 12/21/04 ButtonsLeft & ButtonsRight
			#region DetachHandlersFromEditorButtonEvents

		private void DetachHandlersFromEditorButtonEvents( UltraGridComboEditor editor, bool isInternalEditor )
		{
			if( editor == null || editor.Disposed )
				return;

			if( isInternalEditor )
				editor.SubObjectPropChanged				-= new SubObjectPropChangeEventHandler(			this.OnEditorPropertyChanged						);
			editor.AfterEditorButtonCloseUp				-= new EditorButtonEventHandler(				this.internalOnAfterEditorButtonCloseUp				);
			editor.BeforeEditorButtonDropDown			-= new BeforeEditorButtonDropDownEventHandler(	this.internalOnBeforeEditorButtonDropDown			);
			editor.EditorButtonClick					-= new EditorButtonEventHandler(				this.internalOnEditorButtonClick					);
			editor.EditorSpinButtonClick				-= new SpinButtonClickEventHandler(				this.internalOnEditorButtonSpinClick				);
			editor.InitializeEditorButtonCheckState		-= new InitializeCheckStateEventHandler(		this.internalOnInitializeCheckState	 				);
			editor.BeforeEditorButtonCheckStateChanged	-= new BeforeCheckStateChangedEventHandler(		this.internalOnBeforeEditorButtonCheckStateChanged	);
			editor.AfterEditorButtonCheckStateChanged	-= new EditorButtonEventHandler(				this.internalOnAfterEditorButtonCheckStateChanged	);
			editor.EditorButtonAdded					-= new EditorButtonEventHandler(				this.internalOnEditorButtonAdded					);
		}

			#endregion // DetachHandlersFromEditorButtonEvents

		#endregion //Editor related properties

		#region IProvidesEmbeddableEditor
		EmbeddableEditorBase IProvidesEmbeddableEditor.Editor
		{
			get { return this.ExternalEditor; }
		}
		#endregion //IProvidesEmbeddableEditor

		#region SafeDirtyChildElements
		internal void SafeDirtyChildElements()
		{
			if ( this.element != null )
				this.element.DirtyChildElements( true );
		}
		#endregion //SafeDirtyChildElements

		// AS 11/11/03 [UltraCombo as Editor]
		// Methods used to catch the editor key events
		//
		#region Editor key event sinks
		internal void OnEditorKeyDown( object sender, KeyEventArgs args )
		{
			//	BF 7.2.02
			//	Moved up to here from below...we should always call the base
			//	class implementation first, and return if it is marked handled
			//
			//	BF 6.5.02
			//	Call the control's OnKeyDown method so the KeyDown event fires, and also
			//	so we pass the event arguments back to the EmbeddableTextBox
			this.OnKeyDown( args );

			if ( args.Handled )
				return;

			//	BF 6.12.02
			//	We have to be careful about screening bit values here, since there is
			//	overrlapping of KeyData values (i.e., the Keys.Tab bit is also set in other keys,
			//	so the expression, "(KeyData & Keys.Tab)" will return true for the ENTER key as well)
			bool isTabKey = false;

			if ( args.KeyData == Keys.Tab || 
				args.KeyData == ( Keys.Tab | Keys.Shift ) )
			{
				isTabKey = true;

				//	If we are in edit mode, exit now
				//	BF 7.30.02	UWE109
				if ( this.InternalEditor.IsInEditMode )
					//if ( this.InternalEditor.IsInEditMode && ! this.alwaysInEditMode )
					this.InternalEditor.ExitEditMode( true, true );
				
				//	If the SHIFT key is down, we will navigate backwards
				bool forward = true;

				// AS 11/11/03 [UltraCombo as Editor]
				// Why did we check the modifier keys?!?
				//
				//if ( ((Control.ModifierKeys & Keys.Shift) == Keys.Shift) )
				if ( ((args.KeyData & Keys.Shift) == Keys.Shift) )
					forward = false;

				// AS 11/11/03 [UltraCombo as Editor]
				//bool origDontGiveFocusToTextBox = combo.dontEnterEditModeOnFocus;
				bool origDontGiveFocusToTextBox = this.dontGiveFocusToTextBox;

				//	BF 12.9.02	UWE226 (and related)
				//
				//	When we are on a UserControl, don't use SetFocusToNextControl,
				//	as UserControls do not seem to react correctly to the SelectNextControl
				//	method. Instead we set focus to ourself (after setting a flag so that
				//	we don't go right back into edit mode), and then call ProcessDialogKey,
				//	which seems to handle this scenario correctly.
				//
				try
				{
					//	BF 7.28.03	UWE671
					//if ( this.Parent is UserControl )
					if ( IsOnUserControl( this ) )
					{
						//this.dontEnterEditModeOnFocus = true;
						this.dontGiveFocusToTextBox = true;

						if ( ! this.Focus() )
							return;

						args.Handled = this.ProcessDialogKey( args.KeyData );

						// AS 1/7/05 BR01253
						// In theory, we should not have focus at this point
						// if the tab key was processed but if we do, then
						// be sure to put the editor back into edit mode.
						//
						// MRS 7/6/05 - BR04729
						//if (this.Focused)
						if (this.IsFocused)
							this.DelayedInvokeEditorFocus();

						return;
					}
				}
				finally
				{
					//this.dontEnterEditModeOnFocus = false;
					this.dontGiveFocusToTextBox = origDontGiveFocusToTextBox;
				}

				//	BF 9.3.02	UWE226
				//this.Parent.SelectNextControl( this, forward, true, false, true );
				//
				//	BF 9.4.02	UWE226 (revisited)
				this.SetFocusToNextControl( forward );

			}

			//	If it was the TAB key that was pressed, we should return now
			//	so the Editor does not try to process the key
			if ( isTabKey )
				return;

			//	Delegate the keyboard functionality to the editor
			IEmbeddableTextBoxListener textBox = this.InternalEditor as IEmbeddableTextBoxListener;
			textBox.OnKeyDown( args );
		}

		internal void OnEditorKeyPress( object sender, KeyPressEventArgs args )
		{
			//	Just pass it along
			this.OnKeyPress( args );
		}

		internal void OnEditorKeyUp( object sender, KeyEventArgs args )
		{
			//	Just pass it along
			this.OnKeyUp( args );
		}
		#endregion // Editor key event sinks

		// AS 11/11/03 [UltraCombo as Editor]
		// This is from the editors assembly.
		//
		#region IsOnUserControl
		static internal bool IsOnUserControl( Control control )
		{
			if ( control == null )
				return false;

			try
			{
				System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission(System.Security.Permissions.UIPermissionWindow.AllWindows);

				perm.Assert();

				Control parentControl = control.Parent;
				while ( parentControl != null )
				{
					if ( parentControl is UserControl )
						return true;

					parentControl = parentControl.Parent;
				}
			}
			catch
			{
				return false;
			}

			return false;
		}
		#endregion IsOnUserControl

		#region OnEditorValueChanged
		internal void OnEditorValueChanged( object sender, EventArgs e )
		{
            //  BF NA 9.1 - UltraCombo MultiSelect
            //  When the value changes on this control's InternalEditor,
            //  fire the control's ValueChanged event.
            if ( this.CheckStateColumn != null )
            {
                UltraGridComboEditor editor = sender as UltraGridComboEditor;
                if ( editor != null )
                {
                    CheckedListSettings checkedListSettings = this.GetCheckedListSettings( editor ) as CheckedListSettings;
                    if ( checkedListSettings != null && checkedListSettings.EditorValueSource  == EditorWithComboValueSource.CheckedItems )
                    {
                        this.FireEvent(ComboEventIds.ValueChanged, EventArgs.Empty);
                        return;
                    }
                }
            }

			// AS 11/11/03 [UltraCombo as Editor]
			// Update the text to match the value.
			//
			this.InitializeText( this.InternalEditor.CurrentEditText, false );

			// AS 11/11/03 
			// Further to this - we need to set our text property
			// to synchronize it so that should synchronize the
			// text and value.
			//
			// AS 11/11/03
			// I don't think we need to do this as 
			// changing the selected index should cause this to occur.
			//
			//this.OnValueChanged(EventArgs.Empty);
		}
		#endregion //OnEditorValueChanged

		#region InitializeText
		private void InitializeText( string text, bool preventTextChangedEvent )
		{
			bool previousPreventTextChange = this.preventOnTextChanged;
			this.preventOnTextChanged = preventTextChangedEvent;

			bool previousInitializingText = this.initializingText;
			this.initializingText = true;

			try
			{
				// SSP 5/12/04 UWG3165
				// Added SetText method. Call that instead of setting the Text property and also
				// pass in true for the calledFromEditorValueChanged parameter.
				//
				//this.Text = text;
				// MRS 10/28/04 - UWG2996
				//this.SetText( text, true );
				this.SelectedItemChangeHelper( ComboSelectedItemChangeContext.Text,text);
			}
			finally
			{
				this.initializingText = previousInitializingText;
				this.preventOnTextChanged = previousPreventTextChange;
			}
		}
		#endregion //InitializeText

		#region BaseText
		internal string BaseText
		{
			get { return base.Text; }
		}
		#endregion //BaseText

		#region TextBoxEvent
		internal enum TextBoxEvent
		{
			Click,
			DoubleClick,
			MouseDown,
			MouseMove,
			MouseUp,
			MouseHover,
			MouseWheel,
		}
		#endregion //TextBoxEvent

		#region InvokeTextBoxEvent
		internal void InvokeTextBoxEvent( TextBoxEvent eventId, EventArgs e )
		{
			switch(eventId)
			{
				case TextBoxEvent.Click:
					this.OnClick(e);
					break;
				case TextBoxEvent.DoubleClick:
					this.OnDoubleClick(e);
					break;
				case TextBoxEvent.MouseDown:
					this.OnMouseDown( (MouseEventArgs)e );
					break;
				case TextBoxEvent.MouseMove:
					this.OnMouseMove( (MouseEventArgs)e );
					break;
				case TextBoxEvent.MouseUp:
					this.OnMouseUp( (MouseEventArgs)e );
					break;
				case TextBoxEvent.MouseHover:
					this.OnMouseHover(e);
					break;
				case TextBoxEvent.MouseWheel:
					this.OnMouseWheel( (MouseEventArgs)e );
					break;
			}
		}
		#endregion //InvokeTextBoxEvent

		// AS 11/17/03 [UltraCombo as Editor]
		// This is needed so we don't break UWG1468 and UWG1666
		//
		#region ShouldEnterEditModeOnFocus
		private bool ShouldEnterEditModeOnFocus
		{
			get
			{
				if (this.hasOnEnterBeenCalled)
					return true;

				UIPermission perm = new UIPermission(UIPermissionWindow.AllWindows);

				try
				{
					perm.Assert();

					return (this.Parent != null && this.Parent is UserControl);
				}
				catch (System.Security.SecurityException)
				{
					return true;
				}
			}
		}
		#endregion //ShouldEnterEditModeOnFocus

		// AS 1/16/04
		#region OnAfter Enter/Exit EditMode
		private void OnAfterEditorEnterEditMode(object sender, EventArgs e)
		{
			// MRS 6/3/04 - UWE973
			// Set the Accessiblename on the TextBox
			if ( this.DropDownStyle == UltraComboStyle.DropDown )
				this.InternalEditor.TextBox.AccessibleName = this.AccessibilityObject.Name;

			this.AccessibilityNotifyClientsInternal(AccessibleEvents.Reorder, -1);
		}

		private void OnAfterEditorExitEditMode(object sender, EventArgs e)
		{
			this.AccessibilityNotifyClientsInternal(AccessibleEvents.Reorder, -1);
		}
		#endregion //OnAfter Enter/Exit EditMode

		// AS 5/21/04 UWG3238(aka UWE925) - See UWE917
		#region OnBeforeEditorExitEditMode
		private void OnBeforeEditorExitEditMode(object sender, Infragistics.Win.BeforeExitEditModeEventArgs e)
		{
			//	BF 2/15/06	BR07876
			//	If AlwaysInEditMode is true, cancel the BeforeExitEditMode event.
			if ( this.AlwaysInEditMode )
			{
				e.Cancel = true;
				return;
			}

			// AS 5/21/04 UWG3238(aka UWE925) - See UWE917
			// MRS 4/30/04 - UWE917
			// If the validation was cancelled, we will need to 
			// set the SelStart and SelLength back to what they were. So 
			// store them before we exit edit mode. 
			if (this.InternalEditor.SupportsSelectableText)
			{
				cachedSelStart  = this.InternalEditor.SelectionStart;
				cachedSelLength = this.InternalEditor.SelectionLength;
			}
		}
		#endregion //OnBeforeEditorExitEditMode

		// AS 1/7/04 Accessibility
		#region CreateAccessibilityInstance
		/// <summary>
		/// Creates an accessible object for the related object.
		/// </summary>
		/// <param name="relatedObject">The logically related object (e.g. an UltraGrid, UltraGridRow, ColumnHeader etc.).</param>
		/// <returns>A new <see cref="AccessibleObject"/> object for the related object.</returns>
		internal protected override System.Windows.Forms.AccessibleObject CreateAccessibilityInstance( object relatedObject )
		{
			if (relatedObject == this)
				return new UltraComboAccessibleObject(this);
			else if (relatedObject is ComboDropDownControl)
				return new UltraGridBaseAccessibleObject(this);

			return base.CreateAccessibilityInstance(relatedObject);
		}
		#endregion //CreateAccessibilityInstance

		// AS 1/7/04 Accessibility
		#region UltraComboAccessibleObject
		/// <summary>
		/// Accessible object representing an <see cref="UltraCombo"/> control
		/// </summary>
		// AS 1/16/04 DNF143
		//protected class UltraComboAccessibleObject : ControlAccessibleObject
		protected class UltraComboAccessibleObject : UltraControlBase.UltraControlAccessibleObject
		{
			#region Member Variables

			private AccessibleObject		editor = null;

			#endregion //Member Variables

			#region Constructor
			/// <summary>
			/// Initializes a new <see cref="UltraComboAccessibleObject"/>
			/// </summary>
			/// <param name="ownerControl">Control which is represented by the accessible object</param>
			public UltraComboAccessibleObject(UltraCombo ownerControl) : base(ownerControl)
			{
			}
			#endregion //Constructor

			#region Base class overrides

			#region Name
			/// <summary>
			/// Returns the name of the accessible object.
			/// </summary>
			public override string Name
			{
				get
				{
					string name = base.Name;

					if (name == null && this.GetChildCount() > 0)
						name = this.GetChild(0).Name;

					return name;
				}
				set
				{
					base.Name = value;
				}
			}
			#endregion //Name

			#region Role
			/// <summary>
			/// Returns the role of the accessible object.
			/// </summary>
			public override System.Windows.Forms.AccessibleRole Role
			{
				get	
				{ 
					if (this.Owner.AccessibleRole != AccessibleRole.Default)
						return this.Owner.AccessibleRole;

					if (this.GetChildCount() > 0)
						return this.GetChild(0).Role; 

					return base.Role;
				}
			}
			#endregion //Role

			#region GetChild

			/// <summary>
			/// Retrieves the accessible child corresponding to the specified index.
			/// </summary>
			/// <param name="index">The zero-based index of the accessible child.</param>
			/// <returns>An AccessibleObject that represents the accessible child corresponding to the specified index.</returns>
			public override AccessibleObject GetChild(int index)
			{
				if (index == 0)
				{
					if (this.editor == null)
						this.editor = ((UltraCombo)this.Owner).InternalEditor.CreateAccessibilityInstance(
							this,
							((UltraCombo)this.Owner).EditorOwner,
							this.Owner);

					return this.editor;
				}
				else
					index--;

				// AS 1/16/04
				// Be sure to pass along value change and selected item notifications.
				if (index == 0 && ((UltraCombo)this.Owner).SelectedRow != null)
				{
					return this.GetSelected();
				}
				else
					index--;

				return base.GetChild(index);
			}

			#endregion GetChild

			#region GetChildCount

			/// <summary>
			/// Retrieves the number of children belonging to an accessible object.
			/// </summary>
			/// <returns>The number of children belonging to an accessible object.</returns>
			public override int GetChildCount()
			{
				// only has its editor
				// AS 1/16/04 DNF143
				//return 1;
				return base.GetChildCount() + 1;
			}

			#endregion GetChildCount

			// AS 1/15/04 DNF143
			#region GetFocused
			/// <summary>
			/// Returns the object that has the keyboard focus.
			/// </summary>
			/// <returns>
			/// An <b>AccessibleObject</b> that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (<b>Nothing</b> in Visual Basic) if no object has focus.
			/// </returns>
			public override AccessibleObject GetFocused()
			{
				// get the focused item from the editor
				if (this.Owner.Focused)
					return this.GetChild(0).GetFocused();

				return base.GetFocused();
			}
			#endregion //GetFocused

			// AS 1/16/04
			#region GetSelected
			/// <summary>
			/// Returns the object that has the keyboard focus.
			/// </summary>
			/// <returns>
			/// An <b>AccessibleObject</b> that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (<b>Nothing</b> in Visual Basic) if no object has focus.
			/// </returns>
			public override AccessibleObject GetSelected()
			{
				return this.GetChild(0).GetSelected();
			}
			#endregion //GetFocused

			#endregion //Base class overrides
		}
		#endregion //EditorButtonControlAccessibleObject

		// MRS 10/28/04 - UWG2996
		#region SelectedItemChangeHelper
		bool refreshingEditorValue = false;
		internal override void SelectedItemChangeHelper(ComboSelectedItemChangeContext context, object newData)
		{	
            //  BF 3/5/08   FR09238 (EditorWithCombo AutoCompleteMode)
            //  Short-circuit the selection of rows when the end user is typing
            //  characters during a "suggest mode" session. Note that we also do
            //  this when the selected item is initially cleared to begin a suggest
            //  mode session.
            EditorWithCombo.AutoCompleteInfo autoCompleteInfo = this.AutoCompleteInfo;
            if ( autoCompleteInfo != null &&
                 autoCompleteInfo.IsProcessingTextChanged &&
                 (autoCompleteInfo.IsFilteredDropDownVisible || autoCompleteInfo.IsClearingSelectedIndexToBeginSuggestMode) )
            {
                //  BF 9/17/08  TFS7429
                //  Before returning, set the text if that is the reason we got in here.
                //  Note that this method sets the property directly on the base class.
                if ( context == ComboSelectedItemChangeContext.Text )
                    base.Text = newData as string;

                return;
            }

			 

			// MRS 7/26/05
			// Don't bother if the layout is null or disposed. 
			if ( this.DisplayLayout == null ||
				this.DisplayLayout.Disposed )
			{
				return;
			}

			if (this.refreshingEditorValue)
				return;

			bool oldRefreshingEditorValue;
			bool oldPreventOnTextChanged;
			bool oldInitializingText;

			int newIndex = -1;
			UltraGridRow newRow = null;
			string newText = string.Empty;
			object newValue = null;

			bool hasRowChanged = false;
			bool hasTextChanged = false;
			bool hasValueChanged = false;
			
			// MRS 3/31/05 - BR03120
			// There are three possible editors that might be gettng used here. 			
			UltraGridComboEditor editor = null;
			// 1) If the user created an UltraGridComboEditor in code, it would
			// have automatically created a control which uses the editor as
			// it's ValueListOwner. 
			// 2) The user created a control on a form and is using it as 
			// the EditorControl. In this case, use the ExternalEditor
			// 3) This is a standalone Combo. Use the Internal Editor
			if ( (this.Owner is UltraGridComboEditor) &&
				((UltraGridComboEditor)(this.Owner)).IsInEditMode )
			{
				editor = ((UltraGridComboEditor)(this.Owner));
			}
			else if (this.ExternalEditor.IsInEditMode)
				editor = this.ExternalEditor;
			else
				editor = this.InternalEditor;

            //  BF NA 9.1 - UltraCombo MultiSelect
            //  When the value is determined by the checked list, a change in selection
            //  should not affect the text, so return if that is the change context.
            bool valueIsProvidedByCheckedItems = false;
            if ( this.CheckStateColumn != null )
            {
                valueIsProvidedByCheckedItems = editor != null && editor.InternalIsValueProvidedByCheckedItems;
                if ( valueIsProvidedByCheckedItems && context == ComboSelectedItemChangeContext.Text )
                    return;
            }

			// Based on the context, determine the new values of all 4
			// relevant properties and determine which ones have actually
			// changed.
			switch (context)
			{
				case ComboSelectedItemChangeContext.SelectedIndex:
					newIndex = (int)newData;

					// If the Index is not actually changing, bail;
					if (newIndex == ((IValueList)this).SelectedItemIndex)
						return;
					
					// Get the new selected row, Text, and Value
					// If the new Index is less than 0, it means nothing is selected and we don't have to do anything. The member variables are already initialized to the "nothing selected" values.
					if ( newIndex >=0 )
					{
						// Get the new selected Row.
						if ( newIndex < this.Rows.Count )
							newRow = this.Rows[newIndex];	
						
						// Get the new Text
						// MRS 4/7/05 - BR03150
						//newText = newRow.Cells[this.DisplayMemberResolved].Value.ToString();
						// MRS 6/7/05 - BR00847
//						newText = (null != newRow.Cells[this.DisplayMemberResolved].Value) ?
//							newRow.Cells[this.DisplayMemberResolved].Value.ToString() :
//							null;
						newText = (null != newRow.Cells[this.DisplayMemberResolved].Value) ?
							newRow.Cells[this.DisplayMemberResolved].Text :
							null;

						// Get the new Value
						newValue = newRow.Cells[this.ValueMemberResolved].Value;
					}

					break;
				case ComboSelectedItemChangeContext.SelectedRow:
					// Get the new row
					newRow = newData as UltraGridRow;

					// If the row is not actually changing, bail
					if (newRow == this.selectedRow)
						return;

					// Get the new index, Text, and Value
					// If the new row is null, it means nothing is selected and we don't have to do anything. The member variables are already initialized to the "nothing selected" values.
					if (newRow != null )
					{
						// Get the new Index
						newIndex = this.Rows.IndexOf( newRow );		

						// Get the new Text
						// MRS 4/7/05 - BR03150
						//newText = newRow.Cells[this.DisplayMemberResolved].Value.ToString();
						newText = (null != newRow.Cells[this.DisplayMemberResolved].Value) ?
							newRow.Cells[this.DisplayMemberResolved].Value.ToString() :
							null;
						
						// MRS 1/20/05 - BR01854
						// Should not be doing a ToString here. 
						//Get the new Value
						//newValue = newRow.Cells[this.ValueMemberResolved].Value.ToString();
						newValue = newRow.Cells[this.ValueMemberResolved].Value;
					}

					break;
				// MRS 12/5/05 - BR08027
				// added a ListChangedText context
				case ComboSelectedItemChangeContext.ListChangedText:
				case ComboSelectedItemChangeContext.Text:
					// Get the new Text
					newText = (string)newData;

					// MRS 12/5/05 - BR08027
					// Only bail if the context is Text. If it's ListChanged, 
					// then keep going.
                    if (context != ComboSelectedItemChangeContext.ListChangedText &&
                        // MRS 6/10/2008 - BR33728 
                         !this.Rows.IsRowsDirty)
					{
						// If the Text is not actually changing, bail
						if ((string.Compare(this.Text, newText) == 0))
							return;
					}

					// SSP 10/11/06 - NAS 6.3
					// Added SetInitialValue on the UltraCombo.
					// If Text or Value is set to someething other than what was passed into the SetInitialValue method
					// then load the datasource and find the corresponding row.
					// 
					if ( this.dontLoadDataSource && ! this.DoesTextMatch( newText, this.dontLoadDataSource_initialDisplayText, false ) )
						this.dontLoadDataSource = false;

					// Get the new Value
					if( this.DataFilter!=null &&
						this.InternalEditor.CanHandleConversionInternal(ConversionDirection.DisplayToEditor)==DefaultableBoolean.True )
					{
						// Get the new Value
						newValue = this.InternalEditor.GetDataFilteredDestinationValue(newText, 
							ConversionDirection.DisplayToEditor, this.EditorOwner, this);
						
						// Get the new Index
						// We are basing the selected index on the Value here.
						((IValueList)this).GetText( newValue, ref newIndex );
					}
					else
					{
						// get the new Value and Index
						newValue = ((IValueList)this).GetValue( newText, ref newIndex );	
					}

					if (newIndex >=0 )
						newRow = this.Rows[newIndex];
					else
                    {
                        newRow = null;

                        // MRS 11/18/2008 - TFS10465
                        // If we didn't find a match on the list, then use the Text
                        // as the new value, as long as the control supports it. 
                        if (this.EditorOwner.MustSelectFromList(this) == false)
                            newValue = newText;
                    }

					break;
				
				// MRS 10/24/05 - BR06979
				// Added ListChanged Context.
				// MRS 12/5/05 - BR08027
				//case ComboSelectedItemChangeContext.ListChanged:
				case ComboSelectedItemChangeContext.ListChangedValue:
				case ComboSelectedItemChangeContext.Value:
					// Get the new Value
					newValue = this.InternalEditor.GetDataFilteredDestinationValue(newData, 
						ConversionDirection.OwnerToEditor, this.EditorOwner, this);
					
					//					if ( !this.dontCheckValueBeingOldValue )
					//					{
					
					// MRS 10/24/05 - BR06979
					// Only bail if the context is Value. If it's ListChanged, 
					// then keep going.
					// MRS 12/5/05 - BR08027
					//if (context != ComboSelectedItemChangeContext.ListChanged)
                    if (context != ComboSelectedItemChangeContext.ListChangedValue &&
                        // MRS 6/10/2008 - BR33728 
                         !this.Rows.IsRowsDirty)
					{
						// If the value is not actually changing, bail.
						if (Object.Equals(newValue, this.selectedValue))
							return;
					}
					else
					{
						// MRS 10/24/05 - BR06979
						// If the context was ListChanged, treat it like a Value
						// change from here on. 
						context = ComboSelectedItemChangeContext.Value;
					}					

					//					}
					
					if (newValue != null)
					{
						// SSP 10/11/06 - NAS 6.3
						// Added SetInitialValue on the UltraCombo.
						// If Text or Value is set to someething other than what was passed into the SetInitialValue method
						// then load the datasource and find the corresponding row.
						// 
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( this.dontLoadDataSource && ! this.DoesDataValueMatch( newValue, this.dontLoadDataSource_initialDataValue ) )
						if ( this.dontLoadDataSource && !UltraDropDownBase.DoesDataValueMatch( newValue, this.dontLoadDataSource_initialDataValue ) )
							this.dontLoadDataSource = false;

						// Get the new Text and Index
						newText = ((IValueList)this).GetText( newValue, ref newIndex );
						if (newIndex >= 0)
							newRow = this.Rows[newIndex];
						else
						{
							newRow = null;

							// MRS 10/21/05 - BR06979
							// If the row was not found, we should be setting the
							// Text to the ToString of the value. 
							// SSP 10/12/06 - NAS 6.3
							// Added SetInitialValue on the UltraCombo. If we are in dontLoadDataSource then
							// newText will be assigned a valid value and index will be -1. So in that case
							// don't override the newText.
							// 
							//newText = newValue.ToString();
							if ( null == newText || 0 == newText.Length || ! this.dontLoadDataSource )
								newText = newValue.ToString();
						}							
					}
					else
					{				
						// if the Value is null, nothing is selected. 
						// newIndex is already initialized to -1 and newRow is already null.											
					}

					break;
			}
			
			// See what changed

            //  BF NA 9.1 - UltraCombo MultiSelect
            //
            //  I left the 'hasValueChanged' stack variable unchanged, and changed the ValueChanged
            //  event firing logic to alos account for whether the value is provided by the checked
            //  item list. I am changing 'hasTextChanged', however, because (a) we do not want to
            //  ever fire TextChanged from here, and (b) we also don't want to set base.Text, since
            //  that is used to determine later (in OnEditorValueChanged) whether the TextChanged.
            //  
            //hasTextChanged = (string.Compare(base.Text, newText) != 0);
            hasTextChanged = valueIsProvidedByCheckedItems ? false : (string.Compare(base.Text, newText) != 0);

			hasValueChanged = !Object.Equals(newValue, this.selectedValue);
			// MRS 7/19/05 - BR05008 & BR05101
			//hasRowChanged =  (newIndex != ((IValueList)this).SelectedItemIndex);
			hasRowChanged =  (newIndex != ((IValueList)this).SelectedItemIndex ||
				newRow != this.selectedRow);

			// Phase 1 is complete. We know all the new property settings
			// Begin Phase 2 - set the internal members. 

			// Has the row changed? 
			if ( hasRowChanged )
			{				
				//				if ( null != newRow && newIndex < 0 )
				//				{
				//					throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_287"),  Shared.SR.GetString("LE_ArgumentException_367") );
				//				}

				// De-select the old row, if there was one
				if ( this.selectedRow != null )
				{
					// MRS 7/19/05 - BR05008 & BR05101
					// Don't do this if the row is disposed.
					if (!this.selectedRow.Disposed)
						this.selectedRow.Selected = false;

					// MRS 7/19/05 - BR05008 & BR05101
					//UnHook into the Disposed event of the row
					this.selectedRow.SubObjectDisposed -= new SubObjectDisposedEventHandler(this.SelectedRow_Disposed);
				}

				// Set the selectedRow to the new row
				this.selectedRow = newRow;

				// MRS 7/19/05 - BR05008 & BR05101
				//Hook into the Disposed event of the row
				if (this.selectedRow != null)
					this.selectedRow.SubObjectDisposed += new SubObjectDisposedEventHandler(this.SelectedRow_Disposed);

				// MRS 3/24/05 - BR02958 - I remembered why this was here. 
				// When you drop down the combo and type something, the row needs to be scrolled into view. 
//				// MRS 12/16/04 - I commented this out because I can't remember why
//				// I did it and it was causing a flicker when the dropdown was 
//				// already dropped down and you arrow through the items. 
//				// I think the fix for BR00535 will handle any problems anyway. 
//				// Scroll the row into view	
//				this.ScrollSelectedRowIntoView();

                //  BF 2/24/09  TFS12735
                //
                //  This causes problems when selecting by mouse if the row is
                //  in the last "page" in the dropdown. If the selection was
                //  triggered by ISelectionManager.ActivateItem, we don't need to
                //  scroll it into view, and we can avoid TFS12735.
                //
				//this.ScrollSelectedRowIntoView(true);
                if ( this.IsActivatingRowByMouse == false )
				    this.ScrollSelectedRowIntoView(true);
					
				// Set the activeRow
				this.currentActiveRow = newRow;
				oldRefreshingEditorValue = this.refreshingEditorValue;
				this.refreshingEditorValue = true;
				try
				{
//					// MRS 3/17/05 - BR02861
//					// Only do this when we are in edit mode. 
////					if (this.Owner != null &&
//					if (this.InternalEditor.IsInEditMode &&
//					if ((this.InternalEditor.IsInEditMode ||
//						this.ExternalEditor.IsInEditMode ) &&
					// MRS 6/29/05 - When the Combo is used as a ValueList in the
					// grid, the Owner will not be an UltraGridComboEditorOwner. It will be
					// an EditorWithCombo. In that case, we still need to send 
					// the ValueList notification. 
					//if ( editor.IsInEditMode &&					
					// MRS 9/8/05 - BR06211
					// This means that the internal editor is being used
					// as the ValueList Owner and it's not in edit mode. 
					if ( this.Owner == this.InternalEditor &&
						! this.InternalEditor.IsInEditMode )
					{
						this.EditorOwner.SetEditorContext( this, newIndex );
					}
					else
					if ( (editor.IsInEditMode ||
						! ( this.Owner is UltraGridComboEditorOwner ) )&&
						this.Owner != null &&				
						(context == ComboSelectedItemChangeContext.SelectedRow ||
						context == ComboSelectedItemChangeContext.SelectedIndex) )
					{
						this.Owner.OnSelectedItemChanged();
					}
					else
					{
						// MRS 12/16/04 - BR01235		
						// This is probably not the best implementation, but
						// it's all we can do for duplicate data values when
						// the Control doesn't have focus. 
						this.EditorOwner.SetEditorContext( this, newIndex );
					}
				}				
				finally
				{
					this.refreshingEditorValue = oldRefreshingEditorValue;
				}				

				// Select the row, so it appears highlighted
				if ( this.selectedRow != null )
					this.selectedRow.Selected = true;
			}

			// Has the text changed? 
			if (hasTextChanged)
			{
				// Store these two flags
				oldPreventOnTextChanged = this.preventOnTextChanged;
				oldInitializingText = this.initializingText;

				// Set them both to true. these flags will essentially 
				// prevent the TextChanged from firing.
				this.preventOnTextChanged = true;
				this.initializingText = true;
				try
				{
					// Set the text on the base. 
					base.Text = newText;
				}
				finally
				{
					// Set the flags back to what they were. 
					this.preventOnTextChanged = oldPreventOnTextChanged;
					this.initializingText = oldInitializingText;
				}
			}

			// Has the value changed? 
			if (hasValueChanged)
				//Set the selectedValue member
				this.selectedValue = newValue;

			// MRS 3/31/05- I think we might want to use "editor" here instead of InternalEditor
			// If the editor is in Edit Mode, we might need to refresh it
			if (this.InternalEditor.IsInEditMode &&
				hasTextChanged && 
				this.InternalEditor.TextBox != null)
			{
				oldRefreshingEditorValue = this.refreshingEditorValue;
				this.refreshingEditorValue = true;
				try
				{
					// MRS 3/31/05- I think we might want to use "editor" here instead of InternalEditor
					
                    //  BF 8/4/08   BR34921
                    //  If we are in suggest mode, and closing because the new text
                    //  does not match any items, leave the contents of the textbox alone.
                    //this.InternalEditor.TextBox.Text = newText;	
                    bool preserveTextBoxContents =  autoCompleteInfo.IsSuggestMode &&
                                                    autoCompleteInfo.IsDropDownClosingByNoFilteredItems;

                    //  BF NA 9.1 - UltraCombo MultiSelect
                    //  Also leave the textbox alone if the value is determined by the checked item list.
                    preserveTextBoxContents = preserveTextBoxContents || valueIsProvidedByCheckedItems;

                    if ( preserveTextBoxContents == false )
                        this.InternalEditor.TextBox.Text = newText;	
				}				
				finally
				{
					this.refreshingEditorValue = oldRefreshingEditorValue;
				}
			}

			// SSP 1/21/05 BR01891 
			// Added SyncWithCurrencyManager property on UltraCombo. This will cause the 
			// UltraCombo to synchronize the selected row with the currency manager's Position.
			//
			this.SyncSelectedRowWithCurrencyManager( false );

            //  BF NA 9.1 - UltraCombo MultiSelect
            //  Don't fire ValueChanged or TextChanged if the value is determined by the checked list.
            bool fireValueChanged = hasValueChanged && (valueIsProvidedByCheckedItems == false);

			// Phase 2 is complete - all members are set. 
			// Begin Phase 3 - fire events

			// The event order depends on the context
			switch (context)
			{
				case ComboSelectedItemChangeContext.SelectedIndex:
				case ComboSelectedItemChangeContext.SelectedRow:
					if (this.FireRowSelectedEventHelper(hasRowChanged, newIndex))
						return;
                    //  BF NA 9.1 - UltraCombo MultiSelect
					//if (this.FireValueChangedEventHelper(hasValueChanged, newValue))
					if (this.FireValueChangedEventHelper(fireValueChanged, newValue))
						return;
					if (this.FireTextChangedEventHelper(hasTextChanged, newText))
						return;
					break;
				case ComboSelectedItemChangeContext.Text:
					if (this.FireTextChangedEventHelper(hasTextChanged, newText))
						return;					
                    //  BF NA 9.1 - UltraCombo MultiSelect
					//if (this.FireValueChangedEventHelper(hasValueChanged, newValue))
					if (this.FireValueChangedEventHelper(fireValueChanged, newValue))
						return;
					if (this.FireRowSelectedEventHelper(hasRowChanged, newIndex))
						return;
					break;
				case ComboSelectedItemChangeContext.Value:
                    //  BF NA 9.1 - UltraCombo MultiSelect
					//if (this.FireValueChangedEventHelper(hasValueChanged, newValue))
					if (this.FireValueChangedEventHelper(fireValueChanged, newValue))
						return;
					if (this.FireTextChangedEventHelper(hasTextChanged, newText))
						return;
					if (this.FireRowSelectedEventHelper(hasRowChanged, newIndex))
						return;
					break;					
			}
					
			// Dirty the child elements to make sure everything is displaying correctly. 
			this.UIElement.DirtyChildElements(true);
		}

		private bool FireValueChangedEventHelper(bool hasChanged, object newValue)
		{
			// if the property hasn't changed, don't fire the event
			if (!hasChanged)
				return false;

			// Fire the ValueChanged event				
			this.FireEvent( ComboEventIds.ValueChanged, new EventArgs() );

			// If the user changed something inside the event that 
			// caused the Value to change again, bail out. 
			return (this.selectedValue != newValue);
		}

		private bool FireTextChangedEventHelper(bool hasChanged, string newText)
		{
			// if the property hasn't changed, don't fire the event
			if (!hasChanged)
				return false;

			// Fire the TextChanged event
			this.OnTextChanged(new EventArgs());				
				
			// If the user changed something inside the event that 
			// caused the Text to change again, bail out. 
			return (string.Compare(base.Text, newText) != 0);
		}				

		private bool FireRowSelectedEventHelper(bool hasChanged, int newIndex)
		{
			// if the property hasn't changed, don't fire the event
			if (!hasChanged)
				return false;

			//Fire RowSelected
			RowSelectedEventArgs e = new RowSelectedEventArgs( this, this.selectedRow );
			this.FireRowSelected( e );

			// If the user changed something inside the event that 
			// caused the index to change again, bail out. 
			return (newIndex != ((IValueList)this).SelectedItemIndex);				
		}
		#endregion SelectedItemChangeHelper
	
		#region SyncSelectedRowWithCurrencyManager

		// SSP 1/21/05 BR01891 
		// Added SyncWithCurrencyManager property on UltraCombo. This will cause the 
		// UltraCombo to synchronize the selected row with the currency manager's Position.
		//
		internal void SyncSelectedRowWithCurrencyManager( bool targetSelectedRow )
		{
			if ( this.SyncWithCurrencyManager )
			{
				UltraGridLayout layout = this.DisplayLayout;
				UltraGridBand rootBand = null != layout 
					&& ! layout.Disposed && layout.SortedBands.Count > 0 ? layout.SortedBands[0] : null;

				BindingManagerBase bindingManager = null != rootBand ? rootBand.BindingManager : null;

				if ( null != bindingManager )
				{
					if ( targetSelectedRow )
					{
						this.SelectedRow = rootBand.GetCurrentRow( false, false );
					}
					else
					{
						UltraGridRow newSelectedRow = this.SelectedRow;
						int newPosition = null != newSelectedRow ? newSelectedRow.ListIndex : -1;
					
						if ( newPosition != bindingManager.Position )
						{
							bool origIgnoreDataSourePositionChange = layout.IgnoreDataSourePositionChange;
							try
							{
								layout.IgnoreDataSourePositionChange = true;
								bindingManager.Position = newPosition;
							}
							finally
							{
								layout.IgnoreDataSourePositionChange = origIgnoreDataSourePositionChange;
							}
						}
					}
				}
			}
		}

		#endregion // SyncSelectedRowWithCurrencyManager

		// MRS 7/6/05 - BR04729
		#region Focused
		/// <summary>
		/// Overriden. Indicates if the editor or its contained textbox currently has the input focus.
		/// </summary>
		public override bool Focused
		{
			get 
			{
				// MRS 10/25/05 - BR07187
				if (this.useBaseFocused)
					return base.Focused;

				if (base.Focused)
					return true;

				// if we contain a control with focus...
				if (this.internalEditor != null &&
					((EditorWithText)this.InternalEditor).TextBox != null &&
					((EditorWithText)this.InternalEditor).TextBox.Focused)
					return true;

				return false;
			}
		}
	#endregion //Focused

		// MRS 7/6/05 - BR04729
		#region IsFocused
		internal bool IsFocused
		{
			get { return base.Focused; }
		}
		#endregion //IsFocused

		// MRS 10/25/05 - BR07187
		bool useBaseFocused = false;
		#region Focus (Shadowed)
		/// <summary>
		/// Sets input focus to the control.
		/// </summary>
		/// <returns>true if the input focus request was successful; otherwise, false.</returns>
		public new bool Focus()
		{

			bool oldUseBaseFocused = this.useBaseFocused;
			this.useBaseFocused = true;
			try
			{
				return base.Focus();
			}
			finally
			{
				this.useBaseFocused = oldUseBaseFocused;
			}
		}
		#endregion Focus (Shadowed)

		// MRS 10/25/05 - BR07187
		// Added FocusInternal method. This method just calls Focus on the control.
		// The advantage of this method is that it's overridable (unlike the regular Focus
		// method). So it is overriden so that it w~ill call the shadowed Focus
		// method instead of the base. 
		#region FocusInternal
		/// <summary>
		/// For internal infrastructure use only.
		/// </summary>
		/// <returns>The focus method of the control.</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool FocusInternal()
		{
			// This will call the shadowed Focus method on this class instead of
			// the base. 
			return this.Focus();
		}
		#endregion FocusInternal

		// MRS 7/19/05 - BR05008 & BR05101
		// When the selected row is disposed set the SelectedRow of the Combo to null
		// so we do not maintain an invalid value. 
		#region SelectedRow_Disposed
		
		private void SelectedRow_Disposed(SubObjectBase subObjectBase)
		{			
			if (this.DropDownStyle == UltraComboStyle.DropDownList)
				this.SelectedItemChangeHelper(ComboSelectedItemChangeContext.SelectedRow, null);
			else				
			{
				UltraGridRow row = subObjectBase as UltraGridRow;
				row.SubObjectDisposed -= new SubObjectDisposedEventHandler(this.SelectedRow_Disposed);
			}
		}

		#endregion SelectedRow_Disposed

		// MRS 10/24/05 - BR06979
		// Override this method and call into SelectedItemChangeHelper to
		// force the control to update when the list has new items.
		#region ListChanged event

		internal override void FireListChanged( )
		{
			// MRS 10/24/05 - BR06979
			// Whenever there is a ListChanged, call into SelectedItemChangeHelper
			// with a context of ListChanged. 
			// This will essentially refresh the Text and SelectedRow when the
			// list has changed. 
			// MRS 12/5/05 - BR08027
			// We need to dinstinguish between keeping the value or the text
			// when we get a ListChanged.
			//this.SelectedItemChangeHelper(ComboSelectedItemChangeContext.ListChanged, this.selectedValue);
			if (this.DropDownStyle == UltraComboStyle.DropDownList ||
				this.selectedValue != null)
			{
				this.SelectedItemChangeHelper(ComboSelectedItemChangeContext.ListChangedValue, this.selectedValue);
			}
			else
				this.SelectedItemChangeHelper(ComboSelectedItemChangeContext.ListChangedText, this.Text);

			base.FireListChanged();
		}

		#endregion //	ListChanged event

		// MRS 4/18/06 - BR11468
		#region MRS 4/18/06 - BR11468

		#region AllowNull
		/// <summary>
		/// Determines if the control will allow nulls, regardless of whether or not there is a null item on the list. 
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// When AllowNull is True, the control will allow the user to clear out the text of the control and interpret this as a null value. 
		/// The null value will be considered acceptable for the purposes of validation. IsItemInList will return true for a value of null 
		/// or DBNull, even if no such item exists on the list. LimitToList will permit the control to lose focus when the value is null, 
		/// even when no null item exists on the list. The control will display the as empty; it will not attempt to look for a null item on 
		/// the list in order to determine the display text.
		/// </p>
		/// <p class="body">
		/// When this property is Default, the control will have the same behavior as when the property is False.
		/// </p>
		/// </remarks>
		/// <seealso cref="NullText"/>
		[ LocalizedDescription("LD_UltraCombo_P_AllowNull")]
		[ LocalizedCategory("LC_Behavior") ]
		public DefaultableBoolean AllowNull
		{
			get
			{
				return this.allowNull;
			}

			set
			{
				if ( this.allowNull != value )
				{
                    // MRS NAS v8.3 - Unit Testing
                    GridUtils.ValidateEnum("AllowNull", typeof(DefaultableBoolean), value);

					this.allowNull = value;						
		
					this.DisplayLayout.UIElement.DirtyChildElements();

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.AllowNull );					
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAllowNull() 
		{
			return ( this.allowNull != DefaultableBoolean.Default);
		}
 
		/// <summary>
		/// Reset the <see cref="AllowNull"/> property to the default (DefaultableBoolean.Default).
		/// </summary>
		public void ResetAllowNull() 
		{
			this.AllowNull = DefaultableBoolean.Default;
		}
		#endregion AllowNull

		#region NullText
		/// <summary>
		/// Gets/sets the text to be used by the control when the value is null.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the control's value is null (Nothing in VB), the control displays an empty string by default. The <b>NullText</b> property enables the end developer to change the text that is displayed for null values (for example, '(NULL)' or '(none)').</p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraCombo_P_NullText")]
		[ LocalizedCategory("LC_Behavior") ]
		public string NullText
		{
			get { return this.nullTextValue; }
			set
			{ 
				if ( value != this.nullTextValue )
				{
					this.nullTextValue = value;

					if (this.IsHandleCreated)
						this.UIElement.DirtyChildElements(true);

					//	Notify listeners of the property change
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.NullText );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeNullText() 
		{
			return this.nullTextValue != null && this.nullTextValue.Length > 0;
		}
 
		/// <summary>
		/// Reset the <see cref="NullText"/> property to the default (DefaultableBoolean.Default).
		/// </summary>
		public void ResetNullText() 
		{
            // MRS NAS v8.3 - Unit Testing
			//this.nullTextValue = null;
            this.NullText = null;
		}
		#endregion NullText

		#endregion MRS 4/18/06 - BR11468

		#region SetInitialValue

		// SSP 10/11/06 - NAS 6.3
		// Added SetInitialValue on the UltraCombo.
		// 
		/// <summary>
		/// This method is used to delay loading of the data source until it's dropped down.
		/// </summary>
		/// <param name="dataValue">Initial data value of the control.</param>
		/// <param name="displayText">Initial display text of the control.</param>
		/// <remarks>
		/// <p class="body">
		/// By default the datasource is loaded as soon as the control initialization process
		/// finishes. You can delay loading the datasource by calling this method and specifying
		/// non-null data value and display text (they can be empty strings). <b>However</b> note
		/// that you must set the data source after calling this method. Otherwise the datasource
		/// will be loaded syncrhonously when it's set or when the control initialization process
		/// finishes.
		/// </p>
		/// <p class="body">
		/// Here is a typical way to use this method. At design time either leave the UltraCombo's
		/// DataSource null or bind it to a dummy datasource for the purposes of designing columns.
		/// At runtime in the form's load event handler (or some other appropriate place) call this
		/// method specifying the initial data value and display text. Then bind the UltraCombo
		/// to the actual data source. This will cause the UltraCombo to not load the data source
		/// until it's dropped down or its Value property is set to a value that's not the 
		/// initial data value specified in this method.
		/// </p>
		/// </remarks>
		public void SetInitialValue( object dataValue, string displayText )
		{
			this.dontLoadDataSource_initialDataValue = dataValue;
			this.dontLoadDataSource_initialDisplayText = displayText;

			if ( null != dataValue || null != displayText )
				this.dontLoadDataSource = true;

			// Initialize the Value to the passed in data value. This will initialize the text as well.
			// 
			this.Value = dataValue;
		}

		internal object dontLoadDataSource_initialDataValue = null;
		internal string dontLoadDataSource_initialDisplayText = null;

		#endregion // SetInitialValue

        // MRS 4/3/07 - BR21617
        #region EndUpdate
        /// <summary>
        /// Resets the <see cref="UltraControlBase.IsUpdating"/> flag to false and optionally invalidates the control.
        /// </summary>
        /// <param name="invalidate">True to invalidate the control and dirty the child elements; otherwise false.</param>
        /// <remarks>
        /// <p class="body">This method must be called after <see cref="UltraControlBase.BeginUpdate"/>. If <b>BeginUpdate</b> was called without a subsequent call to <b>EndUpdate</b> the control will not draw itself.</p>
        /// <p class="note"><b>Note:</b> Calling this method passing in false should only be done when it is known that the changes made between 
        /// the <see cref="UltraControlBase.BeginUpdate"/> and <b>EndUpdate</b> calls did not require invalidation or dirtying of the elements or when the invalidation 
        /// is being handled by the programmer.</p>
        /// </remarks>
        /// <seealso cref="UltraControlBase.IsUpdating"/>
        /// <seealso cref="UltraControlBase.BeginUpdate"/>
        public override void EndUpdate(bool invalidate)
        {
            base.EndUpdate(invalidate);

            // We need to invalidate the grid control on the dropdown.
            if (invalidate)
            {
                if (this.IsDroppedDown)
                    this.ControlForGridDisplay.Invalidate();
            }
        }
        #endregion //EndUpdate

        //  BF 2/28/08  FR09238 - AutoCompleteMode
        #region FR09238 - AutoCompleteMode

            #region AutoCompleteFilterCondition
        internal override FilterCondition AutoCompleteFilterCondition
        {
            get
            {
                //  BF 5/13/08  BR32807
                //  I was never able to reproduce this, but when disposing we should
                //  not bother with any of the following and just return null.
                if ( this.IsDisposed || this.Disposing )
                    return null;

                //  If AutoComplete is switched off, or does not include suggest mode,
                //  return null, since we don't need a filter in that case, and a
                //  null return eliminates the caller's need to check the value of
                //  the AutoCompleteMode property.
                AutoCompleteMode autoCompleteMode = this.AutoCompleteModeResolved;
                if ( autoCompleteMode != AutoCompleteMode.Suggest &&
                     autoCompleteMode != AutoCompleteMode.SuggestAppend )
                    return null;

                //  If the member is null, create a new instance, using the column
                //  referenced by the DisplayMemberResolved property as the associated
                //  column.
                if ( this.autoCompleteFilterCondition == null )
                {
                    string displayMember = this.DisplayMemberResolved;
                    BandsCollection bands = this.DisplayLayout.SortedBands;
                    UltraGridBand bandZero = bands.Count > 0 ? bands[0] : null;
                    ColumnsCollection columns = bandZero != null ? bandZero.Columns : null;

                    //  BF 5/13/08  BR32807
                    //UltraGridColumn column = columns != null ? columns[displayMember] : null;
                    UltraGridColumn column = (columns != null && columns.Exists(displayMember)) ?
                        columns[displayMember] :
                        null;

                    if ( column != null )
                        this.autoCompleteFilterCondition = new FilterCondition( column, FilterComparisionOperator.StartsWith, string.Empty );
                }

                return this.autoCompleteFilterCondition;
            }
        }

        internal void ResetAutoCompleteFilterCondition()
        {
            if ( this.autoCompleteFilterCondition != null )
            {
                this.autoCompleteFilterCondition.Dispose();
                this.autoCompleteFilterCondition = null;

                UltraCombo.BumpLayoutRowFilterVersions( this.DisplayLayout );
            }
        }
            #endregion AutoCompleteFilterCondition

            #region RefreshAutoCompleteFilterCondition

        internal void RefreshAutoCompleteFilterCondition( string text )
        {
            this.RefreshAutoCompleteFilterCondition( text, true );
        }

        internal void RefreshAutoCompleteFilterCondition( string text, bool dirtyGridElement )
        {
            //  BF 5/13/08  BR32807
            //this.AutoCompleteFilterCondition.CompareValue = text;
            FilterCondition autoCompleteFilterCondition = this.AutoCompleteFilterCondition;
            if ( autoCompleteFilterCondition == null )
                return;

            autoCompleteFilterCondition.CompareValue = text;

            UltraGridLayout layout = this.DisplayLayout;
            UltraCombo.BumpLayoutRowFilterVersions( layout );

            if ( dirtyGridElement )
                layout.DirtyGridElement();
        }

            #endregion RefreshAutoCompleteFilterCondition

            #region BumpLayoutRowFilterVersions
        static internal void BumpLayoutRowFilterVersions( UltraGridLayout layout )
        {
            if ( layout != null )
            {
                layout.BumpGrandVerifyVersion();
                layout.BumpRowFiltersVersion();
            }
        }

            #endregion BumpLayoutRowFilterVersions

            #region FilterAutoCompleteItems
        internal override int FilterAutoCompleteItems( string text, bool apply )
        {
            int retVal = -1;

            if ( apply )
            {
                if ( text != null )
                {
                    this.RefreshAutoCompleteFilterCondition( text );            
                    retVal = this.Rows.FilteredInRowCount;
                }
            }
            else
                this.ResetAutoCompleteFilterCondition();

            return retVal;
        }
            #endregion FilterAutoCompleteItems

            #region IsInAutoCompleteSuggestMode
        internal override EditorWithCombo.AutoCompleteInfo AutoCompleteInfo
        {
            get
            {
                UltraGridComboEditor editor = this.internalEditor != null && this.internalEditor.IsInEditMode ?
                    this.internalEditor :
                    this.externalEditor != null && this.externalEditor.IsInEditMode ?
                    this.externalEditor :
                    null;

                return editor != null ? editor.AutoCompleteModeInfo : null;
            }
        }
            #endregion IsInAutoCompleteSuggestMode

        #endregion FR09238 - AutoCompleteMode

        //  BF 11/25/08 NA 9.1 - UltraCombo MultiSelect
        #region NA 9.1 - UltraCombo MultiSelect

        #region ICheckedItemList Members

            #region CheckBoxStyle
        /// <summary>
        /// Returns whether checkboxes are displayed, and if so, whether they support
        /// an indeterminate state.
        /// </summary>
        CheckStyle ICheckedItemList.CheckBoxStyle
        {
            get
            {
                UltraGridColumn column = this.checkedListSettings != null ? this.CheckStateColumn : null;
                if ( column != null )
                {
                    ColumnStyle columnStyle = column.GetStyleResolved( null );
                    return columnStyle == ColumnStyle.TriStateCheckBox ? CheckStyle.TriState : CheckStyle.CheckBox;
                }

                return CheckStyle.None;
            }
        }
            #endregion CheckBoxStyle

        //    #region CheckStateChanged
        ///// <summary>
        ///// This event is used by the Infragistics internal infrastructure
        ///// and is not intended to be used by your code.
        ///// </summary>
        //[EditorBrowsable(EditorBrowsableState.Never)]
        //public event EventHandler CheckStateChanged;

        //internal void FireCheckStateChanged()
        //{
        //    if ( this.CheckStateChanged != null )
        //        this.CheckStateChanged( this, EventArgs.Empty );
        //}
        //    #endregion CheckStateChanged


            #region CheckStateChanged
        event EditorCheckedListSettings.CheckStateChangedHandler ICheckedItemList.CheckStateChanged
        {
            add{ this.CheckStateChanged += value; }
            remove{ this.CheckStateChanged -= value; }
        }

        /// <summary>
        /// Fires when the CheckState of a ValueListItem has changed.
        /// </summary>
        internal event EditorCheckedListSettings.CheckStateChangedHandler CheckStateChanged;

        internal void FireCheckStateChanged( UltraGridRow row )
        {
            if ( this.CheckStateChanged != null )
                this.CheckStateChanged( this, new EditorCheckedListSettings.CheckStateChangedEventArgs(row) );
        }
            #endregion CheckStateChanged

            #region GetCheckState
        /// <summary>
        /// Returns the check state of the <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow">row</see>
        /// at the specified ordinal position within the control's <see cref="Infragistics.Win.UltraWinGrid.UltraGridBase.Rows">Rows</see>
        /// collection.
        /// </summary>
        /// <param name="index">The index of the row to test.</param>
        CheckState ICheckedItemList.GetCheckState(int index)
        {
            RowsCollection rows = this.Rows;
            if ( index >= 0 && index < rows.Count )
            {
                UltraGridColumn checkStateColumn = this.CheckStateColumn;
                if ( checkStateColumn != null )
                {
                    UltraGridRow row = rows[index];
                    object cellValue = row != null ? row.GetCellValue(checkStateColumn) : null;
                    return CheckedListSettings.ToCheckState( checkStateColumn, cellValue );
                }
            }

            return CheckState.Unchecked;
        }
            #endregion GetCheckState

            #region GetText
        /// <summary>
        /// Returns the string representation of the Value property of this
        /// interface implementation, using the specified <paramref name="listSeparator"/>
        /// as a delimiter.
        /// </summary>
        /// <param name="values">The list of values for which the text is to be returned.</param>
        /// <param name="listSeparator">The character string that appears between the display text of each entry in the list.</param>
        string ICheckedItemList.GetText( IList values, string listSeparator)
        {
            return this.CheckedRows.ToString( values, listSeparator );
        }
            #endregion GetText

            #region SetCheckState
        /// <summary>
        /// Sets the logical check state of the <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow">row</see>
        /// at the specified ordinal position within the control's <see cref="Infragistics.Win.UltraWinGrid.UltraGridBase.Rows">Rows</see>
        /// collection to the specified <paramref name="checkState"/>.
        /// </summary>
        /// <param name="index">The index of the row on which to set the check state.</param>
        /// <param name="checkState">The new check state value.</param>
        void ICheckedItemList.SetCheckState(int index, CheckState checkState)
        {
            UltraGridColumn checkStateColumn = this.CheckStateColumn;
            if ( checkStateColumn != null )
            {
                RowsCollection rows = this.Rows;
                if ( index >= 0 && index < rows.Count )
                {
                    ColumnStyle columnStyle = checkStateColumn.GetStyleResolved( null );

                    //  If the column style does not support an indeterminate state,
                    //  but the value is indeterminate, make it unchecked instead.
                    if ( columnStyle != ColumnStyle.TriStateCheckBox &&
                         checkState == CheckState.Indeterminate )
                        checkState = CheckState.Unchecked;

                    //  Get the current value, and only set the new value if it is
                    //  different, since SetCellValue doesn't check that.
                    UltraGridRow row = rows[index];
                    object currentValue = row.GetCellValue( checkStateColumn );
                    CheckState currentCheckState = CheckedListSettings.ToCheckState( checkStateColumn, currentValue );
                    if ( object.Equals(currentCheckState, checkState) == false )
                    {
                        //  Set the new value
                        this.CheckedListSettings.SetCheckState( row, checkStateColumn, checkState, true, true );

                        //  Changes made by SetCellValue cause the checkbox to be updated
                        //  visually only when the new value is checked, so update the
                        //  row's UIElement in any RSR's which might be displaying it.
                        row.InvalidateCellAllRegions( checkStateColumn, true, true );
                    }
                }
            }
        }
            #endregion SetCheckState

            #region Value
        /// <summary>
        /// Gets/sets the list of values that describe which
        /// <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow">rows</see>
        /// are logically checked.
        /// </summary>
        IList ICheckedItemList.Value
        {
            get
            {
                return this.CheckedRows.Values;
            }
            set
            {
                UltraGridBand bandZero = this.DisplayLayout.BandZero;                
                string valueMember = this.ValueMemberResolved;
                UltraGridColumn valueMemberColumn = bandZero != null && bandZero.Columns.Exists(valueMember) ? bandZero.Columns[valueMember] : null;
                string checkStateMember = this.CheckedListSettings.CheckStateMember;
                UltraGridColumn checkStateMemberColumn = bandZero != null && bandZero.Columns.Exists(checkStateMember) ? bandZero.Columns[checkStateMember] : null;
                if ( checkStateMemberColumn == null || valueMemberColumn == null )
                    return;

                CheckedRowsCollection checkedItems = this.CheckedRows;

                //  If the value was null, normalize and make it an empty list
                //  instead, to simplify the rest of the logic.
                if ( value == null )
                    value = new List<object>(0);

                //  Populate a dictionary so we can look these up quickly                    
                Dictionary<object, object> valueDictionary = new Dictionary<object,object>(value.Count);

                for ( int i = 0, count = value.Count; i < count; i ++ )
                {
                    object val = value[i];

                    //  Note that we want to exclude duplicates here
                    if ( valueDictionary.ContainsKey(val) == false )
                        valueDictionary.Add( val, null );
                }

                //  Compare the new value to the existing one; if they are the same,
                //  we want to avoid triggering a ListChanged event off the data source.
                bool valueChanged = value.Count != checkedRows.Count;
                foreach( UltraGridRow checkedRow in checkedRows )
                {
                    object cellValue = checkedRow.GetCellValue( valueMemberColumn );

                    if ( valueDictionary.ContainsKey(cellValue) == false )
                    {
                        valueChanged = true;
                        break;
                    }
                }

                //  If the new value is no different, return now.
                if ( valueChanged == false )
                    return;

                //  Build a list of the rows that we will be "unchecking"
                List<UltraGridRow> rowsToUncheck = new List<UltraGridRow>( checkedRows.Count );
                foreach( UltraGridRow checkedRow in checkedRows )
                {
                    object cellValue = checkedRow.GetCellValue( valueMemberColumn );

                    if ( valueDictionary.ContainsKey(cellValue) == false )
                        rowsToUncheck.Add( checkedRow );
                }

                //  Build a list of the rows that we will be "checking"
                RowsCollection rows = this.Rows;
                List<UltraGridRow> rowsToCheck = new List<UltraGridRow>( rows.Count );
                foreach( UltraGridRow row in rows )
                {
                    object cellValue = row.GetCellValue( valueMemberColumn );

                    if ( object.Equals(cellValue, null) == false && valueDictionary.ContainsKey(cellValue) )
                        rowsToCheck.Add( row );

                    //  Since we are not supporting duplicates, we can be finished
                    //  when these two counts match.
                    if ( rowsToCheck.Count == valueDictionary.Count )
                        break;
                }

                //  Uncheck the rows that are currently checked but are not in the
                //  new list of values. Note that we pass false for the value of the
                //  'fireDataChanged' parameter, so that we issue only one notification
                //  after all affected rows are checked/unchecked.
                foreach ( UltraGridRow row in rowsToUncheck )
                {
                    this.CheckedListSettings.SetCheckState( row, checkStateMemberColumn, CheckState.Unchecked, false, false );
                }

                //  Check the rows that are in the new list of values.
                foreach ( UltraGridRow row in rowsToCheck )
                {
                    this.CheckedListSettings.SetCheckState( row, checkStateMemberColumn, CheckState.Checked, false, false );
                }

                //  Raise the DataChanged event to notify listeners.
                this.Rows.RaiseDataChangedEvent( checkStateMember );
            }
        }
            #endregion Value

        #endregion ICheckedItemList Members

            #region EditorInEditMode
        internal UltraGridComboEditor EditorInEditMode
        {
            get
            {
                UltraGridComboEditor editor = null;
                IValueListOwner owner = this.Owner;
                if ( owner is UltraGridComboEditor && ((UltraGridComboEditor)owner).IsInEditMode )
                    editor = owner as UltraGridComboEditor;
                else
                {
                    UltraGridComboEditor externalEditor = this.ExternalEditor;
                    UltraGridComboEditor internalEditor = this.InternalEditor;

                    if ( externalEditor != null && externalEditor.IsInEditMode )
                        editor = externalEditor;
                    else
                    if ( internalEditor != null && internalEditor.IsInEditMode )
                        editor = internalEditor;
                }

                return editor;
            }
        }
        #endregion EditorInEditMode

            #region GetCheckedListSettings
        internal EditorCheckedListSettings GetCheckedListSettings()
        {
            UltraGridComboEditor editor = this.EditorInEditMode;
            return this.GetCheckedListSettings( editor );
        }

        internal EditorCheckedListSettings GetCheckedListSettings( EmbeddableEditorBase editor )
        {
            //  Get the associated embeddable element
            EmbeddableUIElementBase embeddableElement = editor != null ? editor.ElementBeingEdited : null;

            if ( embeddableElement == null )
            {
                Debug.Fail( "Could not get an embeddable editor, or editor not in edit mode - GetCheckedListSettings cannot be used in these cases." );
                return null;
            }

            EmbeddableEditorOwnerBase editorOwner = embeddableElement.Owner;
            object ownerContext = embeddableElement.OwnerContext;
            EditorCheckedListSettings checkedListSettings = editorOwner.GetCheckedListSettings(ownerContext);

            return checkedListSettings;
        }
        #endregion GetCheckedListSettings

            #region ProcessMouseUp
        internal override void ProcessMouseUp( MouseMessageInfo msgInfo, out bool closeDropDown )
        {
            closeDropDown = true;

            UltraGridColumn checkStateColumn = this.CheckStateColumn;
            if ( checkStateColumn == null )
                return;

            EditorCheckedListSettings editorCheckedListSettings = this.GetCheckedListSettings();
            if ( editorCheckedListSettings == null )
                return;

            //  Get the UIElement that received the MouseUp
            Point clientPos = new Point( msgInfo.X, msgInfo.Y );
            ComboDropDownControl dropdownControl = this.ControlForGridDisplay as ComboDropDownControl;
            UIElement gridControlElement = dropdownControl != null ? ((IUltraControlElement)dropdownControl).MainUIElement : null;
            UIElement elementAtPoint = gridControlElement != null ? gridControlElement.ElementFromPoint( clientPos ) : null;

            //  Get the CheckBoxUIElement and/or RowUIElement that was clicked
            CheckBoxUIElement checkBoxElement = elementAtPoint != null ? elementAtPoint.GetAncestor( typeof(CheckBoxUIElement) ) as CheckBoxUIElement : null;
            
            //  BF 2/12/09  TFS13942
            //  Just because a CheckBoxUIElement was clicked doesn't mean it
            //  is necessarily the CheckStateMember column.
            UltraGridColumn column = checkBoxElement != null ? checkBoxElement.GetContext( typeof(UltraGridColumn) ) as UltraGridColumn : null;
            
            //  BF 3/3/09   TFS14876
            //  This check only applies when ItemCheckArea is not 'Item'; in that
            //  case, we want to close the dropdown and change the check state.
            //if ( column != checkStateColumn )
            if ( editorCheckedListSettings.ItemCheckArea == ItemCheckArea.CheckBox &&
                 column != checkStateColumn )
                return;

            RowUIElement rowElement = elementAtPoint != null ? elementAtPoint.GetAncestor( typeof(RowUIElement) ) as RowUIElement : null;

            //  If a row was clicked...
            if ( rowElement != null )
            {
                UltraGridRow row = rowElement.Row;

                //  Only close the dropdown if the ItemCheckArea is CheckBox, and some
                //  other part of the row was clicked.            
                closeDropDown =
                    editorCheckedListSettings.ItemCheckArea == ItemCheckArea.CheckBox &&
                    checkBoxElement == null;

                //  Only change the check state if the click was received over the checkbox,
                //  or the ItemCheckArea property is set to 'Item'.
                bool changeCheckState =
                    editorCheckedListSettings.ItemCheckArea == ItemCheckArea.Item ||
                    checkBoxElement != null;

                if ( changeCheckState )
                {
                    ICheckedItemList checkedItemList = this as ICheckedItemList;
                    CheckState currentCheckState = checkedItemList.GetCheckState( row.Index );
                    CheckState nextCheckState = Utilities.GetNextCheckState( currentCheckState, checkedItemList.CheckBoxStyle == CheckStyle.TriState );
                    checkedItemList.SetCheckState( row.Index, nextCheckState );
                }
            }
        }
            #endregion ProcessMouseUp

            #region DirtyCheckedRowsCollection
        internal void DirtyCheckedRowsCollection( bool dirtyAll )
        {
            if ( this.checkedRows != null )
            {
                if ( dirtyAll )
                    this.checkedRows.DirtyAll();
                else
                    this.checkedRows.DirtyCheckedRows();
            }
        }
            #endregion DirtyCheckedRowsCollection

            #region SetSelectedIndexBeforeDroppingDown
        internal override void SetSelectedIndexBeforeDroppingDown(string text, ref int index)
        {
            IValueList valueList = this as IValueList;

            //  If the value is determined by the checked list, preserve the selected index,
            //  i.e., just keep it at whatever it is, without regard to the control's text.
            if ( this.checkedListSettings != null &&
                 this.checkedListSettings.IsValueProvidedByCheckedItemList )
                valueList.SelectedItemIndex = this.selectedRow != null ? this.selectedRow.Index : -1;
            else
                base.SetSelectedIndexBeforeDroppingDown(text, ref index);
        }
            #endregion SetSelectedIndexBeforeDroppingDown
            
            #region RaiseTextChangedEvent
        internal void RaiseTextChangedEvent()
        {
            base.OnTextChanged( EventArgs.Empty );
        }
            #endregion RaiseTextChangedEvent

        //  BF 2/5/09   TFS13564
            #region VerifyValueMember
        internal override void VerifyValueMember( string newValue )
        {            
            if ( this.checkedListSettings != null )
                this.checkedListSettings.VerifyCheckStateColumn( PropertyIds.ValueMember, newValue );
        }
            #endregion VerifyValueMember

        //  BF 2/5/09   TFS13564
        //  After deserialization, verify the ValueMember and CheckStateMember properties
        //  to make sure they are not one and the same.
            #region OnEndInit
        /// <summary>
        /// Invoked during the <see cref="ISupportInitialize.EndInit"/> of the component.
        /// </summary>
        protected override void OnEndInit()
        {
            base.OnEndInit();

            if ( this.checkedListSettings != null )
                this.checkedListSettings.VerifyCheckStateColumn( null, this.ValueMemberResolved );
        }
            #endregion OnEndInit

        #endregion NA 9.1 - UltraCombo MultiSelect

		// MD 3/11/09 - TFS14790
		#region ApplyBoundsConstraints

		private void ApplyBoundsConstraints( ref Size proposedSize )
		{
			Size minimumSize = this.MinimumSize;
			Size maximumSize = this.MaximumSize;

			if ( maximumSize.Width == 0 )
				maximumSize.Width = Int32.MaxValue;

			if ( maximumSize.Height == 0 )
				maximumSize.Height = Int32.MaxValue;

			proposedSize.Width = Math.Max( minimumSize.Width, Math.Min( proposedSize.Width, maximumSize.Width ) );
			proposedSize.Height = Math.Max( minimumSize.Height, Math.Min( proposedSize.Height, maximumSize.Height ) );
		}

		#endregion ApplyBoundsConstraints

		// MD 3/11/09 - TFS14790
		#region GetPreferredSize

		/// <summary>
		/// Used to determine the preferred size for the control
		/// </summary>
		/// <param name="proposedSize">Proposed size</param>
		/// <returns>The preferred size of the control</returns>
		public override Size GetPreferredSize( Size proposedSize )
		{
			bool widthSpecified = true;
			bool heightSpecified = true;

			// Both 0 and 1 are considered unbounded
			if ( proposedSize.Width == 0 || proposedSize.Width == 1 )
			{
				// Using the max int value causes a rounding error when being converted to a float and back to an int during 
				// text measurements, so we have to use a slightly smaller value or the layout area might be negative, forcing 
				// an empty size for the measured string.
				proposedSize.Width = 2000000000;

				widthSpecified = false;
			}

			// Both 0 and 1 are considered unbounded
			if ( proposedSize.Height == 0 || proposedSize.Height == 1 )
			{
				// Using the max int value causes a rounding error when being converted to a float and back to an int during 
				// text measurements, so we have to use a slightly smaller value or the layout area might be negative, forcing 
				// an empty size for the measured string.
				proposedSize.Height = 2000000000;

				heightSpecified = false;
			}

			// Apply the constraints to the proposed size before calculating the preferred size.
			this.ApplyBoundsConstraints( ref proposedSize );

			Size preferredSize = proposedSize;

			if ( widthSpecified == false )
				preferredSize.Width = this.Width;

			if ( this.AutoSize )
				preferredSize.Height = Math.Min( preferredSize.Height, this.IdealHeight );
			else if ( heightSpecified == false )
				preferredSize.Height = this.Height;

			// The preferred size might be outside the control's constraints due to auto-sizing, so apply the constraints again.
			this.ApplyBoundsConstraints( ref preferredSize );

			return preferredSize;
		}

		#endregion GetPreferredSize

        // CDS 6/25/09 TFS18798 9.2 UltraCombo - DropDownButtonDisplayStyle
        #region 9.2 DropDownButtonDisplayStyle

        private const Infragistics.Win.ButtonDisplayStyle defaultDropdownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;

        private Infragistics.Win.ButtonDisplayStyle dropdownButtonDisplayStyle = UltraCombo.defaultDropdownButtonDisplayStyle;

        #region DropDownButtonDisplayStyle

        /// <summary>
        /// Determines when the control's dropdown button should be displayed.
        /// </summary>
        [LocalizedDescription("LD_UltraCombo_P_ButtonDisplayStyle")]
        [LocalizedCategory("LC_Appearance")]
        public Infragistics.Win.ButtonDisplayStyle DropDownButtonDisplayStyle
        {
            get { return this.dropdownButtonDisplayStyle; }
            set
            {
                if (!Enum.IsDefined(typeof(Infragistics.Win.ButtonDisplayStyle), value))
                    throw new InvalidEnumArgumentException("DropDownButtonDisplayStyle", (int)value, typeof(ButtonDisplayStyle));

                #region Set editor property

                if (value != this.InternalEditor.ButtonDisplayStyle)
                {
                    this.InternalEditor.ButtonDisplayStyle = value;

                    if (this.HasExternalEditor)
                        this.ExternalEditor.ButtonDisplayStyle = value;
                }
                #endregion Set editor property

                if (this.dropdownButtonDisplayStyle != value)
                {
                    this.dropdownButtonDisplayStyle = value;

                    this.SafeDirtyChildElements();
                    this.NotifyPropChange(PropertyIds.DropDownButtonDisplayStyle);
                }
            }
        }

        #endregion //DropDownButtonDisplayStyle

        #region ResetDropDownButtonDisplayStyle

        /// <summary>
        /// Resets the <see cref="UltraCombo.DropDownButtonDisplayStyle"/> property to its default value.
        /// </summary>
        /// <remarks>
        /// <p class="body">Invoke this method to reset the <see cref="UltraCombo.DropDownButtonDisplayStyle"/> property to its default value.</p>
        /// <p class="body">Once this method is invoked, the <see cref="UltraCombo.ShouldSerializeDropDownButtonDisplayStyle"/> method will return False until the <see cref="UltraCombo.DropDownButtonDisplayStyle"/> property is set again.</p>
        /// </remarks>
        /// <seealso cref="DropDownButtonDisplayStyle"/>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetDropDownButtonDisplayStyle()
        {
            this.DropDownButtonDisplayStyle = UltraCombo.defaultDropdownButtonDisplayStyle;
        }
        #endregion ResetDropDownButtonDisplayStyle

        #region ShouldSerializeDropDownButtonDisplayStyle
        /// <summary>
        /// <p class="body">Returns a Boolean value that determines whether the <see cref="UltraCombo.DropDownButtonDisplayStyle"/> property is set to its default value.</p>
        /// </summary>
        /// <remarks>
        /// <p class="body">Returns True if the <see cref="UltraCombo.DropDownButtonDisplayStyle"/> property is not set to its default value; otherwise, it returns False.</p>
        /// <p class="body">Invoke the <see cref="UltraCombo.ResetDropDownButtonDisplayStyle"/> method to reset this property to its default value.</p>
        /// </remarks>
        /// <returns>Boolean indicating whether the property should be serialized</returns>
        protected bool ShouldSerializeDropDownButtonDisplayStyle()
        {
            return (this.dropdownButtonDisplayStyle != UltraCombo.defaultDropdownButtonDisplayStyle);
        }

        #endregion ShouldSerializeDropDownButtonDisplayStyle

        #region HasExternalEditor

        /// <summary>
        /// Indicates if the external editor has already been allocated.
        /// </summary>
        protected bool HasExternalEditor { get { return this.externalEditor != null; } }

        #endregion HasExternalEditor

        #endregion 9.2 DropDownButtonDisplayStyle
    }

}
