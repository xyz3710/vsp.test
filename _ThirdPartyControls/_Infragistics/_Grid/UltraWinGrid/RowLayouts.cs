#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;
using Infragistics.Win.Layout;

// SSP 1/30/03 - Row Layout Functionality
// Added RowLayout.cs file.
//

namespace Infragistics.Win.UltraWinGrid
{
	#region RowLayoutsCollection

	/// <summary>
	/// RowLayoutsCollection class.
	/// </summary>
	/// <remarks>
	/// <p><seealso cref="RowLayout"/>, <seealso cref="UltraGridBand.UseRowLayout"/>, <seealso cref="UltraGridColumn.RowLayoutColumnInfo"/></p>
	/// </remarks>
	[ TypeConverter( typeof( RowLayoutsCollectionConverter ) ) ]
	[ Serializable( ) ]
	public class RowLayoutsCollection : KeyedSubObjectsCollectionBase, ISerializable
	{
		#region Private Variables

		private UltraGridBand band = null;

		#endregion // Private Variables

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="band"></param>
		internal RowLayoutsCollection( UltraGridBand band ) : base( )
		{
			if ( null == band )
				throw new ArgumentNullException( "band" );

			this.band = band;
		}

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected RowLayoutsCollection(SerializationInfo info, StreamingContext context)
		{
			this.Clear( );

			// Everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop.
			//
			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( RowLayout ) )
				{
					RowLayout item = (RowLayout)Utils.DeserializeProperty( entry, typeof( RowLayout ), null );

					// Add the item to the collection
					if ( item != null )
						this.InternalAdd( item );
				}
				else
				{
					switch ( entry.Name )
					{
						case "Tag":
							this.DeserializeTag(entry);
							break;
						default:
							Debug.Assert( false, "Invalid entry in RowLayoutsCollection's de-serialization constructor." );
							break;
					}
				}
			}
		}

		#endregion // Constructor

		#region Internal Properties/Methods

		#region Band

		internal UltraGridBand Band
		{
			get
			{
				return this.band;
			}
		}

		#endregion // Band

		#region InitializeFrom
		
		internal void InitializeFrom( RowLayoutsCollection source )
		{
			this.Clear( );

			for ( int i = 0; i < source.Count; i++ )
			{
				RowLayout rowLayout = this.Add( source[i].Key );
				rowLayout.InitializeFrom( source[i] );
			}
		}

		#endregion // InitializeFrom

		#endregion // Internal Properties/Methods

		#region Public Properties/Methods

		#region Indexer

		/// <summary>
		/// indexer (by string key)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowLayout this[ string key ] 
		{
			get
			{
				return (Infragistics.Win.UltraWinGrid.RowLayout)this.GetItem( key );
			}
		}

		/// <summary>
		/// indexer
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowLayout this[ int index ] 
		{
			get
			{
				return (Infragistics.Win.UltraWinGrid.RowLayout)this.GetItem( index );
			}
		}

		#endregion // Indexer

		#region Add

		/// <summary>
		/// Creates a new RowLayout object with the specified key and adds it to the collection. Keys must be unique.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		/// <returns>The new RowLayout.</returns>
		public RowLayout Add( string key )
		{
			this.ValidateKey( key );

			RowLayout rowLayout = new RowLayout( this, key );
			this.InternalAdd( rowLayout );

            // MRS 12/5/2008 - TFS11243
            this.NotifyPropChange(PropertyIds.RowLayouts);

			return rowLayout;
		}

		/// <summary>
        /// Creates a new <see cref="RowLayout"/> object with an empty <see cref="RowLayout.Key"/>  and adds it to the collection.
		/// </summary>
		/// <returns>The new RowLayout</returns>
		public RowLayout Add( )
		{
			return this.Add( "" );
		}

		#endregion // Add

		#region Remove

        /// <summary>
        /// Removes the specified item from the collection.
        /// </summary>
        /// <param name="item">The item to remove from the collection.</param>
		public void Remove( RowLayout item )
		{
			this.InternalRemove( item );

            // MRS 12/5/2008 - TFS11243
            this.NotifyPropChange(PropertyIds.RowLayouts);
		}

		#endregion // Remove

		#region RemoveAt

		/// <summary>
		/// Removes the item at specified index in the collection.
		/// </summary>
		/// <param name="index">The index of the item to remove from the collection.</param>
		public void RemoveAt( int index )
		{
			this.InternalRemoveAt( index );

            // MRS 12/5/2008 - TFS11243
            this.NotifyPropChange(PropertyIds.RowLayouts);
		}

		#endregion // RemoveAt

		#region Clear

		/// <summary>
		/// Clears the collection.
		/// </summary>
		public void Clear( )
		{
			this.InternalClear( );

            // MRS 12/5/2008 - TFS11243
            this.NotifyPropChange(PropertyIds.RowLayouts);
		}

		#endregion // Clear

		#region AddRange

		/// <summary>
		/// Adds a range of row layout objects.
		/// </summary>
		/// <param name="rowLayouts"></param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public void AddRange( RowLayout[] rowLayouts )
		{
			if ( null != this.Band && null != this.Band.Layout && null != this.Band.Layout.Grid &&
				! this.Band.Layout.Grid.DesignMode && ! this.Band.Layout.Grid.Initializing )
			{
				throw new InvalidOperationException( "AddRange is not supported during runtime." );
			}

			for ( int i = 0; null != rowLayouts && i < rowLayouts.Length; i++ )
			{
				if ( null != rowLayouts[i] )
					this.InternalAdd( rowLayouts[i] );
			}

            // MRS 12/5/2008 - TFS11243
            this.NotifyPropChange(PropertyIds.RowLayouts);
		}

		#endregion // AddRange

		#endregion // Public Properties/Methods

		#region Base Overrides

		#region InitialCapacity

		/// <summary>
		/// Property that specifies the initial capacity
		/// of the collection
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return 5;
			}
		}

		#endregion // InitialCapacity

		#region IsReadOnly

		/// <summary>
		/// Returns true if the collection is read-only
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		#endregion // IsReadOnly

		#region OnSubObjectPropChanged

		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			this.NotifyPropChange( PropertyIds.RowLayouts, propChange );
		}

		#endregion // OnSubObjectPropChanged
		
		#region OnDispose

		/// <summary>
		/// Called when the object is being disposed.
		/// </summary>
		protected override void OnDispose( )
		{
			this.Clear( );

			base.OnDispose( );
		}

		#endregion // OnDispose

		#region ToString

		// SSP 8/10/04
		// Overrode ToString to return empty string so the designer doesn't display
		// the type name in the property grid.
		//
		/// <summary>
		/// Always returns empty string.
		/// </summary>
        /// <returns>A System.String that represents the object.</returns>
		public override string ToString( )
		{            
			return string.Empty;
		}

		#endregion // ToString

		#endregion // Base Overrides

		#region ISerializable.GetObjectData

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// Serialize the tag.
			//
			this.SerializeTag( info );

			// Serialize the items. Notice we are working off the List here (accessing the 
			// Count and the indexer off the List rather than this object). That's because
			// we don't want to create RowLayoutColumnInfo objects unless already created.
			//
			for ( int i = 0; i < this.List.Count; i++ )
			{
				RowLayout rowLayout = this.List[i] as RowLayout;

				if ( null != rowLayout )
					Utils.SerializeProperty( info, i.ToString( ), rowLayout );
			}
		}

		#endregion // ISerializable.GetObjectData
	}

	#endregion // RowLayoutsCollection

	#region RowLayoutsCollectionConverter Class

	/// <summary>
	/// RowLayoutsCollectionConverter Class
	/// </summary>
	public class RowLayoutsCollectionConverter : TypeConverter
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public RowLayoutsCollectionConverter( )
		{
		}

        /// <summary>
        /// Returns whether this object supports properties, using the specified context.
        /// </summary>
        /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
        /// <returns>true if <see cref="System.ComponentModel.TypeConverter.GetProperties(System.Object)"/> should be called to find the properties of this object; otherwise, false.</returns>
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return true;
		}

        /// <summary>
        /// Returns a collection of properties for the type of array specified by the
        /// value parameter, using the specified context and attributes.
        /// </summary>
        /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
        /// <param name="value">An <see cref="System.Object"/> that specifies the type of array for which to get properties.</param>
        /// <param name="attributes">An array of type <see cref="System.Attribute"/> that is used as a filter.</param>
        /// <returns>A <see cref="System.ComponentModel.PropertyDescriptorCollection"/> with the properties that are exposed for this data type, or null if there are no properties.</returns>
        public override PropertyDescriptorCollection GetProperties(
			ITypeDescriptorContext context,
			object value,
			Attribute[] attributes )
		{
			PropertyDescriptorCollection props = null;

			RowLayoutsCollection rowLayouts = value as RowLayoutsCollection;

			if ( null != rowLayouts && rowLayouts.Count > 0 )
			{
				PropertyDescriptor [] propArray = new PropertyDescriptor[ rowLayouts.Count ];

				for ( int i = 0; i < propArray.Length; i++ )
				{
					RowLayout rowLayout = rowLayouts[i];

					System.Text.StringBuilder sb = new System.Text.StringBuilder( 20 );

					sb.Append( i );

					string key = rowLayout.Key;

					if ( key != null && key.Length > 0 )
					{
						sb.Append( " - " );
						sb.Append( key );
					}

					propArray[i] = new RowLayoutPropertyDescriptor( rowLayout, sb.ToString() );	
				}

				props = new PropertyDescriptorCollection( propArray );
			}

			return props;
		}
	}

	#endregion // RowLayoutsCollectionConverter Class

	#region RowLayoutPropertyDescriptor Class

	/// <summary>
	/// RowLayoutPropertyDescriptor class.
	/// </summary>
	public class RowLayoutPropertyDescriptor : PropertyDescriptor 
	{
        private RowLayout rowLayout = null ;
        private string name = null ;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="rowLayout">The row layout.</param>
        /// <param name="name">The name of the property.</param>
        public RowLayoutPropertyDescriptor( RowLayout rowLayout, string name ) : base( name, null ) 
		{
            this.rowLayout = rowLayout;
			this.name = name;
        }


		/// <summary>
		/// Gets category
		/// </summary>
        public override string Category 
		{ 
            get 
			{
                return typeof( RowLayout ).Name;
			}
        }
  
		/// <summary>
		/// Gets/SEts resource name
		/// </summary>
        public string ResourceName {
            get { return name ; }
            set { name = value ; }
        }

		/// <summary>
		/// Gets resource value
		/// </summary>
        public object ResourceValue {
            get { return this.rowLayout ; }
        }

		/// <summary>
		/// Gets component type
		/// </summary>
        public override Type ComponentType {
             get {
                return typeof( RowLayout );
            }
        }


		/// <summary>
		/// Return false
		/// </summary>
        public override bool IsReadOnly {
             get {
                return false;
            }
        }


		/// <summary>
		/// Gets property type
		/// </summary>
        public override Type PropertyType {
             get {
                return typeof( RowLayout );
            }
        }


		/// <summary>
        /// Returns true
		/// </summary>
        /// <param name="component">The component to test for reset capability.</param>
        /// <returns>true</returns>
        public override bool CanResetValue(object component) {
            return true ;
        }

		/// <summary>
		/// Returns row layout object
		/// </summary>
        /// <param name="component">The component with the property for which to retrieve the value.</param>
		/// <returns>The row layout object</returns>
        public override object GetValue(object component) {
            return this.rowLayout;
        }

		/// <summary>
		/// Resets the row layout
		/// </summary>
        /// <param name="component">The component with the property value that is to be reset to the default value.</param>
        public override void ResetValue(object component) {
			this.rowLayout.Reset( );
        }

		/// <summary>
		/// Does nothing
		/// </summary>
        /// <param name="component">The component with the property value that is to be set.</param>
        /// <param name="value">The new value.</param>
        public override void SetValue(object component, object value) {
        }

		/// <summary>
        /// Returns the ShouldSerialize of the row layout
		/// </summary>
        /// <param name="component">The component with the property to be examined for persistence.</param>
        /// <returns>The ShouldSerialize of the row layout</returns>
        public override bool ShouldSerializeValue(object component) {
			// AS 2/26/06 Filter Modified Properties
			//return false;
            return this.rowLayout.ShouldSerialize();
        }
    }

	#endregion // RowLayoutPropertyDescriptor Class
}
