#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
    using System.Collections;
	using Infragistics.Shared;
	using Infragistics.Win;
    using System.Collections.Generic;

    /// <summary>
    /// This is a collection of all ChildBand objects that relate
    /// to a specific parent row.
    /// 
    /// For example, if band 0 was 'Customers' and it had 2 child 
    /// bands, say 'Orders' and 'Invoices'. customer 12345 would 
    /// have 2 ChildBand objects, one containing a collection of 
    /// all of its orders and the other containing a collection
    /// of all of its invoices.  
    /// </summary>
    public class ChildBandsCollection : KeyedSubObjectsCollectionBase
    {
        private Infragistics.Win.UltraWinGrid.UltraGridRow parentRow;

		// SSP 3/10/04 - Virtual Binding Related
		//
		private int verifiedBandsVersion = -1;

        // MRS 2/2/2009 - TFS12805
        private int verifiedSortedBandsVersion = int.MaxValue;

        internal ChildBandsCollection( Infragistics.Win.UltraWinGrid.UltraGridRow parentRow )
        {
            if ( null == parentRow )
                throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_45"));

            this.parentRow   = parentRow;
        }
 
		/// <summary>
		/// Returns true
		/// </summary>
        public override bool IsReadOnly { get { return true; } }

        /// <summary>
        /// Returns the parent row. 
        /// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridRow ParentRow
        {
            get
            {
                return this.parentRow;
            }
        }

		// SSP 3/10/04 - Virtual Binding Related
		// Added code for synchronizing child band collections against the column version
		// in case a band is added or removed.
		// Added SynchronizeChildBands method.
		//
		#region Virtual Binding Related

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private UltraGridChildBand ExtractChildBand( ArrayList list, int endIndex, UltraGridBand band )
		private static UltraGridChildBand ExtractChildBand( ArrayList list, int endIndex, UltraGridBand band )
		{
			for ( int i = 0; i <= endIndex; i++ )
			{
				UltraGridChildBand childBand = (UltraGridChildBand)list[i];
				
				if ( null != childBand && childBand.Band == band )
				{
					list[i] = null;
					return childBand;
				}
			}

			return null;
		}
		
		private void SynchronizeChildBands( )
		{
            // MRS 2/2/2009 - TFS12805            
            //if ( this.verifiedBandsVersion == this.parentRow.BandInternal.ColumnsVersion
            //    // Group by rows' child bands do not syncrhonize with any bands collection.
            //    //
            //    || this.parentRow is UltraGridGroupByRow )
            //    return;
            if (this.verifiedBandsVersion == this.parentRow.BandInternal.ColumnsVersion &&
                this.verifiedSortedBandsVersion == this.parentRow.BandInternal.Layout.sortedBandsVersion)
            {
                return;
            }

            if (this.parentRow is UltraGridGroupByRow)
                return;


			UltraGridBand parentBand = this.parentRow.BandInternal;
			if ( null == parentBand.Layout )
				return;
			
			BandsCollection bands = parentBand.Layout.SortedBands;
			this.verifiedBandsVersion = parentBand.ColumnsVersion;

            // MRS 2/2/2009 - TFS12805
            this.verifiedSortedBandsVersion = this.parentRow.BandInternal.Layout.sortedBandsVersion;
			
			int i;
			ArrayList list = this.List;
			int oldBandsEndIndex = list.Count - 1;

			// Start at one because the first band is always the top-most band which is never
			// a child band.
			//
			for ( i = 1; i < bands.Count; i++ )
			{
				UltraGridBand band = bands[i];
				if ( band.ParentBand == parentBand )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//UltraGridChildBand childBand = this.ExtractChildBand( list, oldBandsEndIndex, band );
					UltraGridChildBand childBand = ChildBandsCollection.ExtractChildBand( list, oldBandsEndIndex, band );

					if ( null == childBand )
						childBand = new UltraGridChildBand( this, band );

					list.Add( childBand );
				}
			}

			try
			{
				// Dispose any deleted child bands.
				//
				for ( i = 0; i <= oldBandsEndIndex; i++ )
				{
					if ( null != list[i] )
						((UltraGridChildBand)list[i]).Dispose( );
				}
			}
			finally
			{
				// Remove them from the list.
				//
				list.RemoveRange( 0, 1 + oldBandsEndIndex );
			}
		}

		// SSP 9/13/04 UWC89
		// Overrode the List property so we can synchronize the child bands with the bands
		// collection lazily. Apparently a lot of methods on the KeyedSubObjectsCollectionBase
		// (like Exists) access List property so this is an ideal place to do this.
		//
		/// <summary>
		/// The list that contains the item references 
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		protected override ArrayList List 
		{
			get
			{
				this.SynchronizeChildBands( );

				return base.List;
			}
		}

		#endregion // Virtual Binding Related

		// SSP 3/10/04 - Virtual Binding Related
		// Overrode Count so we can synchronize with the bands collection.
		//
		/// <summary>
		/// Returns the number of elements in the collection.
		/// </summary>
		public override int Count
		{
			get
			{
				this.SynchronizeChildBands( );

				return base.Count;
			}
		}
        
		/// <summary>
		/// indexer 
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridChildBand this[ int index ] 
        {
            get
            {
				// SSP 3/10/04 - Virtual Binding Related
				// Overrode Count so we can synchronize with the bands collection.
				//
				this.SynchronizeChildBands( );

                return (UltraGridChildBand)base.GetItem( index );
            }
        }

        
		/// <summary>
		/// indexer (by string key)
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridChildBand this[ String key ] 
        {
            get
            {
				// SSP 3/10/04 - Virtual Binding Related
				// Overrode Count so we can synchronize with the bands collection.
				//
				this.SynchronizeChildBands( );

                return (UltraGridChildBand)base.GetItem( key );
            }
        }

        
		/// <summary>
		/// indexer (by band object)
        /// </summary>
        public UltraGridChildBand this[ UltraGridBand band ] 
        {
            get
            {
				// SSP 3/10/04 - Virtual Binding Related
				// Overrode Count so we can synchronize with the bands collection.
				//
				this.SynchronizeChildBands( );

                for ( int i = 0; i < this.Count; i++ )
                {
                    if ( this[i].Band == band )
                        return this[i];
                }

                return null;
            }
        }

		// SSP 11/14/03 Add Row Feature
		// Added HasChildRowsInternal method.
		//
		internal bool HasChildRowsInternal( IncludeRowTypes includeRowTypes )
		{
			foreach ( UltraGridChildBand childBand in this )
			{
				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// We have to take into account filter row, summary row etc...
				// Took out TemplateAddRow and added SpecialRows instead.
				// Commented out the original code and added the new code below.
				// 
				// --------------------------------------------------------------------------------------
				if ( childBand.Rows.GetRowCount( includeRowTypes ) > 0 )
					return true;
				
				// --------------------------------------------------------------------------------------
			}
        
			return false;           
		}

        /// <summary>
        /// Returns true if there are any rows in any of the
        /// childband rows collections
        /// </summary>
        public bool HasChildRows
        {
            get
            {
				// SSP 11/14/03 Add Row Feature
				// Moved the existing code into HasChildRowsInternal.
				//
				return this.HasChildRowsInternal( IncludeRowTypes.DataRowsOnly );
            }
        }

		internal void Refresh( RefreshRow action, bool refreshDescendantRows )
		{
            foreach ( UltraGridChildBand childBand in this )
            {
                if ( childBand.HaveRowsBeenInitialized )
					childBand.Rows.Refresh( action, refreshDescendantRows );
            }
		}

        /// <summary>
        /// Returns the first child row (across child bands)
        /// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridRow FirstRow
        {
            get
            {
				// JJD 1/19/02 - UWG940
				// Moved logic into new helper methods
				//
				// SSP 11/11/03 Add Row Feature
				//
				//return this.GetFirstRow( false );
				return this.GetFirstRow( false, IncludeRowTypes.DataRowsOnly );
            }

        }

        /// <summary>
        /// Returns the last child row (across child bands)
        /// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridRow LastRow
        {
            get
            {
				// JJD 1/19/02 - UWG940
				// Moved logic into new helper methods
				//
				// SSP 11/11/03 Add Row Feature
				//
				//return this.GetLastRow( false );
				return this.GetLastRow( false, IncludeRowTypes.DataRowsOnly );
            }
        }

		// JJD 1/19/02 - UWG940
		// Added new helper methods
		//
		// SSP 11/11/03 Add Row Feature
		// Added includeRowTypes parameter.
		//
		//internal UltraGridRow GetFirstRow( bool excludeCardBands )
		internal UltraGridRow GetFirstRow( bool excludeCardBands, IncludeRowTypes includeRowTypes )
		{
            for ( int i = 0; i < this.Count; i++ )
            {
				if ( excludeCardBands && this[i].Band.CardView )
					continue;

				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// We have to take into account filter row, summary row etc...
				// Took out TemplateAddRow and added SpecialRows instead.
				// Commented out the original code and added the new code below.
				// 
				// --------------------------------------------------------------------------------------
				RowsCollection rows = this[i].Rows;
				UltraGridRow row = rows.GetRowAtIndex( 0, includeRowTypes );
				if ( null != row )
					return row;
				
				// --------------------------------------------------------------------------------------
            }

            return null;
		}

		internal UltraGridChildBand GetNextChildBand( UltraGridChildBand band, bool excludeCardBands )
		{
			int i = this.IndexOf(band) + 1;

			if ( i >= 0 && i < this.Count )
			{
				for ( ; i < this.Count; i++ )
				{
					if ( excludeCardBands && this[i].Band.CardView )
						continue;

					return this[i];
				}
			}

            return null;
		}

		internal UltraGridChildBand GetPreviousChildBand( UltraGridChildBand band, bool excludeCardBands )
		{
			int i = this.IndexOf(band) - 1;

			if ( i >= 0 )
			{
	            for ( ; i >= 0; i-- )
				{
					if ( excludeCardBands && this[i].Band.CardView )
						continue;

					return this[i];
				}
			}

            return null;
		}

		// SSP 11/11/03 Add Row Feature
		// Added includeRowTypes parameter.
		//
		//internal UltraGridRow GetLastRow( bool excludeCardBands )
		internal UltraGridRow GetLastRow( bool excludeCardBands, IncludeRowTypes includeRowTypes )
		{
            for ( int i = this.Count - 1; i >= 0; i-- )
            {
				if ( excludeCardBands && this[i].Band.CardView )
					continue;

				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// We have to take into account filter row, summary row etc...
				// Took out TemplateAddRow and added SpecialRows instead.
				// Commented out the original code and added the new code below.
				// 
				// --------------------------------------------------------------------------------------
				RowsCollection rows = this[i].Rows;
				int count = rows.GetRowCount( includeRowTypes );
				if ( count > 0 )
					return rows.GetRowAtIndex( count - 1, includeRowTypes );
                
				// --------------------------------------------------------------------------------------
            }

            return null;
		}

        /// <summary>
        /// Abstract property that specifies the initial capacity
        /// of the collection
        /// </summary>
        protected override int InitialCapacity
        {
            get
            {
				// SSP 9/9/03 UWG2308 - Optimizations
				// Typically there is only 1 child band.
				//
                //return 3;
				return 1;
            }
        }
        
		/// <summary>
        /// The collection as an array of objects
        /// </summary>
        public override object[] All 
        {
			// AS 1/8/03 - fxcop
			// Properties should not be write only - even
			// though we know it wasn't anyway
			get { return base.All; }
			set 
            {
				throw new NotSupportedException();
            }
        }
		internal int InternalAdd( UltraGridChildBand ChildBand ) 
        {
			return base.InternalAdd( ChildBand );
        }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Clears the collection
		//        /// </summary>
		//#endif
		//        internal new void InternalClear() 
		//        {
		//            base.InternalClear();
		//        }

		#endregion Not Used
     
        
		/// <summary>
		/// IEnumerable Interface Implementation
        /// returns a type safe enumerator
		/// </summary>
		/// <returns></returns>
        public ChildBandEnumerator Enumerator // non-IEnumerable version
        {
			get
			{
				return new ChildBandEnumerator(this);
			}
        }

		// SSP 9/30/02 UWG1717
		// Overrode AllowDuplicateKeys property off the KeyedSubObjectsCollectionBase
		// to allow for duplicate keys. Look at UWG1018 test project which has a bands
		// structure with 2 different bands with the same key.
		//
		/// <summary>
		/// Returns true if the collection allows 2 or more items to have the same key value.
		/// </summary>
		/// <remarks>
		/// In BandsCollection, this property is overridden to return true because we do
		/// want to allow different bands with the same keys in the collection.
		/// </remarks>
		public override bool AllowDuplicateKeys
		{
			get
			{
				return true;
			}
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        // SSP 4/23/04 - Virtual Binding Related Optimizations
		//        // Added GetFirstVisibleRow and GetLastVisibleRow methods.
		//        //
		//        #region GetFirstVisibleRow

		//#if DEBUG
		//        /// <summary>
		//        /// Gets the first visible immediate child row.
		//        /// </summary>
		//        /// <returns></returns>
		//#endif
		//        // SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
		//        // We have to take into account filter row, summary row etc...
		//        // Got rid of IncludeRowTypes parameter from GetLastVisibleChildRow as it wasn't needed.
		//        // Every place we were calling it with IncludeRowTypes.TemplateAddRow as the param.
		//        // 
		//        //internal UltraGridRow GetFirstVisibleRow( IncludeRowTypes includeRowTypes )
		//        internal UltraGridRow GetFirstVisibleRow( )
		//        {
		//            for ( int i = 0; i < this.Count; i++ )
		//            {
		//                UltraGridChildBand childBand = this[i];
		//                if ( null == childBand || childBand.Band.HiddenResolved )
		//                    continue;

		//                UltraGridRow row = childBand.Rows.GetFirstVisibleRow( );

		//                // SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
		//                // We have to take into account filter row, summary row etc...
		//                // Got rid of IncludeRowTypes parameter from GetLastVisibleChildRow as it wasn't needed.
		//                // Every place we were calling it with IncludeRowTypes.TemplateAddRow as the param.
		//                // 
		//                //if ( null != row && IncludeRowTypes.TemplateAddRow != includeRowTypes && row.IsTemplateAddRow )
		//                //	row = childBand.Rows.GetNextVisibleRow( row );

		//                if ( null != row )
		//                    return row;
		//            }

		//            return null;
		//        }

		//        #endregion // GetFirstVisibleRow

		#endregion Not Used

		#region GetLastVisibleRow

		// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
		// We have to take into account filter row, summary row etc...
		// Got rid of IncludeRowTypes parameter from GetLastVisibleChildRow as it wasn't needed.
		// Every place we were calling it with IncludeRowTypes.TemplateAddRow as the param.
		// 
		//internal UltraGridRow GetLastVisibleRow( IncludeRowTypes includeRowTypes )
		internal UltraGridRow GetLastVisibleRow( )
		{
			for ( int i = this.Count - 1; i >= 0; i-- )
			{
				UltraGridChildBand childBand = this[i];
				if ( null == childBand || childBand.Band.HiddenResolved )
					continue;
				
				UltraGridRow row = childBand.Rows.GetLastVisibleRow( );

				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// We have to take into account filter row, summary row etc...
				// Got rid of IncludeRowTypes parameter from GetLastVisibleChildRow as it wasn't needed.
				// Every place we were calling it with IncludeRowTypes.TemplateAddRow as the param.
				// 
				//if ( null != row && IncludeRowTypes.TemplateAddRow != includeRowTypes && row.IsTemplateAddRow )
				//	row = childBand.Rows.GetPrevVisibleRow( row );

				if ( null != row )
					return row;
			}

			return null;
		}
		
		#endregion // GetLastVisibleRow

		#region OnDispose

		// SSP 9/9/03 UWG2308
		// Override OnDispose.
		//
		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnDispose( )
		{
			for ( int i = 0; i < this.List.Count; i++ )
			{
				UltraGridChildBand childBand = this.List[i] as UltraGridChildBand;

				if ( null != childBand )
					childBand.Dispose( );
			}

			base.OnDispose( );
		}

		#endregion // OnDispose

		// SSP 2/13/04 - Virtual Mode - Optimization
		//
		#region Virtual Mode - Scroll Position Related Optimizations

		internal int ScrollCount
		{
			get
			{
                // MBS 4/24/09 - TFS12665
                // As an optimization, if we know where we're in a safe state to cache data
                // (i.e. we're regenerating all the rows and will ask for this property many times
                // during the same operation), then we can cache this value so that we don't need
                // to walk down the child hierarchy for every single row.  The parent will do this 
                // once, then each time the child needs this property, it will have already been
                // cached by the parent.
                Dictionary<ChildBandsCollection, UltraGridLayout.CachedChildBandsInfo> cachedInfo = null;
                UltraGridLayout.CachedChildBandsInfo childBandsInfo = null;
                if (this.ParentRow != null && this.ParentRow.Layout != null && this.ParentRow.Layout.IsCachingChildBandsInfo)
                {
                    cachedInfo = this.ParentRow.Layout.CachedChildBandsData;
                    if (cachedInfo.TryGetValue(this, out childBandsInfo) && childBandsInfo.ScrollCount > -1)
                        return childBandsInfo.ScrollCount;
                }

				int scrollCount = 0;

				for ( int i = 0, count = this.Count; i < count; i++ )
				{
					UltraGridChildBand childBand = this[i];
				
					if ( ! childBand.Band.HiddenResolved )
						scrollCount += childBand.Rows.ScrollCount;
				}

                // MBS 4/24/09 - TFS12665
                if (cachedInfo != null)
                {
                    if (childBandsInfo != null)
                        childBandsInfo.ScrollCount = scrollCount;
                    // We need to check to see if we've already cached the value at this point, since it's possible that we
                    // cached it when requesting the ScrollCount
                    else
                    {
                        if (!cachedInfo.TryGetValue(this, out childBandsInfo))
                        {
                            childBandsInfo = new UltraGridLayout.CachedChildBandsInfo();
                            cachedInfo.Add(this, childBandsInfo);
                        }                        
                        childBandsInfo.ScrollCount = scrollCount;                        
                    }
                }

				return scrollCount;
			}
		}

		internal UltraGridRow GetItemAtScrollIndex( int scrollIndex )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				UltraGridChildBand childBand = this[i];
				
				if ( ! childBand.Band.HiddenResolved )
				{
					int scrollCount = childBand.Rows.ScrollCount;
					if ( scrollIndex < scrollCount )
						return childBand.Rows.GetRowAtScrollIndex( scrollIndex );
					else
						scrollIndex -= scrollCount;
				}
			}

			Debug.Assert( false );
			if ( null != this.ParentRow )
				this.ParentRow.NotifyScrollCountChanged( );

			return null;
		}

		#endregion // Virtual Mode - Scroll Position Related Optimizations

		// JJD 01/07/04 - Added Accessibility support
		#region GetAccessibleChild

		internal AccessibleObject GetAccessibleChild(int index)
		{

			foreach ( UltraGridChildBand cb in this )
			{
				int count = cb.GetAccessibleChildCount();

				if ( index >= 0 && index < count )
					return cb.GetAccessibleChild( index );

				index -= count;
			}

			return null;
		}

		#endregion GetAccessibleChild

		// JJD 01/07/04 - Added Accessibility support
		#region GetAccessibleChildCount

		internal int GetAccessibleChildCount()
		{
			int count = 0;

			foreach ( UltraGridChildBand cb in this )
				count += cb.GetAccessibleChildCount();

			return count;
		}

		#endregion GetAccessibleChildCount

		#region Exists

		// SSP 9/15/04
		// Added Exists method.
		//
		internal bool Exists( UltraGridBand band )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				if ( band == this[i].Band )
					return true;
			}

			return false;
		}

		#endregion // Exists
    }

	#region ChildBandEnumerator class

    /// <summary>
    /// Enumerator for the ChildBandsCollection
    /// </summary>
    public class ChildBandEnumerator: DisposableObjectEnumeratorBase
    {   
		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="ChildBands">The ChildBandsCollection to enumerate</param>
        public ChildBandEnumerator( ChildBandsCollection ChildBands ) : base( ChildBands )
        {
        }

       
		/// <summary>
		/// Type-safe version of Current
		/// </summary>
        public UltraGridChildBand Current 
        {
            get
            {
                return (UltraGridChildBand)((IEnumerator)this).Current;
            }
        }
    }

	#endregion ChildBandEnumerator class
}
