#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	internal class ViewStyleOutlookGroupBy : ViewStyleVertical
	{
		internal ViewStyleOutlookGroupBy( Infragistics.Win.UltraWinGrid.UltraGridLayout layout ) : base( layout )
		{
		}



		internal override bool IsOutlookGroupBy 
		{ 
			get
			{
				return true; 
			}
		}

		
		internal override void AdjustBandOrigin( UltraGridBand band, 
			UltraGridBand priorBand,
			ref int origin )
		{	
			if ( band.ParentBand != null )
				origin = band.ParentBand.GetOrigin() + 
					band.ParentBand.PreRowAreaExtent;
			else
				origin = 0;

			int groupByIndent = 0;

			if ( band.HasGroupBySortColumns )
			{
				// JJD 1/23/02 - UWG971
				// Moved groupby indent calculation logic into helper method
				//
				groupByIndent = this.CalculateGroupByIndent( band );

			}

			origin += groupByIndent;
		}

		internal override void AdjustExclusiveMetrics( Infragistics.Win.UltraWinGrid.ColScrollRegion csr,
			HeaderBase header,
			HeaderBase priorHeader)
		{
			int nOrigin = 0;

			// on a band break recalc the origin 
			//
			if ( header.FirstItem )
			{
				// JJD 1/25/02 - UWG971
				// We want to add the groupby indent but only for the first item
				//
				nOrigin = this.CalculateGroupByIndent( header.Band );

				// if there is a parent band indent so that this child band starts indented
				// by VSVERTICAL_BAND_OFFSET pixels
				// 
				if ( header.Band.ParentBand != null )
					nOrigin += header.Band.ParentBand.GetOrigin() + 
						// SSP 6/13/05 BR04558
						// Seems like the intention here was to calculate the band origin. Changed the
						// following code.
						// 
						// ----------------------------------------------------------------------------
						header.Band.ParentBand.PreRowAreaExtent;
						
						// ----------------------------------------------------------------------------
			}
			else
			{
				if ( priorHeader != null )
					nOrigin += priorHeader.Extent +  //pPriorPositionItem->GetOverallWidth() +
						priorHeader.Origin; //pPriorPositionItem->GetRelativeOrigin();
			}

			this.SetExclusiveMetrics(header, nOrigin);		    
		}


		/// <summary>
		/// Sets the row's top, tier and has header flag when we are fetching
		/// forward. 
		/// </summary>
		protected override void OrientVisibleRowForward ( ref VisibleRowFetchRowContext context, 
			VisibleRow visibleRow )
		{
			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
			HeaderPlacement headerPlacement = visibleRow.Band.HeaderPlacementResolved;
			bool isGroupByRow = visibleRow.Row.IsGroupByRow;

			if ( context.lastRowInserted == null )
			{
				visibleRow.Tier = 0;

				// SSP 7/18/05 - NAS 5.3 Header Placement
				// Now the vertical view style can also have fixed headers.
				// 
				// --------------------------------------------------------------------------
				//visibleRow.SetTop( context.rowScrollRegion.ScrollOffset );
				//visibleRow.HasHeader = !( visibleRow.Row is UltraGridGroupByRow );
				visibleRow.SetTop( this.GetFixedHeaderHeight() + context.rowScrollRegion.scrollOffset );
				this.SetHasHeaderHelper( ref context, visibleRow, ! isGroupByRow 
					|| HeaderPlacement.OncePerGroupedRowIsland == headerPlacement );
				// --------------------------------------------------------------------------

				visibleRow.PostRowSpacing = 0;

				// JJD 3/28/00
				// Set the special flags here
				//
				visibleRow.SpecialFlags = this.CalculateRowSpecialFlags ( visibleRow, context.lastRowInserted );
				return;
			}

			// bump the tier
			//
			visibleRow.Tier = context.lastRowInserted.Tier + 1;

			// check if this row is a sibling of the last row
			//
			bool sameBand = ( visibleRow.Band == context.lastRowInserted.Band );

			// set the has header flag 
			//
			// SSP 8/21/03
			// 
			bool hasHeader;

			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
			// ----------------------------------------------------------------------------
			
			if ( HeaderPlacement.OncePerGroupedRowIsland == headerPlacement )
				hasHeader = ! sameBand;
			else
				hasHeader = ! isGroupByRow 
					&& ( ! sameBand || visibleRow.Row.ParentRow != context.lastRowInserted.Row.ParentRow );

			this.SetHasHeaderHelper( ref context, visibleRow, hasHeader );
			// ----------------------------------------------------------------------------
            
			int lastRowPostRowSpacing = 0;

			// SSP 7/11/05 - NAS 5.3 Empty Rows
			// Don't leave inter-band spacing between the last non-root row from the 
			// root band and the empty rows that follow it. Enclosed the existing code 
			// into the else block.
			// 
			// SSP 7/18/05
			// Interband spacing doesn't apply to summary rows either.
			// 
			//if ( ! sameBand || ! visibleRow.Row.IsEmptyRow )
			if ( ! sameBand || ! visibleRow.Row.IsEmptyRow && ! visibleRow.Row.IsSummaryRow )
			{
				// SSP 8/21/03
				// If the headers are not visible then HasHeader will return false (look inside the
				// property's get). So even though we set the property to true above, it might return
				// false here. Intead use a local variable.
				//
				//if ( visibleRow.HasHeader )
				if ( hasHeader )
				{
					if ( !( context.lastRowInserted.Row  is UltraGridGroupByRow ) || 
						context.lastRowInserted.Row != visibleRow.Row.ParentRow )
						lastRowPostRowSpacing =	this.Layout.InterBandSpacing;				
				}
				else
				{
					if ( !( context.lastRowInserted.Row is UltraGridGroupByRow ) )
					{
						if ( visibleRow.Row.ParentRow == context.lastRowInserted.Row )
							lastRowPostRowSpacing = this.Layout.InterBandSpacing;
					}
				}
			}


			// set the PostRowSpacing
			//
			context.lastRowInserted.PostRowSpacing = //( sameBand ? 0 : this.Layout.InterBandSpacing );
				lastRowPostRowSpacing;

			// set the top 
			//
			visibleRow.SetTop( context.lastRowInserted.GetTop()            + 
				context.lastRowInserted.GetTotalHeight()    + 
				context.lastRowInserted.PostRowSpacing );

			SetNextCrossBandSibling( ref context, visibleRow );

			// JJD 3/28/00
			// Set the special flags here
			//
			visibleRow.SpecialFlags = this.CalculateRowSpecialFlags ( visibleRow, context.lastRowInserted );
		}

		/// <summary>
		/// Sets the row's top, tier and has header flag when we are fetching
		/// backward. 
		/// </summary>
		protected override void OrientVisibleRowBackward ( ref VisibleRowFetchRowContext context, 
			VisibleRow visibleRow )
		{
			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
			HeaderPlacement headerPlacement = visibleRow.Band.HeaderPlacementResolved;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//bool canGroupByRowHaveHeader = HeaderPlacement.OncePerGroupedRowIsland == headerPlacement;

			if ( context.lastRowInserted == null )
			{
				// set tier to arbitrary high number since after we fetch bacward we 
				// always do a fixup going forward that will set the tier back to zero
				//
				visibleRow.Tier = 5000;
				visibleRow.SetTop( context.rowScrollRegion.Extent - visibleRow.GetTotalHeight() );
				if ( visibleRow.Band.GetTotalHeaderHeight() >= visibleRow.GetTop() + context.rowScrollRegion.ScrollOffset )
					visibleRow.SetTop( context.rowScrollRegion.ScrollOffset );

				// SSP 7/18/05 - NAS 5.3 Header Placement
				// 
				//visibleRow.HasHeader = ( 0 >= visibleRow.GetTop() );
				this.SetHasHeaderHelper( ref context, visibleRow, ( 0 >= visibleRow.GetTop() ) );

				visibleRow.PostRowSpacing = 0;
                
				return;
			}

			// decrement the tier
			//
			visibleRow.Tier = context.lastRowInserted.Tier - 1;

			// check if this row is a sibling of the last row
			//
			bool sameBand = ( visibleRow.Band == context.lastRowInserted.Band );

			// set the has header flag on the previously inserted row
			//
			// SSP 8/21/03
			//
			bool hasHeader;

			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
			// ----------------------------------------------------------------------------
			
			if ( HeaderPlacement.OncePerGroupedRowIsland == headerPlacement )
				hasHeader = ! sameBand;
			else
				hasHeader = ! context.lastRowInserted.Row.IsGroupByRow 
					&& ( ! sameBand || visibleRow.Row.ParentRow != context.lastRowInserted.Row.ParentRow );

			this.SetHasHeaderHelper( ref context, context.lastRowInserted, hasHeader );
			// ----------------------------------------------------------------------------
				  
				//( visibleRow.Row is GroupByRow );

			// if we set the has header to true on the previous row we need to
			// adjust its top value to allow for that header
			//
			if ( context.lastRowInserted.HasHeader )
				context.lastRowInserted.SetTop( context.lastRowInserted.GetTop() - context.lastRowInserted.Band.GetTotalHeaderHeight() );
            
			int rowPostRowSpacing = 0;

			// SSP 7/11/05 - NAS 5.3 Empty Rows
			// Don't leave inter-band spacing between the last non-root row from the 
			// root band and the empty rows that follow it. Enclosed the existing code 
			// into the else block.
			// 
			// SSP 7/18/05 - NAS 5.3 Empty Rows
			// Interband spacing doesn't apply to summary rows either.
			// 
			//if ( ! sameBand || ! visibleRow.Row.IsEmptyRow )
			if ( ! sameBand || ! visibleRow.Row.IsEmptyRow && ! visibleRow.Row.IsSummaryRow )
			{
				// SSP 8/21/03
				// If the headers are not visible then HasHeader will return false (look inside the
				// property's get). So eventhogh we set the property to true above, it might return
				// false here. Intead use a local variable.
				//
				//if ( context.lastRowInserted.HasHeader )
				if ( hasHeader )
				{
					if ( !( visibleRow.Row  is UltraGridGroupByRow ) || 
						context.lastRowInserted.Row.ParentRow != visibleRow.Row )
						rowPostRowSpacing =	this.Layout.InterBandSpacing;
				}
				else
				{
					if ( !( context.lastRowInserted.Row is UltraGridGroupByRow ) )
					{
						if ( visibleRow.Row.ParentRow == context.lastRowInserted.Row )
							rowPostRowSpacing = this.Layout.InterBandSpacing;
					}
				}
			}

			// set the PostRowSpacing 
			//
			visibleRow.PostRowSpacing = //( sameBand ? 0 : this.Layout.InterBandSpacing );
				rowPostRowSpacing;
				

			// set the top 
			//
			visibleRow.SetTop( context.lastRowInserted.GetTop() - 
				( visibleRow.GetTotalHeight() + visibleRow.PostRowSpacing ) ) ;

			if ( 0 >= visibleRow.GetTop() + context.rowScrollRegion.ScrollOffset )
			{
				// The region has been filled so set this row's top to zero and
				// set its 'hasheader' flag to true
				//
				visibleRow.SetTop( context.rowScrollRegion.ScrollOffset );

				// SSP 7/18/05 - NAS 5.3 Header Placement
				// 
				//visibleRow.HasHeader = true;
				this.SetHasHeaderHelper( ref context, visibleRow, true );
			}
			else
			{
				// The region has not yet been filled so assume no header set this row's top to zero and
				// set its 'hasheader' flag to true
				//
				// SSP 7/18/05 - NAS 5.3 Header Placement
				// 
				//visibleRow.HasHeader = false;
				this.SetHasHeaderHelper( ref context, visibleRow, false );
			}
		}

		/// <summary>
		/// Calculates the special flags needed to draw the row and cells
		/// properly. These flags include whether the row is responsible for
		/// its top bnorder, et al.
		/// </summary>
		protected override Infragistics.Win.UltraWinGrid.ViewStyleSpecialFlags CalculateRowSpecialFlags ( VisibleRow visibleRow, VisibleRow lastVisibleRow )
		{
			Infragistics.Win.UltraWinGrid.ViewStyleSpecialFlags specialFlags = base.CalculateRowSpecialFlags( visibleRow, lastVisibleRow );
			
			specialFlags &= ~ViewStyleSpecialFlags.SkipTopLevelHierarchyLines;
			specialFlags |= ViewStyleSpecialFlags.DrawLongParentConnector;

			return specialFlags;
		}


		internal override bool SupportsGroupByRows
		{
			get
			{
				return true;
			}
		}

	}
}
