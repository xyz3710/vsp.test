#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

	/// <summary>
	///	UIElement that displays the band caption in the <see cref="Infragistics.Win.UltraWinGrid.GroupByBox"/>
	/// </summary>
	public class GroupByBandLabelUIElement : UIElement
	{   
		#region Member Variables

		// AS 1/7/04 accessibility
		private AccessibleObject			accessibleObject;

		private GroupByBox.ButtonDefinition buttonDef;

		#endregion //Member Variables

		internal GroupByBandLabelUIElement( UIElement parent, GroupByBox.ButtonDefinition buttonDef ) : base( parent )
		{
			this.init( buttonDef );
		}

		internal void init( GroupByBox.ButtonDefinition buttonDef )
		{
			this.buttonDef = buttonDef;
			
			TextUIElement textElement = (TextUIElement)GroupByBandLabelUIElement.ExtractExistingElement( 
				this.childElementsCollection, typeof( TextUIElement ), true );

			if ( null != textElement )
				textElement.Text = buttonDef.Caption;
		}

		/// <summary>
		/// <see cref="Infragistics.Win.UIElementBorderStyle"/>
		/// </summary>
		public override Infragistics.Win.UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.GroupByBox.BandLabelBorderStyleResolved;
			}
		}

		internal GroupByBox GroupByBox
		{
			get
			{
				return (GroupByBox)this.Parent.GetContext( typeof( GroupByBox ), true );
			}
		}

		internal UltraGridBand Band
		{
			get
			{
				Debug.Assert( null != this.buttonDef.band );

				return this.buttonDef.band;
			}
		}


        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			this.GroupByBox.ResolveBandLabelAppearance( ref appearance, requestedProps );
			
			//RobA UWG213 8/27/01
			requestedProps &= ~AppearancePropFlags.FontData;			
		}

		

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			UIElementsCollection oldElements = this.childElementsCollection;

			this.childElementsCollection = null;

			TextUIElement textElement = (TextUIElement)GroupByBandLabelUIElement.ExtractExistingElement( 
				oldElements, typeof( TextUIElement ), true );

			// SSP 10/9/02 UWG1522
			// Added support for displaying images.
			//
			// ----------------------------------------------------------------------------------------------
			ImageUIElement imageElement = (ImageUIElement)GroupByBandLabelUIElement.ExtractExistingElement( 
				oldElements, typeof( ImageUIElement ), true );

			Rectangle textRect = this.RectInsideBorders;

			AppearanceData appData = new AppearanceData( );

			AppearancePropFlags flags = 
				AppearancePropFlags.Image | 
				AppearancePropFlags.ImageAlpha | 
				AppearancePropFlags.ImageHAlign | 
				AppearancePropFlags.ImageVAlign;

			this.InitAppearance( ref appData, ref flags );

			Image image = null;
			Size imageSize = Size.Empty;
			this.GroupByBox.CalcDisplayedImageSize( appData, textRect.Height, out image, out imageSize );

			if ( image != null )
			{
				if ( null == imageElement )
				{
					imageElement = new ImageUIElement( this, image );
				}
				else
				{
					imageElement.Image = image;
				}

				imageElement.Scaled = true;

				// make sure the width isn't too big
				//
				if ( textRect.Width > 2 && imageSize.Width > textRect.Width - 2 )
					imageSize.Width = textRect.Width - 2;

				Rectangle imgRect	= textRect;
				imgRect.Width		= imageSize.Width;
				imgRect.Height		= imageSize.Height;

				//move image based on alignment
				DrawUtility.AdjustHAlign( appData.ImageHAlign, ref imgRect, textRect ); 
				DrawUtility.AdjustVAlign( appData.ImageVAlign, ref imgRect, textRect );

				// make sure the imgae has at least a 1 pixel 
				// padding on top and bottom
				//
				if ( imgRect.Top == textRect.Top )
					imgRect.Y++;
				else
					if ( imgRect.Bottom == textRect.Bottom )
					imgRect.Y--;


				// adjust the text rect appropriately
				//
				switch ( appData.ImageHAlign )
				{
					case HAlign.Center:
						break;

					case HAlign.Right:
						imgRect.X--;
						textRect.Width -= imgRect.Width + 1;
						break;

					default:
					case HAlign.Left:
						imgRect.X++;
						textRect.Width	-= imgRect.Width + 1;
						textRect.X		+= imgRect.Width + 1;
						break;
				}

				imageElement.Rect = imgRect;

				this.ChildElements.Add( imageElement );
			}

			// ----------------------------------------------------------------------------------------------

			if ( null == textElement )
				textElement = new TextUIElement( this, this.buttonDef.Caption );

			textElement.Text = this.buttonDef.Caption;

			// SSP 10/9/02 UWG1522
			// Added support for displaying images.
			//
			
			textElement.Rect = textRect;

			this.ChildElements.Add( textElement );
		}

		// AS 1/7/04 Accessibility
		#region IsAccessibleElement
		/// <summary>
		/// Indicates if the element supports accessibility.
		/// </summary>
		public override bool IsAccessibleElement
		{
			get { return true; }
		}
		#endregion //IsAccessibleElement

		#region AccessibilityInstance
		/// <summary>
		/// Returns the accessible object associated with the element.
		/// </summary>
		/// <remarks>
		/// <p class="note"><b>Note</b> Derived elements that plan to return an accessible object must override 
		/// the <see cref="IsAccessibleElement"/> member.</p>
		/// </remarks>
		public override AccessibleObject AccessibilityInstance
		{
			get 
			{ 
				if (this.accessibleObject == null)
					this.accessibleObject = this.Band.Layout.Grid.CreateAccessibilityInstance(this);

				return this.accessibleObject; 
			}
		}
		#endregion //AccessibilityInstance

		#region GroupByBandlLabelAccessibleObject
		/// <summary>
		/// Accessible object representing a <see cref="GroupByBandLabelUIElement"/> in the <see cref="Infragistics.Win.UltraWinGrid.GroupByBox"/>
		/// </summary>
		public class GroupByBandlLabelAccessibleObject : UIElementAccessibleObject
		{
			#region Constructor
			/// <summary>
			/// Initializes a new <see cref="GroupByBandlLabelAccessibleObject"/>
			/// </summary>
			/// <param name="element">Associated element</param>
			public GroupByBandlLabelAccessibleObject(GroupByBandLabelUIElement element) : base(element, AccessibleRole.StaticText)
			{
			}
			#endregion //Constructor

			#region Properties

			private GroupByBox.ButtonDefinition Button
			{
				get { return ((GroupByBandLabelUIElement)this.UIElement).buttonDef; }
			}

			#endregion //Properties

			#region Base class overrides

			#region Name
			/// <summary>
			/// Returns the name of the accessible object.
			/// </summary>
			public override string Name
			{
				get
				{
					string name = base.Name;

					if (name == null)
						name = Infragistics.Win.Utilities.StripMnemonics(this.Button.Caption);

					return name;
				}
				set
				{
					base.Name = value;
				}
			}
			#endregion //Name

			#endregion //Base class overrides
		}
		#endregion //AddNewButtonAccessibleObject

	}
	
}
