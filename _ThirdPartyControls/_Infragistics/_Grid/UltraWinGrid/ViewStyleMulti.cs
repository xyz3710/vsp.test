#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	internal abstract class ViewStyleMultiBase : ViewStyleBase
	{
		internal ViewStyleMultiBase( Infragistics.Win.UltraWinGrid.UltraGridLayout layout ) : base( layout )
		{
		}
		internal override bool IsMultiBandDisplay 
		{ 
			get
			{
				return true; 
			}
		}


		// SSP 8/22/01
		internal override bool IsLastRow ( UltraGridRow row )
		{
			// for multi band display we need to check for a next sibling,
			// children and rows in a next sibling band
			//   
			
			// SSP 11/11/03 Add Row Feature
			//
			//bool hasNext = row.HasNextSibling(false, true) ||
			//    ( row.Expanded && row.HasChild(true) ) ||
			bool hasNext = row.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) ||				
				( row.Expanded && row.HasChild( true, IncludeRowTypes.SpecialRows ) ) ||
				row.HasSiblingInNextSiblingBand(true);

			if ( hasNext )
				return false;
			
			UltraGridRow parentRow = row.ParentRow;

			// walk up the parent chain checking to see if any of our
			// parent's have next siblings
			//
			while ( null != parentRow )
			{
				// SSP 11/11/03 Add Row Feature
				//
				//hasNext =  parentRow.HasNextSibling(false, true) ||
				hasNext =  parentRow.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) ||
					parentRow.HasSiblingInNextSiblingBand(true);

				if ( hasNext )
					return false;

				parentRow = parentRow.ParentRow;
			}

			// we haven't found a next logical row so we must be the last
			//
			return true;
		}

		// SSP 1/6/04
		// Overrode LastRow to take into account potentially multiple bands. I noticed
		// this while I was fixing UWG2833.
		//
		internal override UltraGridRow LastRow
		{
			get
			{	
				UltraGridRow returnValue = null;

				// Get the last row in band 0.( base.LastRow does exactly that so use that
				// since it's more convenient ).
				//
				UltraGridRow row = base.LastRow;				

				// Now if the descendants are visible, then get the last of the descendants.
				// 
				while ( null != row )
				{
					returnValue = row;
					row = row.GetNextVisibleRow( );
				}

				return returnValue;
			}			
		}

	}
}
