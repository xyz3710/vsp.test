#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

	#region RowUIElementBase Class

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Split the RowUIElement into RowUIElementBase class and RowUIElement class so the filter
	// row element can derive from RowUIElementBase.
	//
	/// <summary>
	/// The DataAreaUIElement contains the row and column scrolling
	/// regions.
	/// </summary>
	public abstract class RowUIElementBase : AdjustableUIElement
	{
		#region Private/Internal Vars

		private bool hookedIntoNotifications = false;
		private bool overlapTopBorder;

		// SSP 5/16/05 - Optimization
		// Row col region intersection element had an optimization for not causing the existing
		// row elements to reposition their descendants. It never worked so made it work.
		//
		//private Size origParentSize;
		internal Rectangle lastClipRect;

		private bool inFirstCardAreaCol	= false;
		private bool inFirstCardAreaRow	= false;
		private bool inLastCardAreaCol	= false;

		// SSP 5/14/03 - Optimizations
		// Embeddable owner gets the row from cell ui element quite a number of times. To
		// speed it up, it would be better if the row was stored in a member variable of the
		// row ui element.
		//
		internal UltraGridRow contextRow = null;

		// SSP 11/22/04 - Merged Cell Feature
		// This flag is set by the RowColRegionIntersectionUIElement.PositionMergedCellElements.
		// Indicates if this row element has merged cells over it.
		//
		internal bool hasMergedCells = false;

		// SSP 11/30/04 - Merged Cell Feature
		// Added overlappedWithHeader flag which indicates whether the row's top is overlapped
		// with the band headers. NOTE: Band header element is added after the row element even
		// if the row element visually appears after the headers. What this means is that the
		// headers will overwrite the top row border.
		//
		internal bool topBorderOverlappedWithHeaders = false;

		// SSP 3/16/05 - NAS 5.2 Filter Row
		// Added rowBorderStyle member var.
		//
		internal UIElementBorderStyle rowBorderStyle = UIElementBorderStyle.Default;
		
		// SSP 3/24/05 BR02974
		// While performing pixel level scrolling (by holding down the mouse over the down
		// scroll arrow), the row element may end up being underneath the header elements.
		// If there was alphablending on headers then the row would show through. Also in
		// row layout mode with gaps the row would show through as well. To fix it we need
		// to set the row element's region to exclude the band headers area element rect.
		//
		private Rectangle clipSelfRect = Rectangle.Empty;

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal bool isMouseOverSelectorOrCellArea = false;

		#endregion // Private/Internal Vars

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		public RowUIElementBase( UIElement parent ) : base( parent, true, false )
		{
		}

		#endregion // Constructor

		#region Private/Internal Methods/Properties

		#region InitializeRow

		// SSP 3/19/03 - Row Layout Functionality
		// Made this method protected because we need to call it from the designer code.
		//
		//internal void InitializeRow( Infragistics.Win.UltraWinGrid.UltraGridRow row )
		/// <summary>
		/// Initializes the row element with the passed in row.
		/// </summary>
		/// <param name="row"></param>
		internal protected void InitializeRow( Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			this.UnhookRromRowNotifications();

			this.PrimaryContext     = row;

			// SSP 5/14/03 - Optimizations
			// Embeddable owner gets the row from cell ui element quite a number of times. To
			// speed it up, it would be better if the row was stored in a member variable of the
			// row ui element.
			//
			this.contextRow = row;

			// JJD 10/30/01
			// Cache the parent element's size so that we can compare
			// 
			// SSP 5/16/05 - Optimization
			// Row col region intersection element had an optimization for not causing the existing
			// row elements to reposition their descendants. It never worked so made it work.
			//
			//this.origParentSize		= this.Parent.Rect.Size;
            
			// SSP 3/24/05 BR02974
			// While performing pixel level scrolling (by holding down the mouse over the down
			// scroll arrow), the row element may end up being underneath the header elements.
			// If there was alphablending on headers then the row would show through. Also in
			// row layout mode with gaps the row would show through as well. To fix it we need
			// to set the row element's region to exclude the band headers area element rect.
			//
			this.clipSelfRect = Rectangle.Empty;

			// JJD 1/11/02
			// For cards we want to hook into the row's change notifications
			// so we know when to invalidate the card element
			//
			if ( row.IsCard )
			{
				this.hookedIntoNotifications = true;
				row.SubObjectPropChanged += this.SubObjectPropChangeHandler;

				// SSP 11/17/03 Add Row Feature
				// Now we are hooking into the SubObjDisposed event of the rows so we can unhook
				// ourselves from rows when they get disposed of.
				//
				row.SubObjectDisposed += this.RowDisposedHandler;
			}
		}

		#endregion // InitializeRow

		// JAS v5.2 DoubleClick Events 4/27/05
		//
		#region GetRowAreaUnderCursor

		private bool GetRowAreaUnderCursor( out RowArea rowArea, UltraGrid grid )
		{
			// Determine which element in the row the double click occurred over.
			//					
			Point ptCursor = grid.PointToClient( Control.MousePosition );
			UIElement elemUnderCursor = this.ElementFromPoint( ptCursor );					

			// There is no RowArea value for the pre-row area.
			//
			bool isOverValidArea = 
				elemUnderCursor != null && 
				elemUnderCursor.GetAncestor( typeof(PreRowAreaUIElement) ) == null;

			if( isOverValidArea )
			{
				// ---------------------------------------------------------------------
				// Note: Even though there is a RowArea value called 'GroupByRowArea',
				// there is no need to check if the cursor is over a GroupBy row because
				// GroupByRowUIElements would never be a descendant of RowUIElementBase.  
				// ---------------------------------------------------------------------

				if( elemUnderCursor.GetAncestor( typeof(RowSelectorUIElementBase) ) != null )
					rowArea = RowArea.RowSelectorArea;

				else if( elemUnderCursor.GetAncestor( typeof(RowAutoPreviewUIElement) ) != null )
					rowArea = RowArea.RowPreviewArea;

				// Most of the double clicks on a cell will be routed through the owner, but if the user
				// double clicks exactly on the cell border then the double click notification comes from
				// the cell element.  In that situation the following condition evaluates to true.
				//
				else if( elemUnderCursor.GetAncestor( typeof(CellUIElementBase) ) != null )
					rowArea = RowArea.Cell;

				else
					rowArea = RowArea.CellArea;
			}
			else
			{
				rowArea = 0;
			}

			return isOverValidArea;
		}

		#endregion // GetRowAreaUnderCursor

		#region PropChangeHandlers

		private SubObjectPropChangeEventHandler subObjectPropChangeHandler = null;
		
		// SSP 11/17/03 Add Row Feature
		//
		private SubObjectDisposedEventHandler rowDisposedHandler = null;

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChangeInfo">A structure containing the property change information.</param>
		protected virtual void OnSubObjectPropChanged(PropChangeInfo propChangeInfo) 
		{
			this.DirtyChildElements();		
		}

		// SSP 11/17/03 Add Row Feature
		// Now we are hooking into the SubObjDisposed event of the rows so we can unhook
		// ourselves from rows when they get disposed of.
		//
		private void OnRowDisposed( SubObjectBase obj )
		{
			// When the row is disposed off, unhook from it.
			//
			this.UnhookRromRowNotifications( );
		}

		/// <summary>
		/// Returns the event handler that notifies OnSubObjectPropChanged
		/// </summary>
		protected SubObjectPropChangeEventHandler SubObjectPropChangeHandler
		{
			get
			{
				if (this.subObjectPropChangeHandler == null)
					this.subObjectPropChangeHandler = new SubObjectPropChangeEventHandler(OnSubObjectPropChanged);

				return this.subObjectPropChangeHandler;
			}
		}

		/// <summary>
		/// Returns the event handler that notifies OnSubObjectPropChanged
		/// </summary>
		protected SubObjectDisposedEventHandler RowDisposedHandler
		{
			get
			{
				if ( null == this.rowDisposedHandler )
					this.rowDisposedHandler = new SubObjectDisposedEventHandler( OnRowDisposed );

				return this.rowDisposedHandler;
			}
		}

		#endregion PropChangeHandlers

		#region UnhookRromRowNotifications

		private void UnhookRromRowNotifications()
		{

			if (this.hookedIntoNotifications)
			{
				this.hookedIntoNotifications = false;

				// SSP 11/17/03 Add Row Feature
				// Now we are hooking into the SubObjDisposed event of the rows so we can unhook
				// ourselves from rows when they get disposed of.
				//
				// ----------------------------------------------------------------------------
				//this.Row.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				UltraGridRow row = this.Row;
				if ( null != row )
				{
					row.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					row.SubObjectDisposed -= this.RowDisposedHandler;
				}
				// ----------------------------------------------------------------------------
			}

		}

		#endregion UnhookRromRowNotifications

		#region GetItemNearestPoint

		internal ISelectableItem GetItemNearestPoint ( Point point, ISelectableItem lastItem )
		{
			// if we are looking for a row then return it
			//
			if ( lastItem is UltraGridRow )
				return this.Row;

			// if we aren't looking for a cell then exit
			//
			if ( !(lastItem is UltraGridCell ) )
				return null;

			// get the cell area
			//
			RowCellAreaUIElement cellarea = this.GetDescendant( typeof(RowCellAreaUIElement) ) as RowCellAreaUIElement;

			if ( cellarea == null )
				return null;
				
			CellUIElementBase cellElement;
			CellUIElementBase nearestCellElement = null;
			int nearestCellOffset = 0;
			int cellOffset = 0;

			// loop over the child row elements looking for the
			// one nearest the point
			//
			for ( int i = 0; i < cellarea.ChildElements.Count; i++ )
			{
				cellElement = cellarea.ChildElements[i] as CellUIElementBase;

				if ( cellElement == null )
					continue;

				Rectangle rect = cellElement.Rect;

				// if the point is contained in the cell then return it
				//
				if ( rect.Contains( point ) )
					return cellElement.Cell;

				// calculate how far the rect is from the point in both the
				// x and y dimensions
				//
				cellOffset = 0;

				if ( point.X < rect.Left )
					cellOffset += rect.Left - point.X;
				else if ( point.X > rect.Right )
					cellOffset += point.X - rect.Right;

				if ( point.Y < rect.Top )
					cellOffset += rect.Top - point.Y;
				else if ( point.Y > rect.Bottom )
					cellOffset += point.Y - rect.Bottom;

				// if this element is the nearest then save it
				// and its offset
				// 
				if ( nearestCellElement == null ||
					cellOffset < nearestCellOffset )
				{
					nearestCellElement	= cellElement;
					nearestCellOffset	= cellOffset;
				}

			}

			if ( nearestCellElement != null )
				return nearestCellElement.Cell;

			return null;
		}

		#endregion GetItemNearestPoint

		#region InitializeOverlapTopBorder

		internal void InitializeOverlapTopBorder( bool overlapTopBorder )
		{
			this.overlapTopBorder   = overlapTopBorder;

			// SSP 11/30/04 - Merged Cell Feature
			// Reset overlappedWithHeader flag to false. Row col intersection element will set this 
			// via InitializeOverlappedWithHeader method.
			//
			this.topBorderOverlappedWithHeaders = false;
		}

		#endregion // InitializeOverlapTopBorder

		#region OverlapTopBorder

		internal bool OverlapTopBorder
		{
			get
			{
				return this.overlapTopBorder;
			}
		}

		#endregion // OverlapTopBorder

		#region InitializeTopBorderOverlappedWithHeaders

		// SSP 11/30/04 - Merged Cell Feature
		// Added topBorderOverlappedWithHeaders flag which indicates whether the row's top is
		// overlapped with the band headers. NOTE: Band header element is added after the row 
		// element even if the row element visually appears after the headers. What this means 
		// is that the headers will overwrite the top row border.
		//
		internal void InitializeTopBorderOverlappedWithHeaders( bool topBorderOverlappedWithHeaders )
		{
			this.topBorderOverlappedWithHeaders = topBorderOverlappedWithHeaders;
		}

		#endregion // InitializeTopBorderOverlappedWithHeaders

		#region InFirstCardAreaCol
		
		internal bool InFirstCardAreaCol
		{
			get	{ return this.inFirstCardAreaCol; }
			set { this.inFirstCardAreaCol = value; }
		}

		#endregion // InFirstCardAreaCol

		#region InFirstCardAreaRow
		
		internal bool InFirstCardAreaRow
		{
			get	{ return this.inFirstCardAreaRow; }
			set { this.inFirstCardAreaRow = value; }
		}

		#endregion // InFirstCardAreaRow

		#region InLastCardAreaCol
		
		internal bool InLastCardAreaCol
		{
			get	{ return this.inLastCardAreaCol; }
			set { this.inLastCardAreaCol = value; }
		}

		#endregion // InLastCardAreaCol

		#region MergeRowSelectorTop

		// JJD 1/24/02
		// Added helper method
		//
		private bool MergeRowSelectorTop
		{
			get 
			{
				Infragistics.Win.UltraWinGrid.UltraGridRow row = this.Row;

				// SSP 12/3/01
				// This was causing the first row's row selector in a outlook group by row
				// to not have top border. So if the row right above is an outlook group by row
				// then don't merge the top border with it.
				// Added merge flag and code to figure out whether to merge or not.
				//

				bool merge = true;

				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( row.ParentRow is UltraGridGroupByRow )
				UltraGridRow parentRow = row.ParentRow;

				if ( parentRow is UltraGridGroupByRow )
				{
					// MD 8/2/07 - 7.3 Performance
					// This check is redundant: if the parent row is an instance of UltraGridGroupByRow, 
					// it cannot be null
					//if ( null != row.ParentRow  )
					{
						RowScrollRegion rsr = (RowScrollRegion)this.GetContext( typeof( RowScrollRegion ), true );
						
						if ( 0 == row.Index && null != rsr )
						{
							VisibleRowsCollection vrs = rsr.VisibleRows;

							for ( int i = 0; i < vrs.Count; i++ )
							{
								// MD 8/2/07 - 7.3 Performance
								// Prevent calling expensive getters multiple times
								//if ( vrs[i].Row == row.ParentRow )
								if ( vrs[ i ].Row == parentRow )
								{
									merge = false;
									break;
								}

								if ( row == vrs[i].Row )
									break;
							}
						}
					}
				}
				else
				{
					// JJD 1/24/02
					// Adde logic to prevent mergeing in horizontal view style
					// in certain band break cases.
					//
					RowScrollRegion rsr = (RowScrollRegion)this.GetContext( typeof( RowScrollRegion ), true );
					
					if ( null != rsr )
					{
						VisibleRowsCollection vrs = rsr.VisibleRows;
						
						VisibleRow previousVr = null;
						
						for ( int i = 0; i < vrs.Count; i++ )
						{
							if ( vrs[i].Row == row )
							{
								// JJD 1/24/02
								// Don't merge if there is spacing between
								// the rows
								//
								if ( row.RowSpacingBeforeResolved > 0 ||
									( previousVr != null && previousVr.Row.RowSpacingAfterResolved > 0))
									merge = false;
								else
								{
									// if the previous row is from a different band but not 
									// adjacent to a header then don't merge the top border
									// in horizontal view style
									//
									if ( previousVr != null	&&
										previousVr.Band != row.Band &&
										vrs[i].Tier > 0	&&
										!vrs[i].HasHeader &&
										row.Layout.ViewStyleImpl.HasMultiRowTiers )
									{
										merge = false;
									}
								}

								break;
							}
							
							// save the row to check on the next loop
							//
							previousVr = vrs[i];
						}
					}
				}

				return merge;
			}
		}

		#endregion // MergeRowSelectorTop

		#region OrigParentSize 
		
		// SSP 5/16/05 - Optimization
		// Row col region intersection element had an optimization for not causing the existing
		// row elements to reposition their descendants. It never worked so made it work.
		//
		

		#endregion // OrigParentSize 

		#region Row

		internal Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				// SSP 5/14/03 - Optimizations
				// Embeddable owner gets the row from cell ui element quite a number of times. To
				// speed it up, it would be better if the row was stored in a member variable of the
				// row ui element.
				//
				//return (Infragistics.Win.UltraWinGrid.UltraGridRow)this.PrimaryContext;
				return this.contextRow;
			}
		}

		#endregion // Row

		#region InitCachedInfo

		// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
		// Added InitCachedInfo method.
		//
		internal void InitCachedInfo( UltraGridRow row )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridBand band  = row.BandInternal;

			this.rowBorderStyle = row.BorderStyleResolved;
		}

		#endregion // InitCachedInfo

		#region InvalidateIfHotTracked

		// SSP 9/9/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal bool InvalidateIfHotTracked( )
		{
			if ( this.isMouseOverSelectorOrCellArea )
			{
				RowCellAreaUIElementBase.OnMouseEnterLeaveHelper( this, this.isMouseOverSelectorOrCellArea );

				UIElement rowCellArea = this.GetDescendant( typeof( RowCellAreaUIElementBase ) );
				if ( null != rowCellArea && rowCellArea.HasChildElements )
				{
					foreach ( UIElement childElem in rowCellArea.ChildElements )
					{
						CellUIElementBase cellElem = childElem as CellUIElementBase;
						if ( null != cellElem && cellElem.InvalidateIfHotTracked( ) )
							break;
					}
				}

				return true;
			}

			return false;
		}

		#endregion // InvalidateIfHotTracked

		#endregion // Private/Internal Methods/Properties

		#region Protected Virtual Methods

		#region CreateRowCellAreaUIElement

		/// <summary>
		/// Method for creating RowCellAreaUIElementBase derived class instances.
		/// </summary>
		/// <returns></returns>
		protected abstract RowCellAreaUIElementBase CreateRowCellAreaUIElement( UIElementsCollection oldElems );

		#endregion // CreateRowCellAreaUIElement

		#region CreateRowSelectorUIElement

		/// <summary>
		/// Method for creating RowSelectorUIElementBase derived class instances.
		/// </summary>
		/// <returns></returns>
		protected abstract RowSelectorUIElementBase CreateRowSelectorUIElement( UIElementsCollection oldElems );

		#endregion // CreateRowSelectorUIElement

		#region CreatePreRowAreaUIElement

		/// <summary>
		/// Method for creating PreRowAreaUIElement derived class instances.
		/// </summary>
		/// <returns></returns>
		protected abstract PreRowAreaUIElement CreatePreRowAreaUIElement( UIElementsCollection oldElems );

		#endregion // CreatePreRowAreaUIElement

		#endregion // Protected Virtual Methods

		#region Base Overrides

		#region Offset

		// SSP 5/18/05 - Optimizations
		// Overrode Offset so we can offset the clipSelfRect as well.
		//
		/// <summary>
		/// Overridden. Offsets this element's rect and (optionally) all of its descendant elements.
		/// </summary>
		/// <param name="deltaX">The number of pixels to offset left/right</param>
		/// <param name="deltaY">The number of pixels to offset up/down </param>
		/// <param name="recursive">If true will offset all descendant elements as well</param>
		public override void Offset( int deltaX, int deltaY, bool recursive )
		{
			if ( ! this.clipSelfRect.IsEmpty )
				this.clipSelfRect.Offset( deltaX, deltaY );

			base.Offset( deltaX, deltaY, recursive );
		}

		#endregion // Offset

		#region OnDispose

		/// <summary>
		/// Called when the object is being disposed.
		/// </summary>
		protected override void OnDispose()
		{
			this.UnhookRromRowNotifications();

			base.OnDispose();
		}

		#endregion OnDispose

		#region GetContext

		// SSP 8/28/03 - Row Layout Functionality
		// Row filterind code requires a rows collection context on a header element.
		// BandHeadersUIElemenet provides that. However in the mode where the headers
		// are with cells inside of row elements, there is no band headers ui element.
		// So the row element needs to return the context of the rows collection.
		//
        /// <summary>
        /// Returns an object of requested type that relates to the element or null.
        /// </summary>
        /// <param name="type">The requested type or null to pick up default context object.</param>
        /// <param name="checkParentElementContexts">If true will walk up the parent chain looking for the context.</param>
        /// <returns>Returns null or an object of requested type that relates to the element.</returns>
        /// <remarks> Classes that override this method normally need to override the <see cref="Infragistics.Win.UIElement.ContinueDescendantSearch(System.Type,System.Object[])"/> method as well.</remarks>
        public override object GetContext(Type type, bool checkParentElementContexts)
		{
			if ( typeof( RowsCollection ) == type && null != this.Row )
				return this.Row.ParentCollection;

			// MRS 3/21/05 - Added a context for the band. Need it for Drag/drop in RowLayout mode.
			if ( typeof( UltraGridBand ) == type 
				&& null != this.Row 
				&& null != this.Row.Band )
			{
				return this.Row.Band;
			}

			return base.GetContext( type, checkParentElementContexts );
		}

		#endregion // GetContext

		#region RowLayoutDesignerElement

		// SSP 3/19/03 - Row Layout Functionality
		// Added a property to indicate whether we are in row layout designer mode.
		//
		private bool RowLayoutDesignerElement
		{
			get
			{
				DataAreaUIElement dataAreaElem = (DataAreaUIElement)this.GetAncestor( typeof( DataAreaUIElement ) );

				return null != dataAreaElem && dataAreaElem.RowLayoutDesignerElement;
			}
		}

		#endregion // RowLayoutDesignerElement

		#region ContinueDescendantSearch

		// SSP 12/19/02
		// Optimizations.
		// Overrode ContinueDescendantSearch method.
		//
		/// <summary>
		/// This method is called from <see cref="Infragistics.Win.UIElement.GetDescendant(Type, object[])"/> as an optimization to
		/// prevent searching down element paths that can't possibly contain the element that is being searched for. 
		/// </summary>
		/// <remarks><seealso cref="Infragistics.Win.UIElement.ContinueDescendantSearch"/></remarks>
		/// <returns>Returns false if the search should stop walking down the element chain because the element can't possibly be found.</returns>
        protected override bool ContinueDescendantSearch( Type type, object[] contexts )
		{
			if ( null != contexts )
			{
				UltraGridRow row = this.Row;

				for ( int i = 0; i < contexts.Length; i++ )
				{
					object context = contexts[i];

					if ( null != context )
					{
						if ( context is UltraGridRow )
							return context == row;
						
						// MD 8/7/07 - 7.3 Performance
						// FxCop - Do not cast unnecessarily
						//if ( context is UltraGridCell )
						//    return ((UltraGridCell)context).Row == row;
						UltraGridCell cell = context as UltraGridCell;

						if ( cell != null )
							return cell.Row == row;

						// SSP 3/18/03 - Row Layout Functionality
						// In Row Layout mode, headers could be inside of the row. So check to
						// see if we are in row layout mode before returning false.
						//
						//if ( context is HeaderBase )                        
						if ( context is HeaderBase &&
                            // MRS 2/24/2009 - Found while fixing TFS14427
                            // Even when AreColumnHeadersInSeparateLayoutArea is true, there can still be group headers in the cell area. 
                            context is GroupHeader == false &&
							null != row && ( ! row.Band.UseRowLayoutResolved || row.Band.AreColumnHeadersInSeparateLayoutArea ) )
							return false;

						// SSP 11/22/04 - Merged Cell Feature
						//
						if ( context is MergedCell )
							return false;
					}
				}
			}

			return true;
		}

		#endregion // ContinueDescendantSearch

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			Infragistics.Win.UltraWinGrid.UltraGridRow row = this.Row;
			UltraGridBand band = row.BandInternal;

			// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
			// 
			this.InitCachedInfo( row );

			if (row.IsCard)
			{
				this.PositionChildElementsForCardArea();
			}
			else
			{
				// SSP 7/7/05 - NAS 5.3 Empty Rows
				// Use the PreRowAreaExtentResolved off the row instead.
				// 
				//int preRowAreaExtent  = band.PreRowAreaExtent;
				int preRowAreaExtent  = row.PreRowAreaExtentResolved;

				// SSP 5/5/05 - NAS 5.2 Filter Row
				// Use the RowSelectorExtentResolved off the row instead.
				//
				//int rowSelectorExtent = band.RowSelectorExtent;
				int rowSelectorExtent = row.RowSelectorExtentResolved;

				Rectangle workRect = this.Rect;

				UIElementsCollection oldElements = this.ChildElements;

				this.childElementsCollection = null;
				Rectangle preRowAreaRect = new Rectangle();

				// add the pre row area if necessary
				//
				// SSP 3/19/03 - Row Layout Functionality
				// Added a property to indicate whether to add a pre row area element. This is for
				// the row layout designer.
				//
				//if ( preRowAreaExtent > 0 )
				if ( ! this.RowLayoutDesignerElement && preRowAreaExtent > 0 )
				{
					// calculate the rect for this area
					//
					preRowAreaRect = workRect;
					preRowAreaRect.Width = preRowAreaExtent;

					// adjust the work rect so it doesn't overlap
					//
					workRect.Width -= preRowAreaExtent;
					workRect.Offset( preRowAreaExtent, 0 );

					if ( preRowAreaRect.IntersectsWith( this.Parent.Rect ) )
					{
						// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
						// Use the Create method instead.
						//
						//PreRowAreaUIElement preRowAreaElement = (PreRowAreaUIElement)RowUIElement.ExtractExistingElement( oldElements, typeof(PreRowAreaUIElement), true );
						//if ( null == preRowAreaElement)
						//	preRowAreaElement = new PreRowAreaUIElement( this );
						PreRowAreaUIElement preRowAreaElement = this.CreatePreRowAreaUIElement( oldElements );

						preRowAreaElement.Rect = preRowAreaRect;

						this.ChildElements.Add( preRowAreaElement );
					}
				}

				// add the row selector area if necessary
				//
				if ( rowSelectorExtent > 0 )
				{
					// calculate the rect for this area
					//
					Rectangle rowSelectorRect = workRect;
					rowSelectorRect.Width = rowSelectorExtent;

					// adjust the work rect so it doesn't overlap
					//
					workRect.Width -= rowSelectorExtent;
					workRect.Offset( rowSelectorExtent, 0 );

					// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
					// Use the rowBorderStyle member var instead.
					//
					//if ( band.Layout.CanMergeAdjacentBorders( band.BorderStyleRowResolved ) )
					if ( band.Layout.CanMergeAdjacentBorders( this.rowBorderStyle ) )
					{
						workRect.X--;
						workRect.Width++;
					}

					if ( this.overlapTopBorder )
					{
						rowSelectorRect.Y++;
						rowSelectorRect.Height--;
					}

					if ( rowSelectorRect.IntersectsWith( this.Parent.Rect ) )
					{
						// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
						// Use the Create method instead.
						//
						//RowSelectorUIElement rowSelectorElement = (RowSelectorUIElement)RowUIElement.ExtractExistingElement( oldElements, typeof(RowSelectorUIElement), true );
						//if ( null == rowSelectorElement)
						//	rowSelectorElement = new RowSelectorUIElement( this );
						RowSelectorUIElementBase rowSelectorElement = this.CreateRowSelectorUIElement( oldElements );

						//if CanMergeAdjacentBorders
						//adjust widths and heights to overlap each other
						//
						// SSP 3/7/03 - Row Layout Functionality
						// Use the new BorderStyleRowSelector property.
						//
						//if ( this.Row.Layout.CanMergeAdjacentBorders( this.Row.Band.BorderStyleHeaderResolved ) )
						if ( band.Layout.CanMergeAdjacentBorders( band.BorderStyleRowSelectorResolved ) )
						{
							
							//if the rowselector is butted up against
							//the data area's border, merge
							//
							// JJD 11/21/01 - UWG770
							// Instead of checking if the left matches, get the origin of
							// the band's rowselector area. If this is 0 we want to overlap the
							// left border so it won't double up with the control's borders.
							// Otherwise we should always display the row selector's left border.
							//
							//						if ( rowSelectorRect.Left == this.Rect.Left &&
							if ( band.GetOrigin( BandOrigin.RowSelector ) == 0 &&
								// SSP 1/27/05 BR02053
								// Instead of checking IsPrinting flag, make use of IsDisplayLayout. What we are 
								// interested in finding out is this element is in the print layout or not. Simply
								// checking the IsPrinting flag won't tell us that the display layout could verify 
								// it's ui elements while printing (like when the grid gets a paint while printing,
								// especially with the new UltraGridPrintDocument).
								//
								// !band.Layout.Grid.IsPrinting 
								band.Layout.IsDisplayLayout 
								)
							{
								rowSelectorRect.X--;
								rowSelectorRect.Width++;
							}

							// JJD 1/24/02
							// Moved logic into helper method
							//
							if ( this.MergeRowSelectorTop )
							{
								rowSelectorRect.Y--;
								rowSelectorRect.Height++;						
							}						
						}	


					
						rowSelectorElement.Rect = rowSelectorRect;


						this.ChildElements.Add( rowSelectorElement );
					}
				}

				if ( workRect.IntersectsWith( this.Parent.Rect ) )
				{
					// add the row's cellarea
					//
					// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
					// Use the Create method instead.
					//
					//RowCellAreaUIElement rowCellAreaElement = (RowCellAreaUIElement)RowUIElement.ExtractExistingElement( oldElements, typeof(RowCellAreaUIElement), true );
					//if ( null == rowCellAreaElement)
					//	rowCellAreaElement = new RowCellAreaUIElement( this );
					RowCellAreaUIElementBase rowCellAreaElement = this.CreateRowCellAreaUIElement( oldElements );

					rowCellAreaElement.Rect = workRect;

					this.ChildElements.Add( rowCellAreaElement );
				}

				// SSP 5/17/05 BR03705
				// Dispose of the elements that weren't reused.
				//
				if ( null != oldElements )
					oldElements.DisposeElements( );
			}

			// SSP 7/28/05 - Header, Row, Summary Tooltips
			// 
			GridUtils.SetupElementToolTip( this, row.ToolTipText, false );
		}

		#endregion // PositionChildElements

		#region PositionChildElementsForCardArea

		/// <summary>
		/// Positions elements for rows that are CardView area rows.
		/// </summary>
		private void PositionChildElementsForCardArea( )
		{
			UltraGridRow			row			= this.Row;
			Rectangle				rectWork;
			UIElementsCollection	oldElements = this.ChildElements;

			this.childElementsCollection = null;

			// Create a CardCaptionUIElement if needed
			int cardCaptionHeight = 0;
			if (row.Band.CardSettings.ShowCaption)
			{
				// Try to reuse an element from the old elements list.
				CardCaptionUIElement cardCaptionElement = (CardCaptionUIElement)RowUIElementBase.ExtractExistingElement(oldElements, typeof(CardCaptionUIElement), true);
				if (cardCaptionElement == null)
					cardCaptionElement = new CardCaptionUIElement(this);

				rectWork				= this.RectInsideBorders;
				rectWork.Height			= row.Band.CardCaptionHeight;
				
				// If the border style of the card area is None, shrink the caption width
				// by the CellSpacing amount so it lines up with the cells.
				//
				//JM 01-16-02 now using BorderStyleRow for cards
				//if (row.Band.BorderStyleCardResolved == UIElementBorderStyle.None)
				// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
				// Use the rowBorderStyle member var instead.
				//
				//if (row.Band.BorderStyleRowResolved == UIElementBorderStyle.None)
				if ( this.rowBorderStyle == UIElementBorderStyle.None)
					rectWork.Inflate(-row.Band.CellSpacingResolved, 0);

				cardCaptionElement.Rect = rectWork;
				this.ChildElements.Add(cardCaptionElement);

				// Save the card caption height - we'll use it below.
				cardCaptionHeight = rectWork.Height;
			}

			// SSP 2/28/03 - Row Layout Functionality
			// Actually this doesn't have anything to with row layout functionality but it's something
			// I noticed while implementing row layout functionality. When the card row is compressed 
			// return without adding the row cell area and the card label area.
			//
			// SSP 5/17/05 BR03705
			// We need to dispose the un-reused elements at the end of the method. Instead of returing
			// enclose the code in the if block.
			//
			//if ( row.IsCardCompressed )
			//	return;
			if ( ! row.IsCardCompressed )
			{
				// Create a CardLabelAreaUIElement if we are not using the MergeLabels style.
				// SSP 2/28/03 - Row Layout Functionality
				// Also don't add a card label area if we are using a row layout since the layout
				// manager is the one that will be laying out headers with the cell.s
				//
				//if (row.Band.CardSettings.StyleResolved != CardStyle.MergedLabels)
				// SSP 2/28/03 - Row Layout Functionality
				// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
				// to StandardLabels card style so use the StyleResolved instead of the Style property.
				//
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( ! row.Band.UseRowLayoutResolved && row.Band.CardSettings.StyleResolved != CardStyle.MergedLabels 
				//    // SSP 12/16/04 BR00242
				//    // If the ColHeadersVisible property is set to false then don't display 
				//    // the card labels. Added below condition.
				//    //
				//    && row.Band.CardLabelWidthResolved > 0 )
				bool createCardLabelArea = !row.Band.UseRowLayoutResolved && row.Band.CardSettings.StyleResolved != CardStyle.MergedLabels;
				int cardLabelWidthResolved = createCardLabelArea
					? row.Band.CardLabelWidthResolved :
					-1;

				if ( createCardLabelArea
					// SSP 12/16/04 BR00242
					// If the ColHeadersVisible property is set to false then don't display 
					// the card labels. Added below condition.
					//
					&& cardLabelWidthResolved > 0 )
				{
					// Try to reuse an element from the old elements list.
					CardLabelAreaUIElement cardLabelAreaElement = (CardLabelAreaUIElement)RowUIElementBase.ExtractExistingElement(oldElements, typeof(CardLabelAreaUIElement), true);
					if (cardLabelAreaElement == null)
						cardLabelAreaElement = new CardLabelAreaUIElement(this);

					// Initialize element.
					cardLabelAreaElement.InFirstCardAreaRow = this.InFirstCardAreaRow;

					rectWork		 = this.RectInsideBorders;
					rectWork.Y		+= (cardCaptionHeight);
					rectWork.Height	-= (cardCaptionHeight);

					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//rectWork.Width	 = row.Band.CardLabelWidthResolved;
					rectWork.Width = cardLabelWidthResolved;

					// If the border style of the card area is None, shrink the label
					// area left edge by the CellSpacing amount so it lines up with the caption.
					//JM 01-16-02 now using BorderStyleRow for cards
					//
					//if (row.Band.BorderStyleCardResolved == UIElementBorderStyle.None)
					// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
					// Use the rowBorderStyle member var instead.
					//
					//if (row.Band.BorderStyleRowResolved == UIElementBorderStyle.None)
					if ( this.rowBorderStyle == UIElementBorderStyle.None )
					{
						rectWork.X		+= row.Band.CellSpacingResolved;
						rectWork.Width	-= row.Band.CellSpacingResolved;
					}

					cardLabelAreaElement.Rect = rectWork;
					this.ChildElements.Add(cardLabelAreaElement);
				}

				// Create a RowCellAreaUIElement.
				//
				// Try to reuse an element from the old elements list.
				// SSP 3/16/05 - NAS 5.2 Filter Row Functionality
				// Use the Create method instead.
				//
				//RowCellAreaUIElement rowCellAreaElement = (RowCellAreaUIElement)RowUIElement.ExtractExistingElement(oldElements, typeof(RowCellAreaUIElement), true);
				//if (rowCellAreaElement == null)
				//	rowCellAreaElement = new RowCellAreaUIElement(this);
				RowCellAreaUIElementBase rowCellAreaElement = this.CreateRowCellAreaUIElement( oldElements );

				// Initialize element.
				rowCellAreaElement.InFirstCardAreaRow	= this.InFirstCardAreaRow;
				rowCellAreaElement.InLastCardAreaCol	= this.InLastCardAreaCol;

				rectWork		 = this.RectInsideBorders;
				rectWork.Y		+= (cardCaptionHeight);
				rectWork.Height	-= (cardCaptionHeight);

				// SSP 2/28/03 - Row Layout Functionality
				// Also don't add a card label area if we are using a row layout since the layout
				// manager is the one that will be laying out headers with the cell.s
				//
				//if (row.Band.CardSettings.StyleResolved != CardStyle.MergedLabels)
				// SSP 2/28/03 - Row Layout Functionality
				// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
				// to StandardLabels card style so use the StyleResolved instead of the Style property.
				//
				//if ( ! row.Band.UseRowLayoutResolved && row.Band.CardSettings.StyleResolved != CardStyle.MergedLabels )
				if ( createCardLabelArea )
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//rectWork.X	   += row.Band.CardLabelWidthResolved;
					//rectWork.Width	= rectWork.Width - row.Band.CardLabelWidthResolved;
					rectWork.X += cardLabelWidthResolved;
					rectWork.Width = rectWork.Width - cardLabelWidthResolved;
				}

				rowCellAreaElement.Rect = rectWork;
				this.ChildElements.Add(rowCellAreaElement);
			}

			// SSP 5/17/05 BR03705
			// Dispose of the elements that weren't reused.
			//
			if ( null != oldElements )
				oldElements.DisposeElements( );
		}
 
		#endregion // PositionChildElementsForCardArea

		#region DrawBackColor 

		/// <summary>
		/// This element doesn't draw a background.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{
			// this element doesn't draw a background
		}

		#endregion // DrawBackColor 
		
		#region DrawImageBackground 

		/// <summary>
		/// This element doesn't draw an image background.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
		}

		#endregion // DrawImageBackground 

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>The border style of cells, rows, and headers can be set by the BorderStyleCell, BorderStyleRow, and BorderStyleHeader properties respectively.</para>
        /// <para>The border style of the AddNew box buttons can be set by the ButtonBorderStyle property.</para>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				if (this.Row.IsCard)
					// JM 01/16/02 
					//return this.Row.Band.BorderStyleCardResolved;				
					// SSP 3/16/05 - NAS 5.2 Filter Row
					// Use the rowBorderStyle member var instead.
					//
					//return this.Row.Band.BorderStyleRowResolved;
					return this.rowBorderStyle;
				else
					return UIElementBorderStyle.None;
			}
		}

		#endregion // BorderStyle

		#region BorderSides

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				if (this.Row.IsCard)
					return Border3DSide.All;
				else
					return 0;
			}
		}

		#endregion // BorderSides

		#region ClipChildren 

		/// <summary>
		/// Returning true causes all drawing of this element's children to be explicitly clipped
		/// to its region
		/// </summary>
		protected override bool ClipChildren 
		{
			get
			{
				if (this.Row.IsCard)
					return true; 
				else
					return false;
			} 
		}

		#endregion // ClipChildren 

		// SSP 3/24/05 BR02974
		// While performing pixel level scrolling (by holding down the mouse over the down
		// scroll arrow), the row element may end up being underneath the header elements.
		// If there was alphablending on headers then the row would show through. Also in
		// row layout mode with gaps the row would show through as well. To fix it we need
		// to set the row element's region to exclude the band headers area element rect.
		//
		#region Fix for BR02974 

		#region ClipSelf

		/// <summary>
		/// Returning true causes all drawing of this element to be expicitly clipped
		/// to its region
		/// </summary>
		protected override bool ClipSelf 
		{ 
			get 		  
			{ 
				return ! this.clipSelfRect.IsEmpty;
			} 
		}
 
		#endregion // ClipSelf

		#region Region

		/// <summary>
		/// Returns the region of this element. The deafult returns the element's
		/// Rect as a region. This method can be overriden to supply an irregularly
		/// shaped region 
		/// </summary>
		public override System.Drawing.Region Region
		{
			get
			{
				// The reason why we are creating a new instance of region is because the element's
				// Draw method disposes of this after accessing it.
				//
				return ! this.clipSelfRect.IsEmpty ? new Region( this.clipSelfRect ) : base.Region;
			}
		}
 
		#endregion // Region

		#region IntersectInvalidRect

		// SSP 8/6/04 UWG3380
		// Overrode IntersectInvalidRect to fix the problem where a check indicator in 
		// a non-fixed cell that was scrolled under a fixed cell was not clipped and
		// appeared through the fixed cell. This is because the themes-drawing does not
		// honor UIElement.Region property that we are overriding.
		//
		/// <summary>
		/// Returns the intersection of the element's rect with the invalid rect for the
		/// current draw operation.
		/// </summary>
		/// <param name="invalidRect">Invalid rect</param>
		/// <returns>The intersection of the element's rect with the invalid rect.</returns>
		protected override Rectangle IntersectInvalidRect( Rectangle invalidRect )
		{
			return this.clipSelfRect.IsEmpty
				? base.IntersectInvalidRect( invalidRect )
				: Rectangle.Intersect( invalidRect, this.clipSelfRect );
		}

		#endregion IntersectInvalidRect 

		#region InternalSetClipSelfRegion
		
		internal void InternalSetClipSelfRegion( Rectangle clipSelfRect )
		{
			this.clipSelfRect = clipSelfRect;
		}

		#endregion // InternalSetClipSelfRegion

		#region ClipSelfRect

		// SSP 6/27/05 BR04617
		// 
		internal Rectangle ClipSelfRect
		{
			get
			{
				return this.clipSelfRect;
			}
		}

		#endregion // ClipSelfRect

		#endregion // Fix for BR02974 

		#region AdjustableUIElement Overrides

			#region ApplyAdjustment

        /// <summary>
        /// Called after a move/resize operation. 
        /// </summary>
        /// <param name="delta">The delta</param>
        public override void ApplyAdjustment(Point delta)
		{
			this.Row.Band.CardSettings.Width = this.Rect.Width + delta.X;

			this.ControlElement.DirtyChildElements();
		}

			#endregion ApplyAdjustment
			
			#region GetAdjustmentRange

		/// <summary>
		/// Returns the range limits for adjusting the element in either or both
		/// dimensions. It also returns the initial rects for the vertical and horizontal
		/// bars that will need to be inverted during the mouse drag operation.
		/// </summary>
		/// <param name="point">The point where the mouse is in client coordinates</param>
		/// <param name="range">Returned limits</param>
		public override void GetAdjustmentRange(System.Drawing.Point point,
			out UIElementAdjustmentRangeParams range)
		{
			// Initialize
			range = new UIElementAdjustmentRangeParams();

			if (this.SupportsLeftRightAdjustmentsFromPoint(point))
			{
				range.maxDeltaLeft      = - (this.Rect.Width - 20);

				RowColRegionIntersectionUIElement rowColRegionElement = this.GetAncestor(typeof(RowColRegionIntersectionUIElement)) as RowColRegionIntersectionUIElement;
				if (rowColRegionElement != null)
					range.maxDeltaRight	= rowColRegionElement.Rect.Right  - this.Rect.Right;
				else
					range.maxDeltaRight	= this.Parent.Rect.Right  - this.Rect.Right;

				range.leftRightAdjustmentBar.Height  = this.Parent.Rect.Height;
				range.leftRightAdjustmentBar.Width   = 0;
				range.leftRightAdjustmentBar.Offset(this.Rect.Right, this.Parent.Rect.Top + 1);
			}
		}

			#endregion GetAdjustmentRange

			#region OnElementAdjustmentStart

        /// <summary>
        /// Called when a mousedown is received and a resize operation is started.
        /// </summary>
        /// <param name="isUpDownAdjustment">isUpDownAdjustment</param>
        /// <param name="initialUpDownAdjustmentPointInBottomBorder">initialUpDownAdjustmentPointInBottomBorder</param>
        /// <param name="isLeftRightAdjustment">isLeftRightAdjustment</param>
        /// <param name="initialLeftRightAdjustmentPointInRightBorder">initialLeftRightAdjustmentPointInRightBorder</param>
        protected override void OnElementAdjustmentStart(bool isUpDownAdjustment, 
			bool initialUpDownAdjustmentPointInBottomBorder,
			bool isLeftRightAdjustment,
			bool initialLeftRightAdjustmentPointInRightBorder)
		{
			// Exit edit mode.
			if (this.Row.Band.Layout.ActiveCell != null)
				this.Row.Band.Layout.ActiveCell.ExitEditMode();
		}

			#endregion OnElementAdjustmentStart

			#region SupportsLeftRightAdjustmentsFromRightBorder

		/// <summary>
		/// True if this element supports left to right adjustments by grabbing the right border
		/// </summary>
		protected override bool SupportsLeftRightAdjustmentsFromRightBorder
		{
			get
			{ 
				// SSP 3/14/03 - Row Layout Functionality
				// In row-layout mode disable resizing of the whole card area since individual
				// cell resizing is enabled in the row-layout mode.
				//
				if ( null != this.Row && this.Row.Band.UseRowLayoutResolved )
					return false;

				if (this.Row.Band.CardView					&& 
					this.Row.Band.CardSettings.AllowSizing	&&
					!this.Row.Band.CardSettings.AutoFit)
					return true;
				else
					return false;
			}
		}

			#endregion SupportsLeftRightAdjustmentsFromRightBorder

		#endregion AdjustableUIElement Overrides

		// JJD 12/24/03 - Accessibility
		#region IsAccessibleElement property

		/// <summary>
		/// Indicates if the element supports accessibility
		/// </summary>
		public override bool IsAccessibleElement 
		{ 
			get 
			{
				RowColRegionIntersectionUIElement scrollingRegion = this.GetAncestor( typeof(RowColRegionIntersectionUIElement ) ) as RowColRegionIntersectionUIElement;

				if ( scrollingRegion != null &&
					scrollingRegion.RowScrollRegion == this.Row.Layout.ActiveRowScrollRegion &&
					scrollingRegion.ColScrollRegion == this.Row.Layout.ActiveColScrollRegion )
					return true;

				return false; 
			} 
		}

		#endregion IsAccessibleElement property

		// JJD 12/24/03 - Added Accessibility support
		#region AccessibilityInstance property

		/// <summary>
		/// Returns the accesible object representing this row.
		/// </summary>
		public override AccessibleObject AccessibilityInstance
		{
			get
			{
				return this.Row.AccessibilityObject;
			}
		}

		#endregion AccessibilityInstance property

        // JAS v5.2 DoubleClick Events 4/27/05
		//
		#region OnDoubleClick

        /// <summary>
        /// Called when the mouse is double clicked on this element.
        /// </summary>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        protected override void OnDoubleClick(bool adjustableArea)
		{			
			if( this.Row			 != null &&
				this.Row.Band		 != null &&
				this.Row.Band.Layout != null )
			{
				UltraGrid grid = this.Row.Band.Layout.Grid as UltraGrid;

				// JAS BR04668 6/22/05 - If the client code closes a modal dialog
				// that the grid is on within the DoubleClick event handler then 
				// the grid's handle will not exist.
				//if( grid != null )
				if( grid != null && grid.IsHandleCreated )
				{
					RowArea rowArea;
					if( this.GetRowAreaUnderCursor( out rowArea, grid ) )
						grid.FireEvent( 
							GridEventIds.DoubleClickRow, 
							new DoubleClickRowEventArgs( this.Row, rowArea ) );				
	
                    // MBS 5/6/08 - RowEditTemplate NA2008 V2
                    RowEditTemplateUIType templateType = this.Row.Band.RowEditTemplateUITypeResolved;
                    if ((templateType & RowEditTemplateUIType.OnDoubleClickRow) != 0)
                        this.Row.ShowEditTemplate(true, TemplateDisplaySource.OnRowDoubleClick);
				}
			}
		
			base.OnDoubleClick( adjustableArea );
		}

		#endregion // OnDoubleClick

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Row.BandInternal, StyleUtils.Role.Row );
			}
		}

		#endregion //UIRole

		#endregion // Base Overrides		
	}

	#endregion // RowUIElementBase Class

	#region RowUIElement Class

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Split the RowUIElement into RowUIElementBase class and RowUIElement class so the filter
	// row element can derive from RowUIElementBase.
	//
	/// <summary>
	/// The DataAreaUIElement contains the row and column scrolling
	/// regions.
	/// </summary>
	public class RowUIElement : RowUIElementBase
	{
		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public RowUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region Row

		/// <summary>
		/// The associated Row object (read-only).
		/// </summary>
		public new Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return base.Row;
			}
		}

		#endregion // Row

		#region Base Overrides

		#region CreateRowCellAreaUIElement

		/// <summary>
		/// Method for creating RowCellAreaUIElementBase derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>An existing RowCellAreaUIElementBase if one exists. Otherwise, a new instance.</returns>
        protected override RowCellAreaUIElementBase CreateRowCellAreaUIElement(UIElementsCollection oldElems)
		{
			RowCellAreaUIElement elem = (RowCellAreaUIElement)FilterRowCellAreaUIElement.ExtractExistingElement( oldElems, typeof( RowCellAreaUIElement ), true );
			return null != elem ? elem : new RowCellAreaUIElement( this );
		}

		#endregion // CreateRowCellAreaUIElement

		#region CreateRowSelectorUIElement

		/// <summary>
		/// Method for creating RowSelectorUIElementBase derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>An existing RowSelectorUIElementBase if one exists. Otherwise, a new instance.</returns>
        protected override RowSelectorUIElementBase CreateRowSelectorUIElement(UIElementsCollection oldElems)
		{
			RowSelectorUIElement elem = (RowSelectorUIElement)FilterRowCellAreaUIElement.ExtractExistingElement( oldElems, typeof( RowSelectorUIElement ), true );
			return null != elem ? elem : new RowSelectorUIElement( this );
		}

		#endregion // CreateRowSelectorUIElement

		#region CreatePreRowAreaUIElement

		/// <summary>
		/// Method for creating PreRowAreaUIElement derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>An existing PreRowAreaUIElement if one exists. Otherwise, a new instance.</returns>
        protected override PreRowAreaUIElement CreatePreRowAreaUIElement(UIElementsCollection oldElems)
		{
			PreRowAreaUIElement elem = (PreRowAreaUIElement)FilterRowCellAreaUIElement.ExtractExistingElement( oldElems, typeof( PreRowAreaUIElement ), true );
			return null != elem ? elem : new PreRowAreaUIElement( this );
		}

		#endregion // CreatePreRowAreaUIElement

        //  BF 12/2/08  NA 9.1 - UltraCombo MultiSelect
            #region OnMouseDown / OnMouseUp
        /// <summary>
        /// Called when the mouse down message is received over the element.
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        /// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
        /// <returns>If true then bypass default processing</returns>
        protected override bool OnMouseDown(MouseEventArgs e, bool adjustableArea, ref UIElement captureMouseForElement)
        {
            ControlUIElementBase controlElement = this.ControlElement;
            UIElement elementAtPoint = controlElement.ElementFromPoint( e.Location );
            if ( elementAtPoint != null )
                System.Diagnostics.Debug.WriteLine( string.Format( "{0} OnMouseDown", elementAtPoint.GetType()) );

            return base.OnMouseDown(e, adjustableArea, ref captureMouseForElement);
        }

        /// <summary>
        /// Called when the mous is released.
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        protected override bool OnMouseUp(MouseEventArgs e)
        {
            ControlUIElementBase controlElement = this.ControlElement;
            UIElement elementAtPoint = controlElement.ElementFromPoint( e.Location );
            if ( elementAtPoint != null )
                System.Diagnostics.Debug.WriteLine( string.Format( "{0} OnMouseUp", elementAtPoint.GetType()) );

            return base.OnMouseUp(e);
        }
            #endregion OnMouseDown / OnMouseUp

		#endregion // Base Overrides
	}

	#region Commented Out Original RowUIElement Class

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Split the RowUIElement into RowUIElementBase class and RowUIElement class so the filter
	// row element can derive from RowUIElementBase.
	// Here is the original commented out RowUIElement class.
	//


	#endregion // Commented Out Original RowUIElement Class

	#endregion // RowUIElement Class
}
