#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Reflection;
using System.Drawing.Design;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// BandsCollectionUITypeEditor prevents the default collection type editor
	/// from displaying an ellipsis button.
	/// </summary>
	// AS 1/8/03 - FxCop
	// Added attribute to prevent FxCop violations
	
	public sealed class BandsCollectionUITypeEditor : UITypeEditor
	{

		/// <summary>
		/// Used to determine the type of UIEditor that will be displayed.
		/// </summary>
		/// <param name="context">ITypeDescriptorContext</param>
		/// <returns>UITypeEditorEditStyle specifying the type of UIEditor.</returns>
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context) 
		{
			return UITypeEditorEditStyle.None;
		}

	}

	/// <summary>
	/// Summary description for BandsCollectionConverter.
	/// </summary>
	public class BandsCollectionConverter : TypeConverter
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public BandsCollectionConverter()
		{
			
		}

        /// <summary>
        /// Returns whether this object supports properties, using the specified context.
        /// </summary>
        /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
        /// <returns>true if <see cref="System.ComponentModel.TypeConverter.GetProperties(System.Object)"/> should be called to find the properties of this object; otherwise, false.</returns>
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return true;
		}
		
		/// <summary>
        /// Returns a collection of properties for the type of array specified by the
        /// value parameter, using the specified context and attributes.
		/// </summary>
        /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
        /// <param name="value">An <see cref="System.Object"/> that specifies the type of array for which to get properties.</param>
        /// <param name="attributes">An array of type <see cref="System.Attribute"/> that is used as a filter.</param>
        /// <returns>A <see cref="System.ComponentModel.PropertyDescriptorCollection"/> with the properties that are exposed for this data type, or null if there are no properties.</returns>
		public override PropertyDescriptorCollection GetProperties(
					ITypeDescriptorContext context,
					object value,
					Attribute[] attributes )
		{
			PropertyDescriptorCollection props = null;

			BandsCollection bands = value as BandsCollection;

			// JJD 9/25/01 - UWG313
			// Added a check to make sure band zero has at least one column
			//
			// JJD 11/26/01 - UWG290
			// Removed check for columns count since we also want to
			// expose band 0 at design time so they can add columns 
			// even if they aren't hooked up to a data source.
			//
			if ( bands != null		&& 
				 bands.Count > 0 ) //	&&
				 //bands[0].Columns.Count > 0 )
			{
				int bandCount = bands.Count;

				// JJD 2/5/02
				// If this is a dropdown or combo we only want to show band 0
				// so break out of for loop
				//
				if ( bands.Layout != null &&
					bands.Layout.Grid is UltraDropDownBase )
					bandCount = 1;

				PropertyDescriptor [] propArray = new PropertyDescriptor[bandCount];
				int i = 0;

				foreach ( UltraGridBand band in bands )
				{
					// JJD 2/5/02
					// Use a string builder to append the key to the index value
					//
					System.Text.StringBuilder sb = new System.Text.StringBuilder(3);

					sb.Append( i );

					string key = band.Key;

					if ( key != null && key.Length > 0 )
					{
						sb.Append( " - " );
						sb.Append( key );
					}

					propArray[i] = new BandPropertyDescriptor( band, sb.ToString() );	
					
					// JJD 2/5/02
					// If this is a dropdown or combo we only want to show band 0
					// so break out of for loop
					//
					if ( band.Layout != null &&
						band.Layout.Grid is UltraDropDownBase )
						break;

					i++;
				}

				props = new PropertyDescriptorCollection( propArray );
			}
			return props;
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public class BandPropertyDescriptor : PropertyDescriptor 
	{

        private UltraGridBand band = null ;
        private string name = null ;

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="band">UltraGridBand</param>
        /// <param name="name">name</param>
        public BandPropertyDescriptor(UltraGridBand band, string name) : base( name, null) 
		{
            this.band = band;
			this.name = name;
        }


		/// <summary>
		/// Gets category
		/// </summary>
        public override string Category 
		{ 
            get 
			{
                return typeof(UltraGridBand).Name;
			}
        }
  
		/// <summary>
		/// Gets/SEts resource name
		/// </summary>
        public string ResourceName {
            get { return name ; }
            set { name = value ; }
        }

		/// <summary>
		/// Gets resource value
		/// </summary>
        public object ResourceValue {
            get { return this.band ; }
        }

		/// <summary>
		/// Gets component type
		/// </summary>
        public override Type ComponentType {
             get {
                return typeof(UltraGridBand);
            }
        }


		/// <summary>
		/// Return false
		/// </summary>
        public override bool IsReadOnly {
             get {
                return false;
            }
        }


		/// <summary>
		/// Gets property type
		/// </summary>
        public override Type PropertyType {
             get {
                return typeof(UltraGridBand);
            }
        }


		/// <summary>
		/// Returns true
		/// </summary>
        /// <param name="component">The component to test for reset capability.</param>
        /// <returns>Returns true</returns>
        public override bool CanResetValue(object component) {
            return true ;
        }

		/// <summary>
		/// Returns the band object
		/// </summary>
		/// <param name="component">The component with the property for which to retrieve the value.</param>
		/// <returns>The band</returns>
        public override object GetValue(object component) {
            return this.band;
        }

		/// <summary>
        /// Resets the Band
		/// </summary>
        /// <param name="component">The component with the property value that is to be reset to the default value.</param>
        public override void ResetValue(object component) {
			this.band.Reset();
        }

		/// <summary>
		/// Does nothing
		/// </summary>
        /// <param name="component">The component with the property value that is to be set.</param>
        /// <param name="value">The new value.</param>
        public override void SetValue(object component, object value) {
        }

		/// <summary>
        /// Returns the ShouldSerialize of the band
		/// </summary>
        /// <param name="component">The component with the property to be examined for persistence.</param>
        /// <returns>Returns the ShouldSerialize of the band</returns>
        public override bool ShouldSerializeValue(object component) {
			// AS 6/15/04
			// If the band is dirty, it should be displayed as such.
			//
            //return false;
			return this.band.ShouldSerialize();
        }
    }   
}
