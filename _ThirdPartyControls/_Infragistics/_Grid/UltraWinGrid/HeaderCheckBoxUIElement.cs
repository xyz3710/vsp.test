#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Collections.Generic;
using System.Text;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{

    // CDS NAS v9.1 Header CheckBox

    /// <summary>
    /// Checkbox UIElement class used for implementing the Header Checkbox functionality.
    /// </summary>
    public class HeaderCheckBoxUIElement : CheckBoxUIElement
    {
        #region Constants

        internal const int HEADERCHECKBOXPADDING = 3;

        #endregion Constants

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parent">Parent UIElement</param>
        public HeaderCheckBoxUIElement(UIElement parent) : base(parent)
        {
        }

        #endregion Constructor

        #region Properties

        #region Column

        /// <summary>
        /// The associated Column object (read-only)
        /// </summary>
        public UltraGridColumn Column
        {
            get
            {
                return (UltraGridColumn)this.GetContext(typeof(UltraGridColumn));
            }
        }

        #endregion Column

        #region Rows

        /// <summary>
        /// The associated RowsCollection object (read-only)
        /// </summary>
        public RowsCollection Rows
        {
            get
            {
                return (RowsCollection)this.GetContext(typeof(RowsCollection));
            }
        }

        #endregion Rows

        #region Header

        /// <summary>
        /// The associated Header object (read-only)
        /// </summary>
        public ColumnHeader Header
        {
            get
            {
                return (ColumnHeader)this.GetContext(typeof(ColumnHeader));
            }
        }

        #endregion Header

        #region Grid

        // CDS 02/02/09 TFS12512 - Changed UltraGrid to UltraGridBase
        /// <summary>
        /// The associated UltraGridBase object (read-only)
        /// </summary>
        public UltraGridBase Grid
        {
            get
            {
                return (UltraGridBase)this.GetContext(typeof(UltraGridBase));
            }
        }

        #endregion Grid

        #endregion Properties

        #region Overrides

        #region OnClick

        /// <summary>
        /// Called when this element is clicked. 		
        /// </summary>
        protected override void OnClick()
        {
            if (this.leftButtonDown)
                this.DoClick();

            this.leftButtonDown = false;
        }

        #endregion OnClick

        #endregion Overrides

        #region Private/Internal Methods

        private void DoClick()
        {
            
            // CDS 02/02/09 TFS12512 -  Changed from UltraGrid to UltraGridBase for UltraCombo support.
            //UltraGrid grid = this.Grid;
            UltraGridBase grid = this.Grid;

            // do nothing if in design-mode
            // CDS 02/02/09 TFS12512 -  Put in a null check.
            //if (grid.DesignMode)
            if (grid == null ||
                grid.DesignMode)
                return;

            UltraGridColumn column = this.Column;
            RowsCollection rows = this.Rows;

            base.OnClick();

            if (column != null)
            {
                switch (column.HeaderCheckBoxSynchronizationResolved)
                {
                    case HeaderCheckBoxSynchronization.Band:
                        column.SetHeaderCheckedState(null, this.CheckState);
                        break;
                    case HeaderCheckBoxSynchronization.RowsCollection:
                    case HeaderCheckBoxSynchronization.None:
                    default:
                        if (rows != null)
                            column.SetHeaderCheckedState(rows, this.CheckState);
                        break;
                }
            }
        }

        #endregion Private/Internal Methods

    }
}
