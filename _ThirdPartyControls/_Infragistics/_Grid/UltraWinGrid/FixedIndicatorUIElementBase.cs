#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using System.Diagnostics;
using System.Drawing.Imaging;


namespace Infragistics.Win.UltraWinGrid
{
	// SSP 3/14/05 - NAS 5.2 Fixed Rows
	// Separated FixedHeaderIndicatorUIElement into FixedIndicatorUIElementBase and
	// FixedHeaderIndicatorUIElement so the FixedRowIndicatorUIElement can derive from
	// FixedIndicatorUIElementBase.
	//

	#region FixedIndicatorUIElementBase Class

	/// <summary>
	/// Base UIElement calss for fixed header and row indicators.
	/// </summary>
	public abstract class FixedIndicatorUIElementBase : ButtonUIElementBase
	{
		#region FixedIndicatorUIElementBase

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		public FixedIndicatorUIElementBase( UIElement parent ) : base( parent )
		{
		}

		#endregion // FixedHeaderIndicatorUIElementBase

		#region ButtonStyle

		/// <summary>
		/// Gets the button style for the element.
		/// </summary>
		public override UIElementButtonStyle ButtonStyle
		{
			get 
			{ 
				return UIElementButtonStyle.PopupSoftBorderless;
			}
		}
		
		#endregion //ButtonStyle

		#region DrawTheme

		// SSP 8/8/03 UWG2575
		// Overrode DrawTheme. Since fixed header indicator element derives from button element
		// we don't want the base class to draw the button themed.
		//
		/// <summary>
		/// Handles the rendering of the element using the system themes.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		/// <returns>A boolean indicating if the element was able to successfully
		/// render with the system theme. Used to determine whether the remaining
		/// draw methods are invoked.</returns>
		protected override bool DrawTheme( ref UIElementDrawParams drawParams )
		{
			return false;
		}
		
		#endregion //	DrawTheme

		#region DrawBackColor

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{			
		}

		#endregion // DrawBackColor

		#region DrawImageBackground

		/// <summary>
		/// this element doesn't draw an image background
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{			
		}

		#endregion // DrawImageBackground

		#region DrawFocus

		/// <summary>
		/// does nothing as SwapButtonUIElement does not get a focus
		/// </summary>
		protected override void DrawFocus ( ref UIElementDrawParams drawParams )
		{			
			// doesn't need a focus
		}

		#endregion // DrawFocus

		#region GetButtonImage

		internal abstract System.Drawing.Image GetButtonImage( out bool setTransparencyKey, out UltraGridLayout layout );

		#endregion // GetButtonImage

		#region DrawForeground
		
		/// <summary>
		/// Draws small pin icon in the foreground.
		/// </summary>
		protected override void DrawForeground ( ref UIElementDrawParams drawParams ) 
		{
			bool setTransparencyKey;
			UltraGridLayout layout;
			System.Drawing.Image image = this.GetButtonImage( out setTransparencyKey, out layout );
			
			Debug.Assert( null != image && null != layout );
			if ( null == image || null == layout )
				return;

			Rectangle rect = this.Rect;
			int borderWidth = layout.GetBorderThickness( UIElementBorderStyle.RaisedSoft );
			rect.Inflate( -borderWidth, -borderWidth );
			
			// Don't do the color maps if the user specified the image.
			//
			if ( setTransparencyKey )
			{
				ImageAttributes imageAttr = new ImageAttributes( );

				// SSP 1/12/04 UWG2742
				// Added following block of code.
				//
				// --------------------------------------------------------------------------------
				// SSP 8/12/05 BR05338
				// 
				// ----------------------------------------------------
				AppearanceData appData = drawParams.AppearanceData;
				
				// ----------------------------------------------------

				// If the fore color of the appearance data is white which is also the
				// transparent back color of the filter image (later on we are setting
				// the color key to this color) then change it slightly so it doesn't
				// get treated as transparent back color by the SetColorKey(255,255,255)
				// below.
				//
				Color foreColor = appData.ForeColor;
				if ( 255 == foreColor.R && 255 == foreColor.G && 255 == foreColor.B )
					appData.ForeColor = Color.FromArgb( foreColor.A, 254, 255, 255 );

				ColorMap[] cmaps = new ColorMap[] { new ColorMap( ), new ColorMap( ) };
				cmaps[0].OldColor = Color.FromArgb( 0, 0, 0 );
				cmaps[0].NewColor = appData.ForeColor;

				cmaps[1].OldColor = Color.FromArgb( 64, 64, 64 );
				cmaps[1].NewColor = UltraGridLayout.GetColorShade( appData.ForeColor, 64 );
				imageAttr.SetRemapTable( cmaps );
				// --------------------------------------------------------------------------------

				imageAttr.SetColorKey( Color.FromArgb( 255, 255, 255 ), Color.FromArgb( 255, 255, 255 ) );

				//drawParams.DrawImage( image, imageRect, new Rectangle( Point.Empty, imageRect.Size ), imageAttr );
				drawParams.DrawImage( image, rect, true, imageAttr, true );

				if ( null != imageAttr )
					imageAttr.Dispose( );
				imageAttr = null;

				return;
			}
			
			drawParams.DrawImage( image, rect, true, null, true );

			// SSP 3/29/05 - NAS 5.2 Fixed Rows
			// This is the original code
			//
			
		}

		#endregion // DrawForeground

		#region UIRole

		// SSP 5/25/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				// Return null as not much can be done using this role. Background is transparent.
				// The image is set via custom properties. Even though the base class is a button,
				// it does not behave like a button as it's too small and simply meant to draw an
				// icon.
				// 
				return null;
			}
		}

		#endregion // UIRole
	}

	#endregion // FixedIndicatorUIElementBase Class
}

