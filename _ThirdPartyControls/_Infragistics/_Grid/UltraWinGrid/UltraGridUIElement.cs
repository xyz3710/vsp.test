#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
	using System.Windows.Forms;
    using System.Diagnostics;

	// JM 04-21-04 - NAS 2004 Vol 2
	using Microsoft.Win32;
	using System.Security;
	using System.Security.Permissions;

	using Infragistics.Shared;
	using Infragistics.Win;
	using System.ComponentModel;


    /// <summary>
    /// The main element for an UltraGrid (occupies the
    /// entire client area.
    /// </summary>
	public class UltraGridUIElement : ControlUIElementBase
	{
		private CaptionAreaUIElement    captionArea = null;
		private DataAreaUIElement       dataArea = null;
		private AddNewBoxUIElement		addNewBoxArea = null;
		private GroupByBoxUIElement		groupByBoxArea = null;
		private PageFooterUIElement		pageFooterArea = null;
		private PageHeaderUIElement		pageHeaderArea = null;

		// JM 03-25-04 - NAS 2004 Vol 2
		private DesignerPromptAreaUIElement		designerPromptAreaElement = null;

		private Infragistics.Win.UltraWinGrid.UltraGridLayout layout;
				
		// SSP 10/18/05 BR07074
		// 
		internal bool inVerifyChildElements = false;

		internal UltraGridUIElement( UltraGridBase grid, 
									 Infragistics.Win.UltraWinGrid.UltraGridLayout layout,
									 bool hookEvents ) : base( grid.ControlForGridDisplay, grid, hookEvents )
		{
			this.layout            = layout;
			this.PrimaryContext    = layout;
		}

		internal DataAreaUIElement DataAreaElement
		{
			get
			{
				return this.dataArea;
			}
		}

		/// <summary>
		/// Returns the Layout object that determines the layout of the object. 
		/// This property is read-only at run-time.
		/// </summary><remarks><para>The Layout property of an object is used to access 
		/// the Layout object that determines the settings of various properties 
		/// related to the appearance and behavior of the object. The Layout object 
		/// provides a simple way to maintain multiple layouts for the grid and 
		/// apply them as needed. You can also save grid layouts to disk, the 
		/// registry or a storage stream and restore them later.</para><para>The 
		/// Layout object has properties such as Appearance and Override, so the 
		/// Layout object has sub-objects of these types, and their settings are 
		/// included as part of the layout. However, the information that is 
		/// actually persisted depends on how the settings of these properties were 
		/// assigned. If the properties were set using the Layout object's intrinsic 
		/// objects, the property settings will be included as part of the layout. 
		/// However, if a named object was assigned to the property from a 
		/// collection, the layout will only include the reference into the collection, 
		/// not the actual settings of the named object. 
		/// (For an overview of the difference between named and intrinsic objects, 
		/// please see the <see cref="UltraGridLayout.Appearance"/>property.</para>
		/// <para>
		/// For example, if the Layout object's Appearance property is used to 
		/// set values for the intrinsic Appearance object like this:</para><pre>
		/// UltraGrid1.DisplayLayout.Appearance.ForeColor = System.Drawing.Color.Blue</pre><para>
		/// Then the setting (in this case, ForeColor) will be included as part 
		/// of the layout, and will be saved, loaded and applied along with the 
		/// other layout data. However, suppose you apply the settings of a named 
		/// object to the UltraGridLayout's Appearance property in this manner:</para>
		/// <p class="code">UltraGrid1.Appearances.Add "New1"</p>
		/// <p class="code">UltraGrid1.Appearances("New1").ForeColor = System.Drawing.Color.Blue</p>
		/// <p class="code">UltraGrid1.Layout.Appearance = UltraGrid1.Appearances("New1")</p>
		/// <para>In this case, the ForeColor setting will not be persisted as 
		/// part of the layout. Instead, the layout will include a reference to 
		/// the "New1" Appearance object and use whatever setting is present in 
		/// that object when the layout is applied.</para><para>By default, the 
		/// layout includes a copy of the entire Appearances collection, so if the 
		/// layout is saved and restored using the default settings, the object 
		/// should always be present in the collection when it is referred to. 
		/// However, it is possible to use the Load and Save methods of the Layout object 
		/// in such a way that the collection will not be re-created when the layout 
		/// is applied. If this is the case, and the layout contains a reference to 
		/// a nonexistent object, the default settings for that object's properties 
		/// will be used.</para>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridLayout Layout
		{
			get
			{
				return this.layout;
			}
		}

		/// <summary>
		/// Returns true if this is the print layout (read-only)
		/// </summary>
		public override bool IsPrint
		{
			get
			{
				return this.Layout.IsPrintLayout;
			}
		}

		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		#region PrintColorMode
		/// <summary>
		/// Indicates how images and colors are rendered when printed.
		/// </summary>
		public override Infragistics.Win.ColorRenderMode PrintColorMode
		{
			get 
			{ 
				UltraGridLayout layout = this.Layout;
				UltraGrid grid = this.Grid as UltraGrid;

				if (layout != null				&&
					layout.IsPrintLayout		&&
					grid != null				&&
					grid.IsPrinting				&&
					grid.PrintManager != null	&&
					grid.PrintManager.PrintDocument is UltraGridPrintDocument)
				{
					return ((Infragistics.Win.Printing.UltraPrintDocument)grid.PrintManager.PrintDocument).PrintColorStyle;
				}

				return Infragistics.Win.ColorRenderMode.Color; 
			}
		}
		#endregion //PrintColorMode

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			                                     ref AppearancePropFlags requestedProps )
		{
            // MRS v7.2 - PDF Report Writer
            // Moved this into a helper method on the Layout. 
            //
            #region Old Code
            
            #endregion //Old Code
            //
            UltraGridLayout layout = this.Layout;
            layout.ResolveLayoutAppearance(ref appearance, ref requestedProps, true);
            base.InitAppearance(ref appearance, ref requestedProps);
		}

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// JJD 12/17/01
				// Moved logic into BorderStyleResolved property
				//
				return this.Layout.BorderStyleResolved;
			}
		}

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return Border3DSide.All;
			}
		}

		
		// SSP 7/17/06 BR12782
		// 
		

		// SSP 12/17/03 UWG2799
		// Added InternalInitializeRect method.
		//
		internal void InternalInitializeRect( bool verify )
		{
			Debug.Assert( null != this.Grid, "No grid !" );

			if ( null != this.Grid )
			{
				Rectangle rect = new Rectangle( new Point( 0, 0 ), this.Grid.ControlForGridDisplay.Size );

				if ( verify )
				{
					this.Rect = rect;

					// SSP 7/17/06 BR12782
					// This is related to the other change that was made of commenting out the overridden
					// Rect property.
					// 
					this.VerifyChildElements( false );
				}
				else
					base.Rect = rect;
			}
		}

		#region VerifyChildElements

		// SSP 10/18/05 BR07074
		// Overrode VerifyChildElements method.
		// 
        /// <summary>
        /// Called during a drawing operation to ensure that all child elements are created and positioned properly. If the ChildElementsDirty flag is true then the default implementation will call PositionChildElements and reset the flag.
        /// </summary>
        /// <param name="controlElement">The control's main UIElement</param>
        /// <param name="recursive">If true will call this method on all descendant elements</param>
        protected override void VerifyChildElements(ControlUIElementBase controlElement, bool recursive)
		{
			bool origInVerifyChildElements = this.inVerifyChildElements;

			try
			{
				this.inVerifyChildElements = true;
				base.VerifyChildElements( controlElement, recursive );
			}
			finally
			{
				this.inVerifyChildElements = origInVerifyChildElements;
			}
		}

		#endregion // VerifyChildElements

		//int tmp = 0;
		
		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
//			if ( UltraGridDisplayLayout.debug ) 
//				Debug.WriteLine( ++tmp + ": Child Elements Verified." );

			this.ChildElements.Clear();

			//Always create a dataArea
			//
			if ( null == this.dataArea || this.dataArea.Disposed )
				this.dataArea = new DataAreaUIElement( this );

			Rectangle dataRect = this.RectInsideBorders;

			UltraGrid grid = this.Grid as UltraGrid;

            //If we are printing create a pagefooter and pageheader
			//
			// SSP 1/27/05 BR02053
			// Instead of checking IsPrinting flag, make use of IsDisplayLayout. What we are 
			// interested in finding out is this element is in the print layout or not. Simply
			// checking the IsPrinting flag won't tell us that the display layout could verify 
			// it's ui elements while printing (like when the grid gets a paint while printing,
			// especially with the new UltraGridPrintDocument).
			//
			//if ( grid != null && grid.IsPrinting )
			if ( this.Layout.IsPrintLayout )
			{
				//Get the LogicalPageInfo of the current page
				//
				LogicalPageInfo printingLogicalPageInfo = grid.PrintManager.GetLogicalPageInfo( grid.PrintManager.CurrentPrintingPage.y );

				Rectangle footerRect = dataRect;
				Rectangle headerRect = dataRect;

				//Set foot and header rects
				//
				if ( printingLogicalPageInfo == null )
				{
					footerRect.Height = 0;
					headerRect.Height = 0;

					headerRect.Height += ( 2 * grid.PrintLayout.GetBorderThickness( this.BorderStyle ) );
					footerRect.Height += ( 2 * grid.PrintLayout.GetBorderThickness( this.BorderStyle ) );
				}

				else
				{
					footerRect.Height = printingLogicalPageInfo.LogicalPageLayoutInfo.PageFooterHeight;
					headerRect.Height = printingLogicalPageInfo.LogicalPageLayoutInfo.PageHeaderHeight;

					headerRect.Height += ( 2 * grid.PrintLayout.GetBorderThickness( printingLogicalPageInfo.LogicalPageLayoutInfo.PageHeaderBorderStyle ) );
					footerRect.Height += ( 2 * grid.PrintLayout.GetBorderThickness( printingLogicalPageInfo.LogicalPageLayoutInfo.PageFooterBorderStyle ) );
				}

				if ( footerRect.Height == -1 )
					footerRect.Height = 0;

				if ( headerRect.Height == -1 )
					headerRect.Height = 0;				

				// if header height is less than 0 no need to add it
				//
				if ( headerRect.Height > 0 )
				{
					if ( null == this.pageHeaderArea || this.pageHeaderArea.Disposed )
						this.pageHeaderArea = new PageHeaderUIElement( this );

					//Position dataArea under page header
					//
					dataRect.Height -= headerRect.Height;
					dataRect.Offset( 0, headerRect.Height );

					//Add header
					//
					this.ChildElements.Add( this.pageHeaderArea );
					this.pageHeaderArea.Rect = headerRect;
				}

				// if footer height is less than 0 no need to add it
				//
				if ( footerRect.Height > 0 )
				{				
					if ( null == this.pageFooterArea || this.pageFooterArea.Disposed)
						this.pageFooterArea = new PageFooterUIElement( this );
                    
					//Make room for footer
					//
					dataRect.Height -= footerRect.Height;

					//Position footer
					//
					footerRect.Offset( 0, headerRect.Height + dataRect.Height );
					
					//Add footer
					//
					this.ChildElements.Add( this.pageFooterArea );
					this.pageFooterArea.Rect = footerRect;		
				}							
			}

				//create a caption area and an addnewbox area
				//
			else if ( grid != null )
			{

				Rectangle captionRect = dataRect;
				Rectangle addNewBoxRect = dataRect;				
				//ROBA UWG160 8/15/01 we were not caching the height
				captionRect.Height = this.layout.CachedCaptionHeight;
				//captionRect.Height = 20; 

				if ( null == this.captionArea || this.captionArea.Disposed )
					this.captionArea = new CaptionAreaUIElement( this );			

				if ( null == this.addNewBoxArea || this.addNewBoxArea.Disposed )
				{
					if ( !this.Layout.AddNewBox.Hidden )
					{
						this.addNewBoxArea = new AddNewBoxUIElement( this );
						this.addNewBoxArea.InitializeAddNewBox( this.Layout.AddNewBox );
					}
				}

				if ( null == this.groupByBoxArea || this.groupByBoxArea.Disposed )
				{
					if ( !this.Layout.GroupByBox.HiddenResolved )					
					{
						this.groupByBoxArea = new GroupByBoxUIElement( this );
						this.groupByBoxArea.InitializeGroupByBox( this.Layout.GroupByBox );
					}
				}

				// ----------------------------
				// JM 03-25-04 - NAS 2004 Vol 1 - Create a DesignerPromptAreaUIElement if we are at designtime.
				// JM 04-22-04 - NAS 2004 Vol 1 - Also check the the registry setting that stores a user preference.
				if (grid.DesignMode  &&  this.CreateDesignerPromptArea())
					this.designerPromptAreaElement = new DesignerPromptAreaUIElement(this);			
				// ----------------------------

				//Since we are not printing make room for caption
				//
				// RobA UWG665 11/27/01 
				// do not add a caption if there is none
				//
				if ( grid.HasCaption )
				{
					dataRect.Height -= captionRect.Height;						
					dataRect.Offset( 0, captionRect.Height );
				}

				if ( !this.Layout.GroupByBox.HiddenResolved )
				{
					Rectangle groupByBoxRect = dataRect;
					groupByBoxRect.Height = Math.Min( dataRect.Height / 2, this.Layout.GroupByBox.IdealHeight );
					
					this.groupByBoxArea.Rect = groupByBoxRect;

					dataRect.Y += groupByBoxRect.Height;
					dataRect.Height -= groupByBoxRect.Height;

					this.ChildElements.Add( this.groupByBoxArea );
				}

				// ----------------------------
				// JM 03-25-04 - NAS 2004 Vol 2
				int			totalSpaceNeededAtBottom = 0;
				Rectangle	designerPromptAreaRect = dataRect;

				// JM 04-22-04 - NAS 2004 Vol 1 - Also check the the registry setting that stores a user preference.
				if (grid.DesignMode == true  &&  this.CreateDesignerPromptArea())
					totalSpaceNeededAtBottom += UltraGrid.DESIGNER_PROMPT_AREA_HEIGHT;
				if (!this.Layout.AddNewBox.Hidden)
					totalSpaceNeededAtBottom += this.layout.AddNewBoxMaxHeight;

				dataRect.Height	-= totalSpaceNeededAtBottom;
				// ----------------------------

				//Create a addnewBox only if it is not hidden
				//and we are not printing
				//
				if ( !this.Layout.AddNewBox.Hidden )
				{
					addNewBoxRect = dataRect;
					addNewBoxRect.Height = this.layout.AddNewBoxMaxHeight;

					// JM 03-25094 - NAS 2004 Vol1 - We are now subtracting this out above.
					//dataRect.Height -= addNewBoxRect.Height;

					addNewBoxRect.Offset(0, dataRect.Height );
				}

				// ----------------------------
				// JM 03-25-04 - NAS 2004 Vol 1 - Setup a rect for the designer prompt area element if we are at designtime.
				// JM 04-22-04 - NAS 2004 Vol 1 - Also check the the registry setting that stores a user preference.
				if (grid.DesignMode  &&  this.CreateDesignerPromptArea())
				{
					designerPromptAreaRect			= dataRect;
					designerPromptAreaRect.Height	= UltraGrid.DESIGNER_PROMPT_AREA_HEIGHT;

					designerPromptAreaRect.Offset(0, dataRect.Height);
					if (!this.Layout.AddNewBox.Hidden)
						designerPromptAreaRect.Offset(0, addNewBoxRect.Height);
				}
				// ----------------------------

				//if CanMergeAdjacentBorders
				//adjust widths and heights of caption area to overlap each other
				//
				// RobA UWG665 11/27/01 
				// do not add a caption if there is none
				//
				if ( grid.HasCaption )
				{
					// SSP 3/25/06 - App Styling
					// Use the BorderStyleCaptionResolved instead of BorderStyleHeaderDefault. 
					// BorderStyleCaptionResolved returns the actual border style that the caption will end up
					// using where as the BorderStyleHeaderDefault gives the default border style of the headers.
					//
					//if ( this.Layout.CanMergeAdjacentBorders( this.Grid.DisplayLayout.BorderStyleHeaderDefault ) )
					if ( this.Layout.CanMergeAdjacentBorders( this.Layout.BorderStyleCaptionResolved ) )
					{
						// JJD 1/21/02 - UWG934
						// Only merge if themes aren't active
						//
						// AS 3/20/06 AppStyling
						//if ( !this.Layout.Grid.SupportThemes ||
						if ( !this.Layout.Grid.UseOsThemesResolved ||
							this.Layout.IsPrintLayout )
						{
							captionRect.Y--;
							captionRect.Height++;
							captionRect.X--;
							// JJD 1/17/02
							// Add 2 to the width to account for overlapping both the left
							// and right caption borders
							//
							captionRect.Width += 2;				
						}
					}	

					//Since we are not printing add caption
					//					
					this.ChildElements.Add( this.captionArea );
					this.captionArea.Rect = captionRect;
				}

				//if we are not printing and the addnewbox is not 
				//hidden add it
				//
				if ( !this.Layout.AddNewBox.Hidden )
				{
					this.ChildElements.Add( this.addNewBoxArea );
					this.addNewBoxArea.Rect = addNewBoxRect;
				}

				// ----------------------------
				// JM 03-25-04 - NAS 2004 Vol 1 - Add a DesignerPromptAreaUIElement if we are at designtime.
				// JM 04-22-04 - NAS 2004 Vol 1 - Also check the the registry setting that stores a user preference.
				if (grid.DesignMode  &&  this.CreateDesignerPromptArea())
				{
					this.ChildElements.Add(this.designerPromptAreaElement);
					this.designerPromptAreaElement.Rect = designerPromptAreaRect;
				}
				// ----------------------------
			}
			else
			{
				// JM 04-05-04 - NAS 2004 Vol 1 - If this control is an UltraDropDown and we are at designtime
				//								  create a DesignerPromptAreaUIElement. 
				// JM 04-22-04 - NAS 2004 Vol 1 - Also check the the registry setting that stores a user preference.
				if (this.Grid.DesignMode  &&  this.CreateDesignerPromptArea())
				{
					UltraDropDown ultraDropDown = this.Grid as UltraDropDown;
					if (ultraDropDown != null)
					{
						this.designerPromptAreaElement = new DesignerPromptAreaUIElement(this);			

						// Adjust the height of the data rect to account for the designer prompt area.
						dataRect.Height -= UltraGrid.DESIGNER_PROMPT_AREA_HEIGHT;
						

						// Setup the designer prompt area rect.
						Rectangle	designerPromptAreaRect	= dataRect;

						designerPromptAreaRect.Height		= UltraGrid.DESIGNER_PROMPT_AREA_HEIGHT;
						designerPromptAreaRect.Offset(0, dataRect.Height);

						this.designerPromptAreaElement.Rect = designerPromptAreaRect;


						// Add the element.
						this.ChildElements.Add(this.designerPromptAreaElement);
					}
				}
			}

			//Add the data area
			//
			this.ChildElements.Add( this.dataArea );
			this.dataArea.Rect = dataRect;		
		
		}

		/// <summary>
		/// The related UltraGrid control
		/// </summary>
		public UltraGridBase Grid
		{
			get
			{
				return this.Layout.Grid;
			}
		}

        /// <summary>
        /// Called when a mouse down message is received. Returning true will cause normal mouse down processing to be skipped
        /// </summary>
        /// <param name="msgInfo">The <see cref="Infragistics.Win.MouseMessageInfo"/> providing mouse information.</param>
        /// <returns>true if the mouse processing has been handled.</returns>
        protected override bool OnPreMouseDown(ref MouseMessageInfo msgInfo)
		{
			if ( ! ( this.Grid is Infragistics.Win.UltraWinGrid.UltraGrid ) )
				return false;

			UIElement element = this.ElementFromPoint( new Point( msgInfo.X, msgInfo.Y ) );

			if ( element != null )
			{
				// JJD 12/19/01
				// Only set the active row and col scroll regions if they
				// are over an intersection element
				//
				if ( element is RowColRegionIntersectionUIElement ||
					 element.GetAncestor( typeof( RowColRegionIntersectionUIElement ) ) != null )
				{
					RowScrollRegion rsr = (RowScrollRegion)element.GetContext( typeof( RowScrollRegion ) );
					ColScrollRegion csr = (ColScrollRegion)element.GetContext( typeof( ColScrollRegion ) );

					// SSP 9/11/03 UWG2218
					// When the user clicks on the same cell that's in edit mode in a different rowcol 
					// region nothing happens. In this case we should exit the edit mode and re-enter
					// in the rowcol region that was clicked.
					//
					// ------------------------------------------------------------------------------------
					bool setActiveRegion = true;
					UltraGrid gridCtrl = this.Grid as UltraGrid;
					if ( null != csr && null != rsr && null != gridCtrl
						&& ( csr != gridCtrl.DisplayLayout.EmbeddableElementBeingEdited_ColScrollRegion ||
						     rsr != gridCtrl.DisplayLayout.EmbeddableElementBeingEdited_RowScrollRegion ) )
					{
						UltraGridCell activeCell = gridCtrl.ActiveCell;

						if ( null != activeCell && activeCell.IsInEditMode )
						{
							UltraGridCell cellUnderMouse = (UltraGridCell)element.GetContext( typeof( UltraGridCell ), true );
							if ( cellUnderMouse == activeCell )
							{
								activeCell.ExitEditMode( false, false );

								// If the cell is still in edit mode then don't change the active row/col regions.
								//
								if ( activeCell.IsInEditMode )
									setActiveRegion = false;
							}
						}
					}
					// ------------------------------------------------------------------------------------

					// SSP 9/11/03 UWG2218
					// Related to above change. Enclosed the below code in the if block.
					//
					if ( setActiveRegion )
					{
						if ( rsr != null )
							this.Grid.ActiveRowScrollRegion = rsr;

						if ( csr != null )
							this.Grid.ActiveColScrollRegion = csr;
					}
				}

				if ( msgInfo.Buttons != MouseButtons.Right &&
					this.Grid.Enabled && !this.Grid.ContainsFocus )
				{
					// AS - 1/14/02
					// We need to assert putting focus on the control in order for this to work
					// when the assembly is local but the executable is running under internet
					// type restricted security access. However, to prevent a security hole, we
					// will only do so on controls that have our public key token.
					//
					//this.Grid.Focus();
					if (!this.Grid.DesignMode)
					{
						try
						{
                            // MRS 12/19/06 - fxCop
                            //if ( DisposableObject.HasSamePublicKey( this.Grid.GetType() ))
                            //{
                            //    System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

                            //    perm.Assert();
                            //}

                            //this.Grid.Focus();
                            if (DisposableObject.HasSamePublicKey(this.Grid.GetType()))
                                GridUtils.FocusControl(this.Grid);
                            else
                                this.Grid.Focus();
						}
						catch {}
					}


					// AS - 11/21/01
					// Do not bail out if we are in design mode.
					//
					// SSP 11/20/01 UWG508
					// If the grid was not able to get the focus, then return true
					// causing the message to be bypassed.
					//
					if ( !this.Grid.ContainsFocus && !this.Grid.DesignMode )
						return true;
				}
			}

			return false;
		}


		
		/// <summary>
		/// Draws the element.
		/// </summary>
		/// <param name="graphics">Graphics object to render into</param>
		/// <param name="invalidRectangle">Invalidated area</param>
		/// <param name="doubleBuffer">True if the element should double buffer the rendering</param>
		/// <param name="alphaBlendMode">Determines how the alphablending should be processed</param>
		public override void Draw( Graphics		graphics,
			Rectangle		invalidRectangle, 
			bool			doubleBuffer,
			AlphaBlendMode	alphaBlendMode )
		{
			//ROBA UWG173 8/15/01
			//check to see if rowscrollregion's or colscrollregion's
			//size has changed
			//
			this.Layout.ColScrollRegions.CheckIfSizeChanged();

			this.Layout.RowScrollRegions.CheckIfSizeChanged();

			base.Draw( graphics, invalidRectangle, doubleBuffer, alphaBlendMode );

		}
	
		/// <summary>
		/// 
		/// </summary>
		protected override KeyActionMappingsBase KeyActionMappings
		{ 
			get { return this.Grid.GetKeyActionMappings(); } 
		}

		/// <summary>
		/// The current state.
		/// </summary>
		protected override Int64 CurrentState
		{ 
			get { return this.Grid.GetCurrentState(); } 
		}

        /// <summary>
        /// Performs a specific key action
        /// </summary>
        ///	<param name="actionCode">An enumeration value that determines the user action to be performed.</param>
        ///	<param name="ctlKeyDown">A boolean specifies whether the action should be performed as if the control key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is maintained, as it is when the user holds down the control key and selects a row in a grid.</param>
        ///	<param name="shiftKeyDown">A boolean specifies whether the action should be performed as if the shift key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is extended, as it is when the user holds down the shift key and selects a range of rows in a grid.</param>                
        ///	<returns>true if the action completed successfully, false if the action failed.</returns>
        protected override bool PerformKeyAction(Enum actionCode, bool shiftKeyDown, bool ctlKeyDown)
		{ 
			return this.Grid.PerformKeyAction ( actionCode, shiftKeyDown, ctlKeyDown );
		}


		// SSP 8/12/02 UWG1191
		// Overrode DrawImageBackground for printing. When the ImageBackgroundOrigin
		// is set to form or container, when printing, we shouldn't use the grid's
		// or grid's form's dimensions because for printing it doesn't make sense.
		//
		/// <summary>
		/// Default background picture drawing
		/// </summary>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
			// If this element is not for printing, then just call the base class' 
			// implementation because it will do the right thing.
			//
			if ( null == this.Layout || !this.Layout.IsPrintLayout )
			{
				base.DrawImageBackground( ref drawParams );
				return;
			}

			// Also call the base class for ImageBackgroundOrigins othe than Form and Container
			// since the base class will do the right thing for them as well.
			//
			if ( ImageBackgroundOrigin.Form != drawParams.AppearanceData.ImageBackgroundOrigin &&
				ImageBackgroundOrigin.Container != drawParams.AppearanceData.ImageBackgroundOrigin && 
				// SSP 10/9/03 UWG1191
				// Added below conditions. Skip Client as well.
				// 
				ImageBackgroundOrigin.Client != drawParams.AppearanceData.ImageBackgroundOrigin )
			{
				base.DrawImageBackground( ref drawParams );
				return;
			}

			if( drawParams.AppearanceData.ImageBackground  == null ) 
				return; 

			// get the rect inside the borders
			//
			Rectangle rectInsideBorders = this.RectInsideBorders;

			if ( !rectInsideBorders.IntersectsWith( drawParams.InvalidRect ) )
				return;
            
			// Get the rect inside the borders (which is where the background image
			// is drawn
			//
			Rectangle rectClip = rectInsideBorders;

			//Determine bounding rect based on imageBackground origin prop
            
			Rectangle rectBounds = new Rectangle(0,0,0,0);

			// For printing, its always the 
			switch(drawParams.AppearanceData.ImageBackgroundOrigin)
			{
				case ImageBackgroundOrigin.Form:
				case ImageBackgroundOrigin.Container :
					// SSP 10/9/03 UWG1191
					// Added below case for Client.
					// 
				case ImageBackgroundOrigin.Client:
					rectBounds = rectInsideBorders;
					break;				
			}

			// JJD 8/2/01 - uWG62 and 65
			// Save the existing clipping region so that it can be
			// restored after the image drawing
			//
			// existingClipRegion is used to store the currrent clipping 
			// region so that we can restore it afterward
			//
			Region existingClipRegion = drawParams.Graphics.Clip;

			//set clipping rect
			System.Drawing.Region rgnInsideBorder = new System.Drawing.Region(rectClip);

			drawParams.Graphics.IntersectClip( rgnInsideBorder );
			
			rgnInsideBorder.Dispose();

			//Now that we have our bounding rect and clipping rect go ahead and draw 
			
			switch( drawParams.AppearanceData.ImageBackgroundStyle)
			{
				case ImageBackgroundStyle.Tiled:
				{

					//Tiling
					Rectangle imageRect = new Rectangle(0, 0, drawParams.AppearanceData.ImageBackground.Width, drawParams.AppearanceData.ImageBackground.Height);

					// use a nested loop to tile the image
					//
					for( int y = rectBounds.Top; y < rectBounds.Bottom; y += imageRect.Height )
					{
						// once we pass the invalid are we can break out of the loop
						//
						if ( y > rectClip.Bottom )
							break;

						// if we are into the invalid area then tile left to right
						//
						if ( y + imageRect.Height >= rectClip.Top )
						{
							for(int x = rectBounds.Left; x < rectBounds.Right; x += imageRect.Width )
							{
								// once we pass the invalid are we can break out of the loop
								//
								if ( x > rectClip.Right )
									break;

								// if we are in the invalid area then draw the image
								//
								if ( x + imageRect.Width >= rectClip.Left )
								{
									drawParams.Graphics.DrawImage( drawParams.AppearanceData.ImageBackground,
										x,
										y,
										imageRect.Width,
										imageRect.Height );
								}
							}
						}
					}
					break;
				}

				case ImageBackgroundStyle.Centered:
				{
					Rectangle imageRect = new Rectangle(0, 0, drawParams.AppearanceData.ImageBackground.Width, drawParams.AppearanceData.ImageBackground.Height);
					// center the image vertically and horizontally
					//
					DrawUtility.AdjustHAlign( HAlign.Center, ref imageRect, rectBounds  ); 
					DrawUtility.AdjustVAlign( VAlign.Middle, ref imageRect, rectBounds  ); 
					
					if ( imageRect.IntersectsWith( drawParams.InvalidRect ) )
						drawParams.Graphics.DrawImage(drawParams.AppearanceData.ImageBackground,imageRect);
									
					break;
				}

				case ImageBackgroundStyle.Stretched:
					//drawParams.Draw.Graphics.DrawImage(drawParams.AppearanceData.ImageBackground,rectClip);
					drawParams.Graphics.DrawImage(drawParams.AppearanceData.ImageBackground,rectBounds);
					
					break;

				default:
					//drawParams.Draw.Graphics.DrawImage(drawParams.AppearanceData.ImageBackground,rectClip);
					drawParams.Graphics.DrawImage(drawParams.AppearanceData.ImageBackground,rectBounds);
					break;
			}

			
			// JJD 8/2/01 - uWG62 and 65
			// Restore the original clipping region 
			//
			if ( null != existingClipRegion )
			{
				drawParams.Graphics.Clip = existingClipRegion;
				existingClipRegion.Dispose();
			}

		}

		// SSP 10/9/03 UWG1868
		//
		internal void InternalSuspendSyncMouseEnter( )
		{
			this.SuspendSyncMouseEnter( );
		}

		internal void InternalResumeSyncMouseEnter( )
		{
			this.ResumeSyncMouseEnter( );
		}

		internal bool InternalIsSyncMouseEnterSuspended
		{
			get
			{
				return this.IsSyncMouseEnterSuspended;
			}
		}

		// JM - NAS 2004 Vol 2
		#region CreateDesignerPromptArea

		private bool CreateDesignerPromptArea()
		{
			// This should never happen...
			if (this.Grid.DesignMode == false)
				return false;


			// Assert registry permissions and read the registry for the user's preference.
			RegistryKey regKey = null;
			try
			{
				RegistryPermission perm = new RegistryPermission(RegistryPermissionAccess.AllAccess, Registry.CurrentUser.Name);

				// Assert rights to the current user registry path
				perm.Assert();


				regKey = Registry.CurrentUser.CreateSubKey("Software\\Infragistics\\UltraWinGrid\\DesignerDialog");
				if (regKey != null)
					return Convert.ToBoolean(regKey.GetValue("ShowStartButton", true));
				else
					return true;
			}
			catch
			{
				return true;
			}
			finally
			{
				if (regKey != null)
					regKey.Close();
			}
		}

		#endregion CreateDesignerPromptArea

		#region ProcessKeyDown

		// SSP 6/28/05 BR04831
		// Split UltraGrid.OnKeyDown into ForwardToValueListHelper and ForwardKeyToEditorHelper.
		// Moved the code from the overridden OnKeyDown to these methods. Before the OnKeyDown
		// was called by the framework directly. Now UltraControlBase.OnKeyDown is called. It calls
		// UltraGridUIElement.ProcessKeyDown which in turn calls these two methods. This change was 
		// made because we need to fire KeyDown first then process keys for swap/filter drop down 
		// value lists if they are dropped down and then process the key action mappings.
		// 
		/// <summary>
		/// Overridden. Keydown event handler.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>		
		[EditorBrowsable(EditorBrowsableState.Advanced)]
        // MBS 12/15/06 - FxCop
        //public override void ProcessKeyDown( object sender, KeyEventArgs e )
        public override void ProcessKeyDown(KeyEventArgs e)
		{
			UltraGrid grid = this.Grid as UltraGrid;

			if ( null != grid )
				grid.ForwardToValueListHelper( e );

			if ( ! e.Handled )
                // MBS 12/15/06 - FxCop
                //base.ProcessKeyDown( sender, e );
                base.ProcessKeyDown(e);

			if ( ! e.Handled && null != grid )
				grid.ForwardKeyToEditorHelper( e );
		}

		#endregion // ProcessKeyDown

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.layout, StyleUtils.GetControlAreaRole( this.layout ) );
			}
		}

		#endregion // UIRole
	}
}
