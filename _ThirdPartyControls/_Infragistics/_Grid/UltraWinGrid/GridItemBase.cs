#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
    using System.Drawing;
    using System.ComponentModel;
    using System.ComponentModel.Design;
    using System.Windows.Forms;
    using System.Diagnostics;
	using System.Security.Permissions;
    using Infragistics.Shared;
    using Infragistics.Win;

	/// <summary>
	///	Abstract base class for grid items (cells, rows, columns,
	///	groups and bands)
	/// </summary>
	

	public abstract class GridItemBase : SubObjectBase,
										Infragistics.Shared.ISelectableItem
	{
		// JJD 3/8/02
		// Change member name from 'selected' to 'selectedValue' since it is not CLSCompliant
		// to have a member and a property whose name differs by case only
		//
		/// <summary>
		/// The internal selectedValue state of the item.
		/// </summary>
		protected bool selectedValue = false;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <returns></returns>
		// AS 1/8/03 - fxcop
		// abstract types should not have public constructors
		//public GridItemBase()
		protected GridItemBase()
		{
		}

		/// <summary>
		/// Returns the Band that the object belongs to, if any. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Band</b> property of an object refers to a specific band in the grid as defined by an Band object. You use the <b>Band</b> property to access the properties of a specified Band object, or to return a reference to the Band object that is associated with the current object.</p>
		/// <p class="body">Band objects are the foundation of the hierarchical data structure used by UltraWinGrid. Any row or cell in the grid must be accessed through its Band object. Bands are also used to apply consistent formatting and behavior to the rows that they comprise. A Band object is used to display all the data rows from a single level of a data hierarchy. Band objects contain multiple sets of child Row objects that actually display the data of the recordset. All of the rows that are drawn from a single Command in the DataEnvironment make up a band.</p>
		/// <p class="body">The rows of a band are generally displayed in groups of one more in order to show rows from subsequent bands that are linked to rows in the current band via the structure of the data hierarchy. For example, if a hierarchical recordset has Commands that display Customer, Order and Order Detail data, each one of these Commands maps to its own Band in the UltraWinGrid. The rows in the Customer band will appear separated by any Order data rows that exist for the customers. By the same token, rows in the Order band will be appear separated to make room for Order Detail rows. How this looks depends on the ViewStyle settings selectedValue for the grid, but the concept of visual separation is readily apparent when the UltraWinGrid is used with any hierarchical recordset.</p>
		/// <p class="body">Although the rows in a band may appear to be separated, they are treated contiguously. When selecting a column in a band, you will see that the cells of that column become selectedValue in all rows for the band, regardless of any intervening rows. Also, it is possible to collapse the hierarchical display so that any children of the rows in the current band are hidden.</p>
		/// </remarks>
		public abstract Infragistics.Win.UltraWinGrid.UltraGridBand Band { get; }

		// SSP 4/16/05
		// Added an overload of GetUIElement that only takes in verifyElements.
		//
		/// <summary>
		/// Returns the UIElement associated with the object, in the active scroll region.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to an object's UIElement. The reference can be used to set properties of, and invoke methods on, the UIElement object associated with an object. You can use this reference to access any of the UIElement's properties or methods.</p>
		/// <p class="body"> The <b>Type</b> property can be used to determine what type of UIElement was returned. If no UIElement exists, meaning the object is not displayed, Nothing is returned.</p>
		/// <p class="body">The <b>ParentUIElement</b> property can be used to return a reference to a UIElement's parent UIElement object. The <b>UIElements</b> property can be used to return a reference to a collection of child UIElement objects for a UIElement.</p>
		/// <p class="body">The <b>UIElementFromPoint</b> method can be invoked to return a reference to a UIElement object residing at specific coordinates.</p>
		/// <p class="body">The <b>CanResolveUIElement</b> method can be invoked to determine whether an object or one of its ancestors can be resolved as a specific type of UIElement.</p>
		/// </remarks>
		/// <returns></returns>
		public UIElement GetUIElement( bool verifyElements )
		{
			UltraGridLayout layout = this.Band.Layout;
			return this.GetUIElement( 
				layout.ActiveRowScrollRegion, layout.ActiveColScrollRegion, verifyElements );
		}

		/// <summary>
		/// Returns the UIElement associated with the object, in the active scroll region.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to an object's UIElement. The reference can be used to set properties of, and invoke methods on, the UIElement object associated with an object. You can use this reference to access any of the UIElement's properties or methods.</p>
		/// <p class="body"> The <b>Type</b> property can be used to determine what type of UIElement was returned. If no UIElement exists, meaning the object is not displayed, Nothing is returned.</p>
		/// <p class="body">The <b>ParentUIElement</b> property can be used to return a reference to a UIElement's parent UIElement object. The <b>UIElements</b> property can be used to return a reference to a collection of child UIElement objects for a UIElement.</p>
		/// <p class="body">The <b>UIElementFromPoint</b> method can be invoked to return a reference to a UIElement object residing at specific coordinates.</p>
		/// <p class="body">The <b>CanResolveUIElement</b> method can be invoked to determine whether an object or one of its ancestors can be resolved as a specific type of UIElement.</p>
		/// </remarks>
		/// <returns></returns>
		public UIElement GetUIElement( )
		{
			return this.GetUIElement( this.Band.Layout.Grid.ActiveRowScrollRegion );
		}

		/// <summary>
		/// Returns the UIElement associated with the object, in the scroll region formed by the intersection of the specified row scrolling region and the active column scrolling region.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to an object's UIElement. The reference can be used to set properties of, and invoke methods on, the UIElement object associated with an object. You can use this reference to access any of the UIElement's properties or methods.</p>
		/// <p class="body"> The <b>Type</b> property can be used to determine what type of UIElement was returned. If no UIElement exists, meaning the object is not displayed, Nothing is returned.</p>
		/// <p class="body">The <b>ParentUIElement</b> property can be used to return a reference to a UIElement's parent UIElement object. The <b>UIElements</b> property can be used to return a reference to a collection of child UIElement objects for a UIElement.</p>
		/// <p class="body">The <b>UIElementFromPoint</b> method can be invoked to return a reference to an UltraGridUIElement object residing at specific coordinates.</p>
		/// <p class="body">The <b>CanResolveUIElement</b> method can be invoked to determine whether an object or one of its ancestors can be resolved as a specific type of UIElement.</p>
		/// </remarks>		
		/// <param name="rsr">The RowScrollRegion</param>
        /// <returns>The UIElement associated with the object</returns>
		public UIElement GetUIElement( RowScrollRegion rsr )
		{
			return this.GetUIElement( rsr, this.Band.Layout.Grid.ActiveColScrollRegion );
		}


		/// <summary>
		/// Returns the UIElement associated with the object, in the scroll region formed by the intersection of the specified column scrolling region and the active row scrolling region.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to an object's UIElement. The reference can be used to set properties of, and invoke methods on, the UIElement object associated with an object. You can use this reference to access any of the UIElement's properties or methods.</p>
		/// <p class="body"> The <b>Type</b> property can be used to determine what type of UIElement was returned. If no UIElement exists, meaning the object is not displayed, Nothing is returned.</p>
		/// <p class="body">The <b>ParentUIElement</b> property can be used to return a reference to a UIElement's parent UIElement object. The <b>UIElements</b> property can be used to return a reference to a collection of child UIElement objects for a UIElement.</p>
		/// <p class="body">The <b>UIElementFromPoint</b> method can be invoked to return a reference to an UltraGridUIElement object residing at specific coordinates.</p>
		/// <p class="body">The <b>CanResolveUIElement</b> method can be invoked to determine whether an object or one of its ancestors can be resolved as a specific type of UIElement.</p>
		/// </remarks>
		/// <param name="csr">The ColScrollRegion</param>
        /// <returns>The UIElement associated with the object</returns>
		public UIElement GetUIElement( ColScrollRegion csr )
		{
			return this.GetUIElement( this.Band.Layout.Grid.ActiveRowScrollRegion, csr );
		}
		
		/// <summary>
		/// Returns the UIElement associated with the object, in the scroll region formed by the specified row and column scrolling regions.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to an object's UIElement. The reference can be used to set properties of, and invoke methods on, the UIElement object associated with an object. You can use this reference to access any of the UIElement's properties or methods.</p>
		/// <p class="body"> The <b>Type</b> property can be used to determine what type of UIElement was returned. If no UIElement exists, meaning the object is not displayed, Nothing is returned.</p>
		/// <p class="body">The <b>ParentUIElement</b> property can be used to return a reference to a UIElement's parent UIElement object. The <b>UIElements</b> property can be used to return a reference to a collection of child UIElement objects for a UIElement.</p>
		/// <p class="body">The <b>UIElementFromPoint</b> method can be invoked to return a reference to an UltraGridUIElement object residing at specific coordinates.</p>
		/// <p class="body">The <b>CanResolveUIElement</b> method can be invoked to determine whether an object or one of its ancestors can be resolved as a specific type of UIElement.</p>
		/// </remarks>
		/// <param name="rsr">The RowScrollRegion</param>
		/// <param name="csr">The ColScrolRegion</param>
        /// <returns>The UIElement associated with the object</returns>
		public UIElement GetUIElement( RowScrollRegion rsr, ColScrollRegion csr )
		{
			return this.GetUIElement( rsr, csr, true );
		}
	

		/// <summary>
		/// An abstract method that should be overridden by derived classes
		/// to return an associated UI element contained in a RowScrollRegion and a
		/// ColScrollRegion 
		/// </summary>
		/// <param name="rsr">Row scroll region</param>
		/// <param name="csr">Col scroll region</param>
		/// <param name="verifyElements">True to the elements to be verified</param>
		/// <returns>The associated element or null</returns>
		public abstract UIElement GetUIElement( RowScrollRegion rsr, ColScrollRegion csr, bool verifyElements );

		// True if the pivot is saved by the row scroll region
		internal abstract bool PivotOnRowScrollRegion { get; }

		// True if the pivot is saved by the col scroll region
		internal abstract bool PivotOnColScrollRegion { get; }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Invalidates the item in a specific row an col region intersection.
		//        /// If the row or col scoll regions are not specified then the 
		//        /// active regions will be used.
		//        /// </summary>
		//        /// <param name="rsr"></param>
		//        /// <param name="csr"></param>
		//#endif
		//        internal void InvalidateItem(RowScrollRegion rsr, ColScrollRegion csr)
		//        {
		//            this.InvalidateItem( rsr, csr, false );
		//        }

		#endregion Not Used

		internal void InvalidateItem(RowScrollRegion rsr)
		{
            // MBS 4/2/09 
            // The comments to this method indicate that it should be invalidating the
            // item across all ColScrollRegions, yet the only the active one is used.
            // This causes an issue, such as the case where you have a selected cell that is
            // shown in multiple regions, but clear that selection, since now some regions
            // will incorrectly show the cells as still selected.
            //
            //this.InvalidateItem( rsr,
            //                     this.Band.Layout.Grid.ActiveColScrollRegion,
            //                     false );
            foreach (ColScrollRegion colScrollRegion in this.Band.Layout.ColScrollRegions)
            {
                this.InvalidateItem(rsr, colScrollRegion, false);
            }
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Invalidates the item in a specific col region across all row
		//        /// regions.
		//        /// </summary>
		//        /// <param name="csr"></param>
		//#endif
		//        internal void InvalidateItem(ColScrollRegion csr)
		//        {
		//            this.InvalidateItem( this.Band.Layout.Grid.ActiveRowScrollRegion,
		//                                 csr,
		//                                 false );
		//        }

		#endregion Not Used

		internal void InvalidateItem()
		{
			// MD 8/14/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//this.InvalidateItem( this.Band.Layout.Grid.ActiveRowScrollRegion,
			//                     this.Band.Layout.Grid.ActiveColScrollRegion,
			//                     false );
			UltraGridBase grid = this.Band.Layout.Grid;

			this.InvalidateItem( grid.ActiveRowScrollRegion,
								 grid.ActiveColScrollRegion,
								 false );
		}

		// SSP 5/19/05
		// 
		internal void InvalidateItem( RowScrollRegion rsr, bool recalcRects )
		{
			this.InvalidateItem( rsr, this.Band.Layout.ActiveColScrollRegion, recalcRects );
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//// SSP 5/19/05
		////
		//internal void InvalidateItem( ColScrollRegion csr, bool recalcRects )
		//{
		//    this.InvalidateItem( this.Band.Layout.ActiveRowScrollRegion, csr, recalcRects );
		//}

		#endregion Not Used
		
		internal void InvalidateItem( bool recalcRects )
		{
			this.InvalidateItem( this.Band.Layout.Grid.ActiveRowScrollRegion,
				this.Band.Layout.Grid.ActiveColScrollRegion,
				recalcRects );
		}

		internal void InvalidateItem( RowScrollRegion rsr,
									  ColScrollRegion csr,
									  bool recalcRects )
		{
			// SSP 5/23/02
			// If we are currently updating, then don't invalidate the element
			//
			// MD 8/14/07 - 7.3 Performance
			// Band is abstract, cache it so we don't make multiple virtual calls
			//if ( null != this.Band && null != this.Band.Layout && null != this.Band.Layout.Grid 
			//    && this.Band.Layout.Grid.IsUpdating )
			//    return;
			UltraGridBand band = this.Band;

			if ( null != band && null != band.Layout && null != band.Layout.Grid
				&& band.Layout.Grid.IsUpdating )
				return;

			// SSP 10/25/04 UWG3623
			// Revert back the optimization because in the case of the cells we don't want 
			// to return without dirtying the cell element because of that optimization we
			// have in place that causes us to not reposition the cells if their sizes haven't
			// changed. (The one having to do with BumpCellChildElementsVersion method off the
			// layout).
			//
			

			// SSP 12/10/03 UWG2787
			// We shouldn't really throw an exception here. Just return if the rsr or csr is null.
			// Commented out below code.
			//
			

			// get the grid's main element
			// We don't want to cause reverification of ui elements. For example
			// if this method is called when the grid element is already dirtied,
			// then we will cause the the ui elemnets to be verified prematurely.
			// 
			//UIElement element = this.GetUIElement( rsr,csr );
			UIElement element = this.GetUIElement( rsr, csr, false );
			
			if ( null != element )
			{
				// SSP 7/12/05 BR04897
				// Moved the code into the new InvalidateElem method.
				// 
				// --------------------------------------------------------------------
				this.InvalidateElem( element, recalcRects );
				
				// --------------------------------------------------------------------
			}
	
		}

		// SSP 7/12/05 BR04897
		// 
		internal virtual void InvalidateElem( UIElement element, bool recalcRects )
		{
			if ( recalcRects )
				// SSP 11/14/01 UWG600
				// Setting the ChildElementsDirty does not invalidate the element.
				// And just like the name suggests, we need to invalidate the element.
				// So calling DirtyChildElements instead which will set the
				// ChildElementsDirty to true and invalidate.
				//
				//element.ChildElementsDirty = true;
				element.DirtyChildElements( );
			else
				element.Invalidate();				
		}

		internal void InvalidateItemAllRegions()
        {
            this.InvalidateItemAllRegions( false );
        }
		internal void InvalidateItemAllRegions( bool recalcRects )
		{
			// SSP 5/23/02
			// If we are currently updating, then don't invalidate the element
			//
			// MD 8/14/07 - 7.3 Performance
			// Band is abstract, cache it so we don't make multiple virtual calls
			//if ( null != this.Band && null != this.Band.Layout && null != this.Band.Layout.Grid 
			//    && this.Band.Layout.Grid.IsUpdating )
			//    return;
			UltraGridBand band = this.Band;

			if ( null != band && null != band.Layout && null != band.Layout.Grid
				&& band.Layout.Grid.IsUpdating )
				return;

			// SSP 10/25/04 UWG3623
			// Revert back the optimization because in the case of the cells we don't want 
			// to return without dirtying the cell element because of that optimization we
			// have in place that causes us to not reposition the cells if their sizes haven't
			// changed. (The one having to do with BumpCellChildElementsVersion method off the
			// layout).
			//
			

			// MD 8/14/07 - 7.3 Performance
			// Use the cached Band
			//RowScrollRegionsCollection RSRs = this.Band.Layout.RowScrollRegions;
			//ColScrollRegionsCollection CSRs = this.Band.Layout.ColScrollRegions;
			RowScrollRegionsCollection RSRs = band.Layout.RowScrollRegions;
			ColScrollRegionsCollection CSRs = band.Layout.ColScrollRegions;
			
			// Using 2 nested loops invalidate the item in each intersection
			//
			// MD 8/14/07 - 7.3 Performance
			// The Count getter is virtual, get it once
			//for ( int i=0; i < RSRs.Count; i++)
			for ( int i = 0, RSRCount = RSRs.Count; i < RSRCount; i++ )
			{
				// MD 8/14/07 - 7.3 Performance
				// The indexer makes a virtual call, only call it once
				//if ( RSRs[i] == null ||
				//    RSRs[i].Hidden	 
				RowScrollRegion rowScrollRegion = RSRs[ i ];

				if ( rowScrollRegion == null ||
					rowScrollRegion.Hidden	 
					// SSP 8/13/04 UWG3623
					// Commented out below condition because in the case of the UltraCombo the
					// extent can be 0 or less before the combo is dropped down because the
					// ControlForDisplay is Size(0, 0). However the data area elem could have 
					// been positioned previously with the correct size and if we are looking for
					// a cell element here, that cell elem may have the same rect in the next verification.
					// If it does then due to a certain optimization RowCellAreaUIElement eploys
					// cell element will never be positioned (cell element's PositionChildElements
					// will never be called) causing it not to update its image element to any
					// new image that might have been set. Look at the bug for more info if any
					// of this doesn't make sense.
					// 
					// || RSRs[i].Extent < 1 
					)
					continue;

				// MD 8/14/07 - 7.3 Performance
				// The Count getter is virtual, get it once
				//for ( int j=0; j < CSRs.Count; j++)
				for ( int j = 0, CSRCount = CSRs.Count; j < CSRCount; j++ )
				{
					// MD 8/14/07 - 7.3 Performance
					// The indexer makes a virtual call, only call it once
					//if( CSRs[j] == null ||
					//    CSRs[j].Hidden	
					ColScrollRegion colScrollRegion = CSRs[ j ];

					if ( colScrollRegion == null ||
						colScrollRegion.Hidden	
						// SSP 8/13/04 UWG3623
						// Commented out below condition because in the case of the UltraCombo the
						// extent can be 0 or less before the combo is dropped down because the
						// ControlForDisplay is Size(0, 0). However the data area elem could have 
						// been positioned previously with the correct size and if we are looking for
						// a cell element here, that cell elem may have the same rect in the next verification.
						// If it does then due to a certain optimization RowCellAreaUIElement eploys
						// cell element will never be positioned (cell element's PositionChildElements
						// will never be called) causing it not to update its image element to any
						// new image that might have been set. Look at the bug for more info if any
						// of this doesn't make sense.
						// 
						// || CSRs[j].Extent < 1 
						)
                        continue;
	
					// MD 8/14/07 - 7.3 Performance
					// The indexer makes a virtual call, use the cached values
					//this.InvalidateItem( RSRs[i],
					//                     CSRs[j],
					//                     recalcRects );
					this.InvalidateItem( rowScrollRegion, 
										colScrollRegion, 
										recalcRects );
					
				}
			}
		}
		internal virtual bool IsDisabled
		{
			get
			{
				return ( this.ActivationResolved == Activation.Disabled );
			}
		}
		internal virtual bool SetFocusAndActivate()
		{
			return this.SetFocusAndActivate ( true, false, false );
		}
		internal virtual bool SetFocusAndActivate(bool byMouse)
		{
			return this.SetFocusAndActivate ( byMouse, false, false );
		}

		internal virtual bool SetFocusAndActivate( bool byMouse, bool enterEditMode )
		{
			return this.SetFocusAndActivate ( byMouse, enterEditMode, false );
		}
		internal virtual bool SetFocusAndActivate( bool byMouse, bool enterEditMode, bool byTabKey )
		{
			return this.IsDisabled; //|| 
            //RobA UWG586 11/7/01
			//RobA UWG586 11/9/01 we decided to not check this here
			//( this.ActivationResolved == Activation.Disabled );
		}

		/// <summary>
		/// Property: Returns true only if all columns are marked as selectedValue
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public abstract bool Selected { get; set; }
		/// <summary>
		/// Property: Returns true only if activated
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public virtual bool Activated{ get { return false; } set{} }


		// AS - 10/24/01
		// Checked with Vlad about the reason behind the IsSelectable
		// property of the ISelectableItem. Essentially, it was implemented
		// so that empty groups could be dragged even though they could not
		// be selectedValue. Everything else should be returning true.

		/// <summary>
		/// Property: Returns true only if selectable
		/// </summary>
		// SSP 4/20/05 - NAS 5.2
		// Added internal modifier.
		//
		internal protected virtual bool Selectable{ get { return true; } }

		/// <summary>
		/// Property: Returns true only if Draggable
		/// </summary>
		internal protected virtual bool Draggable{ get { return false; }}

		/// <summary>
		/// Property: Returns true only if tab stop
		/// </summary>
		public virtual bool IsTabStop{ get { return true; } }

		
		internal virtual void InternalSelect( bool value )
		{
			if( this.selectedValue != value )
			{
				this.selectedValue = value;
				this.NotifyPropChange( PropertyIds.Selected, null );
			}
		}

		// SSP 7/11/06 BR14276
		// 
		internal void InternalSelect( bool value, bool raiseNotification )
		{
			if ( raiseNotification )
				this.InternalSelect( value );
			else
				this.selectedValue = value;
		}

		// This needed to be an internal method so that it didn't conflict 
		// with the Cell's public Row property
		//
		internal virtual Infragistics.Win.UltraWinGrid.UltraGridRow GetRow() 
		{ 
			return null;
		}

		// This needed to be an internal method so that it didn't conflict 
		// with the Cell's public Column property
		//
		internal virtual Infragistics.Win.UltraWinGrid.UltraGridColumn GetColumn()
		{
			return null; 
		}

		internal abstract Activation ActivationResolved{ get; }
		internal virtual void AddToCollection( Selected selectedValue ) {}
		internal virtual void RemoveFromCollection( Selected selectedValue ) {}
		internal virtual void SetInitialSelection( Selected currentSelection, 
												   Selected initialSelection ) {}
		internal abstract ISelectionStrategy SelectionStrategyDefault { get; }

		internal abstract void CalcSelectionRange( Selected newSelection, 
												   bool clearExistingSelection,
												   bool select,
												   Selected initialSelection );		


		// each derived guy will simply return his type (ssSelectChangeRow, 
		// ssSelectChangeCell or ssSelectChangeCol
		internal abstract SelectChange SelectChangeType { get; }

		
		/// <summary>
		/// Property for determining if the selectable item is currently selectedValue.
		/// </summary>
		bool ISelectableItem.IsSelected
		{
			get { return this.Selected; }
		}

		/// <summary>
		/// Property for determining if the object is selectable.
		/// </summary>
		bool ISelectableItem.IsSelectable
		{
			get { return this.Selectable; }
		}

		/// <summary>
		/// Property for determining if the object is draggable.
		/// </summary>
		bool ISelectableItem.IsDraggable
		{
			// JJD 1/22/02 - UWG876
			// Always return true so the user gets a chance when the SelectionDrag event
			// in fired
			//
			get { return true; } //this.Draggable; }
		}

	}
}
