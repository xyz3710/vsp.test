#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Collections;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    /// Enumerates the possible actions that can be performed on the grid
    /// </summary>
    public enum UltraGridAction 
    {
		/// <summary>
		/// Activates the next visible row in the grid after the ActiveRow. This action will span bands, so if the ActiveRow is expanded and has visible child rows, the first child row will be activated. If a cell is currently in edit mode, this action will attempt to exit edit mode before changing the active row. To prevent spanning across bands, use the BelowRow action, instead.
		/// </summary>
		NextRow          = 1,

		/// <summary>
		/// Activates the previous visible row in the grid before the ActiveRow. This action will span bands, so if the ActiveRow is the first row in an island, it's parent row (or the last row in a sibling island) will be activated. If a cell is currently in edit mode, this action will attempt to exit edit mode before changing the active row. To prevent spanning across bands, use the AboveRow action, instead.
		/// </summary>
        PrevRow          = 2,

		/// <summary>
		/// Activates the next visible cell following the ActiveCell. If the ActiveCell is in edit mode, this action will attempt to exit edit mode. If exiting edit mode fails, the action will fail. This action will span rows and bands, so it may activate a cell in the next row or a row in a child band. This action will not place the new ActiveCell into edit mode. To move to the next cell and enter edit mode, use the NextCellByTab action, instead. 
		/// </summary>
        NextCell		 = 3,

		/// <summary>
		/// Activates the previous visible cell relative to the ActiveCell. If the ActiveCell is in edit mode, this action will attempt to exit edit mode. If exiting edit mode fails, the action will fail. This action will span rows and bands, so it may activate a cell in the previous row or a row in a sibling or parent band. This action will not place the new ActiveCell into edit mode. To move to the previous cell and enter edit mode, use the NextCellByTab action, instead. 
		/// </summary>
        PrevCell         = 4,

		/// <summary>
		/// Activates the next visible cell following the ActiveCell. If the ActiveCell is in edit mode, this action will attempt to exit edit mode. If exiting edit mode fails, the action will fail. This action will span rows, but not bands, so it may activate a cell in the next row, but will never leave the current island of data.
		/// </summary>
        NextCellInBand   = 5,

		/// <summary>
		/// Activates the previous visible cell relative to the ActiveCell. If the ActiveCell is in edit mode, this action will attempt to exit edit mode. If exiting edit mode fails, the action will fail. This action will span rows, but not bands, so it may activate a cell in the previous row, but will never leave the current island of data.
		/// </summary>
        PrevCellInBand   = 6,

		/// <summary>
		/// Activates the first visible cell in the ActiveRow.
		/// </summary>
        FirstCellInRow   = 7,
		/// <summary>
		/// Activates the last visible cell in the ActiveRow.
		/// </summary>
        LastCellInRow    = 8,

		/// <summary>
		/// Activates a visible cell that is one page down from the ActiveCell. This action will fail if there is no ActiveCell in the grid. This action will span islands of data, but will not span bands. This means the new ActiveCell will always be a cell in the same band as the prior ActiveCell, but may be under a different parent row.
		/// </summary>
        PageDownCell     = 9,

		/// <summary>
		/// Activates a visible cell that is one page up from the ActiveCell. This action will fail if there is no ActiveCell in the grid. This action will span islands of data, but will not span bands. This means the new ActiveCell will always be a cell in the same band as the prior ActiveCell, but may be under a different parent row.
		/// </summary>
        PageUpCell       = 10,

		/// <summary>
		/// Activates a visible row that is one page down from the ActiveRow. This action will span islands of data, but will not span bands. This means the new ActiveRow will always be a row in the same band as the prior ActiveRow, but may be under a different parent row.
		/// </summary>
        PageDownRow      = 11,

		/// <summary>
		/// Activates a visible row that is one page up from the ActiveRow. This action will span islands of data, but will not span bands. This means the new ActiveRow will always be a row in the same band as the prior ActiveRow, but may be under a different parent row.
		/// </summary>
        PageUpRow        = 12,

		/// <summary>
		/// Toggles edit mode on the current cell. This action will cause the ActiveCell to enter or exit edit mode. The action will fail if there is no active cell. If a cell enters edit mode while the grid does not have focus, the grid will attempt to take focus before entering edit mode. If it fails to take focus, the action will fail. 
		/// </summary>
        ToggleEditMode   = 13,

		/// <summary>
		/// Toggles the drop down in the current ActiveCell. This method will fail if the ActiveCell has no dropdown, the grid does not have focus, or the ActiveCell is not in edit mode.
		/// </summary>
        ToggleDropdown   = 14,

		/// <summary>
		/// Sets focus to the ActiveCell in the next ColScrollRegion or RowScrollRegion. This action will fail if there is no ActiveCell, if the ActiveCell is in edit mode or if there is no ColScrollRegion or RowScrollRegion to put focus on.
		/// </summary>
        NextRegion       = 15,

		/// <summary>
		/// Sets focus to the ActiveCell in the previous ColScrollRegion or RowScrollRegion. This action will fail if there is no ActiveCell, if the ActiveCell is in edit mode or if there is no ColScrollRegion or RowScrollRegion to put focus on.
		/// </summary>
        PrevRegion       = 16,

		/// <summary>
		/// Activates the previous visible row in the grid above the ActiveRow. This action will span islands of data, but not bands, so the new ActiveRow will always be in the same band as the prior ActiveRow, but it may be under a different parent row. If a cell is currently in edit mode, this action will attempt to exit edit mode before changing the active row. To span across bands, use the PrevRow action.
		/// </summary>
        AboveRow         = 17,

		/// <summary>
		/// Activates the next visible row in the grid below the ActiveRow. This action will span islands of data, but not bands, so the new ActiveRow will always be in the same band as the prior ActiveRow, but it may be under a different parent row. If a cell is currently in edit mode, this action will attempt to exit edit mode before changing the active row. To span across bands, use the NextRow action, instead.
		/// </summary>
        BelowRow         = 18,

		/// <summary>
		/// Activates the previous visible cell in the grid above the ActiveCell. This action will span islands of data, but not bands, so the new ActiveCell will always be in the same band as the prior ActiveCell, but it may be in a row under a different parent row. This action will fail if there is no ActiveCell. If the ActiveCell is currently in edit mode, this action will attempt to exit edit mode before changing the active cell.
		/// </summary>
        AboveCell        = 19,

		/// <summary>
		/// Activates the next visible cell in the grid below the ActiveCell. This action will span islands of data, but not bands, so the new ActiveCell will always be in the same band as the prior ActiveCell, but it may be in a row under a different parent row. This action will fail if there is no ActiveCell. If the ActiveCell is currently in edit mode, this action will attempt to exit edit mode before changing the active cell.
		/// </summary>
        BelowCell        = 20,

		/// <summary>
		/// Cancels any pending edits in the ActiveCell. This action will revert the ActiveCell to the value from the underlying DataSource and cancel any pending changes made by the user. The action will fail if there is no ActiveCell, of the ActiveCell is not in edit mode. This action will also cause the ActiveCell to exit edit mode. 
		/// </summary>
        UndoCell         = 21,

		/// <summary>
		/// Cancels any pendits edits in the ActiveRow. This action will revert all cells in the ActiveRow to the value from the underlying DataSource, discarding any pending changes made by the user. The action will fail if there is no ActiveRow or if a cell is in edit mode. 
		/// </summary>
        UndoRow          = 22,

		/// <summary>
		/// Closes the dropdown in the ActiveCell. This action will fail if there is no ActiveCell, the ActiveCell has no dropdown, or the dropdown is not currently dropped down.
		/// </summary>
        CloseDropdown    = 23,

		/// <summary>
		/// Causes the ActiveCell to enter edit mode. This action will fail if there is no ActiveCell. If the grid does not have focus, it will attempt to take focus. If it fails to take focus, the action will fail.
		/// </summary>
        EnterEditMode    = 24,

		/// <summary>
		/// Causes the ActiveCell to enter edit mode and drop down it's DropDown. This action will fail if there is no ActiveCell or if the ActiveCell has no dropdown. If the grid does not have focus, it will attempt to take focus. If it fails to take focus, the action will fail.
		/// </summary>
        EnterEditModeAndDropdown = 25,

		/// <summary>
		/// Activates the first cell in the current island of data (the first visible cell in the first visible row of the island). This action will fail if there is no Activecell or the ActiveCell is not in the first visible column.
		/// </summary>
        FirstCellInBand  = 26,

		/// <summary>
		/// Activates the last cell in the current island of data (the last visible cell in the last visible row of the island). This action will fail if there is no Activecell or the ActiveCell is not in the last visible column.
		/// </summary>
        LastCellInBand   = 27,

		/// <summary>
		/// Activates the first visible cell in the grid (this first visible cell in the first visible row of the root band).
		/// </summary>
        FirstCellInGrid  = 28,

		/// <summary>
		/// Activates the last visible cell in the grid (this last visible cell in the last visible row of the grid).
		/// </summary>
        LastCellInGrid   = 29,

		/// <summary>
		/// Expands the active row. This action will fail if the row is already expanded. 
		/// </summary>
        ExpandRow		 = 30,

		/// <summary>
		/// Activates the first visible row in the grid. This action will fail if there is no ActiveRow in the grid. 
		/// </summary>
        FirstRowInGrid   = 31,

		/// <summary>
		/// Activates the last visible row in the grid. This action will fail if there is no ActiveRow in the grid. 
		/// </summary>
        LastRowInGrid    = 32,

		/// <summary>
		/// Activates the first visible row in the current island of data. This action will fail if there is no ActiveRow.
		/// </summary>
        FirstRowInBand	 = 33,

		/// <summary>
		/// Activates the last visible row in the current island of data. This action will fail if there is no ActiveRow.
		/// </summary>
        LastRowInBand	 = 34,

		/// <summary>
		/// Toggles the Selected state of the ActiveCell. This action will fail if there is no ActiveCell or if the ActiveCell is in edit mode. 
		/// </summary>
        ToggleCellSel	 = 35,

		/// <summary>
		/// Toggles the Selected state of the ActiveRow. This action will fail if there is no ActiveRow or if there is an ActiveCell. 
		/// </summary>
        ToggleRowSel	 = 36,

		/// <summary>
		/// Deletes the currently selected rows. Like all UltraGridActions, this action simulates user input, which means it will display a confirmation dialog to the user by default. To cancel the confirmation dialog, set e.DisplayPromptMsg to false in the BeforeRowsDeleted event. Or use the <see cref="UltraGrid.DeleteSelectedRows(bool)"/> method, instead of an action.
		/// </summary>
        DeleteRows		 = 37,

		/// <summary>
		/// Deactivates the ActiveCell. This action will fail if there is no ActiveCell. 
		/// </summary>
        DeactivateCell	 = 38,

		/// <summary>
		/// Activates the first visible cell in the ActiveRow. This action will fail if there is no active row, or if there is already an ActiveCell.
		/// </summary>
        ActivateCell	 = 39,

		/// <summary>
		/// Collapses the active row. This action will fail if the row is already collapsed. 
		/// </summary>
        CollapseRow		 = 40,

		/// <summary>
		/// Toggles the state of the CheckBox or TriStateCheckBox in the ActiveCell. This action will fail if there is no ActiveCell or if the ActiveCell is not a CheckBox or TriStateCheckBox. 
		/// </summary>
        ToggleCheckbox	 = 41,

		/// <summary>
		/// Activates the next visible cell following the ActiveCell and puts it into edit mode. If the ActiveCell is in edit mode, this action will attempt to exit edit mode. If exiting edit mode fails, the action will fail. This action will span rows and bands, so it may activate a cell in the next row or a row in a child band. To move to the next cell without entering edit mode, use the NextCell action, instead.
		/// </summary>
        NextCellByTab    = 42,

		/// <summary>
		/// Activates the previous visible cell relative to the ActiveCell and puts it into edit mode. If the ActiveCell is in edit mode, this action will attempt to exit edit mode. If exiting edit mode fails, the action will fail. This action will span rows and bands, so it may activate a cell in the previous row or a row in a sibling or parent band. This action will not place the new ActiveCell into edit mode. To move to the previous cell without entering edit mode, use the NextCell action, instead.
		/// </summary>
        PrevCellByTab    = 43,

		/// <summary>
		/// Exist edit mode on the ActiveCell. This action will fail if there is no ActiveCell of if the ActiveCell is not in edit mode. 
		/// </summary>
        ExitEditMode     = 44, 

		/// <summary>
		/// Activates the next visible row in the grid after the ActiveRow. This action will span bands, so if the ActiveRow is expanded and has visible child rows, the first child row will be activated. If a cell is currently in edit mode, this action will attempt to exit edit mode before changing the active row. To prevent spanning across bands, use the BelowRow action, instead. NextRowByTab differs from NextRow in that it will not select rows, even if the Control and Shift keys are specified true. 
		/// </summary>
		NextRowByTab     = 45,

		/// <summary>
		/// Activates the next visible row in the grid after the ActiveRow. This action will span bands, so if the ActiveRow is expanded and has visible child rows, the first child row will be activated. If a cell is currently in edit mode, this action will attempt to exit edit mode before changing the active row. To prevent spanning across bands, use the BelowRow action, instead. PrevRowByTab differs from PrevRow in that it will not select rows, even if the Control and Shift keys are specified true.
		/// </summary>
        PrevRowByTab     = 46,

		// SSP 4/23/05 - NAS 5.2 Filter Row/Fixed Add Row
		// Added CommitRow action. This is for applying filters when Enter key is 
		// pressed while the filter row is active depending on the 
		// FilterEvaluationTrigger settings. Also it's used for committing the
		// add-row. It can be used to commit regular rows as well however for that
		// the developer has to add a key action mapping.
		//
		/// <summary>
		/// When the active row is a filter row, this action evaluates the filter values 
		/// input into a filter row. When the active row is an add-row, commits the 
		/// add-row if it has been modified by the user. Otherwise it commits the active 
		/// row.
		/// </summary>
		CommitRow		= 47,

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
		// 
		// --------------------------------------------------------------------
		/// <summary>
		/// Copies the selected cells to the clipboard. This action will fail if the <see cref="UltraGridOverride.AllowMultiCellOperations"/> property does not allow Copying.
		/// </summary>
		Copy			= 48,

		/// <summary>
		/// Copies the selected cells to the clipboard and deletes the contents of the cells. This action will fail if the <see cref="UltraGridOverride.AllowMultiCellOperations"/> property does not allow Cutting. 
		/// </summary>
		Cut				= 49,

		/// <summary>
		/// Deletes the contents of the selected cells. This action will fail if the <see cref="UltraGridOverride.AllowMultiCellOperations"/> property does not allow Deleting. 
		/// </summary>
		DeleteCells		= 50,

		/// <summary>
		/// Pastes cells from the clipboard to the selected cells in the grid. This action will fail if the <see cref="UltraGridOverride.AllowMultiCellOperations"/> property does not allow Pasting. 
		/// </summary>
		Paste			= 51,

		/// <summary>
		/// Action for undoing the last multi-cell cut or paste operation.
		/// </summary>
		Undo			= 52,

		/// <summary>
		/// Action for redoing the last undo operation.
		/// </summary>
		Redo			= 53
		// --------------------------------------------------------------------
    }

	

    /// <summary>
    /// Bit flags that describe the state of the control. For example, if
    /// the first cell in the second row is active but not in edit mode bits
    /// Row, Cell, and FirstCell will be set. 
    /// </summary>
    [Flags] public enum UltraGridState
    {
		/// <summary>
		/// Active Cell not null
		/// </summary>
        Cell              = 0x00000001,
		/// <summary>
		/// Active Cell is first cell
		/// </summary>
        CellFirst         = 0x00000002,
		/// <summary>
		/// Active Cell is last cell
		/// </summary>
        CellLast          = 0x00000004,
		/// <summary>
		/// Active Cell can be dropped down
		/// </summary>
        HasDropdown       = 0x00000008,
		/// <summary>
		/// Active Cell is in edit state
		/// </summary>
        InEdit            = 0x00000010,
		/// <summary>
		/// Active Cell has a check box
		/// </summary>
        IsCheckbox	      = 0x00000020,
		/// <summary>
		/// Active Cell is dropped down state
		/// </summary>
        IsDroppedDown     = 0x00000040,
		/// <summary>
		/// Active Row is not null
		/// </summary>
        Row               = 0x00000080,
		/// <summary>
		/// Active Row is a child row
		/// </summary>
        RowChild          = 0x00000100,
		/// <summary>
		/// Active Row is expandable
		/// </summary>
        RowExpandable     = 0x00000200,
		/// <summary>
		/// Active Row is expanded
		/// </summary>
        RowExpanded       = 0x00000400,
		/// <summary>
		/// Active Row is the first Row
		/// </summary>
        RowFirst          = 0x00000800,
		/// <summary>
		/// Active Row is the first child (in its parent row)
		/// </summary>
        RowFirstChild     = 0x00001000,
		/// <summary>
		/// Active Row is the last Row
		/// </summary>
        RowLast           = 0x00002000,
		/// <summary>
		/// Active Row is the last child (in its parent row)
		/// </summary>
        RowLastChild	  = 0x00004000,

		/// <summary>
		/// A GroupByRow is the active row.
		/// </summary>
		GroupByRow		  = 0x00008000,

		// SSP 6/26/02
		// Added a new RowDirty and SwapDroppedDown states
		//
		/// <summary>
		/// If the current row is dirty.
		/// </summary>
		RowDirty		  = 0x00010000,

		// SSP 6/26/02
		// We need this now because of the change in the way we were doing
		// keyboard handling.
		//
		/// <summary>
		/// If the swap drop down is dropped down.
		/// </summary>
		SwapDroppedDown	  = 0x00020000,

		/// <summary>
		/// If the filter drop down is dropped down.
		/// </summary>
		FilterDroppedDown = 0x00040000,

		// SSP 9/18/02 UWG1120
		// Added FirstRowInGrid and LastRowInGrid states. Unlike RowLast and RowFirst,
		// these are the absolute first row and last row in the grid.
		//
		/// <summary>
		/// Active row is the fist row in the grid. This differs from RowFirst
		/// in that RowFirst only takes into account sibling bands while 
		/// FirstRowInGrid cosiders the whole grid (all the bands).
		/// </summary>
		FirstRowInGrid	  = 0x00080000,
		
		/// <summary>
		/// Active row is the last row in the grid. This differs from Rowlast
		/// in that RowLast only takes into account sibling bands while 
		/// LastRowInGrid cosiders the whole grid (all the bands).
		/// </summary>
		LastRowInGrid	  = 0x00100000,

		// SSP 4/23/05 - NAS 5.2 Filter Row
		// Added FilterRow state. This is for applying filters when Enter key is 
		// pressed while the filter row is active depending on the 
		// FilterEvaluationTrigger settings.
		//
		/// <summary>
		/// Active row is a filter row.
		/// </summary>
		FilterRow		  = 0x00200000,

		// SSP 4/23/05 - NAS 5.2 Fixed Add Row
		// Added AddRow state.
		//
		/// <summary>
		/// Active row is an add-row.
		/// </summary>
		AddRow			  = 0x00400000,

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
		// 
		// --------------------------------------------------------------------
		/// <summary>
		/// A state that indicates whether multi-cell copy operation can be performed.
		/// </summary>
		CanCopy			  = 0x00800000,

		/// <summary>
		/// A state that indicates whether multi-cell cut operation can be performed.
		/// </summary>
		CanCut			  = 0x01000000,

		/// <summary>
		/// A state that indicates whether multi-cell delete (deleting the contents of the cells) 
		/// operation can be performed.
		/// </summary>
		CanDeleteCells	  = 0x02000000,

		/// <summary>
		/// A state that indicates whether multi-cell paste operation can be performed.
		/// </summary>
		CanPaste		  = 0x04000000,

		/// <summary>
		/// A state that indicates whether multi-cell undo operation can be performed.
		/// </summary>
		CanUndo			  = 0x08000000,

		/// <summary>
		/// A state that indicates whether multi-cell redo operation can be performed.
		/// </summary>
		CanRedo			  = 0x10000000,
		// --------------------------------------------------------------------

		// SSP 2/17/06 BR09132
		// If the cell is in edit mode and it's a multi-line cell then don't process
		// the CommitRow action since the Enter key in such scenario should cause a carriage
		// return to be entered into the cell.
		// 
		/// <summary>
		/// A state that indicates whether the current cell in edit mode is a multi-line cell.
		/// </summary>
		CellMultiline	  = 0x20000000
    }

    /// <summary>
    /// Key/Action mapping object for UltraGrid.
    /// </summary>
    public class GridKeyActionMapping : KeyActionMappingBase
    {
		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="keyCode">Indicates the key being mapped.</param>
        /// <param name="actionCode">The action to perform.</param>
        /// <param name="stateDisallowed">The disallowed states. These are bit flags that specify the state that the control MUST NOT be in for this mapping to be active. If the current state of the control has any of these bits turned on this mapping will be ignored. </param>
        /// <param name="stateRequired">The required states. These are bit flags that specify the state that the control MUST be in for this mapping to be active.</param>
        /// <param name="specialKeysDisallowed">The special keys that are NOT allowed. If shift, ctrl or alt are pressed and their corresponding bit is turned on in this property the mapping will be ignored.</param>
        /// <param name="specialKeysRequired">The special keys that are required. These keys (any combination of shift/control/alt) MUST be pressed. Otherwise, this mapping will be ignored.</param>
        public GridKeyActionMapping( Keys keyCode,
                                          UltraGridAction actionCode,
                                          UltraGridState stateDisallowed,
                                          UltraGridState stateRequired,
                                          SpecialKeys specialKeysDisallowed,
                                          SpecialKeys specialKeysRequired )
            : base( keyCode,
                    actionCode,
                    (Int64)stateDisallowed,
                    (Int64)stateRequired,
                    specialKeysDisallowed,
                    specialKeysRequired )
        {
        }

		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// Delete key action for DeleteCells requires that we bypass the DeleteRows action.
		// Added an overload of constructor that takes in byPassTrailingActions.
		// 
		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="keyCode">Indicates the key being mapped.</param>
        /// <param name="actionCode">The action to perform.</param>
        /// <param name="stateDisallowed">The disallowed states. These are bit flags that specify the state that the control MUST NOT be in for this mapping to be active. If the current state of the control has any of these bits turned on this mapping will be ignored. </param>
        /// <param name="stateRequired">The required states. These are bit flags that specify the state that the control MUST be in for this mapping to be active.</param>
        /// <param name="specialKeysDisallowed">The special keys that are NOT allowed. If shift, ctrl or alt are pressed and their corresponding bit is turned on in this property the mapping will be ignored.</param>
        /// <param name="specialKeysRequired">The special keys that are required. These keys (any combination of shift/control/alt) MUST be pressed. Otherwise, this mapping will be ignored.</param>
        /// <param name="byPassTrailingActions">Indicates whether to bypass trailing actions once this action matches the current state.</param>
		public GridKeyActionMapping(	Keys keyCode,
										UltraGridAction actionCode,
										UltraGridState stateDisallowed,
										UltraGridState stateRequired,
										SpecialKeys specialKeysDisallowed,
										SpecialKeys specialKeysRequired,
										bool byPassTrailingActions )
			: base( keyCode,
					actionCode,
					(Int64)stateDisallowed,
					(Int64)stateRequired,
					specialKeysDisallowed,
					specialKeysRequired,
					byPassTrailingActions )
		{
		}

        /// <summary>
        /// Gets/sets the action code. 
        /// </summary>
        public new UltraGridAction ActionCode
        {
            get 
            {
                return (UltraGridAction)base.ActionCode;
            }
            set
            {
				// test that the value is in range
				//
				if ( !Enum.IsDefined( typeof(UltraGridAction), value ) )
					throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_162") );

				base.ActionCode = value;
            }
        }

        /// <summary>
        /// Gets/sets the disallowed state. These are bit flags that specify
        /// the state that the control MUST NOT be in for this mapping to be
        /// active. If the current state of the control has any of these 
        /// bits turned on this mapping will be ignored.
        /// </summary>
        public new UltraGridState StateDisallowed
        {
            get 
            {
                return (UltraGridState)base.StateDisallowed;
            }
            set
            {
                base.StateDisallowed = (int)value;
            }
        }

        /// <summary>
        /// Gets/sets the required state. These are bit flags that specify
        /// the state that the control MUST be in for this mapping to be
        /// active.  
        /// </summary>
        public new UltraGridState StateRequired
        {
            get 
            {
                return (UltraGridState)base.StateRequired;
            }
            set
            {
                base.StateRequired = (int)value;
            }
        }
    }

}
