#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyConfiguration(AssemblyRef.Configuration)]
[assembly: AssemblyDescription(AssemblyRef.AssemblyDescriptionBase + " - " + AssemblyRef.Configuration + " Version")]
[assembly: AssemblyTitle(AssemblyRef.AssemblyName + AssemblyRef.ProductTitleSuffix)]
[assembly: AssemblyProduct(AssemblyRef.AssemblyProduct + AssemblyRef.ProductTitleSuffix)]
[assembly: AssemblyCompany(Infragistics.Shared.AssemblyVersion.CompanyName)]
[assembly: AssemblyCopyright("Copyright(c) 2001-"+Infragistics.Shared.AssemblyVersion.EndCopyrightYear+ " Infragistics, Inc.")]
[assembly: AssemblyTrademark("UltraGrid")]
[assembly: AssemblyCulture("")]		
[assembly: System.CLSCompliant(true)]		
[assembly: System.Security.AllowPartiallyTrustedCallers()]		 

// AS 5/1/06 AppStyling
[assembly: Infragistics.Win.AppStyling.Definitions.AssemblyStyleInfoAttribute(typeof(Infragistics.Win.UltraWinGrid.UltraWinGridAssemblyStyleInfo))] 

// AS 1/8/03 - fxcop
// Added attributes to avoid fxcop violation
[assembly: System.Runtime.InteropServices.ComVisible(true)]

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:


// this needs to be bumped after we release anything to the public
//
//************************************************************************** 
// IMPORTANT:	You MUST also change the version number in the 
//				Design string in the AssemblyRef class below
//				to match the Design assembly's version.
//
[assembly: AssemblyVersion(Infragistics.Shared.AssemblyVersion.Version)] // SEE NOTE ABOVE
//
//************************************************************************** 
[assembly: AssemblyFileVersion(Infragistics.Shared.AssemblyVersion.Version)] // AS 7/29/08

//[assembly: SatelliteContractVersion( Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit.Version ) ]
//[assembly: AssemblyInformationalVersion("1.0.5004.9")]

//
// In order to sign your assembly you must specify a key to use. Refer to the 
// Microsoft .NET Framework documentation for more information on assembly signing.
//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified - the assembly cannot be signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. 
//   (*) If the key file and a key name attributes are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP - that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the file is installed into the CSP and used.
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//
// MBS 8/9/07 
// We're now always delay-signing through the project settings
//
//#if DEBUG
//    [assembly: AssemblyDelaySign(false)]
//#else
//    [assembly: AssemblyDelaySign(true)]
//#endif


// JJD 3/19/04 - DNF159 added attribute to prevent looking for satellite
// assemblies when the language is en-US 
[assembly: System.Resources.NeutralResourcesLanguageAttribute("en-US")]

[assembly: System.Resources.SatelliteContractVersion(Infragistics.Shared.AssemblyVersion.SatelliteContractVersion)]

// IMPORTANT:	You must change the version number in the 
//				Design string in the AssemblyRef class below
//				to match the Design assembly's version.
//
class AssemblyRef
{
	public const string BaseResourceName = "Infragistics.Win.UltraWinGrid.strings";

	// AS 12/19/07 BR29058
	internal static readonly string[] AllowedAssemblyPrefixes = new string[] { "Infragistics", "Infragistics2" };

	private const string AssemblyPrefix = "Infragistics2";

	// MRS 10/19/04 changed to separate numbering system for debug builds
	// AS 10/27/06 NA 2007 Vol 1
	//public const string Design = AssemblyPrefix+".Win.UltraWinGrid.v" + Infragistics.Shared.AssemblyVersion.MajorMinor + ".Design, Version=" + Infragistics.Shared.AssemblyVersion.Version + ", Culture=neutral";
	public const string Design = AssemblyPrefix + ".Win.v" + Infragistics.Shared.AssemblyVersion.MajorMinor + ".Design, Version=" + Infragistics.Shared.AssemblyVersion.Version + ", Culture=neutral";

	// SSP 8/10/04 - UltraCalc
	// Added FormulaBuilderDesign constant.
	//
	public const string FormulaBuilderDesign = AssemblyPrefix+".Win.UltraWinCalcManager.v" + Infragistics.Shared.AssemblyVersion.MajorMinor + ".FormulaBuilder, Version=" + Infragistics.Shared.AssemblyVersion.Version + ", Culture=neutral";

	// AS 6/17/04 Moved from Binder.cs to make it easier to manage.
	internal const string BinderAssemblyName = AssemblyPrefix+".Win.UltraWinGrid.v" + Infragistics.Shared.AssemblyVersion.MajorMinor;

	internal const string AssemblyName = AssemblyPrefix+".Win.UltraWinGrid.v" + Infragistics.Shared.AssemblyVersion.MajorMinor;
	internal const string AssemblyProduct = AssemblyPrefix+".Win.UltraWinGrid";
	internal const string AssemblyDescriptionBase = "Infragistics UltraGrid control class library";

	internal const string Configuration = Infragistics.Shared.AssemblyVersion.Configuration;


	internal const string ProductTitleSuffix = "";
}


