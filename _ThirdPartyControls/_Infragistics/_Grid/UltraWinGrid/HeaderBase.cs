#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Drawing.Drawing2D;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Shared.Serialization;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Security.Permissions;

	// SSP 1/30/03 - Row Layout Functionality
	// 
	using Infragistics.Win.Layout;

	/// <summary>
	/// Returns the Header object associated with the object. This property is read-only at run-time. This property is not available at design-time.
	/// </summary>
	/// <remarks>
	/// <p class="body">A Header object represents a column or group header that specifies information about the column or group, and can also serve as the interface for functionality such as moving, swapping or sorting the column or group. Group headers have the added functionality of serving to aggregate multiple columns under a single heading.</p> 
	/// <p class="body">The <b>Header</b> property provides access to the header that is associated with an object. The <b>Header</b> property provides access to the header that is associated with an object. In some instances, the type of header may be ambiguous, such as when accessing the <b>Header</b> property of a UIElement object. You can use the <b>Type</b> property of the Header object returned by the <b>Header</b> property to determine whether the header belongs to a column or a group.</p>
	/// </remarks>
	[ TypeConverter( typeof( System.ComponentModel.ExpandableObjectConverter) )]
	

	public abstract class HeaderBase : GridItemBase, IValueListOwner,
		// SSP 1/31/03 - Row Layout Functionality
		// Column and header will be implementing the ILayoutItem and IGridBagConstraint interfaces.
		//
        // MRS - NAS 9.1 - Groups in RowLayout
		//Infragistics.Win.Layout.ILayoutItem,
        Infragistics.Win.Layout.ILayoutChildItem,
		Infragistics.Win.Layout.IGridBagConstraint,        
        IProvideRowLayoutColumnInfo
	{
		private int     origin = 0;
        private bool    firstItem = false;
        private bool    lastItem = false;
        private string  caption = null;

		// SSP 5/20/04 UWG3097
		// Changed the access modifier of appearanceHolder from private to internal.
		//
		//private Infragistics.Win.AppearanceHolder appearanceHolder = null;
		internal Infragistics.Win.AppearanceHolder appearanceHolder = null;

		private bool			enabled = true;
		private ColScrollRegion	exclusiveColScrollRegion = null;	
	
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private bool			inBeforePosChanged = false;

		// JJD 3/8/02
		// Change member name from 'visible' to 'visiblePos' since it is not CLSCompliant
		// to have a member and a property whose name differs by case only
		//
		/// <summary>
		/// visible position of header
		/// </summary>
		protected int			visiblePos = 0;

		// SSP 12/8/03 UWG2769
		// When the user resizes a column in auto-fit columns mode, we do not want to apply
		// the extra delta width to the column that's being resized in Band.FitColumnsToWidth
		// method. Added this internal flag which is temporarily set to accomplish this.
		//
		internal DefaultableBoolean proportionalResizeOverride = DefaultableBoolean.Default;

		// SSP 7/18/03 - Fixed headers
		//
		private bool isHeaderFixed = false;
		private Infragistics.Win.UltraWinGrid.FixedHeaderIndicator fixedHeaderIndicator;

		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		private string toolTipText = null;

		private TextOrientationInfo textOrientation;

		// MD 12/8/08 - 9.1 - Column Pinning Right
		private DefaultableBoolean fixOnRight;
        
		internal HeaderBase()
		{
		}

		/// <summary>
		/// Always returns false for headers
		/// </summary>
		[ Browsable( false ) ] 
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public override bool IsTabStop
		{
			get
			{
				return false;
			}
		}
		


		/// <summary>
		/// Returns true if any properties are not set to their default values
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        // CDS NAS v9.1 Header CheckBox
        //internal protected bool ShouldSerialize() 
        internal protected virtual bool ShouldSerialize() 
		{
			return	this.ShouldSerializeAppearance()	||
				this.ShouldSerializeTag() ||
				this.ShouldSerializeCaption()			||
				this.ShouldSerializeEnabled()			||
				this.ShouldSerializeVisiblePosition()	||
				this.ShouldSerializeExclusiveColScrollRegion() ||
				// SSP 7/18/03 - Fixed headers
				//
				this.ShouldSerializeFixed( ) ||
				this.ShouldSerializeFixedHeaderIndicator( ) ||
				// SSP 7/27/05 - Header, Row, Summary Tooltips
				// 
				this.ShouldSerializeToolTipText( ) ||

				// MD 4/17/08 - 8.2 - Rotated Column Headers
				this.ShouldSerializeTextOrientation() ||

				// MD 12/8/08 - 9.1 - Column Pinning Right
				this.ShouldSerializeFixOnRight();
		}

		/// <summary>
		/// Resets all properties back to their default values.
		/// </summary>
        // CDS NAS v9.1 Header CheckBox
        //public void Reset()
        public virtual void Reset()
        {
			this.ResetAppearance();
			this.ResetCaption();
			this.ResetEnabled();
			this.ResetExclusiveColScrollRegion();
			// SSP 7/18/03 - Fixed headers
			//
			this.ResetFixed( );
			this.ResetFixedHeaderIndicator( );
			this.ResetToolTipText( );

			// MD 4/17/08 - 8.2 - Rotated Column Headers
			this.ResetTextOrientation();

			// MD 12/8/08 - 9.1 - Column Pinning Right
			this.ResetFixOnRight();
		}

		internal virtual bool FirstItem
        {
            get
            {
                return this.firstItem;
            }
            set
            {
                this.firstItem = value;
            }
        }

        //  CDS 01/27/09 TFS12927 Make this method virtual, so we can override it and set derived-class specific properties
		//internal void InitializeFrom( HeaderBase source, PropertyCategories propertyCategories )
		internal virtual void InitializeFrom( HeaderBase source, PropertyCategories propertyCategories )
		{
			bool initData = (propertyCategories & PropertyCategories.Bands) !=0;
			// Since ssPropCatGroups has more than one bit tunred on we need
			// to check the anded bits for equality
			//
			if ( initData && this.IsGroup )
				initData = ( PropertyCategories.Groups == ( (propertyCategories & PropertyCategories.Groups) ) );

			if ( !initData ) 
				return;

			this.enabled = source.enabled;

			// SSP 11/4/05 BR07503
			// Clone copies over the AppearanceCollection as well. We don't want that.
			// 
			// ------------------------------------------------------------------------------
			//if ( source.appearanceHolder != null )
			//	this.appearanceHolder = source.appearanceHolder.Clone();
			//else
			//	this.ResetAppearance();

			if ( null != source.appearanceHolder &&
				source.appearanceHolder.HasAppearance )
			{
				// Get appearance to trigger allocation of holder
				//
				Infragistics.Win.AppearanceBase app = this.Appearance;
			
				// init the holder from the source
				//
				this.appearanceHolder.InitializeFrom( source.appearanceHolder );
			}
			else
			{
				// reset the appearance holder
				//
				if ( null != this.appearanceHolder &&
					this.appearanceHolder.HasAppearance )
				{
					this.appearanceHolder.Reset();
				}
			}
			// ------------------------------------------------------------------------------
							
			this.Caption = source.caption;

			if ( source.exclusiveColScrollRegion != null )
			{
				if ( this.Band.Layout.ColScrollRegions != null )
				{
					// SSP 12/4/02 nnUWG1840
					// Implemented.
					//
					int index = source.exclusiveColScrollRegion.Index;
					
					Debug.Assert( index >= 0, "Invalid index of the source.exclusiveColScrollRegion." );
					Debug.Assert( index < this.Band.Layout.ColScrollRegions.Count, "This shouldn't be the case. !!!" );

					if ( index >= 0 && index < this.Band.Layout.ColScrollRegions.Count )
					{
						this.exclusiveColScrollRegion = this.Band.Layout.ColScrollRegions[index];
					}
				}
			}

			// JJD 10/16/01
			// Init the visible position
			//
			// SSP 10/27/03 UWG2724
			// Initialize the visiblePos to the source.visiblePos regardless of whether 
			// it's the default visible pos in the source.
			// Commented out the if condition.
			//
			//if ( source.ShouldSerializeVisiblePosition() )
				this.visiblePos = source.visiblePos;

			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			// SSP 7/18/03 - Fixed headers
            //
			this.isHeaderFixed = source.isHeaderFixed;
			this.fixedHeaderIndicator = source.fixedHeaderIndicator;

			// SSP 7/27/05 - Header, Row, Summary Tooltips
			// 
			this.toolTipText = source.toolTipText;

			// MD 4/17/08 - 8.2 - Rotated Column Headers
			this.textOrientation = source.textOrientation;

			// MD 12/9/08 - 9.1 - Column Pinning Right
			this.fixOnRight = source.fixOnRight;
		}

		internal virtual bool LastItem
        {
            get
            {
                return this.lastItem;
            }
            set
            {
                this.lastItem = value;
            }
        }

		
		/// <summary>
		/// The UltraGridColumn object associated with the header. Returns null if the header is not a column header. This property is read-only.
		/// </summary>
		[Browsable(false)]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.UltraWinGrid.UltraGridColumn Column
		{
			get
			{
				return this.GetColumn();
			}
		}
   
        /// <summary>
        /// The UltraGridGroup object associated with the header. Returns null if the header is not a group header. This property is read-only.
        /// </summary>
        [ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public virtual Infragistics.Win.UltraWinGrid.UltraGridGroup Group
        {
            get { return null; }
        }

		internal virtual void InitSynchronization()
		{		
		}

		internal abstract bool Hidden { get; }

		// SSP 7/26/02 UWG1416
		// Made the caption property localizable by adding Localiazable attribute.
		//
		/// <summary>
		/// Returns or sets the caption text of the header.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Caption</b> property is used to determine the text that will be displayed for an object. Generally, text specified by <b>Caption</b> is static (cannot be edited by the user). Editable text is usually specified by the <b>Value</b> property of an object.</p>
		/// <p class="body">If <b>Caption</b> is set to null or to a zero length string, then the property returns the default text of the object (the <b>Key</b> value of the object associated with the Header.)</p>
		/// </remarks>
		[LocalizedDescription("LD_HeaderBase_P_Caption")]
		[LocalizedCategory("LC_Appearance")]
		[ Localizable( true ) ]
		// SSP 11/26/03 UWG1155
		// Added below attribute to allow for multiline editing of caption during design time.
		//
		[ System.ComponentModel.Editor( typeof(  Infragistics.Win.Design.MultiLineTextEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		public string Caption
		{
			get
			{
				// SSP 10/23/03 UWG2267
				// Use null as the default and not the "". The reason for doing this is that
				// if we use "" as the default and resolve to the column's key if it's "", then
				// there is no way for the user to set the column's Key to really an "". They
				// can use " " (space) but the VS.NET designer deserializes " " as "" so
				// setting the property to " " doesn't work. So the only solution was to set the
				// Caption to " " during runtime. However we should make it easy for the user
				// to set the Caption to an "" during design time.
				// Commented out the original code and added the new code below.
				//
				// --------------------------------------------------------------------------------
				
				if ( null == this.caption )
					return this.DefaultHeaderCaption;
				// --------------------------------------------------------------------------------
                
				return this.caption;
			}
			set
			{
				// SSP 8/12/04
				// Check to see if the new property value is the same as the old one before
				// notifying of the prop change.
				//
				if ( this.caption != value )
				{
					this.caption   = value;
					this.NotifyPropChange( PropertyIds.Caption, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected virtual bool ShouldSerializeCaption() 
		{
			// SSP 10/23/03 UWG2267
			// Use null as the default and not the "". The reason for doing this is that
			// if we use "" as the default and resolve to the column's key if it's "", then
			// there is no way for the user to set the column's Key to really an "". They
			// can use " " (space) but the VS.NET designer deserializes " " as "" so
			// setting the property to " " doesn't work. So the only solution was to set the
			// Caption to " " during runtime. However we should make it easy for the user
			// to set the Caption to an "" during design time.
			// Commented out the original code and added the new code below.
			//
			// --------------------------------------------------------------------------------
			
			if ( null == this.caption )
				return false;
			// --------------------------------------------------------------------------------

			// JJD 11/21/01
			// Only return true if the caption isn't the same as the default string.
			//
			return this.caption != this.DefaultHeaderCaption;
		}
 
		/// <summary>
		/// Resets Caption to null.
		/// </summary>
		public void ResetCaption() 
		{
			this.Caption = null;
		}
		internal int Origin
		{
			get
			{
				return this.origin;
			}
		}

		internal abstract bool IsValidDropArea(DropLocationInfo dropLocationInfo, Point point );

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal void SetInBeforePosChanged( bool newValue )
		//{
		//    this.inBeforePosChanged = newValue;
		//}
	

		internal virtual void SetOrigin( int origin )
		{
			this.origin = origin;
		}

		#region Selectable

		// SSP 4/20/05 - NAS 5.2 Filter Row
		// Overrode Selectable. Moved the code from CanSelectCell into here and commented
		// out CanSelectCell.
		//
		/// <summary>
		/// Property: Returns true only if selectable
		/// </summary>
		internal protected override bool Selectable
		{ 
			get 
			{ 
				ISelectionStrategy strategy = this.SelectionStrategyDefault;
				if ( null == strategy || strategy is SelectionStrategyNone )
					return false;

				// Cells can't be selected in ultradropdown.
				//
				if ( this.Band.Layout.Grid is UltraDropDownBase )
					return false;

				// Also return false if Activation is Disabled.
				//
				if ( this.IsDisabled )
					return false;

				return base.Selectable;
			}
		}

		#endregion // Selectable

		// SSP 4/20/05 - NAS 5.2 Filter Row
		// Moved the logic into Selectable property. Selectable is defined as virtual in
		// GridItemBase. Commented out CanSelectHeader.
		//
		

		/// <summary>  
		/// Returns or sets the selected state of the header.
		/// </summary>  
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override bool Selected
		{
			get
			{
				return this.selectedValue;
			}
			set
			{
				if( this.selectedValue != value )
				{
					// RobA UWG865 12/26/01 
					// added code that calls InternalSelectItem
					//
					UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

					if( null != grid )
					{
						// SSP 4/10/02 UWG1075
						// Look at the selection strategy to determine whether to clear
						// any already selected column headers.
						//
						//grid.InternalSelectItem( this, true, value );

						bool clearExistingItems = true;

						if ( this is ColumnHeader )
						{
							SelectionStrategyBase selectionStrategy = ((ISelectionStrategyProvider)this.Band).SelectionStrategyColumn;

							if ( null != selectionStrategy )
								clearExistingItems = selectionStrategy.IsSingleSelect;
						}

						grid.InternalSelectItem( this, clearExistingItems, value );
					}
					
					// if this is not an UltraGrid just set the selected property
					//
					else
					{
						this.selectedValue = value;
						this.NotifyPropChange( PropertyIds.Selected, null );					
					}
				}

			}
		}

		internal void SetSelectedFlag( bool selected )
		{
			this.selectedValue = selected;
		}

		// AS 10/24/01 Moved to GridItemBase
//		/// <summary>
//		/// True if the item is selectable (read-only)
//		/// </summary>
//		protected override bool Selectable
//		{ 
//			get
//			{
//				return !this.IsDisabled; 
//			}
//		}

		internal void InitAppearanceHolder()
		{
			if ( this.Band != null &&
				 this.appearanceHolder != null )
				this.appearanceHolder.Collection = this.Band.Layout.Appearances;
		}

		/// <summary>  
		/// Returns or sets the Appearance object that controls the object's formatting.
		/// </summary>  
		/// <remarks>
		/// <p class="body">The <b>Appearance</b> property of an object is used to associate the object with an Appearance object that will determine its appearance. The Appearance object has properties that control settings such as color, borders, font, transparency, etc. For many of the objects in the UltraWinGrid, you do not set formatting properties directly. Instead, you set the properties of an Appearance object, which controls the formatting of the object it is attached to.</p>
		/// <p class="body">There are two ways of working with the <b>Appearance</b> property and assigning the attributes of an Appearance object to other objects. One way is to create a new Appearance object, adding it directly to the Appearances collection. Then you assign the new Appearance object  to the <b>Appearance</b> property of the object you want to format. This method uses a "named" Appearance object that you must explicitly create (and to which you must assign property settings) before it can be used. For instance, you could create an object in the grid's Appearances collection and assign it some values as follows:</p>
		/// <p class="code">UltraWinGrid1.Appearances.Add "New1"</p>
		/// <p class="code">UltraWinGrid1.Appearances("New1").BorderColor = vbBlue</p>
		/// <p class="code">UltraWinGrid1.Appearances("New1").ForeColor = vbRed</p>
		/// <p class="body">Creating the object in this way does not apply formatting to any visible part of the grid. The object simply exists in the collection with its property values, waiting to be used. To actually use the object, you must assign it to the grid's (or another object's) <b>Appearance</b> property:</p>
		/// <p class="code">UltraWinGrid1.Appearance = UltraWinGrid1.Appearances("New1")</p>
		/// <p class="body">In this case, only one Appearance object exists. The grid's appearance is governed by the settings of the "New1" object in the collection. Any changes you make to the object in the collection will immediately be reflected in the grid.</p>
		/// <p class="body">The second way of working with the <b>Appearance</b> property is to use it to set property values directly, such as:</p>
		/// <p class="code">UltraWinGrid1.Appearance.ForeColor = vbBlue</p>
		/// <p class="body">In this case, an Appearance object is automatically created by the control. This Appearance object is not a member of an Appearances collection and it does not have a name. It is specific to the object for which it was created; it is an "intrinsic" Appearance object. Changes to the properties of an intrinsic Appearance object are reflected only in the object to which it is attached.</p>
		/// <p class="body">Note that you can assign properties from a named Appearance object to an intrinsic Appearance object without creating a dependency relationship. For example, the following code...</p>
		/// <p class="code">UltraWinGrid1.Appearance.ForeColor = UltraWinGrid1.Appearances("New1").ForeColor</p>
		/// <p class="body">...does <i>not</i> establish a relationship between the foreground color of the intrinsic object and that of the named object. It is simply a one-time assignment of the named object's value to that of the intrinsic object. In this case, two Appearance objects exist - one in the collection and one attached to the grid - and they operate independently of one another.</p>
		/// <p class="body">If you wish to assign all the properties of a named object to an intrinsic object at once without creating a dependency relationship, you can use the <b>Clone</b> method of the Appearance object to duplicate its settings and apply them. So if you wanted to apply all the property settings of the named Appearance object "New1" to the grid's intrinsic Appearance object, but you did not want changes made to "New1" automatically reflected in the grid, you would use the following code:</p>
		/// <p class="code">UltraWinGrid1.Appearance = UltraWinGrid1.Appearances("New1").Clone</p>
		/// <p class="body">Note that the properties of an Appearance object can also operate in a hierarchical fashion. Certain properties can be set to a "use default" value, which indicates to the control that the property should take its setting from the object's parent. This functionality is enabled by default, so that unless you specify otherwise, child objects resemble their parents, and formatting set at higher levels of the grid hierarchy is inherited by objects lower in the hierarchy.</p>
		/// </remarks>
		[LocalizedDescription("LD_HeaderBase_P_Appearance")]
		[LocalizedCategory("LC_Appearance")]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase Appearance
		{
			get
			{
				if ( null == this.appearanceHolder )
				{
					this.appearanceHolder = new Infragistics.Win.AppearanceHolder();
				
					// hook up the prop change notifications
					//
					this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if ( this.Band != null &&
					 this.Band.Layout != null )
					this.appearanceHolder.Collection = this.Band.Layout.Appearances;

				return this.appearanceHolder.Appearance;

			}
			set
			{
				if( this.appearanceHolder == null  ||
					!this.appearanceHolder.HasAppearance ||
					this.appearanceHolder.Appearance != value )
				{
					// remove the old prop change notifications
					//
					// SSP 11/9/01 UWG722
					// if this.appearanceHolder is null, create a new one before going ahead 
					// with setting appearance on the appearance holder.
					//
					//if ( null != this.appearanceHolder.Appearance )
					if ( null == this.appearanceHolder )
					{
						// SSP 11/9/01 UWG722
						//this.appearanceHolder = new Infragistics.Win.AppearanceHolder( this.Band.Layout.Appearances );
						this.appearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}
						
				                 
					// Initialize the collection
					//
					if ( this.Band != null &&
					 this.Band.Layout != null )
						this.appearanceHolder.Collection = this.Band.Layout.Appearances;

					this.appearanceHolder.Appearance = value;            
					
					this.NotifyPropChange( PropertyIds.Appearance, null );
				}
			}
		}

		/// <summary>
		/// Returns true if an appearance object has been created.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
		Browsable( false ) ]
		
		public bool HasAppearance
		{
			get
			{
				return ( null != this.appearanceHolder &&
					this.appearanceHolder.HasAppearance );
			}
		}
		
		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAppearance() 
		{ 
			return ( this.HasAppearance &&
				this.appearanceHolder.ShouldSerialize() );
		}

		/// <summary>
		/// Resets Appearance to its default settings.
		/// </summary>
		public void ResetAppearance() 
		{ 
			if ( this.HasAppearance )
			{
//				// remove the prop change notifications
//				//
//				this.appearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
//                    
//				this.appearanceHolder.Reset();
				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//this.appearanceHolder.Appearance.Reset();

				// Call reset on the holder instead of the appearance
				//
				this.appearanceHolder.Reset();

				// Notify listeners that the appearance has changed. 
				this.NotifyPropChange(PropertyIds.Appearance);
			} 
		}

		internal override bool IsDisabled
		{
			get
			{
				return ( !this.enabled );
			}
		}
	 
		/// <summary>  
		/// Determines if the object associated with the header is enabled.
		/// </summary>  
		[LocalizedDescription("LD_HeaderBase_P_Enabled")]
		[LocalizedCategory("LC_Behavior")]
		public bool Enabled
		{
			get
			{
				return this.enabled;
			}
			set
			{
				if( this.enabled != value )
				{
					this.enabled = value;
					this.NotifyPropChange( PropertyIds.Enabled, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeEnabled() 
		{ 
			return this.enabled != true; 
		}
		/// <summary>
		/// Resets Enabled to its default value (True).
		/// </summary>
		public void ResetEnabled() 
		{ 
			this.Enabled = true; 
		}

		internal abstract string DefaultHeaderCaption { get; }
		internal   abstract int Extent { get; }

        internal virtual UltraGridColumn GetFirstVisibleCol( bool includeNonActivateable ) { return null; }	       
        internal virtual UltraGridColumn GetNextVisibleCol( UltraGridColumn column, bool includeNonActivateable ){ return null; }
        internal virtual UltraGridColumn GetPrevVisibleCol( UltraGridColumn column, bool includeNonActivateable ){ return null; }
        internal virtual UltraGridColumn GetLastVisibleCol( bool includeNonActivateable ){ return null; }
        internal virtual UltraGridColumn GetVisibleColAbove( UltraGridColumn column, bool topMost, bool includeNonActivateable ){ return null; }
        internal virtual UltraGridColumn GetVisibleColBelow( UltraGridColumn column, bool bottomMost, bool includeNonActivateable ){ return null; }

        internal virtual bool IsGroup { get { return false; } }	
        internal virtual void AdjustForCellLevel ( ref Rectangle rect, UltraGridRow row ){}
        internal virtual bool AdjustForCellPicture ( ref Rectangle rect, ref Rectangle rcPicture, ref AppearanceData resolvedCellAppearance ){return false;}

		// JJD 1/23/02 - UWG971
		// Added property to get the column or groups exclusive region
		//
		internal Infragistics.Win.UltraWinGrid.ColScrollRegion ExclusiveColScrollRegionResolved
		{
			get
			{
				ColScrollRegion exclusive = this.ExclusiveColScrollRegion;

				ColumnHeader colheader = this as ColumnHeader;

				if ( colheader != null )
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( colheader.Column.Group != null )
					//    exclusive = colheader.Column.Group.Header.ExclusiveColScrollRegion;
					UltraGridColumn column = colheader.Column;

					// MD 1/21/09 - Groups in RowLayouts
					// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
					//if ( column.Group != null )
					//    exclusive = column.Group.Header.ExclusiveColScrollRegion;
					if ( column.GroupResolved != null )
						exclusive = column.GroupResolved.Header.ExclusiveColScrollRegion;
				}

				return exclusive;
			}
		}

		/// <summary>  
		/// Returns or sets the only colscrollregion in which a column is displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the colscrollregion whose value will be modified. You can use this reference to access any of the returned colscrollregion's properties or methods.</p>
		/// <p class="body">When this property is set, the column will only appear in the specified colscrollregion; it will not appear in any other colscrollregion. When a colscrollregion is first made exclusive, only the column whose header had this property set will appear in the scrolling region. However, additional columns can be added to the colscrollregion by setting this property for their headers.</p>
		/// <p class="body">If an exclusive colscrollregion is unable to display its columns because their headers have been hidden, the colscrollregion will display all visible columns.</p>
		/// <p class="body">The <b>VisibleHeaders</b> property of a colscrollregion can be used to return references to the columns that are displayed in a colscrollregion.</p>
		/// </remarks>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.UltraWinGrid.ColScrollRegion ExclusiveColScrollRegion
		{
			get
			{
				// MRS 9/24/04
				// Get the band once
				UltraGridBand band = this.Band;

				// JJD 12/27/01
				// We don't support exclusive col scroll regions with
				// cardview or AutoFitColumns
				//
				if ( band.CardView || band.Layout.AutoFitAllColumns
					// SSP 2/19/03 - Row Layout Functionality
					// In row layout functionality, exclusive col scroll region functionality is disabled.
					//
					|| band.UseRowLayoutResolved )
					return null;

				// SSP 6/12/03 UWG2256
				// If the exclusive col scroll region has been removed from the collection (and thus it
				// would've been disposed of), then set the exclusiveColScrollRegion to null.
				//
				// ------------------------------------------------------------------------------------
				if ( null != this.exclusiveColScrollRegion && this.exclusiveColScrollRegion.Disposed )
				{
					this.exclusiveColScrollRegion = null;

					// SSP 7/18/03 - Fixed headers
					// Bump the fixed headers version number so that we recreate the fixed headers list
					// since nulling out exclusive col scroll region could potentially cause the 
					// FixedResolved start returning true when before it was returning false due to
					// the exclusinve col scroll region.
					//
					if ( null != band )
						band.BumpFixedHeadersVerifyVersion( true );
				}
				// ------------------------------------------------------------------------------------

				return this.exclusiveColScrollRegion;
			}
			set
			{
				if( this.exclusiveColScrollRegion != value )
				{
					// make sure that if the value is not null that it is
					// at least from the same layout as this header
					//
					// AS 2/28/02 UWG1033
					// Make sure that the band is not null before referencing it.
					//
					if ( null != value  &&
						(this.Band != null && value.Layout != this.Band.Layout) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_160"));

					this.exclusiveColScrollRegion = value;

					// SSP 7/18/03 - Fixed headers
					// Bump the fixed headers version number so that we recreate the fixed headers list
					// since nulling out exclusive col scroll region could potentially cause the 
					// FixedResolved start returning true when before it was returning false due to
					// the exclusinve col scroll region.
					//
					if ( null != this.Band )
						this.Band.BumpFixedHeadersVerifyVersion( true );

					this.NotifyPropChange( PropertyIds.ExclusiveColScrollRegion, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeExclusiveColScrollRegion() 
		{ 
			return this.exclusiveColScrollRegion != null; 
		}

		/// <summary>
		/// Resets to null
		/// </summary>
		public void ResetExclusiveColScrollRegion() 
		{ 
			this.ExclusiveColScrollRegion = null; 
		}

 
		/// <summary>
		/// Returns or sets the height of the row (excluding spacing).
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Height</b> property is used to determine the vertical dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">For the Header object, this property is read-only. In a particular band, each column header has the same height. This height is determined by taking the largest height that results from the resolution of each column's header's <b>Appearance</b> attributes and the band's <b>ColHeaderLines</b> property.</p>
		/// </remarks>
		[ Browsable(false) ] 
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public abstract int Height
		{
			get;
		}


		internal int GetActualWidth( )
		{
			return this.GetActualWidth( false );
		}

		internal abstract int GetActualWidth( bool ignoreDraggingWidth );

		internal int SerializedVisiblePosition
		{
			get 
			{
				// JJD 10/16/01
				// The first time through the visible position is -1 unless
				// a different value was persisted. In this case initialize
				// the position to the column's index
				// 
				if ( this.visiblePos < 0 )
					this.visiblePos = this.DefaultVisiblePosition;

				return this.visiblePos;
			}
		}

		internal virtual int DefaultVisiblePosition
		{
			get 
			{
				return 0;
			}
		}

		
		/// <summary>
		/// Returns or sets the visible position of a header.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property can be used to specify the ordinal positions of groups and columns.</p>
		/// <p class="body">For group headers, this property returns or sets the position of the group within that group's band. For column headers, this property returns or sets the position of the column within its group, if the column belongs to a group, or its band, if the column belongs to a band.</p>
		/// </remarks>
		[LocalizedDescription("LD_HeaderBase_P_VisiblePosition")]
		[LocalizedCategory("LC_Appearance")]
		[MergableProperty(false)]
		public virtual int VisiblePosition
		{
			get
			{
				return this.visiblePos;
			}
			set
			{
				if( this.visiblePos != value )
				{
					this.visiblePos = value;

                    // MRS 5/14/2009 - TFS17620
                    // Ripped this out because it's causing performance (TFS17620) and serilalization (TFS17457) issues.
                    //
                    //// MBS 10/7/08
                    //// Discovered while addressing TFS8698.  If we set the visible position
                    //// of a column, it is possible that this causes a row's height to change
                    //// (such as if the summaries in a GroupByRow need to wrap), but the 
                    //// existing metrics have not been recalculated so we will incorrectly
                    //// clip data.  This will ensure that we have the updated metrics.
                    //if (this.Column != null && this.Column.ParentCollection != null)
                    //    this.Column.ParentCollection.RecalculateMetrics();

					this.NotifyPropChange( PropertyIds.VisiblePosition, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected virtual bool ShouldSerializeVisiblePosition()
		{
			int defaultPosition = this.DefaultVisiblePosition;

			if ( defaultPosition < 0 )
				return ( this.SerializedVisiblePosition >= 0 );
			
			return ( this.VisiblePosition != defaultPosition ); 
		}

		// SSP 10/27/03 UWG2724
		// Added InternalResetVisiblePos method.
		//
		internal void InternalResetVisiblePos( )
		{
			this.visiblePos = -1;
		}

		/// <summary>
		/// Returns an empty string so that property window display nothing 
		/// </summary>
        /// <returns>an empty string.</returns>
		public override string ToString() 
		{
			return string.Empty; 
		}

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// SSP 8/13/04 UWG3605
			// If the header is not connected to a layout then there there's no point
			// in responding to sub object prop change. The reason for doing this is that
			// by prematurely accessing AppearanceHolder.Appearance property when it's
			// appearances collection is not initialized because the header/column isn't
			// connected to a layout yet, we are causing it's RootAppearance to get
			// initialized to a brand new appearance without finding the appropriate
			// linked appearance from the layout's appearance's collection. This happens
			// in initialize component when the transaction is canceled and might happen
			// other times as well.
			// 
			// SSP 1/24/05 BR00255
			// This could be a group header or a band header as well and thus its Column 
			// would be null. To make it work for all three header types, use the Band
			// property instead.
			//
			//if ( null == this.Column || null == this.Column.Layout )
			if ( null == this.Band || null == this.Band.Layout )
				return;

			if ( this.HasAppearance  && 
				propChange.Source == this.appearanceHolder.RootAppearance )
			{
				// Pass this notifcation on to our listeners with the
				// property id of 'Bands'
				if ( ( null != propChange.FindPropId( AppearancePropIds.FontData ) ) ||
					( null != propChange.FindPropId( AppearancePropIds.Image ) ) )
				{
					this.Band.ClearCachedHeaderFontHeights();
				}
				this.NotifyPropChange( PropertyIds.Appearance, propChange );
				return;
			}


			//Debug.Assert( false, "Unknown sub object in Row.OnSubObjectPropChanged" );
		}

		/// <summary>
		/// Called when the object is disposed of
		/// </summary>
		protected override void OnDispose()
		{
			
			// It is importantant to unhook the holder and call reset on it
			// when we are disposed
			//
			if ( this.appearanceHolder != null )
			{
				this.appearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.appearanceHolder.Reset();
				this.appearanceHolder = null;
			}


			base.OnDispose();
		}

		


		/// <summary>
		/// Returns an Appearance object with its properties set to the actual values that will be used to display the object.
		/// </summary>
		///	<remarks>
		///	<p class="body">Examining the value of an Appearance object property that has not been set will return the "use default" value, not the internal value that is actually being used to display the object affected by the Appearance object. In order to find out what values are being used, you must use the <b>ResolveAppearance</b> method. This method will evaluate the property values of an Appearance object and return an Appearance object with all of its properties set to meaningful values that can be used to determine how the object will look.</p>
		///	<p class="body">When you change the properties of an Appearance object, you are not required to specify a value for every property that object supports. Whether the Appearance object is a stand-alone object you are creating from scratch, or an intrinsic object that is already attached to some other object, you can set certain properties and ignore others. The properties you do not explicitly set are given a "use default" value that indicates there is no specific setting for that property.</p>
		///	<p class="body">Properties that are set to the "use default" value derive their settings from other objects by following an appearance hierarchy. In the appearance hierarchy, each object has a parent object from which it can inherit the actual numeric values to use in place of the "use default" values. The "use default" value should not be confused with the initial setting of the property, which is generally referred to as the default value. In many cases, the default setting of an object's property will be "use default"; this means that the property is initially set not to use a specific value. The "use default" value will be 0 for an enumerated property (usually indicated by a constant ending in the word "default", such as AlignDefault) or -1 (0xFFFFFFFF) for a numeric setting, such as that used by color-related properties.</p>
		///	<p class="body">So for example, if the Appearance object of a cell has its <b>BackColor</b> property set to -1, the control will use the setting of the row's <b>BackColor</b> property for the cell, because the row is above the cell in the appearance hierarchy. The top level of the appearance hierarchy is the UltraWinGrid control itself. If any of the UltraWinGrid's Appearance object properties are set to their "use default" values, the control uses built-in values (the "factory presets") for those properties. For example, the factory preset of the <b>BackColor</b> property of the grid's Appearance object is the system button face color (0x8000000F). This is the value that will be used for the grid's background color when the <b>BackColor</b> property of the grid's Appearance object is set to the "use default" value.</p>
		///	<p class="body">The <b>ResolveAppearance</b> method will return an Appearance object with all of its "use default" settings converted into actual values. It does this by navigating the appearance hierarchy for each property until an explicit setting or a factory preset is encountered. If you simply place a grid on a form, run the project, and examine the setting of the <b>BackColor</b> property of the grid's intrinsic Appearance object:</p>
		///	<p class="code">MsgBox Hex(UltraWinGrid1.Appearance.BackColor)</p>
		///	<p class="body">...you will see that it is set to the "use default" value (0xFFFFFFFF). However, if you use the <b>ResolveAppearance</b> method to return the same value:</p>
		///	<p class="code">MsgBox Hex(UltraWinGrid1.ResolveAppearance.BackColor)</p>
		///	<p class="body">...you will see that it is set to the system button face color (0x8000000F). Note that this code takes advantage of the fact that the <b>ResolveAppearance</b> method returns an Appearance object to simplify the code that must be written. This code could be written out in a longer form as follows:</p>
		///	<p class="code">Dim objAppearance as UltraWinGrid.Appearance</p>
		///	<p class="code">Set objAppearance = UltraWinGrid1.ResolveAppearance</p>
		///	<p class="code">MsgBox Hex(objAppearance.BackColor)</p> 
		///	</remarks>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		public void ResolveAppearance( ref AppearanceData appData )
		{
			this.ResolveAppearance( ref appData, AppearancePropFlags.AllRenderAndCursor );
		}

		/// <summary>
		/// Returns an Appearance object with its properties set to the actual values that will be used to display the object.
		/// </summary>
		///	<remarks>
		///	<p class="body">Examining the value of an Appearance object property that has not been set will return the "use default" value, not the internal value that is actually being used to display the object affected by the Appearance object. In order to find out what values are being used, you must use the <b>ResolveAppearance</b> method. This method will evaluate the property values of an Appearance object and return an Appearance object with all of its properties set to meaningful values that can be used to determine how the object will look.</p>
		///	<p class="body">When you change the properties of an Appearance object, you are not required to specify a value for every property that object supports. Whether the Appearance object is a stand-alone object you are creating from scratch, or an intrinsic object that is already attached to some other object, you can set certain properties and ignore others. The properties you do not explicitly set are given a "use default" value that indicates there is no specific setting for that property.</p>
		///	<p class="body">Properties that are set to the "use default" value derive their settings from other objects by following an appearance hierarchy. In the appearance hierarchy, each object has a parent object from which it can inherit the actual numeric values to use in place of the "use default" values. The "use default" value should not be confused with the initial setting of the property, which is generally referred to as the default value. In many cases, the default setting of an object's property will be "use default"; this means that the property is initially set not to use a specific value. The "use default" value will be 0 for an enumerated property (usually indicated by a constant ending in the word "default", such as AlignDefault) or -1 (0xFFFFFFFF) for a numeric setting, such as that used by color-related properties.</p>
		///	<p class="body">So for example, if the Appearance object of a cell has its <b>BackColor</b> property set to -1, the control will use the setting of the row's <b>BackColor</b> property for the cell, because the row is above the cell in the appearance hierarchy. The top level of the appearance hierarchy is the UltraWinGrid control itself. If any of the UltraWinGrid's Appearance object properties are set to their "use default" values, the control uses built-in values (the "factory presets") for those properties. For example, the factory preset of the <b>BackColor</b> property of the grid's Appearance object is the system button face color (0x8000000F). This is the value that will be used for the grid's background color when the <b>BackColor</b> property of the grid's Appearance object is set to the "use default" value.</p>
		///	<p class="body">The <b>ResolveAppearance</b> method will return an Appearance object with all of its "use default" settings converted into actual values. It does this by navigating the appearance hierarchy for each property until an explicit setting or a factory preset is encountered. If you simply place a grid on a form, run the project, and examine the setting of the <b>BackColor</b> property of the grid's intrinsic Appearance object:</p>
		///	<p class="code">MsgBox Hex(UltraWinGrid1.Appearance.BackColor)</p>
		///	<p class="body">...you will see that it is set to the "use default" value (0xFFFFFFFF). However, if you use the <b>ResolveAppearance</b> method to return the same value:</p>
		///	<p class="code">MsgBox Hex(UltraWinGrid1.ResolveAppearance.BackColor)</p>
		///	<p class="body">...you will see that it is set to the system button face color (0x8000000F). Note that this code takes advantage of the fact that the <b>ResolveAppearance</b> method returns an Appearance object to simplify the code that must be written. This code could be written out in a longer form as follows:</p>
		///	<p class="code">Dim objAppearance as UltraWinGrid.Appearance</p>
		///	<p class="code">Set objAppearance = UltraWinGrid1.ResolveAppearance</p>
		///	<p class="code">MsgBox Hex(objAppearance.BackColor)</p> 
		///	</remarks>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		/// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
		public void ResolveAppearance( ref AppearanceData appData, 
			AppearancePropFlags requestedProps ) 
		{
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingHeader parameter.
			// 
			//this.ResolveAppearance( ref appData, ref requestedProps, true );
			this.ResolveAppearance( ref appData, ref requestedProps, true, false );
		}

		// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
		// Added an overload of ResolveAppearance that takes in resolveDefaultColors.
		// 
		internal void ResolveAppearance( 
			ref AppearanceData appData, 
			ref AppearancePropFlags requestedProps,
			bool resolveDefaultColors,
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingHeader parameter.
			// 
			bool hotTrackingHeader ) 
		{
			this.ResolveAppearance( ref appData, ref requestedProps, resolveDefaultColors, hotTrackingHeader, false );
		}

		// SSP 3/14/06 - App Styling
		// Added an overload that takes in isGroupByBoxButton parameter.
		// 
		internal void ResolveAppearance( 
			ref AppearanceData appData, 
			ref AppearancePropFlags requestedProps,
			bool resolveDefaultColors,
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingHeader parameter.
			// 
			bool hotTrackingHeader, 
			bool isGroupByBoxButton ) 
		{
			UltraGridBand band = this.Band;

			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof (Infragistics.Win.UltraWinGrid.HeaderBase), requestedProps );

			// SSP 3/12/06 - App Styling
			// 
			// ----------------------------------------------------------------------------------
			StyleUtils.Role eRole = StyleUtils.GetHeaderRole( this, isGroupByBoxButton );
			context.Role = null != band ? StyleUtils.GetRole( band, eRole, out context.ResolutionOrder ) : null;
			// ----------------------------------------------------------------------------------

			// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
			// Added an overload of ResolveAppearance that takes in resolveDefaultColors.
			//
			context.ResolveDefaultColors = resolveDefaultColors;

			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingHeader parameter.
			// 
			if ( hotTrackingHeader && null != band )
			{
				band.MergeBLOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.HotTrackHeaderAppearance
					// SSP 3/13/06 - App Styling
					// Use the new overload that takes in the app styling related params.
					// 
					, AppStyling.RoleState.HotTracked );
			}

			// SSP 7/25/03 
			// Commented below code out and added new code for resolving group-by column header
			// appearance towards the end of the method.
			//
			

			// JJD 1/10/02
			// Set the IsCard flag if this is a cardview band
			//
			// SSP 8/8/05 - Optimizations
			// 
			//if ( this.GetType() != typeof( BandHeader ) && this.Band != null && this.Band.CardView )
			if ( !( this is BandHeader ) && band != null && band.CardView )
				context.IsCard = true;

			if ( this.HasAppearance )
			{
				// SSP 3/8/06 - App Styling
				// Added if condition to the existing code.
				// 
				if ( context.ResolutionOrder.UseControlInfo )
				{
					// JJD 12/12/02 - Optimization
					// Call the Appearance object's MergeData method instead so
					// we don't make unnecessary copies of the data structure
					//AppearanceData.MergeAppearance(ref appData, 
					//							   this.appearanceHolder.Appearance.Data,
					//							   ref context.UnresolvedProps );
					this.appearanceHolder.Appearance.MergeData(ref appData, ref context.UnresolvedProps );
				}
			}

			// JJD 10/22/01
			// Make sure the band is not null
			//
			if ( null != band )
			{
				// SSP 7/25/03 - Fixed headers
				// Apply the fixed header appearance.
				//
				if ( this.FixedResolved )
				{
					context.IsFixedHeader = true;
					band.ResolveAppearance( ref appData, ref context );
					context.IsFixedHeader = false;

					if ( 0 == context.UnresolvedProps )
						return;
				}

				// SSP 7/25/03 
				// Commented code in the beginning of the method that sets teh IsGroupByColumnHeader 
				// and added this code here.
				// Apply the group-by column header appearance.
				//
				UltraGridColumn column = this.Column;
				if ( null != column && column.IsGroupByColumn && 
					ViewStyleBand.OutlookGroupBy == band.Layout.ViewStyleBand )
				{
					context.IsGroupByColumnHeader = true;
					band.ResolveAppearance( ref appData, ref context );
					context.IsGroupByColumnHeader = false;

					if ( 0 == context.UnresolvedProps )
						return;
				}

                // MBS 7/1/09 - NA9.2 ActiveCellHeaderAppearances
                if (column != null && !column.IsGroupByColumn && 
                    this.Layout != null && this.Layout.ActiveCellInternal != null &&
                    this.Layout.ActiveCellInternal.Column == column)
                {
                    band.MergeBLOverrideAppearances(ref appData, ref context, 
                        UltraGridOverride.OverrideAppearanceIndex.ActiveCellColumnHeaderAppearance,
                        Infragistics.Win.AppStyling.RoleState.ActiveCell);

                    if (0 == context.UnresolvedProps)
                        return;
                }

				// SSP 5/4/06 - App Styling
				// Resolve the CardView state appearances.
				// 
				if ( band.CardView )
					StyleUtils.ResolveAppearance( AppStyling.RoleState.CardView, ref appData, ref context );

				band.ResolveAppearance( ref appData, ref context );
			}

			requestedProps = context.UnresolvedProps;
		}

        /// <summary>
        /// Returns the UIElement associated with the object, in the specified row and column scrolling regions.
        /// </summary>
        /// <param name="csr">The ColScrollRegion</param>
        /// <param name="rsr">The RowScrollRegion</param>
        /// <param name="verifyElements">Indicates whether to VerifyChildElements</param>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to an object's UIElement. The reference can be used to set properties of, and invoke methods on, the UIElement object associated with an object. You can use this reference to access any of the UIElement's properties or methods.</p>
		/// <p class="body"> The <b>Type</b> property can be used to determine what type of UIElement was returned. If no UIElement exists, meaning the object is not displayed, Nothing is returned.</p>
		/// <p class="body">The <b>ParentUIElement</b> property can be used to return a reference to a UIElement's parent UIElement object. The <b>UIElements</b> property can be used to return a reference to a collection of child UIElement objects for a UIElement.</p>
		/// <p class="body">The <b>UIElementFromPoint</b> method can be invoked to return a reference to an UltraGridUIElement object residing at specific coordinates.</p>
		/// <p class="body">The <b>CanResolveUIElement</b> method can be invoked to determine whether an object or one of its ancestors can be resolved as a specific type of UIElement.</p>
		/// </remarks>
        /// <returns>The UIElement associated with the object, in the specified row and column scrolling regions.</returns>
        public override UIElement GetUIElement( RowScrollRegion rsr, ColScrollRegion csr, bool verifyElements )
        {
			// SSP 12/10/03 UWG2787
			// We shouldn't really throw an exception here. Just return if the rsr or csr is null.
			// Commented out below code.
			//
			

            // get the grid's main element
            //
            UIElement gridElement = this.Band.Layout.GetUIElement( verifyElements );

			UIElement element = null;

            if ( null != gridElement )
            {
				object[] arr = new object[] { this, rsr, csr };

                element = gridElement.GetDescendant( typeof( HeaderUIElement ), arr );

				// SSP 12/21/01
				// If the band is card view, then the associated ui element with this
				// header will be a CardLabelUIElement. So get that.
				//
				if ( null == element )
				{
					element = gridElement.GetDescendant( typeof( CardLabelUIElement ), arr );
				}
            }

            return element;
        }
		internal virtual UltraGridColumn GetRelatedVisibleCol ( UltraGridColumn column, 
            NavigateType navType, 
            bool includeNonActivateable )
        {
            //CColumn* pRelatedCol = NULL;
            UltraGridColumn relatedCol = null;

            switch( navType )
            {
                case NavigateType.First :
                    //pRelatedCol = GetFirstVisibleCol ( bIncludeNonActivateable );
                    relatedCol = this.GetFirstVisibleCol( includeNonActivateable );					
                    break;
                case NavigateType.Last :
                    //pRelatedCol = GetLastVisibleCol ( bIncludeNonActivateable );
                    relatedCol = this.GetLastVisibleCol ( includeNonActivateable );
                    break;
                case NavigateType.Next :
                    //pRelatedCol = GetNextVisibleCol ( Column, bIncludeNonActivateable );
                    relatedCol = this.GetNextVisibleCol ( column, includeNonActivateable );
                    break;
                case NavigateType.Prev :
                    //pRelatedCol = GetPrevVisibleCol ( Column, bIncludeNonActivateable );
                    relatedCol = this.GetPrevVisibleCol ( column, includeNonActivateable );
                    break;
                case NavigateType.Above :
                    //pRelatedCol = GetVisibleColAbove ( Column, false, bIncludeNonActivateable );
                    relatedCol = this.GetVisibleColAbove ( column, false, includeNonActivateable );
                    break;
                case NavigateType.Below :
                    //pRelatedCol = GetVisibleColBelow ( Column, false, bIncludeNonActivateable );
                    relatedCol = this.GetVisibleColBelow ( column, false, includeNonActivateable );
                    break;

                case NavigateType.Topmost :
                    //pRelatedCol = GetVisibleColAbove ( Column, true, bIncludeNonActivateable );
                    relatedCol = this.GetVisibleColAbove ( column, true, includeNonActivateable );
                    break;
                case NavigateType.Bottommost :
                    relatedCol = this.GetVisibleColBelow ( column, true, includeNonActivateable );
                    break;    
                default:
                    //FAIL("Invalid navtupe passed into CPositionItem::GetRelatedVisibleCol");
                    Debug.Assert(false, "Invalid navtupe passed into HeaderBase.GetRelatedVisibleCol");
                    break;
            }

            return relatedCol;
        }
		
		/// <summary>
		/// Adjusts the passed in rect based on the from and to enumerators
		/// </summary>
		/// <param name="enumDimFrom"></param>
		/// <param name="enumDimto"></param>
		/// <param name="rect"></param>
		/// <param name="row"></param>
		/// <param name="resolvedCellAppearance"></param>
		/// <param name="pictureRect"></param>
        void AdjustDimensions( Infragistics.Win.UltraWinGrid.PositionDimensions enumDimFrom,
            Infragistics.Win.UltraWinGrid.PositionDimensions enumDimto,
            ref Rectangle rect,
            UltraGridRow row,
            ref AppearanceData resolvedCellAppearance,
            ref Rectangle pictureRect )
        {			
            //ASSERT ( enumDimto >= enumDimFrom, "enumDimto < enumDimFrom in CPositionItem::AdjustDimensions" );
            Debug.Assert( enumDimto >=  enumDimFrom, "enumDimto < enumDimFrom in HeaderBase.AdjustDimensions" );

            // Note: These enumerated values are organized so that a higher values implies
            //       the adjustments for ALL lower value enumerations have already been
            //       adjusted for. Therefore, we can conventiantly nest the adjustments
            //       below.
            //
            //if ( enumDimto >= POSITEM_DIMENSIONS_INSIDE_PREROWSELECTORS )
            if ( enumDimto >= PositionDimensions.InsidePreRowSelectors )
            {
                //if ( enumDimFrom < POSITEM_DIMENSIONS_INSIDE_PREROWSELECTORS )
                if ( enumDimFrom < PositionDimensions.InsidePreRowSelectors )
                    //AdjustForPreRowSelectorArea( rect );
                    this.AdjustForPreRowSelectorArea ( ref rect );

                //if ( enumDimto >= POSITEM_DIMENSIONS_INSIDE_ROWSELECTORS )
                if ( enumDimto >= PositionDimensions.InsideRowSelectors )
                {
                    //if ( enumDimFrom < POSITEM_DIMENSIONS_INSIDE_ROWSELECTORS )
                    if ( enumDimFrom < PositionDimensions.InsideRowSelectors )
                        //AdjustForRowSelector( rect );
                        this.AdjustForRowSelector( ref rect );

                    //if ( enumDimto >= POSITEM_DIMENSIONS_INSIDE_ROWBORDERS )
                    if ( enumDimto >= PositionDimensions.InsideRowBorders )
                    {
                        //if ( enumDimFrom < POSITEM_DIMENSIONS_INSIDE_ROWBORDERS )
                        if ( enumDimFrom < PositionDimensions.InsideRowBorders )
                            //AdjustForRowBorders( rect );
							// SSP 2/16/05
							// UltraGridColumn has the same method and does the same thing. Use that instead because
							// that has bug fixes too.
							//
                            //this.AdjustForRowBorders( ref rect );
							this.Column.AdjustForRowBorders( ref rect );

                        //if ( enumDimto >= POSITEM_DIMENSIONS_INSIDE_CELLSPACING )
                        if ( enumDimto >= PositionDimensions.InsideCellSpacing )
                        {
                            // The rest of the adjustements do not apply to groups
                            //
                            //ASSERT( !IsGroup(), "Col adjustments attemped for group in CPositionItem::AdjustDimensions" ); 
                            Debug.Assert( !this.IsGroup, "Col adjustments attemped for group in HeaderBase.AdjustDimensions" ); 
                            //if ( !IsGroup() )
                            if ( !this.IsGroup )
                            {
                                //if ( enumDimto >= POSITEM_DIMENSIONS_INSIDE_CELLSPACING )
                                if ( enumDimto >= PositionDimensions.InsideCellSpacing )
                                {
                                    //if ( enumDimFrom < POSITEM_DIMENSIONS_INSIDE_CELLSPACING )
                                    if ( enumDimFrom < PositionDimensions.InsideCellSpacing )
                                        //AdjustForCellSpacing( rect );
                                        this.AdjustForCellSpacing( ref rect );

                                    //if ( enumDimto >= POSITEM_DIMENSIONS_INSIDE_CELLBORDERS )
                                    if ( enumDimto >= PositionDimensions.InsideCellBorders )
                                    {
                                        //if ( enumDimFrom < POSITEM_DIMENSIONS_INSIDE_CELLBORDERS )
                                        if ( enumDimFrom < PositionDimensions.InsideCellBorders )
                                            //AdjustForCellBorders(rect );
                                            this.AdjustForCellBorders( ref rect );

                                        //if ( enumDimto >= POSITEM_DIMENSIONS_INSIDE_CELLPADDING )
                                        if ( enumDimto >= PositionDimensions.InsideCellPadding )
                                        {
                                            //if ( enumDimFrom < POSITEM_DIMENSIONS_INSIDE_CELLPADDING )
                                            if ( enumDimFrom < PositionDimensions.InsideCellPadding )
                                                //AdjustForCellPadding( rect );
                                                this.AdjustForCellPadding( ref rect );

                                            //if ( enumDimto >= POSITEM_DIMENSIONS_CELL_TEXT_AND_PICTURE )
                                            if ( enumDimto >= PositionDimensions.CellTextAndPicture )
                                            {
                                                //if ( enumDimFrom < POSITEM_DIMENSIONS_CELL_TEXT_AND_PICTURE )
                                                if ( enumDimFrom < PositionDimensions.CellTextAndPicture )
                                                {
                                                    //ASSERT( pResolvedCellAppearance, "Can't convert to a cell picture or text rect in CPositionItem::AdjustDimensions without a resolved cell appearance");
                                                    //ASSERT( prcPicture, "Can't convert to a cell picture or text rect in CPositionItem::AdjustDimensions without a picture rect ptr.");
                                                    //Debug.Assert( null != resolvedCellAppearance, "Can't convert to a cell picture or text rect in HeaderBase.AdjustDimensions without a resolved cell appearance");
                                                    //Debug.Assert( null != pictureRect, "Can't convert to a cell picture or text rect in HeaderBase.AdjustDimensions without a picture rect ptr.");
                                    
                                                    //if ( prcPicture && pResolvedCellAppearance )
                                                    if ( !pictureRect.IsEmpty  )
                                                    {
                                                        // Call AdjustForCellPicture which adjusts the rect into
                                                        // the area left for the text after the picture area has 
                                                        // been removed. It also returns the picture rect.
                                                        //
                                                        //AdjustForCellPicture( rect, *prcPicture, *pResolvedCellAppearance );
                                                        this.AdjustForCellPicture( ref rect, ref pictureRect, ref resolvedCellAppearance );
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        internal void GetDimensions(
            PositionDimensions enumDimensions,
            DimOriginBase enumOriginBase, 
            ref int origin, //return value
            ref int extent //return value
             )
        {
            this.GetDimensions(
                enumDimensions, 
                enumOriginBase, 
                ref origin,
                ref extent,
                null 
                );
        }

		internal void GetDimensions(
            PositionDimensions enumDimensions,
            DimOriginBase enumOriginBase, 
            ref int origin, //return value
            ref int extent, //return value
            ColScrollRegion colScrollRegion  )
        {
			// SSP 6/10/02
			// changed enumDimensions > 0 to enumDimensions >= 0.
			//
            Debug.Assert(enumDimensions >= 0 && enumDimensions <= Infragistics.Win.UltraWinGrid.PositionDimensions.InsideCellPadding, 
                "Invalid dimensions enumerator in HeaderBase.GetDimensions" );

            origin = 0;
            extent = 0;

			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
            //if ( this.Extent < 1) 
			int currentExtent = this.Extent;

			if ( currentExtent < 1 ) 
                return;

            Rectangle rect = new Rectangle(0, 0, 0, 0);

            // set the rect.left based on the enumOriginBase passed in
            //
            switch ( enumOriginBase )
            {
                case Infragistics.Win.UltraWinGrid.DimOriginBase.Normalized:
                    rect.X = 0;
                    break;
                case Infragistics.Win.UltraWinGrid.DimOriginBase.OnScreen:
                    Debug.Assert( null != colScrollRegion, "Col scroll region not supplied in HeaderBase.GetDimensions" );
                    if ( null != colScrollRegion )
                        rect.X = this.GetOnScreenOrigin( colScrollRegion, true );
                    break;
                case Infragistics.Win.UltraWinGrid.DimOriginBase.Absolute:
                    rect.X = this.OverallOrigin; 
                    break;
                case Infragistics.Win.UltraWinGrid.DimOriginBase.Relative:
                    Debug.Assert( null != colScrollRegion , "Col scroll region not supplied in HeaderBase.GetDimensions" );
					if ( null != colScrollRegion )
					{
						rect.X = this.OverallOrigin - colScrollRegion.Position;

						// SSP 6/16/03 - Fixed Headers
						// Add any fixed-headers related adjustments.
						// Note: IsHeaderFixed will return false if fixed headers is not enabled so there
						// is no need to check for it here.
						//
						// ----------------------------------------------------------------------------------
						if ( this.Band.IsHeaderFixed( colScrollRegion, this ) )
						{
							rect.X += this.Band.GetFixedHeaders_OriginDelta( colScrollRegion );
						}
						// ----------------------------------------------------------------------------------
					}
                    break;
                default:
                    Debug.Assert(false, "Invalid origin base enumerator in HeaderBase.GetDimensions" );
                    break;
            }

            // set the right based on the left
            //
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
            //rect.Width = this.Extent;
			rect.Width = currentExtent;
    
            // we aren't interested in the top or bottom so set them so the the height 
            // is arbitrarily large enough to not effect any adjustments we might make
            //
            rect.Y = 0;
            rect.Height = 1000 - rect.Y;

			// SSP 12/21/04 - BR00985
			// Call to AdjustDimensions below assumes the rect includes the pre row area.
			// So include the pre row area before calling it.
			//
			// ----------------------------------------------------------------------------
			if ( this.FirstItem )
			{
				int preRowAreaExtent = this.Band.PreRowAreaExtent;
				rect.X -= preRowAreaExtent;
				rect.Width += preRowAreaExtent;
			}
			// ----------------------------------------------------------------------------

            // Then adjust the rect based on the enumDimensions value passed in.
            //
            this.AdjustDimensions( 
                Infragistics.Win.UltraWinGrid.PositionDimensions.Outside,
                enumDimensions,
                ref rect
                );

            origin = rect.Left;    
            extent    =  rect.Width; 	
        }	
		internal void GetDimensions( VisibleRow visibleRow, PositionDimensions dimensions,
			DimOriginBase originBase, out Rectangle rect, ColScrollRegion colScrollRegion ,
			ref AppearanceData resolvedCellAppearance,
			ref Rectangle pictureRect )
		{		
			Debug.Assert( dimensions >= 0 && dimensions <= PositionDimensions.CellTextAndPicture,
				"Invalid dimensions enumerator in HeaderBase.GetDimensions" );

            rect = Rectangle.Empty;

			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.OverallExtent < 1 )
			int overallExtent = this.OverallExtent;

			if ( overallExtent < 1 )
				return;

			// set the rect.left based on the enumOriginBase passed in
			//
			switch ( originBase )
			{
				case DimOriginBase.Normalized:
					rect.X = 0;
					rect.Y  = 0;
					break;
				case DimOriginBase.OnScreen:
					Debug.Assert( null != colScrollRegion, "Col scroll region not supplied in HeaderBase.GetDimensions" );
					if ( null != colScrollRegion )
						rect.X = this.GetOnScreenOrigin( colScrollRegion, true );
					rect.Y  = visibleRow.GetTopOfRowInClient();
					break;
				case DimOriginBase.Absolute:
					rect.X = this.OverallOrigin;
					rect.Y  = visibleRow.GetTopOfRow();
					break;
				case DimOriginBase.Relative:
					Debug.Assert( null != colScrollRegion, "Col scroll region not supplied in HeaderBase.GetDimensions" );
					if ( null != colScrollRegion )
					{
						rect.X = this.OverallOrigin - colScrollRegion.Position;
					
						// SSP 6/16/03 - Fixed Headers
						// Add any fixed-headers related adjustments.
						// Note: IsHeaderFixed will return false if fixed headers is not enabled so there
						// is no need to check for it here.
						//
						// ----------------------------------------------------------------------------------
						if ( this.Band.IsHeaderFixed( colScrollRegion, this ) )
						{
							rect.X += this.Band.GetFixedHeaders_OriginDelta( colScrollRegion );
						}
						// ----------------------------------------------------------------------------------
					}
					rect.Y  = visibleRow.GetTopOfRow();
					break;

				default:
					Debug.Fail("Invalid origin base enumerator in HeaderBase.GetDimensions" );
					break;
			}

			// set the right based on the left
			//
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//rect.Width = this.OverallExtent;
			rect.Width = overallExtent;
    
			// set the bottom based on the top
			//
			rect.Height = visibleRow.Row.BaseHeight;

			// Then adjust the rect based on the enumDimensions value passed in.
			//
			this.AdjustDimensions(
				PositionDimensions.Outside, dimensions, ref rect, visibleRow.Row,
				ref resolvedCellAppearance, ref pictureRect );			
		}

		internal int GetOnScreenOrigin( ColScrollRegion csr, bool includeRowSelectors )
        {
			if ( this.Hidden )
				return 0;

			// JJD 1/23/02 - UWG971
			// Get resolved exclusive region
			//
			ColScrollRegion exclusive = this.ExclusiveColScrollRegionResolved;

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			bool isBandHeader = this is BandHeader;

			if ( exclusive != null )
			{ 
				if ( exclusive != csr )
					return -100000;
			}
			else
			{
				// JJD 1/23/02 - UWG971
				// Always let band headers thru
				//
				// MD 8/7/07 - 7.3 Performance
				// FxCop - Do not cast unnecessarily
				//if ( csr.IsExclusive && !( this is BandHeader ))
				if ( csr.IsExclusive && !isBandHeader )
					return -100000;
			}

			// MD 12/8/08 - 9.1 - Column Pinning Right
			// This code had to be refactored and updated to allow for right-fixed headers
			#region Old Code

			

			#endregion Old Code
			int overallOrigin = csr.HorizontalAdjustment;

			if ( !csr.IsExclusive )
				overallOrigin += this.Band.GetOrigin( BandOrigin.RowSelector );

			if ( !includeRowSelectors )
			{
				// Adjust the origin by the extent of the row selector if they
				// aren't included
				//
				overallOrigin += this.RowSelectorExtent;
			}

			if ( this.Band.IsHeaderFixed( csr, this ) )
			{
				if ( this.FixOnRightResolved )
					return this.GetRightFixedOnScreenOrigin( csr, overallOrigin );
				
				overallOrigin += this.Band.GetFixedHeaders_OriginDelta( csr );
			}

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( ( this is BandHeader ) )
			if ( isBandHeader )
			{
				// JJD 1/23/02 - UWG971
				// For band headers in exclusive regions get the origin from the
				// band after the prerow area
				//
				if ( csr.IsExclusive )
					overallOrigin += csr.GetBandOrigin( this.Band ) + this.Band.PreRowAreaExtent;
			}
			else
				overallOrigin += this.origin;

			return overallOrigin;
        }

		// MD 12/8/08 - 9.1 - Column Pinning Right
		#region GetRightFixedOnScreenOrigin

		private int GetRightFixedOnScreenOrigin( ColScrollRegion csr, int onScreenVirtualBandOrigin )
		{
			int extentOfRightFixedHeadersToLeft = 0;

			// The minimum position for the right-fixed header is the on-screen left edge of the band's row selector
			int rowSelectorOnScreenLeftEdge = onScreenVirtualBandOrigin + this.Band.GetFixedHeaders_OriginDelta( csr );
			int minOriginForFirstRightFixedHeader = rowSelectorOnScreenLeftEdge;

			HeadersCollection headers = this.Band.OrderedHeaders;
			for ( int i = headers.Count - 1; i >= 0; i-- )
			{
				HeaderBase header = headers[ i ];

				// Skip past all hidden headers and header after this one in the ordered headers collection.
				if ( header.Hidden || header != this )
					continue;

				// Once this header is encoutered in the collection, iterate through the rest of the right-fixed headers and determine
				// the extent of the right-fixed headers to the left of this one.
				for ( i--; i >= 0; i-- )
				{
					header = headers[ i ];

					if ( header.Hidden )
						continue;

					// A non right-fixed header has been encountered, so we should stop looping.
					if ( this.Band.IsHeaderFixed( csr, header ) == false ||
						header.FixOnRightResolved == false )
					{
						// The minOriginForFirstRightFixedHeader was intialized to the left edge of the row selector, but if other headers are 
						// before the first right-fixed header, this must be changed to the right edge of the row selectors, so add in the row 
						// selector extent.
						minOriginForFirstRightFixedHeader += this.Band.RowSelectorExtent;
						break;
					}

					extentOfRightFixedHeadersToLeft += header.Extent;
				}

				break;
			}

			// If there are any left fixed headers, the right fixed headers cannot overlap them
			HeaderBase lastLeftFixedHeader = this.Band.GetLastLeftFixedHeader( csr );
			if ( lastLeftFixedHeader != null )
				minOriginForFirstRightFixedHeader = lastLeftFixedHeader.GetOnScreenOrigin( csr, true ) + lastLeftFixedHeader.Extent;

			// Determine how much space is available in the col scroll region to push out right-fixed columns.
			int availableColScrollRegionSpace = csr.Origin + csr.Extent;

			// If this is the last col scroll region, it may have vertical scrollbar in any intersecting row scroll region.
			// If any row scroll regions intersecting with the col scroll region will display scroll bars, the scrollbar area 
			// is assumed to be occupied and the right-fixed columns cannot go into that area.
			if ( csr == csr.Collection.LastVisibleRegion )
			{
				foreach ( RowScrollRegion rsr in csr.Layout.RowScrollRegions )
				{
					if ( rsr.Hidden )
						continue;

					// If any row scroll region has a vertical scroll bar, remove that space from the col scroll region extent.
					if ( rsr.WillScrollbarBeShown() )
					{
						availableColScrollRegionSpace -= SystemInformation.VerticalScrollBarWidth;
						break;
					}
				}
			}

			HeaderBase lastVisibleHeader = headers.LastVisibleHeader;

			if ( lastVisibleHeader == null )
			{
				Debug.Fail( "I don't think we should be getting the on screen origin for a header is there are no visible headers." );
				return minOriginForFirstRightFixedHeader;
			}

			int unscrolledLastHeaderRightEdge = lastVisibleHeader.origin + lastVisibleHeader.Extent;
			int extentOfThisAndHeadersToRight = unscrolledLastHeaderRightEdge - this.origin;

			// Determine where the right edge of the right fixed headers should be. It should not extend past the edge of the 
			// col scroll region, but it should not be out that far if all headers can be displayed without scrolling.
			int rightEdgeOfRightFixedColumns = Math.Min( 
				availableColScrollRegionSpace, 
				onScreenVirtualBandOrigin + unscrolledLastHeaderRightEdge );

			// The right-fixed headers should be pushed out to the right side as far as possible while staying connected to the rest of 
			// the headers in the band. The headers will stop being pushed out once they hit the right edge of the col scrool region. However,
			// if the headers will be pushed off screen if the col scroll region is too small and the left edge of the first right-fixed header 
			// is touching the row selector and the col scroll region is sized smaller.
			return Math.Max(
				rightEdgeOfRightFixedColumns - extentOfThisAndHeadersToRight,
				minOriginForFirstRightFixedHeader + extentOfRightFixedHeadersToLeft );
		} 

		#endregion GetRightFixedOnScreenOrigin

		internal virtual int RowSelectorExtent
		{
			get
			{ 
				return this.FirstItem 
					? this.Band.RowSelectorExtent
					: 0;
			}
		}


        internal void AdjustDimensions( Infragistics.Win.UltraWinGrid.PositionDimensions enumDimFrom,
            Infragistics.Win.UltraWinGrid.PositionDimensions enumDimto,
            ref Rectangle rect			
            )		
        {
            AppearanceData tmpA = new AppearanceData();
            Rectangle tmpB = new Rectangle(0,0,0,0);
            this.AdjustDimensions(enumDimFrom,
                enumDimto,
                ref rect,
                null,
                ref tmpA,
                ref tmpB				
                );                
        }

		internal virtual void AdjustForPreRowSelectorArea( ref Rectangle rect )
        {			
            // If this is marked as the first position item then adjust for the 
            // pre-rowselector area
            //
            //if ( IsFirstItem() )
            if ( this.FirstItem )
            {
                //rect.left += GetBand().GetPreRowSelectorWidth();			
                int temp = this.Band.PreRowAreaExtent;
                rect.X += temp;
                rect.Width -= temp;
            }
        }

		internal virtual void AdjustForRowSelector( ref Rectangle rect )
        {		
            // If this is marked as the first position item then adjust for the 
            // row selector
            //			
            if ( this.FirstItem ) 
            {
                int temp = this.Band.RowSelectorExtent;
                rect.X += temp;
                rect.Width -= temp;
            }
        }

		// SSP 2/16/05
		// UltraGridColumn has the same method and does the same thing. Use that instead because
		// that has bug fixes too.
		//
		

		internal virtual void AdjustForCellSpacing( ref Rectangle rect )
        {
            int cellSpacing = this.Band.CellSpacingResolved;

            if ( cellSpacing < 1)                
                return;

            int borderThickness = this.Band.Layout.GetBorderThickness( this.Band.BorderStyleCellResolved );

            int maxSpacingW = rect.Width/2 - ( 1 + borderThickness );
            int maxSpacingH = rect.Height/2 - ( 1 + borderThickness );

            int spacingH = System.Math.Max( 0, System.Math.Min( cellSpacing, maxSpacingH ));
            int spacingW = System.Math.Max( 0, System.Math.Min( cellSpacing, maxSpacingW ));

            rect.Inflate( -spacingW, -spacingH );
        }

		internal virtual void AdjustForCellBorders( ref Rectangle rect )
        {			
            int borderThickness = this.Band.Layout.GetBorderThickness( this.Band.BorderStyleCellResolved );

            if ( borderThickness < 1 )
                return;

            // adjust the rect all around
            //
            rect.Inflate( -borderThickness, -borderThickness );
        }

		internal virtual void AdjustForCellPadding( ref Rectangle rect )
        {			
            int cellPadding = this.Band.CellPaddingResolved;

            if ( cellPadding < 1 )
                return;

            // adjust the rect all around
            //
            rect.Inflate( -cellPadding, -cellPadding );
        }
		internal virtual int OverallOrigin
        {
            get
            {
                if ( this.Hidden )
                    return 0;

				int overallOrigin = this.origin;

                if ( null == this.ExclusiveColScrollRegion )
                {
                    // if this is not a ExclusiveColScrollRegion then
                    // add the band's origin in
                    overallOrigin += this.Band.GetOrigin( BandOrigin.RowSelector );
                }

                return overallOrigin;
            }
        }		
		
		
		//internal int GetActualWidth( /* bool bIgnoreDraggingWidth = false */ )
		//{
		//	return this.GetActualWidth( false );
		//}
		//internal virtual int GetActualWidth( bool ignoreDraggingWidth )
		//{
		//	return 0;
		//}

		internal virtual bool AllowSizing
		{
			get
			{
				return false;
			}			
		}


		// SSP 5/31/02
		// Added code for summary rows feature.
		//
		#region AllowRowSummaries

		internal virtual bool AllowRowSummaries 
		{ 
			get
			{
				// By default return false. ColumnHeader will override this
				// and return appropriate value.
				//
				return false;
			}
		}

		#endregion // AllowRowSummaries


		internal virtual void OnClickSummaryRowsButton( HeaderUIElement headerElement )
		{
			Debug.Assert( false, "OnClickSummaryRowsButton called on the HeaderBase. It must be overridden by the derived class." );
		}

		// SP 3/21/02
		// Adde row filtering code
		//
		#region Row Filter Code

		// SSP 4/16/05 - NAS 5.2 Filter Row
		// Changed the name from AllowRowFiltering to HasFilterIcons.
		//
		//internal virtual bool AllowRowFiltering
		internal virtual bool HasFilterIcons
		{
			get
			{
				// By default it's false. The column header will override this
				// returning appropriate values.
				//
				return false;
			}
		}


		internal virtual void OnClickFilterDropDown( HeaderUIElement headerElem, bool shiftKeyDown )
		{
			Debug.Assert( false, "OnClickFilterDropDown called on the HeaderBase. It must be overridden by the derived class." );
		}

		#endregion //Row Filter Code


		internal virtual void GetResizeRange( ref int minWidth, ref int maxWidth) 
		{
			//NOTE: this function is overridden in the derived classes
			//There is no default implementation
		}
		internal virtual void Synchronize()
		{
			//NOTE: this function is overridden in the derived classes
			//There is no default implementation
		}

		internal virtual int GetRelativeColSyncPosition( UltraGridColumn column )
		{
			return -1;
		}

		internal virtual int GetTotalColSyncPositions ( ColScrollRegion exclusiveRegion )
		{
			return 0;
		}		

		internal virtual void InternalSetWidth ( int newWidth, bool autofit )
		{			
		}

		internal int OverallExtent
		{
			get
			{
				// SSP 2/25/03 - Row Layout Functionality
				//
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( null != this.Column && 
				//    null != this.Column.Band && 
				//    this.Column.Band.UseRowLayoutResolved )
				//{
				//    return this.Column.RowLayoutColumnInfo.CachedHeaderRect.Width;
				//}
				UltraGridColumn column = this.Column;

				if ( null != column &&
					null != column.Band &&
					column.Band.UseRowLayoutResolved )
				{
					return column.RowLayoutColumnInfo.CachedHeaderRect.Width;
				}

				if ( this.FirstItem )
				{
					// Adjust the actual data cell rect and the row's left
					//
					return this.Extent  + this.Band.PreRowAreaExtent;
				}
				else
					return this.Extent;
			}
		}

		internal virtual bool AllowSwapping
		{
			get
			{
				return false;
			}
		}

		internal void SetupSwapDropDown( ValueList swapDropDown )
		{	
			// Activate the valuelist's owner
			//
			((IValueList)swapDropDown).Activate( this );

			// Remove any items in the list		
			//
			//swapDropDown.DropDown.ResetContents();
			swapDropDown.ValueListItems.Clear();	
		}
		
		internal void ResetSwapButton()
		{
			UIElement headerElem = this.GetUIElement();

			if ( null != headerElem )
			{
				UIElement swapButtonElem = headerElem.GetDescendant( typeof ( SwapButtonUIElement ) );

				if ( null != swapButtonElem )
					swapButtonElem.Invalidate();
			}
		}

		internal void OnClickSwapButton( Rectangle itemBounds )
		{			
			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			if( null == grid )
				return;

			if( null == grid.SwapDropDown )
			{
				Debug.Fail( "grid.GetSwapDropDown() retuned null" );
				return;
			}

			// Exit editmode
			//
			if ( this.Band.Layout.ActiveCell != null && 
					this.Band.Layout.ActiveCell.IsInEditMode )
				this.Band.Layout.ActiveCell.ExitEditMode( );


			if ( ((IValueList)grid.SwapDropDown).IsDroppedDown )
			{
				//this.ResetSwapButton();
				((IValueList)grid.SwapDropDown).CloseUp();
			}

			else
			{        
				Rectangle workRect = itemBounds;

				// if this is a group, set the width to zero
				// so that we don't make the drop down as wide as the 
				// group
				// 
				if ( this.IsGroup )
				{
					workRect.X = workRect.Right;
					workRect.Width = 0;					
				}

				// Remove all items from the list and activate the 
				// dropdown's owner
				//
				this.SetupSwapDropDown( grid.SwapDropDown );					
        
				grid.SwapDropDown.BumpContentsVersion();

				// Build the List of Items to Add to the SwapList.
				//
				this.CreateSwapList( grid.SwapDropDown );				
 
				// move the rect up so that we don't get a double border
				//
				workRect.Offset( 0, -1 );

				workRect = grid.RectangleToScreen( workRect );
				
				// Drop the list down
				//
				((IValueList)grid.SwapDropDown).DropDown( workRect, 0, null );
			}

		}

		internal virtual void CreateSwapList( ValueList swapDropDown )
		{
			//This will be overriden by base classes
		}

		internal virtual void Swap( HeaderBase swapItem )
		{
			//derived class will override this
		}

		internal virtual void AddColsToSyncColsList ( SynchedBandAbove pSynchedBandAbove )
		{
			//derived classes will override this
			Debug.Assert(false, "Accessing base when should be accessing derived class" );
		}

		internal virtual UltraGridColumn GetColAtRelativeSyncPosition( int nRelativeSyncPosition, 
			int nLevel,
			int nTotalSyncPositions,
			ColScrollRegion exclusiveRegion )
		{
			return null;
		}
		internal virtual SortIndicator SortIndicator
		{
			get
			{
				return SortIndicator.Disabled;
			}
		}

		internal bool SetDragBitmap( DragEffect dragEffect, bool multipleItems )
		{
			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			if( null == grid )
				return false;

			// Use the element to get the dimensions
			//
			UIElement element = null;

			// MD 8/3/07 - 7.3 Performance
			// Cached column - Prevent calling expensive getters multiple times
			UltraGridColumn headerColumn = this.Column;

			// If we are currently dragging a GroupByButton from the group by box
			//
			if ( null != dragEffect.DragStrategy && ( dragEffect.DragStrategy is DragStrategyColumn ) &&
				((DragStrategyColumn)dragEffect.DragStrategy).IsDraggingGroupByButton &&
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//this.Column.Layout.HasGroupByBox )
				headerColumn.Layout.HasGroupByBox )
			{	
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//element = this.Column.Layout.GetUIElement( false );
				//element = element.GetDescendant( typeof( GroupByBoxUIElement ), this.Column.Layout.GroupByBox );
				element = headerColumn.Layout.GetUIElement( false );
				element = element.GetDescendant( typeof( GroupByBoxUIElement ), headerColumn.Layout.GroupByBox );

				if ( null != element )
					element = element.GetDescendant( typeof( GroupByButtonUIElement ), this );				
			}

			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// If a column from a column chooser is being dragged then get the bitmap from the 
			// column chooser instead.
			// 
			if ( null == element )
			{
				UltraGridColumnChooser cc = grid.GetDraggingColumnChooser( );
				if ( null != cc )
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//element = cc.GetHeaderUIElement( this.Column );
					element = cc.GetHeaderUIElement( headerColumn );
				}
			}

			// If not dragging a group by button, then use header's ui element
			if ( null == element )
				 element = this.GetUIElement( );

			int width  = this.GetActualWidth( );
			int height = this.Height;


			// If we don't get a UIElement, try to find another
			// UIElement that we can use.
			//
			if ( null == element &&
				multipleItems &&
				null != grid &&
				null != grid.Selected  && 
				null != grid.Selected.Columns &&
				grid.Selected.Columns.Count > 0 )
			{
				SelectedColsCollection  columns = grid.Selected.Columns;
				UltraGridColumn				    column  = null;
				Infragistics.Win.UltraWinGrid.ColumnHeader            columnHeader = null;

				// loop through the columns in the selectedcols collection
				for ( int i = 0; i < columns.Count && null == element; i++ )
				{
					column = null;

					Infragistics.Win.UltraWinGrid.ColumnHeader columnHeaderTemp = columns.GetItem( i ) as ColumnHeader;

					if ( null != columnHeaderTemp )
						column = columnHeaderTemp.Column;

					if ( null != column )
					{
						columnHeader = null;
							
						// get the column's header.
						columnHeader = column.Header;

						if ( null != columnHeader )
						{								
							// try to get the header's UIElement.
							element = columnHeader.GetUIElement( );
						}

					}
				}
			}

			Debug.Assert( null != element, "No associated UIElement found" );

			if ( null == element )
				return false;
			
			width  = element.Rect.Width;
			height = element.Rect.Height;
			

			// Header is in relative coordinates (0,0 upper left of header)
			//
			Rectangle headerRect = new Rectangle( 0, 0, width, height );
			
			// invalidRect is in grid coordinates
			//
			Rectangle invalidRect = element.Rect;
			invalidRect.Width  = width;
			invalidRect.Height = height;



			Bitmap bitmap = null;
			Region  windowRegion  = null;

			// Create a bitmap to draw the header ui element on
			//			
			Graphics gr = this.Band.Layout.Grid.CreateGraphics();

			bitmap = new Bitmap( width, height, gr );
	

			Graphics bitmapGraphics = Graphics.FromImage( bitmap );


			// Offset hdc so it syncs with grid coordinates, currently, it is only
			// as big as the element and element.Draw() calls will assume the device context
			// is in grid coordinates
			//
			System.Drawing.Drawing2D.Matrix matrix = null;
		
			matrix = new System.Drawing.Drawing2D.Matrix( 1, 0, 0, 1, -invalidRect.X, -invalidRect.Y );
			bitmapGraphics.Transform = matrix;			
	
			// This call was added to avoid a potential problem where
			// a partial uielement-tree draw like this could incorrectly
			// walk up the parent chain and reclip on parents that were not drawn!
			// I'm leaving it in because it may be needed in the future
			//
			//pElement->SetIgnoreParentClipping(true);

			// AS 12/17/02 UWG1831
			// Use the new overload
			//
			//element.Draw( bitmapGraphics, invalidRect, false, false );
			// SSP 7/16/03 - Fixed headers.
			// Clear the self clip rect of the header before drawing it and then
			// reset it back. In fixed headers mode, non-fixed headers partially 
			// scrolled under fixed headers will not draw properly.
			//
			HeaderUIElement headerElem = element as HeaderUIElement;
			Rectangle headerSelfClipRect = Rectangle.Empty;
			if ( null != headerElem )
			{
				headerSelfClipRect = headerElem.InternalGetClipSelfRegion( );
				headerElem.InternalSetClipSelfRegion( Rectangle.Empty );
			}

			try
			{
				element.Draw( bitmapGraphics, invalidRect, false, AlphaBlendMode.Disabled );
			}
			finally
			{
				if ( null != headerElem )
					headerElem.InternalSetClipSelfRegion( headerSelfClipRect );
			}

			// See comment, above											
			//pElement->SetIgnoreParentClipping(false);

			if ( null != bitmapGraphics )			
				bitmapGraphics.Dispose( );

			bitmapGraphics = null;

			if ( null != matrix )
				matrix.Dispose( );

			matrix = null;				
			

			if ( multipleItems && null != bitmap )
			{
				// these offsets are used for creating stacked headers bitmap
				const int DX = 2;
				const int DY = 2;

				// adjust the width and the height for stacked headers
				width  += 2*DX;
				height += 2*DY;

				// Draw the multiple stacked headers						

				Bitmap bitmap2 = new Bitmap( width, height, gr );

				
				Graphics tmpGraphics = Graphics.FromImage( bitmap2 );

				tmpGraphics.DrawImageUnscaled( bitmap, headerRect );
				
				windowRegion = new Region( headerRect );


				headerRect.Offset( DX, DY );
				tmpGraphics.DrawImageUnscaled( bitmap, headerRect );
				windowRegion.Union( headerRect );

				headerRect.Offset( DX, DY );
				tmpGraphics.DrawImageUnscaled( bitmap, headerRect );
				windowRegion.Union( headerRect );

				tmpGraphics.Dispose( );
				tmpGraphics = null;

				// use the newly created bitmap and dispose the old one
				bitmap.Dispose( );
				bitmap = bitmap2; 
			}
			
			if ( null != gr )
				gr.Dispose();
			gr = null;

			// SSP 6/27/05 - NAS 5.3 Column Chooser
			// Width and height params have no meaning since they are always the same size 
			// as the bitmap.
			// 
			// --------------------------------------------------------------------------------
			//dragEffect.SetBitmap( bitmap, width, height, windowRegion );

			Point dragWindowOffset = null != element 
				? GridUtils.GetDragIndicatorOffset( new UIElement[] { element } )
				: new Point( - width / 2, - height );

			dragEffect.SetBitmap( bitmap, windowRegion, dragWindowOffset );
			// --------------------------------------------------------------------------------

			return true;
		}
		

		/// <summary>
		/// Serialize the header properties
		/// </summary>
		protected void GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";
			
			if ( this.ShouldSerializeAppearance() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AppearanceHolder", this.appearanceHolder );
				//info.AddValue("AppearanceHolder", this.appearanceHolder );
			}

			if ( this.ShouldSerializeCaption() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Caption", this.Caption );
				//info.AddValue("Caption", this.Caption );
			}

			if ( this.ShouldSerializeEnabled() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Enabled", this.Enabled  );
				//info.AddValue("Enabled", this.Enabled );
			}

			if ( this.ShouldSerializeExclusiveColScrollRegion() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ExclusiveColScrollRegion", this.ExclusiveColScrollRegion );
				//info.AddValue("ExclusiveColScrollRegion", this.ExclusiveColScrollRegion );
			}

			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			// JJD 10/16/01
			// Serialize the visible position
			//
			if ( this.ShouldSerializeVisiblePosition() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "VisiblePosition", this.VisiblePosition );
				//info.AddValue("VisiblePosition", this.VisiblePosition );
			}

			// SSP 7/18/03 - Fixed headers
			//
			// ----------------------------------------------------------------------------------
			if ( this.ShouldSerializeFixed( ) )
			{
				Utils.SerializeProperty( info, "Fixed", this.isHeaderFixed );
			}

			if ( this.ShouldSerializeFixedHeaderIndicator( ) )
			{
				Utils.SerializeProperty( info, "FixedHeaderIndicator", this.fixedHeaderIndicator );
			}
			// ----------------------------------------------------------------------------------

			// SSP 7/27/05 - Header, Row, Summary Tooltips
			// 
			if ( this.ShouldSerializeToolTipText( ) )
			{
				Utils.SerializeProperty( info, "ToolTipText", this.toolTipText );
			}

			// MD 5/5/08 - 8.2 - Rotated Column Headers
			if ( this.ShouldSerializeTextOrientation() )
				Utils.SerializeProperty( info, "TextOrientation", this.textOrientation );

			// MD 12/8/08 - 9.1 - Column Pinning Right
			if ( this.ShouldSerializeFixOnRight() )
				Utils.SerializeProperty( info, "FixOnRight", this.fixOnRight );
		}

		/// <summary>
		/// Called by the constructor used for de-serialization
		/// </summary>        
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected void DeserializeHelper(SerializationInfo info, StreamingContext context)
		{
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "AppearanceHolder":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.appearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						//this.appearanceHolder = (Infragistics.Win.AppearanceHolder)entry.Value;
						break;

					case "Caption":
					{
						//this.Caption = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Caption = (string)Utils.DeserializeProperty( entry, typeof(string), this.caption );
						//this.Caption = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;
					}

					case "Enabled":
					{
						//this.Enabled = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Enabled = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.Enabled );
						//this.Enabled = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
					}

					case "ExclusiveColScrollRegion":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.ExclusiveColScrollRegion = (ColScrollRegion)Utils.DeserializeProperty( entry, typeof(ColScrollRegion), null );
						//this.ExclusiveColScrollRegion = (Infragistics.Win.UltraWinGrid.ColScrollRegion)entry.Value;
						break;
					}

					case "Tag":
						// AS 8/15/02
						// Use our tag deserializer.
						//
						//this.tagValue = entry.Value;
						this.DeserializeTag(entry);
						break;
					
					// JJD 10/16/01
					// De-serialize the visible position
					//
					case "VisiblePosition":
						//this.visiblePos = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.visiblePos = (int)Utils.DeserializeProperty( entry, typeof(int), this.visiblePos );
						//this.visiblePos = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					// SSP 7/18/03 - Fixed headers
					//
					case "FixedHeaderIndicator":
						this.fixedHeaderIndicator = (Infragistics.Win.UltraWinGrid.FixedHeaderIndicator)Utils.DeserializeProperty( entry, typeof( Infragistics.Win.UltraWinGrid.FixedHeaderIndicator ), this.fixedHeaderIndicator );
						break;
					// SSP 7/27/05 - Header, Row, Summary Tooltips
					// 
					case "ToolTipText":
						this.toolTipText = (string)Utils.DeserializeProperty( entry, typeof( string ), this.toolTipText );
						break;
					case "Fixed":
						this.isHeaderFixed = (bool)Utils.DeserializeProperty( entry, typeof( bool ), this.isHeaderFixed );
                        break;

					// MD 5/5/08 - 8.2 - Rotated Column Headers
					case "TextOrientation":
						this.textOrientation = (TextOrientationInfo)Utils.DeserializeProperty( entry, typeof( TextOrientationInfo ), this.textOrientation );
						break;

					// MD 12/8/08 - 9.1 - Column Pinning Right
					case "FixOnRight":
						this.fixOnRight = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), this.fixOnRight );
						break;
				}
			}
		}

		//returns a collection of headers between this header and the passed-in pivot header
		internal HeadersCollection GetHeadersForSelection( bool select, HeaderBase pivotHeader )
		{
			HeadersCollection headers = new HeadersCollection(10);

			//return empty collection if pivot is null
			if ( pivotHeader == null )
				return headers;

			int position;
			int positionPivotItem;

			// JJD 1/11/02
			// For columns use VisiblePositionWithinBand property 
			//
			if ( pivotHeader is ColumnHeader )
			{
				if ( !(this is ColumnHeader) )
				{
					Debug.Fail("Header type mismatch in GetHeadersForSelection"); 
					return headers;
				}

				position = ((ColumnHeader)this).VisiblePositionWithinBand;
				positionPivotItem = ((ColumnHeader)pivotHeader).VisiblePositionWithinBand;
			}
            // MRS - NAS 9.1 - Groups in RowLayout
            else if (pivotHeader is GroupHeader)
            {
                if (!(this is GroupHeader))
                {
                    Debug.Fail("Header type mismatch in GetHeadersForSelection");
                    return headers;
                }

                position = ((GroupHeader)this).VisiblePositionWithinBand;
                positionPivotItem = ((GroupHeader)pivotHeader).VisiblePositionWithinBand;
            }
			else
			{
				position = this.VisiblePosition;
				positionPivotItem = pivotHeader.VisiblePosition;
			}


			// If item is selected and we are toggling, there is nothing to do here. 
			// We simply want to add the initial selection, which is outside the range 
			// of position to positionPivotItem.
			if ( select )
			{
				// Select the range between position and positionPivotItem. If positions 
				// are the same, we only want to select the pivot item below. 
				if ( position != positionPivotItem )
				{
					// Since both position and positionPivotItem are NOT the same, the firstItem
					// will always be smaller than the lastItem -- making it easier to loop through
					
					int firstItem = Math.Min( positionPivotItem, position );
					int lastItem = Math.Max( positionPivotItem, position );

					HeaderBase header = null;

					for ( int index = firstItem; index <= lastItem; index++ )
					{
						//check if the pivot is a column header
						if ( pivotHeader.GetType() == typeof( ColumnHeader ) )
						{
							UltraGridColumn column = this.Band.GetColumnAtOverallPosition( index );

							if ( column != null )
								header = column.Header;
						}
						// the header is a group, simply get header by indexing 
						// into OrderedGroupHeaders
						else
							header = this.Band.OrderedGroupHeaders[index];

						// before adding to collection, make sure header is not hidden and 
						// both are part of the same ExclusiveColScrollRegion
						//RobA UWG585 11/6/01 check for disabled
						//
						if ( header != null && !header.Hidden && !header.IsDisabled &&
							//RobA UWG586 11/7/01 check ActivationResolved
							//RobA UWG586 11/9/01 we decided to not check this here
							 //header.ActivationResolved != Activation.Disabled &&
							 header.ExclusiveColScrollRegion == this.ExclusiveColScrollRegion )
							headers.InternalAdd( header );
					}
				}

				//this header and pivot are the same -- add either one to collection
				else
					headers.InternalAdd( pivotHeader );
			}

			//return collection containing at least one header
			return headers;
		}

		Control IValueListOwner.Control
		{
			get
			{
				return this.Band.Layout.Grid;				
			}
		}

		// SSP 4/26/06 - App Styling
		// Added UltraControl to the IValueListOwner.
		// 
		/// <summary>
		/// Returns the owner's IUltraControl.
		/// </summary>
		IUltraControl IValueListOwner.UltraControl
		{ 
			get
			{
				UltraGridLayout layout = this.Layout;
				return null != layout ? layout.Grid : null;
			}
		}

		Control IValueListOwner.EditControl
		{
			get
			{
				return null;
			}
		}

		System.Windows.Forms.ImageList IValueListOwner.ImageList 
		{ 
			get
			{
				return null;
			}
		}

		ValueListsCollection IValueListOwner.ValueLists
		{
			get
			{
				return null;
			}
		}


		/// <summary>
		/// Called when the list portion receives focus.
		/// </summary>
		void IValueListOwner.OnListGotFocus()
		{
			//	No implementation
		}

		void IValueListOwner.ResolveValueListAppearance( ref AppearanceData appData, AppearancePropFlags requestedProps )
		{
			// SSP 8/11/03 UWG2208
			// Don't resolve the appearance of the header. Instead resolve the layout appearance like
			// we do with the filter drop down. Commented out the original code and copied the code
			// from the ResolveValueListAppearance of the filter value list owner off the ColumnHeader.
			// 
			// ----------------------------------------------------------------------------------------
			//this.ResolveAppearance( ref appData, requestedProps );
			// AS 4/7/06 ImageBackgroundDisabled
			//requestedProps &= ~( AppearancePropFlags.Image | AppearancePropFlags.ImageBackground );
			requestedProps &= ~GridUtils.ImageProperties;

			// MD 6/5/06 - BR13245
			// The don't resolve the bach hatch or back gradient styles.
			// This creates adverse effects when a selected item is being displayed
			requestedProps &= ~( AppearancePropFlags.BackHatchStyle | AppearancePropFlags.BackGradientStyle );

			ResolveAppearanceContext rc = new ResolveAppearanceContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridLayout ), requestedProps );

			// Resolve the colors first because otherwise we will end up picking up the 
			// layout's appearance which by default has gray back color which is not suitable
			// for the value list.
			//
			this.Band.Layout.ResolveColors( ref appData, ref rc, SystemColors.Window, SystemColors.WindowFrame, SystemColors.WindowText, false );
			
			this.Band.Layout.ResolveAppearance( ref appData, ref rc );
			// ----------------------------------------------------------------------------------------
		}

		Size IValueListOwner.SizeOfImages
		{
			get
			{
				return new Size(0,0);
			}
		}

		/// <summary>
		/// True if this is a drop down list type where
		/// there is no edit control
		/// </summary>
		bool IValueListOwner.IsDropDownList
		{ 
			get
			{
				return true;
			}
		}

		void IValueListOwner.OnCloseUp()
		{		
			// Need to give the grid focus because for some reason after you drop 
			// the drop down again without giving the grid focus, the drop down 
			// gets focus
			//
			UltraGrid grid = ((IValueListOwner)this).Control as UltraGrid;

			if ( grid != null )
			{
				// AS - 1/14/02
				// We need to assert putting focus on the control in order for this to work
				// when the assembly is local but the executable is running under internet
				// type restricted security access. However, to prevent a security hole, we
				// will only do so on controls that have our public key token.
				//
				//grid.Focus();
				try
				{
					if ( DisposableObject.HasSamePublicKey( grid.GetType() ))
					{
						System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

						perm.Assert();
					}

					grid.Focus();
				}
				catch {}
			}

			this.InvalidateItem();
		}

		void IValueListOwner.OnSelectedItemChanged()
		{
			//Don't do anything
		}

		void IValueListOwner.OnSelectionChangeCommitted()
		{
			UltraGrid grid = ((IValueListOwner)this).Control as UltraGrid;

			if ( grid == null )
				return;

			int index = ((IValueList)grid.SwapDropDown).SelectedItemIndex;

			object swapItem = ((IValueList)grid.SwapDropDown).GetValue(index);

			// SSP 8/16/01 
			// When the control get's created, this event may get fired
			// and the sected item may be null. So no assert, just return
			//
			//Debug.Assert( null != swapItem, "GetSelectedItem() returned null");
			
			// The pointer comes back as void*. It was
			// inserted as a Column* or Group*. Extract
			if( null != swapItem )
			{
				//this.ResetSwapButton();

				// SSP 7/25/03 - Fixed headers
				//
				// --------------------------------------------------------------------------
				// SSP 4/21/05 BR03376
				//
				ColScrollRegion activeCSR = this.Band.Layout.ActiveColScrollRegion;

				if ( UltraGridBand.SWAP_DROPDOWN_ITEM_FIX_HEADER == swapItem )
				{
					// SSP 4/21/05 BR03376
					// We need to fire Before/After ColPosChanged notifications. To do that 
					// use the same code that we use for the fix header button.
					// 
					//this.Fixed = true;
					if ( ! this.FixedResolved )
						this.OnFixedHeaderIndicatorClicked( activeCSR );

					return;
				}
				else if ( UltraGridBand.SWAP_DROPDOWN_ITEM_UNFIX_HEADER == swapItem )
				{
					// SSP 4/21/05 BR03376
					// We need to fire Before/After ColPosChanged notifications. To do that 
					// use the same code that we use for the fix header button.
					//
					//this.Fixed = false;
					if ( this.FixedResolved )
						this.OnFixedHeaderIndicatorClicked( activeCSR );

					if ( null != activeCSR )
						activeCSR.ScrollHeaderIntoView( this, false );
					return;
				}
					// SSP 7/30/03 UWG2543
					// Added an Unfix All option to the swap drop down.
					//
				else if ( UltraGridBand.SWAP_DROPDOWN_ITEM_UNFIX_ALL_HEADERS == swapItem )
				{
					if ( null != this.Band )
						this.Band.UnfixAllHeaders( );
				}
				// --------------------------------------------------------------------------

				if( this.IsGroup )
				{
					// SSP 7/25/03 - Fixed headers
					// Make sure the swapItem is an UltraGridGroup before type casting.
					//
					//UltraGridGroup groupSwapItem = (UltraGridGroup)swapItem;
					//this.Swap( groupSwapItem.Header );
					UltraGridGroup groupSwapItem = swapItem as UltraGridGroup;
					if ( null != groupSwapItem )
						this.Swap( groupSwapItem.Header );						
				}
				else
				{
					// SSP 7/25/03 - Fixed headers
					// Make sure the swapItem is an UltraGridGrou before type casting.
					//
					//UltraGridColumn colSwapItem = (UltraGridColumn)swapItem;
					//this.Swap( colSwapItem.Header );					
					UltraGridColumn colSwapItem = swapItem as UltraGridColumn;
					if ( null != colSwapItem )
						this.Swap( colSwapItem.Header );
				}
			}
			
			//Deactivate 
			//
			((IValueList)grid.SwapDropDown).DeActivate( this );					
		}

		//	BF 3/20/06	NAS2006 Vol2 - ValueListDropDown
		#region IValueListOwner.ScrollBarLook
		/// <summary>
		/// Returns the ScrollBarLook instance which defines the appearance of the dropdown's scrollbar.
		/// </summary>
		Infragistics.Win.UltraWinScrollBar.ScrollBarLook IValueListOwner.ScrollBarLook
		{
			get
			{ 
				UltraGrid grid = ((IValueListOwner)this).Control as UltraGrid;
				UltraGridLayout layout = grid != null ? grid.DisplayLayout : null;
				return layout != null ? layout.ScrollBarLook : null;
			}
		}
		#endregion IValueListOwner.ScrollBarLook

		// AS 8/17/06 NA 2006 Vol 3 - Office 2007 L&F
		#region IValueListOwner.ScrollBarViewStyle
		Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle IValueListOwner.ScrollBarViewStyle
		{
			get
			{
				return Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.Default;
			}
		}
		#endregion IValueListOwner.ScrollBarViewStyle

        // MRS - NA v6.3 - Office2007 - DisplayStyle
        #region IValueListOwner.DisplayStyle
        /// <summary>
        /// Returns the preferred displaystyle for this editor.
        /// </summary>
        EmbeddableElementDisplayStyle IValueListOwner.DisplayStyle
        {
            get
            {
                return EmbeddableElementDisplayStyle.Default;
            }
        }
        #endregion IValueListOwner.DisplayStyle

        #region Row Layout Code

        // SSP 2/14/03 - Row Layout Functionality
		// Added below code.
		//

		#region MinimumSize

		internal Size MinimumSize
		{
			get
			{
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//Size size = this.Column.RowLayoutColumnInfo.MinimumLabelSize;
                // MRS - NAS 9.1 - Groups in RowLayout
				//UltraGridColumn column = this.Column;
                IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = this.RowLayoutColumnInfoProvider;

                // MRS - NAS 9.1 - Groups in RowLayout
				//Size size = column.RowLayoutColumnInfo.MinimumLabelSize;
                Size size = iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.MinimumLabelSize;

				if ( this.Band.AreColumnHeadersInSeparateLayoutArea )
				{
					if ( this.Band.CardView )
					{
						if ( size.Width <= 0 )
						{
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//size.Width = this.Column.MinWidth;
                            // MRS - NAS 9.1 - Groups in RowLayout
							//size.Width = column.MinWidth;
                            size.Width = iProvideRowLayoutColumnInfo.MinWidth;
						}

						// In card view when the headers are separate from the cells, use the cell's
						// preferred height.
						//
						// SSP 6/30/03 UWG2375
						// Changed it so that we will use the bigger of the minimum cell height
						// and the minimum label height.
						//
						//size.Height = this.Column.MinimumSize.Height;
						// SSP 8/16/05 BR05387
						// Only do so if cells are actually visible.
						// 
						//size.Height = Math.Max( size.Height, this.Column.MinimumSize.Height );
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( LabelPosition.LabelOnly != this.Column.RowLayoutColumnInfo.LabelPositionResolved )
						//    size.Height = Math.Max( size.Height, this.Column.MinimumSize.Height );
                        // MRS - NAS 9.1 - Groups in RowLayout
                        //if ( LabelPosition.LabelOnly != column.RowLayoutColumnInfo.LabelPositionResolved )
                        //    size.Height = Math.Max( size.Height, column.MinimumSize.Height );
                        if (LabelPosition.LabelOnly != iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.LabelPositionResolved)
                            size.Height = Math.Max(size.Height, iProvideRowLayoutColumnInfo.MinimumSize.Height);
					}
					else
					{
						// In the regular view when the headers are separate from the cells, use the
						// cell's width.
						//
						// SSP 6/30/03 UWG2375
						// Changed it so that we will use the bigger of the minimum cell width
						// and the minimum label width.
						//
						//size.Width = this.Column.MinimumSize.Width;
						// SSP 8/16/05 BR05387
						// Only do so if cells are actually visible.
						// 
						//size.Width = Math.Max( size.Width, this.Column.MinimumSize.Width );
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( LabelPosition.LabelOnly != this.Column.RowLayoutColumnInfo.LabelPositionResolved )
						//    size.Width = Math.Max( size.Width, this.Column.MinimumSize.Width );
                        // MRS - NAS 9.1 - Groups in RowLayout
                        //if ( LabelPosition.LabelOnly != column.RowLayoutColumnInfo.LabelPositionResolved )
                        //    size.Width = Math.Max( size.Width, column.MinimumSize.Width );
                        if (LabelPosition.LabelOnly != iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.LabelPositionResolved)
                            size.Width = Math.Max(size.Width, iProvideRowLayoutColumnInfo.MinimumSize.Width);

						if ( size.Height <= 0 )
							size.Height = this.Height;
					}
				}
				else
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//Size cardLabelSize = this.Column.CardLabelSize;
                    // MRS - NAS 9.1 - Groups in RowLayout
					//Size cardLabelSize = column.CardLabelSize;
                    Size preferredHeaderSize = iProvideRowLayoutColumnInfo.MinimumHeaderSize;

					// If the headers are laid out in rows with cells, then apply the same cell
					// spacing as cells to the headers.
					//
					int cellSpacing = this.Band.CellSpacingResolved;
					
					if ( this.Band.CardView )
					{
						if ( size.Height <= 0 )
							size.Height = 2 * cellSpacing + preferredHeaderSize.Height;

						if ( size.Width <= 0 )
						{
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//size.Width = 2 * cellSpacing + this.Column.MinimumSize.Width;
                            // MRS - NAS 9.1 - Groups in RowLayout
							//size.Width = 2 * cellSpacing + column.MinimumSize.Width;
                            size.Width = 2 * cellSpacing + iProvideRowLayoutColumnInfo.MinimumSize.Width;
						}
					}
					else
					{
						if ( size.Height <= 0 )
							size.Height = 2 * cellSpacing + preferredHeaderSize.Height;

						if ( size.Width <= 0 )
						{
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//size.Width = 2 * cellSpacing + this.Column.MinimumSize.Width;
                            // MRS - NAS 9.1 - Groups in RowLayout
							//size.Width = 2 * cellSpacing + column.MinimumSize.Width;
                            size.Width = 2 * cellSpacing + iProvideRowLayoutColumnInfo.MinimumSize.Width;
						}
					}
				}

				return size;
			}
		}

		#endregion // MinimumSize

		#region MaximumSize

		// SSP 10/26/04 UWG3729
		// Restrict the user from resizing layout items greater than the max width. Added 
		// MaximumSize property.
		//
		internal Size MaximumSize
		{
			get
			{
				Size size = new Size( 0, 0 );

				// MD 8/3/07 - 7.3 Performance
				// Cached column - Prevent calling expensive getters multiple times
				UltraGridColumn column = this.Column;

				if ( this.Band.AreColumnHeadersInSeparateLayoutArea )
				{
					if ( ! this.Band.CardView )			
					{
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//size.Width = this.Column.MaxWidth;					
						size.Width = column.MaxWidth;
					}
				}
				else
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//LabelPosition headerPos = this.Column.RowLayoutColumnInfo.LabelPosition;
					//if ( LabelPosition.Left == headerPos || LabelPosition.Right == headerPos )
					//    size.Width = this.Column.MaxWidth;
					LabelPosition headerPos = column.RowLayoutColumnInfo.LabelPosition;

					if ( LabelPosition.Left == headerPos || LabelPosition.Right == headerPos )
						size.Width = column.MaxWidth;
				}

				return size;
			}
		}

		#endregion // MaximumSize

		#region PreferredSize

        // MRS - NAS 9.1 - Groups in RowLayout
		//internal Size PreferredSize
        internal virtual Size PreferredSize
		{
			get
			{
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//Size size = this.Column.RowLayoutColumnInfo.PreferredLabelSize;
				//
				//if ( this.Column.Band.AreColumnHeadersInSeparateLayoutArea )
                // MRS - NAS 9.1 - Groups in RowLayout
				//UltraGridColumn column = this.Column;
                IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = this.RowLayoutColumnInfoProvider;                

                // MRS - NAS 9.1 - Groups in RowLayout
				//Size size = column.RowLayoutColumnInfo.PreferredLabelSize;
                Size size = iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.PreferredLabelSize;
                                
                // MRS - NAS 9.1 - Groups in RowLayout
				//if ( column.Band.AreColumnHeadersInSeparateLayoutArea )
                if (this.Band.AreColumnHeadersInSeparateLayoutArea)
				{
					// When headers are going to be displayed in separate area, we need to make sure
					// that the header layout matches the row layout. To do that we have to make sure
					// that widths of the cells and the headers are the same in regular view and 
					// heights of the cells and the headers are the same in card view.
					//

					if ( this.Band.CardView )
					{
						if ( size.Width <= 0 )
						{
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//size.Width = this.Column.CardLabelSize.Width;
                            // MRS - NAS 9.1 - Groups in RowLayout
							//size.Width = column.CardLabelSize.Width;
                            size.Width = iProvideRowLayoutColumnInfo.PreferredHeaderSize.Width;
						}

						// In card view when the headers are separate from the cells, use the cell's
						// preferred height.
						//
						// SSP 6/30/03 UWG2375
						// Changed it so that we will use the bigger of the preferred cell height
						// and the preferred label height.
						//
						//size.Height = this.Column.PreferredSize.Height;
						// SSP 8/16/05 BR05387
						// Only do so if cells are actually visible.
						// 
						//size.Height = Math.Max( size.Height, this.Column.PreferredSize.Height );
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( LabelPosition.LabelOnly != this.Column.RowLayoutColumnInfo.LabelPositionResolved )
						//    size.Height = Math.Max( size.Height, this.Column.PreferredSize.Height );
                        // MRS - NAS 9.1 - Groups in RowLayout
                        //if ( LabelPosition.LabelOnly != column.RowLayoutColumnInfo.LabelPositionResolved )
                        //    size.Height = Math.Max( size.Height, column.PreferredSize.Height );
                        if (LabelPosition.LabelOnly != iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.LabelPositionResolved)
                            size.Height = Math.Max(size.Height, iProvideRowLayoutColumnInfo.PreferredHeaderSize.Height);
					}
					else
					{
						// In the regular view when the headers are separate from the cells, use the
						// cell's width.
						//
						// SSP 6/30/03 UWG2375
						// Changed it so that we will use the bigger of the preferred cell width
						// and the preferred label width.
						//
						//size.Width = this.Column.PreferredSize.Width;
						// SSP 8/16/05 BR05387
						// Only do so if cells are actually visible.
						// 
						//size.Width = Math.Max( size.Width, this.Column.PreferredSize.Width );
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( LabelPosition.LabelOnly != this.Column.RowLayoutColumnInfo.LabelPositionResolved )
						//    size.Width = Math.Max( size.Width, this.Column.PreferredSize.Width );
                        // MRS - NAS 9.1 - Groups in RowLayout
                        //if ( LabelPosition.LabelOnly != column.RowLayoutColumnInfo.LabelPositionResolved )
                        //    size.Width = Math.Max( size.Width, column.PreferredSize.Width );
                        if (LabelPosition.LabelOnly != iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.LabelPositionResolved)
                            size.Width = Math.Max(size.Width, iProvideRowLayoutColumnInfo.PreferredSize.Width);

						if ( size.Height <= 0 )
							size.Height = this.Height;
					}
				}
				else
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//Size cardLabelSize = this.Column.CardLabelSize;
                    // MRS - NAS 9.1 - Groups in RowLayout
					//Size cardLabelSize = column.CardLabelSize;
                    Size cardLabelSize = iProvideRowLayoutColumnInfo.PreferredHeaderSize;

					// If the headers are laid out in rows with cells, then apply the same cell
					// spacing as cells to the headers.
					//
					int cellSpacing = this.Band.CellSpacingResolved;

					if ( this.Band.CardView )
					{
						if ( size.Width <= 0 )
							size.Width = 2 * cellSpacing + cardLabelSize.Width;

						if ( size.Height <= 0 )
							size.Height = 2 * cellSpacing + cardLabelSize.Height;
					}
					else
					{
						if ( size.Width <= 0 )
							size.Width = 2 * cellSpacing + cardLabelSize.Width;

						if ( size.Height <= 0 )
							size.Height = 2 * cellSpacing + this.Height;
					}
				}

				// SSP 6/30/03 UWG2375
				// Return the greater of preferred size and the minimum size.
				//
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//Size minSize = this.Column.RowLayoutColumnInfo.MinimumLabelSize;
                // MRS - NAS 9.1 - Groups in RowLayout
				//Size minSize = column.RowLayoutColumnInfo.MinimumLabelSize;
                Size minSize = iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.MinimumLabelSize;

				size.Width = Math.Max( size.Width, minSize.Width );
				size.Height = Math.Max( size.Height, minSize.Height );

				return size;
			}
		}

		#endregion // PreferredSize

		#region Implementation of ILayoutItem

		#region ILayoutItem.MinimumSize
		
		Size ILayoutItem.MinimumSize
		{
			get
			{
				// SSP 11/17/04
				// Added a way to hide the header or the cell of a column in the row layout mode.
				// This way you can do things like hide a row of headers or add unbound columns
				// and hide their cells to show grouping headers etc...
				//
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( LabelPosition.None == this.Column.RowLayoutColumnInfo.LabelPositionResolved )
                // MRS - NAS 9.1 - Groups in RowLayout
				//UltraGridColumn column = this.Column;
                IProvideRowLayoutColumnInfo iProviderRowLayoutColumnInfo = this.RowLayoutColumnInfoProvider;

                // MRS - NAS 9.1 - Groups in RowLayout
				//if ( LabelPosition.None == column.RowLayoutColumnInfo.LabelPositionResolved )
                if (LabelPosition.None == iProviderRowLayoutColumnInfo.RowLayoutColumnInfo.LabelPositionResolved)
				{
					// SSP 8/16/05 BR05387
					// 
					//return new Size( 0, 0 );
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//return LayoutUtility.AccountForLabelPosition( this.Band, ((ILayoutItem)this.Column).MinimumSize );
                    // MRS - NAS 9.1 - Groups in RowLayout
					//return LayoutUtility.AccountForLabelPosition( this.Band, ( (ILayoutItem)column ).MinimumSize );
                    return LayoutUtility.AccountForLabelPosition(this.Band, iProviderRowLayoutColumnInfo.ContentLayoutItem.MinimumSize);
				}

				return this.MinimumSize;
			}
		}

		#endregion // ILayoutItem.MinimumSize

		#region ILayoutItem.PreferredSize

		Size ILayoutItem.PreferredSize
		{
			get
			{
				// SSP 11/17/04
				// Added a way to hide the header or the cell of a column in the row layout mode.
				// This way you can do things like hide a row of headers or add unbound columns
				// and hide their cells to show grouping headers etc...
				//
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( LabelPosition.None == this.Column.RowLayoutColumnInfo.LabelPositionResolved )
                // MRS - NAS 9.1 - Groups in RowLayout
				//UltraGridColumn column = this.Column;
                IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = this.RowLayoutColumnInfoProvider;

                // MRS - NAS 9.1 - Groups in RowLayout
				//if ( LabelPosition.None == column.RowLayoutColumnInfo.LabelPositionResolved )
                if (LabelPosition.None == iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.LabelPositionResolved)
				{
					// SSP 8/16/05 BR05387
					// 
					//return new Size( 0, 0 );
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//return LayoutUtility.AccountForLabelPosition( this.Band, ((ILayoutItem)this.Column).PreferredSize );
                    // MRS - NAS 9.1 - Groups in RowLayout
					//return LayoutUtility.AccountForLabelPosition( this.Band, ( (ILayoutItem)column ).PreferredSize );
                    return LayoutUtility.AccountForLabelPosition(this.Band, iProvideRowLayoutColumnInfo.ContentLayoutItem.PreferredSize);
				}

				return this.PreferredSize;
			}
		}

		#endregion // ILayoutItem.PreferredSize
		
		#region ILayoutItem.IsVisible

		/// <summary>
		/// Indicates whether this column is visible.
		/// </summary>
		bool ILayoutItem.IsVisible
		{
			get
			{
				return this.Column.IsVisibleInLayout;
			}
		}

		#endregion // ILayoutItem.IsVisible

		#endregion // ILayoutItem.GetMinimumSize
		
		#region IGridBagConstraint interface implementation

		#region Anchor

		/// <summary>
		/// If the display area of the item is larger than the item, this property indicates where to anchor the item.
		/// </summary>
		AnchorType IGridBagConstraint.Anchor 
		{ 
			get
			{
				return AnchorType.Center;
			}
		}

		#endregion // Anchor

		#region Fill

		/// <summary>
		/// <p>Fill indicates whether to resize the item to fill the extra spance if the layout item's display area is larger than its size,</p> 
		/// </summary>
		FillType IGridBagConstraint.Fill 
		{ 
			get
			{
				return Infragistics.Win.Layout.FillType.Both;
			}
		}

		#endregion // Fill

		#region Insets

		/// <summary>
		/// Indicates the padding around the layout item.
		/// </summary>
		Insets IGridBagConstraint.Insets 
		{ 
			get
			{
				// Return an empty inset which is what the column's RowLayoutColumnInfo retruns.
				//
				// SSP 12/1/03 UWG2641
				// Added CellInsets and LabelInsets properties.
				//
				//return this.Column.RowLayoutColumnInfo.Insets;
                // MRS - NAS 9.1 - Groups in RowLayout
				//return this.Column.RowLayoutColumnInfo.LabelInsets;
                return this.RowLayoutColumnInfo.LabelInsets;
			}
		}

		#endregion // Insets

		#region OriginX

		/// <summary>
		/// <p>OriginX and OriginY define where the layout item will be placed in the virtual grid of the grid-bag layout. OriginX specifies the location horizontally while specifies the location vertically. These locations are the coordinates of the cells in the virtual grid that the grid-bag layout represents.</p>
		/// <p>The leftmost cell has OriginX of 0. The constant <see cref="GridBagConstraintConstants.Relative"/> specifies that the item be placed just to the right of the item that was added to the layout manager just before this item was added. </p>
		/// <p>The default value is <see cref="GridBagConstraintConstants.Relative"/>. OriginX should be a non-negative value.</p>
		/// </summary>
		int IGridBagConstraint.OriginX
		{ 
			get
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo = this.Column.RowLayoutColumnInfo;
                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo = this.RowLayoutColumnInfo;

                if (!this.Band.AreColumnHeadersInSeparateLayoutArea ||
                    // MRS - NAS 9.1 - Groups in RowLayout
                    this is GroupHeader)
				{
					if ( LabelPosition.Right == rowLayoutColumnInfo.LabelPositionResolved )
						return rowLayoutColumnInfo.OriginXResolved + rowLayoutColumnInfo.SpanXResolved - rowLayoutColumnInfo.LabelSpanResolved;
				}

				return rowLayoutColumnInfo.OriginXResolved;
			}
		}

		#endregion // OriginX

		#region OriginY

		/// <summary>
		/// <p>OriginX and OriginY define where the layout item will be placed in the virtual grid of the grid-bag layout. OriginX specifies the location horizontally while specifies the location vertically. These locations are the coordinates of the cells in the virtual grid that the grid-bag layout represents.</p>
		/// <p>The topmost cell has OriginY of 0. The constant <see cref="GridBagConstraintConstants.Relative"/> specifies that the item be placed just below the item that was added to the layout manager just before this item was added.</p>
		/// <p>The default value is <see cref="GridBagConstraintConstants.Relative"/>. OriginY should be a non-negative value.</p>
		/// </summary>
		int IGridBagConstraint.OriginY 
		{ 
			get
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo = this.Column.RowLayoutColumnInfo;
                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo = this.RowLayoutColumnInfo;

				if (! this.Band.AreColumnHeadersInSeparateLayoutArea  ||
                    // MRS - NAS 9.1 - Groups in RowLayout
                    this is GroupHeader)
				{
					if ( LabelPosition.Bottom == rowLayoutColumnInfo.LabelPositionResolved )
						return rowLayoutColumnInfo.OriginYResolved + rowLayoutColumnInfo.SpanYResolved - rowLayoutColumnInfo.LabelSpanResolved;
				}

				return rowLayoutColumnInfo.OriginYResolved;
			}
		}

		#endregion // OriginY

		#region SpanX

		/// <summary>
		/// <p>Specifies the number of cells this item will span horizontally. The constant <see cref="GridBagConstraintConstants.Remainder"/> specifies that this item be the last one in the row and thus occupy remaining space.</p>
		/// </summary>
		int IGridBagConstraint.SpanX 
		{ 
			get
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo = this.Column.RowLayoutColumnInfo;
                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo = this.RowLayoutColumnInfo;

				if ( ! this.Band.AreColumnHeadersInSeparateLayoutArea ||
                    // MRS - NAS 9.1 - Groups in RowLayout
                    this is GroupHeader)
				{
					LabelPosition labelPosition = rowLayoutColumnInfo.LabelPositionResolved;
					if ( LabelPosition.Left == labelPosition || LabelPosition.Right == labelPosition )
					{
						return rowLayoutColumnInfo.LabelSpanResolved;
					}
				}

				return rowLayoutColumnInfo.SpanXResolved;
			}
		}

		#endregion // SpanX

		#region SpanY

		/// <summary>
		/// <p>Specifies the number of cells this item will span vertically. The constant <see cref="GridBagConstraintConstants.Remainder"/> specifies that this item be the last one in the column and thus occupy remaining space.</p>
		/// </summary>
		int IGridBagConstraint.SpanY 
		{ 
			get
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo = this.Column.RowLayoutColumnInfo;
                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo rowLayoutColumnInfo = this.RowLayoutColumnInfo;
                                
				if (! this.Band.AreColumnHeadersInSeparateLayoutArea ||
                    // MRS - NAS 9.1 - Groups in RowLayout
                    this is GroupHeader)
				{
					LabelPosition labelPosition = rowLayoutColumnInfo.LabelPositionResolved;
					if ( LabelPosition.Top == labelPosition || LabelPosition.Bottom == labelPosition )
					{
						return rowLayoutColumnInfo.LabelSpanResolved;
					}
				}

				return rowLayoutColumnInfo.SpanYResolved;
			}
		}

		#endregion // SpanY

		#region WeightX

		/// <summary>
		/// Indicates how the extra horizontal space will be distributed among items. Default value is 0.0. Higher values give higher priority. The weight of the column in the virtual grid the grid-bag layout represents is the maximum WeightX of all the items in the row.
		/// </summary>
		float IGridBagConstraint.WeightX
		{ 
			get
			{
				// SSP 8/22/03
				// In regular view when the headers are in seperate area return the same
				// weightX as returned by the column.
				//
				//return 0.0f;
				// SSP 6/21/05
				// Also return the weight if the cells aren't visible and only the label is visible.
				// Noticed while implementing column chooser functionality.
				// 
				//if ( this.Band.AreColumnHeadersInSeparateLayoutArea && ! this.Band.CardView )
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.Band.AreColumnHeadersInSeparateLayoutArea && ! this.Band.CardView
				//    || LabelPosition.LabelOnly == this.Column.RowLayoutColumnInfo.LabelPositionResolved )
				//    return ((IGridBagConstraint)this.Column).WeightX;
                // MRS - NAS 9.1 - Groups in RowLayout
                //UltraGridColumn column = this.Column;

                //if ( this.Band.AreColumnHeadersInSeparateLayoutArea && !this.Band.CardView
                //    || LabelPosition.LabelOnly == column.RowLayoutColumnInfo.LabelPositionResolved )
                //    return ( (IGridBagConstraint)column ).WeightX;
                IProvideRowLayoutColumnInfo iProviderRowLayoutColumnInfo = this.RowLayoutColumnInfoProvider;
                if ( this.Band.AreColumnHeadersInSeparateLayoutArea && !this.Band.CardView
                    || LabelPosition.LabelOnly == iProviderRowLayoutColumnInfo.RowLayoutColumnInfo.LabelPositionResolved)
                    return iProviderRowLayoutColumnInfo.HeaderConstraint.WeightX;

				return 0.0f;
			}
		}

		#endregion // WeightX

		#region WeightY

		/// <summary>
		/// Indicates how the extra vertical space will be distributed among items. Default value is 0.0. Higher values give higher priority. The weight of the column in the virtual grid the grid-bag layout represents is the maximum WeightY of all the items in the column.
		/// </summary>
		float IGridBagConstraint.WeightY
		{ 
			get
			{
				// SSP 8/22/03
				// In card view when the headers are in seperate area return the same
				// weightY as returned by the column.
				//
				//return 0.0f;
				// SSP 6/21/05 - Column Chooser
				// Also return the weight if the cells aren't visible and only the label is visible.
				// Also there shouldn't be not in front of card view. Noticed while implementing
				// column chooser functionality.
				// 
				//if ( this.Band.AreColumnHeadersInSeparateLayoutArea && ! this.Band.CardView )
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.Band.AreColumnHeadersInSeparateLayoutArea && this.Band.CardView 
				//    || LabelPosition.LabelOnly == this.Column.RowLayoutColumnInfo.LabelPositionResolved )
				//    return ((IGridBagConstraint)this.Column).WeightY;
                // MRS - NAS 9.1 - Groups in RowLayout
                //UltraGridColumn column = this.Column;

                //if ( this.Band.AreColumnHeadersInSeparateLayoutArea && this.Band.CardView
                //    || LabelPosition.LabelOnly == column.RowLayoutColumnInfo.LabelPositionResolved )
                //    return ( (IGridBagConstraint)column ).WeightY;

                IProvideRowLayoutColumnInfo iProviderRowLayoutColumnInfo = this.RowLayoutColumnInfoProvider;
                if (this.Band.AreColumnHeadersInSeparateLayoutArea && this.Band.CardView
                    || LabelPosition.LabelOnly == iProviderRowLayoutColumnInfo.RowLayoutColumnInfo.LabelPositionResolved)
                    return iProviderRowLayoutColumnInfo.HeaderConstraint.WeightY;

				return 0.0f;
			}
		}

		#endregion // WeightY

		#endregion // IGridBagConstraint interface implementation

		#region SizeResolved

		// SSP 6/25/03 - Row Layout Functionality
		// Added SizeResolved method.
		//
		/// <summary>
		/// Resolved header size. This property returns the actual width and the height of the header.
		/// </summary>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public abstract Size SizeResolved { get; }

		#endregion // SizeResolved

		#endregion // Row Layout Code

		// SSP 5/19/03 - Fixed headers
		// 
		#region Fixed Headers Code

		// SSP 7/10/03 - Fixed headers
		// Overrode SetFocusAndActivate method so we can scroll it into view if it's
		// scrolled underneath fixed headers when it's beign selected.
		//
		#region SetFocusAndActivate
		
		internal override bool SetFocusAndActivate( bool byMouse, bool enterEditMode, bool byTabKey )
		{
			bool canceled = base.SetFocusAndActivate( byMouse, enterEditMode, byTabKey );

			// If the header was activated, then scroll it into view if it's scrolled underneath
			// fixed headers.
			//
			if ( ! canceled )
			{
				UltraGridLayout layout = null != this.Band ? this.Band.Layout : null;
				if ( null != layout && null != layout.ActiveColScrollRegion )
				{
					ColScrollRegion activeCSR = layout.ActiveColScrollRegion;
					if ( ! this.Band.IsHeaderFixed( activeCSR, this ) 
						&& this.Band.IsHeaderScrolledUnderFixedHeaders( activeCSR, this ) )
						activeCSR.ScrollHeaderIntoView( this, false );
				}
			}

			return canceled;
		}

		#endregion // SetFocusAndActivate

		#region Fixed

		/// <summary>
		/// Specifies whether the header is fixed. When a header is fixed column(s) associated with the header remain in view when the grid is scrolled horizontally. This property is settable on column headers and group headers. If there are groups, then the Fixed property settings off the group headers will be used and fixed property settings off the column headers will be ignored. Attempting to set it on a BandHeader object will lead to a NotSupportedException. NOTE: This property is ignored in Row-Layout mode as headers can't be fixed in Row-Layout mode. You must enable the fixed-headers functionality by setting <see cref="UltraGridLayout.UseFixedHeaders"/> to true in order for this property to have any effect.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies whether the header is fixed. When a header is fixed column(s) associated with the header remain in view when the grid is scrolled horizontally.</p>
		/// <p class="body">Specifies whether the header is fixed. This property is settable on column headers and group headers. Attempting to set it on a BandHeader object will lead to a NonSupportedException.</p>
		/// <p calss="body">You must enable the fixed-headers functionality by setting <see cref="UltraGridLayout.UseFixedHeaders"/> to true in order for this property to have any effect.</p>
		/// <p class="body">NOTE: This property is ignored in Row-Layout mode as headers can't be fixed in Row-Layout mode. Also if there are groups, then the Fixed settings off the column headers will be ignored and Fixed settings off the group headers will be used.</p>
		/// <p><seealso cref="UltraGridLayout.UseFixedHeaders"/> <seealso cref="HeaderBase.Fixed"/> <seealso cref="UltraGridOverride.FixedHeaderIndicator"/> <seealso cref="HeaderBase.FixedHeaderIndicator"/></p>
		/// </remarks>
		// SSP 7/1/05 BR04857
		// Made the property virtual and overrode it on the BandHeader so it can be marked Browsable( false ).
		// 
		//public bool Fixed
		public virtual bool Fixed
		{
			get
			{
				return this.isHeaderFixed;
			}
			set
			{
				if ( value != this.isHeaderFixed )
				{
					// SSP 7/1/05 BR04857
					// Made the property virtual and overrode it on the BandHeader. Now it throws the
					// exception so this is not necessary anymore.
					// 
					//if ( this is BandHeader )
					//	throw new NotSupportedException( "Fixed property is not supported on BandHeaders." );
                    
					this.isHeaderFixed = value;

					if ( null != this.Band )
						this.Band.BumpFixedHeadersVerifyVersion( true );

					this.NotifyPropChange( PropertyIds.Fixed );
				}
			}
		}

		#endregion // Fixed

		#region NotifyFixedChanged
		
		internal void NotifyFixedChanged( )
		{
			this.NotifyPropChange( PropertyIds.Fixed );
		}

		#endregion // NotifyFixedChanged

		#region InternalSetFixed

		internal void InternalSetFixed( bool val )
		{
			this.isHeaderFixed = val;
		}

		#endregion // InternalSetFixed

		// MD 12/9/08 - 9.1 - Column Pinning Right
		#region InternalSetFixOnRight

		internal void InternalSetFixOnRight( DefaultableBoolean val )
		{
			this.fixOnRight = val;
		} 

		#endregion InternalSetFixOnRight

		#region ShouldSerializeFixed

		/// <summary>
		/// Returns true if the property has been set to a non-default value of true.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFixed( )
		{
			return this.isHeaderFixed;
		}

		#endregion // ShouldSerializeFixed

		#region ResetFixed

		/// <summary>
		/// Resets the property to its default value of false.
		/// </summary>
		// SSP 7/29/05 BR04857
		// Made virtual and overrode it on the BandHeader since set of BandHeader.Fixed 
		// throws an exception.
		// 
		//public void ResetFixed( )
		public virtual void ResetFixed( )
		{
			this.Fixed = false;
		}

		#endregion // ResetFixed

		#region FixedHeaderIndicator

		/// <summary>
		/// Specifies whether the user is allowed to fix or unfix the header. NOTE: This property is ignored in Row-Layout mode as headers can't be fixed in Row-Layout mode.
		/// </summary>
		/// <remarks>
		/// <p class="body">FixedHeaderIndicator property specifies whether the user is allowed to fix or unfix the header.</p>
		/// <p class="body">NOTE: This property is ignored in Row-Layout mode as headers can't be fixed in Row-Layout mode.</p>
		/// <p><seealso cref="HeaderBase.Fixed"/> <seealso cref="Infragistics.Win.UltraWinGrid.FixedHeaderIndicator"/> <seealso cref="UltraGridOverride.FixedHeaderIndicator"/></p>
		/// </remarks>
		// SSP 7/1/05 BR04857
		// Made virtual and overrode it on the BandHeader to mark it Browsable( false ) since
		// the band headers can't be fixed.
		// 
		//public Infragistics.Win.UltraWinGrid.FixedHeaderIndicator FixedHeaderIndicator
		public virtual Infragistics.Win.UltraWinGrid.FixedHeaderIndicator FixedHeaderIndicator
		{
			get
			{
				return this.fixedHeaderIndicator;
			}
			set
			{
				if ( value != this.fixedHeaderIndicator )
				{
					if ( ! Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.FixedHeaderIndicator ), value ) )
						throw new InvalidEnumArgumentException( "FixedHeaderIndicator", (int)value, typeof( Infragistics.Win.UltraWinGrid.FixedHeaderIndicator ) );

					this.fixedHeaderIndicator = value;
					
					this.NotifyPropChange( PropertyIds.FixedHeaderIndicator );
				}
			}
		}

		#endregion // FixedHeaderIndicator

		#region InternalSetVisiblePosValue
		
		internal void InternalSetVisiblePosValue( int val )
		{
			this.visiblePos = val;
		}

		#endregion // InternalSetVisiblePosValue

		#region ShouldSerializeFixedHeaderIndicator

		/// <summary>
		/// Retruns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFixedHeaderIndicator( )
		{
			return Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default != this.fixedHeaderIndicator;
		}

		#endregion // ShouldSerializeFixedHeaderIndicator

		#region ResetFixedHeaderIndicator

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		// SSP 7/29/05 BR04857
		// Made virtual and overrode it on the BandHeader since set of BandHeader.Fixed 
		// throws an exception.
		// 
		//public void ResetFixedHeaderIndicator( )
		public virtual void ResetFixedHeaderIndicator( )
		{
			this.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default;
		}

		#endregion // ResetFixedHeaderIndicator

		#region FixedHeaderIndicatorResolved

		internal Infragistics.Win.UltraWinGrid.FixedHeaderIndicator FixedHeaderIndicatorResolved
		{
			get
			{
				// For band headers, return false.
				//
				if ( this is BandHeader )
					return FixedHeaderIndicator.None;

				// If we have groups, then only groups should have the fixed header indicator
				// because with groups, you can't fix individual columns. Only whole groups.
				//
				// MD 1/21/09 - Groups in RowLayouts
				// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
				//if ( this is ColumnHeader && null != this.Column.Group )
				if ( this is ColumnHeader && null != this.Column.GroupResolved )
					return FixedHeaderIndicator.None;

				// In row-layout mode, the headers can't be fixed.
				//
				if ( this.Band.UseRowLayoutResolved )
					return FixedHeaderIndicator.None;

				// We don't support fixing headers in exclusive col scroll regions.
				//
				if ( null != this.ExclusiveColScrollRegionResolved )
					return FixedHeaderIndicator.None;

				if ( Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default != this.FixedHeaderIndicator )
					return this.FixedHeaderIndicator;

				UltraGridOverride ovrrd = this.Band.HasOverride ? this.Band.Override : null;
				if ( null != ovrrd && ovrrd.ShouldSerializeFixedHeaderIndicator( ) )
					return ovrrd.FixedHeaderIndicator;

				ovrrd = this.Band.Layout.HasOverride ? this.Band.Layout.Override : null;
				if ( null != ovrrd && ovrrd.ShouldSerializeFixedHeaderIndicator( ) )
					return ovrrd.FixedHeaderIndicator;

				return FixedHeaderIndicator.Button;
			}
		}

		#endregion // FixedHeaderIndicatorResolved

		#region FixedResolved
		
		internal bool FixedResolved
		{
			get
			{
				// If we have groups and this is a column header, then return the FixedResolved
				// value of the group-header.
				//
				// MD 8/3/07 - 7.3 Performance
				// Refactored - Prevent calling expensive getters multiple times
				//if ( this is ColumnHeader && null != this.Column.Group )
				//    return this.Column.Group.Header.FixedResolved;
				if ( this is ColumnHeader )
				{
					UltraGridColumn column = this.Column;

					// MD 1/21/09 - Groups in RowLayouts
					// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
					//if ( null != column.Group )
					//    return column.Group.Header.FixedResolved;
					if ( null != column.GroupResolved )
						return column.GroupResolved.Header.FixedResolved;
				}

				// Fixed headers are not supported in exclusive col scroll regions or in row-layout mode.
				//
				if ( ! this.Fixed || this is BandHeader || null == this.Band || ! this.Band.UseFixedHeaders 
					|| this.Band.UseRowLayoutResolved || null != this.ExclusiveColScrollRegionResolved )
					return false;

				return true;
			}
		}

		#endregion // FixedResolved

		#region OnFixedHeaderIndicatorClicked
		
		internal void OnFixedHeaderIndicatorClicked( ColScrollRegion csr )
		{
			Debug.Assert( null != csr || csr.IsExclusive, "No csr passed in or is exclusive !" );

			if ( null == csr )
				return;

			// Fixing headers is only supported on column headers and group headers. It doesn't make
			// sense to fix band header as there is only one band header per band anyways.
			//
			Debug.Assert( this is ColumnHeader || this is GroupHeader, "This must be either a column header or a group header." );
			if ( ! ( this is ColumnHeader || this is GroupHeader ) )
				return;

			UltraGridBase grid = this.Band.Layout.Grid;

			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Renamed the local variable for clarity and added a new variable holding the first right fixed header.
			//HeaderBase lastFixedHeader = this.Band.GetLastFixedHeader( csr );
			HeaderBase lastLeftFixedHeader = this.Band.GetLastLeftFixedHeader( csr );
			HeaderBase firstRightFixedHeader = this.Band.GetFirstRightFixedHeader( csr );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//HeaderBase firstNonFixedHeader = this.Band.GetFirstNonFixedHeader( csr );

			HeaderBase headerClone;

			// This could be a column header or a group header. Clone whichever it is to get
			// the cloned header which we can pass into the Before Col/Group Pos Changed event.
			//
			if ( this is ColumnHeader )
			{
				UltraGridColumn cloneColumn = this.Column.Clone( PosChanged.Moved );
				headerClone = null != cloneColumn ? cloneColumn.Header : null;
			}
			else if ( this is GroupHeader )
			{
				UltraGridGroup cloneGroup = this.Group.Clone( PosChanged.Moved, false );
				headerClone = null != cloneGroup ? cloneGroup.Header : null;
			}
			else
			{
				Debug.Assert( false, "Header is neither a column header nor a group header !" );
				return;
			}

			Debug.Assert( null != headerClone, "Clone failed !" );
			if ( null == headerClone )
				return;
			
			// Toggle the isHeaderFixed state of the header. However set the new state on the
			// cloned header and not the original header. We need to do this for the proper
			// workings of the Before Col/Group PosCHanged event.
			//
			headerClone.isHeaderFixed = ! this.Fixed;
			
			// Fire the Before Col/Group Pos Changed event.
			//
			if ( this is ColumnHeader )
			{
				BeforeColPosChangedEventArgs e = new BeforeColPosChangedEventArgs(
					PosChanged.Moved, new ColumnHeader[] { (ColumnHeader)headerClone } );

				grid.FireBeforeColPosChanged( e );
				if ( e.Cancel )
					return;
			}
			else if ( this is GroupHeader )
			{
				BeforeGroupPosChangedEventArgs e = new BeforeGroupPosChangedEventArgs(
					PosChanged.Moved, new GroupHeader[] { (GroupHeader)headerClone } );
					
				grid.FireBeforeGroupPosChanged( e );
				if ( e.Cancel )
					return;
			}
			else
			{
				Debug.Assert( false, "Header is neither a column header nor a group header !" );
				return;
			}

			// If the Before Col/Group PosChanged event wasn't cancelled then copy the fixed state
			// of the cloned header to the original header also setting its VisiblePosition in
			// such way as to ensure that the fixed headers are with other fixed headers and 
			// non-fixed headers are with the other non-fixed headers.
			//
			if ( headerClone.isHeaderFixed )
			{
				// MD 12/8/08 - 9.1 - Column Pinning Right
				// The new visible position will be different depending on whether the header is fixing on the left or right.
				//int newVisiblePosition = null != lastFixedHeader ? 1 + lastFixedHeader.VisiblePosition : 0;
				int newVisiblePosition;
				if ( this.FixOnRightResolved )
				{
					newVisiblePosition = firstRightFixedHeader != null
						? firstRightFixedHeader.VisiblePosition - 1
						: this.Band.OrderedHeaders.LastVisibleHeader.VisiblePosition;
				}
				else
				{
					newVisiblePosition = lastLeftFixedHeader != null
						? 1 + lastLeftFixedHeader.VisiblePosition 
						: 0;
				}

				this.Fixed = true;
				this.VisiblePosition = newVisiblePosition;
			}
			else
			{
				// MD 12/8/08 - 9.1 - Column Pinning Right
				// The new visible position will be different depending on whether the header is unfixing from the left or right.
				//Debug.Assert( null != lastFixedHeader, "Even though the header was initially fixed, Band.GetLastFixedHeader returned null !" );
				//int newVisiblePosition = null != lastFixedHeader ? lastFixedHeader.VisiblePosition : 0;
				Debug.Assert( lastLeftFixedHeader != null || firstRightFixedHeader != null, "Even though the header was initially fixed, Band.GetLastLeftFixedHeader and GetFirstRightFixedHeader returned null !" );
				int newVisiblePosition;
				if ( this.FixOnRightResolved )
				{
					newVisiblePosition = firstRightFixedHeader != null
						? firstRightFixedHeader.VisiblePosition
						: this.Band.OrderedHeaders.LastVisibleHeader.VisiblePosition;
				}
				else
				{
					newVisiblePosition = lastLeftFixedHeader != null
						? lastLeftFixedHeader.VisiblePosition
						: 0;
				}

				this.Fixed = false;
				this.VisiblePosition = newVisiblePosition;
			}

			// Scroll the header into view.
			//
			csr.ScrollHeaderIntoView( this, false );

			// Fire the After Col/Group Pos Changed event.
			//
			if ( this is ColumnHeader )
			{
				AfterColPosChangedEventArgs e = new AfterColPosChangedEventArgs(
					// SSP 7/11/05 BR04687
					// Pass in the original header object instead of the clone.
					// 
					//PosChanged.Moved, new ColumnHeader[] { (ColumnHeader)headerClone } );
					PosChanged.Moved, new ColumnHeader[] { (ColumnHeader)this } );

				grid.FireAfterColPosChanged( e );
			}
			else if ( this is GroupHeader )
			{
				AfterGroupPosChangedEventArgs e = new AfterGroupPosChangedEventArgs(
					// SSP 7/11/05 BR04687
					// Pass in the original header object instead of the clone.
					// 
					//PosChanged.Moved, new GroupHeader[] { (GroupHeader)headerClone } );
					PosChanged.Moved, new GroupHeader[] { (GroupHeader)this } );
					
				grid.FireAfterGroupPosChanged( e );
			}
			else
			{
				Debug.Assert( false, "Header is neither a column header nor a group header !" );
				return;
			}
		}

		#endregion // OnFixedHeaderIndicatorClicked

		#endregion // Fixed Headers Code


		#region HeaderAccessibleObjectBase Class

		/// <summary>
		/// The Accessible object for a column header.
		/// </summary>
		public class HeaderAccessibleObjectBase : AccessibleObjectWrapper
		{
			#region Private Members

			private Infragistics.Win.UltraWinGrid.HeaderBase header;
			private HeadersCollection.HeadersAccessibleObject parent;

			#endregion Private Members

			#region Constructor
			
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="header">The header</param>
			/// <param name="parent">The parent AccessibleObject</param>
			public HeaderAccessibleObjectBase( HeaderBase header, HeadersCollection.HeadersAccessibleObject parent )
			{
				if ( null == header || null == parent )
					throw new ArgumentNullException( null == header ? "header" : "parent" );

				this.header = header;
				this.parent = parent;
			}

			#endregion Constructor

			#region Base Class Overrides

				#region Bounds

			/// <summary>
			/// Gets the location and size of the accessible object.
			/// </summary>
			public override System.Drawing.Rectangle Bounds
			{
				get
				{
					Infragistics.Win.UIElement element = this.parent.UIElement;

					if ( element == null )
						return Rectangle.Empty;

					element = element.GetDescendant( typeof(HeaderUIElement), this.header );

					if ( element == null )
						return Rectangle.Empty;

					Control control = element.Control;

					if ( control == null )
						return Rectangle.Empty;

					return control.RectangleToScreen( element.ClipRect );
				}
			}

				#endregion Bounds

				#region GetChild

			/// <summary>
			/// Retrieves the accessible child corresponding to the specified index.
			/// </summary>
			/// <param name="index">The zero-based index of the accessible child.</param>
			/// <returns>An AccessibleObject that represents the accessible child corresponding to the specified index.</returns>
			public override AccessibleObject GetChild(int index)
			{
				return null;
			}

				#endregion GetChild

				#region GetChildCount

			/// <summary>
			/// Retrieves the number of children belonging to an accessible object.
			/// </summary>
			/// <returns>The number of children belonging to an accessible object.</returns>
			public override int GetChildCount()
			{
				return 0;
			}

				#endregion GetChildCount

				#region GetFocused

			/// <summary>
			/// Retrieves the object that has the keyboard focus.
			/// </summary>
			/// <returns>An AccessibleObject that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (Nothing in Visual Basic) if no object has focus.</returns>
			public override AccessibleObject GetFocused()
			{
				return null;
			}

				#endregion GetFocused

			//JJD 12/17/03
				#region GetMarshallingControl

			/// <summary>
			/// Returns the control used to synchronize accessibility calls.
			/// </summary>
			/// <returns>A control to be used to synchronize accessibility calls.</returns>
			protected override Control GetMarshallingControl()
			{
				return this.header.Band.Layout.Grid;
			}

				#endregion GetMarshallingControl

				#region GetSelected

			/// <summary>
			/// Retrieves the currently selected child.
			/// </summary>
			/// <returns>An AccessibleObject that represents the currently selected child. This method returns the calling object if the object itself is selected. Returns a null reference (Nothing in Visual Basic) if is no child is currently selected and the object itself does not have focus.</returns>
			public override AccessibleObject GetSelected()
			{
				
				return null;
			}

				#endregion GetSelected

				#region Help

			/// <summary>
			/// Gets a description of what the object does or how the object is used.
			/// </summary>
			public override string Help
			{
				get
				{
					return null;
				}
			}

				#endregion Help

				#region HitTest

			/// <summary>
			/// Retrieves the child object at the specified screen coordinates.
			/// </summary>
			/// <param name="x">The horizontal screen coordinate.</param>
			/// <param name="y">The vertical screen coordinate.</param>
			/// <returns>An AccessibleObject that represents the child object at the given screen coordinates. This method returns the calling object if the object itself is at the location specified. Returns a null reference (Nothing in Visual Basic) if no object is at the tested location.</returns>
			public override AccessibleObject HitTest(int x, int y)
			{
				
				return base.HitTest( x, y );
			}

				#endregion HitTest

				#region Name

			/// <summary>
			/// The accessible name for the data area.
			/// </summary>
			public override string Name
			{
				get
				{
					string name = base.Name;

					if ( name != null && name.Length > 0 )
						return name;

					return this.header.Caption;
				}
				set
				{
					base.Name = value;
				}
			}

				#endregion Name

//				#region Navigate
//
//			/// <summary>
//			/// Navigates to another accessible object.
//			/// </summary>
//			/// <param name="navdir">One of the <see cref="System.Windows.Forms.AccessibleNavigation"/> values.</param>
//			/// <returns></returns>
//			public override AccessibleObject Navigate(AccessibleNavigation navdir)
//			{
//				switch ( navdir )
//				{
//						//TODO: implement
//						#region case FirstChild
//
//					case AccessibleNavigation.FirstChild:
//						return null;
//
//						#endregion case FirstChild
//
//						//TODO: implement
//						#region case LastChild
//
//					case AccessibleNavigation.LastChild:
//					{
//						return null;
//					}
//
//						#endregion case LastChild
//
//				}
//
//				// SSP 1/14/04
//				// Commented out the original code and added the new code.
//				//
//				switch ( navdir )
//				{
//						#region case Next
//
//					case AccessibleNavigation.Next:
//						navType = NavigateType.Next;
//						break;
//
//						#endregion case Next
//
//						#region case Previous
//
//					case AccessibleNavigation.Previous:
//						navType = NavigateType.Prev;
//						break;
//
//						#endregion case Previous
//
//						#region case Right
//
//					case AccessibleNavigation.Right:
//						navType = NavigateType.Next;
//						break;
//
//						#endregion case Right
//
//						#region case Left
//
//					case AccessibleNavigation.Left:
//						navType = NavigateType.Prev;
//						break;
//
//						#endregion case Left
//				}
//
//				if ( navType != NavigateType.Topmost )
//				{
//					UltraGridColumn navigateToColumn = this.column.Band.GetRelatedVisibleCol( this.column, navType, true );
//
//					if ( navigateToColumn != null &&
//						navigateToColumn != this.column )
//					{
//						HeadersCollection headers;
//
//						if ( this.column.Band.UseRowLayoutResolved )
//							headers = this.column.Band.LayoutOrderedVisibleColumnHeaders;
//						else
//							headers = this.column.Band.OrderedColumnHeaders;
//
//						int index = headers.IndexOf( navigateToColumn.Header );
//
//						if ( index >= 0 )
//						{
//							System.Windows.Forms.AccessibleObject acc = this.parent.GetChild( index );
//
//							if ( acc != null )
//							{
//								if ( !this.Bounds.IsEmpty && !acc.Bounds.IsEmpty )
//								{
//									if ( navdir == AccessibleNavigation.Left &&
//										acc.Bounds.Right > this.Bounds.Left )
//										return null;
//
//									if ( navdir == AccessibleNavigation.Right &&
//										acc.Bounds.Left < this.Bounds.Right )
//										return null;
//
//									// SSP 1/13/04
//									// Also compare the tops in case of navigating up or down.
//									//
//									if ( AccessibleNavigation.Up == navdir
//										&& acc.Bounds.Top > this.Bounds.Top )
//										return null;
//
//									if ( AccessibleNavigation.Down == navdir 
//										&& acc.Bounds.Top < this.Bounds.Top )
//										return null;
//								}
//
//								return acc;
//							}
//						}
//					}
//				}
//
//				return null;
//
//
//				// Original code:
//				//
//				/*
//				NavigateType navType = NavigateType.Topmost;
//
//				switch ( navdir )
//				{
//						#region case Down
//
//					case AccessibleNavigation.Down:
//						navType = NavigateType.Below;
//						break;
//
//						#endregion case Down
//
//						#region case Up
//
//					case AccessibleNavigation.Up:
//						navType = NavigateType.Above;
//						break;
//
//						#endregion case Up
//
//						#region case Next
//
//					case AccessibleNavigation.Next:
//						navType = NavigateType.Next;
//						break;
//
//						#endregion case Next
//
//						#region case Previous
//
//					case AccessibleNavigation.Previous:
//						navType = NavigateType.Prev;
//						break;
//
//						#endregion case Previous
//
//						#region case Right
//
//					case AccessibleNavigation.Right:
//						navType = NavigateType.Next;
//						break;
//
//						#endregion case Right
//
//						#region case Left
//
//					case AccessibleNavigation.Left:
//						navType = NavigateType.Prev;
//						break;
//
//						#endregion case Left
//				}
//
//				if ( navType != NavigateType.Topmost )
//				{
//					UltraGridColumn navigateToColumn = this.column.Band.GetRelatedVisibleCol( this.column, navType, true );
//
//					if ( navigateToColumn != null &&
//						navigateToColumn != this.column )
//					{
//						HeadersCollection headers;
//
//						if ( this.column.Band.UseRowLayoutResolved )
//							headers = this.column.Band.LayoutOrderedVisibleColumnHeaders;
//						else
//							headers = this.column.Band.OrderedColumnHeaders;
//
//						int index = headers.IndexOf( navigateToColumn.Header );
//
//						if ( index >= 0 )
//						{
//							System.Windows.Forms.AccessibleObject acc = this.parent.GetChild( index );
//
//							if ( acc != null )
//							{
//								if ( !this.Bounds.IsEmpty && !acc.Bounds.IsEmpty )
//								{
//									if ( navdir == AccessibleNavigation.Left &&
//										acc.Bounds.Right > this.Bounds.Left )
//										return null;
//
//									if ( navdir == AccessibleNavigation.Right &&
//										acc.Bounds.Left < this.Bounds.Right )
//										return null;
//
//									// SSP 1/13/04
//									// Also compare the tops in case of navigating up or down.
//									//
//									if ( AccessibleNavigation.Up == navdir
//										&& acc.Bounds.Top > this.Bounds.Top )
//										return null;
//
//									if ( AccessibleNavigation.Down == navdir 
//										&& acc.Bounds.Top < this.Bounds.Top )
//										return null;
//								}
//
//								return acc;
//							}
//						}
//					}
//				}
//
//				return null;
//				*/
//			}
//
//				#endregion Navigate

				#region Parent

			/// <summary>
			/// Gets the parent of an accessible object.
			/// </summary>
			public override AccessibleObject Parent
			{
				get
				{
					return this.parent.AccessibleObject;
				}
			}

				#endregion Parent

				#region State

			/// <summary>
			/// Gets the state of this accessible object.
			/// </summary>
			public override AccessibleStates State
			{
				get
				{
					if ( this.HiddenResolved )
						return AccessibleStates.Invisible;

					if ( this.header.IsDisabled )
						return AccessibleStates.Unavailable;

					AccessibleStates state = AccessibleStates.ReadOnly;

					// SSP 4/20/05 - NAS 5.2 Filter Row
					// Use the Selectable property instead.
					//
					//if ( this.header.CanSelectHeader )
					if ( this.header.Selectable )
					{
						state = AccessibleStates.Selectable;

						if ( this.header.Selected )
							state |= AccessibleStates.Selected;
					}

					return state;
				}
			}

				#endregion State

				#region Value

			/// <summary>
			/// Returns the caption
			/// </summary>
			public override string Value
			{
				get
				{
					return null;
				}
				set
				{

				}
			}

				#endregion Value

				#region Select

			/// <summary>
			/// Modifies the selection or moves the keyboard focus of the accessible object.
			/// </summary>
			/// <param name="flags">One of the <see cref="System.Windows.Forms.AccessibleSelection"/> values.</param>
			public override void Select(System.Windows.Forms.AccessibleSelection flags)
			{
				// We only support selection of columns and groups. Actually, only the column
				// selection because selecting a group really selects columns.
				//
				if ( ! ( this.Header is ColumnHeader ) && ! ( this.Header is GroupHeader ) )
					return;

				HeaderBase item = this.Header;

				UltraGridBase gridBase = this.Header.Band.Layout.Grid;
				ISelectionManager selectionManager = gridBase as ISelectionManager;
				ISelectionStrategy selectionStrategy = null != selectionManager ? selectionManager.GetSelectionStrategy( item ) : null;

				Debug.Assert( null != selectionManager && null != selectionStrategy );
				if ( null == selectionManager || null == selectionStrategy )
					return;

				// We have a selection strategy that only allows contiguous selection.
				//
				bool isStrategyContiguous = 
					selectionStrategy is SelectionStrategyExtended 
					? ! ((SelectionStrategyExtended)selectionStrategy).IsDiscontiguousAllowed 
					: false;

				// NOTE: The order in which following if statements appear is important (at least for
				// some of them) because flags can contain multiple entries and thus they have to be
				// processed in a certain order. For example, TakeFocus can be combined with the 
				// ExtendSelection which necessiates processing ExtendSelection (which is supposed to
				// emulate Shift+Click on an item to range select) before processing TakeFocus.
				//

				if ( gridBase is UltraGrid )
				{
					UltraGrid grid = (UltraGrid)gridBase;

					// ExtendSelection does the range selection. It emulates the Shift + Click.
					// AddSelection adds the item to the selection. It emulates the Ctrl + Click.
					// Both of these actions do not clear the current selection.
					//
					if ( AccessibleSelection.ExtendSelection == ( AccessibleSelection.ExtendSelection & flags ) 
						|| AccessibleSelection.AddSelection == ( AccessibleSelection.AddSelection & flags ) )
					{
						// SSP 4/20/05 - NAS 5.2 Filter Row
						// Use the Selectable property instead.
						//
						//if ( item.CanSelectHeader )
						if ( item.Selectable )
						{
							if ( selectionStrategy.IsSingleSelect )
							{
								// If selection strategy is single select, then clear the exisitng
								// selection.
								//
								grid.InternalSelectItem( item, true, true );
							}
								// Make sure the item can be selected with current selection. For example,
								// we don't allow seleting columns from different bands.
								//
							else if ( selectionManager.IsItemSelectableWithCurrentSelection( item ) 
								&& ! selectionManager.IsMaxSelectedItemsExceeded( item ) )
							{
								if ( AccessibleSelection.ExtendSelection == ( AccessibleSelection.ExtendSelection & flags ) )
								{
									// If ExtendedSelection do range select.
									//
									grid.InternalSelectRange( item, false, true );
								}
								else 
								{
									// If AddSelection then add the item to the selection.
									//
									grid.InternalSelectItem( item, isStrategyContiguous, true );
								}
							}
						}
					}

					// TakeFocus makes the item the pivot item.
					//
					if ( AccessibleSelection.TakeFocus == ( AccessibleSelection.TakeFocus & flags ) )
					{
						// Make the item the pivot item.
						//
						selectionManager.SetPivotItem( item, false );
					}
			
					// Unselect the item.
					//
					if ( AccessibleSelection.RemoveSelection == ( AccessibleSelection.RemoveSelection & flags ) )
					{
						grid.InternalSelectItem( item, isStrategyContiguous, false );
					}

					// Select the item clearing other selected items.
					//
					if ( AccessibleSelection.TakeSelection == ( AccessibleSelection.TakeSelection & flags ) )
					{
						// SSP 4/20/05 - NAS 5.2 Filter Row
						// Use the Selectable property instead.
						//
						//if ( item.CanSelectHeader )
						if ( item.Selectable )
							grid.InternalSelectItem( item, true, true );
					}
				}
				else if ( gridBase is UltraDropDownBase )
				{
					// UltraDropDown does not support header selection.
					//
				}
				else
				{
					Debug.Assert( false, "Unknwon type of grid !" );
				}			
			}

				#endregion Select

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

				#endregion Public Properties

				#region Private/Internal Properties

					#region HiddenResolved
			
			internal bool HiddenResolved
			{
				get
				{
					if ( this.header.Band.HiddenResolved )
						return true;

					if ( this.header.Hidden )
						return true;

					return false;
				}
			}

					#endregion // HiddenResolved
				
					#region Header
			
			internal HeaderBase Header
			{
				get
				{
					return this.header;
				}
			}

					#endregion // Header

					#region ParentInternal
			
			internal HeadersCollection.HeadersAccessibleObject ParentInternal
			{
				get
				{
					return this.parent;
				}
			}

					#endregion // ParentInternal

				#endregion // Private/Internal Properties

			#endregion Properties

		}

		#endregion // HeaderAccessibleObjectBase Class

		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		#region ToolTipText
		
		/// <summary>
		/// Specifies the tooltip to display when the user hovers the mouse over the header.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies the tooltip to display when the user hovers the mouse over the header. No tooltip is displayed if this property is set to null or empty string.</p>
		/// </remarks>
		[LocalizedDescription("LD_Header_P_ToolTipText")]
        public string ToolTipText
		{
			get
			{
				return this.toolTipText;
			}
			set
			{
				if ( this.toolTipText != value )
				{
					this.toolTipText = value;

					this.NotifyPropChange( PropertyIds.ToolTipText );
				}
			}
		}

		#endregion // ToolTipText

		#region ShouldSerializeToolTipText

		/// <summary>
		/// Returns true if the property has been set to a non-default value of true.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeToolTipText( )
		{
			return null != this.toolTipText && this.toolTipText.Length > 0;
		}

		#endregion // ShouldSerializeToolTipText

		#region ResetToolTipText

		/// <summary>
		/// Resets the property to its default value of false.
		/// </summary>
		public void ResetToolTipText( )
		{
			this.ToolTipText = null;
		}

		#endregion // ResetToolTipText

		#region TipStyleHeaderResolved

		// SSP 3/3/06 BR10430
		// Added TipStyleHeader on the Override.
		// 
		internal TipStyle TipStyleHeaderResolved
		{
			get
			{
				return this.Band.TipStyleHeaderResolved;
			}
		}

		#endregion // TipStyleHeaderResolved

		#region Layout

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal UltraGridLayout Layout
		{
			get
			{
				UltraGridBand band = this.Band;
				return null != band ? band.Layout : null;
			}
		}

		#endregion // Layout

		#region HasHotTrackingAppearances

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearancse
		// 
		internal bool HasHotTrackingAppearances
		{
			get
			{
				UltraGridBand band = this.Band;
				return null != band && band.HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex.HotTrackHeaderAppearance
					// SSP 3/21/06 - App Styling
					// 
                    , StyleUtils.GetHeaderRole( this, false ), AppStyling.RoleState.Normal );
			}
		}

		#endregion // HasHotTrackingAppearances

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		#region 8.2 - Rotated Column Headers

		internal abstract TextOrientationInfo DefaultTextOrientation { get; }

		#region TextOrientation

		/// <summary>
		/// Gets or sets the orientation of text in the header.
		/// </summary>
		/// <value>The orientation of text in the header, or null to use the default orientation.</value>
		/// <seealso cref="UltraGridOverride.ColumnHeaderTextOrientation"/>
		/// <seealso cref="UltraGridOverride.GroupHeaderTextOrientation"/>
        [LocalizedDescription("LD_Header_P_TextOrientation")]
        public TextOrientationInfo TextOrientation
		{
			get { return this.textOrientation; }
			set
			{
				if ( Object.Equals( this.textOrientation, value ) )
					return;

				this.textOrientation = value;
				this.NotifyPropChange( PropertyIds.TextOrientation );
			}
		}

		/// <summary>
		/// Returns true if the <see cref="HeaderBase.TextOrientation"/> property needs to be serialized.
		/// </summary>
		/// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeTextOrientation()
		{
			return this.textOrientation != null;
		}

		/// <summary>
		/// Resets the <see cref="HeaderBase.TextOrientation"/> property to its default value.
		/// </summary>
		public void ResetTextOrientation()
		{
			this.TextOrientation = null;
		}

		#endregion TextOrientation

		#region TextOrientationResolved

		/// <summary>
		/// Gets the resolved value of <see cref="HeaderBase.TextOrientation"/>.
		/// </summary> 
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public TextOrientationInfo TextOrientationResolved
		{
			get
			{
				if ( this.ShouldSerializeTextOrientation() )
					return this.TextOrientation;

				return this.DefaultTextOrientation;
			}
		}

		#endregion TextOrientationResolved 

		#endregion 8.2 - Rotated Column Headers

		// MD 12/8/08 - 9.1 - Column Pinning Right
		#region 9.1 - Column Pinning Right

		#region FixOnRight

		/// <summary>
		/// Gets or sets a value which indicates whether the column or group will be fixed to the right side of the grid when <see cref="Fixed"/> is True.
		/// </summary>
		/// <exception cref="InvalidEnumArgumentException">
		/// The assigned value is not defined in the <see cref="DefaultableBoolean"/> enumeration.
		/// </exception>
		/// <seealso cref="UltraGridOverride.FixHeadersOnRight"/>
		/// <seealso cref="FixOnRightResolved"/>
		public DefaultableBoolean FixOnRight
		{
			get { return this.fixOnRight; }
			set
			{
				if ( this.fixOnRight == value )
					return;

				if ( Enum.IsDefined( typeof( DefaultableBoolean ), value ) == false )
					throw new InvalidEnumArgumentException( "value", (int)value, typeof( DefaultableBoolean ) );

				this.fixOnRight = value;

				if ( null != this.Band )
					this.Band.BumpFixedHeadersVerifyVersion( true );

				this.NotifyPropChange( PropertyIds.FixOnRight );
			}
		}

		/// <summary>
		/// Returns true if the <see cref="HeaderBase.FixOnRight"/> property needs to be serialized.
		/// </summary>
		/// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFixOnRight()
		{
			return this.fixOnRight != DefaultableBoolean.Default;
		}

		/// <summary>
		/// Resets the <see cref="HeaderBase.FixOnRight"/> property to its default value.
		/// </summary>
		public void ResetFixOnRight()
		{
			this.FixOnRight = DefaultableBoolean.Default;
		}

		#endregion FixOnRight

		#region FixOnRightResolved

		/// <summary>
		/// Gets the resolved value which determines whether the column or group will be fixed on the right side of the grid when <see cref="Fixed"/> is True.
		/// </summary>
		/// <seealso cref="FixOnRight"/>
		/// <seealso cref="UltraGridOverride.FixHeadersOnRight"/>
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public bool FixOnRightResolved
		{
			get
			{
				if ( this.ShouldSerializeFixOnRight() )
					return this.FixOnRight == DefaultableBoolean.True;

				UltraGridBand band = this.Band;

				if ( band != null )
				{
					if ( band.HasOverride && band.Override.ShouldSerializeFixHeadersOnRight() )
						return band.Override.FixHeadersOnRight == DefaultableBoolean.True;

					if ( band.Layout.HasOverride && band.Layout.Override.ShouldSerializeFixHeadersOnRight() )
						return band.Layout.Override.FixHeadersOnRight == DefaultableBoolean.True;
				}

				return false;
			}
		}

		#endregion FixOnRightResolved 

		#endregion 9.1 - Column Pinning Right	

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout

        #region RowLayoutColumnInfo
        /// <summary>
        /// Returns the RowLayoutColumnInfo for the object (column or group) associated with the header.
        /// </summary>
        internal protected virtual RowLayoutColumnInfo RowLayoutColumnInfo
        {
            get
            {
                // MRS 4/27/2009 - TFS17076
                if (this.Column == null)
                    return null;

                return this.Column.RowLayoutColumnInfo;
            }
        }
        #endregion //RowLayoutColumnInfo

        #region RowLayoutColumnInfoProvider
        internal IProvideRowLayoutColumnInfo RowLayoutColumnInfoProvider
        {
            get
            {
                IProvideRowLayoutColumnInfo iProviderRowLayoutColumnInfo = this.Column as IProvideRowLayoutColumnInfo;
                if (iProviderRowLayoutColumnInfo == null)
                    iProviderRowLayoutColumnInfo = this.Group as IProvideRowLayoutColumnInfo;

                return iProviderRowLayoutColumnInfo;
            }
        }
        #endregion //RowLayoutColumnInfoProvider

        #region IProvideRowLayoutColumnInfo Members

        UltraGridBand IProvideRowLayoutColumnInfo.Band
        {
            get { return this.RowLayoutColumnInfoProvider.Band; }
        }

        RowLayoutColumnInfo IProvideRowLayoutColumnInfo.RowLayoutColumnInfo
        {
            get 
            {
                // MRS 4/27/2009 - TFS17076
                if (this.RowLayoutColumnInfo == null)
                    return null;

                return this.RowLayoutColumnInfoProvider.RowLayoutColumnInfo; 
            }
        }

        int IProvideRowLayoutColumnInfo.MinWidth
        {
            get { return this.RowLayoutColumnInfoProvider.MinWidth; }
        }

        Size IProvideRowLayoutColumnInfo.MinimumHeaderSize
        {
            get { return this.RowLayoutColumnInfoProvider.MinimumHeaderSize; }
        }

        Size IProvideRowLayoutColumnInfo.MinimumSize
        {
            get { return this.RowLayoutColumnInfoProvider.MinimumSize; }
        }

        Size IProvideRowLayoutColumnInfo.PreferredHeaderSize
        {
            get { return this.RowLayoutColumnInfoProvider.PreferredHeaderSize; }
        }

        Size IProvideRowLayoutColumnInfo.PreferredSize
        {
            get { return this.RowLayoutColumnInfoProvider.PreferredSize; }
        }

        IGridBagConstraint IProvideRowLayoutColumnInfo.HeaderConstraint
        {
            get { return this.RowLayoutColumnInfoProvider.HeaderConstraint; }
        }

        ILayoutItem IProvideRowLayoutColumnInfo.HeaderLayoutItem
        {
            get { return this.RowLayoutColumnInfoProvider.HeaderLayoutItem; }
        }

        ILayoutItem IProvideRowLayoutColumnInfo.ContentLayoutItem
        {
            get { return this.RowLayoutColumnInfoProvider.ContentLayoutItem; }
        }

        HeaderBase IProvideRowLayoutColumnInfo.Header
        {
            get { return this.RowLayoutColumnInfoProvider.Header; }
        }

        ILayoutItem IProvideRowLayoutColumnInfo.ResizeLayoutItem
        {
            get { return this.RowLayoutColumnInfoProvider.ResizeLayoutItem; }
        }

        #endregion // IProvideRowLayoutColumnInfo Members

        #region ILayoutChildItem Members

		// MD 2/17/09 - TFS14116
		// With the original fix for TFS13834, columns cannot be moved to new groups by default at design-time. The has been changed 
		// to allow for different behavior at run-time and design-time.
		//// MRS 2/13/2009 - TFS13834
		///// <summary>
		///// Returns whether the item is allowed to be moved to another group via drag / drop.
		///// </summary>        
		//bool ILayoutChildItem.AllowParentGroupChange
		//{
		//    get
		//    {
		//        if (this.Column != null)
		//            return ((ILayoutChildItem)this.Column).AllowParentGroupChange;
		//
		//        if (this.Group != null)
		//            return ((ILayoutChildItem)this.Group.Header).AllowParentGroupChange;
		//
		//        return false;
		//    }
		//}
        
        /// <summary>
        /// Returns the group associated with this LayoutItem, or null, if the layoutitem does not represent a group. 
        /// </summary>
        ILayoutGroup ILayoutChildItem.AssociatedGroup
        {
            get { return null; }
        }

        /// <summary>
        /// The ILayoutGroup which contains this ILayoutChildItem, or null if it is not contained in a group. 
        /// </summary>
        ILayoutGroup ILayoutChildItem.ParentGroup
        {
            get
            {
                return UltraGridGroup.GetLayoutGroup(this.Band, this);
            }
        }

        /// <summary>
        /// Returns the group into which an item will be dropped relative to this element.
        /// </summary>
        /// <param name="dropLocation"></param>
        /// <returns></returns>
        /// <remarks>
        /// This methods takes a DropLocation and determines what group an item dropped at that location 
        /// should be dropped into. If the item is a content, then the group is the parent group
        /// of that content. If the item is a group header, then the item may be dropped inside the group
        /// or into it's parent, depending on the DropLocation relative to the position of the header. 
        /// For example, if the group header is on top, dropping an item on the bottom of the header 
        /// should place that item inside the group while dropping the item to the left, right, or top
        /// of the header should place the item as a sibling of the group. 
        /// </remarks>
        ILayoutGroup ILayoutChildItem.GetDropGroup(GridBagLayoutDragStrategy.DropLocation dropLocation)
        {
            return ((ILayoutChildItem)this).ParentGroup;
        }        

        #endregion // ILayoutChildItem Members

        #endregion // NAS 9.1 - Groups in RowLayout        
    }


    internal enum PositionDimensions
    {
        Outside = 0,
        InsidePreRowSelectors = 1,
        InsideRowSelectors = 2,
        InsideRowBorders = 3,
        InsideCellSpacing = 4,
        InsideCellBorders = 5,
        InsideCellPadding = 6,
        CellTextAndPicture = 7
    };

    internal enum DimOriginBase
    {
        Absolute = 0, // relative to scrolling regions without regard to scroll position
        Normalized = 1,   // 0 based (to position item)
        OnScreen = 2,   // client coords of window
        Relative = 3    // to scrolling regions taking into effect scroll position	    
    };
}
