#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Diagnostics;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    /// The element that appears to the left of the horizontal
    /// scrollbar that is used for spliiting ColScrollRegions
    /// regions.
    /// </summary>
    public class ColSplitterBarUIElement : SplitterUIElement
    {

        private ColScrollRegion colScrollRegion;
		internal ColSplitterBarUIElement( UIElement parent )
            : base( parent, true )
        {
        }

		// SSP 1/5/04 UWG1606
		// Added appearances for the splitter bars.
		//
        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			UltraGridLayout layout = null != this.ColScrollRegion ? this.ColScrollRegion.Layout : null;
			if ( null != layout )
			{
				// SSP 3/13/06 - App Styling
				// 
				// ----------------------------------------------------------------------------------------------------
				//if ( layout.GetHasAppearance( UltraGridLayout.LayoutAppearanceIndex.SplitterBarVerticalAppearance ) )
				//	layout.SplitterBarVerticalAppearance.MergeData( ref appearance, ref requestedProps );
				layout.MergeLayoutAppearances( ref appearance, ref requestedProps, 
					UltraGridLayout.LayoutAppearanceIndex.SplitterBarVerticalAppearance, 
					StyleUtils.Role.ColScrollRegionSplitterBar, AppStyling.RoleState.Normal );
				// ----------------------------------------------------------------------------------------------------
			}
		}

		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b></b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="colScrollRegion">Associated <b>ColScrollRegion</b></param>
		public ColSplitterBarUIElement( UIElement parent, ColScrollRegion colScrollRegion )
            : this( parent )
        {
			this.initialize(colScrollRegion);
        }

		internal void initialize( ColScrollRegion colScrollRegion )
		{
			this.colScrollRegion    = colScrollRegion;
			this.PrimaryContext     = colScrollRegion;
		}


		/// <summary>
		/// Returns the ColScrollRegion object to which a UIElement belongs. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a ColScrollRegion object that can be used to set properties of, and invoke methods on, the colscrollregion to which the UIElement belongs. You can use this reference to access any of the returned colscrollregion's  properties or methods.</p>
		/// <p class="body">If the UIElement does not belong to a colscrollregion, Nothing is returned.</p>
		/// <p class="body">The <b>RowScrollRegion</b> property can be used to return a reference to a RowScrollRegion object to which a UIElement belongs.</p>
		/// </remarks>
        public ColScrollRegion ColScrollRegion
        {
            get
            {
                return this.colScrollRegion;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.colScrollRegion.Layout.BorderStyleSplitter;
			}
		}

		/// <summary>
        /// Returns true if the element can be moved or resized horizontally
        /// </summary>
        /// <param name="point">In client coordinates</param>
        public override bool SupportsLeftRightAdjustmentsFromPoint( System.Drawing.Point point )
        {
			//RobA UWG524 10/15/01 
			//implemented this method

			int count = this.ColScrollRegion.Layout.ColScrollRegions.Count;
			int index = this.ColScrollRegion.Index;

			//if this ColScrollRegion and the ColScrollRegion to the right are both 
			//fixed, return false
			//
			if ( this.ColScrollRegion.SizingMode == SizingMode.Fixed &&
				 this.ColScrollRegion.Layout.ColScrollRegions[index + 1].SizingMode == SizingMode.Fixed )
				return false;

			//if any of the ColScrollRegions to the right are free
			//and there is a ColScrollRegion to the left that is free
			//return true, otherwise return false
			//
			for ( int i = index + 1; i < count ; ++i )
			{
				if ( this.ColScrollRegion.Layout.ColScrollRegions[i].SizingMode == SizingMode.Free )					
				{
					for ( int j = index; j >= 0; --j )
						if ( this.ColScrollRegion.Layout.ColScrollRegions[j].SizingMode == SizingMode.Free )
							return true;				
				}
			}

			return false;
		}

        /// <summary>
        /// Called after a move/resize operation. 
        /// </summary>
        /// <param name="delta">The delta</param>
        public override void ApplyAdjustment(Point delta)
        {
            // call OnResize 
            //
            if ( 0 != delta.X )
			{
				//RobA UWG524 10/16/01 
				//rewrote this method

				//if this region is fixed then walk to the left and resize each region
				//until you reach a free sizing region
				//
				if ( this.colScrollRegion.SizingMode == SizingMode.Fixed )
				{
					for ( int i = this.colScrollRegion.Index; i>=0; --i )
					{
						if ( this.colScrollRegion.Layout.ColScrollRegions[i].SizingMode == SizingMode.Free )
						{			
							this.colScrollRegion.Layout.ColScrollRegions.OnResize( this.colScrollRegion.Layout.ColScrollRegions[i], delta.X );
							break;
						}
						this.colScrollRegion.Layout.ColScrollRegions.OnResize( this.colScrollRegion.Layout.ColScrollRegions[i], delta.X );					
					}
				}

				else
	               this.colScrollRegion.Layout.ColScrollRegions.OnResize( this.colScrollRegion, delta.X );
			
				//walk to the right of this colScrollRegion resizing each region
				//until you reach a free sizing colScrollRegion. 
				//
				for ( int i = this.colScrollRegion.Index + 1; i < this.colScrollRegion.Layout.ColScrollRegions.Count; ++i )
				{
					if ( this.colScrollRegion.Layout.ColScrollRegions[i].SizingMode == SizingMode.Free )
						return;
					
					this.colScrollRegion.Layout.ColScrollRegions.OnResize( this.colScrollRegion.Layout.ColScrollRegions[i], delta.X );
				}											
			
					
			}           
        }


		// SSP 8/16/01 UWG133
		// We need to exit the edit mode when user pressed the mouse on the
		// split box for resizing the region
        /// <summary>
        /// Called when the mouse down message is received over the element. 
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        /// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
        /// <returns>If true then bypass default processing</returns>
        protected override bool OnMouseDown( MouseEventArgs e, 
										 bool adjustableArea,
										 ref UIElement captureMouseForElement )
		{
			bool ret = base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );

			if ( null != this.ColScrollRegion )
			{
				// exit edit mode when user clicks on the splitter bar
				if ( null != this.ColScrollRegion.Layout.ActiveCell )
				{				
					this.ColScrollRegion.Layout.ActiveCell.ExitEditMode();
				}
			}

			return ret;
		}

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return null != this.colScrollRegion 
					? StyleUtils.GetRole( this.colScrollRegion.Layout, StyleUtils.Role.ColScrollRegionSplitterBar )
					: null;
			}
		}

		#endregion // UIRole
    }
}
