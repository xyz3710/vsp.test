﻿namespace xGridCustomColumns
{
	partial class CustomColumns
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.xGridAddGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.xGridReportGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.xbtnAdd = new Infragistics.Win.Misc.UltraButton();
			this.xbtnReport = new Infragistics.Win.Misc.UltraButton();
			this.tlpMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xGridAddGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xGridReportGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 4;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tlpMain.Controls.Add(this.xGridAddGrid, 0, 0);
			this.tlpMain.Controls.Add(this.xGridReportGrid, 2, 0);
			this.tlpMain.Controls.Add(this.xbtnAdd, 0, 1);
			this.tlpMain.Controls.Add(this.xbtnReport, 1, 1);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 2;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
			this.tlpMain.Size = new System.Drawing.Size(905, 449);
			this.tlpMain.TabIndex = 0;
			// 
			// xGridAddGrid
			// 
			this.tlpMain.SetColumnSpan(this.xGridAddGrid, 2);
			appearance1.BackColor = System.Drawing.SystemColors.Window;
			appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.xGridAddGrid.DisplayLayout.Appearance = appearance1;
			this.xGridAddGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xGridAddGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance2.BorderColor = System.Drawing.SystemColors.Window;
			this.xGridAddGrid.DisplayLayout.GroupByBox.Appearance = appearance2;
			appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xGridAddGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
			this.xGridAddGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xGridAddGrid.DisplayLayout.GroupByBox.Hidden = true;
			appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance4.BackColor2 = System.Drawing.SystemColors.Control;
			appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xGridAddGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
			this.xGridAddGrid.DisplayLayout.MaxColScrollRegions = 1;
			this.xGridAddGrid.DisplayLayout.MaxRowScrollRegions = 1;
			appearance5.BackColor = System.Drawing.SystemColors.Window;
			appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.xGridAddGrid.DisplayLayout.Override.ActiveCellAppearance = appearance5;
			appearance6.BackColor = System.Drawing.SystemColors.Highlight;
			appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.xGridAddGrid.DisplayLayout.Override.ActiveRowAppearance = appearance6;
			this.xGridAddGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.xGridAddGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance7.BackColor = System.Drawing.SystemColors.Window;
			this.xGridAddGrid.DisplayLayout.Override.CardAreaAppearance = appearance7;
			appearance8.BorderColor = System.Drawing.Color.Silver;
			appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.xGridAddGrid.DisplayLayout.Override.CellAppearance = appearance8;
			this.xGridAddGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xGridAddGrid.DisplayLayout.Override.CellPadding = 0;
			appearance9.BackColor = System.Drawing.SystemColors.Control;
			appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance9.BorderColor = System.Drawing.SystemColors.Window;
			this.xGridAddGrid.DisplayLayout.Override.GroupByRowAppearance = appearance9;
			appearance10.TextHAlignAsString = "Left";
			this.xGridAddGrid.DisplayLayout.Override.HeaderAppearance = appearance10;
			this.xGridAddGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xGridAddGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance11.BackColor = System.Drawing.SystemColors.Window;
			appearance11.BorderColor = System.Drawing.Color.Silver;
			this.xGridAddGrid.DisplayLayout.Override.RowAppearance = appearance11;
			this.xGridAddGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
			this.xGridAddGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
			this.xGridAddGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.xGridAddGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.xGridAddGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.xGridAddGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xGridAddGrid.Location = new System.Drawing.Point(3, 3);
			this.xGridAddGrid.Name = "xGridAddGrid";
			this.xGridAddGrid.Size = new System.Drawing.Size(446, 408);
			this.xGridAddGrid.TabIndex = 0;
			this.xGridAddGrid.Text = "Add Grid";
			this.xGridAddGrid.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.xGridAddGrid_InitializeRow);
			this.xGridAddGrid.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.xGridAddGrid_InitializeLayout);
			this.xGridAddGrid.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.xGridAddGrid_AfterRowUpdate);
			// 
			// xGridReportGrid
			// 
			this.tlpMain.SetColumnSpan(this.xGridReportGrid, 2);
			appearance13.BackColor = System.Drawing.SystemColors.Window;
			appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.xGridReportGrid.DisplayLayout.Appearance = appearance13;
			this.xGridReportGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xGridReportGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance14.BorderColor = System.Drawing.SystemColors.Window;
			this.xGridReportGrid.DisplayLayout.GroupByBox.Appearance = appearance14;
			appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xGridReportGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
			this.xGridReportGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xGridReportGrid.DisplayLayout.GroupByBox.Hidden = true;
			appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance16.BackColor2 = System.Drawing.SystemColors.Control;
			appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xGridReportGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
			this.xGridReportGrid.DisplayLayout.MaxColScrollRegions = 1;
			this.xGridReportGrid.DisplayLayout.MaxRowScrollRegions = 1;
			appearance17.BackColor = System.Drawing.SystemColors.Window;
			appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
			this.xGridReportGrid.DisplayLayout.Override.ActiveCellAppearance = appearance17;
			appearance18.BackColor = System.Drawing.SystemColors.Highlight;
			appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.xGridReportGrid.DisplayLayout.Override.ActiveRowAppearance = appearance18;
			this.xGridReportGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.xGridReportGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance19.BackColor = System.Drawing.SystemColors.Window;
			this.xGridReportGrid.DisplayLayout.Override.CardAreaAppearance = appearance19;
			appearance20.BorderColor = System.Drawing.Color.Silver;
			appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.xGridReportGrid.DisplayLayout.Override.CellAppearance = appearance20;
			this.xGridReportGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xGridReportGrid.DisplayLayout.Override.CellPadding = 0;
			appearance21.BackColor = System.Drawing.SystemColors.Control;
			appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance21.BorderColor = System.Drawing.SystemColors.Window;
			this.xGridReportGrid.DisplayLayout.Override.GroupByRowAppearance = appearance21;
			appearance22.TextHAlignAsString = "Left";
			this.xGridReportGrid.DisplayLayout.Override.HeaderAppearance = appearance22;
			this.xGridReportGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xGridReportGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance23.BackColor = System.Drawing.SystemColors.Window;
			appearance23.BorderColor = System.Drawing.Color.Silver;
			this.xGridReportGrid.DisplayLayout.Override.RowAppearance = appearance23;
			this.xGridReportGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
			this.xGridReportGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
			this.xGridReportGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.xGridReportGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.xGridReportGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.xGridReportGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xGridReportGrid.Location = new System.Drawing.Point(455, 3);
			this.xGridReportGrid.Name = "xGridReportGrid";
			this.xGridReportGrid.Size = new System.Drawing.Size(447, 408);
			this.xGridReportGrid.TabIndex = 1;
			this.xGridReportGrid.Text = "Report Grid";
			// 
			// xbtnAdd
			// 
			this.xbtnAdd.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xbtnAdd.Location = new System.Drawing.Point(3, 417);
			this.xbtnAdd.Name = "xbtnAdd";
			this.xbtnAdd.Size = new System.Drawing.Size(220, 29);
			this.xbtnAdd.TabIndex = 2;
			this.xbtnAdd.Text = "Add";
			
			// 
			// xbtnReport
			// 
			this.xbtnReport.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xbtnReport.Location = new System.Drawing.Point(229, 417);
			this.xbtnReport.Name = "xbtnReport";
			this.xbtnReport.Size = new System.Drawing.Size(220, 29);
			this.xbtnReport.TabIndex = 3;
			this.xbtnReport.Text = "Reporting";
			this.xbtnReport.Click += new System.EventHandler(this.xbtnReport_Click);
			// 
			// CustomColumns
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(905, 449);
			this.Controls.Add(this.tlpMain);
			this.Name = "CustomColumns";
			this.Text = "Custom Columns";
			this.tlpMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xGridAddGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xGridReportGrid)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private Infragistics.Win.UltraWinGrid.UltraGrid xGridAddGrid;
		private Infragistics.Win.UltraWinGrid.UltraGrid xGridReportGrid;
		private Infragistics.Win.Misc.UltraButton xbtnReport;
		private Infragistics.Win.Misc.UltraButton xbtnAdd;
	}
}

