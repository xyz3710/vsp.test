﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System.Collections;

namespace xGridCustomColumns
{
	internal enum Branch
	{
		/// <summary>
		/// Army = 육군, Navy = 해군, AirForce = 공군
		/// </summary>
		Army,
		Navy,
		AirForce
	}

	public partial class CustomColumns : Form
	{
		#region Data

		private DataTable _addGridDataTable;
		private DataTable _reportGridDataTable;
		private uint _rowCounter;
		private bool _addSwitch;
		#endregion


		#region Constructor

		public CustomColumns()
		{
			InitializeComponent();

			TableSet();
			GridSet();

			DesignGrid(ref xGridAddGrid);
			DesignGrid(ref xGridReportGrid);

		}
		#endregion


		#region Events

		private void xGridAddGrid_InitializeLayout(object sender, InitializeLayoutEventArgs e)
		{
			e.Layout.Override.AllowAddNew = AllowAddNew.FixedAddRowOnTop;
			e.Layout.Override.TemplateAddRowPrompt = "데이터를 추가해 주세요";
		}

		private void xGridAddGrid_AfterRowUpdate(object sender, RowEventArgs e)
		{
			_rowCounter = (uint)(xGridAddGrid.Rows.Count) + 1;		
		}

		private void xGridAddGrid_InitializeRow(object sender, InitializeRowEventArgs e)
		{
			EmbeddableEditorBase editor;
			DefaultEditorOwnerSettings editorSettings;
			ValueList valueList;

			// 행 추가시 적절한 Row Counter를 주기 위한 조건처리
			if (e.Row.Cells["No"].Value is uint)
			{
			}
			else if (_rowCounter != 0)
			{
				e.Row.Cells["No"].Value = _rowCounter;

			}
			else
			{
				e.Row.Cells["No"].Value = 1;
				_rowCounter = 1;
			}

			// "이름" 컬럼 설정. SingleLine Text형식
			valueList = new ValueList();
			editorSettings = new DefaultEditorOwnerSettings();
			editorSettings.DataType = typeof(string);
			editor = new EditorWithText(new DefaultEditorOwner(editorSettings));

			e.Row.Cells["이름"].Editor = editor;

			// "성별" 컬럼 설정. RadioButton형식
			valueList = new ValueList();
			editorSettings = new DefaultEditorOwnerSettings();
			editorSettings.DataType = typeof(string);
			valueList.ValueListItems.Add("남자", "남자");
			valueList.ValueListItems.Add("여자", "여자");
			editorSettings.ValueList = valueList;
			editor = new OptionSetEditor(new DefaultEditorOwner(editorSettings));

			e.Row.Cells["성별"].Editor = editor;

			// "병과" 컬럼 설정. ComboBox형식
			valueList = new ValueList();
			editorSettings = new DefaultEditorOwnerSettings();
			editorSettings.DataType = typeof(Branch);
			valueList.ValueListItems.Add(Branch.Army, "육군");
			valueList.ValueListItems.Add(Branch.Navy, "해군");
			valueList.ValueListItems.Add(Branch.AirForce, "공군");
			editorSettings.ValueList = valueList;
			editor = new EditorWithCombo(new DefaultEditorOwner(editorSettings));

			e.Row.Cells["병과"].Editor = editor;

			// "비고" 컬럼 설정. MultiLine Text형식
			valueList = new ValueList();
			editorSettings = new DefaultEditorOwnerSettings();
			editorSettings.DataType = typeof(string);
			editorSettings.ValueList = valueList;
			editor = new EditorWithText(new DefaultEditorOwner(editorSettings));

			e.Row.Cells["비고"].Editor = editor;
		}

		private void xbtnReport_Click(object sender, EventArgs e)
		{
			if (xGridAddGrid.Rows.Count >= 1)
			{
				_reportGridDataTable.Rows.Clear();
				foreach (UltraGridRow xrow in xGridAddGrid.Rows)
				{
					DataRow reportRow = _reportGridDataTable.NewRow();
					foreach (UltraGridCell xcell in xrow.Cells)
						reportRow[xcell.Column.Header.Caption] = xcell.Value;
					_reportGridDataTable.Rows.Add(reportRow);
				}
			}
		}
		#endregion
		

		#region CustomMethod

		private void TableSet()
		{
			_addGridDataTable = new DataTable("HR");
			_addGridDataTable.Columns.Add("No", typeof(uint));
			_addGridDataTable.Columns.Add("이름", typeof(string));
			_addGridDataTable.Columns.Add("성별", typeof(string));
			_addGridDataTable.Columns.Add("병과", typeof(Branch));
			_addGridDataTable.Columns.Add("비고", typeof(string));

			_addGridDataTable.PrimaryKey = new DataColumn[] { _addGridDataTable.Columns["No"] };

			_reportGridDataTable = new DataTable("HR");
			_reportGridDataTable.Columns.Add("No", typeof(uint));
			_reportGridDataTable.Columns.Add("이름", typeof(string));
			_reportGridDataTable.Columns.Add("성별", typeof(string));
			_reportGridDataTable.Columns.Add("병과", typeof(Branch));
			_reportGridDataTable.Columns.Add("비고", typeof(string));

			_reportGridDataTable.PrimaryKey = new DataColumn[] { _reportGridDataTable.Columns["No"] };
		}

		private void GridSet()
		{
			xGridAddGrid.DataSource = _addGridDataTable;
			xGridReportGrid.DataSource = _reportGridDataTable;

			xGridAddGrid.DisplayLayout.Bands[0].Columns[0].CellClickAction = CellClickAction.CellSelect;
			xGridReportGrid.Rows.Band.Override.CellClickAction = CellClickAction.RowSelect;
		}

		void DesignGrid(ref UltraGrid xgrid)
		{
			// UltraGrid의 대략적인 디자인

			Infragistics.Win.Appearance xcolAppr = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance xrowAppr = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance xrowAltAppr = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance xactiveRowAppr = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance xeditCellAppr = new Infragistics.Win.Appearance();

			xcolAppr.BackColor = Color.DarkSlateGray;
			xcolAppr.BackColor2 = Color.Silver;
			xcolAppr.BackGradientStyle = GradientStyle.ForwardDiagonal;
			xcolAppr.ForeColor = Color.WhiteSmoke;
			xcolAppr.TextHAlign = HAlign.Center;
			xgrid.DisplayLayout.Override.HeaderAppearance = xcolAppr;

			xrowAppr.BackColor = Color.GhostWhite;
			xrowAppr.TextHAlign = HAlign.Center;
			xrowAppr.BorderColor = Color.DarkSlateGray;
			xgrid.DisplayLayout.Override.RowAppearance = xrowAppr;

			xrowAltAppr.BackColor = Color.LightGray;
			xrowAltAppr.BorderColor2 = Color.GhostWhite;
			xrowAltAppr.BackGradientStyle = GradientStyle.ForwardDiagonal;
			xrowAltAppr.TextHAlign = HAlign.Center;
			xrowAltAppr.BorderColor = Color.DarkSlateGray;
			xgrid.DisplayLayout.Override.RowAlternateAppearance = xrowAltAppr;

			xactiveRowAppr.BackColor = Color.SkyBlue;
			xactiveRowAppr.BackColor2 = Color.SeaShell;
			xactiveRowAppr.BackGradientStyle = GradientStyle.ForwardDiagonal;
			xactiveRowAppr.TextHAlign = HAlign.Center;
			xgrid.DisplayLayout.Override.ActiveRowAppearance = xactiveRowAppr;

			xgrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;


			// "비고" 컬럼의 MultiLine 설정
			UltraGridBand xband = xgrid.DisplayLayout.Bands[0];
			RowLayoutColumnInfo xinfo = xband.Columns["비고"].RowLayoutColumnInfo;

			xband.UseRowLayout = true;
			xband.Columns["비고"].CellMultiLine = DefaultableBoolean.True;

			xinfo.PreferredCellSize = new Size(100, 32);
		}
		#endregion
	}
}