﻿namespace ColumnChart02
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect2 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
			this.xChart = new Infragistics.Win.UltraWinChart.UltraChart();
			((System.ComponentModel.ISupportInitialize)(this.xChart)).BeginInit();
			this.SuspendLayout();
			// 
			// xChart
			// 
			this.xChart.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
			this.xChart.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChart.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
			this.xChart.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChart.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.X.LineThickness = 1;
			this.xChart.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.X.MajorGridLines.Visible = true;
			this.xChart.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.X.MinorGridLines.Visible = false;
			this.xChart.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.X.Visible = true;
			this.xChart.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChart.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
			this.xChart.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChart.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.X2.LineThickness = 1;
			this.xChart.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.X2.MajorGridLines.Visible = true;
			this.xChart.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.X2.MinorGridLines.Visible = false;
			this.xChart.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.X2.Visible = false;
			this.xChart.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChart.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			this.xChart.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChart.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Y.LineThickness = 1;
			this.xChart.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Y.MajorGridLines.Visible = true;
			this.xChart.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Y.MinorGridLines.Visible = false;
			this.xChart.Axis.Y.TickmarkInterval = 10;
			this.xChart.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.Y.Visible = true;
			this.xChart.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChart.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			this.xChart.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChart.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Y2.LineThickness = 1;
			this.xChart.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Y2.MajorGridLines.Visible = true;
			this.xChart.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Y2.MinorGridLines.Visible = false;
			this.xChart.Axis.Y2.TickmarkInterval = 10;
			this.xChart.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.Y2.Visible = false;
			this.xChart.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChart.Axis.Z.Labels.ItemFormatString = "";
			this.xChart.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Z.LineThickness = 1;
			this.xChart.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Z.MajorGridLines.Visible = true;
			this.xChart.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Z.MinorGridLines.Visible = false;
			this.xChart.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.Z.Visible = false;
			this.xChart.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChart.Axis.Z2.Labels.ItemFormatString = "";
			this.xChart.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Z2.LineThickness = 1;
			this.xChart.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Z2.MajorGridLines.Visible = true;
			this.xChart.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Z2.MinorGridLines.Visible = false;
			this.xChart.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.Z2.Visible = false;
			this.xChart.ColorModel.AlphaLevel = ((byte)(150));
			this.xChart.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
			this.xChart.Effects.Effects.Add(gradientEffect2);
			this.xChart.Location = new System.Drawing.Point(12, 12);
			this.xChart.Name = "xChart";
			this.xChart.Size = new System.Drawing.Size(818, 375);
			this.xChart.TabIndex = 0;
			this.xChart.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
			this.xChart.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
			this.xChart.Tooltips.TooltipControl = null;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(842, 411);
			this.Controls.Add(this.xChart);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.xChart)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Infragistics.Win.UltraWinChart.UltraChart xChart;
	}
}

