﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.UltraChart.Resources.Appearance;
using System.Collections;
using Infragistics.UltraChart.Shared.Styles;
using Infragistics.UltraChart.Core.Layers;
using Infragistics.Win.UltraWinChart;

namespace Stack3DColumnChart01
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			createChart();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		public DataSet GetRawTats(DateTime startDate, DateTime endDate)
		{
			string filePath = @"C:\iDASiT\Demonstration\iCube\iCube.iDASiT\App_Data\Tats\";
			string target = "tat";
			DataSet ds = new DataSet(target);
			DataTable dtChart = new DataTable("Tat");
			DataRow[] dr = null;

			ds.ReadXmlSchema(string.Format("{0}{1}.xsd", filePath, target));
			ds.ReadXml(string.Format("{0}{1}.xml", filePath, target), XmlReadMode.ReadSchema);

			if (ds.Tables.Count == 1)
			{
				dr = ds.Tables[0].Select(string.Format("prodDate >= '{0}' AND prodDate <= '{1}'", startDate.ToShortDateString(), endDate.ToShortDateString()));

				// 컬럼 추가
				dtChart.Columns.Add("ProductId", typeof(string));
				dtChart.Columns.Add("ProdDate", typeof(DateTime));
				dtChart.Columns.Add("OperationId", typeof(string));
				dtChart.Columns.Add("Output", typeof(double));
				dtChart.Columns.Add("Lot", typeof(double));
				dtChart.Columns.Add("Tat", typeof(double));

				// 검색된 data만 추가
				for (int i = 0; i < dr.Length; i++)
				{
					object[] data = new object[] { dr[i]["ProductId"], dr[i]["ProdDate"], dr[i]["OperationId"], dr[i]["Output"], dr[i]["Lot"], dr[i]["Tat"] };

					dtChart.Rows.Add(data);
				}
			}

			DataSet dsResult = new DataSet();

			dsResult.Tables.Add(dtChart);

			return dsResult;
		}

		public DataTable GetTats(DateTime startDate, DateTime endDate)
		{
			DataSet ds = GetRawTats(startDate, endDate);
			DataTable dtChart = new DataTable("Tat");

			if (ds.Tables.Count == 1)
			{
				DataRow[] dr = null;
				ArrayList arProduct = new ArrayList();
				string[] operations = new string[] { "1000", "1100", "1200", "1300", "1330", "1350", "1400", "1450", "2000", "2100", "2200", "3000", "3100" };				
				int sampleDays = (int)(endDate.ToOADate() - startDate.ToOADate() + 1);

				// 제품 목록 구하기
				for (int i = 0; i < sampleDays; i++)
                {
					DateTime seekDate = startDate.AddDays(i);

                	dr = ds.Tables[0].Select(string.Format("prodDate = '{0}' AND operationId = '{1}'", seekDate.ToShortDateString(), operations[0]));

					if (dr != null)
						break;
                }

				foreach (DataRow row in dr)
					arProduct.Add((string)row["ProductId"]);

				string[] products = (string[])arProduct.ToArray(typeof(string));

				// chart 용 Table Column 추가
				dtChart.Columns.Add("ProdDate", typeof(string));

				for (int i = 0; i < products.Length; i++)
					dtChart.Columns.Add(products[i], typeof(double));

				// DataTable에 생산 일자별, 제품별 Output의 SUM을 구한다.
				ArrayList arRow = new ArrayList();
				double summary = 0.0;

				for (int i = 0; i < sampleDays; i++)
				{
					summary = 0.0;
					arRow.Add(startDate.AddDays(i).ToShortDateString());

					for (int j = 0; j < products.Length; j++)
					{
						summary = (double)ds.Tables[0].Compute("SUM(Output)",
							string.Format("prodDate='{0}' AND productId='{1}'", startDate.AddDays(i), products[j]));

						arRow.Add(summary);
					}

					dtChart.Rows.Add((object[])arRow.ToArray(typeof(object)));
					arRow.Clear();
				}
			}

			return dtChart;
		}

		private DateTime _startDate = DateTime.Parse("2007-03-01");
		private DateTime _endDate = DateTime.Parse("2007-03-15");

		private void createChart()
		{
			UltraChart xChart = new UltraChart();

			xChart.ChartType = ChartType.CylinderStackColumnChart3D;
			xChart.ColorModel.ModelStyle = ColorModels.Office2007Style;
			xChart.Transform3D.XRotation = 120;
			xChart.Transform3D.YRotation = 0;
			xChart.Transform3D.ZRotation = 0;
			xChart.Border.Color = System.Drawing.Color.SlateBlue;
			xChart.Border.Thickness = 1;
			xChart.Border.CornerRadius = 10;
	
			AxisItem axisX = new AxisItem();
			AxisItem axisY1 = new AxisItem();
			AxisItem axisY2 = new AxisItem();
			AxisItem axisZ = new AxisItem();
	
			axisX.Labels.Orientation = TextOrientation.VerticalLeftFacing;
			axisX.Extent = 12;
	
			axisY1.OrientationType = AxisNumber.Y_Axis;
			axisY1.DataType = AxisDataType.Numeric;
			axisY1.Labels.ItemFormatString = "<DATA_VALUE:#,##0>";
			axisY1.Labels.Layout.Behavior = AxisLabelLayoutBehaviors.Auto;
			axisY1.TickmarkStyle = AxisTickStyle.Smart;
			axisY1.Extent = 32;
	
			axisY2.OrientationType = AxisNumber.Y2_Axis;
			axisY2.DataType = AxisDataType.Numeric;
			axisY2.Labels.ItemFormatString = "<DATA_VALUE:#0.##>";
			axisY2.Labels.Layout.Behavior = AxisLabelLayoutBehaviors.Auto;
			axisY2.TickmarkStyle = AxisTickStyle.Smart;
			axisY2.Visible = false;
	
			axisZ.OrientationType = AxisNumber.Z_Axis;
			axisZ.DataType = AxisDataType.String;
			axisZ.SetLabelAxisType = SetLabelAxisType.GroupBySeries;
			axisZ.Labels.ItemFormatString = "<ITEM_LABEL>";
	
			xChart.Axis.X = axisX;
			xChart.Axis.Y = axisY1;
			xChart.Axis.Y2 = axisY2;
			xChart.Axis.Z = axisZ;
	
			LegendAppearance legend = new LegendAppearance();
	
			legend.Location = LegendLocation.Right;
			legend.Margins.Top = 10;
			legend.Margins.Right = 10;
			legend.SpanPercentage = 10;
			legend.Visible = true;
	
			xChart.Legend = legend;

			DataTable table = GetTats(_startDate, _endDate);
			xChart.DataSource = table;
			xChart.DataBind();

			Controls.Add(xChart);
			xChart.Dock = System.Windows.Forms.DockStyle.Fill;

			xChart.DeploymentScenario.RenderingType = RenderingType.Flash;

//			xChart.DataSource = GetTats(_startDate, _endDate);
//			xChart.DataBind();

			#region Test
			/*
			this.xChart.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
			this.xChart.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChart.Axis.X.Labels.ItemFormatString = "";
			this.xChart.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChart.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.X.Labels.SeriesLabels.FormatString = "";
			this.xChart.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChart.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChart.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.X.LineThickness = 1;
			this.xChart.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.X.MajorGridLines.Visible = true;
			this.xChart.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.X.MinorGridLines.Visible = false;
			this.xChart.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.X.Visible = true;
			this.xChart.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChart.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
			this.xChart.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChart.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.X2.Labels.SeriesLabels.FormatString = "";
			this.xChart.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChart.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChart.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.X2.LineThickness = 1;
			this.xChart.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.X2.MajorGridLines.Visible = true;
			this.xChart.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.X2.MinorGridLines.Visible = false;
			this.xChart.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.X2.Visible = true;
			this.xChart.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChart.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			this.xChart.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.Y.Labels.SeriesLabels.FormatString = "";
			this.xChart.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChart.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Y.LineThickness = 1;
			this.xChart.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Y.MajorGridLines.Visible = true;
			this.xChart.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Y.MinorGridLines.Visible = false;
			this.xChart.Axis.Y.TickmarkInterval = 20;
			this.xChart.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.Y.Visible = true;
			this.xChart.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChart.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			this.xChart.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.Y2.Labels.SeriesLabels.FormatString = "";
			this.xChart.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChart.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Y2.LineThickness = 1;
			this.xChart.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Y2.MajorGridLines.Visible = true;
			this.xChart.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Y2.MinorGridLines.Visible = false;
			this.xChart.Axis.Y2.TickmarkInterval = 20;
			this.xChart.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.Y2.Visible = true;
			this.xChart.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChart.Axis.Z.Labels.ItemFormatString = "<ITEM_LABEL>";
			this.xChart.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChart.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChart.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Z.LineThickness = 1;
			this.xChart.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Z.MajorGridLines.Visible = true;
			this.xChart.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Z.MinorGridLines.Visible = false;
			this.xChart.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.Z.Visible = true;
			this.xChart.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChart.Axis.Z2.Labels.ItemFormatString = "";
			this.xChart.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChart.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChart.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChart.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChart.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChart.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChart.Axis.Z2.LineThickness = 1;
			this.xChart.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChart.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Z2.MajorGridLines.Visible = true;
			this.xChart.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChart.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChart.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChart.Axis.Z2.MinorGridLines.Visible = false;
			this.xChart.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChart.Axis.Z2.Visible = true;
			this.xChart.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.Stack3DColumnChart;
			this.xChart.ColorModel.AlphaLevel = ((byte)(255));
			this.xChart.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
//			this.xChart.Data.DataMember = "Tat";
//			this.xChart.DataMember = "Tat";
//			this.xChart.DataSource = GetTats(_startDate, _endDate);
			this.xChart.Legend.Location = Infragistics.UltraChart.Shared.Styles.LegendLocation.Bottom;
			this.xChart.Legend.Visible = true;
			this.xChart.Location = new System.Drawing.Point(12, 12);
			this.xChart.Name = "xChart";
			this.xChart.Size = new System.Drawing.Size(813, 373);
			this.xChart.TabIndex = 0;
			this.xChart.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
			this.xChart.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
			this.xChart.Tooltips.TooltipControl = null;
			*/
			#endregion
		}

		private NumericSeries getColumnSeries()
		{
			NumericSeries columnSeries = new NumericSeries();

			columnSeries.Data.DataSource = GetTats(_startDate, _endDate);
			columnSeries.Data.LabelColumn = "ProdDate";
			columnSeries.Data.ValueColumn = "CHINA";

			return columnSeries;
		}

		private NumericSeries getSplineSeries()
		{
			NumericSeries splineSeries = new NumericSeries();

			splineSeries.Data.DataSource = GetTats(_startDate, _endDate);
			splineSeries.Data.LabelColumn = "ProdDate";
			splineSeries.Data.ValueColumn = "EVER";

			return splineSeries;
		}
/*
		private DataTable getDataTable(DataTypePreFix dataTypePrefix)
		{
			//ReportFactory reportFactory = new ReportFactory();
			DataTable rawData = GetTats(_startDate, _startDate, false);
			//reportFactory.GetTats(DateTime.Parse(hfStartDate.Value), DateTime.Parse(hfEndDate.Value), false);
			DataTable retTable = new DataTable(dataTypePrefix.ToString());

			// TAT, Output용 Table Column 추가
			retTable.Columns.Add("OperationId", typeof(string));
			retTable.Columns.Add("Value", typeof(double));

			object[] value = new object[retTable.Columns.Count];
			string operationId = string.Empty;
			int rowNum = 0;

			switch (dataTypePrefix)
			{
				case DataTypePreFix.TAT:
					rowNum = 0;

					break;
				case DataTypePreFix.Output:
					rowNum = 1;

					break;
			}

			// 각 공정별로 Prefix에 대한 SUM을 한다
			for (int i = 2; i < rawData.Columns.Count; i++)  // ProductId, Distinct 컬럼 제외
			{
				operationId = rawData.Columns[i].ColumnName;

				value[0] = operationId;
				value[1] = rawData.Rows[rowNum][i];

				retTable.Rows.Add(value);
			}

			return retTable;
		}
*/
	}
}