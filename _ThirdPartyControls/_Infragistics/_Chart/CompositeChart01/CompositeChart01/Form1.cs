﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.UltraChart.Shared.Styles;
using Infragistics.UltraChart.Resources.Appearance;
using Infragistics.UltraChart.Core.Layers;

namespace ComopositeChart
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private DataTable getData()
		{
			DataTable table = new DataTable();

			table.Columns.Add("Label Column", typeof(string));
			table.Columns.Add("Value Column", typeof(double));
			table.Columns.Add("Another Value Column", typeof(double));

			table.Rows.Add(new object[] { "Point A", 1.0, 3.0 });
			table.Rows.Add(new object[] { "Point B", 2.0, 2.0 });
			table.Rows.Add(new object[] { "Point C", 3.0, 1.0 });
			table.Rows.Add(new object[] { "Point D", 4.0, 2.0 });
			table.Rows.Add(new object[] { "Point E", 5.0, 3.0 });

			return table; 
		}

		private void button1_Click(object sender, EventArgs e)
		{
			// composite type으로 선언
			ultraChart1.ChartType = ChartType.Composite;

			// 1. Add Chart area 
			ChartArea myChartArea = new ChartArea();

			ultraChart1.CompositeChart.ChartAreas.Add(myChartArea);

			// 2. Add some Axes to the ChartArea
			AxisItem axisX = new AxisItem();

			axisX.OrientationType = AxisNumber.X_Axis;
			axisX.DataType = AxisDataType.String;
			axisX.SetLabelAxisType = SetLabelAxisType.GroupBySeries;
			axisX.Labels.ItemFormatString = "<ITEM_LABEL>";
			axisX.Labels.Orientation = TextOrientation.VerticalLeftFacing;

			AxisItem axisY = new AxisItem();

			axisY.OrientationType = AxisNumber.Y_Axis;
			axisY.DataType = AxisDataType.Numeric;
			axisY.Labels.ItemFormatString = "<DATA_VALUE:0.#>";

			myChartArea.Axes.Add(axisX);
			myChartArea.Axes.Add(axisY);

			// 3. The series should be added to the chart's series collection
			NumericSeries seriesA = getNumericSeriesBound();
			NumericSeries seriesB = getNumericSeriesUnBound();

			ultraChart1.CompositeChart.Series.Add(seriesA);
			ultraChart1.CompositeChart.Series.Add(seriesB);

			// 4. Add a chart layer
			ChartLayerAppearance myColumnLayer = new ChartLayerAppearance();

			myColumnLayer.ChartType = ChartType.ColumnChart;
			myColumnLayer.ChartArea = myChartArea;
			myColumnLayer.AxisX = axisX;
			myColumnLayer.AxisY = axisY;
			myColumnLayer.Series.Add(seriesA);
			myColumnLayer.Series.Add(seriesB);

			ultraChart1.CompositeChart.ChartLayers.Add(myColumnLayer);

			// 5. Add a legen to the chart
			CompositeLegend myLegend = new CompositeLegend();

			myLegend.ChartLayers.Add(myColumnLayer);
			myLegend.Bounds = new Rectangle(0, 75, 20, 25);
			myLegend.BoundsMeasureType = MeasureType.Percentage;
			myLegend.PE.FillGradientStyle = GradientStyle.ForwardDiagonal;
			myLegend.PE.Fill = Color.CornflowerBlue;
			myLegend.PE.FillStopColor = Color.Transparent;
			myLegend.Border.CornerRadius = 10;
			myLegend.Border.Thickness = 0;

			ultraChart1.CompositeChart.Legends.Add(myLegend);
		}

		private NumericSeries getNumericSeriesBound()
		{
			NumericSeries series = new NumericSeries();

			series.Label = "Series A";

			// this code populate the series from an external data source
			DataTable table = getData();

			series.Data.DataSource = table;
			series.Data.LabelColumn = "Label Column";
			series.Data.ValueColumn = "Value Column";

			return series; 
		}

		private NumericSeries getNumericSeriesUnBound()
		{
			NumericSeries series = new NumericSeries();

			series.Label = "Series B";

			// this code populates the series using unbound data
			series.Points.Add(new NumericDataPoint(5.0, "Point A", false));
			series.Points.Add(new NumericDataPoint(4.0, "Point B", false));
			series.Points.Add(new NumericDataPoint(3.0, "Point C", false));
			series.Points.Add(new NumericDataPoint(2.0, "Point D", false));
			series.Points.Add(new NumericDataPoint(1.0, "Point E", false));

			return series; 
		}
	}
}