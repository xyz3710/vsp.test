﻿namespace CompositeChartWizard01
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			Infragistics.UltraChart.Resources.Appearance.ChartArea chartArea1 = new Infragistics.UltraChart.Resources.Appearance.ChartArea();
			Infragistics.UltraChart.Resources.Appearance.AxisItem axisItem1 = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
			Infragistics.UltraChart.Resources.Appearance.AxisItem axisItem2 = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
			Infragistics.UltraChart.Resources.Appearance.AxisItem axisItem3 = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
			Infragistics.UltraChart.Resources.Appearance.AxisItem axisItem4 = new Infragistics.UltraChart.Resources.Appearance.AxisItem();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement2 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance chartLayerAppearance1 = new Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance();
			Infragistics.UltraChart.Resources.Appearance.ColumnChartAppearance columnChartAppearance1 = new Infragistics.UltraChart.Resources.Appearance.ColumnChartAppearance();
			Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance chartLayerAppearance2 = new Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance();
			Infragistics.UltraChart.Resources.Appearance.SplineChartAppearance splineChartAppearance1 = new Infragistics.UltraChart.Resources.Appearance.SplineChartAppearance();
			Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance chartTextAppearance1 = new Infragistics.UltraChart.Resources.Appearance.ChartTextAppearance();
			Infragistics.UltraChart.Resources.Appearance.NumericSeries numericSeries1 = new Infragistics.UltraChart.Resources.Appearance.NumericSeries();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint1 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement3 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint2 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement4 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint3 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement5 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint4 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement6 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint5 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement7 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint6 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement8 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericSeries numericSeries2 = new Infragistics.UltraChart.Resources.Appearance.NumericSeries();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint7 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement9 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint8 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement10 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint9 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement11 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint10 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement12 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint11 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement13 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.NumericDataPoint numericDataPoint12 = new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement14 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
			this.ultraChart1 = new Infragistics.Win.UltraWinChart.UltraChart();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.ultraChart1)).BeginInit();
			this.SuspendLayout();
			// 
			// ultraChart1
			// 
			this.ultraChart1.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
			this.ultraChart1.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
			this.ultraChart1.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.ultraChart1.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
			this.ultraChart1.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.ultraChart1.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.ultraChart1.Axis.X.Labels.SeriesLabels.FormatString = "";
			this.ultraChart1.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.ultraChart1.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.ultraChart1.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.X.LineThickness = 1;
			this.ultraChart1.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.ultraChart1.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.X.MajorGridLines.Visible = true;
			this.ultraChart1.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.ultraChart1.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.X.MinorGridLines.Visible = false;
			this.ultraChart1.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.ultraChart1.Axis.X.Visible = true;
			this.ultraChart1.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
			this.ultraChart1.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.ultraChart1.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
			this.ultraChart1.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.ultraChart1.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.ultraChart1.Axis.X2.Labels.SeriesLabels.FormatString = "";
			this.ultraChart1.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.ultraChart1.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.ultraChart1.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.X2.Labels.Visible = false;
			this.ultraChart1.Axis.X2.LineThickness = 1;
			this.ultraChart1.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.ultraChart1.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.X2.MajorGridLines.Visible = true;
			this.ultraChart1.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.ultraChart1.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.X2.MinorGridLines.Visible = false;
			this.ultraChart1.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.ultraChart1.Axis.X2.Visible = false;
			this.ultraChart1.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
			this.ultraChart1.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.ultraChart1.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			this.ultraChart1.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.ultraChart1.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.ultraChart1.Axis.Y.Labels.SeriesLabels.FormatString = "";
			this.ultraChart1.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.ultraChart1.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.ultraChart1.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.Y.LineThickness = 1;
			this.ultraChart1.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.ultraChart1.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.Y.MajorGridLines.Visible = true;
			this.ultraChart1.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.ultraChart1.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.Y.MinorGridLines.Visible = false;
			this.ultraChart1.Axis.Y.TickmarkInterval = 10;
			this.ultraChart1.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.ultraChart1.Axis.Y.Visible = true;
			this.ultraChart1.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
			this.ultraChart1.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.ultraChart1.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			this.ultraChart1.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.ultraChart1.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.ultraChart1.Axis.Y2.Labels.SeriesLabels.FormatString = "";
			this.ultraChart1.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.ultraChart1.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.ultraChart1.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.Y2.Labels.Visible = false;
			this.ultraChart1.Axis.Y2.LineThickness = 1;
			this.ultraChart1.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.ultraChart1.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.Y2.MajorGridLines.Visible = true;
			this.ultraChart1.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.ultraChart1.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.Y2.MinorGridLines.Visible = false;
			this.ultraChart1.Axis.Y2.TickmarkInterval = 10;
			this.ultraChart1.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.ultraChart1.Axis.Y2.Visible = false;
			this.ultraChart1.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
			this.ultraChart1.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.ultraChart1.Axis.Z.Labels.ItemFormatString = "";
			this.ultraChart1.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.ultraChart1.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.ultraChart1.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.ultraChart1.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.ultraChart1.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.Z.Labels.Visible = false;
			this.ultraChart1.Axis.Z.LineThickness = 1;
			this.ultraChart1.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.ultraChart1.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.Z.MajorGridLines.Visible = true;
			this.ultraChart1.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.ultraChart1.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.Z.MinorGridLines.Visible = false;
			this.ultraChart1.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.ultraChart1.Axis.Z.Visible = false;
			this.ultraChart1.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
			this.ultraChart1.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.ultraChart1.Axis.Z2.Labels.ItemFormatString = "";
			this.ultraChart1.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.ultraChart1.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.ultraChart1.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.ultraChart1.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.ultraChart1.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.ultraChart1.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.ultraChart1.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.ultraChart1.Axis.Z2.Labels.Visible = false;
			this.ultraChart1.Axis.Z2.LineThickness = 1;
			this.ultraChart1.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.ultraChart1.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.Z2.MajorGridLines.Visible = true;
			this.ultraChart1.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.ultraChart1.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.ultraChart1.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.ultraChart1.Axis.Z2.MinorGridLines.Visible = false;
			this.ultraChart1.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.ultraChart1.Axis.Z2.Visible = false;
			this.ultraChart1.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.Composite;
			this.ultraChart1.ColorModel.AlphaLevel = ((byte)(150));
			this.ultraChart1.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
			axisItem1.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
			axisItem1.Extent = 10;
			axisItem1.Key = "axis1";
			axisItem1.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			axisItem1.Labels.ItemFormatString = "<ITEM_LABEL>";
			axisItem1.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			axisItem1.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			axisItem1.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			axisItem1.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			axisItem1.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			axisItem1.MajorGridLines.AlphaLevel = ((byte)(255));
			axisItem1.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			axisItem1.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			axisItem1.MajorGridLines.Visible = true;
			axisItem1.MinorGridLines.AlphaLevel = ((byte)(255));
			axisItem1.MinorGridLines.Color = System.Drawing.Color.LightGray;
			axisItem1.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			axisItem1.MinorGridLines.Visible = false;
			axisItem1.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X_Axis;
			axisItem1.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.GroupBySeries;
			axisItem1.TickmarkInterval = 1;
			axisItem2.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
			axisItem2.Extent = 20;
			axisItem2.Key = "axis2";
			axisItem2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			axisItem2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			axisItem2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			axisItem2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			axisItem2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			axisItem2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			axisItem2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			axisItem2.MajorGridLines.AlphaLevel = ((byte)(255));
			axisItem2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			axisItem2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			axisItem2.MajorGridLines.Visible = true;
			axisItem2.MinorGridLines.AlphaLevel = ((byte)(255));
			axisItem2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			axisItem2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			axisItem2.MinorGridLines.Visible = false;
			axisItem2.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y_Axis;
			axisItem2.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.GroupBySeries;
			axisItem2.TickmarkInterval = 50;
			axisItem2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			axisItem3.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.String;
			axisItem3.Extent = 10;
			axisItem3.Key = "axis3";
			axisItem3.Labels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			axisItem3.Labels.ItemFormatString = "<ITEM_LABEL>";
			axisItem3.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Custom;
			axisItem3.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			axisItem3.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			axisItem3.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			axisItem3.Labels.VerticalAlign = System.Drawing.StringAlignment.Far;
			axisItem3.MajorGridLines.AlphaLevel = ((byte)(255));
			axisItem3.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			axisItem3.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			axisItem3.MajorGridLines.Visible = true;
			axisItem3.MinorGridLines.AlphaLevel = ((byte)(255));
			axisItem3.MinorGridLines.Color = System.Drawing.Color.LightGray;
			axisItem3.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			axisItem3.MinorGridLines.Visible = false;
			axisItem3.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.X2_Axis;
			axisItem3.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.ContinuousData;
			axisItem3.TickmarkInterval = 1;
			axisItem4.DataType = Infragistics.UltraChart.Shared.Styles.AxisDataType.Numeric;
			axisItem4.Extent = 20;
			axisItem4.Key = "axis4";
			axisItem4.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			axisItem4.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			axisItem4.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			axisItem4.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			axisItem4.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			axisItem4.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			axisItem4.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			axisItem4.MajorGridLines.AlphaLevel = ((byte)(255));
			axisItem4.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			axisItem4.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			axisItem4.MajorGridLines.Visible = true;
			axisItem4.MinorGridLines.AlphaLevel = ((byte)(255));
			axisItem4.MinorGridLines.Color = System.Drawing.Color.LightGray;
			axisItem4.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			axisItem4.MinorGridLines.Visible = false;
			axisItem4.OrientationType = Infragistics.UltraChart.Shared.Styles.AxisNumber.Y2_Axis;
			axisItem4.SetLabelAxisType = Infragistics.UltraChart.Core.Layers.SetLabelAxisType.GroupBySeries;
			axisItem4.TickmarkInterval = 0.2;
			axisItem4.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			chartArea1.Axes.Add(axisItem1);
			chartArea1.Axes.Add(axisItem2);
			chartArea1.Axes.Add(axisItem3);
			chartArea1.Axes.Add(axisItem4);
			chartArea1.Bounds = new System.Drawing.Rectangle(5, 10, 90, 90);
			chartArea1.BoundsMeasureType = Infragistics.UltraChart.Shared.Styles.MeasureType.Percentage;
			paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			chartArea1.GridPE = paintElement1;
			chartArea1.Key = "area1";
			chartArea1.PE = paintElement2;
			this.ultraChart1.CompositeChart.ChartAreas.Add(chartArea1);
			chartLayerAppearance1.AxisXKey = "axis1";
			chartLayerAppearance1.AxisYKey = "axis2";
			chartLayerAppearance1.ChartAreaKey = "area1";
			chartLayerAppearance1.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.ColumnChart;
			columnChartAppearance1.ColumnSpacing = 1;
			chartLayerAppearance1.ChartTypeAppearance = columnChartAppearance1;
			chartLayerAppearance1.Key = "chartLayer1";
			chartLayerAppearance1.SeriesList = "series1";
			chartLayerAppearance2.AxisXKey = "axis3";
			chartLayerAppearance2.AxisYKey = "axis4";
			chartLayerAppearance2.ChartAreaKey = "area1";
			chartLayerAppearance2.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.SplineChart;
			chartTextAppearance1.ChartTextFont = new System.Drawing.Font("Arial", 7F);
			chartTextAppearance1.Column = 0;
			chartTextAppearance1.Row = 0;
			splineChartAppearance1.ChartText.Add(chartTextAppearance1);
			chartLayerAppearance2.ChartTypeAppearance = splineChartAppearance1;
			chartLayerAppearance2.Key = "chartLayer2";
			chartLayerAppearance2.SeriesList = "series2";
			this.ultraChart1.CompositeChart.ChartLayers.AddRange(new Infragistics.UltraChart.Resources.Appearance.ChartLayerAppearance[] {
            chartLayerAppearance1,
            chartLayerAppearance2});
			numericSeries1.Key = "series1";
			numericDataPoint1.Label = "1000";
			paintElement3.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint1.PE = paintElement3;
			numericDataPoint1.Value = 748;
			numericDataPoint2.Label = "1200";
			paintElement4.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint2.PE = paintElement4;
			numericDataPoint2.Value = 793;
			numericDataPoint3.Label = "1300";
			paintElement5.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint3.PE = paintElement5;
			numericDataPoint3.Value = 948;
			numericDataPoint4.Label = "1400";
			paintElement6.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint4.PE = paintElement6;
			numericDataPoint4.Value = 677;
			numericDataPoint5.Label = "1500";
			paintElement7.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint5.PE = paintElement7;
			numericDataPoint5.Value = 1068;
			numericDataPoint6.Label = "1600";
			paintElement8.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint6.PE = paintElement8;
			numericDataPoint6.Value = 837;
			numericSeries1.Points.AddRange(new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint[] {
            numericDataPoint1,
            numericDataPoint2,
            numericDataPoint3,
            numericDataPoint4,
            numericDataPoint5,
            numericDataPoint6});
			numericSeries2.Key = "series2";
			numericDataPoint7.Label = "1000";
			paintElement9.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint7.PE = paintElement9;
			numericDataPoint7.Value = 1.85;
			numericDataPoint8.Label = "1200";
			paintElement10.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint8.PE = paintElement10;
			numericDataPoint8.Value = 1.27;
			numericDataPoint9.Label = "1300";
			paintElement11.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint9.PE = paintElement11;
			numericDataPoint9.Value = 1.78;
			numericDataPoint10.Label = "1400";
			paintElement12.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint10.PE = paintElement12;
			numericDataPoint10.Value = 1.33;
			numericDataPoint11.Label = "1500";
			paintElement13.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint11.PE = paintElement13;
			numericDataPoint11.Value = 2.21;
			numericDataPoint12.Label = "1600";
			paintElement14.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			numericDataPoint12.PE = paintElement14;
			numericDataPoint12.Value = 1.83;
			numericSeries2.Points.AddRange(new Infragistics.UltraChart.Resources.Appearance.NumericDataPoint[] {
            numericDataPoint7,
            numericDataPoint8,
            numericDataPoint9,
            numericDataPoint10,
            numericDataPoint11,
            numericDataPoint12});
			this.ultraChart1.CompositeChart.Series.AddRange(new Infragistics.UltraChart.Data.Series.ISeries[] {
            numericSeries1,
            numericSeries2});
			this.ultraChart1.Effects.Effects.Add(gradientEffect1);
			this.ultraChart1.Location = new System.Drawing.Point(12, 12);
			this.ultraChart1.Name = "ultraChart1";
			this.ultraChart1.Size = new System.Drawing.Size(886, 384);
			this.ultraChart1.TabIndex = 0;
			this.ultraChart1.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
			this.ultraChart1.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
			this.ultraChart1.Tooltips.TooltipControl = null;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(418, 402);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 2;
			this.button1.Text = "&Close";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form1
			// 
			this.ClientSize = new System.Drawing.Size(910, 428);
			this.Controls.Add(this.ultraChart1);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.ultraChart1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Infragistics.Win.UltraWinChart.UltraChart ultraChart1;
		private System.Windows.Forms.Button button1;
	}
}

