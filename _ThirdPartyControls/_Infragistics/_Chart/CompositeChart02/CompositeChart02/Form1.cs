﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Infragistics.UltraChart.Shared.Styles;
using Infragistics.UltraChart.Resources.Appearance;
using Infragistics.UltraChart.Core.Layers;

namespace CompositeChart02
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			createChart();
		}

		private void btnCompositeChart_Click(object sender, EventArgs e)
		{
			createChart();
		}

		public DataSet GetRawTats(DateTime startDate, DateTime endDate)
		{
			string filePath = @"C:\iDASiT\Demonstration\iCube\iCube.iDASiT\App_Data\Tats\";
			string target = "tat";
			DataSet ds = new DataSet(target);
			DataTable dtChart = new DataTable("Tat");
			DataRow[] dr = null;

			ds.ReadXmlSchema(string.Format("{0}{1}.xsd", filePath, target));
			ds.ReadXml(string.Format("{0}{1}.xml", filePath, target), XmlReadMode.ReadSchema);

			if (ds.Tables.Count == 1)
			{
				dr = ds.Tables[0].Select(string.Format("prodDate >= '{0}' AND prodDate <= '{1}'", startDate.ToShortDateString(), endDate.ToShortDateString()));

				// 컬럼 추가
				dtChart.Columns.Add("ProductId", typeof(string));
				dtChart.Columns.Add("ProdDate", typeof(DateTime));
				dtChart.Columns.Add("OperationId", typeof(string));
				dtChart.Columns.Add("Output", typeof(double));
				dtChart.Columns.Add("Lot", typeof(double));
				dtChart.Columns.Add("Tat", typeof(double));

				// 검색된 data만 추가
				for (int i = 0; i < dr.Length; i++)
				{
					object[] data = new object[] { dr[i]["ProductId"], dr[i]["ProdDate"], dr[i]["OperationId"], dr[i]["Output"], dr[i]["Lot"], dr[i]["Tat"] };

					dtChart.Rows.Add(data);
				}
			}

			DataSet dsResult = new DataSet();

			dsResult.Tables.Add(dtChart);

			return dsResult;
		}

		public DataTable GetTats(DateTime startDate, DateTime endDate)
		{
			DataSet ds = GetRawTats(startDate, endDate);
			DataTable dtChart = new DataTable("Tat");

			if (ds.Tables.Count == 1)
			{
				const int SAMPLE_DAYS = 15;
				string[] operations = new string[] { "1000", "1100", "1200", "1300", "1330", "1350", "1400", "1450", "2000", "2100", "2200", "3000", "3100" };
				string[] distinctCol = new string[] { "TAT", "Output" };
				int productCnt = 0;
				DataRow[] dr = null;

				// 제품수 = record 수
				productCnt = (int)ds.Tables[0].Compute("COUNT(productId)", string.Format("operationId = '{0}'", operations[0])) / SAMPLE_DAYS;

				// chart 용 Table Column 추가
				dtChart.Columns.Add("ProductId", typeof(string));
				dtChart.Columns.Add("Distinct", typeof(string));

				for (int i = 0; i < operations.Length; i++)
					dtChart.Columns.Add(operations[i], typeof(double));

				// 제품명 구하기
				ArrayList arProduct = new ArrayList();
				string tmpProduct = string.Empty;

				dr = ds.Tables[0].Select(string.Format("prodDate = '{0}' AND operationId = '{1}'", startDate, operations[0]));

				if (dr.Length == 0)
					return dtChart;

				tmpProduct = dr[0]["ProductId"].ToString();

				for (int i = 0; i < dr.Length; i++)
				{
					if (tmpProduct != dr[i]["ProductId"].ToString())
						arProduct.Add(tmpProduct);

					tmpProduct = dr[i]["ProductId"].ToString();
				}

				arProduct.Add(tmpProduct);

				// DataTable에 Output과 tat 만을 구해서 넣는다
				double output = 0.0;
				double lot = 0.0;
				double tat = 0.0;

				for (int i = 0; i < arProduct.Count; i++)
				{
					ArrayList arRow = new ArrayList();
					tat = 0.0;
					lot = 0.0;
					output = 0.0;

					for (int j = 0; j < 2; j++)
					{
						// Add ProductId
						arRow.Add(arProduct[i]);
						// TAT, Output 구분 컬럼을 추가한다
						arRow.Add(distinctCol[j]);

						for (int k = 0; k < operations.Length; k++)
						{
							output = (double)ds.Tables[0].Compute("SUM(Output)",
								string.Format("productId='{0}' AND operationId='{1}' AND prodDate >= '{2}' AND prodDate <= '{3}'",
									arProduct[i], operations[k], startDate, endDate));
							lot = (double)ds.Tables[0].Compute("SUM(Lot)",
								string.Format("productId='{0}' AND operationId='{1}' AND prodDate >= '{2}' AND prodDate <= '{3}'",
									arProduct[i], operations[k], startDate, endDate));
							tat = Math.Round(100 - (output - lot) / output * 100, 2);

							if (j == 0)
								arRow.Add(tat);
							else
								arRow.Add(output);
						}

						dtChart.Rows.Add((object[])arRow.ToArray(typeof(object)));
						arRow.Clear();
					}

				}
			}

			return dtChart;
		}

		private void createChart()
		{
			DataTable table = new DataTable();
			table.Columns.Add("Year", typeof(string));
			table.Columns.Add("Category", typeof(int));
			table.Columns.Add("Zone", typeof(int));
			table.Columns.Add("Week", typeof(int));
			table.Columns.Add("MaleCount", typeof(int));
			table.Columns.Add("FemaleCount", typeof(int));
			table.Rows.Add(new object[] { "2006", 0, 1, 1, 5, 8 });
			table.Rows.Add(new object[] { "2007", 1, 1, 1, 7, 12 });
			table.Rows.Add(new object[] { "2008", 0, 1, 2, 4, 7 });
			table.Rows.Add(new object[] { "2009", 1, 1, 2, 8, 15 });
			table.Rows.Add(new object[] { "2010", 0, 1, 2, 8, 5 });
			table.Rows.Add(new object[] { "2011", 1, 1, 2, 8, 15 });

			// set the charttype

			xChart.ChartType = ChartType.Composite;

			// create the series and add them to the series collection

			NumericSeries seriesMale = new NumericSeries();
			seriesMale.PEs.Add(new PaintElement(Color.Blue));
			seriesMale.Data.DataSource = table;
			seriesMale.Data.LabelColumn = "Year";
			seriesMale.Data.ValueColumn = "MaleCount";
			seriesMale.DataBind();

			NumericSeries seriesFemale = new NumericSeries();
			seriesFemale.PEs.Add(new PaintElement(Color.YellowGreen));
			seriesFemale.Data.DataSource = table;
			seriesFemale.Data.LabelColumn = "Year";
			seriesFemale.Data.ValueColumn = "FemaleCount";
			seriesFemale.DataBind();

			this.xChart.Series.Add(seriesMale);
			this.xChart.Series.Add(seriesFemale);

			// create the axes

			AxisItem axisX = new AxisItem();
			axisX.OrientationType = AxisNumber.X_Axis;
			axisX.DataType = AxisDataType.String;
			axisX.SetLabelAxisType = SetLabelAxisType.ContinuousData;
			axisX.Labels.ItemFormatString = "<ITEM_LABEL>";

			AxisItem axisYMale = new AxisItem();
			axisYMale.OrientationType = AxisNumber.Y_Axis;
			axisYMale.DataType = AxisDataType.Numeric;
			axisYMale.Extent = 10;
			axisYMale.Labels.ItemFormatString = "<DATA_VALUE:0>";
			axisYMale.TickmarkStyle = AxisTickStyle.Smart;
			axisYMale.LineColor = Color.Blue;

			AxisItem axisYFemale = new AxisItem();
			axisYFemale.OrientationType = AxisNumber.Y_Axis;
			axisYFemale.DataType = AxisDataType.Numeric;
			axisYFemale.Extent = 30;
			axisYFemale.Labels.ItemFormatString = "<DATA_VALUE:0>";
			axisYFemale.TickmarkStyle = AxisTickStyle.Smart;
			axisYFemale.LineColor = Color.Pink;

			// create the chartarea and add its axes

			ChartArea area1 = new ChartArea();
			area1.Axes.Add(axisX);
			area1.Axes.Add(axisYMale);
			area1.Axes.Add(axisYFemale);

			// addthe chartarea to the chartareas collection

			this.xChart.CompositeChart.ChartAreas.Add(area1);

			// create the layers

			ChartLayerAppearance maleLineLayer = new ChartLayerAppearance();
			maleLineLayer.ChartType = ChartType.LineChart;
			maleLineLayer.ChartArea = area1;
			maleLineLayer.AxisX = axisX;
			maleLineLayer.AxisY = axisYMale;
			maleLineLayer.Series.Add(seriesMale);

			ChartLayerAppearance femaleLineLayer = new ChartLayerAppearance();
			femaleLineLayer.ChartType = ChartType.LineChart;
			femaleLineLayer.ChartArea = area1;
			femaleLineLayer.AxisX = axisX;
			femaleLineLayer.AxisY = axisYFemale;
			femaleLineLayer.Series.Add(seriesFemale);

			// add the layers to the layers collection

			this.xChart.CompositeChart.ChartLayers.Add(maleLineLayer);
			this.xChart.CompositeChart.ChartLayers.Add(femaleLineLayer);
		}
	}
}