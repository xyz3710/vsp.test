﻿namespace DoughChart3D01
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect1 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
			Infragistics.UltraChart.Resources.Appearance.PaintElement paintElement1 = new Infragistics.UltraChart.Resources.Appearance.PaintElement();
			Infragistics.UltraChart.Resources.Appearance.GradientEffect gradientEffect2 = new Infragistics.UltraChart.Resources.Appearance.GradientEffect();
			this.xChartWipProduct = new Infragistics.Win.UltraWinChart.UltraChart();
			this.btnChart = new System.Windows.Forms.Button();
			this.xChartWipOperation = new Infragistics.Win.UltraWinChart.UltraChart();
			((System.ComponentModel.ISupportInitialize)(this.xChartWipProduct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xChartWipOperation)).BeginInit();
			this.SuspendLayout();
			// 
			// xChartWipProduct
			// 
			this.xChartWipProduct.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
			this.xChartWipProduct.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipProduct.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipProduct.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
			this.xChartWipProduct.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChartWipProduct.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipProduct.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipProduct.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.X.LineThickness = 1;
			this.xChartWipProduct.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipProduct.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.X.MajorGridLines.Visible = true;
			this.xChartWipProduct.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipProduct.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.X.MinorGridLines.Visible = false;
			this.xChartWipProduct.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipProduct.Axis.X.Visible = true;
			this.xChartWipProduct.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipProduct.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChartWipProduct.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
			this.xChartWipProduct.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChartWipProduct.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipProduct.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipProduct.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.X2.Labels.Visible = false;
			this.xChartWipProduct.Axis.X2.LineThickness = 1;
			this.xChartWipProduct.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipProduct.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.X2.MajorGridLines.Visible = true;
			this.xChartWipProduct.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipProduct.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.X2.MinorGridLines.Visible = false;
			this.xChartWipProduct.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipProduct.Axis.X2.Visible = false;
			this.xChartWipProduct.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipProduct.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChartWipProduct.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			this.xChartWipProduct.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipProduct.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipProduct.Axis.Y.Labels.SeriesLabels.FormatString = "";
			this.xChartWipProduct.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChartWipProduct.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChartWipProduct.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.Y.LineThickness = 1;
			this.xChartWipProduct.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipProduct.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.Y.MajorGridLines.Visible = true;
			this.xChartWipProduct.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipProduct.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.Y.MinorGridLines.Visible = false;
			this.xChartWipProduct.Axis.Y.TickmarkInterval = 20;
			this.xChartWipProduct.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipProduct.Axis.Y.Visible = true;
			this.xChartWipProduct.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipProduct.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipProduct.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			this.xChartWipProduct.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipProduct.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipProduct.Axis.Y2.Labels.SeriesLabels.FormatString = "";
			this.xChartWipProduct.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipProduct.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChartWipProduct.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.Y2.Labels.Visible = false;
			this.xChartWipProduct.Axis.Y2.LineThickness = 1;
			this.xChartWipProduct.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipProduct.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.Y2.MajorGridLines.Visible = true;
			this.xChartWipProduct.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipProduct.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.Y2.MinorGridLines.Visible = false;
			this.xChartWipProduct.Axis.Y2.TickmarkInterval = 20;
			this.xChartWipProduct.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipProduct.Axis.Y2.Visible = false;
			this.xChartWipProduct.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipProduct.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipProduct.Axis.Z.Labels.ItemFormatString = "";
			this.xChartWipProduct.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipProduct.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipProduct.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipProduct.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipProduct.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.Z.Labels.Visible = false;
			this.xChartWipProduct.Axis.Z.LineThickness = 1;
			this.xChartWipProduct.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipProduct.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.Z.MajorGridLines.Visible = true;
			this.xChartWipProduct.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipProduct.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.Z.MinorGridLines.Visible = false;
			this.xChartWipProduct.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipProduct.Axis.Z.Visible = false;
			this.xChartWipProduct.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipProduct.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipProduct.Axis.Z2.Labels.ItemFormatString = "";
			this.xChartWipProduct.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipProduct.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipProduct.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipProduct.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipProduct.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipProduct.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipProduct.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipProduct.Axis.Z2.Labels.Visible = false;
			this.xChartWipProduct.Axis.Z2.LineThickness = 1;
			this.xChartWipProduct.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipProduct.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.Z2.MajorGridLines.Visible = true;
			this.xChartWipProduct.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipProduct.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipProduct.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipProduct.Axis.Z2.MinorGridLines.Visible = false;
			this.xChartWipProduct.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipProduct.Axis.Z2.Visible = false;
			this.xChartWipProduct.ColorModel.AlphaLevel = ((byte)(150));
			this.xChartWipProduct.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
			this.xChartWipProduct.Effects.Effects.Add(gradientEffect1);
			this.xChartWipProduct.Location = new System.Drawing.Point(14, 12);
			this.xChartWipProduct.Name = "xChartWipProduct";
			this.xChartWipProduct.Size = new System.Drawing.Size(225, 225);
			this.xChartWipProduct.TabIndex = 0;
			this.xChartWipProduct.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
			this.xChartWipProduct.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
			this.xChartWipProduct.Tooltips.TooltipControl = null;
			// 
			// btnChart
			// 
			this.btnChart.Location = new System.Drawing.Point(218, 274);
			this.btnChart.Name = "btnChart";
			this.btnChart.Size = new System.Drawing.Size(75, 23);
			this.btnChart.TabIndex = 2;
			this.btnChart.Text = "Chart";
			this.btnChart.UseVisualStyleBackColor = true;
			this.btnChart.Click += new System.EventHandler(this.btnChart_Click);
			// 
			// xChartWipOperation
			// 
			this.xChartWipOperation.Axis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
			paintElement1.ElementType = Infragistics.UltraChart.Shared.Styles.PaintElementType.None;
			paintElement1.Fill = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(220)))));
			this.xChartWipOperation.Axis.PE = paintElement1;
			this.xChartWipOperation.Axis.X.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.X.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipOperation.Axis.X.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipOperation.Axis.X.Labels.ItemFormatString = "<ITEM_LABEL>";
			this.xChartWipOperation.Axis.X.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.X.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChartWipOperation.Axis.X.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.X.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipOperation.Axis.X.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.X.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.X.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipOperation.Axis.X.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.X.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.X.LineThickness = 1;
			this.xChartWipOperation.Axis.X.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.X.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipOperation.Axis.X.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.X.MajorGridLines.Visible = true;
			this.xChartWipOperation.Axis.X.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.X.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipOperation.Axis.X.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.X.MinorGridLines.Visible = false;
			this.xChartWipOperation.Axis.X.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipOperation.Axis.X.Visible = true;
			this.xChartWipOperation.Axis.X2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.X2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipOperation.Axis.X2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChartWipOperation.Axis.X2.Labels.ItemFormatString = "<ITEM_LABEL>";
			this.xChartWipOperation.Axis.X2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.X2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChartWipOperation.Axis.X2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.X2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipOperation.Axis.X2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.X2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.X2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipOperation.Axis.X2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.X2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.X2.Labels.Visible = false;
			this.xChartWipOperation.Axis.X2.LineThickness = 1;
			this.xChartWipOperation.Axis.X2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.X2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipOperation.Axis.X2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.X2.MajorGridLines.Visible = true;
			this.xChartWipOperation.Axis.X2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.X2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipOperation.Axis.X2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.X2.MinorGridLines.Visible = false;
			this.xChartWipOperation.Axis.X2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipOperation.Axis.X2.Visible = false;
			this.xChartWipOperation.Axis.Y.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.Y.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipOperation.Axis.Y.Labels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChartWipOperation.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			this.xChartWipOperation.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.Y.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipOperation.Axis.Y.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.Y.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipOperation.Axis.Y.Labels.SeriesLabels.FormatString = "";
			this.xChartWipOperation.Axis.Y.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Far;
			this.xChartWipOperation.Axis.Y.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.Y.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChartWipOperation.Axis.Y.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.Y.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.Y.LineThickness = 1;
			this.xChartWipOperation.Axis.Y.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.Y.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipOperation.Axis.Y.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.Y.MajorGridLines.Visible = true;
			this.xChartWipOperation.Axis.Y.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.Y.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipOperation.Axis.Y.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.Y.MinorGridLines.Visible = false;
			this.xChartWipOperation.Axis.Y.TickmarkInterval = 20;
			this.xChartWipOperation.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipOperation.Axis.Y.Visible = true;
			this.xChartWipOperation.Axis.Y2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.Y2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipOperation.Axis.Y2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipOperation.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
			this.xChartWipOperation.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.Y2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipOperation.Axis.Y2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.Y2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipOperation.Axis.Y2.Labels.SeriesLabels.FormatString = "";
			this.xChartWipOperation.Axis.Y2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipOperation.Axis.Y2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.Y2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.VerticalLeftFacing;
			this.xChartWipOperation.Axis.Y2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.Y2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.Y2.Labels.Visible = false;
			this.xChartWipOperation.Axis.Y2.LineThickness = 1;
			this.xChartWipOperation.Axis.Y2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.Y2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipOperation.Axis.Y2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.Y2.MajorGridLines.Visible = true;
			this.xChartWipOperation.Axis.Y2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.Y2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipOperation.Axis.Y2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.Y2.MinorGridLines.Visible = false;
			this.xChartWipOperation.Axis.Y2.TickmarkInterval = 20;
			this.xChartWipOperation.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipOperation.Axis.Y2.Visible = false;
			this.xChartWipOperation.Axis.Z.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.Z.Labels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipOperation.Axis.Z.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipOperation.Axis.Z.Labels.ItemFormatString = "";
			this.xChartWipOperation.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.Z.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipOperation.Axis.Z.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.Z.Labels.SeriesLabels.FontColor = System.Drawing.Color.DimGray;
			this.xChartWipOperation.Axis.Z.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipOperation.Axis.Z.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.Z.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipOperation.Axis.Z.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.Z.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.Z.Labels.Visible = false;
			this.xChartWipOperation.Axis.Z.LineThickness = 1;
			this.xChartWipOperation.Axis.Z.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.Z.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipOperation.Axis.Z.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.Z.MajorGridLines.Visible = true;
			this.xChartWipOperation.Axis.Z.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.Z.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipOperation.Axis.Z.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.Z.MinorGridLines.Visible = false;
			this.xChartWipOperation.Axis.Z.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipOperation.Axis.Z.Visible = false;
			this.xChartWipOperation.Axis.Z2.Labels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.Z2.Labels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipOperation.Axis.Z2.Labels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipOperation.Axis.Z2.Labels.ItemFormatString = "";
			this.xChartWipOperation.Axis.Z2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.Z2.Labels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipOperation.Axis.Z2.Labels.SeriesLabels.Font = new System.Drawing.Font("Verdana", 7F);
			this.xChartWipOperation.Axis.Z2.Labels.SeriesLabels.FontColor = System.Drawing.Color.Gray;
			this.xChartWipOperation.Axis.Z2.Labels.SeriesLabels.HorizontalAlign = System.Drawing.StringAlignment.Near;
			this.xChartWipOperation.Axis.Z2.Labels.SeriesLabels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
			this.xChartWipOperation.Axis.Z2.Labels.SeriesLabels.Orientation = Infragistics.UltraChart.Shared.Styles.TextOrientation.Horizontal;
			this.xChartWipOperation.Axis.Z2.Labels.SeriesLabels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.Z2.Labels.VerticalAlign = System.Drawing.StringAlignment.Center;
			this.xChartWipOperation.Axis.Z2.Labels.Visible = false;
			this.xChartWipOperation.Axis.Z2.LineThickness = 1;
			this.xChartWipOperation.Axis.Z2.MajorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.Z2.MajorGridLines.Color = System.Drawing.Color.Gainsboro;
			this.xChartWipOperation.Axis.Z2.MajorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.Z2.MajorGridLines.Visible = true;
			this.xChartWipOperation.Axis.Z2.MinorGridLines.AlphaLevel = ((byte)(255));
			this.xChartWipOperation.Axis.Z2.MinorGridLines.Color = System.Drawing.Color.LightGray;
			this.xChartWipOperation.Axis.Z2.MinorGridLines.DrawStyle = Infragistics.UltraChart.Shared.Styles.LineDrawStyle.Dot;
			this.xChartWipOperation.Axis.Z2.MinorGridLines.Visible = false;
			this.xChartWipOperation.Axis.Z2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
			this.xChartWipOperation.Axis.Z2.Visible = false;
			this.xChartWipOperation.ColorModel.AlphaLevel = ((byte)(150));
			this.xChartWipOperation.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
			this.xChartWipOperation.Effects.Effects.Add(gradientEffect2);
			this.xChartWipOperation.Location = new System.Drawing.Point(266, 12);
			this.xChartWipOperation.Name = "xChartWipOperation";
			this.xChartWipOperation.Size = new System.Drawing.Size(225, 225);
			this.xChartWipOperation.TabIndex = 0;
			this.xChartWipOperation.Tooltips.HighlightFillColor = System.Drawing.Color.DimGray;
			this.xChartWipOperation.Tooltips.HighlightOutlineColor = System.Drawing.Color.DarkGray;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(510, 310);
			this.Controls.Add(this.xChartWipOperation);
			this.Controls.Add(this.xChartWipProduct);
			this.Controls.Add(this.btnChart);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.xChartWipProduct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xChartWipOperation)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Infragistics.Win.UltraWinChart.UltraChart xChartWipProduct;
		private System.Windows.Forms.Button btnChart;
		private Infragistics.Win.UltraWinChart.UltraChart xChartWipOperation;
	}
}

