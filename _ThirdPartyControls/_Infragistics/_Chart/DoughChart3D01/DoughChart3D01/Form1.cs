﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Infragistics.UltraChart.Resources.Appearance;
using Infragistics.UltraChart.Shared.Styles;

namespace DoughChart3D01
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnChart_Click(object sender, EventArgs e)
		{
			createChart();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			createChart();
		}

		public DataSet GetRawWip(DateTime workDate)
		{
			string filePath = @"C:\iDASiT\Demonstration\iCube\iCube.iDASiT\App_Data\Reporting\Wip";
			string target = "wip";
			DataSet ds = new DataSet(target);
			DataTable dtChart = new DataTable("Wip");
			DataRow[] dr = null;

			ds.ReadXmlSchema(string.Format("{0}\\{1}.xsd", filePath, target));
			ds.ReadXml(string.Format("{0}\\{1}.xml", filePath, target), XmlReadMode.ReadSchema);

			if (ds.Tables.Count == 1)
			{
				dr = ds.Tables[0].Select(string.Format("prodDate = '{0}'", workDate.ToShortDateString()));

				// 컬럼 추가
				dtChart.Columns.Add("ProductId", typeof(string));
				dtChart.Columns.Add("ProdDate", typeof(DateTime));
				dtChart.Columns.Add("OperationId", typeof(string));
				dtChart.Columns.Add("Output", typeof(double));

				// 검색된 data만 추가
				for (int i = 0; i < dr.Length; i++)
				{
					object[] data = new object[] { dr[i]["ProductId"], dr[i]["ProdDate"], dr[i]["OperationId"], dr[i]["Output"] };

					dtChart.Rows.Add(data);
				}
			}

			DataSet dsResult = new DataSet();

			dsResult.Tables.Add(dtChart);

			return dsResult;
		}

		public DataTable GetWipOperation(DateTime workDate)
		{
			DataSet ds = GetRawWip(workDate);
			DataTable dtChart = new DataTable("Wip");

			if (ds.Tables.Count == 1)
			{
				DataRow[] dr = null;
				ArrayList arProduct = new ArrayList();
				string[] operations = new string[] { "1000", "1100", "1200", "1300", "1330", "1350", "1400", "1450", "2000", "2100", "2200", "3000", "3100" };
				int sampleDays = 1;

				// 제품 목록 구하기
				for (int i = 0; i < sampleDays; i++)
				{
					DateTime seekDate = workDate.AddDays(i);

					dr = ds.Tables[0].Select(string.Format("prodDate = '{0}' AND operationId = '{1}'", seekDate.ToShortDateString(), operations[0]));

					if (dr != null)
						break;
				}

				foreach (DataRow row in dr)
					arProduct.Add((string)row["ProductId"]);

				string[] products = (string[])arProduct.ToArray(typeof(string));

				// chart 용 Table Column 추가
				dtChart.Columns.Add("ProductId", typeof(string));

				for (int i = 0; i < operations.Length; i++)
					dtChart.Columns.Add(operations[i], typeof(double));

				// DataTable에 Output 만을 구해서 넣는다
				ArrayList arRow = new ArrayList();
				double output = 0.0;

				for (int i = 0; i < products.Length; i++)
				{
					output = 0.0;

					arRow.Add(products[i]);

					for (int j = 0; j < operations.Length; j++)
					{
						output = (double)ds.Tables[0].Compute("SUM(Output)",
							string.Format("productId='{0}' AND operationId='{1}' AND prodDate = '{2}'",
								products[i], operations[j], workDate));

						arRow.Add(output);
					}

					dtChart.Rows.Add((object[])arRow.ToArray(typeof(object)));
					arRow.Clear();
				}
			}

			return dtChart;
		}

		public DataTable GetWipProduct(DateTime workDate)
		{
			DataSet ds = GetRawWip(workDate);
			DataTable dtChart = new DataTable("Wip");

			if (ds.Tables.Count == 1)
			{
				DataRow[] dr = null;
				ArrayList arProduct = new ArrayList();
				string[] operations = new string[] { "1000", "1100", "1200", "1300", "1330", "1350", "1400", "1450", "2000", "2100", "2200", "3000", "3100" };
				int sampleDays = 1;

				// 제품 목록 구하기
				for (int i = 0; i < sampleDays; i++)
				{
					DateTime seekDate = workDate.AddDays(i);

					dr = ds.Tables[0].Select(string.Format("prodDate = '{0}' AND operationId = '{1}'", seekDate.ToShortDateString(), operations[0]));

					if (dr != null)
						break;
				}

				foreach (DataRow row in dr)
					arProduct.Add((string)row["ProductId"]);

				string[] products = (string[])arProduct.ToArray(typeof(string));

				// chart 용 Table Column 추가
				dtChart.Columns.Add("OperationId", typeof(string));

				for (int i = 0; i < products.Length; i++)
					dtChart.Columns.Add(products[i], typeof(double));

				// DataTable에 Output 만을 구해서 넣는다
				ArrayList arRow = new ArrayList();
				double output = 0.0;

				for (int i = 0; i < operations.Length; i++)
				{
					output = 0.0;

					arRow.Add(operations[i]);

					for (int j = 0; j < products.Length; j++)
					{
						output = (double)ds.Tables[0].Compute("SUM(Output)",
							string.Format("productId='{0}' AND operationId='{1}' AND prodDate = '{2}'",
								products[j], operations[i], workDate));

						arRow.Add(output);
					}

					dtChart.Rows.Add((object[])arRow.ToArray(typeof(object)));
					arRow.Clear();
				}
			}

			return dtChart;
		}

		private DataTable getSummaryData(DataTable dataTable)
		{
			DataTable retTable = new DataTable("summary");
			ArrayList arColumn = new ArrayList();

			// 첫번째 컬럼은 skip하고 두번째 컬럼만 Summary하여 column을 생성한다
			for (int i = 1; i < dataTable.Columns.Count; i++)
			{
				retTable.Columns.Add(dataTable.Columns[i].ColumnName, dataTable.Columns[i].DataType);

				arColumn.Add(dataTable.Columns[i].ColumnName);
			}
			// make summary data
			string[] columns = (string[])arColumn.ToArray(typeof(string));
			object[] row = new object[columns.Length];

			for (int i = 0; i < retTable.Columns.Count; i++)
			{
				double value = (double)dataTable.Compute(string.Format("SUM([{0}])", columns[i]), string.Empty);

				row[i] = value;
			}

			retTable.Rows.Add(row);

			return retTable; 
		}

		private void createChart()
		{
			xChartWipProduct.ChartType = ChartType.DoughnutChart3D;
			xChartWipProduct.Data.SwapRowsAndColumns = true;

			DoughnutChartAppearance apprProduct = new DoughnutChartAppearance();

			apprProduct.Labels.FormatString = "<ITEM_LABEL>(<PERCENT_VALUE:#0.00>%)";

			xChartWipProduct.DoughnutChart = apprProduct;
			xChartWipProduct.Transform3D.XRotation = 75;
			xChartWipProduct.Transform3D.Scale = 100;
			
			xChartWipProduct.DataSource = getSummaryData(GetWipProduct(DateTime.Parse("2007-03-01")));
			xChartWipProduct.DataBind();


			xChartWipOperation.ChartType = ChartType.DoughnutChart3D;
			xChartWipOperation.Data.SwapRowsAndColumns = true;

			DoughnutChartAppearance apprOperation = new DoughnutChartAppearance();

			apprOperation.Labels.FormatString = "<ITEM_LABEL>(<PERCENT_VALUE:#0.00>%)";

			xChartWipOperation.DoughnutChart3D = apprOperation;
			xChartWipOperation.Transform3D.XRotation = 75;
			xChartWipOperation.Transform3D.Scale = 100;

			xChartWipOperation.DataSource = getSummaryData(GetWipOperation(DateTime.Parse("2007-03-01")));
			xChartWipOperation.DataBind();
		}
	}
}