﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinChart;
using Infragistics.UltraChart.Shared.Styles;
using Infragistics.UltraChart.Resources.Appearance;
using Infragistics.UltraChart.Core.Layers;

namespace StackColumnChart01
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

			DataTable table = new DataTable("Test");

			#region String Data

			//table.Columns.Add("ProdDate", typeof(string));
			table.Columns.Add("OperationId", typeof(string));
			//table.Columns.Add("ProductId", typeof(string));
			table.Columns.Add("Qty", typeof(int));
			table.Columns.Add("Code", typeof(string));

//			table.Rows.Add(new object[] { "2007-03-01", "1000", "CHINA", 5, "LOSS01" });
//			table.Rows.Add(new object[] { "2007-03-01", "1200", "CHINA", 7, "LOSS03" });
//			table.Rows.Add(new object[] { "2007-03-01", "1300", "EVER", 3, "LOSS02" });
//			table.Rows.Add(new object[] { "2007-03-01", "1500", "GINA", 8, "LOSS20" });
//			table.Rows.Add(new object[] { "2007-03-01", "1300", "CHINA", 9, "LOSS02" });
			table.Rows.Add(new object[] { "1000", 5, "LOSS01" });
			table.Rows.Add(new object[] { "1200", 7, "LOSS03" });
			table.Rows.Add(new object[] { "1300", 3, "LOSS02" });
			table.Rows.Add(new object[] { "1500", 8, "LOSS20" });
			table.Rows.Add(new object[] { "1300", 9, "LOSS02" });

			#endregion

			UltraChart xChart = new UltraChart();

			xChart.ChartType = ChartType.StackColumnChart;
			xChart.ColorModel.ModelStyle = ColorModels.CustomLinear;

			AxisItem axisX = new AxisItem();
			AxisItem axisY1 = new AxisItem();
			AxisItem axisY2 = new AxisItem();

			axisX.OrientationType = AxisNumber.X_Axis;
			axisX.DataType = AxisDataType.String;
			axisX.SetLabelAxisType = SetLabelAxisType.GroupBySeries;
			axisX.Labels.ItemFormatString = "<ITEM_LABEL>";

			axisY1.OrientationType = AxisNumber.Y_Axis;
			axisY1.DataType = AxisDataType.Numeric;
			axisY1.Labels.ItemFormatString = "<DATA_VALUE:#,##0>";
			axisY1.Labels.Layout.Behavior = AxisLabelLayoutBehaviors.Auto;
			axisY1.TickmarkStyle = AxisTickStyle.Smart;

			axisY2.OrientationType = AxisNumber.Y2_Axis;
			axisY2.DataType = AxisDataType.Numeric;
			axisY2.Labels.ItemFormatString = "<DATA_VALUE:#0.##>";
			axisY2.Labels.Layout.Behavior = AxisLabelLayoutBehaviors.Auto;
			axisY2.TickmarkStyle = AxisTickStyle.Smart;

			xChart.Axis.X = axisX;
			xChart.Axis.Y = axisY1;
			xChart.Axis.Y2 = axisY2;

			NumericSeries series01 = new NumericSeries();
			
			series01.Data.DataSource = table;
			series01.Data.LabelColumn = "Code";
			series01.Data.ValueColumn = "Qty";

			xChart.Series.Add(series01);
			
			NumericSeries series02 = new NumericSeries();

			series02.Data.DataSource = table;
			series02.Data.LabelColumn = "OperationId";
			series02.Data.LabelColumn = "Qty";

			LegendAppearance legend = new LegendAppearance();

			legend.Location = LegendLocation.Right;
			legend.Visible = true;

			xChart.Legend = legend;

			xChart.DataSource = table;
			xChart.DataBind();

			Controls.Add(xChart);
			xChart.Dock = System.Windows.Forms.DockStyle.Fill;
		}
	}
}