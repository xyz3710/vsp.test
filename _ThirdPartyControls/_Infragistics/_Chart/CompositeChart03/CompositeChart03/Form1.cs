﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Infragistics.UltraChart.Shared.Styles;
using Infragistics.UltraChart.Resources.Appearance;
using Infragistics.UltraChart.Core.Layers;
using Infragistics.Win.UltraWinChart;

namespace CompositeChart03
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			createChart();
		}

		private void btnCompositeChart_Click(object sender, EventArgs e)
		{
			createChart();
		}

		public DataSet GetRawTats(DateTime startDate, DateTime endDate)
		{
			string filePath = @"C:\iDASiT\Demonstration\iCube\iCube.iDASiT\App_Data\Tats\";
			string target = "tat";
			DataSet ds = new DataSet(target);
			DataTable dtChart = new DataTable("Tat");
			DataRow[] dr = null;

			ds.ReadXmlSchema(string.Format("{0}{1}.xsd", filePath, target));
			ds.ReadXml(string.Format("{0}{1}.xml", filePath, target), XmlReadMode.ReadSchema);

			if (ds.Tables.Count == 1)
			{
				dr = ds.Tables[0].Select(string.Format("prodDate >= '{0}' AND prodDate <= '{1}'", startDate.ToShortDateString(), endDate.ToShortDateString()));

				// 컬럼 추가
				dtChart.Columns.Add("ProductId", typeof(string));
				dtChart.Columns.Add("ProdDate", typeof(DateTime));
				dtChart.Columns.Add("OperationId", typeof(string));
				dtChart.Columns.Add("Output", typeof(double));
				dtChart.Columns.Add("Lot", typeof(double));
				dtChart.Columns.Add("Tat", typeof(double));

				// 검색된 data만 추가
				for (int i = 0; i < dr.Length; i++)
				{
					object[] data = new object[] { dr[i]["ProductId"], dr[i]["ProdDate"], dr[i]["OperationId"], dr[i]["Output"], dr[i]["Lot"], dr[i]["Tat"] };

					dtChart.Rows.Add(data);
				}
			}

			DataSet dsResult = new DataSet();

			dsResult.Tables.Add(dtChart);

			return dsResult;
		}

		public DataTable GetTats(DateTime startDate, DateTime endDate)
		{
			DataSet ds = GetRawTats(startDate, endDate);
			DataTable dtChart = new DataTable("Tat");

			if (ds.Tables.Count == 1)
			{
				string[] operations = new string[] { "1000", "1100", "1200", "1300", "1330", "1350", "1400", "1450", "2000", "2100", "2200", "3000", "3100" };
				string[] distinctCol = new string[] { "TAT", "Output" };
				int sampleDays = (int)(endDate.ToOADate() - startDate.ToOADate() + 1);
				int productCnt = 0;
				DataRow[] dr = null;

				// 제품수 = record 수
				productCnt = (int)ds.Tables[0].Compute("COUNT(productId)", string.Format("operationId = '{0}'", operations[0])) / sampleDays;

				// chart 용 Table Column 추가
				dtChart.Columns.Add("ProductId", typeof(string));
				dtChart.Columns.Add("Distinct", typeof(string));

				for (int i = 0; i < operations.Length; i++)
					dtChart.Columns.Add(operations[i], typeof(double));

				// 제품명 구하기
				ArrayList arProduct = new ArrayList();
				string tmpProduct = string.Empty;

				dr = ds.Tables[0].Select(string.Format("prodDate = '{0}' AND operationId = '{1}'", startDate, operations[0]));

				if (dr.Length == 0)
					return dtChart;

				tmpProduct = dr[0]["ProductId"].ToString();

				for (int i = 0; i < dr.Length; i++)
				{
					if (tmpProduct != dr[i]["ProductId"].ToString())
						arProduct.Add(tmpProduct);

					tmpProduct = dr[i]["ProductId"].ToString();
				}

				arProduct.Add(tmpProduct);

				// DataTable에 Output과 tat 만을 구해서 넣는다
				double output = 0.0;
				double lot = 0.0;
				double tat = 0.0;

				for (int i = 0; i < arProduct.Count; i++)
				{
					ArrayList arRow = new ArrayList();
					tat = 0.0;
					lot = 0.0;
					output = 0.0;

					for (int j = 0; j < 2; j++)
					{
						// Add ProductId
						arRow.Add(arProduct[i]);
						// TAT, Output 구분 컬럼을 추가한다
						arRow.Add(distinctCol[j]);

						for (int k = 0; k < operations.Length; k++)
						{
							output = (double)ds.Tables[0].Compute("SUM(Output)",
								string.Format("productId='{0}' AND operationId='{1}' AND prodDate >= '{2}' AND prodDate <= '{3}'",
									arProduct[i], operations[k], startDate, endDate));
							lot = (double)ds.Tables[0].Compute("SUM(Lot)",
								string.Format("productId='{0}' AND operationId='{1}' AND prodDate >= '{2}' AND prodDate <= '{3}'",
									arProduct[i], operations[k], startDate, endDate));
							tat = Math.Round(100 - (output - lot) / output * 100, 2);

							if (j == 0)
								arRow.Add(tat);
							else
								arRow.Add(output);
						}

						dtChart.Rows.Add((object[])arRow.ToArray(typeof(object)));
						arRow.Clear();
					}

				}
			}

			return dtChart;
		}

		private DateTime _startDate = DateTime.Parse("2007-03-01");
		private DateTime _endDate = DateTime.Parse("2007-03-15");

		internal enum DataTypePreFix
		{
			TAT,
			Output
		}

		/// <summary>
		/// Tat Table 결과에서 공정별 구분자 별 Sum을 구한다.
		/// </summary>
		/// <param name="dataTypePrefix"></param>
		/// <returns></returns>
		private DataTable getDataTable(DataTypePreFix dataTypePrefix)
		{
			DataTable retTable = new DataTable(dataTypePrefix.ToString());
			DataTable rawData = GetTats(_startDate, _endDate);
			DataRow[] dr = rawData.Select(string.Format("Distinct = '{0}'", dataTypePrefix.ToString()));
			double summary = 0.0;

			// TAT, Output용 Table Column 추가
			retTable.Columns.Add("OperationId", typeof(string));
			retTable.Columns.Add("Value", typeof(double));

			object[] value = new object[retTable.Columns.Count];
			string operationId = string.Empty;

			// 각 공정별로 Prefix에 대한 SUM을 한다
			for (int i = 0; i < rawData.Columns.Count - 2; i++)  // ProductId, Distinct 컬럼 제외
			{
				operationId = rawData.Columns[i + 2].ColumnName;
				summary = (double)rawData.Compute(string.Format("SUM([{0}])", operationId),
					string.Format("Distinct='{0}'", dataTypePrefix.ToString()));

				value[0] = operationId;
				value[1] = Math.Round(summary, 2);				

				retTable.Rows.Add(value);
			}

			return retTable; 
		}

		private void createChart()
		{
			DataTable table = GetTats(DateTime.Parse("2007-03-01"), DateTime.Parse("2007-03-15"));

			// Composite Type 선언
			xChart.ChartType = ChartType.Composite;
			xChart.ColorModel.ModelStyle = ColorModels.Office2007Style;
			xChart.Border.Color = System.Drawing.Color.SlateBlue;
			xChart.Border.Thickness = 1;
			xChart.Border.CornerRadius = 10;

			// 1. Add Chart Area
			ChartArea chartArea = new ChartArea();

			chartArea.Border.Thickness = 0;

			xChart.CompositeChart.ChartAreas.Add(chartArea);

			// 2. Add some Axes to the ChartArea
			AxisItem axisX1 = new AxisItem();
			AxisItem axisX2 = new AxisItem();
			AxisItem axisY1 = new AxisItem();
			AxisItem axisY2 = new AxisItem();

			axisX1.OrientationType = AxisNumber.X_Axis;
			axisX1.DataType = AxisDataType.String;
			axisX1.SetLabelAxisType = SetLabelAxisType.GroupBySeries;
			axisX1.Labels.ItemFormatString = "<ITEM_LABEL>";
			axisX1.Margin.Near.Value = 2.5;
			axisX1.Margin.Far.Value = 1.5;
			axisX1.Extent = 20;

			axisY1.OrientationType = AxisNumber.Y_Axis;
			axisY1.DataType = AxisDataType.Numeric;
			axisY1.Labels.ItemFormatString = "<DATA_VALUE:#,##0>";
			axisY1.Labels.Layout.Behavior = AxisLabelLayoutBehaviors.Auto;
			axisY1.Extent = 45;
			axisY1.TickmarkStyle = AxisTickStyle.Smart;

			axisX2.OrientationType = AxisNumber.X_Axis;
			axisX2.DataType = AxisDataType.String;
			axisX2.SetLabelAxisType = SetLabelAxisType.ContinuousData;
			axisX2.Extent = 20;
			axisX2.Margin.Near.MarginType = LocationType.Pixels;
			axisX2.Margin.Near.Value = 30;
			axisX2.Margin.Far.Value = 3.5;

			axisY2.OrientationType = AxisNumber.Y2_Axis;
			axisY2.DataType = AxisDataType.Numeric;
			axisY2.Labels.ItemFormatString = "<DATA_VALUE:#0.##>";
			axisY2.Labels.Layout.Behavior = AxisLabelLayoutBehaviors.Auto;
			axisY2.Extent = 30;
			axisY2.TickmarkStyle = AxisTickStyle.Smart;

			chartArea.Axes.Add(axisX1);
			chartArea.Axes.Add(axisY1);
			chartArea.Axes.Add(axisX2);
			chartArea.Axes.Add(axisY2);

			// 3. The Series should be added to the chart's series collection
			NumericSeries columnValue = getColumnSeries();
			NumericSeries splineValue = getSplineSeries();

			xChart.CompositeChart.Series.Add(columnValue);
			xChart.CompositeChart.Series.Add(splineValue);

			// 4. Add a chart layer
			ChartLayerAppearance columnLayer = new ChartLayerAppearance();
			ColumnChartAppearance columnChartApperance = new ColumnChartAppearance();		// ColumnChart ColumnSpacing 조절용
			ChartLayerAppearance splineLayer = new ChartLayerAppearance();

			columnLayer.ChartType = ChartType.ColumnChart;
			columnLayer.ChartArea = chartArea;
			columnLayer.AxisX = axisX1;
			columnLayer.AxisY = axisY1;
			columnChartApperance.ColumnSpacing = 1;
			columnLayer.ChartTypeAppearance = columnChartApperance;
			columnLayer.Series.Add(columnValue);

			splineLayer.ChartType = ChartType.SplineChart;
			splineLayer.ChartArea = chartArea;
			splineLayer.AxisX = axisX2;
			splineLayer.AxisY = axisY2;
			splineLayer.Series.Add(splineValue);

			xChart.CompositeChart.ChartLayers.Add(columnLayer);
			xChart.CompositeChart.ChartLayers.Add(splineLayer);
		}

		private NumericSeries getColumnSeries()
		{
			NumericSeries columnSeries = new NumericSeries();

			columnSeries.Data.DataSource = getDataTable(DataTypePreFix.Output);
			columnSeries.Data.LabelColumn = "OperationId";
			columnSeries.Data.ValueColumn = "Value";

			return columnSeries; 
		}

		private NumericSeries getSplineSeries()
		{
			NumericSeries splineSeries = new NumericSeries();

			splineSeries.Data.DataSource = getDataTable(DataTypePreFix.TAT);
			splineSeries.Data.LabelColumn = "OperationId";
			splineSeries.Data.ValueColumn = "Value";

			return splineSeries; 
		}
	}
}