﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Win.UltraWinChart;
using Infragistics.UltraChart.Resources.Appearance;
using Infragistics.UltraChart.Shared.Styles;
using Infragistics.UltraChart.Core.Layers;

namespace Stack3DColumnChart02
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			DataTable table = new DataTable("Test");

			#region String Data
			
			table.Columns.Add("ProdDate", typeof(string));
			table.Columns.Add("CHINA", typeof(double));
			table.Columns.Add("EVER", typeof(double));
			table.Columns.Add("GINA", typeof(double));
			table.Columns.Add("ADAM", typeof(double));
			table.Columns.Add("EVE", typeof(double));
			table.Columns.Add("EVA", typeof(double));

			table.Rows.Add(new object[] { "2007-03-01", 25, 85, 65 , 0, 0, 75});
			table.Rows.Add(new object[] { "2007-03-02", 15, 200, 95, 60, 113, 50 });
			table.Rows.Add(new object[] { "2007-03-03", 100, 45, 110, 25, 85, 65 });
			table.Rows.Add(new object[] { "2007-03-04", 120, 75, 99, 60, 113, 50 });
			table.Rows.Add(new object[] { "2007-03-05", 60, 113, 50, 25, 85, 65 });

			#endregion

			#region DateTime Data

//			table.Columns.Add("ProdDate", typeof(DateTime));
//			table.Columns.Add("CHINA", typeof(double));
//			table.Columns.Add("EVER", typeof(double));
//			table.Columns.Add("GINA", typeof(double));
//			table.Columns.Add("ADAM", typeof(double));
//			table.Columns.Add("EVE", typeof(double));
//			table.Columns.Add("EVA", typeof(double));
//
//			table.Rows.Add(new object[] { DateTime.Parse("2007-03-01"), 25, 85, 65, 25, 85, 65 });
//			table.Rows.Add(new object[] { DateTime.Parse("2007-03-02"), 15, 200, 95, 60, 113, 50 });
//			table.Rows.Add(new object[] { DateTime.Parse("2007-03-03"), 100, 45, 110, 25, 85, 65 });
//			table.Rows.Add(new object[] { DateTime.Parse("2007-03-04"), 120, 75, 99, 60, 113, 50 });
//			table.Rows.Add(new object[] { DateTime.Parse("2007-03-05"), 60, 113, 50, 25, 85, 65 });

			#endregion

			UltraChart xChart = new UltraChart();

			xChart.ChartType = ChartType.CylinderStackColumnChart3D;
			xChart.ColorModel.ModelStyle = ColorModels.CustomLinear;
			xChart.Transform3D.XRotation = 128;
			xChart.Transform3D.YRotation = 0;
			xChart.Transform3D.ZRotation = 0;

			AxisItem axisY1 = new AxisItem();
			AxisItem axisY2 = new AxisItem();
			AxisItem axisZ = new AxisItem();

			axisY1.OrientationType = AxisNumber.Y_Axis;
			axisY1.DataType = AxisDataType.Numeric;
			axisY1.Labels.ItemFormatString = "<DATA_VALUE:#,##0>";
			axisY1.Labels.Layout.Behavior = AxisLabelLayoutBehaviors.Auto;
			axisY1.TickmarkStyle = AxisTickStyle.Smart;

			axisY2.OrientationType = AxisNumber.Y2_Axis;
			axisY2.DataType = AxisDataType.Numeric;
			axisY2.Labels.ItemFormatString = "<DATA_VALUE:#0.##>";
			axisY2.Labels.Layout.Behavior = AxisLabelLayoutBehaviors.Auto;
			axisY2.TickmarkStyle = AxisTickStyle.Smart;

			axisZ.OrientationType = AxisNumber.Z_Axis;
			axisZ.DataType = AxisDataType.String;
			axisZ.SetLabelAxisType = SetLabelAxisType.GroupBySeries;
			axisZ.Labels.ItemFormatString = "<ITEM_LABEL>";
			
			xChart.Axis.Y = axisY1;
			xChart.Axis.Y2 = axisY2;
			xChart.Axis.Z = axisZ;

			LegendAppearance legend = new LegendAppearance();

			legend.Location = LegendLocation.Right;
			legend.Visible = true;

			xChart.Legend = legend;

			xChart.DataSource = table;
			xChart.DataBind();
			
			Controls.Add(xChart);
			xChart.Dock = System.Windows.Forms.DockStyle.Fill;

//			xChart.Axis.Y.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
//			xChart.Axis.Y.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
//			xChart.Axis.Y.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
//			xChart.Axis.Y.Visible = true;

//			xChart.Axis.Y2.Labels.ItemFormatString = "<DATA_VALUE:00.##>";
//			xChart.Axis.Y2.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;
//			xChart.Axis.Y2.TickmarkStyle = Infragistics.UltraChart.Shared.Styles.AxisTickStyle.Smart;
//			xChart.Axis.Y2.Visible = true;

//			xChart.Axis.Z.Labels.ItemFormatString = "<ITEM_LABEL>";
//			xChart.Axis.Z.Labels.Layout.Behavior = Infragistics.UltraChart.Shared.Styles.AxisLabelLayoutBehaviors.Auto;

			//xChart.ChartType = Infragistics.UltraChart.Shared.Styles.ChartType.Stack3DColumnChart;
			//xChart.ColorModel.ModelStyle = Infragistics.UltraChart.Shared.Styles.ColorModels.CustomLinear;
			

		}
	}
}