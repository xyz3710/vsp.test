﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Infragistics.UltraChart.Shared.Styles;
using Infragistics.UltraChart.Resources.Appearance;

namespace ColumnChart
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private DataSet GetRawYields(DateTime startDate, DateTime endDate)
		{
			string filePath = @"C:\iDASiT\Demonstration\iCube\iCube.iDASiT\App_Data\Yields\";
			string target = "yield";
			DataSet ds = new DataSet(target);
			DataTable dtChart = new DataTable("Yield");
			DataRow[] dr = null;

			ds.ReadXmlSchema(string.Format("{0}{1}.xsd", filePath, target));
			ds.ReadXml(string.Format("{0}{1}.xml", filePath, target), XmlReadMode.ReadSchema);

			if (ds.Tables.Count == 1)
			{
				dr = ds.Tables[0].Select(string.Format("prodDate >= '{0}' AND prodDate <= '{1}'", startDate.ToShortDateString(), endDate.ToShortDateString()));

				// 컬럼 추가
				dtChart.Columns.Add("ProductId", typeof(string));
				dtChart.Columns.Add("ProdDate", typeof(DateTime));
				dtChart.Columns.Add("OperationId", typeof(string));
				dtChart.Columns.Add("OutResult", typeof(double));
				dtChart.Columns.Add("Loss", typeof(double));
				dtChart.Columns.Add("Yield", typeof(double));

				// 검색된 data만 추가
				for (int i = 0; i < dr.Length; i++)
				{
					object[] data = new object[] { dr[i]["ProductId"], dr[i]["ProdDate"], dr[i]["OperationId"], dr[i]["OutResult"], dr[i]["Loss"], dr[i]["Yield"] };

					dtChart.Rows.Add(data);
				}
			}

			DataSet dsResult = new DataSet();

			dsResult.Tables.Add(dtChart);

			return dsResult;
		}

		private DataTable getYields()
		{
			DateTime startDate = DateTime.Parse("2007-03-01");
			DateTime endDate = DateTime.Parse("2007-03-15");
			DataSet ds = GetRawYields(startDate, endDate);
			DataTable dtChart = new DataTable("Yield");

			if (ds.Tables.Count == 1)
			{
				const int SAMPLE_DAYS = 15;
				string[] operations = new string[] { "1000", "1100", "1200", "1300", "1330", "1350", "1400", "1450", "2000", "2100", "2200", "3000", "3100" };
				int productCnt = 0;
				DataRow[] dr = null;

				// 제품수 = record 수
				productCnt = (int)ds.Tables[0].Compute("COUNT(productId)", string.Format("operationId = '{0}'", operations[0])) / SAMPLE_DAYS;

				// chart 용 Table Column 추가
				dtChart.Columns.Add("ProductId", typeof(string));

				for (int i = 0; i < operations.Length; i++)
					dtChart.Columns.Add(operations[i], typeof(double));

				// 제품명 구하기
				ArrayList arProduct = new ArrayList();
				string tmpProduct = string.Empty;

				dr = ds.Tables[0].Select(string.Format("prodDate = '{0}' AND operationId = '{1}'", startDate, operations[0]));

				tmpProduct = dr[0]["ProductId"].ToString();

				for (int i = 0; i < dr.Length; i++)
				{
					if (tmpProduct != dr[i]["ProductId"].ToString())
						arProduct.Add(tmpProduct);

					tmpProduct = dr[i]["ProductId"].ToString();
				}

				arProduct.Add(tmpProduct);

				// DataTable에 yield 만을 구해서 넣는다
				for (int i = 0; i < arProduct.Count; i++)
				{
					ArrayList arRow = new ArrayList();

					arRow.Add(arProduct[i]);

					for (int j = 0; j < operations.Length; j++)
					{
						double outResult = (double)ds.Tables[0].Compute("SUM(OutResult)",
							string.Format("productId='{0}' AND operationId='{1}' AND prodDate >= '{2}' AND prodDate <= '{3}'",
								arProduct[i], operations[j], startDate, endDate));
						double loss = (double)ds.Tables[0].Compute("SUM(Loss)",
							string.Format("productId='{0}' AND operationId='{1}' AND prodDate >= '{2}' AND prodDate <= '{3}'",
								arProduct[i], operations[j], startDate, endDate));
						double yield = Math.Round((outResult - loss) / outResult * 100, 2);

						arRow.Add((double)yield);
					}

					dtChart.Rows.Add((object[])arRow.ToArray(typeof(object)));
				}
			}

			return dtChart;
		}

		private void btnColumnChart_Click(object sender, EventArgs e)
		{
			// 1. Column chart로 선언
			xChart.ChartType = ChartType.ColumnChart;

//			// 2. Add Chart Area
//			ChartArea chartArea = new ChartArea();
//
//			xChart.ColumnChart.chart

			xChart.DataSource = getYields();
			xChart.DataBind();

			xChart.Data.SwapRowsAndColumns = true;

//			xChart.Legend.FontColor = Color.AliceBlue;
//			xChart.Legend.BackgroundColor = Color.Aqua;
//			xChart.Legend.BorderStyle = LineDrawStyle.Solid;
//			xChart.Legend.AlphaLevel = 100;
//			xChart.Legend.BorderThickness = 3;
			xChart.Legend.Margins.Top = 0;
			xChart.Legend.Margins.Bottom = 0;
			xChart.Legend.Margins.Left = 0;
			xChart.Legend.Margins.Right = 0;
			xChart.Legend.Location = LegendLocation.Bottom;
			xChart.Legend.Visible = true;
			xChart.Legend.SpanPercentage = 8;
		}

		private NumericSeries getNumericSeries()
		{
			NumericSeries numericSeries = new NumericSeries();
            DataTable table = getYields();

			numericSeries.Label = "1000";
			numericSeries.Data.DataSource = table;
			numericSeries.Data.LabelColumn = "ProductId";
			numericSeries.Data.ValueColumn = "1000";

			return numericSeries; 
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			btnColumnChart_Click(sender, e);
		}
	}
}