﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Framework.Win.IProcessBar
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 11월 26일 월요일 오후 5:32
/*	Purpose		:	Loader와 BaseForm간의 ProgressBar를 관리하기 위해 사용합니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.Framework.Win
{
	/// <summary>
	/// Loader와 BaseForm간의 ProgressBar를 관리하기 위해 사용합니다.
	/// </summary>
	public interface IProcessBar
	{
		/// <summary>
		/// ProcessBar를 시작합니다.
		/// </summary>
		void Start();
		/// <summary>
		/// ProcessBar를 중지합니다.
		/// </summary>
		void Stop();
	}
}
