﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ProgressBar01
{
	public partial class ProgressDialog : Form
	{
		private string _progressMessage;
		private bool _showProgressValue;
		private int _maxValue;
		private int _currentValue;
		private bool _abort;

		#region Properties
		/// <summary>
		/// Message를 구하거나 설정합니다.
		/// </summary>
		public string ProgressMessage
		{
			get
			{
				return _progressMessage;
			}
			set
			{
				_progressMessage = value;
			}
		}

		/// <summary>
		/// ShowProgressValue를 구하거나 설정합니다.
		/// </summary>
		public bool ShowProgressValue
		{
			get
			{
				return _showProgressValue;
			}
			set
			{
				_showProgressValue = value;
			}
		}

		/// <summary>
		/// MaxValue를 구하거나 설정합니다.
		/// </summary>
		public int MaxValue
		{
			get
			{
				return _maxValue;
			}
			set
			{
				_maxValue = value;
			}
		}

		/// <summary>
		/// CurrentValue를 구하거나 설정합니다.
		/// </summary>
		public int CurrentValue
		{
			get
			{
				return _currentValue;
			}
			set
			{
				_currentValue = value;
			}
		}

		#endregion

		public ProgressDialog()
		{
			InitializeComponent();
		}

		public void StartProgress()
		{
			bool reverse = false;
			int cnt = 0;

			while (_abort == false)
			{
				if (cnt == 100)
					reverse = true;
				else if (cnt == 0)
					reverse = false;

				if (reverse == false)
					cnt++;
				else
					cnt--;

				progressBar.Value = cnt;
			}
		}

		public void StopProgress()
		{
			_abort = true;
		}
	}
}