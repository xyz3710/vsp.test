﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

namespace ProgressBar01
{
    public class ProgressUIManager : IDisposable
    {
        // Fields
        private int _CurrentProgressValue;
        private ProgressDialog _Dialog;
        private bool _IsActive;
        private int _MaxProgressValue;
        private string _Message;
        private IntPtr _ParentHandle;
        private bool _ShowProgressValue;
        private bool _ShutdownFlag;
        private static ProgressUIManager _SingletonInstnace;
        private ManualResetEvent _StartEvent;
        private ManualResetEvent _StopEvent;
        private Thread _UIThread;

        private ProgressUIManager()
        {
            this._ParentHandle = IntPtr.Zero;
            this._StartEvent = new ManualResetEvent(false);
            this._StopEvent = new ManualResetEvent(false);
            Application.ThreadExit += new EventHandler(this.Application_ThreadExit);
            this._UIThread = new Thread(new ThreadStart(this.UIThreadMain));
            this._UIThread.SetApartmentState(ApartmentState.STA);
            this._UIThread.Start();
        }

        private void Application_ThreadExit(object sender, EventArgs e)        
		{
            this.Shutdown();
        }

        public static ProgressUIManager CreateInstance()
        {
            if (ProgressUIManager._SingletonInstnace == null)
            {
                ProgressUIManager._SingletonInstnace = new ProgressUIManager();
            }
            else
            {
                ProgressUIManager._SingletonInstnace.Initialize();
            }
            return ProgressUIManager._SingletonInstnace;
        }

        public void Dispose()
        {
            this.Shutdown();
            GC.SuppressFinalize(this);
        }

		~ProgressUIManager()
        {
        }

        private void Initialize()
        {
            this._Message = "";
            this._ShowProgressValue = false;
            this._MaxProgressValue = 0;
            this._CurrentProgressValue = 0;
        }

        internal void Shutdown()
        {
            if (!this._ShutdownFlag)
            {
                this._ShutdownFlag = true;
                this._StartEvent.Set();
                this._StopEvent.Set();
                this._UIThread.Join();
            }
        }

        public void StartProgress()
        {
            if (!this._IsActive)
            {
                this._StartEvent.Set();
                this._IsActive = true;
            }
        }

        public void StopProgress()
        {
            if (this._IsActive)
            {
                this._StopEvent.Set();
            }
        }

        internal static void StopProgressIfExists()
        {
            if ((ProgressUIManager._SingletonInstnace != null) && ProgressUIManager._SingletonInstnace._IsActive)
            {
                ProgressUIManager._SingletonInstnace.StopProgress();
            }
        }

        private void UIThreadMain()
        {
            this._Dialog = new ProgressDialog();

            try
            {
            Label_000B:
                this._StartEvent.WaitOne();
                if (!this._ShutdownFlag)
                {
                    Rectangle rectangle1;
                    this._StartEvent.Reset();
                    this._IsActive = true;
                    this._Dialog.ProgressMessage = this._Message;
                    this._Dialog.ShowProgressValue = this._ShowProgressValue;
                    this._Dialog.MaxValue = this._MaxProgressValue;
                    this._Dialog.CurrentValue = this._CurrentProgressValue;
                    if (this._ParentHandle != IntPtr.Zero)
                    {
						rectangle1 = new Rectangle(_Dialog.Location, _Dialog.Size);
						//rectangle1 = Win32API.GetWindowRect(this._ParentHandle.ToInt32());
                    }
                    else
                    {
						rectangle1 = new Rectangle(_Dialog.Location, _Dialog.Size);
						//rectangle1 = Win32API.GetWindowRect(Win32API.GetDesktopWindow());
                    }
                    int num1 = (rectangle1.Left + (rectangle1.Width / 2)) - (this._Dialog.Width / 2);
                    int num2 = (rectangle1.Top + (rectangle1.Height / 2)) - (this._Dialog.Height / 2);
                    this._Dialog.SetBounds(num1, num2, this._Dialog.Width, this._Dialog.Height, BoundsSpecified.Location);
                    this._Dialog.StartProgress();
                    while (!this._StopEvent.WaitOne(50, false))
                    {
                        this._Dialog.ProgressMessage = this._Message;
                        this._Dialog.CurrentValue = this._CurrentProgressValue;
                        Application.DoEvents();
                    }
                    this._StopEvent.Reset();
                    this._Dialog.StopProgress();
                    this._IsActive = false;
                    this._ParentHandle = IntPtr.Zero;
                    this._Message = null;
                    this._ShowProgressValue = false;
                    this._MaxProgressValue = 0;
                    goto Label_000B;
                }
            }
            catch (ThreadInterruptedException)
            {
                this._StopEvent.Reset();
                return;
            }
            finally
            {
                this._Dialog.Hide();
                this._Dialog.Dispose();
            }
        }
		
        public int CurrentProgressValue
        {
            get
            {
                return this._CurrentProgressValue;
            }
            set
            {
                this._CurrentProgressValue = value;
            }
        }

        public int MaxProgressValue
        {
            get
            {
                return this._MaxProgressValue;
            }
            set
            {
                this._MaxProgressValue = value;
            }
        }

        public string Message
        {
            get
            {
                return this._Message;
            }
            set
            {
                this._Message = value;
            }
        }

        public IntPtr ParentWindow
        {
            set
            {
                this._ParentHandle = value;
            }
        }

        public bool ShowProgressValue
        {
            get
            {
                return this._ShowProgressValue;
            }
            set
            {
                this._ShowProgressValue = value;
            }
        }
    }
}
