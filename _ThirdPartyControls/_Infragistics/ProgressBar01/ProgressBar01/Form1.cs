﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using iDASiT.AppFoundation.Process;
using iDASiT.Framework.Win.Loader;
using iDASiT.TrustCore.Process;
using System.Threading;
using System.ComponentModel;

namespace ProgressBar01
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			InitProgressBar();
		}

		private void btnStart_Click(object sender, EventArgs e)
		{
			#region For Test
			/*
			ProgressUIManager progressUIManager = ProgressUIManager.CreateInstance();

			progressUIManager.MaxProgressValue = 100;
			progressUIManager.ShowProgressValue = true;
			progressUIManager.ParentWindow = Handle;
			
			progressUIManager.StartProgress();
			*/
			#endregion
		
			ProcessBar processBar = ProcessBar.GetInstance(this);

			processBar.Start();

			LotProxy lotProxy = new LotProxy();
			List<Lot> lots = lotProxy.GetLotsInProgressingByPlantOper("DCCPCS1", "3000");

			#region For Test
			/*
			Thread getLotThread = new Thread(
				delegate()
				{
					LotProxy lotProxy = new LotProxy();
					List<Lot> lots = lotProxy.GetLotsInProgressingByPlantOper("DCCPCS1", "3000");

					if (processBar.IsAborted == false)
					{
						ProcessBar.GetInstance(this).Stop();
						MessageBox.Show("Complete", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
						ProcessBar.GetInstance(this).Stop();
				});				

			getLotThread.IsBackground = true;
			getLotThread.Start();
			*/
			#endregion
		}

		private void btnStop_Click(object sender, EventArgs e)
		{
			#region For Test
			/*
			ProgressUIManager progressUIManager = ProgressUIManager.CreateInstance();
			progressUIManager.StopProgress();
			*/
			#endregion

			#region For Test
			ProcessBar processBar = ProcessBar.GetInstance(this);
			
			processBar.Stop();			
			#endregion
		}

		private void InitProgressBar()
		{
			xProgressBar.Appearance.BackColor = Color.FromArgb(233, 234, 238);
			xProgressBar.FillAppearance.BackColor = Color.FromArgb(218, 222, 241);
			xProgressBar.FillAppearance.BackColor2 = Color.FromArgb(131, 146, 226);
			xProgressBar.FillAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			xProgressBar.TextVisible = false;
			xProgressBar.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;

			ProcessBar processBar = ProcessBar.GetInstance(this);

			processBar.UltraProgressBar = xProgressBar;
			processBar.Interval = 0;
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			ProcessBar processBar = ProcessBar.GetInstance(this);

			processBar.Dispose();
		}
	}
}