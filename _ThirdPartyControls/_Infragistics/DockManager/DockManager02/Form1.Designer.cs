﻿namespace DockManager02
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane1 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedLeft, new System.Guid("6692ba04-5e32-457f-a51a-32792864ad32"));
			Infragistics.Win.UltraWinDock.DockableGroupPane dockableGroupPane1 = new Infragistics.Win.UltraWinDock.DockableGroupPane(new System.Guid("0e42dd56-bb92-4755-a340-1e52db4bd5f2"), new System.Guid("00000000-0000-0000-0000-000000000000"), -1, new System.Guid("6692ba04-5e32-457f-a51a-32792864ad32"), -1);
			Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane1 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("9d727a4d-0ae5-4aee-a6c6-5e5a51649b23"), new System.Guid("76c69943-af40-4d13-ad5a-1730aa66cfaa"), -1, new System.Guid("0e42dd56-bb92-4755-a340-1e52db4bd5f2"), 0);
			Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane2 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("d7027b78-51e8-452e-94b4-860655aebdbe"), new System.Guid("76c69943-af40-4d13-ad5a-1730aa66cfaa"), -1, new System.Guid("0e42dd56-bb92-4755-a340-1e52db4bd5f2"), 1);
			Infragistics.Win.UltraWinDock.DockableGroupPane dockableGroupPane2 = new Infragistics.Win.UltraWinDock.DockableGroupPane(new System.Guid("d8d4cdfd-c2f5-4c7a-b3fa-d75613a13fbb"), new System.Guid("00000000-0000-0000-0000-000000000000"), -1, new System.Guid("6692ba04-5e32-457f-a51a-32792864ad32"), -1);
			Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane3 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("55ad55c5-d3fe-4bc4-b6aa-7623d2c7f79c"), new System.Guid("00000000-0000-0000-0000-000000000000"), -1, new System.Guid("d8d4cdfd-c2f5-4c7a-b3fa-d75613a13fbb"), -1);
			Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane4 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("eb33ff9d-4d47-4038-a320-e0f3fc4672a2"), new System.Guid("00000000-0000-0000-0000-000000000000"), -1, new System.Guid("d8d4cdfd-c2f5-4c7a-b3fa-d75613a13fbb"), -1);
			Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane2 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("76c69943-af40-4d13-ad5a-1730aa66cfaa"));
			this.xlblTop1 = new Infragistics.Win.Misc.UltraLabel();
			this.xlblTop2 = new Infragistics.Win.Misc.UltraLabel();
			this.xlblBottom1 = new Infragistics.Win.Misc.UltraLabel();
			this.xlblBottom2 = new Infragistics.Win.Misc.UltraLabel();
			this.xDockManager = new Infragistics.Win.UltraWinDock.UltraDockManager(this.components);
			this._Form1UnpinnedTabAreaLeft = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1UnpinnedTabAreaRight = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1UnpinnedTabAreaTop = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1UnpinnedTabAreaBottom = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1AutoHideControl = new Infragistics.Win.UltraWinDock.AutoHideControl();
			this.dockableWindow4 = new Infragistics.Win.UltraWinDock.DockableWindow();
			this.dockableWindow3 = new Infragistics.Win.UltraWinDock.DockableWindow();
			this.windowDockingArea3 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
			this.dockableWindow2 = new Infragistics.Win.UltraWinDock.DockableWindow();
			this.dockableWindow1 = new Infragistics.Win.UltraWinDock.DockableWindow();
			this.windowDockingArea1 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
			this.btnLoad = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnAddTop = new System.Windows.Forms.Button();
			this.xlblTest = new Infragistics.Win.Misc.UltraLabel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
			((System.ComponentModel.ISupportInitialize)(this.xDockManager)).BeginInit();
			this.dockableWindow4.SuspendLayout();
			this.dockableWindow3.SuspendLayout();
			this.windowDockingArea3.SuspendLayout();
			this.dockableWindow2.SuspendLayout();
			this.dockableWindow1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// xlblTop1
			// 
			this.xlblTop1.Location = new System.Drawing.Point(0, 18);
			this.xlblTop1.Name = "xlblTop1";
			this.xlblTop1.Size = new System.Drawing.Size(95, 267);
			this.xlblTop1.TabIndex = 5;
			this.xlblTop1.Text = "xlblTop1";
			// 
			// xlblTop2
			// 
			this.xlblTop2.Location = new System.Drawing.Point(0, 18);
			this.xlblTop2.Name = "xlblTop2";
			this.xlblTop2.Size = new System.Drawing.Size(95, 497);
			this.xlblTop2.TabIndex = 5;
			this.xlblTop2.Text = "xlblTop2";
			// 
			// xlblBottom1
			// 
			this.xlblBottom1.Location = new System.Drawing.Point(0, 18);
			this.xlblBottom1.Name = "xlblBottom1";
			this.xlblBottom1.Size = new System.Drawing.Size(95, 267);
			this.xlblBottom1.TabIndex = 5;
			this.xlblBottom1.Text = "xlblBottom1";
			// 
			// xlblBottom2
			// 
			this.xlblBottom2.Location = new System.Drawing.Point(0, 18);
			this.xlblBottom2.Name = "xlblBottom2";
			this.xlblBottom2.Size = new System.Drawing.Size(95, 497);
			this.xlblBottom2.TabIndex = 5;
			this.xlblBottom2.Text = "xlblBottom2";
			// 
			// xDockManager
			// 
			dockAreaPane1.DockedBefore = new System.Guid("76c69943-af40-4d13-ad5a-1730aa66cfaa");
			dockableGroupPane1.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
			dockableControlPane1.Control = this.xlblTop1;
			dockableControlPane1.FlyoutSize = new System.Drawing.Size(95, -1);
			dockableControlPane1.OriginalControlBounds = new System.Drawing.Rectangle(249, 160, 100, 23);
			dockableControlPane1.Size = new System.Drawing.Size(100, 100);
			dockableControlPane1.Text = "xlblTop1";
			dockableControlPane2.Control = this.xlblTop2;
			dockableControlPane2.FlyoutSize = new System.Drawing.Size(95, -1);
			dockableControlPane2.OriginalControlBounds = new System.Drawing.Rectangle(390, 160, 100, 23);
			dockableControlPane2.Size = new System.Drawing.Size(100, 100);
			dockableControlPane2.Text = "xlblTop2";
			dockableGroupPane1.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane1,
            dockableControlPane2});
			dockableGroupPane1.Size = new System.Drawing.Size(95, 537);
			dockableGroupPane2.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
			dockableControlPane3.Control = this.xlblBottom1;
			dockableControlPane3.FlyoutSize = new System.Drawing.Size(95, -1);
			dockableControlPane3.OriginalControlBounds = new System.Drawing.Rectangle(208, 160, 100, 33);
			dockableControlPane3.Size = new System.Drawing.Size(100, 100);
			dockableControlPane3.Text = "xlblBottom1";
			dockableControlPane4.Control = this.xlblBottom2;
			dockableControlPane4.FlyoutSize = new System.Drawing.Size(95, -1);
			dockableControlPane4.OriginalControlBounds = new System.Drawing.Rectangle(402, 276, 100, 23);
			dockableControlPane4.Size = new System.Drawing.Size(100, 100);
			dockableControlPane4.Text = "xlblBottom2";
			dockableGroupPane2.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane3,
            dockableControlPane4});
			dockableGroupPane2.Size = new System.Drawing.Size(95, 537);
			dockAreaPane1.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableGroupPane1,
            dockableGroupPane2});
			dockAreaPane1.Size = new System.Drawing.Size(95, 619);
			dockAreaPane2.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
			dockAreaPane2.FloatingLocation = new System.Drawing.Point(777, 277);
			dockAreaPane2.Size = new System.Drawing.Size(95, 537);
			this.xDockManager.DockAreas.AddRange(new Infragistics.Win.UltraWinDock.DockAreaPane[] {
            dockAreaPane1,
            dockAreaPane2});
			this.xDockManager.HostControl = this;
			// 
			// _Form1UnpinnedTabAreaLeft
			// 
			this._Form1UnpinnedTabAreaLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this._Form1UnpinnedTabAreaLeft.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaLeft.Location = new System.Drawing.Point(0, 0);
			this._Form1UnpinnedTabAreaLeft.Name = "_Form1UnpinnedTabAreaLeft";
			this._Form1UnpinnedTabAreaLeft.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaLeft.Size = new System.Drawing.Size(0, 619);
			this._Form1UnpinnedTabAreaLeft.TabIndex = 0;
			// 
			// _Form1UnpinnedTabAreaRight
			// 
			this._Form1UnpinnedTabAreaRight.Dock = System.Windows.Forms.DockStyle.Right;
			this._Form1UnpinnedTabAreaRight.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaRight.Location = new System.Drawing.Point(932, 0);
			this._Form1UnpinnedTabAreaRight.Name = "_Form1UnpinnedTabAreaRight";
			this._Form1UnpinnedTabAreaRight.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaRight.Size = new System.Drawing.Size(0, 619);
			this._Form1UnpinnedTabAreaRight.TabIndex = 1;
			// 
			// _Form1UnpinnedTabAreaTop
			// 
			this._Form1UnpinnedTabAreaTop.Dock = System.Windows.Forms.DockStyle.Top;
			this._Form1UnpinnedTabAreaTop.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaTop.Location = new System.Drawing.Point(0, 0);
			this._Form1UnpinnedTabAreaTop.Name = "_Form1UnpinnedTabAreaTop";
			this._Form1UnpinnedTabAreaTop.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaTop.Size = new System.Drawing.Size(932, 0);
			this._Form1UnpinnedTabAreaTop.TabIndex = 2;
			// 
			// _Form1UnpinnedTabAreaBottom
			// 
			this._Form1UnpinnedTabAreaBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this._Form1UnpinnedTabAreaBottom.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaBottom.Location = new System.Drawing.Point(0, 619);
			this._Form1UnpinnedTabAreaBottom.Name = "_Form1UnpinnedTabAreaBottom";
			this._Form1UnpinnedTabAreaBottom.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaBottom.Size = new System.Drawing.Size(932, 0);
			this._Form1UnpinnedTabAreaBottom.TabIndex = 3;
			// 
			// _Form1AutoHideControl
			// 
			this._Form1AutoHideControl.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1AutoHideControl.Location = new System.Drawing.Point(21, 0);
			this._Form1AutoHideControl.Name = "_Form1AutoHideControl";
			this._Form1AutoHideControl.Owner = this.xDockManager;
			this._Form1AutoHideControl.Size = new System.Drawing.Size(100, 537);
			this._Form1AutoHideControl.TabIndex = 4;
			// 
			// dockableWindow4
			// 
			this.dockableWindow4.Controls.Add(this.xlblTop1);
			this.dockableWindow4.Location = new System.Drawing.Point(0, 0);
			this.dockableWindow4.Name = "dockableWindow4";
			this.dockableWindow4.Owner = this.xDockManager;
			this.dockableWindow4.Size = new System.Drawing.Size(95, 287);
			this.dockableWindow4.TabIndex = 12;
			// 
			// dockableWindow3
			// 
			this.dockableWindow3.Controls.Add(this.xlblTop2);
			this.dockableWindow3.Location = new System.Drawing.Point(-10000, 0);
			this.dockableWindow3.Name = "dockableWindow3";
			this.dockableWindow3.Owner = this.xDockManager;
			this.dockableWindow3.Size = new System.Drawing.Size(95, 517);
			this.dockableWindow3.TabIndex = 13;
			// 
			// windowDockingArea3
			// 
			this.windowDockingArea3.Controls.Add(this.dockableWindow4);
			this.windowDockingArea3.Controls.Add(this.dockableWindow3);
			this.windowDockingArea3.Controls.Add(this.dockableWindow2);
			this.windowDockingArea3.Controls.Add(this.dockableWindow1);
			this.windowDockingArea3.Dock = System.Windows.Forms.DockStyle.Left;
			this.windowDockingArea3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.windowDockingArea3.Location = new System.Drawing.Point(0, 0);
			this.windowDockingArea3.Name = "windowDockingArea3";
			this.windowDockingArea3.Owner = this.xDockManager;
			this.windowDockingArea3.Size = new System.Drawing.Size(100, 619);
			this.windowDockingArea3.TabIndex = 8;
			// 
			// dockableWindow2
			// 
			this.dockableWindow2.Controls.Add(this.xlblBottom1);
			this.dockableWindow2.Location = new System.Drawing.Point(0, 312);
			this.dockableWindow2.Name = "dockableWindow2";
			this.dockableWindow2.Owner = this.xDockManager;
			this.dockableWindow2.Size = new System.Drawing.Size(95, 287);
			this.dockableWindow2.TabIndex = 14;
			// 
			// dockableWindow1
			// 
			this.dockableWindow1.Controls.Add(this.xlblBottom2);
			this.dockableWindow1.Location = new System.Drawing.Point(-10000, 0);
			this.dockableWindow1.Name = "dockableWindow1";
			this.dockableWindow1.Owner = this.xDockManager;
			this.dockableWindow1.Size = new System.Drawing.Size(95, 246);
			this.dockableWindow1.TabIndex = 15;
			// 
			// windowDockingArea1
			// 
			this.windowDockingArea1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.windowDockingArea1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.windowDockingArea1.Location = new System.Drawing.Point(4, 4);
			this.windowDockingArea1.Name = "windowDockingArea1";
			this.windowDockingArea1.Owner = this.xDockManager;
			this.windowDockingArea1.Size = new System.Drawing.Size(95, 537);
			this.windowDockingArea1.TabIndex = 0;
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(137, 60);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(75, 23);
			this.btnLoad.TabIndex = 9;
			this.btnLoad.Text = "&Load";
			this.btnLoad.UseVisualStyleBackColor = true;
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(137, 106);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 9;
			this.btnSave.Text = "&Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnAddTop
			// 
			this.btnAddTop.Location = new System.Drawing.Point(137, 13);
			this.btnAddTop.Name = "btnAddTop";
			this.btnAddTop.Size = new System.Drawing.Size(75, 23);
			this.btnAddTop.TabIndex = 9;
			this.btnAddTop.Text = "&AddTop";
			this.btnAddTop.UseVisualStyleBackColor = true;
			this.btnAddTop.Click += new System.EventHandler(this.btnAddTop_Click);
			// 
			// xlblTest
			// 
			this.xlblTest.Location = new System.Drawing.Point(137, 168);
			this.xlblTest.Name = "xlblTest";
			this.xlblTest.Size = new System.Drawing.Size(100, 23);
			this.xlblTest.TabIndex = 10;
			this.xlblTest.Text = "Test label";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.propertyGrid1, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(493, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(439, 619);
			this.tableLayoutPanel1.TabIndex = 11;
			// 
			// propertyGrid1
			// 
			this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.propertyGrid1.Location = new System.Drawing.Point(3, 3);
			this.propertyGrid1.Name = "propertyGrid1";
			this.propertyGrid1.SelectedObject = this.xDockManager;
			this.propertyGrid1.Size = new System.Drawing.Size(433, 613);
			this.propertyGrid1.TabIndex = 0;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(932, 619);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.xlblTest);
			this.Controls.Add(this._Form1AutoHideControl);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.btnAddTop);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.windowDockingArea3);
			this.Controls.Add(this._Form1UnpinnedTabAreaTop);
			this.Controls.Add(this._Form1UnpinnedTabAreaBottom);
			this.Controls.Add(this._Form1UnpinnedTabAreaLeft);
			this.Controls.Add(this._Form1UnpinnedTabAreaRight);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.xDockManager)).EndInit();
			this.dockableWindow4.ResumeLayout(false);
			this.dockableWindow3.ResumeLayout(false);
			this.windowDockingArea3.ResumeLayout(false);
			this.dockableWindow2.ResumeLayout(false);
			this.dockableWindow1.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private Infragistics.Win.UltraWinDock.UltraDockManager xDockManager;
		private Infragistics.Win.UltraWinDock.AutoHideControl _Form1AutoHideControl;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaTop;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaBottom;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaLeft;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaRight;
		private Infragistics.Win.Misc.UltraLabel xlblTop1;
		private Infragistics.Win.Misc.UltraLabel xlblBottom1;
		private Infragistics.Win.Misc.UltraLabel xlblBottom2;
		private Infragistics.Win.Misc.UltraLabel xlblTop2;
		private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea3;
		private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow4;
		private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow3;
		private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow1;
		private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow2;
		private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea1;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.Button btnAddTop;
		private Infragistics.Win.Misc.UltraLabel xlblTest;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.PropertyGrid propertyGrid1;
	}
}

