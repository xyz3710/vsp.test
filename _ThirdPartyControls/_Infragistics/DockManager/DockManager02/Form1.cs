﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Infragistics.Win.UltraWinDock;
using Infragistics.Win.Misc;

namespace DockManager02
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private string SettingFilePath
		{
			get
			{
				return Path.Combine(Application.StartupPath, "xDocManagerSettings.xml");
			}
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			try
			{
				xDockManager.LoadFromXML(SettingFilePath);

				MessageBox.Show(string.Format("Load OK to {0}", SettingFilePath),
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);

			}
			catch (Exception ex)
			{
				MessageBox.Show(string.Format("Load Fail cause {0}", ex.Message),
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				xDockManager.SaveAsXML(SettingFilePath);

				MessageBox.Show(string.Format("Save OK to {0}", SettingFilePath),
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);
                
			}
			catch (Exception ex)
			{
				MessageBox.Show(string.Format("Save Fail cause {0}", ex.Message),
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);
			}
		}

		private void btnAddTop_Click(object sender, EventArgs e)
		{
			DockableControlPane dockableControlPane11 = new DockableControlPane(new System.Guid(), new System.Guid("76c69943-af40-4d13-ad5a-1730aa66cfaa"), -1, new System.Guid("0e42dd56-bb92-4755-a340-1e52db4bd5f2"), 2)
			{
				Text = DateTime.Now.ToShortTimeString(),
				Control = new UltraLabel()
				{
					Text = DateTime.Now.ToShortTimeString(),
				},
			};

			xDockManager.ControlPanes[xDockManager.ControlPanes.Count - 1].Parent.Panes.Add(dockableControlPane11);
		}
	}
}
