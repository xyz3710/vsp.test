﻿namespace DockManager01
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane1 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedLeft, new System.Guid("7c91edcf-d9ad-4be8-b8d4-28a816fb779b"));
			Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane1 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("9eeabd96-88df-4179-b207-3449b57fc7f2"), new System.Guid("cb7b6c08-aa53-49dc-a268-8e11a4789128"), -1, new System.Guid("7c91edcf-d9ad-4be8-b8d4-28a816fb779b"), 0);
			Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane2 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("4fe13403-d60d-4e42-9226-06543f173b70"), new System.Guid("00000000-0000-0000-0000-000000000000"), -1, new System.Guid("7c91edcf-d9ad-4be8-b8d4-28a816fb779b"), -1);
			Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane2 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.Floating, new System.Guid("cb7b6c08-aa53-49dc-a268-8e11a4789128"));
			this.ultraTree2 = new Infragistics.Win.UltraWinTree.UltraTree();
			this.ultraTree1 = new Infragistics.Win.UltraWinTree.UltraTree();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.xDockManager = new Infragistics.Win.UltraWinDock.UltraDockManager(this.components);
			this._Form1UnpinnedTabAreaLeft = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1UnpinnedTabAreaRight = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1UnpinnedTabAreaTop = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1UnpinnedTabAreaBottom = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1AutoHideControl = new Infragistics.Win.UltraWinDock.AutoHideControl();
			this.dockableWindow2 = new Infragistics.Win.UltraWinDock.DockableWindow();
			this.dockableWindow1 = new Infragistics.Win.UltraWinDock.DockableWindow();
			this.windowDockingArea1 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
			this.windowDockingArea3 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xDockManager)).BeginInit();
			this._Form1AutoHideControl.SuspendLayout();
			this.dockableWindow2.SuspendLayout();
			this.dockableWindow1.SuspendLayout();
			this.SuspendLayout();
			// 
			// ultraTree2
			// 
			this.ultraTree2.Location = new System.Drawing.Point(0, 20);
			this.ultraTree2.Name = "ultraTree2";
			this.ultraTree2.Size = new System.Drawing.Size(95, 495);
			this.ultraTree2.TabIndex = 5;
			// 
			// ultraTree1
			// 
			this.ultraTree1.Location = new System.Drawing.Point(0, 20);
			this.ultraTree1.Name = "ultraTree1";
			this.ultraTree1.Size = new System.Drawing.Size(95, 495);
			this.ultraTree1.TabIndex = 5;
			this.ultraTree1.Click += new System.EventHandler(this.ultraTree1_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(621, 56);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(130, 23);
			this.button1.TabIndex = 7;
			this.button1.Text = "UnPin";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(621, 96);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(130, 23);
			this.button2.TabIndex = 7;
			this.button2.Text = "Area 0 Close";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(768, 56);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(130, 23);
			this.button3.TabIndex = 8;
			this.button3.Text = "PinAll";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(768, 96);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(130, 23);
			this.button4.TabIndex = 8;
			this.button4.Text = "Flyout";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// xDockManager
			// 
			this.xDockManager.AutoHideDelay = 10;
			dockAreaPane1.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
			dockAreaPane1.DockedBefore = new System.Guid("cb7b6c08-aa53-49dc-a268-8e11a4789128");
			dockableControlPane1.Control = this.ultraTree2;
			dockableControlPane1.FlyoutSize = new System.Drawing.Size(95, -1);
			dockableControlPane1.OriginalControlBounds = new System.Drawing.Rectangle(31, 164, 121, 97);
			dockableControlPane1.Pinned = false;
			dockableControlPane1.Size = new System.Drawing.Size(100, 100);
			dockableControlPane1.Text = "ultraTree2";
			dockableControlPane2.Control = this.ultraTree1;
			dockableControlPane2.FlyoutSize = new System.Drawing.Size(95, -1);
			dockableControlPane2.OriginalControlBounds = new System.Drawing.Rectangle(31, 44, 121, 97);
			dockableControlPane2.Pinned = false;
			dockableControlPane2.Size = new System.Drawing.Size(100, 100);
			dockableControlPane2.Text = "ultraTree1";
			dockAreaPane1.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane1,
            dockableControlPane2});
			dockAreaPane1.SelectedTabIndex = 1;
			dockAreaPane1.Size = new System.Drawing.Size(95, 515);
			dockAreaPane2.FloatingLocation = new System.Drawing.Point(60, 314);
			dockAreaPane2.Size = new System.Drawing.Size(95, 515);
			this.xDockManager.DockAreas.AddRange(new Infragistics.Win.UltraWinDock.DockAreaPane[] {
            dockAreaPane1,
            dockAreaPane2});
			this.xDockManager.DragWindowStyle = Infragistics.Win.UltraWinDock.DragWindowStyle.LayeredWindowWithIndicators;
			this.xDockManager.HostControl = this;
			this.xDockManager.SaveSettingsFormat = Infragistics.Win.SaveSettingsFormat.Xml;
			this.xDockManager.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xDockManager.WindowStyle = Infragistics.Win.UltraWinDock.WindowStyle.VisualStudio2005;
			// 
			// _Form1UnpinnedTabAreaLeft
			// 
			this._Form1UnpinnedTabAreaLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this._Form1UnpinnedTabAreaLeft.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaLeft.Location = new System.Drawing.Point(0, 0);
			this._Form1UnpinnedTabAreaLeft.Name = "_Form1UnpinnedTabAreaLeft";
			this._Form1UnpinnedTabAreaLeft.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaLeft.Size = new System.Drawing.Size(21, 515);
			this._Form1UnpinnedTabAreaLeft.TabIndex = 9;
			// 
			// _Form1UnpinnedTabAreaRight
			// 
			this._Form1UnpinnedTabAreaRight.Dock = System.Windows.Forms.DockStyle.Right;
			this._Form1UnpinnedTabAreaRight.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaRight.Location = new System.Drawing.Point(1056, 0);
			this._Form1UnpinnedTabAreaRight.Name = "_Form1UnpinnedTabAreaRight";
			this._Form1UnpinnedTabAreaRight.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaRight.Size = new System.Drawing.Size(0, 515);
			this._Form1UnpinnedTabAreaRight.TabIndex = 10;
			// 
			// _Form1UnpinnedTabAreaTop
			// 
			this._Form1UnpinnedTabAreaTop.Dock = System.Windows.Forms.DockStyle.Top;
			this._Form1UnpinnedTabAreaTop.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaTop.Location = new System.Drawing.Point(21, 0);
			this._Form1UnpinnedTabAreaTop.Name = "_Form1UnpinnedTabAreaTop";
			this._Form1UnpinnedTabAreaTop.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaTop.Size = new System.Drawing.Size(1035, 0);
			this._Form1UnpinnedTabAreaTop.TabIndex = 11;
			// 
			// _Form1UnpinnedTabAreaBottom
			// 
			this._Form1UnpinnedTabAreaBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this._Form1UnpinnedTabAreaBottom.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaBottom.Location = new System.Drawing.Point(21, 515);
			this._Form1UnpinnedTabAreaBottom.Name = "_Form1UnpinnedTabAreaBottom";
			this._Form1UnpinnedTabAreaBottom.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaBottom.Size = new System.Drawing.Size(1035, 0);
			this._Form1UnpinnedTabAreaBottom.TabIndex = 12;
			// 
			// _Form1AutoHideControl
			// 
			this._Form1AutoHideControl.Controls.Add(this.dockableWindow2);
			this._Form1AutoHideControl.Controls.Add(this.dockableWindow1);
			this._Form1AutoHideControl.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1AutoHideControl.Location = new System.Drawing.Point(21, 0);
			this._Form1AutoHideControl.Name = "_Form1AutoHideControl";
			this._Form1AutoHideControl.Owner = this.xDockManager;
			this._Form1AutoHideControl.Size = new System.Drawing.Size(10, 515);
			this._Form1AutoHideControl.TabIndex = 13;
			// 
			// dockableWindow2
			// 
			this.dockableWindow2.Controls.Add(this.ultraTree2);
			this.dockableWindow2.Location = new System.Drawing.Point(-10000, 0);
			this.dockableWindow2.Name = "dockableWindow2";
			this.dockableWindow2.Owner = this.xDockManager;
			this.dockableWindow2.Size = new System.Drawing.Size(95, 515);
			this.dockableWindow2.TabIndex = 15;
			// 
			// dockableWindow1
			// 
			this.dockableWindow1.Controls.Add(this.ultraTree1);
			this.dockableWindow1.Location = new System.Drawing.Point(-10000, 0);
			this.dockableWindow1.Name = "dockableWindow1";
			this.dockableWindow1.Owner = this.xDockManager;
			this.dockableWindow1.Size = new System.Drawing.Size(95, 515);
			this.dockableWindow1.TabIndex = 16;
			// 
			// windowDockingArea1
			// 
			this.windowDockingArea1.Dock = System.Windows.Forms.DockStyle.Left;
			this.windowDockingArea1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.windowDockingArea1.Location = new System.Drawing.Point(0, 0);
			this.windowDockingArea1.Name = "windowDockingArea1";
			this.windowDockingArea1.Owner = this.xDockManager;
			this.windowDockingArea1.Size = new System.Drawing.Size(100, 515);
			this.windowDockingArea1.TabIndex = 14;
			// 
			// windowDockingArea3
			// 
			this.windowDockingArea3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.windowDockingArea3.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.windowDockingArea3.Location = new System.Drawing.Point(4, 4);
			this.windowDockingArea3.Name = "windowDockingArea3";
			this.windowDockingArea3.Owner = this.xDockManager;
			this.windowDockingArea3.Size = new System.Drawing.Size(95, 515);
			this.windowDockingArea3.TabIndex = 0;
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(621, 143);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(130, 23);
			this.button5.TabIndex = 7;
			this.button5.Text = "Save";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(768, 143);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(130, 23);
			this.button6.TabIndex = 7;
			this.button6.Text = "Load";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1056, 515);
			this.Controls.Add(this._Form1AutoHideControl);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.windowDockingArea1);
			this.Controls.Add(this._Form1UnpinnedTabAreaTop);
			this.Controls.Add(this._Form1UnpinnedTabAreaBottom);
			this.Controls.Add(this._Form1UnpinnedTabAreaLeft);
			this.Controls.Add(this._Form1UnpinnedTabAreaRight);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.ultraTree2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xDockManager)).EndInit();
			this._Form1AutoHideControl.ResumeLayout(false);
			this.dockableWindow2.ResumeLayout(false);
			this.dockableWindow1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private Infragistics.Win.UltraWinTree.UltraTree ultraTree1;
		private Infragistics.Win.UltraWinTree.UltraTree ultraTree2;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
		private Infragistics.Win.UltraWinDock.UltraDockManager xDockManager;
		private Infragistics.Win.UltraWinDock.AutoHideControl _Form1AutoHideControl;
		private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow2;
		private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow1;
		private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea1;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaTop;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaBottom;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaLeft;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaRight;
		private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea3;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button5;
	}
}

