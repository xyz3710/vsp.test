﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DockManager01
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			xDockManager.AnimationEnabled = false;
		}

		private string SettingFilePath
		{
			get
			{
				return Path.Combine(Application.StartupPath, "xDockManagerSetting.settings");
			}
		}

		private void ultraTree1_Click(object sender, EventArgs e)
		{
			_Form1AutoHideControl.Hide();
			_Form1AutoHideControl.Invalidate();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			xDockManager.UnpinAll();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			xDockManager.PinAll();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			xDockManager.DockAreas[0].Close();
		}

		private void button4_Click(object sender, EventArgs e)
		{
			// Unpin된 것만 Flyout 할 수 있다.
			if (xDockManager.ControlPanes[0].Pinned == false)
				xDockManager.ControlPanes[0].Flyout(true);
		}

		private void button5_Click(object sender, EventArgs e)
		{
			xDockManager.SaveAsXML(SettingFilePath);
		}

		private void button6_Click(object sender, EventArgs e)
		{
			xDockManager.LoadFromXML(SettingFilePath);
		}
	}
}