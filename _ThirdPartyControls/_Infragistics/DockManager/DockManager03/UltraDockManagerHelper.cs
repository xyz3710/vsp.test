﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Controls.UltraDockManagerHelper.UltraDockManagerHelper
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 10월 21일 수요일 오전 8:38
/*	Purpose		:	UltraDockManager의 설정을 도와주는 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	UltraDockManager는 아래와 같은 구조로 구성해야 한다.
 *					DockAreaPane
 *						[DockableGroupPane]
 *								[DockableControlPane]
 *						[DockableControlPane]
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 10월 21일 수요일 오후 4:17
/**********************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using Infragistics.Win.UltraWinDock;
using System.Windows.Forms;
using System.Drawing;

namespace AnyFactory.FX.Win.Controls.UltraDockManagerHelper
{
	/// <summary>
	/// UltraDockManager의 설정을 도와주는 기능을 제공합니다.
	/// </summary>
	/// <remarks>
	/// UltraDockManager는 아래와 같은 구조로 구성해야 한다.<br/>
	/// 	DockAreaPane<br/>
	/// 		[DockableGroupPane]<br/>
	/// 			[DockableControlPane]<br/>
	/// 		[DockableControlPane]<br/>
	/// 		...
	/// </remarks>
	public class UltraDockManagerHelper
	{
		#region Constructors
		/// <summary>
		/// UltraDockManager helper class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="dockManager">이미 생성된 dockManager가 있을 경우 지정할 수 있습니다.</param>
		public UltraDockManagerHelper(UltraDockManager dockManager)
		{
			DockManager = dockManager;
		}

		/// <summary>
		/// UltraDockManager helper class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="hostControl">dock control을 hosting하는 container(ex: MDI parent form etc...)</param>
		public UltraDockManagerHelper(ContainerControl hostControl)
			: this(hostControl, String.Empty)
		{
		}

		/// <summary>
		/// UltraDockManager helper class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="hostControl">dock control을 hosting하는 container(ex: MDI parent form etc...)</param>
		/// <param name="settingFilePath">setting을 저장하거나 불러올 설정 파일 경로</param>
		protected UltraDockManagerHelper(ContainerControl hostControl, string settingFilePath)
		{
			DockManager = new UltraDockManager();
			DockManager.HostControl = hostControl;
			SettingFilePath = settingFilePath;
		}
		#endregion

		#region Properties
		/// <summary>
		/// 관리되는 UltraDockManager를 구합니다.
		/// </summary>
		public UltraDockManager DockManager
		{
			get;
			private set;
		}

		/// <summary>
		/// Setting 파일 경로를 구하거나 설정합니다.
		/// </summary>
		protected string SettingFilePath
		{
			get;
			set;
		}
		#endregion

		#region AddDockAreaPane
		/// <summary>
		/// dockedLocation 위치에 새로운 Dock area pane을 추가 합니다.(새로운 Guid key를 할당 합니다.)
		/// </summary>
		/// <param name="dockedLocation">pane이 위치할 dock area</param>
		/// <returns>추가된 DockAreaPane의 key(추가되지 못할 경우 <seealso cref="System.String.Empty"/> 반환</returns>
		public string AddDockAreaPane(DockedLocation dockedLocation)
		{
			DockAreaPane dockAreaPane = AddDockAreaPane(dockedLocation, GetNewPaneKey());

			return dockAreaPane != null ? dockAreaPane.Key : string.Empty;
		}

		/// <summary>
		/// dockedLocation 위치에 새로운 Dock area pane을 추가 합니다.
		/// </summary>
		/// <param name="dockedLocation">pane이 위치할 dock area</param>
		/// <param name="key">추가될 pane의 key</param>
		/// <returns>추가된 DockAreaPane</returns>
		public DockAreaPane AddDockAreaPane(DockedLocation dockedLocation, string key)
		{
			return DockManager.DockAreas.Add(dockedLocation, key);
		}
		#endregion

		#region AddDockableGroupPane
		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="parentKey">추가할 parent의 key</param>
		/// <param name="childPaneStyle">group내 추가될 자식 pane의 display style</param>
		/// <returns>추가된 DockableGroupPane의 key(추가되지 못할 경우 <seealso cref="System.String.Empty"></seealso> 반환</returns>
		public string AddDockableGroupPane(string parentKey, ChildPaneStyle childPaneStyle)
		{
			string text = String.Empty;

			return AddDockableGroupPane(parentKey, text, childPaneStyle);
		}

		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="parentKey">추가할 parent의 key</param>
		/// <param name="text">DockableGroupPane의 display text</param>
		/// <param name="childPaneStyle">group내 추가될 자식 pane의 display style</param>
		/// <returns>추가된 DockableGroupPane의 key(추가되지 못할 경우 <seealso cref="System.String.Empty"/> 반환</returns>
		public string AddDockableGroupPane(string parentKey, string text, ChildPaneStyle childPaneStyle)
		{
			DockableGroupPane dockableGroupPane = AddDockableGroupPane(parentKey,
													  GetNewPaneKey(),
													  text,
													  childPaneStyle);

			return dockableGroupPane != null ? dockableGroupPane.Key : string.Empty;
		}

		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="parentPane">추가될 DockAreaPane이나 DockableGroupPane</param>
		/// <param name="text">DockableGroupPane의 display text</param>
		/// <param name="childPaneStyle">group내 추가될 자식 pane의 display style</param>
		/// <returns>추가된 DockableGroupPane의 key(추가되지 못할 경우 <seealso cref="System.String.Empty"></seealso> 반환</returns>
		public string AddDockableGroupPane(DockablePaneBase parentPane, ChildPaneStyle childPaneStyle)
		{
			string text = String.Empty;

			return AddDockableGroupPane(parentPane.Key, text, childPaneStyle);
		}

		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="parentKey">추가할 parent의 key</param>
		/// <param name="key">추가될 pane의 key</param>
		/// <param name="text">DockableGroupPane의 display text</param>
		/// <param name="childPaneStyle">group내 추가될 자식 pane의 display style</param>
		/// <returns>추가된 DockableGroupPane</returns>
		public DockableGroupPane AddDockableGroupPane(string parentKey, string key, string text, ChildPaneStyle childPaneStyle)
		{
			DockablePaneBase dockablePaneBase = DockManager.PaneFromKey(parentKey);

			return AddDockableGroupPane(dockablePaneBase, key, text, childPaneStyle);
		}

		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="parentPane">추가될 DockAreaPane이나 DockableGroupPane</param>
		/// <param name="key">추가될 pane의 key</param>
		/// <param name="text">DockableGroupPane의 display text</param>
		/// <param name="childPaneStyle">group내 추가될 자식 pane의 display style</param>
		/// <returns>추가된 DockableGroupPane</returns>
		public DockableGroupPane AddDockableGroupPane(DockablePaneBase parentPane, string key, string text, ChildPaneStyle childPaneStyle)
		{
			DockableGroupPane newDockableGroupPane = new DockableGroupPane(key, text)
			{
				ChildPaneStyle = childPaneStyle
			};

			if (parentPane is DockAreaPane)
				(parentPane as DockAreaPane).Panes.Add(newDockableGroupPane);
			else if (parentPane is DockableGroupPane)
				(parentPane as DockableGroupPane).Panes.Add(newDockableGroupPane);

			return newDockableGroupPane;
		}
		#endregion

		#region AddDockableControlPane
		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="parentKey">추가할 parent의 key</param>
		/// <param name="control">pane에 놓일 control</param>
		/// <returns>추가된 DockControlPane의 key(추가되지 못할 경우 <seealso cref="System.String.Empty"></seealso> 반환</returns>
		public string AddDockableControlPane(string parentKey, Control control)
		{
			string text = null;

			return AddDockableControlPane(parentKey, text, control);
		}

		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="parentKey">추가할 parent의 key</param>
		/// <param name="text">DockableControlPane의 display text</param>
		/// <param name="control">pane에 놓일 control</param>
		/// <returns>추가된 DockControlPane의 key(추가되지 못할 경우 <seealso cref="System.String.Empty"/> 반환</returns>
		public string AddDockableControlPane(string parentKey, string text, Control control)
		{
			DockableControlPane dockableControlPane = AddDockableControlPane(parentKey,
													  GetNewPaneKey(),
													  text,
													  control);

			return dockableControlPane != null ? dockableControlPane.Key : string.Empty;
		}

		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="dockablePaneBase">추가될 DockAreaPane이나 DockableGroupPane</param>
		/// <param name="control">pane에 놓일 control</param>
		/// <returns>추가된 DockControlPane의 key(추가되지 못할 경우 <seealso cref="System.String.Empty"></seealso> 반환</returns>
		public string AddDockableControlPane(DockablePaneBase dockablePaneBase, Control control)
		{
			string text = null;

			return AddDockableControlPane(dockablePaneBase, text, control);
		}

		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="dockablePaneBase">추가될 DockAreaPane이나 DockableGroupPane</param>
		/// <param name="text">DockableControlPane의 display text</param>
		/// <param name="control">pane에 놓일 control</param>
		/// <returns>추가된 DockControlPane의 key(추가되지 못할 경우 <seealso cref="System.String.Empty"></seealso> 반환</returns>
		public string AddDockableControlPane(DockablePaneBase dockablePaneBase, string text, Control control)
		{
			return AddDockableControlPane(dockablePaneBase.Key, text, control);
		}

		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="parentKey">추가할 parent의 key</param>
		/// <param name="key">추가될 pane의 key</param>
		/// <param name="text">DockableControlPane의 display text</param>
		/// <param name="control">pane에 놓일 control</param>
		/// <returns>추가된 DockableControlPane</returns>
		public DockableControlPane AddDockableControlPane(string parentKey, string key, string text, Control control)
		{
			DockablePaneBase dockablePaneBase = DockManager.PaneFromKey(parentKey);

			return AddDockableControlPane(dockablePaneBase, key, text, control);
		}

		/// <summary>
		/// 지정한 parent 아래 Dockable group pane을 추가합니다.
		/// </summary>
		/// <param name="dockablePaneBase">추가될 DockAreaPane이나 DockableGroupPane</param>
		/// <param name="key">추가될 pane의 key</param>
		/// <param name="text">DockableControlPane의 display text(null일 경우 control의 이름이<br/>
		/// <seealso cref=">System.String.Empty"/>일 경우는 공백이 설정될 수 있습니다.</param>
		/// <param name="control">pane에 놓일 control</param>
		/// <returns>추가된 DockableControlPane</returns>
		public DockableControlPane AddDockableControlPane(DockablePaneBase dockablePaneBase, string key, string text, Control control)
		{
			DockableControlPane newDockableControlPane = null;

			if (text != null)
            	newDockableControlPane = new DockableControlPane(key, text, control);
			else
				newDockableControlPane = new DockableControlPane(key, control.Name, control);

			if (dockablePaneBase is DockAreaPane)
				(dockablePaneBase as DockAreaPane).Panes.Add(newDockableControlPane);
			else if (dockablePaneBase is DockableGroupPane)
				(dockablePaneBase as DockableGroupPane).Panes.Add(newDockableControlPane);

			return newDockableControlPane;
		}
		#endregion

		#region GetDockableGroupPane
		/// <summary>
		/// 현재 UltraDockManager에서 Key에 의해 Dockable Group pane을 구합니다.
		/// </summary>
		/// <param name="key">찾고자 하는 key</param>
		/// <returns>찾은 DockableGroupPane, 또는 null</returns>
		public DockableGroupPane GetDockableGroupPane(string key)
		{
			return DockManager.PaneFromKey(key) as DockableGroupPane;
		}

		/// <summary>
		/// 현재 UltraDockManager에서 Dockable Group pane을 구합니다.
		/// </summary>
		/// <returns>찾은 DockableGroupPane</returns>
		public List<DockableGroupPane> GetDockableGroupPanes()
		{
			List<DockableGroupPane> groupPanes = new List<DockableGroupPane>();
			Queue<DockableGroupPane> queue = new Queue<DockableGroupPane>();

			foreach (DockAreaPane dockAreaPane in DockManager.DockAreas)
			{
				foreach (DockablePaneBase item in dockAreaPane.Panes.All)
				{
					if (item is DockableGroupPane)
					{
						DockableGroupPane groupPane = item as DockableGroupPane;

						groupPanes.Add(groupPane);
						queue.Enqueue(groupPane);
					}
				}
			}

			// GroupPane에 자식이 있는지 검사한다.
			while (queue.Count > 0)
			{
				DockableGroupPane item = queue.Dequeue();

				foreach (DockablePaneBase dockablePaneBase in item.Panes)
				{
					if (dockablePaneBase is DockableGroupPane)
					{
						DockableGroupPane groupPane = dockablePaneBase as DockableGroupPane;

						groupPanes.Add(groupPane);
						queue.Enqueue(groupPane);
					}
				}
			}

			return groupPanes;
		}
		#endregion

		#region SetActivePane
		/// <summary>
		/// 지정한 key를 가진 pane을 활성화 합니다.
		/// </summary>
		/// <param name="key">활성화 할 pane의 key</param>
		/// <returns></returns>
		public bool SetActivePane(string key)
		{
			return DockManager.PaneFromKey(key).Activate();
		}
		#endregion

		#region Load & Save xDocManager setting
		/// <summary>
		/// Docking Manager 상태를 Load 합니다.
		/// </summary>
		/// <param name="filePath">설정 내용을 저장할 파일 path</param>
		public void LoadXmlSetting(string filePath)
		{
			DockManager.LoadFromXML(filePath);
		}

		/// <summary>
		/// Docking Manager 상태를 Save 합니다.
		/// </summary>
		/// <param name="filePath">설정 내용을 저장할 파일 path</param>
		public void SaveXmlSetting(string filePath)
		{
			DockManager.SaveAsXML(filePath);
		}
		#endregion

		#region GetNewPaneKey
		/// <summary>
		/// 새로운 Pane key를 구합니다.
		/// </summary>
		/// <returns>새로운 Guid</returns>
		protected string GetNewPaneKey()
		{
			return Guid.NewGuid().ToString();
		}
		#endregion
	}
}
