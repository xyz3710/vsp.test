﻿namespace DockManager02
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.xlblTop1 = new Infragistics.Win.Misc.UltraLabel();
			this.xlblTop2 = new Infragistics.Win.Misc.UltraLabel();
			this.xlblBottom1 = new Infragistics.Win.Misc.UltraLabel();
			this.xlblBottom2 = new Infragistics.Win.Misc.UltraLabel();
			this.btnLoad = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnInit = new System.Windows.Forms.Button();
			this.propertyGrid = new System.Windows.Forms.PropertyGrid();
			this.btnRefresh = new System.Windows.Forms.Button();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// xlblTop1
			// 
			this.xlblTop1.AutoSize = true;
			this.xlblTop1.Location = new System.Drawing.Point(284, 138);
			this.xlblTop1.Name = "xlblTop1";
			this.xlblTop1.Size = new System.Drawing.Size(53, 14);
			this.xlblTop1.TabIndex = 5;
			this.xlblTop1.Text = "xlblTop1";
			// 
			// xlblTop2
			// 
			this.xlblTop2.AutoSize = true;
			this.xlblTop2.Location = new System.Drawing.Point(284, 179);
			this.xlblTop2.Name = "xlblTop2";
			this.xlblTop2.Size = new System.Drawing.Size(53, 14);
			this.xlblTop2.TabIndex = 5;
			this.xlblTop2.Text = "xlblTop2";
			// 
			// xlblBottom1
			// 
			this.xlblBottom1.AutoSize = true;
			this.xlblBottom1.Location = new System.Drawing.Point(264, 220);
			this.xlblBottom1.Name = "xlblBottom1";
			this.xlblBottom1.Size = new System.Drawing.Size(73, 14);
			this.xlblBottom1.TabIndex = 5;
			this.xlblBottom1.Text = "xlblBottom1";
			// 
			// xlblBottom2
			// 
			this.xlblBottom2.AutoSize = true;
			this.xlblBottom2.Location = new System.Drawing.Point(264, 261);
			this.xlblBottom2.Name = "xlblBottom2";
			this.xlblBottom2.Size = new System.Drawing.Size(73, 14);
			this.xlblBottom2.TabIndex = 5;
			this.xlblBottom2.Text = "xlblBottom2";
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(262, 352);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(75, 23);
			this.btnLoad.TabIndex = 9;
			this.btnLoad.Text = "&Load";
			this.btnLoad.UseVisualStyleBackColor = true;
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(262, 402);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(75, 23);
			this.btnSave.TabIndex = 9;
			this.btnSave.Text = "&Save";
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnInit
			// 
			this.btnInit.Location = new System.Drawing.Point(262, 302);
			this.btnInit.Name = "btnInit";
			this.btnInit.Size = new System.Drawing.Size(75, 23);
			this.btnInit.TabIndex = 9;
			this.btnInit.Text = "&Init";
			this.btnInit.UseVisualStyleBackColor = true;
			this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
			// 
			// propertyGrid
			// 
			this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.propertyGrid.Location = new System.Drawing.Point(0, 0);
			this.propertyGrid.Name = "propertyGrid";
			this.propertyGrid.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
			this.propertyGrid.Size = new System.Drawing.Size(482, 633);
			this.propertyGrid.TabIndex = 0;
			// 
			// btnRefresh
			// 
			this.btnRefresh.Location = new System.Drawing.Point(262, 452);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new System.Drawing.Size(75, 23);
			this.btnRefresh.TabIndex = 13;
			this.btnRefresh.Text = "&Refresh";
			this.btnRefresh.UseVisualStyleBackColor = true;
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.btnInit);
			this.splitContainer1.Panel1.Controls.Add(this.btnLoad);
			this.splitContainer1.Panel1.Controls.Add(this.btnRefresh);
			this.splitContainer1.Panel1.Controls.Add(this.btnSave);
			this.splitContainer1.Panel1.Controls.Add(this.xlblBottom2);
			this.splitContainer1.Panel1.Controls.Add(this.xlblTop1);
			this.splitContainer1.Panel1.Controls.Add(this.xlblBottom1);
			this.splitContainer1.Panel1.Controls.Add(this.xlblTop2);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.propertyGrid);
			this.splitContainer1.Size = new System.Drawing.Size(916, 633);
			this.splitContainer1.SplitterDistance = 430;
			this.splitContainer1.TabIndex = 14;
			// 
			// Form1
			// 
			this.AcceptButton = this.btnInit;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(916, 633);
			this.Controls.Add(this.splitContainer1);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.PerformLayout();
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private Infragistics.Win.Misc.UltraLabel xlblTop1;
		private Infragistics.Win.Misc.UltraLabel xlblBottom1;
		private Infragistics.Win.Misc.UltraLabel xlblBottom2;
		private Infragistics.Win.Misc.UltraLabel xlblTop2;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.Button btnInit;
		private System.Windows.Forms.PropertyGrid propertyGrid;
		private System.Windows.Forms.Button btnRefresh;
		private System.Windows.Forms.SplitContainer splitContainer1;
	}
}

