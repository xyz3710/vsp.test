﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Infragistics.Win.UltraWinDock;
using Infragistics.Win.UltraWinTabs;
using AnyFactory.FX.Win.Controls.UltraDockManagerHelper;

namespace DockManager02
{
	public static class Extension
	{
		//public static DockAreaPane
	}

	public partial class Form1 : Form
	{
		private UltraDockManager xDockManager;

		public Form1()
		{
			InitializeComponent();
			
			xDockManager = new UltraDockManager();
			xDockManager.HostControl = this;
			propertyGrid.SelectedObject = xDockManager;
		}

		private string SettingFilePath
		{
			get
			{
				return Path.Combine(Application.StartupPath, "xDocManagerSettings.xml");
			}
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			try
			{
				xDockManager.LoadFromXML(SettingFilePath);

				MessageBox.Show(string.Format("Load OK to {0}", SettingFilePath),
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);

			}
			catch (Exception ex)
			{
				MessageBox.Show(string.Format("Load Fail cause {0}", ex.Message),
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);
			}
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				xDockManager.SaveAsXML(SettingFilePath);

				MessageBox.Show(string.Format("Save OK to {0}", SettingFilePath),
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);
                
			}
			catch (Exception ex)
			{
				MessageBox.Show(string.Format("Save Fail cause {0}", ex.Message),
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);
			}
		}

		private void btnInit_Click(object sender, EventArgs e)
		{
			InitDockManagerByHelper();
			//InitDockManagerByKey();
		}

		private void InitDockManager()
		{
			xDockManager.DockAreas.Clear();

			DockAreaPane dockAreaPane1 = new DockAreaPane(DockedLocation.DockedLeft, new System.Guid("6692ba04-5e32-457f-a51a-32792864ad32"))
			{
				Text = "dockAreaPane1",
			};
			// Top
			DockableGroupPane dockableGroupPane10 = new DockableGroupPane(new System.Guid("0e42dd56-bb92-4755-a340-1e52db4bd5f2"), Guid.Empty, -1, new System.Guid("6692ba04-5e32-457f-a51a-32792864ad32"), 1)
			{
				Text = "dockableGroupPane10",
				ChildPaneStyle = ChildPaneStyle.TabGroup,
				Size = new Size(200, ClientSize.Height)
			};			
			// Bottom
			DockableGroupPane dockableGroupPane20 = new DockableGroupPane(new System.Guid("d8d4cdfd-c2f5-4c7a-b3fa-d75613a13fbb"), Guid.Empty, -1, new System.Guid("6692ba04-5e32-457f-a51a-32792864ad32"), 2)
			{
				Text = "dockableGroupPane20",
				ChildPaneStyle = ChildPaneStyle.TabGroup,
				Size = new Size(200, ClientSize.Height),
			};
			DockableControlPane dockableControlPane11 = new DockableControlPane(new System.Guid("9d727a4d-0ae5-4aee-a6c6-5e5a51649b23"), new System.Guid("76c69943-af40-4d13-ad5a-1730aa66cfaa"), -1, new System.Guid("0e42dd56-bb92-4755-a340-1e52db4bd5f2"), 0)
			{
				Text = "Top1",
			};
			DockableControlPane dockableControlPane12 = new DockableControlPane(new System.Guid("d7027b78-51e8-452e-94b4-860655aebdbe"), new System.Guid("76c69943-af40-4d13-ad5a-1730aa66cfaa"), -1, new System.Guid("0e42dd56-bb92-4755-a340-1e52db4bd5f2"), 1)
			{
				Text = "Top2",
			};
			DockableControlPane dockableControlPane21 = new DockableControlPane(new System.Guid("55ad55c5-d3fe-4bc4-b6aa-7623d2c7f79c"), Guid.Empty, -1, new System.Guid("d8d4cdfd-c2f5-4c7a-b3fa-d75613a13fbb"), 2)
			{
				Text = "Bottom1",
			};
			DockableControlPane dockableControlPane22 = new DockableControlPane(new System.Guid("eb33ff9d-4d47-4038-a320-e0f3fc4672a2"), Guid.Empty, -1, new System.Guid("d8d4cdfd-c2f5-4c7a-b3fa-d75613a13fbb"), 3)
			{
				Text = "Bottom2",
			};

			// Left DockedLocation에 Top/Bottom group 추가
			dockAreaPane1.Panes.Add(dockableGroupPane10);
			dockAreaPane1.Panes.Add(dockableGroupPane20);

			// Top Group에 ControlPane 추가
			dockableGroupPane10.Panes.Add(dockableControlPane11);
			dockableGroupPane10.Panes.Add(dockableControlPane12);
			// Bottom Group에 ControlPane 추가
			dockableGroupPane20.Panes.Add(dockableControlPane21);
			dockableGroupPane20.Panes.Add(dockableControlPane22);

			// ControlPane에 Control 추가
			dockableControlPane11.Control = xlblTop1;
			dockableControlPane12.Control = xlblTop2;
			// ControlPane에 Control 추가
			dockableControlPane21.Control = xlblBottom1;
			dockableControlPane22.Control = xlblBottom2;

			dockableGroupPane10.Unpin();
			dockableGroupPane20.Unpin();

			//xDockManager.DefaultGroupSettings.TabSizing = TabSizing.SizeToFit;
			xDockManager.DefaultGroupSettings.TabStyle = TabStyle.PropertyPageFlat;
			xDockManager.DefaultGroupSettings.PinButtonBehavior = PinButtonBehavior.PinTabGroup;
			xDockManager.LayoutStyle = DockAreaLayoutStyle.FillContainer;
			// VS2005 style로 변경한 뒤 Drag Indicator가 나오게 한다.
			xDockManager.WindowStyle = WindowStyle.VisualStudio2005;
			xDockManager.DragWindowStyle = DragWindowStyle.LayeredWindowWithIndicators;

			//xDockManager.DefaultPaneSettings.BorderStylePane = UIElementBorderStyle.Rounded4;
			// Hide, AutoHide 메뉴등을 안나오게 한다.(Float 메뉴만 나온다)
			xDockManager.ShowDisabledButtons = false;

			xDockManager.DockAreas.Add(dockAreaPane1);
			xDockManager.HostControl = this;
		}

		const string DockAreaKey = "dockAreaKey";
		const string DockableGroupPane10Key = "dockableGroupPane10Key";
		const string DockableGroupPane20Key = "dockableGroupPane20Key";

		private void ClearPane(UltraDockManager ultraDockManager)
		{
			ultraDockManager.FlyIn(false);
			ultraDockManager.ControlPanes.Clear();
			ultraDockManager.DockAreas.Clear();			
		}

		private void SetDockManagerSetting(UltraDockManager ultraDockManager)
		{
			//ultraDockManager.DefaultGroupSettings.TabSizing = TabSizing.SizeToFit;
			ultraDockManager.DefaultGroupSettings.TabStyle = TabStyle.PropertyPageFlat;
			ultraDockManager.DefaultGroupSettings.PinButtonBehavior = PinButtonBehavior.PinTabGroup;
			ultraDockManager.LayoutStyle = DockAreaLayoutStyle.FillContainer;
			// VS2005 style로 변경한 뒤 Drag Indicator가 나오게 한다.
			ultraDockManager.WindowStyle = WindowStyle.VisualStudio2005;
			ultraDockManager.DragWindowStyle = DragWindowStyle.LayeredWindowWithIndicators;

			//ultraDockManager.DefaultPaneSettings.BorderStylePane = UIElementBorderStyle.Rounded4;
			// Hide, AutoHide 메뉴등을 안나오게 한다.(Float 메뉴만 나온다)
			ultraDockManager.ShowDisabledButtons = false;
		}

		private void InitDockManagerByKey()
		{
			ClearPane(xDockManager);

            DockAreaPane dockAreaPane1 = new DockAreaPane(DockedLocation.DockedLeft, DockAreaKey)
			{
				Text = "dockAreaPane1",
			};
			// Top
            DockableGroupPane dockableGroupPane10 = new DockableGroupPane(DockableGroupPane10Key)
			{
				Text = "dockableGroupPane10",
				ChildPaneStyle = ChildPaneStyle.TabGroup,
				Size = new Size(200, ClientSize.Height),				
			};
			// Bottom
            DockableGroupPane dockableGroupPane20 = new DockableGroupPane(DockableGroupPane20Key)
			{
				Text = "dockableGroupPane20",
				ChildPaneStyle = ChildPaneStyle.SlidingGroup,
				Size = new Size(200, ClientSize.Height),
			};
			DockableControlPane dockableControlPane11 = new DockableControlPane("dockableControlPane11Key")
			{
				Text = "Top1",
			};
			DockableControlPane dockableControlPane12 = new DockableControlPane("dockableControlPane12Key")
			{
				Text = "Top2",
			};
			DockableControlPane dockableControlPane21 = new DockableControlPane("dockableControlPane121Key")
			{
				Text = "Bottom1",
			};
			DockableControlPane dockableControlPane22 = new DockableControlPane("dockableControlPane122Key")
			{
				Text = "Bottom2",
			};

			// Left DockedLocation에 Top/Bottom group 추가
			dockAreaPane1.Panes.Add(dockableGroupPane10);
			dockAreaPane1.Panes.Add(dockableGroupPane20);

			// Top Group에 ControlPane 추가
			dockableGroupPane10.Panes.Add(dockableControlPane11);
			dockableGroupPane10.Panes.Add(dockableControlPane12);
			
			// Bottom Group에 ControlPane 추가
			dockableGroupPane20.Panes.Add(dockableControlPane21);
			dockableGroupPane20.Panes.Add(dockableControlPane22);

			// ControlPane에 Control 추가
			dockableControlPane11.Control = xlblTop1;
			dockableControlPane12.Control = xlblTop2;
			// ControlPane에 Control 추가
			dockableControlPane21.Control = xlblBottom1;
			dockableControlPane22.Control = xlblBottom2;

			dockAreaPane1.Unpin();
			//dockableGroupPane10.Unpin();
			//dockableGroupPane20.Unpin();

			SetDockManagerSetting(xDockManager);
			xDockManager.DockAreas.Add(dockAreaPane1);
			xDockManager.HostControl = this;
		}
		
		private void InitDockManagerByHelper()
		{
			UltraDockManagerHelper xDockManagerHelper = new UltraDockManagerHelper(this);

			ClearPane(xDockManagerHelper.DockManager);
			SetDockManagerSetting(xDockManagerHelper.DockManager);

			xDockManagerHelper.DockManager.ControlPanes.Clear();

			string areaKey = xDockManagerHelper.AddDockAreaPane(DockedLocation.DockedRight);
			DockableGroupPane dockableGroupPane1
				= xDockManagerHelper.AddDockableGroupPane(areaKey, Guid.NewGuid().ToString(), "Test Group1", ChildPaneStyle.SlidingGroup);
			DockableGroupPane dockableGroupPane2
				= xDockManagerHelper.AddDockableGroupPane(areaKey, Guid.NewGuid().ToString(), string.Empty, ChildPaneStyle.TabGroup);

			xDockManagerHelper.AddDockableControlPane(dockableGroupPane1, "Top1", xlblTop1);
			xDockManagerHelper.AddDockableControlPane(dockableGroupPane1, xlblTop2);
			xDockManagerHelper.AddDockableControlPane(dockableGroupPane2, xlblBottom2);
			xDockManagerHelper.AddDockableControlPane(dockableGroupPane2, "Bottom1", xlblBottom1);
			// Unpin 상태로 시작한다.
			xDockManagerHelper.DockManager.UnpinAll();
			xDockManagerHelper.DockManager.FlyIn(false);
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			propertyGrid.Refresh();

			DockablePaneBase dockablePaneBase = xDockManager.PaneFromKey(DockableGroupPane20Key);

			if (dockablePaneBase is DockableGroupPane)
			{
				DockableGroupPane dockableGroupPane = dockablePaneBase as DockableGroupPane;
				List<DockablePaneBase> panes = new List<DockablePaneBase>((IEnumerable<DockablePaneBase>)dockableGroupPane.Panes.All);
			}
		}
	}
}
