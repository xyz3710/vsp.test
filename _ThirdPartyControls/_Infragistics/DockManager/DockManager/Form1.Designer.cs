﻿namespace DockManager
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.UltraWinDock.DockAreaPane dockAreaPane1 = new Infragistics.Win.UltraWinDock.DockAreaPane(Infragistics.Win.UltraWinDock.DockedLocation.DockedLeft, new System.Guid("67ef5e1f-6651-4a5b-b812-bf48c9f37921"));
			Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane1 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("0d96ac19-4234-43a1-a591-44de22493864"), new System.Guid("00000000-0000-0000-0000-000000000000"), -1, new System.Guid("67ef5e1f-6651-4a5b-b812-bf48c9f37921"), -1);
			Infragistics.Win.UltraWinDock.DockableControlPane dockableControlPane2 = new Infragistics.Win.UltraWinDock.DockableControlPane(new System.Guid("317dd8d6-ea00-41d5-b5b4-3e64fe83b71f"), new System.Guid("00000000-0000-0000-0000-000000000000"), -1, new System.Guid("67ef5e1f-6651-4a5b-b812-bf48c9f37921"), -1);
			this.ultraTree1 = new Infragistics.Win.UltraWinTree.UltraTree();
			this.ultraTree2 = new Infragistics.Win.UltraWinTree.UltraTree();
			this.xDockManager = new Infragistics.Win.UltraWinDock.UltraDockManager(this.components);
			this._Form1UnpinnedTabAreaLeft = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1UnpinnedTabAreaRight = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1UnpinnedTabAreaTop = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1UnpinnedTabAreaBottom = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._Form1AutoHideControl = new Infragistics.Win.UltraWinDock.AutoHideControl();
			this.dockableWindow1 = new Infragistics.Win.UltraWinDock.DockableWindow();
			this.dockableWindow2 = new Infragistics.Win.UltraWinDock.DockableWindow();
			this.windowDockingArea1 = new Infragistics.Win.UltraWinDock.WindowDockingArea();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xDockManager)).BeginInit();
			this._Form1AutoHideControl.SuspendLayout();
			this.dockableWindow1.SuspendLayout();
			this.dockableWindow2.SuspendLayout();
			this.SuspendLayout();
			// 
			// ultraTree1
			// 
			this.ultraTree1.Location = new System.Drawing.Point(0, 18);
			this.ultraTree1.Name = "ultraTree1";
			this.ultraTree1.Size = new System.Drawing.Size(200, 575);
			this.ultraTree1.TabIndex = 5;
			this.ultraTree1.Click += new System.EventHandler(this.ultraTree1_Click);
			// 
			// ultraTree2
			// 
			this.ultraTree2.Location = new System.Drawing.Point(0, 18);
			this.ultraTree2.Name = "ultraTree2";
			this.ultraTree2.Size = new System.Drawing.Size(200, 575);
			this.ultraTree2.TabIndex = 5;
			// 
			// xDockManager
			// 
			this.xDockManager.AutoHideDelay = 1;
			dockAreaPane1.ChildPaneStyle = Infragistics.Win.UltraWinDock.ChildPaneStyle.TabGroup;
			dockableControlPane1.Control = this.ultraTree1;
			dockableControlPane1.FlyoutSize = new System.Drawing.Size(200, -1);
			dockableControlPane1.OriginalControlBounds = new System.Drawing.Rectangle(31, 44, 121, 97);
			dockableControlPane1.Pinned = false;
			dockableControlPane1.Size = new System.Drawing.Size(100, 100);
			dockableControlPane1.Text = "ultraTree1";
			dockableControlPane1.TextTab = "test1";
			dockableControlPane1.ToolTipCaption = "tt1";
			dockableControlPane1.ToolTipTab = "tt11";
			dockableControlPane2.Control = this.ultraTree2;
			dockableControlPane2.FlyoutSize = new System.Drawing.Size(200, -1);
			dockableControlPane2.OriginalControlBounds = new System.Drawing.Rectangle(31, 164, 121, 97);
			dockableControlPane2.Pinned = false;
			dockableControlPane2.Size = new System.Drawing.Size(100, 100);
			dockableControlPane2.Text = "ultraTree2";
			dockableControlPane2.TextTab = "test2";
			dockableControlPane2.ToolTipCaption = "tt2";
			dockableControlPane2.ToolTipTab = "tt22";
			dockAreaPane1.Panes.AddRange(new Infragistics.Win.UltraWinDock.DockablePaneBase[] {
            dockableControlPane1,
            dockableControlPane2});
			dockAreaPane1.SelectedTabIndex = 1;
			dockAreaPane1.Size = new System.Drawing.Size(200, 593);
			this.xDockManager.DockAreas.AddRange(new Infragistics.Win.UltraWinDock.DockAreaPane[] {
            dockAreaPane1});
			this.xDockManager.HostControl = this;
			// 
			// _Form1UnpinnedTabAreaLeft
			// 
			this._Form1UnpinnedTabAreaLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this._Form1UnpinnedTabAreaLeft.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaLeft.Location = new System.Drawing.Point(0, 0);
			this._Form1UnpinnedTabAreaLeft.Name = "_Form1UnpinnedTabAreaLeft";
			this._Form1UnpinnedTabAreaLeft.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaLeft.Size = new System.Drawing.Size(21, 593);
			this._Form1UnpinnedTabAreaLeft.TabIndex = 0;
			// 
			// _Form1UnpinnedTabAreaRight
			// 
			this._Form1UnpinnedTabAreaRight.Dock = System.Windows.Forms.DockStyle.Right;
			this._Form1UnpinnedTabAreaRight.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaRight.Location = new System.Drawing.Point(1046, 0);
			this._Form1UnpinnedTabAreaRight.Name = "_Form1UnpinnedTabAreaRight";
			this._Form1UnpinnedTabAreaRight.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaRight.Size = new System.Drawing.Size(0, 593);
			this._Form1UnpinnedTabAreaRight.TabIndex = 1;
			// 
			// _Form1UnpinnedTabAreaTop
			// 
			this._Form1UnpinnedTabAreaTop.Dock = System.Windows.Forms.DockStyle.Top;
			this._Form1UnpinnedTabAreaTop.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaTop.Location = new System.Drawing.Point(21, 0);
			this._Form1UnpinnedTabAreaTop.Name = "_Form1UnpinnedTabAreaTop";
			this._Form1UnpinnedTabAreaTop.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaTop.Size = new System.Drawing.Size(1025, 0);
			this._Form1UnpinnedTabAreaTop.TabIndex = 2;
			// 
			// _Form1UnpinnedTabAreaBottom
			// 
			this._Form1UnpinnedTabAreaBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this._Form1UnpinnedTabAreaBottom.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1UnpinnedTabAreaBottom.Location = new System.Drawing.Point(21, 593);
			this._Form1UnpinnedTabAreaBottom.Name = "_Form1UnpinnedTabAreaBottom";
			this._Form1UnpinnedTabAreaBottom.Owner = this.xDockManager;
			this._Form1UnpinnedTabAreaBottom.Size = new System.Drawing.Size(1025, 0);
			this._Form1UnpinnedTabAreaBottom.TabIndex = 3;
			// 
			// _Form1AutoHideControl
			// 
			this._Form1AutoHideControl.Controls.Add(this.dockableWindow1);
			this._Form1AutoHideControl.Controls.Add(this.dockableWindow2);
			this._Form1AutoHideControl.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this._Form1AutoHideControl.Location = new System.Drawing.Point(21, 0);
			this._Form1AutoHideControl.Name = "_Form1AutoHideControl";
			this._Form1AutoHideControl.Owner = this.xDockManager;
			this._Form1AutoHideControl.Size = new System.Drawing.Size(15, 593);
			this._Form1AutoHideControl.TabIndex = 4;
			// 
			// dockableWindow1
			// 
			this.dockableWindow1.Controls.Add(this.ultraTree1);
			this.dockableWindow1.Location = new System.Drawing.Point(-10000, 0);
			this.dockableWindow1.Name = "dockableWindow1";
			this.dockableWindow1.Owner = this.xDockManager;
			this.dockableWindow1.Size = new System.Drawing.Size(200, 593);
			this.dockableWindow1.TabIndex = 9;
			// 
			// dockableWindow2
			// 
			this.dockableWindow2.Controls.Add(this.ultraTree2);
			this.dockableWindow2.Location = new System.Drawing.Point(0, 0);
			this.dockableWindow2.Name = "dockableWindow2";
			this.dockableWindow2.Owner = this.xDockManager;
			this.dockableWindow2.Size = new System.Drawing.Size(200, 593);
			this.dockableWindow2.TabIndex = 10;
			// 
			// windowDockingArea1
			// 
			this.windowDockingArea1.Dock = System.Windows.Forms.DockStyle.Left;
			this.windowDockingArea1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.windowDockingArea1.Location = new System.Drawing.Point(21, 0);
			this.windowDockingArea1.Name = "windowDockingArea1";
			this.windowDockingArea1.Owner = this.xDockManager;
			this.windowDockingArea1.Size = new System.Drawing.Size(205, 593);
			this.windowDockingArea1.TabIndex = 6;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(766, 89);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 7;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(766, 129);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 7;
			this.button2.Text = "button1";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(866, 89);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 8;
			this.button3.Text = "button3";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(866, 129);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 8;
			this.button4.Text = "button3";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1046, 593);
			this.Controls.Add(this._Form1AutoHideControl);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.windowDockingArea1);
			this.Controls.Add(this._Form1UnpinnedTabAreaBottom);
			this.Controls.Add(this._Form1UnpinnedTabAreaTop);
			this.Controls.Add(this._Form1UnpinnedTabAreaRight);
			this.Controls.Add(this._Form1UnpinnedTabAreaLeft);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.ultraTree1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraTree2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xDockManager)).EndInit();
			this._Form1AutoHideControl.ResumeLayout(false);
			this.dockableWindow1.ResumeLayout(false);
			this.dockableWindow2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private Infragistics.Win.UltraWinDock.UltraDockManager xDockManager;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaLeft;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaRight;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaTop;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _Form1UnpinnedTabAreaBottom;
		private Infragistics.Win.UltraWinDock.AutoHideControl _Form1AutoHideControl;
		private Infragistics.Win.UltraWinTree.UltraTree ultraTree1;
		private Infragistics.Win.UltraWinTree.UltraTree ultraTree2;
		private Infragistics.Win.UltraWinDock.WindowDockingArea windowDockingArea1;
		private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow1;
		private Infragistics.Win.UltraWinDock.DockableWindow dockableWindow2;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
	}
}

