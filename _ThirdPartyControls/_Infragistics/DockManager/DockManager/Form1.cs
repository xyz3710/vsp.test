﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DockManager
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			xDockManager.AnimationEnabled = false;
		}

		private void ultraTree1_Click(object sender, EventArgs e)
		{
			_Form1AutoHideControl.Hide();
			_Form1AutoHideControl.Invalidate();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			xDockManager.UnpinAll();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			xDockManager.PinAll();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			xDockManager.DockAreas[0].Close();
		}

		private void button4_Click(object sender, EventArgs e)
		{
			xDockManager.ControlPanes[0].Flyout(true);

		}
	}
}