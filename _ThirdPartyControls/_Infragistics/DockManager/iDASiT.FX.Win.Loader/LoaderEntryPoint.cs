﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.LoaderEntryPoint
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 10월 18일 목요일 오후 8:04
/*	Purpose		:	Loader를 호출하기 위한 EntryPoint 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	입력받는 Argument를 parsing 해서 3개의 값으로(PlantId, UserId, previous ProcessId)로 분리될경우
 *					On-Line으로 간주로 진행한다(추후 보안 로직을 강화시킨다)
/**********************************************************************************************************************/

using System;
using System.Windows.Forms;
using System.IO;
using Microsoft.SqlServer.MessageBox;
using System.Diagnostics;
using iDASiT.FX.Win.Configuration;

namespace iDASiT.FX.Win.Loader
{
	static class LoaderEntryPoint
	{
		/// <summary>
		/// Loader를 호출하기 위한 EntryPoint 입니다.
		/// </summary>
		[STAThread]
		[LoaderOptimization(LoaderOptimization.MultiDomainHost)]
		public static int Main(string[] args)
		{			
			string plantId = string.Empty;
			string userId = string.Empty;
			bool isDebugging = ConfigWrapper.Loader.DebugMode;

			if (args.Length == 0)
			{
				MessageBox.Show("로그인을 통해서 접근해야 합니다.", "잘못된 접근", MessageBoxButtons.OK, MessageBoxIcon.Warning);

				return -1;
			}
			
			if (args.Length > 0)
			{
				string argPlainText = args[0];

				// space가 문자로 넘어올 경우 args.Length가 1개 이상 나올 수 있다.
				for (int i = 1; i < args.Length; i++)
					argPlainText += string.Format(" {0}", args[i]);

				string[] arguments = ExchangeForm.GetArguments(argPlainText);

				if (arguments.Length >= 2)
				{
					plantId = arguments[0];
					userId = arguments[1];
				}

				if (arguments.Length == 3)
				{
					// LogIn에서 직접 호출한 경우(PlantId, UserId가 없다)와 Updater에서 호출한 경우 해당한다.
					if (arguments[2] != string.Empty)
						// 종료 Process를 지정하지 않으면 skip 시킨다.
						ExchangeForm.Kill(arguments[2]);
				}

				// PlantId와 UserId만 인수로 넘어올 경우 Debug 모드로 인식 시킨다.
				// Loader를 직접 호출 한 경우
				if (arguments.Length == 2)
					isDebugging = true;
			}

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			try
			{
				#region plantId, userId 가 string.Empty면 off-line 연결이므로 file에서 읽는다.
				/*
				// plantId, userId 가 string.Empty면 off-line 연결이므로 file에서 읽는다.
				if (plantId == string.Empty && userId == string.Empty)
				{
					LoaderSetting.LoadDefault();
					plantId = LoaderSetting.PlantId;
					userId = LoaderSetting.UserId;
					isDebugging = true;
				}
				*/
				#endregion

				if (CheckMenuConfigFilePath(isDebugging, userId) == false)
					Application.Exit();
				else
				{
					AutoServerSwitcherSetup();

					Application.Run(new Loader(plantId, userId, isDebugging));
				}
			}
			catch (Exception ex)
			{
				ExceptionHandler exceptionHandler = new ExceptionHandler(null);

				DialogResult dialogResult = 
						exceptionHandler.DoException(string.Format("응용프로그램에서 알 수 없는 오류 입니다.{0}시스템 담당자에게 문의하여 오류 처리를 하십시오.{0}", 
                                                         Environment.NewLine),
						ex);

				if (dialogResult == DialogResult.Abort)
					Application.Exit();
			}

			return 0;
		}

		private static bool CheckMenuConfigFilePath(bool isDebugging, string userInfo)
		{
			string message = "메뉴 파일이 없습니다.\r\n";
			string menuConfigFilePath = LoaderSetting.MenuConfigFilePath;
			string filePath = menuConfigFilePath;
			bool configDebugMode = LoaderWrapper.Instance.DebugMode;

			// parameter가 3개인 경우(Updater나 ServerChanger로 접근한 경우) - configDebugMode가 true : menus, false : menus_userId
			// parameter가 2개인 경우(Loader를 바로가기로 접근한 경우)       - configDebugMode가 true : menus, false : menus_userId
			if (configDebugMode == false)
			{
				string filename = Path.GetFileNameWithoutExtension(menuConfigFilePath);
				string userId = userInfo.Split(ExchangeForm.ArgumentDelimiter)[0];

				// menus_UserId.conifg
				filePath = menuConfigFilePath.Replace(filename, string.Format("{0}_{1}", filename, userId));
				message += "\r\n다시 로그인 하십시오.\r\n문제가 계속 되면 관리자에게 문의 하시기 바랍니다.";

				// Loader 바로 가기로 접근했을 경우 menus_userId 파일이 없으면 menuFile로 생성한다.
				if (isDebugging == true 
					&& File.Exists(filePath) == false && File.Exists(menuConfigFilePath) == true)
				{
					File.Copy(menuConfigFilePath, filePath, true);
					File.SetAttributes(filePath, FileAttributes.Archive);
				}
			}
			else
				message += string.Format("DebugMode에서는 {0} 파일이 존재 해야 합니다.", menuConfigFilePath);

			if (File.Exists(filePath) == false)
			{
				MessageBox.Show(message,
									"Loader 접근 오류",
									MessageBoxButtons.OK,
									MessageBoxIcon.Error);

				return false;
			}                

			return true;
		}

		private static void AutoServerSwitcherSetup()
		{
			// NOTICE: AutoServerSwitcher를 위한 임시 setup
			// by KIMKIWON\xyz37 in 2008년 6월 18일 수요일 오후 12:08
			FrameworkSetup frameworkSetup = FrameworkSetup.Instance;

			// Company setting
			frameworkSetup.GacUtilFilename = string.Format(@"{0}\gacutil.exe", frameworkSetup.WorkingFolder);
			frameworkSetup.LogFolder = string.Format(@"{0}\Log", frameworkSetup.InstallFolder);
			frameworkSetup.MenuConfigFilePath = string.Format(@"{0}\menus.config", frameworkSetup.InstallFolder);
			frameworkSetup.CacheFolder = string.Format(@"{0}\iDASiT.FX.Win.Cache", Path.GetTempPath());
			frameworkSetup.InstallFolder = frameworkSetup.InstallFolder;

			// Framework setting
			frameworkSetup.AutoServerSwitcherPath = string.Format(@"{0}\Utility.AutoServerSwitcher.exe", frameworkSetup.WorkingFolder);

			// 바로가기 변경
			WriteShortcut();
		}

		private static void WriteShortcut()
		{
			#region For Test
			// TEST: For Test in WriteShortcut
			// by KIMKIWON\xyz37 in 2008년 10월 17일 금요일 오후 12:08
			/*
			FrameworkSetup frameworkSetup = FrameworkSetup.Instance;
			string workingFolder = frameworkSetup.WorkingFolder;
			string appPath = frameworkSetup.AutoServerSwitcherPath;
			string shortcutName = "통합 생산정보 시스템";
			string shortcutDesc = "통합 생산정보 시스템(Poly Crystalline Silicon)에 로그인 합니다.";
			string targetPath = Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory);
			string shortcutPath = string.Format("{0}\\{1}.lnk", targetPath, shortcutName);
			int iconNum = 1;		// 2번째 Icon으로 생성한다.
			IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
			IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutPath);

			shortcut.TargetPath = appPath;
			shortcut.Description = shortcutDesc;
			shortcut.WindowStyle = 1; // WshNormalFocus
			shortcut.WorkingDirectory = workingFolder;
			shortcut.IconLocation = string.Format("{0},{1}", appPath, iconNum);
			shortcut.Save();
			*/
			#endregion
		}
	}
}