﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.IMessageAgent
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 8월 11일 월요일 오후 3:34
/*	Purpose		:	Client 폼에서 다른 Message Agent를 제어하기 위한 Instance Interface입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Loader
{
	/// <summary>
	/// Client 폼에서 다른 Message Agent를 제어하기 위한 Instance Interface입니다.
	/// </summary>
	public interface IMessageAgent
	{
		/// <summary>
		/// Loader에서 Server와 통신하는 MessageAgent의 Instance를 구합니다.
		/// </summary>
		MessageAgent NewInstance
		{
			get;
		}
	}

}
