﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.DockManager.DockableGroupPaneKeys
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 11월 30일 금요일 오전 8:15
/*	Purpose		:	DockableGroupPane의 Key를 위한 enum
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Loader
{
	internal enum DockableGroupPaneKeys
	{
		/// <summary>
		/// LeftGroupPaneTopKey
		/// </summary>
		LeftTopGroupPane,
		/// <summary>
		/// LeftGroupPaneBottomKey
		/// </summary>
		LeftBottomGroupPane,
		/// <summary>
		/// LeftMenuAreaPaneKey
		/// </summary>
		LeftAreaPaneKey
	}
}
