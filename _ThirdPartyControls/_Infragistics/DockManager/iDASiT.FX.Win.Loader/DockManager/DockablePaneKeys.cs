﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.DockablePaneKeys
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 11월 29일 목요일 오후 5:18
/*	Purpose		:	DockablePane의 Key를 위한 enum
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Loader
{
	internal enum DockablePaneKeys
	{
		/// <summary>
		/// 1번째 menu pane
		/// </summary>
		Menu1 = 1,
		/// <summary>
		/// 2번째 menu pane
		/// </summary>
		Menu2,
		/// <summary>
		/// 3번째 menu pane
		/// </summary>
		Menu3,
		/// <summary>
		/// 4번째 menu pane
		/// </summary>
		Menu4,
		/// <summary>
		/// 5번째 menu pane
		/// </summary>
		Menu5,
		/// <summary>
		/// 6번째 menu pane
		/// </summary>
		Menu6,
		/// <summary>
		/// 7번째 menu pane
		/// </summary>
		Menu7,
		/// <summary>
		/// 8번째 menu pane
		/// </summary>
		Menu8,
		/// <summary>
		/// 9번째 menu pane
		/// </summary>
		Menu9,
		/// <summary>
		/// 즐겨 찾기 메뉴 pane
		/// </summary>
		Favorite,
		/// <summary>
		/// 검색된 메뉴 pane
		/// </summary>
		SearchResult,
	}
}
