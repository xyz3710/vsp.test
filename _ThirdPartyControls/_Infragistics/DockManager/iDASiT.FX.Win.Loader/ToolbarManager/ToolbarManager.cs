/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.ToolbarManager
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 11월 13일 목요일 오후 6:11
/*	Purpose		:	Loader의 Toolbar에 관련된 class입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using iDASiT.FX.Win.Configuration;
using Infragistics.Win;
using Infragistics.Win.UltraWinToolbars;
using iDASiT.FX.Win.Configuration.Toolbars;
using iDASiT.FX.Win.Configuration.Handler;
using System.Drawing;
using System.IO;
using System.ComponentModel;

namespace iDASiT.FX.Win.Loader
{
	/// <summary>
	/// Loader의 Toolbar에 관련된 class입니다.
	/// </summary>
	public class ToolbarManager : UltraToolbarsManager
	{
		#region Constants
		/// <summary>
		/// 
		/// </summary>
		public static readonly string MainToolbarCategoryName = "메인 도구";
		/// <summary>
		/// 
		/// </summary>
        public static readonly string StandardToolbarCategoryName = "표준 도구";
		private const string MAINMENU_KEY = "MainMenuKey";
		private const string STANDARD_KEY = "StandardKey";
		#endregion

		#region Fields
		private IMainToolbarAction _mainToolbarAction;
		private IStandardToolbarAction _standardToolbarAction;
		private Control _container;
		private bool _existMainToolbar;
		private bool _existStandardToolbar;
		private UltraToolbarsDockArea _DockableTreeForm_Toolbars_Dock_Area_Left;
		private UltraToolbarsDockArea _DockableTreeForm_Toolbars_Dock_Area_Right;
		private UltraToolbarsDockArea _DockableTreeForm_Toolbars_Dock_Area_Top;
        private UltraToolbarsDockArea _DockableTreeForm_Toolbars_Dock_Area_Bottom;
		private UltraToolbar _standardToolbar;
		private UltraToolbar _mainToolbar;        
		#endregion

		#region Constructors
		/// <summary>
		/// ToolbarManager class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="container"></param>
		/// <param name="mainToolbarAction"></param>
		/// <param name="standardToolbarAction"></param>
		public ToolbarManager(Control container, IMainToolbarAction mainToolbarAction, IStandardToolbarAction standardToolbarAction)
		{
			_container = container;
			MainToolbarAction = mainToolbarAction;
			StandardToolbarAction = standardToolbarAction;
			InitToolbarBySetting();
			
			base.ToolClick += new ToolClickEventHandler(OnToolClick);
			base.ToolKeyDown += new ToolKeyEventHandler(OnToolKeyDown);
		}

		private void InitToolbarBySetting()
		{
			List<ToolbarInfo> toolbars = ConfigWrapper.Loader.Toolbars;
			List<ToolbarInfo> mainToolbars = new List<ToolbarInfo>();
			List<ToolbarInfo> standardToolbars = new List<ToolbarInfo>();

			if (toolbars == null)
			{
				toolbars = new List<ToolbarInfo>();
				
				#region Toolbar Collections
				using (MessageSettings mSettings = new MessageSettings())
				{
					ToolbarInfo logoInfo 
						= new ToolbarInfo(ToolbarKeys.Logo,
							  "",
							  Shortcut.None,
							  "회사 로고",
							  0,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Logo.gif",
							  string.Empty,
							  false,
							  false,
							  true,
							  false,
							  true,
							  false,
							  true);
					ToolbarInfo favoritesInfo 
						= new ToolbarInfo(ToolbarKeys.Favorites,
							  "즐겨 찾기",
							  Shortcut.None,
							  mSettings["MenuFavoritesTooltip"],
							  1,
							  ToolStripItemDisplayStyle.ImageAndText,
							  @".\Resources\24\Favorites.gif",
							  @".\Resources\16\Favorites.gif",
							  true,
							  false,
							  true,
							  false,
							  true,
							  false,
							  true);
					ToolbarInfo menuNameInfo 
						= new ToolbarInfo(ToolbarKeys.MenuName,
							  "메뉴 이름",
							  Shortcut.CtrlF,
							  mSettings["MenuMenunameTooltip"],
							  2,
							  ToolStripItemDisplayStyle.Image,
							  string.Empty,
							  string.Empty,
							  false,
							  false,
							  true,
							  false,
							  true,
							  false,
							  true);
					ToolbarInfo findMenuInfo 
						= new ToolbarInfo(ToolbarKeys.FindMenu,
							  "메뉴 검색",
							  Shortcut.None,
							  mSettings["MenuSearchTooltip"],
							  3,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\FindMenu.gif",
							  @".\Resources\16\FindMenu.gif",
							  false,
							  false,
							  true,
							  false,
							  true,
							  false,
							  true);
					ToolbarInfo appUpdateInfo 
						= new ToolbarInfo(ToolbarKeys.AppUpdate,
							  "업데이트",
							  Shortcut.CtrlF10,
							  mSettings["MenuAppUpdateTooltip"],
							  4,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\AppUpdate.gif",
							  @".\Resources\16\AppUpdate.gif",
							  true,
							  false,
							  true,
							  true,
							  true,
							  false,
							  true);
					ToolbarInfo logoutInfo 
						= new ToolbarInfo(ToolbarKeys.Logout,
							  "로그 아웃",
							  Shortcut.CtrlF11,
							  mSettings["MenuLogoutTooltip"],
							  5,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Logout.gif",
							  @".\Resources\16\Logout.gif",
							  true,
							  false,
							  true,
							  true,
							  true,
							  false,
							  true);
					ToolbarInfo closeInfo 
						= new ToolbarInfo(ToolbarKeys.Close,
							  "종료",
							  Shortcut.CtrlF12,
							  mSettings["MenuCloseTooltip"],
							  6,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Close.gif",
							  @".\Resources\16\Close.gif",
							  false,
							  false,
							  true,
							  true,
							  true,
							  false,
							  true);
					ToolbarInfo helpInfo 
						= new ToolbarInfo(ToolbarKeys.Help,
							  "도움말",
							  Shortcut.F1,
							  mSettings["MenuHelpTooltip"],
							  7,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Help.gif",
							  @".\Resources\16\Help.gif",
							  true,
							  false,
							  true,
							  true,
							  true,
							  false,
							  false);
					ToolbarInfo newInfo 
						= new ToolbarInfo(ToolbarKeys.New,
							  "신규",
							  Shortcut.F2,
							  mSettings["MenuNewTooltip"],
							  10,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\New.gif",
							  @".\Resources\16\New.gif",
							  false,
							  false,
							  false,
							  false,
							  false,
							  true,
							  true);
					ToolbarInfo searchInfo 
						= new ToolbarInfo(ToolbarKeys.Search,
							  "조회",
							  Shortcut.F3,
							  mSettings["MenuSearchTooltip"],
							  11,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Search.gif",
							  @".\Resources\16\Search.gif",
							  true,
							  false,
							  false,
							  false,
							  false,
							  true,
							  true);
					ToolbarInfo runInfo 
						= new ToolbarInfo(ToolbarKeys.Run,
							  "실행",
							  Shortcut.F4,
							  mSettings["MenuRunTooltip"],
							  12,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Run.gif",
							  @".\Resources\16\Run.gif",
							  false,
							  false,
							  false,
							  false,
							  false,
							  true,
							  true);
					ToolbarInfo saveInfo 
						= new ToolbarInfo(ToolbarKeys.Save,
							  "저장",
							  Shortcut.None,
							  mSettings["MenuSaveTooltip"],
							  13,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Save.gif",
							  @".\Resources\16\Save.gif",
							  false,
							  false,
							  false,
							  false,
							  false,
							  false,
							  true);
					ToolbarInfo cancelInfo 
						= new ToolbarInfo(ToolbarKeys.Cancel,
							  "취소",
							  Shortcut.F5,
							  mSettings["MenuCancelTooltip"],
							  14,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Cancel.gif",
							  @".\Resources\16\Cancel.gif",
							  true,
							  false,
							  false,
							  false,
							  false,
							  false,
							  true);
					ToolbarInfo deleteInfo 
						= new ToolbarInfo(ToolbarKeys.Delete,
							  "삭제",
							  Shortcut.F6,
							  mSettings["MenuDeleteTooltip"],
							  15,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Delete.gif",
							  @".\Resources\16\Delete.gif",
							  false,
							  false,
							  false,
							  false,
							  false,
							  false,
							  true);
					ToolbarInfo addRowInfo 
						= new ToolbarInfo(ToolbarKeys.AddRow,
							  "행 추가",
							  Shortcut.F7,
							  mSettings["MenuAddRowTooltip"],
							  16,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\AddRow.gif",
							  @".\Resources\16\AddRow.gif",
							  true,
							  false,
							  false,
							  false,
							  false,
							  true,
							  true);
					ToolbarInfo delRowInfo 
						= new ToolbarInfo(ToolbarKeys.DelRow,
							  "행 삭제",
							  Shortcut.F8,
							  mSettings["MenuDelRowTooltip"],
							  17,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\DelRow.gif",
							  @".\Resources\16\DelRow.gif",
							  false,
							  false,
							  false,
							  false,
							  false,
							  true,
							  true);
					ToolbarInfo excelInfo 
						= new ToolbarInfo(ToolbarKeys.Excel,
							  "엑셀",
							  Shortcut.F9,
							  mSettings["MenuExcelTooltip"],
							  18,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Excel.gif",
							  @".\Resources\16\Excel.gif",
							  true,
							  false,
							  false,
							  false,
							  false,
							  true,
							  true);
					ToolbarInfo previewInfo 
						= new ToolbarInfo(ToolbarKeys.Preview,
							  "미리 보기",
							  Shortcut.F10,
							  mSettings["MenuPreviewTooltip"],
							  19,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Preview.gif",
							  @".\Resources\16\Preview.gif",
							  false,
							  false,
							  false,
							  false,
							  false,
							  false,
							  false);
					ToolbarInfo printInfo 
						= new ToolbarInfo(ToolbarKeys.Print,
							  "출력",
							  Shortcut.F11,
							  mSettings["MenuPrintTooltip"],
							  20,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\Print.gif",
							  @".\Resources\16\Print.gif",
							  false,
							  false,
							  false,
							  false,
							  false,
							  false,
							  false);
					ToolbarInfo formExitInfo 
						= new ToolbarInfo(ToolbarKeys.FormExit,
							  "창닫기",
							  Shortcut.F12,
							  mSettings["MenuExitTooltip"],
							  21,
							  ToolStripItemDisplayStyle.Image,
							  @".\Resources\24\FormExit.gif",
							  @".\Resources\16\FormExit.gif",
							  true,
							  false,
							  false,
							  false,
							  false,
							  true,
							  true);
					ToolbarInfo menuPathInfo 
						= new ToolbarInfo(ToolbarKeys.MenuPath,
							  "",
							  Shortcut.None,
							  "",
							  22,
							  ToolStripItemDisplayStyle.Text,
							  string.Empty,
							  string.Empty,
							  false,
							  false,
							  false,
							  false,
							  false,
							  false,
							  true);

					toolbars.Add(logoInfo);
					toolbars.Add(favoritesInfo);
					toolbars.Add(menuNameInfo);
					toolbars.Add(findMenuInfo);
					toolbars.Add(appUpdateInfo);
					toolbars.Add(logoutInfo);
					toolbars.Add(closeInfo);
					toolbars.Add(helpInfo);
					toolbars.Add(newInfo);
					toolbars.Add(saveInfo);
					toolbars.Add(searchInfo);
					toolbars.Add(runInfo);
					toolbars.Add(cancelInfo);
					toolbars.Add(deleteInfo);
					toolbars.Add(addRowInfo);
					toolbars.Add(delRowInfo);
					toolbars.Add(excelInfo);
					toolbars.Add(previewInfo);
					toolbars.Add(printInfo);
					toolbars.Add(formExitInfo);
					toolbars.Add(menuPathInfo);
				}
				#endregion
			}

			toolbars.RemoveAll(delegate(ToolbarInfo ti)
			{
				return (ti.Show == false);                	
			});

			// Toolbar중에서 필요한 부분을 filtering 한다.
			toolbars.ForEach(delegate(ToolbarInfo ti)
			{
				if (ti.Show == true)
				{
					if (ti.ShowInMainToolbar == true)
						mainToolbars.Add(ti);
					else
						standardToolbars.Add(ti);
				}
			});

			// Control 순서를 정렬한다.
			toolbars.Sort(SortByOrder);
			mainToolbars.Sort(SortByOrder);
			standardToolbars.Sort(SortByOrder);

			if (mainToolbars.Count > 0)
				ExistMainToolbar = true;

			if (standardToolbars.Count > 0)
				ExistStandardToolbar = true;
			
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();

			InitToolbarAppearance();
			InitDockableTreeForm();

			// 버튼 생성
			if (ExistMainToolbar == true)
			{
				_mainToolbar = ToolbarControlFactory.CreateMainToolbar(MAINMENU_KEY, MainToolbarCategoryName);
				this.Toolbars.Add(_mainToolbar);
			}

			if (ExistStandardToolbar == true)
			{
				_standardToolbar = ToolbarControlFactory.CreateStandardToolbar(STANDARD_KEY, StandardToolbarCategoryName);
				this.Toolbars.Add(_standardToolbar);
			}

			// 모든 도구 
			this.Tools.AddRange(GetToolButtons(mainToolbars, MainToolbarCategoryName).ToArray());
			this.Tools.AddRange(GetToolButtons(standardToolbars, StandardToolbarCategoryName).ToArray());

			// 메인 도구
			_mainToolbar.NonInheritedTools.AddRange(GetToolButtons(mainToolbars, MainToolbarCategoryName).ToArray());

			// 표준 도구
			_standardToolbar.NonInheritedTools.AddRange(GetToolButtons(standardToolbars, StandardToolbarCategoryName).ToArray());

			#region Ribbon Menu
			List<ToolBase> ribbonFooter = new List<ToolBase>();
			List<ToolBase> ribbonMenu = new List<ToolBase>();
			List<ToolBase> ribbonQuickAccess = new List<ToolBase>();

			toolbars.ForEach(delegate(ToolbarInfo ti)
			{
				// Ribbon Footer bar
				// Help

				// Ribbon Menu
				// AppUpdate
				// Logout
				// Close

				// QuickAccess
				// Logo
				// Favorites
				// MenuName
				// FindMenu
				// AppUpdate
				// Logout
				// Help

				if (ti.Key == ToolbarKeys.Help)
					ribbonFooter.Add(ToolbarControlFactory.CreateButtons(ti, MainToolbarCategoryName));
				else
				{
					if (ti.ShowInRibbonAppMenu == true)
						ribbonMenu.Add(ToolbarControlFactory.CreateButtons(ti, MainToolbarCategoryName));

					if (ti.ShowInRibbonQuickAccess == true)
					{
						if (ti.Key == ToolbarKeys.Logo)
							ribbonQuickAccess.Add(ToolbarControlFactory.CreateProgressBarTool(ti, MainToolbarCategoryName));
						else if (ti.Key == ToolbarKeys.MenuName)
							ribbonQuickAccess.Add(ToolbarControlFactory.CreateTextBoxes(ti, MainToolbarCategoryName));
						else
							ribbonQuickAccess.Add(ToolbarControlFactory.CreateButtons(ti, MainToolbarCategoryName));
					}
				}
			});

			if (ribbonFooter.Count > 0)
				this.Ribbon.ApplicationMenu.FooterToolbar.NonInheritedTools.AddRange(ribbonFooter.ToArray());

			if (ribbonMenu.Count > 0)
				this.Ribbon.ApplicationMenu.ToolAreaLeft.NonInheritedTools.AddRange(ribbonMenu.ToArray());

			if (ribbonQuickAccess.Count > 0)
				this.Ribbon.QuickAccessToolbar.NonInheritedTools.AddRange(ribbonQuickAccess.ToArray());
			#endregion
			
			ShowRibbon = ConfigWrapper.Loader.ShowRibbon;

			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}

		private void InitToolbarAppearance()
		{
			this.AlwaysShowMenusExpanded = Infragistics.Win.DefaultableBoolean.True;
			this.DesignerFlags = 1;
			this.DockWithinContainer = _container;
			this.DockWithinContainerBaseType = typeof(Form);
			this.ImageSizeLarge = new Size(24, 24);
			this.ImageSizeSmall = new Size(16, 16);
			this.MiniToolbar.ToolRowCount = 1;
			this.ShowFullMenusDelay = 0;
			this.ShowMenuShadows = Infragistics.Win.DefaultableBoolean.True;
			this.ShowQuickCustomizeButton = false;
			this.ShowShortcutsInToolTips = true;
			this.Office2007UICompatibility = false;
			this.UseLargeImagesOnMenu = true;
			this.UseLargeImagesOnToolbar = true;

			Color apperearanceColor = Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(214)))), ((int)(((byte)(216)))));
			Infragistics.Win.Appearance RibbonCaptionAreaAppearance = new Infragistics.Win.Appearance();
			RibbonCaptionAreaAppearance.BackColor = apperearanceColor;
			RibbonCaptionAreaAppearance.BackColor2 = Color.White;
			this.Ribbon.CaptionAreaAppearance = RibbonCaptionAreaAppearance;
			this.Ribbon.GroupBorderStyle = Infragistics.Win.UIElementBorderStyle.None;

			Infragistics.Win.Appearance quickAccessToolbarAppearance = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance captionAreaAppearance = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance ribbonAreaAppearance = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance tabAreaAppearance = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance tabItemToolbar = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance tabSettings = new Infragistics.Win.Appearance();

			quickAccessToolbarAppearance.BackColor = apperearanceColor;
			quickAccessToolbarAppearance.BackColor2 = Color.White;
			captionAreaAppearance.BackColor = apperearanceColor;
			captionAreaAppearance.BackColor2 = Color.White;
			ribbonAreaAppearance.BackColor = apperearanceColor;
			ribbonAreaAppearance.BackColor2 = Color.White;
			tabAreaAppearance.BackColor = apperearanceColor;
			tabAreaAppearance.BackColor2 = Color.White;
			tabItemToolbar.BackColor = apperearanceColor;
			tabItemToolbar.BackColor2 = Color.White;
			tabSettings.BackColor = apperearanceColor;
			tabSettings.BackColor2 = Color.White;
			this.Ribbon.QuickAccessToolbar.Settings.Appearance = quickAccessToolbarAppearance;
			this.Ribbon.CaptionAreaAppearance = captionAreaAppearance;
			this.Ribbon.RibbonAreaAppearance = ribbonAreaAppearance;
			this.Ribbon.TabAreaAppearance = tabAreaAppearance;
			this.Ribbon.TabItemToolbar.Settings.Appearance = tabItemToolbar;
			this.Ribbon.TabSettings.Appearance = tabSettings;
		}

		private void InitDockableTreeForm()
		{
			_DockableTreeForm_Toolbars_Dock_Area_Left = new UltraToolbarsDockArea();
			_DockableTreeForm_Toolbars_Dock_Area_Right = new UltraToolbarsDockArea();
			_DockableTreeForm_Toolbars_Dock_Area_Top = new UltraToolbarsDockArea();
			_DockableTreeForm_Toolbars_Dock_Area_Bottom = new UltraToolbarsDockArea();

			// 
			// _DockableTreeForm_Toolbars_Dock_Area_Top
			// 
			_DockableTreeForm_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			_DockableTreeForm_Toolbars_Dock_Area_Top.BackColor = Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			_DockableTreeForm_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
			_DockableTreeForm_Toolbars_Dock_Area_Top.ForeColor = SystemColors.ControlText;
			_DockableTreeForm_Toolbars_Dock_Area_Top.Location = new Point(0, 0);
			_DockableTreeForm_Toolbars_Dock_Area_Top.Name = "_DockableTreeForm_Toolbars_Dock_Area_Top";
			_DockableTreeForm_Toolbars_Dock_Area_Top.Size = new Size(1013, 119);
			_DockableTreeForm_Toolbars_Dock_Area_Top.ToolbarsManager = this;
			// 
			// _DockableTreeForm_Toolbars_Dock_Area_Bottom
			// 
			_DockableTreeForm_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			_DockableTreeForm_Toolbars_Dock_Area_Bottom.BackColor = Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			_DockableTreeForm_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
			_DockableTreeForm_Toolbars_Dock_Area_Bottom.ForeColor = SystemColors.ControlText;
			_DockableTreeForm_Toolbars_Dock_Area_Bottom.InitialResizeAreaExtent = 4;
			_DockableTreeForm_Toolbars_Dock_Area_Bottom.Location = new Point(0, 721);
			_DockableTreeForm_Toolbars_Dock_Area_Bottom.Name = "_DockableTreeForm_Toolbars_Dock_Area_Bottom";
			_DockableTreeForm_Toolbars_Dock_Area_Bottom.Size = new Size(1013, 4);
			_DockableTreeForm_Toolbars_Dock_Area_Bottom.ToolbarsManager = this;
			// 
			// _DockableTreeForm_Toolbars_Dock_Area_Left
			// 
			_DockableTreeForm_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			_DockableTreeForm_Toolbars_Dock_Area_Left.BackColor = Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			_DockableTreeForm_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
			_DockableTreeForm_Toolbars_Dock_Area_Left.ForeColor = SystemColors.ControlText;
			_DockableTreeForm_Toolbars_Dock_Area_Left.InitialResizeAreaExtent = 4;
			_DockableTreeForm_Toolbars_Dock_Area_Left.Location = new Point(0, 119);
			_DockableTreeForm_Toolbars_Dock_Area_Left.Name = "_DockableTreeForm_Toolbars_Dock_Area_Left";
			_DockableTreeForm_Toolbars_Dock_Area_Left.Size = new Size(4, 602);
			_DockableTreeForm_Toolbars_Dock_Area_Left.ToolbarsManager = this;
			// 
			// _DockableTreeForm_Toolbars_Dock_Area_Right
			// 
			_DockableTreeForm_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			_DockableTreeForm_Toolbars_Dock_Area_Right.BackColor = Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			_DockableTreeForm_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
			_DockableTreeForm_Toolbars_Dock_Area_Right.ForeColor = SystemColors.ControlText;
			_DockableTreeForm_Toolbars_Dock_Area_Right.InitialResizeAreaExtent = 4;
			_DockableTreeForm_Toolbars_Dock_Area_Right.Location = new Point(1009, 119);
			_DockableTreeForm_Toolbars_Dock_Area_Right.Name = "_DockableTreeForm_Toolbars_Dock_Area_Right";
			_DockableTreeForm_Toolbars_Dock_Area_Right.Size = new Size(4, 602);
			_DockableTreeForm_Toolbars_Dock_Area_Right.ToolbarsManager = this;

			_container.Controls.Add(_DockableTreeForm_Toolbars_Dock_Area_Left);
			_container.Controls.Add(_DockableTreeForm_Toolbars_Dock_Area_Right);
			_container.Controls.Add(_DockableTreeForm_Toolbars_Dock_Area_Top);
			_container.Controls.Add(_DockableTreeForm_Toolbars_Dock_Area_Bottom);
		}

		private int SortByOrder(ToolbarInfo t1, ToolbarInfo t2)
		{
			int result = 0;

			if (t1.Order > t2.Order)
				result = 1;
			else if (t1.Order < t2.Order)
				result = -1;
			else if (t1.Order < t2.Order)
				result = 0;

			return result;
		}

		private List<ToolBase> GetToolButtons(List<ToolbarInfo> toolbars, string categoryName)
		{		
			List<ToolBase> toolButtons = new List<ToolBase>(toolbars.Count);

			toolbars.ForEach(delegate(ToolbarInfo ti)
			{
				if (ti.Show == true)
				{
					ToolbarKeys key = ti.Key;

					switch (key)
					{
						case ToolbarKeys.Logo:
							toolButtons.Add(ToolbarControlFactory.CreateProgressBarTool(ti, categoryName));

							break;
						case ToolbarKeys.MenuName:
							toolButtons.Add(ToolbarControlFactory.CreateTextBoxes(ti, categoryName));

							break;
						case ToolbarKeys.MenuPath:
						case ToolbarKeys.Space01:
						case ToolbarKeys.Space02:
							toolButtons.Add(ToolbarControlFactory.CreateLabels(ti, categoryName));

							break;
						default:
							toolButtons.Add(ToolbarControlFactory.CreateButtons(ti, categoryName));

							break;
					}
				}
			});

			return toolButtons;
		}
		#endregion

		#region Properties
		/// <summary>
		/// MainToolbarAction를 구하거나 설정합니다.
		/// </summary>
		public IMainToolbarAction MainToolbarAction
		{
			get
			{
				return _mainToolbarAction;
			}
			set
			{
				_mainToolbarAction = value;
			}
		}

        /// <summary>
        /// StandardToolbarAction를 구하거나 설정합니다.
        /// </summary>
		public IStandardToolbarAction StandardToolbarAction
        {
        	get
        	{
        		return _standardToolbarAction;
        	}
        	set
        	{
        		_standardToolbarAction = value;
        	}
        }        
        
		/// <summary>
		/// 
		/// </summary>
		public bool ExistMainToolbar
		{
			get
			{
				return _existMainToolbar;
			}
			set
			{
				_existMainToolbar = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool ExistStandardToolbar
		{
			get
			{
				return _existStandardToolbar;
			}
			set
			{
				_existStandardToolbar = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool MainToolbarVisible
		{
			get
			{
				return (ExistMainToolbar == true && _mainToolbar != null && _mainToolbar.Visible == true);
			}
			set
			{
				if (ExistMainToolbar == true && _mainToolbar != null)
					_mainToolbar.Visible = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool StandardToolbarVisible
		{
			get
			{
				return (ExistStandardToolbar == true && _standardToolbar != null && _standardToolbar.Visible == true);
			}
			set
			{
				if (ExistStandardToolbar == true && _standardToolbar != null)
					_standardToolbar.Visible = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool ShowRibbon
		{
			get
			{
				return this.Ribbon.Visible;
			}
			set
			{
				if (ExistMainToolbar == true)
				{
					if (_mainToolbar != null)
					{
						MainToolbarVisible = !value;
						_mainToolbar.DockedRow = 0;

						if (ExistStandardToolbar == true && _standardToolbar != null)
						{
							// 기본 StandardToolbar는 Visible = false 상태로 시작한다.
							StandardToolbarVisible = false;
							_standardToolbar.DockedRow = 1;
						}
					}

				}

				this.Ribbon.Visible = value;
			}
		}
		#endregion        

		#region Toolbar
		private void OnToolClick(object sender, ToolClickEventArgs e)
		{
			UltraToolbarsManager xToolbar = sender as UltraToolbarsManager;

			if (Enum.IsDefined(typeof(ToolbarKeys), e.Tool.Key) == true)
			{
				switch ((ToolbarKeys)Enum.Parse(typeof(ToolbarKeys), e.Tool.Key))
				{
					case ToolbarKeys.Favorites:
						MainToolbarAction.OnFavoritesClickAction();

						break;
					case ToolbarKeys.FindMenu:
						FindMenuClick();

						break;
					case ToolbarKeys.AppUpdate:
						MainToolbarAction.OnAppUpdateClickAction();

						break;
					case ToolbarKeys.Logout:
						MainToolbarAction.OnLogoutClickAction();

						break;
					case ToolbarKeys.Close:
						MainToolbarAction.OnCloseClickAction();

						break;
					case ToolbarKeys.Help:
						MainToolbarAction.OnHelpClickAction();

						break;
				}
			}

			// TEST: ActiveTab의 여부에 따라 작동하는지
			// by KIMKIWON\xyz37 in 2008년 11월 13일 목요일 오후 6:21
			// ActiveTab != null && 
			if (Enum.IsDefined(typeof(ToolbarKeys), e.Tool.Key) == true)
			{
				switch ((ToolbarKeys)Enum.Parse(typeof(ToolbarKeys), e.Tool.Key))
				{
					case ToolbarKeys.New:
						StandardToolbarAction.OnNewClickAction();

						break;
					case ToolbarKeys.Save:
						StandardToolbarAction.OnSaveClickAction();

						break;
					case ToolbarKeys.Search:
						StandardToolbarAction.OnSearchClickAction();

						break;
					case ToolbarKeys.Run:
						StandardToolbarAction.OnRunClickAction();

						break;
					case ToolbarKeys.Cancel:
						StandardToolbarAction.OnCancelClickAction();

						break;
					case ToolbarKeys.Delete:
						StandardToolbarAction.OnDeleteClickAction();

						break;
					case ToolbarKeys.AddRow:
						StandardToolbarAction.OnAddRowClickAction();

						break;
					case ToolbarKeys.DelRow:
						StandardToolbarAction.OnDelRowClickAction();

						break;
					case ToolbarKeys.Excel:
						StandardToolbarAction.OnExcelClickAction();

						break;
					case ToolbarKeys.Preview:
						StandardToolbarAction.OnPreviewClickAction();

						break;
					case ToolbarKeys.Print:
						StandardToolbarAction.OnPrintClickAction();

						break;
					case ToolbarKeys.FormExit:
						StandardToolbarAction.OnExitClickAction();

						break;
				}
			}
		}

		private void OnToolKeyDown(object sender, ToolKeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter 
				&& e.Tool.Key == ToolbarKeys.MenuName.ToString())
				FindMenuClick();
		}

		private void FindMenuClick()
		{
			UltraToolbar xToolbarMain = base.Toolbars[MAINMENU_KEY];
			TextBoxTool txtMenuName = xToolbarMain.Tools[ToolbarKeys.MenuName.ToString()] as TextBoxTool;

			if (txtMenuName != null && txtMenuName.Text != string.Empty)
				MainToolbarAction.OnFindMenuClickAction(txtMenuName.Text);
		}

		/// <summary>
		/// 표준 도구 바를 보여줍니다.
		/// </summary>
		public void ShowStandardToolbar()
		{
			StandardToolbarVisible = true;
		}

		/// <summary>
		/// 표준 도구 바를 감춥니다.
		/// </summary>
		public void HideStandardToolbar()
		{
			StandardToolbarVisible = false;
		}
		#endregion

		#region Set StandardToolbarProperty 
		/// <summary>
		/// MenuPath를 구하거나 설정합니다.
		/// </summary>
		public string MenuPath
		{
			get
			{
				string menuPath = string.Empty;

				if (base.Tools.Exists(ToolbarKeys.MenuPath.ToString()) == false)
					return menuPath;
				
				LabelTool lblMenuPath = base.Tools[ToolbarKeys.MenuPath.ToString()] as LabelTool;

				if (lblMenuPath != null)
					menuPath = lblMenuPath.SharedProps.Caption;

				return menuPath;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.MenuPath.ToString()) == false)
					return;
				
				LabelTool lblMenuPath = base.Tools[ToolbarKeys.MenuPath.ToString()] as LabelTool;

				if (lblMenuPath != null)
					lblMenuPath.SharedProps.Caption = value;
			}
		}

		#region Enable Toolbar Buttons
		/// <summary>
		/// New Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnableNewButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.New.ToString()) == false)
					return false;

				ButtonTool btnNew = base.Tools[ToolbarKeys.New.ToString()] as ButtonTool;
				bool enableNewButton = false;

				if (btnNew != null)
					enableNewButton = btnNew.SharedProps.Enabled;

				return enableNewButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.New.ToString()) == false)
					return;

				ButtonTool btnNew = base.Tools[ToolbarKeys.New.ToString()] as ButtonTool;

				if (btnNew != null)
					btnNew.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Save Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnableSaveButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.Save.ToString()) == false)
					return false;

				ButtonTool btnSave = base.Tools[ToolbarKeys.Save.ToString()] as ButtonTool;
				bool enableSaveButton = false;

				if (btnSave != null)
					enableSaveButton = btnSave.SharedProps.Enabled;

				return enableSaveButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.Save.ToString()) == false)
					return;

				ButtonTool btnSave = base.Tools[ToolbarKeys.Save.ToString()] as ButtonTool;

				if (btnSave != null)
					btnSave.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Search Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnableSearchButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.Search.ToString()) == false)
					return false;

				ButtonTool btnSearch = base.Tools[ToolbarKeys.Search.ToString()] as ButtonTool;
				bool enableSearchButton = false;

				if (btnSearch != null)
					enableSearchButton = btnSearch.SharedProps.Enabled;

				return enableSearchButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.Search.ToString()) == false)
					return;

				ButtonTool btnSearch = base.Tools[ToolbarKeys.Search.ToString()] as ButtonTool;

				if (btnSearch != null)
					btnSearch.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Run Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnableRunButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.Run.ToString()) == false)
					return false;

				ButtonTool btnRun = base.Tools[ToolbarKeys.Run.ToString()] as ButtonTool;
				bool enableRunButton = false;

				if (btnRun != null)
					enableRunButton = btnRun.SharedProps.Enabled;

				return enableRunButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.Run.ToString()) == false)
					return;

				ButtonTool btnRun = base.Tools[ToolbarKeys.Run.ToString()] as ButtonTool;

				if (btnRun != null)
					btnRun.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Cancel Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnableCancelButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.Cancel.ToString()) == false)
					return false;

				ButtonTool btnCancel = base.Tools[ToolbarKeys.Cancel.ToString()] as ButtonTool;
				bool enableCancelButton = false;

				if (btnCancel != null)
					enableCancelButton = btnCancel.SharedProps.Enabled;

				return enableCancelButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.Cancel.ToString()) == false)
					return;

				ButtonTool btnCancel = base.Tools[ToolbarKeys.Cancel.ToString()] as ButtonTool;

				if (btnCancel != null)
					btnCancel.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Delete Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnableDeleteButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.Delete.ToString()) == false)
					return false;

				ButtonTool btnDelete = base.Tools[ToolbarKeys.Delete.ToString()] as ButtonTool;
				bool enableDeleteButton = false;

				if (btnDelete != null)
					enableDeleteButton = btnDelete.SharedProps.Enabled;

				return enableDeleteButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.Delete.ToString()) == false)
					return;

				ButtonTool btnDelete = base.Tools[ToolbarKeys.Delete.ToString()] as ButtonTool;

				if (btnDelete != null)
					btnDelete.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// AddRow Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnableAddRowButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.AddRow.ToString()) == false)
					return false;

				ButtonTool btnAddRow = base.Tools[ToolbarKeys.AddRow.ToString()] as ButtonTool;
				bool enableAddRowButton = false;

				if (btnAddRow != null)
					enableAddRowButton = btnAddRow.SharedProps.Enabled;

				return enableAddRowButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.AddRow.ToString()) == false)
					return;

				ButtonTool btnAddRow = base.Tools[ToolbarKeys.AddRow.ToString()] as ButtonTool;

				if (btnAddRow != null)
					btnAddRow.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// DelRow Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnableDelRowButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.DelRow.ToString()) == false)
					return false;

				ButtonTool btnDelRow = base.Tools[ToolbarKeys.DelRow.ToString()] as ButtonTool;
				bool enableDelRowButton = false;

				if (btnDelRow != null)
					enableDelRowButton = btnDelRow.SharedProps.Enabled;

				return enableDelRowButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.DelRow.ToString()) == false)
					return;

				ButtonTool btnDelRow = base.Tools[ToolbarKeys.DelRow.ToString()] as ButtonTool;

				if (btnDelRow != null)
					btnDelRow.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Excel Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnableExcelButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.Excel.ToString()) == false)
					return false;

				ButtonTool btnExcel = base.Tools[ToolbarKeys.Excel.ToString()] as ButtonTool;
				bool enableExcelButton = false;

				if (btnExcel != null)
					enableExcelButton = btnExcel.SharedProps.Enabled;

				return enableExcelButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.Excel.ToString()) == false)
					return;

				ButtonTool btnExcel = base.Tools[ToolbarKeys.Excel.ToString()] as ButtonTool;

				if (btnExcel != null)
					btnExcel.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Preview Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnablePreviewButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.Preview.ToString()) == false)
					return false;

				ButtonTool btnPreview = base.Tools[ToolbarKeys.Preview.ToString()] as ButtonTool;
				bool enablePreviewButton = false;

				if (btnPreview != null)
					enablePreviewButton = btnPreview.SharedProps.Enabled;

				return enablePreviewButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.Preview.ToString()) == false)
					return;

				ButtonTool btnPreview = base.Tools[ToolbarKeys.Preview.ToString()] as ButtonTool;

				if (btnPreview != null)
					btnPreview.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Print Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnablePrintButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.Print.ToString()) == false)
					return false;

				ButtonTool btnPrint = base.Tools[ToolbarKeys.Print.ToString()] as ButtonTool;
				bool enablePrintButton = false;

				if (btnPrint != null)
					enablePrintButton = btnPrint.SharedProps.Enabled;

				return enablePrintButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.Print.ToString()) == false)
					return;

				ButtonTool btnPrint = base.Tools[ToolbarKeys.Print.ToString()] as ButtonTool;

				if (btnPrint != null)
					btnPrint.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Exit Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		public bool EnableExitButton
		{
			get
			{
				if (base.Tools.Exists(ToolbarKeys.FormExit.ToString()) == false)
					return false;

				ButtonTool btnExit = base.Tools[ToolbarKeys.FormExit.ToString()] as ButtonTool;
				bool enableExitButton = false;

				if (btnExit != null)
					enableExitButton = btnExit.SharedProps.Enabled;

				return enableExitButton;
			}
			set
			{
				if (base.Tools.Exists(ToolbarKeys.FormExit.ToString()) == false)
					return;

				ButtonTool btnExit = base.Tools[ToolbarKeys.FormExit.ToString()] as ButtonTool;

				if (btnExit != null)
					btnExit.SharedProps.Enabled = value;
			}
		}
		#endregion
		#endregion
	}
}
