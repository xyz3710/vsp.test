﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.ToolbarManager.ToolbarControlFactory
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 11월 14일 금요일 오후 10:08
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using Infragistics.Win.UltraWinToolbars;
using iDASiT.FX.Win.Configuration.Handler;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Infragistics.Win;
using iDASiT.FX.Win.Configuration;

namespace iDASiT.FX.Win.Loader
{
	internal class ToolbarControlFactory
	{
		private static Color _controlBackColor = Color.White;
		
		#region Create Controls
        public static ButtonTool CreateButtons(ToolbarInfo ti, string categoryName)
		{
			string key = ti.Key.ToString();
			ButtonTool buttonTool = new ButtonTool(key);

			SetDefaultProperty(buttonTool, ti, categoryName);

			return buttonTool;
		}

        public static TextBoxTool CreateTextBoxes(ToolbarInfo ti, string categoryName)
		{
			string key = ti.Key.ToString();
			TextBoxTool textBoxTool = new TextBoxTool(key);
			
			SetDefaultProperty(textBoxTool, ti, categoryName);

			textBoxTool.EditAppearance.BackColor = _controlBackColor;

			return textBoxTool;
		}

		public static ProgressBarTool CreateProgressBarTool(ToolbarInfo ti, string categoryName)
		{
			string key = ti.Key.ToString();
			ProgressBarTool progressBarTool = new ProgressBarTool(key);
			
			Infragistics.Win.Appearance largeImageAppearance = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance smallImageAppearance = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance processAppearance = new Infragistics.Win.Appearance();

			string largeImagePath = string.Format(@"{0}\{1}", FrameworkSetup.Instance.WorkingFolder, ti.LargeIconPath);
			string smallImagePath = string.Format(@"{0}\{1}", FrameworkSetup.Instance.WorkingFolder, ti.SmallIconPath);

			if (File.Exists(largeImagePath) == true)
				largeImageAppearance.ImageBackground = new Bitmap(largeImagePath);

			if (File.Exists(smallImagePath) == true)
				smallImageAppearance.ImageBackground = new Bitmap(smallImagePath);

			largeImageAppearance.ImageBackgroundStyle = ImageBackgroundStyle.Centered;
			smallImageAppearance.ImageBackgroundStyle = ImageBackgroundStyle.Centered;
			processAppearance.BackColor = Color.Transparent;

			progressBarTool.ProgressAppearance = processAppearance;
			progressBarTool.SharedProps.AppearancesLarge.AppearanceOnToolbar = largeImageAppearance;
			progressBarTool.SharedProps.AppearancesSmall.Appearance = smallImageAppearance;
			progressBarTool.SharedProps.Shortcut = ti.Shortcut;
			progressBarTool.SharedProps.Caption = string.Empty;
			progressBarTool.SharedProps.CustomizerCaption = string.Empty;
			progressBarTool.SharedProps.Category = categoryName;
			progressBarTool.SharedProps.ToolTipText = string.Empty;
			progressBarTool.SharedProps.StatusText = string.Empty;
			progressBarTool.SharedProps.DisplayStyle = ToolDisplayStyle.ImageOnlyOnToolbars;

			progressBarTool.ProgressBorderStyle = UIElementBorderStyle.None;
			progressBarTool.ProgressFormat = string.Empty;
			progressBarTool.ProgressText = string.Empty;
			progressBarTool.InstanceProps.Width = 147;
			
			return progressBarTool;
		}

        public static LabelTool CreateLabels(ToolbarInfo ti, string categoryName)
		{
			string key = ti.Key.ToString();
			LabelTool labelTool = new LabelTool(key);

			SetDefaultProperty(labelTool, ti, categoryName);

			labelTool.SharedProps.Spring = true;
			labelTool.InstanceProps.Spring = Infragistics.Win.DefaultableBoolean.True;

			return labelTool;
		}

		private static void SetDefaultProperty(ToolBase toolBase, ToolbarInfo ti, string categoryName)
		{
			if (!(toolBase is LabelTool))
			{
				Infragistics.Win.Appearance largeImageAppearance = new Infragistics.Win.Appearance();
				Infragistics.Win.Appearance smallImageAppearance = new Infragistics.Win.Appearance();

				string largeImagePath = string.Format(@"{0}\{1}", FrameworkSetup.Instance.WorkingFolder, ti.LargeIconPath);
				string smallImagePath = string.Format(@"{0}\{1}", FrameworkSetup.Instance.WorkingFolder, ti.SmallIconPath);

				if (File.Exists(largeImagePath) == true)
					largeImageAppearance.Image = new Bitmap(largeImagePath);

				if (File.Exists(smallImagePath) == true)
					smallImageAppearance.Image = new Bitmap(smallImagePath);

				toolBase.SharedProps.AppearancesLarge.Appearance = largeImageAppearance;
				toolBase.SharedProps.AppearancesSmall.Appearance = smallImageAppearance;
			}

			toolBase.SharedProps.Shortcut = ti.Shortcut;
			toolBase.SharedProps.Caption = ti.Caption;
			toolBase.SharedProps.CustomizerCaption = ti.Caption;
			toolBase.SharedProps.Category = categoryName;
			toolBase.SharedProps.ToolTipText = ti.Description;
			toolBase.SharedProps.StatusText = ti.Description;
			toolBase.SharedProps.DisplayStyle = ChangeDisplayStyle(ti.DisplayStyle);
			toolBase.InstanceProps.IsFirstInGroup = ti.IsFirstInGroup;
		}

		private static Infragistics.Win.UltraWinToolbars.ToolDisplayStyle ChangeDisplayStyle(ToolStripItemDisplayStyle displayStyle)
		{
			Infragistics.Win.UltraWinToolbars.ToolDisplayStyle toolDisplayStyle = ToolDisplayStyle.DefaultForToolType;

			switch (displayStyle)
			{
				case ToolStripItemDisplayStyle.Text:
					toolDisplayStyle = ToolDisplayStyle.TextOnlyInMenus;

					break;
				case ToolStripItemDisplayStyle.Image:
					toolDisplayStyle = ToolDisplayStyle.ImageOnlyOnToolbars;
					break;
				case ToolStripItemDisplayStyle.ImageAndText:
					toolDisplayStyle = ToolDisplayStyle.ImageAndText;

					break;
			}

			return toolDisplayStyle;
		}
		#endregion

		#region Create Toolbar
		/// <summary>
		/// 
		/// </summary>
		/// <param name="toolbarKey"></param>
		/// <param name="categoryName"></param>
		/// <returns></returns>
		public static UltraToolbar CreateMainToolbar(string toolbarKey, string categoryName)
		{
			UltraToolbar mainToolbar = new UltraToolbar(toolbarKey);
			Infragistics.Win.Appearance mainToolbarAppearance = new Infragistics.Win.Appearance();

			//mainToolbarAppearance.BackColor = _controlBackColor;
			mainToolbar.DockedColumn = 0;
			mainToolbar.DockedRow = 0;
			mainToolbar.FloatingLocation = new Point(19, 272);
			mainToolbar.FloatingSize = new Size(157, 190);
			mainToolbar.Settings.Appearance = mainToolbarAppearance;
			mainToolbar.Settings.AllowCustomize = Infragistics.Win.DefaultableBoolean.False;
			mainToolbar.Settings.AllowHiding = Infragistics.Win.DefaultableBoolean.False;
			mainToolbar.Settings.PaddingLeft = 10;
			mainToolbar.Settings.PaddingRight = 10;
			mainToolbar.Settings.ToolSpacing = 10;
			mainToolbar.Text = categoryName;
			mainToolbar.Settings.FillEntireRow = DefaultableBoolean.True;
			mainToolbar.Settings.AllowCustomize = DefaultableBoolean.False;
			mainToolbar.Settings.AllowHiding = DefaultableBoolean.False;


			return mainToolbar;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="toolbarKey"></param>
		/// <param name="categoryName"></param>
		/// <returns></returns>
		public static UltraToolbar CreateStandardToolbar(string toolbarKey, string categoryName)
		{
			UltraToolbar standardToolbar = new UltraToolbar(toolbarKey);
			standardToolbar.DockedColumn = 0;
			standardToolbar.DockedRow = 1;
			standardToolbar.FloatingSize = new Size(170, 188);

			Color standardToolbarBackColor = Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(206)))), ((int)(((byte)(213)))));
			Infragistics.Win.Appearance standardToolbarAppearance = new Infragistics.Win.Appearance();

			standardToolbar.Settings.AllowCustomize = Infragistics.Win.DefaultableBoolean.False;
			standardToolbar.Settings.AllowHiding = Infragistics.Win.DefaultableBoolean.True;
			standardToolbarAppearance.BackColor = standardToolbarBackColor;
			standardToolbarAppearance.BackColor2 = _controlBackColor;
			standardToolbarAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			standardToolbar.Settings.Appearance = standardToolbarAppearance;
			standardToolbar.Settings.PaddingLeft = 10;
			standardToolbar.Settings.PaddingRight = 10;
			standardToolbar.Settings.ToolOrientation = Infragistics.Win.UltraWinToolbars.ToolOrientation.Horizontal;
			standardToolbar.Settings.ToolSpacing = 10;
			standardToolbar.Text = categoryName;
			standardToolbar.Settings.FillEntireRow = DefaultableBoolean.True;
			standardToolbar.Settings.AllowCustomize = DefaultableBoolean.False;
			standardToolbar.Settings.AllowHiding = DefaultableBoolean.False;

			return standardToolbar;
		}
		#endregion
	}
}
