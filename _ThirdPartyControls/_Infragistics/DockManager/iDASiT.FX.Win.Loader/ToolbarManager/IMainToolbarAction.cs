﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.IMainToolbarAction
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 11월 13일 목요일 오후 6:25
/*	Purpose		:	메인 툴바 툴 버튼 KeyAction용 Interface
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;

namespace iDASiT.FX.Win
{
	/// <summary>
	/// 메인 툴바 툴 버튼 KeyAction용 Interface
	/// </summary>
	public interface IMainToolbarAction
	{
		/// <summary>
		/// OnFavoritesClickAction
		/// </summary>
		void OnFavoritesClickAction();
		/// <summary>
		/// OnFindMenuClickAction
		/// </summary>
		void OnFindMenuClickAction(string menuName);
		/// <summary>
		/// OnAppUpdateClickAction
		/// </summary>
		void OnAppUpdateClickAction();
		/// <summary>
		/// OnLogoutClickAction
		/// </summary>
		void OnLogoutClickAction();
		/// <summary>
		/// OnCloseClickAction
		/// </summary>
		void OnCloseClickAction();
		/// <summary>
		/// OnHelpClickAction
		/// </summary>
		void OnHelpClickAction();
	}
}