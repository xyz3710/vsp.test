﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.ToolbarManager
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 11월 13일 목요일 오후 6:11
/*	Purpose		:	Loader의 Toolbar에 관련된 class입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using iDASiT.FX.Win.Configuration;
using Infragistics.Win;
using Infragistics.Win.UltraWinToolbars;
using Infragistics.Win.UltraWinTree;
using Infragistics.Win.UltraWinTabbedMdi;
using iDASiT.FX.Win.Configuration.Toolbars;
using iDASiT.FX.Win.Configuration.Handler;
using System.Drawing;
using System.IO;

namespace iDASiT.FX.Win.Loader
{
	/// <summary>
	/// Loader의 Toolbar에 관련된 class입니다.
	/// </summary>
	public class ToolbarManager : UltraToolbarsManager
	{
		#region Constants
		private const string MAINMENU_KEY = "MainMenuKey";
		private const string STANDARD_KEY = "StandardKey";
		#endregion

		#region Fields
		private IMainToolbarAction _mainToolbarAction;
		private IStandardToolbarAction _standardToolbarAction;
		private IStandardToolbarProperty _standardToolbarProperty;
		#endregion

		#region Constructors
		/// <summary>
		/// LoaderToolbarManager class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public ToolbarManager()
		{
			InitToolbarBySetting();
		}

		private void InitToolbarBySetting()
		{
			List<ToolbarInfo> toolbars = new List<ToolbarInfo>();
			List<ToolbarInfo> mainToolbars = new List<ToolbarInfo>();
			List<ToolbarInfo> standardToolbars = new List<ToolbarInfo>();
			bool isMainToolbar = false;
			bool isStandardToolbar = false;
			ImageList imlMainToolbarLarge = new ImageList();
			ImageList imlMainToolbarSmall = new ImageList();
			ImageList imlStandardToolbarLarge = new ImageList();
			ImageList imlStandardToolbarSmall = new ImageList();
			int mainLargeIndex = 0;
			int mainSamllIndex = 0;
			int standardLargeIndex = 0;
			int standardSamllIndex = 0;

			imlMainToolbarLarge.ImageSize = new Size(24, 24);
			imlMainToolbarSmall.ImageSize = new Size(16, 16);
			imlStandardToolbarLarge.ImageSize = new Size(24, 24);
			imlStandardToolbarSmall.ImageSize = new Size(16, 16);

			imlMainToolbarLarge.TransparentColor = Color.Transparent;
			imlMainToolbarSmall.TransparentColor = Color.Transparent;
			imlStandardToolbarLarge.TransparentColor = Color.Transparent;
			imlStandardToolbarSmall.TransparentColor = Color.Transparent;

			#region Toolbar Collections
			using (MessageSettings mSettings = new MessageSettings())
			{
				ToolbarInfo logoInfo 
						= new ToolbarInfo(ToolbarKeys.Logo,
						  "회사 로고",
						  Shortcut.None,
						  "회사 로고",
						  0,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Logo.gif",
						  string.Empty,
						  false,
						  false,
						  true,
						  true);
				ToolbarInfo favoritesInfo 
						= new ToolbarInfo(ToolbarKeys.Favorites,
						  "즐겨 찾기",
						  Shortcut.None,
						  mSettings["MenuFavoritesTooltip"],
						  1,
						  ToolStripItemDisplayStyle.ImageAndText,
						  @".\Resources\24\Favorites.gif",
						  @".\Resources\16\Favorites.gif",
						  true,
						  false,
						  true,
						  true);
				ToolbarInfo menuNameInfo 
						= new ToolbarInfo(ToolbarKeys.MenuName,
						  "메뉴 이름",
						  Shortcut.CtrlF,
						  mSettings["MenuMenunameTooltip"],
						  2,
						  ToolStripItemDisplayStyle.Image,
						  string.Empty,
						  string.Empty,
						  false,
						  false,
						  true,
						  true);
				ToolbarInfo findMenuInfo 
						= new ToolbarInfo(ToolbarKeys.FindMenu,
						  "메뉴 검색",
						  Shortcut.None,
						  mSettings["MenuSearchTooltip"],
						  3,
						  ToolStripItemDisplayStyle.Image,
						  string.Empty,
						  string.Empty,
						  false,
						  false,
						  true,
						  true);
				ToolbarInfo appUpdateInfo 
						= new ToolbarInfo(ToolbarKeys.AppUpdate,
						  "업데이트",
						  Shortcut.CtrlF10,
						  mSettings["MenuAppUpdateTooltip"],
						  4,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\AppUpdate.gif",
						  @".\Resources\16\AppUpdate.gif",
						  true,
						  false,
						  true,
						  true);
				ToolbarInfo logoutInfo 
						= new ToolbarInfo(ToolbarKeys.Logout,
						  "로그 아웃",
						  Shortcut.CtrlF11,
						  mSettings["MenuLogoutTooltip"],
						  5,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Logout.gif",
						  @".\Resources\16\Logout.gif",
						  true,
						  false,
						  true,
						  true);
				ToolbarInfo closeInfo 
						= new ToolbarInfo(ToolbarKeys.Close,
						  "종료",
						  Shortcut.CtrlF12,
						  mSettings["MenuCloseTooltip"],
						  6,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Close.gif",
						  @".\Resources\16\Close.gif",
						  false,
						  false,
						  true,
						  true);
				ToolbarInfo helpInfo 
						= new ToolbarInfo(ToolbarKeys.Help,
						  "도움말",
						  Shortcut.F1,
						  mSettings["MenuHelpTooltip"],
						  7,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Help.gif",
						  @".\Resources\16\Help.gif",
						  true,
						  false,
						  true,
						  false);
				ToolbarInfo newInfo 
						= new ToolbarInfo(ToolbarKeys.New,
						  "신규",
						  Shortcut.F2,
						  mSettings["MenuNewTooltip"],
						  10,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\New.gif",
						  @".\Resources\16\New.gif",
						  false,
						  false,
						  false,
						  true);
				ToolbarInfo searchInfo 
						= new ToolbarInfo(ToolbarKeys.Search,
						  "조회",
						  Shortcut.F3,
						  mSettings["MenuSearchTooltip"],
						  11,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Search.gif",
						  @".\Resources\16\Search.gif",
						  true,
						  false,
						  false,
						  true);
				ToolbarInfo runInfo 
						= new ToolbarInfo(ToolbarKeys.Run,
						  "실행",
						  Shortcut.F4,
						  mSettings["MenuRunTooltip"],
						  12,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Run.gif",
						  @".\Resources\16\Run.gif",
						  false,
						  false,
						  false,
						  true);
				ToolbarInfo saveInfo 
						= new ToolbarInfo(ToolbarKeys.Save,
						  "저장",
						  Shortcut.None,
						  mSettings["MenuSaveTooltip"],
						  13,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Save.gif",
						  @".\Resources\16\Save.gif",
						  false,
						  false,
						  false,
						  true);
				ToolbarInfo cancelInfo 
						= new ToolbarInfo(ToolbarKeys.Cancel,
						  "취소",
						  Shortcut.F5,
						  mSettings["MenuCancelTooltip"],
						  14,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Cancel.gif",
						  @".\Resources\16\Cancel.gif",
						  true,
						  false,
						  false,
						  true);
				ToolbarInfo deleteInfo 
						= new ToolbarInfo(ToolbarKeys.Delete,
						  "삭제",
						  Shortcut.F6,
						  mSettings["MenuDeleteTooltip"],
						  15,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Delete.gif",
						  @".\Resources\16\Delete.gif",
						  false,
						  false,
						  false,
						  true);
				ToolbarInfo addRowInfo 
						= new ToolbarInfo(ToolbarKeys.AddRow,
						  "행 추가",
						  Shortcut.F7,
						  mSettings["MenuAddRowTooltip"],
						  16,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\AddRow.gif",
						  @".\Resources\16\AddRow.gif",
						  true,
						  false,
						  false,
						  true);
				ToolbarInfo delRowInfo 
						= new ToolbarInfo(ToolbarKeys.DelRow,
						  "행 삭제",
						  Shortcut.F8,
						  mSettings["MenuDelRowTooltip"],
						  17,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\DelRow.gif",
						  @".\Resources\16\DelRow.gif",
						  false,
						  false,
						  false,
						  true);
				ToolbarInfo excelInfo 
						= new ToolbarInfo(ToolbarKeys.Excel,
						  "엑셀",
						  Shortcut.F9,
						  mSettings["MenuExcelTooltip"],
						  18,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Excel.gif",
						  @".\Resources\16\Excel.gif",
						  true,
						  false,
						  false,
						  true);
				ToolbarInfo previewInfo 
						= new ToolbarInfo(ToolbarKeys.Preview,
						  "미리 보기",
						  Shortcut.F10,
						  mSettings["MenuPreviewTooltip"],
						  19,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Preview.gif",
						  @".\Resources\16\Preview.gif",
						  false,
						  false,
						  false,
						  false);
				ToolbarInfo printInfo 
						= new ToolbarInfo(ToolbarKeys.Print,
						  "출력",
						  Shortcut.F11,
						  mSettings["MenuPrintTooltip"],
						  20,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\Print.gif",
						  @".\Resources\16\Print.gif",
						  false,
						  false,
						  false,
						  false);
				ToolbarInfo formExitInfo 
						= new ToolbarInfo(ToolbarKeys.FormExit,
						  "창닫기",
						  Shortcut.F12,
						  mSettings["MenuExitTooltip"],
						  21,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\FormExit.gif",
						  @".\Resources\16\FormExit.gif",
						  true,
						  false,
						  false,
						  true);
				ToolbarInfo menuPathInfo 
						= new ToolbarInfo(ToolbarKeys.MenuPath,
						  "",
						  Shortcut.None,
						  "",
						  22,
						  ToolStripItemDisplayStyle.Image,
						  @".\Resources\24\MenuPath.gif",
						  @".\Resources\16\MenuPath.gif",
						  false,
						  false,
						  true,
						  true);

				toolbars.Add(favoritesInfo);
				toolbars.Add(menuNameInfo);
				toolbars.Add(findMenuInfo);
				toolbars.Add(appUpdateInfo);
				toolbars.Add(logoutInfo);
				toolbars.Add(closeInfo);
				toolbars.Add(helpInfo);
				toolbars.Add(newInfo);
				toolbars.Add(saveInfo);
				toolbars.Add(searchInfo);
				toolbars.Add(runInfo);
				toolbars.Add(cancelInfo);
				toolbars.Add(deleteInfo);
				toolbars.Add(addRowInfo);
				toolbars.Add(delRowInfo);
				toolbars.Add(excelInfo);
				toolbars.Add(previewInfo);
				toolbars.Add(printInfo);
				toolbars.Add(formExitInfo);
				toolbars.Add(menuPathInfo);
				toolbars.Add(logoInfo);
			}
			#endregion

			toolbars.ForEach(delegate(ToolbarInfo toolbarInfo)
			{
				if (toolbarInfo.ShowInMainToolbar == true)
					mainToolbars.Add(toolbarInfo);
				else
					standardToolbars.Add(toolbarInfo);
			});

			mainToolbars.Sort(SortByOrder);
			standardToolbars.Sort(SortByOrder);

			if (mainToolbars.Count > 0)
				isMainToolbar = true;

			if (standardToolbars.Count > 0)
				isStandardToolbar = true;

			mainToolbars.ForEach(delegate(ToolbarInfo toolbar)
			{
				if (File.Exists(toolbar.LargeIconPath) == true)
					imlMainToolbarLarge.Images.Add(toolbar.Key.ToString(), new Bitmap(toolbar.LargeIconPath));

				if (File.Exists(toolbar.SmallIconPath) == true)
					imlMainToolbarSmall.Images.Add(toolbar.Key.ToString(), new Bitmap(toolbar.SmallIconPath));
			});

			standardToolbars.ForEach(delegate(ToolbarInfo toolbar)
			{
				if (File.Exists(toolbar.LargeIconPath) == true)
					imlStandardToolbarLarge.Images.Add(toolbar.Key.ToString(), new Bitmap(toolbar.LargeIconPath));

				if (File.Exists(toolbar.SmallIconPath) == true)
					imlStandardToolbarSmall.Images.Add(toolbar.Key.ToString(), new Bitmap(toolbar.SmallIconPath));
			});
		}

		private int SortByOrder(ToolbarInfo t1, ToolbarInfo t2)
		{
			int result = 0;

			if (t1.Order > t2.Order)
				result = 1;
			else if (t1.Order < t2.Order)
				result = -1;
			else if (t1.Order < t2.Order)
				result = 0;

			return result;
		}
		#endregion
		
		#region Properties
		/// <summary>
		/// MainToolbarAction를 구하거나 설정합니다.
		/// </summary>
		public IMainToolbarAction MainToolbarAction
		{
			get
			{
				return _mainToolbarAction;
			}
			set
			{
				_mainToolbarAction = value;
			}
		}

        /// <summary>
        /// StandardToolbarAction를 구하거나 설정합니다.
        /// </summary>
		public IStandardToolbarAction StandardToolbarAction
        {
        	get
        	{
        		return _standardToolbarAction;
        	}
        	set
        	{
        		_standardToolbarAction = value;
        	}
        }        
        
        /// <summary>
		/// StandardToolbarProperty를 구하거나 설정합니다.
        /// </summary>
		public IStandardToolbarProperty StandardToolbarProperty
        {
        	get
        	{
        		return _standardToolbarProperty;
        	}
        	set
        	{
        		_standardToolbarProperty = value;

				_standardToolbarProperty.MenuPath = MenuPath;
				_standardToolbarProperty.EnableNewButton = EnableNewButton;
				_standardToolbarProperty.EnableSaveButton = EnableSaveButton;
				_standardToolbarProperty.EnableSearchButton = EnableSearchButton;
				_standardToolbarProperty.EnableRunButton = EnableRunButton;
				_standardToolbarProperty.EnableCancelButton = EnableCancelButton;
				_standardToolbarProperty.EnableDeleteButton = EnableDeleteButton;
				_standardToolbarProperty.EnableAddRowButton = EnableAddRowButton;
				_standardToolbarProperty.EnableDelRowButton = EnableDelRowButton;
				_standardToolbarProperty.EnableExcelButton = EnableExcelButton;
				_standardToolbarProperty.EnablePreviewButton = EnablePreviewButton;
				_standardToolbarProperty.EnablePrintButton = EnablePrintButton;
				_standardToolbarProperty.EnableExitButton = EnableExitButton;
			}
        }        
        #endregion        

		#region InitMainMenuToolbar
		private void InitMainMenuToolbar()
		{
			// 메인 툴바
			UltraToolbar xtbMainMenu = base.Toolbars[MAINMENU_KEY];

			xtbMainMenu.Settings.FillEntireRow = DefaultableBoolean.True;
			xtbMainMenu.Settings.AllowCustomize = DefaultableBoolean.False;
			xtbMainMenu.Settings.AllowHiding = DefaultableBoolean.False;

			// 버튼을 툴바에 구애 받지 않고 이동 시킬 수 있게 전체 툴바의 Tool로 제어 한다.
			using (MessageSettings mSettings = new MessageSettings())
			{
				base.Tools[ToolbarKeys.Favorites.ToString()].SharedProps.StatusText = mSettings["MenuFavoritesTooltip"];
				base.Tools[ToolbarKeys.MenuName.ToString()].SharedProps.StatusText = mSettings["MenuMenunameTooltip"];
				base.Tools[ToolbarKeys.FindMenu.ToString()].SharedProps.StatusText = mSettings["MenuSearchTooltip"];
				base.Tools[ToolbarKeys.AppUpdate.ToString()].SharedProps.StatusText = mSettings["MenuAppUpdateTooltip"];
				base.Tools[ToolbarKeys.Logout.ToString()].SharedProps.StatusText = mSettings["MenuLogoutTooltip"];
				base.Tools[ToolbarKeys.Close.ToString()].SharedProps.StatusText = mSettings["MenuCloseTooltip"];
				base.Tools[ToolbarKeys.Help.ToString()].SharedProps.StatusText = mSettings["MenuHelpTooltip"];
			}

			base.Tools[ToolbarKeys.Space01.ToString()].SharedProps.Spring = true;

			// 단축키를 할당한다.
			base.Tools[ToolbarKeys.MenuName.ToString()].SharedProps.Shortcut = Shortcut.CtrlF;
			base.Tools[ToolbarKeys.AppUpdate.ToString()].SharedProps.Shortcut = Shortcut.CtrlF10;
			base.Tools[ToolbarKeys.Logout.ToString()].SharedProps.Shortcut = Shortcut.CtrlF11;
			base.Tools[ToolbarKeys.Close.ToString()].SharedProps.Shortcut = Shortcut.CtrlF12;
			base.Tools[ToolbarKeys.Help.ToString()].SharedProps.Shortcut = Shortcut.F1;

			// NOTICE: 도움말 버튼을 없앤다.
			// by KIMKIWON\xyz37 in 2007년 9월 22일 토요일 오후 2:31
			base.Tools[ToolbarKeys.Help.ToString()].SharedProps.Visible = false;

			// 탭이 많을 경우 일부 탭이 보이도록 한다.
			// TODO: xTabbedMdiManager 사용
			// by KIMKIWON\xyz37 in 2008년 11월 13일 목요일 오후 6:18
			//xTabbedMdiManager.TabGroupSettings.ShowPartialTabs = DefaultableBoolean.True;

			base.ToolClick += new ToolClickEventHandler(base_ToolClick);
			base.ToolKeyDown += new ToolKeyEventHandler(base_ToolKeyDown);
		}
		#endregion

		#region InitStandardToolBar
		private void InitStandardToolBar()
		{
			// 표준 툴버튼
			UltraToolbar xtbStandard = base.Toolbars[STANDARD_KEY];

			xtbStandard.Settings.FillEntireRow = DefaultableBoolean.True;
			xtbStandard.Settings.AllowCustomize = DefaultableBoolean.False;
			xtbStandard.Settings.AllowHiding = DefaultableBoolean.False;

			// 버튼을 툴바에 구애 받지 않고 이동 시킬 수 있게 전체 툴바의 Tool로 제어 한다.
			using (MessageSettings mSettings = new MessageSettings())
			{
				base.Tools[ToolbarKeys.New.ToString()].SharedProps.StatusText = mSettings["MenuNewTooltip"];
				base.Tools[ToolbarKeys.Search.ToString()].SharedProps.StatusText = mSettings["MenuSearchTooltip"];
				base.Tools[ToolbarKeys.Run.ToString()].SharedProps.StatusText = mSettings["MenuRunTooltip"];
				base.Tools[ToolbarKeys.Save.ToString()].SharedProps.StatusText = mSettings["MenuSaveTooltip"];
				base.Tools[ToolbarKeys.Cancel.ToString()].SharedProps.StatusText = mSettings["MenuCancelTooltip"];
				base.Tools[ToolbarKeys.Delete.ToString()].SharedProps.StatusText = mSettings["MenuDeleteTooltip"];
				base.Tools[ToolbarKeys.AddRow.ToString()].SharedProps.StatusText = mSettings["MenuAddRowTooltip"];
				base.Tools[ToolbarKeys.DelRow.ToString()].SharedProps.StatusText = mSettings["MenuDelRowTooltip"];
				base.Tools[ToolbarKeys.Excel.ToString()].SharedProps.StatusText = mSettings["MenuExcelTooltip"];
				base.Tools[ToolbarKeys.Preview.ToString()].SharedProps.StatusText = mSettings["MenuPreviewTooltip"];
				base.Tools[ToolbarKeys.Print.ToString()].SharedProps.StatusText = mSettings["MenuPrintTooltip"];
				base.Tools[ToolbarKeys.FormExit.ToString()].SharedProps.StatusText = mSettings["MenuExitTooltip"];
			}

			// 단축키를 할당한다.
			base.Tools[ToolbarKeys.New.ToString()].SharedProps.Shortcut = Shortcut.F2;
			base.Tools[ToolbarKeys.Search.ToString()].SharedProps.Shortcut = Shortcut.F3;
			base.Tools[ToolbarKeys.Run.ToString()].SharedProps.Shortcut = Shortcut.F4;
			//base.Tools[StandardToolbarKeys.Save.ToString()].SharedProps.Shortcut = Shortcut.F5;
			base.Tools[ToolbarKeys.Cancel.ToString()].SharedProps.Shortcut = Shortcut.F5;
			base.Tools[ToolbarKeys.Delete.ToString()].SharedProps.Shortcut = Shortcut.F6;
			base.Tools[ToolbarKeys.AddRow.ToString()].SharedProps.Shortcut = Shortcut.F7;
			base.Tools[ToolbarKeys.DelRow.ToString()].SharedProps.Shortcut = Shortcut.F8;
			base.Tools[ToolbarKeys.Excel.ToString()].SharedProps.Shortcut = Shortcut.F9;
			base.Tools[ToolbarKeys.Preview.ToString()].SharedProps.Shortcut = Shortcut.F10;
			base.Tools[ToolbarKeys.Print.ToString()].SharedProps.Shortcut = Shortcut.F11;
			base.Tools[ToolbarKeys.FormExit.ToString()].SharedProps.Shortcut = Shortcut.F12;

			// MenuPath를 Spring 상태로 설정한다.
			base.Tools[ToolbarKeys.MenuPath.ToString()].SharedProps.Spring = true;

			//			base.Toolbars[0].Tools[StandardToolbarKeys.Query.ToString()].InstanceProps.IsFirstInGroup = true;
			//			base.Toolbars[0].Tools[StandardToolbarKeys.Cancel.ToString()].InstanceProps.IsFirstInGroup = true;
			//			base.Toolbars[0].Tools[StandardToolbarKeys.AddRow.ToString()].InstanceProps.IsFirstInGroup = true;
			//			base.Toolbars[0].Tools[StandardToolbarKeys.Excel.ToString()].InstanceProps.IsFirstInGroup = true;
			//			base.Toolbars[0].Tools[StandardToolbarKeys.Exit.ToString()].InstanceProps.IsFirstInGroup = true;

			xtbStandard.Visible = false;
		}
		#endregion

		#region Toolbar
		private void base_ToolClick(object sender, ToolClickEventArgs e)
		{
			UltraToolbarsManager xToolbar = sender as UltraToolbarsManager;

			if (Enum.IsDefined(typeof(ToolbarKeys), e.Tool.Key) == true)
			{
				switch ((ToolbarKeys)Enum.Parse(typeof(ToolbarKeys), e.Tool.Key))
				{
					case ToolbarKeys.Favorites:
						MainToolbarAction.OnFavoritesClickAction();

						break;
					case ToolbarKeys.FindMenu:
						FindMenuClick();

						break;
					case ToolbarKeys.AppUpdate:
						MainToolbarAction.OnAppUpdateClickAction();

						break;
					case ToolbarKeys.Logout:
						MainToolbarAction.OnLogoutClickAction();

						break;
					case ToolbarKeys.Close:
						MainToolbarAction.OnCloseClickAction();

						break;
					case ToolbarKeys.Help:
						MainToolbarAction.OnHelpClickAction();

						break;
				}
			}

			// TEST: ActiveTab의 여부에 따라 작동하는지
			// by KIMKIWON\xyz37 in 2008년 11월 13일 목요일 오후 6:21
			// ActiveTab != null && 
			if (Enum.IsDefined(typeof(ToolbarKeys), e.Tool.Key) == true)
			{
				switch ((ToolbarKeys)Enum.Parse(typeof(ToolbarKeys), e.Tool.Key))
				{
					case ToolbarKeys.New:
						StandardToolbarAction.OnNewClickAction();

						break;
					case ToolbarKeys.Save:
						StandardToolbarAction.OnSaveClickAction();

						break;
					case ToolbarKeys.Search:
						StandardToolbarAction.OnSearchClickAction();

						break;
					case ToolbarKeys.Run:
						StandardToolbarAction.OnRunClickAction();

						break;
					case ToolbarKeys.Cancel:
						StandardToolbarAction.OnCancelClickAction();

						break;
					case ToolbarKeys.Delete:
						StandardToolbarAction.OnDeleteClickAction();

						break;
					case ToolbarKeys.AddRow:
						StandardToolbarAction.OnAddRowClickAction();

						break;
					case ToolbarKeys.DelRow:
						StandardToolbarAction.OnDelRowClickAction();

						break;
					case ToolbarKeys.Excel:
						StandardToolbarAction.OnExcelClickAction();

						break;
					case ToolbarKeys.Preview:
						StandardToolbarAction.OnPreviewClickAction();

						break;
					case ToolbarKeys.Print:
						StandardToolbarAction.OnPrintClickAction();

						break;
					case ToolbarKeys.FormExit:
						StandardToolbarAction.OnExitClickAction();

						break;
				}
			}
		}

		private void base_ToolKeyDown(object sender, ToolKeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter 
				&& e.Tool.Key == ToolbarKeys.MenuName.ToString())
				FindMenuClick();
		}

		private void FindMenuClick()
		{
			UltraToolbar xToolbarMain = base.Toolbars[MAINMENU_KEY];
			TextBoxTool txtMenuName = xToolbarMain.Tools[ToolbarKeys.MenuName.ToString()] as TextBoxTool;

			if (txtMenuName != null && txtMenuName.Text != string.Empty)
				MainToolbarAction.OnFindMenuClickAction(txtMenuName.Text);
		}

		/// <summary>
		/// 표준 도구 바를 보여줍니다.
		/// </summary>
		public void ShowStandardToolbar()
		{
			// TODO: 표준 도구 바가 있을경우 작업한다.
			// by KIMKIWON\xyz37 in 2008년 11월 14일 금요일 오전 9:11
			base.Toolbars[STANDARD_KEY].Visible = true;
		}

		/// <summary>
		/// 표준 도구 바를 감춥니다.
		/// </summary>
		public void HideStandardToolbar()
		{
			base.Toolbars[STANDARD_KEY].Visible = false;
		}
		#endregion

		#region Set StandardToolbarProperty 
		private string MenuPath
		{
			get
			{
				string menuPath = string.Empty;

				LabelTool lblMenuPath = base.Tools[ToolbarKeys.MenuPath.ToString()] as LabelTool;

				if (lblMenuPath != null)
					menuPath = lblMenuPath.SharedProps.Caption;

				return menuPath;
			}
			set
			{
				LabelTool lblMenuPath = base.Tools[ToolbarKeys.MenuPath.ToString()] as LabelTool;

				if (lblMenuPath != null)
					lblMenuPath.SharedProps.Caption = value;
			}
		}

		#region Enable Toolbar Buttons
		/// <summary>
		/// New Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnableNewButton
		{
			get
			{
				ButtonTool btnNew = base.Tools[ToolbarKeys.New.ToString()] as ButtonTool;
				bool enableNewButton = false;

				if (btnNew != null)
					enableNewButton = btnNew.SharedProps.Enabled;

				return enableNewButton;
			}
			set
			{
				ButtonTool btnNew = base.Tools[ToolbarKeys.New.ToString()] as ButtonTool;

				if (btnNew != null)
					btnNew.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Save Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnableSaveButton
		{
			get
			{
				ButtonTool btnSave = base.Tools[ToolbarKeys.Save.ToString()] as ButtonTool;
				bool enableSaveButton = false;

				if (btnSave != null)
					enableSaveButton = btnSave.SharedProps.Enabled;

				return enableSaveButton;
			}
			set
			{
				ButtonTool btnSave = base.Tools[ToolbarKeys.Save.ToString()] as ButtonTool;

				if (btnSave != null)
					btnSave.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Search Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnableSearchButton
		{
			get
			{
				ButtonTool btnSearch = base.Tools[ToolbarKeys.Search.ToString()] as ButtonTool;
				bool enableSearchButton = false;

				if (btnSearch != null)
					enableSearchButton = btnSearch.SharedProps.Enabled;

				return enableSearchButton;
			}
			set
			{
				ButtonTool btnSearch = base.Tools[ToolbarKeys.Search.ToString()] as ButtonTool;

				if (btnSearch != null)
					btnSearch.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Run Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnableRunButton
		{
			get
			{
				ButtonTool btnRun = base.Tools[ToolbarKeys.Run.ToString()] as ButtonTool;
				bool enableRunButton = false;

				if (btnRun != null)
					enableRunButton = btnRun.SharedProps.Enabled;

				return enableRunButton;
			}
			set
			{
				ButtonTool btnRun = base.Tools[ToolbarKeys.Run.ToString()] as ButtonTool;

				if (btnRun != null)
					btnRun.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Cancel Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnableCancelButton
		{
			get
			{
				ButtonTool btnCancel = base.Tools[ToolbarKeys.Cancel.ToString()] as ButtonTool;
				bool enableCancelButton = false;

				if (btnCancel != null)
					enableCancelButton = btnCancel.SharedProps.Enabled;

				return enableCancelButton;
			}
			set
			{
				ButtonTool btnCancel = base.Tools[ToolbarKeys.Cancel.ToString()] as ButtonTool;

				if (btnCancel != null)
					btnCancel.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Delete Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnableDeleteButton
		{
			get
			{
				ButtonTool btnDelete = base.Tools[ToolbarKeys.Delete.ToString()] as ButtonTool;
				bool enableDeleteButton = false;

				if (btnDelete != null)
					enableDeleteButton = btnDelete.SharedProps.Enabled;

				return enableDeleteButton;
			}
			set
			{
				ButtonTool btnDelete = base.Tools[ToolbarKeys.Delete.ToString()] as ButtonTool;

				if (btnDelete != null)
					btnDelete.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// AddRow Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnableAddRowButton
		{
			get
			{
				ButtonTool btnAddRow = base.Tools[ToolbarKeys.AddRow.ToString()] as ButtonTool;
				bool enableAddRowButton = false;

				if (btnAddRow != null)
					enableAddRowButton = btnAddRow.SharedProps.Enabled;

				return enableAddRowButton;
			}
			set
			{
				ButtonTool btnAddRow = base.Tools[ToolbarKeys.AddRow.ToString()] as ButtonTool;

				if (btnAddRow != null)
					btnAddRow.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// DelRow Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnableDelRowButton
		{
			get
			{
				ButtonTool btnDelRow = base.Tools[ToolbarKeys.DelRow.ToString()] as ButtonTool;
				bool enableDelRowButton = false;

				if (btnDelRow != null)
					enableDelRowButton = btnDelRow.SharedProps.Enabled;

				return enableDelRowButton;
			}
			set
			{
				ButtonTool btnDelRow = base.Tools[ToolbarKeys.DelRow.ToString()] as ButtonTool;

				if (btnDelRow != null)
					btnDelRow.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Excel Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnableExcelButton
		{
			get
			{
				ButtonTool btnExcel = base.Tools[ToolbarKeys.Excel.ToString()] as ButtonTool;
				bool enableExcelButton = false;

				if (btnExcel != null)
					enableExcelButton = btnExcel.SharedProps.Enabled;

				return enableExcelButton;
			}
			set
			{
				ButtonTool btnExcel = base.Tools[ToolbarKeys.Excel.ToString()] as ButtonTool;

				if (btnExcel != null)
					btnExcel.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Preview Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnablePreviewButton
		{
			get
			{
				ButtonTool btnPreview = base.Tools[ToolbarKeys.Preview.ToString()] as ButtonTool;
				bool enablePreviewButton = false;

				if (btnPreview != null)
					enablePreviewButton = btnPreview.SharedProps.Enabled;

				return enablePreviewButton;
			}
			set
			{
				ButtonTool btnPreview = base.Tools[ToolbarKeys.Preview.ToString()] as ButtonTool;

				if (btnPreview != null)
					btnPreview.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Print Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnablePrintButton
		{
			get
			{
				ButtonTool btnPrint = base.Tools[ToolbarKeys.Print.ToString()] as ButtonTool;
				bool enablePrintButton = false;

				if (btnPrint != null)
					enablePrintButton = btnPrint.SharedProps.Enabled;

				return enablePrintButton;
			}
			set
			{
				ButtonTool btnPrint = base.Tools[ToolbarKeys.Print.ToString()] as ButtonTool;

				if (btnPrint != null)
					btnPrint.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Exit Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		private bool EnableExitButton
		{
			get
			{
				ButtonTool btnExit = base.Tools[ToolbarKeys.FormExit.ToString()] as ButtonTool;
				bool enableExitButton = false;

				if (btnExit != null)
					enableExitButton = btnExit.SharedProps.Enabled;

				return enableExitButton;
			}
			set
			{
				ButtonTool btnExit = base.Tools[ToolbarKeys.FormExit.ToString()] as ButtonTool;

				if (btnExit != null)
					btnExit.SharedProps.Enabled = value;
			}
		}
		#endregion
		#endregion
	}
}
