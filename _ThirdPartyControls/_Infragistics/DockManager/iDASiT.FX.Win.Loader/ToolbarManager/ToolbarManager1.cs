﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.Loader
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 11월 29일 목요일 오후 5:06
/*	Purpose		:	Main Toolbar에 관련된 partial class
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using iDASiT.FX.Win.Configuration;
using Infragistics.Win;
using Infragistics.Win.UltraWinToolbars;
using Infragistics.Win.UltraWinTree;
using Infragistics.Win.UltraWinTabbedMdi;
using iDASiT.FX.Win.Configuration.Toolbars;

namespace iDASiT.FX.Win.Loader
{
    public partial class Loader : IStandardToolbarProperty
	{
		#region Constants
		private const string MAINMENU_KEY = "MainMenuKey";
		private const string STANDARD_KEY = "StandardKey";
		#endregion
				
		#region InitMainMenuToolbar
		private void InitMainMenuToolbar()
		{
			// 메인 툴바
			UltraToolbar xtbMainMenu = xToolbarsManager.Toolbars[MAINMENU_KEY];

			xtbMainMenu.Settings.FillEntireRow = DefaultableBoolean.True;
			xtbMainMenu.Settings.AllowCustomize = DefaultableBoolean.False;
			xtbMainMenu.Settings.AllowHiding = DefaultableBoolean.False;

			// 버튼을 툴바에 구애 받지 않고 이동 시킬 수 있게 전체 툴바의 Tool로 제어 한다.
			using (MessageSettings mSettings = new MessageSettings())
			{
				xToolbarsManager.Tools[ToolbarKeys.Favorites.ToString()].SharedProps.StatusText = mSettings["MenuFavoritesTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.MenuName.ToString()].SharedProps.StatusText = mSettings["MenuMenunameTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.FindMenu.ToString()].SharedProps.StatusText = mSettings["MenuSearchTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.AppUpdate.ToString()].SharedProps.StatusText = mSettings["MenuAppUpdateTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Logout.ToString()].SharedProps.StatusText = mSettings["MenuLogoutTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Close.ToString()].SharedProps.StatusText = mSettings["MenuCloseTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Help.ToString()].SharedProps.StatusText = mSettings["MenuHelpTooltip"];
			}

			xToolbarsManager.Tools[ToolbarKeys.Space01.ToString()].SharedProps.Spring = true;

			// 단축키를 할당한다.
			xToolbarsManager.Tools[ToolbarKeys.MenuName.ToString()].SharedProps.Shortcut = Shortcut.CtrlF;
			xToolbarsManager.Tools[ToolbarKeys.AppUpdate.ToString()].SharedProps.Shortcut = Shortcut.CtrlF10;
			xToolbarsManager.Tools[ToolbarKeys.Logout.ToString()].SharedProps.Shortcut = Shortcut.CtrlF11;
			xToolbarsManager.Tools[ToolbarKeys.Close.ToString()].SharedProps.Shortcut = Shortcut.CtrlF12;
			xToolbarsManager.Tools[ToolbarKeys.Help.ToString()].SharedProps.Shortcut = Shortcut.F1;

			// NOTICE: 도움말 버튼을 없앤다.
			// by KIMKIWON\xyz37 in 2007년 9월 22일 토요일 오후 2:31
			xToolbarsManager.Tools[ToolbarKeys.Help.ToString()].SharedProps.Visible = false;

			// 탭이 많을 경우 일부 탭이 보이도록 한다.
			xTabbedMdiManager.TabGroupSettings.ShowPartialTabs = DefaultableBoolean.True;

			xToolbarsManager.ToolClick += new ToolClickEventHandler(xToolbarsManager_ToolClick);
			xToolbarsManager.ToolKeyDown += new ToolKeyEventHandler(xToolbarsManager_ToolKeyDown);
		}
		#endregion

		#region InitStandardToolBar
		private void InitStandardToolBar()
		{
			// 표준 툴버튼
			UltraToolbar xtbStandard = xToolbarsManager.Toolbars[STANDARD_KEY];

			xtbStandard.Settings.FillEntireRow = DefaultableBoolean.True;
			xtbStandard.Settings.AllowCustomize = DefaultableBoolean.False;
			xtbStandard.Settings.AllowHiding = DefaultableBoolean.False;

			// 버튼을 툴바에 구애 받지 않고 이동 시킬 수 있게 전체 툴바의 Tool로 제어 한다.
			using (MessageSettings mSettings = new MessageSettings())
			{
				xToolbarsManager.Tools[ToolbarKeys.New.ToString()].SharedProps.StatusText = mSettings["MenuNewTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Search.ToString()].SharedProps.StatusText = mSettings["MenuSearchTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Run.ToString()].SharedProps.StatusText = mSettings["MenuRunTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Save.ToString()].SharedProps.StatusText = mSettings["MenuSaveTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Cancel.ToString()].SharedProps.StatusText = mSettings["MenuCancelTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Delete.ToString()].SharedProps.StatusText = mSettings["MenuDeleteTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.AddRow.ToString()].SharedProps.StatusText = mSettings["MenuAddRowTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.DelRow.ToString()].SharedProps.StatusText = mSettings["MenuDelRowTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Excel.ToString()].SharedProps.StatusText = mSettings["MenuExcelTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Preview.ToString()].SharedProps.StatusText = mSettings["MenuPreviewTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.Print.ToString()].SharedProps.StatusText = mSettings["MenuPrintTooltip"];
				xToolbarsManager.Tools[ToolbarKeys.FormExit.ToString()].SharedProps.StatusText = mSettings["MenuExitTooltip"];
			}

			// 단축키를 할당한다.
			xToolbarsManager.Tools[ToolbarKeys.New.ToString()].SharedProps.Shortcut = Shortcut.F2;
			xToolbarsManager.Tools[ToolbarKeys.Search.ToString()].SharedProps.Shortcut = Shortcut.F3;
			xToolbarsManager.Tools[ToolbarKeys.Run.ToString()].SharedProps.Shortcut = Shortcut.F4;
			//xToolbarsManager.Tools[StandardToolbarKeys.Save.ToString()].SharedProps.Shortcut = Shortcut.F5;
			xToolbarsManager.Tools[ToolbarKeys.Cancel.ToString()].SharedProps.Shortcut = Shortcut.F5;
			xToolbarsManager.Tools[ToolbarKeys.Delete.ToString()].SharedProps.Shortcut = Shortcut.F6;
			xToolbarsManager.Tools[ToolbarKeys.AddRow.ToString()].SharedProps.Shortcut = Shortcut.F7;
			xToolbarsManager.Tools[ToolbarKeys.DelRow.ToString()].SharedProps.Shortcut = Shortcut.F8;
			xToolbarsManager.Tools[ToolbarKeys.Excel.ToString()].SharedProps.Shortcut = Shortcut.F9;
			xToolbarsManager.Tools[ToolbarKeys.Preview.ToString()].SharedProps.Shortcut = Shortcut.F10;
			xToolbarsManager.Tools[ToolbarKeys.Print.ToString()].SharedProps.Shortcut = Shortcut.F11;
			xToolbarsManager.Tools[ToolbarKeys.FormExit.ToString()].SharedProps.Shortcut = Shortcut.F12;

			// MenuPath를 Spring 상태로 설정한다.
			xToolbarsManager.Tools[ToolbarKeys.MenuPath.ToString()].SharedProps.Spring = true;

			//			xToolbarsManager.Toolbars[0].Tools[StandardToolbarKeys.Query.ToString()].InstanceProps.IsFirstInGroup = true;
			//			xToolbarsManager.Toolbars[0].Tools[StandardToolbarKeys.Cancel.ToString()].InstanceProps.IsFirstInGroup = true;
			//			xToolbarsManager.Toolbars[0].Tools[StandardToolbarKeys.AddRow.ToString()].InstanceProps.IsFirstInGroup = true;
			//			xToolbarsManager.Toolbars[0].Tools[StandardToolbarKeys.Excel.ToString()].InstanceProps.IsFirstInGroup = true;
			//			xToolbarsManager.Toolbars[0].Tools[StandardToolbarKeys.Exit.ToString()].InstanceProps.IsFirstInGroup = true;

			xtbStandard.Visible = false;
		}
		#endregion

		#region Main Toolbar
		private void xToolbarsManager_ToolClick(object sender, ToolClickEventArgs e)
		{
			UltraToolbarsManager xToolbar = sender as UltraToolbarsManager;

			if (Enum.IsDefined(typeof(ToolbarKeys), e.Tool.Key) == true)
			{
				switch ((ToolbarKeys)Enum.Parse(typeof(ToolbarKeys), e.Tool.Key))
				{
					case ToolbarKeys.Favorites:
						FavoritesClick();

						break;
					case ToolbarKeys.FindMenu:
						FindMenuClick();

						break;
					case ToolbarKeys.AppUpdate:
						AppUpdateClick();

						break;
					case ToolbarKeys.Logout:
						LogoutClick();

						break;
					case ToolbarKeys.Close:
						CloseFormClick();

						break;
					case ToolbarKeys.Help:
						HelpClick();

						break;
				}
			}

			if (ActiveTab != null && Enum.IsDefined(typeof(ToolbarKeys), e.Tool.Key) == true)
			{
				switch ((ToolbarKeys)Enum.Parse(typeof(ToolbarKeys), e.Tool.Key))
				{
					case ToolbarKeys.New:
						OnNewButtonClick();

						break;
					case ToolbarKeys.Save:
						OnSaveButtonClick();

						break;
					case ToolbarKeys.Search:
						OnSearchButtonClick();

						break;
					case ToolbarKeys.Run:
						OnRunButtonClick();

						break;
					case ToolbarKeys.Cancel:
						OnCancelButtonClick();

						break;
					case ToolbarKeys.Delete:
						OnDeleteButtonClick();

						break;
					case ToolbarKeys.AddRow:
						OnAddRowButtonClick();

						break;
					case ToolbarKeys.DelRow:
						OnDelRowButtonClick();

						break;
					case ToolbarKeys.Excel:
						OnExcelButtonClick();

						break;
					case ToolbarKeys.Preview:
						OnPreviewButtonClick();

						break;
					case ToolbarKeys.Print:
						OnPrintButtonClick();

						break;
					case ToolbarKeys.FormExit:
						OnExitButtonClick();

						break;
				}
			}
		}

		private void xToolbarsManager_ToolKeyDown(object sender, ToolKeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				if (e.Tool.Key == ToolbarKeys.MenuName.ToString())
					FindMenuClick();
			}
		}
		#endregion

		#region MainToolbar Action
		private void FavoritesClick()
		{
			SetActivePane(DockablePaneKeys.Favorite);
			// NOTICE: 즐겨찾기 버튼을 누를 경우 현재 세팅을 저장 한다.
			// by KIMKIWON\xyz37 in 2007년 10월 18일 목요일 오후 8:44
			MenuManager menuManager = new MenuManager(_userId);

			menuManager.SaveFavoriteMenusToLocalFile(_xTreeFavorites);
		}

		private void FindMenuClick()
		{
			UltraToolbar xToolbarMain = xToolbarsManager.Toolbars[MAINMENU_KEY];
			TextBoxTool txtMenuName = xToolbarMain.Tools[ToolbarKeys.MenuName.ToString()] as TextBoxTool;

			if (txtMenuName != null && txtMenuName.Text != string.Empty)
			{
				_xTreeSearchResult.Nodes.Clear();
				SetRootNode(_xTreeSearchResult, _rootNode);
				SearchResultAction(_rootNode, txtMenuName.Text);
			}
		}

		private void SearchResultAction(UltraTreeNode targetNode, string searchMenuText)
		{
			UltraToolbar xToolbarMain = xToolbarsManager.Toolbars[MAINMENU_KEY];
			TextBoxTool txtMenuName = xToolbarMain.Tools[ToolbarKeys.MenuName.ToString()] as TextBoxTool;

			if (txtMenuName != null && txtMenuName.Text != string.Empty)
			{
				bool isCaseSensitive = LoaderSetting.SearchMenuCaseSensitive;
				bool isContains = false;
				List<UltraTreeNode> allNodes = GetAllNodes(targetNode);

				foreach (UltraTreeNode node in allNodes)
				{
					if (IsRootNode(node) == true)
						continue;

					if (isCaseSensitive == true)
						isContains = node.Text.Contains(searchMenuText);
					else
						isContains = node.Text.ToLower().Contains(searchMenuText.ToLower());

					System.Diagnostics.Debug.Print(node.Text.ToLower());
					if (isContains == true)
					{
						string rootNodeKey = node.RootNode.Key;
						UltraTreeNode targetRootNode = _xTreeSearchResult.Nodes[rootNodeKey];
						UltraTreeNode searchedNode =  node.Clone() as UltraTreeNode;

						if (searchedNode != null)
						{
							searchedNode.Nodes.Clear();
							targetRootNode.Nodes.Add(searchedNode);
						}

						_xTreeSearchResult.Nodes[0].RootNode.ExpandAll(ExpandAllType.Always);
					}
				}

				SetActivePane(DockablePaneKeys.SearchResult);
			}
		}

		private void AppUpdateClick()
		{
			BeforeFormClosingAction();

			using (MessageSettings mSettings = new MessageSettings())
			{
				DialogResult result = MessageBox.Show(mSettings["QuestAppUpdate"], mSettings["QuestAppUpdateCaption"], MessageBoxButtons.YesNo, MessageBoxIcon.Question);

				if (result == DialogResult.Yes)
				{
					// 열린 Form에 대허서 종료 처리 한다.
					if (ConfigWrapper.Loader.EnableMessageService == true && _mdiTabs != null)
					{
						foreach (MdiTab tab in _mdiTabs)
						{
							MenuView menuView = tab.Tag as MenuView;

							// MESSAGE if (menuView != null)
								// MESSAGE MessageAgent.Instance.SendFormCloseMessage(menuView.FormId, GetMenuPath(menuView.FormDesc), GetVersion(menuView.FormDesc));
						}
					}

					// MESSAGE MessageAgent.Instance.AppUpdate();

					// UserName은 UserName(UserId) 형태이므로 변경해줘야 한다.
					ExchangeForm.Call(PlantId,
						string.Format("{0}{1}{2}",
							UserId,
							ExchangeForm.ArgumentDelimiter,
							UserName.Replace(' ', ExchangeForm.ArgumentDelimiter).Replace(string.Format("({0})", UserId), string.Empty)),
						ExchangeFormType.FXWinUpdater);
				}
			}
		}

		private void LogoutClick()
		{
			BeforeFormClosingAction();

			using (MessageSettings mSettings = new MessageSettings())
			{
				DialogResult result = MessageBox.Show(mSettings["QuestLogout"], mSettings["QuestLogoutCaption"], MessageBoxButtons.YesNo, MessageBoxIcon.Question);

				if (result == DialogResult.Yes)
				{
					// MESSAGE MessageAgent.Instance.Logout();

					ExchangeForm.Call(PlantId, UserId, ExchangeFormType.FXWinLogin);
				}
			}
		}

		private void CloseFormClick()
		{
			Close();
		}

		private void HelpClick()
		{
			//			
		}
		#endregion

		#region StandardToolbar Action
		private void OnNewButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnNewClickAction();
		}

		private void OnSaveButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnSaveClickAction();
		}

		private void OnSearchButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnSearchClickAction();
		}

		private void OnRunButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnRunClickAction();
		}

		private void OnCancelButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnCancelClickAction();
		}

		private void OnDeleteButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnDeleteClickAction();
		}

		private void OnAddRowButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnAddRowClickAction();
		}

		private void OnDelRowButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnDelRowClickAction();
		}

		private void OnExcelButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnExcelClickAction();
		}

		private void OnPreviewButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnPreviewClickAction();
		}

		private void OnPrintButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnPrintClickAction();
		}

		private void OnExitButtonClick()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnExitClickAction();
		}
		#endregion

		#region IStandardToolbarProperty 멤버
		string IStandardToolbarProperty.MenuPath
		{
			get
			{
				string menuPath = string.Empty;

				LabelTool lblMenuPath = xToolbarsManager.Tools[ToolbarKeys.MenuPath.ToString()] as LabelTool;

				if (lblMenuPath != null)
					menuPath = lblMenuPath.SharedProps.Caption;

				return menuPath;
			}
			set
			{
				LabelTool lblMenuPath = xToolbarsManager.Tools[ToolbarKeys.MenuPath.ToString()] as LabelTool;

				if (lblMenuPath != null)
					lblMenuPath.SharedProps.Caption = value;
			}
		}

		#region Enable Toolbar Buttons
		/// <summary>
		/// New Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnableNewButton
		{
			get
			{
				ButtonTool btnNew = xToolbarsManager.Tools[ToolbarKeys.New.ToString()] as ButtonTool;
				bool enableNewButton = false;

				if (btnNew != null)
					enableNewButton = btnNew.SharedProps.Enabled;

				return enableNewButton;
			}
			set
			{
				ButtonTool btnNew = xToolbarsManager.Tools[ToolbarKeys.New.ToString()] as ButtonTool;

				if (btnNew != null)
					btnNew.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Save Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnableSaveButton
		{
			get
			{
				ButtonTool btnSave = xToolbarsManager.Tools[ToolbarKeys.Save.ToString()] as ButtonTool;
				bool enableSaveButton = false;

				if (btnSave != null)
					enableSaveButton = btnSave.SharedProps.Enabled;

				return enableSaveButton;
			}
			set
			{
				ButtonTool btnSave = xToolbarsManager.Tools[ToolbarKeys.Save.ToString()] as ButtonTool;

				if (btnSave != null)
					btnSave.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Search Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnableSearchButton
		{
			get
			{
				ButtonTool btnSearch = xToolbarsManager.Tools[ToolbarKeys.Search.ToString()] as ButtonTool;
				bool enableSearchButton = false;

				if (btnSearch != null)
					enableSearchButton = btnSearch.SharedProps.Enabled;

				return enableSearchButton;
			}
			set
			{
				ButtonTool btnSearch = xToolbarsManager.Tools[ToolbarKeys.Search.ToString()] as ButtonTool;

				if (btnSearch != null)
					btnSearch.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Run Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnableRunButton
		{
			get
			{
				ButtonTool btnRun = xToolbarsManager.Tools[ToolbarKeys.Run.ToString()] as ButtonTool;
				bool enableRunButton = false;

				if (btnRun != null)
					enableRunButton = btnRun.SharedProps.Enabled;

				return enableRunButton;
			}
			set
			{
				ButtonTool btnRun = xToolbarsManager.Tools[ToolbarKeys.Run.ToString()] as ButtonTool;

				if (btnRun != null)
					btnRun.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Cancel Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnableCancelButton
		{
			get
			{
				ButtonTool btnCancel = xToolbarsManager.Tools[ToolbarKeys.Cancel.ToString()] as ButtonTool;
				bool enableCancelButton = false;

				if (btnCancel != null)
					enableCancelButton = btnCancel.SharedProps.Enabled;

				return enableCancelButton;
			}
			set
			{
				ButtonTool btnCancel = xToolbarsManager.Tools[ToolbarKeys.Cancel.ToString()] as ButtonTool;

				if (btnCancel != null)
					btnCancel.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Delete Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnableDeleteButton
		{
			get
			{
				ButtonTool btnDelete = xToolbarsManager.Tools[ToolbarKeys.Delete.ToString()] as ButtonTool;
				bool enableDeleteButton = false;

				if (btnDelete != null)
					enableDeleteButton = btnDelete.SharedProps.Enabled;

				return enableDeleteButton;
			}
			set
			{
				ButtonTool btnDelete = xToolbarsManager.Tools[ToolbarKeys.Delete.ToString()] as ButtonTool;

				if (btnDelete != null)
					btnDelete.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// AddRow Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnableAddRowButton
		{
			get
			{
				ButtonTool btnAddRow = xToolbarsManager.Tools[ToolbarKeys.AddRow.ToString()] as ButtonTool;
				bool enableAddRowButton = false;

				if (btnAddRow != null)
					enableAddRowButton = btnAddRow.SharedProps.Enabled;

				return enableAddRowButton;
			}
			set
			{
				ButtonTool btnAddRow = xToolbarsManager.Tools[ToolbarKeys.AddRow.ToString()] as ButtonTool;

				if (btnAddRow != null)
					btnAddRow.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// DelRow Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnableDelRowButton
		{
			get
			{
				ButtonTool btnDelRow = xToolbarsManager.Tools[ToolbarKeys.DelRow.ToString()] as ButtonTool;
				bool enableDelRowButton = false;

				if (btnDelRow != null)
					enableDelRowButton = btnDelRow.SharedProps.Enabled;

				return enableDelRowButton;
			}
			set
			{
				ButtonTool btnDelRow = xToolbarsManager.Tools[ToolbarKeys.DelRow.ToString()] as ButtonTool;

				if (btnDelRow != null)
					btnDelRow.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Excel Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnableExcelButton
		{
			get
			{
				ButtonTool btnExcel = xToolbarsManager.Tools[ToolbarKeys.Excel.ToString()] as ButtonTool;
				bool enableExcelButton = false;

				if (btnExcel != null)
					enableExcelButton = btnExcel.SharedProps.Enabled;

				return enableExcelButton;
			}
			set
			{
				ButtonTool btnExcel = xToolbarsManager.Tools[ToolbarKeys.Excel.ToString()] as ButtonTool;

				if (btnExcel != null)
					btnExcel.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Preview Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnablePreviewButton
		{
			get
			{
				ButtonTool btnPreview = xToolbarsManager.Tools[ToolbarKeys.Preview.ToString()] as ButtonTool;
				bool enablePreviewButton = false;

				if (btnPreview != null)
					enablePreviewButton = btnPreview.SharedProps.Enabled;

				return enablePreviewButton;
			}
			set
			{
				ButtonTool btnPreview = xToolbarsManager.Tools[ToolbarKeys.Preview.ToString()] as ButtonTool;

				if (btnPreview != null)
					btnPreview.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Print Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnablePrintButton
		{
			get
			{
				ButtonTool btnPrint = xToolbarsManager.Tools[ToolbarKeys.Print.ToString()] as ButtonTool;
				bool enablePrintButton = false;

				if (btnPrint != null)
					enablePrintButton = btnPrint.SharedProps.Enabled;

				return enablePrintButton;
			}
			set
			{
				ButtonTool btnPrint = xToolbarsManager.Tools[ToolbarKeys.Print.ToString()] as ButtonTool;

				if (btnPrint != null)
					btnPrint.SharedProps.Enabled = value;
			}
		}

		/// <summary>
		/// Exit Button의 Enabled 상태를 구하거나 설정합니다.
		/// </summary>
		bool IStandardToolbarProperty.EnableExitButton
		{
			get
			{
				ButtonTool btnExit = xToolbarsManager.Tools[ToolbarKeys.FormExit.ToString()] as ButtonTool;
				bool enableExitButton = false;

				if (btnExit != null)
					enableExitButton = btnExit.SharedProps.Enabled;

				return enableExitButton;
			}
			set
			{
				ButtonTool btnExit = xToolbarsManager.Tools[ToolbarKeys.FormExit.ToString()] as ButtonTool;

				if (btnExit != null)
					btnExit.SharedProps.Enabled = value;
			}
		}
		#endregion

		#endregion
	}
}
