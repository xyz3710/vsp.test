﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.LoaderSetting
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 10월 8일 월요일 오후 4:02
/*	Purpose		:	iDASiT.FX.Win.Loader를 config 파일을 통하여 기본 세팅을 변경할 수 있도록 한다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using iDASiT.FX.Win.Loader.Setting;

namespace iDASiT.FX.Win.Loader
{
	internal class LoaderSetting
	{
		#region Application Settings Properties
		/// <summary>
		/// MenuConfigFilePath를 구합니다.
		/// </summary>
		public static String MenuConfigFilePath
		{
			get
			{
				return Settings.Default.MenuConfigFilePath;
			}
		}

		/// <summary>
		/// FavoritesMenuConfigFilePath를 구합니다.
		/// </summary>
		public static String FavoritesMenuConfigFilePath
		{
			get
			{
				return Settings.Default.FavoritesMenuConfigFilePath;
			}
		}

		/// <summary>
		/// Text를 구합니다.
		/// </summary>
		public static String Text
		{
			get
			{
				return Settings.Default.Text;
			}
		}

		/// <summary>
		/// ProcessBar Interval를 구합니다.
		/// </summary>
		public static int ProcessBarInterval
		{
			get
			{
				return Settings.Default.ProcessBarInterval;
			}
		}

		/// <summary>
		/// StartPositoin를 구합니다.
		/// </summary>
		public static FormStartPosition StartPosition
		{
			get
			{
				return Settings.Default.StartPosition;
			}
		}
		#endregion

		#region User Settings Properties
		/// <summary>
		/// WindowsState를 구하거나 설정합니다.
		/// </summary>
		public static FormWindowState WindowsState
		{
			get
			{
				return Settings.Default.WindowsState;
			}
			set
			{
				Settings.Default.WindowsState = value;
			}
		}

		/// <summary>
		/// ShowCloseConfirmMessage를 구하거나 설정합니다.
		/// </summary>
		public static bool IsShowCloseConfirmMessage
		{
			get
			{
				return Settings.Default.IsShowCloseConfirmMessage;
			}
			set
			{
				Settings.Default.IsShowCloseConfirmMessage = value;
			}
		}
		
		/// <summary>
		/// Form Size를 구하거나 설정합니다.
		/// </summary>
		public static Size Size
		{
			get
			{
				return Settings.Default.Size;
			}
			set
			{
				Settings.Default.Size = value;
			}
		}

		/// <summary>
		/// Form의 Location을 구하거나 설정합니다.
		/// </summary>
		public static Point Location
		{
			get
			{
				return Settings.Default.Location;
			}
			set
			{
				Settings.Default.Location = value;
			}
		}

		/// <summary>
		/// IsAutoHideMenu를 구하거나 설정합니다.
		/// </summary>
		public static bool IsAutoHideMenu
		{
			get
			{
				return Settings.Default.IsAutoHideMenu;
			}
			set
			{
				Settings.Default.IsAutoHideMenu = value;
			}
		}

		/// <summary>
		/// IsAutoHideFavorites를 구하거나 설정합니다.
		/// </summary>
		public static bool IsAutoHideFavorites
		{
			get
			{
				return Settings.Default.IsAutoHideFavorites;
			}
			set
			{
				Settings.Default.IsAutoHideFavorites = value;
			}
		}

		/// <summary>
		/// IsFloatMenu를 구하거나 설정합니다.
		/// </summary>
		public static bool IsFloatMenu
		{
			get
			{
				return Settings.Default.IsFloatMenu;
			}
			set
			{
				Settings.Default.IsFloatMenu = value;
			}
		}

		/// <summary>
		/// IsFloatFavorites를 구하거나 설정합니다.
		/// </summary>
		public static bool IsFloatFavorites
		{
			get
			{
				return Settings.Default.IsFloatFavorites;
			}
			set
			{
				Settings.Default.IsFloatFavorites = value;
			}
		}

		/// <summary>
		/// 사용자 설정의 저장 여부를 구하거나 설정합니다.
		/// </summary>
		public static bool IsSaveUserSetting
		{
			get
			{
				return Settings.Default.IsSaveUserSetting;
			}
			set
			{
				Settings.Default.IsSaveUserSetting = value;
			}
		}
		/// <summary>
		/// FloatMenuLocation를 구하거나 설정합니다.
		/// </summary>
		public static Point FloatMenuLocation
		{
			get
			{
				return Settings.Default.FloatMenuLocation;
			}
			set
			{
				Settings.Default.FloatMenuLocation = value;
			}
		}

		/// <summary>
		/// FloatFavoritesLocation를 구하거나 설정합니다.
		/// </summary>
		public static Point FloatFavoritesLocation
		{
			get
			{
				return Settings.Default.FloatFavoritesLocation;
			}
			set
			{
				Settings.Default.FloatFavoritesLocation = value;
			}
		}

		/// <summary>
		/// FloatMenuSize를 구하거나 설정합니다.
		/// </summary>
		public static Size FloatMenuSize
		{
			get
			{
				return Settings.Default.FloatMenuSize;
			}
			set
			{
				Settings.Default.FloatMenuSize = value;
			}
		}

		/// <summary>
		/// FloatFavoritesSize를 구하거나 설정합니다.
		/// </summary>
		public static Size FloatFavoritesSize
		{
			get
			{
				return Settings.Default.FloatFavoritesSize;
			}
			set
			{
				Settings.Default.FloatFavoritesSize = value;
			}
		}

		/// <summary>
		/// SearchMenuCaseSensitive를 구하거나 설정합니다.
		/// </summary>
		public static bool SearchMenuCaseSensitive
		{
			get
			{
				return Settings.Default.SearchMenuCaseSensitive;
			}
			set
			{
				Settings.Default.SearchMenuCaseSensitive = value;
			}
		}		
		#endregion

		/// <summary>
		/// 설정 파일을 저장 합니다.
		/// </summary>
		public static void Save()
		{
			Settings.Default.Save();
		}

		/// <summary>
		/// 마지막으로 저장된 설정값을 다시 로드 합니다.
		/// </summary>
		public static void LoadLastSavedSetting()
		{
			Settings.Default.Reload();
			Save();
		}

		/// <summary>
		/// 기본 값을 로드 합니다.
		/// </summary>
		public static void LoadDefault()
		{
			Settings.Default.Reset();
			Save();
		}
	}
}
