﻿namespace iDASiT.FX.Win.Loader
{
	partial class Loader
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Loader));
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel2 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel3 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel4 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel5 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
			this.xProgressBar = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
			this.xTabbedMdiManager = new Infragistics.Win.UltraWinTabbedMdi.UltraTabbedMdiManager(this.components);
			this.xDockManager = new Infragistics.Win.UltraWinDock.UltraDockManager(this.components);
			this.imlPanes = new System.Windows.Forms.ImageList(this.components);
			this._ToolBarFormUnpinnedTabAreaLeft = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._ToolBarFormUnpinnedTabAreaRight = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._ToolBarFormUnpinnedTabAreaTop = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._ToolBarFormUnpinnedTabAreaBottom = new Infragistics.Win.UltraWinDock.UnpinnedTabArea();
			this._ToolBarFormAutoHideControl = new Infragistics.Win.UltraWinDock.AutoHideControl();
			this.imlMenus = new System.Windows.Forms.ImageList(this.components);
			this.imlFavoriteContext = new System.Windows.Forms.ImageList(this.components);
			this.xStatusBar = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
			((System.ComponentModel.ISupportInitialize)(this.xTabbedMdiManager)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xDockManager)).BeginInit();
			this.xStatusBar.SuspendLayout();
			this.SuspendLayout();
			// 
			// xProgressBar
			// 
			appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(234)))), ((int)(((byte)(238)))));
			this.xProgressBar.Appearance = appearance1;
			this.xProgressBar.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
			appearance2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(222)))), ((int)(((byte)(241)))));
			appearance2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(146)))), ((int)(((byte)(226)))));
			appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			this.xProgressBar.FillAppearance = appearance2;
			this.xProgressBar.Location = new System.Drawing.Point(492, 3);
			this.xProgressBar.Name = "xProgressBar";
			this.xProgressBar.SegmentWidth = 5;
			this.xProgressBar.Size = new System.Drawing.Size(198, 23);
			this.xProgressBar.Step = 1;
			this.xProgressBar.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.SegmentedPartial;
			this.xProgressBar.TabIndex = 29;
			this.xProgressBar.Text = "[Formatted]";
			this.xProgressBar.TextVisible = false;
			this.xProgressBar.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xProgressBar.Value = 100;
			// 
			// xTabbedMdiManager
			// 
			this.xTabbedMdiManager.AllowNestedTabGroups = Infragistics.Win.DefaultableBoolean.True;
			this.xTabbedMdiManager.MdiParent = this;
			appearance47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(206)))), ((int)(((byte)(213)))));
			this.xTabbedMdiManager.TabSettings.ActiveTabAppearance = appearance47;
			appearance48.BackColor = System.Drawing.Color.White;
			this.xTabbedMdiManager.TabSettings.TabAppearance = appearance48;
			this.xTabbedMdiManager.ViewStyle = Infragistics.Win.UltraWinTabbedMdi.ViewStyle.VisualStudio2005;
			this.xTabbedMdiManager.TabActivated += new Infragistics.Win.UltraWinTabbedMdi.MdiTabEventHandler(this.xTabbedMdiManager_TabActivated);
			this.xTabbedMdiManager.TabClosing += new Infragistics.Win.UltraWinTabbedMdi.CancelableMdiTabEventHandler(this.xTabbedMdiManager_TabClosing);
			this.xTabbedMdiManager.InitializeContextMenu += new Infragistics.Win.UltraWinTabbedMdi.MdiTabContextMenuEventHandler(this.xTabbedMdiManager_InitializeContextMenu);
			this.xTabbedMdiManager.TabDisplaying += new Infragistics.Win.UltraWinTabbedMdi.MdiTabEventHandler(this.xTabbedMdiManager_TabDisplaying);
			// 
			// xDockManager
			// 
			this.xDockManager.AutoHideDelay = 10;
			this.xDockManager.DragWindowStyle = Infragistics.Win.UltraWinDock.DragWindowStyle.LayeredWindowWithIndicators;
			this.xDockManager.HostControl = this;
			this.xDockManager.ImageList = this.imlPanes;
			this.xDockManager.SaveSettingsFormat = Infragistics.Win.SaveSettingsFormat.Xml;
			this.xDockManager.SettingsKey = "xDockManagerMainMenu";
			this.xDockManager.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xDockManager.WindowStyle = Infragistics.Win.UltraWinDock.WindowStyle.VisualStudio2005;
			// 
			// imlPanes
			// 
			this.imlPanes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlPanes.ImageStream")));
			this.imlPanes.TransparentColor = System.Drawing.Color.Transparent;
			this.imlPanes.Images.SetKeyName(0, "favorites");
			this.imlPanes.Images.SetKeyName(1, "searchresult");
			this.imlPanes.Images.SetKeyName(2, "m");
			this.imlPanes.Images.SetKeyName(3, "f");
			this.imlPanes.Images.SetKeyName(4, "p");
			this.imlPanes.Images.SetKeyName(5, "s");
			this.imlPanes.Images.SetKeyName(6, "r");
			this.imlPanes.Images.SetKeyName(7, "u");
			// 
			// _ToolBarFormUnpinnedTabAreaLeft
			// 
			this._ToolBarFormUnpinnedTabAreaLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this._ToolBarFormUnpinnedTabAreaLeft.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._ToolBarFormUnpinnedTabAreaLeft.Location = new System.Drawing.Point(0, 0);
			this._ToolBarFormUnpinnedTabAreaLeft.Name = "_ToolBarFormUnpinnedTabAreaLeft";
			this._ToolBarFormUnpinnedTabAreaLeft.Owner = this.xDockManager;
			this._ToolBarFormUnpinnedTabAreaLeft.Size = new System.Drawing.Size(0, 704);
			this._ToolBarFormUnpinnedTabAreaLeft.TabIndex = 0;
			// 
			// _ToolBarFormUnpinnedTabAreaRight
			// 
			this._ToolBarFormUnpinnedTabAreaRight.Dock = System.Windows.Forms.DockStyle.Right;
			this._ToolBarFormUnpinnedTabAreaRight.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._ToolBarFormUnpinnedTabAreaRight.Location = new System.Drawing.Point(1020, 0);
			this._ToolBarFormUnpinnedTabAreaRight.Name = "_ToolBarFormUnpinnedTabAreaRight";
			this._ToolBarFormUnpinnedTabAreaRight.Owner = this.xDockManager;
			this._ToolBarFormUnpinnedTabAreaRight.Size = new System.Drawing.Size(0, 704);
			this._ToolBarFormUnpinnedTabAreaRight.TabIndex = 1;
			// 
			// _ToolBarFormUnpinnedTabAreaTop
			// 
			this._ToolBarFormUnpinnedTabAreaTop.Dock = System.Windows.Forms.DockStyle.Top;
			this._ToolBarFormUnpinnedTabAreaTop.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._ToolBarFormUnpinnedTabAreaTop.Location = new System.Drawing.Point(0, 0);
			this._ToolBarFormUnpinnedTabAreaTop.Name = "_ToolBarFormUnpinnedTabAreaTop";
			this._ToolBarFormUnpinnedTabAreaTop.Owner = this.xDockManager;
			this._ToolBarFormUnpinnedTabAreaTop.Size = new System.Drawing.Size(1020, 0);
			this._ToolBarFormUnpinnedTabAreaTop.TabIndex = 2;
			// 
			// _ToolBarFormUnpinnedTabAreaBottom
			// 
			this._ToolBarFormUnpinnedTabAreaBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this._ToolBarFormUnpinnedTabAreaBottom.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._ToolBarFormUnpinnedTabAreaBottom.Location = new System.Drawing.Point(0, 704);
			this._ToolBarFormUnpinnedTabAreaBottom.Name = "_ToolBarFormUnpinnedTabAreaBottom";
			this._ToolBarFormUnpinnedTabAreaBottom.Owner = this.xDockManager;
			this._ToolBarFormUnpinnedTabAreaBottom.Size = new System.Drawing.Size(1020, 0);
			this._ToolBarFormUnpinnedTabAreaBottom.TabIndex = 3;
			// 
			// _ToolBarFormAutoHideControl
			// 
			this._ToolBarFormAutoHideControl.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._ToolBarFormAutoHideControl.Location = new System.Drawing.Point(0, 31);
			this._ToolBarFormAutoHideControl.Name = "_ToolBarFormAutoHideControl";
			this._ToolBarFormAutoHideControl.Owner = this.xDockManager;
			this._ToolBarFormAutoHideControl.Size = new System.Drawing.Size(100, 641);
			this._ToolBarFormAutoHideControl.TabIndex = 4;
			// 
			// imlMenus
			// 
			this.imlMenus.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlMenus.ImageStream")));
			this.imlMenus.TransparentColor = System.Drawing.Color.Transparent;
			this.imlMenus.Images.SetKeyName(0, "root");
			this.imlMenus.Images.SetKeyName(1, "menu");
			this.imlMenus.Images.SetKeyName(2, "folder");
			this.imlMenus.Images.SetKeyName(3, "disabled_menu");
			this.imlMenus.Images.SetKeyName(4, "not_exist_menu");
			this.imlMenus.Images.SetKeyName(5, "v_split");
			this.imlMenus.Images.SetKeyName(6, "h_split");
			this.imlMenus.Images.SetKeyName(7, "close");
			// 
			// imlFavoriteContext
			// 
			this.imlFavoriteContext.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlFavoriteContext.ImageStream")));
			this.imlFavoriteContext.TransparentColor = System.Drawing.Color.Transparent;
			this.imlFavoriteContext.Images.SetKeyName(0, "first");
			this.imlFavoriteContext.Images.SetKeyName(1, "last");
			this.imlFavoriteContext.Images.SetKeyName(2, "folder");
			this.imlFavoriteContext.Images.SetKeyName(3, "rename");
			this.imlFavoriteContext.Images.SetKeyName(4, "delete");
			this.imlFavoriteContext.Images.SetKeyName(5, "default");
			this.imlFavoriteContext.Images.SetKeyName(6, "delete_default");
			// 
			// xStatusBar
			// 
			this.xStatusBar.Controls.Add(this.xProgressBar);
			this.xStatusBar.Location = new System.Drawing.Point(0, 704);
			this.xStatusBar.Name = "xStatusBar";
			appearance42.TextVAlignAsString = "Middle";
			ultraStatusPanel1.Appearance = appearance42;
			ultraStatusPanel1.Key = "StatusMessage";
			ultraStatusPanel1.Padding = new System.Drawing.Size(3, 0);
			ultraStatusPanel1.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Spring;
			ultraStatusPanel1.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.AutoStatusText;
			ultraStatusPanel1.Width = 0;
			appearance43.FontData.BoldAsString = "True";
			appearance43.FontData.Name = "Tahoma";
			ultraStatusPanel2.Appearance = appearance43;
			ultraStatusPanel2.Control = this.xProgressBar;
			ultraStatusPanel2.Key = "Processbar";
			appearance44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(234)))), ((int)(((byte)(238)))));
			appearance44.FontData.BoldAsString = "True";
			appearance44.FontData.Name = "Tahoma";
			ultraStatusPanel2.ProgressBarInfo.Appearance = appearance44;
			appearance45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(222)))), ((int)(((byte)(241)))));
			appearance45.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(146)))), ((int)(((byte)(226)))));
			appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			ultraStatusPanel2.ProgressBarInfo.FillAppearance = appearance45;
			ultraStatusPanel2.ProgressBarInfo.Label = "";
			ultraStatusPanel2.ProgressBarInfo.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.Continuous;
			ultraStatusPanel2.ProgressBarInfo.Value = 80;
			ultraStatusPanel2.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.ControlContainer;
			ultraStatusPanel2.Width = 200;
			appearance49.TextHAlignAsString = "Center";
			appearance49.TextVAlignAsString = "Middle";
			ultraStatusPanel3.Appearance = appearance49;
			ultraStatusPanel3.Key = "UserInfo";
			ultraStatusPanel3.Padding = new System.Drawing.Size(3, 0);
			ultraStatusPanel3.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
			ultraStatusPanel3.Text = "UserInfo";
			ultraStatusPanel3.ToolTipText = "현재 접속한 사용자를 나타냅니다";
			ultraStatusPanel4.Key = "ClientIp";
			ultraStatusPanel4.Padding = new System.Drawing.Size(3, 0);
			ultraStatusPanel4.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
			ultraStatusPanel4.Text = "192.168.254.254";
			appearance46.TextHAlignAsString = "Center";
			appearance46.TextVAlignAsString = "Middle";
			ultraStatusPanel5.Appearance = appearance46;
			ultraStatusPanel5.DateTimeFormat = "yyyy-MM-dd tt hh:mm";
			ultraStatusPanel5.Key = "WorkDateTime";
			ultraStatusPanel5.Padding = new System.Drawing.Size(3, 0);
			ultraStatusPanel5.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
			ultraStatusPanel5.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Date;
			this.xStatusBar.Panels.AddRange(new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel[] {
            ultraStatusPanel1,
            ultraStatusPanel2,
            ultraStatusPanel3,
            ultraStatusPanel4,
            ultraStatusPanel5});
			this.xStatusBar.Size = new System.Drawing.Size(1020, 27);
			this.xStatusBar.TabIndex = 28;
			// 
			// Loader
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(234)))), ((int)(((byte)(238)))));
			this.ClientSize = new System.Drawing.Size(1020, 731);
			this.Controls.Add(this._ToolBarFormAutoHideControl);
			this.Controls.Add(this._ToolBarFormUnpinnedTabAreaTop);
			this.Controls.Add(this._ToolBarFormUnpinnedTabAreaBottom);
			this.Controls.Add(this._ToolBarFormUnpinnedTabAreaLeft);
			this.Controls.Add(this._ToolBarFormUnpinnedTabAreaRight);
			this.Controls.Add(this.xStatusBar);
			this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MinimumSize = new System.Drawing.Size(1020, 731);
			this.Name = "Loader";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Poly Crystalline Silicon";
			this.Load += new System.EventHandler(this.Loader_Load);
			this.Shown += new System.EventHandler(this.Loader_Shown);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Loader_FormClosing);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Loader_KeyDown);
			((System.ComponentModel.ISupportInitialize)(this.xTabbedMdiManager)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xDockManager)).EndInit();
			this.xStatusBar.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private Infragistics.Win.UltraWinTabbedMdi.UltraTabbedMdiManager xTabbedMdiManager;
		private Infragistics.Win.UltraWinDock.AutoHideControl _ToolBarFormAutoHideControl;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _ToolBarFormUnpinnedTabAreaTop;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _ToolBarFormUnpinnedTabAreaBottom;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _ToolBarFormUnpinnedTabAreaLeft;
		private Infragistics.Win.UltraWinDock.UnpinnedTabArea _ToolBarFormUnpinnedTabAreaRight;
		private Infragistics.Win.UltraWinDock.UltraDockManager xDockManager;
		private System.Windows.Forms.ImageList imlPanes;
		private System.Windows.Forms.ImageList imlMenus;
		private System.Windows.Forms.ImageList imlFavoriteContext;
		private Infragistics.Win.UltraWinStatusBar.UltraStatusBar xStatusBar;
		private Infragistics.Win.UltraWinProgressBar.UltraProgressBar xProgressBar;

	}
}