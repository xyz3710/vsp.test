﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.StatusbarKeys
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 4월 17일 목요일 오전 8:52
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Loader
{
	internal enum StatusbarKeys
	{
		/// <summary>
		/// StatusMessage
		/// </summary>
		StatusMessage = 0,
		/// <summary>
		/// Processbar
		/// </summary>
		Processbar,
		/// <summary>
		/// 사용자 정보
		/// </summary>
		UserInfo,
		/// <summary>
		/// Client Ip Address
		/// </summary>
		ClientIp,
		/// <summary>
		/// WorkDateTime
		/// </summary>
		WorkDateTime,
	}
}
