﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.Loader
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 11월 29일 목요일 오후 5:02
/*	Purpose		:	IProcessBar를 구현한 partial class
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;

namespace iDASiT.FX.Win.Loader
{
	public partial class Loader : IProcessBar
	{
		private void ProcessbarStart()
		{
			ShowProcessBar();
			ProcessBar.GetInstance(this).Start();
		}

		private void ProcessbarStop()
		{
			ProcessBar.GetInstance(this).Stop();
			HideProcessBar();
		}

		#region IProcessBar 멤버

		/// <summary>
		/// ProcessBar를 시작합니다.
		/// </summary>
		void IProcessBar.Start()
		{
			ProcessbarStart();
		}

		/// <summary>
		/// ProcessBar를 종료합니다.
		/// </summary>
		void IProcessBar.Stop()
		{
			ProcessbarStop();
		}

		#endregion
	}
}
