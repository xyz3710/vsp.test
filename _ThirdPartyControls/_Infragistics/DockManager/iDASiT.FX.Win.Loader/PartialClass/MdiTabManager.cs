﻿using System;
using System.Collections.Generic;
using System.Text;
using Infragistics.Win.UltraWinTabbedMdi;
using iDASiT.FX.Win.Configuration;
using Infragistics.Win.IGControls;
using System.Text.RegularExpressions;

namespace iDASiT.FX.Win.Loader
{
	public partial class Loader
	{
		/// <summary>
		/// 현재 활성화된 탭을 구하거나 설정합니다.
		/// </summary>
		private MdiTab ActiveTab
		{
			get
			{
				return xTabbedMdiManager.ActiveTab;
			}
			set
			{
				if (value != null)
                	value.Activate();
			}
		}

		private void xTabbedMdiManager_InitializeContextMenu(object sender, Infragistics.Win.UltraWinTabbedMdi.MdiTabContextMenuEventArgs e)
		{
			// 메뉴는 [닫기, -, 새 가로 탭 그룹, 새 세로 탭 그룹] 로 구성되어 있다.
			if (e.ContextMenu.MenuItems.Count > 0)
				TranslateMdiContextMenu(e);
		}

		private void xTabbedMdiManager_TabDisplaying(object sender, MdiTabEventArgs e)
		{
			MenuView menuView = e.Tab.Tag as MenuView;

			// MESSAGE if (menuView != null)
				// MESSAGE MessageAgent.Instance.SendFormOpenMessage(menuView.FormId, GetMenuPath(menuView.FormDesc), GetVersion(menuView.FormDesc));
		}

		private void xTabbedMdiManager_TabActivated(object sender, MdiTabEventArgs e)
		{
			// TODO: 현재 활성화된 Form이 있을 경우 Form의 StatusBarMessage를 저장한다.
			// by KIMKIWON\xyz37 in 2008년 11월 17일 월요일 오전 10:20
			// TabDeActivated Event가 없다 Infragistics Control Upgrade 후 진행
			if (ActiveTab.Form is BaseForm)
			{
				MenuView menuView = e.Tab.Tag as MenuView;

				//if (menuView != null)
				// MESSAGE MessageAgent.Instance.SendFormActiveMessage(menuView.FormId);

				_toolbarManager.ShowStandardToolbar();
				SetRight(this as IStandardToolbarProperty, e.Tab.Tag as MenuView);
			}			
		}

		private void xTabbedMdiManager_TabClosing(object sender, CancelableMdiTabEventArgs e)
		{
			IStandardToolbarProperty standardToolbarProperty = this as IStandardToolbarProperty;

			if (standardToolbarProperty != null)
				standardToolbarProperty.MenuPath = string.Empty;

			if (_mdiTabs != null)
			{
				if (_mdiTabs.Contains(e.Tab) == true)
					_mdiTabs.Remove(e.Tab);

				MenuView menuView = e.Tab.Tag as MenuView;

				//if (menuView != null)
					// MESSAGE MessageAgent.Instance.SendFormCloseMessage(menuView.FormId, GetMenuPath(menuView.FormDesc), GetVersion(menuView.FormDesc));
				
				if (_mdiTabs.Count == 0)
					 // ActiveTab이 없으면 제거 한다.
					_toolbarManager.HideStandardToolbar();
			}
		}

		private string GetMenuPath(string menuPath)
		{
			if (string.IsNullOrEmpty(menuPath) == true)
				return string.Empty;

			Regex regex = new Regex(@"(?<MenuPath>[\w\s\(\),.+->]+)(?<Version>[\(][\d\.]+[\)])", RegexOptions.None);
			
			return regex.Match(menuPath).Groups["MenuPath"].Value;
		}

		private string GetVersion(string menuPath)
		{
			if (string.IsNullOrEmpty(menuPath) == true)
				return "1.0.0.0";

			Regex regex = new Regex(@"(?<MenuPath>[\w\s\(\),.>]+)\((?<Version>[\d\.]+)\)", RegexOptions.None);
			string version = regex.Match(menuPath).Groups["Version"].Value;

			return version;
		}

		private void TranslateMdiContextMenu(MdiTabContextMenuEventArgs e)
		{
			bool isClose = false;

			using (ContextCaptionSettings ccSettings = new ContextCaptionSettings())
			{
				foreach (IGMenuItem menuItem in e.ContextMenu.MenuItems)
				{
					if (menuItem.Text == "&Close")
					{
						menuItem.Text = ccSettings["Close"];	// 닫기(&C)
						isClose = true;

						menuItem.Image = imlMenus.Images["exit"];
					}
					else if (menuItem.Text == "New Hori&zontal Tab Group")
					{
						menuItem.Text = ccSettings["NewHorTabGroup"];		// 새 가로 탭 그룹(&Z)
						menuItem.Image = imlMenus.Images["h_split"];
					}
					else if (menuItem.Text == "New &Vertical Tab Group")
					{
						menuItem.Text = ccSettings["NewVerTabGroup"];		// 새 세로 탭 그룹(&V)
						menuItem.Image = imlMenus.Images["v_split"];
					}
					else if (menuItem.Text == "Move to Ne&xt Tab Group")
						menuItem.Text = ccSettings["MoveNextTabGroup"];		// 다음 탭 그룹으로 이동(&X)
					else if (menuItem.Text == "Move to P&revious Tab Group")
						menuItem.Text = ccSettings["MovePrevTabGroup"];		// 이전 탭 그룹으로 이동(&R)
					else if (menuItem.Text == "C&ancel")
						menuItem.Text = ccSettings["Cancel"];		// 취소(&A)
				}

				if (isClose == true)
				{
					IGMenuItem mItemCloseOtherTabs = new IGMenuItem(ccSettings["CloseAllOtherTabs"], OnCloseAllOtherTabs);		// 이 창을 제외하고 모두 닫기(&A)

					mItemCloseOtherTabs.Tag = e.Tab;
					e.ContextMenu.MenuItems.Add(1, mItemCloseOtherTabs);
				}
			}
		}

		private void OnCloseAllOtherTabs(object sender, EventArgs ea)
		{
			IGMenuItem menuItem = sender as IGMenuItem;

			if (menuItem != null)
			{
				MdiTab selectedTab = menuItem.Tag as MdiTab;

				if (selectedTab != null)
				{
					List<MdiTab> removedList = new List<MdiTab>();

					foreach (MdiTab mdiTab in _mdiTabs)
					{
						if (selectedTab == mdiTab)
							continue;
						else
							removedList.Add(mdiTab);
					}

					foreach (MdiTab mdiTab in removedList)
						mdiTab.Close();

					_mdiTabs.Clear();
					_mdiTabs.Add(selectedTab);
				}
			}
		}

		private void SetRight(IStandardToolbarProperty standardToolbarProperty, MenuView menuView)
		{
			if (standardToolbarProperty == null || menuView == null)
				return;

			// Notice: AddRow, DelRow만 중복 권한을 체크하고 나머지는 하나의 권한만 있어야 한다.
			// by KIMKIWON\xyz37 in 2007년 9월 21일 금요일 오후 12:15
			standardToolbarProperty.EnableNewButton = menuView.CanRead;
			standardToolbarProperty.EnableSearchButton = menuView.CanRead;
			standardToolbarProperty.EnableRunButton = menuView.CanExecute;
			standardToolbarProperty.EnableSaveButton = menuView.CanUpdate;
			standardToolbarProperty.EnableCancelButton = menuView.CanUndo;
			standardToolbarProperty.EnableDeleteButton = menuView.CanDelete;
			standardToolbarProperty.EnableAddRowButton = menuView.CanCreate && (menuView.CanUpdate || menuView.CanExecute);		// 저장이나 실행이 가능해야 한다.
			standardToolbarProperty.EnableDelRowButton = menuView.CanCreate && (menuView.CanUpdate || menuView.CanExecute);		// 저장이나 실행이 가능해야 한다.
			standardToolbarProperty.EnableExcelButton = menuView.CanRead;
			standardToolbarProperty.EnablePreviewButton = false;
			standardToolbarProperty.EnablePrintButton = false;
			// TODO: 프린트 구현 후 풀어준다
			// by KIMKIWON\xyz37 in 2007년 12월 20일 목요일 오후 2:17
			/*
			menuForm.EnablePreviewButton = menuView.CanRead;
			menuForm.EnablePrintButton = menuView.CanRead;
			*/

			// NOTICE: TreeViewManager에 주석처리
			// MenuView와 MenuPath를 BaseForm에서 Loader로 승격 시켜서 MenuView.FormDesc에 MenuPath를 지정한다.
			// by KIMKIWON\xyz37 in 2008년 4월 16일 수요일 오전 11:41
			standardToolbarProperty.MenuPath = menuView.FormDesc;
		}
	}
}
