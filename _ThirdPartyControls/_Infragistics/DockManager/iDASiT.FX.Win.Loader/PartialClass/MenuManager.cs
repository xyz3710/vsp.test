﻿//#define USE_SECURITY_BLOCK
/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.MenuManager
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 12월 4일 화요일 오후 7:05
/*	Purpose		:	Menu를 구성하고 관리합니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using iDASiT.FX.Win.Configuration;
using Infragistics.Win.UltraWinToolTip;
using Infragistics.Win.UltraWinTree;

namespace iDASiT.FX.Win.Loader
{
	/// <summary>
	/// Menu를 구성하고 관리합니다.
	/// </summary>
	public class MenuManager
	{
		#region Fields
		private string _userId;
		#endregion

		#region Constructors
		/// <summary>
		/// MenuManager 클래스의 인스턴스를 생성합니다.
		/// </summary>
		/// <param name="userId"></param>
		public MenuManager(string userId)
		{
			_userId = userId;
		}
		#endregion

		#region Properties
		/// <summary>
		/// 사용자 Id가 포함된 Favorite 파일의 경로를 구합니다.
		/// </summary>
		private string LocalFavoriteConfigNameInUserId
		{
			get
			{
				string favoriteConfigFilePath = LoaderSetting.FavoritesMenuConfigFilePath;
				string filename = Path.GetFileNameWithoutExtension(favoriteConfigFilePath);

				// favorites_UserId.conifg
				return favoriteConfigFilePath.Replace(filename, string.Format("{0}_{1}", filename, _userId));
			}
		}
		
		/// <summary>
		/// 사용자 Id가 포함된 Menu 파일의 경로를 구합니다.
		/// </summary>
		private string LocalMenuConfigNameInUserId
		{
			get
			{
				string menuConfigFilePath = LoaderSetting.MenuConfigFilePath;
				string filename = Path.GetFileNameWithoutExtension(menuConfigFilePath);

				// menus_UserId.conifg
				return menuConfigFilePath.Replace(filename, string.Format("{0}_{1}", filename, _userId));
			}
		}

		/// <summary>
		/// Menu 파일의 경로를 구합니다.
		/// </summary>
		private string LocalMenuConfigName
		{
			get
			{
				return LoaderSetting.MenuConfigFilePath;
			}
		}
		
		private bool DebugMode
		{
			get
			{
				return LoaderWrapper.Instance.DebugMode;
			}
		}
		#endregion

		#region Private methods

		private UltraTreeNode MenuViewToTreeNode(Dictionary<string, MenuView> menus)
		{
			UltraTreeNode menuRootNode = new UltraTreeNode();
			UltraTree xTreeForNodeSearch = new UltraTree();		// Node 검색을 위한 임시 Tree
			// Root의 Seq에 따라서 배치를 하기 위해 임시로 저장한다.
			List<UltraTreeNode> rootNodes = new List<UltraTreeNode>();
			UltraTreeNode newRootNodes = new UltraTreeNode();

			// 메뉴 파일이 없을 경우 빈 Instance만 Return 한다.
			if (menus == null)
				return menuRootNode;

			xTreeForNodeSearch.Nodes.Add(menuRootNode);

			foreach (string key in menus.Keys)
			{
				MenuView menuView = menus[key];
				string parentNodeKey = menuView.ParentMenuId;
				UltraTreeNode parentNode =  xTreeForNodeSearch.GetNodeByKey(parentNodeKey);
				UltraTreeNode currentNode = new UltraTreeNode(menuView.MenuId, menuView.MenuName);
				UltraToolTipInfo menuToolTip = new UltraToolTipInfo();

				currentNode.ExpandAll(ExpandAllType.OnlyNodesWithChildren);
				currentNode.Tag = menuView;

				// TODO: Visible 속성이 없어서 일단 IsEnabled 속성으로 Visible을 체크했다
				// by KIMKIWON\xyz37 in 2008년 1월 21일 월요일 오후 1:49
				if (menus[key].IsEnabled == false)
					currentNode.Visible = false;

				if (parentNode != null)
					// Node가 있다면 부모 노드에 추가
					parentNode.Nodes.Add(currentNode);
				else if (menuView.IsRoot == true)
				{
					// Node가 없다면 Root Node에 추가
					menuRootNode.Nodes.Add(currentNode);
					rootNodes.Add(currentNode);
				}
			}

			// Tree 구성이 완료된 뒤 root의 Seq에 따라서 재 배열 한다.
			rootNodes.Sort(delegate(UltraTreeNode node1, UltraTreeNode node2)
			{
				int result = -1;
				int nodeKey1 = (node1.Tag as MenuView).Seq;
				int nodeKey2 = (node2.Tag as MenuView).Seq;

				if (nodeKey1 > nodeKey2)
					result = 1;
				else if (nodeKey1 == nodeKey2)
					result = 0;

				return result;
			});

			// 재 배열한 Root로 구성한다.
			foreach (UltraTreeNode rootNode in rootNodes)
			{
				UltraTreeNode newRootNode = rootNode.Clone() as UltraTreeNode;

				newRootNode.ExpandAll(ExpandAllType.OnlyNodesWithChildren);
				newRootNodes.Nodes.Add(newRootNode);
			}
			
			xTreeForNodeSearch.Nodes.Clear();

			return newRootNodes;
		}
		
		private Dictionary<string, MenuView> LoadMenuFromFile(string filename)
		{
			MenuConfiguration menuConfig = new MenuConfiguration(filename);
			// 개발자 세팅이 되어 있거나 즐겨찾기일 경우 파일에서 Load
			Dictionary<string, MenuView> menus = menuConfig.GetMenuConfig();

			return menus;
		}

#if USE_SECURITY_BLOCK
		/// <summary>
		/// Database에서 MenuView를 Load 합니다.
		/// </summary>
		/// <returns></returns>
		private Dictionary<string, MenuView> GetMenuViewFromDb()
		{
			Dictionary<string, MenuView> menus = null;

			try
			{
				iDASiT.AppFoundation.Security.MenuViewProxy menuViewProxy = new iDASiT.AppFoundation.Security.MenuViewProxy();
				List<iDASiT.TrustCore.Security.MenuView> menuViews = menuViewProxy.GetMenuViewsByUser(_userId);

				if (menuViews == null)
				{
					using (MessageSettings mSettings = new MessageSettings())
					{
						MessageBox.Show(mSettings["CouldNotReceiveMenuView"], GetType().Name, MessageBoxButtons.OK, MessageBoxIcon.Error);

						Application.Exit();
					}
				}

				menus = new Dictionary<string, MenuView>();

				foreach (iDASiT.TrustCore.Security.MenuView tcMenuView in menuViews)
				{
					MenuView menuView = MenuView.ConvertTrustCoreMenuViewToMenuView(tcMenuView);

					menus.Add(menuView.MenuId, menuView);
				}

				// 저장된 메뉴를 Root의 Seq로 변경한다.
			}
			catch (Exception ex)
			{
				throw new InvalidProgramException("메뉴를 로드 하지 못했습니다.", ex);
			}

			return menus;
		}
#endif

		/// <summary>
		/// Local menu 파일에서 MenuId를 Key로 하는 MenuView Dcitionary를 Load 합니다.(Debug mode에서 사용)
		/// </summary>
		/// <param name="isDebugging">디버그 모드이면 Menus.config를 아닐 경우는 Menus_UserId.config를 로드 한다.</param>
		/// <returns></returns>
		private Dictionary<string, MenuView> GetMenuViewFromFile(bool isDebugging)
		{
			string menuFilename = isDebugging == true ? LocalMenuConfigName : LocalMenuConfigNameInUserId;

			return LoadMenuFromFile(menuFilename);
		}
		#endregion

		#region Public methods		
#if USE_SECURITY_BLOCK
		/// <summary>
		/// Database에서 MenuView를 UltraTreeNode로 Load합니다
		/// </summary>
		/// <returns></returns>
		public UltraTreeNode LoadMenusFromDb()
		{
			Dictionary<string, MenuView> menus = GetMenuViewFromDb();

			return MenuViewToTreeNode(menus);
		}
#endif

		/// <summary>
		/// Local menu 파일에서 MenuView를 Load 하여 UltraTreeNode로 Load합니다.(Debug mode에서 사용)
		/// </summary>
		/// <param name="isDebugging">디버그 모드이면 Menus.config를 아닐 경우는 Menus_UserId.config를 로드 한다.</param>
		/// <returns></returns>
		public UltraTreeNode LoadMenuNodeFromFile(bool isDebugging)
		{
			Dictionary<string, MenuView> menus = GetMenuViewFromFile(isDebugging);

			if (isDebugging == true && menus == null)
			{
				using (MessageSettings mSettings = new MessageSettings())
				{
					// 즐겨찾기 파일은 없으면 생성하므로 메시지 출력을 하지 않는다.
					MessageBox.Show(mSettings["Off_LineButMenuFileNotFound"],
						GetType().Name,
						MessageBoxButtons.OK,
						MessageBoxIcon.Warning);
				}

				Application.Exit();
			}
			
			return MenuViewToTreeNode(menus);
		}

		/// <summary>
		/// Local 즐겨찾기 파일에서 MenuView를 Load 합니다.
		/// </summary>
		/// <param name="isDebugging">디버깅 여부에 따라서 Local menu와 비교할지 Db menu와 비교할지 결정한다.</param>
		/// <returns></returns>
		public UltraTreeNode LoadFavoriteMenusFromLocalFile(bool isDebugging)
		{
			string favoriteFilename = LocalFavoriteConfigNameInUserId;
			Dictionary<string, MenuView> favoritesMenus = LoadMenuFromFile(favoriteFilename);

			// 즐겨찾기는 없는 경우 기본 파일로 저장한다.
			if (favoritesMenus == null)
			{
				SaveFavoriteMenusToLocalFile(new UltraTree());
				favoritesMenus = new Dictionary<string, MenuView>();
			}

			Dictionary<string, MenuView> readingMenus = null;

			// 실제 Load된 Menu와 비교해서 권한 부분을 Update 시킨다.
			// isDebuging = true면 Menu.config를 false를 Menu_UserId.config를 로드 한다.
			readingMenus = GetMenuViewFromFile(isDebugging);
		
#if USE_SECURITY_BLOCK			
			if (isDebugging == true)
				readingMenus = GetMenuViewFromFile(isDebugging);
			else
				readingMenus = GetMenuViewFromDb();
#endif

			if (readingMenus != null)
			{
				foreach (MenuView item in favoritesMenus.Values)
				{
					if (readingMenus.ContainsKey(item.MenuId) == true)
					{
						MenuView originalMenuView = readingMenus[item.MenuId];

						item.Arguments = originalMenuView.Arguments;
						item.MenuName = originalMenuView.MenuName;
						item.MenuDesc = originalMenuView.MenuDesc;
						item.FormDesc = originalMenuView.FormDesc;
						item.FormPath = originalMenuView.FormPath;
						item.CanCreate = originalMenuView.CanCreate;
						item.CanRead = originalMenuView.CanRead;
						item.CanUpdate = originalMenuView.CanUpdate;
						item.CanDelete = originalMenuView.CanDelete;
						item.CanExecute = originalMenuView.CanExecute;
						item.CanUndo = originalMenuView.CanUndo;
					}
				}
			}

			return MenuViewToTreeNode(favoritesMenus);
		}

		/// <summary>
		/// MenuView를 Local 즐겨찾기 파일에 Save 합니다.
		/// </summary>
		/// <param name="xTreeFavorites">즐겨찾기 Tree</param>
		public bool SaveFavoriteMenusToLocalFile(UltraTree xTreeFavorites)
		{
			// TODO: Loader => TreeViewManager로 변경
			// by KIMKIWON\xyz37 in 2007년 12월 5일 수요일 오후 1:42
			Loader loader = new Loader();
			Dictionary<string, MenuView> favoritesMenu = loader.RescanFavorites(xTreeFavorites);
			MenuConfiguration menuConfig = new MenuConfiguration(LocalFavoriteConfigNameInUserId);
			string defaultMenuId = string.Empty;
			
			if (xTreeFavorites != null)
			{
				MenuView defaultMenuView = xTreeFavorites.Tag as MenuView;

				if (defaultMenuView != null)
					defaultMenuId = defaultMenuView.MenuId;
			}

			return menuConfig.WriteMenuConfig(favoritesMenu, defaultMenuId);
		}

		/// <summary>
		/// 기본 메뉴로 등록 합니다.
		/// </summary>
		/// <param name="defaultMenuId">기본 menuId</param>
		public bool SetDefaultMenu(string defaultMenuId)
		{
			string filename = LocalFavoriteConfigNameInUserId;
			MenuConfiguration menuConfig = new MenuConfiguration(filename);
			
			return menuConfig.SetDefaultMenuId(defaultMenuId);
		}

		/// <summary>
		/// 기본 메뉴의 MenuView를 구합니다.
		/// </summary>
		/// <returns></returns>
		public MenuView GetDefaultMenuView()
		{
			string filename = LocalFavoriteConfigNameInUserId;
			MenuConfiguration menuConfig = new MenuConfiguration(filename);
			string menuId = menuConfig.GetDefaultMenuId();
			MenuView defaultMenuView = null;
			Dictionary<string, MenuView> menus = LoadMenuFromFile(LocalFavoriteConfigNameInUserId);

			if (menus.ContainsKey(menuId) == true)
				defaultMenuView = menus[menuId];            	

			return defaultMenuView;
		}
		#endregion
	}
}
