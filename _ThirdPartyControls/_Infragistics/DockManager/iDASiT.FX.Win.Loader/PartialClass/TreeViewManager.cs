﻿//#define APPDOMAIN
//#define OPTIMIZE

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using iDASiT.FX.Win.Configuration;
using iDASiT.FX.Win.Controls;
using iDASiT.FX.Win.Controls.UltraTreeHelper;
using Infragistics.Win;
using Infragistics.Win.IGControls;
using Infragistics.Win.UltraWinDock;
using Infragistics.Win.UltraWinTabbedMdi;
using Infragistics.Win.UltraWinTabs;
using Infragistics.Win.UltraWinTree;
using System.Data;
using System.Net;
using iDASiT.FX.Win.Utility;

namespace iDASiT.FX.Win.Loader
{
	public partial class Loader
	{
		private const string ROOT_KEY_PREFIX = "RT";
		private const string FOLDER_KEY_PREFIX = "FD";

		private UltraTreeNode _rootNode;
		private UltraTree _xTreeFavorites;
		private UltraTree _xTreeSearchResult;
		private DropHightLightDrawFilter _dropHighLightDrawFilter;
#if APPDOMAIN
		private Dictionary<string, string> _assemblyFullName;
#endif
		private bool DebugMode
		{
			get
			{
				return ConfigWrapper.Loader.DebugMode;
			}
		}

		#region InitMenu setting with Tree
		private void SetRootNode(UltraTree xTreeTarget, UltraTreeNode rootNode)
		{
			foreach (UltraTreeNode systemNode in rootNode.Nodes)
			{
				UltraTreeNode targetNode = systemNode.Clone() as UltraTreeNode;

				targetNode.Nodes.Clear();
				targetNode.ExpandAll(ExpandAllType.Always);
				xTreeTarget.Nodes.Add(targetNode);
			}
		}

		private void InitMenuTree()
		{
			xDockManager.ImageList = imlPanes;

			// 기본 DockAreaPane을 생성한다.
			DockAreaPane dapMenuLeft = new DockAreaPane(DockedLocation.DockedLeft, DockableGroupPaneKeys.LeftAreaPaneKey.ToString());
			bool exists = false;

			foreach (DockAreaPane dap in xDockManager.DockAreas)
			{
				if (dap.Key == dapMenuLeft.Key)
				{
					exists = true;

					break;
				}
			}

			if (exists == false)
				xDockManager.DockAreas.Add(dapMenuLeft);

			DockableGroupPane dgpTopMenu = GetGroupPane(DockableGroupPaneKeys.LeftTopGroupPane);

			// menu를 구한다.
			MenuManager menuManager = new MenuManager(UserId);

			do
			{
				_rootNode = menuManager.LoadMenuNodeFromFile(DebugMode);

				if (_rootNode.Nodes.Count == 0)
				{
					if (MessageBox.Show("메뉴 구성중입니다.\r\n다시 시도하시겠습니까?", 
                            Text, 
                            MessageBoxButtons.YesNo, 
                            MessageBoxIcon.Question) == DialogResult.No)
						break;
				}

				Thread.Sleep(10);
			} while (_rootNode.Nodes.Count == 0);

#if USE_SECURITY_BLOCK
			if (DebugMode == true)
				_rootNode = menuManager.LoadMenuNodeFromFile(DebugMode);
			else
			{
				do
				{
					_rootNode = menuManager.LoadMenusFromDb();

					if (_rootNode.Nodes.Count == 0)
					{
						if (MessageBox.Show("메뉴 구성중입니다.\r\n다시 시도하시겠습니까?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
							break;
					}
				} while (_rootNode.Nodes.Count == 0);
			}
#endif

			int menuPaneCount = 0;

			// Root의 아래 node 만큼 Loop를 돌면서 ControlPane을 생성한다.
			foreach (UltraTreeNode systemNode in _rootNode.Nodes)
			{
				UltraTree xTree = new UltraTree();
				// NOTICE: 첫번째 image는 Favorites, 두번째 image는 Search Result 에 할당, 이후는 System의 첫 1글자로 구별한다.
				// by KIMKIWON\xyz37 in 2007년 9월 20일 목요일 오전 9:18
				DockableControlPane dcpMenu = new DockableControlPane(Enum.GetName(typeof(DockablePaneKeys), ++menuPaneCount),
												  systemNode.Text,
												  xTree);

				dcpMenu.ToolTipTab = string.Format("{0}(Ctrl+{1})", systemNode.Text, menuPaneCount);

				InitTreeAppearance(xTree, false);
				// Dragable EventHandler를 할당한다.
				InitDragStartAction(xTree);

				// 생성된 TreeView에 Node를 추가한다.
				xTree.Nodes.Add(systemNode);

				// 노드 이미지를 변경한다.
				//RecursiveNode(systemNode, SetMenuImageAction);
				SetMenuImageAction(systemNode);

				string paneKey = systemNode.Text.Substring(0, 1);

				dcpMenu.Settings.TabAppearance.Image = imlPanes.Images[paneKey];
				dcpMenu.Settings.AllowClose = DefaultableBoolean.False;

				// TabGroupPannel에 DockableControlPane을 추가한다.
				dgpTopMenu.Panes.Add(dcpMenu);
			}

			dapMenuLeft.Panes.Add(dgpTopMenu);
			dapMenuLeft.Pin();
			dapMenuLeft.Size = new Size(200, 748);

			xDockManager.DefaultGroupSettings.TabSizing = TabSizing.SizeToFit;
			xDockManager.DefaultGroupSettings.TabStyle = TabStyle.PropertyPageFlat;
			xDockManager.DefaultGroupSettings.PinButtonBehavior = PinButtonBehavior.PinTabGroup;
			xDockManager.LayoutStyle = DockAreaLayoutStyle.FillContainer;
			// VS2005 style로 변경한 뒤 Drag Indicator가 나오게 한다.
			xDockManager.WindowStyle = WindowStyle.VisualStudio2005;
			xDockManager.DragWindowStyle = DragWindowStyle.LayeredWindowWithIndicators;

			//xDockManager.DefaultPaneSettings.BorderStylePane = UIElementBorderStyle.Rounded4;
			// Hide, AutoHide 메뉴등을 안나오게 한다.(Float 메뉴만 나온다)
			xDockManager.ShowDisabledButtons = false;

			// Float 메뉴를 구성한다.
			//dapMenuLeft.Float(false, new Rectangle(5, 50, 230, 700));
		}
		#endregion

		#region Menu Tree ContextMenu
		private void MenuClickAction(UltraTreeNode targetNode, params object[] param)
		{
			#region MenuClick시 호출 속도를 높이기 위해 테스트
			/*
			_ToolBarFormAutoHideControl.SuspendLayout();
			_ToolBarFormAutoHideControl.Hide();
			_ToolBarFormAutoHideControl.Update();
			_ToolBarFormAutoHideControl.ResumeLayout();
			_DockableTreeForm_Toolbars_Dock_Area_Left.BeginUpdate();
			_DockableTreeForm_Toolbars_Dock_Area_Left.Hide();
			_DockableTreeForm_Toolbars_Dock_Area_Left.Update();
			_DockableTreeForm_Toolbars_Dock_Area_Left.EndUpdate();
			*/
			#endregion

			if (targetNode == null || IsRootNode(targetNode) == true || IsFolder(targetNode) == true)
				return;

			// 폼이 있을 경우 parameter 처리를 한다.
			// TODO: 해당 폼이 있을 경우 Form을 Active 한 뒤 parameter를 처리 해야 하나 현재는 생성자로서만 parameter를 전달하므로 
			// Form을 강제로 종료 한 뒤 다시 실행한다.
			// by KIMKIWON\xyz37 in 2008년 1월 9일 수요일 오후 2:11
			Form foundForm = FindTabByKey(targetNode.Key);

			if (foundForm != null)
			{
				if (param.Length > 0)
					foundForm.Close();
				else
				{
					foundForm.Activate();

					return;
				}
			}

			#region Before logic
			/*
			foreach (Form form in MdiChildren)
			{
				if ((form.Tag as string) == targetNode.Key)
				{
					form.Activate();

					return;
				}
			}
			*/
			#endregion

			// ProcessBar start
			ProcessbarStart();

			MenuView menuView = targetNode.Tag as MenuView;

			if (menuView != null)
			{
#if OPTIMIZE
			#region Performance Test
			DateTime start = DateTime.Now;
			LogManager.Write("Optimize.log", string.Format("{2}MenuClickAction Start({0}) : {1}{2}", menuView.FormId, start.ToLongTimeString(), Environment.NewLine));
			#endregion
#endif
#if !APPDOMAIN
				// 로드할 어셈블리의 경로는 menus.config 파일 경로 + Win 으로 지정한다.
				string assemblyPath = GetLoadMenuAssemblyPath(menuView);

				if (DebugMode == true && File.Exists(assemblyPath) == false)
				{
					// ProcessBar stop
					ProcessbarStop();

					MessageBox.Show(string.Format("개발자 Debugging Mode 입니다.{0}{0}{1} 파일이 없습니다.{0}다시 확인해 주시기 바랍니다.",
										Environment.NewLine,
										assemblyPath.Replace("/", @"\")), Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);

					// TODO: Message Resource 시킨다.
					// by KIMKIWON\xyz37 in 2008년 9월 10일 수요일 오후 10:33
					//using (MessageSettings mSettings = new MessageSettings())
					//{
					//	if (MessageBox.Show(mSettings["FileNotFoundAndUpdate"], Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					//		AppUpdateClick();
					//}

					return;
				}

#if OPTIMIZE
				#region Performance Test
				DateTime now1 = DateTime.Now;
				LogManager.Write("Optimize.log", "MenuClickAction Before EmulateShadowCopyFilePath : " + string.Format("{0:HHmmss}", (now1 - start)) + Environment.NewLine);
				#endregion
#endif

				string targetFile = EmulateShadowCopyFilePath(assemblyPath);
#if OPTIMIZE
				#region Performance Test
				DateTime now2 = DateTime.Now;
				LogManager.Write("Optimize.log", "MenuClickAction After EmulateShadowCopyFilePath : " + string.Format("{0:HHmmss}", (now2 - now1)) + Environment.NewLine);
				#endregion
#endif
				Assembly assembly = Assembly.LoadFile(targetFile);
				string fullName = string.Empty;

				try
				{
					foreach (Type type in assembly.GetTypes())
					{
						if (type.Name == assembly.GetName().Name)
						{
							fullName = type.FullName;

							break;
						}
					}
				}
				catch (ReflectionTypeLoadException ex)
				{
					string exMsg = string.Format("요청한 형식 중 하나 이상을 로드할 수 없습니다.{0}{0}", Environment.NewLine);

					foreach (Exception LEx in ex.LoaderExceptions)
						exMsg += string.Format("{0}{1}", LEx.Message, Environment.NewLine);

					DoException(exMsg, ex);

					// ProcessBar stop
					ProcessbarStop();

					return;
				}

				if (fullName == string.Empty)
				{
					const string msg = "메뉴 어셈블리 파일의 네임스페이스가 틀리거나 정상적으로 생성된 파일이 아닙니다";

					DoException(msg, new InvalidDataException(msg));
					// ProcessBar stop
					ProcessbarStop();

					return;
				}

				Type typeToLoad = assembly.GetType(fullName);

				if (typeToLoad == null)
				{
					// TODO: 메세지 변경
					// by KIMKIWON\xyz37 in 2007년 9월 20일 목요일 오전 11:09
					MessageBox.Show("창을 여는 도중 에러가 발생했습니다.\r\n업데이트 하십시오", "어셈블리 로드 오류", MessageBoxButtons.OK, MessageBoxIcon.Error);

					// ProcessBar stop
					ProcessbarStop();

					return;
				}
#else
				if (DebugMode == true)				
				{
					// 로드할 어셈블리의 경로는 menus.config 파일 경로 + Win 으로 지정한다.
					string assemblyPath = GetLoadMenuAssemblyPath(menuView);

					if (File.Exists(assemblyPath) == false)
					{
						// ProcessBar stop
						ProcessbarStop();

						using (MessageSettings mSettings = new MessageSettings())
						{
							if (MessageBox.Show(mSettings["FileNotFoundAndUpdate"], Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								AppUpdateClick();
						}
	                    
						return;
					}
				}

				AppDomain newDomain = null;
#endif
				try
				{
#if APPDOMAIN
					#region AppDoman
					AppDomainSetup newDomainInfo = new AppDomainSetup();
					string shadowFolderName = string.Format(@"Win\{0}", menuView.FormPath);
					string remoteBase = "http://192.168.25.28:8088/iDASiT.FX.Win.Deploy";
					string applicationBase = FrameworkSetup.Instance.InstallFolder;
					string fullName = GetAssemblyFullName(menuView);

					newDomainInfo.ApplicationName = menuView.FormId;
					newDomainInfo.ApplicationBase = DebugMode == false ? remoteBase : applicationBase;
					// NOTICE: WorkingFolder이 \\을 포함하면 GetDirectoryName으로 해야 한다.
					// by KIMKIWON\xyz37 in 2008년 9월 9일 화요일 오전 11:53
					newDomainInfo.PrivateBinPath = string.Format(@"{0};{1}", 
                                                       shadowFolderName,
													   Path.GetFileName(FrameworkSetup.Instance.WorkingFolder));			// 폴더명만
					newDomainInfo.CachePath = string.Format(@"{0}\iDASiT Cache\{1}\", 
                                                  Path.GetTempPath(), 
                                                  shadowFolderName);															// \을 포함한 full path
					newDomainInfo.ShadowCopyFiles = "true";
					newDomainInfo.ShadowCopyDirectories = string.Format(@"{0}\{1}\", applicationBase, shadowFolderName);		// \을 포함한 full path;

					newDomain = AppDomain.CreateDomain(menuView.FormId, AppDomain.CurrentDomain.Evidence, newDomainInfo);

					BindingFlags bindingFlags = BindingFlags.CreateInstance | BindingFlags.Instance | BindingFlags.Public;
					object menuFormInstance = newDomain.CreateInstance(menuView.FormId,
												fullName,
												true,
												bindingFlags,
												null,
												param,
												null,
												null,
												null);
					// NOTICE: AppDomain으로 생성할 경우 ShadowCopy를 하여 Update까지 가능하나
                    // menuFormInstance type이 TransparentProxy 형태로 생성되어 MdiParent에 할당을 할 수 없고
					// 내부에서 PlantId를 가져갈 수 없어서
					// ShadowCopy를 Emulation 한다.
                    // by KIMKIWON\xyz37 in 2008년 9월 10일 수요일 오후 5:36
					#endregion
#endif

#if !APPDOMAIN
#if OPTIMIZE
					#region Performance Test
					DateTime now3 = DateTime.Now;
					LogManager.Write("Optimize.log", "MenuClickAction After GetType : " + string.Format("{0:HHmmss}", (now3 - now2)) + Environment.NewLine);
					#endregion
#endif
					// FormDesc를 Menu를 호출하는 Parameter로 사용한다.
					// 호출하는 Form이 parameter 생성자가 없을 경우 기본 생성자를 호출 한다.
					object menuFormInstance = null;

					try
					{
						// parameter 생성자가 있을 경우
						if (param.Length == 0)
							menuFormInstance = Activator.CreateInstance(typeToLoad, menuView.Arguments);
						else
							menuFormInstance = Activator.CreateInstance(typeToLoad, param);
					}
					catch (MissingMethodException)
					{
						// 기본 생성자의 경우
						menuFormInstance = Activator.CreateInstance(typeToLoad);
					}
					catch (Exception ex)
					{
						if (ex.InnerException != null)
							throw new Exception(ex.InnerException.Message, ex.InnerException);
						else
							throw new Exception(ex.Message, ex);
					}
#endif
					if (menuFormInstance != null)
					{
						BaseForm menuForm = menuFormInstance as BaseForm;

						// NOTICE: POP-UP창도 고려해서 작성해야 한다(팝업인지 여부)
						// by KIMKIWON\xyz37 in 2007년 9월 20일 목요일 오전 10:53
						if (menuForm != null)
						{
							menuForm.Text = menuView.MenuName;
							menuForm.Tag = targetNode.Key;			// Mdi parent에서 키가 존재하는지 체크하여 중복으로 열지 않도록 한다.
#if !APPDOMAIN
							// NOTICE: MenuView와 MenuPath를 BaseForm에서 Loader로 승격 시켜서 MenuView.FormDesc에 MenuPath를 지정한다.
							// by KIMKIWON\xyz37 in 2008년 4월 16일 수요일 오전 10:18
							menuView.FormDesc = string.Format("{0}({1})", targetNode.FullPath.Replace("\\", " > "), assembly.FullName.Split(',')[1].Replace(" Version=", string.Empty));
							menuForm.MdiParent = this;

							MdiTab activatedTab = xTabbedMdiManager.TabFromForm(menuForm);

							// 현재 Form의 ToolTip을 설정한다.
							activatedTab.ToolTip = menuView.FormDesc;
							activatedTab.Tag = menuView;
#if OPTIMIZE
							#region Performance Test
							DateTime now4 = DateTime.Now;
							LogManager.Write("Optimize.log", "MenuClickAction Before Show : " + string.Format("{0:HHmmss}", (now4 - now3)) + Environment.NewLine);
							#endregion
#endif

							// Form이 Activation 되면 해당 권한 및 MenuPath가 설정된다
							// 해당 폼을 일반 폼으로 Load 한다.
							menuForm.Show();
#if OPTIMIZE
							#region Performance Test
							DateTime now5 = DateTime.Now;
							LogManager.Write("Optimize.log", "MenuClickAction After Show : " + string.Format("{0:HHmmss}", (now5 - now4)) + Environment.NewLine);
							#endregion
#endif

							if (_mdiTabs == null)
								_mdiTabs = new List<MdiTab>();

							_mdiTabs.Add(ActiveTab);
#else
							// TODO: Form closing 시 Domain Unload 실행한다.
							// by KIMKIWON\xyz37 in 2008년 9월 9일 화요일 오전 11:33
#endif
						}
					}
				}
				catch (Exception ex)
				{
#if APPDOMAIN
				if (newDomain != null)
						AppDomain.Unload(newDomain);	
#endif
					DoException("메뉴 폼을 초기화 할 수 없습니다.", ex);
				}
				finally
				{
					// ProcessBar stop
					ProcessbarStop();
				}
			}
			else
			{
				using (MessageSettings mSettings = new MessageSettings())
				{
					MessageBox.Show(mSettings["InvalidMenuConfiguration"], "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
					StatusbarMessage = mSettings["InvalidMenuConfiguration"];
				}

				// ProcessBar stop
				ProcessbarStop();
			}
		}

		#region Emulate Shadow Copy
		/// <summary>
		/// ShadowCopy를 Emulation한 Assembly의 FilePath를 구한다.
		/// <remarks>Login 할 때 주기적으로 Cache Folder를 삭제 한다.</remarks>
		/// </summary>
		/// <param name="assemblyPath">원본의 assembly path</param>
		/// <returns>복사본의 local 경로를 return 합니다.</returns>
		private string EmulateShadowCopyFilePath(string assemblyPath)
		{
			DateTime creationTime = DateTime.MinValue;
			DateTime lastWriteTime = DateTime.MinValue;
			string assemblyName = Path.GetFileName(assemblyPath);

			if (DebugMode == false)
				// 원격지 파일 정보를 구한다.
				GetRemoteFileInfo(assemblyName, out creationTime, out lastWriteTime);
			else
			{
				FileInfo fileInfo = new FileInfo(assemblyPath);

				creationTime = fileInfo.CreationTime;
				lastWriteTime = fileInfo.LastWriteTime;
			}

			string cachFolder = string.Format(@"{0}\{1}\{2:X}",
									FrameworkSetup.Instance.CacheFolder,
									Path.GetFileNameWithoutExtension(assemblyPath),
				// long형을 갖는 LocalTime이 같은 시간이라고 해도 미묘한 차이를 발생하여 다른 Key를 생성하므로 다시 변환을 한다.
									DateTime.ParseExact(lastWriteTime.ToString("yyyy-MM-dd HH:mm:ss:fff"),
										"yyyy-MM-dd HH:mm:ss:fff",
										null).ToFileTime());
			string targetFile = string.Format(@"{0}\{1}", cachFolder, Path.GetFileName(assemblyPath));

			if (Directory.Exists(cachFolder) == false)
				Directory.CreateDirectory(cachFolder);

			if (File.Exists(targetFile) == false)
			{
				if (DebugMode == false)
				{
					bool result = GetFileFromServer(assemblyPath, targetFile, creationTime, lastWriteTime);

					if (result == false)
					{
						string msg = string.Format("서버에서 {0} 파일을 다운로드 할 수 없습니다.\r\n관리자에게 문의하십시오.", assemblyName);

						DoException(msg, new InvalidDataException(msg));
					}
				}
				else
					File.Copy(assemblyPath, targetFile);
			}

			return targetFile;
		}

		/// <summary>
		/// Remote의 UpdateFile.xml에서 client의 assembly 이름으로 정보를 구합니다.
		/// </summary>
		/// <param name="remoteFileNameOnly">client assembly 이름(M01F0020.dll)</param>
		/// <param name="creationTime">파일 생성 일시</param>
		/// <param name="lastWriteTime">파일 수정 일시</param>
		private void GetRemoteFileInfo(string remoteFileNameOnly, out DateTime creationTime, out DateTime lastWriteTime)
		{
			DataSet dsUpdateFile = new DataSet();

			creationTime = DateTime.Now;
			lastWriteTime = DateTime.Now;

			// TODO: Update config에서 정보를 가져오도록 수정한다.
			// by KIMKIWON\xyz37 in 2008년 9월 10일 수요일 오후 9:43
			// TODO: CopyOnlyFile를 Loader setting에서 변경할 수 있도록 한다.
			// UpdaterCore에서 참조하여 Skip 파일의 개수를 하는 부분?
			// FileWatcher의 설정 파일이 이름이 같도록 한다.
			// by KIMKIWON\xyz37 in 2008년 9월 26일 금요일 오후 6:03
			dsUpdateFile.ReadXml(string.Format("{0}:{1}/{2}/CopyOnlyFile.xml", 
                                     ConfigWrapper.UpdaterCore.Default.Uri, 
                                     ConfigWrapper.UpdaterCore.Default.Port, 
                                     ConfigWrapper.UpdaterCore.DeployFolder));

			if (dsUpdateFile != null && dsUpdateFile.Tables.Count > 0)
			{
				DataRow[] rows = dsUpdateFile.Tables[0].Select(string.Format("Name = '{0}'", remoteFileNameOnly));

				if (rows != null && rows.Length == 1)
				{
					creationTime = DateTime.ParseExact(rows[0]["CreationTime"].ToString(),
																			  "yyyy-MM-dd HH:mm:ss:fff",
																			  null);
					lastWriteTime = DateTime.ParseExact(rows[0]["LastWriteTime"].ToString(),
																			   "yyyy-MM-dd HH:mm:ss:fff",
																			   null);
				}
			}
		}

		private bool GetFileFromServer(string remotePath, string targetFile, DateTime creationTime, DateTime lastWriteTime)
		{
			const int OFFSET = 0;
			int bufferSize = ConfigWrapper.UpdaterCore.BufferSize;
			WebRequest request = WebRequest.Create(remotePath);
			WebResponse response = null;
			Stream stream = null;

			try
			{
				response = request.GetResponse();
				stream = response.GetResponseStream();

				using (FileStream fs = new FileStream(targetFile, FileMode.Create))
				{
					byte[] buffer = new byte[bufferSize];
					int recvLength = 0;

					while ((recvLength = stream.Read(buffer, OFFSET, bufferSize)) > 0)
						fs.Write(buffer, 0, recvLength);

					fs.Close();
				}
			}
			catch
			{
				return false;
			}
			finally
			{
				if (stream != null)
				{
					stream.Close();

					File.SetCreationTime(targetFile, creationTime);
					File.SetLastWriteTime(targetFile, lastWriteTime);
				}

				if (response != null)
					response.Close();
			}

			return true;
		}
		#endregion

#if APPDOMAIN
		private string GetAssemblyFullName(MenuView menuView)
		{
			if (_assemblyFullName == null)
			{
				_assemblyFullName = new Dictionary<string, string>();

				_assemblyFullName.Add("M", "iDASiT.Win.Mes");
				_assemblyFullName.Add("F", "iDASiT.Win.Fgms");
				_assemblyFullName.Add("S", "iDASiT.Win.Setup");
				_assemblyFullName.Add("R", "iDASiT.Win.Report");
				_assemblyFullName.Add("P", "iDASiT.Win.PI");
			}

			return string.Format(@"{0}.{1}", _assemblyFullName[menuView.FormPath], menuView.FormId);
		}
#endif

		private Form FindTabByKey(string key)
		{
			Form foundForm = null;

			foreach (Form form in MdiChildren)
			{
				if ((form.Tag as string) == key)
				{
					foundForm = form;

					break;
				}
			}

			return foundForm;
		}

		private void UnSelectAll()
		{
			UltraTree activeTree = GetActiveTree();

			if (activeTree != null && activeTree.Nodes.Count > 0)
				UnSelectAction(GetActiveTree().Nodes[0].RootNode);
		}

		private void UnSelectAction(UltraTreeNode targetNode)
		{
			List<UltraTreeNode> allNodes = GetAllNodes(targetNode);

			foreach (UltraTreeNode node in allNodes)
				node.Selected = false;
		}

		private void InitMenuContextMenu(UltraTree xTreeTarget, UltraTreeNode nodeAtPoint)
		{
			if (nodeAtPoint == null)
				return;

			bool isDefaultMenu = nodeAtPoint.Key == GetDefaultMenuId();

			using (ContextCaptionSettings ccSettings = new ContextCaptionSettings())
			{
				IGContextMenu contextMenu = new IGContextMenu();
				IGMenuItem mItemAddFavorites = new IGMenuItem(string.Format("{0} [{1}]",
																  ccSettings["AddFavorites"],
																  nodeAtPoint),
																  new EventHandler(OnAddFavoritesMenuItemClick));
				IGMenuItem mItemBar1 = new IGMenuItem("-");
				IGMenuItem mItemSetDefaultMenu = new IGMenuItem(string.Format("{0} {1} [{2}]",
																	ccSettings[isDefaultMenu == true ? "ResetDefaultMenu" : "SetDefaultMenu"],
																	"Alt+Home",
																	nodeAtPoint),
																	new EventHandler(isDefaultMenu == true ? (EventHandler)OnResetDefaultContextMenuItemClick : (EventHandler)OnSetDefaultContextMenuItemClick));

				mItemAddFavorites.Image = imlPanes.Images["favorites"];
				mItemSetDefaultMenu.Image = imlFavoriteContext.Images[isDefaultMenu == true ? "delete_default" : "default"];

				mItemAddFavorites.Tag = xTreeTarget.SelectedNodes;
				mItemSetDefaultMenu.Tag = xTreeTarget.SelectedNodes;

				contextMenu.MenuItems.Clear();
				contextMenu.MenuItems.Add(0, mItemAddFavorites);
				contextMenu.MenuItems.Add(1, mItemBar1);
				contextMenu.MenuItems.Add(2, mItemSetDefaultMenu);

				xTreeTarget.ContextMenu = contextMenu;
			}
		}

		private void OnAddFavoritesMenuItemClick(object sender, EventArgs e)
		{
			IGMenuItem menuItem = sender as IGMenuItem;
			SelectedNodesCollection selectedNodes = menuItem != null ? menuItem.Tag as SelectedNodesCollection : null;

			if (selectedNodes != null)
				AddFavoriteMenu(selectedNodes);
		}
		#endregion

		#region InitTreeAppearance
		private void InitTreeAppearance(UltraTree xTreeTarget, bool isFavoriteTree)
		{
			xTreeTarget.Override.HotTracking = DefaultableBoolean.True;
			// 포커스를 잃어도 선택영역을 감추지 않는다.
			xTreeTarget.HideSelection = false;
			xTreeTarget.Override.SelectionType = SelectType.ExtendedAutoDrag;
			xTreeTarget.AutoDragExpandDelay = 1500;

			xTreeTarget.MouseDown += new MouseEventHandler(OnMouseDown);

			if (isFavoriteTree == false)
				xTreeTarget.KeyDown += new KeyEventHandler(OnKeyDown);
			else
				xTreeTarget.KeyDown += new KeyEventHandler(OnFavoriteTreeKeyDown);

			xTreeTarget.BeforeCollapse += new BeforeNodeChangedEventHandler(OnBeforeCollapse);

			xTreeTarget.MouseEnterElement += new UIElementEventHandler(OnTreeMouseEnterElement);
			xTreeTarget.MouseLeaveElement += new UIElementEventHandler(OnTreeMouseLeaveElement);
		}

		private void OnTreeMouseEnterElement(object sender, UIElementEventArgs e)
		{
			TreeNodeUIElement nodeElement = e.Element as TreeNodeUIElement;
			if (nodeElement != null)
			{
				UltraTreeNode node = nodeElement.Node;
				MenuView menuView = node.Tag as MenuView;

				// 메뉴에 ToolTip을 적용한다.
				if (menuView != null)
					StatusbarMessage = string.Format("{0}({1})",
										   menuView.MenuDesc == string.Empty ? menuView.MenuName : menuView.MenuDesc,
										   menuView.FormId);
			}
		}

		private void OnTreeMouseLeaveElement(object sender, UIElementEventArgs e)
		{
			// TODO: Interface 구현
			// by KIMKIWON\xyz37 in 2007년 12월 4일 화요일 오후 6:59
			//StatusbarMessage = string.Empty;
			//ClearStatusBar();
		}

		private void OnMouseDown(object sender, MouseEventArgs e)
		{
			UltraTree xTree = sender as UltraTree;

			// Double Click일 경우
			if (e.Clicks == 2 && e.Button == MouseButtons.Left)
			{
				if (xTree != null)
				{
					UltraTreeNode nodeAtPoint = xTree.GetNodeFromPoint(e.X, e.Y);

					if (nodeAtPoint != null && IsFolder(nodeAtPoint) == false)
						MenuClickAction(nodeAtPoint);
				}
			}

			if (e.Button == MouseButtons.Right)
			{
				if (xTree != null)
				{
					UltraTreeNode nodeAtPoint = xTree.GetNodeFromPoint(e.X, e.Y);

					if (nodeAtPoint != null)
					{
						// 즐겨찾기에서 호출할 때
						if (xTree == _xTreeFavorites)
							InitFavoriteContextMenu(xTree, nodeAtPoint);
						// 일반 메뉴에서 호출할 때
						else
						{
							if (IsRootNode(nodeAtPoint) == false)
							{
								UnSelectAll();
								xTree.ActiveNode = nodeAtPoint;
								xTree.ActiveNode.Selected = true;

								InitMenuContextMenu(xTree, nodeAtPoint);
							}
							else
								xTree.ContextMenu = null;
						}
					}
					else
						xTree.ContextMenu = null;
				}
			}
		}

		private void OnKeyDown(object sender, KeyEventArgs e)
		{
			UltraTree xTreeTarget = sender as UltraTree;

			if (xTreeTarget == null)
				return;

			UltraTreeNode node = xTreeTarget.ActiveNode;

			if (e.KeyCode == Keys.Enter)
				MenuClickAction(node);

			#region 메뉴는 수정할 수 없다.
			/*
			if (e.Alt == true && e.KeyCode == Keys.Up)
				MoveFirstNode(node);

			if (e.Alt == true && e.KeyCode == Keys.Down)
				MoveLastNode(node);

			if (e.Alt == true && e.KeyCode == Keys.Right)
				AddFolderNode(node);
			*/
			#endregion

			if (e.Alt == true && e.KeyCode == Keys.Home)
				SetDefaultMenu(node);
		}

		private void OnFavoriteTreeKeyDown(object sender, KeyEventArgs e)
		{
			UltraTree xTreeTarget = sender as UltraTree;

			if (xTreeTarget == null)
				return;

			UltraTreeNode node = xTreeTarget.ActiveNode;

			if (e.KeyCode == Keys.Enter)
				MenuClickAction(node);

			if (e.Alt == true && e.KeyCode == Keys.Up)
				MoveFirstNode(node);

			if (e.Alt == true && e.KeyCode == Keys.Down)
				MoveLastNode(node);

			if (e.Alt == true && e.KeyCode == Keys.Right)
				AddFolderNode(node);

			if (e.Control == true && e.KeyCode == Keys.R)
				RenameNode(node);

			if (e.KeyCode == Keys.Delete)
				DeleteNode(node);

			if (e.Alt == true && e.KeyCode == Keys.Home)
				SetDefaultMenu(node);
		}

		private void OnBeforeCollapse(object sender, CancelableNodeEventArgs e)
		{
			UltraTree xTreeSender = sender as UltraTree;

			// NOTICE: 하위 Node가 있는 메뉴를 클릭했을 경우 child form이 열리기 때문에 더블클릭으로 메뉴를 열도록 구현하기 위해서
			// by SUPERMOBILE\xyz37 in 2007년 9월 3일 월요일 오후 12:16
			if (xTreeSender != null)
				if (xTreeSender.HotTrackingNode != null)
					e.Cancel = true;
		}
		#endregion

		private void InitFavoriteTree()
		{
			if (_xTreeFavorites == null)
				_xTreeFavorites = new UltraTree();

			InitTreeAppearance(_xTreeFavorites, true);
			InitFavoritesDragableAction();
			InitDropHIghLightHelper(_xTreeFavorites);

			DockableGroupPane dgpBottomMenu = GetGroupPane(DockableGroupPaneKeys.LeftBottomGroupPane);

			if (dgpBottomMenu != null)
			{
				using (MessageSettings mSettings = new MessageSettings())
				{
					DockableControlPane dcpFavorites = new DockableControlPane(DockablePaneKeys.Favorite.ToString(), mSettings["FavoritesCaption"], _xTreeFavorites);

					dcpFavorites.ToolTipTab = string.Format("{0}({1})", mSettings["FavoritesCaption"], "Ctrl+~");
					dcpFavorites.Settings.TabAppearance.Image = imlPanes.Images["favorites"];
					dcpFavorites.Settings.AllowClose = DefaultableBoolean.False;

					dgpBottomMenu.Panes.Add(dcpFavorites);

					dgpBottomMenu.Pin();
				}
			}

			SetRootNode(_xTreeFavorites, _rootNode);

			if (_rootNode.Nodes.Count > 0)
			{
				LoadFavoritesMenus();
				LoadDefaultMenu();
			}
		}

		private void LoadDefaultMenu()
		{
			using (MessageSettings messageSettings = new MessageSettings())
			{
				StatusbarMessage = messageSettings["LoadDefaultMenu"];

				MenuView defaultMenuVIew = GetDefaultMenuView();

				if (defaultMenuVIew != null)
				{
					UltraTreeNode defaultNode = _xTreeFavorites.GetNodeByKey(defaultMenuVIew.MenuId);

					if (defaultNode != null)
					{
						// NOTICE: System menu를 먼저 호출 뒤 즐겨찾기를 호출할 경우
						// 기본 메뉴는 즐겨찾기에서 구해지므로 System 메뉴의 이미지를 갱신하지 못해서
						// Image 만을 다시 갱신한다.
						// 추후에 DockManager의 구성이 완료되면 즐겨 찾기를 먼저 호출 한 뒤 System menu를 호출 한다.
						// by KIMKIWON\xyz37 in 2008년 6월 16일 월요일 오후 8:43
						SetMenuImageAction(_rootNode);
						MenuClickAction(defaultNode, defaultMenuVIew.Arguments);
					}

					StatusbarMessage = messageSettings["LoadDefaultMenuFinished"];
				}
				else
					StatusbarMessage = string.Empty;
			}
		}

		#region Favorite Tree ContextMenu
		private void InitFavoriteContextMenu(UltraTree xTreeTarget, UltraTreeNode nodeAtPoint)
		{
			if (nodeAtPoint == null)
				return;

			IGContextMenu contextMenu = new IGContextMenu();
			bool isRoot = IsRootNode(nodeAtPoint);
			bool isFolder = IsFolder(nodeAtPoint);
			bool isDefaultMenu = nodeAtPoint.Key == GetDefaultMenuId();

			using (ContextCaptionSettings ccSettings = new ContextCaptionSettings())
			{
				IGMenuItem mItemMoveFirst = new IGMenuItem(string.Format("{0} {1} [{2}]", ccSettings["MoveFirst"], "Alt+Up", nodeAtPoint),
					new EventHandler(OnMoveFirstContextMenuItemClick));
				IGMenuItem mItemMoveLast = new IGMenuItem(string.Format("{0} {1} [{2}]", ccSettings["MoveLast"], "Alt+Down", nodeAtPoint),
					new EventHandler(OnMoveLastContextMenuItemClick));
				IGMenuItem mItemBar1 = new IGMenuItem("-");
				IGMenuItem mItemAddFolder = new IGMenuItem(string.Format("{0} {1} [{2}]", ccSettings["AddFolder"], "Alt+Right", nodeAtPoint),
					new EventHandler(OnAddFolderContextMenuItemClick));
				IGMenuItem mItemRename = new IGMenuItem(string.Format("{0} {1} [{2}]", ccSettings["RenameMenu"], "Ctrl+R", nodeAtPoint),
					new EventHandler(OnRenameContextMenuItemClick));
				IGMenuItem mItemDelete = new IGMenuItem(string.Format("{0} {1} [{2}]", ccSettings["DeleteMenu"], "Delete", nodeAtPoint),
					new EventHandler(OnDeleteContextMenuItemClick));
				IGMenuItem mItemBar2 = new IGMenuItem("-");
				IGMenuItem mItemSetDefaultMenu = new IGMenuItem(string.Format("{0} {1} [{2}]",
																	ccSettings[isDefaultMenu == true ? "ResetDefaultMenu" : "SetDefaultMenu"],
																	"Alt+Home",
																	nodeAtPoint),
																	new EventHandler(isDefaultMenu == true ? (EventHandler)OnResetDefaultContextMenuItemClick : (EventHandler)OnSetDefaultContextMenuItemClick));

				mItemMoveFirst.Image = imlFavoriteContext.Images["first"];
				mItemMoveLast.Image = imlFavoriteContext.Images["last"];
				mItemAddFolder.Image = imlFavoriteContext.Images["folder"];
				mItemRename.Image = imlFavoriteContext.Images["rename"];
				mItemDelete.Image = imlFavoriteContext.Images["delete"];
				mItemSetDefaultMenu.Image = imlFavoriteContext.Images[isDefaultMenu == true ? "delete_default" : "default"];

				mItemMoveFirst.Tag = nodeAtPoint;
				mItemMoveLast.Tag = nodeAtPoint;
				mItemAddFolder.Tag = nodeAtPoint;
				mItemRename.Tag = nodeAtPoint;
				mItemDelete.Tag = nodeAtPoint;
				mItemSetDefaultMenu.Tag = nodeAtPoint;

				contextMenu.MenuItems.Clear();
				// 폴더일 경우 전체 메뉴 표시
				contextMenu.MenuItems.Add(0, mItemMoveFirst);
				contextMenu.MenuItems.Add(1, mItemMoveLast);
				contextMenu.MenuItems.Add(2, mItemBar1);
				contextMenu.MenuItems.Add(3, mItemAddFolder);
				contextMenu.MenuItems.Add(4, mItemRename);
				contextMenu.MenuItems.Add(5, mItemDelete);
				contextMenu.MenuItems.Add(6, mItemBar2);
				contextMenu.MenuItems.Add(7, mItemSetDefaultMenu);
			}

			// 일반 메뉴일 경우 Rename 제거
			if (isFolder == false && isRoot == false)
				contextMenu.MenuItems.RemoveAt(4);
			// Root일 경우 AddFolder, Delete만 추가
			else if (isRoot == true)
			{
				contextMenu.MenuItems.RemoveAt(7);
				contextMenu.MenuItems.RemoveAt(6);
				contextMenu.MenuItems.RemoveAt(4);
				contextMenu.MenuItems.RemoveAt(2);
				contextMenu.MenuItems.RemoveAt(1);
				contextMenu.MenuItems.RemoveAt(0);
			}

			xTreeTarget.ContextMenu = contextMenu;
		}

		private void OnMoveFirstContextMenuItemClick(object sender, EventArgs e)
		{
			IGMenuItem menuItem = sender as IGMenuItem;
			UltraTreeNode node = menuItem != null ? menuItem.Tag as UltraTreeNode : null;

			MoveFirstNode(node);
		}

		private void MoveFirstNode(UltraTreeNode node)
		{
			if (node != null && node.RootNode.Nodes.Count > 0)
				node.Reposition(node.RootNode.Nodes[0], NodePosition.First);
		}

		private void OnMoveLastContextMenuItemClick(object sender, EventArgs e)
		{
			IGMenuItem menuItem = sender as IGMenuItem;
			UltraTreeNode node = menuItem != null ? menuItem.Tag as UltraTreeNode : null;

			MoveLastNode(node);
		}

		private void MoveLastNode(UltraTreeNode node)
		{
			if (node != null && node.RootNode.Nodes.Count > 0)
				node.Reposition(node.RootNode.Nodes[0], NodePosition.Last);
		}

		private void OnAddFolderContextMenuItemClick(object sender, EventArgs e)
		{
			IGMenuItem menuItem = sender as IGMenuItem;
			UltraTreeNode node = menuItem != null ? menuItem.Tag as UltraTreeNode : null;

			AddFolderNode(node);
		}

		private void AddFolderNode(UltraTreeNode node)
		{
			if (node != null)
			{
				using (MessageSettings mSettings = new MessageSettings())
				{
					UltraTreeNode folderNode = new UltraTreeNode(GetFolderKey(), mSettings["NewFolder"]);

					folderNode.Override.NodeAppearance.Image = imlFavoriteContext.Images["folder"];

					node.Nodes.Add(folderNode);
					node.ExpandAll(ExpandAllType.Always);
					folderNode.BeginEdit();
				}
			}
		}

		private void OnRenameContextMenuItemClick(object sender, EventArgs e)
		{
			IGMenuItem menuItem = sender as IGMenuItem;
			UltraTreeNode node = menuItem != null ? menuItem.Tag as UltraTreeNode : null;

			RenameNode(node);
		}

		private void RenameNode(UltraTreeNode node)
		{
			if (node != null)
				node.BeginEdit();
		}

		private void OnDeleteContextMenuItemClick(object sender, EventArgs e)
		{
			IGMenuItem menuItem = sender as IGMenuItem;
			UltraTreeNode node = menuItem != null ? menuItem.Tag as UltraTreeNode : null;

			DeleteNode(node);
		}

		private void OnSetDefaultContextMenuItemClick(object sender, EventArgs e)
		{
			IGMenuItem menuItem = sender as IGMenuItem;
			UltraTreeNode node = null;

			if (menuItem != null)
			{
				if (menuItem.Tag is UltraTreeNode)
					node = menuItem.Tag as UltraTreeNode;
				else if (menuItem.Tag is SelectedNodesCollection && (menuItem.Tag as SelectedNodesCollection).Count > 0)
					node = (menuItem.Tag as SelectedNodesCollection)[0];
			}

			// 즐겨 찾기에 먼저 추가 해야 한다.
			OnAddFavoritesMenuItemClick(sender, e);

			SetDefaultMenu(node);
		}

		private void OnResetDefaultContextMenuItemClick(object sender, EventArgs e)
		{
			IGMenuItem menuItem = sender as IGMenuItem;
			UltraTreeNode node = null;

			if (menuItem != null)
			{
				if (menuItem.Tag is UltraTreeNode)
					node = menuItem.Tag as UltraTreeNode;
				else if (menuItem.Tag is SelectedNodesCollection && (menuItem.Tag as SelectedNodesCollection).Count > 0)
					node = (menuItem.Tag as SelectedNodesCollection)[0];
			}

			ResetDefaultMenu(node);
		}

		private void SetDefaultMenu(UltraTreeNode node)
		{
			MenuManager menuManager = new MenuManager(UserId);
			string defaultMenuId = string.Empty;

			if (node != null)
			{
				defaultMenuId = node.Key;

				if (node.Key != string.Empty)
				{
					menuManager.SetDefaultMenu(node.Key);
					_xTreeFavorites.Tag = node.Tag as MenuView;

					// System의 기본 메뉴 Icon을 변경한다.
					SetMenuImageAction(_rootNode);

					// 즐겨 찾기 메뉴의 Icon을 변경한다.
					SetMenuImageAction(_xTreeFavorites.Nodes[0]);
					SetActivePane(DockablePaneKeys.Favorite);
				}
			}
		}

		private void ResetDefaultMenu(UltraTreeNode node)
		{
			MenuManager menuManager = new MenuManager(UserId);
			string defaultMenuId = string.Empty;

			defaultMenuId = string.Empty;

			menuManager.SetDefaultMenu(defaultMenuId);
			_xTreeFavorites.Tag = null;

			if (node != null)
				// 기본 메뉴이 Icon을 변경한다.
				SetMenuImageAction(node.RootNode);

			// 즐겨 찾기 메뉴의 Icon을 변경한다.
			SetMenuImageAction(_xTreeFavorites.Nodes[0]);
		}

		private void DeleteNode(UltraTreeNode node)
		{
			if (node != null)
			{
				if (IsRootNode(node) == true)
				{
					using (MessageSettings mSettings = new MessageSettings())
					{
						DialogResult question = MessageBox.Show(mSettings["QuestDeleteSubFavoritesCaption"], mSettings["QuestDeleteSubFavoritesCaption"], MessageBoxButtons.YesNo, MessageBoxIcon.Question);

						if (question == DialogResult.Yes)
						{
							if (node.Nodes.Count > 0)
								for (int i = node.Nodes.Count - 1; i >= 0; i--)
									DeleteRootAction(node.Nodes[i]);
						}
					}
				}
				else
					node.Remove();
			}
		}

		private void DeleteRootAction(UltraTreeNode targetNode)
		{
			List<UltraTreeNode> allNodes = GetAllNodes(targetNode);

			foreach (UltraTreeNode node in allNodes)
				node.Remove();
		}
		#endregion

		private void InitFavoritesDragableAction()
		{
			_xTreeFavorites.AllowDrop = true;
			_xTreeFavorites.SelectionDragStart += new EventHandler(OnMoveSelectionDragStart);
			_xTreeFavorites.DragOver += new DragEventHandler(OnMoveDragOver);
			_xTreeFavorites.DragDrop += new DragEventHandler(OnFavoriteTreeDragDrop);
			_xTreeFavorites.DragLeave += new EventHandler(OnDragLeave);
			_xTreeFavorites.QueryContinueDrag += new QueryContinueDragEventHandler(OnQueryContinueDrag);

			//			btnFavorites.AllowDrop = true;
			//			btnFavorites.DragOver += new DragEventHandler(OnCopyDragOver);
			//			btnFavorites.DragDrop += new DragEventHandler(OnDragDrop);
		}

		private void InitSearchResultTree()
		{
			if (_xTreeSearchResult == null)
				_xTreeSearchResult = new UltraTree();

			InitTreeAppearance(_xTreeSearchResult, true);
			InitDragStartAction(_xTreeSearchResult);

			DockableGroupPane dgpBottomMenu = GetGroupPane(DockableGroupPaneKeys.LeftBottomGroupPane);

			if (dgpBottomMenu != null)
			{
				using (MessageSettings mSettings = new MessageSettings())
				{
					DockableControlPane dcpSearchResult = new DockableControlPane(DockablePaneKeys.SearchResult.ToString(), mSettings["SearchResultCaption"], _xTreeSearchResult);

					dcpSearchResult.ToolTipTab = string.Format("{0}({1})", mSettings["SearchResultCaption"], "Ctrl+Alt+~");
					dcpSearchResult.Settings.TabAppearance.Image = imlPanes.Images["searchresult"];
					dcpSearchResult.Settings.AllowClose = DefaultableBoolean.False;

					dgpBottomMenu.Panes.Add(dcpSearchResult);

					dgpBottomMenu.Unpin();
				}
			}

			SetRootNode(_xTreeSearchResult, _rootNode);
		}

		#region Drop HighLight DrawFilter
		private void InitDropHIghLightHelper(UltraTree xTreeTarget)
		{
			_dropHighLightDrawFilter = new DropHightLightDrawFilter(xTreeTarget);
			_dropHighLightDrawFilter.DropHighLightBackColor = Color.Orange;
			_dropHighLightDrawFilter.Invalidate += new EventHandler(DropHighLightDrawFilter_Invalidate);
			_dropHighLightDrawFilter.QueryStateAllowedForNode += new DropHightLightDrawFilter.QueryStateAllowedForNodeEventHandler(DropHighLightDrawFilter_QueryStateAllowedForNode);
			xTreeTarget.DrawFilter = _dropHighLightDrawFilter;
		}

		private void DropHighLightDrawFilter_Invalidate(object sender, System.EventArgs e)
		{
			DropHightLightDrawFilter drawFilter = sender as DropHightLightDrawFilter;

			if (drawFilter != null)
				drawFilter.TargetTree.Invalidate();
		}

		/// <summary>
		/// 특별한 종류의 노드에 Drop 될 때 발생한다.
		/// <remarks>Node type에 따라서 Drop 여부를 결정 할 수 있다.</remarks>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DropHighLightDrawFilter_QueryStateAllowedForNode(object sender, QueryStateAllowedForNodeEventArgs e)
		{
			DropHightLightDrawFilter drawFilter = sender as DropHightLightDrawFilter;

			if (drawFilter != null)
			{
				// RootNode의 위와 아래에 추가를 할 수 없다(GrandRoot에는 추가할 수 없다)
				if (IsRootNode(e.Node) == true)
				{
					e.StatesAllowed = DropHighLightPosition.OnNode;
					_dropHighLightDrawFilter.EdgeSensitivity = e.Node.Bounds.Height / 3;		// 1로 나누면 선택할 수 없다.

					return;
				}
			}
		}
		#endregion

		#region InitDragStartAction
		private void InitDragStartAction(UltraTree xTreeTarget)
		{
			xTreeTarget.SelectionDragStart += new EventHandler(OnCopySelectionDragStart);
		}

		#region OnSelectionDragStart
		private void OnCopySelectionDragStart(object sender, EventArgs e)
		{
			UltraTree xTree = sender as UltraTree;

			if (xTree != null)
				xTree.DoDragDrop(xTree.SelectedNodes, DragDropEffects.Copy);
		}

		private void OnMoveSelectionDragStart(object sender, EventArgs e)
		{
			UltraTree xTree = sender as UltraTree;

			if (xTree != null)
				xTree.DoDragDrop(xTree.SelectedNodes, DragDropEffects.Move);
		}

		private void SetKeyDropEffect(Type dataType, DragEventArgs e)
		{
			KeyState keyState = (KeyState)e.KeyState;

			// 데이터가 해당 타입일 경우만 처리한다.
			if (e.Data.GetDataPresent(dataType) == true)
			{
				if ((keyState & KeyState.CtrlKey) == KeyState.CtrlKey)
					e.Effect = DragDropEffects.Copy;
				else
					e.Effect = DragDropEffects.Move;
			}
			else
				e.Effect = DragDropEffects.None;
		}
		#endregion
		#endregion

		#region OnDragOver
		private void OnCopyDragOver(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.Copy;
		}

		private void OnMoveDragOver(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.Copy;
			//SetKeyDropEffect(typeof(SelectedNodesCollection), e);

			UltraTree xTreeSender = sender as UltraTree;

			if (xTreeSender != null)
			{
				// 현재 메뉴 Tree에서 이동할 경우
				Point pointInTree = xTreeSender.PointToClient(new Point(e.X, e.Y));
				UltraTreeNode nodeInPoint = xTreeSender.GetNodeFromPoint(pointInTree);

				if (e.Data.GetDataPresent(typeof(SelectedNodesCollection)) == false)
					return;

				SelectedNodesCollection selectedNodes = e.Data.GetData(typeof(SelectedNodesCollection)) as SelectedNodesCollection;
				UltraTreeNode sourceNode = null;

				selectedNodes = selectedNodes.Clone() as SelectedNodesCollection;

				foreach (UltraTreeNode node in selectedNodes)
					sourceNode = node;

				if (selectedNodes == null && selectedNodes.Count == 0)
					return;

				// Root가 틀릴 경우, GrandRoot로는 이동할 수 없다.
				if (nodeInPoint == null || IsOtherRoot(sourceNode, nodeInPoint) == true)
				{
					NonDrageEffect(e);

					return;
				}

				//				if (IsFolder(nodeInPoint) == true && IsFolderNodeSelected(xTreeSender) == true)
				//				{
				//					if (pointInTree.Y > (nodeInPoint.Bounds.Top + 2) && pointInTree.Y < (nodeInPoint.Bounds.Bottom - 2))
				//					{
				//						NonDrageEffect(e);
				//
				//						return;
				//					}
				//				}

				//				if (IsAnyParentSelected(nodeInPoint) == true)
				//				{
				//					NonDrageEffect(e);
				//
				//					return;
				//				}

				if (IsExistsKey(selectedNodes, nodeInPoint) == true && e.AllowedEffect != DragDropEffects.Move)
				{
					NonDrageEffect(e);

					return;
				}

				e.Effect = e.AllowedEffect;
				_dropHighLightDrawFilter.SetDropHighlightNode(nodeInPoint, pointInTree);
			}
		}

		private void NonDrageEffect(DragEventArgs e)
		{
			// 마우스가 node위에 있지 않으면 dropping 할 수 없게 한다.
			e.Effect = DragDropEffects.None;
			_dropHighLightDrawFilter.ClearDropHighlight();
		}

		private bool IsOtherRoot(UltraTreeNode targetNodeA, UltraTreeNode targetNodeB)
		{
			if (targetNodeA.RootNode.Key != targetNodeB.RootNode.Key)
				return true;

			return false;
		}

		private bool IsFolderNodeSelected(UltraTree xTreeTarget)
		{
			foreach (UltraTreeNode selectedNode in xTreeTarget.SelectedNodes)
				if (IsFolder(selectedNode) == true)
					return true;

			return false;
		}

		private bool IsAnyParentSelected(UltraTreeNode targetNode)
		{
			UltraTreeNode parentNode = targetNode.Parent;
			bool result = false;

			while (parentNode != null)
			{
				if (parentNode.Selected == true)
				{
					result = true;

					break;
				}
				else
					parentNode = parentNode.Parent;
			}

			return result;
		}

		private bool IsExistsKey(SelectedNodesCollection selectedNodes, UltraTreeNode targetNode)
		{
			selectedNodes = selectedNodes.Clone() as SelectedNodesCollection;

			if (selectedNodes.Count == 0)
				return false;

			string rootNodeKey = selectedNodes[0].RootNode.Key;
			string[] getPathList = GetExistsPaths(selectedNodes, targetNode.RootNode);

			return getPathList.Length > 0 ? true : false;
		}
		#endregion

		#region OnDragDrop
		private void OnFavoriteTreeDragDrop(object sender, DragEventArgs e)
		{
			UltraTreeNode droppedNode = _dropHighLightDrawFilter.DropHightLightNode;
			SelectedNodesCollection selectedNodes = e.Data.GetData(typeof(SelectedNodesCollection)) as SelectedNodesCollection;

			selectedNodes = selectedNodes.Clone() as SelectedNodesCollection;
			selectedNodes.SortByPosition();

			// MenuTree에서 FavoritesTree로 DragDrop
			if ((e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
			{
				if (selectedNodes == null && selectedNodes.Count == 0)
					return;

				selectedNodes = selectedNodes.Clone() as SelectedNodesCollection;

				UltraTree xTreeSender = sender as UltraTree;

				if (xTreeSender == null)
					return;

				UltraTreeNode targetNode = _xTreeFavorites.Nodes[0].Clone() as UltraTreeNode;

				GetActiveTree().CopySelectedNodes();
				xTreeSender.PasteNodes(droppedNode);
			}

			if ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
			{
				switch (_dropHighLightDrawFilter.DropLinePosition)
				{
					case DropHighLightPosition.OnNode:
						foreach (UltraTreeNode xTreeNode in selectedNodes)
							xTreeNode.Reposition(droppedNode.Nodes);

						break;
					case DropHighLightPosition.BelowNode:
						foreach (UltraTreeNode xTreeNode in selectedNodes)
						{
							xTreeNode.Reposition(droppedNode, NodePosition.Next);
							droppedNode = xTreeNode;
						}

						break;
					case DropHighLightPosition.AboveNode:
						foreach (UltraTreeNode xTreeNode in selectedNodes)
							xTreeNode.Reposition(droppedNode, NodePosition.Previous);

						break;
				}
			}

			// Drop이 완료된 후 HighLight를 지워준다.
			_dropHighLightDrawFilter.ClearDropHighlight();
		}

		private void OnDragDrop(object sender, DragEventArgs e)
		{
			SelectedNodesCollection selectedNodes = e.Data.GetData(typeof(SelectedNodesCollection)) as SelectedNodesCollection;

			if (selectedNodes != null)
				AddFavoriteMenu(selectedNodes);
		}

		private UltraTree GetActiveTree()
		{
			UltraTree xTree = new UltraTree();

			if (xDockManager.ActivePane != null)
				xTree = xDockManager.ActivePane.Control as UltraTree;

			return xTree;
		}

		private void AddFavoriteMenu(SelectedNodesCollection selectedNodes)
		{
			selectedNodes = selectedNodes.Clone() as SelectedNodesCollection;

			if (selectedNodes.Count == 0)
				return;

			string rootNodeKey = selectedNodes[0].RootNode.Key;
			UltraTreeNode targetNode = new UltraTreeNode();

			// 같은 root만 추가되어야 한다.
			// 선택된 노드가 아니고 전 Root에서 검색한다.
			#region For Test
			/*
				if (_xTreeFavorites.SelectedNodes.Count >= 1 && IsOtherRoot(selectedNodes[0], _xTreeFavorites.SelectedNodes[0]) == false)
					targetNode = _xTreeFavorites.SelectedNodes[0];
				else
			*/
			#endregion
			targetNode = _xTreeFavorites.Nodes[rootNodeKey];

			string[] getPathList = GetExistsPaths(selectedNodes, _xTreeFavorites.Nodes[0].RootNode);

			if (getPathList.Length > 0)
			{
				StringBuilder msg = new StringBuilder();

				foreach (string path in getPathList)
					msg.AppendFormat("[{0}]{1}", System.IO.Path.GetFileName(path), Environment.NewLine);

				using (MessageSettings mSettings = new MessageSettings())
				{
					// 아래 메뉴가 이미 존재합니다.
					MessageBox.Show(string.Format("{0}{1}{2}  ", mSettings["MenuWasDuplicated"], Environment.NewLine, msg.ToString(0, msg.Length - 2)),
						mSettings["MenuDuplicatedCaption"], MessageBoxButtons.OK, MessageBoxIcon.Information);		// 메뉴 중복
				}
			}
			else
			{
				GetActiveTree().CopySelectedNodes();
				_xTreeFavorites.PasteNodes(targetNode);

				_xTreeFavorites.Nodes[0].RootNode.ExpandAll(ExpandAllType.Always);

				SetActivePane(DockablePaneKeys.Favorite);
			}
		}
		#endregion

		#region OnDragLeave
		private void OnDragLeave(object sender, EventArgs e)
		{
			_dropHighLightDrawFilter.ClearDropHighlight();
		}
		#endregion

		#region OnQueryContinueDrag
		private void OnQueryContinueDrag(object sender, QueryContinueDragEventArgs e)
		{
			if (e.EscapePressed == true)
			{
				e.Action = DragAction.Cancel;
				_dropHighLightDrawFilter.ClearDropHighlight();
			}
		}
		#endregion

		#region Rescan
		/// <summary>
		/// 
		/// </summary>
		/// <param name="xTreeFavorites"></param>
		/// <returns></returns>
		public Dictionary<string, MenuView> RescanFavorites(UltraTree xTreeFavorites)
		{
			Dictionary<string, MenuView> favoritesMenu = new Dictionary<string, MenuView>();

			if (xTreeFavorites == null)
				return favoritesMenu;

			// 메뉴가 이동되거나 값이 변경되었을 경우 Root를 재 검색을 하여 Depth, HasChildren, ParentMenuId, Seq를 수정한다.
			foreach (UltraTreeNode node in xTreeFavorites.Nodes)
				RescanAction(xTreeFavorites, node, ref favoritesMenu);

			return favoritesMenu;
		}

		private void RescanAction(UltraTree xTree, UltraTreeNode targetNode, ref Dictionary<string, MenuView> favoritesMenu)
		{
			List<UltraTreeNode> allNodes = GetAllNodes(targetNode);

			foreach (UltraTreeNode node in allNodes)
			{
				string menuId = node.Key;
				MenuView menuView = node.Tag as MenuView;

				if (menuView == null)
				{
					// 폴더가 추가된 경우는 Tag에 MenuView가 없다
					menuView = new MenuView();

					menuView.MenuId = GetFolderKey();
					menuView.MenuName = node.Text;
					menuView.IsContainer = true;
					menuView.IsEnabled = true;
				}

				UpdateMenuView(xTree, node, ref menuView);
				favoritesMenu.Add(menuId, menuView);
			}
		}

		private void UpdateMenuView(UltraTree xTree, UltraTreeNode node, ref MenuView menuView)
		{
			node.Key = menuView.MenuId;

			if (node.IsRootLevelNode == true)
				menuView.ParentMenuId = string.Empty;
			else
				menuView.ParentMenuId = node.Parent.Key;

			menuView.Depth = node.Level;

			if (node.IsRootLevelNode == true)
				menuView.Seq = xTree.Nodes.IndexOf(node.Key) + 1;
			else
				menuView.Seq = node.Parent.Nodes.IndexOf(node.Key) + 1;

			menuView.IsRoot = node.IsRootLevelNode;
			menuView.HasChildren = node.HasNodes;
		}
		#endregion

		private string[] GetExistsPaths(SelectedNodesCollection sourceNode, UltraTreeNode targetNode)
		{
			Dictionary<string, string> sourceKeys = GetKeynPathList(sourceNode[0]);
			Dictionary<string, string> targetKeys = GetKeynPathList(targetNode);
			List<string> duplicatedMenu = new List<string>();

			foreach (string targetKey in targetKeys.Keys)
				if (sourceKeys.ContainsKey(targetKey) == true)
					duplicatedMenu.Add(targetKeys[targetKey]);

			return duplicatedMenu.ToArray();
		}

		private Dictionary<string, string> GetKeynPathList(UltraTreeNode targetNode)
		{
			Dictionary<string, string> treeNodeKeynPath = new Dictionary<string, string>();

			if (targetNode != null)
			{
				List<UltraTreeNode> allNodes = GetAllNodes(targetNode);

				foreach (UltraTreeNode node in allNodes)
					treeNodeKeynPath.Add(node.Key, node.FullPath);
			}

			return treeNodeKeynPath;
		}

		private List<UltraTreeNode> GetAllNodes(UltraTreeNode rootNode)
		{
			List<UltraTreeNode> nodeList = new List<UltraTreeNode>();
			Queue<TreeNodesCollection> queue = new Queue<TreeNodesCollection>();

			if (rootNode != null)
				nodeList.Add(rootNode);

			queue.Enqueue(rootNode.Nodes);

			while (queue.Count > 0)
			{
				TreeNodesCollection nodes = queue.Dequeue();

				if (nodes != null && nodes.Count == 0)
					continue;

				foreach (UltraTreeNode node in nodes)
				{
					nodeList.Add(node);

					queue.Enqueue(node.Nodes);
				}
			}

			return nodeList;
		}

		private void SetMenuImageAction(UltraTreeNode targetNode)
		{
			List<UltraTreeNode> allNodes = GetAllNodes(targetNode);
			string defaultMenuId = GetDefaultMenuId();

			foreach (UltraTreeNode node in allNodes)
			{
				if (IsRootNode(node) == true)
					node.Override.NodeAppearance.Image = imlMenus.Images["root"];
				else
				{
					MenuView menuView = node.Tag as MenuView;

					if (menuView != null)
					{
						if (menuView.IsContainer == true)
							node.Override.NodeAppearance.Image = imlMenus.Images["folder"];
						else
						{
							if (defaultMenuId == menuView.MenuId)
								node.Override.NodeAppearance.Image = imlFavoriteContext.Images["default"];		// 기본 메뉴 image를 할당 한다.
							else
							{
								// 디버그 모드일 경우, 어셈블리가 없을 경우 다른 모양으로 표시한다.
								if (DebugMode == true && File.Exists(GetMenuAssemblyPath(menuView)) == false)
									node.Override.NodeAppearance.Image = imlMenus.Images["not_exist_menu"];
								else
									node.Override.NodeAppearance.Image = imlMenus.Images["menu"];
							}
						}
					}
				}
			}
		}

		private MenuView GetDefaultMenuView()
		{
			if (_xTreeFavorites != null)
				return _xTreeFavorites.Tag as MenuView;
			else
				return null;
		}

		private string GetDefaultMenuId()
		{
			string defaultMenuId = String.Empty;
			MenuView menuView = GetDefaultMenuView();

			if (menuView != null)
				defaultMenuId = menuView.MenuId;

			return defaultMenuId;
		}

		private string GetMenuAssemblyPath(MenuView menuView)
		{
			return string.Format(@"{0}\Win\{1}\{2}", FrameworkSetup.Instance.InstallFolder, menuView.FormPath, menuView.FormName);
		}

		private string GetLoadMenuAssemblyPath(MenuView menuView)
		{
			string targetBase = FrameworkSetup.Instance.InstallFolder;

			if (DebugMode == false)
				targetBase= string.Format("{0}:{1}/{2}", 
                                ConfigWrapper.UpdaterCore.Default.Uri, 
                                ConfigWrapper.UpdaterCore.Default.Port, 
                                ConfigWrapper.UpdaterCore.DeployFolder);

			return string.Format(@"{0}/{1}/{2}/{3}",
					   targetBase,
					   menuView.FormType,
					   menuView.FormPath,
					   menuView.FormName);
		}

		private string GetFolderKey()
		{
			Thread.Sleep(1);

			return string.Format("{0}:{1}", FOLDER_KEY_PREFIX, DateTime.Now.ToOADate().ToString().Replace(".", string.Empty));
		}

		private bool IsRootNode(UltraTreeNode targetNode)
		{
			return (targetNode.Key.Split(':')[0] == ROOT_KEY_PREFIX);
		}

		private bool IsFolder(UltraTreeNode targetNode)
		{
			return (targetNode.Key.Split(':')[0] == FOLDER_KEY_PREFIX);
		}

		private void LoadFavoritesMenus()
		{
			MenuManager menuManager = new MenuManager(UserId);
			UltraTreeNode favoritesRootNodes = menuManager.LoadFavoriteMenusFromLocalFile(DebugMode);

			if (favoritesRootNodes.Nodes.Count == 0)
				return;

			_xTreeFavorites.Nodes.Clear();

			// 생성된 TreeView에 Node를 추가한다.
			foreach (UltraTreeNode systemNode in favoritesRootNodes.Nodes)
				_xTreeFavorites.Nodes.Add(systemNode);

			// NOTICE: 즐겨찾기 Tree의 Tag에 DefaultMenuView를 할당한다.
			// by KIMKIWON\xyz37 in 2008년 6월 16일 월요일 오후 6:06
			_xTreeFavorites.Tag = menuManager.GetDefaultMenuView();

			// 노드 이미지를 변경한다.
			SetMenuImageAction(favoritesRootNodes);
		}

		private bool IsExistFavoritesMenu()
		{
			bool loadFavoritesPane = false;
			//MenuManager menuManager = new MenuManager(UserId);
			//UltraTreeNode favoritesRootNodes = menuManager.LoadFavoriteMenusFromLocalFile(_isDebugging);

			//if (favoritesRootNodes.GetNodeCount(true) != 0 && 	
			//	(_rootNode.Nodes.Count != favoritesRootNodes.GetNodeCount(true)))
			//	loadFavoritesPane = true;

			// RootNode의 count와 즐겨찾기 전체 Node의 count가 같으면 추가된 즐겨찾기 메뉴는 없다.
			// 단 즐겨찾기의 RootNodes.Count는 0 이상이어야 한다.
			int totalNodeCount = _xTreeFavorites != null ? _xTreeFavorites.GetNodeCount(true) : 0;

			if (totalNodeCount != 0 && _xTreeFavorites.Nodes.Count != totalNodeCount)
				loadFavoritesPane = true;

			return loadFavoritesPane;
		}
	}
}
