﻿using System;
using System.Collections.Generic;
using System.Text;
using Infragistics.Win.UltraWinDock;

namespace iDASiT.FX.Win.Loader
{
	public partial class Loader
	{
		private DockableControlPane _activePane;

		private void SetActivePane(DockablePaneKeys dockablePaneKeys)
		{
			DockableControlPanesCollection leftMenuDockablePanes = GetLeftMenuDockablePanes();

			if (leftMenuDockablePanes != null)
			{
				int searchResultPaneIndex = leftMenuDockablePanes.IndexOf(dockablePaneKeys.ToString());

				if (searchResultPaneIndex != -1)
					leftMenuDockablePanes[searchResultPaneIndex].Activate();
			}
		}

		private DockableControlPanesCollection GetLeftMenuDockablePanes()
		{
			// TODO: 메뉴를 구할 수 없다 ㅠ.ㅠ
			//			if (xDockManager.DockAreas.IndexOf(LEFT_MENU_AREA_PANE_KEY) != -1)
			//				if (((DockableGroupPane)xDockManager.DockAreas[LEFT_MENU_AREA_PANE_KEY]).Panes.IndexOf(LEFT_GROUP_PANE_KEY) != -1)
			//					{
			//						DockableControlPanesCollection dockableControlPanesCollection = new DockableControlPanesCollection();
			//								DockableControlPanesCollection dockableControlPanesCollection = (DockableControlPanesCollection)(new PanesCollectionConverter()).ConvertFrom(
			//									((DockableGroupPane)xDockManager.DockAreas[LEFT_MENU_AREA_PANE_KEY].Panes[LEFT_GROUP_PANE_KEY]).Panes);
			//
			//								return dockableControlPanesCollection;
			//					}

			return xDockManager.ControlPanes;
		}

		private void SaveActivePane()
		{
			foreach (DockableControlPane pane in GetLeftMenuDockablePanes())
			{
				if (pane.IsInView == true)
				{
					_activePane = pane;

					break;
				}
			}
		}

		private void LoadActivePane()
		{
			if (_activePane != null)
				_activePane.Activate();
		}
		
		private DockableGroupPane GetGroupPane(DockableGroupPaneKeys dockableGroupPaneKeys)
		{
			DockableGroupPane dockableGroupPane = null;
			string leftAreaPaneKey = DockableGroupPaneKeys.LeftAreaPaneKey.ToString();
			string groupKey = dockableGroupPaneKeys.ToString();

			if (xDockManager.DockAreas.Count > 0 && xDockManager.DockAreas[leftAreaPaneKey].Panes.Count > 0)
				if (xDockManager.DockAreas[leftAreaPaneKey].Panes.IndexOf(groupKey) > 0)
					dockableGroupPane = ((DockableGroupPane)xDockManager.DockAreas[leftAreaPaneKey].Panes[groupKey]);

			if (dockableGroupPane == null)
			{
				dockableGroupPane = new DockableGroupPane(groupKey);

				if (xDockManager.DockAreas.Count == 0)
				{
					DockAreaPane dapMenuLeft = new DockAreaPane(DockedLocation.DockedLeft, leftAreaPaneKey);

					xDockManager.DockAreas.Add(dapMenuLeft);
				}

				bool exists = false;

				foreach (DockablePaneBase dpb in xDockManager.DockAreas[leftAreaPaneKey].Panes)
				{
					if (dpb.Key == dockableGroupPane.Key)
					{
						exists = true;

						break;
					}
				}

				if (exists == false)
					xDockManager.DockAreas[leftAreaPaneKey].Panes.Add(dockableGroupPane);				
			}

			dockableGroupPane.ChildPaneStyle = ChildPaneStyle.TabGroup;

			return dockableGroupPane;
		}

		private void SetMenuPin()
		{
			bool isAutoHideMenu = LoaderSetting.IsAutoHideMenu;
			// TODO: 즐겨찾기 Pane 상태도 변경하는 루틴 추가
			// by KIMKIWON\xyz37 in 2007년 10월 25일 목요일 오전 11:49
			DockAreaPane dockAreaPane = null;

			foreach (DockAreaPane dap in xDockManager.DockAreas)
			{
				if (dap.Key == DockableGroupPaneKeys.LeftAreaPaneKey.ToString())
				{
					dockAreaPane = xDockManager.DockAreas[DockableGroupPaneKeys.LeftAreaPaneKey.ToString()];

					break;
				}
			}

			if (dockAreaPane != null && dockAreaPane.FirstVisiblePane != null)
			{
				if (isAutoHideMenu == true)
					dockAreaPane.Unpin();
				else
					dockAreaPane.Pin();
			}
		}

		#region Load & Save xDocManager setting
		/// <summary>
		/// Docking Manager 상태를 Load 합니다.
		/// </summary>
		private void LoadxDocManagerSetting()
		{
			// TODO: Load and Save DocManager setting
			// by KIMKIWON\xyz37 in 2007년 10월 25일 목요일 오후 12:02
			//			bool isSaveDocPaneSetting = bool.Parse(GetConfig(("IsSaveDocPaneSetting")));
			//
			//			if (isSaveDocPaneSetting == true)
			//			{
			//				// 파일이 없으면 기본 세팅으로 저장한다.
			//				if (File.Exists(DOC_Setting_Filename) == false)
			//					xDockManager.SaveAsXML(DOC_Setting_Filename);
			//
			//				xDockManager.LoadFromXML(DOC_Setting_Filename);
			//			}
		}

		/// <summary>
		/// Docking Manager 상태를 Save 합니다.
		/// </summary>
		private void SavexDocManagerSetting()
		{
			//			bool isSaveDocPaneSetting = bool.Parse(GetConfig(("IsSaveDocPaneSetting")));
			//
			//			if (isSaveDocPaneSetting == true)
			//			{
			//				xDockManager.SaveSettingsFormat = SaveSettingsFormat.Xml;
			//				xDockManager.SaveAsXML(DOC_Setting_Filename);
			//			}
		}
		#endregion
	}
}
