﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.MessageAgent
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 24일 목요일 오후 1:43
/*	Purpose		:	Message Agent를 위한 class입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using iDASiT.FX.Win.Configuration;
using iDASiT.FX.Win.Messenger.Agent;
using iDASiT.FX.Win.Messenger;
using System.Windows.Forms;
using System.Diagnostics;

namespace iDASiT.FX.Win.Loader
{
    /// <summary>
	/// Message Agent를 위한 class입니다.
	/// </summary>
	public class MessageAgent : IXMessage, IMessageAgent
	{
		#region Fields
		private static CallbackAgent _agent;
		private string _targetServerIp;
		private string _userId;
		private bool _enableMessageService;
		#endregion
				
        #region Use Singleton Pattern
        private static MessageAgent _newInstance;
        
        #region Constructor for Single Ton Pattern
        /// <summary>
        /// MessageAgent class의 Single Ton Pattern을 위한 생성자 입니다.
        /// </summary>
		/// <param name="targetServerIp"></param>
		/// <param name="userId"></param>
        /// <returns></returns>
        private MessageAgent(string targetServerIp, string userId)
        {
        	_newInstance = null;
        	_targetServerIp = targetServerIp;
			_userId = userId;
			_enableMessageService = LoaderSetting.EnableMessageService;
        }
        #endregion         
		
        #region GetInstance
        /// <summary>
        /// MessageAgent class의 Single Ton Pattern을 위한 새 인스턴스를 초기화 합니다.
        /// </summary>
		/// <param name="targetServerIp"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static MessageAgent GetInstance(string targetServerIp, string userId)
        {
        	if (_newInstance == null)
        		_newInstance = new MessageAgent(targetServerIp, userId);
        
        	return _newInstance;
        }
		
		/// <summary>
		/// GetInstance(string targetServerIp, string userId)를 사용하여 초기화한 instance에 접근할 수 있습니다.
		/// </summary>
		/// <returns></returns>
		public static MessageAgent Instance
		{
			get
			{
				if (_newInstance == null)
	            	throw new InvalidOperationException("GetInstance(string targetServerIp, string userId)를 사용하여 먼저 정보를 입력하십시오.");
            	
				return _newInstance;
			}
		}
        #endregion
        #endregion        
        		
		#region IXMessage 멤버

		/// <summary>
		/// XMessage에 접근해서 Agent를 통하지 않고 Message를 전달할 수 있습니다.
		/// </summary>
		XMessage IXMessage.XMessage
		{
			get
			{
				return (_agent as IXMessage).XMessage;
			}
		}

		#endregion

		/// <summary>
		/// 서버에 연결할지 여부를 체크 합니다.
		/// </summary>
		/// <returns></returns>
		private bool Connectable
		{
			get
			{
				if (_enableMessageService == false)
					return false;

				bool result = false;

				// 연결할 수 없을 경우 off-line Message로 작동 시킨다.
				try
				{
					if (_agent == null)
					{
						_agent = new CallbackAgent(AgentMode.Controller);

						_agent.BroadCastedMessage += new SendMessageEventHandler(_agent_ListenedMessage);
						_agent.SendedMessage += new SendMessageEventHandler(_agent_ListenedMessage);
						_agent.GetFileSendRequest += new SendMessageEventHandler(_agent_GetFileSendRequest);
						_agent.GetFileSendCompleted += new TransferEventHandler(_agent_GetFileSendCompleted);
						_agent.PutFileReceived += new SendMessageEventHandler(_agent_PutFileReceived);
						_agent.PutFileCompleted += new TransferEventHandler(_agent_PutFileCompleted);
					}

					result = _agent.CheckConnection;
				}
				catch
				{
				}

				return result;
			}
		}

		void _agent_PutFileReceived(object sender, MessageEventArgs e)
		{
			// NOTICE: 확인 MessageBox를 띄울 수 없다(통보만 한다)
			// by KIMKIWON\xyz37 in 2008년 7월 25일 금요일 오후 2:48
			// TODO: 추후 Tray에 Notification을 위한 Message Box 생성 후 구현
			// by KIMKIWON\xyz37 in 2008년 7월 25일 금요일 오후 2:49
			//MessageBox.Show(string.Format("{0} 전송 요청", e.Message), "PutFileReceived", MessageBoxButtons.OK, MessageBoxIcon.Information);            
		}

		void _agent_PutFileCompleted(object sender, TransferInfoEventArgs e)
		{
			//MessageBox.Show(string.Format("{0} 전송 완료", e.FileName), "PutFileCompleted", MessageBoxButtons.OK, MessageBoxIcon.Information);            
		}

		private void _agent_GetFileSendRequest(object sender, MessageEventArgs e)
		{
			// TODO: 추후 Tray에 Notification을 위한 Message Box 생성 후 구현
			// by KIMKIWON\xyz37 in 2008년 7월 25일 금요일 오후 2:32
			//MessageBox.Show(e.Message + "보내기 요청", e.FromIpAddress, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void _agent_GetFileSendCompleted(object sender, TransferInfoEventArgs e)
		{
			//MessageBox.Show(e.FileName + "보내기 완료", e.ServerIpAddress, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
				
		private void _agent_ListenedMessage(object sender, MessageEventArgs e)
		{
			if (string.IsNullOrEmpty(e.Message) == true)
				return;
            	
			string message = e.Message;
			string title = e.SendType == SendType.BroadCast ? "관리자 알림" : string.Format("{0} 사용자 알림", e.FromUserId);
			
			MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Information);            
		}

		/// <summary>
		/// Agent에 Login합니다.
		/// </summary>
		public void Login()
		{
			if (Connectable == true)
				_agent.Login(_userId, _targetServerIp);
		}

		/// <summary>
		/// Agent에서 Logout합니다.
		/// </summary>
		public void Logout()
		{
			if (Connectable == true)
				_agent.Logout(_targetServerIp);
		}

		/// <summary>
		/// Agent에 Update Message를 전송합니다.
		/// </summary>
		public void AppUpdate()
		{
			if (Connectable == true)
				_agent.AppUpdate(_targetServerIp);
		}

		/// <summary>
		/// Server로 Exception message를 전달합니다.
		/// </summary>
		/// <param name="message">사용자가 처리할 수 있는 메세지를 입력합니다.</param>
		/// <param name="exception">실제 프로그램에서 발생한 Exception입니다.</param>
		public void SendExceptionMessage(string message, Exception exception)
		{
			if (Connectable == true)
				_agent.SendExceptionMessage(message, exception, _userId, _targetServerIp);
		}

		/// <summary>
		/// Client Form Action Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="formId">FormId</param>
		/// <param name="formPath">Form Path</param>
		/// <param name="formVersion">Form의 Version</param>
		public void SendFormOpenMessage(string formId, string formPath, string formVersion)
		{
			if (Connectable == true)
				_agent.SendFormOpenMessage(_userId, formId, formPath, formVersion, _targetServerIp);
		}

		/// <summary>
		/// Client Form Action Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="formId">FormId</param>
		/// <param name="formPath">Form Path</param>
		/// <param name="formVersion">Form의 Version</param>
		public void SendFormCloseMessage(string formId, string formPath, string formVersion)
		{
			if (Connectable == true)
				_agent.SendFormCloseMessage(_userId, formId, formPath, formVersion, _targetServerIp);
		}

		/// <summary>
		/// Client Form Action Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="formId">FormId</param>
		public void SendFormActiveMessage(string formId)
		{
			if (Connectable == true)
				_agent.SendFormActiveMessage(_userId, formId, _targetServerIp);
		}
		
		/// <summary>
		/// 선택된 client로 message를 전달합니다.
		/// </summary>
		/// <param name="message">사용자가 처리할 수 있는 메세지를 입력합니다.</param>
		public void SendMessage(string message, string[] toIpAddresses)
		{
			if (Connectable == true)
				_agent.SendMessage(message, toIpAddresses);
		}

		/// <summary>
		/// 전체 client에 message를 전파합니다.
		/// </summary>
		/// <param name="message">사용자가 처리할 수 있는 메세지를 입력합니다.</param>
		public void BroadCastMessage(string message)
		{
			if (Connectable == true)
				_agent.BroadCastMessage(message);
		}

		/// <summary>
		/// for Test
		/// </summary>
		public void Put()
		{
			if (Connectable == true)
            	_agent.PutFiles(@"C:\WINDOWS\system32", new string[] { "at.exe", "alg.exe" }, @"D:\Temp", "192.168.25.247");
		}

		#region IMessageAgent 멤버

		MessageAgent IMessageAgent.NewInstance
		{
			get
			{
				return _newInstance;
			}
		}

		#endregion
	}
}
