﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.Loader
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 11월 29일 목요일 오후 4:58
/*	Purpose		:	IExchangeMessage를 구현한 partial class
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTree;
using iDASiT.FX.Win.Configuration;
using System.Text;
using Infragistics.Win.UltraWinStatusBar;

namespace iDASiT.FX.Win.Loader
{
	public partial class Loader
	{
		#region Fields
		private static string _plantId;
		private string _userId;
		#endregion

		#region IExchangeMessage 멤버

		/// <summary>
		/// PlantId를 구하거나 설정합니다.
		/// </summary>
		public string PlantId
		{
			get
			{
				if (ConfigWrapper.Loader.DebugMode == true)
					_plantId = ConfigWrapper.Loader.PlantId;
				
				return _plantId;
			}
			set
			{
				_plantId = value;
			}
		}

		/// <summary>
		/// UserId를 구하거나 설정합니다.
		/// </summary>
		public string UserId
		{
			get
			{
				if (ConfigWrapper.Loader.DebugMode == true && _userId == string.Empty)
					_userId = ConfigWrapper.Loader.UserId;

				return _userId;
			}
			set
			{
				_userId = value;
			}
		}

		/// <summary>
		/// 현재 사용중인 Client의 local IpAddress를 구합니다.
		/// </summary>
		public string ClientIpAddress
		{
			get
			{
				return xStatusBar.Panels[StatusbarKeys.ClientIp.ToString()].Text;
			}
			set
			{
				xStatusBar.Panels[StatusbarKeys.ClientIp.ToString()].Text = value;
			}
		}

		/// <summary>
		/// 현재 활성화된 FormId를 구합니다.
		/// </summary>
		public string FormId
		{
			get
			{
				string formId = string.Empty;

				MenuView menuView = ActiveTab.Tag as MenuView;

				if (menuView != null)
					formId = menuView.FormId;

				return formId;
			}
		}

		private delegate void SetUltraStatusBarMessage(UltraStatusBar ultraStatusBar, string message);
		private delegate string GetUltraStatusBarMessage(UltraStatusBar ultraStatusBar);

		private void WriteUltraStatusBarMessage(UltraStatusBar ultraStatusBar, string message)
		{
			if (ultraStatusBar.InvokeRequired == true)
			{
				SetUltraStatusBarMessage setUltraStatusBarMessage = new SetUltraStatusBarMessage(WriteUltraStatusBarMessage);
				
				Invoke(setUltraStatusBarMessage, ultraStatusBar, message);
			}
			else
			{
				ultraStatusBar.BeginUpdate();
				ultraStatusBar.Panels[StatusbarKeys.StatusMessage.ToString()].Text = message;
				ultraStatusBar.Update();
				ultraStatusBar.EndUpdate();
			}
		}

		private string ReadUltraStatusBarMessage(UltraStatusBar ultraStatusBar)
		{
			string message = string.Empty;

			if (ultraStatusBar.InvokeRequired == true)
			{
				GetUltraStatusBarMessage getUltraStatusBarMessage = new GetUltraStatusBarMessage(ReadUltraStatusBarMessage);

				Invoke(getUltraStatusBarMessage, ultraStatusBar);
			}
			else
				message = ultraStatusBar.Panels[StatusbarKeys.StatusMessage.ToString()].Text;

			return message;
		}

		/// <summary>
		/// Loader의 Statusbar에 Message를 구하거나 입력합니다.(Thread에서 작업할 수있습니다.)
		/// </summary>
		public string StatusbarMessage
		{
			get
			{
				//return xStatusBar.Panels[StatusbarKeys.StatusMessage.ToString()].Text;
				return ReadUltraStatusBarMessage(xStatusBar);
			}
			set
			{
				WriteUltraStatusBarMessage(xStatusBar, value);
			}
		}

		/// <summary>
		/// Child Form의 Exception 처리를 UserFriendly한 화면으로 처리합니다.<br/>
		/// <remarks>DialogResult.Abort, DialogResult.Retry, DialogResult.Ignore를 처리힙니다.<br/>
		/// Abort : 현재 열려진 폼을 닫습니다..<br/>
		/// Retry : 재시도 할 수 있도록 합니다. 호출한 Method에서 처리합니다.<br/>
		/// Ignore : 아무 동작도 하지 않습니다. 호출한 Method에서 처리합니다.
		/// </remarks>
		/// </summary>
		/// <param name="exception">실제 프로그램에서 발생한 Exception입니다.</param>
		/// <returns>DialogResult.Retry, DialogResult.Ignore를 리턴합니다.</returns>
		public DialogResult DoException(Exception exception)
		{
			return DoException(exception.Message, exception);
		}

		/// <summary>
		/// Child Form의 Exception 처리를 UserFriendly한 화면으로 처리합니다.<br/>
		/// <remarks>DialogResult.Abort, DialogResult.Retry, DialogResult.Ignore를 처리힙니다.<br/>
		/// Abort : 현재 열려진 폼을 닫습니다..<br/>
		/// Retry : 재시도 할 수 있도록 합니다. 호출한 Method에서 처리합니다.<br/>
		/// Ignore : 아무 동작도 하지 않습니다. 호출한 Method에서 처리합니다.
		/// </remarks>
		/// </summary>
		/// <param name="message">사용자가 처리할 수 있는 메세지를 입력합니다.</param>
		/// <param name="exception">실제 프로그램에서 발생한 Exception입니다.</param>
		/// <returns>DialogResult.Retry, DialogResult.Ignore를 리턴합니다.</returns>
		public DialogResult DoException(string message, Exception exception)
		{
			ExceptionHandler exceptionHandler = new ExceptionHandler(Owner);

			try
			{
				// Server로 exception message를 전달합니다.
				// NOTICE: Exception을 그대로 전달할 경우 동일한 Exception이 발생하여 다시 생성한다.
				// by KIMKIWON\xyz37 in 2008년 8월 5일 화요일 오후 2:01
				// MESSAGE MessageAgent.Instance.SendExceptionMessage(message, new Exception(message));
			}
			catch
			{
				// 여기서 예외 발생하면 Loader가 down 된다.
			}
			
			DialogResult dialogResult = exceptionHandler.DoException(message, exception);

			switch (dialogResult)
			{
				case System.Windows.Forms.DialogResult.Abort:
					ActiveTab.Close();

					break;
				case System.Windows.Forms.DialogResult.Retry:

					break;
				case System.Windows.Forms.DialogResult.Ignore:

					break;
			}

			return dialogResult;
		}
		
		/// <summary>
		/// 등록된 Menu에서 Popup 폼을 호출 합니다.
		/// </summary>
		/// <param name="formId">등록된 FormId</param>
		/// <param name="param">parameter</param>
		public void Popup(string formId, params object[] param)
		{
			UltraTreeNode selectedNode = GetTreeNodeByFormId(formId, _rootNode);

			if (selectedNode.Tag == null)
			{
				StringBuilder msg = new StringBuilder();

				msg.AppendLine(string.Format("{0}를 가진 Menu가 구성 되지 않았습니다.", formId));
				msg.AppendLine("호출하려는 Form이 메뉴에 구성이 되어 있어야 합니다.");
				msg.AppendLine("구성 후 Enable 속성을 False로 하면 메뉴에는 없지만 호출할 수 있습니다.");
				msg.AppendLine("호출 FormId를 확인하십시오.");

				throw new InvalidOperationException(msg.ToString());
			}

			MenuClickAction(selectedNode, param);
		}

		private UltraTreeNode GetTreeNodeByFormId(string formId, UltraTreeNode rootNode)
		{
			UltraTreeNode findNode = new UltraTreeNode();

			foreach (UltraTreeNode node in GetAllNodes(rootNode))
			{
				MenuView menuView = node.Tag as MenuView;

				if (menuView != null && menuView.FormId == formId)
				{
					findNode = node;

					break;
				}
			}

			return findNode;
		}

		/// <summary>
		/// 등록된 Menu에서 모달 형식의 Popup 폼을 호출 합니다.
		/// </summary>
		/// <param name="formId">등록된 FormId</param>
		/// <param name="param">parameter</param>
		public void PopupModal(string formId, params object[] param)
		{
			// TODO: Modal Popup을 구현 해야 한다.
			// by KIMKIWON\xyz37 in 2008년 1월 9일 수요일 오전 9:30
			Popup(formId, param);
		}

		#endregion
	}
}
