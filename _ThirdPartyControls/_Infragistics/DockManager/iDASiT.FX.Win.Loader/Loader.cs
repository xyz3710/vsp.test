//#define OPTIMIZE

/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.Loader(MainClass)
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 8월 22일 수요일 오전 11:48
/*	Purpose		:	WinForm client의 main form 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	image list key는 모두 lower case, Toolbar의 key는 PascalCase
 *					// MESSAGE 주석으로 messagner관련 부분은 주석 처리
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using iDASiT.FX.Win;
using iDASiT.FX.Win.Configuration;
using iDASiT.FX.Win.Utility;
using Infragistics.Win.UltraWinTabbedMdi;
using Infragistics.Win.UltraWinTree;
using Infragistics.Win.UltraWinToolbars;
using iDASiT.FX.Win.Configuration.Toolbars;
using Infragistics.Win;

namespace iDASiT.FX.Win.Loader
{
	/// <summary>
	/// WinForm client의 main form 입니다.
	/// </summary>
	[Serializable]
	public partial class Loader : Form, IExchangeMessage, IProcessBar, IMainToolbarAction, IStatusbarMessage, IStandardToolbarAction, IStandardToolbarProperty//, IXMessage, IMessageAgent
	{
		#region Delegate
		private delegate void RecursiveAction(UltraTreeNode node);
		#endregion

		#region Fields
		private List<MdiTab> _mdiTabs;
		private ToolbarManager _toolbarManager;
		#endregion

		#region Constructor
		/// <summary>
		/// Loader 클래스의 인스턴스를 생성합니다.
		/// </summary>
		/// <param name="plantId">plantId</param>
		/// <param name="userId">userId</param>
		/// <param name="isDebugging">debugging 모드인지 Loader 실행파일의 parameter에 의해 판별 한다.(PlantId, UserId만 있을 경우 true)<br/>
		/// <seealso cref="iDASiT.FX.Win.ExchangeForm.ProcessArgumentDelimiter"/>로 PlantId와 UserId를 구별한다.</param>
		public Loader(string plantId, string userId, bool isDebugging)
		{
#if OPTIMIZE
			#region Performance Test
			DateTime start = DateTime.Now;
			LogManager.Write("Optimize.log", string.Format("{1}Loader ctor Start({0}){1}", start.ToLongTimeString(), Environment.NewLine));
			#endregion
#endif
			// Form 초기화
			InitializeComponent();
#if OPTIMIZE
			#region Performance Test
			DateTime now6 = DateTime.Now;
			LogManager.Write("Optimize.log", "After InitializeComponent\t" + string.Format("{0:HHmmss}", (now6 - start)) + Environment.NewLine);
			#endregion
#endif

			#region Set PlantId & UserId
			PlantId = plantId;

			string[] userInfo = userId.Split(new char[] { ExchangeForm.ArgumentDelimiter }, StringSplitOptions.RemoveEmptyEntries);
			
			if (userInfo.Length >= 2)
			{
				// Id;Name 형태로 온다.
				UserId = userInfo[0];

				string userName = userInfo[1];

				// space나 ExchangeForm.ArgumentDelimiter가 있을 수 있으므로 변경 한다.
				for (int i = 2; i < userInfo.Length; i++)
					userName += string.Format(" {0}", userInfo[i]);

				UserName = string.Format("{0}({1})", userName, UserId);
			}
			else
			{
				// UserName이 없으면 Id만 할당한다.
				UserId = userInfo[0];
				UserName = UserId;
			}
			#endregion

			_rootNode = new UltraTreeNode();

			SuspendLayout();
#if OPTIMIZE
			#region Performance Test
			DateTime now1 = DateTime.Now;
			LogManager.Write("Optimize.log", "Before InitMainForm\t" + string.Format("{0:HHmmss}", (now1 - now6)) + Environment.NewLine);
			#endregion
#endif
			InitMainForm();
#if OPTIMIZE
			#region Performance Test
			DateTime now2 = DateTime.Now;
			LogManager.Write("Optimize.log", "Before InitMainMenuToolbar\t" + string.Format("{0:HHmmss}", (now2 - now1)) + Environment.NewLine);
			#endregion
#endif
			// ToolBar 초기화
			_toolbarManager = new ToolbarManager(this, this, this);
#if OPTIMIZE
			#region Performance Test
			DateTime now4 = DateTime.Now;
			LogManager.Write("Optimize.log", "After InitStandardToolBar\t" + string.Format("{0:HHmmss}", (now4 - now3)) + Environment.NewLine);
			#endregion
#endif
			// 탭이 많을 경우 일부 탭이 보이도록 한다.
			// TODO: xTabbedMdiManager 사용
			// by KIMKIWON\xyz37 in 2008년 11월 13일 목요일 오후 6:18
			xTabbedMdiManager.TabGroupSettings.ShowPartialTabs = DefaultableBoolean.True;

			ResumeLayout();

			// Message Agent를 등록 한다.
			// MESSAGE MessageAgent.GetInstance(TargetServer, UserId).Login();

#if OPTIMIZE
			#region Performance Test
			DateTime now5 = DateTime.Now;
			LogManager.Write("Optimize.log", "Loader ctor End\t" + string.Format("{0:HHmmss}", (now5 - now4)) + Environment.NewLine);
			#endregion
#endif
		}

		/// <summary>
		/// Loader 클래스의 인스턴스를 생성합니다.
		/// </summary>
		public Loader()
		{
		  
		}
		#endregion

		#region Initialization
		private void InitMainForm()
		{
			// GetThemeSetting
			// ThemeSetting에서 BackColor와 Font를 읽어들여 적용한다.
			using (ThemeSettings themeSettings = new ThemeSettings())
			{
				BackColor = themeSettings.BackColor;
				Font = themeSettings.ThemeFont;
				Text = string.Format("{0} {1} - Server({2}){3}", 
                           LoaderSetting.Text, 
                           Version, 
                           TargetServer,
						   ConfigWrapper.Loader.DebugMode == true ? " << For Developer Version >>" : string.Empty);
			}
			
			IsMdiContainer = true;

			// 1024 * 768에서 Taskbar를 제외한 영역
			Size = new Size(1022, 740);

			// Menu Pane Shortcut이 작동하도록 설정한다
			KeyPreview = true;

			// LocalIp를 setting한다.
			ClientIpAddress = IpManager.LocalIp;
		}

		#region Properties
		private string TargetServer
		{
			get
			{
				return ConfigWrapper.UpdaterCore.Default.Host;
			}
		}

		private string Version
		{
			get
			{
				AssemblyName assemblyName = Assembly.GetCallingAssembly().GetName();

				return string.Format("Ver. {0}", assemblyName.Version.ToString());
			}
		}

		private string UserName
		{
			get
			{
				return xStatusBar.Panels[StatusbarKeys.UserInfo.ToString()].Text;
			}
			set
			{
				xStatusBar.Panels[StatusbarKeys.UserInfo.ToString()].Text = value;
			}
		}
		#endregion

		#region Load & Save FormState
		/// <summary>
		/// Form 상태를 Load 합니다.
		/// </summary>
		private void LoadFormSetting()
		{			
			StartPosition = LoaderSetting.StartPosition;

			Size = LoaderSetting.Size;
			Location = LoaderSetting.Location;

			if (Location == Point.Empty)
				CenterToScreen();

			WindowState = LoaderSetting.WindowsState;
		}

		/// <summary>
		/// Form 상태를 Save 합니다.
		/// </summary>
		private void SaveFormSetting()
		{
			LoaderSetting.WindowsState = WindowState;

			if (WindowState == FormWindowState.Normal)
			{
				// 화면상에 보여줄 수 없는 좌표는 기본값을 Save한다.
				Rectangle workingArea = Screen.GetWorkingArea(this);
				int clientX = DesktopBounds.X;
				int clientY = DesktopBounds.Y;
				int clientWidth = Size.Width;
				int clientHeight = Size.Height;
				int screenWidth = workingArea.X + workingArea.Width;
				int screenHeight = workingArea.Y + workingArea.Height;

				if ((clientX < workingArea.X || clientX > screenWidth)						// 현재 Screen의 Left, Right에서 벗어난 경우
					|| (clientY < workingArea.Y || clientY > screenHeight)					// 현재 Screen의 Top, Bottom에서 벗어난 경우
					|| ((clientX + clientWidth) > screenWidth)								// Client Form이 현재 우측으로 벗어난 경우
					|| ((clientY + clientHeight) > screenHeight))							// Client Form이 현재 하단에서 벗어난 경우
				{
					LoaderSetting.LoadLastSavedSetting();
					LoaderSetting.Save();

					return;
				}

				LoaderSetting.Size = Size;
				LoaderSetting.Location = Location;
			}
			else if (WindowState == FormWindowState.Minimized)								// 최소화 되었을 경우 기본값으로 Load한다.
				LoaderSetting.LoadDefault();

			LoaderSetting.Save();
		}
		#endregion

		private void Loader_Load(object sender, EventArgs e)
		{
			LoadFormSetting();
			LoadxDocManagerSetting();
		}

		private void Loader_Shown(object sender, EventArgs e)
		{
			// TODO: 초기화 한다는 Timer 메세지 출력
			// by KIMKIWON\xyz37 in 2008년 1월 17일 목요일 오후 2:38

			// NOTICE: 현재는 메뉴가 먼저 초기화 되어야 순서가 결정된다.
			// by KIMKIWON\xyz37 in 2008년 1월 17일 목요일 오후 2:41
			// 메뉴 초기화
			InitMenuTree();

			// 즐겨찾기 초기화
			InitFavoriteTree();
			// 검색된 메뉴 초기화
			InitSearchResultTree();
						
			// 메뉴의 Auto Hide 상태를 처리한다.
			SetMenuPin();

			// ProcessBar를 초기화 한다.
			InitProgressBar();
			// ProgressBar를 숨긴다
			HideProcessBar();

			if (IsExistFavoritesMenu() == true)
				SetActivePane(DockablePaneKeys.Favorite);

			// UNDONE: Infragistics control upgrade를 위해
			// by KIMKIWON\xyz37 in 2008년 1월 3일 목요일 오후 9:38
			//UpgradeInfragistics();
		}

		#region Upgrade Infragistics Controls
		private void UpgradeInfragistics()
		{
			List<string> gacFiles = new List<string>();
			string path = string.Format(@"{0}\Infragistics", FrameworkSetup.Instance.InstallFolder);
			gacFiles.AddRange(Directory.GetFiles(path, "*.v7.2.dll"));
			gacFiles.AddRange(Directory.GetFiles(path, "*.v7.2.Design.dll"));

			UnRegisterGac(gacFiles);
			DeleteFiles(gacFiles);
		}

		private void DeleteFiles(List<string> gacFiles)
		{
			try
			{
				foreach (string file in gacFiles)
					File.Delete(file);
			}
			catch	
			{
			}
		}

		private void UnRegisterGac(List<string> gacFiles)
		{
			for (int i = 0; i < gacFiles.Count; i++)
			{
				// 성공 실패 여부는 검사하지 않는다(해제시 강제 해제를 시킨다)
				GacManager.UnRegister(gacFiles[i]);
			}
		}

		private void RegisterGac(List<string> gacFiles)
		{
			for (int i = 0; i < gacFiles.Count; i++)
			{
				// 성공 실패 여부는 검사하지 않는다(등록시 강제 등록을 시킨다)
				GacManager.Register(gacFiles[i]);
			}
		}
		#endregion

		private void Loader_KeyDown(object sender, KeyEventArgs e)
		{
			// Menu Pane Shortcut 작동
			if (e.Control == true)
			{
				// Ctrl+~ 면 즐겨찾기
				if (e.KeyValue == 192)
					SetActivePane(DockablePaneKeys.Favorite);

				// Ctrl+Alt+~ 면 검색된 메뉴
				if (e.Alt == true && e.KeyValue == 192)
					SetActivePane(DockablePaneKeys.SearchResult);

				// Ctrl+1, 2, 3, ... 면 생성된 메인 메뉴(Ascii >> 1(49), 2(50), 3(51), ... 9(57)
				if (e.KeyValue >= 49 && e.KeyValue <= 57)
					SetActivePane((DockablePaneKeys)Enum.Parse(typeof(DockablePaneKeys), Enum.GetName(typeof(DockablePaneKeys), (int)e.KeyCode - 48)));
			}
		}

		private void Loader_FormClosing(object sender, FormClosingEventArgs e)
		{
			BeforeFormClosingAction();

			using (MessageSettings mSettings = new MessageSettings())
			{
				bool showConfirmMessage = LoaderSetting.IsShowCloseConfirmMessage;

				if (showConfirmMessage == true
						&& MessageBox.Show(mSettings["QuestCloseForm"], 
                               mSettings["QuestCloseFormCaption"], 
                               MessageBoxButtons.YesNo, 
                               MessageBoxIcon.Question) == DialogResult.No)
				{
					e.Cancel = true;

					return;
				}
				else
				{
					// MESSAGE MessageAgent.Instance.Logout();
					DeleteMenuFileInUserId();
				}
			}
		}

		private void DeleteMenuFileInUserId()
		{
			if (DebugMode == false)
			{
				// 해당 메뉴 파일을 제거 한다.
				string menuConfigFilePath = LoaderSetting.MenuConfigFilePath;
				string filename = Path.GetFileNameWithoutExtension(menuConfigFilePath);
				string filePath = menuConfigFilePath.Replace(filename, string.Format("{0}_{1}", filename, UserId));

				// menus_UserId.conifg
				if (File.Exists(filePath) == true)
					try
					{
						File.Delete(filePath);
					}
					catch
					{						
					}
			}
		}

		/// <summary>
		/// Form을 종료하기 전에 상태를 저장합니다.
		/// </summary>
		private void BeforeFormClosingAction()
		{
			ProcessBar.GetInstance(this).Dispose();

			// Docking Manager 상태를 저장한다.
			SavexDocManagerSetting();
			// Form 상태를 저장한다.
			SaveFormSetting();

			using (MessageSettings mSettings = new MessageSettings())
			{
				// 즐겨찾기의 Node가 0이면 메뉴를 잘못 로드 한것이므로 즐겨찾기를 저장하지 않는다.
				if (_xTreeFavorites.Nodes.Count > 0)
				{
					MenuManager menuManager = new MenuManager(UserId);

					if (menuManager.SaveFavoriteMenusToLocalFile(_xTreeFavorites) == false)
						MessageBox.Show(mSettings["FavoritesMenuCouldNotSaved"], mSettings["Failure"], MessageBoxButtons.OK, MessageBoxIcon.Warning);		// 실패
					else
						StatusbarMessage = mSettings["FavoritesMenuSaveSuccess"];		// 즐겨찾기 메뉴가 저장되었습니다.
				}
			}
		}

		#endregion		

		#region ProgressBar
		private void InitProgressBar()
		{
			ThemeSettings themeSettings = new ThemeSettings();

			xProgressBar.Appearance.BackColor = themeSettings.ControlBackColor;
			xProgressBar.FillAppearance.BackColor = themeSettings.ProcessBarGradientStartColor;
			xProgressBar.FillAppearance.BackColor2 = themeSettings.ProcessBarGradientEndColor;
			xProgressBar.FillAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			xProgressBar.SegmentWidth = 5;
			xProgressBar.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.SegmentedPartial;
			xProgressBar.TextVisible = false;
			xProgressBar.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;

			ProcessBar processBar = ProcessBar.GetInstance(this);

			processBar.UltraProgressBar = xProgressBar;
			processBar.Interval = LoaderSetting.ProcessBarInterval;
		}

		private void ShowProcessBar()
		{
			xStatusBar.Panels[1].Visible = true;
			xStatusBar.Update();
		}

		private void HideProcessBar()
		{
			xStatusBar.Panels[1].Visible = false;
			xStatusBar.Update();
		}
		#endregion

		#region IStatusbarMessage 멤버

		string IStatusbarMessage.StatusbarMessage
		{
			get
			{
				return xStatusBar.Panels[0].Text;
			}
			set
			{
				xStatusBar.Panels[0].Text = value;
			}
		}

		#endregion

		#region For Message Test
		// MESSAGE: For Test in Messanger 개발 후 정리
		// by KIMKIWON\xyz37 in 2008년 11월 12일 수요일 오후 1:20
		/*
		#region IMessageAgent 멤버

		MessageAgent IMessageAgent.NewInstance
		{
			get
			{
				return MessageAgent.Instance;
			}
		}

		#endregion

		#region IXMessage 멤버

		XMessage IXMessage.XMessage
		{
			get
			{
				return (MessageAgent.Instance as IXMessage).XMessage;
			}
		}

		#endregion
		*/
		#endregion


		#region IMainToolbarAction 멤버

		void IMainToolbarAction.OnFavoritesClickAction()
		{
			SetActivePane(DockablePaneKeys.Favorite);
			// NOTICE: 즐겨찾기 버튼을 누를 경우 현재 세팅을 저장 한다.
			// by KIMKIWON\xyz37 in 2007년 10월 18일 목요일 오후 8:44
			MenuManager menuManager = new MenuManager(_userId);

			menuManager.SaveFavoriteMenusToLocalFile(_xTreeFavorites);
		}

		void IMainToolbarAction.OnFindMenuClickAction(string menuName)
		{
			_xTreeSearchResult.Nodes.Clear();
			SetRootNode(_xTreeSearchResult, _rootNode);
			SearchResultAction(_rootNode, menuName);
		}

		void IMainToolbarAction.OnAppUpdateClickAction()
		{
			BeforeFormClosingAction();

			using (MessageSettings mSettings = new MessageSettings())
			{
				DialogResult result = MessageBox.Show(mSettings["QuestAppUpdate"], mSettings["QuestAppUpdateCaption"], MessageBoxButtons.YesNo, MessageBoxIcon.Question);

				if (result == DialogResult.Yes)
				{
					// 열린 Form에 대허서 종료 처리 한다.
					if (ConfigWrapper.Loader.EnableMessageService == true && _mdiTabs != null)
					{
						foreach (MdiTab tab in _mdiTabs)
						{
							MenuView menuView = tab.Tag as MenuView;

							// MESSAGE if (menuView != null)
							// MESSAGE MessageAgent.Instance.SendFormCloseMessage(menuView.FormId, GetMenuPath(menuView.FormDesc), GetVersion(menuView.FormDesc));
						}
					}

					// MESSAGE MessageAgent.Instance.AppUpdate();

					// UserName은 UserName(UserId) 형태이므로 변경해줘야 한다.
					ExchangeForm.Call(PlantId,
						string.Format("{0}{1}{2}",
							UserId,
							ExchangeForm.ArgumentDelimiter,
							UserName.Replace(' ', ExchangeForm.ArgumentDelimiter).Replace(string.Format("({0})", UserId), string.Empty)),
						ExchangeFormType.FXWinUpdater);
				}
			}
		}

		void IMainToolbarAction.OnLogoutClickAction()
		{
			BeforeFormClosingAction();

			using (MessageSettings mSettings = new MessageSettings())
			{
				DialogResult result = MessageBox.Show(mSettings["QuestLogout"], mSettings["QuestLogoutCaption"], MessageBoxButtons.YesNo, MessageBoxIcon.Question);

				if (result == DialogResult.Yes)
				{
					// MESSAGE MessageAgent.Instance.Logout();

					ExchangeForm.Call(PlantId, UserId, ExchangeFormType.FXWinLogin);
				}
			}
		}

		void IMainToolbarAction.OnCloseClickAction()
		{
			Close();
		}

		void IMainToolbarAction.OnHelpClickAction()
		{
			//
		}

		private void SearchResultAction(UltraTreeNode targetNode, string searchMenuText)
		{
			bool isCaseSensitive = LoaderSetting.SearchMenuCaseSensitive;
			bool isContains = false;
			List<UltraTreeNode> allNodes = GetAllNodes(targetNode);

			foreach (UltraTreeNode node in allNodes)
			{
				if (IsRootNode(node) == true)
					continue;

				if (isCaseSensitive == true)
					isContains = node.Text.Contains(searchMenuText);
				else
					isContains = node.Text.ToLower().Contains(searchMenuText.ToLower());

				if (isContains == true)
				{
					string rootNodeKey = node.RootNode.Key;
					UltraTreeNode targetRootNode = _xTreeSearchResult.Nodes[rootNodeKey];
					UltraTreeNode searchedNode =  node.Clone() as UltraTreeNode;

					if (searchedNode != null)
					{
						searchedNode.Nodes.Clear();
						targetRootNode.Nodes.Add(searchedNode);
					}

					_xTreeSearchResult.Nodes[0].RootNode.ExpandAll(ExpandAllType.Always);
				}
			}

			SetActivePane(DockablePaneKeys.SearchResult);
		}
		#endregion

		#region MainToolbar Action
		private void FavoritesClick()
		{
			SetActivePane(DockablePaneKeys.Favorite);
			// NOTICE: 즐겨찾기 버튼을 누를 경우 현재 세팅을 저장 한다.
			// by KIMKIWON\xyz37 in 2007년 10월 18일 목요일 오후 8:44
			MenuManager menuManager = new MenuManager(_userId);

			menuManager.SaveFavoriteMenusToLocalFile(_xTreeFavorites);
		}

		private void FindMenuClick(string menuName)
		{
			_xTreeSearchResult.Nodes.Clear();
			SetRootNode(_xTreeSearchResult, _rootNode);
			SearchResultAction(_rootNode, menuName);
		}

		private void AppUpdateClick()
		{
			BeforeFormClosingAction();

			using (MessageSettings mSettings = new MessageSettings())
			{
				DialogResult result = MessageBox.Show(mSettings["QuestAppUpdate"], mSettings["QuestAppUpdateCaption"], MessageBoxButtons.YesNo, MessageBoxIcon.Question);

				if (result == DialogResult.Yes)
				{
					// 열린 Form에 대허서 종료 처리 한다.
					if (ConfigWrapper.Loader.EnableMessageService == true && _mdiTabs != null)
					{
						foreach (MdiTab tab in _mdiTabs)
						{
							MenuView menuView = tab.Tag as MenuView;

							// MESSAGE if (menuView != null)
							// MESSAGE MessageAgent.Instance.SendFormCloseMessage(menuView.FormId, GetMenuPath(menuView.FormDesc), GetVersion(menuView.FormDesc));
						}
					}

					// MESSAGE MessageAgent.Instance.AppUpdate();

					// UserName은 UserName(UserId) 형태이므로 변경해줘야 한다.
					ExchangeForm.Call(PlantId,
						string.Format("{0}{1}{2}",
							UserId,
							ExchangeForm.ArgumentDelimiter,
							UserName.Replace(' ', ExchangeForm.ArgumentDelimiter).Replace(string.Format("({0})", UserId), string.Empty)),
						ExchangeFormType.FXWinUpdater);
				}
			}
		}

		private void LogoutClick()
		{
			BeforeFormClosingAction();

			using (MessageSettings mSettings = new MessageSettings())
			{
				DialogResult result = MessageBox.Show(mSettings["QuestLogout"], mSettings["QuestLogoutCaption"], MessageBoxButtons.YesNo, MessageBoxIcon.Question);

				if (result == DialogResult.Yes)
				{
					// MESSAGE MessageAgent.Instance.Logout();

					ExchangeForm.Call(PlantId, UserId, ExchangeFormType.FXWinLogin);
				}
			}
		}

		private void CloseFormClick()
		{
			Close();
		}

		private void HelpClick()
		{
			//			
		}
		#endregion
				
		#region IStandardToolbarAction 멤버
		void IStandardToolbarAction.OnNewClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnNewClickAction();
		}

		void IStandardToolbarAction.OnSaveClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnSaveClickAction();
		}

		void IStandardToolbarAction.OnSearchClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnSearchClickAction();
		}

		void IStandardToolbarAction.OnRunClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnRunClickAction();
		}

		void IStandardToolbarAction.OnCancelClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnCancelClickAction();
		}

		void IStandardToolbarAction.OnDeleteClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnDeleteClickAction();
		}

		void IStandardToolbarAction.OnAddRowClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnAddRowClickAction();
		}

		void IStandardToolbarAction.OnDelRowClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnDelRowClickAction();
		}

		void IStandardToolbarAction.OnExcelClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnExcelClickAction();
		}

		void IStandardToolbarAction.OnPreviewClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnPreviewClickAction();
		}

		void IStandardToolbarAction.OnPrintClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnPrintClickAction();
		}

		void IStandardToolbarAction.OnExitClickAction()
		{
			IStandardToolbarAction standardToolbarAction = ActiveTab.Form as IStandardToolbarAction;

			if (standardToolbarAction != null)
				standardToolbarAction.OnExitClickAction();
		}
		#endregion

		#region IStandardToolbarProperty 멤버

		bool IStandardToolbarProperty.EnableAddRowButton
		{
			get
			{
				return _toolbarManager.EnableAddRowButton;
			}
			set
			{
				_toolbarManager.EnableAddRowButton = value;
			}
		}

		bool IStandardToolbarProperty.EnableCancelButton
		{
			get
			{
				return _toolbarManager.EnableCancelButton;
			}
			set
			{
				_toolbarManager.EnableCancelButton = value;
			}
		}

		bool IStandardToolbarProperty.EnableDelRowButton
		{
			get
			{
				return _toolbarManager.EnableDelRowButton;
			}
			set
			{
				_toolbarManager.EnableDelRowButton = value;
			}
		}

		bool IStandardToolbarProperty.EnableDeleteButton
		{
			get
			{
				return _toolbarManager.EnableDeleteButton;
			}
			set
			{
				_toolbarManager.EnableDeleteButton = value;
			}
		}

		bool IStandardToolbarProperty.EnableExcelButton
		{
			get
			{
				return _toolbarManager.EnableExcelButton;
			}
			set
			{
				_toolbarManager.EnableExcelButton = value;
			}
		}

		bool IStandardToolbarProperty.EnableExitButton
		{
			get
			{
				return _toolbarManager.EnableExitButton;
			}
			set
			{
				_toolbarManager.EnableExitButton = value;
			}
		}

		bool IStandardToolbarProperty.EnableNewButton
		{
			get
			{
				return _toolbarManager.EnableNewButton;
			}
			set
			{
				_toolbarManager.EnableNewButton = value;
			}
		}

		bool IStandardToolbarProperty.EnablePreviewButton
		{
			get
			{
				return _toolbarManager.EnablePreviewButton;
			}
			set
			{
				_toolbarManager.EnablePreviewButton = value;
			}
		}

		bool IStandardToolbarProperty.EnablePrintButton
		{
			get
			{
				return _toolbarManager.EnablePrintButton;
			}
			set
			{
				_toolbarManager.EnablePrintButton = value;
			}
		}

		bool IStandardToolbarProperty.EnableRunButton
		{
			get
			{
				return _toolbarManager.EnableRunButton;
			}
			set
			{
				_toolbarManager.EnableRunButton = value;
			}
		}

		bool IStandardToolbarProperty.EnableSaveButton
		{
			get
			{
				return _toolbarManager.EnableSaveButton;
			}
			set
			{
				_toolbarManager.EnableSaveButton = value;
			}
		}

		bool IStandardToolbarProperty.EnableSearchButton
		{
			get
			{
				return _toolbarManager.EnableSearchButton;
			}
			set
			{
				_toolbarManager.EnableSearchButton = value;
			}
		}

		string IStandardToolbarProperty.MenuPath
		{
			get
			{
				return _toolbarManager.MenuPath;
			}
			set
			{
				_toolbarManager.MenuPath = value;
			}
		}

		#endregion
	}
}
