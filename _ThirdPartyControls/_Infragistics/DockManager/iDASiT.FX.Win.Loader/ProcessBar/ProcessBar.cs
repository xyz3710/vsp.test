﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Loader.ProcessBar
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 11월 26일 월요일 오후 5:53
/*	Purpose		:	현재 진행중인 프로세스에 대한 진행 상태를 표시합니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Infragistics.Win.UltraWinProgressBar;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace iDASiT.FX.Win.Loader
{
	/// <summary>
	/// 현재 진행중인 프로세스에 대한 진행 상태를 표시합니다.
	/// </summary>
	public class ProcessBar : IProcessBar, IDisposable
	{
		#region Constants
		private const int MAX_VALUE = 100;
		#endregion

		#region Delegates
		private delegate void ProcessBarCrossThreadDelegate(int value);
		#endregion

		#region Fields
		private Thread _currentThread;
		private UltraProgressBar _ultraProgressBar;
		private bool _abort;
		private Form _form;
		private int _interval;
		#endregion

		#region Use Singleton Pattern
		private static ProcessBar _newInstance;

		#region Constructor for Single Ton Pattern
		/// <summary>
		/// Constructor for Single Ton Pattern
		/// </summary>
		/// <returns></returns>
		private ProcessBar(Form form)
		{
			_newInstance = null;
			_form = form;

			InitProgressBar();
		}
		#endregion

		#region GetInstance
		/// <summary>
		/// Create unique instance
		/// </summary>
		/// <param name="form">ProgressBar가 있는 Form</param>
		/// <returns></returns>
		public static ProcessBar GetInstance(Form form)
		{
			if (_newInstance == null)
				_newInstance = new ProcessBar(form);

			return _newInstance;
		}
		#endregion
		#endregion

		#region Properties
		/// <summary>
		/// UltraProgressBar를 구하거나 설정합니다.
		/// </summary>
		public UltraProgressBar UltraProgressBar
		{
			get
			{
				return _ultraProgressBar;
			}
			set
			{
				_ultraProgressBar = value;
				_ultraProgressBar.Minimum = 0;
				_ultraProgressBar.Maximum = MAX_VALUE;
			}
		}

		/// <summary>
		/// ProcessBar의 움직이는 Interval(milisecond)을 구하거나 설정합니다.
		/// </summary>
		public int Interval
		{
			get
			{
				return _interval;
			}
			set
			{
				_interval = value;
			}
		}

		/// <summary>
		/// 현재 ProcessBar의 중지 상태를 구합니다.
		/// </summary>
		public bool IsAborted
		{
			get
			{
				return _abort;
			}
		}
		#endregion

		#region Private methods
		private void InitProgressBar()
		{
			_ultraProgressBar = new UltraProgressBar();

			_ultraProgressBar.Maximum = MAX_VALUE;
		}

		private void SetProgressbarValue(int value)
		{
			if (_ultraProgressBar.InvokeRequired == true)
			{
				ProcessBarCrossThreadDelegate processBarCrossThreadDelegate = new ProcessBarCrossThreadDelegate(SetProgressbarValue);

				if (_form != null)
				{
					try
					{
						_form.Invoke(processBarCrossThreadDelegate, value);
					}
					catch (ObjectDisposedException)
					{
					}
				}
			}
			else
			{
				_ultraProgressBar.Value = value;
				_ultraProgressBar.Update();
			}
		}

		private void StartThread()
		{
			_abort = false;

			if (_currentThread == null)
				_currentThread = new Thread(ProcessThread);
			
			if (_currentThread != null)
			{
				if (_currentThread.ThreadState == ThreadState.Unstarted)
				{
					_currentThread.Name = "ProcessBar";
					_currentThread.IsBackground = true;
					_currentThread.Start();
					_currentThread.Join(10);
				}
			}
		}

		private void StopThread()
		{
			_abort = true;

			if (_currentThread != null)
				_currentThread = null;
		}

		private void ProcessThread()
		{
			bool reverse = false;
			int cnt = 0;

			while (_abort == false)
			{
				if (cnt == MAX_VALUE)
					reverse = true;
				else if (cnt == 0)
					reverse = false;

				if (reverse == false)
					cnt++;
				else
					cnt--;

				SetProgressbarValue(cnt);
				Thread.Sleep(_interval == 0 ? Convert.ToInt32(cnt * 0.01) : _interval);
			}
		}
		#endregion

		#region IProcessBar 멤버

		/// <summary>
		/// ProcessBar를 시작합니다.
		/// </summary>
		public void Start()
		{
			//StartThread();
			Cursor.Current = AdvancedCursors.Create("aero_busy.ani");
		}

		/// <summary>
		/// ProcessBar를 종료합니다.
		/// </summary>
		public void Stop()
		{
			//StopThread();
			Cursor.Current = Cursors.Default;
		}

		internal class AdvancedCursors
		{
			[System.Runtime.InteropServices.DllImport("User32.dll")]
			private static extern IntPtr LoadCursorFromFile(String str);

			/// <summary>
			/// System에서 지원하는 Cursor를 Load할 수 있습니다.
			/// </summary>
			/// <param name="filename"></param>
			/// <returns></returns>
			public static Cursor Create(string filename)
			{
				if (File.Exists(filename) == false)
					return Cursors.Default;

				IntPtr hCursor = LoadCursorFromFile(filename);

				if (!IntPtr.Zero.Equals(hCursor))
					return new Cursor(hCursor);
				else
					throw new ApplicationException("Could not create cursor from file " + filename);
			}
		}
		#endregion

		#region IDisposable 멤버

		/// <summary>
		/// 사용중인 모든 리소스를 정리합니다.
		/// </summary>
		public void Dispose()
		{
			StopThread();
		}

		#endregion
	}
}
