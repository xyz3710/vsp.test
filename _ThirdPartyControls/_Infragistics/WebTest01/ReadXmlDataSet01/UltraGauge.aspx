﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UltraGauge.aspx.cs" Inherits="UltraGauge" %>

<%@ Register TagPrefix="igGauge" Namespace="Infragistics.WebUI.UltraWebGauge" Assembly="Infragistics2.WebUI.UltraWebGauge.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igGaugeProp" Namespace="Infragistics.UltraGauge.Resources" Assembly="Infragistics2.WebUI.UltraWebGauge.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igmisc" Namespace="Infragistics.WebUI.Misc" Assembly="Infragistics2.WebUI.Misc.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>제목 없음</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <igGauge:UltraGauge ID="xGauge" runat="server" Height="250px" StyleSetName="" StyleSetPath=""
            StyleSheetDirectory="" Width="250px">
        </igGauge:UltraGauge>
        <igmisc:WebPanel ID="WebPanel1" runat="server">
        </igmisc:WebPanel>
    
    </div>
    </form>
</body>
</html>
