﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Infragistics.UltraGauge.Resources;
using System.Drawing;
using Infragistics.WebUI.UltraWebGrid;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
		if (IsPostBack == false)
		{

			xWebGrid.DataSource = readXml("yield");
			//xWebGrid.DataBind();

			xWebGridExcelExporter1.CellExported += new Infragistics.WebUI.UltraWebGrid.ExcelExport.CellExportedEventHandler(xWebGridExcelExporter1_CellExported);
			//xWebGrid.Columns[0].MergeCells = true;

 			// Enable AJAX
			xWebGrid.Browser = BrowserLevel.Xml;
			xWebGrid.DisplayLayout.LoadOnDemand = LoadOnDemand.Xml;
			xWebGrid.DisplayLayout.ViewType = ViewType.Hierarchical;

			xWebGrid.InitializeDataSource += new Infragistics.WebUI.UltraWebGrid.InitializeDataSourceEventHandler(xWebGrid_InitializeDataSource);
		}

   }

	void xWebGrid_InitializeDataSource(object sender, Infragistics.WebUI.UltraWebGrid.UltraGridEventArgs e)
	{
		xWebGrid.DataSource = readXml("yield");
	}

	public DataSet readXml(string fileName)
	{
		DataSet dataSet = new DataSet(fileName);
		string filePath = @"D:\VSP\_Infragistics\WebTest01\ReadXmlDataSet01\App_Data\";

		dataSet.ReadXmlSchema(string.Format("{0}{1}.xsd", filePath, fileName));
		dataSet.ReadXml(string.Format("{0}{1}.xml", filePath, fileName), XmlReadMode.ReadSchema);

		//return dataSet.Tables[0];
		return dataSet; 
	}

	void xWebGridExcelExporter1_CellExported(object sender, Infragistics.WebUI.UltraWebGrid.ExcelExport.CellExportedEventArgs e)
	{
		if (e.GridColumn.Key != "ProdDate")
			return;
		else
		{
			if ((e.Value != null) &&(e.GridRow != null) && ((DateTime)(e.Value)) < DateTime.Now)
			{
				Infragistics.Excel.IWorksheetCellFormat cfCellFmt;
				int iRdex = e.CurrentRowIndex;
				int iCdex = e.CurrentColumnIndex;

				// Obtain reference to CellFormat object for current cell.
				cfCellFmt = e.CurrentWorksheet.Rows[iRdex].Cells[iCdex].CellFormat;

				// Set format property for the font color to red.
				cfCellFmt.Font.Color = System.Drawing.Color.Red;

				// Apply the formatting, this step minimizes the number of
				// Worksheet Font objects that need to be instantiated.
				e.CurrentWorksheet.Rows[iRdex].Cells[iCdex].CellFormat.SetFormatting(cfCellFmt);
			}
		}

	}

	protected void Button1_Click(object sender, EventArgs e)
	{
		xWebGridExcelExporter1.DownloadName = "test.xls";
		xWebGridExcelExporter1.WorksheetName = "쉬트1";
		xWebGridExcelExporter1.Export(xWebGrid);
		xWebGridExcelExporter1.ExportMode = Infragistics.WebUI.UltraWebGrid.ExcelExport.ExportMode.InBrowser;
	}
}
