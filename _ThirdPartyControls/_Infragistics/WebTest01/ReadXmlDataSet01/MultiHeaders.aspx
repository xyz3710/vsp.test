﻿<%@ Page language="c#" Inherits="WebSamplesCS.WebGrid.MultiHeader.MultiHeaders" CodeFile="MultiHeaders.aspx.cs" %>

<%@ Register TagPrefix="igtbl" Namespace="Infragistics.WebUI.UltraWebGrid" Assembly="Infragistics2.WebUI.UltraWebGrid.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>MultiHeaders</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR" />
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../../webSample_style.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form method="post" runat="server" action="multiheaders.aspx">
        <igtbl:UltraWebGrid ID="UltraWebGrid1" runat="server" Width="627px" Height="156px"
            UseAccessibleHeader="False">
            <DisplayLayout RowHeightDefault="21px" Version="3.00" SelectTypeRowDefault="Single"
                BorderCollapseDefault="Separate" Name="UltraWebGrid1" TableLayout="Fixed" CellClickActionDefault="Edit"
                AllowUpdateDefault="Yes">
                <AddNewBox>
                    <Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">


<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>
                    </Style>
                        
                </AddNewBox>
                <Pager>
                    <Style BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">   

<BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
</BorderDetails>
                    </Style>                        
                </Pager>
                <HeaderStyleDefault Font-Size="X-Small" Font-Names="Arial" Font-Bold="True" BorderStyle="Solid"
                    ForeColor="#E1E8F5" BackColor="#11459E" CustomRules="background-image:url(/ig_common/images/Office2003BlueBG.png);background-repeat:repeat-x;">
                    <BorderDetails ColorTop="173, 197, 235" WidthLeft="1px" WidthTop="1px" ColorLeft="173, 197, 235">
                    </BorderDetails>
                </HeaderStyleDefault>
                <FrameStyle Width="627px" BorderWidth="1px" Font-Size="8pt" Font-Names="Verdana" BorderStyle="Solid"
                    Height="156px">
                </FrameStyle>
                <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                </FooterStyleDefault>
                <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                </EditCellStyleDefault>
                <SelectedRowStyleDefault ForeColor="White" BackColor="#F09D21" CustomRules="background-image:url(/ig_common/images/Office2003SelRow.png);background-repeat:repeat-x;">
                </SelectedRowStyleDefault>
                <RowStyleDefault BorderWidth="1px" BorderColor="Black" BorderStyle="Solid">
                    <Padding Left="3px"></Padding>
                    <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                </RowStyleDefault>
                <ActivationObject BorderColor="" BorderWidth="">
                </ActivationObject>
            </DisplayLayout>
            <Bands>
                <igtbl:UltraGridBand>
                    <AddNewRow View="NotSet" Visible="NotSet">
                    </AddNewRow>
                </igtbl:UltraGridBand>
            </Bands>
        </igtbl:UltraWebGrid>
        &nbsp;&nbsp;&nbsp;<br />
        &nbsp;
    </form>
</body>
</html>

