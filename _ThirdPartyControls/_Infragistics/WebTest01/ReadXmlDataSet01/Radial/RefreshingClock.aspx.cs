using Infragistics.UltraGauge.Resources;

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class WebGauge_Samples_Radial_RefreshingClock : System.Web.UI.Page
{

    protected void intervalPicker_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UltraGauge1.RefreshInterval = int.Parse(this.intervalPicker.SelectedItem.Text);
    }
    private void UpdateGauge()
    {
        RadialGauge gauge = this.UltraGauge1.Gauges[0] as RadialGauge;
        gauge.Scales[0].Markers[0].Value = DateTime.Now;
        gauge.Scales[1].Markers[0].Value = DateTime.Now.Minute;
        gauge.Scales[1].Markers[1].Value = DateTime.Now.Second;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.UpdateGauge();
    }
}
