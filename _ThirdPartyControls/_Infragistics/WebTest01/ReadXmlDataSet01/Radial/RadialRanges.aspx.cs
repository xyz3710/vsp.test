using Infragistics.UltraGauge.Resources;

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class WebGauge_Samples_Radial_RadialRanges : System.Web.UI.Page
{

    protected void editors_ValueChanged(object sender, Infragistics.WebUI.WebDataInput.ValueChangeEventArgs e)
    {
        RadialGaugeRange range = this.GetSelectedRange();
        range.StartValue = this.startValueEditor.ValueDouble;
        range.EndValue = this.endValueEditor.ValueDouble;
        range.InnerExtentStart = this.innerExtentStartEditor.ValueDouble;
        range.InnerExtentEnd = this.innerExtentEndEditor.ValueDouble;
        range.OuterExtent = this.outerExtentEditor.ValueDouble;
    }
    protected void rangePicker_SelectedIndexChanged(object sender, EventArgs e)
    {
        RadialGaugeRange range = this.GetSelectedRange();
        this.startValueEditor.ValueDouble = Convert.ToDouble(range.StartValue);
        this.endValueEditor.ValueDouble = Convert.ToDouble(range.EndValue);
        this.innerExtentStartEditor.ValueDouble = range.InnerExtentStart;
        this.innerExtentEndEditor.ValueDouble = range.InnerExtentEnd;
        this.outerExtentEditor.ValueDouble = range.OuterExtent;
    }
    private RadialGaugeRange GetSelectedRange()
    {
        RadialGauge gauge = this.UltraGauge1.Gauges[0] as RadialGauge;
        RadialGaugeRange range;
        switch (this.rangePicker.SelectedItem.Text)
        {
            case "green":
                return gauge.Scales[0].Ranges[0];
            case "orange":
                return gauge.Scales[0].Ranges[1];
            default:
            case "red":
                return gauge.Scales[0].Ranges[2];
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.rangePicker_SelectedIndexChanged(this, EventArgs.Empty);
        }
    }
}
