﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RadialRanges.aspx.cs" Inherits="WebGauge_Samples_Radial_RadialRanges" %>

<%@ Register TagPrefix="igGaugeProp" Namespace="Infragistics.UltraGauge.Resources" Assembly="Infragistics2.WebUI.UltraWebGauge.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igmisc" Namespace="Infragistics.WebUI.Misc" Assembly="Infragistics2.WebUI.Misc.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igGauge" Namespace="Infragistics.WebUI.UltraWebGauge" Assembly="Infragistics2.WebUI.UltraWebGauge.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igGaugeProp" Namespace="Infragistics.UltraGauge.Resources" Assembly="Infragistics2.WebUI.UltraWebGauge.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igtxt" Namespace="Infragistics.WebUI.WebDataInput" Assembly="Infragistics2.WebUI.WebDataInput.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Radial Gauge Ranges</title>
    <link href="../../../webSample_style.css" type="text/css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
        <igmisc:WebPanel id="WebPanel1" runat="server" ToolTip="Click arrow to the right to expand and close this description." CssClass="descriptionPanel">
			<PanelStyle CssClass="descriptionPanelContent" />
			<Header Text="Radial Gauge Ranges" TextAlignment="Left">
				<ExpandedAppearance>
					<Style CssClass="descriptionPanelHeader" />
				</ExpandedAppearance>
			</Header>
			<Template>
			    Radial Gauge Ranges
			</Template>
		</igmisc:WebPanel>
		<div>
		    Select a Range: <asp:DropDownList runat="server" ID="rangePicker" OnSelectedIndexChanged="rangePicker_SelectedIndexChanged" AutoPostBack="True" >
                <asp:ListItem>green</asp:ListItem>
                <asp:ListItem>orange</asp:ListItem>
                <asp:ListItem>red</asp:ListItem>
            </asp:DropDownList>
		</div>
		<div>
		    StartValue: <igtxt:WebNumericEdit runat="server" ID="startValueEditor" OnValueChange="editors_ValueChanged" >
                <SpinButtons Display="OnRight" />
            </igtxt:WebNumericEdit>
		</div>
		<div>
		    EndValue: <igtxt:WebNumericEdit runat="server" ID="endValueEditor" OnValueChange="editors_ValueChanged" >
                <SpinButtons Display="OnRight" />
            </igtxt:WebNumericEdit>
		</div>
		<div>
		    InnerExtentStart: <igtxt:WebNumericEdit runat="server" ID="innerExtentStartEditor" OnValueChange="editors_ValueChanged" >
                <SpinButtons Display="OnRight" />
            </igtxt:WebNumericEdit>
		</div>
		<div>
		    InnerExtentEnd: <igtxt:WebNumericEdit runat="server" ID="innerExtentEndEditor" OnValueChange="editors_ValueChanged" >
                <SpinButtons Display="OnRight" />
            </igtxt:WebNumericEdit>
		</div>
		<div>
		    OuterExtent: <igtxt:WebNumericEdit runat="server" ID="outerExtentEditor" OnValueChange="editors_ValueChanged" >
                <SpinButtons Display="OnRight" />
            </igtxt:WebNumericEdit>
		</div>
		<div><asp:Button runat="server" ID="submitButton" Text="Submit Changes" /></div>
		<igGauge:UltraGauge runat="server" ID="UltraGauge1">
            <Gauges>
                <igGaugeProp:RadialGauge>
                <BrushElements />
                  <Scales>
                    <igGaugeProp:RadialGaugeScale EndAngle="450" StartAngle="90">
                      <Ranges>
                        <igGaugeProp:RadialGaugeRange InnerExtentStart="90" Key="green" EndValue="7" OuterExtent="90" StartValue="0">
                          <BrushElements>
                            <igGaugeProp:SolidFillBrushElement Color="Green" />
                          </BrushElements>
                          <StrokeElement Color="Black" />
                        </igGaugeProp:RadialGaugeRange>
                        <igGaugeProp:RadialGaugeRange Key="orange" InnerExtentEnd="60" EndValue="9" OuterExtent="90" StartValue="7">
                          <BrushElements>
                            <igGaugeProp:SolidFillBrushElement Color="Orange" />
                          </BrushElements>
                          <StrokeElement Color="Black" />
                        </igGaugeProp:RadialGaugeRange>
                        <igGaugeProp:RadialGaugeRange InnerExtentStart="60" Key="red" InnerExtentEnd="50" EndValue="10" OuterExtent="90" StartValue="9">
                          <BrushElements>
                            <igGaugeProp:SolidFillBrushElement Color="Red" />
                          </BrushElements>
                          <StrokeElement Color="Black" />
                        </igGaugeProp:RadialGaugeRange>
                      </Ranges>
                      <Markers>
                        <igGaugeProp:RadialGaugeNeedle MidWidth="15" Value="2.3">
                          <Anchor>
                            <BrushElements>
                              <igGaugeProp:RadialGradientBrushElement FocusScales="0, 0" CenterPoint="0, 0" />
                            </BrushElements>
                            <StrokeElement Color="MidnightBlue" />
                          </Anchor>
                          <BrushElements>
                            <igGaugeProp:SimpleGradientBrushElement EndColor="150, 255, 255, 0" StartColor="150, 150, 150, 0" GradientStyle="BackwardDiagonal" />
                          </BrushElements>
                          <StrokeElement Color="Black" />
                        </igGaugeProp:RadialGaugeNeedle>
                      </Markers>
                      <Axes>
                        <igGaugeProp:NumericAxis />
                      </Axes>
                      <Labels PreTerminal="1" SpanMaximum="20" Font="Arial, 40pt">
                        <BrushElements>
                          <igGaugeProp:SolidFillBrushElement Color="White" />
                        </BrushElements>
                        <Shadow Depth="1">
                          <BrushElements>
                            <igGaugeProp:SolidFillBrushElement Color="Black" />
                          </BrushElements>
                        </Shadow>
                      </Labels>
                    </igGaugeProp:RadialGaugeScale>
                  </Scales>
                  <Dial>
                    <BrushElements>
                      <igGaugeProp:RadialGradientBrushElement FocusScales="0.1, 0.1" CenterPoint="50, -25" SurroundColor="LightSlateGray" CenterColor="WhiteSmoke" />
                    </BrushElements>
                    <StrokeElement Color="Black" />
                  </Dial>
                </igGaugeProp:RadialGauge>
            </Gauges>		
            <DeploymentScenario FilePath="..\..\GaugeImages" ImageURL="../../GaugeImages/Gauge_#SEQNUM(100).#EXT" />
        </igGauge:UltraGauge>
    </form>
</body>
</html>
