﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RadialPresetGallery.aspx.cs" Inherits="WebGauge_Samples_Radial_RadialPresetGallery" %>

<%@ Register TagPrefix="igGaugeProp" Namespace="Infragistics.UltraGauge.Resources" Assembly="Infragistics2.WebUI.UltraWebGauge.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igmisc" Namespace="Infragistics.WebUI.Misc" Assembly="Infragistics2.WebUI.Misc.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Register TagPrefix="igGauge" Namespace="Infragistics.WebUI.UltraWebGauge" Assembly="Infragistics2.WebUI.UltraWebGauge.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Radial Gauge Presets</title>
    <link href="../../../webSample_style.css" type="text/css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
        <igmisc:WebPanel id="WebPanel1" runat="server" ToolTip="Click arrow to the right to expand and close this description." CssClass="descriptionPanel">
			<PanelStyle CssClass="descriptionPanelContent" />
			<Header Text="Radial Gauge Presets" TextAlignment="Left">
				<ExpandedAppearance>
					<Style CssClass="descriptionPanelHeader" />
				</ExpandedAppearance>
			</Header>
			<Template>
			    Radial Gauge Presets
			</Template>
		</igmisc:WebPanel>
		<div>
		    <asp:DropDownList runat="server" ID="presetPicker" OnSelectedIndexChanged="presetPicker_SelectedIndexChanged" AutoPostBack="true" />
		</div>
		<igGauge:UltraGauge runat="server" ID="UltraGauge1" >
            <DeploymentScenario FilePath="..\..\GaugeImages" ImageURL="../../GaugeImages/Gauge_#SEQNUM(100).#EXT" />
        </igGauge:UltraGauge>
    </form>
</body>
</html>
