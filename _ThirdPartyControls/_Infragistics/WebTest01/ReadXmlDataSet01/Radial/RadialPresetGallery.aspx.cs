using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class WebGauge_Samples_Radial_RadialPresetGallery : System.Web.UI.Page
{
    private string _PresetsPath;
    private string PresetsPath { get { return this._PresetsPath; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        this._PresetsPath = this.MapPath("..\\..\\Presets\\Radial");
        if (!this.IsPostBack)
        {
            DirectoryInfo presetDir = new DirectoryInfo(this.PresetsPath);
            FileInfo[] presetFiles = presetDir.GetFiles("*.xml");
            foreach (FileInfo currentPresetFile in presetFiles)
            {
                this.presetPicker.Items.Add(currentPresetFile.Name);
            }
            this.presetPicker.SelectedIndex = 0;
            this.presetPicker_SelectedIndexChanged(this, EventArgs.Empty);
        }
    }
    protected void presetPicker_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UltraGauge1.Gauges.Clear();
        this.UltraGauge1.LoadPreset(Path.Combine(this.PresetsPath, this.presetPicker.SelectedItem.Text), true);
    }
}
