using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Infragistics.WebUI.UltraWebGrid;

namespace WebSamplesCS.WebGrid.MultiHeader
{
    /// <summary>
    /// Summary description for MultiHeaders.
    /// </summary>
    public partial class MultiHeaders : System.Web.UI.Page
    {
        protected System.Data.OleDb.OleDbDataAdapter oleDbDataAdapter1;
        protected System.Data.OleDb.OleDbCommand oleDbSelectCommand1;
        protected System.Data.OleDb.OleDbCommand oleDbInsertCommand1;
        protected System.Data.OleDb.OleDbCommand oleDbUpdateCommand1;
        protected System.Data.OleDb.OleDbCommand oleDbDeleteCommand1;
        protected System.Data.OleDb.OleDbConnection oleDbConnection1;
    
        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
			if (IsPostBack == false)
			{
				DataSet ds = new DataSet();

				ds.Tables.Add();

				ds.Tables[0].Columns.Add("TitleOfCourtesy", typeof(string));
				ds.Tables[0].Columns.Add("FirstName", typeof(string));
				ds.Tables[0].Columns.Add("LastName", typeof(string));
				ds.Tables[0].Columns.Add("Title", typeof(string));
				ds.Tables[0].Columns.Add("Address", typeof(string));
				ds.Tables[0].Columns.Add("City", typeof(string));

				ds.Tables[0].Rows.Add(new object[] { "Mr.1", "F1", "L1", "T1", "Addr1", "City1" });
				ds.Tables[0].Rows.Add(new object[] { "Mr.2", "F2", "L2", "T2", "Addr2", "City2" });
				ds.Tables[0].Rows.Add(new object[] { "Mr.3", "F3", "L3", "T3", "Addr3", "City3" });
				ds.Tables[0].Rows.Add(new object[] { "Mr.4", "F4", "L4", "T4", "Addr4", "City4" });
				ds.Tables[0].Rows.Add(new object[] { "Mr.5", "F5", "L5", "T5", "Addr5", "City5" });
				ds.Tables[0].Rows.Add(new object[] { "Mr.6", "F6", "L6", "T6", "Addr6", "City6" });
				ds.Tables[0].Rows.Add(new object[] { "Mr.7", "F7", "L7", "T7", "Addr7", "City7" });

				this.UltraWebGrid1.DataSource = ds;

				// merge된 컬럼 header를 Fix 시킨다
				UltraWebGrid1.DisplayLayout.StationaryMargins = Infragistics.WebUI.UltraWebGrid.StationaryMargins.Header;

				// 지정한 컬럼 Header를 Fix 시킨다
				UltraWebGrid1.DisplayLayout.UseFixedHeaders = true;
//				UltraWebGrid1.Bands[0].Columns[0].Header.Fixed = true;
//				UltraWebGrid1.Bands[0].Columns[1].Header.Fixed = true;
//				UltraWebGrid1.Bands[0].Columns[2].Header.Fixed = true;
			}
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {   			
			this.UltraWebGrid1.InitializeLayout += new Infragistics.WebUI.UltraWebGrid.InitializeLayoutEventHandler(this.UltraWebGrid1_InitializeLayout);
            this.UltraWebGrid1.InitializeDataSource += new Infragistics.WebUI.UltraWebGrid.InitializeDataSourceEventHandler(this.UltraWebGrid1_InitializeDataSource);
        }
        #endregion

        private void UltraWebGrid1_InitializeDataSource(object sender, Infragistics.WebUI.UltraWebGrid.UltraGridEventArgs e)
        {
        
		}

        private void UltraWebGrid1_InitializeLayout(object sender, Infragistics.WebUI.UltraWebGrid.LayoutEventArgs e)
        {
//			e.Layout.Bands[0].Columns.FromKey("Title").Move(0);
//			e.Layout.Bands[0].Columns.FromKey("TitleOfCourtesy").Move(1);
//			e.Layout.Bands[0].Columns.FromKey("FirstName").Move(2);
//			e.Layout.Bands[0].Columns.FromKey("LastName").Move(3);
//			e.Layout.Bands[0].Columns.FromKey("Address").Move(4);
//			e.Layout.Bands[0].Columns.FromKey("City").Move(5);

            e.Layout.Bands[0].Columns.FromKey("TitleOfCourtesy").Header.Caption = "Greeting";

			// All the Headers for bound columns initialize to OriginX = 0.  Since we want them
            // to appear below the added column headers we are going to need to move them down a level
            foreach(Infragistics.WebUI.UltraWebGrid.UltraGridColumn c in e.Layout.Bands[0].Columns)
            {
                c.Header.RowLayoutColumnInfo.OriginY = 1;
            }

			// Binding 되지 않는 Header를 추가한다.("Employee's Name")
            ColumnHeader ch = new ColumnHeader(true);
            ch.Caption = "Employee's Name";            
            // set the origin to be on the top most level of the header
			ch.RowLayoutColumnInfo.OriginX = 0;
			ch.RowLayoutColumnInfo.OriginY = 0;
			// extend the newly added header over 3 columns
            ch.RowLayoutColumnInfo.SpanX = 3;

            // Span 되지 않는 컬럼은 해당 행 수 만큼 spanning 시킨다.
            e.Layout.Bands[0].HeaderLayout.Add(ch);

			ch = e.Layout.Bands[0].Columns.FromKey("Title").Header;

			//ch.RowLayoutColumnInfo.OriginX = 0;		// 지정하지 않아도 ch 변수에 컬럼의 위치 정보가 저장되어 있다.
			ch.RowLayoutColumnInfo.OriginY = 0;
			ch.RowLayoutColumnInfo.SpanY = 2;

			ColumnHeader chFullAddress = new ColumnHeader(true);

			chFullAddress.Caption = "Full Address";
			chFullAddress.RowLayoutColumnInfo.OriginX = 4;
			chFullAddress.RowLayoutColumnInfo.OriginY = 0;
			chFullAddress.RowLayoutColumnInfo.SpanX = 2;
			e.Layout.Bands[0].HeaderLayout.Add(chFullAddress);

		}
    }
}

 