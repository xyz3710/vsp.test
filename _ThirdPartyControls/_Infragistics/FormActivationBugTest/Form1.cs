using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FormActivationBugTest {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();

            //((Infragistics.Win.UltraWinToolbars.ControlContainerTool)this.ultraToolbarsManager1.Tools["ControlContainerTool1"]).Control = new TextBox();
            //((Infragistics.Win.UltraWinToolbars.ControlContainerTool)this.ultraToolbarsManager1.Tools["ControlContainerTool1"]).Control.Dock = DockStyle.Fill;

            for (int index = 0; index < 3; index++) {
                Form frm = new Form();
                frm.Text = frm.Name = "Form_Index_" + index.ToString();
                frm.Activated += new EventHandler(frm_Activated);
                frm.Deactivate += new EventHandler(frm_Deactivate);
                frm.MdiParent = this;
                frm.Show();
            }
        }

        void frm_Deactivate(object sender, EventArgs e) {
            System.Diagnostics.Debug.WriteLine(((Form)sender).Name + " deactivated...");
            System.Diagnostics.Debug.WriteLine("=====================================");
            DeactiveTools();
        }

        void frm_Activated(object sender, EventArgs e) {
            System.Diagnostics.Debug.WriteLine(((Form)sender).Name + " activated...");
            DeactiveTools();
            if (((Form)sender).Name == "Form_Index_2") {
                this.ultraToolbarsManager1.Tools["ControlContainerTool1"].SharedProps.Visible = true;
                this.ultraToolbarsManager1.Ribbon.Tabs["ribbon2"].Groups["ribbonGroup1"].Visible = true;
                this.ultraToolbarsManager1.Ribbon.ContextualTabGroups[0].Visible = true;
            }
        }

        void DeactiveTools() {
            this.ultraToolbarsManager1.Tools["ControlContainerTool1"].SharedProps.Visible = false;
            this.ultraToolbarsManager1.Tools["ButtonTool2"].SharedProps.Visible = false;
            this.ultraToolbarsManager1.Ribbon.Tabs["ribbon2"].Groups["ribbonGroup1"].Visible = false;
            this.ultraToolbarsManager1.Ribbon.ContextualTabGroups[0].Visible = false;
        }
    }
}