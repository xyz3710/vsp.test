﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Microsoft.Office.Tools.Excel;

// 어셈블리의 일반 정보는 다음 특성 집합을 통해 제어됩니다. 
// 어셈블리와 관련된 정보를 수정하려면
// 이 특성 값을 변경하십시오.
[assembly: AssemblyTitle("ExcelWorkbook1")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ISET-DA")]
[assembly: AssemblyProduct("ExcelWorkbook1")]
[assembly: AssemblyCopyright("Copyright © ISET-DA 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible을 false로 설정하면 이 어셈블리의 형식이 COM 구성 요소에 
// 노출되지 않습니다. COM에서 이 어셈블리의 형식에 액세스하려면 
// 해당 형식에 대해 ComVisible 특성을 true로 설정하십시오.
[assembly: ComVisible(false)]

// 이 프로젝트가 COM에 노출되는 경우 다음 GUID는 typelib의 ID를 나타냅니다.
[assembly: Guid("b9648848-2aa9-4d1c-909b-d32bc5b22ac0")]

// 어셈블리의 버전 정보는 다음 네 가지 값으로 구성됩니다.
//
//      주 버전
//      부 버전 
//      빌드 번호
//      수정 버전
//
// 모든 값을 지정하거나 아래와 같이 '*'를 사용하여 빌드 번호 및 수정 버전이 자동으로 
// 지정되도록 할 수 있습니다.
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

// 
// ExcelLocale1033 특성은 Excel 개체 모델에 전달되는 로캘을
// 제어합니다. ExcelLocale1033을 True로 설정하면 Excel 개체 모델이 모든 로캘에서 
// 동일하게 동작하며, 이러한 동작은 Visual Basic for Applications의 동작과 
// 일치합니다. ExcelLocale1033을 False로 설정하면 사용자가 다른 로캘 설정을 사용할 때
// Excel 개체 모델이 다르게 동작하며, 이러한 동작은 Visual Studio Tools for Office 버전 2003의 
// 동작과 일치합니다. 이 경우 수식 이름 및 날짜 형식과 같은 로캘 관련 정보에 예기치 않은 
// 결과가 발생할 수 있습니다.
// 
[assembly: ExcelLocale1033(true)]
