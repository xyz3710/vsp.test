using System;
using System.Collections.Generic;
using System.Text;
using AnyFactory.Process;
using Infragistics.Win.UltraWinEditors;
using AnyFactory.FX.Win.Configuration;

namespace AnyFactory.Win.Mes
{
	public class InputManualData
	{
		public static void SetLotDate(ref Lot lot, UltraDateTimeEditor ultraDateTimeEditor)
		{
			SetLotDate(ref lot, ultraDateTimeEditor.DateTime);
		}

		public static void SetLotDate(ref Lot lot, DateTime dateTime)
		{
			List<Lot> lots = new List<Lot>();

			lots.Add(lot);
			SetLotDate(ref lots, dateTime);
		}

		public static void SetLotDate(ref List<Lot> lots, UltraDateTimeEditor ultraDateTimeEditor)
		{
			SetLotDate(ref lots, ultraDateTimeEditor.DateTime);
		}

		public static void SetLotDate(ref List<Lot> lots, DateTime dateTime)
		{
			string addedTime = DateTime.Now.ToString(string.Format("{0}HHmmssfff", dateTime.ToShortDateString().Replace("-", string.Empty)));
						
			foreach (Lot lot in lots)
				lot.WhenRequested = addedTime;
		}
	}
}
