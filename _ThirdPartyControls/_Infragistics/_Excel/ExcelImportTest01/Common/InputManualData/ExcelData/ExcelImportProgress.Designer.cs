namespace AnyFactory.Win.Mes
{
	partial class ExcelImportProgressForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.xpbProgress = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
			this.xgridExcel = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.xbtnExportExcel = new Infragistics.Win.Misc.UltraButton();
			this.pnlContainerBody.SuspendLayout();
			this.tlpMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgridExcel)).BeginInit();
			this.SuspendLayout();
			// 
			// pnlContainerBody
			// 
			this.pnlContainerBody.BackColor = System.Drawing.Color.White;
			this.pnlContainerBody.Controls.Add(this.tlpMain);
			this.pnlContainerBody.Size = new System.Drawing.Size(494, 700);
			// 
			// tlpMain
			// 
			this.tlpMain.BackColor = System.Drawing.Color.White;
			this.tlpMain.ColumnCount = 1;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Controls.Add(this.xpbProgress, 0, 2);
			this.tlpMain.Controls.Add(this.xgridExcel, 0, 1);
			this.tlpMain.Controls.Add(this.xbtnExportExcel, 0, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(3, 3);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 3;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tlpMain.Size = new System.Drawing.Size(486, 692);
			this.tlpMain.TabIndex = 0;
			// 
			// xpbProgress
			// 
			appearance3.BackColor = System.Drawing.Color.White;
			appearance3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(163)))), ((int)(((byte)(255)))));
			appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance3.ForeColor = System.Drawing.Color.Black;
			this.xpbProgress.Appearance = appearance3;
			this.xpbProgress.BackColor = System.Drawing.Color.White;
			this.xpbProgress.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded1Etched;
			this.xpbProgress.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(209)))), ((int)(((byte)(255)))));
			this.xpbProgress.FillAppearance = appearance4;
			this.xpbProgress.Location = new System.Drawing.Point(3, 663);
			this.xpbProgress.Name = "xpbProgress";
			this.xpbProgress.Size = new System.Drawing.Size(480, 26);
			this.xpbProgress.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.SegmentedPartial;
			this.xpbProgress.TabIndex = 1;
			this.xpbProgress.Text = "[Formatted]";
			this.xpbProgress.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// xgridExcel
			// 
			appearance1.BackColor = System.Drawing.Color.White;
			appearance1.BorderColor = System.Drawing.Color.Gray;
			this.xgridExcel.DisplayLayout.Appearance = appearance1;
			this.xgridExcel.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridExcel.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			this.xgridExcel.DisplayLayout.GroupByBox.Hidden = true;
			appearance2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(176)))), ((int)(((byte)(198)))));
			appearance2.BorderColor = System.Drawing.Color.Orange;
			this.xgridExcel.DisplayLayout.Override.ActiveRowAppearance = appearance2;
			this.xgridExcel.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridExcel.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridExcel.DisplayLayout.Override.BorderStyleSpecialRowSeparator = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridExcel.DisplayLayout.Override.BorderStyleSummaryFooter = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridExcel.DisplayLayout.Override.BorderStyleSummaryValue = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridExcel.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xgridExcel.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
			this.xgridExcel.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
			this.xgridExcel.DisplayLayout.Override.FixedCellSeparatorColor = System.Drawing.Color.OrangeRed;
			this.xgridExcel.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
			appearance9.TextHAlignAsString = "Center";
			appearance9.TextVAlignAsString = "Middle";
			this.xgridExcel.DisplayLayout.Override.HeaderAppearance = appearance9;
			this.xgridExcel.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xgridExcel.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance5.BackColor = System.Drawing.Color.LightBlue;
			this.xgridExcel.DisplayLayout.Override.HotTrackCellAppearance = appearance5;
			appearance6.BorderColor = System.Drawing.Color.Orange;
			this.xgridExcel.DisplayLayout.Override.HotTrackRowAppearance = appearance6;
			appearance7.BackColor = System.Drawing.Color.LightBlue;
			this.xgridExcel.DisplayLayout.Override.HotTrackRowCellAppearance = appearance7;
			appearance8.BackColor = System.Drawing.Color.Silver;
			this.xgridExcel.DisplayLayout.Override.HotTrackRowSelectorAppearance = appearance8;
			appearance10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(226)))), ((int)(((byte)(244)))));
			this.xgridExcel.DisplayLayout.Override.RowAlternateAppearance = appearance10;
			appearance16.BackColor = System.Drawing.Color.White;
			this.xgridExcel.DisplayLayout.Override.RowAppearance = appearance16;
			appearance11.TextHAlignAsString = "Center";
			appearance11.TextVAlignAsString = "Middle";
			this.xgridExcel.DisplayLayout.Override.RowSelectorAppearance = appearance11;
			appearance12.TextHAlignAsString = "Center";
			appearance12.TextVAlignAsString = "Middle";
			this.xgridExcel.DisplayLayout.Override.RowSelectorHeaderAppearance = appearance12;
			this.xgridExcel.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			this.xgridExcel.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow;
			appearance13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(208)))), ((int)(((byte)(226)))));
			this.xgridExcel.DisplayLayout.Override.SpecialRowSeparatorAppearance = appearance13;
			this.xgridExcel.DisplayLayout.Override.SpecialRowSeparatorHeight = 3;
			appearance14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(229)))), ((int)(((byte)(239)))));
			appearance14.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			this.xgridExcel.DisplayLayout.Override.SummaryFooterAppearance = appearance14;
			this.xgridExcel.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(229)))), ((int)(((byte)(239)))));
			appearance15.TextVAlignAsString = "Middle";
			this.xgridExcel.DisplayLayout.Override.SummaryValueAppearance = appearance15;
			this.xgridExcel.DisplayLayout.UseFixedHeaders = true;
			this.xgridExcel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xgridExcel.Location = new System.Drawing.Point(3, 35);
			this.xgridExcel.Name = "xgridExcel";
			this.xgridExcel.Size = new System.Drawing.Size(480, 622);
			this.xgridExcel.TabIndex = 0;
			this.xgridExcel.Text = "ultraGrid1";
			this.xgridExcel.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnUpdate;
			this.xgridExcel.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// xbtnExportExcel
			// 
			this.xbtnExportExcel.BackColorInternal = System.Drawing.Color.White;
			this.xbtnExportExcel.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.xbtnExportExcel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xbtnExportExcel.Location = new System.Drawing.Point(3, 3);
			this.xbtnExportExcel.Name = "xbtnExportExcel";
			this.xbtnExportExcel.Size = new System.Drawing.Size(480, 26);
			this.xbtnExportExcel.TabIndex = 2;
			this.xbtnExportExcel.Text = "&Excel Export";
			this.xbtnExportExcel.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnExportExcel.Click += new System.EventHandler(this.xbtnExportExcel_Click);
			// 
			// ExcelImportProgressForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(494, 700);
			this.Name = "ExcelImportProgressForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ExcelDataProgress Form";
			this.ThemeStyle = AnyFactory.FX.Win.ThemeStyle.Theme1;
			this.pnlContainerBody.ResumeLayout(false);
			this.tlpMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgridExcel)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private Infragistics.Win.UltraWinGrid.UltraGrid xgridExcel;
		private Infragistics.Win.UltraWinProgressBar.UltraProgressBar xpbProgress;
		private Infragistics.Win.Misc.UltraButton xbtnExportExcel;
	}
}
