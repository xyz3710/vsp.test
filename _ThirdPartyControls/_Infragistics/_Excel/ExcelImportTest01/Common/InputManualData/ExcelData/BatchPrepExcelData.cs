using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AnyFactory.Win.Mes
{
	public class BatchPrepExcelData : ExcelData
	{
		private string _preHeater;
		public string PreHeater
		{
			get
			{
				return _preHeater;
			}
			set
			{
				_preHeater = value;
			}
		}

		private string _filamentType;
		public string FilamentType
		{
			get
			{
				return _filamentType;
			}
			set
			{
				_filamentType = value;
			}
		}

		private int _filamentCount;
		public int FilamentCount
		{
			get
			{
				return _filamentCount;
			}
			set
			{
				_filamentCount = value;
			}
		}

		private int _bridgeCount;
		public int BridgeCount
		{
			get
			{
				return _bridgeCount;
			}
			set
			{
				_bridgeCount = value;
			}
		}

		private int _carbonChuckCount;
		public int CarbonChuckCount
		{
			get
			{
				return _carbonChuckCount;
			}
			set
			{
				_carbonChuckCount = value;
			}
		}
	}
}
