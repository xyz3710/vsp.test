using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AnyFactory.Win.Mes
{
	public class LossExcelData : ExcelData
	{
		private string _lossCodeId;
		private double _lossQtyAmount;

		#region Properties
		/// <summary>
		/// LossCodeId를 구하거나 설정합니다.
		/// </summary>
		public string LossCodeId
		{
			get
			{
				return _lossCodeId;
			}
			set
			{
				_lossCodeId = value;
			}
		}

		/// <summary>
		/// LossQtyAmount를 구하거나 설정합니다.
		/// </summary>
		public double LossQtyAmount
		{
			get
			{
				return _lossQtyAmount;
			}
			set
			{
				_lossQtyAmount = value;
			}
		}

		#endregion
        
	}
}
