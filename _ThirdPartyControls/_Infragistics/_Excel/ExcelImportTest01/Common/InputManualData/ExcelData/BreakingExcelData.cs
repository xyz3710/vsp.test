using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AnyFactory.Win.Mes
{
	public class BreakingExcelData : ExcelData
	{
		private string _etcLossCodeId;
		private double _etcLossQtyAmount;
		private string _lossCodeId;
		private double _lossQtyAmount;
		private double _bonusQtyAmount;

		#region Properties
		/// <summary>
		/// EtcLossCodeId를 구하거나 설정합니다.
		/// </summary>
		public string EtcLossCodeId
		{
			get
			{
				return _etcLossCodeId;
			}
			set
			{
				_etcLossCodeId = value;
			}
		}

		/// <summary>
		/// EtcLossQtyAmount를 구하거나 설정합니다.
		/// </summary>
		public double EtcLossQtyAmount
		{
			get
			{
				return _etcLossQtyAmount;
			}
			set
			{
				_etcLossQtyAmount = value;
			}
		}

		/// <summary>
		/// LossCodeId를 구하거나 설정합니다.
		/// </summary>
		public string LossCodeId
		{
			get
			{
				return _lossCodeId;
			}
			set
			{
				_lossCodeId = value;
			}
		}

		/// <summary>
		/// LossQtyAmount를 구하거나 설정합니다.
		/// </summary>
		public double LossQtyAmount
		{
			get
			{
				return _lossQtyAmount;
			}
			set
			{
				_lossQtyAmount = value;
			}
		}

		/// <summary>
		/// BonusQtyAmount를 구하거나 설정합니다.
		/// </summary>
		public double BonusQtyAmount
		{
			get
			{
				return _bonusQtyAmount;
			}
			set
			{
				_bonusQtyAmount = value;
			}
		}

		#endregion
        
	}
}
