using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using AnyFactory.FX.Win.Configuration;

namespace AnyFactory.Win.Mes
{
	[Serializable]
	public class ExcelData
	{
		#region Delegates
		public delegate bool ExcelImportAction<T>(T t) where T : ExcelData;
		public delegate void AfterSuccessAction();
		#endregion

		#region Properties
		private DateTime _workDate;
		public DateTime WorkDate
		{
			get
			{
				return _workDate;
			}
			set
			{
				_workDate = value;
			}
		}

		private string _lotId;
		public string LotId
		{
			get
			{
				return _lotId;
			}
			set
			{
				_lotId = value;
			}
		}

		private string _userId;
		public string UserId
		{
			get
			{
				return _userId;
			}
			set
			{
				_userId = value;
			}
		}
		#endregion

		#region Private methods
		private static string GetSelectCommand(Type type, string sheetName)
		{
			StringBuilder sb = new StringBuilder();
			
			foreach (PropertyInfo pi in type.GetProperties())
			{
				if (sb.ToString() == string.Empty)
				{
					sb.Append("SELECT ");
					sb.AppendFormat("\r\n	{0} ", pi.Name);
				}
				else
					sb.AppendFormat("\r\n	,{0} ", pi.Name);
			}

			sb.AppendFormat("\r\n	FROM [{0}$] ", sheetName);

			return sb.ToString();
		}
		#endregion

		#region Public methods
		/// <summary>
		/// Excel에서 ExcelData type의 List collection을 구합니다.(default Sheet name : Sheet1)
		/// </summary>
		/// <typeparam name="T">ExcelData type</typeparam>
		/// <param name="fileName">fileName</param>
		/// <returns></returns>
		public static List<T> GetExcelData<T>(string fileName)
			where T : ExcelData
		{
			string sheetName = "Sheet1";

			return GetExcelData<T>(fileName, sheetName);
		}

		private void GetWorkSheet()
		{
			string fileName = @"D:\z_DCC\M02\생산량 수동 입력\Breaking_데이타입력용.xls";
			string[] sheetNames = new string[]{
				"00067B02S",
				"00077B01S",
				"00087B01S",
				"00097B02S",
				"00107B03S",
				"00117B02S",
				"00127B03S",
				"00147B01S",
				"00157B03S",
				"00187B03S",
				"00167C02S",
				"00197C03S",
				"00207C01S",
				"00227C04S",
				"00237C03S",
				"00247C02S",
				"00257C01S",
				"00287C03S",
				"00297C01S",
				"00307C04S",
				"00317C05S",
				"00327C03S",
				"00337C01S",
				"00347C02S",
				"00357C04S",
				"00367C01S",
				"00377C05S",
				"00397C02S"};

			ExcelDataSource excelDataSource = new ExcelDataSource(fileName);
			excelDataSource.ShowHeader = false;
			Dictionary<string, DateTime> dic = new Dictionary<string, DateTime>();
			List<CCERemoveExcelData> list = ExcelData.GetExcelData<CCERemoveExcelData>(@"D:\z_DCC\M02\생산량 수동 입력\04. CCERemove.xls");

			foreach (CCERemoveExcelData cCERemoveExcelData in list)
				dic.Add(cCERemoveExcelData.LotId, cCERemoveExcelData.WorkDate);

			FileStream fs = new FileStream(@"D:\z_DCC\M02\생산량 수동 입력\06. BreakingPEBag.txt", FileMode.Create);
			TextWriter tw = new StreamWriter(fs);
			string productId = string.Empty;

			try
			{
				foreach (string sheetName in sheetNames)
				{
                    // TODO : PO, LOT ID 변경사항 적용
					string selectCommand = string.Format("select * FROM [{0}$]", sheetName.Substring(0, 8));
					DataTable dataTable = excelDataSource.GetDataTable(selectCommand);

					foreach (DataRow row in dataTable.Rows)
					{
						string lotId = Convert.ToString(row[1]);

						if (lotId != string.Empty)
						{
							double lotQtyValue = 0d;
							
							if (double.TryParse(row[2].ToString(), out lotQtyValue) == true)
							{
								int seq = 0;

								//Debug.Print(lotId);

								if (int.TryParse(lotId.Substring(9, 3), out seq) == true)
								{
									if (seq >= 1 && seq < 601)
										productId = "DCC-N-S500";
									else if (seq >= 601 && seq < 801)
										productId = "DCC-N-S700";

									tw.WriteLine("{0}\t{1}\t{2}\t{3}", dic[lotId.Substring(0, 9)].ToShortDateString(), lotId, lotQtyValue, productId);
								}
							}
						}
					}
				}
			}
			catch
			{

			}
			finally
			{
				if (tw != null)
					tw.Close();

				if (fs != null)
					fs.Close();                
			}
		}

		/// <summary>
		/// Excel에서 ExcelData type의 List collection을 구합니다.
		/// </summary>
		/// <typeparam name="T">ExcelData type</typeparam>
		/// <param name="fileName">fileName</param>
		/// <param name="sheetName">특정한 Sheet name을 지정 합니다.</param>
		/// <returns></returns>
		public static List<T> GetExcelData<T>(string fileName, string sheetName)
			where T : ExcelData
		{
			List<T> list = new List<T>();
			Type type = typeof(T);
			string selectCommand = GetSelectCommand(type, sheetName);
			ExcelDataSource excelDataSource = new ExcelDataSource(fileName);

			excelDataSource.ShowHeader = true;

			DataTable dataTable = excelDataSource.GetDataTable(selectCommand);
			Dictionary<string, Type> columns = new Dictionary<string, Type>();

			foreach (PropertyInfo pi in type.GetProperties())
				columns.Add(pi.Name, pi.PropertyType);

			foreach (DataRow dataRow in dataTable.Rows)
			{
				T bindedClass = Activator.CreateInstance<T>();
				Type bindType = typeof(T);

				if (dataRow["WorkDate"].ToString() == string.Empty)
					continue;

				foreach (string key in columns.Keys)
				{
					if (columns[key] == typeof(double))
						bindType.InvokeMember(key, BindingFlags.SetProperty, null, bindedClass, new object[] { Math.Round(Convert.ToDouble(dataRow[key]), GlobalFormat.DoubleDecimalCount) });
					else if (columns[key] == typeof(int))
						bindType.InvokeMember(key, BindingFlags.SetProperty, null, bindedClass, new object[] { Convert.ToInt32(dataRow[key]) });
					else if (columns[key] == typeof(DateTime))
						bindType.InvokeMember(key, BindingFlags.SetProperty, null, bindedClass, new object[] { Convert.ToDateTime(dataRow[key]) });
					else
						bindType.InvokeMember(key, BindingFlags.SetProperty, null, bindedClass, new object[] { dataRow[key] });	
				}

				list.Add(bindedClass);
			}

			return list;
		}

		/// <summary>
		/// Excel import용 파일 이름을 구합니다.
		/// </summary>
		/// <param name="fileName">Dialog에서 구해진 fileName을 참조합니다.</param>
		/// <returns>DialogResult 결과를 return합니다.</returns>
		public static DialogResult GetExcelFileNameDialog(ref string fileName)
		{
			DialogResult dialogResult = DialogResult.None;
			OpenFileDialog importExcelDialog = new OpenFileDialog();
			importExcelDialog.Title = "Excel Import";
			importExcelDialog.Filter = "Excel files (*.xls)|*.xls|All files (*.*)|*.*";
			importExcelDialog.FilterIndex = 1;
			importExcelDialog.RestoreDirectory = true;

			dialogResult = importExcelDialog.ShowDialog();

			if (dialogResult == DialogResult.OK)
				fileName = importExcelDialog.FileName;

			return dialogResult;
		}

		/// <summary>
		/// Excel로 부터 자료를 읽어서 자료 만큼 excelImportAction을 실행합니다.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="excelImportAction"></param>
		/// <param name="afterSuccessAction"></param>
		public static void RunFromExcel<T>(ExcelImportAction<T> excelImportAction, AfterSuccessAction afterSuccessAction)
			where T : ExcelData		
		{
			const string TITLE = "Excel import process";
			string excelFilename = string.Empty;

			if (ExcelData.GetExcelFileNameDialog(ref excelFilename) != DialogResult.OK)
				return;

            if (MessageBox.Show("Excel 자료로 부터 실행하시겠습니까?", TITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				return;

			List<T> list = ExcelData.GetExcelData<T>(excelFilename);
			int successCount = 0;
			int failureCount = 0;

			IExcelImportProgress excelImportProgress = new ExcelImportProgressForm();
			
			excelImportProgress.Show();
			excelImportProgress.Preparing<T>(list);

			for (int index = 0; index < list.Count; index++)
			{
				if (excelImportAction(list[index]) == true)
				{
					excelImportProgress.Success<T>(list[index], index + 1);
					successCount++;
				}
				else
				{
					excelImportProgress.Failure<T>(list[index], index + 1);
					failureCount++;
				}
			}

			MessageBox.Show(string.Format("입력 : {0} 건\r\n성공 : {1} 건\r\n실패 : {2} 건", 
                                list.Count, 
                                successCount, 
                                failureCount), TITLE, MessageBoxButtons.OK, MessageBoxIcon.Information);

			if (list.Count == successCount && afterSuccessAction != null)
				afterSuccessAction();
		}
		#endregion
	}
}
