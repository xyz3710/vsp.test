using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AnyFactory.Win.Mes
{
	public class BonusExcelData : ExcelData
	{
		private string _bonusCodeId;
		private double _bonusQtyAmount;

		#region Properties
		/// <summary>
		/// BonusCodeId를 구하거나 설정합니다.
		/// </summary>
		public string BonusCodeId
		{
			get
			{
				return _bonusCodeId;
			}
			set
			{
				_bonusCodeId = value;
			}
		}

		/// <summary>
		/// BonusQtyAmount를 구하거나 설정합니다.
		/// </summary>
		public double BonusQtyAmount
		{
			get
			{
				return _bonusQtyAmount;
			}
			set
			{
				_bonusQtyAmount = value;
			}
		}

		#endregion
        
	}
}
