using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AnyFactory.Win.Mes
{
	public class BreakingBagExcelData : ExcelData
	{
		private double _lotQtyValue;
		private string _productType;

		#region Properties
		/// <summary>
		/// LotQtyValue를 구하거나 설정합니다.
		/// </summary>
		public double LotQtyValue
		{
			get
			{
				return _lotQtyValue;
			}
			set
			{
				_lotQtyValue = value;
			}
		}

		/// <summary>
		/// ProductType를 구하거나 설정합니다.
		/// </summary>
		public string ProductType
		{
			get
			{
				return _productType;
			}
			set
			{
				_productType = value;
			}
		}

		#endregion

	}
}
