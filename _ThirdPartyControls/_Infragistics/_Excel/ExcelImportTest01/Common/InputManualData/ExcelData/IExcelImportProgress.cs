using System;
using System.Collections.Generic;
using System.Text;

namespace AnyFactory.Win.Mes
{
	interface IExcelImportProgress
	{
		void Show();

		void Preparing<T>(List<T> list) where T : ExcelData;

		void Success<T>(T t, int count) where T : ExcelData;

		void Failure<T>(T t, int count) where T : ExcelData;
	}
}
