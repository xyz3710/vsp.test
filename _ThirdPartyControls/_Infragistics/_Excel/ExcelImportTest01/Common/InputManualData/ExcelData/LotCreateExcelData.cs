using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AnyFactory.Win.Mes
{
	[Serializable]
	public class LotCreateExcelData : ExcelData
	{
		private string _batchId;
		public string BatchId
		{
			get
			{
				return _batchId;
			}
			set
			{
				_batchId = value;
			}
		}

		private string _yearMonth;
		public string YearMonth
		{
			get
			{
				return _yearMonth;
			}
			set
			{
				_yearMonth = value;
			}
		}

		private string _eqpId;
		public string EqpId
		{
			get
			{
				return _eqpId;
			}
			set
			{
				_eqpId = value;
			}
		}

		private string _productType;
		public string ProductType
		{
			get
			{
				return _productType;
			}
			set
			{
				_productType = value;
			}
		}
	}
}
