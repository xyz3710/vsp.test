using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AnyFactory.Win.Mes
{
	public class HarvestingExcelData : ExcelData
	{
		private double _cartWeight;
		public double CartWeight
		{
			get
			{
				return _cartWeight;
			}
			set
			{
				_cartWeight = value;
			}
		}

		private double _fallDownCartWeight;
		public double FallDownCartWeight
		{
			get
			{
				return _fallDownCartWeight;
			}
			set
			{
				_fallDownCartWeight = value;
			}
		}
	}
}
