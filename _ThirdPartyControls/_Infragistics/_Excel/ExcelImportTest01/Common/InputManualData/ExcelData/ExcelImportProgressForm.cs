using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AnyFactory.FX.Win;
using AnyFactory.FX.Win.Controls.UltraGridHelper;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using AnyFactory.FX.Win.Configuration;

namespace AnyFactory.Win.Mes
{
	public partial class ExcelImportProgressForm : BaseForm, IExcelImportProgress
	{
		#region Delegates
		private delegate void BindAddNewLotDelegate(UltraGrid ultraGrid, Dictionary<string, object> newRowColumns);
		private delegate void SetProgressbarDelegate(int count);
		#endregion

		#region Constants
		private const string SELECT_KEY = "Sel";
		private const string LOTID_KEY = "LotId";
		private const string COUNT = "Count";
		#endregion

		public ExcelImportProgressForm()
			: base()
		{
			InitializeComponent();
			InitExcelGrid();
		}

		private void xbtnExportExcel_Click(object sender, EventArgs e)
		{
			base.OnExcelClick(new UltraGrid[] { xgridExcel }, new string[] { "Excel Import ���� ���" }, new string[] { "Excel Import" });
		}

		private void InitExcelGrid()
		{
			UltraGridHelperColumn xcolSel = new UltraGridHelperColumn(xgridExcel, SELECT_KEY, "Success", UltraGridColumnStyle.CheckBox, false);
			UltraGridHelperColumn xcolLotId = new UltraGridHelperColumn(xgridExcel, LOTID_KEY, 200);
			UltraGridHelperColumn xcolWorkDate = new UltraGridHelperColumn(xgridExcel, "WorkDate", UltraGridColumnStyle.DateTime);
			UltraGridHelperColumn xcolUserId = new UltraGridHelperColumn(xgridExcel, "UserId", 150);

			#region Summary
			UltraGridBand bandAmount = xgridExcel.DisplayLayout.Bands[0];

			bandAmount.Summaries.Add(SELECT_KEY, SummaryType.Custom, new CustomSummaryCalculator(SELECT_KEY, true), bandAmount.Columns[SELECT_KEY], SummaryPosition.UseSummaryPositionColumn, null);
            bandAmount.Summaries.Add(COUNT, SummaryType.Count, bandAmount.Columns["LotId"]);

			bandAmount.Summaries[SELECT_KEY].Appearance.TextHAlign = HAlign.Center;
			bandAmount.Summaries[COUNT].Appearance.TextHAlign = HAlign.Center;

			bandAmount.Summaries[SELECT_KEY].DisplayFormat = string.Format("{{0:{0}}}", GlobalFormat.IntegerFormatString);
			bandAmount.Summaries[COUNT].DisplayFormat = string.Format("Total:{{0:{0}}}", GlobalFormat.IntegerFormatString);

			// Summary Position
			xgridExcel.DisplayLayout.Override.SummaryDisplayArea = SummaryDisplayAreas.BottomFixed;
			#endregion

			xcolSel.ShowHeaderCheckBox = false;
			xgridExcel.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
			UltraGridHelper.SetGridOptions(xgridExcel, false, false, false, true);
		}

		private void ClearExcelGrid()
		{
			UltraGridHelper.DataBind(xgridExcel, null);
			InitExcelGrid();
		}

		private void BindGrid(List<ExcelData> list)
		{
			ClearExcelGrid();

			UltraGridHelper.DataBind(xgridExcel, list, true);
		}

		private void BindAddNewRow(UltraGrid ultraGrid, Dictionary<string, object> newRowColumns)
		{
			if (ultraGrid.InvokeRequired == true)
			{
				BindAddNewLotDelegate bindAddNewLotDelegate = new BindAddNewLotDelegate(BindAddNewRow);

				Invoke(bindAddNewLotDelegate, ultraGrid, newRowColumns);
			}
			else
			{
				ultraGrid.BeginUpdate();
				UltraGridHelper.AddNewRow(ultraGrid, newRowColumns);
				ultraGrid.PerformAction(UltraGridAction.FirstRowInGrid);
				ultraGrid.Update();
				ultraGrid.EndUpdate();
			}
		}

		private void AddCompeletedInfo(ExcelData excelData, bool result)
		{
			Dictionary<string, object> newRowColumns = new Dictionary<string, object>();

			newRowColumns.Add(SELECT_KEY, result);
			newRowColumns.Add(LOTID_KEY, excelData.LotId);
			newRowColumns.Add("WorkDate", excelData.WorkDate);
			newRowColumns.Add("UserId", excelData.UserId);

			BindAddNewRow(xgridExcel, newRowColumns);			
		}

		private void InitProgressbar(int maximum)
		{
			xpbProgress.BeginUpdate();
			xpbProgress.Minimum = 0;
			xpbProgress.Maximum = maximum;
			xpbProgress.EndUpdate();
		}

		private void SetProgressbar(int count)
		{
			if (xpbProgress.InvokeRequired == true)
            {
				SetProgressbarDelegate setProgressbarDelegate = new SetProgressbarDelegate(SetProgressbar);

				Invoke(setProgressbarDelegate, count);
            }
            else
            {
				xpbProgress.Value = count;
				xpbProgress.Update();
            }
		}

		#region IExcelImportProgress ���

		void IExcelImportProgress.Preparing<T>(List<T> list)
		{
			InitProgressbar(list.Count);
		}

		void IExcelImportProgress.Success<T>(T t, int count)
		{
			AddCompeletedInfo(t, true);
			SetProgressbar(count);
		}

		void IExcelImportProgress.Failure<T>(T t, int count)
		{
			AddCompeletedInfo(t, false);
			SetProgressbar(count);
		}

		#endregion
	}
}
