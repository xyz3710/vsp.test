using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace AnyFactory.Win.Mes
{
	public class CCERemoveExcelData : ExcelData
	{
		private double _cCEWeight;
		public double CCEWeight
		{
			get
			{
				return _cCEWeight;
			}
			set
			{
				_cCEWeight = value;
			}
		}

		private double _sampleWeight;
		public double SampleWeight
		{
			get
			{
				return _sampleWeight;
			}
			set
			{
				_sampleWeight = value;
			}
		}

		private double _metalWeight;
		public double MetalWeight
		{
			get
			{
				return _metalWeight;
			}
			set
			{
				_metalWeight = value;
			}
		}

		private double _filamentWeight;
		public double FilamentWeight
		{
			get
			{
				return _filamentWeight;
			}
			set
			{
				_filamentWeight = value;
			}
		}
	}
}
