#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Infragistics.Win;

namespace Infragistics.Win.Printing
{
	/// <summary>
	/// Control used to select a number of rows and columns.
	/// </summary>
	[ToolboxItem(false)]
	internal class PreviewRowColumnSelector : Control
	{
		#region Constants

		private const int			OuterPadding = 1;
		private const int			InterImageSpacing = 2;
		private const int			ImageBorderWidth = 1;
		private const int			ImagePadding = 3;
		private const int			PreTextSpacingY = 4;

		#endregion //Constants

		#region Member Variables

		private Bitmap					rowColImage;
		private Stream					rowColImageStream;

		private Size					textSize;

		private const int				RowsColumnsDefault = 3;
		private int						rows = RowsColumnsDefault;
		private int						columns = RowsColumnsDefault;

		private int						selectedColumns = 0;
		private int						selectedRows = 0;

		private Rectangle[,]			imgRects;

		private Color					imageBorderColor = Color.Empty;

		private static readonly Color	BackColorDefault = SystemColors.Window;
		private static readonly Color	ForeColorDefault = SystemColors.WindowText;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="PreviewRowColumnSelector"/>
		/// </summary>
		public PreviewRowColumnSelector()
		{
			this.SetStyle(ControlStyles.Selectable, false);
			this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			this.SetStyle(ControlStyles.UserPaint, true);
			this.SetStyle(ControlStyles.FixedHeight, true);
			this.SetStyle(ControlStyles.FixedWidth, true);
			this.SetStyle(ControlStyles.DoubleBuffer, true);
			this.SetStyle(ControlStyles.ResizeRedraw, true);

			this.ResetBackColor();
			this.ResetForeColor();

			this.TabStop = false;

			this.SetSelected(0,0);
		}
		#endregion //Constructor

		#region Properties

		#region Columns
		/// <summary>
		/// Returns or sets the number of image columns displayed.
		/// </summary>
		[DefaultValue(RowsColumnsDefault)]
		public int Columns
		{
			get { return this.columns; }
			set
			{
				if (this.columns == value)
					return;

				if (value < 1)
					throw new ArgumentOutOfRangeException();

				this.columns = value;

				this.DirtyImageRects();
			}
		}
		#endregion //Columns

		#region ImageBorderColor
		/// <summary>
		/// Returns or sets the color used to render the border around the images.
		/// </summary>
		[DefaultValue(typeof(Color), "")]
		public Color ImageBorderColor
		{
			get { return this.imageBorderColor; }
			set 
			{ 
				if (value == this.imageBorderColor)
					return;

				this.imageBorderColor = value; 

				if (this.IsHandleCreated)
					this.Invalidate();
			}
		}

		private Color ImageBorderColorResolved 
		{
			get
			{
				Color color = this.imageBorderColor;

				if (color.IsEmpty)
					color = Office2003Colors.PopupMenuBorder;

				if (color.IsKnownColor)
					color = Color.FromArgb(color.ToArgb());

				return color;
			}
		}
		#endregion //ImageBorderColor

		#region RowColImage
		private Image RowColImage
		{
			get
			{
				if (this.rowColImage == null)
				{
					this.rowColImageStream = typeof(PreviewRowColumnSelector).Assembly.GetManifestResourceStream(typeof(PreviewRowColumnSelector), "MultiplePagesImage.bmp");

					Debug.Assert(this.rowColImageStream != null, "Unable to access the RowColImage!");

					if (this.rowColImageStream != null)
					{
						this.rowColImage = (Bitmap)Bitmap.FromStream(this.rowColImageStream);

						Debug.Assert(this.rowColImage != null, "Unable to create an image from the resource stream!");

						if (this.rowColImage != null)
							this.rowColImage.MakeTransparent(Color.Magenta);
					}
				}

				return this.rowColImage;
			}
		}
		#endregion //RowColImage

		#region RowColImageSize
		private Size RowColImageSize
		{
			get { return this.RowColImage.Size; }
		}
		#endregion //RowColImageSize

		#region Rows
		/// <summary>
		/// Returns or sets the number of image rows displayed.
		/// </summary>
		[DefaultValue(RowsColumnsDefault)]
		public int Rows
		{
			get { return this.rows; }
			set
			{
				if (this.rows == value)
					return;

				if (value < 1)
					throw new ArgumentOutOfRangeException();

				this.rows = value;

				this.DirtyImageRects();
			}
		}
		#endregion //Rows

		#region Selection
		/// <summary>
		/// Returns a size indicating the number of selected columns and rows.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Size Selection
		{
			get { return new Size(this.selectedColumns, this.selectedRows); }
		}
		#endregion //Selection

		#region TextSize
		private Size TextSize
		{
			get
			{
				if (this.textSize == Size.Empty)
				{
					Graphics g = DrawUtility.GetCachedGraphics(this);

					try
					{
						this.textSize = Size.Ceiling(g.MeasureString(this.GetDisplayText(10,10), this.Font, PointF.Empty, null));
					}
					finally
					{
						DrawUtility.ReleaseCachedGraphics(g);
					}
				}

				return this.textSize;
			}
		}
		#endregion //TextSize

		#endregion //Properties

		#region Methods

		#region DirtyImageRects
		private void DirtyImageRects()
		{
			this.imgRects = null;

			this.SetSelected(0,0);

			// force a resize
			if (this.Created)
				this.Width += 1;
		}
		#endregion //DirtyImageRects

		#region DirtyTextSize
		private void DirtyTextSize()
		{
			this.textSize = Size.Empty;

			if (this.Created)
				this.Width += 1;
		}
		#endregion //DirtyTextSize

		#region GetDisplayText
		private string GetDisplayText()
		{
			if (this.selectedColumns < 1 ||
				this.selectedRows < 1)
				return Infragistics.Shared.SR.GetString("PreviewRowColSelection_Cancel"); //"Cancel";

			return this.GetDisplayText(this.selectedColumns, this.selectedRows);
		}

		private string GetDisplayText(int columns, int rows)
		{
			// follow office's approach and list rows first
			//return String.Format("{0} x {1} Pages", columns, rows);
			//return String.Format("{0} x {1} Pages", rows, columns);
			return Infragistics.Shared.SR.GetString("PreviewRowColSelection_SelectedPages", rows, columns);
		}
		#endregion //GetDisplayText

		#region GetImageRectangles
		private Rectangle[,] GetImageRectangles()
		{
			if (this.imgRects == null)
			{
				if (this.rows * this.columns == 0)
					this.imgRects = new Rectangle[0,0];
				else
				{

					Rectangle workRect = this.DisplayRectangle;
					System.Drawing.Size imgSize = this.RowColImage.Size;

					workRect.Inflate(-OuterPadding, -OuterPadding);

					Rectangle[,] rects = new Rectangle[this.rows, this.columns];

					for(int row = 0; row < this.rows; row++)
					{
						int left = workRect.Left;
						int top = workRect.Top;

						for(int col = 0; col < this.columns; col++)
						{
							rects[row,col] = new Rectangle(
								left, top,
								imgSize.Width + (ImagePadding * 2) + (ImageBorderWidth * 2),
								imgSize.Height + (ImagePadding * 2) + (ImageBorderWidth * 2));

							left += InterImageSpacing + imgSize.Width + 
								(ImagePadding * 2) + (ImageBorderWidth * 2);
						}

						workRect.Y += imgSize.Height + InterImageSpacing + (ImagePadding * 2) + (ImageBorderWidth * 2);
					}

					this.imgRects = rects;
				}
			}

			return this.imgRects;
		}
		#endregion //GetImageRectangles

		// AS 1/18/05
		#region ResetSelection
		/// <summary>
		/// Clears the current selection.
		/// </summary>
		public void ResetSelection()
		{
			this.SetSelected(0,0);
		}
		#endregion //ResetSelection

		#region SetSelected
		private void SetSelected(int selectedColumns, int selectedRows)
		{
			if (this.selectedColumns == selectedColumns &&
				this.selectedRows == selectedRows)
				return;

			this.selectedColumns = selectedColumns;
			this.selectedRows = selectedRows;

			this.Text = this.GetDisplayText();

			if (this.IsHandleCreated)
				this.Invalidate();
		}
		#endregion //SetSelected

		#endregion //Methods

		#region Base class overrides

		#region DefaultImeMode
		/// <summary>
		/// Overriden. Determines the default ImeMode for the control.
		/// </summary>
		/// <remarks>
		/// <p class="body">Since the <b>UltraProgressBar</b> control does not receive focus and does not use the key messages, the default ImeMode is disabled.</p>
		/// </remarks>
		protected override ImeMode DefaultImeMode
		{
			get { return ImeMode.Disable; }
		}
		#endregion DefaultImeMode

		#region OnCreateControl
		/// <summary>
		/// Invoked when the control is created.
		/// </summary>
		protected override void OnCreateControl()
		{
			base.OnCreateControl();

			// force a resize
			this.Width += 1;
		}
		#endregion //OnCreateControl

		#region OnFontChanged
		/// <summary>
		/// Used to invoke the <see cref="Control.FontChanged"/> event when the Font of the control has been changed.
		/// </summary>
		/// <param name="e">Event args</param>
		protected override void OnFontChanged(EventArgs e)
		{
			this.DirtyTextSize();

			base.OnFontChanged(e);
		}
		#endregion //OnFontChanged

		#region OnMouseLeave
		/// <summary>
		/// Used to invoke the <see cref="Control.MouseLeave"/> event when the mouse leaves the bounds of the control.
		/// </summary>
		/// <param name="e">Event args</param>
		protected override void OnMouseLeave(EventArgs e)
		{
			this.SetSelected(0,0);

			base.OnMouseLeave(e);
		}
		#endregion //OnMouseLeave

		#region OnMouseMove
		/// <summary>
		/// Used to invoke the <see cref="Control.MouseMove"/> event when the mouse is over the control.
		/// </summary>
		/// <param name="e">Mouse event args</param>
		protected override void OnMouseMove(MouseEventArgs e)
		{
			Rectangle[,] imgRects = this.GetImageRectangles();

			if (imgRects != null && imgRects.Length > 0)
			{
				int selectedRows = 0;
				int selectedCols = 0;

				for(int row = 0; row < this.rows; row++)
				{
					if (e.Y > imgRects[row,0].Y)
						selectedRows++;
					else
						break;
				}

				for(int col = 0; col < this.columns; col++)
				{
					if (e.X > imgRects[0,col].X)
						selectedCols++;
					else
						break;
				}

				this.SetSelected(selectedCols, selectedRows);
			}

			base.OnMouseMove(e);
		}
		#endregion //OnMouseMove

		#region OnPaint
		/// <summary>
		/// Used to invoke the <see cref="Control.Paint"/> event when the control is to render itself.
		/// </summary>
		/// <param name="e">Paint event args</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			Image img = this.RowColImage;
			System.Drawing.Size imgSize = img.Size;

			Rectangle workRect = this.DisplayRectangle;

			#region Draw Backcolor
			Color backColor = Color.FromArgb(this.BackColor.ToArgb());

			if (backColor.A > 0)
			{
				// clear the background
				using (Brush brush = new SolidBrush(backColor))
					e.Graphics.FillRectangle(brush, workRect);
			}
			#endregion //Draw Backcolor

			// leave space around the edge
			workRect.Inflate(-OuterPadding, -OuterPadding);

			#region Draw Images
			Rectangle[,] imgRects = this.GetImageRectangles();

			if (imgRects != null && imgRects.Length > 0)
			{
				// create the pen for the image border color once for the draw operation
				using(Pen pen = new Pen(this.ImageBorderColorResolved))
				{
					for(int row = 0; row < this.rows; row++)
					{
						for(int col = 0; col < this.columns; col++)
						{
							Rectangle imgAreaRect = imgRects[row, col];

							if (row < this.selectedRows &&
								col < this.selectedColumns)
								e.Graphics.FillRectangle(SystemBrushes.Highlight, imgAreaRect);

							imgAreaRect.Width--;
							imgAreaRect.Height--;

							e.Graphics.DrawRectangle(pen, imgAreaRect);

							Rectangle imgRect = imgRects[row, col];
							imgRect.Inflate(-(ImagePadding + ImageBorderWidth), -(ImagePadding + ImageBorderWidth));

							e.Graphics.DrawImage(img, imgRect);
						}

						workRect.Height -= imgRects[row, 0].Height;
						workRect.Y += imgRects[row, 0].Height;
					}
				}

				workRect.Height -= InterImageSpacing * (this.rows - 1);
				workRect.Y += InterImageSpacing * (this.rows - 1);
			}
			#endregion //Draw Images

			#region Draw Text
			using(StringFormat sf = new StringFormat( StringFormatFlags.FitBlackBox ))
			{
				sf.Alignment = StringAlignment.Center;
				sf.LineAlignment = StringAlignment.Center;

				Color foreColor = Color.FromArgb(this.ForeColor.ToArgb());

				if (foreColor.A > 0)
				{
					using (Brush brush = new SolidBrush(foreColor))
						e.Graphics.DrawString(this.GetDisplayText(), this.Font, brush, workRect, sf);
				}
			}
			#endregion //Draw Text
			
			base.OnPaint(e);
		}
		#endregion //OnPaint

		#region ResetBackColor
		/// <summary>
		/// Resets the <see cref="Control.BackColor"/> to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public override void ResetBackColor()
		{
			this.BackColor = BackColorDefault;
		}
		#endregion //ResetBackColor

		#region ResetForeColor
		/// <summary>
		/// Resets the <see cref="Control.ForeColor"/> to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public override void ResetForeColor()
		{
			this.ForeColor = ForeColorDefault;
		}
		#endregion //ResetForeColor

		#region SetBoundsCore
		/// <summary>
		/// Invoked when the size and/or position of the control has changed.
		/// </summary>
		/// <param name="x">Proposed x coordinate</param>
		/// <param name="y">Proposed y coordinate</param>
		/// <param name="width">Proposed width</param>
		/// <param name="height">Proposed height</param>
		/// <param name="specified">Indicates the type of change</param>
		protected override void SetBoundsCore(int x, int y, int width, int height, System.Windows.Forms.BoundsSpecified specified)
		{
			if (this.Created && (specified & BoundsSpecified.Size) != 0)
			{
				System.Drawing.Size imgSize = this.RowColImageSize;

				imgSize.Width += ImageBorderWidth * 2;
				imgSize.Height += ImageBorderWidth * 2;

				imgSize.Width += ImagePadding * 2;
				imgSize.Height += ImagePadding * 2;

				imgSize.Width *= this.columns;
				imgSize.Width += InterImageSpacing * (this.columns - 1);

				imgSize.Height *= this.rows;
				imgSize.Height += InterImageSpacing * (this.rows - 1);

				width = Math.Max(imgSize.Width, this.TextSize.Width);

				height = imgSize.Height + this.TextSize.Height +
					PreTextSpacingY;

				width += OuterPadding * 2;
				height += OuterPadding * 2;
			}

			base.SetBoundsCore(x,y,width,height,specified);
		}
		#endregion //SetBoundsCore

		#region ShouldSerializeBackColor
		/// <summary>
		/// Determines if the <see cref="Control.BackColor"/> should be serialized.
		/// </summary>
		protected virtual bool ShouldSerializeBackColor()
		{
			return !this.BackColor.Equals(BackColorDefault);
		}
		#endregion //ShouldSerializeBackColor

		#region ShouldSerializeForeColor
		/// <summary>
		/// Determines if the <see cref="Control.ForeColor"/> should be serialized.
		/// </summary>
		protected virtual bool ShouldSerializeForeColor()
		{
			return !this.ForeColor.Equals(ForeColorDefault);
		}
		#endregion //ShouldSerializeForeColor

		#region TabStop (new)
		/// <summary>
		/// Overriden. Determines whether the control will receive focus when tabbing through the controls.
		/// </summary>
		/// <remarks>
		/// <p class="body">The control does not receive focus so changing this property will not affect the control.</p>
		/// </remarks>
		[DefaultValue(false)]
		public new bool TabStop 
		{ 
			get	{ return base.TabStop; }
			set { base.TabStop = value; }
		}
		#endregion TabStop (new)

		#endregion //Base class overrides
	}
}
