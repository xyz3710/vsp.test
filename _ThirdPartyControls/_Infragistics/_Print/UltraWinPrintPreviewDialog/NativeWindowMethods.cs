#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

namespace Infragistics.Win.Printing
{
	[ System.Security.SuppressUnmanagedCodeSecurityAttribute() ]
	[ System.Runtime.InteropServices.ComVisible(false) ]
	internal class NativeWindowMethods
	{
		#region Constructor
		private NativeWindowMethods()
		{
		}
		#endregion //Constructor

		#region Constants

		private const int		LOCALE_USER_DEFAULT = 0x400;

		//#define LOCALE_IMEASURE               0x0000000D   // 0 = metric, 1 = US
		private const int		LOCALE_IMEASURE = 0xD;

		#endregion //Constants
		
		#region APIs

        // MRS 01/03/07 - fxCop
        //[DllImport("kernel32", CharSet=CharSet.Auto)]
        [DllImport("kernel32", CharSet = CharSet.Auto, BestFitMapping = false)]
		// MD 10/31/06 - 64-Bit Support
		//private static extern int GetLocaleInfo(int Locale, int LCType, string lpLCData, int cchData);
		private static extern int GetLocaleInfo( int Locale, int LCType, [MarshalAs( UnmanagedType.LPWStr )]string lpLCData, int cchData );

		#endregion //APIs

		#region Functions

		#region IsMetric
		internal static bool IsMetric()
		{
			try
			{
				// AS 1/24/05 BR01978
				SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);

				perm.Assert();

				string text = new string(' ', 2);

				GetLocaleInfoApi(
					LOCALE_USER_DEFAULT,
					LOCALE_IMEASURE,
					text,
					text.Length);

				return string.Compare(text, "0") == 0;
			}
			catch(Exception)
			{
				return System.Globalization.RegionInfo.CurrentRegion.IsMetric;
			}
		}
		#endregion //IsMetric

		#region GetLocaleInfoApi
		private static int GetLocaleInfoApi(int Locale, int LCType, string lpLCData, int cchData)
		{
			return GetLocaleInfo(Locale, LCType, lpLCData, cchData);
		}
		#endregion //GetLocaleInfoApi

		#endregion //Functions
	}
}
