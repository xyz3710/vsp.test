#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;

namespace Infragistics.Win.Printing
{
	#region Event Handlers (delegates)

		#region PageSetupDialogDisplayingEventHandler

	/// <summary>
	/// Delegate for handling the <see cref="UltraPrintPreviewDialog.PageSetupDialogDisplaying"/> event.
	/// </summary>
	public delegate void PageSetupDialogDisplayingEventHandler(object sender, PageSetupDialogDisplayingEventArgs e); 

		#endregion //PageSetupDialogDisplayingEventHandler

		#region PrintingEventHandler

	/// <summary>
	/// Delegate for handling the <see cref="UltraPrintPreviewDialog.Printing"/> event.
	/// </summary>
	public delegate void PrintingEventHandler(object sender, PrintingEventArgs e); 

		#endregion //PrintingEventHandler

	#endregion Event Handlers (delegates)

	#region Event Args

		#region PageSetupDialogDisplayingEventArgs

	/// <summary>
	/// Event parameters used for the <see cref="UltraPrintPreviewDialog.PageSetupDialogDisplaying"/> event.
	/// </summary>
	/// <remarks>
	/// <p class="body">This class is the event arguments for the <see cref="UltraPrintPreviewDialog.PageSetupDialogDisplaying"/> event. 
	/// Setting <see cref="CancelEventArgs.Cancel"/> to true will prevent the <see cref="Dialog"/> from 
	/// being displayed.</p>
	/// </remarks>
	/// <seealso cref="PageSetupDialogDisplayingEventHandler"/>
	/// <seealso cref="UltraPrintPreviewDialog.PageSetupDialogDisplaying"/>
	/// <seealso cref="UltraPrintPreviewDialog"/>
	public class PageSetupDialogDisplayingEventArgs : CancelEventArgs
	{
		#region Member Variables

		private PageSetupDialog		dialog = null;

		#endregion Member Variables

		#region Constructor

		/// <summary>
		/// Initializes a new <see cref="PageSetupDialogDisplayingEventArgs"/>
		/// </summary>
		/// <param name="dialog">The <b>PageSetupDialog</b> that will be displayed if the event is not cancelled.</param>
		/// <seealso cref="Dialog"/>
		public PageSetupDialogDisplayingEventArgs(PageSetupDialog dialog)
		{
			this.dialog    = dialog;
		}

		#endregion Constructor

		#region Properties

			#region Dialog

		/// <summary>
		/// Read-only property that returns the PageSetupDialog that is about to be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">This is a read-only property that returns the <see cref="PageSetupDialog"/> 
		/// that will be displayed if the event is not cancelled.</p>
		/// </remarks>
		/// <seealso cref="UltraPrintPreviewDialog.PageSetupDialogDisplaying"/>
		/// <seealso cref="UltraPrintPreviewDialog"/>
		public PageSetupDialog Dialog
		{
			get	{ return this.dialog; }
		}

			#endregion Dialog

		#endregion Properties
	}

		#endregion //PageSetupDialogDisplayingEventArgs

		#region PrintingEventArgs

	/// <summary>
	/// Event parameters used for the <see cref="UltraPrintPreviewDialog.Printing"/> event.
	/// </summary>
	/// <remarks>
	/// <p class="body">This class is the event arguments for the <see cref="UltraPrintPreviewDialog.Printing"/> event. 
	/// Setting <see cref="CancelEventArgs.Cancel"/> to true will prevent the print document from 
	/// rendering to the printer. In addition, the class includes a <see cref="DisplayPrintStatus"/> property that 
	/// can be used to control whether a status dialog is displayed during the print operation.</p>
	/// </remarks>
	/// <seealso cref="PrintingEventHandler"/>
	/// <seealso cref="UltraPrintPreviewDialog.DisplayPrintStatus"/>
	/// <seealso cref="UltraPrintPreviewDialog.Printing"/>
	/// <seealso cref="UltraPrintPreviewDialog"/>
	public class PrintingEventArgs : CancelEventArgs
	{
		#region Member Variables

		private bool displayPrintStatus;

		#endregion Member Variables

		#region Constructor

		/// <summary>
		/// Initializes a new <see cref="PrintingEventArgs"/>
		/// </summary>
		/// <param name="displayPrintStatus">Default value used to initialize the <see cref="DisplayPrintStatus"/> property</param>
		/// <seealso cref="DisplayPrintStatus"/>
		public PrintingEventArgs(bool displayPrintStatus)
		{
			this.displayPrintStatus    = displayPrintStatus;
		}

		#endregion Constructor

		#region Properties

			#region DisplayPrintStatus

		/// <summary>
		/// Gets/sets whether a print status dialog will be displayed during the print operation.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>DisplayPrintStatus</b> is used to determine whether a status dialog 
		/// will be displayed while the associated <see cref="UltraPrintPreviewDialog.Document"/> is 
		/// being printed to the printer. The value of this property is initialized based on the 
		/// <see cref="UltraPrintPreviewDialog.DisplayPrintStatus"/> property.</p>
		/// </remarks>
		/// <seealso cref="UltraPrintPreviewDialog.DisplayPrintStatus"/>
		/// <seealso cref="UltraPrintPreviewDialog.Printing"/>
		/// <seealso cref="UltraPrintPreviewDialog"/>
		public bool DisplayPrintStatus
		{
			get	{ return this.displayPrintStatus; }
			set	{ this.displayPrintStatus = value; }
		}

			#endregion DisplayPrintStatus

		#endregion Properties
	}

	#endregion //PrintingEventArgs

	#endregion Event Args
}
