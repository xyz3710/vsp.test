#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win.Printing;
using Infragistics.Win.UltraWinToolbars;
using Infragistics.Win.UltraWinStatusBar;

namespace Infragistics.Win.Printing
{
	/// <summary>
	/// Represents a dialog box form that contains an <b>UltraPrintPreviewControl</b>, <b>UltraPrintPreviewThumbnail</b> and <b>UltraToolbarsManager</b>.
	/// </summary>
	/// <p class="body">The <b>UltraPrintPreviewDialog</b> is a derived form class that 
	/// like the inbox <see cref="System.Windows.Forms.PrintPreviewDialog"/> appears as a 
	/// component in the form designer and is used to display a print preview dialog. The 
	/// <b>UltraPrintPreviewDialog</b> uses the Infragistics <b>UltraPrintPreviewControl</b> 
	/// and <see cref="UltraPrintPreviewThumbnail"/> to display the preview pages of the 
	/// associated <see cref="Document"/>. Many of the properties of these controls are exposed 
	/// as properties of this class but you may also access the controls themselves programatically 
	/// if necessary using the <see cref="PrintPreviewControl"/> and <see cref="PrintPreviewThumbnail"/> 
	/// properties. The dialog also uses an <b>UltraToolbarsManager</b> and 
	/// an <b>UltraStatusBar</b> to provide additional aspects of the user interface. Likewise, these 
	/// components may be accessed programatically using the <see cref="ToolbarsManager"/> and 
	/// <see cref="StatusBar"/> properties respectively.</p>
	/// <p class="body">The dialog includes several additional events that may be used to 
	/// interact with the print operations. The <see cref="PageSetupDialogDisplaying"/> event 
	/// is invoked when the end-user clicks the <b>Page Setup</b> toolbar button and can be 
	/// used to prevent the default page setup dialog from being displayed and allow you to 
	/// display a custom page setup dialog. The <see cref="Printing"/> event is invoked when 
	/// the end-user clicks the <b>Print</b> toolbar button and allows you to determine 
	/// whether the status dialog should be displayed as well as providing a way to 
	/// cancel the print operation if necessary. There is also a corresponding <see cref="Printed"/> 
	/// event that is invoked when the print operation has completed.</p>
	
	
	[ToolboxBitmap(typeof(UltraPrintPreviewDialog), AssemblyVersion.ToolBoxBitmapFolder + "UltraPrintPreviewDialog.bmp")] // MRS 11/13/05
	[ToolboxItemFilter("System.Windows.Forms", ToolboxItemFilterType.Allow )]
	// AS 4/8/05 BR03289
	//[Designer(typeof(UltraComponentDesigner))] 
	[Designer(typeof(UltraPrintPreviewDialogDesigner))] 
	[DefaultProperty("Document")]
	[DefaultEvent("PageSetupDialogDisplaying")]
	[DesignTimeVisible(true)]
	[ToolboxItem(true)]
	[LocalizedDescription("LD_UltraPrintPreviewDialog")] // JAS BR06191 10/10/05
	public class UltraPrintPreviewDialog : System.Windows.Forms.Form, IUltraLicensedComponent
	{
		#region Private Members

		private UltraLicense license = null;
		private IContainer	 container = null;

		private Infragistics.Win.Printing.UltraPrintPreviewThumbnail ultraPrintPreviewThumbnail1;
		private Infragistics.Win.Printing.UltraPrintPreviewControl ultraPrintPreviewControl1;
		private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager ultraToolbarsManager1;
		private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _UltraPrintPreviewDialog_Toolbars_Dock_Area_Left;
		private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _UltraPrintPreviewDialog_Toolbars_Dock_Area_Right;
		private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _UltraPrintPreviewDialog_Toolbars_Dock_Area_Top;
		private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom;
		private Infragistics.Win.UltraWinStatusBar.UltraStatusBar ultraStatusBar1;
		private System.Windows.Forms.Splitter spltThumbnail;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
		private System.Windows.Forms.PrintDialog printDialog1;
		private PreviewRowColumnSelector rowColumnSelector;

		private int lastRowsSetting = 0;
		private bool thumbnailAreaVisible = true;
		private bool displayPrintStatus = true;
		private bool statusBarVisible = true;

		// AS 1/18/05
		private bool isFirstLoad = true;
		private bool canResetTrackMouseEvent = true;

		#endregion Private Members

		#region Constructors

		/// <summary>
		/// Constructor
		/// </summary>
		public UltraPrintPreviewDialog()
		{
			// Verify and cache the license
			// AS 3/5/03 DNF37
			// Wrapped in a try/catch for a FileNotFoundException.
			// When the assembly is loaded dynamically, VS seems 
			// to be trying to reload a copy of Shared even though 
			// one is in memory. This generates a FileNotFoundException
			// when the dll is not in the gac and not in the AppBase
			// for the AppDomain.
			//
			try
			{
				this.license = LicenseManager.Validate( typeof(UltraPrintPreviewDialog), this ) as UltraLicense;
			}
			catch (System.IO.FileNotFoundException)	{}

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// create the row column selector but make sure its out of the way
			this.rowColumnSelector = new PreviewRowColumnSelector();
			this.rowColumnSelector.Location = new Point(-10000,-10000);
			this.Controls.Add(this.rowColumnSelector);
			this.rowColumnSelector.MouseUp += new MouseEventHandler(this.OnRowColumnSelectorMouseUp);

			this.spltThumbnail.BackColor = Infragistics.Win.Office2003Colors.TearawayStrip;

			this.InitializeDialogStrings();

			this.Name = "";

			this.ShowInTaskbar = false;

			// MD 6/1/06 - BR13194
			// Moved the following code to the InitializeZoomCombo method
			//
			#region Initialize Zoom combo

			//ComboBoxTool cbo = this.ultraToolbarsManager1.Tools["Zoom"] as ComboBoxTool;
			//ListTool list = this.ultraToolbarsManager1.Tools["Zoom Increments"] as ListTool;

			//double[] zoomList = new double[] { .0833, .12, .25, .3333, .50, .6667, .75, 1d, 1.25, 1.5, 2d, 3d, 4d, 6d, 8d, 12d, 16d };

			//Array.Sort(zoomList);
			//Array.Reverse(zoomList);

			//for(int i = 0; i < zoomList.Length; i++)
			//{
			//    string formattedZoom = this.FormatPercent(zoomList[i]);
			//    ZoomWrapper zoomItem = new ZoomWrapper(zoomList[i]);

			//    cbo.ValueList.ValueListItems.Add(zoomItem, formattedZoom);
			//    ListToolItem listToolItem = list.ListToolItems.Add(formattedZoom, formattedZoom, false);
			//    listToolItem.Tag = zoomItem;
			//}

			//cbo.ValueList.ValueListItems.Add( new ZoomWrapper(ZoomMode.PageWidth), SR.GetString("PrintPreview_ZoomListItem_PageWidth") ); //"Page Width" );
			//cbo.ValueList.ValueListItems.Add( new ZoomWrapper(ZoomMode.MarginsWidth), SR.GetString("PrintPreview_ZoomListItem_MarginWidth") ); // "Margin Width" );
			//cbo.ValueList.ValueListItems.Add( new ZoomWrapper(ZoomMode.WholePage), SR.GetString("PrintPreview_ZoomListItem_WholePage") ); // "Whole Page" );

			#endregion //Initialize Zoom combo
			this.InitializeZoomCombo();

			// AS 1/18/05
			// Be sure to update the state of the component.
			this.UpdateMouseActionTools();
		}

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="container">An <see cref="IContainer"/> that represents the container of the <see cref="UltraPrintPreviewDialog"/></param>
		public UltraPrintPreviewDialog(IContainer container ) : this()
		{
			this.container = container;

			if ( this.container != null )
				this.container.Add( this );
		}

		#endregion

		#region Base class overrides

			#region Dispose

		/// <summary>
        /// Disposes of the resources (other than memory) used by the <see cref="System.Windows.Forms.Form"/>.
		/// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

			#endregion //Dispose

			#region OnClosing

		/// <summary>
        /// Raises the <see cref="System.Windows.Forms.Form.Closing"/> event.
		/// </summary>
        /// <param name="e">A <see cref="System.ComponentModel.CancelEventArgs"/> that contains the event data.</param>
		protected override void OnClosing( CancelEventArgs e )
		{
			this.ultraPrintPreviewControl1.InvalidatePreview();

			// AS 1/24/05
			base.OnClosing(e);
		}

			#endregion //OnClosing

            #region Padding

            // MRS 11/2/05 - BR04592
            // Microsoft added a Padding property to Control in CLR2. 
            // Since our controls don't support it, we need to shadow and 
            // hide it. 
            /// <summary>
            /// The Padding property is shadowed and hidden. It is not supported by this class. 
            /// </summary>
            [EditorBrowsable(EditorBrowsableState.Never)]
            [Browsable(false)]
            public new Padding Padding
            {
                get { return base.Padding; }
                set { base.Padding = value; }
            }
            #endregion // Padding

            #region BackgroundImageLayout

        // MRS 11/2/05 - BR04592
        // Microsoft added a BackgroundImageLayout property to Control in CLR2. 
        // Since our controls don't support it, we need to shadow and 
        // hide it. 
        /// <summary>
        /// The BackgroundImageLayout property is shadowed and hidden. It is not supported by this class. 
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public new ImageLayout BackgroundImageLayout
        {
            get { return base.BackgroundImageLayout; }
            set { base.BackgroundImageLayout = value; }
        }
        #endregion // BackgroundImageLayout

		#endregion //Base class overrides

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.UltraWinToolbars.OptionSet optionSet1 = new Infragistics.Win.UltraWinToolbars.OptionSet("Tools");
			Infragistics.Win.UltraWinToolbars.OptionSet optionSet2 = new Infragistics.Win.UltraWinToolbars.OptionSet("ZoomMode");
			Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar1 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("Standard");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool47 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Page Setup");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool35 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Landscape", "");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool1 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Hand Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool2 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Snapshot Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool3 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Dynamic Zoom Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool4 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom Out Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool5 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom In Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool34 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Thumbnails", "");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool6 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool7 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool8 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.UltraWinToolbars.ComboBoxTool comboBoxTool1 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("Zoom");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool9 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Continuous", "");
			Infragistics.Win.UltraWinToolbars.PopupControlContainerTool popupControlContainerTool1 = new Infragistics.Win.UltraWinToolbars.PopupControlContainerTool("Page Layout");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ClosePreview");
			Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar2 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("MenuBar");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool1 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("File");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool2 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("View");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool3 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Tools");
			Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar3 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("View");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.UltraWinToolbars.TextBoxTool textBoxTool1 = new Infragistics.Win.UltraWinToolbars.TextBoxTool("Current Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool7 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool8 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool9 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool10 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool10 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Thumbnails", "");
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool11 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Hand Tool", "Tools");
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool12 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Snapshot Tool", "Tools");
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool13 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Dynamic Zoom Tool", "Tools");
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool14 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom Out Tool", "Tools");
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool15 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom In Tool", "Tools");
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool11 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ComboBoxTool comboBoxTool2 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("Zoom");
			Infragistics.Win.ValueList valueList1 = new Infragistics.Win.ValueList(0);
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool4 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("File");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool12 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool13 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Page Setup");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool14 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exit");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool5 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("View");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool16 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Thumbnails", "");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool15 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool16 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.UltraWinToolbars.ComboBoxTool comboBoxTool3 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("Zoom");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool17 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool18 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool19 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool6 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Go To");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool7 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool20 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Hand Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool21 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Snapshot Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool22 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Dynamic Zoom Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool23 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom In Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool24 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom Out Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool17 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exit");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool18 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool19 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool20 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool21 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.TextBoxTool textBoxTool2 = new Infragistics.Win.UltraWinToolbars.TextBoxTool("Current Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool22 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool23 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool24 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool25 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool25 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool26 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool27 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool8 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Go To");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool26 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool27 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool28 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool29 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool30 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool31 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.UltraWinToolbars.PopupControlContainerTool popupControlContainerTool2 = new Infragistics.Win.UltraWinToolbars.PopupControlContainerTool("Page Layout");
			Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool32 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Page Setup");
			Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool28 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Continuous", "");
			Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool9 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("ContextMenuThumbnail");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool33 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Reduce Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool34 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Enlarge Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool29 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Show Page Numbers", "");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool10 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("ContextMenuPreviewHand");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool35 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool36 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool37 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool38 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool39 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool40 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool41 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool11 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("ContextMenuPreviewZoom");
			Infragistics.Win.UltraWinToolbars.ListTool listTool1 = new Infragistics.Win.UltraWinToolbars.ListTool("Zoom Increments");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool30 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool31 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool32 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool42 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool43 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.UltraWinToolbars.ListTool listTool2 = new Infragistics.Win.UltraWinToolbars.ListTool("Zoom Increments");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool44 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Reduce Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool45 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Enlarge Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool33 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Show Page Numbers", "");
			Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool46 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ClosePreview");
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateButtonTool36 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Landscape", "");
			Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel2 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UltraPrintPreviewDialog));
			this.ultraPrintPreviewControl1 = new Infragistics.Win.Printing.UltraPrintPreviewControl();
			this.ultraPrintPreviewThumbnail1 = new Infragistics.Win.Printing.UltraPrintPreviewThumbnail();
			this.spltThumbnail = new System.Windows.Forms.Splitter();
			this.ultraToolbarsManager1 = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
			this.printDialog1 = new System.Windows.Forms.PrintDialog();
			this.ultraStatusBar1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewThumbnail1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).BeginInit();
			this.SuspendLayout();
			// 
			// ultraPrintPreviewControl1
			// 
			this.ultraPrintPreviewControl1.BackColorInternal = System.Drawing.SystemColors.ControlDark;
			this.ultraPrintPreviewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ultraPrintPreviewControl1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.ultraPrintPreviewControl1.Location = new System.Drawing.Point(184, 89);
			this.ultraPrintPreviewControl1.Name = "ultraPrintPreviewControl1";
			this.ultraPrintPreviewControl1.Size = new System.Drawing.Size(678, 389);
			this.ultraPrintPreviewControl1.TabIndex = 0;
			this.ultraPrintPreviewControl1.CurrentPageChanged += new System.EventHandler(this.ultraPrintPreviewControl1_CurrentPageChanged);
			this.ultraPrintPreviewControl1.PropertyChanged += new Infragistics.Win.PropertyChangedEventHandler(this.ultraPrintPreviewControl1_PropertyChanged);
			this.ultraPrintPreviewControl1.CurrentZoomChanged += new System.EventHandler(this.ultraPrintPreviewControl1_CurrentZoomChanged);
			this.ultraPrintPreviewControl1.ViewHistoryChanged += new System.EventHandler(this.ultraPrintPreviewControl1_ViewHistoryChanged);
			this.ultraPrintPreviewControl1.PreviewGenerated += new System.EventHandler(this.ultraPrintPreviewControl1_PreviewGenerated);
			// 
			// ultraPrintPreviewThumbnail1
			// 
			this.ultraPrintPreviewThumbnail1.BackColorInternal = System.Drawing.SystemColors.ControlDark;
			this.ultraToolbarsManager1.SetContextMenuUltra(this.ultraPrintPreviewThumbnail1, "ContextMenuThumbnail");
			this.ultraPrintPreviewThumbnail1.Dock = System.Windows.Forms.DockStyle.Left;
			this.ultraPrintPreviewThumbnail1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.ultraPrintPreviewThumbnail1.Location = new System.Drawing.Point(26, 89);
			this.ultraPrintPreviewThumbnail1.Name = "ultraPrintPreviewThumbnail1";
			this.ultraPrintPreviewThumbnail1.PreviewControl = this.ultraPrintPreviewControl1;
			this.ultraPrintPreviewThumbnail1.Size = new System.Drawing.Size(152, 389);
			this.ultraPrintPreviewThumbnail1.TabIndex = 1;
			this.ultraPrintPreviewThumbnail1.CurrentZoomChanged += new System.EventHandler(this.ultraPrintPreviewThumbnail1_CurrentZoomChanged);
			// 
			// spltThumbnail
			// 
			this.spltThumbnail.Location = new System.Drawing.Point(178, 89);
			this.spltThumbnail.Name = "spltThumbnail";
			this.spltThumbnail.Size = new System.Drawing.Size(6, 389);
			this.spltThumbnail.TabIndex = 2;
			this.spltThumbnail.TabStop = false;
			// 
			// ultraToolbarsManager1
			// 
			this.ultraToolbarsManager1.DesignerFlags = 1;
			this.ultraToolbarsManager1.DockWithinContainer = this;
			this.ultraToolbarsManager1.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
			this.ultraToolbarsManager1.ImageSizeSmall = new System.Drawing.Size(20, 20);
			optionSet1.AllowAllUp = false;
			this.ultraToolbarsManager1.OptionSets.Add(optionSet1);
			this.ultraToolbarsManager1.OptionSets.Add(optionSet2);
			this.ultraToolbarsManager1.ShowFullMenusDelay = 500;
			this.ultraToolbarsManager1.Style = Infragistics.Win.UltraWinToolbars.ToolbarStyle.Office2007;
			ultraToolbar1.DockedColumn = 0;
			ultraToolbar1.DockedRow = 1;
			buttonTool1.InstanceProps.IsFirstInGroup = true;
			stateButtonTool1.Checked = true;
			stateButtonTool1.InstanceProps.IsFirstInGroup = true;
			stateButtonTool34.Checked = true;
			stateButtonTool34.InstanceProps.IsFirstInGroup = true;
			stateButtonTool6.InstanceProps.IsFirstInGroup = true;
			stateButtonTool8.Checked = true;
			buttonTool2.InstanceProps.IsFirstInGroup = true;
			stateButtonTool9.Checked = true;
			popupControlContainerTool1.InstanceProps.IsFirstInGroup = true;
			ultraToolbar1.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool1,
            buttonTool47,
            stateButtonTool35,
            stateButtonTool1,
            stateButtonTool2,
            stateButtonTool3,
            stateButtonTool4,
            stateButtonTool5,
            stateButtonTool34,
            stateButtonTool6,
            stateButtonTool7,
            stateButtonTool8,
            buttonTool2,
            comboBoxTool1,
            buttonTool3,
            stateButtonTool9,
            popupControlContainerTool1,
            buttonTool4});
			ultraToolbar1.Text = "Standard";
			ultraToolbar2.DockedColumn = 0;
			ultraToolbar2.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
			ultraToolbar2.DockedRow = 0;
			ultraToolbar2.IsMainMenuBar = true;
			ultraToolbar2.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            popupMenuTool1,
            popupMenuTool2,
            popupMenuTool3});
			ultraToolbar2.ShowInToolbarList = false;
			ultraToolbar2.Text = "MenuBar";
			ultraToolbar3.DockedColumn = 0;
			ultraToolbar3.DockedRow = 0;
			buttonTool9.InstanceProps.IsFirstInGroup = true;
			ultraToolbar3.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool5,
            buttonTool6,
            textBoxTool1,
            buttonTool7,
            buttonTool8,
            buttonTool9,
            buttonTool10});
			ultraToolbar3.Text = "View";
			this.ultraToolbarsManager1.Toolbars.AddRange(new Infragistics.Win.UltraWinToolbars.UltraToolbar[] {
            ultraToolbar1,
            ultraToolbar2,
            ultraToolbar3});
			stateButtonTool10.Checked = true;
			appearance1.Image = ((object)(resources.GetObject("appearance1.Image")));
			stateButtonTool10.SharedPropsInternal.AppearancesSmall.Appearance = appearance1;
			stateButtonTool10.SharedPropsInternal.Caption = "Thumbnails";
			stateButtonTool10.SharedPropsInternal.Category = "View";
			stateButtonTool11.Checked = true;
			stateButtonTool11.OptionSetKey = "Tools";
			appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
			stateButtonTool11.SharedPropsInternal.AppearancesLarge.Appearance = appearance2;
			appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
			stateButtonTool11.SharedPropsInternal.AppearancesSmall.Appearance = appearance3;
			stateButtonTool11.SharedPropsInternal.Caption = "&Hand Tool";
			stateButtonTool11.SharedPropsInternal.Category = "Tools";
			stateButtonTool11.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
			stateButtonTool12.OptionSetKey = "Tools";
			appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
			stateButtonTool12.SharedPropsInternal.AppearancesLarge.Appearance = appearance4;
			appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
			stateButtonTool12.SharedPropsInternal.AppearancesSmall.Appearance = appearance5;
			stateButtonTool12.SharedPropsInternal.Caption = "&Snapshot Tool";
			stateButtonTool12.SharedPropsInternal.Category = "Tools";
			stateButtonTool13.OptionSetKey = "Tools";
			appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
			stateButtonTool13.SharedPropsInternal.AppearancesLarge.Appearance = appearance6;
			appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
			stateButtonTool13.SharedPropsInternal.AppearancesSmall.Appearance = appearance7;
			stateButtonTool13.SharedPropsInternal.Caption = "&Dynamic Zoom Tool";
			stateButtonTool13.SharedPropsInternal.Category = "Tools";
			stateButtonTool14.OptionSetKey = "Tools";
			appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
			stateButtonTool14.SharedPropsInternal.AppearancesLarge.Appearance = appearance8;
			appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
			stateButtonTool14.SharedPropsInternal.AppearancesSmall.Appearance = appearance9;
			stateButtonTool14.SharedPropsInternal.Caption = "Zoom &Out Tool";
			stateButtonTool14.SharedPropsInternal.Category = "Tools";
			stateButtonTool15.OptionSetKey = "Tools";
			appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
			stateButtonTool15.SharedPropsInternal.AppearancesLarge.Appearance = appearance10;
			appearance11.Image = ((object)(resources.GetObject("appearance11.Image")));
			stateButtonTool15.SharedPropsInternal.AppearancesSmall.Appearance = appearance11;
			stateButtonTool15.SharedPropsInternal.Caption = "Zoom &In Tool";
			stateButtonTool15.SharedPropsInternal.Category = "Tools";
			appearance12.Image = ((object)(resources.GetObject("appearance12.Image")));
			buttonTool11.SharedPropsInternal.AppearancesSmall.Appearance = appearance12;
			buttonTool11.SharedPropsInternal.Caption = "&Print";
			buttonTool11.SharedPropsInternal.Category = "File";
			buttonTool11.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
			comboBoxTool2.DropDownStyle = Infragistics.Win.DropDownStyle.DropDown;
			comboBoxTool2.SharedPropsInternal.Caption = "&Zoom:";
			comboBoxTool2.SharedPropsInternal.Category = "View";
			comboBoxTool2.SharedPropsInternal.ToolTipText = "Zoom";
			comboBoxTool2.ValueList = valueList1;
			popupMenuTool4.SharedPropsInternal.Caption = "&File";
			popupMenuTool4.SharedPropsInternal.Category = "Menus";
			buttonTool14.InstanceProps.IsFirstInGroup = true;
			popupMenuTool4.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool12,
            buttonTool13,
            buttonTool14});
			popupMenuTool5.SharedPropsInternal.Caption = "&View";
			popupMenuTool5.SharedPropsInternal.Category = "Menus";
			stateButtonTool16.Checked = true;
			buttonTool15.InstanceProps.IsFirstInGroup = true;
			stateButtonTool17.InstanceProps.IsFirstInGroup = true;
			stateButtonTool19.Checked = true;
			popupMenuTool6.InstanceProps.IsFirstInGroup = true;
			popupMenuTool5.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            stateButtonTool16,
            buttonTool15,
            buttonTool16,
            comboBoxTool3,
            stateButtonTool17,
            stateButtonTool18,
            stateButtonTool19,
            popupMenuTool6});
			popupMenuTool7.SharedPropsInternal.Caption = "&Tools";
			popupMenuTool7.SharedPropsInternal.Category = "Menus";
			stateButtonTool20.Checked = true;
			stateButtonTool22.InstanceProps.IsFirstInGroup = true;
			popupMenuTool7.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            stateButtonTool20,
            stateButtonTool21,
            stateButtonTool22,
            stateButtonTool23,
            stateButtonTool24});
			buttonTool17.SharedPropsInternal.Caption = "E&xit";
			buttonTool17.SharedPropsInternal.Category = "File";
			appearance13.Image = ((object)(resources.GetObject("appearance13.Image")));
			buttonTool18.SharedPropsInternal.AppearancesLarge.Appearance = appearance13;
			appearance14.Image = ((object)(resources.GetObject("appearance14.Image")));
			buttonTool18.SharedPropsInternal.AppearancesSmall.Appearance = appearance14;
			buttonTool18.SharedPropsInternal.Caption = "Zoom In";
			buttonTool18.SharedPropsInternal.Category = "View";
			appearance15.Image = ((object)(resources.GetObject("appearance15.Image")));
			buttonTool19.SharedPropsInternal.AppearancesLarge.Appearance = appearance15;
			appearance16.Image = ((object)(resources.GetObject("appearance16.Image")));
			buttonTool19.SharedPropsInternal.AppearancesSmall.Appearance = appearance16;
			buttonTool19.SharedPropsInternal.Caption = "Zoom Out";
			buttonTool19.SharedPropsInternal.Category = "View";
			appearance17.Image = ((object)(resources.GetObject("appearance17.Image")));
			buttonTool20.SharedPropsInternal.AppearancesLarge.Appearance = appearance17;
			appearance18.Image = ((object)(resources.GetObject("appearance18.Image")));
			buttonTool20.SharedPropsInternal.AppearancesSmall.Appearance = appearance18;
			buttonTool20.SharedPropsInternal.Caption = "&Previous View";
			buttonTool20.SharedPropsInternal.Category = "View";
			buttonTool20.SharedPropsInternal.Enabled = false;
			appearance19.Image = ((object)(resources.GetObject("appearance19.Image")));
			buttonTool21.SharedPropsInternal.AppearancesLarge.Appearance = appearance19;
			appearance20.Image = ((object)(resources.GetObject("appearance20.Image")));
			buttonTool21.SharedPropsInternal.AppearancesSmall.Appearance = appearance20;
			buttonTool21.SharedPropsInternal.Caption = "&Next View";
			buttonTool21.SharedPropsInternal.Category = "View";
			buttonTool21.SharedPropsInternal.Enabled = false;
			textBoxTool2.SharedPropsInternal.Caption = "Current Page";
			textBoxTool2.SharedPropsInternal.Category = "View";
			appearance21.Image = ((object)(resources.GetObject("appearance21.Image")));
			buttonTool22.SharedPropsInternal.AppearancesLarge.Appearance = appearance21;
			appearance22.Image = ((object)(resources.GetObject("appearance22.Image")));
			buttonTool22.SharedPropsInternal.AppearancesSmall.Appearance = appearance22;
			buttonTool22.SharedPropsInternal.Caption = "First Page";
			buttonTool22.SharedPropsInternal.Category = "View";
			appearance23.Image = ((object)(resources.GetObject("appearance23.Image")));
			buttonTool23.SharedPropsInternal.AppearancesLarge.Appearance = appearance23;
			appearance24.Image = ((object)(resources.GetObject("appearance24.Image")));
			buttonTool23.SharedPropsInternal.AppearancesSmall.Appearance = appearance24;
			buttonTool23.SharedPropsInternal.Caption = "Next Page";
			buttonTool23.SharedPropsInternal.Category = "View";
			appearance25.Image = ((object)(resources.GetObject("appearance25.Image")));
			buttonTool24.SharedPropsInternal.AppearancesLarge.Appearance = appearance25;
			appearance26.Image = ((object)(resources.GetObject("appearance26.Image")));
			buttonTool24.SharedPropsInternal.AppearancesSmall.Appearance = appearance26;
			buttonTool24.SharedPropsInternal.Caption = "Previous Page";
			buttonTool24.SharedPropsInternal.Category = "View";
			appearance27.Image = ((object)(resources.GetObject("appearance27.Image")));
			buttonTool25.SharedPropsInternal.AppearancesLarge.Appearance = appearance27;
			appearance28.Image = ((object)(resources.GetObject("appearance28.Image")));
			buttonTool25.SharedPropsInternal.AppearancesSmall.Appearance = appearance28;
			buttonTool25.SharedPropsInternal.Caption = "Last Page";
			buttonTool25.SharedPropsInternal.Category = "View";
			stateButtonTool25.OptionSetKey = "ZoomMode";
			appearance29.Image = ((object)(resources.GetObject("appearance29.Image")));
			stateButtonTool25.SharedPropsInternal.AppearancesLarge.Appearance = appearance29;
			appearance30.Image = ((object)(resources.GetObject("appearance30.Image")));
			stateButtonTool25.SharedPropsInternal.AppearancesSmall.Appearance = appearance30;
			stateButtonTool25.SharedPropsInternal.Caption = "Page Width";
			stateButtonTool25.SharedPropsInternal.Category = "Zoom Mode";
			stateButtonTool26.OptionSetKey = "ZoomMode";
			appearance31.Image = ((object)(resources.GetObject("appearance31.Image")));
			stateButtonTool26.SharedPropsInternal.AppearancesLarge.Appearance = appearance31;
			appearance32.Image = ((object)(resources.GetObject("appearance32.Image")));
			stateButtonTool26.SharedPropsInternal.AppearancesSmall.Appearance = appearance32;
			stateButtonTool26.SharedPropsInternal.Caption = "Margin Width";
			stateButtonTool26.SharedPropsInternal.Category = "Zoom Mode";
			stateButtonTool27.Checked = true;
			stateButtonTool27.OptionSetKey = "ZoomMode";
			appearance33.Image = ((object)(resources.GetObject("appearance33.Image")));
			stateButtonTool27.SharedPropsInternal.AppearancesLarge.Appearance = appearance33;
			appearance34.Image = ((object)(resources.GetObject("appearance34.Image")));
			stateButtonTool27.SharedPropsInternal.AppearancesSmall.Appearance = appearance34;
			stateButtonTool27.SharedPropsInternal.Caption = "Whole Page";
			stateButtonTool27.SharedPropsInternal.Category = "Zoom Mode";
			popupMenuTool8.SharedPropsInternal.Caption = "Go To";
			popupMenuTool8.SharedPropsInternal.Category = "Menus";
			buttonTool30.InstanceProps.IsFirstInGroup = true;
			popupMenuTool8.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool26,
            buttonTool27,
            buttonTool28,
            buttonTool29,
            buttonTool30,
            buttonTool31});
			popupControlContainerTool2.DropDownArrowStyle = Infragistics.Win.UltraWinToolbars.DropDownArrowStyle.Standard;
			appearance35.Image = ((object)(resources.GetObject("appearance35.Image")));
			popupControlContainerTool2.SharedPropsInternal.AppearancesLarge.Appearance = appearance35;
			appearance36.Image = ((object)(resources.GetObject("appearance36.Image")));
			popupControlContainerTool2.SharedPropsInternal.AppearancesSmall.Appearance = appearance36;
			popupControlContainerTool2.SharedPropsInternal.Caption = "Page Layout";
			popupControlContainerTool2.SharedPropsInternal.Category = "View";
			appearance50.Image = ((object)(resources.GetObject("appearance50.Image")));
			buttonTool32.SharedPropsInternal.AppearancesLarge.Appearance = appearance50;
			appearance49.Image = ((object)(resources.GetObject("appearance49.Image")));
			buttonTool32.SharedPropsInternal.AppearancesSmall.Appearance = appearance49;
			buttonTool32.SharedPropsInternal.Caption = "Page Set&up...";
			buttonTool32.SharedPropsInternal.Category = "File";
			buttonTool32.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.DefaultForToolType;
			stateButtonTool28.Checked = true;
			appearance37.Image = ((object)(resources.GetObject("appearance37.Image")));
			stateButtonTool28.SharedPropsInternal.AppearancesSmall.Appearance = appearance37;
			stateButtonTool28.SharedPropsInternal.Caption = "Continuous";
			stateButtonTool28.SharedPropsInternal.Category = "View";
			popupMenuTool9.SharedPropsInternal.Caption = "ContextMenuThumbnail";
			popupMenuTool9.SharedPropsInternal.Category = "Context Menus";
			stateButtonTool29.Checked = true;
			stateButtonTool29.InstanceProps.IsFirstInGroup = true;
			popupMenuTool9.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool33,
            buttonTool34,
            stateButtonTool29});
			popupMenuTool10.SharedPropsInternal.Caption = "ContextMenuPreviewHand";
			popupMenuTool10.SharedPropsInternal.Category = "Context Menus";
			buttonTool39.InstanceProps.IsFirstInGroup = true;
			buttonTool41.InstanceProps.IsFirstInGroup = true;
			popupMenuTool10.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool35,
            buttonTool36,
            buttonTool37,
            buttonTool38,
            buttonTool39,
            buttonTool40,
            buttonTool41});
			popupMenuTool11.SharedPropsInternal.Caption = "ContextMenuPreviewZoom";
			popupMenuTool11.SharedPropsInternal.Category = "Context Menus";
			stateButtonTool30.InstanceProps.IsFirstInGroup = true;
			stateButtonTool32.Checked = true;
			buttonTool42.InstanceProps.IsFirstInGroup = true;
			popupMenuTool11.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            listTool1,
            stateButtonTool30,
            stateButtonTool31,
            stateButtonTool32,
            buttonTool42,
            buttonTool43});
			listTool2.SharedPropsInternal.Caption = "Zoom Increments";
			listTool2.SharedPropsInternal.Category = "View";
			buttonTool44.SharedPropsInternal.Caption = "Reduce Page Thumbnails";
			buttonTool44.SharedPropsInternal.Category = "View";
			buttonTool45.SharedPropsInternal.Caption = "Enlarge Page Thumbnails";
			buttonTool45.SharedPropsInternal.Category = "View";
			stateButtonTool33.Checked = true;
			appearance38.Image = ((object)(resources.GetObject("appearance38.Image")));
			stateButtonTool33.SharedPropsInternal.AppearancesSmall.Appearance = appearance38;
			stateButtonTool33.SharedPropsInternal.Caption = "Show Thumbnail Page Numbers";
			stateButtonTool33.SharedPropsInternal.Category = "View";
			buttonTool46.SharedPropsInternal.Caption = "&Close";
			buttonTool46.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.TextOnlyAlways;
			buttonTool46.SharedPropsInternal.ShowInCustomizer = false;
			buttonTool46.SharedPropsInternal.ToolTipText = "Close Preview";
			appearance52.Image = ((object)(resources.GetObject("appearance52.Image")));
			stateButtonTool36.SharedPropsInternal.AppearancesLarge.Appearance = appearance52;
			appearance51.Image = ((object)(resources.GetObject("appearance51.Image")));
			stateButtonTool36.SharedPropsInternal.AppearancesSmall.Appearance = appearance51;
			stateButtonTool36.SharedPropsInternal.Caption = "Landscape";
			stateButtonTool36.SharedPropsInternal.Category = "Tools";
			stateButtonTool36.SharedPropsInternal.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
			stateButtonTool36.SharedPropsInternal.ToolTipText = "용지를 가로 방향으로 설정합니다.";
			this.ultraToolbarsManager1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            stateButtonTool10,
            stateButtonTool11,
            stateButtonTool12,
            stateButtonTool13,
            stateButtonTool14,
            stateButtonTool15,
            buttonTool11,
            comboBoxTool2,
            popupMenuTool4,
            popupMenuTool5,
            popupMenuTool7,
            buttonTool17,
            buttonTool18,
            buttonTool19,
            buttonTool20,
            buttonTool21,
            textBoxTool2,
            buttonTool22,
            buttonTool23,
            buttonTool24,
            buttonTool25,
            stateButtonTool25,
            stateButtonTool26,
            stateButtonTool27,
            popupMenuTool8,
            popupControlContainerTool2,
            buttonTool32,
            stateButtonTool28,
            popupMenuTool9,
            popupMenuTool10,
            popupMenuTool11,
            listTool2,
            buttonTool44,
            buttonTool45,
            stateButtonTool33,
            buttonTool46,
            stateButtonTool36});
			this.ultraToolbarsManager1.ToolKeyDown += new Infragistics.Win.UltraWinToolbars.ToolKeyEventHandler(this.ultraToolbarsManager1_ToolKeyDown);
			this.ultraToolbarsManager1.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.ultraToolbarsManager1_ToolClick);
			this.ultraToolbarsManager1.ToolValueChanged += new Infragistics.Win.UltraWinToolbars.ToolEventHandler(this.ultraToolbarsManager1_ToolValueChanged);
			this.ultraToolbarsManager1.BeforeToolDropdown += new Infragistics.Win.UltraWinToolbars.BeforeToolDropdownEventHandler(this.ultraToolbarsManager1_BeforeToolDropdown);
			// 
			// _UltraPrintPreviewDialog_Toolbars_Dock_Area_Left
			// 
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 89);
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Left.Name = "_UltraPrintPreviewDialog_Toolbars_Dock_Area_Left";
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(26, 389);
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Left.ToolbarsManager = this.ultraToolbarsManager1;
			// 
			// _UltraPrintPreviewDialog_Toolbars_Dock_Area_Right
			// 
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(862, 89);
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Right.Name = "_UltraPrintPreviewDialog_Toolbars_Dock_Area_Right";
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(0, 389);
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Right.ToolbarsManager = this.ultraToolbarsManager1;
			// 
			// _UltraPrintPreviewDialog_Toolbars_Dock_Area_Top
			// 
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Top.Name = "_UltraPrintPreviewDialog_Toolbars_Dock_Area_Top";
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(862, 89);
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Top.ToolbarsManager = this.ultraToolbarsManager1;
			// 
			// _UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom
			// 
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 478);
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom.Name = "_UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom";
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(862, 0);
			this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.ultraToolbarsManager1;
			// 
			// ultraStatusBar1
			// 
			this.ultraStatusBar1.Location = new System.Drawing.Point(0, 478);
			this.ultraStatusBar1.Name = "ultraStatusBar1";
			ultraStatusPanel1.Padding = new System.Drawing.Size(2, 0);
			ultraStatusPanel1.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Spring;
			ultraStatusPanel1.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.AutoStatusText;
			ultraStatusPanel2.Key = "PageNumber";
			ultraStatusPanel2.Padding = new System.Drawing.Size(2, 0);
			ultraStatusPanel2.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
			this.ultraStatusBar1.Panels.AddRange(new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel[] {
            ultraStatusPanel1,
            ultraStatusPanel2});
			this.ultraStatusBar1.Size = new System.Drawing.Size(862, 24);
			this.ultraStatusBar1.TabIndex = 7;
			this.ultraStatusBar1.Text = "ultraStatusBar1";
			this.ultraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2003;
			// 
			// UltraPrintPreviewDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(862, 502);
			this.Controls.Add(this.ultraPrintPreviewControl1);
			this.Controls.Add(this.spltThumbnail);
			this.Controls.Add(this.ultraPrintPreviewThumbnail1);
			this.Controls.Add(this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Left);
			this.Controls.Add(this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Right);
			this.Controls.Add(this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Top);
			this.Controls.Add(this._UltraPrintPreviewDialog_Toolbars_Dock_Area_Bottom);
			this.Controls.Add(this.ultraStatusBar1);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "UltraPrintPreviewDialog";
			this.Load += new System.EventHandler(this.UltraPrintPreviewDialog_Load);
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewThumbnail1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		#region About Dialog and Licensing Interface

		/// <summary>
		/// Display the about dialog
		/// </summary>
		[DesignOnly(true)]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_About")]
		[LocalizedCategory("LC_Design")]
		[EditorBrowsable( EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[ParenthesizePropertyName(true)]
		[Editor(typeof(AboutDialogEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public object About
		{
			get { return null; } 
		}

		/// <summary>
		/// Return the license we cached inside the constructor
		/// </summary>
		UltraLicense IUltraLicensedComponent.License { get { return this.license; } }
	
		#endregion

		#region Event Sinks

		#region Form

		#region Load
		private void UltraPrintPreviewDialog_Load(object sender, System.EventArgs e)
		{
			// AS 1/18/05
			// There is an issue in .net where mouse enter/leave/hover will
			// not fire for a form after it has been shown once using showdialog.
			// Note, this issue doesn't exist in whidbey so don't even try
			// to get involved.
			if (isFirstLoad && Environment.Version.Major >= 2)
				this.canResetTrackMouseEvent = false;
			
			if (!this.isFirstLoad && this.canResetTrackMouseEvent)
				this.ResetTrackMouseEventState();

			this.isFirstLoad = false;

			// AS 3/18/05
			this.InitializeDialogStrings();

			// AS 1/24/05
			// Make sure the status text is cleared.
			this.UpdateCurrentPageInfo();

			this.lastRowsSetting = this.ultraPrintPreviewControl1.Settings.Rows;

			this.ultraStatusBar1.Visible = this.statusBarVisible;
			this.spltThumbnail.Visible = this.thumbnailAreaVisible; 
			this.ultraPrintPreviewThumbnail1.Visible = this.thumbnailAreaVisible; 

			if ( this.ultraToolbarsManager1.Tools.Exists("Thumbnails"))
			{
				StateButtonTool viewThumbnails = this.ultraToolbarsManager1.Tools["Thumbnails"] as StateButtonTool;

				Debug.Assert( viewThumbnails != null );

				if ( viewThumbnails != null )
					viewThumbnails.Checked = this.thumbnailAreaVisible; 
			}

			// make sure the view history is cleared
			this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ClearViewHistory);

			this.UpdateViewHistoryTools();
			this.UpdateMouseActionTools();
			this.UpdateZoomModeTools();
			this.UpdateContinuousTool();

			// AS 2/24/05 BR02258
			this.UpdateDocumentRelatedTools();

			// initialize the control that should be dropped down
			((PopupControlContainerTool)this.ultraToolbarsManager1.Tools["Page Layout"]).Control = this.rowColumnSelector;

			// initialize the thumbnail check state
			((StateButtonTool)this.ultraToolbarsManager1.Tools["Show Page Numbers"]).Checked = this.ultraPrintPreviewThumbnail1.Settings.PageNumberDisplayStyle != PageNumberDisplayStyle.None;
		}
		#endregion //Load

		#endregion //Form

		#region UltraToolbarsManager

		#region BeforeToolDropdown
		private void ultraToolbarsManager1_BeforeToolDropdown(object sender, Infragistics.Win.UltraWinToolbars.BeforeToolDropdownEventArgs e)
		{
			// AS 1/24/05 BR01961
			if (e.Tool.Key == "Page Layout" && this.ultraPrintPreviewControl1 != null && this.ultraPrintPreviewControl1.PageCount == 0)
				e.Cancel = true;
		}
		#endregion //BeforeToolDropdown

		#region ToolClick
		private void ultraToolbarsManager1_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
		{
			switch (e.Tool.Key)
			{
					#region Tools
				case "Hand Tool":
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.Hand;
					break;
				case "Snapshot Tool":
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.Snapshot;
					break;
				case "Dynamic Zoom Tool":
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.DynamicZoom;
					break;
				case "Zoom Out Tool":
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.ZoomOut;
					break;
				case "Zoom In Tool":
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.ZoomIn;
					break;
					#endregion //Tools

					#region Zoom
				case "Zoom In":
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ZoomIn);
					break;
				case "Zoom Out":
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ZoomOut);
					break;

				case "Page Width":
					if ( ((StateButtonTool)e.Tool).Checked )
						this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.PageWidth;
					else
						this.UseStandardZoomModeIfNecessary();
					break;

				case "Margin Width":
					if ( ((StateButtonTool)e.Tool).Checked )
						this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.MarginsWidth;
					else
						this.UseStandardZoomModeIfNecessary();
					break;

				case "Whole Page":
					if ( ((StateButtonTool)e.Tool).Checked )
						this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.WholePage;
					else
						this.UseStandardZoomModeIfNecessary();
					break;

				case "Zoom Increments":
					this.UpdateUsingZoomIncrementsList(e.ListToolItem);
					break;

					#endregion //Zoom

					#region Misc
				case "Thumbnails":
					this.SuspendLayout();
					
					if ( ((StateButtonTool)e.Tool).Checked == true )
						this.ultraPrintPreviewThumbnail1.SendToBack();

					this.ThumbnailAreaVisible = ((StateButtonTool)e.Tool).Checked;
					this.ResumeLayout(true);
					break;

				case "Print":
				{
					// AS 2/24/05 BR02258
					if (this.ultraPrintPreviewControl1.Document == null)
						break;

					// raise the before event
					PrintingEventArgs args = new PrintingEventArgs(this.displayPrintStatus);

					this.OnPrinting( args );

					// if the event wasn't cancelled then call print on the preview control
					if ( !args.Cancel )
					{
						this.ultraPrintPreviewControl1.Print(args.DisplayPrintStatus);

						// raise the after event
						this.OnPrinted( new EventArgs());
					}
					break;
				}

				case "Exit":
				case "ClosePreview":
					if (this.Modal)
						this.DialogResult = DialogResult.OK;
					else
						this.Close();
					break;

				case "Page Setup":
					this.ShowPageSetupDialog();
					break;

					#endregion //Misc

					#region Previous/Next View
				case "Previous View":
					if (this.ultraPrintPreviewControl1.HasPreviousView)
						this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.PreviousView);
					break;

				case "Next View":
					if (this.ultraPrintPreviewControl1.HasNextView)
						this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.NextView);
					break;
					#endregion //Previous/Next View

					#region Scroll Page
				case "First Page":
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToFirstPage);
					break;
				case "Last Page":
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToLastPage);
					break;
				case "Previous Page":
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToPreviousPage);
					break;
				case "Next Page":
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToNextPage);
					break;
					#endregion //Scroll Page

					#region Continuous
				case "Continuous":
					if (((StateButtonTool)e.Tool).Checked)
						this.ultraPrintPreviewControl1.Settings.Rows = 0;
					else
						this.ultraPrintPreviewControl1.Settings.Rows = Math.Max(1, this.lastRowsSetting);
					break;
					#endregion //Continuous

					#region Thumbnails
				case "Reduce Page Thumbnails":
					this.ultraPrintPreviewThumbnail1.PerformAction(UltraPrintPreviewThumbnailAction.ZoomOut);
					break;

				case "Enlarge Page Thumbnails":
					this.ultraPrintPreviewThumbnail1.PerformAction(UltraPrintPreviewThumbnailAction.ZoomIn);
					break;

				case "Show Page Numbers":
					if (((StateButtonTool)e.Tool).Checked)
						this.ultraPrintPreviewThumbnail1.Settings.ResetPageNumberDisplayStyle();
					else
						this.ultraPrintPreviewThumbnail1.Settings.PageNumberDisplayStyle = PageNumberDisplayStyle.None;
					break;

					#endregion //Thumbnails
			}
		}
		#endregion //ToolClick

		#region ToolValueChanged
		private void ultraToolbarsManager1_ToolValueChanged(object sender, Infragistics.Win.UltraWinToolbars.ToolEventArgs e)
		{
			switch(e.Tool.Key)
			{
				case "Zoom":
				{
					ComboBoxTool cbo = e.Tool as ComboBoxTool;

					Debug.WriteLine( string.Format("ToolValueChanged: Text={0}, Value={1}, SelectedIndex={2}", cbo.Text, cbo.Value, cbo.SelectedIndex) );

					// if a value is selected from the list, then update the zoom now
					if (cbo.SelectedIndex >= 0 && object.Equals(cbo.Value, cbo.ValueList.ValueListItems[cbo.SelectedIndex].DataValue))
						((ZoomWrapper)cbo.ValueList.ValueListItems[cbo.SelectedIndex].DataValue).UpdateZoom(this.ultraPrintPreviewControl1.Settings);
					break;
				}
			}
		}
		#endregion //ToolValueChanged

		#region ToolKeyDown
		private void ultraToolbarsManager1_ToolKeyDown(object sender, Infragistics.Win.UltraWinToolbars.ToolKeyEventArgs e)
		{
			if (e.Tool == null)
				return;

			bool successful = false;

			switch(e.Tool.Key)
			{
				case "Zoom":
				{
					#region Zoom
					if (e.KeyData == Keys.Return)
					{
						ComboBoxTool cbo = e.Tool as ComboBoxTool;

						if (cbo.SelectedIndex >= 0)
							((ZoomWrapper)cbo.Value).UpdateZoom(this.ultraPrintPreviewControl1.Settings);
						else
						{
							double newZoom = 0;

							// Strip out percent signs so the parse will work
							string newZoomText = cbo.Text.Replace(@"%", "");

							try	
							{ 
								successful = double.TryParse(newZoomText, NumberStyles.Float, null, out newZoom); 
							}
							catch(Exception exception )
							{
								Debug.Fail("Zoom double.TryParse failed. " + exception.ToString() );
							}

							if (successful)
							{
								// don't allow negative numbers
								if ( newZoom < 0.0d )
									newZoom = - newZoom;

								new ZoomWrapper(newZoom / 100d).UpdateZoom(this.ultraPrintPreviewControl1.Settings);
							}
						}
					}
					break;
					#endregion //Zoom
				}
				case "Current Page":
				{
					#region Current Page

					if (e.KeyData == Keys.Return)
					{
						TextBoxTool txtTool = e.Tool as TextBoxTool;

						double page = 1;

						try
						{
							successful = double.TryParse(txtTool.Text, NumberStyles.Integer, null, out page);
						}
						catch ( Exception exception )
						{
							Debug.Fail("Current Page double.TryParse failed. " + exception.ToString() );
						}

						if (successful)
						{
							if (page > 0 && page <= this.ultraPrintPreviewControl1.PageCount)
							{
								this.ultraPrintPreviewControl1.CurrentPage = (int)page;
								txtTool.SelectionStart = 0;
								txtTool.SelectionLength = txtTool.Text.Length;
							}
						}
					}
					break;
					#endregion //Current Page
				}
			}
		}
		#endregion //ToolKeyDown

		#endregion //UltraToolbarsManager

		#region UltraPrintPreviewControl

		#region CurrentZoomChanged
		private void ultraPrintPreviewControl1_CurrentZoomChanged(object sender, System.EventArgs e)
		{
			//MD 6/1/06 - BR13194
			this.UpdateZoomComboAndList();

			//MD 6/1/06 - BR13194
			// Moved to UpdateZoomComboAndList method
			//
			//// we need to update the combo so its text shows
			//// the current zoom
			//ComboBoxTool cbo = this.ultraToolbarsManager1.Tools["Zoom"] as ComboBoxTool;

			//// prevent the toolvalue changed from firing 
			//// so we don't try to set the zoom
			//bool wasValueChangeEnabled = this.ultraToolbarsManager1.EventManager.IsEnabled(ToolbarEventIds.ToolValueChanged);
			//this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolValueChanged, false);

			//string formattedZoom = this.FormatPercent(this.ultraPrintPreviewControl1.CurrentZoom);

			//// initialize the text
			//cbo.Text = formattedZoom;

			//// re-enable the event
			//this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolValueChanged, wasValueChangeEnabled);

			//// update the enabled state of the zoom buttons
			//this.ultraToolbarsManager1.Tools["Zoom Out"].SharedProps.Enabled = this.ultraPrintPreviewControl1.CanZoomOut;
			//this.ultraToolbarsManager1.Tools["Zoom In"].SharedProps.Enabled = this.ultraPrintPreviewControl1.CanZoomIn;

			//// prevent the toolclick changed from firing 
			//// so we don't try to set the zoom
			//bool wasToolClickEnabled = this.ultraToolbarsManager1.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			//this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);

			//// update the checked state of the zoom increments tool
			//ListTool list = this.ultraToolbarsManager1.Tools["Zoom Increments"] as ListTool;

			//int newIndex = list.ListToolItems.IndexOf(formattedZoom);

			//if (newIndex != list.SelectedItemIndex)
			//    list.SelectedItemIndex = newIndex;

			//// re-enable the event
			//this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolClick, wasToolClickEnabled);
		}
		#endregion //CurrentZoomChanged

		#region ViewHistoryChanged
		private void ultraPrintPreviewControl1_ViewHistoryChanged(object sender, System.EventArgs e)
		{
			this.UpdateViewHistoryTools();
		}
		#endregion //ViewHistoryChanged

		#region CurrentPageChanged
		private void ultraPrintPreviewControl1_CurrentPageChanged(object sender, System.EventArgs e)
		{
			
			this.UpdateCurrentPageInfo();
		}
		#endregion //CurrentPageChanged

		#region PropertyChanged
		private void ultraPrintPreviewControl1_PropertyChanged(object sender, Infragistics.Win.PropertyChangedEventArgs e)
		{
			// find the source of the notification
			PropChangeInfo pci = e.ChangeInfo.FindTrigger(null);

			if (pci.PropId is UltraPrintPreviewControlPropertyIds)
			{
				switch((UltraPrintPreviewControlPropertyIds)pci.PropId)
				{
					// AS 2/24/05 BR02258
					case UltraPrintPreviewControlPropertyIds.Document:
						this.UpdateDocumentRelatedTools();
						break;
					case UltraPrintPreviewControlPropertyIds.Settings:
					{
						this.UpdateZoomModeTools();
						this.UpdateMouseActionTools();
						this.UpdateContinuousTool();
						break;
					}
					case UltraPrintPreviewControlPropertyIds.ZoomMode:
					{
						this.UpdateZoomModeTools();
						break;
					}
					case UltraPrintPreviewControlPropertyIds.MouseAction:
					{
						this.UpdateMouseActionTools();
						break;
					}
					case UltraPrintPreviewControlPropertyIds.Rows:
					{
						this.UpdateContinuousTool();
						break;
					}
				}
			}
		}
		#endregion //PropertyChanged

		#region PreviewGenerated
		private void ultraPrintPreviewControl1_PreviewGenerated(object sender, System.EventArgs e)
		{
			// try not to show more pages then the end user can use but
			// still let them arrange them how they want
			// AS 1/24/05 BR01961
			//this.rowColumnSelector.Columns = Math.Min(this.ultraPrintPreviewControl1.PageCount, 8);
			//this.rowColumnSelector.Rows = Math.Min(this.ultraPrintPreviewControl1.PageCount, 5);
			this.rowColumnSelector.Columns = Math.Max(1, Math.Min(this.ultraPrintPreviewControl1.PageCount, 8));
			this.rowColumnSelector.Rows = Math.Max(1, Math.Min(this.ultraPrintPreviewControl1.PageCount, 5));

			// AS 1/24/05 BR01961
			this.ultraToolbarsManager1.Tools["Page Layout"].SharedProps.Enabled = this.ultraPrintPreviewControl1.PageCount > 0;

			// AS 1/18/05
			this.UpdateMouseActionTools();

			// AS 1/24/05
			this.UpdateCurrentPageInfo();
		}
		#endregion //PreviewGenerated

		#endregion //UltraPrintPreviewControl

		#region PreviewRowColumnSelector

		#region OnRowColumnSelectorMouseUp
		private void OnRowColumnSelectorMouseUp(object sender, MouseEventArgs e)
		{
			System.Drawing.Size selection = this.rowColumnSelector.Selection;

			// if nothing was selected, don't change anything
			if (selection.IsEmpty)
				return;

			this.ultraPrintPreviewControl1.Settings.Columns = selection.Width;

			if (selection.Width == 1 && selection.Height == 1)
			{
				this.ultraPrintPreviewControl1.Settings.Rows = 0;
				this.lastRowsSetting = 1;
			}
			else
			{
				this.ultraPrintPreviewControl1.Settings.Rows = selection.Height;
				this.lastRowsSetting = selection.Height;
			}

			((StateButtonTool)this.ultraToolbarsManager1.Tools["Continuous"]).Checked = false;

			this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.WholePage;

			ToolBase tool = this.ultraToolbarsManager1.GetToolThatContainsControl(this.rowColumnSelector);

			this.ClosePopupContainingTool(tool);

			// AS 1/18/05
			// Because there is a .net bug whereby the mouseenter/leave
			// may not fire for a control after showing a dialog for the
			// second and subsequent time, we should manually force a
			// reset of the selection so it shows as clear when dropped down
			// next time.
			//
			this.rowColumnSelector.ResetSelection();
		}
		#endregion //OnRowColumnSelectorMouseUp

		#endregion //PreviewRowColumnSelector

		#region UltraPrintPreviewThumbnail

		#region Thumbnail CurrentZoomChanged
		private void ultraPrintPreviewThumbnail1_CurrentZoomChanged(object sender, System.EventArgs e)
		{
			this.ultraToolbarsManager1.Tools["Reduce Page Thumbnails"].SharedProps.Enabled = this.ultraPrintPreviewThumbnail1.CanZoomOut;
			this.ultraToolbarsManager1.Tools["Enlarge Page Thumbnails"].SharedProps.Enabled = this.ultraPrintPreviewThumbnail1.CanZoomIn;
		}
		#endregion //Thumbnail CurrentZoomChanged

		#endregion //UltraPrintPreviewThumbnail

		#endregion //Event Sinks

		#region Properties

			#region Shadowed Properties

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new IButtonControl AcceptButton
		{
			get { return base.AcceptButton; }
			set { base.AcceptButton = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new string AccessibleDescription
		{
			get { return base.AccessibleDescription; }
			set { base.AccessibleDescription = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new string AccessibleName
		{
			get { return base.AccessibleName; }
			set { base.AccessibleName = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new AccessibleRole AccessibleRole
		{
			get { return base.AccessibleRole; }
			set { base.AccessibleRole = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool AllowDrop
		{
			get { return base.AllowDrop; }
			set { base.AllowDrop = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new AnchorStyles Anchor
		{
			get { return base.Anchor; }
			set { base.Anchor = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool AutoScale
        {
#pragma warning disable 0618
            get { return base.AutoScale; }
            set { base.AutoScale = value; }
#pragma warning restore 0618
        }

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new System.Drawing.Size AutoScaleBaseSize
		{
			get { return base.AutoScaleBaseSize; }
			set { base.AutoScaleBaseSize = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool AutoScroll
		{
			get { return base.AutoScroll; }
			set { base.AutoScroll = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new System.Drawing.Size AutoScrollMargin
		{
			get { return base.AutoScrollMargin; }
			set { base.AutoScrollMargin = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new System.Drawing.Size AutoScrollMinSize
		{
			get { return base.AutoScrollMinSize; }
			set { base.AutoScrollMinSize = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Color BackColor
		{
			get { return base.BackColor; }
			set { base.BackColor = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new System.Drawing.Image BackgroundImage
		{
			get { return base.BackgroundImage; }
			set { base.BackgroundImage = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new IButtonControl CancelButton
		{
			get { return base.CancelButton; }
			set { base.CancelButton = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool CausesValidation
		{
			get { return base.CausesValidation; }
			set { base.CausesValidation = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size ClientSize
		{
			get { return base.ClientSize; }
			set { base.ClientSize = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ContextMenu ContextMenu
		{
			get { return base.ContextMenu; }
			set { base.ContextMenu = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ControlBox
		{
			get { return base.ControlBox; }
			set { base.ControlBox = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Cursor Cursor
		{
			get { return base.Cursor; }
			set { base.Cursor = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ControlBindingsCollection DataBindings
		{
			get { return base.DataBindings; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new DockStyle Dock
		{
			get { return base.Dock; }
			set { base.Dock = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ScrollableControl.DockPaddingEdges DockPadding
		{
			get { return base.DockPadding; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool Enabled
		{
			get { return base.Enabled; }
			set { base.Enabled = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Font Font
		{
			get { return base.Font; }
			set { base.Font = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Color ForeColor
		{
			get { return base.ForeColor; }
			set { base.ForeColor = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new FormBorderStyle FormBorderStyle
		{
			get { return base.FormBorderStyle; }
			set { base.FormBorderStyle = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool HelpButton
		{
			get { return base.HelpButton; }
			set { base.HelpButton = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Icon Icon
		{
			get { return base.Icon; }
			set { base.Icon = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ImeMode ImeMode
		{
			get { return base.ImeMode; }
			set { base.ImeMode = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool IsMdiContainer
		{
			get { return base.IsMdiContainer; }
			set { base.IsMdiContainer = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool KeyPreview
		{
			get { return base.KeyPreview; }
			set { base.KeyPreview = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Point Location
		{
			get { return base.Location; }
			set { base.Location = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool MaximizeBox
		{
			get { return base.MaximizeBox; }
			set { base.MaximizeBox = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size MaximumSize
		{
			get { return base.MaximumSize; }
			set { base.MaximumSize = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new MainMenu Menu
		{
			get { return base.Menu; }
			set { base.Menu = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool MinimizeBox
		{
			get { return base.MinimizeBox; }
			set { base.MinimizeBox = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size MinimumSize
		{
			get { return base.MinimumSize; }
			set { base.MinimumSize = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Advanced)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new double Opacity
		{
			get { return base.Opacity; }
			set { base.Opacity = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new RightToLeft RightToLeft
		{
			get { return base.RightToLeft; }
			set { base.RightToLeft = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ShowInTaskbar
		{
			get { return base.ShowInTaskbar; }
			set { base.ShowInTaskbar = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size Size
		{
			get { return base.Size; }
			set { base.Size = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new SizeGripStyle SizeGripStyle
		{
			get { return base.SizeGripStyle; }
			set { base.SizeGripStyle = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new FormStartPosition StartPosition
		{
			get { return base.StartPosition; }
			set { base.StartPosition = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool TabStop
		{
			get { return base.TabStop; }
			set { base.TabStop = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new object Tag
		{
			get { return base.Tag; }
			set { base.Tag = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new string Text
		{
			get { return base.Text; }
			set { base.Text = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool TopMost
		{
			get { return base.TopMost; }
			set { base.TopMost = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Color TransparencyKey
		{
			get { return base.TransparencyKey; }
			set { base.TransparencyKey = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool Visible
		{
			get { return base.Visible; }
			set { base.Visible = value; }
		}

		/// <summary>
		/// Shadowed and hidden property
		/// </summary>
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new FormWindowState WindowState
		{
			get { return base.WindowState; }
			set { base.WindowState = value; }
		}

			#endregion Shadowed Properties

			#region Public Properties

				#region AutoGeneratePreview

		/// <summary>
		/// Returns or sets whether the preview is automatically created when the document is changed and when the control is first displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>AutoGeneratePreview</b> property is used to determine whether the control 
		/// should automatically create the print preview pages when they are needed. Normally, the preview 
		/// pages are automatically generated when the control is rendered. You can set this property to 
		/// false to prevent that behavior. You would then need to explicitly call the 
		/// <b>GeneratePreview</b> of the <see cref="PrintPreviewControl"/> when you want the preview pages to be 
		/// synchronously generated.</p>
		/// </remarks>
		/// <seealso cref="PrintPreviewControl"/>
		[DefaultValue(true)]
		[LocalizedCategory("LC_Behavior")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_AutoGeneratePreview")]
		public bool AutoGeneratePreview
		{
			get { return this.ultraPrintPreviewControl1.AutoGeneratePreview; }
			set { this.ultraPrintPreviewControl1.AutoGeneratePreview = value; }
		}

				#endregion //AutoGeneratePreview

				#region DisplayPreviewStatus

		/// <summary>
		/// Returns or sets whether a dialog will be displayed while the preview pages are being generated.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, when a print operation is running a status dialog is 
		/// displayed. This dialog indicates the page that is being printed and also includes a 
		/// button that can be used to end the operation. If you do not want to display this dialog, 
		/// you must set the <b>DisplayPreviewStatus</b> property to false.</p>
		/// </remarks>
		/// <seealso cref="DisplayPrintStatus"/>
		/// <seealso cref="PrintPreviewControl"/>
		[DefaultValue(true)]
		[LocalizedCategory("LC_Behavior")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_DisplayPreviewStatus")]
		public bool DisplayPreviewStatus
		{
			get { return this.ultraPrintPreviewControl1.DisplayPreviewStatus; }
			set { this.ultraPrintPreviewControl1.DisplayPreviewStatus = value; }
		}
				#endregion //DisplayPreviewStatus

				#region DisplayPrintStatus

		/// <summary>
		/// Returns or sets whether a dialog will be displayed while pages are being generated during a print operation.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, when a print operation is running a status dialog is 
		/// displayed. This dialog indicates the page that is being printed and also includes a 
		/// button that can be used to end the operation. If you do not want to display this dialog, 
		/// you must set the <b>DisplayPrintStatus</b> property to false.</p>
		/// <p class="body">This is used as the default value of the parameter passed into 
		/// the <see cref="UltraPrintPreviewDialog.Printing"/> event.</p>
		/// </remarks>
		/// <seealso cref="UltraPrintPreviewDialog.Printing"/>
		/// <seealso cref="UltraPrintPreviewDialog.Printed"/>
		/// <seealso cref="DisplayPreviewStatus"/>
		[DefaultValue(true)]
		[LocalizedCategory("LC_Behavior")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_DisplayPrintStatus")]
		public bool DisplayPrintStatus
		{
			get { return this.displayPrintStatus; }
			set { this.displayPrintStatus = value; }
		}
				#endregion //DisplayPrintStatus

				#region Document

		/// <summary>
		/// The 'Print Document' that will be previewed
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Document</b> property is the <see cref="System.Drawing.Printing.PrintDocument"/> 
		/// that is used to provide the print preview pages that will be displayed by the <see cref="PrintPreviewControl"/> 
		/// and <see cref="PrintPreviewThumbnail"/> controls.</p>
		/// <p class="note"><b>Note:</b> Changing this property will cause the entire preview to be regenerated.</p>
		/// </remarks>
		/// <seealso cref="PrintPreviewControl"/>
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_Document")]
		[LocalizedCategory("LC_Behavior")]
		[DefaultValue(null)]
		public System.Drawing.Printing.PrintDocument Document
		{
			get
			{
				return this.ultraPrintPreviewControl1.Document;
			}
			set
			{
				this.ultraPrintPreviewControl1.Document = value;
				this.pageSetupDialog1.Document = null;
			}
		}

				#endregion //Document

				#region MaximumPreviewPages

		/// <summary>
		/// Returns or sets the maximum number of preview pages that should be generated.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MaximumPreviewPages</b> is used to restrict how many pages 
		/// will be created when the print operation is performed and therefore how many 
		/// pages may be displayed by the control.</p>
		/// <p class="note"><b>Note:</b> Changing this property after the print preview 
		/// operation has completed will not affect how many pages are displayed but 
		/// instead will affect the next print operation, if one occurs.</p>
		/// </remarks>
		/// <seealso cref="PrintPreviewControl"/>
		[DefaultValue(0)]
		[LocalizedCategory("LC_Behavior")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_MaximumPreviewPages")]
		public int MaximumPreviewPages
		{
			get { return this.ultraPrintPreviewControl1.MaximumPreviewPages; }
			set { this.ultraPrintPreviewControl1.MaximumPreviewPages = value; }
		}

				#endregion //MaximumPreviewPages

				#region PreviewMouseAction

		/// <summary>
		/// Returns or sets how the control will react to the left mouse button.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MouseAction</b> is used to determine how the control will 
		/// react while it has the input focus. When used with a value of <b>Hand</b>, the 
		/// left mouse button may be pressed on a page and dragged to scroll the visible 
		/// area. A value of <b>Snapshot</b> is used to allow the end-user to click on 
		/// or lasso an area of a page that should be copied to the clipboard. The area is 
		/// copied using the current zoom value.</p>
		/// <p class="body">The <b>MouseAction</b> also includes several options for 
        /// controlling the current <see cref="Infragistics.Win.Printing.PreviewSettings.Zoom"/> using the 
		/// mouse. When <b>DynamicZoom</b> is used, the mouse may be pressed and dragged 
		/// up or down to synchronously zoom in or out respectively. When <b>ZoomIn</b> is 
		/// used, the end-user can either click on a page to zoom in at a set increment or 
		/// they may lasso the area that they want to zoom into. Lastly, when <b>ZoomOut</b> 
		/// is used, the end-user can either click on a page to zoom out to a fixed increment 
		/// or they may lasso an area that is used to determine the new zoom level.</p>
		/// </remarks>
		/// <seealso cref="PrintPreviewControl"/>
		[DefaultValue(PreviewMouseAction.Hand)]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_PreviewMouseAction")]
		[LocalizedCategory("LC_Behavior")]
		public PreviewMouseAction PreviewMouseAction
		{
			get { return this.ultraPrintPreviewControl1.MouseAction; }
			set { this.ultraPrintPreviewControl1.MouseAction = value; }
		}

				#endregion //PreviewMouseAction

				#region PreviewSettings
		
		/// <summary>
		/// Returns an object used to modify settings for the print preview control.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Settings</b> property returns an instance of a 
		/// <see cref="PreviewSettings"/> class. The settings of this object are 
		/// used to change common preview settings such as the maximum number of 
        /// <see cref="Infragistics.Win.Printing.PreviewSettings.Rows"/> and <see cref="Infragistics.Win.Printing.PreviewSettings.Columns"/> 
		/// that are displayed within the <see cref="PrintPreviewControl"/>.</p>
		/// </remarks>
		/// <seealso cref="ThumbnailSettings"/>
		/// <seealso cref="PrintPreviewControl"/>
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_PreviewSettings")]
		[LocalizedCategory("LC_Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PreviewSettings PreviewSettings
		{
			get 
			{
				return this.ultraPrintPreviewControl1.Settings;
			}
		}

		/// <summary>
		/// Returns a Boolean value that determines whether the <see cref="PreviewSettings"/> property is set to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">This method returns true if any properties on the <see cref="PreviewSettings"/> have been changed from their default values.</p>
		/// </remarks>
		/// <seealso cref="PreviewSettings"/>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializePreviewSettings()
		{
			return this.ultraPrintPreviewControl1.ShouldSerializeSettings();
		}

		/// <summary>
		/// Resets the <see cref="PreviewSettings"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Resets the property values of the <see cref="PreviewSettings"/> to their default values.</p>
		/// </remarks>
		/// <seealso cref="PreviewSettings"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetPreviewSettings()
		{
			this.ultraPrintPreviewControl1.ResetSettings();
		}

		#endregion //PreviewSettings

				#region PrintPreviewControl

		/// <summary>
		/// The preview control on the dialog
		/// </summary>
		/// <remarks>
		/// <p class="body">This is a read-only property that returns the <b>UltraPrintPreviewControl</b> 
		/// on the dialog that displays the preview pages generated using the specified 
		/// <see cref="Document"/>.</p>
		/// </remarks>
		/// <seealso cref="PreviewSettings"/>
		/// <seealso cref="StatusBar"/>
		/// <seealso cref="ToolbarsManager"/>
		/// <seealso cref="PrintPreviewThumbnail"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		public UltraPrintPreviewControl PrintPreviewControl
		{
			get
			{
				return this.ultraPrintPreviewControl1;
			}
		}

				#endregion //PrintPreviewControl

				#region PrintPreviewThumbnail

		/// <summary>
		/// The thumbnail control on the dialog
		/// </summary>
		/// <remarks>
		/// <p class="body">This is a read-only property that returns the <b>UltraPrintPreviewThumbnail</b> 
		/// on the dialog that displays thumbnails of the preview pages generated using the specified 
		/// <see cref="Document"/>.</p>
		/// </remarks>
		/// <seealso cref="ThumbnailSettings"/>
		/// <seealso cref="StatusBar"/>
		/// <seealso cref="ToolbarsManager"/>
		/// <seealso cref="PrintPreviewControl"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		public UltraPrintPreviewThumbnail PrintPreviewThumbnail
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1;
			}
		}

				#endregion //PrintPreviewThumbnail

				#region StatusBar

		/// <summary>
		/// The status bar.
		/// </summary>
		/// <remarks>
		/// <p class="body">This is a read-only property that returns the <b>UltraStatusBar</b> 
		/// that is displayed on the dialog. The statusbar by default contains 2 panels - one 
		/// which displays status information about toolbar tools and controls on the dialog and 
		/// another that displays the current and total page count.</p>
		/// </remarks>
		/// <seealso cref="ToolbarsManager"/>
		/// <seealso cref="PrintPreviewControl"/>
		/// <seealso cref="PrintPreviewThumbnail"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		public UltraStatusBar StatusBar
		{
			get
			{
				return this.ultraStatusBar1;
			}
		}

				#endregion //StatusBar

				#region StatusBarVisible
		/// <summary>
		/// Returns or sets whether the status bar is visible.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>StatusBarVisible</b> is a boolean property that is 
		/// used to hide/show the <see cref="StatusBar"/> on the dialog is displayed. By default, this 
		/// property is set to true so that statusbar will be displayed.</p>
		/// </remarks>
		/// <seealso cref="StatusBar"/>
		[DefaultValue(true)]
		[LocalizedCategory("LC_Thumbnail")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_StatusBarVisible")]
		public bool StatusBarVisible
		{
			get { return this.statusBarVisible; }
			set 
			{
				this.statusBarVisible = value;
				this.ultraStatusBar1.Visible = value; 
			}
		}
				#endregion //StatusBarVisible

				#region Style

		/// <summary>
		/// Returns or sets the style of toolbars and the status bar in the dialog.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Style</b> is used to determine the 
		/// display style for the <see cref="ToolbarsManager"/> and <see cref="StatusBar"/> 
		/// that are on the dialog.</p>
		/// </remarks>
		/// <seealso cref="StatusBar"/>
		/// <seealso cref="ToolbarsManager"/>
		[DefaultValue(ToolbarStyle.Office2003)]
		[LocalizedCategory("LC_Appearance")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_Style")]
		public ToolbarStyle Style
		{
			get { return this.ultraToolbarsManager1.Style; }
			set 
			{ 
				this.ultraToolbarsManager1.Style = value; 

				ViewStyle viewStyle = ViewStyle.Standard;

				switch ( value )
				{
					case ToolbarStyle.Office2003:
						viewStyle = ViewStyle.Office2003;
						break;
					case ToolbarStyle.VisualStudio2005:
						viewStyle = ViewStyle.VisualStudio2005;
						break;
				}

				this.ultraStatusBar1.ViewStyle = viewStyle;
			}
		}

				#endregion //Style

				#region ThumbnailAreaVisible
		/// <summary>
		/// Returns or sets whether the thumbnail area is visible when the dialog is shown initially.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ThumbnailAreaVisible</b> is a boolean property that is 
		/// used to hide/show the <see cref="PrintPreviewThumbnail"/> on the dialog is displayed. By default, this 
		/// property is set to true so that thumbnail will be displayed.</p>
		/// </remarks>
		/// <seealso cref="PrintPreviewThumbnail"/>
		[DefaultValue(true)]
		[LocalizedCategory("LC_Thumbnail")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_ThumbnailAreaVisible")]
		public bool ThumbnailAreaVisible
		{
			get { return this.thumbnailAreaVisible; }
			set 
			{
				this.thumbnailAreaVisible = value;
				this.spltThumbnail.Visible = value; 
				this.ultraPrintPreviewThumbnail1.Visible = value; 
			}
		}
				#endregion //ThumbnailAreaVisible

				#region ThumbnailCurrentPreviewPageNumberAppearance

		/// <summary>
		/// Returns or sets the appearance used to render the page number for the <see cref="UltraPrintPreviewControlBase.CurrentPage"/> of the associated <see cref="UltraPrintPreviewThumbnail.PreviewControl"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ThumbnailCurrentPreviewPageNumberAppearance</b> is an <see cref="AppearanceBase"/> that is 
        /// used to render the page number (see <see cref="Infragistics.Win.Printing.PreviewSettings.PageNumberDisplayStyle"/>) of the 
		/// <see cref="PrintPreviewThumbnail"/> based on the <see cref="UltraPrintPreviewControlBase.CurrentPage"/> of 
		/// the <see cref="PrintPreviewControl"/>.</p>
		/// </remarks>
		/// <seealso cref="PrintPreviewThumbnail"/>
		/// <seealso cref="ThumbnailSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedCategory("LC_Thumbnail")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_ThumbnailCurrentPreviewPageNumberAppearance")]
		public AppearanceBase ThumbnailCurrentPreviewPageNumberAppearance
		{
			get { return this.ultraPrintPreviewThumbnail1.CurrentPreviewPageNumberAppearance; }
			set { this.ultraPrintPreviewThumbnail1.CurrentPreviewPageNumberAppearance = value; }
		}

		/// <summary>
		/// <p class="body">Returns a Boolean value that determines whether the <see cref="ThumbnailCurrentPreviewPageNumberAppearance"/> property is set to its default value.</p>
		/// </summary>
		/// <remarks>
		/// <p class="body">This is a read-only property that returns a boolean indicating if any properties of the <see cref="AppearanceBase"/> 
		/// of the <see cref="ThumbnailCurrentPreviewPageNumberAppearance"/> have been modified from their default values.</p>
		/// </remarks>
		/// <seealso cref="ThumbnailCurrentPreviewPageNumberAppearance"/>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeThumbnailCurrentPreviewPageNumberAppearance()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeCurrentPreviewPageNumberAppearance();
		}

		/// <summary>
		/// Resets the <see cref="ThumbnailCurrentPreviewPageNumberAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Resets the property values of the <see cref="ThumbnailCurrentPreviewPageNumberAppearance"/> to their default values.</p>
		/// </remarks>
		/// <seealso cref="ThumbnailCurrentPreviewPageNumberAppearance"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailCurrentPreviewPageNumberAppearance()
		{
			 this.ultraPrintPreviewThumbnail1.ResetCurrentPreviewPageNumberAppearance();
		}

				#endregion //ThumbnailCurrentPreviewPageNumberAppearance

				#region ThumbnailCurrentPreviewPageAppearance

		/// <summary>
		/// Returns or sets the appearance used to render the page for the <see cref="UltraPrintPreviewControlBase.CurrentPage"/> of the associated <see cref="UltraPrintPreviewThumbnail.PreviewControl"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ThumbnailCurrentPreviewPageAppearance</b> is an <see cref="AppearanceBase"/> that is 
		/// used to render the page of the <see cref="PrintPreviewThumbnail"/> based on the 
		/// <see cref="UltraPrintPreviewControlBase.CurrentPage"/> of the 
		/// <see cref="PrintPreviewControl"/>.</p>
		/// </remarks>
		/// <seealso cref="PrintPreviewThumbnail"/>
		/// <seealso cref="ThumbnailSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedCategory("LC_Thumbnail")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_ThumbnailCurrentPreviewPageAppearance")]
		public AppearanceBase ThumbnailCurrentPreviewPageAppearance
		{
			get { return this.ultraPrintPreviewThumbnail1.CurrentPreviewPageAppearance; }
			set { this.ultraPrintPreviewThumbnail1.CurrentPreviewPageAppearance = value; }
		}

		/// <summary>
		/// <p class="body">Returns a Boolean value that determines whether the <see cref="ThumbnailCurrentPreviewPageAppearance"/> property is set to its default value.</p>
		/// </summary>
		/// <remarks>
		/// <p class="body">This is a read-only property that returns a boolean indicating if any properties of the <see cref="AppearanceBase"/> 
		/// of the <see cref="ThumbnailCurrentPreviewPageAppearance"/> have been modified from their default values.</p>
		/// </remarks>
		/// <seealso cref="ThumbnailCurrentPreviewPageAppearance"/>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeThumbnailCurrentPreviewPageAppearance()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeCurrentPreviewPageAppearance();
		}

		/// <summary>
		/// Resets the <see cref="ThumbnailCurrentPreviewPageAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Resets the property values of the <see cref="ThumbnailCurrentPreviewPageAppearance"/> to their default values.</p>
		/// </remarks>
		/// <seealso cref="ThumbnailCurrentPreviewPageAppearance"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailCurrentPreviewPageAppearance()
		{
			 this.ultraPrintPreviewThumbnail1.ResetCurrentPreviewPageAppearance();
		}

				#endregion //ThumbnailCurrentPreviewPageAppearance

				#region ThumbnailSettings
		
		/// <summary>
		/// Returns an object used to modify settings for the print preview thumbnail control.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Settings</b> property returns an instance of a 
		/// <see cref="PreviewSettings"/> class. The settings of this object are 
		/// used to change common preview settings such as the maximum number of 
        /// <see cref="Infragistics.Win.Printing.PreviewSettings.Rows"/> and <see cref="Infragistics.Win.Printing.PreviewSettings.Columns"/> 
		/// that are displayed within the <see cref="PrintPreviewThumbnail"/>.</p>
		/// </remarks>
		/// <seealso cref="PreviewSettings"/>
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_ThumbnailSettings")]
		[LocalizedCategory("LC_Thumbnail")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PreviewSettings ThumbnailSettings
		{
			get 
			{
				return this.ultraPrintPreviewThumbnail1.Settings;
			}
		}

		/// <summary>
		/// Returns a Boolean value that determines whether the <see cref="ThumbnailSettings"/> property is set to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">This method returns true if any properties on the <see cref="ThumbnailSettings"/> have been changed from their default values.</p>
		/// </remarks>
		/// <seealso cref="ThumbnailSettings"/>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeThumbnailSettings()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeSettings();
		}

		/// <summary>
		/// Resets the <see cref="ThumbnailSettings"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Resets the property values of the <see cref="ThumbnailSettings"/> to their default values.</p>
		/// </remarks>
		/// <seealso cref="ThumbnailSettings"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailSettings()
		{
			this.ultraPrintPreviewThumbnail1.ResetSettings();
		}

		#endregion //ThumbnailSettings

				#region ThumbnailViewBoxDisplayStyle

		/// <summary>
		/// Returns or sets a value indicating where the view box will be displayed in the thumbnail pages.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ThumbnailViewBoxDisplayStyle</b> determines which preview pages, if any, will include 
		/// a viewbox element. The viewbox element indicates what portion of that page is currently displayed within 
		/// the associated <b>PreviewControl</b> of the <see cref="PrintPreviewThumbnail"/>.</p>
		/// </remarks>
		/// <seealso cref="PrintPreviewThumbnail"/>
		[DefaultValue(ViewBoxDisplayStyle.AllVisiblePreviewPages)]
		[LocalizedCategory("LC_Thumbnail")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_ThumbnailViewBoxDisplayStyle")]
		public ViewBoxDisplayStyle ThumbnailViewBoxDisplayStyle
		{
			get { return this.ultraPrintPreviewThumbnail1.ViewBoxDisplayStyle; }
			set { this.ultraPrintPreviewThumbnail1.ViewBoxDisplayStyle = value; }
		}

				#endregion //ThumbnailViewBoxDisplayStyle

				#region ThumbnailViewBoxDragMode

		/// <summary>
		/// Returns or sets a value indicating where the thumbnail view box may be dragged within the page.
		/// </summary>
		/// <seealso cref="ThumbnailViewBoxDisplayStyle"/>
		/// <seealso cref="PrintPreviewThumbnail"/>
		[DefaultValue(ViewBoxDragMode.Default)]
		[LocalizedCategory("LC_Thumbnail")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_ThumbnailViewBoxDragMode")]
		public ViewBoxDragMode ThumbnailViewBoxDragMode
		{
			get { return this.ultraPrintPreviewThumbnail1.ViewBoxDragMode; }
			set { this.ultraPrintPreviewThumbnail1.ViewBoxDragMode = value; }
		}
				#endregion //ThumbnailViewBoxDragMode

				#region ThumbnailViewBoxAppearance

		/// <summary>
		/// Returns or sets the appearance used to render the view box. The view box is the area within a page that indicates what portion of a page is currently in view in the associated preview control.
		/// </summary>
		/// <remarks>
		/// <p class="body">The viewbox is the area within the preview pages of the <b>UltraPrintPreviewThumbnail</b> 
		/// that indicates the pages currently in view within the associated <b>PreviewControl</b> of the associated <see cref="PrintPreviewThumbnail"/>.</p>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedCategory("LC_Thumbnail")]
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_ThumbnailViewBoxAppearance")]
		public AppearanceBase ThumbnailViewBoxAppearance
		{
			get { return this.ultraPrintPreviewThumbnail1.ViewBoxAppearance; }
			set { this.ultraPrintPreviewThumbnail1.ViewBoxAppearance = value; }
		}

		/// <summary>
		/// <p class="body">Returns a Boolean value that determines whether the <see cref="ThumbnailViewBoxAppearance"/> property is set to its default value.</p>
		/// </summary>
		/// <remarks>
		/// <p class="body">This is a read-only property that returns a boolean indicating if any properties of the <see cref="AppearanceBase"/> 
		/// of the <see cref="ThumbnailViewBoxAppearance"/> have been modified from their default values.</p>
		/// </remarks>
		/// <seealso cref="ThumbnailViewBoxAppearance"/>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeThumbnailViewBoxAppearance()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeViewBoxAppearance();
		}

		/// <summary>
		/// Resets the <see cref="ThumbnailViewBoxAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Resets the property values of the <see cref="ThumbnailViewBoxAppearance"/> to their default values.</p>
		/// </remarks>
		/// <seealso cref="ThumbnailViewBoxAppearance"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailViewBoxAppearance()
		{
			this.ultraPrintPreviewThumbnail1.ResetViewBoxAppearance();
		}

				#endregion //ThumbnailViewBoxAppearance

				#region ToolbarsManager

		/// <summary>
		/// The toolbars manager.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ToolbarsManager</b> returns the <b>UltraToolbarsManager</b> 
		/// that is used by the dialog to provide the toolbars that the end-user can use to interact 
		/// with the preview pages.</p>
		/// </remarks>
		/// <seealso cref="StatusBar"/>
		/// <seealso cref="PrintPreviewControl"/>
		/// <seealso cref="PrintPreviewThumbnail"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		[ EditorBrowsable(EditorBrowsableState.Never)]
		public UltraToolbarsManager ToolbarsManager
		{
			get
			{
				return this.ultraToolbarsManager1;
			}
		}

				#endregion //ToolbarsManager

				#region UseAntiAlias

		/// <summary>
		/// Returns or sets whether the print preview will be rendered using anti aliasing.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property is used to determine whether the print preview will 
		/// be rendered using anti-aliasing. By default, anti-aliasing is not used.</p>
		/// <p class="note"><b>Note:</b> Changing this property will cause the entire preview to be regenerated.</p>
		/// </remarks>
		/// <seealso cref="PrintPreviewControl"/>
		[LocalizedDescription("LD_UltraPrintPreviewDialog_P_UseAntiAlias")]
		[LocalizedCategory("LC_Behavior")]
		[DefaultValue(false)]
		public bool UseAntiAlias
		{
			get
			{
				return this.ultraPrintPreviewControl1.UseAntiAlias;
			}
			set
			{
				this.ultraPrintPreviewControl1.UseAntiAlias = value;
			}
		}

				#endregion //UseAntiAlias

			#endregion //Public Properties

		#endregion //Properties

		#region Methods

		#region InvalidatePreview

		/// <summary>
		/// Used to dirty the preview information so that it will be regenerated upon the next request.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>InvalidatePreview</b> method is used to invalidate the 
		/// preview as displayed by the contained <see cref="PrintPreviewControl"/> thereby 
		/// causing a new print preview operation to occur regenerating the preview pages.</p>
		/// </remarks>
		/// <seealso cref="PrintPreviewControl"/>
		public void InvalidatePreview()
		{
			this.ultraPrintPreviewControl1.InvalidatePreview();			
		}

		#endregion //InvalidatePreview

		#region FormatPercent
		private string FormatPercent(double percent)
		{
			double tmp = percent * 100d;

			if (tmp % 1 == 0)
				return percent.ToString("P0");
			else if (tmp % .1 == 0)
				return percent.ToString("P1");
			else
				return percent.ToString("P2");
		}
		#endregion //FormatPercent

		#region UpdateCurrentPageInfo
		private void UpdateCurrentPageInfo()
		{
			// AS 1/24/05
			int currentPage = this.ultraPrintPreviewControl1.CurrentPage;
			int pageCount = this.ultraPrintPreviewControl1.PageCount;

			TextBoxTool textTool = this.ultraToolbarsManager1.Tools["Current Page"] as TextBoxTool;

			if (textTool != null)
			{
				if (pageCount == 0)
					textTool.Text = string.Empty;
				else
					textTool.Text = currentPage.ToString();
			}

			if ( this.ultraStatusBar1.Panels.Exists("PageNumber") )
			{
				if (pageCount == 0)
					this.ultraStatusBar1.Panels["PageNumber"].Text = string.Empty;
				else
					this.ultraStatusBar1.Panels["PageNumber"].Text = Shared.SR.GetString("StatusBar_Page_X_OF_X", new object[] { currentPage, pageCount } );
			}
		}
		#endregion //UpdateCurrentPageInfo

		#region UpdateZoomModeTools
		private void UpdateZoomModeTools()
		{
			bool pageWidth = this.ultraPrintPreviewControl1.Settings.ZoomMode == ZoomMode.PageWidth;
			bool marginWidth = this.ultraPrintPreviewControl1.Settings.ZoomMode == ZoomMode.MarginsWidth;
			bool wholePage = this.ultraPrintPreviewControl1.Settings.ZoomMode == ZoomMode.WholePage;

			bool wasToolClickEnabled = this.ultraToolbarsManager1.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);

			((StateButtonTool)this.ultraToolbarsManager1.Tools["Page Width"]).Checked = pageWidth;
			((StateButtonTool)this.ultraToolbarsManager1.Tools["Margin Width"]).Checked = marginWidth;
			((StateButtonTool)this.ultraToolbarsManager1.Tools["Whole Page"]).Checked = wholePage;

			this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolClick, wasToolClickEnabled);
		}
		#endregion //UpdateZoomModeTools

		#region UseStandardZoomModeIfNecessary
		private void UseStandardZoomModeIfNecessary()
		{
			if (this.ultraToolbarsManager1.OptionSets["ZoomMode"].SelectedTool == null &&
				this.ultraPrintPreviewControl1.Settings.ZoomMode != ZoomMode.Standard)
			{
				double newZoom = this.ultraPrintPreviewControl1.CurrentZoom;
				this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.Standard;
				this.ultraPrintPreviewControl1.Settings.Zoom = newZoom;
			}
		}
		#endregion //UseStandardZoomModeIfNecessary

		#region UpdateMouseActionTools
		private void UpdateMouseActionTools()
		{
			string newContextMenuKey = null;
			string newToolKey = null;

			// AS 1/18/05
			// Base it on the resolved property.
			//
			//switch(this.ultraPrintPreviewControl1.MouseAction)
			switch(this.ultraPrintPreviewControl1.MouseActionResolved)
			{
				case PreviewMouseAction.Hand:
					newToolKey = "Hand Tool";
					newContextMenuKey = "ContextMenuPreviewHand";
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, SR.GetString("StatusBar_Hand_Instructions"));
					break;
				case PreviewMouseAction.DynamicZoom:
					newToolKey = "Dynamic Zoom Tool";
					newContextMenuKey = "ContextMenuPreviewZoom";
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, SR.GetString("StatusBar_DynamicZoom_Instructions"));
					break;
				case PreviewMouseAction.Snapshot:
					newToolKey = "Snapshot Tool";
					newContextMenuKey = "ContextMenuPreviewHand";
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, SR.GetString("StatusBar_SnapShot_Instructions"));
					break;
				case PreviewMouseAction.ZoomIn:
					newToolKey = "Zoom In Tool";
					newContextMenuKey = "ContextMenuPreviewZoom";
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, SR.GetString("StatusBar_ZoomIn_Instructions"));
					break;
				case PreviewMouseAction.ZoomOut:
					newToolKey = "Zoom Out Tool";
					newContextMenuKey = "ContextMenuPreviewZoom";
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, SR.GetString("StatusBar_ZoomOut_Instructions"));
					break;
				default:
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "");
					break;
			}

			if (newToolKey != null)
			{
				// AS 1/18/05
				// Disable the event while updating.
				bool wasToolClickEnabled = this.ultraToolbarsManager1.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
				this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);

				((StateButtonTool)this.ultraToolbarsManager1.Tools[newToolKey]).Checked = true;

				// AS 1/18/05
				this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolClick, wasToolClickEnabled);
			}

			if (newContextMenuKey != null)
				this.ultraToolbarsManager1.SetContextMenuUltra(this.ultraPrintPreviewControl1, newContextMenuKey);
		}
		#endregion //UpdateMouseActionTools

		#region UpdateViewHistoryTools
		private void UpdateViewHistoryTools()
		{
			this.ultraToolbarsManager1.Tools["Previous View"].SharedProps.Enabled = this.ultraPrintPreviewControl1.HasPreviousView;
			this.ultraToolbarsManager1.Tools["Next View"].SharedProps.Enabled = this.ultraPrintPreviewControl1.HasNextView;
		}
		#endregion //UpdateViewHistoryTools

		#region UpdateContinuousTool
		private void UpdateContinuousTool()
		{
			bool wasToolClickEnabled = this.ultraToolbarsManager1.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);

			((StateButtonTool)this.ultraToolbarsManager1.Tools["Continuous"]).Checked = this.ultraPrintPreviewControl1.Settings.Rows == 0;

			this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolClick, wasToolClickEnabled);
		}
		#endregion //UpdateContinuousTool

		#region ClosePopupContainingTool
		private void ClosePopupContainingTool(ToolBase tool)
		{
			if (tool == null)
				return;

			if (tool.OwnerIsMenu)
				this.ClosePopupContainingTool(tool.OwningMenu);
			else if (tool.OwnerIsToolbar && tool.OwningToolbar.TearawayToolOwner != null)
				this.ClosePopupContainingTool(tool.OwningToolbar.TearawayToolOwner);
			else if (tool is PopupToolBase)
				((PopupToolBase)tool).ClosePopup();
		}
		#endregion //ClosePopupContainingTool

		#region ShowPageSetupDialog
		private void ShowPageSetupDialog()
		{
			// AS 2/24/05 BR02258
			if (this.ultraPrintPreviewControl1.Document == null)
				return;
			
			// initialize the print document for which
			// the dialog will be displayed
			this.pageSetupDialog1.Document = this.ultraPrintPreviewControl1.Document;
		
			// cache the settings
			PageSettings origPageSettings = this.pageSetupDialog1.PageSettings.Clone() as PageSettings;

			// AS 3/3/05 BR02654
			// For some reason, cloning the object doesn't really clone it because when
			// the dialog comes back the clone that was created before showing the dialog
			// is also modified after the dialog has been shown. It seems like they aren't 
			// really cloning it until its dirtied.
			//
			origPageSettings.Landscape = origPageSettings.Landscape;

			// AS 3/3/05
			// This wasn't being used.
			//PrinterSettings origPrinterSettings = this.pageSetupDialog1.PrinterSettings.Clone() as PrinterSettings;

			// AS 1/24/05 BR01978
			// There is a .net bug whereby the margins are not being converted properly
			// when running on a system with metrics measurement system. Basically,
			// they end up assuming that the system is non-metric and convert from
			// Display (Hundreths of an inch) to ThousandsOfAnInch instead of
			// from Display to HundredsOfAMillimeter. To account for this, we need
			// to offset the margins before showing the dialog.
			// See http://support.microsoft.com/default.aspx?scid=kb;en-us;814355
			//
			bool adjustForMetricSystem = false;

			if (Environment.Version.Major < 2)
				adjustForMetricSystem = NativeWindowMethods.IsMetric();
			else
			{
				try
				{
                    // MRS 1/4/06 - BR08574
                    // We should only be doing this when IsMetric is true.
                    //
                    if (NativeWindowMethods.IsMetric())
                    {
                        PropertyDescriptor pd = TypeDescriptor.GetProperties(this.pageSetupDialog1)["EnableMetric"];

                        Debug.Assert(pd != null, "The 'EnableMetric' property of the PageSetupDialog in whidbey may have been changed!");

                        // whidbey exposes a property so only adjust if the property
                        // is not set to make the adjustment
                        if (pd != null && pd.PropertyType == typeof(bool))
                            adjustForMetricSystem = !(bool)pd.GetValue(this.pageSetupDialog1);
                    }
				}
				catch 
				{
				}
			}

			if (adjustForMetricSystem)
			{
				this.pageSetupDialog1.PageSettings.Margins = PrinterUnitConvert.Convert(
					this.pageSetupDialog1.PageSettings.Margins,
					PrinterUnit.Display,
					PrinterUnit.TenthsOfAMillimeter);
			}

			// raise the PageSetupDialogDisplaying event
			PageSetupDialogDisplayingEventArgs args = new PageSetupDialogDisplayingEventArgs(this.pageSetupDialog1);

			this.OnPageSetupDialogDisplaying( args );

			// if the event was canceled then bail out
			if ( args.Cancel == true )
				return;

			if (this.pageSetupDialog1.ShowDialog(this) == DialogResult.OK)
			{
				if (IsNewPreviewRequired(origPageSettings, this.pageSetupDialog1.PageSettings))
					this.InvalidatePreview();
			}
            // MBS 4/10/06 BR10945
            // If the user has cancelled this dialog, we need to convert the values back to Display,
            // otherwise the margins will approximately double if the user re-opens the dialog.
            else if (adjustForMetricSystem)
            {
                this.pageSetupDialog1.PageSettings.Margins = PrinterUnitConvert.Convert(
                    this.pageSetupDialog1.PageSettings.Margins,
                    PrinterUnit.TenthsOfAMillimeter,
                    PrinterUnit.Display);
            }
		}
		#endregion //ShowPageSetupDialog

		#region IsSameObject
		private static bool IsSameObject(object printObject1, object printObject2)
		{
			if (printObject1 == printObject2)
				return true;

			if (printObject1 == null || printObject2 == null)
				return false;

			if (printObject1.GetType() != printObject2.GetType())
				return false;

			if (printObject1 is PaperSize)
			{
				PaperSize p1 = printObject1 as PaperSize;
				PaperSize p2 = printObject2 as PaperSize;

				return p1.Kind == p2.Kind	&&
					p1.Height == p2.Height	&&
					p1.Width == p2.Width	&&
					p1.PaperName == p2.PaperName;
			}
			else if (printObject1 is PaperSource)
			{
				PaperSource p1 = printObject1 as PaperSource;
				PaperSource p2 = printObject2 as PaperSource;

				return p1.Kind == p2.Kind &&
					p1.SourceName == p2.SourceName;
			}
			else if (printObject1 is PrinterResolution)
			{
				PrinterResolution p1 = printObject1 as PrinterResolution;
				PrinterResolution p2 = printObject2 as PrinterResolution;

				return p1.Kind == p2.Kind	&&
					p1.X == p2.X			&&
					p1.Y == p2.Y;
			}

			return false;
		}
		#endregion //IsSameObject

		#region InitializeDialogStrings
		private void InitializeDialogStrings()
		{
			this.Text = SR.GetString("PrintPreview_DialogCaption");

			//Accessibility
			this.ultraPrintPreviewControl1.AccessibleName = SR.GetString("AccessibleName_Preview");
			this.ultraPrintPreviewThumbnail1.AccessibleName = SR.GetString("AccessibleName_Thumbnail");
			this.ultraStatusBar1.AccessibleName = SR.GetString("AccessibleName_StatusBar");

			this.ultraPrintPreviewControl1.AccessibleDescription = SR.GetString("AccessibleDescription_Preview");
			this.ultraPrintPreviewThumbnail1.AccessibleDescription = SR.GetString("AccessibleDescription_Thumbnail");
			this.ultraStatusBar1.AccessibleDescription = SR.GetString("AccessibleDescription_StatusBar");

			this.ultraToolbarsManager1.Tools["Thumbnails"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Thumbnails");
			this.ultraToolbarsManager1.Tools["Hand Tool"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Hand_Tool");
			this.ultraToolbarsManager1.Tools["Snapshot Tool"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Snapshot_Tool");
			this.ultraToolbarsManager1.Tools["Dynamic Zoom Tool"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Dynamic_Zoom_Tool");
			this.ultraToolbarsManager1.Tools["Zoom Out Tool"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Zoom_Out_Tool");
			this.ultraToolbarsManager1.Tools["Zoom In Tool"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Zoom_In_Tool");
			this.ultraToolbarsManager1.Tools["Print"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Print");
			this.ultraToolbarsManager1.Tools["Zoom"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Zoom");
			this.ultraToolbarsManager1.Tools["File"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_File");
			this.ultraToolbarsManager1.Tools["View"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_View");
			this.ultraToolbarsManager1.Tools["Tools"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Tools");
			this.ultraToolbarsManager1.Tools["Exit"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Exit");
			this.ultraToolbarsManager1.Tools["Zoom In"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Zoom_In");
			this.ultraToolbarsManager1.Tools["Zoom Out"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Zoom_Out");
			this.ultraToolbarsManager1.Tools["Previous View"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Previous_View");
			this.ultraToolbarsManager1.Tools["Next View"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Next_View");
			this.ultraToolbarsManager1.Tools["Current Page"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Current_Page");
			this.ultraToolbarsManager1.Tools["First Page"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_First_Page");
			this.ultraToolbarsManager1.Tools["Next Page"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Next_Page");
			this.ultraToolbarsManager1.Tools["Previous Page"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Previous_Page");
			this.ultraToolbarsManager1.Tools["Last Page"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Last_Page");
			this.ultraToolbarsManager1.Tools["Page Width"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Page_Width");
			this.ultraToolbarsManager1.Tools["Margin Width"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Margin_Width");
			this.ultraToolbarsManager1.Tools["Whole Page"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Whole_Page");
			this.ultraToolbarsManager1.Tools["Go To"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Go_To");
			this.ultraToolbarsManager1.Tools["Page Layout"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Page_Layout");
			this.ultraToolbarsManager1.Tools["Page Setup"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Page_Setup");
			this.ultraToolbarsManager1.Tools["Continuous"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Continuous");
			this.ultraToolbarsManager1.Tools["ContextMenuThumbnail"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_ContextMenuThumbnail");
			this.ultraToolbarsManager1.Tools["ContextMenuPreviewHand"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_ContextMenuPreviewHand");
			this.ultraToolbarsManager1.Tools["ContextMenuPreviewZoom"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_ContextMenuPreviewZoom");
			this.ultraToolbarsManager1.Tools["Zoom Increments"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Zoom_Increments");
			this.ultraToolbarsManager1.Tools["Reduce Page Thumbnails"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Reduce_Page_Thumbnails");
			this.ultraToolbarsManager1.Tools["Enlarge Page Thumbnails"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Enlarge_Page_Thumbnails");
			this.ultraToolbarsManager1.Tools["Show Page Numbers"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_Show_Page_Numbers");
			this.ultraToolbarsManager1.Tools["ClosePreview"].SharedProps.Caption = SR.GetString("PrintPreview_Tool_ClosePreview");

			// ToolTips
			this.ultraToolbarsManager1.Tools["Zoom"].SharedProps.ToolTipText = SR.GetString("PrintPreview_ToolTip_Zoom");
			this.ultraToolbarsManager1.Tools["ClosePreview"].SharedProps.ToolTipText = SR.GetString("PrintPreview_ToolTip_ClosePreview");

			// Toolbars
			this.ultraToolbarsManager1.Toolbars["Standard"].Text = SR.GetString("CustomizeImg_ToolBar_Standard");
			this.ultraToolbarsManager1.Toolbars["MenuBar"].Text = SR.GetString("CustomizeImg_ToolBar_MenuBar");
			this.ultraToolbarsManager1.Toolbars["View"].Text = SR.GetString("CustomizeImg_ToolBar_View");

			// Tool Categories
			this.ultraToolbarsManager1.Tools["Thumbnails"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Hand Tool"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Tools");
			this.ultraToolbarsManager1.Tools["Snapshot Tool"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Tools");
			this.ultraToolbarsManager1.Tools["Dynamic Zoom Tool"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Tools");
			this.ultraToolbarsManager1.Tools["Zoom Out Tool"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Tools");
			this.ultraToolbarsManager1.Tools["Zoom In Tool"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Tools");
			this.ultraToolbarsManager1.Tools["Print"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_File");
			this.ultraToolbarsManager1.Tools["Zoom"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["File"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Menus");
			this.ultraToolbarsManager1.Tools["View"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Menus");
			this.ultraToolbarsManager1.Tools["Tools"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Menus");
			this.ultraToolbarsManager1.Tools["Exit"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_File");
			this.ultraToolbarsManager1.Tools["Zoom In"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Zoom Out"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Previous View"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Next View"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Current Page"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["First Page"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Next Page"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Previous Page"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Last Page"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Page Width"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Zoom_Mode");
			this.ultraToolbarsManager1.Tools["Margin Width"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Zoom_Mode");
			this.ultraToolbarsManager1.Tools["Whole Page"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Zoom_Mode");
			this.ultraToolbarsManager1.Tools["Go To"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Menus");
			this.ultraToolbarsManager1.Tools["Page Layout"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Page Setup"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_File");
			this.ultraToolbarsManager1.Tools["Continuous"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["ContextMenuThumbnail"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Context_Menus");
			this.ultraToolbarsManager1.Tools["ContextMenuPreviewHand"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Context_Menus");
			this.ultraToolbarsManager1.Tools["ContextMenuPreviewZoom"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_Context_Menus");
			this.ultraToolbarsManager1.Tools["Zoom Increments"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Reduce Page Thumbnails"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Enlarge Page Thumbnails"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");
			this.ultraToolbarsManager1.Tools["Show Page Numbers"].SharedProps.Category = SR.GetString("PrintPreview_ToolCategory_View");

			// MD 11/15/06
			// Dialog Accessibility
			this.ultraToolbarsManager1.Tools[ "Thumbnails" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Thumbnails_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Hand Tool" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Hand_Tool_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Snapshot Tool" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Snapshot_Tool_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Dynamic Zoom Tool" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Dynamic_Zoom_Tool_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Zoom Out Tool" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Zoom_Out_Tool_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Zoom In Tool" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Zoom_In_Tool_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Print" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Print_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Zoom" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Zoom_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "File" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_File_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "View" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_View_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Tools" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Tools_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Exit" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Exit_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Zoom In" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Zoom_In_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Zoom Out" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Zoom_Out_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Previous View" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Previous_View_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Next View" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Next_View_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Current Page" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Current_Page_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "First Page" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_First_Page_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Next Page" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Next_Page_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Previous Page" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Previous_Page_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Last Page" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Last_Page_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Page Width" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Page_Width_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Margin Width" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Margin_Width_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Whole Page" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Whole_Page_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Go To" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Go_To_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Page Layout" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Page_Layout_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Page Setup" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Page_Setup_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Continuous" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Continuous_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Reduce Page Thumbnails" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Reduce_Page_Thumbnails_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Enlarge Page Thumbnails" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Enlarge_Page_Thumbnails_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "Show Page Numbers" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_Show_Page_Numbers_AccessibleDescription" );
			this.ultraToolbarsManager1.Tools[ "ClosePreview" ].SharedProps.AccessibleDescription = SR.GetString( "PrintPreview_Tool_ClosePreview_AccessibleDescription" );


			// MD 6/1/06 - BR13194
			this.InitializeZoomCombo();
		}
		#endregion //InitializeDialogStrings

		// MD 6/1/06 - BR13194
		// Moved from constructor
		#region InitializeZoomCombo
		private void InitializeZoomCombo()
		{
			ComboBoxTool cbo = this.ultraToolbarsManager1.Tools[ "Zoom" ] as ComboBoxTool;
			ListTool list = this.ultraToolbarsManager1.Tools[ "Zoom Increments" ] as ListTool;

			// MD 6/1/06 - BR13194
			// Clear the combo box and list tool
			cbo.ValueList.ValueListItems.Clear();
			list.ListToolItems.Clear();

			double[] zoomList = new double[] { .0833, .12, .25, .3333, .50, .6667, .75, 1d, 1.25, 1.5, 2d, 3d, 4d, 6d, 8d, 12d, 16d };

			Array.Sort( zoomList );
			Array.Reverse( zoomList );

			for ( int i = 0; i < zoomList.Length; i++ )
			{
				string formattedZoom = this.FormatPercent( zoomList[ i ] );
				ZoomWrapper zoomItem = new ZoomWrapper( zoomList[ i ] );

				cbo.ValueList.ValueListItems.Add( zoomItem, formattedZoom );
				ListToolItem listToolItem = list.ListToolItems.Add( formattedZoom, formattedZoom, false );
				listToolItem.Tag = zoomItem;
			}

			cbo.ValueList.ValueListItems.Add( new ZoomWrapper( ZoomMode.PageWidth ), SR.GetString( "PrintPreview_ZoomListItem_PageWidth" ) ); //"Page Width" );
			cbo.ValueList.ValueListItems.Add( new ZoomWrapper( ZoomMode.MarginsWidth ), SR.GetString( "PrintPreview_ZoomListItem_MarginWidth" ) ); // "Margin Width" );
			cbo.ValueList.ValueListItems.Add( new ZoomWrapper( ZoomMode.WholePage ), SR.GetString( "PrintPreview_ZoomListItem_WholePage" ) ); // "Whole Page" );

			this.UpdateZoomComboAndList();
		}
		#endregion

		// MD 6/1/06 - BR13194
		// Moved from the ultraPrintPreviewControl1_CurrentZoomChanged event handler
		#region UpdateZoomComboAndList
		private void UpdateZoomComboAndList()
		{
			// we need to update the combo so its text shows
			// the current zoom
			ComboBoxTool cbo = this.ultraToolbarsManager1.Tools[ "Zoom" ] as ComboBoxTool;

			// prevent the toolvalue changed from firing 
			// so we don't try to set the zoom
			bool wasValueChangeEnabled = this.ultraToolbarsManager1.EventManager.IsEnabled( ToolbarEventIds.ToolValueChanged );
			this.ultraToolbarsManager1.EventManager.SetEnabled( ToolbarEventIds.ToolValueChanged, false );

			string formattedZoom = this.FormatPercent( this.ultraPrintPreviewControl1.CurrentZoom );

			// initialize the text
			cbo.Text = formattedZoom;

			// re-enable the event
			this.ultraToolbarsManager1.EventManager.SetEnabled( ToolbarEventIds.ToolValueChanged, wasValueChangeEnabled );

			// update the enabled state of the zoom buttons
			this.ultraToolbarsManager1.Tools[ "Zoom Out" ].SharedProps.Enabled = this.ultraPrintPreviewControl1.CanZoomOut;
			this.ultraToolbarsManager1.Tools[ "Zoom In" ].SharedProps.Enabled = this.ultraPrintPreviewControl1.CanZoomIn;

			// prevent the toolclick changed from firing 
			// so we don't try to set the zoom
			bool wasToolClickEnabled = this.ultraToolbarsManager1.EventManager.IsEnabled( ToolbarEventIds.ToolClick );
			this.ultraToolbarsManager1.EventManager.SetEnabled( ToolbarEventIds.ToolClick, false );

			// update the checked state of the zoom increments tool
			ListTool list = this.ultraToolbarsManager1.Tools[ "Zoom Increments" ] as ListTool;

			int newIndex = list.ListToolItems.IndexOf( formattedZoom );

			if ( newIndex != list.SelectedItemIndex )
				list.SelectedItemIndex = newIndex;

			// re-enable the event
			this.ultraToolbarsManager1.EventManager.SetEnabled( ToolbarEventIds.ToolClick, wasToolClickEnabled );
		}
		#endregion

		#region IsNewPreviewRequired
		private static bool IsNewPreviewRequired(PageSettings oldSettings, PageSettings newSettings)
		{
			if (oldSettings.Bounds != newSettings.Bounds)
				return true;

			if (oldSettings.Color != newSettings.Color)
				return true;

			if (oldSettings.Landscape != newSettings.Landscape)
				return true;

			if (!Object.Equals(oldSettings.Margins, newSettings.Margins))
				return true;

			if (!IsSameObject(oldSettings.PaperSize, newSettings.PaperSize))
				return true;

			if (!IsSameObject(oldSettings.PaperSource, newSettings.PaperSource))
				return true;

			if (!IsSameObject(oldSettings.PrinterResolution, newSettings.PrinterResolution))
				return true;

			return false;
		}
		#endregion //IsNewPreviewRequired

		#region UpdateUsingZoomIncrementsList
		private void UpdateUsingZoomIncrementsList(ListToolItem item)
		{
			// update the checked state of the zoom increments tool
			ListTool list = this.ultraToolbarsManager1.Tools["Zoom Increments"] as ListTool;

			ZoomWrapper zoomItem = (list.SelectedItem == null ? (item == null ? null : item.Tag) : list.SelectedItem.Tag) as ZoomWrapper;

			if (zoomItem != null)
				zoomItem.UpdateZoom(this.ultraPrintPreviewControl1.Settings);

			// prevent the toolclick changed from firing 
			// so we don't try to set the zoom
			bool wasToolClickEnabled = this.ultraToolbarsManager1.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);

			int newIndex = item == null ? -1 : item.Index;

			if (newIndex != list.SelectedItemIndex)
				list.SelectedItemIndex = newIndex;

			// re-enable the event
			this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.ToolClick, wasToolClickEnabled);
		}
		#endregion //UpdateUsingZoomIncrementsList

		// AS 1/18/05
		// Work around the .net bug where by mouse enter/leave/hover don't fire
		// if a dialog has been shown once already.
		//
		#region ResetTrackMouseEventState
		private void ResetTrackMouseEventState()
		{
			try
			{
                // MRS 8/28/07 - Fixed warning
				//System.Security.Permissions.ReflectionPermission perm = new System.Security.Permissions.ReflectionPermission(System.Security.Permissions.ReflectionPermissionFlag.TypeInformation | System.Security.Permissions.ReflectionPermissionFlag.MemberAccess);
                System.Security.Permissions.ReflectionPermission perm = new System.Security.Permissions.ReflectionPermission(System.Security.Permissions.ReflectionPermissionFlag.MemberAccess);

				perm.Assert();

				System.Reflection.MethodInfo resetMethod = typeof(Control).GetMethod("UnhookMouseEvent", 
					System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

				System.Reflection.FieldInfo trackMouseField = typeof(Control).GetField("trackMouseEvent", 
					System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

				// recursively do it for any child control
				this.ResetTrackMouseEventStateHelper(this, resetMethod, trackMouseField);
			}
			catch (Exception ex)
			{
				Debug.Assert(ex is System.Security.SecurityException, "Unexpected exception occurred while clearing the track mouse information.");

				this.canResetTrackMouseEvent = false;
			}
		}

		private void ResetTrackMouseEventStateHelper(Control control, System.Reflection.MethodInfo resetMethod, System.Reflection.FieldInfo trackMouseField)
		{
			resetMethod.Invoke(control, System.Reflection.BindingFlags.InvokeMethod |
				System.Reflection.BindingFlags.Instance | 
				System.Reflection.BindingFlags.NonPublic, null, null, null);

			trackMouseField.SetValue(control, null, 
				System.Reflection.BindingFlags.Instance | 
				System.Reflection.BindingFlags.NonPublic,
				null, null);

			if (!control.HasChildren)
				return;

			foreach(Control child in control.Controls)
				this.ResetTrackMouseEventStateHelper(child, resetMethod, trackMouseField);
		}
		#endregion //ResetTrackMouseEventState

		// AS 2/24/05 BR02258
		#region UpdateDocumentRelatedTools
		private void UpdateDocumentRelatedTools()
		{
			bool hasDocument = this.ultraPrintPreviewControl1.Document != null;

			// AS 8/25/05 BR05917
			// Disable these options if the printer is invalid.
			//
			//this.ultraToolbarsManager1.Tools["Print"].SharedProps.Enabled = hasDocument;
			//this.ultraToolbarsManager1.Tools["Page Setup"].SharedProps.Enabled = hasDocument;
			bool isValid = hasDocument && this.ultraPrintPreviewControl1.Document.PrinterSettings.IsValid;

			this.ultraToolbarsManager1.Tools["Print"].SharedProps.Enabled = hasDocument && isValid;
			this.ultraToolbarsManager1.Tools["Page Setup"].SharedProps.Enabled = hasDocument && isValid;
		}
		#endregion //UpdateDocumentRelatedTools

		#endregion //Methods

		#region Events

		#region Event objects

		private static readonly object			EventPageSetupDialogDisplaying = new object();
		private static readonly object			EventPrinting = new object();
		private static readonly object			EventPrinted = new object();

		#endregion //Event objects

		#region OnXXX methods

		#region OnPageSetupDialogDisplaying
        /// <summary>
        /// Raises the <see cref="PageSetupDialogDisplaying"/> event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		/// <seealso cref="UltraPrintPreviewDialog.PageSetupDialogDisplaying"/>
		/// <seealso cref="PageSetupDialogDisplayingEventArgs"/>
		/// <seealso cref="PageSetupDialogDisplayingEventHandler"/>
		protected virtual void OnPageSetupDialogDisplaying(PageSetupDialogDisplayingEventArgs e)
		{
			PageSetupDialogDisplayingEventHandler eDelegate = (PageSetupDialogDisplayingEventHandler)Events[UltraPrintPreviewDialog.EventPageSetupDialogDisplaying];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnPageSetupDialogDisplaying

		#region OnPrinting
        /// <summary>
        /// Raises the <see cref="Printing"/> event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        /// <seealso cref="UltraPrintPreviewDialog.Printing"/>
		/// <seealso cref="PrintingEventArgs"/>
		/// <seealso cref="PrintingEventHandler"/>
		protected virtual void OnPrinting(PrintingEventArgs e)
		{
			PrintingEventHandler eDelegate = (PrintingEventHandler)Events[UltraPrintPreviewDialog.EventPrinting];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnPrinting

		#region OnPrinted
		/// <summary>
        /// Raises the <see cref="Printed"/> event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>		
		/// <seealso cref="UltraPrintPreviewDialog.Printed"/>
		/// <seealso cref="UltraPrintPreviewDialog.Printing"/>
		/// <seealso cref="UltraPrintPreviewDialog.OnPrinting"/>
		protected virtual void OnPrinted(EventArgs e)
		{
			EventHandler eDelegate = (EventHandler)Events[UltraPrintPreviewDialog.EventPrinted];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnPrinted

		#endregion //OnXXX methods

		#region Event declarations

		#region PageSetupDialogDisplaying

		/// <summary>
		/// Occurs before the page setup dialog is displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PageSetupDialogDisplaying</b> event is raised when the 
		/// end-user has clicked the <b>Page Setup</b> button and allows you to alter the 
		/// settings of the <see cref="PageSetupDialogDisplayingEventArgs.Dialog"/> before 
		/// it is displayed or even cancel the event and display your own custom page 
		/// setup dialog.</p>
		/// </remarks>
		/// <seealso cref="UltraPrintPreviewDialog.OnPageSetupDialogDisplaying"/>
		/// <seealso cref="PageSetupDialogDisplayingEventArgs"/>
		/// <seealso cref="PageSetupDialogDisplayingEventHandler"/>
		[LocalizedDescription("LD_UltraPrintPreviewDialog_E_PageSetupDialogDisplaying")]
		public event PageSetupDialogDisplayingEventHandler PageSetupDialogDisplaying
		{
			add { this.Events.AddHandler( UltraPrintPreviewDialog.EventPageSetupDialogDisplaying, value ); }
			remove { this.Events.RemoveHandler( UltraPrintPreviewDialog.EventPageSetupDialogDisplaying, value ); }
		}
		#endregion //PageSetupDialogDisplaying

		#region Printing

		/// <summary>
		/// Occurs in response to a click on the 'Print' tool before the print operation begins.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Printing</b> event is a cancellable event that may be used to 
		/// cancel the print operation or just control whether the status dialog should be 
		/// displayed during the print operation (using the <see cref="PrintingEventArgs.DisplayPrintStatus"/>).</p>
		/// </remarks>
		/// <seealso cref="UltraPrintPreviewDialog.Printed"/>
		/// <seealso cref="UltraPrintPreviewDialog.OnPrinting"/>
		/// <seealso cref="PrintingEventArgs"/>
		/// <seealso cref="PrintingEventHandler"/>
		[LocalizedDescription("LD_UltraPrintPreviewDialog_E_Printing")]
		public event PrintingEventHandler Printing
		{
			add { this.Events.AddHandler( UltraPrintPreviewDialog.EventPrinting, value ); }
			remove { this.Events.RemoveHandler( UltraPrintPreviewDialog.EventPrinting, value ); }
		}

		#endregion //Printing

		#region Printed

		/// <summary>
		/// Occurs in response to a click on the 'Print' tool after the print operation ends.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Printed</b> event is raised in response to the end-user clicking 
		/// the <b>Print</b> toolbar button after the print operation has completed.</p>
		/// </remarks>
		/// <seealso cref="UltraPrintPreviewDialog.Printing"/>
		/// <seealso cref="UltraPrintPreviewDialog.OnPrinted"/>
		[LocalizedDescription("LD_UltraPrintPreviewDialog_E_Printed")]
		public event EventHandler Printed
		{
			add { this.Events.AddHandler( UltraPrintPreviewDialog.EventPrinted, value ); }
			remove { this.Events.RemoveHandler( UltraPrintPreviewDialog.EventPrinted, value ); }
		}

		#endregion //Printed

		#endregion //Event declarations

		#endregion //Events

		#region ZoomWrapper
		private class ZoomWrapper
		{
			#region Member Variables

			private double			zoom;
			private ZoomMode		zoomMode;
			private bool			useZoomMode;

			#endregion //Member Variables

			#region Constructor
			internal ZoomWrapper(double zoom)
			{
				this.zoom = zoom;
				this.useZoomMode = false;
			}

			internal ZoomWrapper(ZoomMode zoomMode)
			{
				this.zoom = 1d;
				this.zoomMode = zoomMode;
				this.useZoomMode = true;
			}
			#endregion //Constructor

			// MD 6/1/06 - BR13194
			// Added so the cached value in the zoom combo would be equal
			// to one of the new values created when the combo was repopulated
			//
			#region Equals
			public override bool Equals( object obj )
			{
				ZoomWrapper wrapper = obj as ZoomWrapper;

				if ( wrapper != null )
				{
					return wrapper.zoom == this.zoom &&
						wrapper.zoomMode == this.zoomMode &&
						wrapper.useZoomMode == this.useZoomMode;
				}

				return false;
			}
			#endregion

			// MD 6/1/06 - BR13194
			// Added to prevent a compiler warning
			//
			#region GetHashCode
			public override int GetHashCode()
			{
				return base.GetHashCode();
			}
			#endregion

			#region UpdateZoom
			internal void UpdateZoom(PreviewSettings settings)
			{
				if (this.useZoomMode)
					settings.ZoomMode = this.zoomMode;
				else
				{
					settings.Zoom = this.zoom;

					if (settings.ZoomMode != ZoomMode.Standard &&
						settings.ZoomMode != ZoomMode.AutoFlow)
						settings.ZoomMode = ZoomMode.Standard;
				}
			}
			#endregion //UpdateZoom
		}
		#endregion //ZoomWrapper
	}

	#region UltraPrintPreviewDialogDesigner
	/// <summary>
	/// Designer for managing an <see cref="UltraPrintPreviewDialog"/>
	/// </summary>
    // MRS 01/03/07 - fxCop
    
    
    public partial class UltraPrintPreviewDialogDesigner : UltraComponentDesigner
	{
		#region Initialize
		/// <summary>
		/// Initializes the designer with the specified component.
		/// </summary>
		/// <param name="component">Component the designer is working with.</param>
		public override void Initialize(System.ComponentModel.IComponent component)
		{
			UltraPrintPreviewDialog dlg = component as UltraPrintPreviewDialog;

			// AS 4/8/05 BR03289
			// By default, the start position for a form is WindowsDefaultLocation
			// so it ends up getting calculated by the system. This is not desirable
			// at design time since the location ends up getting used by the 
			// component designer to position it within the component tray.
			// Since the StartPosition is not available at design time and is
			// not serializable, its safe to initialize it here to manual - 
			// which means it will use the specified location.
			//
			if (dlg != null)
				dlg.StartPosition = FormStartPosition.Manual;

			base.Initialize(component);
		}
		#endregion //Initialize
	}
	#endregion //UltraPrintPreviewDialogDesigner
}
