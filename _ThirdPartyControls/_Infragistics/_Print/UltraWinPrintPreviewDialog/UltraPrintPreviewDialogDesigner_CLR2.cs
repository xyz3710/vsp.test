#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
using System;
using System.ComponentModel.Design;
using System.ComponentModel;
using Infragistics.Win.Design;
using Infragistics.Shared;

namespace Infragistics.Win.Printing
{
    // JAS v5.3 SmartTags 7/11/05
    public partial class UltraPrintPreviewDialogDesigner : UltraComponentDesigner
    {
        #region Data

        private DesignerActionListCollection actionLists;

        #endregion // Data

        #region ActionLists

        /// <summary>
        /// Gets the <see cref="DesignerActionListCollection"/> for this designer's control/component.
        /// </summary>
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if( this.actionLists == null )
                {
                    this.actionLists = new DesignerActionListCollection();
                    this.actionLists.Add( new UltraPrintPreviewDialogActionList( this.Component as UltraPrintPreviewDialog ) );
                }
                return this.actionLists;
            }
        }

        #endregion // ActionLists

        #region UltraPrintPreviewDialogActionList [nested class]

        /// <summary>
        /// Provides DesignerActionItems for the SmartTag associated with the UltraPrintPreviewDialog.
        /// </summary>
        // MRS 01/03/07 - fxCop
        
        
        public class UltraPrintPreviewDialogActionList : UltraActionListBase<UltraPrintPreviewDialog>
        {
            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="product">The UltraPrintPreviewDialog</param>
            public UltraPrintPreviewDialogActionList( UltraPrintPreviewDialog product ) : base( product ) { }

            #endregion // Constructor

            #region CreateActionItems

            /// <summary>
            /// Overrides CreateActionItems.
            /// </summary>
            /// <param name="actionItems">The list to populate.</param>
            protected override void CreateActionItems( DesignerActionItemCollection actionItems )
            {
                #region Document

                // MBS 6/29/06 BR13853
                // Use new helper method
                //actionItems.Add( new DesignerActionPropertyItem(
                //    "Document",
                //    SR.GetString( "UltraPrintPreviewDialogActionList_P_Document_DisplayName" ),
                //    this.GetCategory( "Document" ),
                //    this.GetDescription( "Document" ) )
                //);
                this.AddActionPropertyItem(
                    actionItems,
                    "Document",
                    SR.GetString("UltraPrintPreviewDialogActionList_P_Document_DisplayName"),
                    "Document"
                );

                #endregion // Document

                #region Style

                // MBS 6/29/06 BR13853
                //actionItems.Add( new DesignerActionPropertyItem(
                //    "Style",
                //    SR.GetString( "UltraPrintPreviewDialogActionList_P_Style_DisplayName" ),
                //    this.GetCategory( "Style" ),
                //    this.GetDescription( "Style" ) )
                //);
                this.AddActionPropertyItem(
                    actionItems,
                    "Style",
                    SR.GetString("UltraPrintPreviewDialogActionList_P_Style_DisplayName"),
                    "Style"
                );

                #endregion // Style
            }

            #endregion // CreateActionItems

            #region Proxies

                #region Document

            /// <summary>
            /// Used by SmartTag Panel.
            /// </summary>
            public System.Drawing.Printing.PrintDocument Document
            {
                get { return this.GetReferenceTypeValue<System.Drawing.Printing.PrintDocument>( "Document" ); }
                set { this.SetValue( "Document", value ); }
            }

                #endregion // Document

                #region Style

            /// <summary>
            /// Used by SmartTag Panel.
            /// </summary>
            public Infragistics.Win.UltraWinToolbars.ToolbarStyle Style
            {
                get { return this.GetValueTypeValue<Infragistics.Win.UltraWinToolbars.ToolbarStyle>( "Style" ); }
                set { this.SetValue( "Style", value ); }
            }

                #endregion // Style

            #endregion // Proxies
        }

        #endregion // UltraPrintPreviewDialogActionList [nested class]
    }
}
