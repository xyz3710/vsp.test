﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.Win.Mes
{
	interface ISupportPrint
	{
        System.Drawing.Printing.PrintDocument PrintDocument { get; }
	}
}
