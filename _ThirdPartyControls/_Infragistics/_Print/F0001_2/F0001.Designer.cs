﻿using Infragistics.Win.UltraWinToolbars;
using System.Windows.Forms;
using System.Resources;
using Infragistics.Win;
using System.ComponentModel;
using Infragistics.Win.Printing;
using System.Drawing;
using System;
using Infragistics.Win.UltraWinStatusBar;

namespace iDASiT.Win.Mes
{
	partial class F0001
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드
		///// <summary>
		///// 디자이너 지원에 필요한 메서드입니다.
		///// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		///// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.UltraWinToolbars.OptionSet optionSet1 = new Infragistics.Win.UltraWinToolbars.OptionSet("Tools");
			Infragistics.Win.UltraWinToolbars.OptionSet optionSet2 = new Infragistics.Win.UltraWinToolbars.OptionSet("ZoomMode");
			Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar1 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("Standard");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Page Setup");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool2 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Hand Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool3 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Snapshot Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool4 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Dynamic Zoom Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool5 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom Out Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool6 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom In Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool7 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool8 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool9 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool10 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.UltraWinToolbars.ComboBoxTool tool11 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("Zoom");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool12 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool13 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Continuous", "");
			Infragistics.Win.UltraWinToolbars.PopupControlContainerTool tool14 = new Infragistics.Win.UltraWinToolbars.PopupControlContainerTool("Page Layout");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool15 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ClosePreview");
			Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar2 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("MenuBar");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool16 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("File");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool17 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("View");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool18 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Tools");
			Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar3 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("View");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool19 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool20 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.UltraWinToolbars.TextBoxTool tool21 = new Infragistics.Win.UltraWinToolbars.TextBoxTool("Current Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool22 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool23 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool24 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool25 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool26 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Thumbnails", "");
			Infragistics.Win.Appearance appearance = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool27 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Hand Tool", "Tools");
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool28 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Snapshot Tool", "Tools");
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool29 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Dynamic Zoom Tool", "Tools");
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool30 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom Out Tool", "Tools");
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool31 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom In Tool", "Tools");
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool32 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ComboBoxTool tool33 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("Zoom");
			Infragistics.Win.ValueList valueList1 = new Infragistics.Win.ValueList(0);
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool34 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("File");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool35 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool36 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Page Setup");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool37 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exit");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool38 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("View");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool39 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Thumbnails", "");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool40 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool41 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.UltraWinToolbars.ComboBoxTool tool42 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("Zoom");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool43 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool44 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool45 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool46 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Go To");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool47 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool48 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Hand Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool49 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Snapshot Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool50 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Dynamic Zoom Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool51 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom In Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool52 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom Out Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool53 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exit");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool54 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool55 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool56 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool57 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.TextBoxTool tool58 = new Infragistics.Win.UltraWinToolbars.TextBoxTool("Current Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool59 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool60 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool61 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool62 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool63 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool64 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool65 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool66 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Go To");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool67 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool68 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool69 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool70 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool71 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool72 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.UltraWinToolbars.PopupControlContainerTool tool73 = new Infragistics.Win.UltraWinToolbars.PopupControlContainerTool("Page Layout");
			Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool74 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Page Setup");
			Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool75 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Continuous", "");
			Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool76 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("ContextMenuThumbnail");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool77 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Reduce Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool78 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Enlarge Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool79 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Show Page Numbers", "");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool80 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("ContextMenuPreviewHand");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool81 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool82 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool83 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool84 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool85 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool86 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool87 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool88 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("ContextMenuPreviewZoom");
			Infragistics.Win.UltraWinToolbars.ListTool tool89 = new Infragistics.Win.UltraWinToolbars.ListTool("Zoom Increments");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool90 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool91 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool92 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool93 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool94 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.UltraWinToolbars.ListTool tool95 = new Infragistics.Win.UltraWinToolbars.ListTool("Zoom Increments");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool96 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Reduce Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool97 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Enlarge Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool98 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Show Page Numbers", "");
			Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool99 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ClosePreview");
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel2 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(F0001));
			this.ultraPrintPreviewControl = new Infragistics.Win.Printing.UltraPrintPreviewControl();
			this.ultraPrintPreviewThumbnail1 = new Infragistics.Win.Printing.UltraPrintPreviewThumbnail();
			this.xToolbarsManagerPreview = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
			this.pageSetupDialog = new System.Windows.Forms.PageSetupDialog();
			this.printDialog = new System.Windows.Forms.PrintDialog();
			this.ultraStatusBar1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
			this.pnlContainerBody_Fill_Panel_1 = new System.Windows.Forms.Panel();
			this._pnlContainerBody_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this._pnlContainerBody_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this._pnlContainerBody_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.ultraGridPrintDocument = new Infragistics.Win.UltraWinGrid.UltraGridPrintDocument(this.components);
			this.pnlContainerBody.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewThumbnail1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xToolbarsManagerPreview)).BeginInit();
			this.pnlContainerBody_Fill_Panel_1.SuspendLayout();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlContainerBody
			// 
			this.pnlContainerBody.BackColor = System.Drawing.Color.White;
			this.pnlContainerBody.Controls.Add(this.splitContainer1);
			this.pnlContainerBody.Controls.Add(this._pnlContainerBody_Toolbars_Dock_Area_Left);
			this.pnlContainerBody.Controls.Add(this._pnlContainerBody_Toolbars_Dock_Area_Right);
			this.pnlContainerBody.Controls.Add(this._pnlContainerBody_Toolbars_Dock_Area_Top);
			this.pnlContainerBody.Controls.Add(this._pnlContainerBody_Toolbars_Dock_Area_Bottom);
			this.pnlContainerBody.Size = new System.Drawing.Size(899, 545);
			// 
			// ultraPrintPreviewControl
			// 
			this.ultraPrintPreviewControl.BackColorInternal = System.Drawing.Color.White;
			this.ultraPrintPreviewControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ultraPrintPreviewControl.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.ultraPrintPreviewControl.Location = new System.Drawing.Point(0, 0);
			this.ultraPrintPreviewControl.Name = "ultraPrintPreviewControl";
			this.ultraPrintPreviewControl.Size = new System.Drawing.Size(711, 487);
			this.ultraPrintPreviewControl.TabIndex = 0;
			this.ultraPrintPreviewControl.CurrentPageChanged += new System.EventHandler(this.ultraPrintPreviewControl1_CurrentPageChanged);
			this.ultraPrintPreviewControl.PropertyChanged += new Infragistics.Win.PropertyChangedEventHandler(this.ultraPrintPreviewControl1_PropertyChanged);
			this.ultraPrintPreviewControl.CurrentZoomChanged += new System.EventHandler(this.ultraPrintPreviewControl1_CurrentZoomChanged);
			this.ultraPrintPreviewControl.ViewHistoryChanged += new System.EventHandler(this.ultraPrintPreviewControl1_ViewHistoryChanged);
			this.ultraPrintPreviewControl.PreviewGenerated += new System.EventHandler(this.ultraPrintPreviewControl1_PreviewGenerated);
			// 
			// ultraPrintPreviewThumbnail1
			// 
			this.ultraPrintPreviewThumbnail1.BackColorInternal = System.Drawing.Color.White;
			this.xToolbarsManagerPreview.SetContextMenuUltra(this.ultraPrintPreviewThumbnail1, "ContextMenuThumbnail");
			this.ultraPrintPreviewThumbnail1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ultraPrintPreviewThumbnail1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.ultraPrintPreviewThumbnail1.Location = new System.Drawing.Point(0, 0);
			this.ultraPrintPreviewThumbnail1.Name = "ultraPrintPreviewThumbnail1";
			this.ultraPrintPreviewThumbnail1.PreviewControl = this.ultraPrintPreviewControl;
			this.ultraPrintPreviewThumbnail1.Size = new System.Drawing.Size(176, 487);
			this.ultraPrintPreviewThumbnail1.TabIndex = 1;
			// 
			// xToolbarsManagerPreview
			// 
			this.xToolbarsManagerPreview.DesignerFlags = 1;
			this.xToolbarsManagerPreview.DockWithinContainer = this.pnlContainerBody;
			this.xToolbarsManagerPreview.ImageSizeSmall = new System.Drawing.Size(20, 20);
			optionSet1.AllowAllUp = false;
			this.xToolbarsManagerPreview.OptionSets.Add(optionSet1);
			this.xToolbarsManagerPreview.OptionSets.Add(optionSet2);
			this.xToolbarsManagerPreview.ShowFullMenusDelay = 500;
			ultraToolbar1.DockedColumn = 0;
			ultraToolbar1.DockedRow = 0;
			tool.InstanceProps.IsFirstInGroup = true;
			tool2.Checked = true;
			tool2.InstanceProps.IsFirstInGroup = true;
			tool7.InstanceProps.IsFirstInGroup = true;
			tool9.Checked = true;
			tool10.InstanceProps.IsFirstInGroup = true;
			tool13.Checked = true;
			tool14.InstanceProps.IsFirstInGroup = true;
			ultraToolbar1.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool,
            buttonTool1,
            tool2,
            tool3,
            tool4,
            tool5,
            tool6,
            tool7,
            tool8,
            tool9,
            tool10,
            tool11,
            tool12,
            tool13,
            tool14,
            tool15});
			ultraToolbar1.Text = "Standard";
			ultraToolbar2.DockedColumn = 0;
			ultraToolbar2.DockedRow = 1;
			ultraToolbar2.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool16,
            tool17,
            tool18});
			ultraToolbar2.ShowInToolbarList = false;
			ultraToolbar2.Text = "MenuBar";
			ultraToolbar3.DockedColumn = 1;
			ultraToolbar3.DockedRow = 0;
			ultraToolbar3.FloatingSize = new System.Drawing.Size(366, 26);
			tool24.InstanceProps.IsFirstInGroup = true;
			ultraToolbar3.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool19,
            tool20,
            tool21,
            tool22,
            tool23,
            tool24,
            tool25});
			ultraToolbar3.Text = "View";
			this.xToolbarsManagerPreview.Toolbars.AddRange(new Infragistics.Win.UltraWinToolbars.UltraToolbar[] {
            ultraToolbar1,
            ultraToolbar2,
            ultraToolbar3});
			tool26.Checked = true;
			appearance.Image = global::iDASiT.Win.Mes.Properties.Resources.ThumbNailS;
			tool26.SharedProps.AppearancesSmall.Appearance = appearance;
			tool26.SharedProps.Caption = "축소판 그림";
			tool26.SharedProps.Category = "View";
			tool27.Checked = true;
			tool27.OptionSetKey = "Tools";
			appearance2.Image = global::iDASiT.Win.Mes.Properties.Resources.HandToolL;
			tool27.SharedProps.AppearancesLarge.Appearance = appearance2;
			appearance3.Image = global::iDASiT.Win.Mes.Properties.Resources.HandToolS;
			tool27.SharedProps.AppearancesSmall.Appearance = appearance3;
			tool27.SharedProps.Caption = "손 도구(&H)";
			tool27.SharedProps.Category = "Tools";
			tool27.SharedProps.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
			tool28.OptionSetKey = "Tools";
			appearance4.Image = global::iDASiT.Win.Mes.Properties.Resources.SnapshotToolL;
			tool28.SharedProps.AppearancesLarge.Appearance = appearance4;
			appearance5.Image = global::iDASiT.Win.Mes.Properties.Resources.SnapshotToolS;
			tool28.SharedProps.AppearancesSmall.Appearance = appearance5;
			tool28.SharedProps.Caption = "스넵샷 도구";
			tool28.SharedProps.Category = "Tools";
			tool29.OptionSetKey = "Tools";
			appearance6.Image = global::iDASiT.Win.Mes.Properties.Resources.DynamicZoomToolL;
			tool29.SharedProps.AppearancesLarge.Appearance = appearance6;
			appearance7.Image = global::iDASiT.Win.Mes.Properties.Resources.DynamicZoomToolS;
			tool29.SharedProps.AppearancesSmall.Appearance = appearance7;
			tool29.SharedProps.Caption = "동적 확대/축소 도구";
			tool29.SharedProps.Category = "Tools";
			tool30.OptionSetKey = "Tools";
			appearance8.Image = global::iDASiT.Win.Mes.Properties.Resources.ZoomOutToolL;
			tool30.SharedProps.AppearancesLarge.Appearance = appearance8;
			appearance9.Image = global::iDASiT.Win.Mes.Properties.Resources.ZoomOutToolS;
			tool30.SharedProps.AppearancesSmall.Appearance = appearance9;
			tool30.SharedProps.Caption = "확대 도구";
			tool30.SharedProps.Category = "Tools";
			tool31.OptionSetKey = "Tools";
			appearance10.Image = global::iDASiT.Win.Mes.Properties.Resources.ZoomInToolL;
			tool31.SharedProps.AppearancesLarge.Appearance = appearance10;
			appearance11.Image = global::iDASiT.Win.Mes.Properties.Resources.ZoomInToolS;
			tool31.SharedProps.AppearancesSmall.Appearance = appearance11;
			tool31.SharedProps.Caption = "축소 도구";
			tool31.SharedProps.Category = "Tools";
			appearance12.Image = global::iDASiT.Win.Mes.Properties.Resources.PrintS;
			tool32.SharedProps.AppearancesSmall.Appearance = appearance12;
			tool32.SharedProps.Caption = "프린트(&P)";
			tool32.SharedProps.Category = "File";
			tool32.SharedProps.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
			tool33.DropDownStyle = Infragistics.Win.DropDownStyle.DropDown;
			tool33.SharedProps.Caption = "확대/축소:";
			tool33.SharedProps.Category = "View";
			tool33.SharedProps.ToolTipText = "Zoom";
			tool33.ValueList = valueList1;
			tool34.SharedProps.Caption = "프린트(&F)";
			tool34.SharedProps.Category = "Menus";
			tool37.InstanceProps.IsFirstInGroup = true;
			tool34.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool35,
            tool36,
            tool37});
			tool38.SharedProps.Caption = "보기(&V)";
			tool38.SharedProps.Category = "Menus";
			tool39.Checked = true;
			tool40.InstanceProps.IsFirstInGroup = true;
			tool43.InstanceProps.IsFirstInGroup = true;
			tool45.Checked = true;
			tool46.InstanceProps.IsFirstInGroup = true;
			tool38.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool39,
            tool40,
            tool41,
            tool42,
            tool43,
            tool44,
            tool45,
            tool46});
			tool47.SharedProps.Caption = "도구(&T)";
			tool47.SharedProps.Category = "Menus";
			tool48.Checked = true;
			tool50.InstanceProps.IsFirstInGroup = true;
			tool47.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool48,
            tool49,
            tool50,
            tool51,
            tool52});
			tool53.SharedProps.Caption = "닫기(&X)";
			tool53.SharedProps.Category = "File";
			appearance13.Image = global::iDASiT.Win.Mes.Properties.Resources.ZoomInL;
			tool54.SharedProps.AppearancesLarge.Appearance = appearance13;
			appearance14.Image = global::iDASiT.Win.Mes.Properties.Resources.ZoomInS;
			tool54.SharedProps.AppearancesSmall.Appearance = appearance14;
			tool54.SharedProps.Caption = "축소";
			tool54.SharedProps.Category = "View";
			appearance15.Image = global::iDASiT.Win.Mes.Properties.Resources.ZoomOutL;
			tool55.SharedProps.AppearancesLarge.Appearance = appearance15;
			appearance16.Image = global::iDASiT.Win.Mes.Properties.Resources.ZoomOutS;
			tool55.SharedProps.AppearancesSmall.Appearance = appearance16;
			tool55.SharedProps.Caption = "확대";
			tool55.SharedProps.Category = "View";
			appearance17.Image = global::iDASiT.Win.Mes.Properties.Resources.PreviousViewL;
			tool56.SharedProps.AppearancesLarge.Appearance = appearance17;
			appearance18.Image = global::iDASiT.Win.Mes.Properties.Resources.PreviousViewS;
			tool56.SharedProps.AppearancesSmall.Appearance = appearance18;
			tool56.SharedProps.Caption = "이전 보기(&P)";
			tool56.SharedProps.Category = "View";
			tool56.SharedProps.Enabled = false;
			appearance19.Image = global::iDASiT.Win.Mes.Properties.Resources.NextViewL;
			tool57.SharedProps.AppearancesLarge.Appearance = appearance19;
			appearance20.Image = global::iDASiT.Win.Mes.Properties.Resources.NextViewS;
			tool57.SharedProps.AppearancesSmall.Appearance = appearance20;
			tool57.SharedProps.Caption = "다음보기(&N)";
			tool57.SharedProps.Category = "View";
			tool57.SharedProps.Enabled = false;
			tool58.SharedProps.Caption = "현재 페이지";
			tool58.SharedProps.Category = "View";
			appearance21.Image = global::iDASiT.Win.Mes.Properties.Resources.FirstPageL;
			tool59.SharedProps.AppearancesLarge.Appearance = appearance21;
			appearance22.Image = global::iDASiT.Win.Mes.Properties.Resources.FirstPageS;
			tool59.SharedProps.AppearancesSmall.Appearance = appearance22;
			tool59.SharedProps.Caption = "첫 페이지";
			tool59.SharedProps.Category = "View";
			appearance23.Image = global::iDASiT.Win.Mes.Properties.Resources.NextPageL;
			tool60.SharedProps.AppearancesLarge.Appearance = appearance23;
			appearance24.Image = global::iDASiT.Win.Mes.Properties.Resources.NextPageS;
			tool60.SharedProps.AppearancesSmall.Appearance = appearance24;
			tool60.SharedProps.Caption = "다음 페이지";
			tool60.SharedProps.Category = "View";
			appearance25.Image = global::iDASiT.Win.Mes.Properties.Resources.PreviousPageL;
			tool61.SharedProps.AppearancesLarge.Appearance = appearance25;
			appearance26.Image = global::iDASiT.Win.Mes.Properties.Resources.PreviousPageS;
			tool61.SharedProps.AppearancesSmall.Appearance = appearance26;
			tool61.SharedProps.Caption = "이전 페이지";
			tool61.SharedProps.Category = "View";
			appearance27.Image = global::iDASiT.Win.Mes.Properties.Resources.LastPageL;
			tool62.SharedProps.AppearancesLarge.Appearance = appearance27;
			appearance28.Image = global::iDASiT.Win.Mes.Properties.Resources.LastPageS;
			tool62.SharedProps.AppearancesSmall.Appearance = appearance28;
			tool62.SharedProps.Caption = "마지막 페이지";
			tool62.SharedProps.Category = "View";
			tool63.OptionSetKey = "ZoomMode";
			appearance29.Image = global::iDASiT.Win.Mes.Properties.Resources.PageWidthL;
			tool63.SharedProps.AppearancesLarge.Appearance = appearance29;
			appearance30.Image = global::iDASiT.Win.Mes.Properties.Resources.PageWidthS;
			tool63.SharedProps.AppearancesSmall.Appearance = appearance30;
			tool63.SharedProps.Caption = "페이지 너비";
			tool63.SharedProps.Category = "Zoom Mode";
			tool64.OptionSetKey = "ZoomMode";
			appearance31.Image = global::iDASiT.Win.Mes.Properties.Resources.MarginWidthL;
			tool64.SharedProps.AppearancesLarge.Appearance = appearance31;
			appearance32.Image = global::iDASiT.Win.Mes.Properties.Resources.MarginWidthS;
			tool64.SharedProps.AppearancesSmall.Appearance = appearance32;
			tool64.SharedProps.Caption = "텍스트 너비";
			tool64.SharedProps.Category = "Zoom Mode";
			tool65.Checked = true;
			tool65.OptionSetKey = "ZoomMode";
			appearance33.Image = global::iDASiT.Win.Mes.Properties.Resources.WholePageL;
			tool65.SharedProps.AppearancesLarge.Appearance = appearance33;
			appearance34.Image = global::iDASiT.Win.Mes.Properties.Resources.WholePageS;
			tool65.SharedProps.AppearancesSmall.Appearance = appearance34;
			tool65.SharedProps.Caption = "전체 페이지";
			tool65.SharedProps.Category = "Zoom Mode";
			tool66.SharedProps.Caption = "이동";
			tool66.SharedProps.Category = "Menus";
			tool71.InstanceProps.IsFirstInGroup = true;
			tool66.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool67,
            tool68,
            tool69,
            tool70,
            tool71,
            tool72});
			tool73.DropDownArrowStyle = Infragistics.Win.UltraWinToolbars.DropDownArrowStyle.Standard;
			appearance35.Image = global::iDASiT.Win.Mes.Properties.Resources.PageLayoutL;
			tool73.SharedProps.AppearancesLarge.Appearance = appearance35;
			appearance36.Image = global::iDASiT.Win.Mes.Properties.Resources.PageLayoutS;
			tool73.SharedProps.AppearancesSmall.Appearance = appearance36;
			tool73.SharedProps.Caption = "Page Layout";
			tool73.SharedProps.Category = "View";
			appearance48.Image = global::iDASiT.Win.Mes.Properties.Resources.PageSetup;
			tool74.SharedProps.AppearancesSmall.Appearance = appearance48;
			tool74.SharedProps.Caption = "페이지 설정(&U)";
			tool74.SharedProps.Category = "File";
			tool74.SharedProps.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.DefaultForToolType;
			tool75.Checked = true;
			appearance37.Image = global::iDASiT.Win.Mes.Properties.Resources.ContinuousS;
			tool75.SharedProps.AppearancesSmall.Appearance = appearance37;
			tool75.SharedProps.Caption = "이어보기";
			tool75.SharedProps.Category = "View";
			tool76.SharedProps.Caption = "ContextMenuThumbnail";
			tool76.SharedProps.Category = "Context Menus";
			tool79.Checked = true;
			tool79.InstanceProps.IsFirstInGroup = true;
			tool76.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool77,
            tool78,
            tool79});
			tool80.SharedProps.Caption = "ContextMenuPreviewHand";
			tool80.SharedProps.Category = "Context Menus";
			tool85.InstanceProps.IsFirstInGroup = true;
			tool87.InstanceProps.IsFirstInGroup = true;
			tool80.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool81,
            tool82,
            tool83,
            tool84,
            tool85,
            tool86,
            tool87});
			tool88.SharedProps.Caption = "ContextMenuPreviewZoom";
			tool88.SharedProps.Category = "Context Menus";
			tool90.InstanceProps.IsFirstInGroup = true;
			tool92.Checked = true;
			tool93.InstanceProps.IsFirstInGroup = true;
			tool88.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool89,
            tool90,
            tool91,
            tool92,
            tool93,
            tool94});
			tool95.SharedProps.Caption = "Zoom Increments";
			tool95.SharedProps.Category = "View";
			tool96.SharedProps.Caption = "Reduce Page Thumbnails";
			tool96.SharedProps.Category = "View";
			tool97.SharedProps.Caption = "Enlarge Page Thumbnails";
			tool97.SharedProps.Category = "View";
			tool98.Checked = true;
			appearance38.Image = global::iDASiT.Win.Mes.Properties.Resources.ShowThumbnailPageNumbers;
			tool98.SharedProps.AppearancesSmall.Appearance = appearance38;
			tool98.SharedProps.Caption = "Show Thumbnail Page Numbers";
			tool98.SharedProps.Category = "View";
			tool99.SharedProps.Caption = "닫기(&C)";
			tool99.SharedProps.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.TextOnlyAlways;
			tool99.SharedProps.ShowInCustomizer = false;
			tool99.SharedProps.ToolTipText = "Close Preview";
			this.xToolbarsManagerPreview.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool26,
            tool27,
            tool28,
            tool29,
            tool30,
            tool31,
            tool32,
            tool33,
            tool34,
            tool38,
            tool47,
            tool53,
            tool54,
            tool55,
            tool56,
            tool57,
            tool58,
            tool59,
            tool60,
            tool61,
            tool62,
            tool63,
            tool64,
            tool65,
            tool66,
            tool73,
            tool74,
            tool75,
            tool76,
            tool80,
            tool88,
            tool95,
            tool96,
            tool97,
            tool98,
            tool99});
			this.xToolbarsManagerPreview.ToolKeyDown += new Infragistics.Win.UltraWinToolbars.ToolKeyEventHandler(this.ultraToolbarsManager2_ToolKeyDown);
			this.xToolbarsManagerPreview.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.ultraToolbarsManager2_ToolClick);
			this.xToolbarsManagerPreview.ToolValueChanged += new Infragistics.Win.UltraWinToolbars.ToolEventHandler(this.ultraToolbarsManager2_ToolValueChanged);
			// 
			// printDialog
			// 
			this.printDialog.AllowCurrentPage = true;
			this.printDialog.AllowSelection = true;
			this.printDialog.UseEXDialog = true;
			// 
			// ultraStatusBar1
			// 
			this.ultraStatusBar1.BackColor = System.Drawing.Color.White;
			this.ultraStatusBar1.Location = new System.Drawing.Point(0, 545);
			this.ultraStatusBar1.Name = "ultraStatusBar1";
			ultraStatusPanel1.Padding = new System.Drawing.Size(2, 0);
			ultraStatusPanel1.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Spring;
			ultraStatusPanel1.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.AutoStatusText;
			ultraStatusPanel2.Key = "PageNumber";
			ultraStatusPanel2.Padding = new System.Drawing.Size(2, 0);
			ultraStatusPanel2.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
			this.ultraStatusBar1.Panels.AddRange(new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel[] {
            ultraStatusPanel1,
            ultraStatusPanel2});
			this.ultraStatusBar1.Size = new System.Drawing.Size(899, 24);
			this.ultraStatusBar1.TabIndex = 7;
			this.ultraStatusBar1.Text = "ultraStatusBar1";
			this.ultraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2003;
			// 
			// pnlContainerBody_Fill_Panel_1
			// 
			this.pnlContainerBody_Fill_Panel_1.BackColor = System.Drawing.Color.White;
			this.pnlContainerBody_Fill_Panel_1.Controls.Add(this.ultraPrintPreviewControl);
			this.pnlContainerBody_Fill_Panel_1.Cursor = System.Windows.Forms.Cursors.Default;
			this.pnlContainerBody_Fill_Panel_1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlContainerBody_Fill_Panel_1.Location = new System.Drawing.Point(0, 0);
			this.pnlContainerBody_Fill_Panel_1.Name = "pnlContainerBody_Fill_Panel_1";
			this.pnlContainerBody_Fill_Panel_1.Size = new System.Drawing.Size(711, 487);
			this.pnlContainerBody_Fill_Panel_1.TabIndex = 0;
			// 
			// _pnlContainerBody_Toolbars_Dock_Area_Left
			// 
			this._pnlContainerBody_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._pnlContainerBody_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.White;
			this._pnlContainerBody_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
			this._pnlContainerBody_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
			this._pnlContainerBody_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(3, 53);
			this._pnlContainerBody_Toolbars_Dock_Area_Left.Name = "_pnlContainerBody_Toolbars_Dock_Area_Left";
			this._pnlContainerBody_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(0, 487);
			this._pnlContainerBody_Toolbars_Dock_Area_Left.ToolbarsManager = this.xToolbarsManagerPreview;
			// 
			// _pnlContainerBody_Toolbars_Dock_Area_Right
			// 
			this._pnlContainerBody_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._pnlContainerBody_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.White;
			this._pnlContainerBody_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
			this._pnlContainerBody_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
			this._pnlContainerBody_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(894, 53);
			this._pnlContainerBody_Toolbars_Dock_Area_Right.Name = "_pnlContainerBody_Toolbars_Dock_Area_Right";
			this._pnlContainerBody_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(0, 487);
			this._pnlContainerBody_Toolbars_Dock_Area_Right.ToolbarsManager = this.xToolbarsManagerPreview;
			// 
			// _pnlContainerBody_Toolbars_Dock_Area_Top
			// 
			this._pnlContainerBody_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._pnlContainerBody_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.White;
			this._pnlContainerBody_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
			this._pnlContainerBody_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
			this._pnlContainerBody_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(3, 3);
			this._pnlContainerBody_Toolbars_Dock_Area_Top.Name = "_pnlContainerBody_Toolbars_Dock_Area_Top";
			this._pnlContainerBody_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(891, 50);
			this._pnlContainerBody_Toolbars_Dock_Area_Top.ToolbarsManager = this.xToolbarsManagerPreview;
			// 
			// _pnlContainerBody_Toolbars_Dock_Area_Bottom
			// 
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.White;
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(3, 540);
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.Name = "_pnlContainerBody_Toolbars_Dock_Area_Bottom";
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(891, 0);
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.xToolbarsManagerPreview;
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.White;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(3, 53);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
			this.splitContainer1.Panel1.Controls.Add(this.ultraPrintPreviewThumbnail1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
			this.splitContainer1.Panel2.Controls.Add(this.pnlContainerBody_Fill_Panel_1);
			this.splitContainer1.Size = new System.Drawing.Size(891, 487);
			this.splitContainer1.SplitterDistance = 176;
			this.splitContainer1.TabIndex = 5;
			// 
			// F0001
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(899, 569);
			this.Controls.Add(this.ultraStatusBar1);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "F0001";
			this.Load += new System.EventHandler(this.PrintPreview_Load);
			this.Controls.SetChildIndex(this.ultraStatusBar1, 0);
			this.Controls.SetChildIndex(this.pnlContainerBody, 0);
			this.pnlContainerBody.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewThumbnail1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xToolbarsManagerPreview)).EndInit();
			this.pnlContainerBody_Fill_Panel_1.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

 


		#endregion

	//	private Infragistics.Win.Misc.UltraButton ultraButton1;
		private Infragistics.Win.Printing.UltraPrintPreviewControl ultraPrintPreviewControl;
	//	private Infragistics.Win.UltraWinGrid.UltraGridPrintDocument ultraGridPrintDocument1;
	//	private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
	//	private Infragistics.Win.Misc.UltraButton ultraButton2;
	//	private Infragistics.Win.Misc.UltraButton ultraButton3;
	//	private System.Windows.Forms.Panel pnlContainerBody_Fill_Panel;
	//	private System.Windows.Forms.Panel pnlContainerBody_Fill_Panel_Fill_Panel;
		private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager xToolbarsManagerPreview;
//		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
	//	private WinSchedulePrintDemo.PrinterSettingsControl printerSettingsControl1;
//		private System.Windows.Forms.Panel panel1;
//		private System.Windows.Forms.ImageList imageList1;
//		private System.Windows.Forms.ImageList imageList2;
		private Infragistics.Win.UltraWinStatusBar.UltraStatusBar ultraStatusBar1;
		private Infragistics.Win.Printing.UltraPrintPreviewThumbnail ultraPrintPreviewThumbnail1 = null;
//		private System.Windows.Forms.Panel panel3;
		private Panel pnlContainerBody_Fill_Panel_1;
		private UltraToolbarsDockArea _pnlContainerBody_Toolbars_Dock_Area_Left;
		private UltraToolbarsDockArea _pnlContainerBody_Toolbars_Dock_Area_Right;
		private UltraToolbarsDockArea _pnlContainerBody_Toolbars_Dock_Area_Top;
		private UltraToolbarsDockArea _pnlContainerBody_Toolbars_Dock_Area_Bottom;
		private SplitContainer splitContainer1;
		private Infragistics.Win.UltraWinGrid.UltraGridPrintDocument ultraGridPrintDocument;
	}
}
  