﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Win.Mes.PrintPreview
/*	Creator		:	iDASiT (YHJUN)
/*	Create		:	2008-01-20 오후 4:39:25
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	base.Popup("M02F0001", ultraGrid1);
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;
using AnyFactory.FX.Win;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.Printing;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinStatusBar;
using Infragistics.Win.UltraWinToolbars;
using iDASiT.Win.Mes.Properties;

namespace iDASiT.Win.Mes
{
	/// <summary>
	/// PrintPreview 클래스를 선언합니다.
	/// </summary>
	public partial class F0001 : BaseForm
	{
		#region Constants
		private const string FILE_KEY = "File";
		private const string PAGE_SETUP_KEY = "Page Setup";
		private const string PRINT_KEY = "Print";
		private const string EXIT_KEY = "Exit";
		private const string VIEW_KEY = "View";
		private const string THUMBNAILS_KEY = "Thumbnails";
		private const string ZOOMIN_KEY = "Zoom In";
		private const string ZOOMOUT_KEY = "Zoom Out";
		private const string ZOOM_KEY = "Zoom";
		private const string PAGEWIDTH_KEY = "Page Width";
		private const string MARGINWIDTH_KEY = "Margin Width";
		private const string WHOLEPAGE_KEY = "Whole Page";
		private const string GOTO_KEY = "Go To";
		private const string FIRSTPAGE_KEY = "First Page";
		private const string PREVIOUSPAGE_KEY = "Previous Page";
		private const string NEXTPAGE_KEY = "Next Page";
		private const string LASTPAGE_KEY = "Last Page";
		private const string PREVIOUSVIEW_KEY = "Previous View";
		private const string NEXTVIEW_KEY = "Next View";
		private const string TOOLS_KEY = "Tools";
		private const string HANDTOOL_KEY = "Hand Tool";
		private const string SNAPSHOTTOOL_KEY = "Snapshot Tool";
		private const string DYNAMICZOOMTOOL_KEY = "Dynamic Zoom Tool";
		private const string ZOOMINTOOL_KEY = "Zoom In Tool";
		private const string ZOOMOUTTOOL_KEY = "Zoom Out Tool";
		private const string CURRENTPAGE_KEY = "Current Page";
		private const string CONTINUOUS_KEY = "Continuous";
		private const string PAGELAYOUT_KEY = "Page Layout";
		private const string CLOSEPREVIEW_KEY = "ClosePreview";
		private const string CONTEXTMENUTHUMBNAIL_KEY = "ContextMenuThumbnail";
		private const string REDUCEPAGETHUMBNAILS_KEY = "Reduce Page Thumbnails";
		private const string ENLARGEPAGETHUMBNAILS_KEY = "Enlarge Page Thumbnails";
		private const string SHOWPAGENUMBERS_KEY = "Show Page Numbers";
		private const string CONTEXTMENUPREVIEWHAND_KEY = "ContextMenuPreviewHand";
		private const string CONTEXTMENUPREVIEWZOOM_KEY = "ContextMenuPreviewZoom";
		private const string ZOOMINCREMENTS_KEY = "Zoom Increments";
		private const string STANDARD_KEY = "Standard";
		private const string MENUBAR_KEY = "MenuBar";
		private const string PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY = "PrintPreview_ToolCategory_View";
		private const string PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY = "PrintPreview_ToolCategory_Tools";
		private const string PRINTPREVIEW_TOOLCATEGORY_FILE_KEY = "PrintPreview_ToolCategory_File";
		private const string PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY = "PrintPreview_ToolCategory_Menus";
		private const string PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY = "PrintPreview_ToolCategory_Zoom_Mode";
		private const string PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY = "PrintPreview_ToolCategory_Context_Menus";
    	#endregion
		private class ZoomWrapper
		{
			// Fields
			private bool useZoomMode;
			private double zoom;
			private ZoomMode zoomMode;

			// Methods
			internal ZoomWrapper(ZoomMode zoomMode)
			{
				this.zoom = 1.0;
				this.zoomMode = zoomMode;
				this.useZoomMode = true;
			}

			internal ZoomWrapper(double zoom)
			{
				this.zoom = zoom;
				this.useZoomMode = false;
			}

			public override bool Equals(object obj)
			{
				F0001.ZoomWrapper wrapper = obj as F0001.ZoomWrapper;
				if (wrapper == null)
				{
					return false;
				}
				return (((wrapper.zoom == this.zoom) && (wrapper.zoomMode == this.zoomMode)) && (wrapper.useZoomMode == this.useZoomMode));
			}

			public override int GetHashCode()
			{
				return base.GetHashCode();
			}

			internal void UpdateZoom(PreviewSettings settings)
			{
				if (this.useZoomMode)
				{
					settings.ZoomMode = this.zoomMode;
				}
				else
				{
					settings.Zoom = this.zoom;
					if ((settings.ZoomMode != ZoomMode.Standard) && (settings.ZoomMode != ZoomMode.AutoFlow))
					{
						settings.ZoomMode = ZoomMode.Standard;
					}
				}
			}

		}

		internal class NativeWindowMethods
		{
			// Fields
			private const int LOCALE_IMEASURE = 13;
			private const int LOCALE_USER_DEFAULT = 0x400;

			// Methods
			[DllImport("kernel32", CharSet=CharSet.Auto)]
			private static extern int GetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.LPWStr)] string lpLCData, int cchData);
			private static int GetLocaleInfoApi(int Locale, int LCType, string lpLCData, int cchData)
			{
				return GetLocaleInfo(Locale, LCType, lpLCData, cchData);
			}

			internal static bool IsMetric()
			{
				try
				{
					new SecurityPermission(SecurityPermissionFlag.UnmanagedCode).Assert();
					string lpLCData = new string(' ', 2);
					GetLocaleInfoApi(0x400, 13, lpLCData, lpLCData.Length);
					return (string.Compare(lpLCData, "0") == 0);
				}
				catch (Exception)
				{
					return RegionInfo.CurrentRegion.IsMetric;
				}
			}
		}
		

		// Events
		public event PageSetupDialogDisplayingEventHandler PageSetupDialogDisplaying
		{
			add
			{
				base.Events.AddHandler(EventPageSetupDialogDisplaying, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventPageSetupDialogDisplaying, value);
			}
		}
		public event EventHandler Printed
		{
			add
			{
				base.Events.AddHandler(EventPrinted, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventPrinted, value);
			}
		}
		public event PrintingEventHandler Printing
		{
			add
			{
				base.Events.AddHandler(EventPrinting, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventPrinting, value);
			}
		}
 
		#region Fields

		private bool canResetTrackMouseEvent = false;
		private IContainer container;
		private bool displayPrintStatus;
		private static readonly object EventPageSetupDialogDisplaying;
		private static readonly object EventPrinted;
		private static readonly object EventPrinting;
		private bool isFirstLoad = false;
		private int lastRowsSetting;
		private PageSetupDialog pageSetupDialog1;
		private PrintDialog printDialog;
		private PreviewRowColumnSelector rowColumnSelector;
		private bool statusBarVisible = false;
		private bool thumbnailAreaVisible;

		public object About
		{
			get
			{
				return null;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new IButtonControl AcceptButton
		{
			get
			{
				return base.AcceptButton;
			}
			set
			{
				base.AcceptButton = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new string AccessibleDescription
		{
			get
			{
				return base.AccessibleDescription;
			}
			set
			{
				base.AccessibleDescription = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new string AccessibleName
		{
			get
			{
				return base.AccessibleName;
			}
			set
			{
				base.AccessibleName = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new AccessibleRole AccessibleRole
		{
			get
			{
				return base.AccessibleRole;
			}
			set
			{
				base.AccessibleRole = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool AllowDrop
		{
			get
			{
				return base.AllowDrop;
			}
			set
			{
				base.AllowDrop = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new AnchorStyles Anchor
		{
			get
			{
				return base.Anchor;
			}
			set
			{
				base.Anchor = value;
			}
		}

		[DefaultValue(true)]
		public bool AutoGeneratePreview
		{
			get
			{
				return this.ultraPrintPreviewControl1.AutoGeneratePreview;
			}
			set
			{
				this.ultraPrintPreviewControl1.AutoGeneratePreview = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new Size AutoScaleBaseSize
		{
			get
			{
				return base.AutoScaleBaseSize;
			}
			set
			{
				base.AutoScaleBaseSize = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new bool AutoScroll
		{
			get
			{
				return base.AutoScroll;
			}
			set
			{
				base.AutoScroll = value;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new Size AutoScrollMargin
		{
			get
			{
				return base.AutoScrollMargin;
			}
			set
			{
				base.AutoScrollMargin = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new Size AutoScrollMinSize
		{
			get
			{
				return base.AutoScrollMinSize;
			}
			set
			{
				base.AutoScrollMinSize = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}
				
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Image BackgroundImage
		{
			get
			{
				return base.BackgroundImage;
			}
			set
			{
				base.BackgroundImage = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new ImageLayout BackgroundImageLayout
		{
			get
			{
				return base.BackgroundImageLayout;
			}
			set
			{
				base.BackgroundImageLayout=(value);
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new IButtonControl CancelButton
		{
			get
			{
				return base.CancelButton;
			}
			set
			{
				base.CancelButton = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new bool CausesValidation
		{
			get
			{
				return base.CausesValidation;
			}
			set
			{
				base.CausesValidation = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new Size ClientSize
		{
			get
			{
				return base.ClientSize;
			}
			set
			{
				base.ClientSize = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ContextMenu ContextMenu
		{
			get
			{
				return base.ContextMenu;
			}
			set
			{
				base.ContextMenu = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ControlBox
		{
			get
			{
				return base.ControlBox;
			}
			set
			{
				base.ControlBox = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Cursor Cursor
		{
			get
			{
				return base.Cursor;
			}
			set
			{
				base.Cursor = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ControlBindingsCollection DataBindings
		{
			get
			{
				return base.DataBindings;
			}
		}
		
		[DefaultValue(true)]
		public bool DisplayPreviewStatus
		{
			get
			{
				return this.ultraPrintPreviewControl1.DisplayPreviewStatus;
			}
			set
			{
				this.ultraPrintPreviewControl1.DisplayPreviewStatus = value;
			}
		}
		
		[DefaultValue(true)]
		public bool DisplayPrintStatus
		{
			get
			{
				return this.displayPrintStatus;
			}
			set
			{
				this.displayPrintStatus = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new DockStyle Dock
		{
			get
			{
				return base.Dock;
			}
			set
			{
				base.Dock = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new ScrollableControl.DockPaddingEdges DockPadding
		{
			get
			{
				return base.DockPadding;
			}
		}
		
		[DefaultValue((string)null)]
		public PrintDocument Document
		{
			get
			{
				return this.ultraPrintPreviewControl1.Document;
			}
			set
			{
				this.ultraPrintPreviewControl1.Document = value;
				this.pageSetupDialog1.Document = null;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new FormBorderStyle FormBorderStyle
		{
			get
			{
				return base.FormBorderStyle;
			}
			set
			{
				base.FormBorderStyle = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new bool HelpButton
		{
			get
			{
				return base.HelpButton;
			}
			set
			{
				base.HelpButton = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Icon Icon
		{
			get
			{
				return base.Icon;
			}
			set
			{
				base.Icon = value;
			}
		}
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new ImeMode ImeMode
		{
			get
			{
				return base.ImeMode;
			}
			set
			{
				base.ImeMode = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool IsMdiContainer
		{
			get
			{
				return base.IsMdiContainer;
			}
			set
			{
				base.IsMdiContainer = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool KeyPreview
		{
			get
			{
				return base.KeyPreview;
			}
			set
			{
				base.KeyPreview = value;
			}
		}
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Point Location
		{
			get
			{
				return base.Location;
			}
			set
			{
				base.Location = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool MaximizeBox
		{
			get
			{
				return base.MaximizeBox;
			}
			set
			{
				base.MaximizeBox = value;
			}
		}

		[DefaultValue(0)]
		public int MaximumPreviewPages
		{
			get
			{
				return this.ultraPrintPreviewControl1.MaximumPreviewPages;
			}
			set
			{
				this.ultraPrintPreviewControl1.MaximumPreviewPages = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size MaximumSize
		{
			get
			{
				return base.MaximumSize;
			}
			set
			{
				base.MaximumSize = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new MainMenu Menu
		{
			get
			{
				return base.Menu;
			}
			set
			{
				base.Menu = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool MinimizeBox
		{
			get
			{
				return base.MinimizeBox;
			}
			set
			{
				base.MinimizeBox = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size MinimumSize
		{
			get
			{
				return base.MinimumSize;
			}
			set
			{
				base.MinimumSize = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced)]
		public new double Opacity
		{
			get
			{
				return base.Opacity;
			}
			set
			{
				base.Opacity = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new Padding Padding
		{
			get
			{
				return base.Padding;
			}
			set
			{
				base.Padding = (value);
			}
		}
		
		[DefaultValue(1)]
		public PreviewMouseAction PreviewMouseAction
		{
			get
			{
				return this.ultraPrintPreviewControl1.MouseAction;
			}
			set
			{
				this.ultraPrintPreviewControl1.MouseAction = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PreviewSettings PreviewSettings
		{
			get
			{
				return this.ultraPrintPreviewControl1.Settings;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public UltraPrintPreviewControl PrintPreviewControl
		{
			get
			{
				return this.ultraPrintPreviewControl1;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraPrintPreviewThumbnail PrintPreviewThumbnail
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new RightToLeft RightToLeft
		{
			get
			{
				return base.RightToLeft;
			}
			set
			{
				base.RightToLeft = value;
			}
		}
				
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ShowInTaskbar
		{
			get
			{
				return base.ShowInTaskbar;
			}
			set
			{
				base.ShowInTaskbar = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Size Size
		{
			get
			{
				return base.Size;
			}
			set
			{
				base.Size = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new SizeGripStyle SizeGripStyle
		{
			get
			{
				return base.SizeGripStyle;
			}
			set
			{
				base.SizeGripStyle = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new FormStartPosition StartPosition
		{
			get
			{
				return base.StartPosition;
			}
			set
			{
				base.StartPosition = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraStatusBar StatusBar
		{
			get
			{
				return this.ultraStatusBar1;
			}
		}

		[DefaultValue(true)]
		public bool StatusBarVisible
		{
			get
			{
				return this.statusBarVisible;
			}
			set
			{
				this.statusBarVisible = value;
				this.ultraStatusBar1.Visible = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new bool TabStop
		{
			get
			{
				return base.TabStop;
			}
			set
			{
				base.TabStop = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new object Tag
		{
			get
			{
				return base.Tag;
			}
			set
			{
				base.Tag = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}
		
		[DefaultValue(true)]
		public bool ThumbnailAreaVisible
		{
			get
			{
				return this.thumbnailAreaVisible;
			}
			set
			{
				this.thumbnailAreaVisible = value;

				this.ultraPrintPreviewThumbnail1.Visible = value;
				if (value == true)
				{
					this.splitContainer1.SplitterDistance = 176;
				}
				else
				{
					this.splitContainer1.SplitterDistance = 0;
				}
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailCurrentPreviewPageAppearance
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.CurrentPreviewPageAppearance;
			}
			set
			{
				this.ultraPrintPreviewThumbnail1.CurrentPreviewPageAppearance = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailCurrentPreviewPageNumberAppearance
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.CurrentPreviewPageNumberAppearance;
			}
			set
			{
				this.ultraPrintPreviewThumbnail1.CurrentPreviewPageNumberAppearance = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PreviewSettings ThumbnailSettings
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.Settings;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailViewBoxAppearance
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.ViewBoxAppearance;
			}
			set
			{
				this.ultraPrintPreviewThumbnail1.ViewBoxAppearance = value;
			}
		}

		[DefaultValue(0)]
		public ViewBoxDisplayStyle ThumbnailViewBoxDisplayStyle
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.ViewBoxDisplayStyle;
			}
			set
			{
				this.ultraPrintPreviewThumbnail1.ViewBoxDisplayStyle = value;
			}
		}
		
		[DefaultValue(0)]
		public ViewBoxDragMode ThumbnailViewBoxDragMode
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.ViewBoxDragMode;
			}
			set
			{
				this.ultraPrintPreviewThumbnail1.ViewBoxDragMode = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool TopMost
		{
			get
			{
				return base.TopMost;
			}
			set
			{
				base.TopMost = value;
			}
		}
		
		[DefaultValue(false)]
		public bool UseAntiAlias
		{
			get
			{
				return this.ultraPrintPreviewControl1.UseAntiAlias;
			}
			set
			{
				this.ultraPrintPreviewControl1.UseAntiAlias = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool Visible
		{
			get
			{
				return base.Visible;
			}
			set
			{
				base.Visible = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new FormWindowState WindowState
		{
			get
			{
				return base.WindowState;
			}
			set
			{
				base.WindowState = value;
			}
		}
 
		#endregion

		#region Constructor
		/// <summary>
		/// PrintPreview의 인스턴스를 생성합니다.
		/// </summary>
		public F0001()
			: base()
		{
			this.thumbnailAreaVisible = true;
			this.displayPrintStatus = true;
			this.statusBarVisible = true;
			this.isFirstLoad = true;
			this.canResetTrackMouseEvent = true;
			
			//초기화
			this.InitializeComponent();
			InitializeForm();

			this.rowColumnSelector = new PreviewRowColumnSelector();
			this.rowColumnSelector.Location = new Point(-10000, -10000);
			base.Controls.Add(this.rowColumnSelector);
			this.rowColumnSelector.MouseUp += new MouseEventHandler(this.OnRowColumnSelectorMouseUp);

			this.InitializeDialogStrings();
			base.Name = "";
			this.ShowInTaskbar = false;
			this.InitializeZoomCombo();
			this.UpdateMouseActionTools();

			InitializeDialogStrings();
		}
	 
		public F0001(IContainer container)
			: this()
		{
			this.container = container;
			if (this.container != null)
			{
				this.container.Add(this);
			}
		}

		public F0001(UltraGrid xgridMain)
			: this()
		{
			if (xgridMain != null)
			{
				ultraGridPrintDocument1.Grid = xgridMain;
				this.Document = ultraGridPrintDocument1;
			}
		}

		static F0001()
		{
			EventPageSetupDialogDisplaying = new object();
			EventPrinting = new object();
			EventPrinted = new object();
		}

		#endregion

		#region InitializeForm
		private void InitializeForm()
		{
			// Form 초기화 method를 삽입합니다.


			base.ThemeStyle = ThemeStyle;
		}
		#endregion

		#region Properties

		#endregion

		#region Event Methods
		private void PrintPreview_Shown(object sender, EventArgs e)
		{

		}
		
		private void PrintPreview_Load(object sender, EventArgs e)
		{


		}

		private void ultraToolbarsManager2_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
		{
			switch (e.Tool.Key)
			{
				case HANDTOOL_KEY:
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.Hand;
					return;

				case SNAPSHOTTOOL_KEY:
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.Snapshot;
					return;

				case DYNAMICZOOMTOOL_KEY:
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.DynamicZoom;
					return;

				case ZOOMOUTTOOL_KEY:
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.ZoomOut;
					return;

				case ZOOMINTOOL_KEY:
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.ZoomIn;
					return;

				case ZOOMIN_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ZoomIn);
					return;

				case ZOOMOUT_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ZoomOut);
					return;

				case PAGEWIDTH_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.UseStandardZoomModeIfNecessary();
						return;
					}
					this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.PageWidth;
					return;

				case MARGINWIDTH_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.UseStandardZoomModeIfNecessary();
						return;
					}
					this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.MarginsWidth;
					return;

				case WHOLEPAGE_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.UseStandardZoomModeIfNecessary();
						return;
					}
					this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.WholePage;
					return;

				case ZOOMINCREMENTS_KEY:
					this.UpdateUsingZoomIncrementsList(e.ListToolItem);

					return;

				case THUMBNAILS_KEY:
					base.SuspendLayout();
					if (((StateButtonTool)e.Tool).Checked)
					{
						this.ultraPrintPreviewThumbnail1.SendToBack();
					}
					this.ThumbnailAreaVisible = ((StateButtonTool)e.Tool).Checked;
					base.ResumeLayout(true);
					return;

				case PRINT_KEY:
					if (this.ultraPrintPreviewControl1.Document != null)
					{
						// Allow the user to choose the page range he or she would
						// like to print.
						printDialog.AllowSomePages = true;
						// Show the help button.
						printDialog.ShowHelp = true;

						this.printDialog.Document = ultraPrintPreviewControl1.Document;

						DialogResult result = printDialog.ShowDialog(this);

						// If the result is OK then print the document.
						if (result==DialogResult.OK)
						{
							//docToPrint.Print();
							PrintingEventArgs args = new PrintingEventArgs(this.displayPrintStatus);
							this.OnPrinting(args);
							if (args.Cancel)
							{
								break;
							}
							this.ultraPrintPreviewControl1.Print(args.DisplayPrintStatus);
							this.OnPrinted(new EventArgs());
							return;
						}
					}
					return;

				case EXIT_KEY:
				case CLOSEPREVIEW_KEY:
					if (!base.Modal)
					{
						base.Close();
						return;
					}
					base.DialogResult = DialogResult.OK;
					return;

				case PAGE_SETUP_KEY:
					this.ShowPageSetupDialog();
					return;

				case PREVIOUSVIEW_KEY:
					if (!this.ultraPrintPreviewControl1.HasPreviousView)
					{
						break;
					}
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.PreviousView);
					return;

				case NEXTVIEW_KEY:
					if (!this.ultraPrintPreviewControl1.HasNextView)
					{
						break;
					}
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.NextView);
					return;

				case FIRSTPAGE_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToFirstPage);
					return;

				case LASTPAGE_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToLastPage);
					return;

				case PREVIOUSPAGE_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToPreviousPage);
					return;

				case NEXTPAGE_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToNextPage);
					return;

				case CONTINUOUS_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.ultraPrintPreviewControl1.Settings.Rows = Math.Max(1, this.lastRowsSetting);
						return;
					}
					this.ultraPrintPreviewControl1.Settings.Rows = 0;
					return;

				case REDUCEPAGETHUMBNAILS_KEY:
					this.ultraPrintPreviewThumbnail1.PerformAction(UltraPrintPreviewThumbnailAction.ZoomOut);
					return;

				case ENLARGEPAGETHUMBNAILS_KEY:
					this.ultraPrintPreviewThumbnail1.PerformAction(UltraPrintPreviewThumbnailAction.ZoomIn);
					return;

				case SHOWPAGENUMBERS_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.ultraPrintPreviewThumbnail1.Settings.PageNumberDisplayStyle = PageNumberDisplayStyle.None;
						break;
					}
					this.ultraPrintPreviewThumbnail1.Settings.ResetPageNumberDisplayStyle();
					return;

				default:
					return;
			}

		}

		private void ultraToolbarsManager2_ToolKeyDown(object sender, Infragistics.Win.UltraWinToolbars.ToolKeyEventArgs e)
		{
			if (e.Tool != null)
			{
				bool flag = false;
				string key = e.Tool.Key;
				if (key != null)
				{
					if (!(key == ZOOM_KEY))
					{
						if ((key == CURRENTPAGE_KEY) && (e.KeyData == Keys.Return))
						{
							TextBoxTool tool2 = e.Tool as TextBoxTool;
							double result = 1.0;
							try
							{
								flag = double.TryParse(tool2.Text, NumberStyles.Integer, null, out result);
							}
							catch (Exception)
							{
							}
							if ((flag && (result > 0.0)) && (result <= this.ultraPrintPreviewControl1.PageCount))
							{
								this.ultraPrintPreviewControl1.CurrentPage = (int)result;
								tool2.SelectionStart = 0;
								tool2.SelectionLength = tool2.Text.Length;
							}
						}
					}
					else if (e.KeyData == Keys.Return)
					{
						ComboBoxTool tool = e.Tool as ComboBoxTool;
						if (tool.SelectedIndex >= 0)
						{
							((ZoomWrapper)tool.Value).UpdateZoom(this.ultraPrintPreviewControl1.Settings);
						}
						else
						{
							double num = 0.0;
							string s = tool.Text.Replace("%", "");
							try
							{
								flag = double.TryParse(s, NumberStyles.Float, null, out num);
							}
							catch (Exception)
							{
							}
							if (flag)
							{
								if (num < 0.0)
								{
									num = -num;
								}
								new ZoomWrapper(num / 100.0).UpdateZoom(this.ultraPrintPreviewControl1.Settings);
							}
						}
					}
				}
			}

		}

		private void ultraToolbarsManager2_BeforeToolbarListDropdown(object sender, Infragistics.Win.UltraWinToolbars.BeforeToolbarListDropdownEventArgs e)
		{
			if (((e.Tool.Key == PAGELAYOUT_KEY) && (this.ultraPrintPreviewControl1 != null)) && (this.ultraPrintPreviewControl1.PageCount == 0))
			{
				e.Cancel = true;
			}

		}

		private void ultraToolbarsManager2_ToolValueChanged(object sender, Infragistics.Win.UltraWinToolbars.ToolEventArgs e)
		{
			string str;
			if (((str = e.Tool.Key) != null) && (str == ZOOM_KEY))
			{
				ComboBoxTool tool = e.Tool as ComboBoxTool;
				if ((tool.SelectedIndex >= 0) && object.Equals(tool.Value, tool.ValueList.ValueListItems[tool.SelectedIndex].DataValue))
				{
					((ZoomWrapper)tool.ValueList.ValueListItems[tool.SelectedIndex].DataValue).UpdateZoom(this.ultraPrintPreviewControl1.Settings);
				}
			}

		}

		private void ultraPrintPreviewControl1_CurrentPageChanged(object sender, EventArgs e)
		{
			this.UpdateCurrentPageInfo();

		}

		private void ultraPrintPreviewControl1_CurrentZoomChanged(object sender, EventArgs e)
		{
			this.UpdateZoomComboAndList();

		}

		private void ultraPrintPreviewControl1_PreviewGenerated(object sender, EventArgs e)
		{
			this.rowColumnSelector.Columns = Math.Max(1, Math.Min(this.ultraPrintPreviewControl1.PageCount, 8));
			this.rowColumnSelector.Rows = Math.Max(1, Math.Min(this.ultraPrintPreviewControl1.PageCount, 5));
			this.ultraToolbarsManager2.Tools[PAGELAYOUT_KEY].SharedProps.Enabled = this.ultraPrintPreviewControl1.PageCount > 0;
			this.UpdateMouseActionTools();
			this.UpdateCurrentPageInfo();

		}

		private void ultraPrintPreviewControl1_PropertyChanged(object sender, Infragistics.Win.PropertyChangedEventArgs e)
		{
			PropChangeInfo info = e.ChangeInfo.FindTrigger(null);
			if (info.PropId is UltraPrintPreviewControlPropertyIds)
			{
				UltraPrintPreviewControlPropertyIds propId = (UltraPrintPreviewControlPropertyIds)info.PropId;
				if (propId <= UltraPrintPreviewControlPropertyIds.ZoomMode)
				{
					if (propId != UltraPrintPreviewControlPropertyIds.Document)
					{
						if (propId == UltraPrintPreviewControlPropertyIds.ZoomMode)
						{
							this.UpdateZoomModeTools();
						}
						return;
					}
				}
				else
				{
					switch (propId)
					{
						case UltraPrintPreviewControlPropertyIds.Rows:
							this.UpdateContinuousTool();
							return;

						case UltraPrintPreviewControlPropertyIds.MouseAction:
							this.UpdateMouseActionTools();
							return;

						case UltraPrintPreviewControlPropertyIds.Settings:
							this.UpdateZoomModeTools();
							this.UpdateMouseActionTools();
							this.UpdateContinuousTool();
							return;
					}
					return;
				}
				this.UpdateDocumentRelatedTools();
			}

		}

		private void ultraPrintPreviewControl1_ViewHistoryChanged(object sender, EventArgs e)
		{
			this.UpdateViewHistoryTools();

		}
		#endregion
		
		#region User defined Methods

		private void ClosePopupContainingTool(ToolBase tool)
		{
			if (tool != null)
			{
				if (tool.OwnerIsMenu)
				{
					this.ClosePopupContainingTool(tool.OwningMenu);
				}
				else if (tool.OwnerIsToolbar && (tool.OwningToolbar.TearawayToolOwner != null))
				{
					this.ClosePopupContainingTool(tool.OwningToolbar.TearawayToolOwner);
				}
				else if (tool is PopupToolBase)
				{
					((PopupToolBase)tool).ClosePopup();
				}
			}
		}

		private string FormatPercent(double percent)
		{
			double num = percent * 100.0;
			if ((num % 1.0) == 0.0)
			{
				return percent.ToString("P0");
			}
			if ((num % 0.1) == 0.0)
			{
				return percent.ToString("P1");
			}
			return percent.ToString("P2");
		}

		private void InitializeDialogStrings()
		{
			this.Text = "PrintPreview_DialogCaption";

			ultraToolbarsManager2.Tools[FILE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.File;
			ultraToolbarsManager2.Tools[PRINT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Print;
			ultraToolbarsManager2.Tools[PAGE_SETUP_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PageSetup;
			ultraToolbarsManager2.Tools[EXIT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Exit;
			ultraToolbarsManager2.Tools[VIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.View;
			ultraToolbarsManager2.Tools[THUMBNAILS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Thumbnails;
			ultraToolbarsManager2.Tools[ZOOMIN_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomIn;
			ultraToolbarsManager2.Tools[ZOOMOUT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomOut;
			ultraToolbarsManager2.Tools[ZOOM_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Zoom;
			ultraToolbarsManager2.Tools[PAGEWIDTH_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PageWidth;
			ultraToolbarsManager2.Tools[MARGINWIDTH_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.MarginWidth;
			ultraToolbarsManager2.Tools[WHOLEPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.WholePage;
			ultraToolbarsManager2.Tools[GOTO_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.GoTo;
			ultraToolbarsManager2.Tools[FIRSTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.FirstPage;
			ultraToolbarsManager2.Tools[PREVIOUSPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PreviousPage;
			ultraToolbarsManager2.Tools[NEXTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.NextPage;
			ultraToolbarsManager2.Tools[LASTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.LastPage;
			ultraToolbarsManager2.Tools[PREVIOUSVIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PreviousView;
			ultraToolbarsManager2.Tools[NEXTVIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.NextView;
			ultraToolbarsManager2.Tools[TOOLS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Tools;
			ultraToolbarsManager2.Tools[HANDTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.HandTool;
			ultraToolbarsManager2.Tools[SNAPSHOTTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.SnapshotTool;
			ultraToolbarsManager2.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.DynamicZoomTool;
			ultraToolbarsManager2.Tools[ZOOMINTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomInTool;
			ultraToolbarsManager2.Tools[ZOOMOUTTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomOutTool;
			ultraToolbarsManager2.Tools[CURRENTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.CurrentPage;
			ultraToolbarsManager2.Tools[CONTINUOUS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Continuous;
			ultraToolbarsManager2.Tools[PAGELAYOUT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PageLayout;
			ultraToolbarsManager2.Tools[CLOSEPREVIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ClosePreview;
			ultraToolbarsManager2.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ContextMenuThumbnail;
			ultraToolbarsManager2.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ReducePageThumbnails;
			ultraToolbarsManager2.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.EnlargePageThumbnails;
			ultraToolbarsManager2.Tools[SHOWPAGENUMBERS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ShowPageNumbers;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ContextMenuPreviewHand;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ContextMenuPreviewZoom;
			ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomIncrements;

			ultraToolbarsManager2.Tools[FILE_KEY].SharedProps.Caption = Captions.File;
			ultraToolbarsManager2.Tools[PRINT_KEY].SharedProps.Caption = Captions.Print;
			ultraToolbarsManager2.Tools[PAGE_SETUP_KEY].SharedProps.Caption = Captions.PageSetup;
			ultraToolbarsManager2.Tools[EXIT_KEY].SharedProps.Caption = Captions.Exit;
			ultraToolbarsManager2.Tools[VIEW_KEY].SharedProps.Caption = Captions.View;
			ultraToolbarsManager2.Tools[THUMBNAILS_KEY].SharedProps.Caption = Captions.Thumbnails;
			ultraToolbarsManager2.Tools[ZOOMIN_KEY].SharedProps.Caption = Captions.ZoomIn;
			ultraToolbarsManager2.Tools[ZOOMOUT_KEY].SharedProps.Caption = Captions.ZoomOut;
			ultraToolbarsManager2.Tools[ZOOM_KEY].SharedProps.Caption = Captions.Zoom;
			ultraToolbarsManager2.Tools[PAGEWIDTH_KEY].SharedProps.Caption = Captions.PageWidth;
			ultraToolbarsManager2.Tools[MARGINWIDTH_KEY].SharedProps.Caption = Captions.MarginWidth;
			ultraToolbarsManager2.Tools[WHOLEPAGE_KEY].SharedProps.Caption = Captions.WholePage;
			ultraToolbarsManager2.Tools[GOTO_KEY].SharedProps.Caption = Captions.GoTo;
			ultraToolbarsManager2.Tools[FIRSTPAGE_KEY].SharedProps.Caption = Captions.FirstPage;
			ultraToolbarsManager2.Tools[PREVIOUSPAGE_KEY].SharedProps.Caption = Captions.PreviousPage;
			ultraToolbarsManager2.Tools[NEXTPAGE_KEY].SharedProps.Caption = Captions.NextPage;
			ultraToolbarsManager2.Tools[LASTPAGE_KEY].SharedProps.Caption = Captions.LastPage;
			ultraToolbarsManager2.Tools[PREVIOUSVIEW_KEY].SharedProps.Caption = Captions.PreviousView;
			ultraToolbarsManager2.Tools[NEXTVIEW_KEY].SharedProps.Caption = Captions.NextView;
			ultraToolbarsManager2.Tools[TOOLS_KEY].SharedProps.Caption = Captions.Tools;
			ultraToolbarsManager2.Tools[HANDTOOL_KEY].SharedProps.Caption = Captions.HandTool;
			ultraToolbarsManager2.Tools[SNAPSHOTTOOL_KEY].SharedProps.Caption = Captions.SnapshotTool;
			ultraToolbarsManager2.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.Caption = Captions.DynamicZoomTool;
			ultraToolbarsManager2.Tools[ZOOMINTOOL_KEY].SharedProps.Caption = Captions.ZoomInTool;
			ultraToolbarsManager2.Tools[ZOOMOUTTOOL_KEY].SharedProps.Caption = Captions.ZoomOutTool;
			ultraToolbarsManager2.Tools[CURRENTPAGE_KEY].SharedProps.Caption = Captions.CurrentPage;
			ultraToolbarsManager2.Tools[CONTINUOUS_KEY].SharedProps.Caption = Captions.Continuous;
			ultraToolbarsManager2.Tools[PAGELAYOUT_KEY].SharedProps.Caption = Captions.PageLayout;
			ultraToolbarsManager2.Tools[CLOSEPREVIEW_KEY].SharedProps.Caption = Captions.ClosePreview;
			ultraToolbarsManager2.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.Caption = Captions.ContextMenuThumbnail;
			ultraToolbarsManager2.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.Caption = Captions.ReducePageThumbnails;
			ultraToolbarsManager2.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.Caption = Captions.EnlargePageThumbnails;
			ultraToolbarsManager2.Tools[SHOWPAGENUMBERS_KEY].SharedProps.Caption = Captions.ShowPageNumbers;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.Caption = Captions.ContextMenuPreviewHand;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.Caption = Captions.ContextMenuPreviewZoom;
			ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY].SharedProps.Caption = Captions.ZoomIncrements;

			ultraToolbarsManager2.Tools[FILE_KEY].SharedProps.ToolTipText = ToolTips.File;
			ultraToolbarsManager2.Tools[PRINT_KEY].SharedProps.ToolTipText = ToolTips.Print;
			ultraToolbarsManager2.Tools[PAGE_SETUP_KEY].SharedProps.ToolTipText = ToolTips.PageSetup;
			ultraToolbarsManager2.Tools[EXIT_KEY].SharedProps.ToolTipText = ToolTips.Exit;
			ultraToolbarsManager2.Tools[VIEW_KEY].SharedProps.ToolTipText = ToolTips.View;
			ultraToolbarsManager2.Tools[THUMBNAILS_KEY].SharedProps.ToolTipText = ToolTips.Thumbnails;
			ultraToolbarsManager2.Tools[ZOOMIN_KEY].SharedProps.ToolTipText = ToolTips.ZoomIn;
			ultraToolbarsManager2.Tools[ZOOMOUT_KEY].SharedProps.ToolTipText = ToolTips.ZoomOut;
			ultraToolbarsManager2.Tools[ZOOM_KEY].SharedProps.ToolTipText = ToolTips.Zoom;
			ultraToolbarsManager2.Tools[PAGEWIDTH_KEY].SharedProps.ToolTipText = ToolTips.PageWidth;
			ultraToolbarsManager2.Tools[MARGINWIDTH_KEY].SharedProps.ToolTipText = ToolTips.MarginWidth;
			ultraToolbarsManager2.Tools[WHOLEPAGE_KEY].SharedProps.ToolTipText = ToolTips.WholePage;
			ultraToolbarsManager2.Tools[GOTO_KEY].SharedProps.ToolTipText = ToolTips.GoTo;
			ultraToolbarsManager2.Tools[FIRSTPAGE_KEY].SharedProps.ToolTipText = ToolTips.FirstPage;
			ultraToolbarsManager2.Tools[PREVIOUSPAGE_KEY].SharedProps.ToolTipText = ToolTips.PreviousPage;
			ultraToolbarsManager2.Tools[NEXTPAGE_KEY].SharedProps.ToolTipText = ToolTips.NextPage;
			ultraToolbarsManager2.Tools[LASTPAGE_KEY].SharedProps.ToolTipText = ToolTips.LastPage;
			ultraToolbarsManager2.Tools[PREVIOUSVIEW_KEY].SharedProps.ToolTipText = ToolTips.PreviousView;
			ultraToolbarsManager2.Tools[NEXTVIEW_KEY].SharedProps.ToolTipText = ToolTips.NextView;
			ultraToolbarsManager2.Tools[TOOLS_KEY].SharedProps.ToolTipText = ToolTips.Tools;
			ultraToolbarsManager2.Tools[HANDTOOL_KEY].SharedProps.ToolTipText = ToolTips.HandTool;
			ultraToolbarsManager2.Tools[SNAPSHOTTOOL_KEY].SharedProps.ToolTipText = ToolTips.SnapshotTool;
			ultraToolbarsManager2.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.ToolTipText = ToolTips.DynamicZoomTool;
			ultraToolbarsManager2.Tools[ZOOMINTOOL_KEY].SharedProps.ToolTipText = ToolTips.ZoomInTool;
			ultraToolbarsManager2.Tools[ZOOMOUTTOOL_KEY].SharedProps.ToolTipText = ToolTips.ZoomOutTool;
			ultraToolbarsManager2.Tools[CURRENTPAGE_KEY].SharedProps.ToolTipText = ToolTips.CurrentPage;
			ultraToolbarsManager2.Tools[CONTINUOUS_KEY].SharedProps.ToolTipText = ToolTips.Continuous;
			ultraToolbarsManager2.Tools[PAGELAYOUT_KEY].SharedProps.ToolTipText = ToolTips.PageLayout;
			ultraToolbarsManager2.Tools[CLOSEPREVIEW_KEY].SharedProps.ToolTipText = ToolTips.ClosePreview;
			ultraToolbarsManager2.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.ToolTipText = ToolTips.ContextMenuThumbnail;
			ultraToolbarsManager2.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.ToolTipText = ToolTips.ReducePageThumbnails;
			ultraToolbarsManager2.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.ToolTipText = ToolTips.EnlargePageThumbnails;
			ultraToolbarsManager2.Tools[SHOWPAGENUMBERS_KEY].SharedProps.ToolTipText = ToolTips.ShowPageNumbers;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.ToolTipText = ToolTips.ContextMenuPreviewHand;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.ToolTipText = ToolTips.ContextMenuPreviewZoom;
			ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY].SharedProps.ToolTipText = ToolTips.ZoomIncrements;

			this.ultraToolbarsManager2.Toolbars[STANDARD_KEY].Text = Captions.Standard;
			this.ultraToolbarsManager2.Toolbars[MENUBAR_KEY].Text = Captions.MenuBar;
			this.ultraToolbarsManager2.Toolbars[VIEW_KEY].Text = Captions.View;

			this.ultraToolbarsManager2.Tools[FILE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[PRINT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_FILE_KEY;
			this.ultraToolbarsManager2.Tools[PAGE_SETUP_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_FILE_KEY;
			this.ultraToolbarsManager2.Tools[EXIT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_FILE_KEY;
			this.ultraToolbarsManager2.Tools[VIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[THUMBNAILS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[ZOOMIN_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[ZOOMOUT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[ZOOM_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[PAGEWIDTH_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY;
			this.ultraToolbarsManager2.Tools[MARGINWIDTH_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY;
			this.ultraToolbarsManager2.Tools[WHOLEPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY;
			this.ultraToolbarsManager2.Tools[GOTO_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[FIRSTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[PREVIOUSPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;			
			this.ultraToolbarsManager2.Tools[NEXTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[LASTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[PREVIOUSVIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[NEXTVIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[TOOLS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[HANDTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			this.ultraToolbarsManager2.Tools[SNAPSHOTTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			this.ultraToolbarsManager2.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			this.ultraToolbarsManager2.Tools[ZOOMOUTTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			this.ultraToolbarsManager2.Tools[ZOOMINTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;			
			this.ultraToolbarsManager2.Tools[CURRENTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[CONTINUOUS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[PAGELAYOUT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[CLOSEPREVIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			this.ultraToolbarsManager2.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[SHOWPAGENUMBERS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			
			this.InitializeZoomCombo();
		}

		private void InitializeZoomCombo()
		{
			ComboBoxTool tool = this.ultraToolbarsManager2.Tools[ZOOM_KEY] as ComboBoxTool;
			//ListTool tool2 = this.ultraToolbarsManager2.Tools["Zoom Increments"] as ListTool;
			tool.ValueList.ValueListItems.Clear();
			//tool2.ListToolItems.Clear();
			double[] array = new double[] { 0.0833, 0.12, 0.25, 0.3333, 0.5, 0.6667, 0.75, 1.0, 1.25, 1.5, 2.0, 3.0, 4.0, 6.0, 8.0, 12.0, 16.0 };
			Array.Sort<double>(array);
			Array.Reverse(array);
			for (int i = 0; i < array.Length; i++)
			{
				string displayText = this.FormatPercent(array[i]);
				ZoomWrapper dataValue = new ZoomWrapper(array[i]);
				tool.ValueList.ValueListItems.Add(dataValue, displayText);
				//tool2.ListToolItems.Add(displayText, displayText, false).Tag = dataValue;
			}
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.PageWidth), "PrintPreview_ZoomListItem_PageWidth");
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.MarginsWidth), "PrintPreview_ZoomListItem_MarginWidth");
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.WholePage), "PrintPreview_ZoomListItem_WholePage");
			this.UpdateZoomComboAndList();
		}

		public void InvalidatePreview()
		{
			this.ultraPrintPreviewControl1.InvalidatePreview();
			this.ultraPrintPreviewControl1.GeneratePreview(false);
		}

		private static bool IsNewPreviewRequired(PageSettings oldSettings, PageSettings newSettings)
		{
			return ((oldSettings.Bounds != newSettings.Bounds) || ((oldSettings.Color != newSettings.Color) || ((oldSettings.Landscape != newSettings.Landscape) || (!object.Equals(oldSettings.Margins, newSettings.Margins) || (!IsSameObject(oldSettings.PaperSize, newSettings.PaperSize) || (!IsSameObject(oldSettings.PaperSource, newSettings.PaperSource) || !IsSameObject(oldSettings.PrinterResolution, newSettings.PrinterResolution)))))));
		}

		private static bool IsSameObject(object printObject1, object printObject2)
		{
			if (printObject1 == printObject2)
			{
				return true;
			}
			if ((printObject1 == null) || (printObject2 == null))
			{
				return false;
			}
			if (printObject1.GetType() != printObject2.GetType())
			{
				return false;
			}
			if (printObject1 is PaperSize)
			{
				PaperSize size = printObject1 as PaperSize;
				PaperSize size2 = printObject2 as PaperSize;
				return ((((size.Kind == size2.Kind) && (size.Height == size2.Height)) && (size.Width == size2.Width)) && (size.PaperName == size2.PaperName));
			}
			if (printObject1 is PaperSource)
			{
				PaperSource source = printObject1 as PaperSource;
				PaperSource source2 = printObject2 as PaperSource;
				return ((source.Kind == source2.Kind) && (source.SourceName == source2.SourceName));
			}
			if (!(printObject1 is PrinterResolution))
			{
				return false;
			}
			PrinterResolution resolution = printObject1 as PrinterResolution;
			PrinterResolution resolution2 = printObject2 as PrinterResolution;
			return (((resolution.Kind == resolution2.Kind) && (resolution.X == resolution2.X)) && (resolution.Y == resolution2.Y));
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			this.ultraPrintPreviewControl1.InvalidatePreview();
			base.OnClosing(e);
		}

		protected virtual void OnPageSetupDialogDisplaying(PageSetupDialogDisplayingEventArgs e)
		{
			PageSetupDialogDisplayingEventHandler handler = (PageSetupDialogDisplayingEventHandler)base.Events[EventPageSetupDialogDisplaying];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		protected virtual void OnPrinted(EventArgs e)
		{
			EventHandler handler = (EventHandler)base.Events[EventPrinted];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		protected virtual void OnPrinting(PrintingEventArgs e)
		{
			PrintingEventHandler handler = (PrintingEventHandler)base.Events[EventPrinting];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		private void OnRowColumnSelectorMouseUp(object sender, MouseEventArgs e)
		{
			Size selection = this.rowColumnSelector.Selection;
			if (!selection.IsEmpty)
			{
				this.ultraPrintPreviewControl1.Settings.Columns = selection.Width;
				if ((selection.Width == 1) && (selection.Height == 1))
				{
					this.ultraPrintPreviewControl1.Settings.Rows = 0;
					this.lastRowsSetting = 1;
				}
				else
				{
					this.ultraPrintPreviewControl1.Settings.Rows = selection.Height;
					this.lastRowsSetting = selection.Height;
				}
				((StateButtonTool)this.ultraToolbarsManager2.Tools[CONTINUOUS_KEY]).Checked = false;
				this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.WholePage;
				ToolBase toolThatContainsControl = this.ultraToolbarsManager2.GetToolThatContainsControl(this.rowColumnSelector);
				this.ClosePopupContainingTool(toolThatContainsControl);
				this.rowColumnSelector.ResetSelection();
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetPreviewSettings()
		{
			this.ultraPrintPreviewControl1.ResetSettings();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailCurrentPreviewPageAppearance()
		{
			this.ultraPrintPreviewThumbnail1.ResetCurrentPreviewPageAppearance();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailCurrentPreviewPageNumberAppearance()
		{
			this.ultraPrintPreviewThumbnail1.ResetCurrentPreviewPageNumberAppearance();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailSettings()
		{
			this.ultraPrintPreviewThumbnail1.ResetSettings();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailViewBoxAppearance()
		{
			this.ultraPrintPreviewThumbnail1.ResetViewBoxAppearance();
		}

		private void ResetTrackMouseEventState()
		{
			try
			{
				new ReflectionPermission(ReflectionPermissionFlag.MemberAccess).Assert();
				MethodInfo method = typeof(Control).GetMethod("UnhookMouseEvent", BindingFlags.NonPublic | BindingFlags.Instance);
				FieldInfo field = typeof(Control).GetField("trackMouseEvent", BindingFlags.NonPublic | BindingFlags.Instance);
				this.ResetTrackMouseEventStateHelper(this, method, field);
			}
			catch (Exception)
			{
				this.canResetTrackMouseEvent = false;
			}
		}

		private void ResetTrackMouseEventStateHelper(Control control, MethodInfo resetMethod, FieldInfo trackMouseField)
		{
			resetMethod.Invoke(control, BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance, null, null, null);
			trackMouseField.SetValue(control, null, BindingFlags.NonPublic | BindingFlags.Instance, null, null);
			if (control.HasChildren)
			{
				foreach (Control control2 in control.Controls)
				{
					this.ResetTrackMouseEventStateHelper(control2, resetMethod, trackMouseField);
				}
			}
		}

		protected bool ShouldSerializePreviewSettings()
		{
			return this.ultraPrintPreviewControl1.ShouldSerializeSettings();
		}

		protected bool ShouldSerializeThumbnailCurrentPreviewPageAppearance()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeCurrentPreviewPageAppearance();
		}

		protected bool ShouldSerializeThumbnailCurrentPreviewPageNumberAppearance()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeCurrentPreviewPageNumberAppearance();
		}

		protected bool ShouldSerializeThumbnailSettings()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeSettings();
		}

		protected bool ShouldSerializeThumbnailViewBoxAppearance()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeViewBoxAppearance();
		}

		private void ShowPageSetupDialog()
		{

			// Custom PrintSetupDialog
			//----------------------------------------------------------------
			if (this.ultraPrintPreviewControl1.Document != null)
			{
				UltraPageSetup setup = new UltraPageSetup((UltraGridPrintDocument)this.ultraPrintPreviewControl1.Document);
				DialogResult dr = setup.ShowDialog(this);
				setup.Dispose();

				if (dr == DialogResult.OK)
				{
					this.ultraPrintPreviewControl1.InvalidatePreview();
				}
			}

			// 윈도우 기본 PrintSetupDialog
			//----------------------------------------------------------------
			//if (this.ultraPrintPreviewControl1.Document != null)
			//{
			//    this.pageSetupDialog1.Document = this.ultraPrintPreviewControl1.Document;
			//    PageSettings oldSettings = this.pageSetupDialog1.PageSettings.Clone() as PageSettings;
			//    oldSettings.Landscape = oldSettings.Landscape;
			//    bool flag = false;
			//    if (Environment.Version.Major < 2)
			//    {
			//        flag = NativeWindowMethods.IsMetric();
			//    }
			//    else
			//    {
			//        try
			//        {
			//            if (NativeWindowMethods.IsMetric())
			//            {
			//                PropertyDescriptor descriptor = TypeDescriptor.GetProperties(this.pageSetupDialog1)["EnableMetric"];
			//                if ((descriptor != null) && (descriptor.PropertyType == typeof(bool)))
			//                {
			//                    flag = !((bool)descriptor.GetValue(this.pageSetupDialog1));
			//                }
			//            }
			//        }
			//        catch
			//        {
			//        }
			//    }
			//    if (flag)
			//    {
			//        this.pageSetupDialog1.PageSettings.Margins = PrinterUnitConvert.Convert(this.pageSetupDialog1.PageSettings.Margins, PrinterUnit.Display, PrinterUnit.TenthsOfAMillimeter);
			//    }
			//    PageSetupDialogDisplayingEventArgs e = new PageSetupDialogDisplayingEventArgs(this.pageSetupDialog1);
			//    this.OnPageSetupDialogDisplaying(e);
			//    if (!e.Cancel)
			//    {
			//        if (this.pageSetupDialog1.ShowDialog(this) == DialogResult.OK)
			//        {
			//            if (IsNewPreviewRequired(oldSettings, this.pageSetupDialog1.PageSettings))
			//            {
			//                this.InvalidatePreview();
			//            }
			//        }
			//        else if (flag)
			//        {
			//            this.pageSetupDialog1.PageSettings.Margins = PrinterUnitConvert.Convert(this.pageSetupDialog1.PageSettings.Margins, PrinterUnit.TenthsOfAMillimeter, PrinterUnit.Display);
			//        }
			//    }
			//}
		}

		private void UpdateContinuousTool()
		{
			bool enabled = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			((StateButtonTool)this.ultraToolbarsManager2.Tools[CONTINUOUS_KEY]).Checked = this.ultraPrintPreviewControl1.Settings.Rows == 0;
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UpdateCurrentPageInfo()
		{
			int currentPage = this.ultraPrintPreviewControl1.CurrentPage;
			int pageCount = this.ultraPrintPreviewControl1.PageCount;
			TextBoxTool tool = this.ultraToolbarsManager2.Tools[CURRENTPAGE_KEY] as TextBoxTool;
			if (tool != null)
			{
				if (pageCount == 0)
				{
					tool.Text = string.Empty;
				}
				else
				{
					tool.Text = currentPage.ToString();
				}
			}
			if (this.ultraStatusBar1.Panels.Exists("PageNumber"))
			{
				if (pageCount == 0)
				{
					this.ultraStatusBar1.Panels["PageNumber"].Text = string.Empty;
				}
				else
				{
					this.ultraStatusBar1.Panels["PageNumber"].Text = string.Format(" Page {0} OF {1}", currentPage, pageCount);
				}
			}
		}

		private void UpdateDocumentRelatedTools()
		{
			bool flag = this.ultraPrintPreviewControl1.Document != null;
			bool flag2 = flag && this.ultraPrintPreviewControl1.Document.PrinterSettings.IsValid;
			this.ultraToolbarsManager2.Tools[PRINT_KEY].SharedProps.Enabled = flag && flag2;
			this.ultraToolbarsManager2.Tools[PAGE_SETUP_KEY].SharedProps.Enabled = flag && flag2;
		}

		private void UpdateMouseActionTools()
		{
			string menuKey = null;
			string str2 = null;
			switch (this.ultraPrintPreviewControl1.MouseActionResolved)
			{
				case PreviewMouseAction.Hand:
					str2 = HANDTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWHAND_KEY;
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "StatusBar_Hand_Instructions");
					break;

				case PreviewMouseAction.Snapshot:
					str2 = SNAPSHOTTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWHAND_KEY;
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "StatusBar_SnapShot_Instructions");
					break;

				case PreviewMouseAction.DynamicZoom:
					str2 = DYNAMICZOOMTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWZOOM_KEY;
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "StatusBar_DynamicZoom_Instructions");
					break;

				case PreviewMouseAction.ZoomOut:
					str2 = ZOOMOUTTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWZOOM_KEY;
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "StatusBar_ZoomOut_Instructions");
					break;

				case PreviewMouseAction.ZoomIn:
					str2 = ZOOMINTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWZOOM_KEY;
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "StatusBar_ZoomIn_Instructions");
					break;

				default:
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "");
					break;
			}
			if (str2 != null)
			{
				bool enabled = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
				this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
				((StateButtonTool)this.ultraToolbarsManager2.Tools[str2]).Checked = true;
				this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
			}
			if (menuKey != null)
			{
				this.ultraToolbarsManager2.SetContextMenuUltra(this.ultraPrintPreviewControl1, menuKey);
			}
		}

		private void UpdateUsingZoomIncrementsList(Infragistics.Win.UltraWinToolbars.ListToolItem item)
		{
			ListTool tool = this.ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY] as ListTool;
			ZoomWrapper wrapper = ((tool.SelectedItem == null) ? ((ZoomWrapper)((item == null) ? null : ((ZoomWrapper)item.Tag))) : ((ZoomWrapper)tool.SelectedItem.Tag)) as ZoomWrapper;
			if (wrapper != null)
			{
				wrapper.UpdateZoom(this.ultraPrintPreviewControl1.Settings);
			}
			bool enabled = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			int num = (item == null) ? -1 : item.Index;
			if (num != tool.SelectedItemIndex)
			{
				tool.SelectedItemIndex = num;
			}
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UpdateViewHistoryTools()
		{
			this.ultraToolbarsManager2.Tools[PREVIOUSVIEW_KEY].SharedProps.Enabled = this.ultraPrintPreviewControl1.HasPreviousView;
			this.ultraToolbarsManager2.Tools[NEXTVIEW_KEY].SharedProps.Enabled = this.ultraPrintPreviewControl1.HasNextView;
		}

		private void UpdateZoomComboAndList()
		{
			ComboBoxTool tool = this.ultraToolbarsManager2.Tools[ZOOM_KEY] as ComboBoxTool;
			bool enabled = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolValueChanged);
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolValueChanged, false);
			string key = this.FormatPercent(this.ultraPrintPreviewControl1.CurrentZoom);
			tool.Text = key;
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolValueChanged, enabled);
			this.ultraToolbarsManager2.Tools[ZOOMOUT_KEY].SharedProps.Enabled = this.ultraPrintPreviewControl1.CanZoomOut;
			this.ultraToolbarsManager2.Tools[ZOOMIN_KEY].SharedProps.Enabled = this.ultraPrintPreviewControl1.CanZoomIn;
			bool flag2 = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			ListTool tool2 = this.ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY] as ListTool;
			int index = tool2.ListToolItems.IndexOf(key);
			if (index != tool2.SelectedItemIndex)
			{
				tool2.SelectedItemIndex = index;
			}
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, flag2);
		}

		private void UpdateZoomModeTools()
		{
			bool flag = this.ultraPrintPreviewControl1.Settings.ZoomMode == ZoomMode.PageWidth;
			bool flag2 = this.ultraPrintPreviewControl1.Settings.ZoomMode == ZoomMode.MarginsWidth;
			bool flag3 = this.ultraPrintPreviewControl1.Settings.ZoomMode == ZoomMode.WholePage;
			bool enabled = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			((StateButtonTool)this.ultraToolbarsManager2.Tools[PAGEWIDTH_KEY]).Checked = flag;
			((StateButtonTool)this.ultraToolbarsManager2.Tools[MARGINWIDTH_KEY]).Checked = flag2;
			((StateButtonTool)this.ultraToolbarsManager2.Tools[WHOLEPAGE_KEY]).Checked = flag3;
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UseStandardZoomModeIfNecessary()
		{
			if ((this.ultraToolbarsManager2.OptionSets["ZoomMode"].SelectedTool == null) && (this.ultraPrintPreviewControl1.Settings.ZoomMode != ZoomMode.Standard))
			{
				double currentZoom = this.ultraPrintPreviewControl1.CurrentZoom;
				this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.Standard;
				this.ultraPrintPreviewControl1.Settings.Zoom = currentZoom;
			}
		}

		#endregion
	}
}