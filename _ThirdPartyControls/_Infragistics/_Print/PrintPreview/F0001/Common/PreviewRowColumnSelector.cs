﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Infragistics.Win;

namespace iDASiT.Win.Mes
{
	[ToolboxItem(false)]
	internal class PreviewRowColumnSelector : Control
	{
		#region Fields
		private static readonly Color _backColorDefault;
		private static readonly Color _fForeColorDefault;
		private int _columns;
		private int _rows;
		private Color _imageBorderColor;
		private Rectangle[,] _imgRects;
		private Bitmap _rowColImage;
		private Stream _rowColImageStream;
		private int _selectedColumns;
		private int _selectedRows;
		private Size _textSize;
		#endregion

		#region Constructor
		/// <summary>
		/// PreviewRowColumnSelector 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		static PreviewRowColumnSelector()
		{
			_backColorDefault = SystemColors.Window;
			_fForeColorDefault = SystemColors.WindowText;
		}

		/// <summary>
		/// 
		/// </summary>
		public PreviewRowColumnSelector()
		{
			_rows = 3;
			_columns = 3;
			_imageBorderColor = Color.Empty;

			base.SetStyle(ControlStyles.Selectable, false);
			base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			base.SetStyle(ControlStyles.UserPaint, true);
			base.SetStyle(ControlStyles.FixedHeight, true);
			base.SetStyle(ControlStyles.FixedWidth, true);
			base.SetStyle(ControlStyles.DoubleBuffer, true);
			base.SetStyle(ControlStyles.ResizeRedraw, true);
			
			ResetBackColor();	
			ResetForeColor();
			TabStop = false;
			SetSelected(0, 0);
		}
		#endregion

		private void DirtyImageRects()
		{
			_imgRects = null;

			SetSelected(0, 0);

			if (base.Created == true)
				base.Width++;
		}

		private void DirtyTextSize()
		{
			_textSize = Size.Empty;

			if (base.Created == true)
				base.Width++;
		}

		private string GetDisplayText()
		{
			if ((_selectedColumns >= 1) && (_selectedRows >= 1))
				return GetDisplayText(_selectedColumns, _selectedRows);

			return "PreviewRowColSelection_Cancel";
		}

		private string GetDisplayText(int columns, int rows)
		{
			return string.Format("PreviewRowColSelection_SelectedPages {0}/{1}", rows, columns);
		}

		private Rectangle[,] GetImageRectangles()
		{
			if (_imgRects == null)
			{
				if ((_rows * _columns) == 0)
					_imgRects = new Rectangle[0, 0];
				else
				{
					Rectangle displayRectangle = DisplayRectangle;
					Size size = RowColImage.Size;
					displayRectangle.Inflate(-1, -1);
					Rectangle[,] rectangleArray = new Rectangle[_rows, _columns];

					for (int i = 0; i < _rows; i++)
					{
						int left = displayRectangle.Left;
						int top = displayRectangle.Top;

						for (int j = 0; j < _columns; j++)
						{
							(rectangleArray[i, j]) = new Rectangle(left, top, (size.Width + 6) + 2, (size.Height + 6) + 2);
							left += ((2 + size.Width) + 6) + 2;
						}

						displayRectangle.Y += ((size.Height + 2) + 6) + 2;
					}

					_imgRects = rectangleArray;
				}
			}

			return _imgRects;
		}

		protected override void OnCreateControl()
		{
			base.OnCreateControl();
			base.Width++;
		}

		protected override void OnFontChanged(EventArgs e)
		{
			DirtyTextSize();
			base.OnFontChanged(e);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			SetSelected(0, 0);
			base.OnMouseLeave(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			Rectangle[,] imageRectangles = GetImageRectangles();

			if ((imageRectangles != null) && (imageRectangles.Length > 0))
			{
				int selectedRows = 0;
				int selectedColumns = 0;

				for (int i = 0; i < _rows; i++)
				{
					if (e.Y <= imageRectangles[i, 0].Y)
						break;

					selectedRows++;
				}

				for (int j = 0; j < _columns; j++)
				{
					if (e.X <= imageRectangles[0, j].X)
						break;

					selectedColumns++;
				}

				SetSelected(selectedColumns, selectedRows);
			}

			base.OnMouseMove(e);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			Image rowColImage = RowColImage;
			Size size = rowColImage.Size;
			Rectangle displayRectangle = DisplayRectangle;
			Color color = Color.FromArgb(BackColor.ToArgb());

			if (color.A > 0)
			{
				using (Brush brush = new SolidBrush(color))
				{
					e.Graphics.FillRectangle(brush, displayRectangle);
				}
			}

			displayRectangle.Inflate(-1, -1);
			Rectangle[,] imageRectangles = GetImageRectangles();

			if ((imageRectangles != null) && (imageRectangles.Length > 0))
			{
				using (Pen pen = new Pen(ImageBorderColorResolved))
				{
					for (int i = 0; i < _rows; i++)
					{
						for (int j = 0; j < _columns; j++)
						{
							//Rectangle rect = *(imageRectangles[i, j]);
							Rectangle rect = (imageRectangles[i, j]);

							if ((i < _selectedRows) && (j < _selectedColumns))
								e.Graphics.FillRectangle(SystemBrushes.Highlight, rect);

							rect.Width--;
							rect.Height--;
							e.Graphics.DrawRectangle(pen, rect);
							//Rectangle rectangle3 = *(imageRectangles[i, j]);
							Rectangle rectangle3 = (imageRectangles[i, j]);
							rectangle3.Inflate(-4, -4);
							e.Graphics.DrawImage(rowColImage, rectangle3);
						}

						displayRectangle.Height -= imageRectangles[i, 0].Height;
						displayRectangle.Y += imageRectangles[i, 0].Height;
					}
				}

				displayRectangle.Height -= 2 * (_rows - 1);
				displayRectangle.Y += 2 * (_rows - 1);
			}

			using (StringFormat format = new StringFormat(StringFormatFlags.FitBlackBox))
			{
				format.Alignment = StringAlignment.Center;
				format.LineAlignment = StringAlignment.Center;
				Color color2 = Color.FromArgb(ForeColor.ToArgb());

				if (color2.A > 0)
				{
					using (Brush brush2 = new SolidBrush(color2))
					{
						e.Graphics.DrawString(GetDisplayText(), Font, brush2, displayRectangle, format);
					}
				}
			}

			base.OnPaint(e);
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public override void ResetBackColor()
		{
			BackColor = _backColorDefault;
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public override void ResetForeColor()
		{
			ForeColor = _fForeColorDefault;
		}

		protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
		{
			if (base.Created && ((specified & BoundsSpecified.Size) != BoundsSpecified.None))
			{
				Size rowColImageSize = RowColImageSize;
				rowColImageSize.Width += 2;
				rowColImageSize.Height += 2;
				rowColImageSize.Width += 6;
				rowColImageSize.Height += 6;
				rowColImageSize.Width *= _columns;
				rowColImageSize.Width += 2 * (_columns - 1);
				rowColImageSize.Height *= _rows;
				rowColImageSize.Height += 2 * (_rows - 1);
				width = Math.Max(rowColImageSize.Width, TextSize.Width);
				height = (rowColImageSize.Height + TextSize.Height) + 4;
				width += 2;
				height += 2;
			}

			base.SetBoundsCore(x, y, width, height, specified);
		}

		private void SetSelected(int selectedColumns, int selectedRows)
		{
			if ((_selectedColumns != selectedColumns) || (_selectedRows != selectedRows))
			{
				_selectedColumns = selectedColumns;
				_selectedRows = selectedRows;
				Text = GetDisplayText();

				if (base.IsHandleCreated)
					base.Invalidate();
			}
		}

		protected virtual bool ShouldSerializeBackColor()
		{
			return !BackColor.Equals(_backColorDefault);
		}

		protected virtual bool ShouldSerializeForeColor()
		{
			return !ForeColor.Equals(_fForeColorDefault);
		}

		/// <summary>
		/// 
		/// </summary>
		public void ResetSelection()
		{
			SetSelected(0, 0);
		}

		[DefaultValue(3)]
		public int Columns
		{
			get
			{
				return _columns;
			}
			set
			{
				if (_columns != value)
				{
					if (value < 1)
						throw new ArgumentOutOfRangeException();

					_columns = value;
					DirtyImageRects();
				}
			}
		}

		protected override ImeMode DefaultImeMode
		{
			get
			{
				return ImeMode.Disable;
			}
		}

		[DefaultValue(typeof(Color), "")]
		public Color ImageBorderColor
		{
			get
			{
				return _imageBorderColor;
			}
			set
			{
				if (value != _imageBorderColor)
				{
					_imageBorderColor = value;

					if (base.IsHandleCreated == true)
						base.Invalidate();
				}
			}
		}

		private Color ImageBorderColorResolved
		{
			get
			{
				Color imageBorderColor = ImageBorderColor;

				if (imageBorderColor.IsEmpty)
					imageBorderColor = Office2003Colors.PopupMenuBorder;
				else if (imageBorderColor.IsKnownColor)
					imageBorderColor = Color.FromArgb(imageBorderColor.ToArgb());

				return imageBorderColor;
			}
		}

		private Image RowColImage
		{
			get
			{
				if (_rowColImage == null)
				{
					_rowColImage = Properties.Resources.MultiplePagesImage;

					if (_rowColImage != null)
						_rowColImage.MakeTransparent(Color.Magenta);
				}

				return _rowColImage;
			}
		}

		private Size RowColImageSize
		{
			get
			{
				return RowColImage.Size;
			}
		}

		[DefaultValue(3)]
		public int Rows
		{
			get
			{
				return _rows;
			}
			set
			{
				if (_rows != value)
				{
					if (value < 1)
						throw new ArgumentOutOfRangeException();

					_rows = value;
					DirtyImageRects();
				}
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Size Selection
		{
			get
			{
				return new Size(_selectedColumns, _selectedRows);
			}
		}

		[DefaultValue(false)]
		public new bool TabStop
		{
			get
			{
				return base.TabStop;
			}
			set
			{
				base.TabStop = value;
			}
		}

		private Size TextSize
		{
			get
			{
				if (_textSize == Size.Empty)
				{
					Graphics cachedGraphics = DrawUtility.GetCachedGraphics(this);

					try
					{
						_textSize = Size.Ceiling(cachedGraphics.MeasureString(GetDisplayText(10, 10), Font, PointF.Empty, null));
					}
					finally
					{
						DrawUtility.ReleaseCachedGraphics(cachedGraphics);
					}
				}

				return _textSize;
			}
		}
	}
}