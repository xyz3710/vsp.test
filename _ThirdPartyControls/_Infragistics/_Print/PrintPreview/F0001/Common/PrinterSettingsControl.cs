using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Printing;

using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinTree;
using Infragistics.Win.UltraWinMaskedEdit;

namespace WinSchedulePrintDemo
{
	/// <summary>
	/// Control for manipulating the properties of a printer settings instance.
	/// </summary>
	public class PrinterSettingsControl : System.Windows.Forms.UserControl
	{
		private Infragistics.Win.Misc.UltraLabel lblPaperType;
		private Infragistics.Win.Misc.UltraLabel lblPaperTypeWidth;
		private Infragistics.Win.Misc.UltraLabel lblPaperTypeHeight;
		private Infragistics.Win.Misc.UltraLabel lblPaperSource;
		private Infragistics.Win.UltraWinEditors.UltraComboEditor cboPaperSource;
		private Infragistics.Win.UltraWinEditors.UltraPictureBox picOrientation;
		private Infragistics.Win.UltraWinEditors.UltraOptionSet optOrientation;
		private Infragistics.Win.Misc.UltraLabel lblMarginsTop;
		private Infragistics.Win.Misc.UltraLabel lblMarginsLeft;
		private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit mskMarginsTop;
		private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit mskMarginsLeft;
		private Infragistics.Win.Misc.UltraLabel lblMarginsBottom;
		private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit mskMarginsBottom;
		private Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit mskMarginsRight;
		private Infragistics.Win.Misc.UltraLabel lblMarginsRight;
		private Infragistics.Win.Misc.UltraLabel lblPrinterResolution;
		private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkColor;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDimensionsWidth;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDimensionsHeight;
		private Infragistics.Win.UltraWinTree.UltraTree trePaperType;
		private Infragistics.Win.UltraWinTree.UltraTree trePrinterResolutions;
		private System.Windows.Forms.GroupBox grpPaper;
		private System.Windows.Forms.GroupBox grpMargins;
		private System.Windows.Forms.GroupBox grpPage;
		private System.Windows.Forms.GroupBox grpOrientation;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		#region Constructor
		public PrinterSettingsControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.SetStyle(ControlStyles.AllPaintingInWmPaint |
				ControlStyles.DoubleBuffer					 |
				ControlStyles.SupportsTransparentBackColor, true);
		}
		#endregion //Constructor

		#region Dispose
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion //Dispose

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Infragistics.Win.ValueListItem valueListItem9 = new Infragistics.Win.ValueListItem();
			Infragistics.Win.ValueListItem valueListItem10 = new Infragistics.Win.ValueListItem();
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			this.trePaperType = new Infragistics.Win.UltraWinTree.UltraTree();
			this.lblPaperType = new Infragistics.Win.Misc.UltraLabel();
			this.lblPrinterResolution = new Infragistics.Win.Misc.UltraLabel();
			this.trePrinterResolutions = new Infragistics.Win.UltraWinTree.UltraTree();
			this.lblPaperTypeWidth = new Infragistics.Win.Misc.UltraLabel();
			this.lblPaperTypeHeight = new Infragistics.Win.Misc.UltraLabel();
			this.lblPaperSource = new Infragistics.Win.Misc.UltraLabel();
			this.cboPaperSource = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
			this.picOrientation = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
			this.optOrientation = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
			this.lblMarginsTop = new Infragistics.Win.Misc.UltraLabel();
			this.lblMarginsLeft = new Infragistics.Win.Misc.UltraLabel();
			this.mskMarginsTop = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
			this.mskMarginsLeft = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
			this.lblMarginsBottom = new Infragistics.Win.Misc.UltraLabel();
			this.mskMarginsBottom = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
			this.mskMarginsRight = new Infragistics.Win.UltraWinMaskedEdit.UltraMaskedEdit();
			this.lblMarginsRight = new Infragistics.Win.Misc.UltraLabel();
			this.chkColor = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
			this.txtDimensionsWidth = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.txtDimensionsHeight = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.grpPaper = new System.Windows.Forms.GroupBox();
			this.grpMargins = new System.Windows.Forms.GroupBox();
			this.grpPage = new System.Windows.Forms.GroupBox();
			this.grpOrientation = new System.Windows.Forms.GroupBox();
			((System.ComponentModel.ISupportInitialize)(this.trePaperType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trePrinterResolutions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cboPaperSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.optOrientation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDimensionsWidth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDimensionsHeight)).BeginInit();
			this.grpPaper.SuspendLayout();
			this.grpMargins.SuspendLayout();
			this.grpPage.SuspendLayout();
			this.grpOrientation.SuspendLayout();
			this.SuspendLayout();
			// 
			// trePaperType
			// 
			this.trePaperType.Location = new System.Drawing.Point(24, 32);
			this.trePaperType.Name = "trePaperType";
			this.trePaperType.Size = new System.Drawing.Size(200, 84);
			this.trePaperType.TabIndex = 1;
			this.trePaperType.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.trePaperType_AfterSelect);
			// 
			// lblPaperType
			// 
			this.lblPaperType.AutoSize = true;
			this.lblPaperType.Location = new System.Drawing.Point(8, 16);
			this.lblPaperType.Name = "lblPaperType";
			this.lblPaperType.Size = new System.Drawing.Size(62, 16);
			this.lblPaperType.TabIndex = 0;
			this.lblPaperType.Text = "용지 크기:";
			// 
			// lblPrinterResolution
			// 
			this.lblPrinterResolution.AutoSize = true;
			this.lblPrinterResolution.Location = new System.Drawing.Point(8, 16);
			this.lblPrinterResolution.Name = "lblPrinterResolution";
			this.lblPrinterResolution.Size = new System.Drawing.Size(69, 14);
			this.lblPrinterResolution.TabIndex = 0;
			this.lblPrinterResolution.Text = "&Resolution:";
			// 
			// trePrinterResolutions
			// 
			this.trePrinterResolutions.Location = new System.Drawing.Point(16, 32);
			this.trePrinterResolutions.Name = "trePrinterResolutions";
			this.trePrinterResolutions.Size = new System.Drawing.Size(208, 84);
			this.trePrinterResolutions.TabIndex = 1;
			this.trePrinterResolutions.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.trePrinterResolutions_AfterSelect);
			// 
			// lblPaperTypeWidth
			// 
			this.lblPaperTypeWidth.AutoSize = true;
			this.lblPaperTypeWidth.Location = new System.Drawing.Point(21, 123);
			this.lblPaperTypeWidth.Name = "lblPaperTypeWidth";
			this.lblPaperTypeWidth.Size = new System.Drawing.Size(54, 16);
			this.lblPaperTypeWidth.TabIndex = 3;
			this.lblPaperTypeWidth.Text = "너비(&W):";
			// 
			// lblPaperTypeHeight
			// 
			this.lblPaperTypeHeight.AutoSize = true;
			this.lblPaperTypeHeight.Location = new System.Drawing.Point(128, 123);
			this.lblPaperTypeHeight.Name = "lblPaperTypeHeight";
			this.lblPaperTypeHeight.Size = new System.Drawing.Size(50, 16);
			this.lblPaperTypeHeight.TabIndex = 5;
			this.lblPaperTypeHeight.Text = "높이(&E):";
			// 
			// lblPaperSource
			// 
			this.lblPaperSource.AutoSize = true;
			this.lblPaperSource.Location = new System.Drawing.Point(8, 154);
			this.lblPaperSource.Name = "lblPaperSource";
			this.lblPaperSource.Size = new System.Drawing.Size(62, 16);
			this.lblPaperSource.TabIndex = 7;
			this.lblPaperSource.Text = "용지 공급:";
			// 
			// cboPaperSource
			// 
			this.cboPaperSource.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
			this.cboPaperSource.Location = new System.Drawing.Point(24, 173);
			this.cboPaperSource.Name = "cboPaperSource";
			this.cboPaperSource.Size = new System.Drawing.Size(200, 21);
			this.cboPaperSource.TabIndex = 8;
			this.cboPaperSource.ValueChanged += new System.EventHandler(this.cboPaperSource_ValueChanged);
			// 
			// picOrientation
			// 
			this.picOrientation.BorderShadowColor = System.Drawing.Color.Empty;
			this.picOrientation.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.picOrientation.Location = new System.Drawing.Point(16, 20);
			this.picOrientation.MaintainAspectRatio = false;
			this.picOrientation.Name = "picOrientation";
			this.picOrientation.Padding = new System.Drawing.Size(2, 2);
			this.picOrientation.Size = new System.Drawing.Size(100, 100);
			this.picOrientation.TabIndex = 0;
			this.picOrientation.Paint += new System.Windows.Forms.PaintEventHandler(this.picOrientation_Paint);
			// 
			// optOrientation
			// 
			this.optOrientation.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
			valueListItem9.DataValue = false;
			valueListItem9.DisplayText = "세로";
			valueListItem10.DataValue = true;
			valueListItem10.DisplayText = "가로";
			this.optOrientation.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem9,
            valueListItem10});
			this.optOrientation.ItemSpacingVertical = 10;
			this.optOrientation.Location = new System.Drawing.Point(136, 40);
			this.optOrientation.Name = "optOrientation";
			this.optOrientation.Size = new System.Drawing.Size(63, 56);
			this.optOrientation.TabIndex = 1;
			this.optOrientation.TextIndentation = 2;
			this.optOrientation.ValueChanged += new System.EventHandler(this.optOrientation_ValueChanged);
			// 
			// lblMarginsTop
			// 
			this.lblMarginsTop.AutoSize = true;
			this.lblMarginsTop.Location = new System.Drawing.Point(14, 24);
			this.lblMarginsTop.Name = "lblMarginsTop";
			this.lblMarginsTop.Size = new System.Drawing.Size(50, 16);
			this.lblMarginsTop.TabIndex = 0;
			this.lblMarginsTop.Text = "위쪽(&T):";
			// 
			// lblMarginsLeft
			// 
			this.lblMarginsLeft.AutoSize = true;
			this.lblMarginsLeft.Location = new System.Drawing.Point(14, 47);
			this.lblMarginsLeft.Name = "lblMarginsLeft";
			this.lblMarginsLeft.Size = new System.Drawing.Size(49, 16);
			this.lblMarginsLeft.TabIndex = 2;
			this.lblMarginsLeft.Text = "왼쪽(&L):";
			// 
			// mskMarginsTop
			// 
			this.mskMarginsTop.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
			this.mskMarginsTop.InputMask = "nnn.nnn";
			this.mskMarginsTop.Location = new System.Drawing.Point(64, 20);
			this.mskMarginsTop.MinValue = 0;
			this.mskMarginsTop.Name = "mskMarginsTop";
			this.mskMarginsTop.Nullable = false;
			this.mskMarginsTop.PromptChar = ' ';
			this.mskMarginsTop.Size = new System.Drawing.Size(48, 21);
			this.mskMarginsTop.TabIndex = 1;
			this.mskMarginsTop.Text = "0.000";
			this.mskMarginsTop.Validated += new System.EventHandler(this.mskMarginsTop_Validated);
			// 
			// mskMarginsLeft
			// 
			this.mskMarginsLeft.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
			this.mskMarginsLeft.InputMask = "nnn.nnn";
			this.mskMarginsLeft.Location = new System.Drawing.Point(64, 43);
			this.mskMarginsLeft.MinValue = 0;
			this.mskMarginsLeft.Name = "mskMarginsLeft";
			this.mskMarginsLeft.Nullable = false;
			this.mskMarginsLeft.PromptChar = ' ';
			this.mskMarginsLeft.Size = new System.Drawing.Size(48, 21);
			this.mskMarginsLeft.TabIndex = 3;
			this.mskMarginsLeft.Text = "0.000";
			this.mskMarginsLeft.Validated += new System.EventHandler(this.mskMarginsLeft_Validated);
			// 
			// lblMarginsBottom
			// 
			this.lblMarginsBottom.AutoSize = true;
			this.lblMarginsBottom.Location = new System.Drawing.Point(113, 24);
			this.lblMarginsBottom.Name = "lblMarginsBottom";
			this.lblMarginsBottom.Size = new System.Drawing.Size(63, 16);
			this.lblMarginsBottom.TabIndex = 4;
			this.lblMarginsBottom.Text = "아래쪽(&B):";
			// 
			// mskMarginsBottom
			// 
			this.mskMarginsBottom.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
			this.mskMarginsBottom.InputMask = "nnn.nnn";
			this.mskMarginsBottom.Location = new System.Drawing.Point(176, 20);
			this.mskMarginsBottom.MinValue = 0;
			this.mskMarginsBottom.Name = "mskMarginsBottom";
			this.mskMarginsBottom.Nullable = false;
			this.mskMarginsBottom.PromptChar = ' ';
			this.mskMarginsBottom.Size = new System.Drawing.Size(48, 21);
			this.mskMarginsBottom.TabIndex = 5;
			this.mskMarginsBottom.Text = "0.000";
			this.mskMarginsBottom.Validated += new System.EventHandler(this.mskMarginsBottom_Validated);
			// 
			// mskMarginsRight
			// 
			this.mskMarginsRight.EditAs = Infragistics.Win.UltraWinMaskedEdit.EditAsType.Double;
			this.mskMarginsRight.InputMask = "nnn.nnn";
			this.mskMarginsRight.Location = new System.Drawing.Point(176, 43);
			this.mskMarginsRight.MinValue = 0;
			this.mskMarginsRight.Name = "mskMarginsRight";
			this.mskMarginsRight.Nullable = false;
			this.mskMarginsRight.PromptChar = ' ';
			this.mskMarginsRight.Size = new System.Drawing.Size(48, 21);
			this.mskMarginsRight.TabIndex = 7;
			this.mskMarginsRight.Text = "0.000";
			this.mskMarginsRight.Validated += new System.EventHandler(this.mskMarginsRight_Validated);
			// 
			// lblMarginsRight
			// 
			this.lblMarginsRight.AutoSize = true;
			this.lblMarginsRight.Location = new System.Drawing.Point(113, 47);
			this.lblMarginsRight.Name = "lblMarginsRight";
			this.lblMarginsRight.Size = new System.Drawing.Size(63, 16);
			this.lblMarginsRight.TabIndex = 6;
			this.lblMarginsRight.Text = "오른쪽(&R):";
			// 
			// chkColor
			// 
			this.chkColor.Location = new System.Drawing.Point(8, 123);
			this.chkColor.Name = "chkColor";
			this.chkColor.Size = new System.Drawing.Size(88, 20);
			this.chkColor.TabIndex = 2;
			this.chkColor.Text = "Color";
			this.chkColor.CheckedChanged += new System.EventHandler(this.chkColor_CheckedChanged);
			// 
			// txtDimensionsWidth
			// 
			appearance2.TextHAlignAsString = "Right";
			this.txtDimensionsWidth.Appearance = appearance2;
			this.txtDimensionsWidth.Location = new System.Drawing.Point(72, 123);
			this.txtDimensionsWidth.Name = "txtDimensionsWidth";
			this.txtDimensionsWidth.ReadOnly = true;
			this.txtDimensionsWidth.Size = new System.Drawing.Size(48, 21);
			this.txtDimensionsWidth.TabIndex = 4;
			// 
			// txtDimensionsHeight
			// 
			appearance3.TextHAlignAsString = "Right";
			this.txtDimensionsHeight.Appearance = appearance3;
			this.txtDimensionsHeight.Location = new System.Drawing.Point(176, 123);
			this.txtDimensionsHeight.Name = "txtDimensionsHeight";
			this.txtDimensionsHeight.ReadOnly = true;
			this.txtDimensionsHeight.Size = new System.Drawing.Size(48, 21);
			this.txtDimensionsHeight.TabIndex = 6;
			// 
			// grpPaper
			// 
			this.grpPaper.Controls.Add(this.txtDimensionsWidth);
			this.grpPaper.Controls.Add(this.cboPaperSource);
			this.grpPaper.Controls.Add(this.lblPaperSource);
			this.grpPaper.Controls.Add(this.lblPaperTypeWidth);
			this.grpPaper.Controls.Add(this.trePaperType);
			this.grpPaper.Controls.Add(this.lblPaperType);
			this.grpPaper.Controls.Add(this.txtDimensionsHeight);
			this.grpPaper.Controls.Add(this.lblPaperTypeHeight);
			this.grpPaper.Location = new System.Drawing.Point(8, 7);
			this.grpPaper.Name = "grpPaper";
			this.grpPaper.Size = new System.Drawing.Size(240, 206);
			this.grpPaper.TabIndex = 0;
			this.grpPaper.TabStop = false;
			this.grpPaper.Text = "용지";
			// 
			// grpMargins
			// 
			this.grpMargins.Controls.Add(this.mskMarginsBottom);
			this.grpMargins.Controls.Add(this.mskMarginsLeft);
			this.grpMargins.Controls.Add(this.mskMarginsTop);
			this.grpMargins.Controls.Add(this.mskMarginsRight);
			this.grpMargins.Controls.Add(this.lblMarginsRight);
			this.grpMargins.Controls.Add(this.lblMarginsBottom);
			this.grpMargins.Controls.Add(this.lblMarginsLeft);
			this.grpMargins.Controls.Add(this.lblMarginsTop);
			this.grpMargins.Location = new System.Drawing.Point(8, 219);
			this.grpMargins.Name = "grpMargins";
			this.grpMargins.Size = new System.Drawing.Size(240, 72);
			this.grpMargins.TabIndex = 1;
			this.grpMargins.TabStop = false;
			this.grpMargins.Text = "여백";
			// 
			// grpPage
			// 
			this.grpPage.Controls.Add(this.chkColor);
			this.grpPage.Controls.Add(this.trePrinterResolutions);
			this.grpPage.Controls.Add(this.lblPrinterResolution);
			this.grpPage.Location = new System.Drawing.Point(259, 7);
			this.grpPage.Name = "grpPage";
			this.grpPage.Size = new System.Drawing.Size(240, 150);
			this.grpPage.TabIndex = 2;
			this.grpPage.TabStop = false;
			this.grpPage.Text = "Page";
			// 
			// grpOrientation
			// 
			this.grpOrientation.Controls.Add(this.picOrientation);
			this.grpOrientation.Controls.Add(this.optOrientation);
			this.grpOrientation.Location = new System.Drawing.Point(259, 163);
			this.grpOrientation.Name = "grpOrientation";
			this.grpOrientation.Size = new System.Drawing.Size(240, 128);
			this.grpOrientation.TabIndex = 3;
			this.grpOrientation.TabStop = false;
			this.grpOrientation.Text = "용지 방향";
			// 
			// PrinterSettingsControl
			// 
			this.Controls.Add(this.grpOrientation);
			this.Controls.Add(this.grpPage);
			this.Controls.Add(this.grpMargins);
			this.Controls.Add(this.grpPaper);
			this.Name = "PrinterSettingsControl";
			this.Size = new System.Drawing.Size(507, 306);
			this.Load += new System.EventHandler(this.PrinterSettingsControl_Load);
			((System.ComponentModel.ISupportInitialize)(this.trePaperType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trePrinterResolutions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cboPaperSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.optOrientation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDimensionsWidth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDimensionsHeight)).EndInit();
			this.grpPaper.ResumeLayout(false);
			this.grpPaper.PerformLayout();
			this.grpMargins.ResumeLayout(false);
			this.grpMargins.PerformLayout();
			this.grpPage.ResumeLayout(false);
			this.grpPage.PerformLayout();
			this.grpOrientation.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private PrinterSettings			printerSettings;
		private PageSettings			pageSettings;
		private bool					isInitializing = false;

		#region PrinterSettings
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Drawing.Printing.PrinterSettings PrinterSettings
		{
			get 
			{ 
				if (this.printerSettings == null)
					this.printerSettings = new PrinterSettings();

				return this.printerSettings; 
			}
			set
			{
				this.printerSettings = value;

				if (this.pageSettings != null)
					this.pageSettings.PrinterSettings = value;
				else
					this.pageSettings = this.printerSettings.DefaultPageSettings;

				this.RefreshUI();
			}
		}
		#endregion //PrinterSettings

		#region PageSettings
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Drawing.Printing.PageSettings PageSettings
		{
			get
			{
				if (this.pageSettings == null)
					this.pageSettings = new PageSettings(this.PrinterSettings);

				return this.pageSettings;
			}
			set
			{
				this.pageSettings = value;

				this.printerSettings = value == null ? null : value.PrinterSettings;

				this.RefreshUI();
			}
		}
		#endregion //PageSettings

		#region RefreshUI
		private void RefreshUI()
		{
			if (this.DesignMode)
				return;

			this.InitializeUI(this.PrinterSettings);
			this.InitializeSelection(this.PageSettings);
		}
		#endregion //RefreshUI

		#region InitializeSelection
		private void InitializeSelection(System.Drawing.Printing.PageSettings pageSettings)
		{
			this.isInitializing = true;

			// paper type/size
			this.InitializeTree(this.trePaperType, pageSettings.PaperSize);

			// paper source
			this.InitializeCombo(this.cboPaperSource, pageSettings.PaperSource);

			// printer resolution
			this.InitializeTree(this.trePrinterResolutions, pageSettings.PrinterResolution);

			// color
			this.chkColor.Checked = pageSettings.Color;

			// landscape/portrait
			this.optOrientation.Value = pageSettings.Landscape;

			// margins
			this.mskMarginsBottom.Value = (double)pageSettings.Margins.Bottom / 100d;
			this.mskMarginsTop.Value = (double)pageSettings.Margins.Top / 100d;
			this.mskMarginsLeft.Value = (double)pageSettings.Margins.Left / 100d;
			this.mskMarginsRight.Value = (double)pageSettings.Margins.Right / 100d;

			this.isInitializing = false;
		}
		#endregion //InitializeSelection

		#region IsSameObject
		private bool IsSameObject(object printObject1, object printObject2)
		{
			if (printObject1 == printObject2)
				return true;

			if (printObject1 == null || printObject2 == null)
				return false;

			if (printObject1.GetType() != printObject2.GetType())
				return false;

			if (printObject1 is PaperSize)
			{
				PaperSize p1 = printObject1 as PaperSize;
				PaperSize p2 = printObject2 as PaperSize;

				return p1.Kind == p2.Kind	&&
					p1.Height == p2.Height	&&
					p1.Width == p2.Width	&&
					p1.PaperName == p2.PaperName;
			}
			else if (printObject1 is PaperSource)
			{
				PaperSource p1 = printObject1 as PaperSource;
				PaperSource p2 = printObject2 as PaperSource;

				return p1.Kind == p2.Kind &&
					p1.SourceName == p2.SourceName;
			}
			else if (printObject1 is PrinterResolution)
			{
				PrinterResolution p1 = printObject1 as PrinterResolution;
				PrinterResolution p2 = printObject2 as PrinterResolution;

				return p1.Kind == p2.Kind	&&
					p1.X == p2.X			&&
					p1.Y == p2.Y;
			}

			return false;
		}
		#endregion //IsSameObject

		#region InitializeCombo
		private void InitializeCombo(UltraComboEditor cbo, object dataValue)
		{
			cbo.SelectedIndex = -1;

			foreach(ValueListItem item in cbo.Items)
			{
				//if (!item.DataValue != dataValue)
				if (!IsSameObject(item.DataValue, dataValue))
					continue;

				cbo.SelectedItem = item;
				break;
			}
		}
		#endregion //InitializeCombo

		#region InitializeTree
		private void InitializeTree(UltraTree tree, object tagValue)
		{
			tree.SelectedNodes.Clear();
			foreach(UltraTreeNode node in tree.Nodes)
			{
				//if (node.Tag != tagValue)
				if (!IsSameObject(node.Tag, tagValue))
					continue;

				node.Selected = true;
				break;
			}
		}
		#endregion //InitializeTree

		#region InitializeUI
		private void InitializeUI(System.Drawing.Printing.PrinterSettings printerSettings)
		{
			// Paper Sizes
			this.trePaperType.Nodes.Clear();

			foreach(PaperSize size in printerSettings.PaperSizes)
			{
				UltraTreeNode node = new UltraTreeNode(null, size.PaperName);
				node.Tag = size;

				this.trePaperType.Nodes.Add(node);
			}

			// Printer Resolutions
			this.trePrinterResolutions.Nodes.Clear();

			foreach(PrinterResolution resolution in printerSettings.PrinterResolutions)
			{
				string text;

				if (resolution.X >= 0)
					text = string.Format("{0} ({1}x{2})", resolution.Kind, resolution.X, resolution.Y);
				else
					text = resolution.Kind.ToString();

				UltraTreeNode node = new UltraTreeNode(null, text);
				node.Tag = resolution;

				this.trePrinterResolutions.Nodes.Add(node);
			}

			// Color Print
			this.chkColor.Enabled = printerSettings.SupportsColor;

			// Paper Sources
			this.cboPaperSource.Items.Clear();

			foreach(PaperSource source in printerSettings.PaperSources)
				this.cboPaperSource.Items.Add(source, source.SourceName);
		}
		#endregion //InitializeUI

		#region PrinterSettingsControl_Load
		private void PrinterSettingsControl_Load(object sender, System.EventArgs e)
		{
			if (this.DesignMode)
				return;

			// a paper source must be selected from the list
			this.cboPaperSource.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;

			// initialize trees for single select only
			this.trePaperType.Override.SelectionType = SelectType.Single;
			this.trePrinterResolutions.Override.SelectionType = SelectType.Single;

			// no root lines
			this.trePaperType.ShowRootLines = false;
			this.trePrinterResolutions.ShowRootLines = false;

			// make sure the backcolor of the trees are not transparent
			((Control)this.trePaperType).BackColor = SystemColors.Window;
			((Control)this.trePrinterResolutions).BackColor = SystemColors.Window;

			this.trePaperType.HideSelection = false;
			this.trePrinterResolutions.HideSelection = false;

			this.RefreshUI();
		}
		#endregion //PrinterSettingsControl_Load

		#region trePaperType_AfterSelect
		private void trePaperType_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
		{
			UltraTreeNode selectedNode = e.NewSelections.Count == 0 ? null : e.NewSelections[0];

			PaperSize ps = selectedNode == null ? null : (PaperSize)selectedNode.Tag;

			if (ps == null)
			{
				this.txtDimensionsWidth.Text = string.Empty;
				this.txtDimensionsHeight.Text = string.Empty;
			}
			else
			{
				double width = ps.Width;
				double height = ps.Height;

				// if its metric convert from hundredths of an inch to hundreds of a millimeter
				/*
				if (System.Globalization.RegionInfo.CurrentRegion.IsMetric)
				{
					width = PrinterUnitConvert.Convert(width, PrinterUnit.Display, PrinterUnit.HundredthsOfAMillimeter);
					height = PrinterUnitConvert.Convert(height, PrinterUnit.Display, PrinterUnit.HundredthsOfAMillimeter);
				}
				*/

				// then convert to whole numbers (inches or millimeters)
				width /= 100d;
				height /= 100d;

				// then store the value
				this.txtDimensionsWidth.Text = string.Format("{0:N2}\"", width);
				this.txtDimensionsHeight.Text = string.Format("{0:N2}\"", height);
			}

			if (!this.isInitializing)
				this.PageSettings.PaperSize = ps;

			this.picOrientation.Invalidate();
		}
		#endregion //trePaperType_AfterSelect

		#region Property Change Events
		private void cboPaperSource_ValueChanged(object sender, System.EventArgs e)
		{
			if (!this.isInitializing)
				this.PageSettings.PaperSource = (PaperSource)((UltraComboEditor)sender).Value;
		}

		private void chkColor_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.isInitializing)
				this.PageSettings.Color = ((UltraCheckEditor)sender).Checked;
		}

		private void trePrinterResolutions_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
		{
			UltraTreeNode selectedNode = e.NewSelections.Count == 0 ? null : e.NewSelections[0];

			PrinterResolution resolution = selectedNode == null ? null : (PrinterResolution)selectedNode.Tag;

			if (!this.isInitializing)
				this.PageSettings.PrinterResolution = resolution;
		}

		private void optOrientation_ValueChanged(object sender, System.EventArgs e)
		{
			if (!this.isInitializing)
				this.PageSettings.Landscape = (bool)((UltraOptionSet)sender).Value;

			this.picOrientation.Invalidate();
		}

		private void mskMarginsTop_Validated(object sender, System.EventArgs e)
		{
			UltraMaskedEdit maskEdit = ((UltraMaskedEdit)sender);

			if (!this.isInitializing)
				this.PageSettings.Margins.Top = (int)( (double)maskEdit.Value * 100 );
			this.picOrientation.Invalidate();
		}

		private void mskMarginsLeft_Validated(object sender, System.EventArgs e)
		{
			UltraMaskedEdit maskEdit = ((UltraMaskedEdit)sender);

			if (!this.isInitializing)
				this.PageSettings.Margins.Left = (int)( (double)maskEdit.Value * 100 );

			this.picOrientation.Invalidate();
		}

		private void mskMarginsBottom_Validated(object sender, System.EventArgs e)
		{
			UltraMaskedEdit maskEdit = ((UltraMaskedEdit)sender);

			if (!this.isInitializing)
				this.PageSettings.Margins.Bottom = (int)( (double)maskEdit.Value * 100 );

			this.picOrientation.Invalidate();
		}

		private void mskMarginsRight_Validated(object sender, System.EventArgs e)
		{
			UltraMaskedEdit maskEdit = ((UltraMaskedEdit)sender);

			if (!this.isInitializing)
				this.PageSettings.Margins.Right = (int)( (double)maskEdit.Value * 100 );

			this.picOrientation.Invalidate();
		}
		#endregion //Property Change Events

		#region PreviewPage Paint
		private void picOrientation_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UltraTreeNode selectedNode = this.trePaperType.SelectedNodes.Count == 0 ? null : this.trePaperType.SelectedNodes[0];

			PaperSize ps = selectedNode == null ? null : (PaperSize)selectedNode.Tag;

			if (ps == null)
				return;

			Rectangle availableRect = this.picOrientation.DisplayRectangle;
			availableRect.Inflate(-4,-4);

			int maxDimension = Math.Max(ps.Width, ps.Height);
			bool landScape = this.PageSettings.Landscape;
			int width = landScape ? ps.Height : ps.Width;
			int height = landScape ? ps.Width : ps.Height;

			int pageWidth = (int)((width / (decimal)maxDimension) * availableRect.Width);
			int pageHeight = (int)((height / (decimal)maxDimension) * availableRect.Height);

			Rectangle pageRect = new Rectangle(0,0, pageWidth, pageHeight);

			// center the page
			DrawUtility.AdjustHAlign(HAlign.Center, ref pageRect, availableRect);
			DrawUtility.AdjustVAlign(VAlign.Middle, ref pageRect, availableRect);

			e.Graphics.FillRectangle(SystemBrushes.Window, pageRect);
			e.Graphics.DrawRectangle(SystemPens.WindowFrame, pageRect);

			// now draw the margins
			int marginLeft = (int)((this.PageSettings.Margins.Left / (decimal)maxDimension) * availableRect.Width);
			int marginRight = (int)((this.PageSettings.Margins.Right / (decimal)maxDimension) * availableRect.Width);
			int marginTop = (int)((this.PageSettings.Margins.Top / (decimal)maxDimension) * availableRect.Width);
			int marginBottom = (int)((this.PageSettings.Margins.Bottom / (decimal)maxDimension) * availableRect.Width);

			pageRect.X += marginLeft;
			pageRect.Width -= marginLeft + marginRight;
			pageRect.Y += marginTop;
			pageRect.Height -= marginTop + marginBottom;

			if (pageRect.Width > 0 && pageRect.Height > 0)
				ControlPaint.DrawFocusRectangle(e.Graphics, pageRect, SystemColors.WindowFrame, SystemColors.Window);
		}
		#endregion //PreviewPage Paint
	}
}
