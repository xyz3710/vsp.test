﻿using Infragistics.Win.UltraWinToolbars;
using System.Windows.Forms;
using System.Resources;
using Infragistics.Win;
using System.ComponentModel;
using Infragistics.Win.Printing;
using System.Drawing;
using System;

namespace iDASiT.Win.Mes
{
	partial class F0002
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드
		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(F0002));
			this.ultraGridPrintDocument1 = new Infragistics.Win.UltraWinGrid.UltraGridPrintDocument(this.components);
			this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
			this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
			this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
			this.pnlContainerBody_Fill_Panel = new System.Windows.Forms.Panel();
			this.ultraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.pnlContainerBody_Fill_Panel_Fill_Panel = new System.Windows.Forms.Panel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.panel3 = new System.Windows.Forms.Panel();

			this.ultraToolbarsManager2 = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.imageList2 = new System.Windows.Forms.ImageList(this.components);
			this.ultraButton4 = new Infragistics.Win.Misc.UltraButton();
			this.pnlContainerBody.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).BeginInit();
			this.pnlContainerBody_Fill_Panel.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.panel3.SuspendLayout();
			
			((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager2)).BeginInit();
			this.SuspendLayout();
			// 
			// pnlContainerBody
			// 
			this.pnlContainerBody.Controls.Add(this.pnlContainerBody_Fill_Panel);
			this.pnlContainerBody.Size = new System.Drawing.Size(928, 671);
			// 
			// ultraGridPrintDocument1
			// 
			this.ultraGridPrintDocument1.Grid = this.ultraGrid1;

			// 
			// ultraButton1
			// 
			this.ultraButton1.Location = new System.Drawing.Point(6, 3);
			this.ultraButton1.Name = "ultraButton1";
			this.ultraButton1.Size = new System.Drawing.Size(75, 23);
			this.ultraButton1.TabIndex = 1;
			this.ultraButton1.Text = "적용";
			this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
			// 
			// ultraButton2
			// 
			this.ultraButton2.Location = new System.Drawing.Point(87, 3);
			this.ultraButton2.Name = "ultraButton2";
			this.ultraButton2.Size = new System.Drawing.Size(75, 23);
			this.ultraButton2.TabIndex = 3;
			this.ultraButton2.Text = "ultraButton2";
			// 
			// ultraButton3
			// 
			this.ultraButton3.Location = new System.Drawing.Point(168, 3);
			this.ultraButton3.Name = "ultraButton3";
			this.ultraButton3.Size = new System.Drawing.Size(75, 23);
			this.ultraButton3.TabIndex = 4;
			this.ultraButton3.Text = "ultraButton3";
			// 
			// pnlContainerBody_Fill_Panel
			// 
			this.pnlContainerBody_Fill_Panel.Controls.Add(this.ultraButton4);
			this.pnlContainerBody_Fill_Panel.Controls.Add(this.ultraGrid1);
			this.pnlContainerBody_Fill_Panel.Controls.Add(this.ultraButton1);
			this.pnlContainerBody_Fill_Panel.Controls.Add(this.ultraButton2);
			this.pnlContainerBody_Fill_Panel.Controls.Add(this.ultraButton3);

			this.pnlContainerBody_Fill_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlContainerBody_Fill_Panel.Location = new System.Drawing.Point(3, 3);
			this.pnlContainerBody_Fill_Panel.Name = "pnlContainerBody_Fill_Panel";
			this.pnlContainerBody_Fill_Panel.Size = new System.Drawing.Size(920, 640);
			this.pnlContainerBody_Fill_Panel.TabIndex = 0;
			// 
			// ultraGrid2
			// 
			this.ultraGrid1.Location = new System.Drawing.Point(28, 41);
			this.ultraGrid1.Name = "ultraGrid2";
			this.ultraGrid1.Size = new System.Drawing.Size(493, 355);
			this.ultraGrid1.TabIndex = 0;
			this.ultraGrid1.Text = "ultraGrid2";
			// 
			// pnlContainerBody_Fill_Panel_Fill_Panel
			// 
			this.pnlContainerBody_Fill_Panel_Fill_Panel.Location = new System.Drawing.Point(0, 0);
			this.pnlContainerBody_Fill_Panel_Fill_Panel.Name = "pnlContainerBody_Fill_Panel_Fill_Panel";
			this.pnlContainerBody_Fill_Panel_Fill_Panel.Size = new System.Drawing.Size(200, 100);
			this.pnlContainerBody_Fill_Panel_Fill_Panel.TabIndex = 0;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 293F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 259F));
			this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
			
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(920, 569);
			this.tableLayoutPanel1.TabIndex = 7;

			// 
			// panel3
			// 
			this.tableLayoutPanel1.SetColumnSpan(this.panel3, 2);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(3, 44);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(655, 507);
			this.panel3.TabIndex = 11;
			// 
			// ultraToolbarsManager2
			// 
			this.ultraToolbarsManager2.DesignerFlags = 1;
			// 
			// imageList1
			// 
			this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// imageList2
			// 
			this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
			this.imageList2.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList2.TransparentColor = System.Drawing.Color.Transparent;

			// 
			// ultraButton4
			// 
			this.ultraButton4.Location = new System.Drawing.Point(732, 50);
			this.ultraButton4.Name = "ultraButton4";
			this.ultraButton4.Size = new System.Drawing.Size(75, 23);
			this.ultraButton4.TabIndex = 1;
			this.ultraButton4.Text = "ultraButton4";
			// 
			// F0550
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.ClientSize = new System.Drawing.Size(928, 671);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "F0550";
			this.Shown += new System.EventHandler(this.PrintPreview_Shown);
			this.Load += new System.EventHandler(this.F0550_Load);
			this.pnlContainerBody.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ultraGrid1)).EndInit();
			this.pnlContainerBody_Fill_Panel.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			
			((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager2)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Infragistics.Win.Misc.UltraButton ultraButton1;
		private Infragistics.Win.UltraWinGrid.UltraGridPrintDocument ultraGridPrintDocument1;
		private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
		private Infragistics.Win.Misc.UltraButton ultraButton2;
		private Infragistics.Win.Misc.UltraButton ultraButton3;
		private System.Windows.Forms.Panel pnlContainerBody_Fill_Panel;
		private System.Windows.Forms.Panel pnlContainerBody_Fill_Panel_Fill_Panel;
		private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager ultraToolbarsManager2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ImageList imageList2;
 
		private System.Windows.Forms.Panel panel3;
		private Infragistics.Win.Misc.UltraButton ultraButton4;
 
 
	}
}
  