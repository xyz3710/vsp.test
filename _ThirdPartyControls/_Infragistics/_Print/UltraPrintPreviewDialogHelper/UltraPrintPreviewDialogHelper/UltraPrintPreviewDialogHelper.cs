﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Controls.Print.UltraPrintPreviewDialogHelper
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 12월 11일 금요일 오후 7:10
/*	Purpose		:	UltraPrintPreviewDialog의 설정을 도와주는 클래스 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 12월 16일 수요일 오후 8:51
/**********************************************************************************************************************/

#region Using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Printing;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win.Printing;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinStatusBar;
using Infragistics.Win.UltraWinToolbars;
using AnyFactory.FX.Win.Controls.InfragisticsResource;
#endregion

namespace AnyFactory.FX.Win.Controls.Print
{
	/// <summary>
	/// UltraPrintPreviewDialog의 설정을 도와주는 클래스 입니다.
	/// </summary>
	public partial class UltraPrintPreviewDialogHelper : UltraPrintPreviewDialog
	{
		#region Constants
		/// <summary>
		/// Math.Round(1 / 2.54, 3)한 값으로 단위를 환산한다.
		/// </summary>
		public const double EXCHANGE_UNIT = 0.394d;
		private const string PAGE_SETUP_KEY = "Page Setup";
		private const string ZOOM_IN_TOOL_KEY = "Zoom In Tool";
		private const string THUMBNAILS_KEY = "Thumbnails";
		private const string PRINT_KEY = "Print";
		private const string HAND_TOOL_KEY = "Hand Tool";
		private const string SNAPSHOT_TOOL_KEY = "Snapshot Tool";
		private const string DYNAMIC_ZOOM_TOOL_KEY = "Dynamic Zoom Tool";
		private const string PAGE_WIDTH_KEY = "Page Width";
		private const string MARGIN_WIDTH_KEY = "Margin Width";
		private const string WHOLE_PAGE_KEY = "Whole Page";
		private const string CONTINUOUS_KEY = "Continuous";
		private const string CURRENT_PAGE_KEY = "Current Page";
		private const string LANDSCAPE_KEY = "Landscape";
		private const string DEFAULT_SETTING_FILENAME = "UltraPrintPreviewDialogHelper.config";
		private const string STANDARD_TOOLBAR_KEY = "Standard";
		private const string VIEW_TOOLBAR_KEY = "View";
		private const string MENUBAR_TOOLBAR_KEY = "MenuBar";
		#endregion

		#region Fields
		private bool _landscape;
		private string _settingFilePath;
		private bool _autoSaveSettings;
		private bool _showContinuousPage;
		private bool _showThumbnails;
		private double _zoom;
		private ZoomMode _zoomMode;
		private UltraGrid _documentGrid;
		private Dictionary<HeaderFooterAlign, string> _headerFooterText;
		#endregion

		#region Constructor
		/// <summary>
		/// UltraPrintPreviewDialog의 설정을 도와주는 새 인스턴스를 초기화 합니다.
		/// </summary>        
		public UltraPrintPreviewDialogHelper()
		{
			_headerFooterText = new Dictionary<HeaderFooterAlign, string>(6);

			CustomizedString.SetUltraPrintingResourceStringToLocalization();
			CustomizedString.SetUltraWinToolbarsResourceStringToLocalization();
			SetInternalControls();
			InitializeComponent();
		}

		private void SetInternalControls()
		{
			GetInternalControls();
			InternalUltraPrintDocument = new UltraGridPrintDocument(components);
			InitUltraToolbarsManager();
			InitPrintDiaglog();
			InitPageSetup();
			InitUltraPrintPreviewControl();

			base.Load += new EventHandler(OnUltraPrintPreviewDialogHelperLoad);
			base.FormClosing += new FormClosingEventHandler(OnUltraPrintPreviewDialogHelperFormClosing);
			base.Printing += new PrintingEventHandler(OnPrinting);

			StartPosition = FormStartPosition.CenterParent;
			WindowState = FormWindowState.Maximized;
			AutoSaveSettings = true;
			ShowContinuousPage = true;
			ShowThumbnails = true;
			Zoom = 1d;
			ZoomMode = ZoomMode.AutoFlow;

			AutoScale = true;
		}

		private void InitPrintDiaglog()
		{
			InternalPrintDialog = new PrintDialog
			{
				AllowCurrentPage = true,
				AllowPrintToFile = false,
				AllowSelection = true,
				AllowSomePages = true,
				ShowHelp = true,
				UseEXDialog = true,
				Document = Document,
			};
		}

		private void InitPageSetup()
		{
			if (InternalPageSetupDialog != null)
				InternalPageSetupDialog.EnableMetric = false;

			ResetPageMargins();
		}

		private void InitUltraToolbarsManager()
		{
			UltraToolbar standardTools = InternalUltraToolbarsManager.Toolbars[STANDARD_TOOLBAR_KEY];
			UltraToolbar viewTools = InternalUltraToolbarsManager.Toolbars[VIEW_TOOLBAR_KEY];
			UltraToolbar menubarTools = InternalUltraToolbarsManager.Toolbars[MENUBAR_TOOLBAR_KEY];

			standardTools.DockedPosition = DockedPosition.Top;
			standardTools.DockedColumn = 0;
			standardTools.DockedRow = 0;

			viewTools.DockedPosition = DockedPosition.Top;
			viewTools.DockedColumn = 1;
			viewTools.DockedRow = 0;

			menubarTools.DockedPosition = DockedPosition.Left;
			menubarTools.DockedColumn = 0;
			menubarTools.DockedRow = 0;

			// Page Setup 추가
			ToolBase pageSetupTool = standardTools.Tools.InsertTool(standardTools.Tools.IndexOf(PRINT_KEY) + 1, PAGE_SETUP_KEY);
			pageSetupTool.SharedProps.DisplayStyle = ToolDisplayStyle.DefaultForToolType;
			pageSetupTool.SharedProps.AppearancesSmall.Appearance.Image = Resources.PageSetupHS;
			pageSetupTool.SharedProps.AppearancesLarge.Appearance.Image = Resources.PageSetupHS;

			// Landscape 추가
			StateButtonTool landScapeTool = new StateButtonTool(LANDSCAPE_KEY);
			landScapeTool.SharedProps.DisplayStyle = ToolDisplayStyle.DefaultForToolType;
			landScapeTool.SharedProps.AppearancesSmall.Appearance.Image = Resources.LandscapeHL;
			landScapeTool.SharedProps.AppearancesLarge.Appearance.Image = Resources.LandscapeHH;
			landScapeTool.SharedProps.Shortcut = Shortcut.CtrlL;
			landScapeTool.SharedProps.Category = standardTools.Tools[PRINT_KEY].SharedProps.Category;
			landScapeTool.SharedProps.ToolTipText = "가로 방향";
			landScapeTool.SharedProps.Caption = "가로 방향";
			landScapeTool.SharedProps.AccessibleDescription = "용지를 가로 방향으로 설정합니다.";
			landScapeTool.InstanceProps.Caption = landScapeTool.SharedProps.Caption;

			InternalUltraToolbarsManager.Tools.Add(landScapeTool);
			standardTools.Tools.InsertTool(standardTools.Tools.IndexOf(PAGE_SETUP_KEY) + 1, LANDSCAPE_KEY);

			// Thumbnails 추가
			StateButtonTool thumbnails = standardTools.Tools.InsertTool(standardTools.Tools.IndexOf(ZOOM_IN_TOOL_KEY) + 1, THUMBNAILS_KEY) as StateButtonTool;
			thumbnails.InstanceProps.IsFirstInGroup = true;

			InternalUltraToolbarsManager.Tools[PRINT_KEY].SharedProps.Shortcut = Shortcut.CtrlP;
			InternalUltraToolbarsManager.Tools[PAGE_SETUP_KEY].SharedProps.Shortcut = Shortcut.CtrlShiftP;

			InternalUltraToolbarsManager.Tools[THUMBNAILS_KEY].SharedProps.Shortcut = Shortcut.CtrlT;
			InternalUltraToolbarsManager.Tools[HAND_TOOL_KEY].SharedProps.Shortcut = Shortcut.CtrlH;
			InternalUltraToolbarsManager.Tools[SNAPSHOT_TOOL_KEY].SharedProps.Shortcut = Shortcut.CtrlS;
			InternalUltraToolbarsManager.Tools[DYNAMIC_ZOOM_TOOL_KEY].SharedProps.Shortcut = Shortcut.CtrlD;

			InternalUltraToolbarsManager.Tools[PAGE_WIDTH_KEY].SharedProps.Shortcut = Shortcut.CtrlA;
			InternalUltraToolbarsManager.Tools[MARGIN_WIDTH_KEY].SharedProps.Shortcut = Shortcut.CtrlM;
			InternalUltraToolbarsManager.Tools[WHOLE_PAGE_KEY].SharedProps.Shortcut = Shortcut.CtrlW;
			InternalUltraToolbarsManager.Tools[CONTINUOUS_KEY].SharedProps.Shortcut = Shortcut.CtrlC;
			InternalUltraToolbarsManager.Tools[CURRENT_PAGE_KEY].SharedProps.Shortcut = Shortcut.CtrlG;

			foreach (ToolBase tool in InternalUltraToolbarsManager.Tools)
				tool.SharedProps.StatusText = tool.SharedProps.AccessibleDescription;

			InternalUltraToolbarsManager.SaveSettings = AutoSaveSettings;
			InternalUltraToolbarsManager.SaveSettingsFormat = Infragistics.Win.SaveSettingsFormat.Xml;
			InternalUltraToolbarsManager.ShowShortcutsInToolTips = true;
			InternalUltraToolbarsManager.ShowFullMenusDelay = 100;
			InternalUltraToolbarsManager.ToolClick += new ToolClickEventHandler(OnInternalUltraToolbarsManagerToolClick);
		}

		private void InitUltraPrintPreviewControl()
		{
			InternalUltraPrintPreviewControl.Invalidated += new InvalidateEventHandler(OnInternalUltraPrintPreviewControlInvalidated);
			InternalUltraPrintPreviewControl.PreviewGenerated += new EventHandler(OnInternalUltraPrintPreviewControlPreviewGenerated);
		}

		private void GetInternalControls()
		{
			// Base의 총 Control
			// UltraPrintPreviewControl
			// Splitter
			// UltraPrintPreviewThumbnail
			// PreviewRowColumnSelector
			// UltraToolbarsDockArea.Left
			// UltraToolbarsDockArea.Right
			// UltraToolbarsDockArea.Top
			// UltraToolbarsDockArea.Bottom
			// UltraStatusBar
			foreach (Control control in base.Controls)
			{
				if (control is UltraToolbarsDockArea && InternalUltraToolbarsManager == null)
					InternalUltraToolbarsManager = (control as UltraToolbarsDockArea).ToolbarsManager;

				if (control is UltraPrintPreviewControl && InternalUltraPrintPreviewControl == null)
					InternalUltraPrintPreviewControl = (control as UltraPrintPreviewControl);

				if (control is UltraPrintPreviewThumbnail && InternalUltraPrintPreviewThumbnail == null)
					InternalUltraPrintPreviewThumbnail = (control as UltraPrintPreviewThumbnail);

				if (control is UltraStatusBar && InternalUltraStatusBar == null)
					InternalUltraStatusBar = (control as UltraStatusBar);
			}

			Type baseType = GetType().BaseType;
			FieldInfo pageSetupDialogField = baseType.GetField("pageSetupDialog1", BindingFlags.NonPublic | BindingFlags.CreateInstance | BindingFlags.Instance);

			if (pageSetupDialogField != null)
				InternalPageSetupDialog = pageSetupDialogField.GetValue(this) as PageSetupDialog;
		}
		#endregion

		#region Properties
		/// <summary>
		/// 내부적으로 사용한 UltraToolbarsManager을 구합니다.
		/// </summary>
		[Description("내부적으로 사용한 UltraToolbarsManager을 구합니다.")]
		[Category("Framework")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Browsable(false)]
		public UltraToolbarsManager InternalUltraToolbarsManager
		{
			get;
			private set;
		}

		/// <summary>
		/// 내부적으로 사용한 UltraPrintPreviewControl을 구합니다.
		/// </summary>
		[Description("내부적으로 사용한 UltraPrintPreviewControl을 구합니다.")]
		[Category("Framework")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Browsable(false)]
		public UltraPrintPreviewControl InternalUltraPrintPreviewControl
		{
			get;
			private set;
		}

		/// <summary>
		/// 내부적으로 사용한 UltraPrintPreviewThumbnail을 구합니다.
		/// </summary>
		[Description("내부적으로 사용한 UltraPrintPreviewThumbnail을 구합니다.")]
		[Category("Framework")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Browsable(false)]
		public UltraPrintPreviewThumbnail InternalUltraPrintPreviewThumbnail
		{
			get;
			private set;
		}

		/// <summary>
		/// 내부적으로 사용한 UltraPrintDocument을 구합니다.
		/// </summary>
		[Description("내부적으로 사용한 UltraPrintDocument을 구합니다.")]
		[Category("Framework")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Browsable(false)]
		public UltraPrintDocument InternalUltraPrintDocument
		{
			get;
			private set;
		}

		/// <summary>
		/// 내부적으로 사용한 UltraGridPrintDocument을 구합니다.
		/// </summary>
		[Description("내부적으로 사용한 UltraGridPrintDocument을 구합니다.")]
		[Category("Framework")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Browsable(false)]
		public UltraGridPrintDocument InternalUltraGridPrintDocument
		{
			get
			{
				return InternalUltraPrintDocument as UltraGridPrintDocument;
			}
		}

		/// <summary>
		/// 내부적으로 사용한 UltraStatusBar을 구합니다.
		/// </summary>
		[Description("내부적으로 사용한 UltraStatusBar을 구합니다.")]
		[Category("Framework")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public UltraStatusBar InternalUltraStatusBar
		{
			get;
			private set;
		}

		/// <summary>
		/// 내부적으로 사용한 PrintDialog을 구합니다.
		/// </summary>
		[Description("내부적으로 사용한 PrintDialog을 구합니다.")]
		[Category("Framework")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public PrintDialog InternalPrintDialog
		{
			get;
			private set;
		}

		/// <summary>
		/// 내부적으로 사용한 PageSetupDialog을 구합니다.
		/// </summary>
		[Description("내부적으로 사용한 PageSetupDialog을 구합니다.")]
		[Category("Framework")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public PageSetupDialog InternalPageSetupDialog
		{
			get;
			private set;
		}

		/// <summary>
		/// 미리 보기를 처리할 PrintDocument를 구하거나 설정합니다.
		/// </summary>
		[Description("미리 보기를 처리할 PrintDocument를 구하거나 설정합니다.")]
		[Category("Framework")]
		public new PrintDocument Document
		{
			get
			{
				return base.Document;
			}
			set
			{
				InitDocumentAssign(value);
				base.Document = value;
				InternalPrintDialog.Document = value;
				InternalPageSetupDialog.Document = value;
				InternalUltraPrintPreviewControl.Document = value;
			}
		}

		/// <summary>
		/// 미리 보기 처리할 Document용 Grid를 구하거나 설정합니다.
		/// </summary>
		[Description("미리 보기 처리할 Document용 Grid를 구하거나 설정합니다.")]
		[Category("Framework")]
		public UltraGrid DocumentGrid
		{
			get
			{
				return _documentGrid;
			}
			set
			{
				UltraGridPrintDocument ultraGridPrintDocument = InternalUltraPrintDocument as UltraGridPrintDocument;

				if (ultraGridPrintDocument != null)
				{
					_documentGrid = value;
					ultraGridPrintDocument.Grid = _documentGrid;
				}

				InternalUltraPrintDocument = ultraGridPrintDocument;
				Document = InternalUltraPrintDocument;
				InternalGridAutoFitWidthToOnePage = true;
			}
		}

		/// <summary>
		/// <seealso cref="InternalUltraPrintDocument"/>에서 사용된 페이지 머리말 속성을 구합니다.
		/// </summary>
		[Description("내부 UltraPrintDocument의 페이지 머리말 속성을 구합니다.")]
		[Category("Pages")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public HeaderFooterPageSection Header
		{
			get
			{
				return InternalUltraPrintDocument.Header;
			}
		}

		/// <summary>
		/// <seealso cref="InternalUltraPrintDocument"/>에서 사용된 페이지 꼬리말 속성을 구합니다.
		/// </summary>
		[Description("내부 UltraPrintDocument의 페이지 꼬리말 속성을 구합니다.")]
		[Category("Pages")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public HeaderFooterPageSection Footer
		{
			get
			{
				return InternalUltraPrintDocument.Footer;
			}
		}

		/// <summary>
		/// <seealso cref="InternalUltraPrintDocument"/>에서 사용된 페이지 본문 속성을 구합니다.
		/// </summary>
		[Description("내부 UltraPrintDocument의 페이지 본문 속성을 구합니다.")]
		[Category("Pages")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PageSection PageBody
		{
			get
			{
				return InternalUltraPrintDocument.PageBody;
			}
		}

		/// <summary>
		/// <seealso cref="InternalUltraPrintDocument"/>에서 사용된 페이지 속성을 구합니다.
		/// </summary>
		[Description("내부 UltraPrintDocument의 페이지 속성을 구합니다.")]
		[Category("Pages")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PageSection Page
		{
			get
			{
				return InternalUltraPrintDocument.Page;
			}
		}

		/// <summary>
		/// 문서의 출력 Margins을 1/10 mm 단위로 구하거나 설정합니다.(10 =&gt; 1mm)
		/// </summary>
		[Description("문서의 출력 Margins을 1/10 mm 단위로 구하거나 설정합니다.(10 => 1mm)")]
		[Category("Pages")]
		[DefaultValue(typeof(Margins), "190, 190, 250, 200")]
		public Margins PageMargins
		{
			get;
			set;
		}

		/// <summary>
		/// 설정을 자동적으로 저장할지 여부를 구하거나 설정합니다.
		/// </summary>
		[Category("Framework")]
		[DefaultValue(true)]
		[Description("설정을 자동적으로 저장할지 여부를 구하거나 설정합니다.")]
		public bool AutoSaveSettings
		{
			get
			{
				return _autoSaveSettings;
			}
			set
			{
				_autoSaveSettings = value;
				ResetSettings();
			}
		}

		/// <summary>
		/// 페이지를 가로 또는 세로 방향으로 인쇄할지 여부를 나타내는 값을 가져오거나 설정합니다.
		/// </summary>
		[Description("페이지를 가로 또는 세로 방향으로 인쇄할지 여부를 나타내는 값을 가져오거나 설정합니다.")]
		[Category("Print Preivew")]
		[DefaultValue(false)]
		public bool Landscape
		{
			get
			{
				return _landscape;
			}
			set
			{
				_landscape = value;

				if (Document != null && Document.DefaultPageSettings.Landscape != _landscape)
				{
					SuspendLayout();
					Document.DefaultPageSettings.Landscape = _landscape;
					InvalidatePreview();
					ResumeLayout();

					SwitchPageMargins();
					SwitchMargins();
				}

				(InternalUltraToolbarsManager.Tools[LANDSCAPE_KEY] as StateButtonTool).Checked = _landscape;
			}
		}

		/// <summary>
		/// 스크롤 시 페이지를 연속적으로 이어서 나타낼지, 한 페이지씩 나타낼지 여부를 구하거나 설정합니다.
		/// </summary>
		[Description("스크롤 시 페이지를 연속적으로 이어서 나타낼지, 한 페이지씩 나타낼지 여부를 구하거나 설정합니다.")]
		[Category("Print Preivew")]
		[DefaultValue(true)]
		public bool ShowContinuousPage
		{
			get
			{
				return _showContinuousPage;
			}
			set
			{
				_showContinuousPage = value;
				(InternalUltraToolbarsManager.Tools[CONTINUOUS_KEY] as StateButtonTool).Checked = _showContinuousPage;

				if (_showContinuousPage == true)
					InternalUltraPrintPreviewControl.Settings.Rows = 0;
				else
					InternalUltraPrintPreviewControl.Settings.Rows = Math.Max(1, InternalUltraPrintPreviewControl.Settings.Rows);
			}
		}

		/// <summary>
		/// 축소판을 왼쪽에 나타낼지 여부를 구하거나 설정합니다.
		/// </summary>
		[Description("축소판을 왼쪽에 나타낼지 여부를 구하거나 설정합니다.")]
		[Category("Print Preivew")]
		[DefaultValue(true)]
		public bool ShowThumbnails
		{
			get
			{
				return _showThumbnails;
			}
			set
			{
				_showThumbnails = value;
				(InternalUltraToolbarsManager.Tools[THUMBNAILS_KEY] as StateButtonTool).Checked = _showThumbnails;
			}
		}

		/// <summary>
		/// 프린트 미리 보기의 확대 비율을 구하거나 설정합니다.
		/// </summary>
		/// <remarks><seealso cref="ZoomMode"/>가 설정되면 입력값은 무시 됩니다.</remarks>
		[Category("Print Preivew")]
		[DefaultValue(1d)]
		[Description("프린트 미리 보기의 확대 비율을 구하거나 설정합니다.\r\nZoomMode가 설정되면 입력값은 무시 됩니다.")]
		public double Zoom
		{
			get
			{
				return _zoom;
			}
			set
			{
				_zoom = value;
				InternalUltraPrintPreviewControl.Settings.Zoom = _zoom;
			}
		}

		/// <summary>
		/// InternalUltraPrintPreviewControl.Zoom이 어떻게 계산될 지 모드를 구하거나 설정합니다.
		/// </summary>
		/// <return>InternalUltraPrintPreviewControl.Zoom이 어떻게 계산될 지 모드입니다. 기본값은 ZoomMode.AutoFlow입니다.</return>
		/// <remarks><seealso cref="Zoom"/>속성 보다 우선 합니다.</remarks>
		[Category("Print Preivew")]
		[DefaultValue(ZoomMode.AutoFlow)]
		[Description("InternalUltraPrintPreviewControl.Zoom이 어떻게 계산될 지 모드를 구하거나 설정합니다.\r\nZoom 속성 보다 우선 합니다.")]
		public ZoomMode ZoomMode
		{
			get
			{
				return _zoomMode;
			}
			set
			{
				_zoomMode = value;
				InternalUltraPrintPreviewControl.Settings.ZoomMode = _zoomMode;
			}
		}

		/// <summary>
		/// 런타임에 폼의 시작 위치를 가져오거나 설정합니다.
		/// </summary>
		/// <return>폼의 시작 위치를 나타내는 System.Windows.Forms.FormStartPosition입니다. 기본값은 FormStartPosition.CenterParent입니다.</return>
		[Description("런타임에 폼의 시작 위치를 가져오거나 설정합니다.")]
		[Category("레이아웃")]
		[DefaultValue(FormStartPosition.CenterParent)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public new FormStartPosition StartPosition
		{
			get
			{
				return base.StartPosition;
			}
			set
			{
				base.StartPosition = value;
			}
		}

		/// <summary>
		/// 폼의 창 상태를 가져오거나 설정합니다.
		/// </summary>
		/// <return>폼의 창 상태를 나타내는 System.Windows.Forms.FormWindowState입니다. 기본값은 FormWindowState.Maximized입니다.</return>
		[Description("폼의 창 상태를 가져오거나 설정합니다.")]
		[Category("레이아웃")]
		[DefaultValue(FormWindowState.Maximized)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public new FormWindowState WindowState
		{
			get
			{
				return base.WindowState;
			}
			set
			{
				base.WindowState = value;
			}
		}

		/// <summary>
		/// UltraPrintPreviewDialogHelper의 설정 파일 경로를 구하거나 설정합니다.
		/// </summary>
		[Category("Framework")]
		[DefaultValue(DEFAULT_SETTING_FILENAME)]
		[Description("UltraPrintPreviewDialogHelper의 설정 파일 경로를 구하거나 설정합니다.")]
		public string SettingFilePath
		{
			get
			{
				if (string.IsNullOrEmpty(_settingFilePath) == true)
					_settingFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), DEFAULT_SETTING_FILENAME);

				return _settingFilePath;
			}
			set
			{
				_settingFilePath = value;
			}
		}

		/// <summary>
		/// 미리 보기가 생성된 뒤 계산된 전체 페이지 숫자를 구합니다.
		/// </summary>
		[Browsable(false)]
		public int TotalNumberOfPages
		{
			get;
			private set;
		}

		/// <summary>
		/// <see cref="DocumentGrid"/>가 null 이 아닐 경우 출력 용지의 가로 크기에 맞춰서 
		/// 한 페이지에 전체 Grid가 출력되도록 페이지 폭을 자동 조절할지 여부를 구하거나 설정합니다.
		/// 내부적으로 <seealso cref="InternalGridFitWitdhToPages"/>를 1로 설정합니다.
		/// </summary>
		[Browsable(false)]
		[DefaultValue(true)]
		public bool InternalGridAutoFitWidthToOnePage
		{
			get
			{
				if (InternalUltraGridPrintDocument != null)
					return InternalUltraGridPrintDocument.FitWidthToPages == 1;

				return false;
			}
			set
			{
				if (InternalUltraGridPrintDocument != null)
					InternalUltraGridPrintDocument.FitWidthToPages = Convert.ToInt32(value);
			}
		}

		/// <summary>
		/// <see cref="DocumentGrid"/>가 null 이 아닐 경우 단일 논리 페이지 출력시 사용될 최대 페이지 시트 수를 구하거나 설정합니다.
		/// </summary>
		/// <seealso cref="InternalGridAutoFitWidthToOnePage"/>
		[Browsable(false)]
		[DefaultValue(1)]
		public int InternalGridFitWitdhToPages
		{
			get
			{
				return InternalUltraGridPrintDocument.FitWidthToPages;
			}
			set
			{
				if (InternalUltraGridPrintDocument != null)
					InternalUltraGridPrintDocument.FitWidthToPages = value;
			}
		}
		#endregion

		#region Unit Conversion
		/// <summary>
		/// 1/10 Millimeter 단위를 Inch 단위로 환산 합니다.
		/// </summary>
		/// <param name="millimeterValue">mm 값</param>
		/// <returns></returns>
		public static int ConvertMM2Inch(int millimeterValue)
		{
			return (int)Math.Round(millimeterValue * EXCHANGE_UNIT);
		}

		/// <summary>
		/// Inch 단위를 1/10 Millimeter 단위로 환산 합니다.
		/// </summary>
		/// <param name="inchValue">inch 값</param>
		/// <returns></returns>
		public static int ConvertInch2MM(int inchValue)
		{
			return (int)Math.Round(inchValue / EXCHANGE_UNIT);
		}
		#endregion

		#region Publics
		/// <summary>
		/// UltraPrintPreviewDialogHelper의 설정을 저장합니다.
		/// </summary>
		public void SaveSettings()
		{
			SaveUltraToolbarsManager();
		}

		/// <summary>
		/// UltraPrintPreviewDialogHelper의 저장된 설정을 초기화합니다.
		/// </summary>
		public void ResetSettings()
		{
			try
			{
				if (File.Exists(SettingFilePath) == true)
					File.Delete(SettingFilePath);
			}
			catch
			{
			}
		}

		/// <summary>
		/// PageMarings을 기본값을 다시 설정합니다.
		/// </summary>
		public void ResetPageMargins()
		{
			if (Landscape == true)
				PageMargins = new Margins(250, 200, 190, 190);
			else
				PageMargins = new Margins(190, 190, 250, 200);
		}

		/// <summary>
		/// 미리 보기의 머리말 꼬리말의 문자열을 지정된 pseudo에 따라서 세팅합니다.
		/// </summary>
		/// <param name="pseudo">사용할 수 있는 의사어에서는<br/>
		/// $CurrentPage$ : 현재 페이지<br/>
		/// $TotalPage$   : 전체 페이지 수<br/>
		/// $UserName$    : 시스템에 로그인한 사용자 이름<br/>
		/// $PrintedDate$ : 출력되는 일자<br/>
		/// $PrintedTime$ : 출력되는 시간<br/>
		/// </param>
		/// <param name="align">해당 pseudo keyword가 parsing되어서 나타낼 위치를 나타냅니다.</param>
		/// <seealso cref="Infragistics.Win.Printing.HeaderFooterPageSection.TextLeft"/>
		public void SetHeaderAndFooterText(string pseudo, HeaderFooterAlign align)
		{
			if (_headerFooterText.ContainsKey(align) == false)
				_headerFooterText.Add(align, pseudo);
			else
				_headerFooterText[align] = pseudo;

			ParsingHeaderAndFooterKeywords();
		}

		/// <summary>
		/// 미리 보기 닫기 툴을 표준 도구의 가장 처음으로 이동시킵니다.
		/// </summary>
		public void SetClosePreviewToolInFirst()
		{
			UltraToolbar standardTools = InternalUltraToolbarsManager.Toolbars[STANDARD_TOOLBAR_KEY];
			const string CLOSE_PREVIEW_KEY = "ClosePreview";
			int closePreviewIndex = standardTools.Tools.IndexOf(CLOSE_PREVIEW_KEY);

			ToolBase closePreviewTool = standardTools.Tools[closePreviewIndex];
			standardTools.Tools.RemoveAt(closePreviewIndex);
			standardTools.Tools.InsertTool(0, CLOSE_PREVIEW_KEY);
		}

		/// <summary>
		/// 설정된 UltraPrintDocument의 설정에 따라서 바로 출력 합니다.
		/// </summary>
		public void Print()
		{
			InternalUltraPrintDocument.Print();
		}

		/// <summary>
		/// 설정된 UltraPrintDocument의 설정에 따라서 프린트 대화상자를 보여줍니다.
		/// </summary>
		public void PrintAtPrintDialog()
		{
			InternalPrintDialog.ShowDialog(Owner);
		}
		#endregion

		#region Protectedes
		/// <summary>
		/// OnUltraPrintPreviewDialogHelper가 로드될 때 처리할 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnUltraPrintPreviewDialogHelperLoad(object sender, EventArgs e)
		{
			// 문서 할당 이후 Margins나 Landscape 상태가 변경될 수 있으므로 초기화시에 재할당 해준다.
			InitDocumentAssign(Document);

			if (AutoSaveSettings == true)
			{
				if (File.Exists(SettingFilePath) == true)
					InternalUltraToolbarsManager.LoadFromXml(SettingFilePath);
			}

			if (ShowThumbnails == false)
			{
				SuspendLayout();
				ThumbnailAreaVisible = ShowThumbnails;
				ResumeLayout();
			}

			// Checked 상태가 True 이면 기능도 True 만든다.
			ShowContinuousPage = ShowContinuousPage;
		}

		/// <summary>
		/// OnUltraPrintPreviewDialogHelper가 닫힐 때 처리할 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnUltraPrintPreviewDialogHelperFormClosing(object sender, FormClosingEventArgs e)
		{
			if (AutoSaveSettings == true)
				SaveUltraToolbarsManager();
		}

		/// <summary>
		/// Toolbar의 Print 버튼을 누른 후 프린트 작업이 일어나기 전에 처리할 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPrinting(object sender, PrintingEventArgs e)
		{
			InternalPrintDialog.Document = Document;
			e.Cancel = InternalPrintDialog.ShowDialog() == DialogResult.Cancel;
		}

		/// <summary>
		/// 페이지 설정 등을 통해서 Invalidate 되었을 때 처리할 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnInternalUltraPrintPreviewControlInvalidated(object sender, InvalidateEventArgs e)
		{
			// 페이지 설정 등을 통해서 Invalidate 되었을 때 Landscape의 Checked 상태를 Update 합니다.
			if (InternalUltraToolbarsManager.Tools.Contains(LANDSCAPE_KEY) == true)
				(InternalUltraToolbarsManager.Tools[LANDSCAPE_KEY] as StateButtonTool).Checked = Document.DefaultPageSettings.Landscape;
		}

		/// <summary>
		/// 내부 UltraPrintPreviewControl의 Preview가 생성되면 처리할 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnInternalUltraPrintPreviewControlPreviewGenerated(object sender, EventArgs e)
		{
			TotalNumberOfPages = InternalUltraPrintPreviewControl.PageCount;
		}

		/// <summary>
		/// Toolbar의 툴이 클릭하면 처리할 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnInternalUltraToolbarsManagerToolClick(object sender, ToolClickEventArgs e)
		{
			switch (e.Tool.Key)
			{
				case LANDSCAPE_KEY:
					Landscape = ((StateButtonTool)e.Tool).Checked == true;

					break;
			}
		}
		#endregion

		#region Privates
		/// <summary>
		/// <seealso cref="PageMargins"/>로 설정된 mm 단위의 여백을 inch 단위로 변경합니다.
		/// </summary>
		/// <returns></returns>
		private Margins ConvertPageMargins2Inch()
		{
			return new Margins
			{
				Left = ConvertMM2Inch(PageMargins.Left),
				Right = ConvertMM2Inch(PageMargins.Right),
				Top = ConvertMM2Inch(PageMargins.Top),
				Bottom = ConvertMM2Inch(PageMargins.Bottom)
			};
		}

		private void SwitchPageMargins()
		{
			if (PageMargins != null)
			{
				Margins before = new Margins();

				before = PageMargins.Clone() as Margins;

				PageMargins = new Margins(before.Top, before.Bottom, before.Left, before.Right);
			}
		}

		private void SwitchMargins()
		{
			if (Document != null)
			{
				Margins before = new Margins();

				before = Document.DefaultPageSettings.Margins.Clone() as Margins;

				Document.DefaultPageSettings.Margins = new Margins(before.Top, before.Bottom, before.Left, before.Right);
			}
		}

		private void SaveUltraToolbarsManager()
		{
			InternalUltraToolbarsManager.SaveAsXml(SettingFilePath, true);
		}

		private void InitDocumentAssign(PrintDocument document)
		{
			if (document != null)
			{
				if (PageMargins != null)
					document.DefaultPageSettings.Margins = ConvertPageMargins2Inch();

				document.DefaultPageSettings.Landscape = Landscape;
			}
		}

		private void ParsingHeaderAndFooterKeywords()
		{
			foreach (HeaderFooterAlign align in _headerFooterText.Keys)
			{
				string text = ParsingKeywords(_headerFooterText[align]);

				if (string.IsNullOrEmpty(text) == true)
					continue;

				switch (align)
				{
					case HeaderFooterAlign.HeaderLeft:
						Header.TextLeft = text;

						break;
					case HeaderFooterAlign.HeaderCenter:
						Header.TextCenter = text;

						break;
					case HeaderFooterAlign.HeaderRight:
						Header.TextRight = text;

						break;
					case HeaderFooterAlign.FooterLeft:
						Footer.TextLeft = text;

						break;
					case HeaderFooterAlign.FooterCenter:
						Footer.TextCenter = text;

						break;
					case HeaderFooterAlign.FooterRight:
						Footer.TextRight = text;

						break;
				}
			}
		}

		private string ParsingKeywords(string keywords)
		{
			if (string.IsNullOrEmpty(keywords) == true)
				return string.Empty;

			return keywords.Replace("$CurrentPage$", "[Page #]")
				.Replace("$TotalPage$", "<##>")
				.Replace("$UserName$", "[User Name]")
				.Replace("$PrintedDate$", "[Date Printed]")
				.Replace("$PrintedTime$", "[Time Printed]");
		}
		#endregion
	}
}
