﻿namespace AnyFactory.FX.Win.Controls.Print
{
	partial class UltraPrintPreviewDialogHelper
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UltraPrintPreviewDialogHelper));
			this.SuspendLayout();
			// 
			// UltraPrintPreviewDialogHelper
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "UltraPrintPreviewDialogHelper";
			this.PreviewSettings.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
			this.PreviewSettings.MousePanning = Infragistics.Win.MousePanning.Both;
			this.PreviewSettings.ScrollBarLook.ViewStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.WindowsVista;
			this.PreviewSettings.ScrollTipStyle = Infragistics.Win.UltraWinScrollBar.ScrollTipStyle.Show;
			this.PreviewSettings.ZoomMode = Infragistics.Win.Printing.ZoomMode.AutoFlow;
			this.Style = Infragistics.Win.UltraWinToolbars.ToolbarStyle.Office2007;
			this.ThumbnailSettings.MousePanning = Infragistics.Win.MousePanning.Both;
			this.ThumbnailSettings.ScrollBarLook.ViewStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.WindowsVista;
			this.UseAntiAlias = true;
			this.ResumeLayout(false);

		}

		#endregion


	}
}