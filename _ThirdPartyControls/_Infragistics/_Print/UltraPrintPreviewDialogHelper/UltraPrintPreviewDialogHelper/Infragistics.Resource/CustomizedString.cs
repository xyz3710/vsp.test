﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Controls.InfragisticsResource.CustomizedString
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 12월 18일 금요일 오전 11:26
/*	Purpose		:	Infragistics Control의 Customized 된 문자열을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 12월 18일 금요일 오전 11:33
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Infragistics.Shared;

namespace AnyFactory.FX.Win.Controls.InfragisticsResource
{
	/// <summary>
	/// Infragistics Control의 Customized 된 문자열을 제공합니다.
	/// </summary>
	public class CustomizedString
	{
		/// <summary>
		/// UltraWinPrinting의 리소스 문자열을 국가 언어 옵션에 맞게 세팅 합니다.
		/// </summary>
		public static void SetUltraPrintingResourceStringToLocalization()
		{
			ResourceCustomizer rcPrinting = Infragistics.Win.Printing.Resources.Customizer;

			switch (Thread.CurrentThread.CurrentUICulture.Name)
			{
				case "ko":
				case "ko-KR":
					rcPrinting.SetCustomizedString("AccessibleDescription_Preview", "미리 보기 영역");
					rcPrinting.SetCustomizedString("AccessibleDescription_StatusBar", "상태바");
					rcPrinting.SetCustomizedString("AccessibleDescription_Thumbnail", "축소판 영역");
					rcPrinting.SetCustomizedString("AccessibleName_Preview", "미리 보기");
					rcPrinting.SetCustomizedString("AccessibleName_StatusBar", "상태바");
					rcPrinting.SetCustomizedString("AccessibleName_Thumbnail", "축소판 영역");
					rcPrinting.SetCustomizedString("CustomizeImg_ToolBar_MenuBar", "메뉴");
					rcPrinting.SetCustomizedString("CustomizeImg_ToolBar_Standard", "표준");
					rcPrinting.SetCustomizedString("CustomizeImg_ToolBar_View", "보기");
					rcPrinting.SetCustomizedString("PreviewRowColSelection_Cancel", "취소");
					rcPrinting.SetCustomizedString("PreviewRowColSelection_SelectedPages", "{0} x {1} 페이지");
					rcPrinting.SetCustomizedString("PrintPreview_DialogCaption", "인쇄 미리 보기");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_ClosePreview", "닫기(&C)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_ClosePreview_AccessibleDescription", "인쇄 미리 보기 대화상자를 닫습니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_ContextMenuPreviewHand", "이동 상황 메뉴");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_ContextMenuPreviewZoom", "확대 상황 메뉴");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_ContextMenuThumbnail", "축소판 상황 메뉴");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Continuous", "연속 보기");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Continuous_AccessibleDescription", "스크롤 시 페이지를 연속적으로 나타냅니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Current_Page", "현재 페이지");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Current_Page_AccessibleDescription", "인쇄 미리 보기에 나타나는 현재 페이지");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Dynamic_Zoom_Tool", "동적 확대 도구(&D)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Dynamic_Zoom_Tool_AccessibleDescription", "마우스를 사용하여 확대 축소를 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Enlarge_Page_Thumbnails", "축소판 확장");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Enlarge_Page_Thumbnails_AccessibleDescription", "축소판을 확장 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Exit", "나가기(&X)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Exit_AccessibleDescription", "인쇄 미리 보기 대화상자를 닫습니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_File", "파일(&F)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_File_AccessibleDescription", "파일 메뉴");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_First_Page", "첫 페이지(&F)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_First_Page_AccessibleDescription", "미리 보기에서 첫번째 페이지를 보여줍니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Go_To", "이동(&G)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Go_To_AccessibleDescription", "미리 보기에서 다른 페이지로 이동 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Hand_Tool", "이동 도구(&H)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Hand_Tool_AccessibleDescription", "미리 보기에서 마우스로 스크롤 하도록 허용합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Last_Page", "마지막 페이지(&L)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Last_Page_AccessibleDescription", "미리 보기에서 마지막 페이지를 보여줍니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Margin_Width", "여백 폭(&M)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Margin_Width_AccessibleDescription", "여백의 가장 넓은 폭에 맞춰서 미리 보기를 확대 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Next_Page", "다음 페이지(&N)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Next_Page_AccessibleDescription", "미리 보기에서 다음 페이지를 보여줍니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Next_View", "다음 보기(&T)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Next_View_AccessibleDescription", "보기 이력 중 다음 보기를 보여 줍니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Page_Layout", "페이지 배치(&L)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Page_Layout_AccessibleDescription", "인쇄 미리 보기에서 페이지 배치를 변경합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Page_Setup", "페이지 설정(&U)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Page_Setup_AccessibleDescription", "페이지 설정 대화 상자를 보여줍니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Page_Width", "페이지 폭(&A)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Page_Width_AccessibleDescription", "페이지의 폭에 맞춰서 미리 보기를 확대 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Previous_Page", "이전 페이지(&P)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Previous_Page_AccessibleDescription", "미리 보기에서 이전 페이지를 보여줍니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Previous_View", "이전 보기(&S)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Previous_View_AccessibleDescription", "보기 이력 중 이전 보기를 보여 줍니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Print", "인쇄(&P)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Print_AccessibleDescription", "문서를 인쇄 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Reduce_Page_Thumbnails", "축소판 축소");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Reduce_Page_Thumbnails_AccessibleDescription", "축소판을 축소 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Show_Page_Numbers", "축소판 페이지 번호 보이기");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Show_Page_Numbers_AccessibleDescription", "축소판 페이지 번호를 보여 줍니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Snapshot_Tool", "스냅 사진 도구(&S)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Snapshot_Tool_AccessibleDescription", "미리 보기의 스냅 사진을 찍은 뒤 클립보드로 복사 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Thumbnails", "축소판(&T)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Thumbnails_AccessibleDescription", "각 페이지의 축소판을 보여줍니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Tools", "도구(&T)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Tools_AccessibleDescription", "도구 메뉴");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_View", "보기(&V)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_View_AccessibleDescription", "보기 메뉴");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Whole_Page", "전체 페이지(&W)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Whole_Page_AccessibleDescription", "전체 페이제에 맞춰서 미리 보기를 확대 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom", "확대(&Z):");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom_AccessibleDescription", "미리 보기의 확대 수준을 설정합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom_In", "확대(Ctrl+Num&+)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom_In_AccessibleDescription", "미리 보기를 확대 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom_In_Tool", "확대 도구(&I)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom_In_Tool_AccessibleDescription", "미리 보기에서 마우스로 확대 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom_Increments", "확대 증가");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom_Out", "축소(Ctrl+Num&-)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom_Out_AccessibleDescription", "미리 보기를 축소 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom_Out_Tool", "축소 도구(&O)");
					rcPrinting.SetCustomizedString("PrintPreview_Tool_Zoom_Out_Tool_AccessibleDescription", "미리 보기에서 마우스로 축소 합니다.");
					rcPrinting.SetCustomizedString("PrintPreview_ToolCategory_Context_Menus", "상황 메뉴");
					rcPrinting.SetCustomizedString("PrintPreview_ToolCategory_File", "파일 관련 메뉴");
					rcPrinting.SetCustomizedString("PrintPreview_ToolCategory_Menus", "메뉴 관련 도구");
					rcPrinting.SetCustomizedString("PrintPreview_ToolCategory_Tools", "표준 관련 도구");
					rcPrinting.SetCustomizedString("PrintPreview_ToolCategory_View", "보기 관련 도구");
					rcPrinting.SetCustomizedString("PrintPreview_ToolCategory_Zoom_Mode", "확대 모드 관련 도구");
					rcPrinting.SetCustomizedString("PrintPreview_ToolTip_ClosePreview", "미리 보기를 닫습니다.");
					rcPrinting.SetCustomizedString("PrintPreview_ToolTip_Zoom", "확대");
					rcPrinting.SetCustomizedString("PrintPreview_ZoomListItem_MarginWidth", "여백 폭(&M)");
					rcPrinting.SetCustomizedString("PrintPreview_ZoomListItem_PageWidth", "페이지 폭(&A)");
					rcPrinting.SetCustomizedString("PrintPreview_ZoomListItem_WholePage", "전체 페이지(&W)");
					rcPrinting.SetCustomizedString("StatusBar_DynamicZoom_Instructions", "클릭 후 위로 드래그 하면 확대 되고 아래로 드래그 하면 축소 됩니다.");
					rcPrinting.SetCustomizedString("StatusBar_Hand_Instructions", "클릭 후 드래그 하면 페이지 내용이 스크롤 됩니다.");
					rcPrinting.SetCustomizedString("StatusBar_Page_X_OF_X", "페이지: {0} of {1}");
					rcPrinting.SetCustomizedString("StatusBar_SnapShot_Instructions", "클릭 후 사각형으로 드래그 하면 선택 영역을 클립 보드로 복사하거나 클릭 한번만 하면 전체 영역을 복사합니다.");
					rcPrinting.SetCustomizedString("StatusBar_ZoomIn_Instructions", "클릭 후 사각형으로 드래그 하면 선택 영역을 확대 하거나 클릭 한번만 하면 확대 비율를 증가 시킵니다.");
					rcPrinting.SetCustomizedString("StatusBar_ZoomOut_Instructions", "클릭 후 사각형으로 드래그 하면 선택 영역을 축소 하거나 클릭 한번만 하면 축소 비율를 증가 시킵니다.");
					rcPrinting.SetCustomizedString("UltraPrintPreviewDialogActionList_P_Document_DisplayName", "문서 인쇄");
					rcPrinting.SetCustomizedString("UltraPrintPreviewDialogActionList_P_Style_DisplayName", "형태");

					break;
				default:
					rcPrinting.ResetAllCustomizedStrings();

					break;
			}
		}

		/// <summary>
		/// UltraWinToolbars의 리소스 문자열을 국가 언어 옵션에 맞게 세팅 합니다.
		/// </summary>
		public static void SetUltraWinToolbarsResourceStringToLocalization()
		{
			ResourceCustomizer rcToolbarsManager = Infragistics.Win.UltraWinToolbars.Resources.Customizer;

			switch (Thread.CurrentThread.CurrentUICulture.Name)
			{
				case "ko":
				case "ko-KR":
					rcToolbarsManager.SetCustomizedString("AddRemoveButtons", "버튼 추가/제거(&A)");
					rcToolbarsManager.SetCustomizedString("QuickCustomizeToolTipXP", "툴바 옵션");
					rcToolbarsManager.SetCustomizedString("Customize", "사용자 지정(&C)");
					rcToolbarsManager.SetCustomizedString("CustomizeCategoryAllCommands", "_모든 명령_");
					rcToolbarsManager.SetCustomizedString("CustomizeCategoryUnassigned", "할당되지 않음");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_Toolbars", "툴바");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_ToolbarsList", "툴바(&A)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_Commands", "명령");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_CommandsEndAmp", "명령(&D)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_CategoriesMidAmp", "분류(&D):");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_CustomizeMain", "사용자 지정");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_CloseNoAmp", "닫기");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_KeyboardBeginAmp", "단축키…(&K)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_Delete", "삭제…(&D)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_New", "신규…(&N)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_RenameAmp", "이름 변경…(&E)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_ResetAmp", "초기화…(&R)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_ModifySelection", "선택 수정");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_RearrangeCommands", "명령 재배치…(&R)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_Options", "옵션");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_PersonalizedMenusAndToolbars", "개인화된 메뉴와 툴");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_AlwaysShowFullMenus", "항상 전체 메뉴 보이기(&N)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_ShowFullMenusAfterAShortDelay", "약간의 지연뒤에 모든 메뉴 보이기");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_ResetMyUsageData", "데이터 초기화(&R)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_Other", "기타");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_LargeIconsOnMenus", "메뉴에 큰 아이콘(&L)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_LargeIconsOnToolbars", "툴바에 큰 아이콘(&A)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_ListFontNamesInTheirFont", "자신의 글꼴 목록의 글꼴 이름");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_ShowScreenTipsOnToolbars", "툴바의 풍선 도움말 보이기(&T)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_ShowShortcutKeysInScreenTips", "풍선 도움말에 단축키 보이기(&H)");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_FloatingToolbarFadeDelay", "부동 툴바 사라지는 지연 시간");
					rcToolbarsManager.SetCustomizedString("CustomizeDialog_MenuAnimations", "메뉴 에니메이션(&M):");
					rcToolbarsManager.SetCustomizedString("CustomizeMenuAnimationFade", "사라짐");
					rcToolbarsManager.SetCustomizedString("CustomizeMenuAnimationNone", "없음");
					rcToolbarsManager.SetCustomizedString("CustomizeMenuAnimationRandom", "무작위");
					rcToolbarsManager.SetCustomizedString("CustomizeMenuAnimationSlide", "미끄러짐");
					rcToolbarsManager.SetCustomizedString("CustomizeMenuAnimationUnfold", "펼쳐짐");
					rcToolbarsManager.SetCustomizedString("CustomizeNewToolbarTitle", "신규 툴바");
					rcToolbarsManager.SetCustomizedString("CustomDlgNewToolBarCancel", "취소");
					rcToolbarsManager.SetCustomizedString("CustomDlgNewToolBarLocation", "위치:");
					rcToolbarsManager.SetCustomizedString("CustomDlgNewToolBarTBName", "툴바 이름(&T):");
					rcToolbarsManager.SetCustomizedString("CustomDlgNewToolBarTBOK", "확인");
					rcToolbarsManager.SetCustomizedString("CustomizeRenameToolbarTitle", "툴바 이름 변경");
					rcToolbarsManager.SetCustomizedString("CustomDlgRenameToolBarTBName", "툴바 이름(&T):");
					rcToolbarsManager.SetCustomizedString("CustomizeConfirmToolbarDelete", "{0}' 툴바를 삭제하시겠습니까?");
					rcToolbarsManager.SetCustomizedString("CustomizeConfirmToolbarDeleteTitle", "툴바 삭제 확인");
					rcToolbarsManager.SetCustomizedString("CustomizeConfirmToolbarReset", "{0}'의 모든 사용자 정의 역할을 초기화 하시겠습니까?");
					rcToolbarsManager.SetCustomizedString("CustomizeConfirmToolbarResetTitle", "툴바 초기화 확인");
					rcToolbarsManager.SetCustomizedString("CustomizeConfirmResetUsageData", "응용 프로그램에서 사용하고 기본 표시 명령의 메뉴 및 도구 모음 설정을 복원 명령의 기록이 삭제됩니다. 그것은 어떠한 명시적 사용자 지정이 취소되지 않습니다. 초기화 하시겠습니까?");
					rcToolbarsManager.SetCustomizedString("CustomizeConfirmResetUsageDataTitle", "사용자 데이터 초기화");
					rcToolbarsManager.SetCustomizedString("CustomizeLocationDockedBottom", "하단에 고정");
					rcToolbarsManager.SetCustomizedString("CustomizeLocationDockedLeft", "좌측에 고정");
					rcToolbarsManager.SetCustomizedString("CustomizeLocationDockedRight", "우측에 고정");
					rcToolbarsManager.SetCustomizedString("CustomizeLocationDockedTop", "상단에 고정");
					rcToolbarsManager.SetCustomizedString("CustomizeLocationFloating", "떠다님");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdTitle", "키보드 사용자 정의");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdCat", "분류(&C):");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdCmd", "명령(&O):");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdSpecCmd", "명령 지정");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdSpecShrtcut", "단축키 지정(&F)");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgAddCommand_Categories", "분류:");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgAddCommand_Commands", "명령:");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgAddCommand_OK", "확인");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgAddCommand_Title", "명령 추가");
					rcToolbarsManager.SetCustomizedString("CustomizeKeyboardShortcutAssignedLiteral", "현재 할당되어 있음: {0}");
					rcToolbarsManager.SetCustomizedString("CustomizeKeyboardShortcutUnassignedLiteral", "[할당되지 않음]");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdDesc", "설명");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdAssign", "할당(&A)");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdAssign_AccessibleDescription", "");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdDesc", "설명");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdRemove&Remove", "제거(&R)");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgKbdResetAll", "초기화…(&S)");
					rcToolbarsManager.SetCustomizedString("ToolbarCloseButtonToolTip", "닫기");
					rcToolbarsManager.SetCustomizedString("CustomizeConfirmShortcutRemoval", "사용자 지정 단축키를 초기화 하시겠습니까?");
					rcToolbarsManager.SetCustomizedString("CustomizeConfirmShortcutRemovalTitle", "사용자 지정 단축키 초기화");
					rcToolbarsManager.SetCustomizedString("CustomizeKeyboardTitle", "키보드 사용자 정의");
					rcToolbarsManager.SetCustomizedString("DefaultAction_Popup_Closed", "열기");
					rcToolbarsManager.SetCustomizedString("DefaultAction_Popup_Opened", "닫기");
					rcToolbarsManager.SetCustomizedString("CustomizeDlgRearrangeCommands_Close", "닫기");
					rcToolbarsManager.SetCustomizedString("LockToolbars", "툴바 잠그기(&L)");

					break;
				default:
					rcToolbarsManager.ResetAllCustomizedStrings();

					break;
			}
		}

		/// <summary>
		/// UltraWinGrid의 리소스 문자열을 국가 언어 옵션에 맞게 세팅 합니다.
		/// </summary>
		public static void SetUltraWinGridResourceStringToLocalization()
		{
			ResourceCustomizer rcWinGrid = Infragistics.Win.UltraWinGrid.Resources.Customizer;

			switch (Thread.CurrentThread.CurrentUICulture.Name)
			{
				case "ko":
				case "ko-KR":
					rcWinGrid.SetCustomizedString("RowFilterDialogBlanksItem", "(공백)");
					rcWinGrid.SetCustomizedString("RowFilterDialogDBNullItem", "(값 없음)");
					rcWinGrid.SetCustomizedString("RowFilterDialogEmptyTextItem", "(빈 텍스트)");
					rcWinGrid.SetCustomizedString("RowFilterDialogOperandHeaderCaption", "피연산자");
					rcWinGrid.SetCustomizedString("RowFilterDialogOperatorHeaderCaption", "연산자");
					rcWinGrid.SetCustomizedString("RowFilterDialogTitlePrefix", "사용자 필터 정의_");

					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_Contains", "~를 포함하는");					// Contains
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_DoesNotContain", "~를 포함하지 않는");		// Does not contain
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_DoesNotEndWith", "~로 끝나지 않는");			// Does not end with
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_DoesNotMatch", "일치하지 않는 정규식 표현");	// Does not match
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_DoesNotStartWith", "~로 시작하지 않는");		// Does not start with
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_EndsWith", "~로 끝나는");						// Ends with
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_Equals", "=");
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_GreaterThan", ">");
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_GreaterThanOrEqualTo", ">=");
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_LessThan", "<");
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_LessThanOrEqualTo", "<=");
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_Like", "비슷한");								// Like
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_Match", "일치하는 정규식 표현");				// Match
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_NotEquals", "!=");
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_NotLike", "비슷하지 않는");					// Not Like
					rcWinGrid.SetCustomizedString("RowFilterDropDown_Operator_StartsWith", "~로 시작하는");					// Starts with

					rcWinGrid.SetCustomizedString("RowFilterDropDownAllItem", "(모두)");
					rcWinGrid.SetCustomizedString("RowFilterDropDownBlanksItem", "(공백)");
					rcWinGrid.SetCustomizedString("RowFilterDropDownCustomItem", "(사용자 정의)");
					rcWinGrid.SetCustomizedString("RowFilterDropDownEquals", "같다");
					rcWinGrid.SetCustomizedString("RowFilterDropDownErrorsItem", "(오류)");
					rcWinGrid.SetCustomizedString("RowFilterDropDownGreaterThan", "~ 보다 크다");
					rcWinGrid.SetCustomizedString("RowFilterDropDownGreaterThanOrEqualTo", "~ 보다 크거나 같다");
					rcWinGrid.SetCustomizedString("RowFilterDropDownLessThan", "~ 보다 작다");
					rcWinGrid.SetCustomizedString("RowFilterDropDownLessThanOrEqualTo", "~ 보다 작거나 같다");
					rcWinGrid.SetCustomizedString("RowFilterDropDownLike", "비슷한");										// Like
					rcWinGrid.SetCustomizedString("RowFilterDropDownMatch", "일치하는 정규식 표현");
					rcWinGrid.SetCustomizedString("RowFilterDropDownNonBlanksItem", "(값 있음)");
					rcWinGrid.SetCustomizedString("RowFilterDropDownNonErrorsItem", "(에러 없음)");
					rcWinGrid.SetCustomizedString("RowFilterDropDownNotEquals", "같지 않은");								// Not Like
					rcWinGrid.SetCustomizedString("RowFilterLogicalOperator_And", "그리고");								// AND
					rcWinGrid.SetCustomizedString("RowFilterLogicalOperator_Or", "또는");									// OR

					rcWinGrid.SetCustomizedString("FilterDialogAddConditionButtonText", "조건 추가(&A)");
					rcWinGrid.SetCustomizedString("FilterDialogAndRadioText", "And 조건");
					rcWinGrid.SetCustomizedString("FilterDialogCancelButtonText", "취소(&C)");
					rcWinGrid.SetCustomizedString("FilterDialogDeleteButtonText", "조건 삭제(&D)");
					rcWinGrid.SetCustomizedString("FilterDialogOkButtonNoFiltersText", "필터 없음(&O)");
					rcWinGrid.SetCustomizedString("FilterDialogOkButtonText", "확인(&O)");
					rcWinGrid.SetCustomizedString("FilterDialogOrRadioText", "Or 조건");
					break;
				default:
					rcWinGrid.ResetAllCustomizedStrings();

					break;
			}
		}
	}
}
