﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Controls.Print.HeaderFooterAlign
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 12월 16일 수요일 오후 5:21
/*	Purpose		:	머리말과 꼬리말의 위치를 구별하기 위한 열거자 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 12월 16일 수요일 오후 8:50
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AnyFactory.FX.Win.Controls.Print
{
	/// <summary>
	/// 머리말과 꼬리말의 위치를 구별하기 위한 열거자 입니다.
	/// </summary>
	public enum HeaderFooterAlign
	{
		/// <summary>
		/// Header Left position
		/// </summary>
		HeaderLeft,
		/// <summary>
		/// Header Center position
		/// </summary>
		HeaderCenter,
		/// <summary>
		/// Header Right position
		/// </summary>
		HeaderRight,
		/// <summary>
		/// Footer Left position
		/// </summary>
		FooterLeft,
		/// <summary>
		/// Footer Center position
		/// </summary>
		FooterCenter,
		/// <summary>
		/// Footer Right position
		/// </summary>
		FooterRight,
	}
}
