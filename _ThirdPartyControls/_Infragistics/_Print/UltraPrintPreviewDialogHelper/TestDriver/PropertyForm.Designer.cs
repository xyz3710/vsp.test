﻿namespace TestDriver
{
	partial class PropertyForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.MainPropertyGrid = new System.Windows.Forms.PropertyGrid();
			this.SuspendLayout();
			// 
			// MainPropertyGrid
			// 
			this.MainPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MainPropertyGrid.Location = new System.Drawing.Point(0, 0);
			this.MainPropertyGrid.Name = "MainPropertyGrid";
			this.MainPropertyGrid.Size = new System.Drawing.Size(384, 644);
			this.MainPropertyGrid.TabIndex = 0;
			// 
			// PropertyForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(384, 644);
			this.Controls.Add(this.MainPropertyGrid);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "PropertyForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "PropertyForm";
			this.TopMost = true;
			this.ResumeLayout(false);

		}

		#endregion

		public System.Windows.Forms.PropertyGrid MainPropertyGrid;

	}
}