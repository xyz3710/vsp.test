﻿namespace TestDriver
{
	partial class TestDriverForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			this.ultraPrintPreviewDialog = new Infragistics.Win.Printing.UltraPrintPreviewDialog(this.components);
			this.ultraGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.btnPreview = new System.Windows.Forms.Button();
			this.ultraGridPrintDocument = new Infragistics.Win.UltraWinGrid.UltraGridPrintDocument(this.components);
			this.btnPreview2 = new System.Windows.Forms.Button();
			this.btnPreviewCode = new System.Windows.Forms.Button();
			this.ultraPrintPreviewDialogHelper = new AnyFactory.FX.Win.Controls.Print.UltraPrintPreviewDialogHelper();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.btnPrintCode = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.ultraGrid)).BeginInit();
			this.tlpMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// ultraPrintPreviewDialog
			// 
			this.ultraPrintPreviewDialog.Name = "ultraPrintPreviewDialog";
			// 
			// ultraGrid
			// 
			this.tlpMain.SetColumnSpan(this.ultraGrid, 2);
			appearance4.BackColor = System.Drawing.SystemColors.Window;
			appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
			this.ultraGrid.DisplayLayout.Appearance = appearance4;
			this.ultraGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.ultraGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance1.BorderColor = System.Drawing.SystemColors.Window;
			this.ultraGrid.DisplayLayout.GroupByBox.Appearance = appearance1;
			appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
			this.ultraGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
			this.ultraGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance3.BackColor2 = System.Drawing.SystemColors.Control;
			appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
			this.ultraGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
			this.ultraGrid.DisplayLayout.MaxColScrollRegions = 1;
			this.ultraGrid.DisplayLayout.MaxRowScrollRegions = 1;
			appearance12.BackColor = System.Drawing.SystemColors.Window;
			appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
			this.ultraGrid.DisplayLayout.Override.ActiveCellAppearance = appearance12;
			appearance7.BackColor = System.Drawing.SystemColors.Highlight;
			appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.ultraGrid.DisplayLayout.Override.ActiveRowAppearance = appearance7;
			this.ultraGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
			this.ultraGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
			appearance6.BackColor = System.Drawing.SystemColors.Window;
			this.ultraGrid.DisplayLayout.Override.CardAreaAppearance = appearance6;
			appearance5.BorderColor = System.Drawing.Color.Silver;
			appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.ultraGrid.DisplayLayout.Override.CellAppearance = appearance5;
			this.ultraGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.ultraGrid.DisplayLayout.Override.CellPadding = 0;
			appearance9.BackColor = System.Drawing.SystemColors.Control;
			appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance9.BorderColor = System.Drawing.SystemColors.Window;
			this.ultraGrid.DisplayLayout.Override.GroupByRowAppearance = appearance9;
			appearance11.TextHAlignAsString = "Left";
			this.ultraGrid.DisplayLayout.Override.HeaderAppearance = appearance11;
			this.ultraGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.ultraGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance10.BackColor = System.Drawing.SystemColors.Window;
			appearance10.BorderColor = System.Drawing.Color.Silver;
			this.ultraGrid.DisplayLayout.Override.RowAppearance = appearance10;
			this.ultraGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
			this.ultraGrid.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
			this.ultraGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.ultraGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.ultraGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.ultraGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ultraGrid.Location = new System.Drawing.Point(3, 3);
			this.ultraGrid.Name = "ultraGrid";
			this.ultraGrid.Size = new System.Drawing.Size(822, 466);
			this.ultraGrid.TabIndex = 0;
			this.ultraGrid.Text = "ultraGrid1";
			// 
			// btnPreview
			// 
			this.btnPreview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnPreview.Location = new System.Drawing.Point(3, 475);
			this.btnPreview.Name = "btnPreview";
			this.btnPreview.Size = new System.Drawing.Size(408, 22);
			this.btnPreview.TabIndex = 1;
			this.btnPreview.Text = "&Original UltraPrintPreviewDialog";
			this.btnPreview.UseVisualStyleBackColor = true;
			this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
			// 
			// ultraGridPrintDocument
			// 
			this.ultraGridPrintDocument.Grid = this.ultraGrid;
			// 
			// btnPreview2
			// 
			this.btnPreview2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnPreview2.Location = new System.Drawing.Point(417, 475);
			this.btnPreview2.Name = "btnPreview2";
			this.btnPreview2.Size = new System.Drawing.Size(408, 22);
			this.btnPreview2.TabIndex = 2;
			this.btnPreview2.Text = "UltraPrintPreviewDialogHelper 기본(&V)";
			this.btnPreview2.UseVisualStyleBackColor = true;
			this.btnPreview2.Click += new System.EventHandler(this.btnPreview2_Click);
			// 
			// btnPreviewCode
			// 
			this.btnPreviewCode.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnPreviewCode.Location = new System.Drawing.Point(3, 503);
			this.btnPreviewCode.Name = "btnPreviewCode";
			this.btnPreviewCode.Size = new System.Drawing.Size(408, 22);
			this.btnPreviewCode.TabIndex = 3;
			this.btnPreviewCode.Text = "UltraPrintPreviewDialogHelper code에서 속성 변경(&C)";
			this.btnPreviewCode.UseVisualStyleBackColor = true;
			this.btnPreviewCode.Click += new System.EventHandler(this.btnPreviewCode_Click);
			// 
			// ultraPrintPreviewDialogHelper
			// 
			this.ultraPrintPreviewDialogHelper.DocumentGrid = null;
			this.ultraPrintPreviewDialogHelper.Footer.TextRight = "<#> of <##>";
			this.ultraPrintPreviewDialogHelper.Header.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.ultraPrintPreviewDialogHelper.Header.TextCenter = "center";
			this.ultraPrintPreviewDialogHelper.Name = "UltraPrintPreviewDialogHelper";
			this.ultraPrintPreviewDialogHelper.PreviewSettings.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
			this.ultraPrintPreviewDialogHelper.PreviewSettings.MousePanning = Infragistics.Win.MousePanning.Both;
			this.ultraPrintPreviewDialogHelper.PreviewSettings.ScrollBarLook.ViewStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.WindowsVista;
			this.ultraPrintPreviewDialogHelper.PreviewSettings.ScrollTipStyle = Infragistics.Win.UltraWinScrollBar.ScrollTipStyle.Show;
			this.ultraPrintPreviewDialogHelper.PreviewSettings.ZoomMode = Infragistics.Win.Printing.ZoomMode.AutoFlow;
			this.ultraPrintPreviewDialogHelper.SettingFilePath = "C:\\Users\\xyz37\\AppData\\Roaming\\UltraPrintPreviewDialogHelper.config";
			this.ultraPrintPreviewDialogHelper.Style = Infragistics.Win.UltraWinToolbars.ToolbarStyle.Office2007;
			this.ultraPrintPreviewDialogHelper.ThumbnailSettings.MousePanning = Infragistics.Win.MousePanning.Both;
			this.ultraPrintPreviewDialogHelper.ThumbnailSettings.ScrollBarLook.ViewStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.WindowsVista;
			this.ultraPrintPreviewDialogHelper.UseAntiAlias = true;
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 2;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.Controls.Add(this.ultraGrid, 0, 0);
			this.tlpMain.Controls.Add(this.btnPreviewCode, 0, 2);
			this.tlpMain.Controls.Add(this.btnPreview, 0, 1);
			this.tlpMain.Controls.Add(this.btnPreview2, 1, 1);
			this.tlpMain.Controls.Add(this.btnPrintCode, 1, 2);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 3;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpMain.Size = new System.Drawing.Size(828, 528);
			this.tlpMain.TabIndex = 2;
			// 
			// btnPrintCode
			// 
			this.btnPrintCode.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnPrintCode.Location = new System.Drawing.Point(417, 503);
			this.btnPrintCode.Name = "btnPrintCode";
			this.btnPrintCode.Size = new System.Drawing.Size(408, 22);
			this.btnPrintCode.TabIndex = 4;
			this.btnPrintCode.Text = "UltraPrintPreviewDialogHelper code에서 속성 변경-바로 출력(&P)";
			this.btnPrintCode.UseVisualStyleBackColor = true;
			this.btnPrintCode.Click += new System.EventHandler(this.btnPrintCode_Click);
			// 
			// TestDriverForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(828, 528);
			this.Controls.Add(this.tlpMain);
			this.Name = "TestDriverForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.ultraGrid)).EndInit();
			this.tlpMain.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private Infragistics.Win.Printing.UltraPrintPreviewDialog ultraPrintPreviewDialog;
		private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid;
		private System.Windows.Forms.Button btnPreview;
		private Infragistics.Win.UltraWinGrid.UltraGridPrintDocument ultraGridPrintDocument;
		private System.Windows.Forms.Button btnPreview2;
		private System.Windows.Forms.Button btnPreviewCode;
		private AnyFactory.FX.Win.Controls.Print.UltraPrintPreviewDialogHelper ultraPrintPreviewDialogHelper;
		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.Button btnPrintCode;

	}
}

