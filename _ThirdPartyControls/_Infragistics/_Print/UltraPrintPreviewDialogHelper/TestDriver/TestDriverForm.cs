﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win.Printing;
using AnyFactory.FX.Win.Controls.Print;
using TestDriver;

namespace TestDriver
{
	public partial class TestDriverForm : Form
	{
		public TestDriverForm()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			BindSampleData();
		}

		private void BindSampleData()
		{
			const int COLUMN_COUNT = 15;
			const int ROW_COUNT = 100;
			DataTable dataTable = new DataTable();

			for (int i = 0; i < COLUMN_COUNT; i++)
			{
				string key = string.Format("column{0:00}", i);

				dataTable.Columns.Add(key, typeof(string));
				ultraGrid.DisplayLayout.Bands[0].Columns.Add(key).Width = 200;
			}

			for (int row = 0; row < ROW_COUNT; row++)
			{
				object[] columnData = new object[COLUMN_COUNT];

				for (int col = 0; col < COLUMN_COUNT; col++)
					columnData[col] = DateTime.Now.Ticks.ToString();

				dataTable.Rows.Add(columnData);
			}
			
			ultraGrid.DataSource = dataTable;
			//ultraGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
		}

		void MainPropertyGrid_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
		{
			ultraPrintPreviewDialogHelper.Invalidate();
		}

		private void btnPreview_Click(object sender, EventArgs e)
		{
			ResourceCustomizer rcPrinting = Infragistics.Win.Printing.Resources.Customizer;

			rcPrinting.SetCustomizedString("PrintPreview_Tool_File", "파일");
			rcPrinting.SetCustomizedString("PrintPreview_ToolCategory_File", "파일");
			rcPrinting.SetCustomizedString("PrintPreview_ToolCategory_View", "보기");
			rcPrinting.SetCustomizedString("StatusBar_Page_X_OF_X", "페이지: {0} / {1}");
			rcPrinting.SetCustomizedString("PrintPreview_Tool_Print", "프린트 툴");

			ultraGridPrintDocument.Grid = ultraGrid;
			ultraPrintPreviewDialog.Document = ultraGridPrintDocument;

			ultraPrintPreviewDialog.ShowDialog();
		}

		private void btnPreview2_Click(object sender, EventArgs e)
		{
			UltraPrintPreviewDialogHelper printPreviewForm = new UltraPrintPreviewDialogHelper()
			{
				DocumentGrid = ultraGrid,
			};
			
			printPreviewForm.ShowDialog();
		}

		private void btnPreviewCode_Click(object sender, EventArgs e)
		{				
			ultraPrintPreviewDialogHelper.WindowState = FormWindowState.Maximized;
			ultraPrintPreviewDialogHelper.ZoomMode = ZoomMode.WholePage;
			ultraPrintPreviewDialogHelper.DocumentGrid = ultraGrid;
			
			ultraPrintPreviewDialogHelper.SetClosePreviewToolInFirst();
			ultraPrintPreviewDialogHelper.AutoGeneratePreview =  true;

			ultraPrintPreviewDialogHelper.Landscape = true;
			ultraPrintPreviewDialogHelper.PageBody.BorderSides = Border3DSide.All;
			ultraPrintPreviewDialogHelper.PageBody.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			ultraPrintPreviewDialogHelper.PageBody.Appearance.BackColor = Color.Blue;
			ultraPrintPreviewDialogHelper.PageBody.Margins.Top = UltraPrintPreviewDialogHelper.ConvertMM2Inch(10);
			ultraPrintPreviewDialogHelper.PageBody.Margins.Bottom = UltraPrintPreviewDialogHelper.ConvertMM2Inch(10);
			ultraPrintPreviewDialogHelper.PageBody.Margins.Left = UltraPrintPreviewDialogHelper.ConvertMM2Inch(10);
			ultraPrintPreviewDialogHelper.PageBody.Margins.Right = UltraPrintPreviewDialogHelper.ConvertMM2Inch(10);
			ultraPrintPreviewDialogHelper.PageMargins = new System.Drawing.Printing.Margins(100, 100, 120, 100);
						
			ultraPrintPreviewDialogHelper.Header.BorderSides = Border3DSide.All;
			ultraPrintPreviewDialogHelper.Header.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			//ultraPrintPreviewDialogHelper1.Header.Appearance.BackColor = Color.Red;

			ultraPrintPreviewDialogHelper.Footer.BorderSides = Border3DSide.All;
			ultraPrintPreviewDialogHelper.Footer.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			ultraPrintPreviewDialogHelper.Footer.Appearance.BackColor = Color.Yellow;

			ultraPrintPreviewDialogHelper.Header.TextRight = "[User Name]";
			ultraPrintPreviewDialogHelper.SetHeaderAndFooterText("$CurrentPage$ of $TotalPage$", HeaderFooterAlign.FooterLeft);
			ultraPrintPreviewDialogHelper.SetHeaderAndFooterText("$PrintedDate$ $PrintedTime$", HeaderFooterAlign.FooterRight);

			ultraPrintPreviewDialogHelper.InternalGridAutoFitWidthToOnePage = true;

			PropertyForm propertyForm = new PropertyForm();

			propertyForm.MainPropertyGrid.SelectedObject = ultraPrintPreviewDialogHelper;
			propertyForm.MainPropertyGrid.SelectedGridItemChanged += new SelectedGridItemChangedEventHandler(MainPropertyGrid_SelectedGridItemChanged);
			//propertyForm.Show();

			ultraPrintPreviewDialogHelper.ShowDialog();
		}

		private void btnPrintCode_Click(object sender, EventArgs e)
		{
			ultraPrintPreviewDialogHelper.WindowState = FormWindowState.Maximized;
			ultraPrintPreviewDialogHelper.ZoomMode = ZoomMode.WholePage;
			ultraPrintPreviewDialogHelper.DocumentGrid = ultraGrid;

			ultraPrintPreviewDialogHelper.SetClosePreviewToolInFirst();
			ultraPrintPreviewDialogHelper.AutoGeneratePreview =  true;

			ultraPrintPreviewDialogHelper.Landscape = true;
			ultraPrintPreviewDialogHelper.PageBody.BorderSides = Border3DSide.All;
			ultraPrintPreviewDialogHelper.PageBody.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			ultraPrintPreviewDialogHelper.PageBody.Appearance.BackColor = Color.Blue;
			ultraPrintPreviewDialogHelper.PageBody.Margins.Top = UltraPrintPreviewDialogHelper.ConvertMM2Inch(10);
			ultraPrintPreviewDialogHelper.PageBody.Margins.Bottom = UltraPrintPreviewDialogHelper.ConvertMM2Inch(10);
			ultraPrintPreviewDialogHelper.PageBody.Margins.Left = UltraPrintPreviewDialogHelper.ConvertMM2Inch(10);
			ultraPrintPreviewDialogHelper.PageBody.Margins.Right = UltraPrintPreviewDialogHelper.ConvertMM2Inch(10);
			ultraPrintPreviewDialogHelper.PageMargins = new System.Drawing.Printing.Margins(100, 100, 120, 100);

			ultraPrintPreviewDialogHelper.Header.BorderSides = Border3DSide.All;
			ultraPrintPreviewDialogHelper.Header.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			//ultraPrintPreviewDialogHelper1.Header.Appearance.BackColor = Color.Red;

			ultraPrintPreviewDialogHelper.Footer.BorderSides = Border3DSide.All;
			ultraPrintPreviewDialogHelper.Footer.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			ultraPrintPreviewDialogHelper.Footer.Appearance.BackColor = Color.Yellow;

			ultraPrintPreviewDialogHelper.Header.TextRight = "[User Name]";
			ultraPrintPreviewDialogHelper.SetHeaderAndFooterText("$CurrentPage$ of $TotalPage$", HeaderFooterAlign.FooterLeft);
			ultraPrintPreviewDialogHelper.SetHeaderAndFooterText("$PrintedDate$ $PrintedTime$", HeaderFooterAlign.FooterRight);

			ultraPrintPreviewDialogHelper.InternalGridAutoFitWidthToOnePage = true;

			PropertyForm propertyForm = new PropertyForm();

			propertyForm.MainPropertyGrid.SelectedObject = ultraPrintPreviewDialogHelper;
			propertyForm.MainPropertyGrid.SelectedGridItemChanged += new SelectedGridItemChangedEventHandler(MainPropertyGrid_SelectedGridItemChanged);
			//propertyForm.Show();

			//ultraPrintPreviewDialogHelper.InternalUltraPrintDocument.Print();
			ultraPrintPreviewDialogHelper.InternalPrintDialog.ShowDialog();
		}
	}
}
