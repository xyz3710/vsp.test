﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnyFactory.FX.Win.Controls.UltraGridHelper.Print
{
	interface ISupportPrint
	{
        System.Drawing.Printing.PrintDocument PrintDocument { get; }
	}
}
