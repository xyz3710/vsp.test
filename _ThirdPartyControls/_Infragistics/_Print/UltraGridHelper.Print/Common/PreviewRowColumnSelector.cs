﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.ComponentModel;
using Infragistics.Win;

namespace AnyFactory.FX.Win.Controls.UltraGridHelper.Print
{
	[ToolboxItem(false)]
	internal class PreviewRowColumnSelector : Control
	{
		// Fields
		private static readonly Color BackColorDefault;
		private int columns;
		private static readonly Color ForeColorDefault;
		private Color imageBorderColor;
		private const int ImageBorderWidth = 1;
		private const int ImagePadding = 3;
		private Rectangle[,] imgRects;
		private const int InterImageSpacing = 2;
		private const int OuterPadding = 1;
		private const int PreTextSpacingY = 4;
		private Bitmap rowColImage;
		private int rows;
		private const int RowsColumnsDefault = 3;
		private int selectedColumns;
		private int selectedRows;
		private Size textSize;

		// Methods
		static PreviewRowColumnSelector()
		{
			BackColorDefault = SystemColors.Window;
			ForeColorDefault = SystemColors.WindowText;
		}



		public PreviewRowColumnSelector()
		{
			this.rows = 3;
			this.columns = 3;
			this.imageBorderColor = Color.Empty;
			base.SetStyle(ControlStyles.Selectable, false);
			base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			base.SetStyle(ControlStyles.UserPaint, true);
			base.SetStyle(ControlStyles.FixedHeight, true);
			base.SetStyle(ControlStyles.FixedWidth, true);
			base.SetStyle(ControlStyles.DoubleBuffer, true);
			base.SetStyle(ControlStyles.ResizeRedraw, true);
			this.ResetBackColor();
			this.ResetForeColor();
			this.TabStop = false;
			this.SetSelected(0, 0);
		}





		private void DirtyImageRects()
		{
			this.imgRects = null;
			this.SetSelected(0, 0);
			if (base.Created)
			{
				base.Width++;
			}
		}




		private void DirtyTextSize()
		{
			this.textSize = Size.Empty;
			if (base.Created)
			{
				base.Width++;
			}
		}




		private string GetDisplayText()
		{
			if ((this.selectedColumns >= 1) && (this.selectedRows >= 1))
			{
				return this.GetDisplayText(this.selectedColumns, this.selectedRows);
			}
			return "PreviewRowColSelection_Cancel";
		}




		private string GetDisplayText(int columns, int rows)
		{
			return string.Format("PreviewRowColSelection_SelectedPages {0}/{1}", rows, columns);
		}


		private Rectangle[,] GetImageRectangles()
		{
			if (this.imgRects == null)
			{
				if ((this.rows * this.columns) == 0)
				{
					this.imgRects = new Rectangle[0, 0];
				}
				else
				{
					Rectangle displayRectangle = this.DisplayRectangle;
					Size size = this.RowColImage.Size;
					displayRectangle.Inflate(-1, -1);
					Rectangle[,] rectangleArray = new Rectangle[this.rows, this.columns];
					for (int i = 0; i < this.rows; i++)
					{
						int left = displayRectangle.Left;
						int top = displayRectangle.Top;
						for (int j = 0; j < this.columns; j++)
						{
							(rectangleArray[i, j]) = new Rectangle(left, top, (size.Width + 6) + 2, (size.Height + 6) + 2);
							//*(rectangleArray[i, j]) = new Rectangle(left, top, (size.Width + 6) + 2, (size.Height + 6) + 2);
							left += ((2 + size.Width) + 6) + 2;
						}
						displayRectangle.Y += ((size.Height + 2) + 6) + 2;
					}
					this.imgRects = rectangleArray;
				}
			}
			return this.imgRects;
		}



		protected override void OnCreateControl()
		{
			base.OnCreateControl();
			base.Width++;
		}



		protected override void OnFontChanged(EventArgs e)
		{
			this.DirtyTextSize();
			base.OnFontChanged(e);
		}



		protected override void OnMouseLeave(EventArgs e)
		{
			this.SetSelected(0, 0);
			base.OnMouseLeave(e);
		}





		protected override void OnMouseMove(MouseEventArgs e)
		{
			Rectangle[,] imageRectangles = this.GetImageRectangles();
			if ((imageRectangles != null) && (imageRectangles.Length > 0))
			{
				int selectedRows = 0;
				int selectedColumns = 0;
				for (int i = 0; i < this.rows; i++)
				{
					if (e.Y <= imageRectangles[i, 0].Y)
					{
						break;
					}
					selectedRows++;
				}
				for (int j = 0; j < this.columns; j++)
				{
					if (e.X <= imageRectangles[0, j].X)
					{
						break;
					}
					selectedColumns++;
				}
				this.SetSelected(selectedColumns, selectedRows);
			}
			base.OnMouseMove(e);
		}




		protected override void OnPaint(PaintEventArgs e)
		{
			Image rowColImage = this.RowColImage;
			Size size = rowColImage.Size;
			Rectangle displayRectangle = this.DisplayRectangle;
			Color color = Color.FromArgb(this.BackColor.ToArgb());
			if (color.A > 0)
			{
				using (Brush brush = new SolidBrush(color))
				{
					e.Graphics.FillRectangle(brush, displayRectangle);
				}
			}
			displayRectangle.Inflate(-1, -1);
			Rectangle[,] imageRectangles = this.GetImageRectangles();
			if ((imageRectangles != null) && (imageRectangles.Length > 0))
			{
				using (Pen pen = new Pen(this.ImageBorderColorResolved))
				{
					for (int i = 0; i < this.rows; i++)
					{
						for (int j = 0; j < this.columns; j++)
						{
							//Rectangle rect = *(imageRectangles[i, j]);
							Rectangle rect = (imageRectangles[i, j]);
							if ((i < this.selectedRows) && (j < this.selectedColumns))
							{
								e.Graphics.FillRectangle(SystemBrushes.Highlight, rect);
							}
							rect.Width--;
							rect.Height--;
							e.Graphics.DrawRectangle(pen, rect);
							//Rectangle rectangle3 = *(imageRectangles[i, j]);
							Rectangle rectangle3 = (imageRectangles[i, j]);
							rectangle3.Inflate(-4, -4);
							e.Graphics.DrawImage(rowColImage, rectangle3);
						}
						displayRectangle.Height -= imageRectangles[i, 0].Height;
						displayRectangle.Y += imageRectangles[i, 0].Height;
					}
				}
				displayRectangle.Height -= 2 * (this.rows - 1);
				displayRectangle.Y += 2 * (this.rows - 1);
			}
			using (StringFormat format = new StringFormat(StringFormatFlags.FitBlackBox))
			{
				format.Alignment = StringAlignment.Center;
				format.LineAlignment = StringAlignment.Center;
				Color color2 = Color.FromArgb(this.ForeColor.ToArgb());
				if (color2.A > 0)
				{
					using (Brush brush2 = new SolidBrush(color2))
					{
						e.Graphics.DrawString(this.GetDisplayText(), this.Font, brush2, displayRectangle, format);
					}
				}
			}
			base.OnPaint(e);
		}




		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public override void ResetBackColor()
		{
			this.BackColor = BackColorDefault;
		}



		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public override void ResetForeColor()
		{
			this.ForeColor = ForeColorDefault;
		}



		public void ResetSelection()
		{
			this.SetSelected(0, 0);
		}



		protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
		{
			if (base.Created && ((specified & BoundsSpecified.Size) != BoundsSpecified.None))
			{
				Size rowColImageSize = this.RowColImageSize;
				rowColImageSize.Width += 2;
				rowColImageSize.Height += 2;
				rowColImageSize.Width += 6;
				rowColImageSize.Height += 6;
				rowColImageSize.Width *= this.columns;
				rowColImageSize.Width += 2 * (this.columns - 1);
				rowColImageSize.Height *= this.rows;
				rowColImageSize.Height += 2 * (this.rows - 1);
				width = Math.Max(rowColImageSize.Width, this.TextSize.Width);
				height = (rowColImageSize.Height + this.TextSize.Height) + 4;
				width += 2;
				height += 2;
			}
			base.SetBoundsCore(x, y, width, height, specified);
		}

 

		 private void SetSelected(int selectedColumns, int selectedRows)
		{
			if ((this.selectedColumns != selectedColumns) || (this.selectedRows != selectedRows))
			{
				this.selectedColumns = selectedColumns;
				this.selectedRows = selectedRows;
				this.Text = this.GetDisplayText();
				if (base.IsHandleCreated)
				{
					base.Invalidate();
				}
			}
		}


		protected virtual bool ShouldSerializeBackColor()
		{
			return !this.BackColor.Equals(BackColorDefault);
		}


		protected virtual bool ShouldSerializeForeColor()
		{
			return !this.ForeColor.Equals(ForeColorDefault);
		}

 

 


		// Properties
		[DefaultValue(3)]
		public int Columns
		{
			get
			{
				return this.columns;
			}
			set
			{
				if (this.columns != value)
				{
					if (value < 1)
					{
						throw new ArgumentOutOfRangeException();
					}
					this.columns = value;
					this.DirtyImageRects();
				}
			}
		}


		protected override ImeMode DefaultImeMode
		{
			get
			{
				return ImeMode.Disable;
			}
		}




		[DefaultValue(typeof(Color), "")]
		public Color ImageBorderColor
		{
			get
			{
				return this.imageBorderColor;
			}
			set
			{
				if (value != this.imageBorderColor)
				{
					this.imageBorderColor = value;
					if (base.IsHandleCreated)
					{
						base.Invalidate();
					}
				}
			}
		}




		private Color ImageBorderColorResolved
		{
			get
			{
				Color imageBorderColor = this.imageBorderColor;
				if (imageBorderColor.IsEmpty)
				{
					imageBorderColor = Office2003Colors.PopupMenuBorder;
				}
				if (imageBorderColor.IsKnownColor)
				{
					imageBorderColor = Color.FromArgb(imageBorderColor.ToArgb());
				}
				return imageBorderColor;
			}
		}



		private Image RowColImage
		{
			get
			{
				if (this.rowColImage == null)
				{
					//this.rowColImageStream = global::AnyFactory.FX.Win.Controls.UltraGridHelper.Print.Properties.Resources.MultiplePagesImage;
					//typeof(PreviewRowColumnSelector).Assembly.GetManifestResourceStream(typeof(PreviewRowColumnSelector), "MultiplePagesImage.bmp");
					//typeof(PreviewRowColumnSelector).Assembly.GetManifestResourceStream(typeof(PreviewRowColumnSelector), "MultiplePagesImage.bmp");
					//if (this.rowColImageStream != null)
					//{
						this.rowColImage = global::AnyFactory.FX.Win.Controls.UltraGridHelper.Print.Properties.Resources.MultiplePagesImage; //(Bitmap)Image.FromStream(this.rowColImageStream);
						if (this.rowColImage != null)
						{
							this.rowColImage.MakeTransparent(Color.Magenta);
						}
					//}
				}
				return this.rowColImage;
			}
		}



		private Size RowColImageSize
		{
			get
			{
				return this.RowColImage.Size;
			}
		}



		[DefaultValue(3)]
		public int Rows
		{
			get
			{
				return this.rows;
			}
			set
			{
				if (this.rows != value)
				{
					if (value < 1)
					{
						throw new ArgumentOutOfRangeException();
					}
					this.rows = value;
					this.DirtyImageRects();
				}
			}
		}




		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Size Selection
		{
			get
			{
				return new Size(this.selectedColumns, this.selectedRows);
			}
		}




		[DefaultValue(false)]
		public new bool TabStop
		{
			get
			{
				return base.TabStop;
			}
			set
			{
				base.TabStop = value;
			}
		}



		private Size TextSize
		{
			get
			{
				if (this.textSize == Size.Empty)
				{
					Graphics cachedGraphics = DrawUtility.GetCachedGraphics(this);
					try
					{
						this.textSize = Size.Ceiling(cachedGraphics.MeasureString(this.GetDisplayText(10, 10), this.Font, PointF.Empty, null));
					}
					finally
					{
						DrawUtility.ReleaseCachedGraphics(cachedGraphics);
					}
				}
				return this.textSize;
			}
		}
 

 

	}


}
