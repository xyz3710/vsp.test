using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.Printing;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinEditors;
using System.IO;

namespace AnyFactory.FX.Win.Controls.UltraGridHelper.Print
{
	/// <summary>
	/// Summary description for UltraPageSetup.
	/// </summary>
	public class UltraPageSetup : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel pnlTop;
		private Infragistics.Win.Misc.UltraLabel lblStyleName;
		private Infragistics.Win.UltraWinEditors.UltraComboEditor cboStyleName;
		private System.Windows.Forms.Panel pnlBottom;
		private Infragistics.Win.Misc.UltraButton btnPrint;
		private Infragistics.Win.Misc.UltraButton btnPrintPreview;
		private Infragistics.Win.Misc.UltraButton btnCancel;
		private Infragistics.Win.Misc.UltraButton btnOk;
		private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
		private WinSchedulePrintDemo.PrinterSettingsControl printerSettingsControl1;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtHeaderFont;
		private Infragistics.Win.Misc.UltraButton btnHeaderFont;
		private Infragistics.Win.Misc.UltraLabel lblHeader;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtHeaderLeft;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtHeaderMiddle;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtHeaderRight;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFooterRight;
		private Infragistics.Win.Misc.UltraLabel lblFooter;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFooterFont;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFooterMiddle;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFooterLeft;
		private Infragistics.Win.Misc.UltraButton btnFooterFont;
		private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkReverseOnEvenPages;
		private System.Windows.Forms.Panel pnlToolbar;
		private Infragistics.Win.Misc.UltraButton btnToolbarDate;
		private Infragistics.Win.Misc.UltraButton btnToolbarTime;
		private Infragistics.Win.Misc.UltraButton btnToolbarPageNumber;
		private Infragistics.Win.Misc.UltraButton btnToolbarUserName;
		private System.Windows.Forms.FontDialog fontDialog1;
		private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabPagePaper;
		private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabPageHeaderFooter;
		private Infragistics.Win.UltraWinTabControl.UltraTabControl tabControlMain;
		private System.Windows.Forms.ImageList imgListPreview;
		private System.ComponentModel.IContainer components;

		private UltraGridPrintDocument _printDocument;
	
		#region Constructors
 
		public UltraPageSetup(UltraGridPrintDocument printDocument)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_printDocument = printDocument;
			btnPrint.Enabled = false;
			btnPrintPreview.Enabled = false;
		}

		#endregion Constructors


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UltraPageSetup));
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
			Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
			tabPagePaper = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
			tabPageHeaderFooter = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
			pnlToolbar = new System.Windows.Forms.Panel();
			btnToolbarUserName = new Infragistics.Win.Misc.UltraButton();
			btnToolbarPageNumber = new Infragistics.Win.Misc.UltraButton();
			btnToolbarTime = new Infragistics.Win.Misc.UltraButton();
			btnToolbarDate = new Infragistics.Win.Misc.UltraButton();
			chkReverseOnEvenPages = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
			lblHeader = new Infragistics.Win.Misc.UltraLabel();
			txtHeaderLeft = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			btnHeaderFont = new Infragistics.Win.Misc.UltraButton();
			txtHeaderFont = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			txtHeaderMiddle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			txtHeaderRight = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			txtFooterRight = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			lblFooter = new Infragistics.Win.Misc.UltraLabel();
			txtFooterFont = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			txtFooterMiddle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			txtFooterLeft = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			btnFooterFont = new Infragistics.Win.Misc.UltraButton();
			imgListPreview = new System.Windows.Forms.ImageList(components);
			pnlTop = new System.Windows.Forms.Panel();
			cboStyleName = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
			lblStyleName = new Infragistics.Win.Misc.UltraLabel();
			pnlBottom = new System.Windows.Forms.Panel();
			btnPrint = new Infragistics.Win.Misc.UltraButton();
			btnPrintPreview = new Infragistics.Win.Misc.UltraButton();
			btnCancel = new Infragistics.Win.Misc.UltraButton();
			btnOk = new Infragistics.Win.Misc.UltraButton();
			tabControlMain = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
			ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
			fontDialog1 = new System.Windows.Forms.FontDialog();
			printerSettingsControl1 = new WinSchedulePrintDemo.PrinterSettingsControl();
			tabPagePaper.SuspendLayout();
			tabPageHeaderFooter.SuspendLayout();
			pnlToolbar.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(txtHeaderLeft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(txtHeaderFont)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(txtHeaderMiddle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(txtHeaderRight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(txtFooterRight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(txtFooterFont)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(txtFooterMiddle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(txtFooterLeft)).BeginInit();
			pnlTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(cboStyleName)).BeginInit();
			pnlBottom.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(tabControlMain)).BeginInit();
			tabControlMain.SuspendLayout();
			SuspendLayout();
			// 
			// tabPagePaper
			// 
			tabPagePaper.Controls.Add(printerSettingsControl1);
			tabPagePaper.Location = new System.Drawing.Point(1, 23);
			tabPagePaper.Name = "tabPagePaper";
			tabPagePaper.Size = new System.Drawing.Size(506, 299);
			// 
			// tabPageHeaderFooter
			// 
			tabPageHeaderFooter.Controls.Add(pnlToolbar);
			tabPageHeaderFooter.Controls.Add(chkReverseOnEvenPages);
			tabPageHeaderFooter.Controls.Add(lblHeader);
			tabPageHeaderFooter.Controls.Add(txtHeaderLeft);
			tabPageHeaderFooter.Controls.Add(btnHeaderFont);
			tabPageHeaderFooter.Controls.Add(txtHeaderFont);
			tabPageHeaderFooter.Controls.Add(txtHeaderMiddle);
			tabPageHeaderFooter.Controls.Add(txtHeaderRight);
			tabPageHeaderFooter.Controls.Add(txtFooterRight);
			tabPageHeaderFooter.Controls.Add(lblFooter);
			tabPageHeaderFooter.Controls.Add(txtFooterFont);
			tabPageHeaderFooter.Controls.Add(txtFooterMiddle);
			tabPageHeaderFooter.Controls.Add(txtFooterLeft);
			tabPageHeaderFooter.Controls.Add(btnFooterFont);
			tabPageHeaderFooter.Location = new System.Drawing.Point(-10000, -10000);
			tabPageHeaderFooter.Name = "tabPageHeaderFooter";
			tabPageHeaderFooter.Size = new System.Drawing.Size(506, 299);
			// 
			// pnlToolbar
			// 
			pnlToolbar.BackColor = System.Drawing.Color.Transparent;
			pnlToolbar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			pnlToolbar.Controls.Add(btnToolbarUserName);
			pnlToolbar.Controls.Add(btnToolbarPageNumber);
			pnlToolbar.Controls.Add(btnToolbarTime);
			pnlToolbar.Controls.Add(btnToolbarDate);
			pnlToolbar.Location = new System.Drawing.Point(164, 256);
			pnlToolbar.Name = "pnlToolbar";
			pnlToolbar.Size = new System.Drawing.Size(136, 24);
			pnlToolbar.TabIndex = 5;
			// 
			// btnToolbarUserName
			// 
			btnToolbarUserName.AcceptsFocus = false;
			appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
			btnToolbarUserName.Appearance = appearance3;
			btnToolbarUserName.AutoSize = true;
			btnToolbarUserName.ButtonStyle = Infragistics.Win.UIElementButtonStyle.OfficeXPToolbarButton;
			btnToolbarUserName.ImageTransparentColor = System.Drawing.Color.Magenta;
			btnToolbarUserName.Location = new System.Drawing.Point(104, 0);
			btnToolbarUserName.Name = "btnToolbarUserName";
			btnToolbarUserName.ShowOutline = false;
			btnToolbarUserName.Size = new System.Drawing.Size(24, 24);
			btnToolbarUserName.TabIndex = 3;
			btnToolbarUserName.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			btnToolbarUserName.Click += new System.EventHandler(OnToolbarButtonClick);
			// 
			// btnToolbarPageNumber
			// 
			btnToolbarPageNumber.AcceptsFocus = false;
			appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
			btnToolbarPageNumber.Appearance = appearance4;
			btnToolbarPageNumber.AutoSize = true;
			btnToolbarPageNumber.ButtonStyle = Infragistics.Win.UIElementButtonStyle.OfficeXPToolbarButton;
			btnToolbarPageNumber.ImageTransparentColor = System.Drawing.Color.Magenta;
			btnToolbarPageNumber.Location = new System.Drawing.Point(8, 0);
			btnToolbarPageNumber.Name = "btnToolbarPageNumber";
			btnToolbarPageNumber.ShowOutline = false;
			btnToolbarPageNumber.Size = new System.Drawing.Size(24, 24);
			btnToolbarPageNumber.TabIndex = 2;
			btnToolbarPageNumber.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			btnToolbarPageNumber.Click += new System.EventHandler(OnToolbarButtonClick);
			// 
			// btnToolbarTime
			// 
			btnToolbarTime.AcceptsFocus = false;
			appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
			btnToolbarTime.Appearance = appearance5;
			btnToolbarTime.AutoSize = true;
			btnToolbarTime.ButtonStyle = Infragistics.Win.UIElementButtonStyle.OfficeXPToolbarButton;
			btnToolbarTime.ImageTransparentColor = System.Drawing.Color.Magenta;
			btnToolbarTime.Location = new System.Drawing.Point(72, 0);
			btnToolbarTime.Name = "btnToolbarTime";
			btnToolbarTime.ShowOutline = false;
			btnToolbarTime.Size = new System.Drawing.Size(24, 24);
			btnToolbarTime.TabIndex = 1;
			btnToolbarTime.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			btnToolbarTime.Click += new System.EventHandler(OnToolbarButtonClick);
			// 
			// btnToolbarDate
			// 
			btnToolbarDate.AcceptsFocus = false;
			appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
			btnToolbarDate.Appearance = appearance6;
			btnToolbarDate.AutoSize = true;
			btnToolbarDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.OfficeXPToolbarButton;
			btnToolbarDate.ImageTransparentColor = System.Drawing.Color.Magenta;
			btnToolbarDate.Location = new System.Drawing.Point(40, 0);
			btnToolbarDate.Name = "btnToolbarDate";
			btnToolbarDate.ShowOutline = false;
			btnToolbarDate.Size = new System.Drawing.Size(24, 24);
			btnToolbarDate.TabIndex = 0;
			btnToolbarDate.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			btnToolbarDate.Click += new System.EventHandler(OnToolbarButtonClick);
			// 
			// chkReverseOnEvenPages
			// 
			chkReverseOnEvenPages.BackColor = System.Drawing.Color.Transparent;
			chkReverseOnEvenPages.BackColorInternal = System.Drawing.Color.Transparent;
			chkReverseOnEvenPages.Location = new System.Drawing.Point(12, 256);
			chkReverseOnEvenPages.Name = "chkReverseOnEvenPages";
			chkReverseOnEvenPages.Size = new System.Drawing.Size(144, 20);
			chkReverseOnEvenPages.TabIndex = 4;
			chkReverseOnEvenPages.Text = "Reverse on even pages";
			chkReverseOnEvenPages.CheckedChanged += new System.EventHandler(chkReverseOnEvenPages_CheckedChanged);
			// 
			// lblHeader
			// 
			lblHeader.AutoSize = true;
			lblHeader.BackColorInternal = System.Drawing.Color.Transparent;
			lblHeader.Location = new System.Drawing.Point(12, 20);
			lblHeader.Name = "lblHeader";
			lblHeader.Size = new System.Drawing.Size(44, 15);
			lblHeader.TabIndex = 3;
			lblHeader.Text = "Header:";
			// 
			// txtHeaderLeft
			// 
			appearance7.TextHAlignAsString = "Left";
			txtHeaderLeft.Appearance = appearance7;
			txtHeaderLeft.AutoSize = false;
			txtHeaderLeft.Location = new System.Drawing.Point(12, 72);
			txtHeaderLeft.Multiline = true;
			txtHeaderLeft.Name = "txtHeaderLeft";
			txtHeaderLeft.Size = new System.Drawing.Size(156, 52);
			txtHeaderLeft.TabIndex = 2;
			txtHeaderLeft.Enter += new System.EventHandler(OnHeaderFooterTextBoxEnter);
			txtHeaderLeft.ValueChanged += new System.EventHandler(txtHeaderLeft_ValueChanged);
			txtHeaderLeft.Leave += new System.EventHandler(OnHeaderFooterTextBoxLeave);
			// 
			// btnHeaderFont
			// 
			btnHeaderFont.BackColorInternal = System.Drawing.Color.Transparent;
			btnHeaderFont.Location = new System.Drawing.Point(172, 40);
			btnHeaderFont.Name = "btnHeaderFont";
			btnHeaderFont.Size = new System.Drawing.Size(56, 21);
			btnHeaderFont.TabIndex = 1;
			btnHeaderFont.Text = "Font...";
			// 
			// txtHeaderFont
			// 
			txtHeaderFont.Location = new System.Drawing.Point(12, 40);
			txtHeaderFont.Name = "txtHeaderFont";
			txtHeaderFont.ReadOnly = true;
			txtHeaderFont.Size = new System.Drawing.Size(156, 22);
			txtHeaderFont.TabIndex = 0;
			// 
			// txtHeaderMiddle
			// 
			appearance8.TextHAlignAsString = "Center";
			txtHeaderMiddle.Appearance = appearance8;
			txtHeaderMiddle.AutoSize = false;
			txtHeaderMiddle.Location = new System.Drawing.Point(172, 72);
			txtHeaderMiddle.Multiline = true;
			txtHeaderMiddle.Name = "txtHeaderMiddle";
			txtHeaderMiddle.Size = new System.Drawing.Size(156, 52);
			txtHeaderMiddle.TabIndex = 2;
			txtHeaderMiddle.Enter += new System.EventHandler(OnHeaderFooterTextBoxEnter);
			txtHeaderMiddle.ValueChanged += new System.EventHandler(txtHeaderMiddle_ValueChanged);
			txtHeaderMiddle.Leave += new System.EventHandler(OnHeaderFooterTextBoxLeave);
			// 
			// txtHeaderRight
			// 
			appearance9.TextHAlignAsString = "Right";
			txtHeaderRight.Appearance = appearance9;
			txtHeaderRight.AutoSize = false;
			txtHeaderRight.Location = new System.Drawing.Point(332, 72);
			txtHeaderRight.Multiline = true;
			txtHeaderRight.Name = "txtHeaderRight";
			txtHeaderRight.Size = new System.Drawing.Size(156, 52);
			txtHeaderRight.TabIndex = 2;
			txtHeaderRight.Enter += new System.EventHandler(OnHeaderFooterTextBoxEnter);
			txtHeaderRight.ValueChanged += new System.EventHandler(txtHeaderRight_ValueChanged);
			txtHeaderRight.Leave += new System.EventHandler(OnHeaderFooterTextBoxLeave);
			// 
			// txtFooterRight
			// 
			appearance10.TextHAlignAsString = "Right";
			txtFooterRight.Appearance = appearance10;
			txtFooterRight.AutoSize = false;
			txtFooterRight.Location = new System.Drawing.Point(332, 192);
			txtFooterRight.Multiline = true;
			txtFooterRight.Name = "txtFooterRight";
			txtFooterRight.Size = new System.Drawing.Size(156, 52);
			txtFooterRight.TabIndex = 2;
			txtFooterRight.Enter += new System.EventHandler(OnHeaderFooterTextBoxEnter);
			txtFooterRight.ValueChanged += new System.EventHandler(txtFooterRight_ValueChanged);
			txtFooterRight.Leave += new System.EventHandler(OnHeaderFooterTextBoxLeave);
			// 
			// lblFooter
			// 
			lblFooter.AutoSize = true;
			lblFooter.BackColorInternal = System.Drawing.Color.Transparent;
			lblFooter.Location = new System.Drawing.Point(12, 140);
			lblFooter.Name = "lblFooter";
			lblFooter.Size = new System.Drawing.Size(40, 15);
			lblFooter.TabIndex = 3;
			lblFooter.Text = "Footer:";
			// 
			// txtFooterFont
			// 
			txtFooterFont.Location = new System.Drawing.Point(12, 160);
			txtFooterFont.Name = "txtFooterFont";
			txtFooterFont.ReadOnly = true;
			txtFooterFont.Size = new System.Drawing.Size(156, 22);
			txtFooterFont.TabIndex = 0;
			// 
			// txtFooterMiddle
			// 
			appearance11.TextHAlignAsString = "Center";
			txtFooterMiddle.Appearance = appearance11;
			txtFooterMiddle.AutoSize = false;
			txtFooterMiddle.Location = new System.Drawing.Point(172, 192);
			txtFooterMiddle.Multiline = true;
			txtFooterMiddle.Name = "txtFooterMiddle";
			txtFooterMiddle.Size = new System.Drawing.Size(156, 52);
			txtFooterMiddle.TabIndex = 2;
			txtFooterMiddle.Enter += new System.EventHandler(OnHeaderFooterTextBoxEnter);
			txtFooterMiddle.ValueChanged += new System.EventHandler(txtFooterMiddle_ValueChanged);
			txtFooterMiddle.Leave += new System.EventHandler(OnHeaderFooterTextBoxLeave);
			// 
			// txtFooterLeft
			// 
			appearance12.TextHAlignAsString = "Left";
			txtFooterLeft.Appearance = appearance12;
			txtFooterLeft.AutoSize = false;
			txtFooterLeft.Location = new System.Drawing.Point(12, 192);
			txtFooterLeft.Multiline = true;
			txtFooterLeft.Name = "txtFooterLeft";
			txtFooterLeft.Size = new System.Drawing.Size(156, 52);
			txtFooterLeft.TabIndex = 2;
			txtFooterLeft.Enter += new System.EventHandler(OnHeaderFooterTextBoxEnter);
			txtFooterLeft.ValueChanged += new System.EventHandler(txtFooterLeft_ValueChanged);
			txtFooterLeft.Leave += new System.EventHandler(OnHeaderFooterTextBoxLeave);
			// 
			// btnFooterFont
			// 
			btnFooterFont.BackColorInternal = System.Drawing.Color.Transparent;
			btnFooterFont.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			btnFooterFont.Location = new System.Drawing.Point(172, 160);
			btnFooterFont.Name = "btnFooterFont";
			btnFooterFont.Size = new System.Drawing.Size(56, 21);
			btnFooterFont.TabIndex = 1;
			btnFooterFont.Text = "Font...";
			// 
			// imgListPreview
			// 
			imgListPreview.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListPreview.ImageStream")));
			imgListPreview.TransparentColor = System.Drawing.Color.Magenta;
			imgListPreview.Images.SetKeyName(0, "");
			imgListPreview.Images.SetKeyName(1, "");
			imgListPreview.Images.SetKeyName(2, "");
			imgListPreview.Images.SetKeyName(3, "");
			imgListPreview.Images.SetKeyName(4, "");
			imgListPreview.Images.SetKeyName(5, "");
			imgListPreview.Images.SetKeyName(6, "");
			imgListPreview.Images.SetKeyName(7, "");
			imgListPreview.Images.SetKeyName(8, "");
			imgListPreview.Images.SetKeyName(9, "");
			// 
			// pnlTop
			// 
			pnlTop.Controls.Add(cboStyleName);
			pnlTop.Controls.Add(lblStyleName);
			pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
			pnlTop.Location = new System.Drawing.Point(0, 0);
			pnlTop.Name = "pnlTop";
			pnlTop.Size = new System.Drawing.Size(510, 40);
			pnlTop.TabIndex = 0;
			// 
			// cboStyleName
			// 
			cboStyleName.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
			cboStyleName.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
			cboStyleName.Location = new System.Drawing.Point(92, 8);
			cboStyleName.Name = "cboStyleName";
			cboStyleName.ReadOnly = true;
			cboStyleName.Size = new System.Drawing.Size(160, 22);
			cboStyleName.TabIndex = 1;
			// 
			// lblStyleName
			// 
			lblStyleName.AutoSize = true;
			lblStyleName.Location = new System.Drawing.Point(16, 12);
			lblStyleName.Name = "lblStyleName";
			lblStyleName.Size = new System.Drawing.Size(65, 15);
			lblStyleName.TabIndex = 0;
			lblStyleName.Text = "Style Name:";
			// 
			// pnlBottom
			// 
			pnlBottom.Controls.Add(btnPrint);
			pnlBottom.Controls.Add(btnPrintPreview);
			pnlBottom.Controls.Add(btnCancel);
			pnlBottom.Controls.Add(btnOk);
			pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			pnlBottom.Location = new System.Drawing.Point(0, 365);
			pnlBottom.Name = "pnlBottom";
			pnlBottom.Size = new System.Drawing.Size(510, 32);
			pnlBottom.TabIndex = 1;
			// 
			// btnPrint
			// 
			btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			btnPrint.BackColorInternal = System.Drawing.Color.Transparent;
			btnPrint.DialogResult = System.Windows.Forms.DialogResult.OK;
			btnPrint.Location = new System.Drawing.Point(412, 4);
			btnPrint.Name = "btnPrint";
			btnPrint.Size = new System.Drawing.Size(92, 24);
			btnPrint.TabIndex = 2;
			btnPrint.Text = "&Print...";
			btnPrint.Click += new System.EventHandler(btnPrint_Click);
			// 
			// btnPrintPreview
			// 
			btnPrintPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			btnPrintPreview.BackColorInternal = System.Drawing.Color.Transparent;
			btnPrintPreview.DialogResult = System.Windows.Forms.DialogResult.OK;
			btnPrintPreview.Location = new System.Drawing.Point(316, 4);
			btnPrintPreview.Name = "btnPrintPreview";
			btnPrintPreview.Size = new System.Drawing.Size(92, 24);
			btnPrintPreview.TabIndex = 2;
			btnPrintPreview.Text = "Print Pre&view...";
			btnPrintPreview.Click += new System.EventHandler(btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			btnCancel.BackColorInternal = System.Drawing.Color.Transparent;
			btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			btnCancel.Location = new System.Drawing.Point(216, 4);
			btnCancel.Name = "btnCancel";
			btnCancel.Size = new System.Drawing.Size(92, 24);
			btnCancel.TabIndex = 2;
			btnCancel.Text = "Cancel";
			// 
			// btnOk
			// 
			btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			btnOk.BackColorInternal = System.Drawing.Color.Transparent;
			btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			btnOk.Location = new System.Drawing.Point(120, 4);
			btnOk.Name = "btnOk";
			btnOk.Size = new System.Drawing.Size(92, 24);
			btnOk.TabIndex = 2;
			btnOk.Text = "OK";
			// 
			// tabControlMain
			// 
			tabControlMain.Controls.Add(ultraTabSharedControlsPage1);
			tabControlMain.Controls.Add(tabPagePaper);
			tabControlMain.Controls.Add(tabPageHeaderFooter);
			tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
			tabControlMain.Location = new System.Drawing.Point(0, 40);
			tabControlMain.Name = "tabControlMain";
			tabControlMain.SharedControlsPage = ultraTabSharedControlsPage1;
			tabControlMain.Size = new System.Drawing.Size(510, 325);
			tabControlMain.TabIndex = 2;
			tabControlMain.TabPadding = new System.Drawing.Size(10, 1);
			ultraTab9.Key = "Paper";
			ultraTab9.TabPage = tabPagePaper;
			ultraTab9.Text = "Paper";
			ultraTab10.Key = "HeaderFooter";
			ultraTab10.TabPage = tabPageHeaderFooter;
			ultraTab10.Text = "Header/Footer";
			tabControlMain.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab9,
            ultraTab10});
			// 
			// ultraTabSharedControlsPage1
			// 
			ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
			ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
			ultraTabSharedControlsPage1.Size = new System.Drawing.Size(506, 299);
			// 
			// fontDialog1
			// 
			fontDialog1.Color = System.Drawing.SystemColors.WindowText;
			fontDialog1.FontMustExist = true;
			fontDialog1.ShowColor = true;
			// 
			// printerSettingsControl1
			// 
			printerSettingsControl1.BackColor = System.Drawing.Color.Transparent;
			printerSettingsControl1.Location = new System.Drawing.Point(0, 0);
			printerSettingsControl1.Name = "printerSettingsControl1";
			printerSettingsControl1.Size = new System.Drawing.Size(503, 320);
			printerSettingsControl1.TabIndex = 0;
			// 
			// UltraPageSetup
			// 
			AcceptButton = btnOk;
			AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			CancelButton = btnCancel;
			ClientSize = new System.Drawing.Size(510, 397);
			Controls.Add(tabControlMain);
			Controls.Add(pnlBottom);
			Controls.Add(pnlTop);
			Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			MaximizeBox = false;
			MinimizeBox = false;
			Name = "UltraPageSetup";
			Text = "UltraPageSetup";
			Load += new System.EventHandler(UltraPageSetup_Load);
			tabPagePaper.ResumeLayout(false);
			tabPageHeaderFooter.ResumeLayout(false);
			tabPageHeaderFooter.PerformLayout();
			pnlToolbar.ResumeLayout(false);
			pnlToolbar.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(txtHeaderLeft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(txtHeaderFont)).EndInit();
			((System.ComponentModel.ISupportInitialize)(txtHeaderMiddle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(txtHeaderRight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(txtFooterRight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(txtFooterFont)).EndInit();
			((System.ComponentModel.ISupportInitialize)(txtFooterMiddle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(txtFooterLeft)).EndInit();
			pnlTop.ResumeLayout(false);
			pnlTop.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(cboStyleName)).EndInit();
			pnlBottom.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(tabControlMain)).EndInit();
			tabControlMain.ResumeLayout(false);
			ResumeLayout(false);

		}
		#endregion

		private MemoryStream _initialSettingsStream;
		private PageSettings _defaultPageSettings;

		#region Dialog Load
		private void UltraPageSetup_Load(object sender, System.EventArgs e)
		{
			// the toolbar buttons are disabled until one of the
			// related textboxes get focus
			pnlToolbar.Enabled = false;

			// save the state of the print document before doing anything in
			// case the dialog is cancelled.
			SaveInitialSettings();
			
			// reset the default page settings
			_printDocument.DefaultPageSettings = new PageSettings(_printDocument.PrinterSettings);
			//printDocument.DefaultPageSettings.Landscape = true;

			// ensure all the controls are populated...
			ResetControls();

			// make sure all the controls have a fore color set
			Utilities.InitializeControlForeColor(Controls);
		}
		#endregion //Dialog Load

		#region Save/Restore/Dispose InitialSettings
		private void SaveInitialSettings()
		{
			// create a memory stream to store the initial settings of 
			// the print document
			_initialSettingsStream = new MemoryStream();

			// clone the default page settings as well
			_defaultPageSettings = _printDocument.DefaultPageSettings.Clone() as PageSettings;

			_printDocument.SaveAsBinary(_initialSettingsStream);
		}

		private void RestoreInitialSettings()
		{
			System.Diagnostics.Debug.Assert(_initialSettingsStream != null, "Invalid original settings stream");

			// create a memory stream to store the initial settings of 
			// the print document
			_initialSettingsStream.Position = 0;

			_printDocument.LoadFromBinary(_initialSettingsStream);

			// clone the default page settings as well
			_printDocument.DefaultPageSettings = _defaultPageSettings;
		}

		private void DisposeInitialSettings()
		{
			System.Diagnostics.Debug.Assert(_initialSettingsStream != null, "Invalid original settings stream");

			// create a memory stream to store the initial settings of 
			// the print document
			_initialSettingsStream.Close();
			_initialSettingsStream = null;

			_defaultPageSettings = null;
		}
		#endregion //Save/Restore/Dispose InitialSettings

		#region ResetControls
		private void ResetControls()
		{
			// edit the default page settings
			printerSettingsControl1.PageSettings = _printDocument.DefaultPageSettings;

			// show the style name in the dialog text
			Text = string.Format("UltraPageSetup: {0}", cboStyleName.Text);
		}
		#endregion //ResetControls

		#region Header/Footer TextBox Enter/Leave
		private void OnHeaderFooterTextBoxEnter(object sender, System.EventArgs e)
		{
			pnlToolbar.Enabled = true;
		}

		private void OnHeaderFooterTextBoxLeave(object sender, System.EventArgs e)
		{
			pnlToolbar.Enabled = false;
		}
		#endregion //Header/Footer TextBox Enter/Leave

		#region Header/Footer ToolbarButtonClick
		private void OnToolbarButtonClick(object sender, System.EventArgs e)
		{
			string token = null;

			if (sender == btnToolbarDate)
				token = "[Date Printed]";
			else if (sender == btnToolbarTime)
				token = "[Time Printed]";
			else if (sender == btnToolbarUserName)
				token = "[User Name]";
			else if (sender == btnToolbarPageNumber)
				token = "[Page #]";
			else
				return;

			Control activeControl = ActiveControl;

			if (activeControl is TextBox)
				((TextBox)activeControl).SelectedText = token;
			else if (activeControl is UltraTextEditor)
				((UltraTextEditor)activeControl).SelectedText = token;
		}
		#endregion //Header/Footer ToolbarButtonClick
	 
		#region GetTabKey
	 
		#endregion //GetTabKey

		#region Header/Footer TextChanged
		private void txtHeaderLeft_ValueChanged(object sender, System.EventArgs e)
		{
			_printDocument.Header.TextLeft = ((UltraTextEditor)sender).Text;
		}

		private void txtHeaderMiddle_ValueChanged(object sender, System.EventArgs e)
		{
			_printDocument.Header.TextCenter = ((UltraTextEditor)sender).Text;
		}

		private void txtHeaderRight_ValueChanged(object sender, System.EventArgs e)
		{
			_printDocument.Header.TextRight = ((UltraTextEditor)sender).Text;
		}

		private void txtFooterLeft_ValueChanged(object sender, System.EventArgs e)
		{
			_printDocument.Footer.TextLeft = ((UltraTextEditor)sender).Text;
		}

		private void txtFooterMiddle_ValueChanged(object sender, System.EventArgs e)
		{
			_printDocument.Footer.TextCenter = ((UltraTextEditor)sender).Text;
		}

		private void txtFooterRight_ValueChanged(object sender, System.EventArgs e)
		{
			_printDocument.Footer.TextRight = ((UltraTextEditor)sender).Text;
		}
		#endregion //Header/Footer TextChanged

		#region ReverseOnEvenPages CheckChanged
		private void chkReverseOnEvenPages_CheckedChanged(object sender, System.EventArgs e)
		{
			_printDocument.Header.ReverseTextOnEvenPages = 
				_printDocument.Footer.ReverseTextOnEvenPages = ((UltraCheckEditor)sender).Checked;
		}
		#endregion //ReverseOnEvenPages CheckChanged

		#region Dialog Closing
		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing(e);

			if (e.Cancel)
				return;

			// if the dialog was closed via one of the "success"
			// buttons (ok, print, print preview), then save
			// this as the initial state for the print style
			if (DialogResult == DialogResult.OK)
			{
				//SavePrintStyleSettings();
			}
			else
			{
				// otherwise, put the document
				// back to its original state
				RestoreInitialSettings();
			}

			// in either case, clean up the initial
			// settings memory stream
			DisposeInitialSettings();
		}
		#endregion //Dialog Closing

		#region PrintPreview/Print Buttons and Events
		/// <summary>
		/// Invoked when the print preview button has been clicked.
		/// </summary>
		public event EventHandler PrintPreviewRequested;

		/// <summary>
		/// Invoked when the print button has been clicked.
		/// </summary>
		public event EventHandler PrintDialogRequested;

		/// <summary>
		/// Used to invoke the <see cref="PrintPreviewRequested"/>
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnPrintPreviewRequested(EventArgs e)
		{
			if (PrintPreviewRequested != null)
				PrintPreviewRequested(this, e);
		}

		/// <summary>
		/// Used to invoke the <see cref="PrintDialogRequested"/>
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnPrintDialogRequested(EventArgs e)
		{
			if (PrintDialogRequested != null)
				PrintDialogRequested(this, e);
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			OnPrintPreviewRequested(EventArgs.Empty);
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			OnPrintDialogRequested(EventArgs.Empty);
		}
		#endregion //PrintPreview/Print Buttons and Events


	}
}
