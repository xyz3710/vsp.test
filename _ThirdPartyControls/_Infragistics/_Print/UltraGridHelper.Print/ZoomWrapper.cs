﻿using System;
using System.Collections.Generic;
using System.Text;
using Infragistics.Win.Printing;

namespace AnyFactory.FX.Win.Controls.UltraGridHelper.Print
{
	partial class PrintPreviewForm
	{
		private class ZoomWrapper
		{
			// Fields
			private bool _useZoomMode;
			private double _zoomRatio;
			private ZoomMode _zoomMode;

			// Methods
			internal ZoomWrapper(ZoomMode zoomMode)
			{
				_zoomRatio = 1.0;
				_zoomMode = zoomMode;
				_useZoomMode = true;
			}

			internal ZoomWrapper(double zoom)
			{
				_zoomRatio = zoom;
				_useZoomMode = false;
			}

			public override bool Equals(object obj)
			{
				PrintPreviewForm.ZoomWrapper wrapper = obj as PrintPreviewForm.ZoomWrapper;

				if (wrapper == null)
					return false;

				return (((wrapper._zoomRatio == _zoomRatio) && (wrapper._zoomMode == _zoomMode)) && (wrapper._useZoomMode == _useZoomMode));
			}

			public override int GetHashCode()
			{
				return base.GetHashCode();
			}

			internal void UpdateZoom(PreviewSettings settings)
			{
				if (_useZoomMode)
				{
					settings.ZoomMode = _zoomMode;
				}
				else
				{
					settings.Zoom = _zoomRatio;
					if ((settings.ZoomMode != ZoomMode.Standard) && (settings.ZoomMode != ZoomMode.AutoFlow))
					{
						settings.ZoomMode = ZoomMode.Standard;
					}
				}
			}
		}
	}
}
