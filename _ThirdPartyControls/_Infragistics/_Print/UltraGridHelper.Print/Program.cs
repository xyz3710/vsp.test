﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using Infragistics.Win.UltraWinGrid;

namespace AnyFactory.FX.Win.Controls.UltraGridHelper.Print
{
	class Program
	{
		private static void Main()
		{
			UltraGrid ultraGrid = new UltraGrid();
			DataTable dataTable = new DataTable();

			dataTable.Columns.Add("column1", typeof(string));
			dataTable.Columns.Add("column2", typeof(string));

			dataTable.Rows.Add("value1", "item1");
			dataTable.Rows.Add("value2", "item2");
			dataTable.Rows.Add("value3", "item3");
			dataTable.Rows.Add("value4", "item4");
			dataTable.Rows.Add("value5", "item5");
			dataTable.Rows.Add("value6", "item6");

			ultraGrid.DataSource = dataTable;

			PrintPreviewForm newF0001 = new PrintPreviewForm(ultraGrid);
			newF0001.ShowDialog();
		}
	}
}
