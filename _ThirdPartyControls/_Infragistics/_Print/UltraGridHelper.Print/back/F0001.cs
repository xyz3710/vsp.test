﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Win.Mes.PrintPreview
/*	Creator		:	iDASiT (YHJUN)
/*	Create		:	2008-01-20 오후 4:39:25
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	base.Popup("M02F0001", ultraGrid1);
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;
using AnyFactory.FX.Win;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.Printing;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinStatusBar;
using Infragistics.Win.UltraWinToolbars;

namespace iDASiT.Win.Mes
{
	/// <summary>
	/// PrintPreview 클래스를 선언합니다.
	/// </summary>
	public partial class F0001 : BaseForm
	{
		#region Constants
		private const string FILE_KEY = "File";
		private const string PAGE_SETUP_KEY = "Page Setup";
		private const string PRINT_KEY = "Print";
		private const string EXIT_KEY = "Exit";
		private const string VIEW_KEY = "View";
		private const string THUMBNAILS_KEY = "Thumbnails";
		private const string ZOOMIN_KEY = "Zoom In";
		private const string ZOOMOUT_KEY = "Zoom Out";
		private const string ZOOM_KEY = "Zoom";
		private const string PAGEWIDTH_KEY = "Page Width";
		private const string MARGINWIDTH_KEY = "Margin Width";
		private const string WHOLEPAGE_KEY = "Whole Page";
		private const string GOTO_KEY = "Go To";
		private const string FIRSTPAGE_KEY = "First Page";
		private const string PREVIOUSPAGE_KEY = "Previous Page";
		private const string NEXTPAGE_KEY = "Next Page";
		private const string LASTPAGE_KEY = "Last Page";
		private const string PREVIOUSVIEW_KEY = "Previous View";
		private const string NEXTVIEW_KEY = "Next View";
		private const string TOOLS_KEY = "Tools";
		private const string HANDTOOL_KEY = "Hand Tool";
		private const string SNAPSHOTTOOL_KEY = "Snapshot Tool";
		private const string DYNAMICZOOMTOOL_KEY = "Dynamic Zoom Tool";
		private const string ZOOMINTOOL_KEY = "Zoom In Tool";
		private const string ZOOMOUTTOOL_KEY = "Zoom Out Tool";
		private const string CURRENTPAGE_KEY = "Current Page";
		private const string CONTINUOUS_KEY = "Continuous";
		private const string PAGELAYOUT_KEY = "Page Layout";
		private const string CLOSEPREVIEW_KEY = "ClosePreview";
		private const string CONTEXTMENUTHUMBNAIL_KEY = "ContextMenuThumbnail";
		private const string REDUCEPAGETHUMBNAILS_KEY = "Reduce Page Thumbnails";
		private const string ENLARGEPAGETHUMBNAILS_KEY = "Enlarge Page Thumbnails";
		private const string SHOWPAGENUMBERS_KEY = "Show Page Numbers";
		private const string CONTEXTMENUPREVIEWHAND_KEY = "ContextMenuPreviewHand";
		private const string CONTEXTMENUPREVIEWZOOM_KEY = "ContextMenuPreviewZoom";
		private const string ZOOMINCREMENTS_KEY = "Zoom Increments";
		private const string STANDARD_KEY = "Standard";
		private const string MENUBAR_KEY = "MenuBar";
		private const string PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY = "PrintPreview_ToolCategory_View";
		private const string PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY = "PrintPreview_ToolCategory_Tools";
		private const string PRINTPREVIEW_TOOLCATEGORY_FILE_KEY = "PrintPreview_ToolCategory_File";
		private const string PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY = "PrintPreview_ToolCategory_Menus";
		private const string PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY = "PrintPreview_ToolCategory_Zoom_Mode";
		private const string PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY = "PrintPreview_ToolCategory_Context_Menus";
		#endregion

		private class ZoomWrapper
		{
			// Fields
			private bool useZoomMode;
			private double zoom;
			private ZoomMode zoomMode;

			// Methods
			internal ZoomWrapper(ZoomMode zoomMode)
			{
				this.zoom = 1.0;
				this.zoomMode = zoomMode;
				this.useZoomMode = true;
			}

			internal ZoomWrapper(double zoom)
			{
				this.zoom = zoom;
				this.useZoomMode = false;
			}

			public override bool Equals(object obj)
			{
				F0001.ZoomWrapper wrapper = obj as F0001.ZoomWrapper;
				if (wrapper == null)
				{
					return false;
				}
				return (((wrapper.zoom == this.zoom) && (wrapper.zoomMode == this.zoomMode)) && (wrapper.useZoomMode == this.useZoomMode));
			}

			public override int GetHashCode()
			{
				return base.GetHashCode();
			}

			internal void UpdateZoom(PreviewSettings settings)
			{
				if (this.useZoomMode)
				{
					settings.ZoomMode = this.zoomMode;
				}
				else
				{
					settings.Zoom = this.zoom;
					if ((settings.ZoomMode != ZoomMode.Standard) && (settings.ZoomMode != ZoomMode.AutoFlow))
					{
						settings.ZoomMode = ZoomMode.Standard;
					}
				}
			}

		}

		internal class NativeWindowMethods
		{
			// Fields
			private const int LOCALE_IMEASURE = 13;
			private const int LOCALE_USER_DEFAULT = 0x400;

			// Methods
			[DllImport("kernel32", CharSet=CharSet.Auto)]
			private static extern int GetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.LPWStr)] string lpLCData, int cchData);
			private static int GetLocaleInfoApi(int Locale, int LCType, string lpLCData, int cchData)
			{
				return GetLocaleInfo(Locale, LCType, lpLCData, cchData);
			}

			internal static bool IsMetric()
			{
				try
				{
					new SecurityPermission(SecurityPermissionFlag.UnmanagedCode).Assert();
					string lpLCData = new string(' ', 2);
					GetLocaleInfoApi(0x400, 13, lpLCData, lpLCData.Length);
					return (string.Compare(lpLCData, "0") == 0);
				}
				catch (Exception)
				{
					return RegionInfo.CurrentRegion.IsMetric;
				}
			}
		}
		

		// Events
		public event PageSetupDialogDisplayingEventHandler PageSetupDialogDisplaying
		{
			add
			{
				base.Events.AddHandler(EventPageSetupDialogDisplaying, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventPageSetupDialogDisplaying, value);
			}
		}
		public event EventHandler Printed
		{
			add
			{
				base.Events.AddHandler(EventPrinted, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventPrinted, value);
			}
		}
		public event PrintingEventHandler Printing
		{
			add
			{
				base.Events.AddHandler(EventPrinting, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventPrinting, value);
			}
		}
 
		#region Fields

		private bool canResetTrackMouseEvent = false;
		private IContainer container;
		private bool displayPrintStatus;
		private static readonly object EventPageSetupDialogDisplaying;
		private static readonly object EventPrinted;
		private static readonly object EventPrinting;
		private bool isFirstLoad = false;
		private int lastRowsSetting;
		private PageSetupDialog pageSetupDialog;
		private PrintDialog printDialog;
		private PreviewRowColumnSelector rowColumnSelector;
		private bool statusBarVisible = false;
		private bool thumbnailAreaVisible;

		public object About
		{
			get
			{
				return null;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new IButtonControl AcceptButton
		{
			get
			{
				return base.AcceptButton;
			}
			set
			{
				base.AcceptButton = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new string AccessibleDescription
		{
			get
			{
				return base.AccessibleDescription;
			}
			set
			{
				base.AccessibleDescription = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new string AccessibleName
		{
			get
			{
				return base.AccessibleName;
			}
			set
			{
				base.AccessibleName = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new AccessibleRole AccessibleRole
		{
			get
			{
				return base.AccessibleRole;
			}
			set
			{
				base.AccessibleRole = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool AllowDrop
		{
			get
			{
				return base.AllowDrop;
			}
			set
			{
				base.AllowDrop = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new AnchorStyles Anchor
		{
			get
			{
				return base.Anchor;
			}
			set
			{
				base.Anchor = value;
			}
		}

		[DefaultValue(true)]
		public bool AutoGeneratePreview
		{
			get
			{
				return this.ultraPrintPreviewControl.AutoGeneratePreview;
			}
			set
			{
				this.ultraPrintPreviewControl.AutoGeneratePreview = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new Size AutoScaleBaseSize
		{
			get
			{
				return base.AutoScaleBaseSize;
			}
			set
			{
				base.AutoScaleBaseSize = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new bool AutoScroll
		{
			get
			{
				return base.AutoScroll;
			}
			set
			{
				base.AutoScroll = value;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new Size AutoScrollMargin
		{
			get
			{
				return base.AutoScrollMargin;
			}
			set
			{
				base.AutoScrollMargin = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new Size AutoScrollMinSize
		{
			get
			{
				return base.AutoScrollMinSize;
			}
			set
			{
				base.AutoScrollMinSize = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}
				
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Image BackgroundImage
		{
			get
			{
				return base.BackgroundImage;
			}
			set
			{
				base.BackgroundImage = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new ImageLayout BackgroundImageLayout
		{
			get
			{
				return base.BackgroundImageLayout;
			}
			set
			{
				base.BackgroundImageLayout=(value);
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new IButtonControl CancelButton
		{
			get
			{
				return base.CancelButton;
			}
			set
			{
				base.CancelButton = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new bool CausesValidation
		{
			get
			{
				return base.CausesValidation;
			}
			set
			{
				base.CausesValidation = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new Size ClientSize
		{
			get
			{
				return base.ClientSize;
			}
			set
			{
				base.ClientSize = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ContextMenu ContextMenu
		{
			get
			{
				return base.ContextMenu;
			}
			set
			{
				base.ContextMenu = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ControlBox
		{
			get
			{
				return base.ControlBox;
			}
			set
			{
				base.ControlBox = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Cursor Cursor
		{
			get
			{
				return base.Cursor;
			}
			set
			{
				base.Cursor = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ControlBindingsCollection DataBindings
		{
			get
			{
				return base.DataBindings;
			}
		}
		
		[DefaultValue(true)]
		public bool DisplayPreviewStatus
		{
			get
			{
				return this.ultraPrintPreviewControl.DisplayPreviewStatus;
			}
			set
			{
				this.ultraPrintPreviewControl.DisplayPreviewStatus = value;
			}
		}
		
		[DefaultValue(true)]
		public bool DisplayPrintStatus
		{
			get
			{
				return this.displayPrintStatus;
			}
			set
			{
				this.displayPrintStatus = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new DockStyle Dock
		{
			get
			{
				return base.Dock;
			}
			set
			{
				base.Dock = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new ScrollableControl.DockPaddingEdges DockPadding
		{
			get
			{
				return base.DockPadding;
			}
		}
		
		[DefaultValue((string)null)]
		public PrintDocument Document
		{
			get
			{
				return this.ultraPrintPreviewControl.Document;
			}
			set
			{
				this.ultraPrintPreviewControl.Document = value;
				this.pageSetupDialog.Document = null;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new FormBorderStyle FormBorderStyle
		{
			get
			{
				return base.FormBorderStyle;
			}
			set
			{
				base.FormBorderStyle = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new bool HelpButton
		{
			get
			{
				return base.HelpButton;
			}
			set
			{
				base.HelpButton = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Icon Icon
		{
			get
			{
				return base.Icon;
			}
			set
			{
				base.Icon = value;
			}
		}
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new ImeMode ImeMode
		{
			get
			{
				return base.ImeMode;
			}
			set
			{
				base.ImeMode = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool IsMdiContainer
		{
			get
			{
				return base.IsMdiContainer;
			}
			set
			{
				base.IsMdiContainer = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool KeyPreview
		{
			get
			{
				return base.KeyPreview;
			}
			set
			{
				base.KeyPreview = value;
			}
		}
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Point Location
		{
			get
			{
				return base.Location;
			}
			set
			{
				base.Location = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool MaximizeBox
		{
			get
			{
				return base.MaximizeBox;
			}
			set
			{
				base.MaximizeBox = value;
			}
		}

		[DefaultValue(0)]
		public int MaximumPreviewPages
		{
			get
			{
				return this.ultraPrintPreviewControl.MaximumPreviewPages;
			}
			set
			{
				this.ultraPrintPreviewControl.MaximumPreviewPages = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size MaximumSize
		{
			get
			{
				return base.MaximumSize;
			}
			set
			{
				base.MaximumSize = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new MainMenu Menu
		{
			get
			{
				return base.Menu;
			}
			set
			{
				base.Menu = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool MinimizeBox
		{
			get
			{
				return base.MinimizeBox;
			}
			set
			{
				base.MinimizeBox = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size MinimumSize
		{
			get
			{
				return base.MinimumSize;
			}
			set
			{
				base.MinimumSize = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced)]
		public new double Opacity
		{
			get
			{
				return base.Opacity;
			}
			set
			{
				base.Opacity = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new Padding Padding
		{
			get
			{
				return base.Padding;
			}
			set
			{
				base.Padding = (value);
			}
		}
		
		[DefaultValue(1)]
		public PreviewMouseAction PreviewMouseAction
		{
			get
			{
				return this.ultraPrintPreviewControl.MouseAction;
			}
			set
			{
				this.ultraPrintPreviewControl.MouseAction = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PreviewSettings PreviewSettings
		{
			get
			{
				return this.ultraPrintPreviewControl.Settings;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public UltraPrintPreviewControl PrintPreviewControl
		{
			get
			{
				return this.ultraPrintPreviewControl;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraPrintPreviewThumbnail PrintPreviewThumbnail
		{
			get
			{
				return this.ultraPrintPreviewThumbnail;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new RightToLeft RightToLeft
		{
			get
			{
				return base.RightToLeft;
			}
			set
			{
				base.RightToLeft = value;
			}
		}
				
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ShowInTaskbar
		{
			get
			{
				return base.ShowInTaskbar;
			}
			set
			{
				base.ShowInTaskbar = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Size Size
		{
			get
			{
				return base.Size;
			}
			set
			{
				base.Size = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new SizeGripStyle SizeGripStyle
		{
			get
			{
				return base.SizeGripStyle;
			}
			set
			{
				base.SizeGripStyle = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new FormStartPosition StartPosition
		{
			get
			{
				return base.StartPosition;
			}
			set
			{
				base.StartPosition = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraStatusBar StatusBar
		{
			get
			{
				return this.ultraStatusBar1;
			}
		}

		[DefaultValue(true)]
		public bool StatusBarVisible
		{
			get
			{
				return this.statusBarVisible;
			}
			set
			{
				this.statusBarVisible = value;
				this.ultraStatusBar1.Visible = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new bool TabStop
		{
			get
			{
				return base.TabStop;
			}
			set
			{
				base.TabStop = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new object Tag
		{
			get
			{
				return base.Tag;
			}
			set
			{
				base.Tag = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}
		
		[DefaultValue(true)]
		public bool ThumbnailAreaVisible
		{
			get
			{
				return this.thumbnailAreaVisible;
			}
			set
			{
				this.thumbnailAreaVisible = value;

				this.ultraPrintPreviewThumbnail.Visible = value;
				if (value == true)
				{
					this.splitContainer.SplitterDistance = 176;
				}
				else
				{
					this.splitContainer.SplitterDistance = 0;
				}
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailCurrentPreviewPageAppearance
		{
			get
			{
				return this.ultraPrintPreviewThumbnail.CurrentPreviewPageAppearance;
			}
			set
			{
				this.ultraPrintPreviewThumbnail.CurrentPreviewPageAppearance = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailCurrentPreviewPageNumberAppearance
		{
			get
			{
				return this.ultraPrintPreviewThumbnail.CurrentPreviewPageNumberAppearance;
			}
			set
			{
				this.ultraPrintPreviewThumbnail.CurrentPreviewPageNumberAppearance = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PreviewSettings ThumbnailSettings
		{
			get
			{
				return this.ultraPrintPreviewThumbnail.Settings;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailViewBoxAppearance
		{
			get
			{
				return this.ultraPrintPreviewThumbnail.ViewBoxAppearance;
			}
			set
			{
				this.ultraPrintPreviewThumbnail.ViewBoxAppearance = value;
			}
		}

		[DefaultValue(0)]
		public ViewBoxDisplayStyle ThumbnailViewBoxDisplayStyle
		{
			get
			{
				return this.ultraPrintPreviewThumbnail.ViewBoxDisplayStyle;
			}
			set
			{
				this.ultraPrintPreviewThumbnail.ViewBoxDisplayStyle = value;
			}
		}
		
		[DefaultValue(0)]
		public ViewBoxDragMode ThumbnailViewBoxDragMode
		{
			get
			{
				return this.ultraPrintPreviewThumbnail.ViewBoxDragMode;
			}
			set
			{
				this.ultraPrintPreviewThumbnail.ViewBoxDragMode = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool TopMost
		{
			get
			{
				return base.TopMost;
			}
			set
			{
				base.TopMost = value;
			}
		}
		
		[DefaultValue(false)]
		public bool UseAntiAlias
		{
			get
			{
				return this.ultraPrintPreviewControl.UseAntiAlias;
			}
			set
			{
				this.ultraPrintPreviewControl.UseAntiAlias = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool Visible
		{
			get
			{
				return base.Visible;
			}
			set
			{
				base.Visible = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new FormWindowState WindowState
		{
			get
			{
				return base.WindowState;
			}
			set
			{
				base.WindowState = value;
			}
		}
 
		#endregion

		#region Constructor
		/// <summary>
		/// PrintPreview의 인스턴스를 생성합니다.
		/// </summary>
		public F0001()
			: base()
		{
			this.thumbnailAreaVisible = true;
			this.displayPrintStatus = true;
			this.statusBarVisible = true;
			this.isFirstLoad = true;
			this.canResetTrackMouseEvent = true;
			
			//초기화
			this.InitializeComponent();
			InitializeForm();

			this.rowColumnSelector = new PreviewRowColumnSelector();
			this.rowColumnSelector.Location = new Point(-10000, -10000);
			base.Controls.Add(this.rowColumnSelector);
			this.rowColumnSelector.MouseUp += new MouseEventHandler(this.OnRowColumnSelectorMouseUp);

			//this.InitializeDialogStrings();
			base.Name = "";
			this.ShowInTaskbar = false;
			this.InitializeZoomCombo();
			this.UpdateMouseActionTools();

			base.ThemeStyle = ThemeStyle;
		}
	 
		public F0001(IContainer container)
			: this()
		{
			this.container = container;
			if (this.container != null)
			{
				this.container.Add(this);
			}
		}

		public F0001(UltraGrid xgridMain)
			: this()
		{
			if (xgridMain != null)
			{
				ultraGridPrintDocument.Grid = xgridMain;
				this.Document = ultraGridPrintDocument;
			}
		}

		static F0001()
		{
			EventPageSetupDialogDisplaying = new object();
			EventPrinting = new object();
			EventPrinted = new object();
		}

		#endregion

		#region InitializeForm
		private void InitializeForm()
		{
			// Form 초기화 method를 삽입합니다.


			base.ThemeStyle = ThemeStyle;
		}
		#endregion

		#region Toolbar Event Handler
		/// <summary>
		/// 표준 툴바의 [신규] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnNewClick()
		{
			base.OnNewClick();


		}

		/// <summary>
		/// 표준 툴바의 [저장] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnSaveClick()
		{
			base.OnSaveClick();


		}

		/// <summary>
		/// 표준 툴바의 [조회] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnSearchClick()
		{
			base.OnSearchClick();


		}

		/// <summary>
		/// 표준 툴바의 [삭제] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnDeleteClick()
		{
			base.OnDeleteClick();


		}

		/// <summary>
		/// 표준 툴바의 [실행] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnRunClick()
		{
			base.OnRunClick();


		}

		/// <summary>
		/// 표준 툴바의 [취소] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnCancelClick()
		{
			base.OnCancelClick();


		}

		/// <summary>
		/// 표준 툴바의 [행 추가] 버튼이 눌렸을 때<br/>
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnAddRowClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnAddRowClick();
		}

		/// <summary>
		/// 표준 툴바의 [행 삭제] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnDelRowClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnDelRowClick();
		}

		/// <summary>
		/// 표준 툴바의 [엑셀] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnExcelClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnExcelClick();
		}

		/// <summary>
		/// 표준 툴바의 [미리보기] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnPreviewClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnPreviewClick();
		}

		/// <summary>
		/// 표준 툴바의 [인쇄] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnPrintClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnPrintClick();
		}

		/// <summary>
		/// 표준 툴바의 [창닫기] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnExitClick()
		{
			base.OnExitClick();
		}
		#endregion

		#region Properties

		#endregion

		#region Event Methods
		private void PrintPreview_Shown(object sender, EventArgs e)
		{

		}


		private void PrintPreview_Load(object sender, EventArgs e)
		{


		}

		private void ultraToolbarsManager2_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
		{
			switch (e.Tool.Key)
			{
				case "Hand Tool":
					this.ultraPrintPreviewControl.MouseAction = PreviewMouseAction.Hand;
					return;

				case "Snapshot Tool":
					this.ultraPrintPreviewControl.MouseAction = PreviewMouseAction.Snapshot;
					return;

				case "Dynamic Zoom Tool":
					this.ultraPrintPreviewControl.MouseAction = PreviewMouseAction.DynamicZoom;
					return;

				case "Zoom Out Tool":
					this.ultraPrintPreviewControl.MouseAction = PreviewMouseAction.ZoomOut;
					return;

				case "Zoom In Tool":
					this.ultraPrintPreviewControl.MouseAction = PreviewMouseAction.ZoomIn;
					return;

				case "Zoom In":
					this.ultraPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ZoomIn);
					return;

				case "Zoom Out":
					this.ultraPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ZoomOut);
					return;

				case "Page Width":
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.UseStandardZoomModeIfNecessary();
						return;
					}
					this.ultraPrintPreviewControl.Settings.ZoomMode = ZoomMode.PageWidth;
					return;

				case "Margin Width":
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.UseStandardZoomModeIfNecessary();
						return;
					}
					this.ultraPrintPreviewControl.Settings.ZoomMode = ZoomMode.MarginsWidth;
					return;

				case "Whole Page":
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.UseStandardZoomModeIfNecessary();
						return;
					}
					this.ultraPrintPreviewControl.Settings.ZoomMode = ZoomMode.WholePage;
					return;

				case "Zoom Increments":
					this.UpdateUsingZoomIncrementsList(e.ListToolItem);

					return;

				case "Thumbnails":
					base.SuspendLayout();
					if (((StateButtonTool)e.Tool).Checked)
					{
						this.ultraPrintPreviewThumbnail.SendToBack();
					}
					this.ThumbnailAreaVisible = ((StateButtonTool)e.Tool).Checked;
					base.ResumeLayout(true);
					return;

				case "Print":
					if (this.ultraPrintPreviewControl.Document != null)
					{
						// Allow the user to choose the page range he or she would
						// like to print.
						printDialog.AllowSomePages = true;
						// Show the help button.
						printDialog.ShowHelp = true;

						this.printDialog.Document = ultraPrintPreviewControl.Document;

						DialogResult result = printDialog.ShowDialog(this);

						// If the result is OK then print the document.
						if (result==DialogResult.OK)
						{
							//docToPrint.Print();
							PrintingEventArgs args = new PrintingEventArgs(this.displayPrintStatus);
							this.OnPrinting(args);
							if (args.Cancel)
							{
								break;
							}
							this.ultraPrintPreviewControl.Print(args.DisplayPrintStatus);
							this.OnPrinted(new EventArgs());
							return;
						}
					}
					return;

				case "Exit":
				case "ClosePreview":
					if (!base.Modal)
					{
						base.Close();
						return;
					}
					base.DialogResult = DialogResult.OK;
					return;

				case "Page Setup":
					this.ShowPageSetupDialog();
					return;

				case "Previous View":
					if (!this.ultraPrintPreviewControl.HasPreviousView)
					{
						break;
					}
					this.ultraPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.PreviousView);
					return;

				case "Next View":
					if (!this.ultraPrintPreviewControl.HasNextView)
					{
						break;
					}
					this.ultraPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.NextView);
					return;

				case "First Page":
					this.ultraPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ScrollToFirstPage);
					return;

				case "Last Page":
					this.ultraPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ScrollToLastPage);
					return;

				case "Previous Page":
					this.ultraPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ScrollToPreviousPage);
					return;

				case "Next Page":
					this.ultraPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ScrollToNextPage);
					return;

				case "Continuous":
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.ultraPrintPreviewControl.Settings.Rows = Math.Max(1, this.lastRowsSetting);
						return;
					}
					this.ultraPrintPreviewControl.Settings.Rows = 0;
					return;

				case "Reduce Page Thumbnails":
					this.ultraPrintPreviewThumbnail.PerformAction(UltraPrintPreviewThumbnailAction.ZoomOut);
					return;

				case "Enlarge Page Thumbnails":
					this.ultraPrintPreviewThumbnail.PerformAction(UltraPrintPreviewThumbnailAction.ZoomIn);
					return;

				case "Show Page Numbers":
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.ultraPrintPreviewThumbnail.Settings.PageNumberDisplayStyle = PageNumberDisplayStyle.None;
						break;
					}
					this.ultraPrintPreviewThumbnail.Settings.ResetPageNumberDisplayStyle();
					return;

				default:
					return;
			}

		}

		private void ultraToolbarsManager2_ToolKeyDown(object sender, Infragistics.Win.UltraWinToolbars.ToolKeyEventArgs e)
		{
			if (e.Tool != null)
			{
				bool flag = false;
				string key = e.Tool.Key;
				if (key != null)
				{
					if (!(key == "Zoom"))
					{
						if ((key == "Current Page") && (e.KeyData == Keys.Return))
						{
							TextBoxTool tool2 = e.Tool as TextBoxTool;
							double result = 1.0;
							try
							{
								flag = double.TryParse(tool2.Text, NumberStyles.Integer, null, out result);
							}
							catch (Exception)
							{
							}
							if ((flag && (result > 0.0)) && (result <= this.ultraPrintPreviewControl.PageCount))
							{
								this.ultraPrintPreviewControl.CurrentPage = (int)result;
								tool2.SelectionStart = 0;
								tool2.SelectionLength = tool2.Text.Length;
							}
						}
					}
					else if (e.KeyData == Keys.Return)
					{
						ComboBoxTool tool = e.Tool as ComboBoxTool;
						if (tool.SelectedIndex >= 0)
						{
							((ZoomWrapper)tool.Value).UpdateZoom(this.ultraPrintPreviewControl.Settings);
						}
						else
						{
							double num = 0.0;
							string s = tool.Text.Replace("%", "");
							try
							{
								flag = double.TryParse(s, NumberStyles.Float, null, out num);
							}
							catch (Exception)
							{
							}
							if (flag)
							{
								if (num < 0.0)
								{
									num = -num;
								}
								new ZoomWrapper(num / 100.0).UpdateZoom(this.ultraPrintPreviewControl.Settings);
							}
						}
					}
				}
			}

		}

		private void ultraToolbarsManager2_BeforeToolbarListDropdown(object sender, Infragistics.Win.UltraWinToolbars.BeforeToolbarListDropdownEventArgs e)
		{
			if (((e.Tool.Key == "Page Layout") && (this.ultraPrintPreviewControl != null)) && (this.ultraPrintPreviewControl.PageCount == 0))
			{
				e.Cancel = true;
			}

		}

		private void ultraToolbarsManager2_ToolValueChanged(object sender, Infragistics.Win.UltraWinToolbars.ToolEventArgs e)
		{
			string str;
			if (((str = e.Tool.Key) != null) && (str == "Zoom"))
			{
				ComboBoxTool tool = e.Tool as ComboBoxTool;
				if ((tool.SelectedIndex >= 0) && object.Equals(tool.Value, tool.ValueList.ValueListItems[tool.SelectedIndex].DataValue))
				{
					((ZoomWrapper)tool.ValueList.ValueListItems[tool.SelectedIndex].DataValue).UpdateZoom(this.ultraPrintPreviewControl.Settings);
				}
			}

		}

		private void ultraPrintPreviewControl1_CurrentPageChanged(object sender, EventArgs e)
		{
			this.UpdateCurrentPageInfo();

		}

		private void ultraPrintPreviewControl1_CurrentZoomChanged(object sender, EventArgs e)
		{
			this.UpdateZoomComboAndList();

		}

		private void ultraPrintPreviewControl1_PreviewGenerated(object sender, EventArgs e)
		{
			this.rowColumnSelector.Columns = Math.Max(1, Math.Min(this.ultraPrintPreviewControl.PageCount, 8));
			this.rowColumnSelector.Rows = Math.Max(1, Math.Min(this.ultraPrintPreviewControl.PageCount, 5));
			this.xToolbarsManagerPreview.Tools["Page Layout"].SharedProps.Enabled = this.ultraPrintPreviewControl.PageCount > 0;
			this.UpdateMouseActionTools();
			this.UpdateCurrentPageInfo();

		}

		private void ultraPrintPreviewControl1_PropertyChanged(object sender, Infragistics.Win.PropertyChangedEventArgs e)
		{
			PropChangeInfo info = e.ChangeInfo.FindTrigger(null);
			if (info.PropId is UltraPrintPreviewControlPropertyIds)
			{
				UltraPrintPreviewControlPropertyIds propId = (UltraPrintPreviewControlPropertyIds)info.PropId;
				if (propId <= UltraPrintPreviewControlPropertyIds.ZoomMode)
				{
					if (propId != UltraPrintPreviewControlPropertyIds.Document)
					{
						if (propId == UltraPrintPreviewControlPropertyIds.ZoomMode)
						{
							this.UpdateZoomModeTools();
						}
						return;
					}
				}
				else
				{
					switch (propId)
					{
						case UltraPrintPreviewControlPropertyIds.Rows:
							this.UpdateContinuousTool();
							return;

						case UltraPrintPreviewControlPropertyIds.MouseAction:
							this.UpdateMouseActionTools();
							return;

						case UltraPrintPreviewControlPropertyIds.Settings:
							this.UpdateZoomModeTools();
							this.UpdateMouseActionTools();
							this.UpdateContinuousTool();
							return;
					}
					return;
				}
				this.UpdateDocumentRelatedTools();
			}

		}

		private void ultraPrintPreviewControl1_ViewHistoryChanged(object sender, EventArgs e)
		{
			this.UpdateViewHistoryTools();

		}
		#endregion
		
		#region User defined Methods

		private void ClosePopupContainingTool(ToolBase tool)
		{
			if (tool != null)
			{
				if (tool.OwnerIsMenu)
				{
					this.ClosePopupContainingTool(tool.OwningMenu);
				}
				else if (tool.OwnerIsToolbar && (tool.OwningToolbar.TearawayToolOwner != null))
				{
					this.ClosePopupContainingTool(tool.OwningToolbar.TearawayToolOwner);
				}
				else if (tool is PopupToolBase)
				{
					((PopupToolBase)tool).ClosePopup();
				}
			}
		}

		private string FormatPercent(double percent)
		{
			double num = percent * 100.0;
			if ((num % 1.0) == 0.0)
			{
				return percent.ToString("P0");
			}
			if ((num % 0.1) == 0.0)
			{
				return percent.ToString("P1");
			}
			return percent.ToString("P2");
		}

		private void InitializeDialogStrings()
		{
			this.Text = "PrintPreview_DialogCaption";
			this.ultraPrintPreviewControl.AccessibleName = "AccessibleName_Preview";
			this.ultraPrintPreviewThumbnail.AccessibleName = "AccessibleName_Thumbnail";
			this.ultraStatusBar1.AccessibleName = "AccessibleName_StatusBar";
			this.ultraPrintPreviewControl.AccessibleDescription = "AccessibleDescription_Preview";
			this.ultraPrintPreviewThumbnail.AccessibleDescription = "AccessibleDescription_Thumbnail";
			this.ultraStatusBar1.AccessibleDescription = "AccessibleDescription_StatusBar";
			this.xToolbarsManagerPreview.Tools["Thumbnails"].SharedProps.Caption = "PrintPreview_Tool_Thumbnails";
			this.xToolbarsManagerPreview.Tools["Hand Tool"].SharedProps.Caption = "PrintPreview_Tool_Hand_Tool";
			this.xToolbarsManagerPreview.Tools["Snapshot Tool"].SharedProps.Caption = "PrintPreview_Tool_Snapshot_Tool";
			this.xToolbarsManagerPreview.Tools["Dynamic Zoom Tool"].SharedProps.Caption = "PrintPreview_Tool_Dynamic_Zoom_Tool";
			this.xToolbarsManagerPreview.Tools["Zoom Out Tool"].SharedProps.Caption = "PrintPreview_Tool_Zoom_Out_Tool";
			this.xToolbarsManagerPreview.Tools["Zoom In Tool"].SharedProps.Caption = "PrintPreview_Tool_Zoom_In_Tool";
			this.xToolbarsManagerPreview.Tools["Print"].SharedProps.Caption = "PrintPreview_Tool_Print";
			this.xToolbarsManagerPreview.Tools["Zoom"].SharedProps.Caption = "PrintPreview_Tool_Zoom";
			this.xToolbarsManagerPreview.Tools["File"].SharedProps.Caption = "PrintPreview_Tool_File";
			this.xToolbarsManagerPreview.Tools["View"].SharedProps.Caption = "PrintPreview_Tool_View";
			this.xToolbarsManagerPreview.Tools["Tools"].SharedProps.Caption = "PrintPreview_Tool_Tools";
			this.xToolbarsManagerPreview.Tools["Exit"].SharedProps.Caption = "PrintPreview_Tool_Exit";
			this.xToolbarsManagerPreview.Tools["Zoom In"].SharedProps.Caption = "PrintPreview_Tool_Zoom_In";
			this.xToolbarsManagerPreview.Tools["Zoom Out"].SharedProps.Caption = "PrintPreview_Tool_Zoom_Out";
			this.xToolbarsManagerPreview.Tools["Previous View"].SharedProps.Caption = "PrintPreview_Tool_Previous_View";
			this.xToolbarsManagerPreview.Tools["Next View"].SharedProps.Caption = "PrintPreview_Tool_Next_View";
			this.xToolbarsManagerPreview.Tools["Current Page"].SharedProps.Caption = "PrintPreview_Tool_Current_Page";
			this.xToolbarsManagerPreview.Tools["First Page"].SharedProps.Caption = "PrintPreview_Tool_First_Page";
			this.xToolbarsManagerPreview.Tools["Next Page"].SharedProps.Caption = "PrintPreview_Tool_Next_Page";
			this.xToolbarsManagerPreview.Tools["Previous Page"].SharedProps.Caption = "PrintPreview_Tool_Previous_Page";
			this.xToolbarsManagerPreview.Tools["Last Page"].SharedProps.Caption = "PrintPreview_Tool_Last_Page";
			this.xToolbarsManagerPreview.Tools["Page Width"].SharedProps.Caption = "PrintPreview_Tool_Page_Width";
			this.xToolbarsManagerPreview.Tools["Margin Width"].SharedProps.Caption = "PrintPreview_Tool_Margin_Width";
			this.xToolbarsManagerPreview.Tools["Whole Page"].SharedProps.Caption = "PrintPreview_Tool_Whole_Page";
			this.xToolbarsManagerPreview.Tools["Go To"].SharedProps.Caption = "PrintPreview_Tool_Go_To";
			this.xToolbarsManagerPreview.Tools["Page Layout"].SharedProps.Caption = "PrintPreview_Tool_Page_Layout";
			this.xToolbarsManagerPreview.Tools["Page Setup"].SharedProps.Caption = "PrintPreview_Tool_Page_Setup";
			this.xToolbarsManagerPreview.Tools["Continuous"].SharedProps.Caption = "PrintPreview_Tool_Continuous";
			this.xToolbarsManagerPreview.Tools["ContextMenuThumbnail"].SharedProps.Caption = "PrintPreview_Tool_ContextMenuThumbnail";
			this.xToolbarsManagerPreview.Tools["ContextMenuPreviewHand"].SharedProps.Caption = "PrintPreview_Tool_ContextMenuPreviewHand";
			this.xToolbarsManagerPreview.Tools["ContextMenuPreviewZoom"].SharedProps.Caption = "PrintPreview_Tool_ContextMenuPreviewZoom";
			this.xToolbarsManagerPreview.Tools["Zoom Increments"].SharedProps.Caption = "PrintPreview_Tool_Zoom_Increments";
			this.xToolbarsManagerPreview.Tools["Reduce Page Thumbnails"].SharedProps.Caption = "PrintPreview_Tool_Reduce_Page_Thumbnails";
			this.xToolbarsManagerPreview.Tools["Enlarge Page Thumbnails"].SharedProps.Caption = "PrintPreview_Tool_Enlarge_Page_Thumbnails";
			this.xToolbarsManagerPreview.Tools["Show Page Numbers"].SharedProps.Caption = "PrintPreview_Tool_Show_Page_Numbers";
			this.xToolbarsManagerPreview.Tools["ClosePreview"].SharedProps.Caption = "PrintPreview_Tool_ClosePreview";
			this.xToolbarsManagerPreview.Tools["Zoom"].SharedProps.ToolTipText = "PrintPreview_ToolTip_Zoom";
			this.xToolbarsManagerPreview.Tools["ClosePreview"].SharedProps.ToolTipText = "PrintPreview_ToolTip_ClosePreview";
			this.xToolbarsManagerPreview.Toolbars["Standard"].Text = "CustomizeImg_ToolBar_Standard";
			this.xToolbarsManagerPreview.Toolbars["MenuBar"].Text = "CustomizeImg_ToolBar_MenuBar";
			this.xToolbarsManagerPreview.Toolbars["View"].Text = "CustomizeImg_ToolBar_View";
			this.xToolbarsManagerPreview.Tools["Thumbnails"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Hand Tool"].SharedProps.Category = "PrintPreview_ToolCategory_Tools";
			this.xToolbarsManagerPreview.Tools["Snapshot Tool"].SharedProps.Category = "PrintPreview_ToolCategory_Tools";
			this.xToolbarsManagerPreview.Tools["Dynamic Zoom Tool"].SharedProps.Category = "PrintPreview_ToolCategory_Tools";
			this.xToolbarsManagerPreview.Tools["Zoom Out Tool"].SharedProps.Category = "PrintPreview_ToolCategory_Tools";
			this.xToolbarsManagerPreview.Tools["Zoom In Tool"].SharedProps.Category = "PrintPreview_ToolCategory_Tools";
			this.xToolbarsManagerPreview.Tools["Print"].SharedProps.Category = "PrintPreview_ToolCategory_File";
			this.xToolbarsManagerPreview.Tools["Zoom"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["File"].SharedProps.Category = "PrintPreview_ToolCategory_Menus";
			this.xToolbarsManagerPreview.Tools["View"].SharedProps.Category = "PrintPreview_ToolCategory_Menus";
			this.xToolbarsManagerPreview.Tools["Tools"].SharedProps.Category = "PrintPreview_ToolCategory_Menus";
			this.xToolbarsManagerPreview.Tools["Exit"].SharedProps.Category = "PrintPreview_ToolCategory_File";
			this.xToolbarsManagerPreview.Tools["Zoom In"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Zoom Out"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Previous View"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Next View"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Current Page"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["First Page"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Next Page"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Previous Page"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Last Page"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Page Width"].SharedProps.Category = "PrintPreview_ToolCategory_Zoom_Mode";
			this.xToolbarsManagerPreview.Tools["Margin Width"].SharedProps.Category = "PrintPreview_ToolCategory_Zoom_Mode";
			this.xToolbarsManagerPreview.Tools["Whole Page"].SharedProps.Category = "PrintPreview_ToolCategory_Zoom_Mode";
			this.xToolbarsManagerPreview.Tools["Go To"].SharedProps.Category = "PrintPreview_ToolCategory_Menus";
			this.xToolbarsManagerPreview.Tools["Page Layout"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Page Setup"].SharedProps.Category = "PrintPreview_ToolCategory_File";
			this.xToolbarsManagerPreview.Tools["Continuous"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["ContextMenuThumbnail"].SharedProps.Category = "PrintPreview_ToolCategory_Context_Menus";
			this.xToolbarsManagerPreview.Tools["ContextMenuPreviewHand"].SharedProps.Category = "PrintPreview_ToolCategory_Context_Menus";
			this.xToolbarsManagerPreview.Tools["ContextMenuPreviewZoom"].SharedProps.Category = "PrintPreview_ToolCategory_Context_Menus";
			this.xToolbarsManagerPreview.Tools["Zoom Increments"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Reduce Page Thumbnails"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Enlarge Page Thumbnails"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Show Page Numbers"].SharedProps.Category = "PrintPreview_ToolCategory_View";
			this.xToolbarsManagerPreview.Tools["Thumbnails"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Thumbnails_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Hand Tool"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Hand_Tool_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Snapshot Tool"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Snapshot_Tool_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Dynamic Zoom Tool"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Dynamic_Zoom_Tool_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Zoom Out Tool"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Zoom_Out_Tool_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Zoom In Tool"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Zoom_In_Tool_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Print"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Print_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Zoom"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Zoom_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["File"].SharedProps.AccessibleDescription = "PrintPreview_Tool_File_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["View"].SharedProps.AccessibleDescription = "PrintPreview_Tool_View_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Tools"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Tools_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Exit"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Exit_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Zoom In"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Zoom_In_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Zoom Out"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Zoom_Out_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Previous View"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Previous_View_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Next View"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Next_View_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Current Page"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Current_Page_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["First Page"].SharedProps.AccessibleDescription = "PrintPreview_Tool_First_Page_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Next Page"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Next_Page_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Previous Page"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Previous_Page_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Last Page"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Last_Page_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Page Width"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Page_Width_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Margin Width"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Margin_Width_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Whole Page"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Whole_Page_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Go To"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Go_To_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Page Layout"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Page_Layout_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Page Setup"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Page_Setup_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Continuous"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Continuous_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Reduce Page Thumbnails"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Reduce_Page_Thumbnails_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Enlarge Page Thumbnails"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Enlarge_Page_Thumbnails_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["Show Page Numbers"].SharedProps.AccessibleDescription = "PrintPreview_Tool_Show_Page_Numbers_AccessibleDescription";
			this.xToolbarsManagerPreview.Tools["ClosePreview"].SharedProps.AccessibleDescription = "PrintPreview_Tool_ClosePreview_AccessibleDescription";
			this.InitializeZoomCombo();
		}

		private void InitializeZoomCombo()
		{
			ComboBoxTool tool = this.xToolbarsManagerPreview.Tools["Zoom"] as ComboBoxTool;
			//ListTool tool2 = this.ultraToolbarsManager2.Tools["Zoom Increments"] as ListTool;
			tool.ValueList.ValueListItems.Clear();
			//tool2.ListToolItems.Clear();
			double[] array = new double[] { 0.0833, 0.12, 0.25, 0.3333, 0.5, 0.6667, 0.75, 1.0, 1.25, 1.5, 2.0, 3.0, 4.0, 6.0, 8.0, 12.0, 16.0 };
			Array.Sort<double>(array);
			Array.Reverse(array);
			for (int i = 0; i < array.Length; i++)
			{
				string displayText = this.FormatPercent(array[i]);
				ZoomWrapper dataValue = new ZoomWrapper(array[i]);
				tool.ValueList.ValueListItems.Add(dataValue, displayText);
				//tool2.ListToolItems.Add(displayText, displayText, false).Tag = dataValue;
			}
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.PageWidth), "PrintPreview_ZoomListItem_PageWidth");
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.MarginsWidth), "PrintPreview_ZoomListItem_MarginWidth");
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.WholePage), "PrintPreview_ZoomListItem_WholePage");
			this.UpdateZoomComboAndList();
		}

		public void InvalidatePreview()
		{
			this.ultraPrintPreviewControl.InvalidatePreview();
			this.ultraPrintPreviewControl.GeneratePreview(false);
		}

		private static bool IsNewPreviewRequired(PageSettings oldSettings, PageSettings newSettings)
		{
			return ((oldSettings.Bounds != newSettings.Bounds) || ((oldSettings.Color != newSettings.Color) || ((oldSettings.Landscape != newSettings.Landscape) || (!object.Equals(oldSettings.Margins, newSettings.Margins) || (!IsSameObject(oldSettings.PaperSize, newSettings.PaperSize) || (!IsSameObject(oldSettings.PaperSource, newSettings.PaperSource) || !IsSameObject(oldSettings.PrinterResolution, newSettings.PrinterResolution)))))));
		}

		private static bool IsSameObject(object printObject1, object printObject2)
		{
			if (printObject1 == printObject2)
			{
				return true;
			}
			if ((printObject1 == null) || (printObject2 == null))
			{
				return false;
			}
			if (printObject1.GetType() != printObject2.GetType())
			{
				return false;
			}
			if (printObject1 is PaperSize)
			{
				PaperSize size = printObject1 as PaperSize;
				PaperSize size2 = printObject2 as PaperSize;
				return ((((size.Kind == size2.Kind) && (size.Height == size2.Height)) && (size.Width == size2.Width)) && (size.PaperName == size2.PaperName));
			}
			if (printObject1 is PaperSource)
			{
				PaperSource source = printObject1 as PaperSource;
				PaperSource source2 = printObject2 as PaperSource;
				return ((source.Kind == source2.Kind) && (source.SourceName == source2.SourceName));
			}
			if (!(printObject1 is PrinterResolution))
			{
				return false;
			}
			PrinterResolution resolution = printObject1 as PrinterResolution;
			PrinterResolution resolution2 = printObject2 as PrinterResolution;
			return (((resolution.Kind == resolution2.Kind) && (resolution.X == resolution2.X)) && (resolution.Y == resolution2.Y));
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			this.ultraPrintPreviewControl.InvalidatePreview();
			base.OnClosing(e);
		}

		protected virtual void OnPageSetupDialogDisplaying(PageSetupDialogDisplayingEventArgs e)
		{
			PageSetupDialogDisplayingEventHandler handler = (PageSetupDialogDisplayingEventHandler)base.Events[EventPageSetupDialogDisplaying];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		protected virtual void OnPrinted(EventArgs e)
		{
			EventHandler handler = (EventHandler)base.Events[EventPrinted];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		protected virtual void OnPrinting(PrintingEventArgs e)
		{
			PrintingEventHandler handler = (PrintingEventHandler)base.Events[EventPrinting];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		private void OnRowColumnSelectorMouseUp(object sender, MouseEventArgs e)
		{
			Size selection = this.rowColumnSelector.Selection;
			if (!selection.IsEmpty)
			{
				this.ultraPrintPreviewControl.Settings.Columns = selection.Width;
				if ((selection.Width == 1) && (selection.Height == 1))
				{
					this.ultraPrintPreviewControl.Settings.Rows = 0;
					this.lastRowsSetting = 1;
				}
				else
				{
					this.ultraPrintPreviewControl.Settings.Rows = selection.Height;
					this.lastRowsSetting = selection.Height;
				}
				((StateButtonTool)this.xToolbarsManagerPreview.Tools["Continuous"]).Checked = false;
				this.ultraPrintPreviewControl.Settings.ZoomMode = ZoomMode.WholePage;
				ToolBase toolThatContainsControl = this.xToolbarsManagerPreview.GetToolThatContainsControl(this.rowColumnSelector);
				this.ClosePopupContainingTool(toolThatContainsControl);
				this.rowColumnSelector.ResetSelection();
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetPreviewSettings()
		{
			this.ultraPrintPreviewControl.ResetSettings();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailCurrentPreviewPageAppearance()
		{
			this.ultraPrintPreviewThumbnail.ResetCurrentPreviewPageAppearance();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailCurrentPreviewPageNumberAppearance()
		{
			this.ultraPrintPreviewThumbnail.ResetCurrentPreviewPageNumberAppearance();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailSettings()
		{
			this.ultraPrintPreviewThumbnail.ResetSettings();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailViewBoxAppearance()
		{
			this.ultraPrintPreviewThumbnail.ResetViewBoxAppearance();
		}

		private void ResetTrackMouseEventState()
		{
			try
			{
				new ReflectionPermission(ReflectionPermissionFlag.MemberAccess).Assert();
				MethodInfo method = typeof(Control).GetMethod("UnhookMouseEvent", BindingFlags.NonPublic | BindingFlags.Instance);
				FieldInfo field = typeof(Control).GetField("trackMouseEvent", BindingFlags.NonPublic | BindingFlags.Instance);
				this.ResetTrackMouseEventStateHelper(this, method, field);
			}
			catch (Exception)
			{
				this.canResetTrackMouseEvent = false;
			}
		}

		private void ResetTrackMouseEventStateHelper(Control control, MethodInfo resetMethod, FieldInfo trackMouseField)
		{
			resetMethod.Invoke(control, BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance, null, null, null);
			trackMouseField.SetValue(control, null, BindingFlags.NonPublic | BindingFlags.Instance, null, null);
			if (control.HasChildren)
			{
				foreach (Control control2 in control.Controls)
				{
					this.ResetTrackMouseEventStateHelper(control2, resetMethod, trackMouseField);
				}
			}
		}

		protected bool ShouldSerializePreviewSettings()
		{
			return this.ultraPrintPreviewControl.ShouldSerializeSettings();
		}

		protected bool ShouldSerializeThumbnailCurrentPreviewPageAppearance()
		{
			return this.ultraPrintPreviewThumbnail.ShouldSerializeCurrentPreviewPageAppearance();
		}

		protected bool ShouldSerializeThumbnailCurrentPreviewPageNumberAppearance()
		{
			return this.ultraPrintPreviewThumbnail.ShouldSerializeCurrentPreviewPageNumberAppearance();
		}

		protected bool ShouldSerializeThumbnailSettings()
		{
			return this.ultraPrintPreviewThumbnail.ShouldSerializeSettings();
		}

		protected bool ShouldSerializeThumbnailViewBoxAppearance()
		{
			return this.ultraPrintPreviewThumbnail.ShouldSerializeViewBoxAppearance();
		}

		private void ShowPageSetupDialog()
		{

			// Custom PrintSetupDialog
			//----------------------------------------------------------------
			if (this.ultraPrintPreviewControl.Document != null)
			{
				UltraPageSetup setup = new UltraPageSetup((UltraGridPrintDocument)this.ultraPrintPreviewControl.Document);
				DialogResult dr = setup.ShowDialog(this);
				setup.Dispose();

				if (dr == DialogResult.OK)
				{
					this.ultraPrintPreviewControl.InvalidatePreview();
				}
			}

			// 윈도우 기본 PrintSetupDialog
			//----------------------------------------------------------------
			//if (this.ultraPrintPreviewControl1.Document != null)
			//{
			//    this.pageSetupDialog1.Document = this.ultraPrintPreviewControl1.Document;
			//    PageSettings oldSettings = this.pageSetupDialog1.PageSettings.Clone() as PageSettings;
			//    oldSettings.Landscape = oldSettings.Landscape;
			//    bool flag = false;
			//    if (Environment.Version.Major < 2)
			//    {
			//        flag = NativeWindowMethods.IsMetric();
			//    }
			//    else
			//    {
			//        try
			//        {
			//            if (NativeWindowMethods.IsMetric())
			//            {
			//                PropertyDescriptor descriptor = TypeDescriptor.GetProperties(this.pageSetupDialog1)["EnableMetric"];
			//                if ((descriptor != null) && (descriptor.PropertyType == typeof(bool)))
			//                {
			//                    flag = !((bool)descriptor.GetValue(this.pageSetupDialog1));
			//                }
			//            }
			//        }
			//        catch
			//        {
			//        }
			//    }
			//    if (flag)
			//    {
			//        this.pageSetupDialog1.PageSettings.Margins = PrinterUnitConvert.Convert(this.pageSetupDialog1.PageSettings.Margins, PrinterUnit.Display, PrinterUnit.TenthsOfAMillimeter);
			//    }
			//    PageSetupDialogDisplayingEventArgs e = new PageSetupDialogDisplayingEventArgs(this.pageSetupDialog1);
			//    this.OnPageSetupDialogDisplaying(e);
			//    if (!e.Cancel)
			//    {
			//        if (this.pageSetupDialog1.ShowDialog(this) == DialogResult.OK)
			//        {
			//            if (IsNewPreviewRequired(oldSettings, this.pageSetupDialog1.PageSettings))
			//            {
			//                this.InvalidatePreview();
			//            }
			//        }
			//        else if (flag)
			//        {
			//            this.pageSetupDialog1.PageSettings.Margins = PrinterUnitConvert.Convert(this.pageSetupDialog1.PageSettings.Margins, PrinterUnit.TenthsOfAMillimeter, PrinterUnit.Display);
			//        }
			//    }
			//}
		}

		private void UpdateContinuousTool()
		{
			bool enabled = this.xToolbarsManagerPreview.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			((StateButtonTool)this.xToolbarsManagerPreview.Tools["Continuous"]).Checked = this.ultraPrintPreviewControl.Settings.Rows == 0;
			this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UpdateCurrentPageInfo()
		{
			int currentPage = this.ultraPrintPreviewControl.CurrentPage;
			int pageCount = this.ultraPrintPreviewControl.PageCount;
			TextBoxTool tool = this.xToolbarsManagerPreview.Tools["Current Page"] as TextBoxTool;
			if (tool != null)
			{
				if (pageCount == 0)
				{
					tool.Text = string.Empty;
				}
				else
				{
					tool.Text = currentPage.ToString();
				}
			}
			if (this.ultraStatusBar1.Panels.Exists("PageNumber"))
			{
				if (pageCount == 0)
				{
					this.ultraStatusBar1.Panels["PageNumber"].Text = string.Empty;
				}
				else
				{
					this.ultraStatusBar1.Panels["PageNumber"].Text = string.Format(" Page {0} OF {1}", currentPage, pageCount);
				}
			}
		}

		private void UpdateDocumentRelatedTools()
		{
			bool flag = this.ultraPrintPreviewControl.Document != null;
			bool flag2 = flag && this.ultraPrintPreviewControl.Document.PrinterSettings.IsValid;
			this.xToolbarsManagerPreview.Tools["Print"].SharedProps.Enabled = flag && flag2;
			this.xToolbarsManagerPreview.Tools["Page Setup"].SharedProps.Enabled = flag && flag2;
		}

		private void UpdateMouseActionTools()
		{
			string menuKey = null;
			string str2 = null;
			switch (this.ultraPrintPreviewControl.MouseActionResolved)
			{
				case PreviewMouseAction.Hand:
					str2 = "Hand Tool";
					menuKey = "ContextMenuPreviewHand";
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl, "StatusBar_Hand_Instructions");
					break;

				case PreviewMouseAction.Snapshot:
					str2 = "Snapshot Tool";
					menuKey = "ContextMenuPreviewHand";
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl, "StatusBar_SnapShot_Instructions");
					break;

				case PreviewMouseAction.DynamicZoom:
					str2 = "Dynamic Zoom Tool";
					menuKey = "ContextMenuPreviewZoom";
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl, "StatusBar_DynamicZoom_Instructions");
					break;

				case PreviewMouseAction.ZoomOut:
					str2 = "Zoom Out Tool";
					menuKey = "ContextMenuPreviewZoom";
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl, "StatusBar_ZoomOut_Instructions");
					break;

				case PreviewMouseAction.ZoomIn:
					str2 = "Zoom In Tool";
					menuKey = "ContextMenuPreviewZoom";
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl, "StatusBar_ZoomIn_Instructions");
					break;

				default:
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl, "");
					break;
			}
			if (str2 != null)
			{
				bool enabled = this.xToolbarsManagerPreview.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
				this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
				((StateButtonTool)this.xToolbarsManagerPreview.Tools[str2]).Checked = true;
				this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
			}
			if (menuKey != null)
			{
				this.xToolbarsManagerPreview.SetContextMenuUltra(this.ultraPrintPreviewControl, menuKey);
			}
		}

		private void UpdateUsingZoomIncrementsList(Infragistics.Win.UltraWinToolbars.ListToolItem item)
		{
			ListTool tool = this.xToolbarsManagerPreview.Tools["Zoom Increments"] as ListTool;
			ZoomWrapper wrapper = ((tool.SelectedItem == null) ? ((ZoomWrapper)((item == null) ? null : ((ZoomWrapper)item.Tag))) : ((ZoomWrapper)tool.SelectedItem.Tag)) as ZoomWrapper;
			if (wrapper != null)
			{
				wrapper.UpdateZoom(this.ultraPrintPreviewControl.Settings);
			}
			bool enabled = this.xToolbarsManagerPreview.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			int num = (item == null) ? -1 : item.Index;
			if (num != tool.SelectedItemIndex)
			{
				tool.SelectedItemIndex = num;
			}
			this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UpdateViewHistoryTools()
		{
			this.xToolbarsManagerPreview.Tools["Previous View"].SharedProps.Enabled = this.ultraPrintPreviewControl.HasPreviousView;
			this.xToolbarsManagerPreview.Tools["Next View"].SharedProps.Enabled = this.ultraPrintPreviewControl.HasNextView;
		}

		private void UpdateZoomComboAndList()
		{
			ComboBoxTool tool = this.xToolbarsManagerPreview.Tools["Zoom"] as ComboBoxTool;
			bool enabled = this.xToolbarsManagerPreview.EventManager.IsEnabled(ToolbarEventIds.ToolValueChanged);
			this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolValueChanged, false);
			string key = this.FormatPercent(this.ultraPrintPreviewControl.CurrentZoom);
			tool.Text = key;
			this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolValueChanged, enabled);
			this.xToolbarsManagerPreview.Tools["Zoom Out"].SharedProps.Enabled = this.ultraPrintPreviewControl.CanZoomOut;
			this.xToolbarsManagerPreview.Tools["Zoom In"].SharedProps.Enabled = this.ultraPrintPreviewControl.CanZoomIn;
			bool flag2 = this.xToolbarsManagerPreview.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			ListTool tool2 = this.xToolbarsManagerPreview.Tools["Zoom Increments"] as ListTool;
			int index = tool2.ListToolItems.IndexOf(key);
			if (index != tool2.SelectedItemIndex)
			{
				tool2.SelectedItemIndex = index;
			}
			this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolClick, flag2);
		}

		private void UpdateZoomModeTools()
		{
			bool flag = this.ultraPrintPreviewControl.Settings.ZoomMode == ZoomMode.PageWidth;
			bool flag2 = this.ultraPrintPreviewControl.Settings.ZoomMode == ZoomMode.MarginsWidth;
			bool flag3 = this.ultraPrintPreviewControl.Settings.ZoomMode == ZoomMode.WholePage;
			bool enabled = this.xToolbarsManagerPreview.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			((StateButtonTool)this.xToolbarsManagerPreview.Tools["Page Width"]).Checked = flag;
			((StateButtonTool)this.xToolbarsManagerPreview.Tools["Margin Width"]).Checked = flag2;
			((StateButtonTool)this.xToolbarsManagerPreview.Tools["Whole Page"]).Checked = flag3;
			this.xToolbarsManagerPreview.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UseStandardZoomModeIfNecessary()
		{
			if ((this.xToolbarsManagerPreview.OptionSets["ZoomMode"].SelectedTool == null) && (this.ultraPrintPreviewControl.Settings.ZoomMode != ZoomMode.Standard))
			{
				double currentZoom = this.ultraPrintPreviewControl.CurrentZoom;
				this.ultraPrintPreviewControl.Settings.ZoomMode = ZoomMode.Standard;
				this.ultraPrintPreviewControl.Settings.Zoom = currentZoom;
			}
		}

		#endregion
	}
}