﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace AnyFactory.FX.Win.Controls.UltraGridHelper.Print
{
	internal class NativeWindowMethods
	{
		#region Constants
		private const int LOCALE_IMEASURE = 13;
		private const int LOCALE_USER_DEFAULT = 0x400;
		#endregion

		[DllImport("kernel32", CharSet=CharSet.Auto)]
		private static extern int GetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.LPWStr)] string lpLCData, int cchData);

		private static int GetLocaleInfoApi(int Locale, int LCType, string lpLCData, int cchData)
		{
			return GetLocaleInfo(Locale, LCType, lpLCData, cchData);
		}

		internal static bool IsMetric()
		{
			try
			{
				new SecurityPermission(SecurityPermissionFlag.UnmanagedCode).Assert();
				string lpLCData = new string(' ', 2);
				GetLocaleInfoApi(0x400, 13, lpLCData, lpLCData.Length);
				return (string.Compare(lpLCData, "0") == 0);
			}
			catch (Exception)
			{
				return RegionInfo.CurrentRegion.IsMetric;
			}
		}
	}
}
