﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Controls.UltraGridHelper.Print.PrintPreviewForm
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 12월 2일 수요일 오전 10:23
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/


using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;
using AnyFactory.FX.Win.Controls.UltraGridHelper.Print.Properties;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.Printing;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinStatusBar;
using Infragistics.Win.UltraWinToolbars;

namespace AnyFactory.FX.Win.Controls.UltraGridHelper.Print
{
	/// <summary>
	/// PrintPreview 클래스를 선언합니다.
	/// </summary>
	public partial class PrintPreviewForm : Form
	{
		#region Constants
		private const string FILE_KEY = "File";
		private const string PAGE_SETUP_KEY = "Page Setup";
		private const string PRINT_KEY = "Print";
		private const string EXIT_KEY = "Exit";
		private const string VIEW_KEY = "View";
		private const string THUMBNAILS_KEY = "Thumbnails";
		private const string ZOOMIN_KEY = "Zoom In";
		private const string ZOOMOUT_KEY = "Zoom Out";
		private const string ZOOM_KEY = "Zoom";
		private const string PAGEWIDTH_KEY = "Page Width";
		private const string MARGINWIDTH_KEY = "Margin Width";
		private const string WHOLEPAGE_KEY = "Whole Page";
		private const string GOTO_KEY = "Go To";
		private const string FIRSTPAGE_KEY = "First Page";
		private const string PREVIOUSPAGE_KEY = "Previous Page";
		private const string NEXTPAGE_KEY = "Next Page";
		private const string LASTPAGE_KEY = "Last Page";
		private const string PREVIOUSVIEW_KEY = "Previous View";
		private const string NEXTVIEW_KEY = "Next View";
		private const string TOOLS_KEY = "Tools";
		private const string HANDTOOL_KEY = "Hand Tool";
		private const string SNAPSHOTTOOL_KEY = "Snapshot Tool";
		private const string DYNAMICZOOMTOOL_KEY = "Dynamic Zoom Tool";
		private const string ZOOMINTOOL_KEY = "Zoom In Tool";
		private const string ZOOMOUTTOOL_KEY = "Zoom Out Tool";
		private const string CURRENTPAGE_KEY = "Current Page";
		private const string CONTINUOUS_KEY = "Continuous";
		private const string PAGELAYOUT_KEY = "Page Layout";
		private const string CLOSEPREVIEW_KEY = "ClosePreview";
		private const string CONTEXTMENUTHUMBNAIL_KEY = "ContextMenuThumbnail";
		private const string REDUCEPAGETHUMBNAILS_KEY = "Reduce Page Thumbnails";
		private const string ENLARGEPAGETHUMBNAILS_KEY = "Enlarge Page Thumbnails";
		private const string SHOWPAGENUMBERS_KEY = "Show Page Numbers";
		private const string CONTEXTMENUPREVIEWHAND_KEY = "ContextMenuPreviewHand";
		private const string CONTEXTMENUPREVIEWZOOM_KEY = "ContextMenuPreviewZoom";
		private const string ZOOMINCREMENTS_KEY = "Zoom Increments";
		private const string STANDARD_KEY = "Standard";
		private const string MENUBAR_KEY = "MenuBar";
		private const string PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY = "PrintPreview_ToolCategory_View";
		private const string PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY = "PrintPreview_ToolCategory_Tools";
		private const string PRINTPREVIEW_TOOLCATEGORY_FILE_KEY = "PrintPreview_ToolCategory_File";
		private const string PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY = "PrintPreview_ToolCategory_Menus";
		private const string PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY = "PrintPreview_ToolCategory_Zoom_Mode";
		private const string PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY = "PrintPreview_ToolCategory_Context_Menus";
    	#endregion

		#region Events
		public event PageSetupDialogDisplayingEventHandler PageSetupDialogDisplaying
		{
			add
			{
				base.Events.AddHandler(_eventPageSetupDialogDisplaying, value);
			}
			remove
			{
				base.Events.RemoveHandler(_eventPageSetupDialogDisplaying, value);
			}
		}
		public event EventHandler Printed
		{
			add
			{
				base.Events.AddHandler(_eventPrinted, value);
			}
			remove
			{
				base.Events.RemoveHandler(_eventPrinted, value);
			}
		}
		public event PrintingEventHandler Printing
		{
			add
			{
				base.Events.AddHandler(_eventPrinting, value);
			}
			remove
			{
				base.Events.RemoveHandler(_eventPrinting, value);
			}
		}
		#endregion
 
		#region Fields
		private bool _canResetTrackMouseEvent;
		private IContainer _container;
		private bool _displayPrintStatus;
		private static readonly object _eventPageSetupDialogDisplaying;
		private static readonly object _eventPrinted;
		private static readonly object _eventPrinting;
		private int _lastRowsSetting;
		private PageSetupDialog _pageSetupDialog;
		private PrintDialog _printDialog;
		private PreviewRowColumnSelector _rowColumnSelector;
		private bool _statusBarVisible = false;
		private bool _thumbnailAreaVisible;
		#endregion

		#region Constructor
		/// <summary>
		/// PrintPreview의 인스턴스를 생성합니다.
		/// </summary>
		public PrintPreviewForm()
			: base()
		{
			_thumbnailAreaVisible = true;
			_displayPrintStatus = true;
			_statusBarVisible = true;
			_canResetTrackMouseEvent = true;
			
			//초기화
			InitializeComponent();

			_rowColumnSelector = new PreviewRowColumnSelector();
			_rowColumnSelector.Location = new Point(-10000, -10000);
			base.Controls.Add(_rowColumnSelector);
			_rowColumnSelector.MouseUp += new MouseEventHandler(OnRowColumnSelectorMouseUp);

			InitializeDialogStrings();
			base.Name = "";
			ShowInTaskbar = false;
			InitializeZoomCombo();
			UpdateMouseActionTools();

			InitializeDialogStrings();
		}
	 
		public PrintPreviewForm(IContainer container)
			: this()
		{
			_container = container;
			if (_container != null)
			{
				_container.Add(this);
			}
		}

		public PrintPreviewForm(UltraGrid xgridMain)
			: this()
		{
			if (xgridMain != null)
			{
				xGridPrintDocument.Grid = xgridMain;
				Document = xGridPrintDocument;
			}
		}

		static PrintPreviewForm()
		{
			_eventPageSetupDialogDisplaying = new object();
			_eventPrinting = new object();
			_eventPrinted = new object();
		}

		#endregion

		#region Override Properties
		public object About
		{
			get
			{
				return null;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new IButtonControl AcceptButton
		{
			get
			{
				return base.AcceptButton;
			}
			set
			{
				base.AcceptButton = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new string AccessibleDescription
		{
			get
			{
				return base.AccessibleDescription;
			}
			set
			{
				base.AccessibleDescription = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new string AccessibleName
		{
			get
			{
				return base.AccessibleName;
			}
			set
			{
				base.AccessibleName = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new AccessibleRole AccessibleRole
		{
			get
			{
				return base.AccessibleRole;
			}
			set
			{
				base.AccessibleRole = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool AllowDrop
		{
			get
			{
				return base.AllowDrop;
			}
			set
			{
				base.AllowDrop = value;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new AnchorStyles Anchor
		{
			get
			{
				return base.Anchor;
			}
			set
			{
				base.Anchor = value;
			}
		}

		[DefaultValue(true)]
		public bool AutoGeneratePreview
		{
			get
			{
				return xPrintPreviewControl.AutoGeneratePreview;
			}
			set
			{
				xPrintPreviewControl.AutoGeneratePreview = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new Size AutoScaleBaseSize
		{
			get
			{
				return base.AutoScaleBaseSize;
			}
			set
			{
				base.AutoScaleBaseSize = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new bool AutoScroll
		{
			get
			{
				return base.AutoScroll;
			}
			set
			{
				base.AutoScroll = value;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new Size AutoScrollMargin
		{
			get
			{
				return base.AutoScrollMargin;
			}
			set
			{
				base.AutoScrollMargin = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new Size AutoScrollMinSize
		{
			get
			{
				return base.AutoScrollMinSize;
			}
			set
			{
				base.AutoScrollMinSize = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Image BackgroundImage
		{
			get
			{
				return base.BackgroundImage;
			}
			set
			{
				base.BackgroundImage = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new ImageLayout BackgroundImageLayout
		{
			get
			{
				return base.BackgroundImageLayout;
			}
			set
			{
				base.BackgroundImageLayout=(value);
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new IButtonControl CancelButton
		{
			get
			{
				return base.CancelButton;
			}
			set
			{
				base.CancelButton = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new bool CausesValidation
		{
			get
			{
				return base.CausesValidation;
			}
			set
			{
				base.CausesValidation = value;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new Size ClientSize
		{
			get
			{
				return base.ClientSize;
			}
			set
			{
				base.ClientSize = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ContextMenu ContextMenu
		{
			get
			{
				return base.ContextMenu;
			}
			set
			{
				base.ContextMenu = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ControlBox
		{
			get
			{
				return base.ControlBox;
			}
			set
			{
				base.ControlBox = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Cursor Cursor
		{
			get
			{
				return base.Cursor;
			}
			set
			{
				base.Cursor = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ControlBindingsCollection DataBindings
		{
			get
			{
				return base.DataBindings;
			}
		}

		[DefaultValue(true)]
		public bool DisplayPreviewStatus
		{
			get
			{
				return xPrintPreviewControl.DisplayPreviewStatus;
			}
			set
			{
				xPrintPreviewControl.DisplayPreviewStatus = value;
			}
		}

		[DefaultValue(true)]
		public bool DisplayPrintStatus
		{
			get
			{
				return _displayPrintStatus;
			}
			set
			{
				_displayPrintStatus = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new DockStyle Dock
		{
			get
			{
				return base.Dock;
			}
			set
			{
				base.Dock = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new ScrollableControl.DockPaddingEdges DockPadding
		{
			get
			{
				return base.DockPadding;
			}
		}

		[DefaultValue((string)null)]
		public PrintDocument Document
		{
			get
			{
				return xPrintPreviewControl.Document;
			}
			set
			{
				xPrintPreviewControl.Document = value;
				_pageSetupDialog.Document = null;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new FormBorderStyle FormBorderStyle
		{
			get
			{
				return base.FormBorderStyle;
			}
			set
			{
				base.FormBorderStyle = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new bool HelpButton
		{
			get
			{
				return base.HelpButton;
			}
			set
			{
				base.HelpButton = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Icon Icon
		{
			get
			{
				return base.Icon;
			}
			set
			{
				base.Icon = value;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new ImeMode ImeMode
		{
			get
			{
				return base.ImeMode;
			}
			set
			{
				base.ImeMode = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool IsMdiContainer
		{
			get
			{
				return base.IsMdiContainer;
			}
			set
			{
				base.IsMdiContainer = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool KeyPreview
		{
			get
			{
				return base.KeyPreview;
			}
			set
			{
				base.KeyPreview = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Point Location
		{
			get
			{
				return base.Location;
			}
			set
			{
				base.Location = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool MaximizeBox
		{
			get
			{
				return base.MaximizeBox;
			}
			set
			{
				base.MaximizeBox = value;
			}
		}

		[DefaultValue(0)]
		public int MaximumPreviewPages
		{
			get
			{
				return xPrintPreviewControl.MaximumPreviewPages;
			}
			set
			{
				xPrintPreviewControl.MaximumPreviewPages = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size MaximumSize
		{
			get
			{
				return base.MaximumSize;
			}
			set
			{
				base.MaximumSize = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new MainMenu Menu
		{
			get
			{
				return base.Menu;
			}
			set
			{
				base.Menu = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool MinimizeBox
		{
			get
			{
				return base.MinimizeBox;
			}
			set
			{
				base.MinimizeBox = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size MinimumSize
		{
			get
			{
				return base.MinimumSize;
			}
			set
			{
				base.MinimumSize = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced)]
		public new double Opacity
		{
			get
			{
				return base.Opacity;
			}
			set
			{
				base.Opacity = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new Padding Padding
		{
			get
			{
				return base.Padding;
			}
			set
			{
				base.Padding = (value);
			}
		}

		[DefaultValue(1)]
		public PreviewMouseAction PreviewMouseAction
		{
			get
			{
				return xPrintPreviewControl.MouseAction;
			}
			set
			{
				xPrintPreviewControl.MouseAction = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PreviewSettings PreviewSettings
		{
			get
			{
				return xPrintPreviewControl.Settings;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public UltraPrintPreviewControl PrintPreviewControl
		{
			get
			{
				return xPrintPreviewControl;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraPrintPreviewThumbnail PrintPreviewThumbnail
		{
			get
			{
				return xPrintPreviewThumbnail;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new RightToLeft RightToLeft
		{
			get
			{
				return base.RightToLeft;
			}
			set
			{
				base.RightToLeft = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ShowInTaskbar
		{
			get
			{
				return base.ShowInTaskbar;
			}
			set
			{
				base.ShowInTaskbar = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Size Size
		{
			get
			{
				return base.Size;
			}
			set
			{
				base.Size = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new SizeGripStyle SizeGripStyle
		{
			get
			{
				return base.SizeGripStyle;
			}
			set
			{
				base.SizeGripStyle = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new FormStartPosition StartPosition
		{
			get
			{
				return base.StartPosition;
			}
			set
			{
				base.StartPosition = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraStatusBar StatusBar
		{
			get
			{
				return xStatusBar;
			}
		}

		[DefaultValue(true)]
		public bool StatusBarVisible
		{
			get
			{
				return _statusBarVisible;
			}
			set
			{
				_statusBarVisible = value;
				xStatusBar.Visible = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new bool TabStop
		{
			get
			{
				return base.TabStop;
			}
			set
			{
				base.TabStop = value;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new object Tag
		{
			get
			{
				return base.Tag;
			}
			set
			{
				base.Tag = value;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}

		[DefaultValue(true)]
		public bool ThumbnailAreaVisible
		{
			get
			{
				return _thumbnailAreaVisible;
			}
			set
			{
				_thumbnailAreaVisible = value;

				xPrintPreviewThumbnail.Visible = value;
				if (value == true)
				{
					splitContainer.SplitterDistance = 176;
				}
				else
				{
					splitContainer.SplitterDistance = 0;
				}
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailCurrentPreviewPageAppearance
		{
			get
			{
				return xPrintPreviewThumbnail.CurrentPreviewPageAppearance;
			}
			set
			{
				xPrintPreviewThumbnail.CurrentPreviewPageAppearance = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailCurrentPreviewPageNumberAppearance
		{
			get
			{
				return xPrintPreviewThumbnail.CurrentPreviewPageNumberAppearance;
			}
			set
			{
				xPrintPreviewThumbnail.CurrentPreviewPageNumberAppearance = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PreviewSettings ThumbnailSettings
		{
			get
			{
				return xPrintPreviewThumbnail.Settings;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailViewBoxAppearance
		{
			get
			{
				return xPrintPreviewThumbnail.ViewBoxAppearance;
			}
			set
			{
				xPrintPreviewThumbnail.ViewBoxAppearance = value;
			}
		}

		[DefaultValue(0)]
		public ViewBoxDisplayStyle ThumbnailViewBoxDisplayStyle
		{
			get
			{
				return xPrintPreviewThumbnail.ViewBoxDisplayStyle;
			}
			set
			{
				xPrintPreviewThumbnail.ViewBoxDisplayStyle = value;
			}
		}

		[DefaultValue(0)]
		public ViewBoxDragMode ThumbnailViewBoxDragMode
		{
			get
			{
				return xPrintPreviewThumbnail.ViewBoxDragMode;
			}
			set
			{
				xPrintPreviewThumbnail.ViewBoxDragMode = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool TopMost
		{
			get
			{
				return base.TopMost;
			}
			set
			{
				base.TopMost = value;
			}
		}

		[DefaultValue(false)]
		public bool UseAntiAlias
		{
			get
			{
				return xPrintPreviewControl.UseAntiAlias;
			}
			set
			{
				xPrintPreviewControl.UseAntiAlias = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool Visible
		{
			get
			{
				return base.Visible;
			}
			set
			{
				base.Visible = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new FormWindowState WindowState
		{
			get
			{
				return base.WindowState;
			}
			set
			{
				base.WindowState = value;
			}
		}
		#endregion
 
		#region Event Methods
		private void PrintPreview_Shown(object sender, EventArgs e)
		{

		}

		private void xToolbarsManager_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
		{
			switch (e.Tool.Key)
			{
				case HANDTOOL_KEY:
					xPrintPreviewControl.MouseAction = PreviewMouseAction.Hand;
					return;

				case SNAPSHOTTOOL_KEY:
					xPrintPreviewControl.MouseAction = PreviewMouseAction.Snapshot;
					return;

				case DYNAMICZOOMTOOL_KEY:
					xPrintPreviewControl.MouseAction = PreviewMouseAction.DynamicZoom;
					return;

				case ZOOMOUTTOOL_KEY:
					xPrintPreviewControl.MouseAction = PreviewMouseAction.ZoomOut;
					return;

				case ZOOMINTOOL_KEY:
					xPrintPreviewControl.MouseAction = PreviewMouseAction.ZoomIn;
					return;

				case ZOOMIN_KEY:
					xPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ZoomIn);
					return;

				case ZOOMOUT_KEY:
					xPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ZoomOut);
					return;

				case PAGEWIDTH_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						UseStandardZoomModeIfNecessary();
						return;
					}
					xPrintPreviewControl.Settings.ZoomMode = ZoomMode.PageWidth;
					return;

				case MARGINWIDTH_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						UseStandardZoomModeIfNecessary();
						return;
					}
					xPrintPreviewControl.Settings.ZoomMode = ZoomMode.MarginsWidth;
					return;

				case WHOLEPAGE_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						UseStandardZoomModeIfNecessary();
						return;
					}
					xPrintPreviewControl.Settings.ZoomMode = ZoomMode.WholePage;
					return;

				case ZOOMINCREMENTS_KEY:
					UpdateUsingZoomIncrementsList(e.ListToolItem);

					return;

				case THUMBNAILS_KEY:
					base.SuspendLayout();
					if (((StateButtonTool)e.Tool).Checked)
					{
						xPrintPreviewThumbnail.SendToBack();
					}
					ThumbnailAreaVisible = ((StateButtonTool)e.Tool).Checked;
					base.ResumeLayout(true);
					return;

				case PRINT_KEY:
					if (xPrintPreviewControl.Document != null)
					{
						// Allow the user to choose the page range he or she would
						// like to print.
						_printDialog.AllowSomePages = true;
						// Show the help button.
						_printDialog.ShowHelp = true;

						_printDialog.Document = xPrintPreviewControl.Document;

						DialogResult result = _printDialog.ShowDialog(this);

						// If the result is OK then print the document.
						if (result==DialogResult.OK)
						{
							//docToPrint.Print();
							PrintingEventArgs args = new PrintingEventArgs(_displayPrintStatus);
							OnPrinting(args);
							if (args.Cancel)
							{
								break;
							}
							xPrintPreviewControl.Print(args.DisplayPrintStatus);
							OnPrinted(new EventArgs());
							return;
						}
					}
					return;

				case EXIT_KEY:
				case CLOSEPREVIEW_KEY:
					if (!base.Modal)
					{
						base.Close();
						return;
					}
					base.DialogResult = DialogResult.OK;
					return;

				case PAGE_SETUP_KEY:
					ShowPageSetupDialog();
					return;

				case PREVIOUSVIEW_KEY:
					if (!xPrintPreviewControl.HasPreviousView)
					{
						break;
					}
					xPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.PreviousView);
					return;

				case NEXTVIEW_KEY:
					if (!xPrintPreviewControl.HasNextView)
					{
						break;
					}
					xPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.NextView);
					return;

				case FIRSTPAGE_KEY:
					xPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ScrollToFirstPage);
					return;

				case LASTPAGE_KEY:
					xPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ScrollToLastPage);
					return;

				case PREVIOUSPAGE_KEY:
					xPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ScrollToPreviousPage);
					return;

				case NEXTPAGE_KEY:
					xPrintPreviewControl.PerformAction(UltraPrintPreviewControlAction.ScrollToNextPage);
					return;

				case CONTINUOUS_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						xPrintPreviewControl.Settings.Rows = Math.Max(1, _lastRowsSetting);
						return;
					}
					xPrintPreviewControl.Settings.Rows = 0;
					return;

				case REDUCEPAGETHUMBNAILS_KEY:
					xPrintPreviewThumbnail.PerformAction(UltraPrintPreviewThumbnailAction.ZoomOut);
					return;

				case ENLARGEPAGETHUMBNAILS_KEY:
					xPrintPreviewThumbnail.PerformAction(UltraPrintPreviewThumbnailAction.ZoomIn);
					return;

				case SHOWPAGENUMBERS_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						xPrintPreviewThumbnail.Settings.PageNumberDisplayStyle = PageNumberDisplayStyle.None;
						break;
					}
					xPrintPreviewThumbnail.Settings.ResetPageNumberDisplayStyle();
					return;

				default:
					return;
			}

		}

		private void xToolbarsManager_ToolKeyDown(object sender, Infragistics.Win.UltraWinToolbars.ToolKeyEventArgs e)
		{
			if (e.Tool != null)
			{
				bool flag = false;
				string key = e.Tool.Key;
				if (key != null)
				{
					if (!(key == ZOOM_KEY))
					{
						if ((key == CURRENTPAGE_KEY) && (e.KeyData == Keys.Return))
						{
							TextBoxTool tool2 = e.Tool as TextBoxTool;
							double result = 1.0;
							try
							{
								flag = double.TryParse(tool2.Text, NumberStyles.Integer, null, out result);
							}
							catch (Exception)
							{
							}
							if ((flag && (result > 0.0)) && (result <= xPrintPreviewControl.PageCount))
							{
								xPrintPreviewControl.CurrentPage = (int)result;
								tool2.SelectionStart = 0;
								tool2.SelectionLength = tool2.Text.Length;
							}
						}
					}
					else if (e.KeyData == Keys.Return)
					{
						ComboBoxTool tool = e.Tool as ComboBoxTool;
						if (tool.SelectedIndex >= 0)
						{
							((ZoomWrapper)tool.Value).UpdateZoom(xPrintPreviewControl.Settings);
						}
						else
						{
							double num = 0.0;
							string s = tool.Text.Replace("%", "");
							try
							{
								flag = double.TryParse(s, NumberStyles.Float, null, out num);
							}
							catch (Exception)
							{
							}
							if (flag)
							{
								if (num < 0.0)
								{
									num = -num;
								}
								new ZoomWrapper(num / 100.0).UpdateZoom(xPrintPreviewControl.Settings);
							}
						}
					}
				}
			}

		}

		private void xToolbarsManager_BeforeToolbarListDropdown(object sender, Infragistics.Win.UltraWinToolbars.BeforeToolbarListDropdownEventArgs e)
		{
			if (((e.Tool.Key == PAGELAYOUT_KEY) && (xPrintPreviewControl != null)) && (xPrintPreviewControl.PageCount == 0))
			{
				e.Cancel = true;
			}

		}

		private void xToolbarsManager_ToolValueChanged(object sender, Infragistics.Win.UltraWinToolbars.ToolEventArgs e)
		{
			string str;
			if (((str = e.Tool.Key) != null) && (str == ZOOM_KEY))
			{
				ComboBoxTool tool = e.Tool as ComboBoxTool;
				if ((tool.SelectedIndex >= 0) && object.Equals(tool.Value, tool.ValueList.ValueListItems[tool.SelectedIndex].DataValue))
				{
					((ZoomWrapper)tool.ValueList.ValueListItems[tool.SelectedIndex].DataValue).UpdateZoom(xPrintPreviewControl.Settings);
				}
			}

		}

		private void xPrintPreviewControl_CurrentPageChanged(object sender, EventArgs e)
		{
			UpdateCurrentPageInfo();

		}

		private void xPrintPreviewControl_CurrentZoomChanged(object sender, EventArgs e)
		{
			UpdateZoomComboAndList();

		}

		private void xPrintPreviewControl_PreviewGenerated(object sender, EventArgs e)
		{
			_rowColumnSelector.Columns = Math.Max(1, Math.Min(xPrintPreviewControl.PageCount, 8));
			_rowColumnSelector.Rows = Math.Max(1, Math.Min(xPrintPreviewControl.PageCount, 5));
			xToolbarsManager.Tools[PAGELAYOUT_KEY].SharedProps.Enabled = xPrintPreviewControl.PageCount > 0;
			UpdateMouseActionTools();
			UpdateCurrentPageInfo();

		}

		private void xPrintPreviewControl_PropertyChanged(object sender, Infragistics.Win.PropertyChangedEventArgs e)
		{
			PropChangeInfo info = e.ChangeInfo.FindTrigger(null);
			if (info.PropId is UltraPrintPreviewControlPropertyIds)
			{
				UltraPrintPreviewControlPropertyIds propId = (UltraPrintPreviewControlPropertyIds)info.PropId;
				if (propId <= UltraPrintPreviewControlPropertyIds.ZoomMode)
				{
					if (propId != UltraPrintPreviewControlPropertyIds.Document)
					{
						if (propId == UltraPrintPreviewControlPropertyIds.ZoomMode)
						{
							UpdateZoomModeTools();
						}
						return;
					}
				}
				else
				{
					switch (propId)
					{
						case UltraPrintPreviewControlPropertyIds.Rows:
							UpdateContinuousTool();
							return;

						case UltraPrintPreviewControlPropertyIds.MouseAction:
							UpdateMouseActionTools();
							return;

						case UltraPrintPreviewControlPropertyIds.Settings:
							UpdateZoomModeTools();
							UpdateMouseActionTools();
							UpdateContinuousTool();
							return;
					}
					return;
				}
				UpdateDocumentRelatedTools();
			}

		}

		private void xPrintPreviewControl_ViewHistoryChanged(object sender, EventArgs e)
		{
			UpdateViewHistoryTools();

		}
		#endregion
		
		#region User defined Methods

		private void ClosePopupContainingTool(ToolBase tool)
		{
			if (tool != null)
			{
				if (tool.OwnerIsMenu)
				{
					ClosePopupContainingTool(tool.OwningMenu);
				}
				else if (tool.OwnerIsToolbar && (tool.OwningToolbar.TearawayToolOwner != null))
				{
					ClosePopupContainingTool(tool.OwningToolbar.TearawayToolOwner);
				}
				else if (tool is PopupToolBase)
				{
					((PopupToolBase)tool).ClosePopup();
				}
			}
		}

		private string FormatPercent(double percent)
		{
			double num = percent * 100.0;
			if ((num % 1.0) == 0.0)
			{
				return percent.ToString("P0");
			}
			if ((num % 0.1) == 0.0)
			{
				return percent.ToString("P1");
			}
			return percent.ToString("P2");
		}

		private void InitializeDialogStrings()
		{
			Text = "PrintPreview_DialogCaption";

			xToolbarsManager.Tools[FILE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.File;
			xToolbarsManager.Tools[PRINT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Print;
			xToolbarsManager.Tools[PAGE_SETUP_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PageSetup;
			xToolbarsManager.Tools[EXIT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Exit;
			xToolbarsManager.Tools[VIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.View;
			xToolbarsManager.Tools[THUMBNAILS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Thumbnails;
			xToolbarsManager.Tools[ZOOMIN_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomIn;
			xToolbarsManager.Tools[ZOOMOUT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomOut;
			xToolbarsManager.Tools[ZOOM_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Zoom;
			xToolbarsManager.Tools[PAGEWIDTH_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PageWidth;
			xToolbarsManager.Tools[MARGINWIDTH_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.MarginWidth;
			xToolbarsManager.Tools[WHOLEPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.WholePage;
			xToolbarsManager.Tools[GOTO_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.GoTo;
			xToolbarsManager.Tools[FIRSTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.FirstPage;
			xToolbarsManager.Tools[PREVIOUSPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PreviousPage;
			xToolbarsManager.Tools[NEXTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.NextPage;
			xToolbarsManager.Tools[LASTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.LastPage;
			xToolbarsManager.Tools[PREVIOUSVIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PreviousView;
			xToolbarsManager.Tools[NEXTVIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.NextView;
			xToolbarsManager.Tools[TOOLS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Tools;
			xToolbarsManager.Tools[HANDTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.HandTool;
			xToolbarsManager.Tools[SNAPSHOTTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.SnapshotTool;
			xToolbarsManager.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.DynamicZoomTool;
			xToolbarsManager.Tools[ZOOMINTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomInTool;
			xToolbarsManager.Tools[ZOOMOUTTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomOutTool;
			xToolbarsManager.Tools[CURRENTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.CurrentPage;
			xToolbarsManager.Tools[CONTINUOUS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Continuous;
			xToolbarsManager.Tools[PAGELAYOUT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PageLayout;
			xToolbarsManager.Tools[CLOSEPREVIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ClosePreview;
			xToolbarsManager.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ContextMenuThumbnail;
			xToolbarsManager.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ReducePageThumbnails;
			xToolbarsManager.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.EnlargePageThumbnails;
			xToolbarsManager.Tools[SHOWPAGENUMBERS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ShowPageNumbers;
			xToolbarsManager.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ContextMenuPreviewHand;
			xToolbarsManager.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ContextMenuPreviewZoom;
			xToolbarsManager.Tools[ZOOMINCREMENTS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomIncrements;

			xToolbarsManager.Tools[FILE_KEY].SharedProps.Caption = Captions.File;
			xToolbarsManager.Tools[PRINT_KEY].SharedProps.Caption = Captions.Print;
			xToolbarsManager.Tools[PAGE_SETUP_KEY].SharedProps.Caption = Captions.PageSetup;
			xToolbarsManager.Tools[EXIT_KEY].SharedProps.Caption = Captions.Exit;
			xToolbarsManager.Tools[VIEW_KEY].SharedProps.Caption = Captions.View;
			xToolbarsManager.Tools[THUMBNAILS_KEY].SharedProps.Caption = Captions.Thumbnails;
			xToolbarsManager.Tools[ZOOMIN_KEY].SharedProps.Caption = Captions.ZoomIn;
			xToolbarsManager.Tools[ZOOMOUT_KEY].SharedProps.Caption = Captions.ZoomOut;
			xToolbarsManager.Tools[ZOOM_KEY].SharedProps.Caption = Captions.Zoom;
			xToolbarsManager.Tools[PAGEWIDTH_KEY].SharedProps.Caption = Captions.PageWidth;
			xToolbarsManager.Tools[MARGINWIDTH_KEY].SharedProps.Caption = Captions.MarginWidth;
			xToolbarsManager.Tools[WHOLEPAGE_KEY].SharedProps.Caption = Captions.WholePage;
			xToolbarsManager.Tools[GOTO_KEY].SharedProps.Caption = Captions.GoTo;
			xToolbarsManager.Tools[FIRSTPAGE_KEY].SharedProps.Caption = Captions.FirstPage;
			xToolbarsManager.Tools[PREVIOUSPAGE_KEY].SharedProps.Caption = Captions.PreviousPage;
			xToolbarsManager.Tools[NEXTPAGE_KEY].SharedProps.Caption = Captions.NextPage;
			xToolbarsManager.Tools[LASTPAGE_KEY].SharedProps.Caption = Captions.LastPage;
			xToolbarsManager.Tools[PREVIOUSVIEW_KEY].SharedProps.Caption = Captions.PreviousView;
			xToolbarsManager.Tools[NEXTVIEW_KEY].SharedProps.Caption = Captions.NextView;
			xToolbarsManager.Tools[TOOLS_KEY].SharedProps.Caption = Captions.Tools;
			xToolbarsManager.Tools[HANDTOOL_KEY].SharedProps.Caption = Captions.HandTool;
			xToolbarsManager.Tools[SNAPSHOTTOOL_KEY].SharedProps.Caption = Captions.SnapshotTool;
			xToolbarsManager.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.Caption = Captions.DynamicZoomTool;
			xToolbarsManager.Tools[ZOOMINTOOL_KEY].SharedProps.Caption = Captions.ZoomInTool;
			xToolbarsManager.Tools[ZOOMOUTTOOL_KEY].SharedProps.Caption = Captions.ZoomOutTool;
			xToolbarsManager.Tools[CURRENTPAGE_KEY].SharedProps.Caption = Captions.CurrentPage;
			xToolbarsManager.Tools[CONTINUOUS_KEY].SharedProps.Caption = Captions.Continuous;
			xToolbarsManager.Tools[PAGELAYOUT_KEY].SharedProps.Caption = Captions.PageLayout;
			xToolbarsManager.Tools[CLOSEPREVIEW_KEY].SharedProps.Caption = Captions.ClosePreview;
			xToolbarsManager.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.Caption = Captions.ContextMenuThumbnail;
			xToolbarsManager.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.Caption = Captions.ReducePageThumbnails;
			xToolbarsManager.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.Caption = Captions.EnlargePageThumbnails;
			xToolbarsManager.Tools[SHOWPAGENUMBERS_KEY].SharedProps.Caption = Captions.ShowPageNumbers;
			xToolbarsManager.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.Caption = Captions.ContextMenuPreviewHand;
			xToolbarsManager.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.Caption = Captions.ContextMenuPreviewZoom;
			xToolbarsManager.Tools[ZOOMINCREMENTS_KEY].SharedProps.Caption = Captions.ZoomIncrements;

			xToolbarsManager.Tools[FILE_KEY].SharedProps.ToolTipText = ToolTips.File;
			xToolbarsManager.Tools[PRINT_KEY].SharedProps.ToolTipText = ToolTips.Print;
			xToolbarsManager.Tools[PAGE_SETUP_KEY].SharedProps.ToolTipText = ToolTips.PageSetup;
			xToolbarsManager.Tools[EXIT_KEY].SharedProps.ToolTipText = ToolTips.Exit;
			xToolbarsManager.Tools[VIEW_KEY].SharedProps.ToolTipText = ToolTips.View;
			xToolbarsManager.Tools[THUMBNAILS_KEY].SharedProps.ToolTipText = ToolTips.Thumbnails;
			xToolbarsManager.Tools[ZOOMIN_KEY].SharedProps.ToolTipText = ToolTips.ZoomIn;
			xToolbarsManager.Tools[ZOOMOUT_KEY].SharedProps.ToolTipText = ToolTips.ZoomOut;
			xToolbarsManager.Tools[ZOOM_KEY].SharedProps.ToolTipText = ToolTips.Zoom;
			xToolbarsManager.Tools[PAGEWIDTH_KEY].SharedProps.ToolTipText = ToolTips.PageWidth;
			xToolbarsManager.Tools[MARGINWIDTH_KEY].SharedProps.ToolTipText = ToolTips.MarginWidth;
			xToolbarsManager.Tools[WHOLEPAGE_KEY].SharedProps.ToolTipText = ToolTips.WholePage;
			xToolbarsManager.Tools[GOTO_KEY].SharedProps.ToolTipText = ToolTips.GoTo;
			xToolbarsManager.Tools[FIRSTPAGE_KEY].SharedProps.ToolTipText = ToolTips.FirstPage;
			xToolbarsManager.Tools[PREVIOUSPAGE_KEY].SharedProps.ToolTipText = ToolTips.PreviousPage;
			xToolbarsManager.Tools[NEXTPAGE_KEY].SharedProps.ToolTipText = ToolTips.NextPage;
			xToolbarsManager.Tools[LASTPAGE_KEY].SharedProps.ToolTipText = ToolTips.LastPage;
			xToolbarsManager.Tools[PREVIOUSVIEW_KEY].SharedProps.ToolTipText = ToolTips.PreviousView;
			xToolbarsManager.Tools[NEXTVIEW_KEY].SharedProps.ToolTipText = ToolTips.NextView;
			xToolbarsManager.Tools[TOOLS_KEY].SharedProps.ToolTipText = ToolTips.Tools;
			xToolbarsManager.Tools[HANDTOOL_KEY].SharedProps.ToolTipText = ToolTips.HandTool;
			xToolbarsManager.Tools[SNAPSHOTTOOL_KEY].SharedProps.ToolTipText = ToolTips.SnapshotTool;
			xToolbarsManager.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.ToolTipText = ToolTips.DynamicZoomTool;
			xToolbarsManager.Tools[ZOOMINTOOL_KEY].SharedProps.ToolTipText = ToolTips.ZoomInTool;
			xToolbarsManager.Tools[ZOOMOUTTOOL_KEY].SharedProps.ToolTipText = ToolTips.ZoomOutTool;
			xToolbarsManager.Tools[CURRENTPAGE_KEY].SharedProps.ToolTipText = ToolTips.CurrentPage;
			xToolbarsManager.Tools[CONTINUOUS_KEY].SharedProps.ToolTipText = ToolTips.Continuous;
			xToolbarsManager.Tools[PAGELAYOUT_KEY].SharedProps.ToolTipText = ToolTips.PageLayout;
			xToolbarsManager.Tools[CLOSEPREVIEW_KEY].SharedProps.ToolTipText = ToolTips.ClosePreview;
			xToolbarsManager.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.ToolTipText = ToolTips.ContextMenuThumbnail;
			xToolbarsManager.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.ToolTipText = ToolTips.ReducePageThumbnails;
			xToolbarsManager.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.ToolTipText = ToolTips.EnlargePageThumbnails;
			xToolbarsManager.Tools[SHOWPAGENUMBERS_KEY].SharedProps.ToolTipText = ToolTips.ShowPageNumbers;
			xToolbarsManager.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.ToolTipText = ToolTips.ContextMenuPreviewHand;
			xToolbarsManager.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.ToolTipText = ToolTips.ContextMenuPreviewZoom;
			xToolbarsManager.Tools[ZOOMINCREMENTS_KEY].SharedProps.ToolTipText = ToolTips.ZoomIncrements;

			xToolbarsManager.Toolbars[STANDARD_KEY].Text = Captions.Standard;
			xToolbarsManager.Toolbars[MENUBAR_KEY].Text = Captions.MenuBar;
			xToolbarsManager.Toolbars[VIEW_KEY].Text = Captions.View;

			xToolbarsManager.Tools[FILE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			xToolbarsManager.Tools[PRINT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_FILE_KEY;
			xToolbarsManager.Tools[PAGE_SETUP_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_FILE_KEY;
			xToolbarsManager.Tools[EXIT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_FILE_KEY;
			xToolbarsManager.Tools[VIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			xToolbarsManager.Tools[THUMBNAILS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[ZOOMIN_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[ZOOMOUT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[ZOOM_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[PAGEWIDTH_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY;
			xToolbarsManager.Tools[MARGINWIDTH_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY;
			xToolbarsManager.Tools[WHOLEPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY;
			xToolbarsManager.Tools[GOTO_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			xToolbarsManager.Tools[FIRSTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[PREVIOUSPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;			
			xToolbarsManager.Tools[NEXTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[LASTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[PREVIOUSVIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[NEXTVIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[TOOLS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			xToolbarsManager.Tools[HANDTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			xToolbarsManager.Tools[SNAPSHOTTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			xToolbarsManager.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			xToolbarsManager.Tools[ZOOMOUTTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			xToolbarsManager.Tools[ZOOMINTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;			
			xToolbarsManager.Tools[CURRENTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[CONTINUOUS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[PAGELAYOUT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[CLOSEPREVIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			xToolbarsManager.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY;
			xToolbarsManager.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[SHOWPAGENUMBERS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			xToolbarsManager.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY;
			xToolbarsManager.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY;
			xToolbarsManager.Tools[ZOOMINCREMENTS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			
			InitializeZoomCombo();
		}

		private void InitializeZoomCombo()
		{
			ComboBoxTool tool = xToolbarsManager.Tools[ZOOM_KEY] as ComboBoxTool;
			//ListTool tool2 = xToolbarsManager.Tools["Zoom Increments"] as ListTool;
			tool.ValueList.ValueListItems.Clear();
			//tool2.ListToolItems.Clear();
			double[] array = new double[] { 0.0833, 0.12, 0.25, 0.3333, 0.5, 0.6667, 0.75, 1.0, 1.25, 1.5, 2.0, 3.0, 4.0, 6.0, 8.0, 12.0, 16.0 };
			Array.Sort<double>(array);
			Array.Reverse(array);
			for (int i = 0; i < array.Length; i++)
			{
				string displayText = FormatPercent(array[i]);
				ZoomWrapper dataValue = new ZoomWrapper(array[i]);
				tool.ValueList.ValueListItems.Add(dataValue, displayText);
				//tool2.ListToolItems.Add(displayText, displayText, false).Tag = dataValue;
			}
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.PageWidth), "PrintPreview_ZoomListItem_PageWidth");
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.MarginsWidth), "PrintPreview_ZoomListItem_MarginWidth");
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.WholePage), "PrintPreview_ZoomListItem_WholePage");
			UpdateZoomComboAndList();
		}

		public void InvalidatePreview()
		{
			xPrintPreviewControl.InvalidatePreview();
			xPrintPreviewControl.GeneratePreview(false);
		}

		private static bool IsNewPreviewRequired(PageSettings oldSettings, PageSettings newSettings)
		{
			return ((oldSettings.Bounds != newSettings.Bounds) || ((oldSettings.Color != newSettings.Color) || ((oldSettings.Landscape != newSettings.Landscape) || (!object.Equals(oldSettings.Margins, newSettings.Margins) || (!IsSameObject(oldSettings.PaperSize, newSettings.PaperSize) || (!IsSameObject(oldSettings.PaperSource, newSettings.PaperSource) || !IsSameObject(oldSettings.PrinterResolution, newSettings.PrinterResolution)))))));
		}

		private static bool IsSameObject(object printObject1, object printObject2)
		{
			if (printObject1 == printObject2)
			{
				return true;
			}
			if ((printObject1 == null) || (printObject2 == null))
			{
				return false;
			}
			if (printObject1.GetType() != printObject2.GetType())
			{
				return false;
			}
			if (printObject1 is PaperSize)
			{
				PaperSize size = printObject1 as PaperSize;
				PaperSize size2 = printObject2 as PaperSize;
				return ((((size.Kind == size2.Kind) && (size.Height == size2.Height)) && (size.Width == size2.Width)) && (size.PaperName == size2.PaperName));
			}
			if (printObject1 is PaperSource)
			{
				PaperSource source = printObject1 as PaperSource;
				PaperSource source2 = printObject2 as PaperSource;
				return ((source.Kind == source2.Kind) && (source.SourceName == source2.SourceName));
			}
			if (!(printObject1 is PrinterResolution))
			{
				return false;
			}
			PrinterResolution resolution = printObject1 as PrinterResolution;
			PrinterResolution resolution2 = printObject2 as PrinterResolution;
			return (((resolution.Kind == resolution2.Kind) && (resolution.X == resolution2.X)) && (resolution.Y == resolution2.Y));
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			xPrintPreviewControl.InvalidatePreview();
			base.OnClosing(e);
		}

		protected virtual void OnPageSetupDialogDisplaying(PageSetupDialogDisplayingEventArgs e)
		{
			PageSetupDialogDisplayingEventHandler handler = (PageSetupDialogDisplayingEventHandler)base.Events[_eventPageSetupDialogDisplaying];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		protected virtual void OnPrinted(EventArgs e)
		{
			EventHandler handler = (EventHandler)base.Events[_eventPrinted];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		protected virtual void OnPrinting(PrintingEventArgs e)
		{
			PrintingEventHandler handler = (PrintingEventHandler)base.Events[_eventPrinting];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		private void OnRowColumnSelectorMouseUp(object sender, MouseEventArgs e)
		{
			Size selection = _rowColumnSelector.Selection;
			if (!selection.IsEmpty)
			{
				xPrintPreviewControl.Settings.Columns = selection.Width;
				if ((selection.Width == 1) && (selection.Height == 1))
				{
					xPrintPreviewControl.Settings.Rows = 0;
					_lastRowsSetting = 1;
				}
				else
				{
					xPrintPreviewControl.Settings.Rows = selection.Height;
					_lastRowsSetting = selection.Height;
				}
				((StateButtonTool)xToolbarsManager.Tools[CONTINUOUS_KEY]).Checked = false;
				xPrintPreviewControl.Settings.ZoomMode = ZoomMode.WholePage;
				ToolBase toolThatContainsControl = xToolbarsManager.GetToolThatContainsControl(_rowColumnSelector);
				ClosePopupContainingTool(toolThatContainsControl);
				_rowColumnSelector.ResetSelection();
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetPreviewSettings()
		{
			xPrintPreviewControl.ResetSettings();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailCurrentPreviewPageAppearance()
		{
			xPrintPreviewThumbnail.ResetCurrentPreviewPageAppearance();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailCurrentPreviewPageNumberAppearance()
		{
			xPrintPreviewThumbnail.ResetCurrentPreviewPageNumberAppearance();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailSettings()
		{
			xPrintPreviewThumbnail.ResetSettings();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailViewBoxAppearance()
		{
			xPrintPreviewThumbnail.ResetViewBoxAppearance();
		}

		private void ResetTrackMouseEventState()
		{
			try
			{
				new ReflectionPermission(ReflectionPermissionFlag.MemberAccess).Assert();
				MethodInfo method = typeof(Control).GetMethod("UnhookMouseEvent", BindingFlags.NonPublic | BindingFlags.Instance);
				FieldInfo field = typeof(Control).GetField("trackMouseEvent", BindingFlags.NonPublic | BindingFlags.Instance);
				ResetTrackMouseEventStateHelper(this, method, field);
			}
			catch (Exception)
			{
				_canResetTrackMouseEvent = false;
			}
		}

		private void ResetTrackMouseEventStateHelper(Control control, MethodInfo resetMethod, FieldInfo trackMouseField)
		{
			resetMethod.Invoke(control, BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance, null, null, null);
			trackMouseField.SetValue(control, null, BindingFlags.NonPublic | BindingFlags.Instance, null, null);
			if (control.HasChildren)
			{
				foreach (Control control2 in control.Controls)
				{
					ResetTrackMouseEventStateHelper(control2, resetMethod, trackMouseField);
				}
			}
		}

		protected bool ShouldSerializePreviewSettings()
		{
			return xPrintPreviewControl.ShouldSerializeSettings();
		}

		protected bool ShouldSerializeThumbnailCurrentPreviewPageAppearance()
		{
			return xPrintPreviewThumbnail.ShouldSerializeCurrentPreviewPageAppearance();
		}

		protected bool ShouldSerializeThumbnailCurrentPreviewPageNumberAppearance()
		{
			return xPrintPreviewThumbnail.ShouldSerializeCurrentPreviewPageNumberAppearance();
		}

		protected bool ShouldSerializeThumbnailSettings()
		{
			return xPrintPreviewThumbnail.ShouldSerializeSettings();
		}

		protected bool ShouldSerializeThumbnailViewBoxAppearance()
		{
			return xPrintPreviewThumbnail.ShouldSerializeViewBoxAppearance();
		}

		private void ShowPageSetupDialog()
		{

			// Custom PrintSetupDialog
			//----------------------------------------------------------------
			if (xPrintPreviewControl.Document != null)
			{
				UltraPageSetup setup = new UltraPageSetup((UltraGridPrintDocument)xPrintPreviewControl.Document);
				DialogResult dr = setup.ShowDialog(this);
				setup.Dispose();

				if (dr == DialogResult.OK)
				{
					xPrintPreviewControl.InvalidatePreview();
				}
			}

			// 윈도우 기본 PrintSetupDialog
			//----------------------------------------------------------------
			//if (xPrintPreviewControl.Document != null)
			//{
			//    pageSetupDialog1.Document = xPrintPreviewControl.Document;
			//    PageSettings oldSettings = pageSetupDialog1.PageSettings.Clone() as PageSettings;
			//    oldSettings.Landscape = oldSettings.Landscape;
			//    bool flag = false;
			//    if (Environment.Version.Major < 2)
			//    {
			//        flag = NativeWindowMethods.IsMetric();
			//    }
			//    else
			//    {
			//        try
			//        {
			//            if (NativeWindowMethods.IsMetric())
			//            {
			//                PropertyDescriptor descriptor = TypeDescriptor.GetProperties(pageSetupDialog1)["EnableMetric"];
			//                if ((descriptor != null) && (descriptor.PropertyType == typeof(bool)))
			//                {
			//                    flag = !((bool)descriptor.GetValue(pageSetupDialog1));
			//                }
			//            }
			//        }
			//        catch
			//        {
			//        }
			//    }
			//    if (flag)
			//    {
			//        pageSetupDialog1.PageSettings.Margins = PrinterUnitConvert.Convert(pageSetupDialog1.PageSettings.Margins, PrinterUnit.Display, PrinterUnit.TenthsOfAMillimeter);
			//    }
			//    PageSetupDialogDisplayingEventArgs e = new PageSetupDialogDisplayingEventArgs(pageSetupDialog1);
			//    OnPageSetupDialogDisplaying(e);
			//    if (!e.Cancel)
			//    {
			//        if (pageSetupDialog1.ShowDialog(this) == DialogResult.OK)
			//        {
			//            if (IsNewPreviewRequired(oldSettings, pageSetupDialog1.PageSettings))
			//            {
			//                InvalidatePreview();
			//            }
			//        }
			//        else if (flag)
			//        {
			//            pageSetupDialog1.PageSettings.Margins = PrinterUnitConvert.Convert(pageSetupDialog1.PageSettings.Margins, PrinterUnit.TenthsOfAMillimeter, PrinterUnit.Display);
			//        }
			//    }
			//}
		}

		private void UpdateContinuousTool()
		{
			bool enabled = xToolbarsManager.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			((StateButtonTool)xToolbarsManager.Tools[CONTINUOUS_KEY]).Checked = xPrintPreviewControl.Settings.Rows == 0;
			xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UpdateCurrentPageInfo()
		{
			int currentPage = xPrintPreviewControl.CurrentPage;
			int pageCount = xPrintPreviewControl.PageCount;
			TextBoxTool tool = xToolbarsManager.Tools[CURRENTPAGE_KEY] as TextBoxTool;
			if (tool != null)
			{
				if (pageCount == 0)
				{
					tool.Text = string.Empty;
				}
				else
				{
					tool.Text = currentPage.ToString();
				}
			}
			if (xStatusBar.Panels.Exists("PageNumber"))
			{
				if (pageCount == 0)
				{
					xStatusBar.Panels["PageNumber"].Text = string.Empty;
				}
				else
				{
					xStatusBar.Panels["PageNumber"].Text = string.Format(" Page {0} OF {1}", currentPage, pageCount);
				}
			}
		}

		private void UpdateDocumentRelatedTools()
		{
			bool flag = xPrintPreviewControl.Document != null;
			bool flag2 = flag && xPrintPreviewControl.Document.PrinterSettings.IsValid;
			xToolbarsManager.Tools[PRINT_KEY].SharedProps.Enabled = flag && flag2;
			xToolbarsManager.Tools[PAGE_SETUP_KEY].SharedProps.Enabled = flag && flag2;
		}

		private void UpdateMouseActionTools()
		{
			string menuKey = null;
			string str2 = null;
			switch (xPrintPreviewControl.MouseActionResolved)
			{
				case PreviewMouseAction.Hand:
					str2 = HANDTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWHAND_KEY;
					xStatusBar.SetStatusBarText(xPrintPreviewControl, "StatusBar_Hand_Instructions");
					break;

				case PreviewMouseAction.Snapshot:
					str2 = SNAPSHOTTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWHAND_KEY;
					xStatusBar.SetStatusBarText(xPrintPreviewControl, "StatusBar_SnapShot_Instructions");
					break;

				case PreviewMouseAction.DynamicZoom:
					str2 = DYNAMICZOOMTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWZOOM_KEY;
					xStatusBar.SetStatusBarText(xPrintPreviewControl, "StatusBar_DynamicZoom_Instructions");
					break;

				case PreviewMouseAction.ZoomOut:
					str2 = ZOOMOUTTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWZOOM_KEY;
					xStatusBar.SetStatusBarText(xPrintPreviewControl, "StatusBar_ZoomOut_Instructions");
					break;

				case PreviewMouseAction.ZoomIn:
					str2 = ZOOMINTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWZOOM_KEY;
					xStatusBar.SetStatusBarText(xPrintPreviewControl, "StatusBar_ZoomIn_Instructions");
					break;

				default:
					xStatusBar.SetStatusBarText(xPrintPreviewControl, "");
					break;
			}
			if (str2 != null)
			{
				bool enabled = xToolbarsManager.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
				xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
				((StateButtonTool)xToolbarsManager.Tools[str2]).Checked = true;
				xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
			}
			if (menuKey != null)
			{
				xToolbarsManager.SetContextMenuUltra(xPrintPreviewControl, menuKey);
			}
		}

		private void UpdateUsingZoomIncrementsList(Infragistics.Win.UltraWinToolbars.ListToolItem item)
		{
			ListTool tool = xToolbarsManager.Tools[ZOOMINCREMENTS_KEY] as ListTool;
			ZoomWrapper wrapper = ((tool.SelectedItem == null) ? ((ZoomWrapper)((item == null) ? null : ((ZoomWrapper)item.Tag))) : ((ZoomWrapper)tool.SelectedItem.Tag)) as ZoomWrapper;
			if (wrapper != null)
			{
				wrapper.UpdateZoom(xPrintPreviewControl.Settings);
			}
			bool enabled = xToolbarsManager.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			int num = (item == null) ? -1 : item.Index;
			if (num != tool.SelectedItemIndex)
			{
				tool.SelectedItemIndex = num;
			}
			xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UpdateViewHistoryTools()
		{
			xToolbarsManager.Tools[PREVIOUSVIEW_KEY].SharedProps.Enabled = xPrintPreviewControl.HasPreviousView;
			xToolbarsManager.Tools[NEXTVIEW_KEY].SharedProps.Enabled = xPrintPreviewControl.HasNextView;
		}

		private void UpdateZoomComboAndList()
		{
			ComboBoxTool tool = xToolbarsManager.Tools[ZOOM_KEY] as ComboBoxTool;
			bool enabled = xToolbarsManager.EventManager.IsEnabled(ToolbarEventIds.ToolValueChanged);
			xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolValueChanged, false);
			string key = FormatPercent(xPrintPreviewControl.CurrentZoom);
			tool.Text = key;
			xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolValueChanged, enabled);
			xToolbarsManager.Tools[ZOOMOUT_KEY].SharedProps.Enabled = xPrintPreviewControl.CanZoomOut;
			xToolbarsManager.Tools[ZOOMIN_KEY].SharedProps.Enabled = xPrintPreviewControl.CanZoomIn;
			bool flag2 = xToolbarsManager.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			ListTool tool2 = xToolbarsManager.Tools[ZOOMINCREMENTS_KEY] as ListTool;
			int index = tool2.ListToolItems.IndexOf(key);
			if (index != tool2.SelectedItemIndex)
			{
				tool2.SelectedItemIndex = index;
			}
			xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolClick, flag2);
		}

		private void UpdateZoomModeTools()
		{
			bool flag = xPrintPreviewControl.Settings.ZoomMode == ZoomMode.PageWidth;
			bool flag2 = xPrintPreviewControl.Settings.ZoomMode == ZoomMode.MarginsWidth;
			bool flag3 = xPrintPreviewControl.Settings.ZoomMode == ZoomMode.WholePage;
			bool enabled = xToolbarsManager.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			((StateButtonTool)xToolbarsManager.Tools[PAGEWIDTH_KEY]).Checked = flag;
			((StateButtonTool)xToolbarsManager.Tools[MARGINWIDTH_KEY]).Checked = flag2;
			((StateButtonTool)xToolbarsManager.Tools[WHOLEPAGE_KEY]).Checked = flag3;
			xToolbarsManager.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UseStandardZoomModeIfNecessary()
		{
			if ((xToolbarsManager.OptionSets["ZoomMode"].SelectedTool == null) && (xPrintPreviewControl.Settings.ZoomMode != ZoomMode.Standard))
			{
				double currentZoom = xPrintPreviewControl.CurrentZoom;
				xPrintPreviewControl.Settings.ZoomMode = ZoomMode.Standard;
				xPrintPreviewControl.Settings.Zoom = currentZoom;
			}
		}

		#endregion
	}
}