﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Win.Mes.PrintPreview
/*	Creator		:	iDASiT (YHJUN)
/*	Create		:	2008-01-20 오후 4:39:25
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Windows.Forms;


using AnyFactory.FX.Win;

namespace iDASiT.Win.Mes
{
 
    /// <summary>
	/// PrintPreview 클래스를 선언합니다.
	/// </summary>
	public partial class F0002 : BaseForm
	{
 		#region Fields
	 
 

 

		#endregion

		#region Constructor
		/// <summary>
		/// PrintPreview의 인스턴스를 생성합니다.
		/// </summary>
		public F0002()
			: base()
		{
			InitializeComponent();

			InitializeForm();
 

		}
		#endregion

		#region InitializeForm
		private void InitializeForm()
		{
			// Form 초기화 method를 삽입합니다.
			base.EnablePrintButton = true;

			base.ThemeStyle = ThemeStyle;

			//printPreview.PageSetupDialogDisplaying +=new PageSetupDialogDisplayingEventHandler(ultraPrintPreviewDialog1_PageSetupDialogDisplaying);

		}
		#endregion

		#region Toolbar Event Handler
		/// <summary>
		/// 표준 툴바의 [신규] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnNewClick()
		{
			base.OnNewClick();


		}

		/// <summary>
		/// 표준 툴바의 [저장] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnSaveClick()
		{
			base.OnSaveClick();


		}

		/// <summary>
		/// 표준 툴바의 [조회] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnSearchClick()
		{
			base.OnSearchClick();


		}

		/// <summary>
		/// 표준 툴바의 [삭제] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnDeleteClick()
		{
			base.OnDeleteClick();


		}

		/// <summary>
		/// 표준 툴바의 [실행] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnRunClick()
		{
			base.OnRunClick();


		}

		/// <summary>
		/// 표준 툴바의 [취소] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnCancelClick()
		{
			base.OnCancelClick();


		}

		/// <summary>
		/// 표준 툴바의 [행 추가] 버튼이 눌렸을 때<br/>
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnAddRowClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnAddRowClick();
		}

		/// <summary>
		/// 표준 툴바의 [행 삭제] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnDelRowClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnDelRowClick();
		}

		/// <summary>
		/// 표준 툴바의 [엑셀] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnExcelClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnExcelClick();
		}

		/// <summary>
		/// 표준 툴바의 [미리보기] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnPreviewClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnPreviewClick();
		}

		/// <summary>
		/// 표준 툴바의 [인쇄] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnPrintClick()
		{
			//base.ToolbarTargetXGrid = ;

			base.Popup("M02F0550", ultraGrid1);
		}

		/// <summary>
		/// 표준 툴바의 [창닫기] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnExitClick()
		{
			base.OnExitClick();
		}
		#endregion

		#region Properties

		#endregion

		#region Event Methods
		private void PrintPreview_Shown(object sender, EventArgs e)
		{
			base.EnablePrintButton = true;
		}
		#endregion

		public static void Main()
		{
			Application.Run(new F0002());
		}
		private void ultraButton1_Click(object sender, EventArgs e)
		{
			//this.ultraGridPrintDocument1.DefaultPageSettings = this.printerSettingsControl1.PageSettings;

			//this.ultraPrintPreviewControl1.Document = this.ultraGridPrintDocument1;
			
			//this.ultraPrintPreviewControl1.InvalidatePreview();

			//this.ultraPrintPreviewControl1.AutoGeneratePreview = true;//			GeneratePreview(false); 
		}

		public System.Drawing.Printing.PrintDocument PrintDocument
		{
			get
			{
				return this.ultraGridPrintDocument1;
			}
		}

		private void F0550_Load(object sender, EventArgs e)
		{
			

			Common.DBWrapper myDB = new iDASiT.Win.Mes.Common.DBWrapper();
			myDB.DbConn();
			System.Data.DataSet ds = myDB.ExecuteDataSet("select * from customers");
			myDB.DbClose();
			//System.IO.DirectoryInfo d = new System.IO.DirectoryInfo(System.Windows.Forms.Application.ExecutablePath);
			
			//string dbPath = @"C:\Documents and Settings\Administrator\바탕 화면\1\WinForms\Data\NWind.mdb";

			//string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Password="""";User ID=Admin;Data Source=" 
			//    + dbPath 
			//    + @";Mode=Share Deny None;Extended Properties="""";Jet OLEDB:System database="""";Jet OLEDB:Registry Path="""";Jet OLEDB:Database Password="""";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database Password="""";Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=False";
  
			this.ultraGrid1.DataSource = ds;



			//ultraGridPrintDocument1.Grid = this.ultraGrid1;

			 
		}
   

 

		#region User defined Methods

		#endregion
	}
}