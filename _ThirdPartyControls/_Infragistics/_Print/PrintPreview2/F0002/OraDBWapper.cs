﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Data.OleDb;
using System.IO;
using System.Xml;
using System.Configuration;
using System.Collections;

namespace iDASiT.Win.Mes.Common
{

	/// <summary>
	/// DBWrapper 클래스
	/// </summary>
	public class DBWrapper
	{
		private int mCount = 0;
		private OleDbConnection mDbConn = null;
		private OleDbCommand mCmd = null;
		private OleDbDataReader mReader = null;
		StringBuilder sb = new StringBuilder();

		/// <summary>
		/// DataBase Connection Open
		/// </summary>
		public void DbConn()
		{
			try
			{

				string dbPath = @"C:\Documents and Settings\Administrator\바탕 화면\1\WinForms\Data\NWind.mdb";

				string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Password="""";User ID=Admin;Data Source=" 
				+ dbPath 
				+ @";Mode=Share Deny None;Extended Properties="""";Jet OLEDB:System database="""";Jet OLEDB:Registry Path="""";Jet OLEDB:Database Password="""";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database Password="""";Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=False";

				mDbConn = new OleDbConnection(connectionString);
				//mDbConn = new OleDbConnection(GetDSN);
				mDbConn.Open();
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "DataBase Open 실패");
				throw e;
			}
		}

		/// <summary>
		/// Get ConnectionString Property
		/// </summary>
		protected string GetDSN
		{
			get
			{
				return GetDSNXml("MES");
			}
		}
		/// <summary>
		/// Config에서 DBConnectionString을 가져옵니다.
		/// </summary>
		/// <param name="connName"></param>
		/// <returns></returns>
		protected string GetDSNXml(string connName)
		{
			string connectionString = string.Empty;
			try
			{
				System.Configuration.Configuration config 
					= ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None) as Configuration;

				ConnectionStringsSection css = config.ConnectionStrings;// ["MES"];

				for (int i=0; i< css.ConnectionStrings.Count; i++)
				{
					if (css.ConnectionStrings[i].Name == connName)
					{
						connectionString = css.ConnectionStrings[i].ConnectionString;
						//
						break;
					}
				}
			}
			catch (ConfigurationErrorsException err)
			{
				Console.WriteLine(err.ToString());
			}

			return connectionString;

		}

		/// <summary>
		/// DataBase Connection Close
		/// </summary>
		public void DbClose()
		{
			if (mDbConn == null)
			{
				return;
			}

			try
			{
				if (mDbConn.State.ToString() == "Open")
				{
					mDbConn.Close();
				}
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "DataBase Close 실패");
				throw e;
			}
		}

		/// <summary>
		/// DataBase Transaction Init
		/// </summary>
		/// <param name="TransName"></param>
		public void BeginTransaction(string TransName)
		{
			try
			{
				mCmd = new OleDbCommand();
				mCmd.Connection = mDbConn;
				mCmd.Transaction = mDbConn.BeginTransaction();
				//BeginTransaction(IsolationLevel.ReadCommitted);
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "Trancsaction Open 실패");
				throw e;
			}
		}

		/// <summary>
		/// Transaction Execute Query
		/// </summary>
		/// <param name="QueryArr"></param>
		public void ExecuteEndTransaction(string[] QueryArr)
		{
			try
			{
				foreach (string Query in QueryArr)
				{
					mCmd.CommandText = Query;
					mCmd.ExecuteNonQuery();
				}
				mCmd.Transaction.Commit();

			}
			catch (Exception e)
			{
				mCmd.Transaction.Rollback();
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "Trancsaction Commit 실패");
				throw e;
			}
		}


		/// <summary>
		/// DataBase Transaction Init 
		/// </summary>
		/// <param name="QueryArr"></param>
		public void ExecuteTransaction(string[] QueryArr)
		{
			try
			{
				mCmd = new OleDbCommand();
				mCmd.Connection = mDbConn;
				mCmd.Transaction = mDbConn.BeginTransaction();

				foreach (string Query in QueryArr)
				{
					mCmd.CommandText = Query;
					mCmd.ExecuteNonQuery();
				}
				mCmd.Transaction.Commit();

			}
			catch (Exception e)
			{
				mCmd.Transaction.Rollback();
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "Trancsaction Commit 실패");
				throw e;
			}
		}

		/// <summary>
		/// DataBase Transaction Init 
		/// </summary>
		/// <param name="QueryArr"></param>
		public void ExecuteTransaction(ArrayList QueryArr)
		{
			try
			{
				mCmd = new OleDbCommand();
				mCmd.Connection = mDbConn;
				mCmd.Transaction = mDbConn.BeginTransaction();

				foreach (string Query in QueryArr)
				{
					mCmd.CommandText = Query;
					mCmd.ExecuteNonQuery();
				}
				mCmd.Transaction.Commit();

			}
			catch (Exception e)
			{
				mCmd.Transaction.Rollback();
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "Trancsaction Commit 실패");
				throw e;
			}
		}
		/// <summary>
		/// Query Execute 
		/// </summary>
		/// <param name="Query"></param>
		public void ExecuteQuery(string Query)
		{
			try
			{
				mCmd = new OleDbCommand(Query, mDbConn);
				mCmd.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, Query);
				throw e;
			}
		}

		/// <summary>
		/// SQL DataReader Fatech Query 
		/// </summary>
		/// <param name="Query"></param>
		/// <returns></returns>
		public OleDbDataReader FatechQuery(string Query)
		{
			try
			{
				mCmd = new OleDbCommand(Query, mDbConn);
				mReader = mCmd.ExecuteReader();
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, Query);
				throw e;
			}
			return mReader;
		}

		/// <summary>
		/// SQL DataReader Close 
		/// </summary>
		public void ReaderClose()
		{
			try
			{
				if (!mReader.IsClosed)
				{
					mReader.Close();
				}
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "SQLReader Close 실패");
				throw e;
			}
		}

		/// <summary>
		/// Procedure Execute 
		/// </summary>
		/// <param name="ProcName"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public Hashtable ExecuteProc(string ProcName, IDataParameter[] parameters)
		{
			Hashtable Result = new Hashtable();
			try
			{
				OleDbCommand Cmd = BuildProcCommand(ProcName, parameters);
				Cmd.ExecuteNonQuery();
				//Console.WriteLine(Cmd.Parameters.Count);
				for (int i=0; i<Cmd.Parameters.Count; i++)
				{
					if (Cmd.Parameters[i].Direction == ParameterDirection.Output || Cmd.Parameters[i].Direction == ParameterDirection.InputOutput)
					{
						Result.Add(Cmd.Parameters[i].ParameterName, Cmd.Parameters[i].Value);
					}
				}
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "Procedure ExecuteProc Error");
				throw e;
			}
			return Result;
		}

		/// <summary>
		/// Procedure Execute 
		/// </summary>
		/// <param name="ProcName"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public int ExecuteProcReturnValue(string ProcName, IDataParameter[] parameters)
		{
			int Result = 0;
			try
			{
				OleDbCommand Cmd = BuildIntCommand(ProcName, parameters);
				Cmd.ExecuteNonQuery();
				Result = (int)Cmd.Parameters["ReturnValue"].Value;
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "Procedure ExecuteProc Error");
				throw e;
			}
			return Result;
		}
		/// <summary>
		/// SQL DataReader Fatech Procedure 
		/// </summary>
		/// <param name="ProcName"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public OleDbDataReader FatechProc(string ProcName, IDataParameter[] parameters)
		{
			OleDbCommand Cmd = BuildProcCommand(ProcName, parameters);
			try
			{
				Cmd.CommandType = CommandType.StoredProcedure;
				mReader = Cmd.ExecuteReader();
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "Procedure FatechProc Error");
				throw e;
			}
			return mReader;
		}

		/// <summary>
		/// Execute Query DateSet 
		/// </summary>
		/// <param name="Query"></param>
		/// <param name="TableName"></param>
		/// <param name="StartRecord"></param>
		/// <param name="PageSize"></param>
		/// <returns></returns>
		public DataSet ExecuteDataSet(string Query, string TableName, int StartRecord, int PageSize)
		{
			DataSet mDataSet = new DataSet();
			try
			{
				OleDbDataAdapter mDataAdapter = new OleDbDataAdapter(Query, mDbConn);
				mDataAdapter.Fill(mDataSet, StartRecord, PageSize, TableName);
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, Query);
				throw e;
			}

			return mDataSet;
		}
		/// <summary>
		/// Execute DateSet 
		/// </summary>
		/// <param name="Query"></param>
		/// <param name="TableName"></param>
		/// <returns></returns>
		public DataSet ExecuteDataSet(string Query, string TableName)
		{
			DataSet mDataSet = new DataSet();
			try
			{
				OleDbDataAdapter mDataAdapter = new OleDbDataAdapter(Query, mDbConn);
				mDataAdapter.Fill(mDataSet, TableName);
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, Query);
				throw e;
			}

			return mDataSet;
		}
		/// <summary>
		/// Execute DateSet 
		/// </summary>
		/// <param name="Query"></param>
		/// <returns></returns>
		public DataSet ExecuteDataSet(string Query)
		{
			DataSet mDataSet = new DataSet();
			try
			{
				OleDbDataAdapter mDataAdapter = new OleDbDataAdapter(Query, mDbConn);
				mDataAdapter.Fill(mDataSet);
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, Query);
				throw e;
			}

			return mDataSet;
		}

		/// <summary>
		/// Execute Procedure DateSet 
		/// </summary>
		/// <param name="ProcName"></param>
		/// <param name="parameters"></param>
		/// <param name="TableName"></param>
		/// <param name="StartRecord"></param>
		/// <param name="PageSize"></param>
		/// <returns></returns>
		public DataSet ExecuteProcDataSet(string ProcName, IDataParameter[] parameters, string TableName, int StartRecord, int PageSize)
		{
			DataSet mDataSet = new DataSet();
			OleDbDataAdapter mDataAdapter = new OleDbDataAdapter();

			mDataAdapter.SelectCommand = BuildProcCommand(ProcName, parameters);
			try
			{
				mDataAdapter.Fill(mDataSet, StartRecord, PageSize, TableName);
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "Procedure ExecuteProcDataSet Error");
				throw e;
			}

			return mDataSet;
		}

		/// <summary>
		/// Total Count Function 
		/// </summary>
		/// <param name="Query"></param>
		/// <returns></returns>
		public int TotalQuery(string Query)
		{
			try
			{
				mCmd = new OleDbCommand(Query, mDbConn);
				mCount = (int)mCmd.ExecuteScalar();
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, Query);
				throw e;
			}
			return mCount;
		}

		/// <summary>
		/// Procedure BuildIntCommand 
		/// </summary>
		/// <param name="ProcName"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		protected OleDbCommand BuildIntCommand(string ProcName, IDataParameter[] parameters)
		{
			OleDbCommand Cmd = BuildProcCommand(ProcName, parameters);

			try
			{
				Cmd.Parameters.Add(new OleDbParameter("ReturnValue",
					OleDbType.Integer,
					4, /* Size */
					ParameterDirection.ReturnValue,
					false, /* is nullable */
					0, /* byte precision */
					0, /* byte scale */
					string.Empty,
					DataRowVersion.Default,
					null));
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "Procedure BuildIntCommand Error");
				throw e;
			}

			return Cmd;
		}

		/// <summary>
		/// Procedure Parameter Build 
		/// </summary>
		/// <param name="ProcName"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		protected OleDbCommand BuildProcCommand(string ProcName, IDataParameter[] parameters)
		{
			try
			{
				mCmd = new OleDbCommand(ProcName, mDbConn);
				mCmd.CommandType = CommandType.StoredProcedure;

				foreach (OleDbParameter parameter in parameters)
				{
					mCmd.Parameters.Add(parameter);
				}
			}
			catch (Exception e)
			{
				DbErrorMsg(e.Source, e.Message, e.StackTrace, "Procedure BuildProcCommand Error");
				throw e;
			}

			return mCmd;
		}

		/// <summary>
		/// Error Message Print 
		/// </summary>
		/// <param name="ErrSource"></param>
		/// <param name="ErrMsg"></param>
		/// <param name="stack"></param>
		/// <param name="Query"></param>
		public void DbErrorMsg(string ErrSource, string ErrMsg, string stack, string Query)
		{
			DbClose();
			string ErrorMsg = "Error Souce =" + ErrSource + "\n"
				+ "Error Message = " +  ErrMsg + "\n"
				+ "Stack = " + stack + "\n\n"
				+ "Query = " + Query + "";
			Console.WriteLine(ErrorMsg);
		}
	}
}

