﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Win.Mes.PrintPreview
/*	Creator		:	iDASiT (YHJUN)
/*	Create		:	2008-01-20 오후 4:39:25
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	base.Popup("M02F0001", ultraGrid1);
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Windows.Forms;
using AnyFactory.FX.Win;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.Printing;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinStatusBar;
using Infragistics.Win.UltraWinToolbars;
using iDASiT.Win.Mes.Properties;

namespace iDASiT.Win.Mes
{
	/// <summary>
	/// PrintPreview 클래스를 선언합니다.
	/// </summary>
	public partial class F0001 : BaseForm
	{
		#region Constants
		private const string FILE_KEY = "File";
		private const string PAGE_SETUP_KEY = "Page Setup";
		private const string PRINT_KEY = "Print";
		private const string EXIT_KEY = "Exit";
		private const string VIEW_KEY = "View";
		private const string THUMBNAILS_KEY = "Thumbnails";
		private const string ZOOMIN_KEY = "Zoom In";
		private const string ZOOMOUT_KEY = "Zoom Out";
		private const string ZOOM_KEY = "Zoom";
		private const string PAGEWIDTH_KEY = "Page Width";
		private const string MARGINWIDTH_KEY = "Margin Width";
		private const string WHOLEPAGE_KEY = "Whole Page";
		private const string GOTO_KEY = "Go To";
		private const string FIRSTPAGE_KEY = "First Page";
		private const string PREVIOUSPAGE_KEY = "Previous Page";
		private const string NEXTPAGE_KEY = "Next Page";
		private const string LASTPAGE_KEY = "Last Page";
		private const string PREVIOUSVIEW_KEY = "Previous View";
		private const string NEXTVIEW_KEY = "Next View";
		private const string TOOLS_KEY = "Tools";
		private const string HANDTOOL_KEY = "Hand Tool";
		private const string SNAPSHOTTOOL_KEY = "Snapshot Tool";
		private const string DYNAMICZOOMTOOL_KEY = "Dynamic Zoom Tool";
		private const string ZOOMINTOOL_KEY = "Zoom In Tool";
		private const string ZOOMOUTTOOL_KEY = "Zoom Out Tool";
		private const string CURRENTPAGE_KEY = "Current Page";
		private const string CONTINUOUS_KEY = "Continuous";
		private const string PAGELAYOUT_KEY = "Page Layout";
		private const string CLOSEPREVIEW_KEY = "ClosePreview";
		private const string CONTEXTMENUTHUMBNAIL_KEY = "ContextMenuThumbnail";
		private const string REDUCEPAGETHUMBNAILS_KEY = "Reduce Page Thumbnails";
		private const string ENLARGEPAGETHUMBNAILS_KEY = "Enlarge Page Thumbnails";
		private const string SHOWPAGENUMBERS_KEY = "Show Page Numbers";
		private const string CONTEXTMENUPREVIEWHAND_KEY = "ContextMenuPreviewHand";
		private const string CONTEXTMENUPREVIEWZOOM_KEY = "ContextMenuPreviewZoom";
		private const string ZOOMINCREMENTS_KEY = "Zoom Increments";
		private const string STANDARD_KEY = "Standard";
		private const string MENUBAR_KEY = "MenuBar";
		private const string PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY = "PrintPreview_ToolCategory_View";
		private const string PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY = "PrintPreview_ToolCategory_Tools";
		private const string PRINTPREVIEW_TOOLCATEGORY_FILE_KEY = "PrintPreview_ToolCategory_File";
		private const string PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY = "PrintPreview_ToolCategory_Menus";
		private const string PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY = "PrintPreview_ToolCategory_Zoom_Mode";
		private const string PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY = "PrintPreview_ToolCategory_Context_Menus";
    	#endregion
		private class ZoomWrapper
		{
			// Fields
			private bool useZoomMode;
			private double zoom;
			private ZoomMode zoomMode;

			// Methods
			internal ZoomWrapper(ZoomMode zoomMode)
			{
				this.zoom = 1.0;
				this.zoomMode = zoomMode;
				this.useZoomMode = true;
			}

			internal ZoomWrapper(double zoom)
			{
				this.zoom = zoom;
				this.useZoomMode = false;
			}

			public override bool Equals(object obj)
			{
				F0001.ZoomWrapper wrapper = obj as F0001.ZoomWrapper;
				if (wrapper == null)
				{
					return false;
				}
				return (((wrapper.zoom == this.zoom) && (wrapper.zoomMode == this.zoomMode)) && (wrapper.useZoomMode == this.useZoomMode));
			}

			public override int GetHashCode()
			{
				return base.GetHashCode();
			}

			internal void UpdateZoom(PreviewSettings settings)
			{
				if (this.useZoomMode)
				{
					settings.ZoomMode = this.zoomMode;
				}
				else
				{
					settings.Zoom = this.zoom;
					if ((settings.ZoomMode != ZoomMode.Standard) && (settings.ZoomMode != ZoomMode.AutoFlow))
					{
						settings.ZoomMode = ZoomMode.Standard;
					}
				}
			}

		}

		internal class NativeWindowMethods
		{
			// Fields
			private const int LOCALE_IMEASURE = 13;
			private const int LOCALE_USER_DEFAULT = 0x400;

			// Methods
			[DllImport("kernel32", CharSet=CharSet.Auto)]
			private static extern int GetLocaleInfo(int Locale, int LCType, [MarshalAs(UnmanagedType.LPWStr)] string lpLCData, int cchData);
			private static int GetLocaleInfoApi(int Locale, int LCType, string lpLCData, int cchData)
			{
				return GetLocaleInfo(Locale, LCType, lpLCData, cchData);
			}

			internal static bool IsMetric()
			{
				try
				{
					new SecurityPermission(SecurityPermissionFlag.UnmanagedCode).Assert();
					string lpLCData = new string(' ', 2);
					GetLocaleInfoApi(0x400, 13, lpLCData, lpLCData.Length);
					return (string.Compare(lpLCData, "0") == 0);
				}
				catch (Exception)
				{
					return RegionInfo.CurrentRegion.IsMetric;
				}
			}
		}
		

		// Events
		public event PageSetupDialogDisplayingEventHandler PageSetupDialogDisplaying
		{
			add
			{
				base.Events.AddHandler(EventPageSetupDialogDisplaying, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventPageSetupDialogDisplaying, value);
			}
		}
		public event EventHandler Printed
		{
			add
			{
				base.Events.AddHandler(EventPrinted, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventPrinted, value);
			}
		}
		public event PrintingEventHandler Printing
		{
			add
			{
				base.Events.AddHandler(EventPrinting, value);
			}
			remove
			{
				base.Events.RemoveHandler(EventPrinting, value);
			}
		}
 
		#region Fields

		private bool canResetTrackMouseEvent = false;
		private IContainer container;
		private bool displayPrintStatus;
		private static readonly object EventPageSetupDialogDisplaying;
		private static readonly object EventPrinted;
		private static readonly object EventPrinting;
		private bool isFirstLoad = false;
		private int lastRowsSetting;
		private PageSetupDialog pageSetupDialog1;
		private PrintDialog printDialog;
		private PreviewRowColumnSelector rowColumnSelector;
		private bool statusBarVisible = false;
		private bool thumbnailAreaVisible;

		public object About
		{
			get
			{
				return null;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new IButtonControl AcceptButton
		{
			get
			{
				return base.AcceptButton;
			}
			set
			{
				base.AcceptButton = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new string AccessibleDescription
		{
			get
			{
				return base.AccessibleDescription;
			}
			set
			{
				base.AccessibleDescription = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new string AccessibleName
		{
			get
			{
				return base.AccessibleName;
			}
			set
			{
				base.AccessibleName = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new AccessibleRole AccessibleRole
		{
			get
			{
				return base.AccessibleRole;
			}
			set
			{
				base.AccessibleRole = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool AllowDrop
		{
			get
			{
				return base.AllowDrop;
			}
			set
			{
				base.AllowDrop = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new AnchorStyles Anchor
		{
			get
			{
				return base.Anchor;
			}
			set
			{
				base.Anchor = value;
			}
		}

		[DefaultValue(true)]
		public bool AutoGeneratePreview
		{
			get
			{
				return this.ultraPrintPreviewControl1.AutoGeneratePreview;
			}
			set
			{
				this.ultraPrintPreviewControl1.AutoGeneratePreview = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new Size AutoScaleBaseSize
		{
			get
			{
				return base.AutoScaleBaseSize;
			}
			set
			{
				base.AutoScaleBaseSize = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new bool AutoScroll
		{
			get
			{
				return base.AutoScroll;
			}
			set
			{
				base.AutoScroll = value;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new Size AutoScrollMargin
		{
			get
			{
				return base.AutoScrollMargin;
			}
			set
			{
				base.AutoScrollMargin = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new Size AutoScrollMinSize
		{
			get
			{
				return base.AutoScrollMinSize;
			}
			set
			{
				base.AutoScrollMinSize = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}
				
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Image BackgroundImage
		{
			get
			{
				return base.BackgroundImage;
			}
			set
			{
				base.BackgroundImage = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new ImageLayout BackgroundImageLayout
		{
			get
			{
				return base.BackgroundImageLayout;
			}
			set
			{
				base.BackgroundImageLayout=(value);
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new IButtonControl CancelButton
		{
			get
			{
				return base.CancelButton;
			}
			set
			{
				base.CancelButton = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new bool CausesValidation
		{
			get
			{
				return base.CausesValidation;
			}
			set
			{
				base.CausesValidation = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new Size ClientSize
		{
			get
			{
				return base.ClientSize;
			}
			set
			{
				base.ClientSize = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ContextMenu ContextMenu
		{
			get
			{
				return base.ContextMenu;
			}
			set
			{
				base.ContextMenu = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ControlBox
		{
			get
			{
				return base.ControlBox;
			}
			set
			{
				base.ControlBox = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Cursor Cursor
		{
			get
			{
				return base.Cursor;
			}
			set
			{
				base.Cursor = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new ControlBindingsCollection DataBindings
		{
			get
			{
				return base.DataBindings;
			}
		}
		
		[DefaultValue(true)]
		public bool DisplayPreviewStatus
		{
			get
			{
				return this.ultraPrintPreviewControl1.DisplayPreviewStatus;
			}
			set
			{
				this.ultraPrintPreviewControl1.DisplayPreviewStatus = value;
			}
		}
		
		[DefaultValue(true)]
		public bool DisplayPrintStatus
		{
			get
			{
				return this.displayPrintStatus;
			}
			set
			{
				this.displayPrintStatus = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new DockStyle Dock
		{
			get
			{
				return base.Dock;
			}
			set
			{
				base.Dock = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new ScrollableControl.DockPaddingEdges DockPadding
		{
			get
			{
				return base.DockPadding;
			}
		}
		
		[DefaultValue((string)null)]
		public PrintDocument Document
		{
			get
			{
				return this.ultraPrintPreviewControl1.Document;
			}
			set
			{
				this.ultraPrintPreviewControl1.Document = value;
				this.pageSetupDialog1.Document = null;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new FormBorderStyle FormBorderStyle
		{
			get
			{
				return base.FormBorderStyle;
			}
			set
			{
				base.FormBorderStyle = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new bool HelpButton
		{
			get
			{
				return base.HelpButton;
			}
			set
			{
				base.HelpButton = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Icon Icon
		{
			get
			{
				return base.Icon;
			}
			set
			{
				base.Icon = value;
			}
		}
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new ImeMode ImeMode
		{
			get
			{
				return base.ImeMode;
			}
			set
			{
				base.ImeMode = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool IsMdiContainer
		{
			get
			{
				return base.IsMdiContainer;
			}
			set
			{
				base.IsMdiContainer = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool KeyPreview
		{
			get
			{
				return base.KeyPreview;
			}
			set
			{
				base.KeyPreview = value;
			}
		}
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Point Location
		{
			get
			{
				return base.Location;
			}
			set
			{
				base.Location = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool MaximizeBox
		{
			get
			{
				return base.MaximizeBox;
			}
			set
			{
				base.MaximizeBox = value;
			}
		}

		[DefaultValue(0)]
		public int MaximumPreviewPages
		{
			get
			{
				return this.ultraPrintPreviewControl1.MaximumPreviewPages;
			}
			set
			{
				this.ultraPrintPreviewControl1.MaximumPreviewPages = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size MaximumSize
		{
			get
			{
				return base.MaximumSize;
			}
			set
			{
				base.MaximumSize = value;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new MainMenu Menu
		{
			get
			{
				return base.Menu;
			}
			set
			{
				base.Menu = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool MinimizeBox
		{
			get
			{
				return base.MinimizeBox;
			}
			set
			{
				base.MinimizeBox = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new Size MinimumSize
		{
			get
			{
				return base.MinimumSize;
			}
			set
			{
				base.MinimumSize = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Advanced)]
		public new double Opacity
		{
			get
			{
				return base.Opacity;
			}
			set
			{
				base.Opacity = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new Padding Padding
		{
			get
			{
				return base.Padding;
			}
			set
			{
				base.Padding = (value);
			}
		}
		
		[DefaultValue(1)]
		public PreviewMouseAction PreviewMouseAction
		{
			get
			{
				return this.ultraPrintPreviewControl1.MouseAction;
			}
			set
			{
				this.ultraPrintPreviewControl1.MouseAction = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PreviewSettings PreviewSettings
		{
			get
			{
				return this.ultraPrintPreviewControl1.Settings;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public UltraPrintPreviewControl PrintPreviewControl
		{
			get
			{
				return this.ultraPrintPreviewControl1;
			}
		}
		
		[EditorBrowsable(EditorBrowsableState.Never), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraPrintPreviewThumbnail PrintPreviewThumbnail
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new RightToLeft RightToLeft
		{
			get
			{
				return base.RightToLeft;
			}
			set
			{
				base.RightToLeft = value;
			}
		}
				
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ShowInTaskbar
		{
			get
			{
				return base.ShowInTaskbar;
			}
			set
			{
				base.ShowInTaskbar = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
		public new Size Size
		{
			get
			{
				return base.Size;
			}
			set
			{
				base.Size = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new SizeGripStyle SizeGripStyle
		{
			get
			{
				return base.SizeGripStyle;
			}
			set
			{
				base.SizeGripStyle = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new FormStartPosition StartPosition
		{
			get
			{
				return base.StartPosition;
			}
			set
			{
				base.StartPosition = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraStatusBar StatusBar
		{
			get
			{
				return this.ultraStatusBar1;
			}
		}

		[DefaultValue(true)]
		public bool StatusBarVisible
		{
			get
			{
				return this.statusBarVisible;
			}
			set
			{
				this.statusBarVisible = value;
				this.ultraStatusBar1.Visible = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
		public new bool TabStop
		{
			get
			{
				return base.TabStop;
			}
			set
			{
				base.TabStop = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new object Tag
		{
			get
			{
				return base.Tag;
			}
			set
			{
				base.Tag = value;
			}
		}
		
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)]
		public new string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}
		
		[DefaultValue(true)]
		public bool ThumbnailAreaVisible
		{
			get
			{
				return this.thumbnailAreaVisible;
			}
			set
			{
				this.thumbnailAreaVisible = value;

				this.ultraPrintPreviewThumbnail1.Visible = value;
				if (value == true)
				{
					this.splitContainer1.SplitterDistance = 176;
				}
				else
				{
					this.splitContainer1.SplitterDistance = 0;
				}
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailCurrentPreviewPageAppearance
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.CurrentPreviewPageAppearance;
			}
			set
			{
				this.ultraPrintPreviewThumbnail1.CurrentPreviewPageAppearance = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailCurrentPreviewPageNumberAppearance
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.CurrentPreviewPageNumberAppearance;
			}
			set
			{
				this.ultraPrintPreviewThumbnail1.CurrentPreviewPageNumberAppearance = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public PreviewSettings ThumbnailSettings
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.Settings;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public AppearanceBase ThumbnailViewBoxAppearance
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.ViewBoxAppearance;
			}
			set
			{
				this.ultraPrintPreviewThumbnail1.ViewBoxAppearance = value;
			}
		}

		[DefaultValue(0)]
		public ViewBoxDisplayStyle ThumbnailViewBoxDisplayStyle
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.ViewBoxDisplayStyle;
			}
			set
			{
				this.ultraPrintPreviewThumbnail1.ViewBoxDisplayStyle = value;
			}
		}
		
		[DefaultValue(0)]
		public ViewBoxDragMode ThumbnailViewBoxDragMode
		{
			get
			{
				return this.ultraPrintPreviewThumbnail1.ViewBoxDragMode;
			}
			set
			{
				this.ultraPrintPreviewThumbnail1.ViewBoxDragMode = value;
			}
		}

		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool TopMost
		{
			get
			{
				return base.TopMost;
			}
			set
			{
				base.TopMost = value;
			}
		}
		
		[DefaultValue(false)]
		public bool UseAntiAlias
		{
			get
			{
				return this.ultraPrintPreviewControl1.UseAntiAlias;
			}
			set
			{
				this.ultraPrintPreviewControl1.UseAntiAlias = value;
			}
		}
		
		[Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool Visible
		{
			get
			{
				return base.Visible;
			}
			set
			{
				base.Visible = value;
			}
		}
		
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
		public new FormWindowState WindowState
		{
			get
			{
				return base.WindowState;
			}
			set
			{
				base.WindowState = value;
			}
		}
 
		#endregion

		#region Constructor
		/// <summary>
		/// PrintPreview의 인스턴스를 생성합니다.
		/// </summary>
		public F0001()
			: base()
		{
			this.thumbnailAreaVisible = true;
			this.displayPrintStatus = true;
			this.statusBarVisible = true;
			this.isFirstLoad = true;
			this.canResetTrackMouseEvent = true;
			
			//초기화
			this.InitializeComponent();
			InitializeForm();

			this.rowColumnSelector = new PreviewRowColumnSelector();
			this.rowColumnSelector.Location = new Point(-10000, -10000);
			base.Controls.Add(this.rowColumnSelector);
			this.rowColumnSelector.MouseUp += new MouseEventHandler(this.OnRowColumnSelectorMouseUp);

			//this.InitializeDialogStrings();
			base.Name = "";
			this.ShowInTaskbar = false;
			this.InitializeZoomCombo();
			this.UpdateMouseActionTools();

			InitializeDialogStrings();
		}
	 
		public F0001(IContainer container)
			: this()
		{
			this.container = container;
			if (this.container != null)
			{
				this.container.Add(this);
			}
		}

		public F0001(UltraGrid xgridMain)
			: this()
		{
			if (xgridMain != null)
			{
				ultraGridPrintDocument1.Grid = xgridMain;
				this.Document = ultraGridPrintDocument1;
			}
		}

		static F0001()
		{
			EventPageSetupDialogDisplaying = new object();
			EventPrinting = new object();
			EventPrinted = new object();
		}

		#endregion

		#region InitializeForm
		private void InitializeForm()
		{
			// Form 초기화 method를 삽입합니다.


			base.ThemeStyle = ThemeStyle;
		}
		#endregion

		#region Properties

		#endregion

		#region Event Methods
		private void PrintPreview_Shown(object sender, EventArgs e)
		{

		}


		private void PrintPreview_Load(object sender, EventArgs e)
		{


		}

		private void ultraToolbarsManager2_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
		{
			switch (e.Tool.Key)
			{
				case HANDTOOL_KEY:
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.Hand;
					return;

				case SNAPSHOTTOOL_KEY:
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.Snapshot;
					return;

				case DYNAMICZOOMTOOL_KEY:
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.DynamicZoom;
					return;

				case ZOOMOUTTOOL_KEY:
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.ZoomOut;
					return;

				case ZOOMINTOOL_KEY:
					this.ultraPrintPreviewControl1.MouseAction = PreviewMouseAction.ZoomIn;
					return;

				case ZOOMIN_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ZoomIn);
					return;

				case ZOOMOUT_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ZoomOut);
					return;

				case PAGEWIDTH_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.UseStandardZoomModeIfNecessary();
						return;
					}
					this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.PageWidth;
					return;

				case MARGINWIDTH_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.UseStandardZoomModeIfNecessary();
						return;
					}
					this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.MarginsWidth;
					return;

				case WHOLEPAGE_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.UseStandardZoomModeIfNecessary();
						return;
					}
					this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.WholePage;
					return;

				case ZOOMINCREMENTS_KEY:
					this.UpdateUsingZoomIncrementsList(e.ListToolItem);

					return;

				case THUMBNAILS_KEY:
					base.SuspendLayout();
					if (((StateButtonTool)e.Tool).Checked)
					{
						this.ultraPrintPreviewThumbnail1.SendToBack();
					}
					this.ThumbnailAreaVisible = ((StateButtonTool)e.Tool).Checked;
					base.ResumeLayout(true);
					return;

				case PRINT_KEY:
					if (this.ultraPrintPreviewControl1.Document != null)
					{
						// Allow the user to choose the page range he or she would
						// like to print.
						printDialog.AllowSomePages = true;
						// Show the help button.
						printDialog.ShowHelp = true;

						this.printDialog.Document = ultraPrintPreviewControl1.Document;

						DialogResult result = printDialog.ShowDialog(this);

						// If the result is OK then print the document.
						if (result==DialogResult.OK)
						{
							//docToPrint.Print();
							PrintingEventArgs args = new PrintingEventArgs(this.displayPrintStatus);
							this.OnPrinting(args);
							if (args.Cancel)
							{
								break;
							}
							this.ultraPrintPreviewControl1.Print(args.DisplayPrintStatus);
							this.OnPrinted(new EventArgs());
							return;
						}
					}
					return;

				case EXIT_KEY:
				case CLOSEPREVIEW_KEY:
					if (!base.Modal)
					{
						base.Close();
						return;
					}
					base.DialogResult = DialogResult.OK;
					return;

				case PAGE_SETUP_KEY:
					this.ShowPageSetupDialog();
					return;

				case PREVIOUSVIEW_KEY:
					if (!this.ultraPrintPreviewControl1.HasPreviousView)
					{
						break;
					}
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.PreviousView);
					return;

				case NEXTVIEW_KEY:
					if (!this.ultraPrintPreviewControl1.HasNextView)
					{
						break;
					}
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.NextView);
					return;

				case FIRSTPAGE_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToFirstPage);
					return;

				case LASTPAGE_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToLastPage);
					return;

				case PREVIOUSPAGE_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToPreviousPage);
					return;

				case NEXTPAGE_KEY:
					this.ultraPrintPreviewControl1.PerformAction(UltraPrintPreviewControlAction.ScrollToNextPage);
					return;

				case CONTINUOUS_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.ultraPrintPreviewControl1.Settings.Rows = Math.Max(1, this.lastRowsSetting);
						return;
					}
					this.ultraPrintPreviewControl1.Settings.Rows = 0;
					return;

				case REDUCEPAGETHUMBNAILS_KEY:
					this.ultraPrintPreviewThumbnail1.PerformAction(UltraPrintPreviewThumbnailAction.ZoomOut);
					return;

				case ENLARGEPAGETHUMBNAILS_KEY:
					this.ultraPrintPreviewThumbnail1.PerformAction(UltraPrintPreviewThumbnailAction.ZoomIn);
					return;

				case SHOWPAGENUMBERS_KEY:
					if (!((StateButtonTool)e.Tool).Checked)
					{
						this.ultraPrintPreviewThumbnail1.Settings.PageNumberDisplayStyle = PageNumberDisplayStyle.None;
						break;
					}
					this.ultraPrintPreviewThumbnail1.Settings.ResetPageNumberDisplayStyle();
					return;

				default:
					return;
			}

		}

		private void ultraToolbarsManager2_ToolKeyDown(object sender, Infragistics.Win.UltraWinToolbars.ToolKeyEventArgs e)
		{
			if (e.Tool != null)
			{
				bool flag = false;
				string key = e.Tool.Key;
				if (key != null)
				{
					if (!(key == ZOOM_KEY))
					{
						if ((key == CURRENTPAGE_KEY) && (e.KeyData == Keys.Return))
						{
							TextBoxTool tool2 = e.Tool as TextBoxTool;
							double result = 1.0;
							try
							{
								flag = double.TryParse(tool2.Text, NumberStyles.Integer, null, out result);
							}
							catch (Exception)
							{
							}
							if ((flag && (result > 0.0)) && (result <= this.ultraPrintPreviewControl1.PageCount))
							{
								this.ultraPrintPreviewControl1.CurrentPage = (int)result;
								tool2.SelectionStart = 0;
								tool2.SelectionLength = tool2.Text.Length;
							}
						}
					}
					else if (e.KeyData == Keys.Return)
					{
						ComboBoxTool tool = e.Tool as ComboBoxTool;
						if (tool.SelectedIndex >= 0)
						{
							((ZoomWrapper)tool.Value).UpdateZoom(this.ultraPrintPreviewControl1.Settings);
						}
						else
						{
							double num = 0.0;
							string s = tool.Text.Replace("%", "");
							try
							{
								flag = double.TryParse(s, NumberStyles.Float, null, out num);
							}
							catch (Exception)
							{
							}
							if (flag)
							{
								if (num < 0.0)
								{
									num = -num;
								}
								new ZoomWrapper(num / 100.0).UpdateZoom(this.ultraPrintPreviewControl1.Settings);
							}
						}
					}
				}
			}

		}

		private void ultraToolbarsManager2_BeforeToolbarListDropdown(object sender, Infragistics.Win.UltraWinToolbars.BeforeToolbarListDropdownEventArgs e)
		{
			if (((e.Tool.Key == PAGELAYOUT_KEY) && (this.ultraPrintPreviewControl1 != null)) && (this.ultraPrintPreviewControl1.PageCount == 0))
			{
				e.Cancel = true;
			}

		}

		private void ultraToolbarsManager2_ToolValueChanged(object sender, Infragistics.Win.UltraWinToolbars.ToolEventArgs e)
		{
			string str;
			if (((str = e.Tool.Key) != null) && (str == ZOOM_KEY))
			{
				ComboBoxTool tool = e.Tool as ComboBoxTool;
				if ((tool.SelectedIndex >= 0) && object.Equals(tool.Value, tool.ValueList.ValueListItems[tool.SelectedIndex].DataValue))
				{
					((ZoomWrapper)tool.ValueList.ValueListItems[tool.SelectedIndex].DataValue).UpdateZoom(this.ultraPrintPreviewControl1.Settings);
				}
			}

		}

		private void ultraPrintPreviewControl1_CurrentPageChanged(object sender, EventArgs e)
		{
			this.UpdateCurrentPageInfo();

		}

		private void ultraPrintPreviewControl1_CurrentZoomChanged(object sender, EventArgs e)
		{
			this.UpdateZoomComboAndList();

		}

		private void ultraPrintPreviewControl1_PreviewGenerated(object sender, EventArgs e)
		{
			this.rowColumnSelector.Columns = Math.Max(1, Math.Min(this.ultraPrintPreviewControl1.PageCount, 8));
			this.rowColumnSelector.Rows = Math.Max(1, Math.Min(this.ultraPrintPreviewControl1.PageCount, 5));
			this.ultraToolbarsManager2.Tools[PAGELAYOUT_KEY].SharedProps.Enabled = this.ultraPrintPreviewControl1.PageCount > 0;
			this.UpdateMouseActionTools();
			this.UpdateCurrentPageInfo();

		}

		private void ultraPrintPreviewControl1_PropertyChanged(object sender, Infragistics.Win.PropertyChangedEventArgs e)
		{
			PropChangeInfo info = e.ChangeInfo.FindTrigger(null);
			if (info.PropId is UltraPrintPreviewControlPropertyIds)
			{
				UltraPrintPreviewControlPropertyIds propId = (UltraPrintPreviewControlPropertyIds)info.PropId;
				if (propId <= UltraPrintPreviewControlPropertyIds.ZoomMode)
				{
					if (propId != UltraPrintPreviewControlPropertyIds.Document)
					{
						if (propId == UltraPrintPreviewControlPropertyIds.ZoomMode)
						{
							this.UpdateZoomModeTools();
						}
						return;
					}
				}
				else
				{
					switch (propId)
					{
						case UltraPrintPreviewControlPropertyIds.Rows:
							this.UpdateContinuousTool();
							return;

						case UltraPrintPreviewControlPropertyIds.MouseAction:
							this.UpdateMouseActionTools();
							return;

						case UltraPrintPreviewControlPropertyIds.Settings:
							this.UpdateZoomModeTools();
							this.UpdateMouseActionTools();
							this.UpdateContinuousTool();
							return;
					}
					return;
				}
				this.UpdateDocumentRelatedTools();
			}

		}

		private void ultraPrintPreviewControl1_ViewHistoryChanged(object sender, EventArgs e)
		{
			this.UpdateViewHistoryTools();

		}
		#endregion
		
		#region User defined Methods

		private void ClosePopupContainingTool(ToolBase tool)
		{
			if (tool != null)
			{
				if (tool.OwnerIsMenu)
				{
					this.ClosePopupContainingTool(tool.OwningMenu);
				}
				else if (tool.OwnerIsToolbar && (tool.OwningToolbar.TearawayToolOwner != null))
				{
					this.ClosePopupContainingTool(tool.OwningToolbar.TearawayToolOwner);
				}
				else if (tool is PopupToolBase)
				{
					((PopupToolBase)tool).ClosePopup();
				}
			}
		}

		private string FormatPercent(double percent)
		{
			double num = percent * 100.0;
			if ((num % 1.0) == 0.0)
			{
				return percent.ToString("P0");
			}
			if ((num % 0.1) == 0.0)
			{
				return percent.ToString("P1");
			}
			return percent.ToString("P2");
		}

		private void InitializeDialogStrings()
		{
			this.Text = "PrintPreview_DialogCaption";

			ultraToolbarsManager2.Tools[FILE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.File;
			ultraToolbarsManager2.Tools[PRINT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Print;
			ultraToolbarsManager2.Tools[PAGE_SETUP_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PageSetup;
			ultraToolbarsManager2.Tools[EXIT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Exit;
			ultraToolbarsManager2.Tools[VIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.View;
			ultraToolbarsManager2.Tools[THUMBNAILS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Thumbnails;
			ultraToolbarsManager2.Tools[ZOOMIN_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomIn;
			ultraToolbarsManager2.Tools[ZOOMOUT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomOut;
			ultraToolbarsManager2.Tools[ZOOM_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Zoom;
			ultraToolbarsManager2.Tools[PAGEWIDTH_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PageWidth;
			ultraToolbarsManager2.Tools[MARGINWIDTH_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.MarginWidth;
			ultraToolbarsManager2.Tools[WHOLEPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.WholePage;
			ultraToolbarsManager2.Tools[GOTO_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.GoTo;
			ultraToolbarsManager2.Tools[FIRSTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.FirstPage;
			ultraToolbarsManager2.Tools[PREVIOUSPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PreviousPage;
			ultraToolbarsManager2.Tools[NEXTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.NextPage;
			ultraToolbarsManager2.Tools[LASTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.LastPage;
			ultraToolbarsManager2.Tools[PREVIOUSVIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PreviousView;
			ultraToolbarsManager2.Tools[NEXTVIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.NextView;
			ultraToolbarsManager2.Tools[TOOLS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Tools;
			ultraToolbarsManager2.Tools[HANDTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.HandTool;
			ultraToolbarsManager2.Tools[SNAPSHOTTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.SnapshotTool;
			ultraToolbarsManager2.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.DynamicZoomTool;
			ultraToolbarsManager2.Tools[ZOOMINTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomInTool;
			ultraToolbarsManager2.Tools[ZOOMOUTTOOL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomOutTool;
			ultraToolbarsManager2.Tools[CURRENTPAGE_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.CurrentPage;
			ultraToolbarsManager2.Tools[CONTINUOUS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.Continuous;
			ultraToolbarsManager2.Tools[PAGELAYOUT_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.PageLayout;
			ultraToolbarsManager2.Tools[CLOSEPREVIEW_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ClosePreview;
			ultraToolbarsManager2.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ContextMenuThumbnail;
			ultraToolbarsManager2.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ReducePageThumbnails;
			ultraToolbarsManager2.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.EnlargePageThumbnails;
			ultraToolbarsManager2.Tools[SHOWPAGENUMBERS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ShowPageNumbers;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ContextMenuPreviewHand;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ContextMenuPreviewZoom;
			ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY].SharedProps.AccessibleDescription = AccessibleDescriptions.ZoomIncrements;

			ultraToolbarsManager2.Tools[FILE_KEY].SharedProps.Caption = Captions.File;
			ultraToolbarsManager2.Tools[PRINT_KEY].SharedProps.Caption = Captions.Print;
			ultraToolbarsManager2.Tools[PAGE_SETUP_KEY].SharedProps.Caption = Captions.PageSetup;
			ultraToolbarsManager2.Tools[EXIT_KEY].SharedProps.Caption = Captions.Exit;
			ultraToolbarsManager2.Tools[VIEW_KEY].SharedProps.Caption = Captions.View;
			ultraToolbarsManager2.Tools[THUMBNAILS_KEY].SharedProps.Caption = Captions.Thumbnails;
			ultraToolbarsManager2.Tools[ZOOMIN_KEY].SharedProps.Caption = Captions.ZoomIn;
			ultraToolbarsManager2.Tools[ZOOMOUT_KEY].SharedProps.Caption = Captions.ZoomOut;
			ultraToolbarsManager2.Tools[ZOOM_KEY].SharedProps.Caption = Captions.Zoom;
			ultraToolbarsManager2.Tools[PAGEWIDTH_KEY].SharedProps.Caption = Captions.PageWidth;
			ultraToolbarsManager2.Tools[MARGINWIDTH_KEY].SharedProps.Caption = Captions.MarginWidth;
			ultraToolbarsManager2.Tools[WHOLEPAGE_KEY].SharedProps.Caption = Captions.WholePage;
			ultraToolbarsManager2.Tools[GOTO_KEY].SharedProps.Caption = Captions.GoTo;
			ultraToolbarsManager2.Tools[FIRSTPAGE_KEY].SharedProps.Caption = Captions.FirstPage;
			ultraToolbarsManager2.Tools[PREVIOUSPAGE_KEY].SharedProps.Caption = Captions.PreviousPage;
			ultraToolbarsManager2.Tools[NEXTPAGE_KEY].SharedProps.Caption = Captions.NextPage;
			ultraToolbarsManager2.Tools[LASTPAGE_KEY].SharedProps.Caption = Captions.LastPage;
			ultraToolbarsManager2.Tools[PREVIOUSVIEW_KEY].SharedProps.Caption = Captions.PreviousView;
			ultraToolbarsManager2.Tools[NEXTVIEW_KEY].SharedProps.Caption = Captions.NextView;
			ultraToolbarsManager2.Tools[TOOLS_KEY].SharedProps.Caption = Captions.Tools;
			ultraToolbarsManager2.Tools[HANDTOOL_KEY].SharedProps.Caption = Captions.HandTool;
			ultraToolbarsManager2.Tools[SNAPSHOTTOOL_KEY].SharedProps.Caption = Captions.SnapshotTool;
			ultraToolbarsManager2.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.Caption = Captions.DynamicZoomTool;
			ultraToolbarsManager2.Tools[ZOOMINTOOL_KEY].SharedProps.Caption = Captions.ZoomInTool;
			ultraToolbarsManager2.Tools[ZOOMOUTTOOL_KEY].SharedProps.Caption = Captions.ZoomOutTool;
			ultraToolbarsManager2.Tools[CURRENTPAGE_KEY].SharedProps.Caption = Captions.CurrentPage;
			ultraToolbarsManager2.Tools[CONTINUOUS_KEY].SharedProps.Caption = Captions.Continuous;
			ultraToolbarsManager2.Tools[PAGELAYOUT_KEY].SharedProps.Caption = Captions.PageLayout;
			ultraToolbarsManager2.Tools[CLOSEPREVIEW_KEY].SharedProps.Caption = Captions.ClosePreview;
			ultraToolbarsManager2.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.Caption = Captions.ContextMenuThumbnail;
			ultraToolbarsManager2.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.Caption = Captions.ReducePageThumbnails;
			ultraToolbarsManager2.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.Caption = Captions.EnlargePageThumbnails;
			ultraToolbarsManager2.Tools[SHOWPAGENUMBERS_KEY].SharedProps.Caption = Captions.ShowPageNumbers;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.Caption = Captions.ContextMenuPreviewHand;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.Caption = Captions.ContextMenuPreviewZoom;
			ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY].SharedProps.Caption = Captions.ZoomIncrements;

			ultraToolbarsManager2.Tools[FILE_KEY].SharedProps.ToolTipText = ToolTips.File;
			ultraToolbarsManager2.Tools[PRINT_KEY].SharedProps.ToolTipText = ToolTips.Print;
			ultraToolbarsManager2.Tools[PAGE_SETUP_KEY].SharedProps.ToolTipText = ToolTips.PageSetup;
			ultraToolbarsManager2.Tools[EXIT_KEY].SharedProps.ToolTipText = ToolTips.Exit;
			ultraToolbarsManager2.Tools[VIEW_KEY].SharedProps.ToolTipText = ToolTips.View;
			ultraToolbarsManager2.Tools[THUMBNAILS_KEY].SharedProps.ToolTipText = ToolTips.Thumbnails;
			ultraToolbarsManager2.Tools[ZOOMIN_KEY].SharedProps.ToolTipText = ToolTips.ZoomIn;
			ultraToolbarsManager2.Tools[ZOOMOUT_KEY].SharedProps.ToolTipText = ToolTips.ZoomOut;
			ultraToolbarsManager2.Tools[ZOOM_KEY].SharedProps.ToolTipText = ToolTips.Zoom;
			ultraToolbarsManager2.Tools[PAGEWIDTH_KEY].SharedProps.ToolTipText = ToolTips.PageWidth;
			ultraToolbarsManager2.Tools[MARGINWIDTH_KEY].SharedProps.ToolTipText = ToolTips.MarginWidth;
			ultraToolbarsManager2.Tools[WHOLEPAGE_KEY].SharedProps.ToolTipText = ToolTips.WholePage;
			ultraToolbarsManager2.Tools[GOTO_KEY].SharedProps.ToolTipText = ToolTips.GoTo;
			ultraToolbarsManager2.Tools[FIRSTPAGE_KEY].SharedProps.ToolTipText = ToolTips.FirstPage;
			ultraToolbarsManager2.Tools[PREVIOUSPAGE_KEY].SharedProps.ToolTipText = ToolTips.PreviousPage;
			ultraToolbarsManager2.Tools[NEXTPAGE_KEY].SharedProps.ToolTipText = ToolTips.NextPage;
			ultraToolbarsManager2.Tools[LASTPAGE_KEY].SharedProps.ToolTipText = ToolTips.LastPage;
			ultraToolbarsManager2.Tools[PREVIOUSVIEW_KEY].SharedProps.ToolTipText = ToolTips.PreviousView;
			ultraToolbarsManager2.Tools[NEXTVIEW_KEY].SharedProps.ToolTipText = ToolTips.NextView;
			ultraToolbarsManager2.Tools[TOOLS_KEY].SharedProps.ToolTipText = ToolTips.Tools;
			ultraToolbarsManager2.Tools[HANDTOOL_KEY].SharedProps.ToolTipText = ToolTips.HandTool;
			ultraToolbarsManager2.Tools[SNAPSHOTTOOL_KEY].SharedProps.ToolTipText = ToolTips.SnapshotTool;
			ultraToolbarsManager2.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.ToolTipText = ToolTips.DynamicZoomTool;
			ultraToolbarsManager2.Tools[ZOOMINTOOL_KEY].SharedProps.ToolTipText = ToolTips.ZoomInTool;
			ultraToolbarsManager2.Tools[ZOOMOUTTOOL_KEY].SharedProps.ToolTipText = ToolTips.ZoomOutTool;
			ultraToolbarsManager2.Tools[CURRENTPAGE_KEY].SharedProps.ToolTipText = ToolTips.CurrentPage;
			ultraToolbarsManager2.Tools[CONTINUOUS_KEY].SharedProps.ToolTipText = ToolTips.Continuous;
			ultraToolbarsManager2.Tools[PAGELAYOUT_KEY].SharedProps.ToolTipText = ToolTips.PageLayout;
			ultraToolbarsManager2.Tools[CLOSEPREVIEW_KEY].SharedProps.ToolTipText = ToolTips.ClosePreview;
			ultraToolbarsManager2.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.ToolTipText = ToolTips.ContextMenuThumbnail;
			ultraToolbarsManager2.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.ToolTipText = ToolTips.ReducePageThumbnails;
			ultraToolbarsManager2.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.ToolTipText = ToolTips.EnlargePageThumbnails;
			ultraToolbarsManager2.Tools[SHOWPAGENUMBERS_KEY].SharedProps.ToolTipText = ToolTips.ShowPageNumbers;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.ToolTipText = ToolTips.ContextMenuPreviewHand;
			ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.ToolTipText = ToolTips.ContextMenuPreviewZoom;
			ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY].SharedProps.ToolTipText = ToolTips.ZoomIncrements;

			this.ultraToolbarsManager2.Toolbars[STANDARD_KEY].Text = Captions.Standard;
			this.ultraToolbarsManager2.Toolbars[MENUBAR_KEY].Text = Captions.MenuBar;
			this.ultraToolbarsManager2.Toolbars[VIEW_KEY].Text = Captions.View;

			this.ultraToolbarsManager2.Tools[FILE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[PRINT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_FILE_KEY;
			this.ultraToolbarsManager2.Tools[PAGE_SETUP_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_FILE_KEY;
			this.ultraToolbarsManager2.Tools[EXIT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_FILE_KEY;
			this.ultraToolbarsManager2.Tools[VIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[THUMBNAILS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[ZOOMIN_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[ZOOMOUT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[ZOOM_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[PAGEWIDTH_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY;
			this.ultraToolbarsManager2.Tools[MARGINWIDTH_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY;
			this.ultraToolbarsManager2.Tools[WHOLEPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_ZOOM_MODE_KEY;
			this.ultraToolbarsManager2.Tools[GOTO_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[FIRSTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[PREVIOUSPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;			
			this.ultraToolbarsManager2.Tools[NEXTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[LASTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[PREVIOUSVIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[NEXTVIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[TOOLS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[HANDTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			this.ultraToolbarsManager2.Tools[SNAPSHOTTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			this.ultraToolbarsManager2.Tools[DYNAMICZOOMTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			this.ultraToolbarsManager2.Tools[ZOOMOUTTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			this.ultraToolbarsManager2.Tools[ZOOMINTOOL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;			
			this.ultraToolbarsManager2.Tools[CURRENTPAGE_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[CONTINUOUS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[PAGELAYOUT_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[CLOSEPREVIEW_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_TOOLS_KEY;
			this.ultraToolbarsManager2.Tools[CONTEXTMENUTHUMBNAIL_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[REDUCEPAGETHUMBNAILS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[ENLARGEPAGETHUMBNAILS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[SHOWPAGENUMBERS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			this.ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWHAND_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[CONTEXTMENUPREVIEWZOOM_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_CONTEXT_MENUS_KEY;
			this.ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY].SharedProps.Category = PRINTPREVIEW_TOOLCATEGORY_VIEW_KEY;
			
			

			this.InitializeZoomCombo();
		}

		#region Windows Form 디자이너에서 생성한 코드
		///// <summary>
		///// 디자이너 지원에 필요한 메서드입니다.
		///// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		///// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.UltraWinToolbars.OptionSet optionSet3 = new Infragistics.Win.UltraWinToolbars.OptionSet("Tools");
			Infragistics.Win.UltraWinToolbars.OptionSet optionSet4 = new Infragistics.Win.UltraWinToolbars.OptionSet("ZoomMode");
			Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar4 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("Standard");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Page Setup");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool2 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Hand Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool3 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Snapshot Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool4 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Dynamic Zoom Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool5 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom Out Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool6 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom In Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool7 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool8 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool9 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool10 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.UltraWinToolbars.ComboBoxTool tool11 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("Zoom");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool12 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool13 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Continuous", "");
			Infragistics.Win.UltraWinToolbars.PopupControlContainerTool tool14 = new Infragistics.Win.UltraWinToolbars.PopupControlContainerTool("Page Layout");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool15 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ClosePreview");
			Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar5 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("MenuBar");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool16 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool(FILE_KEY);
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool17 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("View");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool18 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Tools");
			Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar6 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("View");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool19 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool20 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.UltraWinToolbars.TextBoxTool tool21 = new Infragistics.Win.UltraWinToolbars.TextBoxTool("Current Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool22 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool23 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool24 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool25 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool26 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Thumbnails", "");
			Infragistics.Win.Appearance appearance = new Infragistics.Win.Appearance();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(F0001));
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool27 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Hand Tool", "Tools");
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool28 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Snapshot Tool", "Tools");
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool29 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Dynamic Zoom Tool", "Tools");
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool30 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom Out Tool", "Tools");
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool31 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom In Tool", "Tools");
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool32 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ComboBoxTool tool33 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("Zoom");
			Infragistics.Win.ValueList valueList2 = new Infragistics.Win.ValueList(0);
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool34 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool(FILE_KEY);
			Infragistics.Win.UltraWinToolbars.ButtonTool tool35 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool36 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Page Setup");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool37 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exit");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool38 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("View");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool39 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Thumbnails", "");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool40 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool41 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.UltraWinToolbars.ComboBoxTool tool42 = new Infragistics.Win.UltraWinToolbars.ComboBoxTool("Zoom");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool43 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool44 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool45 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool46 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Go To");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool47 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool48 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Hand Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool49 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Snapshot Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool50 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Dynamic Zoom Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool51 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom In Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool52 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Zoom Out Tool", "Tools");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool53 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Exit");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool54 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool55 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool56 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool57 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.TextBoxTool tool58 = new Infragistics.Win.UltraWinToolbars.TextBoxTool("Current Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool59 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool60 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool61 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool62 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool63 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool64 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool65 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool66 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("Go To");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool67 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool68 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool69 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool70 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool71 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool72 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.UltraWinToolbars.PopupControlContainerTool tool73 = new Infragistics.Win.UltraWinToolbars.PopupControlContainerTool("Page Layout");
			Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool74 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Page Setup");
			Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool75 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Continuous", "");
			Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool76 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("ContextMenuThumbnail");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool77 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Reduce Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool78 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Enlarge Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool79 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Show Page Numbers", "");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool80 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("ContextMenuPreviewHand");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool81 = new Infragistics.Win.UltraWinToolbars.ButtonTool("First Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool82 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool83 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool84 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Last Page");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool85 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Previous View");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool86 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Next View");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool87 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Print");
			Infragistics.Win.UltraWinToolbars.PopupMenuTool tool88 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("ContextMenuPreviewZoom");
			Infragistics.Win.UltraWinToolbars.ListTool tool89 = new Infragistics.Win.UltraWinToolbars.ListTool("Zoom Increments");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool90 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Page Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool91 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Margin Width", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool92 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Whole Page", "ZoomMode");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool93 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom In");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool94 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Zoom Out");
			Infragistics.Win.UltraWinToolbars.ListTool tool95 = new Infragistics.Win.UltraWinToolbars.ListTool("Zoom Increments");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool96 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Reduce Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.ButtonTool tool97 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Enlarge Page Thumbnails");
			Infragistics.Win.UltraWinToolbars.StateButtonTool tool98 = new Infragistics.Win.UltraWinToolbars.StateButtonTool("Show Page Numbers", "");
			Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinToolbars.ButtonTool tool99 = new Infragistics.Win.UltraWinToolbars.ButtonTool("ClosePreview");
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel3 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel4 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
			this.ultraPrintPreviewControl1 = new Infragistics.Win.Printing.UltraPrintPreviewControl();
			this.ultraPrintPreviewThumbnail1 = new Infragistics.Win.Printing.UltraPrintPreviewThumbnail();
			this.ultraToolbarsManager2 = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
			this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
			this.printDialog = new System.Windows.Forms.PrintDialog();
			this.ultraStatusBar1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
			this.pnlContainerBody_Fill_Panel_1 = new System.Windows.Forms.Panel();
			this._pnlContainerBody_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this._pnlContainerBody_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this._pnlContainerBody_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.ultraGridPrintDocument1 = new Infragistics.Win.UltraWinGrid.UltraGridPrintDocument(this.components);
			this.pnlContainerBody.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewThumbnail1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager2)).BeginInit();
			this.pnlContainerBody_Fill_Panel_1.SuspendLayout();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlContainerBody
			// 
			this.pnlContainerBody.BackColor = System.Drawing.Color.White;
			this.pnlContainerBody.Controls.Add(this.splitContainer1);
			this.pnlContainerBody.Controls.Add(this._pnlContainerBody_Toolbars_Dock_Area_Left);
			this.pnlContainerBody.Controls.Add(this._pnlContainerBody_Toolbars_Dock_Area_Right);
			this.pnlContainerBody.Controls.Add(this._pnlContainerBody_Toolbars_Dock_Area_Top);
			this.pnlContainerBody.Controls.Add(this._pnlContainerBody_Toolbars_Dock_Area_Bottom);
			this.pnlContainerBody.Size = new System.Drawing.Size(899, 545);
			// 
			// ultraPrintPreviewControl1
			// 
			this.ultraPrintPreviewControl1.BackColorInternal = System.Drawing.Color.White;
			this.ultraPrintPreviewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ultraPrintPreviewControl1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.ultraPrintPreviewControl1.Location = new System.Drawing.Point(0, 0);
			this.ultraPrintPreviewControl1.Name = "ultraPrintPreviewControl1";
			this.ultraPrintPreviewControl1.Size = new System.Drawing.Size(711, 460);
			this.ultraPrintPreviewControl1.TabIndex = 0;
			this.ultraPrintPreviewControl1.CurrentPageChanged += new System.EventHandler(this.ultraPrintPreviewControl1_CurrentPageChanged);
			this.ultraPrintPreviewControl1.PropertyChanged += new Infragistics.Win.PropertyChangedEventHandler(this.ultraPrintPreviewControl1_PropertyChanged);
			this.ultraPrintPreviewControl1.CurrentZoomChanged += new System.EventHandler(this.ultraPrintPreviewControl1_CurrentZoomChanged);
			this.ultraPrintPreviewControl1.ViewHistoryChanged += new System.EventHandler(this.ultraPrintPreviewControl1_ViewHistoryChanged);
			this.ultraPrintPreviewControl1.PreviewGenerated += new System.EventHandler(this.ultraPrintPreviewControl1_PreviewGenerated);
			// 
			// ultraPrintPreviewThumbnail1
			// 
			this.ultraPrintPreviewThumbnail1.BackColorInternal = System.Drawing.Color.White;
			this.ultraToolbarsManager2.SetContextMenuUltra(this.ultraPrintPreviewThumbnail1, "ContextMenuThumbnail");
			this.ultraPrintPreviewThumbnail1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ultraPrintPreviewThumbnail1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.ultraPrintPreviewThumbnail1.Location = new System.Drawing.Point(0, 0);
			this.ultraPrintPreviewThumbnail1.Name = "ultraPrintPreviewThumbnail1";
			this.ultraPrintPreviewThumbnail1.PreviewControl = this.ultraPrintPreviewControl1;
			this.ultraPrintPreviewThumbnail1.Size = new System.Drawing.Size(176, 460);
			this.ultraPrintPreviewThumbnail1.TabIndex = 1;
			// 
			// ultraToolbarsManager2
			// 
			this.ultraToolbarsManager2.DesignerFlags = 1;
			this.ultraToolbarsManager2.DockWithinContainer = this.pnlContainerBody;
			this.ultraToolbarsManager2.ImageSizeSmall = new System.Drawing.Size(20, 20);
			optionSet3.AllowAllUp = false;
			this.ultraToolbarsManager2.OptionSets.Add(optionSet3);
			this.ultraToolbarsManager2.OptionSets.Add(optionSet4);
			this.ultraToolbarsManager2.ShowFullMenusDelay = 500;
			ultraToolbar4.DockedColumn = 0;
			ultraToolbar4.DockedRow = 0;
			tool.InstanceProps.IsFirstInGroup = true;
			tool2.Checked = true;
			tool2.InstanceProps.IsFirstInGroup = true;
			tool7.InstanceProps.IsFirstInGroup = true;
			tool9.Checked = true;
			tool10.InstanceProps.IsFirstInGroup = true;
			tool13.Checked = true;
			tool14.InstanceProps.IsFirstInGroup = true;
			ultraToolbar4.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool,
            buttonTool1,
            tool2,
            tool3,
            tool4,
            tool5,
            tool6,
            tool7,
            tool8,
            tool9,
            tool10,
            tool11,
            tool12,
            tool13,
            tool14,
            tool15});
			ultraToolbar4.Text = "Standard";
			ultraToolbar5.DockedColumn = 0;
			ultraToolbar5.DockedRow = 2;
			ultraToolbar5.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool16,
            tool17,
            tool18});
			ultraToolbar5.ShowInToolbarList = false;
			ultraToolbar5.Text = "MenuBar";
			ultraToolbar6.DockedColumn = 0;
			ultraToolbar6.DockedRow = 1;
			ultraToolbar6.FloatingSize = new System.Drawing.Size(366, 26);
			tool24.InstanceProps.IsFirstInGroup = true;
			ultraToolbar6.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool19,
            tool20,
            tool21,
            tool22,
            tool23,
            tool24,
            tool25});
			ultraToolbar6.Text = "View";
			this.ultraToolbarsManager2.Toolbars.AddRange(new Infragistics.Win.UltraWinToolbars.UltraToolbar[] {
            ultraToolbar4,
            ultraToolbar5,
            ultraToolbar6});
			tool26.Checked = true;
			appearance.Image = ((object)(resources.GetObject("appearance.Image")));
			tool26.SharedProps.AppearancesSmall.Appearance = appearance;
			tool26.SharedProps.Caption = "축소판 그림";
			tool26.SharedProps.Category = "View";
			tool27.Checked = true;
			tool27.OptionSetKey = "Tools";
			appearance2.Image = ((object)(resources.GetObject("appearance2.Image")));
			tool27.SharedProps.AppearancesLarge.Appearance = appearance2;
			appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
			tool27.SharedProps.AppearancesSmall.Appearance = appearance3;
			tool27.SharedProps.Caption = "손 도구(&H)";
			tool27.SharedProps.Category = "Tools";
			tool27.SharedProps.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
			tool28.OptionSetKey = "Tools";
			appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
			tool28.SharedProps.AppearancesLarge.Appearance = appearance4;
			appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
			tool28.SharedProps.AppearancesSmall.Appearance = appearance5;
			tool28.SharedProps.Caption = "스넵샷 도구";
			tool28.SharedProps.Category = "Tools";
			tool29.OptionSetKey = "Tools";
			appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
			tool29.SharedProps.AppearancesLarge.Appearance = appearance6;
			appearance7.Image = ((object)(resources.GetObject("appearance7.Image")));
			tool29.SharedProps.AppearancesSmall.Appearance = appearance7;
			tool29.SharedProps.Caption = "동적 확대/축소 도구";
			tool29.SharedProps.Category = "Tools";
			tool30.OptionSetKey = "Tools";
			appearance8.Image = ((object)(resources.GetObject("appearance8.Image")));
			tool30.SharedProps.AppearancesLarge.Appearance = appearance8;
			appearance9.Image = ((object)(resources.GetObject("appearance9.Image")));
			tool30.SharedProps.AppearancesSmall.Appearance = appearance9;
			tool30.SharedProps.Caption = "축소 도구";
			tool30.SharedProps.Category = "Tools";
			tool31.OptionSetKey = "Tools";
			appearance10.Image = ((object)(resources.GetObject("appearance10.Image")));
			tool31.SharedProps.AppearancesLarge.Appearance = appearance10;
			appearance11.Image = ((object)(resources.GetObject("appearance11.Image")));
			tool31.SharedProps.AppearancesSmall.Appearance = appearance11;
			tool31.SharedProps.Caption = "확대 도구";
			tool31.SharedProps.Category = "Tools";
			appearance12.Image = ((object)(resources.GetObject("appearance12.Image")));
			tool32.SharedProps.AppearancesSmall.Appearance = appearance12;
			tool32.SharedProps.Caption = "프린트(&P)";
			tool32.SharedProps.Category = FILE_KEY;
			tool32.SharedProps.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
			tool33.DropDownStyle = Infragistics.Win.DropDownStyle.DropDown;
			tool33.SharedProps.Caption = "확대/축소:";
			tool33.SharedProps.Category = "View";
			tool33.SharedProps.ToolTipText = "Zoom";
			tool33.ValueList = valueList2;
			tool34.SharedProps.Caption = "파일(&F)";
			tool34.SharedProps.Category = "Menus";
			tool37.InstanceProps.IsFirstInGroup = true;
			tool34.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool35,
            tool36,
            tool37});
			tool38.SharedProps.Caption = "보기(&V)";
			tool38.SharedProps.Category = "Menus";
			tool39.Checked = true;
			tool40.InstanceProps.IsFirstInGroup = true;
			tool43.InstanceProps.IsFirstInGroup = true;
			tool45.Checked = true;
			tool46.InstanceProps.IsFirstInGroup = true;
			tool38.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool39,
            tool40,
            tool41,
            tool42,
            tool43,
            tool44,
            tool45,
            tool46});
			tool47.SharedProps.Caption = "도구(&T)";
			tool47.SharedProps.Category = "Menus";
			tool48.Checked = true;
			tool50.InstanceProps.IsFirstInGroup = true;
			tool47.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool48,
            tool49,
            tool50,
            tool51,
            tool52});
			tool53.SharedProps.Caption = "닫기(&X)";
			tool53.SharedProps.Category = FILE_KEY;
			appearance13.Image = ((object)(resources.GetObject("appearance13.Image")));
			tool54.SharedProps.AppearancesLarge.Appearance = appearance13;
			appearance14.Image = ((object)(resources.GetObject("appearance14.Image")));
			tool54.SharedProps.AppearancesSmall.Appearance = appearance14;
			tool54.SharedProps.Caption = "축소";
			tool54.SharedProps.Category = "View";
			appearance15.Image = ((object)(resources.GetObject("appearance15.Image")));
			tool55.SharedProps.AppearancesLarge.Appearance = appearance15;
			appearance16.Image = ((object)(resources.GetObject("appearance16.Image")));
			tool55.SharedProps.AppearancesSmall.Appearance = appearance16;
			tool55.SharedProps.Caption = "확대";
			tool55.SharedProps.Category = "View";
			appearance17.Image = ((object)(resources.GetObject("appearance17.Image")));
			tool56.SharedProps.AppearancesLarge.Appearance = appearance17;
			appearance18.Image = ((object)(resources.GetObject("appearance18.Image")));
			tool56.SharedProps.AppearancesSmall.Appearance = appearance18;
			tool56.SharedProps.Caption = "이전 보기(&P)";
			tool56.SharedProps.Category = "View";
			tool56.SharedProps.Enabled = false;
			appearance19.Image = ((object)(resources.GetObject("appearance19.Image")));
			tool57.SharedProps.AppearancesLarge.Appearance = appearance19;
			appearance20.Image = ((object)(resources.GetObject("appearance20.Image")));
			tool57.SharedProps.AppearancesSmall.Appearance = appearance20;
			tool57.SharedProps.Caption = "다음보기(&N)";
			tool57.SharedProps.Category = "View";
			tool57.SharedProps.Enabled = false;
			tool58.SharedProps.Caption = "현재 페이지";
			tool58.SharedProps.Category = "View";
			appearance21.Image = ((object)(resources.GetObject("appearance21.Image")));
			tool59.SharedProps.AppearancesLarge.Appearance = appearance21;
			appearance22.Image = ((object)(resources.GetObject("appearance22.Image")));
			tool59.SharedProps.AppearancesSmall.Appearance = appearance22;
			tool59.SharedProps.Caption = "첫 페이지";
			tool59.SharedProps.Category = "View";
			appearance23.Image = ((object)(resources.GetObject("appearance23.Image")));
			tool60.SharedProps.AppearancesLarge.Appearance = appearance23;
			appearance24.Image = ((object)(resources.GetObject("appearance24.Image")));
			tool60.SharedProps.AppearancesSmall.Appearance = appearance24;
			tool60.SharedProps.Caption = "다음 페이지";
			tool60.SharedProps.Category = "View";
			appearance25.Image = ((object)(resources.GetObject("appearance25.Image")));
			tool61.SharedProps.AppearancesLarge.Appearance = appearance25;
			appearance26.Image = ((object)(resources.GetObject("appearance26.Image")));
			tool61.SharedProps.AppearancesSmall.Appearance = appearance26;
			tool61.SharedProps.Caption = "이전 페이지";
			tool61.SharedProps.Category = "View";
			appearance27.Image = ((object)(resources.GetObject("appearance27.Image")));
			tool62.SharedProps.AppearancesLarge.Appearance = appearance27;
			appearance28.Image = ((object)(resources.GetObject("appearance28.Image")));
			tool62.SharedProps.AppearancesSmall.Appearance = appearance28;
			tool62.SharedProps.Caption = "마지막 페이지";
			tool62.SharedProps.Category = "View";
			tool63.OptionSetKey = "ZoomMode";
			appearance29.Image = ((object)(resources.GetObject("appearance29.Image")));
			tool63.SharedProps.AppearancesLarge.Appearance = appearance29;
			appearance30.Image = ((object)(resources.GetObject("appearance30.Image")));
			tool63.SharedProps.AppearancesSmall.Appearance = appearance30;
			tool63.SharedProps.Caption = "페이지 너비";
			tool63.SharedProps.Category = "Zoom Mode";
			tool64.OptionSetKey = "ZoomMode";
			appearance31.Image = ((object)(resources.GetObject("appearance31.Image")));
			tool64.SharedProps.AppearancesLarge.Appearance = appearance31;
			appearance32.Image = ((object)(resources.GetObject("appearance32.Image")));
			tool64.SharedProps.AppearancesSmall.Appearance = appearance32;
			tool64.SharedProps.Caption = "텍스트 너비";
			tool64.SharedProps.Category = "Zoom Mode";
			tool65.Checked = true;
			tool65.OptionSetKey = "ZoomMode";
			appearance33.Image = ((object)(resources.GetObject("appearance33.Image")));
			tool65.SharedProps.AppearancesLarge.Appearance = appearance33;
			appearance34.Image = ((object)(resources.GetObject("appearance34.Image")));
			tool65.SharedProps.AppearancesSmall.Appearance = appearance34;
			tool65.SharedProps.Caption = "전체 페이지";
			tool65.SharedProps.Category = "Zoom Mode";
			tool66.SharedProps.Caption = "이동";
			tool66.SharedProps.Category = "Menus";
			tool71.InstanceProps.IsFirstInGroup = true;
			tool66.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool67,
            tool68,
            tool69,
            tool70,
            tool71,
            tool72});
			tool73.DropDownArrowStyle = Infragistics.Win.UltraWinToolbars.DropDownArrowStyle.Standard;
			appearance35.Image = ((object)(resources.GetObject("appearance35.Image")));
			tool73.SharedProps.AppearancesLarge.Appearance = appearance35;
			appearance36.Image = ((object)(resources.GetObject("appearance36.Image")));
			tool73.SharedProps.AppearancesSmall.Appearance = appearance36;
			tool73.SharedProps.Caption = "Page Layout";
			tool73.SharedProps.Category = "View";
			appearance48.Image = ((object)(resources.GetObject("appearance48.Image")));
			tool74.SharedProps.AppearancesSmall.Appearance = appearance48;
			tool74.SharedProps.Caption = "페이지 설정(&U)";
			tool74.SharedProps.Category = FILE_KEY;
			tool74.SharedProps.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.DefaultForToolType;
			tool75.Checked = true;
			appearance37.Image = ((object)(resources.GetObject("appearance37.Image")));
			tool75.SharedProps.AppearancesSmall.Appearance = appearance37;
			tool75.SharedProps.Caption = "이어보기";
			tool75.SharedProps.Category = "View";
			tool76.SharedProps.Caption = "ContextMenuThumbnail";
			tool76.SharedProps.Category = "Context Menus";
			tool79.Checked = true;
			tool79.InstanceProps.IsFirstInGroup = true;
			tool76.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool77,
            tool78,
            tool79});
			tool80.SharedProps.Caption = "ContextMenuPreviewHand";
			tool80.SharedProps.Category = "Context Menus";
			tool85.InstanceProps.IsFirstInGroup = true;
			tool87.InstanceProps.IsFirstInGroup = true;
			tool80.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool81,
            tool82,
            tool83,
            tool84,
            tool85,
            tool86,
            tool87});
			tool88.SharedProps.Caption = "ContextMenuPreviewZoom";
			tool88.SharedProps.Category = "Context Menus";
			tool90.InstanceProps.IsFirstInGroup = true;
			tool92.Checked = true;
			tool93.InstanceProps.IsFirstInGroup = true;
			tool88.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool89,
            tool90,
            tool91,
            tool92,
            tool93,
            tool94});
			tool95.SharedProps.Caption = "Zoom Increments";
			tool95.SharedProps.Category = "View";
			tool96.SharedProps.Caption = "Reduce Page Thumbnails";
			tool96.SharedProps.Category = "View";
			tool97.SharedProps.Caption = "Enlarge Page Thumbnails";
			tool97.SharedProps.Category = "View";
			tool98.Checked = true;
			appearance38.Image = ((object)(resources.GetObject("appearance38.Image")));
			tool98.SharedProps.AppearancesSmall.Appearance = appearance38;
			tool98.SharedProps.Caption = "Show Thumbnail Page Numbers";
			tool98.SharedProps.Category = "View";
			tool99.SharedProps.Caption = "닫기(&C)";
			tool99.SharedProps.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.TextOnlyAlways;
			tool99.SharedProps.ShowInCustomizer = false;
			tool99.SharedProps.ToolTipText = "Close Preview";
			this.ultraToolbarsManager2.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            tool26,
            tool27,
            tool28,
            tool29,
            tool30,
            tool31,
            tool32,
            tool33,
            tool34,
            tool38,
            tool47,
            tool53,
            tool54,
            tool55,
            tool56,
            tool57,
            tool58,
            tool59,
            tool60,
            tool61,
            tool62,
            tool63,
            tool64,
            tool65,
            tool66,
            tool73,
            tool74,
            tool75,
            tool76,
            tool80,
            tool88,
            tool95,
            tool96,
            tool97,
            tool98,
            tool99});
			this.ultraToolbarsManager2.ToolKeyDown += new Infragistics.Win.UltraWinToolbars.ToolKeyEventHandler(this.ultraToolbarsManager2_ToolKeyDown);
			this.ultraToolbarsManager2.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this.ultraToolbarsManager2_ToolClick);
			this.ultraToolbarsManager2.ToolValueChanged += new Infragistics.Win.UltraWinToolbars.ToolEventHandler(this.ultraToolbarsManager2_ToolValueChanged);
			// 
			// printDialog
			// 
			this.printDialog.AllowCurrentPage = true;
			this.printDialog.AllowSelection = true;
			this.printDialog.UseEXDialog = true;
			// 
			// ultraStatusBar1
			// 
			this.ultraStatusBar1.BackColor = System.Drawing.Color.White;
			this.ultraStatusBar1.Location = new System.Drawing.Point(0, 545);
			this.ultraStatusBar1.Name = "ultraStatusBar1";
			ultraStatusPanel3.Padding = new System.Drawing.Size(2, 0);
			ultraStatusPanel3.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Spring;
			ultraStatusPanel3.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.AutoStatusText;
			ultraStatusPanel4.Key = "PageNumber";
			ultraStatusPanel4.Padding = new System.Drawing.Size(2, 0);
			ultraStatusPanel4.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Automatic;
			this.ultraStatusBar1.Panels.AddRange(new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel[] {
            ultraStatusPanel3,
            ultraStatusPanel4});
			this.ultraStatusBar1.Size = new System.Drawing.Size(899, 24);
			this.ultraStatusBar1.TabIndex = 7;
			this.ultraStatusBar1.Text = "ultraStatusBar1";
			this.ultraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2003;
			// 
			// pnlContainerBody_Fill_Panel_1
			// 
			this.pnlContainerBody_Fill_Panel_1.BackColor = System.Drawing.Color.White;
			this.pnlContainerBody_Fill_Panel_1.Controls.Add(this.ultraPrintPreviewControl1);
			this.pnlContainerBody_Fill_Panel_1.Cursor = System.Windows.Forms.Cursors.Default;
			this.pnlContainerBody_Fill_Panel_1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlContainerBody_Fill_Panel_1.Location = new System.Drawing.Point(0, 0);
			this.pnlContainerBody_Fill_Panel_1.Name = "pnlContainerBody_Fill_Panel_1";
			this.pnlContainerBody_Fill_Panel_1.Size = new System.Drawing.Size(711, 460);
			this.pnlContainerBody_Fill_Panel_1.TabIndex = 0;
			// 
			// _pnlContainerBody_Toolbars_Dock_Area_Left
			// 
			this._pnlContainerBody_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._pnlContainerBody_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.White;
			this._pnlContainerBody_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
			this._pnlContainerBody_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
			this._pnlContainerBody_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(3, 80);
			this._pnlContainerBody_Toolbars_Dock_Area_Left.Name = "_pnlContainerBody_Toolbars_Dock_Area_Left";
			this._pnlContainerBody_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(0, 460);
			this._pnlContainerBody_Toolbars_Dock_Area_Left.ToolbarsManager = this.ultraToolbarsManager2;
			// 
			// _pnlContainerBody_Toolbars_Dock_Area_Right
			// 
			this._pnlContainerBody_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._pnlContainerBody_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.White;
			this._pnlContainerBody_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
			this._pnlContainerBody_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
			this._pnlContainerBody_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(894, 80);
			this._pnlContainerBody_Toolbars_Dock_Area_Right.Name = "_pnlContainerBody_Toolbars_Dock_Area_Right";
			this._pnlContainerBody_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(0, 460);
			this._pnlContainerBody_Toolbars_Dock_Area_Right.ToolbarsManager = this.ultraToolbarsManager2;
			// 
			// _pnlContainerBody_Toolbars_Dock_Area_Top
			// 
			this._pnlContainerBody_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._pnlContainerBody_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.White;
			this._pnlContainerBody_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
			this._pnlContainerBody_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
			this._pnlContainerBody_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(3, 3);
			this._pnlContainerBody_Toolbars_Dock_Area_Top.Name = "_pnlContainerBody_Toolbars_Dock_Area_Top";
			this._pnlContainerBody_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(891, 77);
			this._pnlContainerBody_Toolbars_Dock_Area_Top.ToolbarsManager = this.ultraToolbarsManager2;
			// 
			// _pnlContainerBody_Toolbars_Dock_Area_Bottom
			// 
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.White;
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(3, 540);
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.Name = "_pnlContainerBody_Toolbars_Dock_Area_Bottom";
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(891, 0);
			this._pnlContainerBody_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.ultraToolbarsManager2;
			// 
			// splitContainer1
			// 
			this.splitContainer1.BackColor = System.Drawing.Color.White;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(3, 80);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
			this.splitContainer1.Panel1.Controls.Add(this.ultraPrintPreviewThumbnail1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
			this.splitContainer1.Panel2.Controls.Add(this.pnlContainerBody_Fill_Panel_1);
			this.splitContainer1.Size = new System.Drawing.Size(891, 460);
			this.splitContainer1.SplitterDistance = 176;
			this.splitContainer1.TabIndex = 5;
			// 
			// F0001
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(899, 569);
			this.Controls.Add(this.ultraStatusBar1);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "F0001";
			this.Load += new System.EventHandler(this.PrintPreview_Load);
			this.Controls.SetChildIndex(this.ultraStatusBar1, 0);
			this.Controls.SetChildIndex(this.pnlContainerBody, 0);
			this.pnlContainerBody.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraPrintPreviewThumbnail1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager2)).EndInit();
			this.pnlContainerBody_Fill_Panel_1.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}




		#endregion

		private void InitializeZoomCombo()
		{
			ComboBoxTool tool = this.ultraToolbarsManager2.Tools[ZOOM_KEY] as ComboBoxTool;
			//ListTool tool2 = this.ultraToolbarsManager2.Tools["Zoom Increments"] as ListTool;
			tool.ValueList.ValueListItems.Clear();
			//tool2.ListToolItems.Clear();
			double[] array = new double[] { 0.0833, 0.12, 0.25, 0.3333, 0.5, 0.6667, 0.75, 1.0, 1.25, 1.5, 2.0, 3.0, 4.0, 6.0, 8.0, 12.0, 16.0 };
			Array.Sort<double>(array);
			Array.Reverse(array);
			for (int i = 0; i < array.Length; i++)
			{
				string displayText = this.FormatPercent(array[i]);
				ZoomWrapper dataValue = new ZoomWrapper(array[i]);
				tool.ValueList.ValueListItems.Add(dataValue, displayText);
				//tool2.ListToolItems.Add(displayText, displayText, false).Tag = dataValue;
			}
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.PageWidth), "PrintPreview_ZoomListItem_PageWidth");
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.MarginsWidth), "PrintPreview_ZoomListItem_MarginWidth");
			tool.ValueList.ValueListItems.Add(new ZoomWrapper(ZoomMode.WholePage), "PrintPreview_ZoomListItem_WholePage");
			this.UpdateZoomComboAndList();
		}

		public void InvalidatePreview()
		{
			this.ultraPrintPreviewControl1.InvalidatePreview();
			this.ultraPrintPreviewControl1.GeneratePreview(false);
		}

		private static bool IsNewPreviewRequired(PageSettings oldSettings, PageSettings newSettings)
		{
			return ((oldSettings.Bounds != newSettings.Bounds) || ((oldSettings.Color != newSettings.Color) || ((oldSettings.Landscape != newSettings.Landscape) || (!object.Equals(oldSettings.Margins, newSettings.Margins) || (!IsSameObject(oldSettings.PaperSize, newSettings.PaperSize) || (!IsSameObject(oldSettings.PaperSource, newSettings.PaperSource) || !IsSameObject(oldSettings.PrinterResolution, newSettings.PrinterResolution)))))));
		}

		private static bool IsSameObject(object printObject1, object printObject2)
		{
			if (printObject1 == printObject2)
			{
				return true;
			}
			if ((printObject1 == null) || (printObject2 == null))
			{
				return false;
			}
			if (printObject1.GetType() != printObject2.GetType())
			{
				return false;
			}
			if (printObject1 is PaperSize)
			{
				PaperSize size = printObject1 as PaperSize;
				PaperSize size2 = printObject2 as PaperSize;
				return ((((size.Kind == size2.Kind) && (size.Height == size2.Height)) && (size.Width == size2.Width)) && (size.PaperName == size2.PaperName));
			}
			if (printObject1 is PaperSource)
			{
				PaperSource source = printObject1 as PaperSource;
				PaperSource source2 = printObject2 as PaperSource;
				return ((source.Kind == source2.Kind) && (source.SourceName == source2.SourceName));
			}
			if (!(printObject1 is PrinterResolution))
			{
				return false;
			}
			PrinterResolution resolution = printObject1 as PrinterResolution;
			PrinterResolution resolution2 = printObject2 as PrinterResolution;
			return (((resolution.Kind == resolution2.Kind) && (resolution.X == resolution2.X)) && (resolution.Y == resolution2.Y));
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			this.ultraPrintPreviewControl1.InvalidatePreview();
			base.OnClosing(e);
		}

		protected virtual void OnPageSetupDialogDisplaying(PageSetupDialogDisplayingEventArgs e)
		{
			PageSetupDialogDisplayingEventHandler handler = (PageSetupDialogDisplayingEventHandler)base.Events[EventPageSetupDialogDisplaying];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		protected virtual void OnPrinted(EventArgs e)
		{
			EventHandler handler = (EventHandler)base.Events[EventPrinted];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		protected virtual void OnPrinting(PrintingEventArgs e)
		{
			PrintingEventHandler handler = (PrintingEventHandler)base.Events[EventPrinting];
			if (handler != null)
			{
				handler(this, e);
			}
		}

		private void OnRowColumnSelectorMouseUp(object sender, MouseEventArgs e)
		{
			Size selection = this.rowColumnSelector.Selection;
			if (!selection.IsEmpty)
			{
				this.ultraPrintPreviewControl1.Settings.Columns = selection.Width;
				if ((selection.Width == 1) && (selection.Height == 1))
				{
					this.ultraPrintPreviewControl1.Settings.Rows = 0;
					this.lastRowsSetting = 1;
				}
				else
				{
					this.ultraPrintPreviewControl1.Settings.Rows = selection.Height;
					this.lastRowsSetting = selection.Height;
				}
				((StateButtonTool)this.ultraToolbarsManager2.Tools[CONTINUOUS_KEY]).Checked = false;
				this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.WholePage;
				ToolBase toolThatContainsControl = this.ultraToolbarsManager2.GetToolThatContainsControl(this.rowColumnSelector);
				this.ClosePopupContainingTool(toolThatContainsControl);
				this.rowColumnSelector.ResetSelection();
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetPreviewSettings()
		{
			this.ultraPrintPreviewControl1.ResetSettings();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailCurrentPreviewPageAppearance()
		{
			this.ultraPrintPreviewThumbnail1.ResetCurrentPreviewPageAppearance();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailCurrentPreviewPageNumberAppearance()
		{
			this.ultraPrintPreviewThumbnail1.ResetCurrentPreviewPageNumberAppearance();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailSettings()
		{
			this.ultraPrintPreviewThumbnail1.ResetSettings();
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetThumbnailViewBoxAppearance()
		{
			this.ultraPrintPreviewThumbnail1.ResetViewBoxAppearance();
		}

		private void ResetTrackMouseEventState()
		{
			try
			{
				new ReflectionPermission(ReflectionPermissionFlag.MemberAccess).Assert();
				MethodInfo method = typeof(Control).GetMethod("UnhookMouseEvent", BindingFlags.NonPublic | BindingFlags.Instance);
				FieldInfo field = typeof(Control).GetField("trackMouseEvent", BindingFlags.NonPublic | BindingFlags.Instance);
				this.ResetTrackMouseEventStateHelper(this, method, field);
			}
			catch (Exception)
			{
				this.canResetTrackMouseEvent = false;
			}
		}

		private void ResetTrackMouseEventStateHelper(Control control, MethodInfo resetMethod, FieldInfo trackMouseField)
		{
			resetMethod.Invoke(control, BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance, null, null, null);
			trackMouseField.SetValue(control, null, BindingFlags.NonPublic | BindingFlags.Instance, null, null);
			if (control.HasChildren)
			{
				foreach (Control control2 in control.Controls)
				{
					this.ResetTrackMouseEventStateHelper(control2, resetMethod, trackMouseField);
				}
			}
		}

		protected bool ShouldSerializePreviewSettings()
		{
			return this.ultraPrintPreviewControl1.ShouldSerializeSettings();
		}

		protected bool ShouldSerializeThumbnailCurrentPreviewPageAppearance()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeCurrentPreviewPageAppearance();
		}

		protected bool ShouldSerializeThumbnailCurrentPreviewPageNumberAppearance()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeCurrentPreviewPageNumberAppearance();
		}

		protected bool ShouldSerializeThumbnailSettings()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeSettings();
		}

		protected bool ShouldSerializeThumbnailViewBoxAppearance()
		{
			return this.ultraPrintPreviewThumbnail1.ShouldSerializeViewBoxAppearance();
		}

		private void ShowPageSetupDialog()
		{

			// Custom PrintSetupDialog
			//----------------------------------------------------------------
			if (this.ultraPrintPreviewControl1.Document != null)
			{
				UltraPageSetup setup = new UltraPageSetup((UltraGridPrintDocument)this.ultraPrintPreviewControl1.Document);
				DialogResult dr = setup.ShowDialog(this);
				setup.Dispose();

				if (dr == DialogResult.OK)
				{
					this.ultraPrintPreviewControl1.InvalidatePreview();
				}
			}

			// 윈도우 기본 PrintSetupDialog
			//----------------------------------------------------------------
			//if (this.ultraPrintPreviewControl1.Document != null)
			//{
			//    this.pageSetupDialog1.Document = this.ultraPrintPreviewControl1.Document;
			//    PageSettings oldSettings = this.pageSetupDialog1.PageSettings.Clone() as PageSettings;
			//    oldSettings.Landscape = oldSettings.Landscape;
			//    bool flag = false;
			//    if (Environment.Version.Major < 2)
			//    {
			//        flag = NativeWindowMethods.IsMetric();
			//    }
			//    else
			//    {
			//        try
			//        {
			//            if (NativeWindowMethods.IsMetric())
			//            {
			//                PropertyDescriptor descriptor = TypeDescriptor.GetProperties(this.pageSetupDialog1)["EnableMetric"];
			//                if ((descriptor != null) && (descriptor.PropertyType == typeof(bool)))
			//                {
			//                    flag = !((bool)descriptor.GetValue(this.pageSetupDialog1));
			//                }
			//            }
			//        }
			//        catch
			//        {
			//        }
			//    }
			//    if (flag)
			//    {
			//        this.pageSetupDialog1.PageSettings.Margins = PrinterUnitConvert.Convert(this.pageSetupDialog1.PageSettings.Margins, PrinterUnit.Display, PrinterUnit.TenthsOfAMillimeter);
			//    }
			//    PageSetupDialogDisplayingEventArgs e = new PageSetupDialogDisplayingEventArgs(this.pageSetupDialog1);
			//    this.OnPageSetupDialogDisplaying(e);
			//    if (!e.Cancel)
			//    {
			//        if (this.pageSetupDialog1.ShowDialog(this) == DialogResult.OK)
			//        {
			//            if (IsNewPreviewRequired(oldSettings, this.pageSetupDialog1.PageSettings))
			//            {
			//                this.InvalidatePreview();
			//            }
			//        }
			//        else if (flag)
			//        {
			//            this.pageSetupDialog1.PageSettings.Margins = PrinterUnitConvert.Convert(this.pageSetupDialog1.PageSettings.Margins, PrinterUnit.TenthsOfAMillimeter, PrinterUnit.Display);
			//        }
			//    }
			//}
		}

		private void UpdateContinuousTool()
		{
			bool enabled = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			((StateButtonTool)this.ultraToolbarsManager2.Tools[CONTINUOUS_KEY]).Checked = this.ultraPrintPreviewControl1.Settings.Rows == 0;
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UpdateCurrentPageInfo()
		{
			int currentPage = this.ultraPrintPreviewControl1.CurrentPage;
			int pageCount = this.ultraPrintPreviewControl1.PageCount;
			TextBoxTool tool = this.ultraToolbarsManager2.Tools[CURRENTPAGE_KEY] as TextBoxTool;
			if (tool != null)
			{
				if (pageCount == 0)
				{
					tool.Text = string.Empty;
				}
				else
				{
					tool.Text = currentPage.ToString();
				}
			}
			if (this.ultraStatusBar1.Panels.Exists("PageNumber"))
			{
				if (pageCount == 0)
				{
					this.ultraStatusBar1.Panels["PageNumber"].Text = string.Empty;
				}
				else
				{
					this.ultraStatusBar1.Panels["PageNumber"].Text = string.Format(" Page {0} OF {1}", currentPage, pageCount);
				}
			}
		}

		private void UpdateDocumentRelatedTools()
		{
			bool flag = this.ultraPrintPreviewControl1.Document != null;
			bool flag2 = flag && this.ultraPrintPreviewControl1.Document.PrinterSettings.IsValid;
			this.ultraToolbarsManager2.Tools[PRINT_KEY].SharedProps.Enabled = flag && flag2;
			this.ultraToolbarsManager2.Tools[PAGE_SETUP_KEY].SharedProps.Enabled = flag && flag2;
		}

		private void UpdateMouseActionTools()
		{
			string menuKey = null;
			string str2 = null;
			switch (this.ultraPrintPreviewControl1.MouseActionResolved)
			{
				case PreviewMouseAction.Hand:
					str2 = HANDTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWHAND_KEY;
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "StatusBar_Hand_Instructions");
					break;

				case PreviewMouseAction.Snapshot:
					str2 = SNAPSHOTTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWHAND_KEY;
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "StatusBar_SnapShot_Instructions");
					break;

				case PreviewMouseAction.DynamicZoom:
					str2 = DYNAMICZOOMTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWZOOM_KEY;
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "StatusBar_DynamicZoom_Instructions");
					break;

				case PreviewMouseAction.ZoomOut:
					str2 = ZOOMOUTTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWZOOM_KEY;
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "StatusBar_ZoomOut_Instructions");
					break;

				case PreviewMouseAction.ZoomIn:
					str2 = ZOOMINTOOL_KEY;
					menuKey = CONTEXTMENUPREVIEWZOOM_KEY;
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "StatusBar_ZoomIn_Instructions");
					break;

				default:
					this.ultraStatusBar1.SetStatusBarText(this.ultraPrintPreviewControl1, "");
					break;
			}
			if (str2 != null)
			{
				bool enabled = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
				this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
				((StateButtonTool)this.ultraToolbarsManager2.Tools[str2]).Checked = true;
				this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
			}
			if (menuKey != null)
			{
				this.ultraToolbarsManager2.SetContextMenuUltra(this.ultraPrintPreviewControl1, menuKey);
			}
		}

		private void UpdateUsingZoomIncrementsList(Infragistics.Win.UltraWinToolbars.ListToolItem item)
		{
			ListTool tool = this.ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY] as ListTool;
			ZoomWrapper wrapper = ((tool.SelectedItem == null) ? ((ZoomWrapper)((item == null) ? null : ((ZoomWrapper)item.Tag))) : ((ZoomWrapper)tool.SelectedItem.Tag)) as ZoomWrapper;
			if (wrapper != null)
			{
				wrapper.UpdateZoom(this.ultraPrintPreviewControl1.Settings);
			}
			bool enabled = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			int num = (item == null) ? -1 : item.Index;
			if (num != tool.SelectedItemIndex)
			{
				tool.SelectedItemIndex = num;
			}
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UpdateViewHistoryTools()
		{
			this.ultraToolbarsManager2.Tools[PREVIOUSVIEW_KEY].SharedProps.Enabled = this.ultraPrintPreviewControl1.HasPreviousView;
			this.ultraToolbarsManager2.Tools[NEXTVIEW_KEY].SharedProps.Enabled = this.ultraPrintPreviewControl1.HasNextView;
		}

		private void UpdateZoomComboAndList()
		{
			ComboBoxTool tool = this.ultraToolbarsManager2.Tools[ZOOM_KEY] as ComboBoxTool;
			bool enabled = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolValueChanged);
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolValueChanged, false);
			string key = this.FormatPercent(this.ultraPrintPreviewControl1.CurrentZoom);
			tool.Text = key;
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolValueChanged, enabled);
			this.ultraToolbarsManager2.Tools[ZOOMOUT_KEY].SharedProps.Enabled = this.ultraPrintPreviewControl1.CanZoomOut;
			this.ultraToolbarsManager2.Tools[ZOOMIN_KEY].SharedProps.Enabled = this.ultraPrintPreviewControl1.CanZoomIn;
			bool flag2 = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			ListTool tool2 = this.ultraToolbarsManager2.Tools[ZOOMINCREMENTS_KEY] as ListTool;
			int index = tool2.ListToolItems.IndexOf(key);
			if (index != tool2.SelectedItemIndex)
			{
				tool2.SelectedItemIndex = index;
			}
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, flag2);
		}

		private void UpdateZoomModeTools()
		{
			bool flag = this.ultraPrintPreviewControl1.Settings.ZoomMode == ZoomMode.PageWidth;
			bool flag2 = this.ultraPrintPreviewControl1.Settings.ZoomMode == ZoomMode.MarginsWidth;
			bool flag3 = this.ultraPrintPreviewControl1.Settings.ZoomMode == ZoomMode.WholePage;
			bool enabled = this.ultraToolbarsManager2.EventManager.IsEnabled(ToolbarEventIds.ToolClick);
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, false);
			((StateButtonTool)this.ultraToolbarsManager2.Tools[PAGEWIDTH_KEY]).Checked = flag;
			((StateButtonTool)this.ultraToolbarsManager2.Tools[MARGINWIDTH_KEY]).Checked = flag2;
			((StateButtonTool)this.ultraToolbarsManager2.Tools[WHOLEPAGE_KEY]).Checked = flag3;
			this.ultraToolbarsManager2.EventManager.SetEnabled(ToolbarEventIds.ToolClick, enabled);
		}

		private void UseStandardZoomModeIfNecessary()
		{
			if ((this.ultraToolbarsManager2.OptionSets["ZoomMode"].SelectedTool == null) && (this.ultraPrintPreviewControl1.Settings.ZoomMode != ZoomMode.Standard))
			{
				double currentZoom = this.ultraPrintPreviewControl1.CurrentZoom;
				this.ultraPrintPreviewControl1.Settings.ZoomMode = ZoomMode.Standard;
				this.ultraPrintPreviewControl1.Settings.Zoom = currentZoom;
			}
		}

		#endregion
	}
}