using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System.IO.IsolatedStorage;

namespace iDASiT.Win.Mes
{
	/// <summary>
	/// Summary description for Utilities.
	/// </summary>
	public class Utilities
	{
		#region Constructor
		private Utilities()
		{
		}
		#endregion //Constructor
	 
		#region InitializeOutlookHeaderLabel
		public static void InitializeOutlookHeaderLabel(Infragistics.Win.Misc.UltraLabel label, Infragistics.Win.UltraWinExplorerBar.UltraExplorerBarGroup group)
		{
			Infragistics.Win.AppearanceData appearance = new Infragistics.Win.AppearanceData();
			Infragistics.Win.AppearancePropFlags requestedProps = Infragistics.Win.AppearancePropFlags.AllRender;

			group.ResolveHeaderAppearance(ref appearance, ref requestedProps, false, false, false, false, false, true);

			label.Padding = new Size(3,0);
			label.Appearance.BackColor = appearance.BackColor;
			label.Appearance.BackColor2 = appearance.BackColor2;
			label.Appearance.BackGradientStyle =	appearance.BackGradientStyle;
			label.Appearance.ForeColor = appearance.ForeColor;
			label.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
			label.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
			label.Appearance.ImageHAlign = Infragistics.Win.HAlign.Right;
			label.Appearance.ImageVAlign = Infragistics.Win.VAlign.Middle;
			label.Appearance.FontData.Bold = appearance.FontData.Bold;
			label.Appearance.FontData.Name = appearance.FontData.Name;
			label.Appearance.FontData.SizeInPoints =	appearance.FontData.SizeInPoints;
			label.Height = label.PreferredSize.Height;
		}
		#endregion //InitializeOutlookHeaderLabel

		#region InitializeOutlookSplitterColor
		public static void InitializeOutlookSplitterColor(Control.ControlCollection controls)
		{
			foreach(Control control in controls)
			{
				if (control is Splitter)
					control.BackColor = Infragistics.Win.Office2003Colors.TearawayStrip;
				else if (control.HasChildren)
					InitializeOutlookSplitterColor(control.Controls);
			}
		}
		#endregion //InitializeOutlookSplitterColor

		#region IsToolChecked
		public static bool IsToolChecked(Infragistics.Win.UltraWinToolbars.ToolBase tool)
		{
			Infragistics.Win.UltraWinToolbars.StateButtonTool stateTool = tool as Infragistics.Win.UltraWinToolbars.StateButtonTool;

			return stateTool != null && stateTool.Checked;
		}
		#endregion //IsToolChecked

		#region InitializeControlForeColor
		public static void InitializeControlForeColor(Control.ControlCollection controls)
		{
			foreach(Control control in controls)
			{
				if (control is GroupBox)
					control.ForeColor = SystemColors.Highlight;
				else
					control.ForeColor = SystemColors.ControlText;

				InitializeControlForeColor(control.Controls);
			}
		}
		#endregion //InitializeControlForeColor

	}
}
