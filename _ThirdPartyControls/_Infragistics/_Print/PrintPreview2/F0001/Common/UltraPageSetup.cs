using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.Printing;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinEditors;
using System.IO;

namespace iDASiT.Win.Mes
{
	/// <summary>
	/// Summary description for UltraPageSetup.
	/// </summary>
	public class UltraPageSetup : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel pnlTop;
		private Infragistics.Win.Misc.UltraLabel lblStyleName;
		private Infragistics.Win.UltraWinEditors.UltraComboEditor cboStyleName;
		private System.Windows.Forms.Panel pnlBottom;
		private Infragistics.Win.Misc.UltraButton btnPrint;
		private Infragistics.Win.Misc.UltraButton btnPrintPreview;
		private Infragistics.Win.Misc.UltraButton btnCancel;
		private Infragistics.Win.Misc.UltraButton btnOk;
		private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
		private WinSchedulePrintDemo.PrinterSettingsControl printerSettingsControl1;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtHeaderFont;
		private Infragistics.Win.Misc.UltraButton btnHeaderFont;
		private Infragistics.Win.Misc.UltraLabel lblHeader;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtHeaderLeft;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtHeaderMiddle;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtHeaderRight;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFooterRight;
		private Infragistics.Win.Misc.UltraLabel lblFooter;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFooterFont;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFooterMiddle;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor txtFooterLeft;
		private Infragistics.Win.Misc.UltraButton btnFooterFont;
		private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkReverseOnEvenPages;
		private System.Windows.Forms.Panel pnlToolbar;
		private Infragistics.Win.Misc.UltraButton btnToolbarDate;
		private Infragistics.Win.Misc.UltraButton btnToolbarTime;
		private Infragistics.Win.Misc.UltraButton btnToolbarPageNumber;
		private Infragistics.Win.Misc.UltraButton btnToolbarUserName;
		private System.Windows.Forms.FontDialog fontDialog1;
		private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabPagePaper;
		private Infragistics.Win.UltraWinTabControl.UltraTabPageControl tabPageHeaderFooter;
		private Infragistics.Win.UltraWinTabControl.UltraTabControl tabControlMain;
		private System.Windows.Forms.ImageList imgListPreview;
		private System.ComponentModel.IContainer components;

		private UltraGridPrintDocument printDocument;
	
		#region Constructors
 
		public UltraPageSetup(UltraGridPrintDocument printDocument)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.printDocument = printDocument;
			this.btnPrint.Enabled = false;
			this.btnPrintPreview.Enabled = false;
		}

		#endregion Constructors


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UltraPageSetup));
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
			Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
			this.tabPagePaper = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
			this.tabPageHeaderFooter = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
			this.pnlToolbar = new System.Windows.Forms.Panel();
			this.btnToolbarUserName = new Infragistics.Win.Misc.UltraButton();
			this.btnToolbarPageNumber = new Infragistics.Win.Misc.UltraButton();
			this.btnToolbarTime = new Infragistics.Win.Misc.UltraButton();
			this.btnToolbarDate = new Infragistics.Win.Misc.UltraButton();
			this.chkReverseOnEvenPages = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
			this.lblHeader = new Infragistics.Win.Misc.UltraLabel();
			this.txtHeaderLeft = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.btnHeaderFont = new Infragistics.Win.Misc.UltraButton();
			this.txtHeaderFont = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.txtHeaderMiddle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.txtHeaderRight = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.txtFooterRight = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.lblFooter = new Infragistics.Win.Misc.UltraLabel();
			this.txtFooterFont = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.txtFooterMiddle = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.txtFooterLeft = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.btnFooterFont = new Infragistics.Win.Misc.UltraButton();
			this.imgListPreview = new System.Windows.Forms.ImageList(this.components);
			this.pnlTop = new System.Windows.Forms.Panel();
			this.cboStyleName = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
			this.lblStyleName = new Infragistics.Win.Misc.UltraLabel();
			this.pnlBottom = new System.Windows.Forms.Panel();
			this.btnPrint = new Infragistics.Win.Misc.UltraButton();
			this.btnPrintPreview = new Infragistics.Win.Misc.UltraButton();
			this.btnCancel = new Infragistics.Win.Misc.UltraButton();
			this.btnOk = new Infragistics.Win.Misc.UltraButton();
			this.tabControlMain = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
			this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
			this.fontDialog1 = new System.Windows.Forms.FontDialog();
			this.printerSettingsControl1 = new WinSchedulePrintDemo.PrinterSettingsControl();
			this.tabPagePaper.SuspendLayout();
			this.tabPageHeaderFooter.SuspendLayout();
			this.pnlToolbar.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtHeaderLeft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeaderFont)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeaderMiddle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeaderRight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFooterRight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFooterFont)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFooterMiddle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFooterLeft)).BeginInit();
			this.pnlTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cboStyleName)).BeginInit();
			this.pnlBottom.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabControlMain)).BeginInit();
			this.tabControlMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabPagePaper
			// 
			this.tabPagePaper.Controls.Add(this.printerSettingsControl1);
			this.tabPagePaper.Location = new System.Drawing.Point(1, 23);
			this.tabPagePaper.Name = "tabPagePaper";
			this.tabPagePaper.Size = new System.Drawing.Size(506, 299);
			// 
			// tabPageHeaderFooter
			// 
			this.tabPageHeaderFooter.Controls.Add(this.pnlToolbar);
			this.tabPageHeaderFooter.Controls.Add(this.chkReverseOnEvenPages);
			this.tabPageHeaderFooter.Controls.Add(this.lblHeader);
			this.tabPageHeaderFooter.Controls.Add(this.txtHeaderLeft);
			this.tabPageHeaderFooter.Controls.Add(this.btnHeaderFont);
			this.tabPageHeaderFooter.Controls.Add(this.txtHeaderFont);
			this.tabPageHeaderFooter.Controls.Add(this.txtHeaderMiddle);
			this.tabPageHeaderFooter.Controls.Add(this.txtHeaderRight);
			this.tabPageHeaderFooter.Controls.Add(this.txtFooterRight);
			this.tabPageHeaderFooter.Controls.Add(this.lblFooter);
			this.tabPageHeaderFooter.Controls.Add(this.txtFooterFont);
			this.tabPageHeaderFooter.Controls.Add(this.txtFooterMiddle);
			this.tabPageHeaderFooter.Controls.Add(this.txtFooterLeft);
			this.tabPageHeaderFooter.Controls.Add(this.btnFooterFont);
			this.tabPageHeaderFooter.Location = new System.Drawing.Point(-10000, -10000);
			this.tabPageHeaderFooter.Name = "tabPageHeaderFooter";
			this.tabPageHeaderFooter.Size = new System.Drawing.Size(506, 299);
			// 
			// pnlToolbar
			// 
			this.pnlToolbar.BackColor = System.Drawing.Color.Transparent;
			this.pnlToolbar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlToolbar.Controls.Add(this.btnToolbarUserName);
			this.pnlToolbar.Controls.Add(this.btnToolbarPageNumber);
			this.pnlToolbar.Controls.Add(this.btnToolbarTime);
			this.pnlToolbar.Controls.Add(this.btnToolbarDate);
			this.pnlToolbar.Location = new System.Drawing.Point(164, 256);
			this.pnlToolbar.Name = "pnlToolbar";
			this.pnlToolbar.Size = new System.Drawing.Size(136, 24);
			this.pnlToolbar.TabIndex = 5;
			// 
			// btnToolbarUserName
			// 
			this.btnToolbarUserName.AcceptsFocus = false;
			appearance3.Image = ((object)(resources.GetObject("appearance3.Image")));
			this.btnToolbarUserName.Appearance = appearance3;
			this.btnToolbarUserName.AutoSize = true;
			this.btnToolbarUserName.ButtonStyle = Infragistics.Win.UIElementButtonStyle.OfficeXPToolbarButton;
			this.btnToolbarUserName.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnToolbarUserName.Location = new System.Drawing.Point(104, 0);
			this.btnToolbarUserName.Name = "btnToolbarUserName";
			this.btnToolbarUserName.ShowOutline = false;
			this.btnToolbarUserName.Size = new System.Drawing.Size(24, 24);
			this.btnToolbarUserName.TabIndex = 3;
			this.btnToolbarUserName.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.btnToolbarUserName.Click += new System.EventHandler(this.OnToolbarButtonClick);
			// 
			// btnToolbarPageNumber
			// 
			this.btnToolbarPageNumber.AcceptsFocus = false;
			appearance4.Image = ((object)(resources.GetObject("appearance4.Image")));
			this.btnToolbarPageNumber.Appearance = appearance4;
			this.btnToolbarPageNumber.AutoSize = true;
			this.btnToolbarPageNumber.ButtonStyle = Infragistics.Win.UIElementButtonStyle.OfficeXPToolbarButton;
			this.btnToolbarPageNumber.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnToolbarPageNumber.Location = new System.Drawing.Point(8, 0);
			this.btnToolbarPageNumber.Name = "btnToolbarPageNumber";
			this.btnToolbarPageNumber.ShowOutline = false;
			this.btnToolbarPageNumber.Size = new System.Drawing.Size(24, 24);
			this.btnToolbarPageNumber.TabIndex = 2;
			this.btnToolbarPageNumber.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.btnToolbarPageNumber.Click += new System.EventHandler(this.OnToolbarButtonClick);
			// 
			// btnToolbarTime
			// 
			this.btnToolbarTime.AcceptsFocus = false;
			appearance5.Image = ((object)(resources.GetObject("appearance5.Image")));
			this.btnToolbarTime.Appearance = appearance5;
			this.btnToolbarTime.AutoSize = true;
			this.btnToolbarTime.ButtonStyle = Infragistics.Win.UIElementButtonStyle.OfficeXPToolbarButton;
			this.btnToolbarTime.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnToolbarTime.Location = new System.Drawing.Point(72, 0);
			this.btnToolbarTime.Name = "btnToolbarTime";
			this.btnToolbarTime.ShowOutline = false;
			this.btnToolbarTime.Size = new System.Drawing.Size(24, 24);
			this.btnToolbarTime.TabIndex = 1;
			this.btnToolbarTime.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.btnToolbarTime.Click += new System.EventHandler(this.OnToolbarButtonClick);
			// 
			// btnToolbarDate
			// 
			this.btnToolbarDate.AcceptsFocus = false;
			appearance6.Image = ((object)(resources.GetObject("appearance6.Image")));
			this.btnToolbarDate.Appearance = appearance6;
			this.btnToolbarDate.AutoSize = true;
			this.btnToolbarDate.ButtonStyle = Infragistics.Win.UIElementButtonStyle.OfficeXPToolbarButton;
			this.btnToolbarDate.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnToolbarDate.Location = new System.Drawing.Point(40, 0);
			this.btnToolbarDate.Name = "btnToolbarDate";
			this.btnToolbarDate.ShowOutline = false;
			this.btnToolbarDate.Size = new System.Drawing.Size(24, 24);
			this.btnToolbarDate.TabIndex = 0;
			this.btnToolbarDate.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.btnToolbarDate.Click += new System.EventHandler(this.OnToolbarButtonClick);
			// 
			// chkReverseOnEvenPages
			// 
			this.chkReverseOnEvenPages.BackColor = System.Drawing.Color.Transparent;
			this.chkReverseOnEvenPages.BackColorInternal = System.Drawing.Color.Transparent;
			this.chkReverseOnEvenPages.Location = new System.Drawing.Point(12, 256);
			this.chkReverseOnEvenPages.Name = "chkReverseOnEvenPages";
			this.chkReverseOnEvenPages.Size = new System.Drawing.Size(144, 20);
			this.chkReverseOnEvenPages.TabIndex = 4;
			this.chkReverseOnEvenPages.Text = "Reverse on even pages";
			this.chkReverseOnEvenPages.CheckedChanged += new System.EventHandler(this.chkReverseOnEvenPages_CheckedChanged);
			// 
			// lblHeader
			// 
			this.lblHeader.AutoSize = true;
			this.lblHeader.BackColorInternal = System.Drawing.Color.Transparent;
			this.lblHeader.Location = new System.Drawing.Point(12, 20);
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Size = new System.Drawing.Size(44, 15);
			this.lblHeader.TabIndex = 3;
			this.lblHeader.Text = "Header:";
			// 
			// txtHeaderLeft
			// 
			appearance7.TextHAlignAsString = "Left";
			this.txtHeaderLeft.Appearance = appearance7;
			this.txtHeaderLeft.AutoSize = false;
			this.txtHeaderLeft.Location = new System.Drawing.Point(12, 72);
			this.txtHeaderLeft.Multiline = true;
			this.txtHeaderLeft.Name = "txtHeaderLeft";
			this.txtHeaderLeft.Size = new System.Drawing.Size(156, 52);
			this.txtHeaderLeft.TabIndex = 2;
			this.txtHeaderLeft.Enter += new System.EventHandler(this.OnHeaderFooterTextBoxEnter);
			this.txtHeaderLeft.ValueChanged += new System.EventHandler(this.txtHeaderLeft_ValueChanged);
			this.txtHeaderLeft.Leave += new System.EventHandler(this.OnHeaderFooterTextBoxLeave);
			// 
			// btnHeaderFont
			// 
			this.btnHeaderFont.BackColorInternal = System.Drawing.Color.Transparent;
			this.btnHeaderFont.Location = new System.Drawing.Point(172, 40);
			this.btnHeaderFont.Name = "btnHeaderFont";
			this.btnHeaderFont.Size = new System.Drawing.Size(56, 21);
			this.btnHeaderFont.TabIndex = 1;
			this.btnHeaderFont.Text = "Font...";
			// 
			// txtHeaderFont
			// 
			this.txtHeaderFont.Location = new System.Drawing.Point(12, 40);
			this.txtHeaderFont.Name = "txtHeaderFont";
			this.txtHeaderFont.ReadOnly = true;
			this.txtHeaderFont.Size = new System.Drawing.Size(156, 22);
			this.txtHeaderFont.TabIndex = 0;
			// 
			// txtHeaderMiddle
			// 
			appearance8.TextHAlignAsString = "Center";
			this.txtHeaderMiddle.Appearance = appearance8;
			this.txtHeaderMiddle.AutoSize = false;
			this.txtHeaderMiddle.Location = new System.Drawing.Point(172, 72);
			this.txtHeaderMiddle.Multiline = true;
			this.txtHeaderMiddle.Name = "txtHeaderMiddle";
			this.txtHeaderMiddle.Size = new System.Drawing.Size(156, 52);
			this.txtHeaderMiddle.TabIndex = 2;
			this.txtHeaderMiddle.Enter += new System.EventHandler(this.OnHeaderFooterTextBoxEnter);
			this.txtHeaderMiddle.ValueChanged += new System.EventHandler(this.txtHeaderMiddle_ValueChanged);
			this.txtHeaderMiddle.Leave += new System.EventHandler(this.OnHeaderFooterTextBoxLeave);
			// 
			// txtHeaderRight
			// 
			appearance9.TextHAlignAsString = "Right";
			this.txtHeaderRight.Appearance = appearance9;
			this.txtHeaderRight.AutoSize = false;
			this.txtHeaderRight.Location = new System.Drawing.Point(332, 72);
			this.txtHeaderRight.Multiline = true;
			this.txtHeaderRight.Name = "txtHeaderRight";
			this.txtHeaderRight.Size = new System.Drawing.Size(156, 52);
			this.txtHeaderRight.TabIndex = 2;
			this.txtHeaderRight.Enter += new System.EventHandler(this.OnHeaderFooterTextBoxEnter);
			this.txtHeaderRight.ValueChanged += new System.EventHandler(this.txtHeaderRight_ValueChanged);
			this.txtHeaderRight.Leave += new System.EventHandler(this.OnHeaderFooterTextBoxLeave);
			// 
			// txtFooterRight
			// 
			appearance10.TextHAlignAsString = "Right";
			this.txtFooterRight.Appearance = appearance10;
			this.txtFooterRight.AutoSize = false;
			this.txtFooterRight.Location = new System.Drawing.Point(332, 192);
			this.txtFooterRight.Multiline = true;
			this.txtFooterRight.Name = "txtFooterRight";
			this.txtFooterRight.Size = new System.Drawing.Size(156, 52);
			this.txtFooterRight.TabIndex = 2;
			this.txtFooterRight.Enter += new System.EventHandler(this.OnHeaderFooterTextBoxEnter);
			this.txtFooterRight.ValueChanged += new System.EventHandler(this.txtFooterRight_ValueChanged);
			this.txtFooterRight.Leave += new System.EventHandler(this.OnHeaderFooterTextBoxLeave);
			// 
			// lblFooter
			// 
			this.lblFooter.AutoSize = true;
			this.lblFooter.BackColorInternal = System.Drawing.Color.Transparent;
			this.lblFooter.Location = new System.Drawing.Point(12, 140);
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Size = new System.Drawing.Size(40, 15);
			this.lblFooter.TabIndex = 3;
			this.lblFooter.Text = "Footer:";
			// 
			// txtFooterFont
			// 
			this.txtFooterFont.Location = new System.Drawing.Point(12, 160);
			this.txtFooterFont.Name = "txtFooterFont";
			this.txtFooterFont.ReadOnly = true;
			this.txtFooterFont.Size = new System.Drawing.Size(156, 22);
			this.txtFooterFont.TabIndex = 0;
			// 
			// txtFooterMiddle
			// 
			appearance11.TextHAlignAsString = "Center";
			this.txtFooterMiddle.Appearance = appearance11;
			this.txtFooterMiddle.AutoSize = false;
			this.txtFooterMiddle.Location = new System.Drawing.Point(172, 192);
			this.txtFooterMiddle.Multiline = true;
			this.txtFooterMiddle.Name = "txtFooterMiddle";
			this.txtFooterMiddle.Size = new System.Drawing.Size(156, 52);
			this.txtFooterMiddle.TabIndex = 2;
			this.txtFooterMiddle.Enter += new System.EventHandler(this.OnHeaderFooterTextBoxEnter);
			this.txtFooterMiddle.ValueChanged += new System.EventHandler(this.txtFooterMiddle_ValueChanged);
			this.txtFooterMiddle.Leave += new System.EventHandler(this.OnHeaderFooterTextBoxLeave);
			// 
			// txtFooterLeft
			// 
			appearance12.TextHAlignAsString = "Left";
			this.txtFooterLeft.Appearance = appearance12;
			this.txtFooterLeft.AutoSize = false;
			this.txtFooterLeft.Location = new System.Drawing.Point(12, 192);
			this.txtFooterLeft.Multiline = true;
			this.txtFooterLeft.Name = "txtFooterLeft";
			this.txtFooterLeft.Size = new System.Drawing.Size(156, 52);
			this.txtFooterLeft.TabIndex = 2;
			this.txtFooterLeft.Enter += new System.EventHandler(this.OnHeaderFooterTextBoxEnter);
			this.txtFooterLeft.ValueChanged += new System.EventHandler(this.txtFooterLeft_ValueChanged);
			this.txtFooterLeft.Leave += new System.EventHandler(this.OnHeaderFooterTextBoxLeave);
			// 
			// btnFooterFont
			// 
			this.btnFooterFont.BackColorInternal = System.Drawing.Color.Transparent;
			this.btnFooterFont.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnFooterFont.Location = new System.Drawing.Point(172, 160);
			this.btnFooterFont.Name = "btnFooterFont";
			this.btnFooterFont.Size = new System.Drawing.Size(56, 21);
			this.btnFooterFont.TabIndex = 1;
			this.btnFooterFont.Text = "Font...";
			// 
			// imgListPreview
			// 
			this.imgListPreview.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListPreview.ImageStream")));
			this.imgListPreview.TransparentColor = System.Drawing.Color.Magenta;
			this.imgListPreview.Images.SetKeyName(0, "");
			this.imgListPreview.Images.SetKeyName(1, "");
			this.imgListPreview.Images.SetKeyName(2, "");
			this.imgListPreview.Images.SetKeyName(3, "");
			this.imgListPreview.Images.SetKeyName(4, "");
			this.imgListPreview.Images.SetKeyName(5, "");
			this.imgListPreview.Images.SetKeyName(6, "");
			this.imgListPreview.Images.SetKeyName(7, "");
			this.imgListPreview.Images.SetKeyName(8, "");
			this.imgListPreview.Images.SetKeyName(9, "");
			// 
			// pnlTop
			// 
			this.pnlTop.Controls.Add(this.cboStyleName);
			this.pnlTop.Controls.Add(this.lblStyleName);
			this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnlTop.Location = new System.Drawing.Point(0, 0);
			this.pnlTop.Name = "pnlTop";
			this.pnlTop.Size = new System.Drawing.Size(510, 40);
			this.pnlTop.TabIndex = 0;
			// 
			// cboStyleName
			// 
			this.cboStyleName.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
			this.cboStyleName.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
			this.cboStyleName.Location = new System.Drawing.Point(92, 8);
			this.cboStyleName.Name = "cboStyleName";
			this.cboStyleName.ReadOnly = true;
			this.cboStyleName.Size = new System.Drawing.Size(160, 22);
			this.cboStyleName.TabIndex = 1;
			// 
			// lblStyleName
			// 
			this.lblStyleName.AutoSize = true;
			this.lblStyleName.Location = new System.Drawing.Point(16, 12);
			this.lblStyleName.Name = "lblStyleName";
			this.lblStyleName.Size = new System.Drawing.Size(65, 15);
			this.lblStyleName.TabIndex = 0;
			this.lblStyleName.Text = "Style Name:";
			// 
			// pnlBottom
			// 
			this.pnlBottom.Controls.Add(this.btnPrint);
			this.pnlBottom.Controls.Add(this.btnPrintPreview);
			this.pnlBottom.Controls.Add(this.btnCancel);
			this.pnlBottom.Controls.Add(this.btnOk);
			this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlBottom.Location = new System.Drawing.Point(0, 365);
			this.pnlBottom.Name = "pnlBottom";
			this.pnlBottom.Size = new System.Drawing.Size(510, 32);
			this.pnlBottom.TabIndex = 1;
			// 
			// btnPrint
			// 
			this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnPrint.BackColorInternal = System.Drawing.Color.Transparent;
			this.btnPrint.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnPrint.Location = new System.Drawing.Point(412, 4);
			this.btnPrint.Name = "btnPrint";
			this.btnPrint.Size = new System.Drawing.Size(92, 24);
			this.btnPrint.TabIndex = 2;
			this.btnPrint.Text = "&Print...";
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnPrintPreview.BackColorInternal = System.Drawing.Color.Transparent;
			this.btnPrintPreview.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnPrintPreview.Location = new System.Drawing.Point(316, 4);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.Size = new System.Drawing.Size(92, 24);
			this.btnPrintPreview.TabIndex = 2;
			this.btnPrintPreview.Text = "Print Pre&view...";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColorInternal = System.Drawing.Color.Transparent;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(216, 4);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(92, 24);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "Cancel";
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOk.BackColorInternal = System.Drawing.Color.Transparent;
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Location = new System.Drawing.Point(120, 4);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(92, 24);
			this.btnOk.TabIndex = 2;
			this.btnOk.Text = "OK";
			// 
			// tabControlMain
			// 
			this.tabControlMain.Controls.Add(this.ultraTabSharedControlsPage1);
			this.tabControlMain.Controls.Add(this.tabPagePaper);
			this.tabControlMain.Controls.Add(this.tabPageHeaderFooter);
			this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControlMain.Location = new System.Drawing.Point(0, 40);
			this.tabControlMain.Name = "tabControlMain";
			this.tabControlMain.SharedControlsPage = this.ultraTabSharedControlsPage1;
			this.tabControlMain.Size = new System.Drawing.Size(510, 325);
			this.tabControlMain.TabIndex = 2;
			this.tabControlMain.TabPadding = new System.Drawing.Size(10, 1);
			ultraTab9.Key = "Paper";
			ultraTab9.TabPage = this.tabPagePaper;
			ultraTab9.Text = "Paper";
			ultraTab10.Key = "HeaderFooter";
			ultraTab10.TabPage = this.tabPageHeaderFooter;
			ultraTab10.Text = "Header/Footer";
			this.tabControlMain.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab9,
            ultraTab10});
			// 
			// ultraTabSharedControlsPage1
			// 
			this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
			this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
			this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(506, 299);
			// 
			// fontDialog1
			// 
			this.fontDialog1.Color = System.Drawing.SystemColors.WindowText;
			this.fontDialog1.FontMustExist = true;
			this.fontDialog1.ShowColor = true;
			// 
			// printerSettingsControl1
			// 
			this.printerSettingsControl1.BackColor = System.Drawing.Color.Transparent;
			this.printerSettingsControl1.Location = new System.Drawing.Point(0, 0);
			this.printerSettingsControl1.Name = "printerSettingsControl1";
			this.printerSettingsControl1.Size = new System.Drawing.Size(503, 320);
			this.printerSettingsControl1.TabIndex = 0;
			// 
			// UltraPageSetup
			// 
			this.AcceptButton = this.btnOk;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(510, 397);
			this.Controls.Add(this.tabControlMain);
			this.Controls.Add(this.pnlBottom);
			this.Controls.Add(this.pnlTop);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "UltraPageSetup";
			this.Text = "UltraPageSetup";
			this.Load += new System.EventHandler(this.UltraPageSetup_Load);
			this.tabPagePaper.ResumeLayout(false);
			this.tabPageHeaderFooter.ResumeLayout(false);
			this.tabPageHeaderFooter.PerformLayout();
			this.pnlToolbar.ResumeLayout(false);
			this.pnlToolbar.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtHeaderLeft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeaderFont)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeaderMiddle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHeaderRight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFooterRight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFooterFont)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFooterMiddle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFooterLeft)).EndInit();
			this.pnlTop.ResumeLayout(false);
			this.pnlTop.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cboStyleName)).EndInit();
			this.pnlBottom.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabControlMain)).EndInit();
			this.tabControlMain.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private MemoryStream		initialSettingsStream;
		private PageSettings		defaultPageSettings;
		private bool isInitializing = false;

		#region Dialog Load
		private void UltraPageSetup_Load(object sender, System.EventArgs e)
		{
			// the toolbar buttons are disabled until one of the
			// related textboxes get focus
			this.pnlToolbar.Enabled = false;

			// save the state of the print document before doing anything in
			// case the dialog is cancelled.
			this.SaveInitialSettings();
			
			// reset the default page settings
			this.printDocument.DefaultPageSettings = new PageSettings(this.printDocument.PrinterSettings);
			//this.printDocument.DefaultPageSettings.Landscape = true;

			// ensure all the controls are populated...
			this.ResetControls();

			// make sure all the controls have a fore color set
			Utilities.InitializeControlForeColor(this.Controls);
		}
		#endregion //Dialog Load

		#region Save/Restore/Dispose InitialSettings
		private void SaveInitialSettings()
		{
			// create a memory stream to store the initial settings of 
			// the print document
			this.initialSettingsStream = new MemoryStream();

			// clone the default page settings as well
			this.defaultPageSettings = this.printDocument.DefaultPageSettings.Clone() as PageSettings;

			this.printDocument.SaveAsBinary(this.initialSettingsStream);
		}

		private void RestoreInitialSettings()
		{
			System.Diagnostics.Debug.Assert(this.initialSettingsStream != null, "Invalid original settings stream");

			// create a memory stream to store the initial settings of 
			// the print document
			this.initialSettingsStream.Position = 0;

			this.printDocument.LoadFromBinary(this.initialSettingsStream);

			// clone the default page settings as well
			this.printDocument.DefaultPageSettings = this.defaultPageSettings;
		}

		private void DisposeInitialSettings()
		{
			System.Diagnostics.Debug.Assert(this.initialSettingsStream != null, "Invalid original settings stream");

			// create a memory stream to store the initial settings of 
			// the print document
			this.initialSettingsStream.Close();
			this.initialSettingsStream = null;

			this.defaultPageSettings = null;
		}
		#endregion //Save/Restore/Dispose InitialSettings

	 
		#region ResetControls
		private void ResetControls()
		{
			this.isInitializing = true;

			// edit the default page settings
			this.printerSettingsControl1.PageSettings = this.printDocument.DefaultPageSettings;

			// show the style name in the dialog text
			this.Text = string.Format("UltraPageSetup: {0}", this.cboStyleName.Text);

			this.isInitializing = false;
		}
		#endregion //ResetControls
 

		#region Header/Footer TextBox Enter/Leave
		private void OnHeaderFooterTextBoxEnter(object sender, System.EventArgs e)
		{
			this.pnlToolbar.Enabled = true;
		}

		private void OnHeaderFooterTextBoxLeave(object sender, System.EventArgs e)
		{
			this.pnlToolbar.Enabled = false;
		}
		#endregion //Header/Footer TextBox Enter/Leave

		#region Header/Footer ToolbarButtonClick
		private void OnToolbarButtonClick(object sender, System.EventArgs e)
		{
			string token = null;

			if (sender == this.btnToolbarDate)
				token = "[Date Printed]";
			else if (sender == this.btnToolbarTime)
				token = "[Time Printed]";
			else if (sender == this.btnToolbarUserName)
				token = "[User Name]";
			else if (sender == this.btnToolbarPageNumber)
				token = "[Page #]";
			else
				return;

			Control activeControl = this.ActiveControl;

			if (activeControl is TextBox)
				((TextBox)activeControl).SelectedText = token;
			else if (activeControl is UltraTextEditor)
				((UltraTextEditor)activeControl).SelectedText = token;
		}
		#endregion //Header/Footer ToolbarButtonClick

	 
		#region GetTabKey
	 
		#endregion //GetTabKey

	

		#region Include Notes CheckChanged
		private bool updatingCheckState = false;


		#endregion //Include Notes CheckChanged

		#region Header/Footer TextChanged
		private void txtHeaderLeft_ValueChanged(object sender, System.EventArgs e)
		{
			this.printDocument.Header.TextLeft = ((UltraTextEditor)sender).Text;
		}

		private void txtHeaderMiddle_ValueChanged(object sender, System.EventArgs e)
		{
			this.printDocument.Header.TextCenter = ((UltraTextEditor)sender).Text;
		}

		private void txtHeaderRight_ValueChanged(object sender, System.EventArgs e)
		{
			this.printDocument.Header.TextRight = ((UltraTextEditor)sender).Text;
		}

		private void txtFooterLeft_ValueChanged(object sender, System.EventArgs e)
		{
			this.printDocument.Footer.TextLeft = ((UltraTextEditor)sender).Text;
		}

		private void txtFooterMiddle_ValueChanged(object sender, System.EventArgs e)
		{
			this.printDocument.Footer.TextCenter = ((UltraTextEditor)sender).Text;
		}

		private void txtFooterRight_ValueChanged(object sender, System.EventArgs e)
		{
			this.printDocument.Footer.TextRight = ((UltraTextEditor)sender).Text;
		}
		#endregion //Header/Footer TextChanged

		#region ReverseOnEvenPages CheckChanged
		private void chkReverseOnEvenPages_CheckedChanged(object sender, System.EventArgs e)
		{
			this.printDocument.Header.ReverseTextOnEvenPages = 
				this.printDocument.Footer.ReverseTextOnEvenPages = ((UltraCheckEditor)sender).Checked;
		}
		#endregion //ReverseOnEvenPages CheckChanged

		#region Dialog Closing
		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing(e);

			if (e.Cancel)
				return;

			// if the dialog was closed via one of the "success"
			// buttons (ok, print, print preview), then save
			// this as the initial state for the print style
			if (this.DialogResult == DialogResult.OK)
			{
				//this.SavePrintStyleSettings();
			}
			else
			{
				// otherwise, put the document
				// back to its original state
				this.RestoreInitialSettings();
			}

			// in either case, clean up the initial
			// settings memory stream
			this.DisposeInitialSettings();
		}
		#endregion //Dialog Closing

		#region PrintPreview/Print Buttons and Events
		/// <summary>
		/// Invoked when the print preview button has been clicked.
		/// </summary>
		public event EventHandler PrintPreviewRequested;

		/// <summary>
		/// Invoked when the print button has been clicked.
		/// </summary>
		public event EventHandler PrintDialogRequested;

		/// <summary>
		/// Used to invoke the <see cref="PrintPreviewRequested"/>
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnPrintPreviewRequested(EventArgs e)
		{
			if (this.PrintPreviewRequested != null)
				this.PrintPreviewRequested(this, e);
		}

		/// <summary>
		/// Used to invoke the <see cref="PrintDialogRequested"/>
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnPrintDialogRequested(EventArgs e)
		{
			if (this.PrintDialogRequested != null)
				this.PrintDialogRequested(this, e);
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			this.OnPrintPreviewRequested(EventArgs.Empty);
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			this.OnPrintDialogRequested(EventArgs.Empty);
		}
		#endregion //PrintPreview/Print Buttons and Events


	}
}
