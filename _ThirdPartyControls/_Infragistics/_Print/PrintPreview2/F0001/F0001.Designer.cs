﻿using Infragistics.Win.UltraWinToolbars;
using System.Windows.Forms;
using System.Resources;
using Infragistics.Win;
using System.ComponentModel;
using Infragistics.Win.Printing;
using System.Drawing;
using System;
using Infragistics.Win.UltraWinStatusBar;

namespace iDASiT.Win.Mes
{
	partial class F0001
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		

	//	private Infragistics.Win.Misc.UltraButton ultraButton1;
		private Infragistics.Win.Printing.UltraPrintPreviewControl ultraPrintPreviewControl1;
	//	private Infragistics.Win.UltraWinGrid.UltraGridPrintDocument ultraGridPrintDocument1;
	//	private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid1;
	//	private Infragistics.Win.Misc.UltraButton ultraButton2;
	//	private Infragistics.Win.Misc.UltraButton ultraButton3;
	//	private System.Windows.Forms.Panel pnlContainerBody_Fill_Panel;
	//	private System.Windows.Forms.Panel pnlContainerBody_Fill_Panel_Fill_Panel;
		private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager ultraToolbarsManager2;
//		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
	//	private WinSchedulePrintDemo.PrinterSettingsControl printerSettingsControl1;
//		private System.Windows.Forms.Panel panel1;
//		private System.Windows.Forms.ImageList imageList1;
//		private System.Windows.Forms.ImageList imageList2;
		private Infragistics.Win.UltraWinStatusBar.UltraStatusBar ultraStatusBar1;
		private Infragistics.Win.Printing.UltraPrintPreviewThumbnail ultraPrintPreviewThumbnail1 = null;
//		private System.Windows.Forms.Panel panel3;
		private Panel pnlContainerBody_Fill_Panel_1;
		private UltraToolbarsDockArea _pnlContainerBody_Toolbars_Dock_Area_Left;
		private UltraToolbarsDockArea _pnlContainerBody_Toolbars_Dock_Area_Right;
		private UltraToolbarsDockArea _pnlContainerBody_Toolbars_Dock_Area_Top;
		private UltraToolbarsDockArea _pnlContainerBody_Toolbars_Dock_Area_Bottom;
		private SplitContainer splitContainer1;
		private Infragistics.Win.UltraWinGrid.UltraGridPrintDocument ultraGridPrintDocument1;
	}
}
  