﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.Mvc.UI.Fluent;
using Telerik.Web.Mvc.UI;

namespace GridExcelExport
{
	public static class TelerikExtension
	{
		/// <summary>
		/// 그리드의 기본 설정을 제어합니다.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="builder">The builder.</param>
		/// <returns></returns>
		public static GridBuilder<T> SetDefaultConfiguration<T>(this GridBuilder<T> builder, bool addExcelLink)
			where T : class
		{
			return builder
				.Resizable(x => x.Columns(true))
				.EnableCustomBinding(true)
				.Pageable(paging => paging
					.PageSize(15)
					.Enabled(true)
					.Style(GridPagerStyles.Status | GridPagerStyles.Numeric | GridPagerStyles.NextPreviousAndDropDown | GridPagerStyles.PageSizeDropDown)
				)
				.Reorderable(reorder => reorder.Columns(true))
				.Sortable(sorting => sorting
					.SortMode(GridSortMode.MultipleColumn)
				)
				.ColumnContextMenu()
				.KeyboardNavigation()
				.Localizable("ko-KR")
				.ClientEvents(events => events
					.OnDataBound(GetOnDataBound4ExcelExportScript())
				)
				//.AddDefaultErrorHandler();
				;
		}

		private static string GetOnDataBound4ExcelExportScript()
		{
			return String.Empty;
		}

	}
}