﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace GridExcelExport.Models
{
	public class NorthwindContext : DbContext
	{
		/// <summary>
		/// Order를 구하거나 설정합니다.
		/// </summary>
		public DbSet<Order> Orders
		{
			get;
			set;
		}
	}
}