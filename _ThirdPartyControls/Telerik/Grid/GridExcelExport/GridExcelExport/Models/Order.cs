using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace GridExcelExport.Models
{
	public class Order
	{
		#region Properties
		/// <summary>
		/// OrderID를 구하거나 설정합니다.
		/// </summary>
		public int OrderID
		{
			get;
			set;
		}

		/// <summary>
		/// CustomerID를 구하거나 설정합니다.
		/// </summary>
		public string CustomerID
		{
			get;
			set;
		}

		/// <summary>
		/// EmployeeID를 구하거나 설정합니다.
		/// </summary>
		public int? EmployeeID
		{
			get;
			set;
		}

		/// <summary>
		/// OrderDate를 구하거나 설정합니다.
		/// </summary>
		public DateTime? OrderDate
		{
			get;
			set;
		}

		/// <summary>
		/// RequiredDate를 구하거나 설정합니다.
		/// </summary>
		public DateTime? RequiredDate
		{
			get;
			set;
		}

		/// <summary>
		/// ShippedDate를 구하거나 설정합니다.
		/// </summary>
		public DateTime? ShippedDate
		{
			get;
			set;
		}

		/// <summary>
		/// ShipVia를 구하거나 설정합니다.
		/// </summary>
		public int? ShipVia
		{
			get;
			set;
		}

		/// <summary>
		/// Freight를 구하거나 설정합니다.
		/// </summary>
		public decimal? Freight
		{
			get;
			set;
		}

		/// <summary>
		/// ShipName를 구하거나 설정합니다.
		/// </summary>
		public string ShipName
		{
			get;
			set;
		}

		/// <summary>
		/// ShipAddress를 구하거나 설정합니다.
		/// </summary>
		public string ShipAddress
		{
			get;
			set;
		}

		/// <summary>
		/// ShipCity를 구하거나 설정합니다.
		/// </summary>
		public string ShipCity
		{
			get;
			set;
		}

		/// <summary>
		/// ShipRegion를 구하거나 설정합니다.
		/// </summary>
		public string ShipRegion
		{
			get;
			set;
		}

		/// <summary>
		/// ShipPostalCode를 구하거나 설정합니다.
		/// </summary>
		public string ShipPostalCode
		{
			get;
			set;
		}

		/// <summary>
		/// ShipCountry를 구하거나 설정합니다.
		/// </summary>
		public string ShipCountry
		{
			get;
			set;
		}

		#endregion

	}
}

