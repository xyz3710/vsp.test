﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using Grid02_UploadFileInCell.Models;
using Telerik.Web.Mvc;
using System.IO;

namespace Grid02_UploadFileInCell.Controllers
{
	public class HomeController : Controller
	{
		private FakeRepository _fakeRepository = new FakeRepository();

		public ActionResult Index()
		{
			ViewBag.Message = "Welcome to ASP.NET MVC!";

			return View(_fakeRepository.GetEntries());
		}

		public ActionResult UpdateFile(UploadEntry fileEntry)
		{
			Debug.Assert(fileEntry.Data != null, "Uploaded file should not be empty!");

			return RedirectToAction("Index");
		}

		public ActionResult About()
		{
			return View();
		}

		private bool UploadFile(UploadEntry model)
		{
			var file = Request.Files["attachments"];

			if (file != null && file.ContentLength != 0)
			{
				byte[] data = new byte[file.ContentLength];
				file.InputStream.Read(data, 0, file.ContentLength);

				string directoryPath = FakeRepository.BASE_FOLDER;

				if (Directory.Exists(directoryPath) == false)
					Directory.CreateDirectory(directoryPath);

				string fileName = Path.GetFileName(file.FileName);
				string filePath = Path.Combine(directoryPath, fileName);

				using (FileStream fileSteam = new FileStream(filePath, FileMode.Create, FileAccess.Write))
				{
					fileSteam.Write(data, 0, data.Length);
				}

				model.FileName = fileName;
				model.Data = file;
			}

			return model.Data != null;
		}

		private void DeleteFile(UploadEntry model)
		{
			if (model != null && string.IsNullOrEmpty(model.FileName) == false)
			{
				string directoryPath = FakeRepository.BASE_FOLDER;

				if (Directory.Exists(directoryPath) == false)
					Directory.CreateDirectory(directoryPath);

				string filePath = Path.Combine(directoryPath, model.FileName);

				System.IO.File.Delete(filePath);
			}
		}

		[GridAction]
		public ActionResult _Index()
		{
			return View(new GridModel
			{
				Data = _fakeRepository.GetEntries(),
			});
		}

		[GridAction]
		public ActionResult _InsertFile(Guid id)
		{
			return View(new GridModel
			{
				Data = _fakeRepository.GetEntries(),
			});
		}

		[GridAction]
		public ActionResult _UpdateFile(int id)
		{
			var model = _fakeRepository.GetEntry(id);
			DeleteFile(model);
			UploadFile(model);

			return View(new GridModel
			{
				Data = _fakeRepository.GetEntries(),
			});
		}

		[GridAction]
		public ActionResult _DeleteFile(int id)
		{
			var model = _fakeRepository.GetEntry(id);
			DeleteFile(model);

			return View(new GridModel
			{
				Data = _fakeRepository.GetEntries(),
			});
		}

		[HttpPost]
		public ActionResult _SaveFile(IEnumerable<HttpPostedFileBase> attachments, int fileId1, string fileId2)
		{
			// Request.Files 로 구할 수 있다.
			UploadEntry model = new UploadEntry
			{
				Data = attachments.ToList()[0],
			};
			UploadFile(model);

			return Content(string.Empty);
		}

		[HttpPost]
		public ActionResult _RemoveFile(string[] fileNames, string fileId1, string fileId2)
		{
			UploadEntry model = new UploadEntry
			{
				FileName = fileNames[0],
			};
			DeleteFile(model);

			return Content(string.Empty);
		}
	}
}
