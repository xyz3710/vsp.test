﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Grid02_UploadFileInCell.Models
{
	public class FakeRepository
	{
		#region Constants
		public const string BASE_FOLDER = @"B:\Study\Telerik\Grid\Grid02-UploadFileInCell\Files";
		#endregion

		public IEnumerable<UploadEntry> GetEntries()
		{
			List<UploadEntry> models = new List<UploadEntry>();
			string[] files = Directory.GetFiles(BASE_FOLDER);

			for (int i = 0; i < files.Length; i++)
				models.Add(new UploadEntry
				{
					Id = i + 1,
					Id2 = DateTime.Now.ToLongTimeString(),
					FileName = Path.GetFileName(files[i]),
				});

			return models;
		}

		public UploadEntry GetEntry(int id)
		{
			return GetEntries().SingleOrDefault(x => x.Id == id);
		}
	}
}