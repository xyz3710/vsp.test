﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace Grid02_UploadFileInCell.Models
{
	[DebuggerDisplay("Id:{Id}, Id2:{Id2}, FileName:{FileName}, Data:{Data}", Name = "UploadEntry")]
	public class UploadEntry
	{
		public int Id
		{
			get;
			set;
		}
		
		public string Id2
		{
			get;
			set;
		}

		public string FileName
		{
			get;
			set;
		}

		[Required]
		public HttpPostedFileBase Data
		{
			get;
			set;
		}
	}
}