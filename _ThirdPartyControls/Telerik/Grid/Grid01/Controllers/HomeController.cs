﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Grid01.Models;
using System.Collections;
using Telerik.Web.Mvc;

namespace Grid01.Controllers
{
	public class HomeController : Controller
	{
		private static List<Employee> _emplyees;
		private static List<Order> _orders;

		public HomeController()
		{
			if (_emplyees == null)
				_emplyees = HtmlHelperExtensions.GetEmplyoees(null);

			if (_orders == null)
				_orders = new List<Order>
				{
					new Order
					{
						Id = 1,
						EmployeeId = 2,
						OrderDate = DateTime.Now.AddHours(-2),
						Description = "설명.........",
					},
					new Order
					{
						Id = 2,
						EmployeeId = 1,
						OrderDate = DateTime.MaxValue,
						Description = "주저리 주저리",
					},
					new Order
					{
						Id = 3,
						EmployeeId = 3,
						OrderDate = DateTime.Now,
						Description = string.Empty,
					},
					new Order
					{
						Id = 4,
						//EmployeeId = 2,
						OrderDate = DateTime.Now.AddDays(-1),
					},
					new Order
					{
						Id = 5,
						EmployeeId = 4,
						OrderDate = DateTime.Now,
					},
				};
		}

		public ActionResult Index()
		{
			ViewBag.Employees = _emplyees as IEnumerable;
			ViewData["employeesCount"] = _emplyees.Count;

			return View(_orders);
		}

		[GridAction]
		public ActionResult _Select()
		{
			return View(new GridModel(_orders));
		}

		[GridAction]
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult _Update(Order model)
		{
			if (ModelState.IsValid == true)
			{
				var thatOrder = _orders.Find(x => x.Id == model.Id);

				TryUpdateModel<Order>(thatOrder);
			}

			return View(new GridModel(_orders));
		}

		public ActionResult About()
		{
			return View();
		}

		public ActionResult ServerSideSelect()
		{
			ViewBag.Employees = _emplyees as IEnumerable;

			return View(_orders);
		}

		[HttpPost]
		public ActionResult ServerSideUpdate(Order model, int id)
		{
			if (ModelState.IsValid == true)
			{
				var thatOrder = _orders.Find(x => x.Id == id);

				thatOrder.EmployeeId = Convert.ToInt32(Request.Form["Emplyoee"]);
				thatOrder.OrderDate = Convert.ToDateTime(Request.Form["OrderDate"]);
				thatOrder.Description = HttpUtility.HtmlDecode(Request.Form["Description"]);
			}

			return RedirectToAction("ServerSideSelect");
		}

		[HttpPost]
		public ActionResult ServerSideInsert(Order model, int id)
		{
			if (ModelState.IsValid == true)
			{
				Order newOrder = new Order
				{
					Id = _orders.Count,
					EmployeeId = Convert.ToInt32(Request.Form["Emplyoee"]),
					OrderDate = Convert.ToDateTime(Request.Form["OrderDate"]),
					Description = HttpUtility.HtmlDecode(Request.Form["Description"]),
				};

				_orders.Add(newOrder);
			}

			return RedirectToAction("ServerSideSelect");
		}
	}
}
