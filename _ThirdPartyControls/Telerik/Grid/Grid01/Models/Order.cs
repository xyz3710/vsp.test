﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.ComponentModel;

namespace Grid01.Models
{
	[DebuggerDisplay("Id:{Id}, EmployeeId:{EmployeeId}, OrderDate:{OrderDate}, Description:{Description}", Name = "Order")]
	public class Order
	{
		/// <summary>
		/// Id를 구하거나 설정합니다.
		/// </summary>
		[ReadOnly(true)]
		public int Id
		{
			get;
			set;
		}

		/// <summary>
		/// EmployeeId를 구하거나 설정합니다.
		/// </summary>
		public int EmployeeId
		{
			get;
			set;
		}

		/// <summary>
		/// OrderDate를 구하거나 설정합니다.
		/// </summary>
		public DateTime OrderDate
		{
			get;
			set;
		}

		/// <summary>
		/// Description를 구하거나 설정합니다.
		/// </summary>
		//[DataType(DataType.MultilineText)]
		public string Description
		{
			get;
			set;
		}


	}
}