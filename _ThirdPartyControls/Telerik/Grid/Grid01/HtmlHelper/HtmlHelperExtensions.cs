﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Grid01.Models;
using System.Web.Mvc;
using System.Linq.Expressions;
using System.Web.Mvc.Html;

namespace Grid01
{
	public static class HtmlHelperExtensions
	{
		public static List<Employee> GetEmplyoees(this HtmlHelper helper)
		{
			return new List<Employee>
				{
					new Employee
					{
						Id = 1,
						Name = "Emp1",
					},
					new Employee
					{
						Id = 2,
						Name = "Emp2",
					},
					new Employee
					{
						Id = 3,
						Name = "Emp3",
					},
					new Employee
					{
						Id = 4,
						Name = "Emp4",
					},
				};
		}

		public static IList<SelectListItem> GetSelectListItems<TModel, TType>(
				IEnumerable<TModel> modelSource,
				Expression<Func<TModel, string>> textExpression,
				Expression<Func<TModel, TType>> valueExpression,
				Expression<Func<TModel, bool>> selectExpression)
		{
			//if (selectExpression == null)
			//	selectExpression = valueExpression;

			var itemList = new List<SelectListItem>();
			var compiledText = textExpression.Compile();
			var compiledValue = valueExpression.Compile();
			var compiledSelect = selectExpression.Compile();

			var model = modelSource
				.Select(x => new SelectListItem
				{
					Text = compiledText(x),
					Value = Convert.ToString(compiledValue(x)),
					Selected = compiledSelect(x),
				});

			itemList = model.ToList();

			return itemList;
		}

		public static IList<SelectListItem> EmployeeListItems(this HtmlHelper helper, int employeeId)
		{
			return GetSelectListItems(GetEmplyoees(null),
				x => x.Name,
				x => x.Id,
				x => x.Id == employeeId);
		}
	}
}