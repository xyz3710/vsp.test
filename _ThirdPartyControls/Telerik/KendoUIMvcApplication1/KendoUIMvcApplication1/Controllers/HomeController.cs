﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using KendoUIMvcApplication1.Models;

namespace KendoUIMvcApplication1.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.Message = "Welcome to ASP.NET MVC!";

			return View();
		}

		public ActionResult _ProductsRead([DataSourceRequest] DataSourceRequest request)
		{
			var products = new List<ProductViewModel>();

			for (int i = 0; i < 115; i++)
				products.Add(new ProductViewModel
				{
					ProductID = i + 1,
					ProductName = "제품 " + i.ToString(),
					UnitPrice = i * 10,
					UnitsInStock = i,
				});			

			return Json(products.ToDataSourceResult(request));
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your app description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}
