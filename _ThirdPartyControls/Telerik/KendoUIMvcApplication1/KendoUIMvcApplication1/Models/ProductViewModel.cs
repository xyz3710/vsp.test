﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace KendoUIMvcApplication1.Models
{
	[DebuggerDisplay("ProductID:{ProductID}", Name = "ProductViewModel")]
	public class ProductViewModel
	{
		public int ProductID
		{
			get;
			set;
		}

		public string ProductName
		{
			get;
			set;
		}

		public double UnitPrice
		{
			get;
			set;
		}

		public double UnitsInStock
		{
			get;
			set;
		}

	}
}