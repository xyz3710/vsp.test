﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace _1_ChoSungQueryTest
{
	class Program
	{
		static void Main(string[] args)
		{
			// 참고 : C# 기초로 한글 검색기(초성 포함) 만들기
			// https://support.microsoft.com/ko-kr/kb/2701840
			// -초성: ㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ (19가지)
			// -중성: ㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ(21가지)
			// - 종성 : 없음, ㄱㄲㄳㄴㄵㄶㄷㄹㄺㄻㄼㄽㄾㄿㅀㅁㅂㅄㅅㅆㅇㅈㅊㅋㅌㅍㅎ(28가지)
			int x = 44032;
			var sb = new StringBuilder();
			var value = string.Empty;

			for (int cho = 1; cho <= 19; cho++)
			{
				for (int jung = 1; jung <= 21; jung++)
				{
					for (int jong = 1; jong <= 28; jong++)
					{
						value = string.Format("{0}:{1}, ", x, (char)x);

						Console.Write(value);
						sb.Append(value);
						x++;
					}

					Console.WriteLine();
					sb.AppendLine();
				}

				Console.WriteLine();
				sb.AppendLine();
			}

			//File.WriteAllText("ChoSung.txt", sb.ToString(), Encoding.UTF8);
		}
	}
}
