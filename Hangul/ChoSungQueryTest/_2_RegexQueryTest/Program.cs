﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace _2_RegexQueryTest
{
	class Program
	{
		static void Main(string[] args)
		{
			List<string> list = new List<string>(new string[] { "가시나무", "까시나무", "소나무", "YMCA", "목련", "백일홍", "청단풍", "홍단풍", "가이즈까향나무", "감나무", "개나리", "금송", "깨죽" });
			var input = string.Empty;

			Console.WriteLine("아래 목록 중에서 초성으로 첫 단어를 검색합니다.\r\n");

			foreach (string item in list)
			{
				Console.WriteLine(item);
			}

			Console.WriteLine();

			do
			{
				input = Console.ReadLine();
				var result = QueryByChoSung(list, input, true);

				foreach (var item in result)
				{
					Console.WriteLine(item);
				}
			}
			while (true);
		}

		private static int[] _choSungValue = { 44032, 44620, 45208, 45796, 46384, 46972, 47560, 48148, 48736, 49324, 49912, 50500, 51088, 51676, 52264, 52852, 53440, 54028, 54616, 55204 };
		private static char[] _choSungChar = { 'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ' };
		private static string[] _characters = { "가", "까", "나", "다", "따", "라", "마", "바", "빠", "사", "싸", "아", "자", "짜", "차", "카", "타", "파", "하" };
		private static int[] _doubleIndex = { 0, 3, 7, 9, 12 };

		/// <summary>
		/// list 중에서 입력되는 글자 또는 초성에 의해 검색된 목록을 반환합니다.
		/// </summary>
		/// <param name="list">검색 대상 목록</param>
		/// <param name="input">검색하려는 글자 또는 초성</param>
		/// <param name="includeDouble">검색어 중에서 쌍자음을 포함할지 여부</param>
		/// <param name="partExtract">검색어의 일부 초성이 포함되어 있는지 여부</param>
		/// <returns>list 중에서 검색된 목록, 결과가 없을 경우 빈 목록</returns>
		/// <example>
		/// <code>
		/// List<string> list = new List<string>(new string[] { "가시나무", "까시나무", "소나무", "YMCA", "목련", "백일홍", "청단풍", "홍단풍", "가이즈까향나무", "감나무", "개나리", "금송", "깨죽" });
		///
		/// QueryByChoSung(list, "ㄱ");
		/// // 결과: 가시나무,가이즈까향나무,감나무,개나리,금송
		///
		/// QueryByChoSung(list, "ㄱ", true);
		/// // 결과: 가시나무,까시나무,가이즈까향나무,감나무,개나리,금송,깨죽
		///
		/// QueryByChoSung(list, "ㄲ", true);
		/// // 결과: 까시나무,깨죽
		/// 
		/// QueryByChoSung(list, "가");
		/// // 결과: 가시나무,가이즈까향나무,감나무
		/// 
		/// QueryByChoSung(list, "ㄷ", true, true);
		/// // 결과: 청단풍,홍단풍
		/// </code>
		/// </example>
		public static IEnumerable<string> QueryByChoSung(
			IEnumerable<string> list,
			string input,
			bool includeDouble = false,
			bool partExtract = false)
		{
			var pattern = string.Empty;

			for (int i = 0; i < input.Length; i++)
			{
				if (input[i] >= 'ㄱ' && input[i] <= 'ㅎ')     // 초성만 입력되었을때
				{
					var condition = false;
					var nextValue = 1;

					for (int j = 0; j < _choSungChar.Length; j++)
					{
						condition = input[i] == _choSungChar[j];

						if (includeDouble == true)
						{
							if (condition == true && _doubleIndex.Contains(j) == true)
							{
								nextValue = 2;
							}
						}

						if (condition == true)
						{
							pattern += string.Format("[{0}-{1}]", _characters[j], (char)(_choSungValue[j + nextValue] - 1));
						}
					}
				}
				else if (input[i] >= '가')       //완성된 문자를 입력했을때 검색패턴 쓰기
				{
					int magic = ((input[i] - '가') % 588);       //받침이 있는지 검사

					if (magic == 0)     //받침이 없을때.
					{
						pattern += string.Format("[{0}-{1}]", input[i], (char)(input[i] + 27));
					}
					else        //받침이 있을때
					{
						magic = 27 - (magic % 28);
						pattern += string.Format("[{0}-{1}]", input[i], (char)(input[i] + magic));
					}
				}
				else if (input[i] >= 'A' && input[i] <= 'z')        // 영어를 입력했을때
				{
					pattern += input[i];
				}
				else if (input[i] >= '0' && input[i] <= '9')        // 숫자를 입력했을때.
				{
					pattern += input[i];
				}
			}

			if (pattern == string.Empty)
			{
				return new List<string>();
			}

			//Console.WriteLine(pattern);

			var query = list.Where(e =>
			{
				if (e.Length < 1 || partExtract == true)
				{
					return Regex.IsMatch(e, pattern);
				}
				else
				{
					return Regex.IsMatch(e.Substring(0, 1), pattern);
				}
			});

			return query;
		}

		private static void BasicQuery()
		{
			char[] chr = { 'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ',
						   'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ','ㅋ','ㅌ', 'ㅍ', 'ㅎ' };
			string[] str = { "가", "까", "나", "다", "따", "라", "마", "바", "빠", "사", "싸",
						   "아", "자", "짜", "차","카","타", "파", "하" };
			string x = Console.ReadLine();
			string temp = "";

			for (int i = 0; i < x.Length; i++)
			{
				if (x[i] < (char)43032)
				{
					for (int j = 0; j < chr.Length; j++)
					{
						if (x[i] == chr[j])
						{
							temp += str[j];
						}
					}
				}
				else
				{
					temp += x[i];
				}
			}

			Console.WriteLine(temp);
		}
	}
}
