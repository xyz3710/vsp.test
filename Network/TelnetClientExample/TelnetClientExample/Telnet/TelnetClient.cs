﻿/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace GS.Common.Net.Telnet
{

	/// <summary>
	/// The TelnetClient class implements the simple network virtual
	/// terminal (NVT) for the Telnet protocol according to RFC 854.  It
	/// does not implement any of the extra Telnet options because it
	/// is meant to be used within a Java program providing automated
	/// access to Telnet accessible resources.
	/// <para>
	/// The class can be used by first connecting to a server using the
	/// SocketClient
	/// <seealso cref="org.apache.commons.net.SocketClient#connect connect"/>
	/// method.  Then an InputStream and OutputStream for sending and
	/// receiving data over the Telnet connection can be obtained by
	/// using the <seealso cref="#getInputStream  getInputStream() "/> and
	/// <seealso cref="#getOutputStream  getOutputStream() "/> methods.
	/// When you finish using the streams, you must call
	/// <seealso cref="#disconnect  disconnect "/> rather than simply
	/// closing the streams.
	/// </para>
	/// </summary>
	public class TelnetClient : TelnetBase
	{
		private System.IO.Stream __input;
		private System.IO.Stream __output;
		protected internal bool readerThread = true;
		private ITelnetInputListener inputListener;

		/// <summary>
		/// Default TelnetClient constructor, sets terminal-type {@code VT100}.
		/// </summary>
		public TelnetClient() : base("VT100")
		{
			/* TERMINAL-TYPE option (start)*/
			/* TERMINAL-TYPE option (end)*/
			__input = null;
			__output = null;
		}

		/// <summary>
		/// Construct an instance with the specified terminal type.
		/// </summary>
		/// <param name="termtype"> the terminal type to use, e.g. {@code VT100} </param>
		/* TERMINAL-TYPE option (start)*/
		public TelnetClient(string termtype) : base(termtype)
		{
			__input = null;
			__output = null;
		}
		/* TERMINAL-TYPE option (end)*/

		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void _flushOutputStream() throws java.io.IOException
		public virtual void FlushOutputStream()
		{
			_output_.Flush();
		}

		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void _closeOutputStream() throws java.io.IOException
		public virtual void CloseOutputStream()
		{
			try
			{
				if (_output_ != null)
				{
					_output_.Close();
				}
			}
			finally
			{
				_output_ = null;
			}
		}

		/// <summary>
		/// Handles special connection requirements.
		/// </summary>
		/// <exception cref="IOException">  If an error occurs during connection setup.</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override protected void _connectAction_() throws java.io.IOException
		internal override void _connectAction_()
		{
			base._connectAction_();
			TelnetInputStream tmp = new TelnetInputStream(_input_, this, readerThread);

			if (readerThread)
			{
				tmp._start();
			}
			// __input CANNOT refer to the TelnetInputStream.  We run into
			// blocking problems when some classes use TelnetInputStream, so
			// we wrap it with a BufferedInputStream which we know is safe.
			// This blocking behavior requires further investigation, but right
			// now it looks like classes like InputStreamReader are not implemented
			// in a safe manner.
			__input = new System.IO.BufferedStream(tmp);
			__output = new TelnetOutputStream(this);
		}

		/// <summary>
		/// Disconnects the telnet session, closing the input and output streams
		/// as well as the socket.  If you have references to the
		/// input and output streams of the telnet connection, you should not
		/// close them yourself, but rather call disconnect to properly close
		/// the connection.
		/// </summary>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public void disconnect() throws java.io.IOException
		public void Disconnect()
		{
			try
			{
				if (__input != null)
				{
					__input.Close();
				}
				if (__output != null)
				{
					__output.Close();
				}
			}
			finally
			{ // NET-594
				__output = null;
				__input = null;
				base.Close();
			}
		}

		/// <summary>
		/// Returns the telnet connection output stream.  You should not close the
		/// stream when you finish with it.  Rather, you should call
		/// <seealso cref="#disconnect  disconnect "/>.
		/// </summary>
		/// <returns> The telnet connection output stream.</returns>
		public virtual System.IO.Stream OutputStream
		{
			get
			{
				return __output;
			}
		}

		/// <summary>
		/// Returns the telnet connection input stream.  You should not close the
		/// stream when you finish with it.  Rather, you should call
		/// <seealso cref="#disconnect  disconnect "/>.
		/// </summary>
		/// <returns> The telnet connection input stream.</returns>
		public virtual System.IO.Stream InputStream
		{
			get
			{
				return __input;
			}
		}

		/// <summary>
		/// Returns the state of the option on the local side.
		/// </summary>
		/// <param name="option"> - Option to be checked.
		/// </param>
		/// <returns> The state of the option on the local side.</returns>
		public virtual bool getLocalOptionState(int option)
		{
			/* BUG (option active when not already acknowledged) (start)*/
			return (_stateIsWill(option) && _requestedWill(option));
			/* BUG (option active when not already acknowledged) (end)*/
		}

		/// <summary>
		/// Returns the state of the option on the remote side.
		/// </summary>
		/// <param name="option"> - Option to be checked.
		/// </param>
		/// <returns> The state of the option on the remote side.</returns>
		public virtual bool getRemoteOptionState(int option)
		{
			/* BUG (option active when not already acknowledged) (start)*/
			return (_stateIsDo(option) && _requestedDo(option));
			/* BUG (option active when not already acknowledged) (end)*/
		}
		/* open TelnetOptionHandler functionality (end)*/

		/* Code Section added for supporting AYT (start)*/

		/// <summary>
		/// Sends an Are You There sequence and waits for the result.
		/// </summary>
		/// <param name="timeout"> - Time to wait for a response (millis.)
		/// </param>
		/// <returns> true if AYT received a response, false otherwise
		/// </returns>
		/// <exception cref="InterruptedException"> on error </exception>
		/// <exception cref="IllegalArgumentException"> on error </exception>
		/// <exception cref="IOException"> on error</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: public boolean sendAYT(long timeout) throws java.io.IOException, IllegalArgumentException, InterruptedException
		public virtual bool sendAYT(long timeout)
		{
			return _sendAYT(timeout);
		}
		/* Code Section added for supporting AYT (start)*/

		/// <summary>
		/// Sends a protocol-specific subnegotiation message to the remote peer.
		/// <seealso cref="TelnetClient"/> will add the IAC SB &amp; IAC SE framing bytes;
		/// the first byte in {@code message} should be the appropriate telnet
		/// option code.
		/// 
		/// <para>
		/// This method does not wait for any response. Subnegotiation messages
		/// sent by the remote end can be handled by registering an approrpriate
		/// <seealso cref="TelnetOptionHandler"/>.
		/// </para>
		/// </summary>
		/// <param name="message"> option code followed by subnegotiation payload </param>
		/// <exception cref="IllegalArgumentException"> if {@code message} has length zero </exception>
		/// <exception cref="IOException"> if an I/O error occurs while writing the message
		/// @since 3.0</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: public void sendSubnegotiation(int[] message) throws java.io.IOException, IllegalArgumentException
		public virtual void sendSubnegotiation(int[] message)
		{
			if (message.Length < 1)
			{
				throw new System.ArgumentException("zero length message");
			}
			_sendSubnegotiation(message);
		}

		/// <summary>
		/// Sends a command byte to the remote peer, adding the IAC prefix.
		/// 
		/// <para>
		/// This method does not wait for any response. Messages
		/// sent by the remote end can be handled by registering an approrpriate
		/// <seealso cref="TelnetOptionHandler"/>.
		/// </para>
		/// </summary>
		/// <param name="command"> the code for the command </param>
		/// <exception cref="IOException"> if an I/O error occurs while writing the message </exception>
		/// <exception cref="IllegalArgumentException">  on error
		/// @since 3.0</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: public void sendCommand(byte command) throws java.io.IOException, IllegalArgumentException
		public virtual void sendCommand(byte command)
		{
			_sendCommand(command);
		}

		/* Code Section added for supporting spystreams (start)*/
		/// <summary>
		/// Registers an OutputStream for spying what's going on in
		/// the TelnetClient session.
		/// </summary>
		/// <param name="spystream"> - OutputStream on which session activity
		/// will be echoed.</param>
		public virtual void RegisterSpyStream(System.IO.Stream spystream)
		{
			base._registerSpyStream(spystream);
		}

		/// <summary>
		/// Stops spying this TelnetClient.
		/// 
		/// </summary>
		public virtual void stopSpyStream()
		{
			base._stopSpyStream();
		}
		/* Code Section added for supporting spystreams (end)*/

		/// <summary>
		/// Registers a notification handler to which will be sent
		/// notifications of received telnet option negotiation commands.
		/// </summary>
		/// <param name="notifhand"> - TelnetNotificationHandler to be registered</param>
		public override void registerNotifHandler(ITelnetNotificationHandler notifhand)
		{
			base.registerNotifHandler(notifhand);
		}

		/// <summary>
		/// Unregisters the current notification handler.
		/// 
		/// </summary>
		public override void unregisterNotifHandler()
		{
			base.unregisterNotifHandler();
		}

		/// <summary>
		/// Sets the status of the reader thread.
		/// 
		/// <para>
		/// When enabled, a seaparate internal reader thread is created for new
		/// connections to read incoming data as it arrives. This results in
		/// immediate handling of option negotiation, notifications, etc.
		/// (at least until the fixed-size internal buffer fills up).
		/// Otherwise, no thread is created an all negotiation and option
		/// handling is deferred until a read() is performed on the
		/// <seealso cref="#getInputStream input stream"/>.
		/// </para>
		/// 
		/// <para>
		/// The reader thread must be enabled for <seealso cref="ITelnetInputListener"/>
		/// support.
		/// </para>
		/// 
		/// <para>
		/// When this method is invoked, the reader thread status will apply to all
		/// subsequent connections; the current connection (if any) is not affected.
		/// </para>
		/// </summary>
		/// <param name="flag"> true to enable the reader thread, false to disable </param>
		/// <seealso cref= #registerInputListener</seealso>
		public virtual bool ReaderThread
		{
			set
			{
				readerThread = value;
			}
			get
			{
				return (readerThread);
			}
		}


		/// <summary>
		/// Register a listener to be notified when new incoming data is
		/// available to be read on the <seealso cref="#getInputStream input stream"/>.
		/// Only one listener is supported at a time.
		/// 
		/// <para>
		/// More precisely, notifications are issued whenever the number of
		/// bytes available for immediate reading (i.e., the value returned
		/// by <seealso cref="InputStream#available"/>) transitions from zero to non-zero.
		/// Note that (in general) multiple reads may be required to empty the
		/// buffer and reset this notification, because incoming bytes are being
		/// added to the internal buffer asynchronously.
		/// </para>
		/// 
		/// <para>
		/// Notifications are only supported when a {@link #setReaderThread
		/// reader thread} is enabled for the connection.
		/// </para>
		/// </summary>
		/// <param name="listener"> listener to be registered; replaces any previous
		/// @since 3.0</param>
		public virtual void registerInputListener(ITelnetInputListener listener)
		{
			lock (this)
			{
				inputListener = listener;
			}
		}

		/// <summary>
		/// Unregisters the current <seealso cref="ITelnetInputListener"/>, if any.
		/// 
		/// @since 3.0
		/// </summary>
		public virtual void unregisterInputListener()
		{
			lock (this)
			{
				inputListener = null;
			}
		}

		// Notify input listener
		internal virtual void notifyInputListener()
		{
			ITelnetInputListener listener;
			lock (this)
			{
				listener = inputListener;
			}
			if (listener != null)
			{
				listener.TelnetInputAvailable();
			}
		}
	}

}
