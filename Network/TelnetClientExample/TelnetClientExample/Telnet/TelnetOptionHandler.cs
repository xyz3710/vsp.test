﻿/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace GS.Common.Net.Telnet
{
	/// <summary>
	/// The TelnetOptionHandler class is the base class to be used
	/// for implementing handlers for telnet options.
	/// <para>
	/// TelnetOptionHandler implements basic option handling
	/// functionality and defines abstract methods that must be
	/// implemented to define subnegotiation behaviour.
	/// </para>
	/// </summary>
	public abstract class TelnetOptionHandler
	{
		/// <summary>
		/// Constructor for the TelnetOptionHandler. Allows defining desired
		/// initial setting for local/remote activation of this option and
		/// behaviour in case a local/remote activation request for this
		/// option is received.
		/// </summary>
		/// <param name="optcode"> - Option code. </param>
		/// <param name="initlocal"> - if set to true, a WILL is sent upon connection. </param>
		/// <param name="initremote"> - if set to true, a DO is sent upon connection. </param>
		/// <param name="acceptlocal"> - if set to true, any DO request is accepted. </param>
		/// <param name="acceptremote"> - if set to true, any WILL request is accepted.</param>
		public TelnetOptionHandler(int optcode,
			bool initlocal,
			bool initremote,
			bool acceptlocal,
			bool acceptremote)
		{
			OptionCode = optcode;
			InitLocal = initlocal;
			InitRemote = initremote;
			AcceptLocal = acceptlocal;
			AcceptRemote = acceptremote;
		}

		/// <summary>
		/// Returns the option code for this option.
		/// </summary>
		/// <returns> Option code.</returns>
		public virtual int OptionCode { get; private set; } = -1;

		/// <summary>
		/// Returns a boolean indicating whether to accept a DO
		/// request coming from the other end.
		/// </summary>
		/// <returns> true if a DO request shall be accepted.</returns>
		public virtual bool AcceptLocal { get; set; } = false;

		/// <summary>
		/// Returns a boolean indicating whether to accept a WILL
		/// request coming from the other end.
		/// </summary>
		/// <returns> true if a WILL request shall be accepted.</returns>
		public virtual bool AcceptRemote { get; set; } = false;

		/// <summary>
		/// Returns a boolean indicating whether to send a WILL request
		/// to the other end upon connection.
		/// </summary>
		/// <returns> true if a WILL request shall be sent upon connection.</returns>
		public virtual bool InitLocal { get; set; } = false;

		/// <summary>
		/// Returns a boolean indicating whether to send a DO request
		/// to the other end upon connection.
		/// </summary>
		/// <returns> true if a DO request shall be sent upon connection.</returns>
		public virtual bool InitRemote { get; set; } = false;

		/// <summary>
		/// Method called upon reception of a subnegotiation for this option
		/// coming from the other end.
		/// <para>
		/// This implementation returns null, and
		/// must be overridden by the actual TelnetOptionHandler to specify
		/// which response must be sent for the subnegotiation request.
		/// </para>
		/// </summary>
		/// <param name="suboptionData"> - the sequence received, without IAC SB &amp; IAC SE </param>
		/// <param name="suboptionLength"> - the length of data in suboption_data
		/// </param>
		/// <returns> response to be sent to the subnegotiation sequence. TelnetClient
		/// will add IAC SB &amp; IAC SE. null means no response</returns>
		public virtual int[] AnswerSubnegotiation(int[] suboptionData, int suboptionLength)
		{
			return null;
		}

		/// <summary>
		/// This method is invoked whenever this option is acknowledged active on
		/// the local end (TelnetClient sent a WILL, remote side sent a DO).
		/// The method is used to specify a subnegotiation sequence that will be
		/// sent by TelnetClient when the option is activated.
		/// <para>
		/// This implementation returns null, and must be overriden by
		/// the actual TelnetOptionHandler to specify
		/// which response must be sent for the subnegotiation request.
		/// </para>
		/// </summary>
		/// <returns> subnegotiation sequence to be sent by TelnetClient. TelnetClient
		/// will add IAC SB &amp; IAC SE. null means no subnegotiation.</returns>
		public virtual int[] StartSubnegotiationLocal()
		{
			return null;
		}

		/// <summary>
		/// This method is invoked whenever this option is acknowledged active on
		/// the remote end (TelnetClient sent a DO, remote side sent a WILL).
		/// The method is used to specify a subnegotiation sequence that will be
		/// sent by TelnetClient when the option is activated.
		/// <para>
		/// This implementation returns null, and must be overriden by
		/// the actual TelnetOptionHandler to specify
		/// which response must be sent for the subnegotiation request.
		/// </para>
		/// </summary>
		/// <returns> subnegotiation sequence to be sent by TelnetClient. TelnetClient
		/// will add IAC SB &amp; IAC SE. null means no subnegotiation.</returns>
		public virtual int[] StartSubnegotiationRemote()
		{
			return null;
		}

		/// <summary>
		/// Returns a boolean indicating whether a WILL request sent to the other
		/// side has been acknowledged.
		/// </summary>
		/// <returns> true if a WILL sent to the other side has been acknowledged.</returns>
		internal virtual bool Will { get; set; } = false;

		/// <summary>
		/// Returns a boolean indicating whether a DO request sent to the other
		/// side has been acknowledged.
		/// </summary>
		/// <returns> true if a DO sent to the other side has been acknowledged.</returns>
		internal virtual bool Do { get; set; } = false;
	}
}
