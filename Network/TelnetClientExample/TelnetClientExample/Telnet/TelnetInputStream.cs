﻿/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distribyted with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distribyted under the License is distribyted on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;
using System.Threading;

namespace GS.Common.Net.Telnet
{
	internal sealed class TelnetInputStream : BufferedStream, IRunnable
	{
		/// <summary>
		/// End of file has been reached </summary>
		private const int EOF = -1;

		/// <summary>
		/// Read would block </summary>
		private const int WOULD_BLOCK = -2;

		// TODO should these be private enums?
		internal const int _STATE_DATA = 0, _STATE_IAC = 1, _STATE_WILL = 2, _STATE_WONT = 3, _STATE_DO = 4, _STATE_DONT = 5, _STATE_SB = 6, _STATE_SE = 7, _STATE_CR = 8, _STATE_IAC_SB = 9;

		private bool __hasReachedEOF; // @GuardedBy("__queue")
		private volatile bool __isClosed;
		private bool __readIsWaiting;
		private int __receiveState, __queueHead, __queueTail, __bytesAvailable;
		private readonly int[] __queue;
		private readonly TelnetClient __client;
		private readonly Thread __thread;
		private ThreadInterruptedException __ioException;

		/* TERMINAL-TYPE option (start)*/
		private readonly int[] __suboption = new int[512];
		private int __suboption_count = 0;
		/* TERMINAL-TYPE option (end)*/

		private volatile bool __threaded;

		internal TelnetInputStream(System.IO.Stream input, TelnetClient client, bool readerThread)
		{
			__client = client;
			__receiveState = _STATE_DATA;
			__isClosed = true;
			__hasReachedEOF = false;
			// Make it 2049, because when full, one slot will go unused, and we
			// want a 2048 byte buffer just to have a round number (base 2 that is)
			__queue = new int[2049];
			__queueHead = 0;
			__queueTail = 0;
			__bytesAvailable = 0;
			__ioException = null;
			__readIsWaiting = false;
			__threaded = false;

			if (readerThread)
			{
				__thread = new Thread(Run);
			}
			else
			{
				__thread = null;
			}
		}

		internal TelnetInputStream(System.IO.Stream input, TelnetClient client)
			: this(input, client, true)
		{
		}

		public override bool CanRead
		{
			get
			{
				return __client.InputStream.CanRead;
			}
		}

		public override bool CanSeek
		{
			get
			{
				return __client.InputStream.CanSeek;
			}
		}

		public override bool CanWrite
		{
			get
			{
				return __client.InputStream.CanWrite;
			}
		}

		public override long Length
		{
			get
			{
				return __client.InputStream.Length;
			}
		}

		public override long Position
		{
			get
			{
				return __client.InputStream.Position;
			}
			set
			{
				__client.InputStream.Position = value;
			}
		}

		internal void _start()
		{
			if (__thread == null)
			{
				return;
			}

			int priority;
			__isClosed = false;
			// TODO remove this
			// Need to set a higher priority in case JVM does not use pre-emptive
			// threads.  This should prevent scheduler induced deadlock (rather than
			// deadlock caused by a bug in this code).
			priority = (int)Thread.CurrentThread.Priority + 1;

			if (priority > (int)ThreadPriority.Highest)
			{
				priority = (int)ThreadPriority.Highest;
			}

			__thread.Priority = (ThreadPriority)priority;
			__thread.IsBackground = true;
			__thread.Start();
			__threaded = true; // tell _processChar that we are running threaded
		}

		// synchronized(__client) critical sections are to protect against
		// TelnetInputStream writing through the telnet client at same time
		// as a processDo/Will/etc. command invoked from TelnetInputStream
		// tries to write.
		/// <summary>
		/// Get the next byte of data.
		/// IAC commands are processed internally and do not return data.
		/// </summary>
		/// <param name="mayBlock"> true if method is allowed to block </param>
		/// <returns> the next byte of data,
		/// or -1 (EOF) if end of stread reached,
		/// or -2 (WOULD_BLOCK) if mayBlock is false and there is no data available </returns>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: private int __read(boolean mayBlock) throws java.io.IOException
		private int __read(bool mayBlock)
		{
			int ch;

			while (true)
			{

				// If there is no more data AND we were told not to block,
				// just return WOULD_BLOCK (-2). (More efficient than exception.)
				if (!mayBlock && __client.Available == 0)
				{
					return WOULD_BLOCK;
				}

				// Otherwise, exit only when we reach end of stream.
				if ((ch = Read()) < 0)
				{
					return EOF;
				}

				ch = (ch & 0xff);

				/* Code Section added for supporting AYT (start)*/
				lock (__client)
				{
					__client._processAYTResponse();
				}
				/* Code Section added for supporting AYT (end)*/

				/* Code Section added for supporting spystreams (start)*/
				__client._spyRead(ch);
				/* Code Section added for supporting spystreams (end)*/

				switch (__receiveState)
				{
					case _STATE_CR:
						if (ch == '\0')
						{
							// Strip null
							continue;
						}
						// How do we handle newline after cr?
						//  else if (ch == '\n' && _requestedDont(TelnetOption.ECHO) &&

						// Handle as normal data by falling through to _STATE_DATA case

						//$FALL-THROUGH$

						break;
					case _STATE_DATA:
						if (ch == (int)TelnetCommands.IAC)
						{
							__receiveState = _STATE_IAC;

							continue;
						}

						if (ch == '\r')
						{
							lock (__client)
							{
								if (__client._requestedDont(TelnetOption.BINARY))
								{
									__receiveState = _STATE_CR;
								}
								else
								{
									__receiveState = _STATE_DATA;
								}
							}
						}
						else
						{
							__receiveState = _STATE_DATA;
						}

						break;

					case _STATE_IAC:
						switch ((TelnetCommands)ch)
						{
							case TelnetCommands.WILL:
								__receiveState = _STATE_WILL;

								continue;
							case TelnetCommands.WONT:
								__receiveState = _STATE_WONT;

								continue;
							case TelnetCommands.DO:
								__receiveState = _STATE_DO;

								continue;
							case TelnetCommands.DONT:
								__receiveState = _STATE_DONT;

								continue;
							/* TERMINAL-TYPE option (start)*/
							case TelnetCommands.SB:
								__suboption_count = 0;
								__receiveState = _STATE_SB;

								continue;
							/* TERMINAL-TYPE option (end)*/
							case TelnetCommands.IAC:
								__receiveState = _STATE_DATA;

								break; // exit to enclosing switch to return IAC from read
							case TelnetCommands.SE: // unexpected byte! ignore it (don't send it as a command)
								__receiveState = _STATE_DATA;

								continue;
							default:
								__receiveState = _STATE_DATA;
								__client._processCommand(ch); // Notify the user

								continue; // move on the next char
						}

						break; // exit and return from read
					case _STATE_WILL:
						lock (__client)
						{
							__client._processWill(ch);
							__client.FlushOutputStream();
						}
						__receiveState = _STATE_DATA;

						continue;
					case _STATE_WONT:
						lock (__client)
						{
							__client._processWont(ch);
							__client.FlushOutputStream();
						}
						__receiveState = _STATE_DATA;

						continue;
					case _STATE_DO:
						lock (__client)
						{
							__client._processDo(ch);
							__client.FlushOutputStream();
						}
						__receiveState = _STATE_DATA;

						continue;
					case _STATE_DONT:
						lock (__client)
						{
							__client._processDont(ch);
							__client.FlushOutputStream();
						}
						__receiveState = _STATE_DATA;

						continue;
					/* TERMINAL-TYPE option (start)*/
					case _STATE_SB:
						switch ((TelnetCommands)ch)
						{
							case TelnetCommands.IAC:
								__receiveState = _STATE_IAC_SB;

								continue;
							default:
								// store suboption char
								if (__suboption_count < __suboption.Length)
								{
									__suboption[__suboption_count++] = ch;
								}

								break;
						}

						__receiveState = _STATE_SB;

						continue;
					case _STATE_IAC_SB: // IAC received during SB phase
						switch ((TelnetCommands)ch)
						{
							case TelnetCommands.SE:
								lock (__client)
								{
									__client._processSuboption(__suboption, __suboption_count);
									__client.FlushOutputStream();
								}
								__receiveState = _STATE_DATA;

								continue;
							case TelnetCommands.IAC: // De-dup the duplicated IAC
								if (__suboption_count < __suboption.Length)
								{
									__suboption[__suboption_count++] = ch;
								}

								break;
							default: // unexpected byte! ignore it

								break;
						}
						__receiveState = _STATE_SB;

						continue;
						/* TERMINAL-TYPE option (end)*/
				}

				break;
			}

			return ch;
		}

		// synchronized(__client) critical sections are to protect against
		// TelnetInputStream writing through the telnet client at same time
		// as a processDo/Will/etc. command invoked from TelnetInputStream
		// tries to write. Returns true if buffer was previously empty.
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: private boolean __processChar(int ch) throws InterruptedException
		private bool __processChar(int ch)
		{
			// Critical section because we're altering __bytesAvailable,
			// __queueTail, and the contents of _queue.
			bool bufferWasEmpty;

			lock (__queue)
			{
				bufferWasEmpty = (__bytesAvailable == 0);
				while (__bytesAvailable >= __queue.Length - 1)
				{
					// The queue is full. We need to wait before adding any more data to it. Hopefully the stream owner
					// will consume some data soon!
					if (__threaded)
					{
						Monitor.Pulse(__queue);
						try
						{
							Monitor.Wait(__queue);
						}
						catch (ThreadInterruptedException e)
						{
							throw e;
						}
					}
					else
					{
						// We've been asked to add another character to the queue, but it is already full and there's
						// no other thread to drain it. This should not have happened!
						throw new System.InvalidOperationException("Queue is full! Cannot process another character.");
					}
				}

				// Need to do this in case we're not full, but block on a read
				if (__readIsWaiting && __threaded)
				{
					Monitor.Pulse(__queue);
				}

				__queue[__queueTail] = ch;
				++__bytesAvailable;

				if (++__queueTail >= __queue.Length)
				{
					__queueTail = 0;
				}
			}
			return bufferWasEmpty;
		}

		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public int read() throws java.io.IOException
		public int Read()
		{
			// Critical section because we're altering __bytesAvailable,
			// __queueHead, and the contents of _queue in addition to
			// testing value of __hasReachedEOF.
			lock (__queue)
			{
				while (true)
				{
					if (__ioException != null)
					{
						var e = __ioException;
						__ioException = null;
						throw e;
					}

					if (__bytesAvailable == 0)
					{
						// Return EOF if at end of file
						if (__hasReachedEOF)
						{
							return EOF;
						}

						// Otherwise, we have to wait for queue to get something
						if (__threaded)
						{
							Monitor.Pulse(__queue);
							try
							{
								__readIsWaiting = true;
								Monitor.Wait(__queue);
								__readIsWaiting = false;
							}
							catch (ThreadInterruptedException e)
							{
								throw new ThreadInterruptedException("Fatal thread interruption during read.", e);
							}
						}
						else
						{
							//__alreadyread = false;
							__readIsWaiting = true;
							int ch;
							bool mayBlock = true; // block on the first read only

							do
							{
								try
								{
									if ((ch = __read(mayBlock)) < 0)
									{ // must be EOF
										if (ch != WOULD_BLOCK)
										{
											return (ch);
										}
									}
								}
								catch (ThreadInterruptedException e)
								{
									lock (__queue)
									{
										__ioException = e;
										Monitor.PulseAll(__queue);
										try
										{
											Monitor.Wait(__queue, TimeSpan.FromMilliseconds(100));
										}
										catch (ThreadInterruptedException)
										{
											// Ignored
										}
									}
									return EOF;
								}


								try
								{
									if (ch != WOULD_BLOCK)
									{
										__processChar(ch);
									}
								}
								catch (ThreadInterruptedException)
								{
									if (__isClosed)
									{
										return EOF;
									}
								}

								// Reads should not block on subsequent iterations. Potentially, this could happen if the
								// remaining buffered socket data consists entirely of Telnet command sequence and no "user" data.
								mayBlock = false;

							} while (available() > 0 && __client.Available > 0 && __bytesAvailable < __queue.Length - 1);
							// Continue reading as long as there is data available and the queue is not full.

							__readIsWaiting = false;
						}
						continue;
					}
					else
					{
						int ch;

						ch = __queue[__queueHead];

						if (++__queueHead >= __queue.Length)
						{
							__queueHead = 0;
						}

						--__bytesAvailable;

						// Need to explicitly notify() so available() works properly
						if (__bytesAvailable == 0 && __threaded)
						{
							Monitor.Pulse(__queue);
						}

						return ch;
					}
				}
			}
		}

		/// <summary>
		/// Reads the next number of bytes from the stream into an array and
		/// returns the number of bytes read.  Returns -1 if the end of the
		/// stream has been reached.
		/// </summary>
		/// <param name="buffer">  The byte array in which to store the data. </param>
		/// <returns> The number of bytes read. Returns -1 if the
		///          end of the message has been reached. </returns>
		/// <exception cref="IOException"> If an error occurs in reading the underlying
		///            stream.</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public int read(byte buffer[]) throws java.io.IOException
		public int Read(byte[] buffer)
		{
			return Read(buffer, 0, buffer.Length);
		}

		/// <summary>
		/// Reads the next number of bytes from the stream into an array and returns
		/// the number of bytes read.  Returns -1 if the end of the
		/// message has been reached.  The characters are stored in the array
		/// starting from the given offset and up to the length specified.
		/// </summary>
		/// <param name="buffer"> The byte array in which to store the data. </param>
		/// <param name="offset">  The offset into the array at which to start storing data. </param>
		/// <param name="length">   The number of bytes to read. </param>
		/// <returns> The number of bytes read. Returns -1 if the
		///          end of the stream has been reached. </returns>
		/// <exception cref="IOException"> If an error occurs while reading the underlying
		///            stream.</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public int read(byte buffer[], int offset, int length) throws java.io.IOException
		public override int Read(byte[] buffer, int offset, int length)
		{
			int ch, off;

			if (length < 1)
			{
				return 0;
			}

			// Critical section because run() may change __bytesAvailable
			lock (__queue)
			{
				if (length > __bytesAvailable)
				{
					length = __bytesAvailable;
				}
			}

			if ((ch = Read()) == EOF)
			{
				return EOF;
			}

			off = offset;

			do
			{
				buffer[offset++] = (byte)ch;
			} while (--length > 0 && (ch = Read()) != EOF);

			//__client._spyRead(buffer, off, offset - off);
			return (offset - off);
		}

		/// <summary>
		/// Returns false.  Mark is not supported.
		/// </summary>
		public bool markSupported()
		{
			return false;
		}

		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public int available() throws java.io.IOException
		public int available()
		{
			// Critical section because run() may change __bytesAvailable
			lock (__queue)
			{
				if (__threaded)
				{ // Must not call super.available when running threaded: NET-466
					return __bytesAvailable;
				}
				else
				{
					return __bytesAvailable + __client.Available;
				}
			}
		}


		// Cannot be synchronized.  Will cause deadlock if run() is blocked
		// in read because BufferedInputStream read() is synchronized.
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public void close() throws java.io.IOException
		public void close()
		{
			// Completely disregard the fact thread may still be running.
			// We can't afford to block on this close by waiting for
			// thread to terminate because few if any JVM's will actually
			// interrupt a system read() from the interrupt() method.
			base.Close();

			lock (__queue)
			{
				__hasReachedEOF = true;
				__isClosed = true;

				if (__thread != null && __thread.IsAlive)
				{
					__thread.Interrupt();
				}

				Monitor.PulseAll(__queue);
			}
		}

		public void Run()
		{
			int ch;

			try
			{
				while (!__isClosed)
				{
					try
					{
						if ((ch = __read(true)) < 0)
						{
							break;
						}
					}
					catch (ThreadInterruptedException e)
					{
						lock (__queue)
						{
							__ioException = e;
							Monitor.PulseAll(__queue);

							try
							{
								Monitor.Wait(__queue, TimeSpan.FromMilliseconds(100));
							}
							catch (ThreadInterruptedException)
							{
								if (__isClosed)
								{
									break;
								}
							}
							continue;
						}
					}
					catch (Exception)
					{
						// We treat any runtime exceptions as though the
						// stream has been closed.  We close the
						// underlying stream just to be sure.
						Close();
						// Breaking the loop has the effect of setting
						// the state to closed at the end of the method.
						break;
					}

					// Process new character
					bool notify = false;
					try
					{
						notify = __processChar(ch);
					}
					catch (ThreadInterruptedException)
					{
						if (__isClosed)
						{
							break;
						}
					}

					// Notify input listener if buffer was previously empty
					if (notify)
					{
						__client.notifyInputListener();
					}
				}
			}
			catch (ThreadInterruptedException ioe)
			{
				lock (__queue)
				{
					__ioException = ioe;
				}
				__client.notifyInputListener();
			}

			lock (__queue)
			{
				__isClosed = true; // Possibly redundant
				__hasReachedEOF = true;
				Monitor.Pulse(__queue);
			}

			__threaded = false;
		}

		/// <summary>
		/// Flushes the stream.
		/// </summary>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public void flush() throws java.io.IOException
		public override void Flush()
		{
			__client.FlushOutputStream();
		}

		/// <summary>
		/// Closes the stream.
		/// </summary>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public void close() throws java.io.IOException
		public override void Close()
		{
			__client.CloseOutputStream();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return __client.InputStream.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			__client.InputStream.SetLength(value);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}
	}

	/* Emacs configuration
	 * Local variables:        **
	 * mode:             java  **
	 * c-basic-offset:   4     **
	 * indent-tabs-mode: nil   **
	 * End:                    **
	 */

}
