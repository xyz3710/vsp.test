﻿/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace GS.Common.Net.Telnet
{
	/// <summary>
	/// Implements the telnet echo option RFC 857.
	/// </summary>
	public class EchoOptionHandler : TelnetOptionHandler
	{
		/// <summary>
		/// Constructor for the EchoOptionHandler. Allows defining desired
		/// initial setting for local/remote activation of this option and
		/// behaviour in case a local/remote activation request for this
		/// option is received.
		/// </summary>
		/// <param name="initlocal"> - if set to true, a WILL is sent upon connection. </param>
		/// <param name="initremote"> - if set to true, a DO is sent upon connection. </param>
		/// <param name="acceptlocal"> - if set to true, any DO request is accepted. </param>
		/// <param name="acceptremote"> - if set to true, any WILL request is accepted.</param>
		public EchoOptionHandler(bool initlocal, bool initremote, bool acceptlocal, bool acceptremote) 
			: base(TelnetOption.ECHO, initlocal, initremote, acceptlocal, acceptremote)
		{
		}

		/// <summary>
		/// Constructor for the EchoOptionHandler. Initial and accept
		/// behaviour flags are set to false
		/// </summary>
		public EchoOptionHandler() 
			: base(TelnetOption.ECHO, false, false, false, false)
		{
		}
	}
}