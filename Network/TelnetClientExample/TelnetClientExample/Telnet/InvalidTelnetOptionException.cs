﻿/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace GS.Common.Net.Telnet
{
	/// <summary>
	/// The InvalidTelnetOptionException is the exception that is
	/// thrown whenever a TelnetOptionHandler with an invlaid
	/// option code is registered in TelnetClient with addOptionHandler.
	/// </summary>
	[Serializable]
	public class InvalidTelnetOptionException : Exception
	{
		/// <summary>
		/// Option code
		/// </summary>
		private readonly int optionCode;
		/// <summary>
		/// Error message
		/// </summary>
		private readonly string msg;

		/// <summary>
		/// Constructor for the exception.
		/// </summary>
		/// <param name="message"> - Error message. </param>
		/// <param name="optcode"> - Option code.</param>
		public InvalidTelnetOptionException(string message, int optcode)
		{
			optionCode = optcode;
			msg = message;
		}

		/// <summary>
		/// Gets the error message of ths exception.
		/// </summary>
		/// <returns> the error message.</returns>
		public override string Message
		{
			get
			{
				return (msg + ": " + optionCode);
			}
		}
	}
}