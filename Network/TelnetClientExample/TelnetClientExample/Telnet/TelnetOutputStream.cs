﻿/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.IO;
using System.Net.Sockets;

namespace GS.Common.Net.Telnet
{
	/// <summary>
	/// Wraps an output stream.
	/// <para>
	/// In binary mode, the only conversion is to double IAC.
	/// </para>
	/// <para>
	/// In ASCII mode, if convertCRtoCRLF is true (currently always true), any CR is converted to CRLF.
	/// IACs are doubled.
	/// Also a bare LF is converted to CRLF and a bare CR is converted to CR\0
	/// </para>
	/// </summary>
	internal sealed class TelnetOutputStream : Stream
	{
		private readonly TelnetClient __client;
		// TODO there does not appear to be any way to change this value - should it be a ctor parameter?
		private readonly bool __convertCRtoCRLF = true;
		private bool __lastWasCR = false;

		internal TelnetOutputStream(TelnetClient client)
		{
			__client = client;
		}

		public override bool CanRead
		{
			get
			{
				return __client.OutputStream.CanRead;
			}
		}

		public override bool CanSeek
		{
			get
			{
				return __client.OutputStream.CanSeek;
			}
		}

		public override bool CanWrite
		{
			get
			{
				return __client.OutputStream.CanWrite;
			}
		}

		public override long Length
		{
			get
			{
				return __client.OutputStream.Length;
			}
		}

		public override long Position
		{
			get
			{
				return __client.OutputStream.Position;
			}
			set
			{
				__client.OutputStream.Position = value;
			}
		}

		/// <summary>
		/// Writes a byte to the stream.
		/// </summary>
		/// <param name="ch"> The byte to write. </param>
		/// <exception cref="IOException"> If an error occurs while writing to the underlying
		///            stream.</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public void write(int ch) throws java.io.IOException
		public void Write(int ch)
		{
			lock(__client)
			{
				ch &= 0xff;

				if(__client._requestedWont(TelnetOption.BINARY)) // i.e. ASCII
				{
					if(__lastWasCR)
					{
						if(__convertCRtoCRLF)
						{
							__client._sendByte('\n');

							if(ch == '\n') // i.e. was CRLF anyway
							{
								__lastWasCR = false;
								return;
							}
						} // __convertCRtoCRLF
						else if(ch != '\n')
						{
							__client._sendByte('\0'); // RFC854 requires CR NUL for bare CR
						}
					}

					switch(ch)
					{
						case '\r':
							__client._sendByte('\r');
							__lastWasCR = true;

							break;
						case '\n':
							if(!__lastWasCR)
							{ // convert LF to CRLF
								__client._sendByte('\r');
							}
							__client._sendByte(ch);
							__lastWasCR = false;

							break;
						case (byte)TelnetCommands.IAC:
							__client._sendByte((byte)TelnetCommands.IAC);
							__client._sendByte((byte)TelnetCommands.IAC);
							__lastWasCR = false;

							break;
						default:
							__client._sendByte(ch);
							__lastWasCR = false;

							break;
					}
				} // end ASCII
				else if(ch == (int)TelnetCommands.IAC)
				{
					__client._sendByte(ch);
					__client._sendByte((byte)TelnetCommands.IAC);
				}
				else
				{
					__client._sendByte(ch);
				}
			}
		}

		/// <summary>
		/// Writes a byte array to the stream.
		/// </summary>
		/// <param name="buffer">  The byte array to write. </param>
		/// <exception cref="IOException"> If an error occurs while writing to the underlying
		///            stream.</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public void write(byte buffer[]) throws java.io.IOException
		public void Write(byte[] buffer)
		{
			Write(buffer, 0, buffer.Length);
		}

		/// <summary>
		/// Writes a number of bytes from a byte array to the stream starting from
		/// a given offset.
		/// </summary>
		/// <param name="buffer">  The byte array to write. </param>
		/// <param name="offset">  The offset into the array at which to start copying data. </param>
		/// <param name="length">  The number of bytes to write. </param>
		/// <exception cref="IOException"> If an error occurs while writing to the underlying
		///            stream.</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public void write(byte buffer[], int offset, int length) throws java.io.IOException
		public override void Write(byte[] buffer, int offset, int length)
		{
			lock(__client)
			{
				while(length-- > 0)
				{
					Write(buffer[offset++]);
				}
			}
		}

		/// <summary>
		/// Flushes the stream.
		/// </summary>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public void flush() throws java.io.IOException
		public override void Flush()
		{
			__client.FlushOutputStream();
		}

		/// <summary>
		/// Closes the stream.
		/// </summary>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override public void close() throws java.io.IOException
		public override void Close()
		{
			__client.CloseOutputStream();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return __client.OutputStream.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			__client.OutputStream.SetLength(value);
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			return __client.OutputStream.Read(buffer, offset, count);
		}
	}
}
