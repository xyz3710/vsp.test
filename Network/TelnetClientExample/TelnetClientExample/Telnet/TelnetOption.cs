﻿/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace GS.Common.Net.Telnet
{
	/// <summary>
	/// The TelnetOption class cannot be instantiated and only serves as a
	/// storehouse for telnet option constants.
	/// <para>
	/// Details regarding Telnet option specification can be found in RFC 855.
	/// </para>
	/// <seealso cref= org.apache.commons.net.telnet.Telnet </seealso>
	/// <seealso cref= org.apache.commons.net.telnet.TelnetClient</seealso>
	/// </summary>
	public class TelnetOption
	{
		private TelnetOption()
		{
		}

		/// <summary>
		/// The maximum value an option code can have.  This value is 255.
		/// </summary>
		public const int MAX_OPTION_VALUE = 255;

		public const int BINARY = 0;

		public const int ECHO = 1;

		public const int PREPARE_TO_RECONNECT = 2;

		public const int SUPPRESS_GO_AHEAD = 3;

		public const int APPROXIMATE_MESSAGE_SIZE = 4;

		public const int STATUS = 5;

		public const int TIMING_MARK = 6;

		public const int REMOTE_CONTROLLED_TRANSMISSION = 7;

		public const int NEGOTIATE_OUTPUT_LINE_WIDTH = 8;

		public const int NEGOTIATE_OUTPUT_PAGE_SIZE = 9;

		public const int NEGOTIATE_CARRIAGE_RETURN = 10;

		public const int NEGOTIATE_HORIZONTAL_TAB_STOP = 11;

		public const int NEGOTIATE_HORIZONTAL_TAB = 12;

		public const int NEGOTIATE_FORMFEED = 13;

		public const int NEGOTIATE_VERTICAL_TAB_STOP = 14;

		public const int NEGOTIATE_VERTICAL_TAB = 15;

		public const int NEGOTIATE_LINEFEED = 16;

		public const int EXTENDED_ASCII = 17;

		public const int FORCE_LOGOUT = 18;

		public const int BYTE_MACRO = 19;

		public const int DATA_ENTRY_TERMINAL = 20;

		public const int SUPDUP = 21;

		public const int SUPDUP_OUTPUT = 22;

		public const int SEND_LOCATION = 23;

		public const int TERMINAL_TYPE = 24;

		public const int END_OF_RECORD = 25;

		public const int TACACS_USER_IDENTIFICATION = 26;

		public const int OUTPUT_MARKING = 27;

		public const int TERMINAL_LOCATION_NUMBER = 28;

		public const int REGIME_3270 = 29;

		public const int X3_PAD = 30;

		public const int WINDOW_SIZE = 31;

		public const int TERMINAL_SPEED = 32;

		public const int REMOTE_FLOW_CONTROL = 33;

		public const int LINEMODE = 34;

		public const int X_DISPLAY_LOCATION = 35;

		public const int OLD_ENVIRONMENT_VARIABLES = 36;

		public const int AUTHENTICATION = 37;

		public const int ENCRYPTION = 38;

		public const int NEW_ENVIRONMENT_VARIABLES = 39;

		public const int EXTENDED_OPTIONS_LIST = 255;

		//JAVA TO C# CONVERTER TODO TASK: Most Java annotations will not have direct .NET equivalent attributes:
		//ORIGINAL LINE: @SuppressWarnings("unused") private static final int __FIRST_OPTION = BINARY;
		private const int __FIRST_OPTION = BINARY;
		private const int __LAST_OPTION = EXTENDED_OPTIONS_LIST;

		private static readonly string[] __optionString = new string[] { "BINARY", "ECHO", "RCP", "SUPPRESS GO AHEAD", "NAME", "STATUS", "TIMING MARK", "RCTE", "NAOL", "NAOP", "NAOCRD", "NAOHTS", "NAOHTD", "NAOFFD", "NAOVTS", "NAOVTD", "NAOLFD", "EXTEND ASCII", "LOGOUT", "BYTE MACRO", "DATA ENTRY TERMINAL", "SUPDUP", "SUPDUP OUTPUT", "SEND LOCATION", "TERMINAL TYPE", "END OF RECORD", "TACACS UID", "OUTPUT MARKING", "TTYLOC", "3270 REGIME", "X.3 PAD", "NAWS", "TSPEED", "LFLOW", "LINEMODE", "XDISPLOC", "OLD-ENVIRON", "AUTHENTICATION", "ENCRYPT", "NEW-ENVIRON", "TN3270E", "XAUTH", "CHARSET", "RSP", "Com Port Control", "Suppress Local Echo", "Start TLS", "KERMIT", "SEND-URL", "FORWARD_X", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "TELOPT PRAGMA LOGON", "TELOPT SSPI LOGON", "TELOPT PRAGMA HEARTBEAT", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "Extended-Options-List" };

		/// <summary>
		/// Returns the string representation of the telnet protocol option
		/// corresponding to the given option code.
		/// </summary>
		/// <param name="code"> The option code of the telnet protocol option </param>
		/// <returns> The string representation of the telnet protocol option.</returns>
		public static string getOption(int code)
		{
			if (__optionString[code].Length == 0)
			{
				return "UNASSIGNED";
			}
			else
			{
				return __optionString[code];
			}
		}


		/// <summary>
		/// Determines if a given option code is valid.  Returns true if valid,
		/// false if not.
		/// </summary>
		/// <param name="code">  The option code to test. </param>
		/// <returns> True if the option code is valid, false if not.
		///  </returns>
		public static bool isValidOption(int code)
		{
			return (code <= __LAST_OPTION);
		}
	}
}
