﻿/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace GS.Common.Net.Telnet
{
	public class TelnetBase : TcpClient
	{
		public bool _debug;
		public bool _debugOptions;

		internal static readonly byte[] _COMMAND_DO = new byte[] { (byte)TelnetCommands.IAC, (byte)TelnetCommands.DO };

		internal static readonly byte[] _COMMAND_DONT = new byte[] { (byte)TelnetCommands.IAC, (byte)TelnetCommands.DONT };

		internal static readonly byte[] _COMMAND_WILL = new byte[] { (byte)TelnetCommands.IAC, (byte)TelnetCommands.WILL };

		internal static readonly byte[] _COMMAND_WONT = new byte[] { (byte)TelnetCommands.IAC, (byte)TelnetCommands.WONT };

		internal static readonly byte[] _COMMAND_SB = new byte[] { (byte)TelnetCommands.IAC, (byte)TelnetCommands.SB };

		internal static readonly byte[] _COMMAND_SE = new byte[] { (byte)TelnetCommands.IAC, (byte)TelnetCommands.SE };

		internal const int _WILL_MASK = 0x01, _DO_MASK = 0x02, _REQUESTED_WILL_MASK = 0x04, _REQUESTED_DO_MASK = 0x08;

		protected NetworkStream _input_;
		protected NetworkStream _output_;

		/* public */
		internal const int DEFAULT_PORT = 23;

		internal int[] _doResponse, _willResponse, _options;

		/* TERMINAL-TYPE option (start)*/
		/// <summary>
		/// Terminal type option
		/// </summary>
		protected internal const int TERMINAL_TYPE = 24;

		/// <summary>
		/// Send (for subnegotiation)
		/// </summary>
		protected internal const int TERMINAL_TYPE_SEND = 1;

		/// <summary>
		/// Is (for subnegotiation)
		/// </summary>
		protected internal const int TERMINAL_TYPE_IS = 0;

		/// <summary>
		/// Is sequence (for subnegotiation)
		/// </summary>
		internal static readonly byte[] _COMMAND_IS = new byte[] { (byte)TERMINAL_TYPE, (byte)TERMINAL_TYPE_IS };

		/// <summary>
		/// Terminal type
		/// </summary>
		private string _terminalType = null;
		/* TERMINAL-TYPE option (end)*/

		/* open TelnetOptionHandler functionality (start)*/
		/// <summary>
		/// Array of option handlers
		/// </summary>
		private readonly TelnetOptionHandler[] _optionHandlers;

		/* open TelnetOptionHandler functionality (end)*/

		/* Code Section added for supporting AYT (start)*/
		/// <summary>
		/// AYT sequence
		/// </summary>
		internal static readonly byte[] _COMMAND_AYT = new byte[] { (byte)TelnetCommands.IAC, (byte)TelnetCommands.AYT };

		/// <summary>
		/// monitor to wait for AYT
		/// </summary>
		private readonly object aytMonitor = new object();

		/// <summary>
		/// flag for AYT
		/// </summary>
		private volatile bool aytFlag = true;
		/* Code Section added for supporting AYT (end)*/

		/// <summary>
		/// The stream on which to spy
		/// </summary>
		private volatile System.IO.Stream spyStream;

		/// <summary>
		/// The notification handler
		/// </summary>
		private ITelnetNotificationHandler __notifhand;

		/// <summary>
		/// Empty Constructor
		/// </summary>
		internal TelnetBase()
		{
			_doResponse = new int[TelnetOption.MAX_OPTION_VALUE + 1];
			_willResponse = new int[TelnetOption.MAX_OPTION_VALUE + 1];
			_options = new int[TelnetOption.MAX_OPTION_VALUE + 1];
			_optionHandlers = new TelnetOptionHandler[TelnetOption.MAX_OPTION_VALUE + 1];
			Encoding = Encoding.ASCII;
		}

		/// <summary>
		/// This constructor lets you specify the terminal type.
		/// </summary>
		/// <param name="termtype"> - terminal type to be negotiated (ej. VT100)</param>
		internal TelnetBase(string termtype)
			: this()
		{
			_terminalType = termtype;
			_optionHandlers = new TelnetOptionHandler[TelnetOption.MAX_OPTION_VALUE + 1];
		}

		protected Encoding Encoding { get; private set; }

		/// <summary>
		/// Looks for the state of the option.
		/// </summary>
		/// <returns> returns true if a will has been acknowledged
		/// </returns>
		/// <param name="option"> - option code to be looked up.</param>
		internal virtual bool _stateIsWill(int option)
		{
			return ((_options[option] & _WILL_MASK) != 0);
		}

		/// <summary>
		/// Looks for the state of the option.
		/// </summary>
		/// <returns> returns true if a wont has been acknowledged
		/// </returns>
		/// <param name="option"> - option code to be looked up.</param>
		internal virtual bool _stateIsWont(int option)
		{
			return !_stateIsWill(option);
		}

		/// <summary>
		/// Looks for the state of the option.
		/// </summary>
		/// <returns> returns true if a do has been acknowledged
		/// </returns>
		/// <param name="option"> - option code to be looked up.</param>
		internal virtual bool _stateIsDo(int option)
		{
			return ((_options[option] & _DO_MASK) != 0);
		}

		/// <summary>
		/// Looks for the state of the option.
		/// </summary>
		/// <returns> returns true if a dont has been acknowledged
		/// </returns>
		/// <param name="option"> - option code to be looked up.</param>
		internal virtual bool _stateIsDont(int option)
		{
			return !_stateIsDo(option);
		}

		/// <summary>
		/// Looks for the state of the option.
		/// </summary>
		/// <returns> returns true if a will has been reuqested
		/// </returns>
		/// <param name="option"> - option code to be looked up.</param>
		internal virtual bool _requestedWill(int option)
		{
			return ((_options[option] & _REQUESTED_WILL_MASK) != 0);
		}

		/// <summary>
		/// Looks for the state of the option.
		/// </summary>
		/// <returns> returns true if a wont has been reuqested
		/// </returns>
		/// <param name="option"> - option code to be looked up.</param>
		internal virtual bool _requestedWont(int option)
		{
			return !_requestedWill(option);
		}

		/// <summary>
		/// Looks for the state of the option.
		/// </summary>
		/// <returns> returns true if a do has been reuqested
		/// </returns>
		/// <param name="option"> - option code to be looked up.</param>
		internal virtual bool _requestedDo(int option)
		{
			return ((_options[option] & _REQUESTED_DO_MASK) != 0);
		}

		/// <summary>
		/// Looks for the state of the option.
		/// </summary>
		/// <returns> returns true if a dont has been reuqested
		/// </returns>
		/// <param name="option"> - option code to be looked up.</param>
		internal virtual bool _requestedDont(int option)
		{
			return !_requestedDo(option);
		}

		/// <summary>
		/// Sets the state of the option.
		/// </summary>
		/// <param name="option"> - option code to be set. </param>
		/// <exception cref="IOException"></exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void _setWill(int option) throws java.io.IOException
		internal virtual void _setWill(int option)
		{
			_options[option] |= _WILL_MASK;

			/* open TelnetOptionHandler functionality (start)*/
			if (_requestedWill(option))
			{
				if (_optionHandlers[option] != null)
				{
					_optionHandlers[option].Will = true;

					int[] subneg = _optionHandlers[option].StartSubnegotiationLocal();

					if (subneg != null)
					{
						_sendSubnegotiation(subneg);
					}
				}
			}
			/* open TelnetOptionHandler functionality (end)*/
		}

		/// <summary>
		/// Sets the state of the option.
		/// </summary>
		/// <param name="option"> - option code to be set. </param>
		/// <exception cref="IOException"></exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void _setDo(int option) throws java.io.IOException
		internal virtual void _setDo(int option)
		{
			_options[option] |= _DO_MASK;

			/* open TelnetOptionHandler functionality (start)*/
			if (_requestedDo(option))
			{
				if (_optionHandlers[option] != null)
				{
					_optionHandlers[option].Do = true;

					int[] subneg = _optionHandlers[option].StartSubnegotiationRemote();

					if (subneg != null)
					{
						_sendSubnegotiation(subneg);
					}
				}
			}
			/* open TelnetOptionHandler functionality (end)*/
		}

		/// <summary>
		/// Sets the state of the option.
		/// </summary>
		/// <param name="option"> - option code to be set.</param>
		internal virtual void _setWantWill(int option)
		{
			_options[option] |= _REQUESTED_WILL_MASK;
		}

		/// <summary>
		/// Sets the state of the option.
		/// </summary>
		/// <param name="option"> - option code to be set.</param>
		internal virtual void _setWantDo(int option)
		{
			_options[option] |= _REQUESTED_DO_MASK;
		}

		/// <summary>
		/// Sets the state of the option.
		/// </summary>
		/// <param name="option"> - option code to be set.</param>
		internal virtual void _setWont(int option)
		{
			_options[option] &= ~_WILL_MASK;

			/* open TelnetOptionHandler functionality (start)*/
			if (_optionHandlers[option] != null)
			{
				_optionHandlers[option].Will = false;
			}
			/* open TelnetOptionHandler functionality (end)*/
		}

		/// <summary>
		/// Sets the state of the option.
		/// </summary>
		/// <param name="option"> - option code to be set.</param>
		internal virtual void _setDont(int option)
		{
			_options[option] &= ~_DO_MASK;

			/* open TelnetOptionHandler functionality (start)*/
			if (_optionHandlers[option] != null)
			{
				_optionHandlers[option].Do = false;
			}
			/* open TelnetOptionHandler functionality (end)*/
		}

		/// <summary>
		/// Sets the state of the option.
		/// </summary>
		/// <param name="option"> - option code to be set.</param>
		internal virtual void _setWantWont(int option)
		{
			_options[option] &= ~_REQUESTED_WILL_MASK;
		}

		/// <summary>
		/// Sets the state of the option.
		/// </summary>
		/// <param name="option"> - option code to be set.</param>
		internal virtual void _setWantDont(int option)
		{
			_options[option] &= ~_REQUESTED_DO_MASK;
		}

		/// <summary>
		/// Processes a COMMAND.
		/// </summary>
		/// <param name="command"> - option code to be set.
		///  </param>
		internal virtual void _processCommand(int command)
		{
			if (_debugOptions == true)
			{
				Console.Error.WriteLine("RECEIVED COMMAND: " + command);
			}

			if (__notifhand != null)
			{
				__notifhand.ReceivedNegotiation(TelnetNotificationHandlers.RECEIVED_COMMAND, command);
			}
		}

		/// <summary>
		/// Processes a DO request.
		/// </summary>
		/// <param name="option"> - option code to be set. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void _processDo(int option) throws java.io.IOException
		internal virtual void _processDo(int option)
		{
			if (_debugOptions == true)
			{
				Console.Error.WriteLine("RECEIVED DO: " + TelnetOption.getOption(option));
			}

			if (__notifhand != null)
			{
				__notifhand.ReceivedNegotiation(TelnetNotificationHandlers.RECEIVED_DO, option);
			}

			bool acceptNewState = false;


			/* open TelnetOptionHandler functionality (start)*/
			if (_optionHandlers[option] != null)
			{
				acceptNewState = _optionHandlers[option].AcceptLocal;
			}
			else
			{
				/* open TelnetOptionHandler functionality (end)*/
				/* TERMINAL-TYPE option (start)*/
				if (option == TERMINAL_TYPE)
				{
					if ((!string.ReferenceEquals(_terminalType, null)) && (_terminalType.Length > 0))
					{
						acceptNewState = true;
					}
				}
				/* TERMINAL-TYPE option (end)*/
				/* open TelnetOptionHandler functionality (start)*/
			}
			/* open TelnetOptionHandler functionality (end)*/

			if (_willResponse[option] > 0)
			{
				--_willResponse[option];
				if (_willResponse[option] > 0 && _stateIsWill(option))
				{
					--_willResponse[option];
				}
			}

			if (_willResponse[option] == 0)
			{
				if (_requestedWont(option))
				{
					switch (option)
					{
						default:
							break;
					}

					if (acceptNewState)
					{
						_setWantWill(option);
						_sendWill(option);
					}
					else
					{
						++_willResponse[option];
						_sendWont(option);
					}
				}
				else
				{
					// Other end has acknowledged option.

					switch (option)
					{

						default:
							break;

					}

				}
			}

			_setWill(option);
		}

		/// <summary>
		/// Processes a DONT request.
		/// </summary>
		/// <param name="option"> - option code to be set. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void _processDont(int option) throws java.io.IOException
		internal virtual void _processDont(int option)
		{
			if (_debugOptions == true)
			{
				Console.Error.WriteLine("RECEIVED DONT: " + TelnetOption.getOption(option));
			}
			if (__notifhand != null)
			{
				__notifhand.ReceivedNegotiation(TelnetNotificationHandlers.RECEIVED_DONT, option);
			}
			if (_willResponse[option] > 0)
			{
				--_willResponse[option];
				if (_willResponse[option] > 0 && _stateIsWont(option))
				{
					--_willResponse[option];
				}
			}

			if (_willResponse[option] == 0 && _requestedWill(option))
			{

				switch (option)
				{

					default:
						break;

				}

				/* FIX for a BUG in the negotiation (start)*/
				if ((_stateIsWill(option)) || (_requestedWill(option)))
				{
					_sendWont(option);
				}

				_setWantWont(option);
				/* FIX for a BUG in the negotiation (end)*/
			}

			_setWont(option);
		}


		/// <summary>
		/// Processes a WILL request.
		/// </summary>
		/// <param name="option"> - option code to be set. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void _processWill(int option) throws java.io.IOException
		internal virtual void _processWill(int option)
		{
			if (_debugOptions == true)
			{
				Console.Error.WriteLine("RECEIVED WILL: " + TelnetOption.getOption(option));
			}

			if (__notifhand != null)
			{
				__notifhand.ReceivedNegotiation(TelnetNotificationHandlers.RECEIVED_WILL, option);
			}

			bool acceptNewState = false;

			/* open TelnetOptionHandler functionality (start)*/
			if (_optionHandlers[option] != null)
			{
				acceptNewState = _optionHandlers[option].AcceptRemote;
			}
			/* open TelnetOptionHandler functionality (end)*/

			if (_doResponse[option] > 0)
			{
				--_doResponse[option];
				if (_doResponse[option] > 0 && _stateIsDo(option))
				{
					--_doResponse[option];
				}
			}

			if (_doResponse[option] == 0 && _requestedDont(option))
			{

				switch (option)
				{

					default:
						break;

				}


				if (acceptNewState)
				{
					_setWantDo(option);
					_sendDo(option);
				}
				else
				{
					++_doResponse[option];
					_sendDont(option);
				}
			}

			_setDo(option);
		}

		/// <summary>
		/// Processes a WONT request.
		/// </summary>
		/// <param name="option"> - option code to be set. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void _processWont(int option) throws java.io.IOException
		internal virtual void _processWont(int option)
		{
			if (_debugOptions == true)
			{
				Console.Error.WriteLine("RECEIVED WONT: " + TelnetOption.getOption(option));
			}

			if (__notifhand != null)
			{
				__notifhand.ReceivedNegotiation(TelnetNotificationHandlers.RECEIVED_WONT, option);
			}

			if (_doResponse[option] > 0)
			{
				--_doResponse[option];
				if (_doResponse[option] > 0 && _stateIsDont(option))
				{
					--_doResponse[option];
				}
			}

			if (_doResponse[option] == 0 && _requestedDo(option))
			{

				switch (option)
				{

					default:
						break;

				}

				/* FIX for a BUG in the negotiation (start)*/
				if ((_stateIsDo(option)) || (_requestedDo(option)))
				{
					_sendDont(option);
				}

				_setWantDont(option);
				/* FIX for a BUG in the negotiation (end)*/
			}

			_setDont(option);
		}

		/* TERMINAL-TYPE option (start)*/
		/// <summary>
		/// Processes a suboption negotiation.
		/// </summary>
		/// <param name="suboption"> - subnegotiation data received </param>
		/// <param name="suboptionLength"> - length of data received </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void _processSuboption(int suboption[], int suboptionLength) throws java.io.IOException
		internal virtual void _processSuboption(int[] suboption, int suboptionLength)
		{
			if (_debug)
			{
				Console.Error.WriteLine("PROCESS SUBOPTION.");
			}

			/* open TelnetOptionHandler functionality (start)*/
			if (suboptionLength > 0)
			{
				if (_optionHandlers[suboption[0]] != null)
				{
					int[] responseSuboption = _optionHandlers[suboption[0]].AnswerSubnegotiation(suboption, suboptionLength);
					_sendSubnegotiation(responseSuboption);
				}
				else
				{
					if (suboptionLength > 1)
					{
						if (_debug)
						{
							for (int ii = 0; ii < suboptionLength; ii++)
							{
								Console.Error.WriteLine("SUB[" + ii + "]: " + suboption[ii]);
							}
						}
						if ((suboption[0] == TERMINAL_TYPE) && (suboption[1] == TERMINAL_TYPE_SEND))
						{
							_sendTerminalType();
						}
					}
				}
			}
			/* open TelnetOptionHandler functionality (end)*/
		}

		/// <summary>
		/// Sends terminal type information.
		/// </summary>
		/// <exception cref="IOException"> - Exception in I/O.</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _sendTerminalType() throws java.io.IOException
		internal void _sendTerminalType()
		{
			lock (this)
			{
				if (_debug)
				{
					Console.Error.WriteLine("SEND TERMINAL-TYPE: " + _terminalType);
				}
				if (!string.ReferenceEquals(_terminalType, null))
				{
					_output_.Write(_COMMAND_SB);
					_output_.Write(_COMMAND_IS);
					_output_.Write(_terminalType.GetBytes(Encoding));
					_output_.Write(_COMMAND_SE);
					_output_.Flush();
				}
			}
		}

		/* TERMINAL-TYPE option (end)*/

		/* open TelnetOptionHandler functionality (start)*/
		/// <summary>
		/// Manages subnegotiation for Terminal Type.
		/// </summary>
		/// <param name="subn"> - subnegotiation data to be sent </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _sendSubnegotiation(int subn[]) throws java.io.IOException
		internal void _sendSubnegotiation(int[] subn)
		{
			lock (this)
			{
				if (_debug)
				{
					Console.Error.WriteLine("SEND SUBNEGOTIATION: ");
					if (subn != null)
					{
						Console.Error.WriteLine(subn.ToString());
					}
				}
				if (subn != null)
				{
					_output_.Write(_COMMAND_SB);
					// Note _output_ is buffered, so might as well simplify by writing single bytes
					foreach (int element in subn)
					{
						byte b = (byte)element;
						if (b == (byte)TelnetCommands.IAC)
						{ // cast is necessary because IAC is outside the signed byte range
							_output_.Write(b); // double any IAC bytes
						}
						_output_.Write(b);
					}
					_output_.Write(_COMMAND_SE);

					/* Code Section added for sending the negotiation ASAP (start)*/
					_output_.Flush();
					/* Code Section added for sending the negotiation ASAP (end)*/
				}
			}
		}
		/* open TelnetOptionHandler functionality (end)*/

		/// <summary>
		/// Sends a command, automatically adds IAC prefix and flushes the output.
		/// </summary>
		/// <param name="cmd"> - command data to be sent </param>
		/// <exception cref="IOException"> - Exception in I/O.
		/// @since 3.0 </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _sendCommand(byte cmd) throws java.io.IOException
		internal void _sendCommand(byte cmd)
		{
			lock (this)
			{
				_output_.Write((byte)TelnetCommands.IAC);
				_output_.Write(cmd);
				_output_.Flush();
			}
		}

		/* Code Section added for supporting AYT (start)*/
		/// <summary>
		/// Processes the response of an AYT
		/// </summary>
		internal void _processAYTResponse()
		{
			lock (this)
			{
				if (!aytFlag)
				{
					lock (aytMonitor)
					{
						aytFlag = true;
						Monitor.PulseAll(aytMonitor);
					}
				}
			}
		}
		/* Code Section added for supporting AYT (end)*/


		public new virtual void Connect(string hostname, int port)
		{
			base.Connect(hostname, port);
			_connectAction_();
		}

		/// <summary>
		/// Called upon connection.
		/// </summary>
		/// <exception cref="IOException"> - Exception in I/O.</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: @Override protected void _connectAction_() throws java.io.IOException
		internal virtual void _connectAction_()
		{
			/* (start). BUGFIX: clean the option info for each connection*/
			for (int ii = 0; ii < TelnetOption.MAX_OPTION_VALUE + 1; ii++)
			{
				_doResponse[ii] = 0;
				_willResponse[ii] = 0;
				_options[ii] = 0;

				if (_optionHandlers[ii] != null)
				{
					_optionHandlers[ii].Do = false;
					_optionHandlers[ii].Will = false;
				}
			}
			/* (end). BUGFIX: clean the option info for each connection*/

			// TODO: Connect 내용 채워야 한다.
			// by DEV\xyz37(김기원) in 2018년 6월 18일 월요일 21:19
			//base.Connect();

			_input_ = base.GetStream();
			_output_ = base.GetStream();

			/* open TelnetOptionHandler functionality (start)*/
			for (int ii = 0; ii < TelnetOption.MAX_OPTION_VALUE + 1; ii++)
			{
				if (_optionHandlers[ii] != null)
				{
					if (_optionHandlers[ii].InitLocal)
					{
						_requestWill(_optionHandlers[ii].OptionCode);
					}

					if (_optionHandlers[ii].InitRemote)
					{
						_requestDo(_optionHandlers[ii].OptionCode);
					}
				}
			}
			/* open TelnetOptionHandler functionality (end)*/
		}

		/// <summary>
		/// Sends a DO.
		/// </summary>
		/// <param name="option"> - Option code. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _sendDo(int option) throws java.io.IOException
		internal void _sendDo(int option)
		{
			lock (this)
			{
				if (_debug || _debugOptions)
				{
					Console.Error.WriteLine("DO: " + TelnetOption.getOption(option));
				}
				_output_.Write(_COMMAND_DO);
				_output_.Write(option);

				/* Code Section added for sending the negotiation ASAP (start)*/
				_output_.Flush();
				/* Code Section added for sending the negotiation ASAP (end)*/
			}
		}

		/// <summary>
		/// Requests a DO.
		/// </summary>
		/// <param name="option"> - Option code. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _requestDo(int option) throws java.io.IOException
		internal void _requestDo(int option)
		{
			lock (this)
			{
				if ((_doResponse[option] == 0 && _stateIsDo(option)) || _requestedDo(option))
				{
					return;
				}
				_setWantDo(option);
				++_doResponse[option];
				_sendDo(option);
			}
		}

		/// <summary>
		/// Sends a DONT.
		/// </summary>
		/// <param name="option"> - Option code. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _sendDont(int option) throws java.io.IOException
		internal void _sendDont(int option)
		{
			lock (this)
			{
				if (_debug || _debugOptions)
				{
					Console.Error.WriteLine("DONT: " + TelnetOption.getOption(option));
				}
				_output_.Write(_COMMAND_DONT);
				_output_.Write(option);

				/* Code Section added for sending the negotiation ASAP (start)*/
				_output_.Flush();
				/* Code Section added for sending the negotiation ASAP (end)*/
			}
		}

		/// <summary>
		/// Requests a DONT.
		/// </summary>
		/// <param name="option"> - Option code. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _requestDont(int option) throws java.io.IOException
		internal void _requestDont(int option)
		{
			lock (this)
			{
				if ((_doResponse[option] == 0 && _stateIsDont(option)) || _requestedDont(option))
				{
					return;
				}
				_setWantDont(option);
				++_doResponse[option];
				_sendDont(option);
			}
		}

		/// <summary>
		/// Sends a WILL.
		/// </summary>
		/// <param name="option"> - Option code. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _sendWill(int option) throws java.io.IOException
		internal void _sendWill(int option)
		{
			lock (this)
			{
				if (_debug || _debugOptions)
				{
					Console.Error.WriteLine("WILL: " + TelnetOption.getOption(option));
				}
				_output_.Write(_COMMAND_WILL);
				_output_.Write(option);

				/* Code Section added for sending the negotiation ASAP (start)*/
				_output_.Flush();
				/* Code Section added for sending the negotiation ASAP (end)*/
			}
		}

		/// <summary>
		/// Requests a WILL.
		/// </summary>
		/// <param name="option"> - Option code. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _requestWill(int option) throws java.io.IOException
		internal void _requestWill(int option)
		{
			lock (this)
			{
				if ((_willResponse[option] == 0 && _stateIsWill(option)) || _requestedWill(option))
				{
					return;
				}
				_setWantWill(option);
				++_doResponse[option];
				_sendWill(option);
			}
		}

		/// <summary>
		/// Sends a WONT.
		/// </summary>
		/// <param name="option"> - Option code. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _sendWont(int option) throws java.io.IOException
		internal void _sendWont(int option)
		{
			lock (this)
			{
				if (_debug || _debugOptions)
				{
					Console.Error.WriteLine("WONT: " + TelnetOption.getOption(option));
				}
				_output_.Write(_COMMAND_WONT);
				_output_.Write(option);

				/* Code Section added for sending the negotiation ASAP (start)*/
				_output_.Flush();
				/* Code Section added for sending the negotiation ASAP (end)*/
			}
		}

		/// <summary>
		/// Requests a WONT.
		/// </summary>
		/// <param name="option"> - Option code. </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _requestWont(int option) throws java.io.IOException
		internal void _requestWont(int option)
		{
			lock (this)
			{
				if ((_willResponse[option] == 0 && _stateIsWont(option)) || _requestedWont(option))
				{
					return;
				}
				_setWantWont(option);
				++_doResponse[option];
				_sendWont(option);
			}
		}

		/// <summary>
		/// Sends a byte.
		/// </summary>
		/// <param name="b"> - byte to send </param>
		/// <exception cref="IOException"> - Exception in I/O.
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final synchronized void _sendByte(int b) throws java.io.IOException
		internal void _sendByte(byte b)
		{
			lock (this)
			{
				_output_.Write(b);

				/* Code Section added for supporting spystreams (start)*/
				_spyWrite(b);
				/* Code Section added for supporting spystreams (end)*/
			}
		}

		internal void _sendByte(char b)
		{
			lock (this)
			{
				_output_.Write(b);

				/* Code Section added for supporting spystreams (start)*/
				_spyWrite(b);
				/* Code Section added for supporting spystreams (end)*/
			}
		}

		internal void _sendByte(int b)
		{
			lock (this)
			{
				_output_.Write(b);

				/* Code Section added for supporting spystreams (start)*/
				_spyWrite(b);
				/* Code Section added for supporting spystreams (end)*/
			}
		}

		/* Code Section added for supporting AYT (start)*/
		/// <summary>
		/// Sends an Are You There sequence and waits for the result.
		/// </summary>
		/// <param name="timeout"> - Time to wait for a response (millis.) </param>
		/// <exception cref="IOException"> - Exception in I/O. </exception>
		/// <exception cref="IllegalArgumentException"> - Illegal argument </exception>
		/// <exception cref="InterruptedException"> - Interrupted during wait. </exception>
		/// <returns> true if AYT received a response, false otherwise
		///  </returns>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: final boolean _sendAYT(long timeout) throws java.io.IOException, IllegalArgumentException, InterruptedException
		internal bool _sendAYT(long timeout)
		{
			bool retValue = false;
			lock (aytMonitor)
			{
				lock (this)
				{
					aytFlag = false;
					_output_.Write(_COMMAND_AYT);
					_output_.Flush();
				}

				Monitor.Wait(aytMonitor, TimeSpan.FromMilliseconds(timeout));

				if (aytFlag == false)
				{
					retValue = false;
					aytFlag = true;
				}
				else
				{
					retValue = true;
				}
			}

			return (retValue);
		}
		/* Code Section added for supporting AYT (end)*/

		/* open TelnetOptionHandler functionality (start)*/

		/// <summary>
		/// Registers a new TelnetOptionHandler for this telnet  to use.
		/// </summary>
		/// <param name="opthand"> - option handler to be registered. </param>
		/// <exception cref="InvalidTelnetOptionException"> - The option code is invalid. </exception>
		/// <exception cref="IOException"> on error
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void addOptionHandler(TelnetOptionHandler opthand) throws InvalidTelnetOptionException, java.io.IOException
		public virtual void AddOptionHandler(TelnetOptionHandler opthand)
		{
			int optcode = opthand.OptionCode;

			if (TelnetOption.isValidOption(optcode))
			{
				if (_optionHandlers[optcode] == null)
				{
					_optionHandlers[optcode] = opthand;

					if (Connected == true)
					{
						if (opthand.InitLocal)
						{
							_requestWill(optcode);
						}

						if (opthand.InitRemote)
						{
							_requestDo(optcode);
						}
					}
				}
				else
				{
					throw (new InvalidTelnetOptionException("Already registered option", optcode));
				}
			}
			else
			{
				throw (new InvalidTelnetOptionException("Invalid Option Code", optcode));
			}
		}

		/// <summary>
		/// Unregisters a  TelnetOptionHandler.
		/// </summary>
		/// <param name="optcode"> - Code of the option to be unregistered. </param>
		/// <exception cref="InvalidTelnetOptionException"> - The option code is invalid. </exception>
		/// <exception cref="IOException"> on error
		///  </exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: void deleteOptionHandler(int optcode) throws InvalidTelnetOptionException, java.io.IOException
		public virtual void DeleteOptionHandler(int optcode)
		{
			if (TelnetOption.isValidOption(optcode))
			{
				if (_optionHandlers[optcode] == null)
				{
					throw (new InvalidTelnetOptionException("Unregistered option", optcode));
				}
				else
				{
					TelnetOptionHandler opthand = _optionHandlers[optcode];
					_optionHandlers[optcode] = null;

					if (opthand.Will)
					{
						_requestWont(optcode);
					}

					if (opthand.Do)
					{
						_requestDont(optcode);
					}
				}
			}
			else
			{
				throw (new InvalidTelnetOptionException("Invalid Option Code", optcode));
			}
		}
		/* open TelnetOptionHandler functionality (end)*/

		/* Code Section added for supporting spystreams (start)*/
		/// <summary>
		/// Registers an OutputStream for spying what's going on in
		/// the Telnet session.
		/// </summary>
		/// <param name="spystream"> - OutputStream on which session activity
		/// will be echoed.</param>
		internal virtual void _registerSpyStream(System.IO.Stream spystream)
		{
			spyStream = spystream;
		}

		/// <summary>
		/// Stops spying this Telnet.
		/// 
		/// </summary>
		internal virtual void _stopSpyStream()
		{
			spyStream = null;
		}

		/// <summary>
		/// Sends a read char on the spy stream.
		/// </summary>
		/// <param name="ch"> - character read from the session</param>
		internal virtual void _spyRead(int ch)
		{
			System.IO.Stream spy = spyStream;
			if (spy != null)
			{
				try
				{
					if (ch != '\r') // never write '\r' on its own
					{
						if (ch == '\n')
						{
							spy.WriteByte((byte)'\r'); // add '\r' before '\n'
						}
						spy.WriteByte((byte)ch); // write original character
						spy.Flush();
					}
				}
				catch (IOException)
				{
					spyStream = null;
				}
			}
		}

		/// <summary>
		/// Sends a written char on the spy stream.
		/// </summary>
		/// <param name="ch"> - character written to the session</param>
		internal virtual void _spyWrite(int ch)
		{
			if (!(_stateIsDo(TelnetOption.ECHO) && _requestedDo(TelnetOption.ECHO)))
			{
				System.IO.Stream spy = spyStream;
				if (spy != null)
				{
					try
					{
						spy.WriteByte((byte)ch);
						spy.Flush();
					}
					catch (IOException)
					{
						spyStream = null;
					}
				}
			}
		}
		/* Code Section added for supporting spystreams (end)*/

		/// <summary>
		/// Registers a notification handler to which will be sent
		/// notifications of received telnet option negotiation commands.
		/// </summary>
		/// <param name="notifhand"> - TelnetNotificationHandler to be registered</param>
		public virtual void registerNotifHandler(ITelnetNotificationHandler notifhand)
		{
			__notifhand = notifhand;
		}

		/// <summary>
		/// Unregisters the current notification handler.
		/// 
		/// </summary>
		public virtual void unregisterNotifHandler()
		{
			__notifhand = null;
		}
	}
}
