﻿/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace GS.Common.Net.Telnet
{
	/// <summary>
	/// Implements the telnet terminal type option RFC 1091.
	/// </summary>
	public class TerminalTypeOptionHandler : TelnetOptionHandler
	{
		/// <summary>
		/// Terminal type
		/// </summary>
		private readonly string termType;

		/// <summary>
		/// Terminal type option
		/// </summary>
		protected internal const int TERMINAL_TYPE = 24;

		/// <summary>
		/// Send (for subnegotiation)
		/// </summary>
		protected internal const int TERMINAL_TYPE_SEND = 1;

		/// <summary>
		/// Is (for subnegotiation)
		/// </summary>
		protected internal const int TERMINAL_TYPE_IS = 0;

		/// <summary>
		/// Constructor for the TerminalTypeOptionHandler. Allows defining desired
		/// initial setting for local/remote activation of this option and
		/// behaviour in case a local/remote activation request for this
		/// option is received.
		/// </summary>
		/// <param name="termtype"> - terminal type that will be negotiated. </param>
		/// <param name="initlocal"> - if set to true, a WILL is sent upon connection. </param>
		/// <param name="initremote"> - if set to true, a DO is sent upon connection. </param>
		/// <param name="acceptlocal"> - if set to true, any DO request is accepted. </param>
		/// <param name="acceptremote"> - if set to true, any WILL request is accepted.</param>
		public TerminalTypeOptionHandler(string termtype, bool initlocal, bool initremote, bool acceptlocal, bool acceptremote)
			: base(TelnetOption.TERMINAL_TYPE, initlocal, initremote, acceptlocal, acceptremote)
		{
			termType = termtype;
		}

		/// <summary>
		/// Constructor for the TerminalTypeOptionHandler. Initial and accept
		/// behaviour flags are set to false
		/// </summary>
		/// <param name="termtype"> - terminal type that will be negotiated.</param>
		public TerminalTypeOptionHandler(string termtype)
			: base(TelnetOption.TERMINAL_TYPE, false, false, false, false)
		{
			termType = termtype;
		}

		/// <summary>
		/// Implements the abstract method of TelnetOptionHandler.
		/// </summary>
		/// <param name="suboptionData"> - the sequence received, without IAC SB &amp; IAC SE </param>
		/// <param name="suboptionLength"> - the length of data in suboption_data
		/// </param>
		/// <returns> terminal type information</returns>
		public override int[] AnswerSubnegotiation(int[] suboptionData, int suboptionLength)
		{
			if ((suboptionData != null) && (suboptionLength > 1) && (!string.ReferenceEquals(termType, null)))
			{
				if ((suboptionData[0] == TERMINAL_TYPE) && (suboptionData[1] == TERMINAL_TYPE_SEND))
				{
					int[] response = new int[termType.Length + 2];

					response[0] = TERMINAL_TYPE;
					response[1] = TERMINAL_TYPE_IS;

					for (int ii = 0; ii < termType.Length; ii++)
					{
						response[ii + 2] = termType[ii];
					}

					return response;
				}
			}
			return null;
		}
	}
}
