﻿//========================================================================
// This conversion was produced by the Free Edition of
// Java to C# Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================

using GS.Common.Net.Telnet;
using System;
using System.Collections.Generic;
using System.Threading;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace examples.telnet
{
	/// <summary>
	/// This is a simple example of use of TelnetClient.
	/// An external option handler (SimpleTelnetOptionHandler) is used.
	/// Initial configuration requested by TelnetClient will be:
	/// WILL ECHO, WILL SUPPRESS-GA, DO SUPPRESS-GA.
	/// VT100 terminal type will be subnegotiated.
	/// <para>
	/// Also, use of the sendAYT(), getLocalOptionState(), getRemoteOptionState()
	/// is demonstrated.
	/// When connected, type AYT to send an AYT command to the server and see
	/// the result.
	/// Type OPT to see a report of the state of the first 25 options.
	/// </para>
	/// </summary>
	public class TelnetClientExample : ITelnetNotificationHandler
	{
		internal static TelnetClient tc = null;

		/// <summary>
		/// Main for the TelnetClientExample. </summary>
		/// <param name="args"> input params </param>
		/// <exception cref="Exception"> on error</exception>
		//JAVA TO C# CONVERTER WARNING: Method 'throws' clauses are not available in .NET:
		//ORIGINAL LINE: public static void main(String[] args) throws Exception
		public static void Main(string[] args)
		{
			System.IO.FileStream fout = null;

			//if (args.Length < 1)
			//{
			//	Console.Error.WriteLine("Usage: TelnetClientExample <remote-ip> [<remote-port>]");
			//	Environment.Exit(1);
			//}

			args = new string[] { "x10.synology.me", "923" };

			string remoteip = args[0];

			int remoteport;

			if (args.Length > 1)
			{
				remoteport = (Convert.ToInt32(args[1]));
			}
			else
			{
				remoteport = 23;
			}

			try
			{
				fout = new System.IO.FileStream("spy.log", System.IO.FileMode.Append);
			}
			catch (Exception e)
			{
				Console.Error.WriteLine("Exception while opening the spy file: " + e.Message);
			}

			tc = new TelnetClient();

			TerminalTypeOptionHandler ttopt = new TerminalTypeOptionHandler("VT100", false, false, true, false);
			EchoOptionHandler echoopt = new EchoOptionHandler(true, false, true, false);
			SuppressGAOptionHandler gaopt = new SuppressGAOptionHandler(true, true, true, true);

			try
			{
				tc.AddOptionHandler(ttopt);
				tc.AddOptionHandler(echoopt);
				tc.AddOptionHandler(gaopt);
			}
			catch (InvalidTelnetOptionException e)
			{
				Console.Error.WriteLine("Error registering option handlers: " + e.Message);
			}

			while (true)
			{
				bool end_loop = false;
				try
				{
					tc.Connect(remoteip, remoteport);

					//Thread reader = new Thread(new TelnetClientExample());
					tc.registerNotifHandler(new TelnetClientExample());
					Console.WriteLine("TelnetClientExample");
					Console.WriteLine("Type AYT to send an AYT telnet command");
					Console.WriteLine("Type OPT to print a report of status of options (0-24)");
					Console.WriteLine("Type REGISTER to register a new SimpleOptionHandler");
					Console.WriteLine("Type UNREGISTER to unregister an OptionHandler");
					Console.WriteLine("Type SPY to register the spy (connect to port 3333 to spy)");
					Console.WriteLine("Type UNSPY to stop spying the connection");
					Console.WriteLine("Type ^[A-Z] to send the control character; use ^^ to send ^");

					//reader.Start();
					System.IO.Stream outstr = tc.OutputStream;

					byte[] buff = new byte[1024];
					int ret_read = 0;

					do
					{
						try
						{
							// ret_read  = Console.Read();
							//if (ret_read > 0)

							var line = Console.ReadLine();

							if (line != string.Empty)
							{
								//JAVA TO C# CONVERTER WARNING: The original Java variable was marked 'final':
								//ORIGINAL LINE: final String line = new String(buff, 0, ret_read);
								if (line.StartsWith("AYT", StringComparison.OrdinalIgnoreCase))
								{
									try
									{
										Console.WriteLine("Sending AYT");

										Console.WriteLine("AYT response:" + tc.sendAYT(5000));
									}
									catch (Exception e)
									{
										Console.Error.WriteLine("Exception waiting AYT response: " + e.Message);
									}
								}
								else if (line.StartsWith("OPT", StringComparison.Ordinal))
								{
									Console.WriteLine("Status of options:");
									for (int ii = 0; ii < 25; ii++)
									{
										Console.WriteLine("Local Option " + ii + ":" + tc.getLocalOptionState(ii) + " Remote Option " + ii + ":" + tc.getRemoteOptionState(ii));
									}
								}
								else if (line.StartsWith("REGISTER", StringComparison.Ordinal))
								{
									StringTokenizer st = new StringTokenizer(StringHelper.NewString(buff));
									try
									{
										st.NextToken();
										int opcode = int.Parse(st.NextToken());
										bool initlocal = bool.Parse(st.NextToken());
										bool initremote = bool.Parse(st.NextToken());
										bool acceptlocal = bool.Parse(st.NextToken());
										bool acceptremote = bool.Parse(st.NextToken());
										SimpleOptionHandler opthand = new SimpleOptionHandler(opcode, initlocal, initremote, acceptlocal, acceptremote);
										tc.AddOptionHandler(opthand);
									}
									catch (Exception e)
									{
										if (e is InvalidTelnetOptionException)
										{
											Console.Error.WriteLine("Error registering option: " + e.Message);
										}
										else
										{
											Console.Error.WriteLine("Invalid REGISTER command.");
											Console.Error.WriteLine("Use REGISTER optcode initlocal initremote acceptlocal acceptremote");
											Console.Error.WriteLine("(optcode is an integer.)");
											Console.Error.WriteLine("(initlocal, initremote, acceptlocal, acceptremote are boolean)");
										}
									}
								}
								else if (line.StartsWith("UNREGISTER", StringComparison.Ordinal))
								{
									//StringTokenizer st = new StringTokenizer(StringHelper.NewString(buff));

									try
									{
										//st.NextToken();
										//int opcode = (new int?(st.NextToken()));
										//tc.DeleteOptionHandler(opcode);
									}
									catch (Exception e)
									{
										if (e is InvalidTelnetOptionException)
										{
											Console.Error.WriteLine("Error unregistering option: " + e.Message);
										}
										else
										{
											Console.Error.WriteLine("Invalid UNREGISTER command.");
											Console.Error.WriteLine("Use UNREGISTER optcode");
											Console.Error.WriteLine("(optcode is an integer)");
										}
									}
								}
								else if (line.StartsWith("SPY", StringComparison.Ordinal))
								{
									tc.RegisterSpyStream(fout);
								}
								else if (line.StartsWith("UNSPY", StringComparison.Ordinal))
								{
									tc.stopSpyStream();
								}
								//else if (line.matches("^\\^[A-Z^]\\r?\\n?$"))
								//{
								//	byte toSend = buff[1];

								//	if (toSend == (byte)'^')
								//	{
								//		outstr.WriteByte(toSend);
								//	}
								//	else
								//	{
								//		outstr.WriteByte(toSend - 'A' + 1);
								//	}
								//	outstr.Flush();
								//}
								else
								{
									try
									{
										outstr.Write(buff, 0, ret_read);
										outstr.Flush();
									}
									catch (Exception)
									{
										end_loop = true;
									}
								}
							}
						}
						catch (Exception e)
						{
							Console.Error.WriteLine("Exception while reading keyboard:" + e.Message);
							end_loop = true;
						}
					} while ((ret_read > 0) && (end_loop == false));

					try
					{
						tc.Disconnect();
					}
					catch (Exception e)
					{
						Console.Error.WriteLine("Exception while connecting:" + e.Message);
					}
				}
				catch (Exception e)
				{
					Console.Error.WriteLine("Exception while connecting:" + e.Message);
					Environment.Exit(1);
				}
			}
		}


		/// <summary>
		/// Callback method called when TelnetClient receives an option
		/// negotiation command.
		/// </summary>
		/// <param name="negotiation_code"> - type of negotiation command received
		/// (RECEIVED_DO, RECEIVED_DONT, RECEIVED_WILL, RECEIVED_WONT, RECEIVED_COMMAND) </param>
		/// <param name="option_code"> - code of the option negotiated</param>
		public void ReceivedNegotiation(TelnetNotificationHandlers negotiation_code, int option_code)
		{
			string command = null;

			switch ((TelnetNotificationHandlers)negotiation_code)
			{
				case TelnetNotificationHandlers.RECEIVED_DO:
					command = "DO";

					break;
				case TelnetNotificationHandlers.RECEIVED_DONT:
					command = "DONT";

					break;
				case TelnetNotificationHandlers.RECEIVED_WILL:
					command = "WILL";

					break;
				case TelnetNotificationHandlers.RECEIVED_WONT:
					command = "WONT";

					break;
				case TelnetNotificationHandlers.RECEIVED_COMMAND:
					command = "COMMAND";

					break;
				default:
					command = Convert.ToString(negotiation_code); // Should not happen
					break;
			}

			Console.WriteLine("Received " + command + " for option code " + option_code);
		}

		/// <summary>
		/// Reader thread.
		/// Reads lines from the TelnetClient and echoes them
		/// on the screen.
		/// </summary>
		public void run()
		{
			System.IO.Stream instr = tc.InputStream;

			try
			{
				byte[] buff = new byte[1024];
				int ret_read = 0;

				do
				{
					ret_read = instr.Read(buff, 0, buff.Length);
					if (ret_read > 0)
					{
						Console.Write(StringHelper.NewString(buff, 0, ret_read));
					}
				} while (ret_read >= 0);
			}
			catch (Exception e)
			{
				Console.Error.WriteLine("Exception while reading socket:" + e.Message);
			}

			try
			{
				tc.Disconnect();
			}
			catch (Exception e)
			{
				Console.Error.WriteLine("Exception while closing telnet:" + e.Message);
			}
		}
	}
}
