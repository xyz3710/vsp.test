using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;

namespace FTClient
{
	public class FTClient
	{
		private const int PORT = 1980;
		private const int SEGMENT_SIZE = 1024;
		
		private string _fileName;
		private IPAddress _serverIPAddress;

		/// <summary>
		/// FTClient class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public FTClient()
		{
			CurrentMessage = "Idle...";

			if (_serverIPAddress == null)
			{
				IPAddress[] ipAddress = Dns.GetHostAddresses(Dns.GetHostName());

				if (ipAddress.Length > 0)
					_serverIPAddress = ipAddress[0];
			}
		}

		public FTClient(string ipAddress)
			: this ()
		{
			_serverIPAddress = IPAddress.Parse(ipAddress);
		}

		#region Properties
		/// <summary>
		/// CurrentMessage를 구하거나 설정합니다.
		/// </summary>
		public string CurrentMessage
		{
			set
			{
				Console.WriteLine(value);				
			}
		}
        
        /// <summary>
        /// FileName를 구하거나 설정합니다.
        /// </summary>
		public string FileName
        {
        	get
        	{
				return _fileName.Replace("\\", "/");
        	}
			private set
        	{
        		_fileName = value;
        	}
        }
        
        #endregion
        
		public void SendFile(string fileName)
		{
			Socket clientSocket = null;
			FileName = fileName;
			FileStream fs = null;

			try
			{
				IPEndPoint ep = new IPEndPoint(_serverIPAddress, PORT);

				clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

				string filePath = string.Empty;

				#region For Test
				/*
				while (FileName.IndexOf("/") > -1)
				{
					filePath += Path.GetDirectoryName(FileName);
					FileName = Path.GetFileName(FileName);
				}
				*/
				#endregion

				FileInfo fileInfo = new FileInfo(fileName);
				long fileSize = fileInfo.Length;
				int bufferSize = SEGMENT_SIZE * SEGMENT_SIZE;

				CurrentMessage = "Connection to server...";

				clientSocket.Connect(ep);
				
				// 2차 파일 데이터를 bufferSize 만큼 전송 한다.
				long totalSendingLen = 0L;
				long totalReadingLen = 0L;
				int readingLen = 0;
				int sendingLen = 0;
				byte[] sendingData = new byte[bufferSize];

				CurrentMessage = "File reading...";

				fs = File.OpenRead(fileName);

				// 전체 크기 보다 bufferSize가 크면 기본 bufferSize를 줄인다.
				bufferSize = (int)Math.Min(fileSize, bufferSize);

				DateTime startTime = DateTime.Now;
				Console.WriteLine(startTime.ToLongTimeString());

				List<byte> readAllData = new List<byte>();

				// 데이터를 읽어들인다.
				while ((readingLen = fs.Read(sendingData, 0, bufferSize)) > 0)
				{
					readAllData.AddRange(sendingData);
					totalReadingLen += readingLen;

					bufferSize = Math.Min(bufferSize, (int)(fileSize - totalReadingLen));
				}

				// 버퍼 이상의 데이터는 지운다.
				readAllData.RemoveRange((int)totalReadingLen, (int)(readAllData.Count - totalReadingLen));

				long afterCompressLength = 0L;
				byte[] targetData = null;

				// Compress
				long beforeCompressLength = readAllData.Count;

				if (totalReadingLen > SEGMENT_SIZE)
				{
					#region 압축
					using (MemoryStream compressedMS = new MemoryStream())
					{
						GZipStream gZip = new GZipStream(compressedMS, CompressionMode.Compress);

						gZip.Write(readAllData.ToArray(), 0, (int)beforeCompressLength);
						gZip.Flush();
						gZip.Close();

						targetData = compressedMS.ToArray();
						compressedMS.Close();

						CurrentMessage = "Compressed.";
					}
					#endregion

					afterCompressLength = targetData.Length;
				}
				else
					targetData = readAllData.ToArray();

				#region Header Sending
				byte[] fileNameByte = Encoding.Unicode.GetBytes(Path.GetFileName(FileName));
				byte[] headerData = new byte[sizeof(long) + fileNameByte.Length + sizeof(long) * 2];
				byte[] fileDataLen = BitConverter.GetBytes(fileSize);
				byte[] compressedLength = BitConverter.GetBytes(afterCompressLength);
				byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);

				fileDataLen.CopyTo(headerData, 0);																	// 데이터 길이(8byte)
				compressedLength.CopyTo(headerData, fileDataLen.Length);											// 압축된 file 크기(8byte)
				fileNameLen.CopyTo(headerData, fileDataLen.Length + compressedLength.Length);						// File 이름 길이(4byte)
				fileNameByte.CopyTo(headerData, fileDataLen.Length + compressedLength.Length + fileNameLen.Length);	// 파일 이름 가변적

				CurrentMessage = "Sending Header...";

				// 1차 File 길이, 파일 이름 길이, 파일 이름을 전송한다.
				int result = clientSocket.Send(headerData);
				#endregion

				bufferSize = (int)Math.Min(afterCompressLength, SEGMENT_SIZE * SEGMENT_SIZE);

				CurrentMessage = "Sending Body...";

				// 데이터를 전송한다.
				while ((sendingLen = clientSocket.Send(targetData, sendingLen, bufferSize, SocketFlags.Partial)) > 0)
				{
					totalSendingLen += sendingLen;

					Console.Write("\rTotal / Current : {0:#,##0} / {1:#,##0}", afterCompressLength, totalSendingLen);

					// 기본 bufferSize 보다 남은 크기가 크면 기본 bufferSize는 (전체 크기 - 읽은 총합) 이다.
					bufferSize = Math.Min(bufferSize, (int)(fileSize - totalReadingLen));
				}

				Console.WriteLine("\r\n{0}", DateTime.Now - startTime);

				CurrentMessage = "Disconnecting...";
				CurrentMessage = "File transferred";
			}
			catch (SocketException ex)
			{
				CurrentMessage = string.Format("File sending fail. {0}", ex.Message);
			}
			finally
			{
				if (fs != null)
					fs.Close();

				if (clientSocket != null)
					clientSocket.Close();
			}
		}
	}
}
