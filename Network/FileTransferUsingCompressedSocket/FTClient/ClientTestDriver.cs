﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FTClient
{
	class ClientTestDriver
	{
		static void Main(string[] args)
		{
			FTClient fTClient = null;
			string fileName = @"F:\plugins.zip";

			if (args.Length == 2)
			{
				fTClient = new FTClient(args[0]);
				fileName = args[1];
			}
			else
			{
				fTClient = new FTClient();
				Console.WriteLine("FTClient.exe [Server IpAddress] [Filename]");
			}

			fTClient.SendFile(fileName);
		}
	}
}
