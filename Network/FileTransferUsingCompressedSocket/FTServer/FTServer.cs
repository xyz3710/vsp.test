using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;

namespace FTServer
{
	public class FTServer
	{
		private const int PORT = 1980;
		private const int BackLog = 100;
		private const int SEGMENT_SIZE = 1024;
		private IPEndPoint _iPEndPoint;
		private Socket _socket;
		private string _receivedPath;
		
		/// <summary>
		/// FTServer class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public FTServer()
		{
			CurrentMessage = "Stopped...";
			_iPEndPoint = new IPEndPoint(IPAddress.Any, PORT);
			_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
			_socket.Bind(_iPEndPoint);
		}

		#region Properties
        /// <summary>
        /// ReceivedPath를 구하거나 설정합니다.
        /// </summary>
        public string ReceivedPath
        {
        	get
        	{
        		return _receivedPath;
        	}
        	set
        	{
        		_receivedPath = value;
        	}
        }
        
        /// <summary>
        /// CurrentMessage를 구하거나 설정합니다.
        /// </summary>
		public string CurrentMessage
        {
        	set
        	{
				Console.WriteLine(value);
        	}
        }
        
        #endregion

		public void StartServer(bool appendable)
		{
			Socket clientSocket = null;

			try
			{
				CurrentMessage = "Starting...";
				_socket.Listen(BackLog);

				CurrentMessage = "Running and waiting to receive file.";

				clientSocket = _socket.Accept();

				int headerSize = sizeof(long) * 2 + sizeof(int);
				byte[] headerData = new byte[headerSize];

				int receivedLength = clientSocket.Receive(headerData);

				CurrentMessage = "Receiving data...";

				// Header 정보를 읽어 들인다.
				long dataLength = BitConverter.ToInt64(headerData, 0);							// data 길이
				long compressedLength = BitConverter.ToInt64(headerData, sizeof(long));			// 압축 길이
				int fileNameLen = BitConverter.ToInt32(headerData, sizeof(long) * 2);
				byte[] fileNameData = new byte[fileNameLen];
				bool isCompressed = compressedLength > 0;

				// File 이름을 읽어 들인다.				
				receivedLength = clientSocket.Receive(fileNameData);

				string fileName = Encoding.Unicode.GetString(fileNameData, 0, fileNameLen);
				string filePath = string.Format(@"{0}/{1}", ReceivedPath, fileName);

				int bufferSize = SEGMENT_SIZE * SEGMENT_SIZE;
				long totalReceived = 0L;
				long targetLength = isCompressed == true ? compressedLength : dataLength;

				bufferSize = (int)Math.Min(bufferSize, targetLength);
				
				DateTime startTime = DateTime.Now;
				Console.WriteLine(startTime.ToLongTimeString());

				List<byte> readAllData = new List<byte>();
				byte[] clientData = new byte[bufferSize];
				byte[] targetData = null;

				receivedLength = 0;

				// 데이터를 읽어들인다.
				while ((receivedLength = clientSocket.Receive(clientData, receivedLength, bufferSize, SocketFlags.Partial)) > 0)
				{
					targetData = new byte[receivedLength];

					// 읽은 데이터 만큼 자른다.
					Array.Copy(clientData, targetData, receivedLength);
					readAllData.AddRange(targetData);
					totalReceived += receivedLength;
					
					Console.Write("\rTotal / Current : {0:#,##0} / {1:#,##0}", targetLength, totalReceived);

					bufferSize = (int)Math.Min(bufferSize, targetLength - totalReceived);
				}

				if (appendable == false && File.Exists(filePath) == true)
					File.Delete(filePath);

				using (MemoryStream readMS = new MemoryStream(readAllData.ToArray()))
				{
					if (isCompressed == true)
					{
						CurrentMessage = string.Format("Compressed : original : {0}, compressed : {1}", dataLength, compressedLength);

						using (GZipStream gZipD = new GZipStream(readMS, CompressionMode.Decompress))
						{
							byte[] data = new byte[dataLength];

							bufferSize = (int)Math.Min(totalReceived, SEGMENT_SIZE * SEGMENT_SIZE);

							int readLength = gZipD.Read(data, 0, (int)dataLength);
							//int readLength = SaveFile(gZipD, bufferSize, filePath, dataLength);
							// 저장 method를 지정한다.

							if (readLength != dataLength)
								throw new Exception("전송 받은 데이터 길이가 원본과 틀립니다.");
						}
					}
				}

				Console.WriteLine("\r\n{0}", DateTime.Now - startTime);

				CurrentMessage = "Saving file...";
			}
			catch
			{
				CurrentMessage = "File Receiving error.";
			}
			finally
			{
				if (clientSocket != null)
					clientSocket.Close();

				CurrentMessage = "Received & Saved file\r\nServer Stopped.";
			}
		}

		private int SaveFile(Stream stream, int bufferSize, string filePath, long dataLength)
		{
			Stream fs = File.Open(filePath, FileMode.Append);
			BinaryWriter bw = null;
			int totalRead = 0;
			int result = 0;
			int readLength = 0;
			byte[] targetData = new byte[bufferSize];
			
			try
			{
				bw = new BinaryWriter(fs);

				// 데이터를 저장한다.
				while ((readLength = stream.Read(targetData, totalRead, bufferSize)) > 0)
				{
					bw.Write(targetData, 0, readLength);
					
                    totalRead += readLength;
					Console.Write("\rTotal / Current : {0:#,##0} / {1:#,##0}", dataLength, totalRead);

					bufferSize = (int)Math.Min(bufferSize, dataLength - totalRead);
				}
			}
			catch (Exception ex)
			{
				CurrentMessage = ex.Message;
			}
			finally
			{
				if (bw != null)
				{
					bw.Flush();
					bw.Close();
				}
			}

			return result;
		}
	}
}
