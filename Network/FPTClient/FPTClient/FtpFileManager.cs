﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.FtpClient;
using System.Text;
using System.Threading.Tasks;

namespace FPTClient
{
	public class FtpFileManager
	{
		private readonly string host;
		private readonly string username;
		private readonly string password;

		public FtpFileManager(string host, string username, string password)
		{
			this.host = host;
			this.username = username;
			this.password = password;
		}

		public Task<string> DownloadFileAsync(string fileName)
		{
			return DownloadFileAsync(fileName, "/");
		}

		public async Task<string> DownloadFileAsync(string fileName, string workingDirectory)
		{
			string fileToWrite = Path.GetTempFileName();
			await DownloadFileAsync(fileName, workingDirectory, fileToWrite).ConfigureAwait(false);

			return fileToWrite;
		}

		public async Task DownloadFileAsync(string fileName, string workingDirectory, string filePathToWrite)
		{
			using (FtpClient ftpClient = CreateFtpClient())
			{
				await Task.Factory.FromAsync(ftpClient.BeginConnect, ftpClient.EndConnect, state: null).ConfigureAwait(false);
				bool doesDirectoryExist = await Task<bool>.Factory.FromAsync<string>(ftpClient.BeginDirectoryExists, ftpClient.EndDirectoryExists, workingDirectory, state: null).ConfigureAwait(false);
				if (doesDirectoryExist == true)
				{
					await Task.Factory.FromAsync<string>(ftpClient.BeginSetWorkingDirectory, ftpClient.EndSetWorkingDirectory, workingDirectory, state: null).ConfigureAwait(false);
					bool doesFileExist = await Task<bool>.Factory.FromAsync<string>(ftpClient.BeginFileExists, ftpClient.EndFileExists, fileName, state: null).ConfigureAwait(false);
					if (doesFileExist == true)
					{
						using (Stream streamToRead = await Task<Stream>.Factory.FromAsync<string>(ftpClient.BeginOpenRead, ftpClient.EndOpenRead, fileName, state: null).ConfigureAwait(false))
						using (Stream streamToWrite = File.Open(filePathToWrite, FileMode.Append))
						{
							await streamToRead.CopyToAsync(streamToWrite).ConfigureAwait(false);
						}
					}
				}
			}
		}

		// privates

		private FtpClient CreateFtpClient()
		{
			FtpClient ftpClient = new FtpClient();
			ftpClient.Host = host;
			ftpClient.Credentials = new NetworkCredential(username, password);

			return ftpClient;
		}
	}
}
