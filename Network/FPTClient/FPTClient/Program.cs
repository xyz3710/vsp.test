﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.FtpClient;
using System.Net.FtpClient.Async;
using System.Text;
using System.Threading.Tasks;

namespace FPTClient
{
	class Program
	{
		private const string HOST = "x10.iptime.org";
		private const int PORT = 21;
		private const string USERNAME = "xyz37";
		private const string PASSWORD = "green7";

		static void Main(string[] args)
		{
			//ConnectFTP(HOST, PORT, USERNAME, PASSWORD);
			//ConnectFTPAsync(host, port, userName, password);
			//Console.ReadLine();
			DownloadFile();
		}

		private static void ConnectFTP(string host, int port, string userName, string password)
		{
			FtpClient ftp = new FtpClient();

			ftp.Host = host;
			ftp.Port = port;
			ftp.Credentials = new NetworkCredential(userName, password);

			ftp.Connect();

			var list = ftp.GetListing("Down");

			foreach (FtpListItem item in list)
			{
				Console.WriteLine("{0}: {1:32} {2:#,##0} {3}", item.Type.ToString().Substring(0, 1), item.Name, item.Size, item.Modified);
			}

			ftp.Disconnect();
		}

		private static async void ConnectFTPAsync(string host, int port, string userName, string password)
		{
			FtpClient ftp = new FtpClient();

			ftp.Host = host;
			ftp.Port = port;
			ftp.Credentials = new NetworkCredential(userName, password);

			await ftp.ConnectAsync();

			var list = await ftp.GetListingAsync();

			foreach (FtpListItem item in list)
			{
				Console.WriteLine("{0}: {1:32} {2:#,##0} {3}", item.Type.ToString().Substring(0, 1), item.Name, item.Size, item.Modified);
			}

			await ftp.DisconnectAsync();
		}

		private static void DownloadFile()
		{
			FtpFileManager ftpFileManager = new FPTClient.FtpFileManager(HOST, USERNAME, PASSWORD);

			ftpFileManager.DownloadFileAsync("/프로젝트 필수 유틸리티/ARS_Server_Patch.7z");
		}
	}
}
