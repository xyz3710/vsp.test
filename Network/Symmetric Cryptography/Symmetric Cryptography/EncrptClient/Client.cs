using System;
using System.IO;
using System.Security.Cryptography;
using System.Net.Sockets;

namespace EncryptClient
{
	public class Client
	{
		public static void Main(string[] args)
		{
			const string Message = "Hello Papa";

			try
			{
				//Create a TCP connection to a listening TCP process.
				//Use "localhost" to specify the current computer or
				//replace "localhost" with the IP address of the 
				//listening process.  
				TcpClient TCP = new TcpClient("localhost", 11000);

				//Create a network stream from the TCP connection. 
				NetworkStream NetStream = TCP.GetStream();

				//Create a new instance of the RijndaelManaged class
				// and encrypt the stream.
				RijndaelManaged RMCrypto = new RijndaelManaged();

				byte[] Key = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
				byte[] IV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };

				//Create a CryptoStream, pass it the NetworkStream, and encrypt 
				//it with the Rijndael class.
				CryptoStream CryptStream = new CryptoStream(NetStream,
				RMCrypto.CreateEncryptor(Key, IV),
				CryptoStreamMode.Write);

				//Create a StreamWriter for easy writing to the 
				//network stream.
				StreamWriter SWriter = new StreamWriter(CryptStream);

				//Write to the stream.				
                SWriter.WriteLine(Message);

				//Inform the user that the message was written
				//to the stream.
				Console.WriteLine("[{0}] message was sent.", Message);

				//Close all the connections.
				SWriter.Close();
				CryptStream.Close();
				NetStream.Close();
				TCP.Close();
			}
			catch
			{
				//Inform the user that an exception was raised.
				Console.WriteLine("The connection failed.");
			}
		}
	}
}