using System;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Net;
using System.Security.Cryptography;

namespace DecryptHost
{
	public class Host
	{
		static void Main(string[] args)
		{
			//The key and IV must be the same values that were used
			//to encrypt the stream.  
			byte[] Key = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
			byte[] IV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };

			try
			{
				//Initialize a TCPListener on port 11000
				//using the current IP address.
				IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
				TcpListener TCPListen = new TcpListener(ipAddress, 11000);

				//Start the listener.
				TCPListen.Start();

				//Check for a connection every five seconds.
				while (!TCPListen.Pending())
				{
					Console.WriteLine("Still listening. Will try in 5 seconds.");
					Thread.Sleep(5000);
				}

				//Accept the client if one is found.
				TcpClient TCP = TCPListen.AcceptTcpClient();

				//Create a network stream from the connection.
				NetworkStream NetStream = TCP.GetStream();

				//Create a new instance of the RijndaelManaged class
				// and decrypt the stream.
				RijndaelManaged RMCrypto = new RijndaelManaged();


				//Create a CryptoStream, pass it the NetworkStream, and decrypt 
				//it with the Rijndael class using the key and IV.
				CryptoStream CryptStream = new CryptoStream(NetStream,
				   RMCrypto.CreateDecryptor(Key, IV),
				   CryptoStreamMode.Read);

				//Read the stream.
				StreamReader SReader = new StreamReader(CryptStream);

				//Display the message.
				Console.WriteLine("The decrypted original message: {0}", SReader.ReadToEnd());

				//Close the streams.
				SReader.Close();
				NetStream.Close();
				TCP.Close();
			}
			//Catch any exceptions. 
			catch
			{
				Console.WriteLine("The Listener Failed.");
			}
		}
	}
}