﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Net;

namespace iDASiT.FX.Win.Messenger.Server
{
	class Server
	{
		static void Main(string[] args)
		{
			string executeFileName = Path.GetFileNameWithoutExtension(Application.ExecutablePath);
			string configFilename = string.Format("{0}.exe.config", executeFileName);
			RemotingConfiguration.Configure(configFilename, false);

			if (EventLog.SourceExists(executeFileName) == false)
				EventLog.CreateEventSource(executeFileName, XMessage.EventLogName);

			EventLog eventLog = new EventLog(XMessage.EventLogName, Dns.GetHostName(), XMessage.EventLogName);

			eventLog.WriteEntry(string.Format("{0} service started{1}in {2} {3}", 
                                    executeFileName, 
                                    Environment.NewLine, 
                                    DateTime.Now.ToLongDateString(), 
                                    DateTime.Now.ToLongTimeString()), EventLogEntryType.Information, 1000);

			Console.WriteLine("Hit enter to exit");
			Console.ReadLine();

			eventLog.WriteEntry(string.Format("{0} service stopped{1}in {2} {3}", 
                                    executeFileName, 
                                    Environment.NewLine, 
                                    DateTime.Now.ToLongDateString(), 
                                    DateTime.Now.ToLongTimeString()), EventLogEntryType.Warning, 9000);
		}
	}
}
