﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.Server.MessageServerService
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 25일 금요일 오후 4:01
/*	Purpose		:	iDASiT.FX.Win.Messenger의 Server Service를 제어 합니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Threading;

namespace iDASiT.FX.Win.Messenger.Server
{
	/// <summary>
	/// iDASiT.FX.Win.Messenger의 Server Service를 제어 합니다.
	/// </summary>
	public class MessageServerCore
	{
		private bool _aborted;

		private bool Aborted
		{
			get
			{
				return _aborted;
			}
			set
			{
				_aborted = value;
			}
		}

		private void ServiceListener()
		{
			string executeFileName = Application.ExecutablePath;
			string configFilename = string.Format("{0}.config", executeFileName);
			string executeFileNameOnly = XMessage.EventLogName;

			if (EventLog.SourceExists(executeFileNameOnly) == false)
				EventLog.CreateEventSource(executeFileNameOnly, XMessage.EventLogName);

			EventLog eventLog = new EventLog(XMessage.EventLogName, Dns.GetHostName(), XMessage.EventLogName);

			RemotingConfiguration.Configure(configFilename, false);

			eventLog.WriteEntry(string.Format("{0} service started{1}in {2} {3}",
									executeFileNameOnly,
									Environment.NewLine,
									DateTime.Now.ToLongDateString(),
									DateTime.Now.ToLongTimeString()), EventLogEntryType.Information, 1000);

			while (Aborted == false)
				Thread.Sleep(100);

			eventLog.WriteEntry(string.Format("{0} service stopped{1}in {2} {3}",
						executeFileNameOnly,
						Environment.NewLine,
						DateTime.Now.ToLongDateString(),
						DateTime.Now.ToLongTimeString()), EventLogEntryType.Warning, 9000);
		}

		/// <summary>
		/// FX Messagenger Server Service를 시작합니다.
		/// </summary>
		public void Start()
		{
			Thread thread = new Thread(new ThreadStart(ServiceListener));

			thread.Name = "FX Message Server Service Thread";

			Aborted = false;
			thread.Start();
		}

		/// <summary>
		/// FX Messagenger Server Service를 시작합니다.
		/// </summary>
		public void Stop()
		{
			_aborted = true;

			Application.ExitThread();
		}
	}
}
