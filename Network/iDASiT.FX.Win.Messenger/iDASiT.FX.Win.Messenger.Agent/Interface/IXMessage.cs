﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.Agent.IXMessage
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 23일 수요일 오전 9:06
/*	Purpose		:	CallbackAgent에서 XMessage를 접근할 수 있는 Interface입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Messenger.Agent
{
	/// <summary>
	/// CallbackAgent에서 XMessage를 접근할 수 있는 Interface입니다.
	/// </summary>
	public interface IXMessage
	{
		/// <summary>
		/// XMessage를 구합니다.
		/// </summary>
		XMessage XMessage
		{
			get;
		}
	}
}
