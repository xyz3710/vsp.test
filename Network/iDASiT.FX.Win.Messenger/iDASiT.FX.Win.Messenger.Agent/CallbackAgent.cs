﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.Agent.CallbackAgent
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 4일 금요일 오후 2:35
/*	Purpose		:	Agent가 Server에 Message 전송 후 응답할 Method를 정의하는 객체 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using System.Security.Permissions;
using System.Runtime.Remoting.Lifetime;
using System.Reflection;

namespace iDASiT.FX.Win.Messenger.Agent
{
	/// <summary>
	/// Message를 전송하기 위한 이벤트 처리기 대리자 입니다.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void LogEventHandler(object sender, LoggingEventArgs e);

	/// <summary>
	/// Agent가 Server에 Message 전송 후 응답할 Method를 정의하는 객체 입니다.
	/// </summary>
	public class CallbackAgent : CallbackObject, IDisposable, IXMessage
	{
		#region Constants
		private const int CHECKSERVERINTERVAL = 3000;
		#endregion

		#region Fields
		private static XMessage _xMessage;
		private string _clientIpAddress;
		private string _userId;
		private AgentMode _agentMode;
		private ClientContext _clientContext;
		private static int _xMessageHashCode;
		#endregion

		#region Constructors
		/// <summary>
		/// CallbackClient class의 새 인스턴스를 초기화 합니다.(첫번째 bind된 Client IpAddress를 입력합니다.)
		/// </summary>
		public CallbackAgent()
			: this(Dns.GetHostAddresses(Dns.GetHostName())[0].ToString(), AgentMode.Client)
		{
		}

		/// <summary>
		/// CallbackClient class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="agentMode"></param>
		public CallbackAgent(AgentMode agentMode)
			: this(Dns.GetHostAddresses(Dns.GetHostName())[0].ToString(), agentMode)
		{
		}

        /// <summary>
		/// CallbackClient class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="clientIpAddress"></param>
		/// <param name="agentMode"></param>
		public CallbackAgent(string clientIpAddress, AgentMode agentMode)
		{
			_clientIpAddress = clientIpAddress;
			_clientContext = new ClientContext(clientIpAddress);

			CallContext.SetData(ClientContext.ContextName, _clientContext);

			string configFilename = string.Format("{0}.config", Path.GetFileName(Application.ExecutablePath));
			
			try
			{
				// 서버에 연결이 안되어 있을 경우 재 연결을 시도 하면 이미 Application은 구성이 되어 있으므로 skip 시킨다.
				RemotingConfiguration.Configure(configFilename, false);
			}
			catch (RemotingException)
			{				
			}
			
			AgentMode = agentMode;
			_xMessage = new XMessage();

			if (_xMessage == null || CheckConnection == false)
				throw new InvalidOperationException("Server에 연결할 수 없습니다.");

			_xMessage.AgentMode = agentMode;

			// CheckConnection에서 할당을 하므로 주석 처리
			//XMessageHashCode = _xMessage.GetHashCode();

			MonitoringServer();
		}

		private void AddCallbackEventHandler()
		{
			try
			{
				_xMessage.SendedMessage += new SendMessageEventHandler(OnSendMessage);

				_xMessage.PutFileConnected += new ServiceEventHandler(OnPutFileConnected);
				_xMessage.PutFileDisconnected += new ServiceEventHandler(OnPutFileDisconnected);
				_xMessage.PutFileHeaderSended += new TransferEventHandler(OnPutFileHeaderSended);
				_xMessage.PutFileSending += new TransferEventHandler(OnPutFileSending);
				_xMessage.PutFileCompleted += new TransferEventHandler(OnPutFileCompleted);
				_xMessage.PutFileError += new ErrorEventHandler(OnPutFileError);
			}
			catch
			{				
			}			
		}

		private void RemoveCallbackEventHandler()
		{
			try
			{
				_xMessage.SendedMessage -= new SendMessageEventHandler(OnSendMessage);

				_xMessage.PutFileConnected -= new ServiceEventHandler(OnPutFileConnected);
				_xMessage.PutFileDisconnected -= new ServiceEventHandler(OnPutFileDisconnected);
				_xMessage.PutFileHeaderSended -= new TransferEventHandler(OnPutFileHeaderSended);
				_xMessage.PutFileSending -= new TransferEventHandler(OnPutFileSending);
				_xMessage.PutFileCompleted -= new TransferEventHandler(OnPutFileCompleted);
				_xMessage.PutFileError -= new ErrorEventHandler(OnPutFileError);
			}
			catch 
			{				
			}
		}

		#region MonitoringServer
		private void MonitoringServer()
		{
			Thread thread = new Thread(new ThreadStart(MonitoringServerThread));

			thread.Name = "MonitoringServer";
			thread.IsBackground = true;
			thread.Start();            
		}

		private void MonitoringServerThread()
		{
			string intervalString = ConfigurationManager.AppSettings["CheckServerInterval"];
			int interval = CHECKSERVERINTERVAL;

			if (string.IsNullOrEmpty(intervalString) == false)
				int.TryParse(intervalString, out interval);
			
			while (true)
			{
				try
				{
					if (_xMessage.CheckConnection == true && XMessageHashCode != _xMessage.GetHashCode())
						XMessageHashCode = _xMessage.GetHashCode();
				}
				catch
				{					
				}

				Thread.Sleep(interval);
			}
		}
		#endregion
		#endregion

		#region Distructor
		/// <summary>
		/// 등록한 Event를 모두 제거한다.
		/// </summary>
		~CallbackAgent()
		{
			Dispose();
		}
		#endregion

		#region IDisposable 멤버

		/// <summary>
		/// CallbackClient에서 사용하는 모든 리소스를 해제합니다.
		/// </summary>
		public void Dispose()
		{
			RemoveCallbackEventHandler();
		}

		#endregion

		#region Events
		#region Message Events
		/// <summary>
		/// SendTo Message를 수신하면 발생하는 이벤트 입니다.
		/// </summary>
		public event SendMessageEventHandler SendedMessage;
		/// <summary>
		/// BroadCast Message를 수신하면 발생하는 이벤트 입니다.
		/// </summary>
		public event SendMessageEventHandler BroadCastedMessage;
		#endregion

		#region Login/Logout Events
		/// <summary>
		/// Login 하면 발생하는 이벤트 입니다.
		/// </summary>
		public event LogEventHandler LoggedIn;
		/// <summary>
		/// Logout 하면 발생하는 이벤트 입니다.
		/// </summary>
		public event LogEventHandler LoggedOut;
		/// <summary>
		/// Framework을 Update 하면 발생하는 이벤트 입니다.
		/// </summary>
		public event LogEventHandler AppUpdated;
		#endregion

		#region GetFile Receiver Events
		/// <summary>
		/// 서비스가 시작되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler GetFileStarted;
		/// <summary>
		/// 서비스가 중지되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler GetFileStopped;
		/// <summary>
		/// Client가 접속했을 경우 발생합니다.
		/// </summary>
		public event AcceptClientEventHandler GetFileAcceptClient;
		/// <summary>
		/// Header를 전송 받았을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileHeaderReceived;
		/// <summary>
		/// 데이터를 전송 받을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileReceiving;
		/// <summary>
		/// 데이터 수신이 완료되면 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileCompleted;
		/// <summary>
		/// Error가 발생할 경우 발생합니다.
		/// </summary>
		public event ErrorEventHandler GetFileError;
		#endregion

		#region GetFile Sender Events
		/// <summary>
		/// 서버와 접속이 되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler GetFileSendConnected;
		/// <summary>
		/// 서버와 접속이 종료되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler GetFileSendDisconnected;
		/// <summary>
		/// Header를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileSendHeaderSended;
		/// <summary>
		/// 데이터를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileSendSending;
		/// <summary>
		/// 데이터 전송이 완료되면 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileSendCompleted;
		/// <summary>
		/// 전송 중 Error가 있을 경우 발생합니다.
		/// </summary>
		public event ErrorEventHandler GetFileSendError;
		/// <summary>
		/// GetFile에 대한 Send 요청이 있을 경우 발생합니다.
		/// </summary>
		public event SendMessageEventHandler GetFileSendRequest;
		#endregion

		#region PutFile Events
		/// <summary>
		/// 서버와 접속이 되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler PutFileConnected;
		/// <summary>
		/// 서버와 접속이 종료되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler PutFileDisconnected;
		/// <summary>
		/// Header를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileHeaderSended;
		/// <summary>
		/// 데이터를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileSending;
		/// <summary>
		/// 데이터 전송이 완료되면 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileCompleted;
		/// <summary>
		/// 전송 중 Error가 있을 경우 발생합니다.
		/// </summary>
		public event ErrorEventHandler PutFileError;
		/// <summary>
		/// PutFile에 대한 데이터 수신이 완료되면 발생합니다.
		/// </summary>
		public event SendMessageEventHandler PutFileReceived;
		#endregion
		#endregion

		#region Properties
		/// <summary>
		/// Client의 IpAddress를 구하거나 설정합니다.
		/// <remarks>Server 객체에서 어떤 EventHandler에서 호출하였는지 구별하기 위해 override된 형태로 제공을 해야합니다.</remarks>
		/// </summary>
		public override string ClientIpAddress
		{
			get
			{
				return _clientIpAddress;
			}
		}

		/// <summary>
		/// Login한 UserId를 구하거나 설정합니다.
		/// </summary>
		public override string UserId
		{
			get
			{
				return _userId;
			}			
		}

		/// <summary>
		/// Client의 AgentMode를 구하거나 설정합니다.
		/// </summary>
		private AgentMode AgentMode
		{
			get
			{
				return _agentMode;
			}
			set
			{
				_agentMode = value;
			}
		}

		/// <summary>
		/// XMessageHashCode를 구하거나 설정합니다.
		/// <remarks>설정할 경우 EventHandler로 같이 재 등록 합니다.</remarks>
		/// </summary>
		private int XMessageHashCode
		{
			get
			{
				return _xMessageHashCode;
			}
			set
			{
				// 접속이 가능한 상태지만 처음 return한 HashCode가 틀릴 경우 
				// Server가 restart 되었을 수 있으므로 Event Handler를 재등록한다.
				RemoveCallbackEventHandler();
				_xMessageHashCode = value;
				AddCallbackEventHandler();
			}
		}
		#endregion

		#region IXMessage 멤버

		/// <summary>
		/// Server 객체의 XMessage를 구합니다.
		/// </summary>
		XMessage IXMessage.XMessage
		{
			get
			{
				return _xMessage;
			}
		}

		#endregion

		#region Protected methods
		#region Message Callback
		/// <summary>
		/// <seealso cref="iDASiT.FX.Win.Messenger.XMessage.SendMessage"/>를 호출할 경우 내부적으로 Message를 처리할 Method입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalSendMessageCallback(object sender, MessageEventArgs e)
		{
			#region Login/Logout
			if (e.EventSource == EventSource.Login)
			{
				LoggingEventArgs loggingEventArgs = new LoggingEventArgs(e.EventSource, e.SendType, e.Message, e.FromIpAddress, e.ToIpAddress, e.Tag);

				OnLoggedIn(sender, loggingEventArgs);
			}

			if (e.EventSource == EventSource.Logout)
			{
				LoggingEventArgs loggingEventArgs = new LoggingEventArgs(e.EventSource, e.SendType, e.Message, e.FromIpAddress, e.ToIpAddress, e.Tag);

				OnLoggedOut(sender, loggingEventArgs);
			}
			
			if (e.EventSource == EventSource.AppUpdate)
			{
				LoggingEventArgs loggingEventArgs = new LoggingEventArgs(e.EventSource, e.SendType, e.Message, e.FromIpAddress, e.ToIpAddress, e.Tag);

				OnAppUpdated(sender, loggingEventArgs);
			}
			#endregion
           	
			#region Peer to Peer Message
			// 특정 client에게 보낼 경우(자기 자신은 응답하지 않는다.)
			if (e.SendType == SendType.SendTo && e.EventSource == EventSource.Message
						&& string.Compare(ClientIpAddress, e.FromIpAddress, true) != 0
						&& string.Compare(ClientIpAddress, e.ToIpAddress, true) == 0)
			{
				OnSendedMessage(sender, e);

				return;
			}
			#endregion

			#region BroadCast Message
			// BroadCasting 일 경우
			if (e.SendType == SendType.BroadCast && e.EventSource == EventSource.Message
						&& string.Compare(ClientIpAddress, e.FromIpAddress, true) != 0)
			{
				OnBroadCastMessage(sender, e);

				return;
			}
			#endregion

			#region PutFile
			if (e.EventSource == EventSource.PutFile 
				&& string.Compare(ClientIpAddress, e.FromIpAddress, true) != 0
				&& string.Compare(ClientIpAddress, e.ToIpAddress, true) == 0)
			{
				try
				{
					FileReceiver fileReceiver = new FileReceiver(XMessage.PutFilePort);

					fileReceiver.ReceivedPath = e.Message;
					fileReceiver.CloseAfterReceive = true;
					fileReceiver.Start(false, 1);

					OnPutFileReceived(sender, e);
				}
				catch
				{
				}

				return;
			}
			#endregion

			#region GetFile Receiver
			// 서버객체를 사용하므로 Receiver를 서버에서 실행하면 서버만 Listen 할 수 있어서 각 client에서 listen 해야 한다.
			if (e.EventSource == EventSource.GetFileSendResponse 
				&& string.Compare(ClientIpAddress, e.ToIpAddress, true) == 0)
			{
				InternalGetFile(e.Message);

				return;
			}
			#endregion

			#region GetFile Sender
			if (e.EventSource == EventSource.GetFileSendRequest 
				&& string.Compare(ClientIpAddress, e.FromIpAddress, true) != 0
				&& string.Compare(ClientIpAddress, e.ToIpAddress, true) == 0)
			{
				try
				{
					#region Message를 parsing해서 FIleList를 구한다.
					string[] filePath = e.Message.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

					if (filePath.Length < 2)
						return;

					string[] fileList = new string[filePath.Length - 1];

					for (int i = 1; i < filePath.Length; i++)
						fileList[i - 1] = string.Format(@"{0}\{1}", filePath[0], filePath[i]);
					#endregion

					FileSender fileSender = new FileSender(e.FromIpAddress, XMessage.GetFilePort);

					fileSender.Connected += new ServiceEventHandler(OnGetFileSendConnected);
					fileSender.Disconnected += new ServiceEventHandler(OnGetFileSendDisconnected);
					fileSender.HeaderSended += new TransferEventHandler(OnGetFileSendHeaderSended);
					fileSender.Sending += new TransferEventHandler(OnGetFileSendSending);
					fileSender.Completed += new TransferEventHandler(OnGetFileSendCompleted);
					fileSender.Error += new ErrorEventHandler(OnGetFileSendError);

					OnGetFileSendRequest(sender, e);

					fileSender.SendFile(fileList);
				}
				catch
				{
				}

				return;
			}
			#endregion

			#region ExecuteCommand
			if (e.EventSource == EventSource.ExecuteCommand
				&& string.Compare(ClientIpAddress, e.FromIpAddress, true) != 0
				&& string.Compare(ClientIpAddress, e.ToIpAddress, true) == 0)
			{
				if (e.Tag != null)
				{
					string fileName = e.Message;
					string remotePath = e.Tag as string;
					
					if (string.IsNullOrEmpty(remotePath) == false)
					{
						string localPath = string.Format(@"{0}\{1}", remotePath, Path.GetFileName(fileName));
						AppDomainSetup newDomainInfo = new AppDomainSetup();

						newDomainInfo.ApplicationName = "AppDomainTest";
						newDomainInfo.ApplicationBase = remotePath;
						newDomainInfo.CachePath = string.Format(@"{0}\Cache\", remotePath);
						newDomainInfo.ShadowCopyFiles = "true";
						newDomainInfo.ShadowCopyDirectories = string.Format(@"{0}\", remotePath);

						AppDomain appDomain = AppDomain.CreateDomain(fileName, null, newDomainInfo);
						AssemblyName assemblyName = new AssemblyName(fileName);
						
						assemblyName.CodeBase = remotePath;
						
						Assembly commandAssembly = appDomain.Load(assemblyName);

						Type[] types = commandAssembly.GetTypes();

						foreach (Type type in types)
						{
							if (type.GetInterface("ICommand") != null)
							{
								ICommand command = commandAssembly.CreateInstance(type.FullName) as ICommand;

								if (command != null)
									command.Execute();

								break;
							}   	
						}
					}
				}

				return;
			}
			#endregion
		}

		/// <summary>
		/// 원격 서버에 Login하여 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnLoggedIn(object sender, LoggingEventArgs e)
		{
			if (LoggedIn != null)
				LoggedIn(sender, e);
		}

		/// <summary>
		/// 원격 서버에서 Logout하여 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnLoggedOut(object sender, LoggingEventArgs e)
		{
			if (LoggedOut != null)
				LoggedOut(sender, e);
		}

		/// <summary>
		/// Framework을 Update하여 Client에 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnAppUpdated(object sender, LoggingEventArgs e)
		{
			if (AppUpdated != null)
				AppUpdated(sender, e);
		}

		/// <summary>
		/// Peer to Peer Message를 수신 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnSendedMessage(object sender, MessageEventArgs e)
		{
			if (SendedMessage != null)
				SendedMessage(sender, e);
		}

		/// <summary>
		/// BroadCast Message를 수신 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnBroadCastMessage(object sender, MessageEventArgs e)
		{
			if (BroadCastedMessage != null)
				BroadCastedMessage(sender, e);
		}

		/// <summary>
		/// PutFile 후 파일 수신 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFileReceived(object sender, MessageEventArgs e)
		{
			if (PutFileReceived != null)
			{
				PutFileReceived(sender, e);

				_xMessage.WriteLog(EventSource.PutFileCompleted, SendType.SendTo, e.FromIpAddress, ClientIpAddress, UserId, e.Message, e.Tag);
			}
		}
		#endregion

		#region PutFile Callback
		/// <summary>
		/// 서버와 접속이 되면 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileConnectedCallback(object sender, ServerInfoEventArgs e)
		{
			if (PutFileConnected != null)
				PutFileConnected(sender, e);
		}

		/// <summary>
		/// 서버와 접속이 종료되면 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileDisconnectedCallback(object sender, ServerInfoEventArgs e)
		{
			if (PutFileDisconnected != null)
				PutFileDisconnected(sender, e);
		}

		/// <summary>
		/// Header를 전송했을 경우 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileHeaderSendedCallback(object sender, TransferInfoEventArgs e)
		{
			if (PutFileHeaderSended != null)
				PutFileHeaderSended(sender, e);
		}

		/// <summary>
		/// 데이터를 전송했을 경우 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileSendingCallback(object sender, TransferInfoEventArgs e)
		{
			if (PutFileSending != null)
				PutFileSending(sender, e);
		}

		/// <summary>
		/// 데이터 전송이 완료되면 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileCompletedCallback(object sender, TransferInfoEventArgs e)
		{
			if (PutFileCompleted != null)
				PutFileCompleted(sender, e);
		}

		/// <summary>
		/// 전송 도중 Error가 발생할 경우 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileErrorCallback(object sender, ErrorEventArgs e)
		{
			if (PutFileError != null)
				PutFileError(sender, e);
		}
		#endregion

		#region GetFile
		private void InternalGetFile(string receivedPath)
		{
			FileReceiver fileReceiver = new FileReceiver(XMessage.GetFilePort);

			fileReceiver.ReceivedPath = receivedPath as string;
			fileReceiver.ReceivedPathWithRemoteIpAddress = true;
			fileReceiver.ReceivedPathWithDate = true;
			fileReceiver.CloseAfterReceive = true;

			fileReceiver.Started += new ServiceEventHandler(OnGetFileStarted);
			fileReceiver.Stopped += new ServiceEventHandler(OnGetFileStopped);
			fileReceiver.AcceptClient += new AcceptClientEventHandler(OnGetFileAcceptClient);
			fileReceiver.HeaderReceived += new TransferEventHandler(OnGetFileHeaderReceived);
			fileReceiver.Receiving += new TransferEventHandler(OnGetFileReceiving);
			fileReceiver.Completed += new TransferEventHandler(OnGetFileCompleted);
			fileReceiver.Error += new ErrorEventHandler(OnGetFileError);

			fileReceiver.Start(false, 100);

			while (fileReceiver.Running == false)
				Thread.Sleep(500);
		}

		#region GetFile Receiver EventHandler
		/// <summary>
		/// Started 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileStarted(object sender, ServerInfoEventArgs e)
		{
			if (GetFileStarted != null)
				GetFileStarted(sender, e);
		}

		/// <summary>
		/// Stopped 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileStopped(object sender, ServerInfoEventArgs e)
		{
			if (GetFileStopped != null)
				GetFileStopped(sender, e);
		}

		/// <summary>
		/// AcceptClient 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileAcceptClient(object sender, SocketAsyncEventArgs e)
		{
			if (GetFileAcceptClient != null)
				GetFileAcceptClient(sender, e);
		}

		/// <summary>
		/// HeaderReceived 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileHeaderReceived(object sender, TransferInfoEventArgs e)
		{
			if (GetFileHeaderReceived != null)
				GetFileHeaderReceived(sender, e);
		}

		/// <summary>
		/// Receiving 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileReceiving(object sender, TransferInfoEventArgs e)
		{
			if (GetFileReceiving != null)
				GetFileReceiving(sender, e);
		}

		/// <summary>
		/// Completed 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileCompleted(object sender, TransferInfoEventArgs e)
		{
			if (GetFileCompleted != null)
			{
				GetFileCompleted(sender, e);

				_xMessage.WriteLog(EventSource.GetFileSendResponse, SendType.SendTo, e.ServerIpAddress, ClientIpAddress, UserId, e.FileName, e.CurrentSize);
			}
		}

		/// <summary>
		/// Error 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileError(object sender, iDASiT.FX.Win.Messenger.ErrorEventArgs e)
		{
			if (GetFileError != null)
				GetFileError(sender, e);
		}
		#endregion

		#region GetFile Sender EventHandler
		/// <summary>
		/// GetFile의 Connected 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileSendConnected(object sender, ServerInfoEventArgs e)
		{
			if (GetFileSendConnected != null)
				GetFileSendConnected(sender, e);
		}

		/// <summary>
		/// GetFile의 Disconnected 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileSendDisconnected(object sender, ServerInfoEventArgs e)
		{
			if (GetFileSendDisconnected != null)
				GetFileSendDisconnected(sender, e);
		}

		/// <summary>
		/// GetFile의 HeaderSended 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileSendHeaderSended(object sender, TransferInfoEventArgs e)
		{
			if (GetFileSendHeaderSended != null)
				GetFileSendHeaderSended(sender, e);
		}

		/// <summary>
		/// GetFile의 Sending 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileSendSending(object sender, TransferInfoEventArgs e)
		{
			if (GetFileSendSending != null)
				GetFileSendSending(sender, e);
		}

		/// <summary>
		/// GetFile의 Completed 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileSendCompleted(object sender, TransferInfoEventArgs e)
		{
			if (GetFileSendCompleted != null)
				GetFileSendCompleted(sender, e);
		}

		/// <summary>
		/// GetFile의 Error 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileSendError(object sender, iDASiT.FX.Win.Messenger.ErrorEventArgs e)
		{
			if (GetFileSendError != null)
				GetFileSendError(sender, e);
		}

		/// <summary>
		/// GetFile에 대한 Send 요청 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnGetFileSendRequest(object sender, MessageEventArgs e)
		{
			if (GetFileSendRequest != null)
				GetFileSendRequest(sender, e);
		}

		#endregion
		#endregion
		#endregion

		#region Public methods
		/// <summary>
		/// 이 인스턴스의 수명 정책을 제어하기 위한 수명 서비스 개체를 가져옵니다.
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}

		/// <summary>
		/// XMessage와의 연결 상태를 확인하는데 사용됩니다.
		/// </summary>
		public bool CheckConnection
		{
			get
			{
				bool result = false;

				// 연결할 수 없을 경우 off-line Message로 작동 시킨다.
				try
				{
					result = _xMessage.CheckConnection;

					if (result == true && XMessageHashCode != _xMessage.GetHashCode())
						XMessageHashCode = _xMessage.GetHashCode();
				}
				catch
				{
				}

				return result;
			}
		}

		/// <summary>
		/// 선택된 서버로 Login 합니다.
		/// <remarks>(모든 client에 로그인 message를 BroadCasting 합니다.)</remarks>
		/// </summary>
		/// <param name="userId">Login한 UserId입니다.</param>
		public bool Login(string userId)
		{
			return Login(userId, null);
		}

		/// <summary>
		/// 선택된 서버로 Login 합니다.
		/// <remarks>(특정 client에만 알립니다.)</remarks>
		/// </summary>
		/// <param name="userId">Login한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public bool Login(string userId, string toIpAddress)
		{
			if (CheckConnection == false)
				return false;

			SendType sendType = SendType.SendTo;
			object tag = null;

			_userId = userId;
			_xMessage.AgentMode = AgentMode;

			if (string.IsNullOrEmpty(toIpAddress) == true)
				sendType = SendType.BroadCast;            	

			if (_clientContext == null)
				_clientContext = new ClientContext(ClientIpAddress, userId);
			else
			{
				_clientContext.ClientIpAddress = ClientIpAddress;
				_clientContext.UserId = userId;
			}

			CallContext.SetData(ClientContext.ContextName, _clientContext);

			return _xMessage.Login(sendType, userId, toIpAddress, tag);
		}

		/// <summary>
		/// 선택된 서버에서 Logout 합니다.
		/// <remarks>(모든 client에 로그인 message를 BroadCasting 합니다.)</remarks>
		/// </summary>
		public bool Logout()
		{			
			return Logout(null);
		}

		/// <summary>
		/// 선택된 서버에서 Logout 합니다.
		/// <remarks>(특정 client에만 알립니다.)</remarks>
		/// </summary>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public bool Logout(string toIpAddress)
		{
			if (CheckConnection == false)
				return false;

			SendType sendType = SendType.SendTo;
			object tag = null;

			_xMessage.AgentMode = AgentMode;

			if (string.IsNullOrEmpty(toIpAddress) == true)
				sendType = SendType.BroadCast;

			return _xMessage.Logout(sendType, UserId, toIpAddress, tag);
		}

		/// <summary>
		/// Loader를 Update 합니다.
		/// <remarks>(모든 client에 로그인 message를 BroadCasting 합니다.)</remarks>
		/// </summary>
		public bool AppUpdate()
		{
			return AppUpdate(null);
		}

		/// <summary>
		/// Loader를 Update 합니다.
		/// <remarks>(특정 client에만 알립니다.)</remarks>
		/// </summary>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
        public bool AppUpdate(string toIpAddress)
		{
			if (CheckConnection == false)
				return false;

			SendType sendType = SendType.SendTo;
			object tag = null;

			_xMessage.AgentMode = AgentMode;

			if (string.IsNullOrEmpty(toIpAddress) == true)
				sendType = SendType.BroadCast;

			return _xMessage.AppUpdate(sendType, UserId, toIpAddress, tag);
		}

		/// <summary>
		/// 선택된 client로 Message를 전파 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="message">전파할 message를 입력합니다.</param>
		/// <returns>전파한 Client의 개수를 return 합니다.</returns>
		public int BroadCastMessage(string message)
		{
			if (CheckConnection == false)
				return -1;

			_xMessage.AgentMode = AgentMode;

			return _xMessage.SendMessage(message, null);
		}

		/// <summary>
		/// 선택된 client로 Message를 전송 합니다.
		/// </summary>
		/// <param name="message">전파할 message를 입력합니다.</param>
		/// <param name="toIpAddresses">Message를 전송할 client IpAddress 입니다.</param>
		/// <returns>전송한 Client의 개수를 return 합니다.</returns>
		public int SendMessage(string message, params string[] toIpAddresses)
		{
			if (CheckConnection == false)
				return -1;

			_xMessage.AgentMode = AgentMode;
			
			return _xMessage.SendMessage(message, toIpAddresses);
		}

		/// <summary>
		/// Controller에서 File을 가져 옵니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 remote path 입니다.</param>
		/// <param name="filePattern">가져오려는 파일 pattern 입니다.</param>
		/// <param name="localPath">가져온 파일을 저장할 로컬 경로입니다.<br/>경로 아래 IpAddress 형태로 폴더가 생성됩니다.</param>
		/// <param name="fromIpAddresses">데이터를 가져올 Client 입니다.</param>
		public void GetFiles(string remotePath, string filePattern, string localPath, params string[] fromIpAddresses)
		{
			if (CheckConnection == false)
				return;

			_xMessage.AgentMode = AgentMode;
			
			_xMessage.GetFiles(remotePath, filePattern, localPath, fromIpAddresses);
		}

		/// <summary>
		/// Controller에서 File을 가져 옵니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 local file path 입니다.</param>
		/// <param name="filePatterns">가져오려는 파일 pattern 입니다.</param>
		/// <param name="localPath">가져온 파일을 저장할 로컬 경로입니다.<br/>경로 아래 IpAddress 형태로 폴더가 생성됩니다.</param>
		/// <param name="fromIpAddresses">데이터를 가져올 Client 입니다.</param>
		public void GetFiles(string remotePath, string[] filePatterns, string localPath, params string[] fromIpAddresses)
		{
			if (CheckConnection == false)
				return;
            	
			_xMessage.AgentMode = AgentMode;
			
			_xMessage.GetFiles(remotePath, filePatterns, localPath, fromIpAddresses);
		}

		/// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.<br></br>
		/// 지정된 Folder만 검색합니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePattern">전송하려는 파일 pattern 입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string filePattern, string remotePath, params string[] toIpAddresses)
		{
			if (CheckConnection == false)
				return;

			_xMessage.AgentMode = AgentMode;
			
			_xMessage.PutFiles(localPath, new string[] { filePattern }, remotePath, toIpAddresses);
		}

		/// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.<br></br>
		/// 지정된 Folder만 검색합니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePatterns">전송하려는 파일 pattern 입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string[] filePatterns, string remotePath, params string[] toIpAddresses)
		{
			if (CheckConnection == false)
				return;

			SearchOption searchOption = SearchOption.TopDirectoryOnly;
			_xMessage.AgentMode = AgentMode;

			_xMessage.PutFiles(localPath, filePatterns, searchOption, remotePath, toIpAddresses);
		}

		/// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePatterns">전송하려는 파일 pattern 입니다.</param>
		/// <param name="searchOption">로컬 파일을 검색하는 Option입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string[] filePatterns, SearchOption searchOption, string remotePath, params string[] toIpAddresses)
		{
			if (CheckConnection == false)
				return;

			_xMessage.AgentMode = AgentMode;
			
			_xMessage.PutFiles(localPath, filePatterns, searchOption, remotePath, toIpAddresses);
		}

		/// <summary>
		/// Client <seealso cref="iDASiT.FX.Win.Messenger.ICommand"/> 을 구현한 class의 명령을 실행 합니다.
		/// </summary>
		/// <param name="commandAssemblyPath">ICommand를 구현하여 실행할 명령이 있는 assembly의 fullPath</param>
		/// <param name="remotePath">commandAssembly가 저장될 원격지의 path</param>
		/// <param name="toIpAddresses">명령을 실행할 Client 입니다.</param>
		/// <returns></returns>
		public bool ExecuteCommand(string commandAssemblyPath, string remotePath, params string[] toIpAddresses)
		{
			if (CheckConnection == false)
				return false;

			_xMessage.AgentMode = AgentMode;

			_executeCommandInfo = new ExecuteCommandInfo(commandAssemblyPath, remotePath, toIpAddresses);

			PutFileCompleted += new TransferEventHandler(OnExecutePutFileCompleted);
			_xMessage.PutFiles(Path.GetDirectoryName(commandAssemblyPath), Path.GetFileName(commandAssemblyPath), remotePath, toIpAddresses);			
						
			return true;
		}

		private ExecuteCommandInfo _executeCommandInfo;

		private class ExecuteCommandInfo
		{
			private string commandAssemblyPath;
			private string remotePath;
			private string[] toIpAddresses;

			/// <summary>
			/// ExecuteCommandInfo structure의 새 인스턴스를 초기화 합니다.
			/// </summary>
			/// <param name="commandAssemblyPath"></param>
			/// <param name="remotePath"></param>
			/// <param name="toIpAddresses"></param>
			public ExecuteCommandInfo(string commandAssemblyPath, string remotePath, string[] toIpAddresses)
			{
				this.commandAssemblyPath = commandAssemblyPath;
				this.remotePath = remotePath;
				this.toIpAddresses = toIpAddresses;
			}

			#region Properties
			/// <summary>
			/// CommandAssemblyPath를 구하거나 설정합니다.
			/// </summary>
			public string CommandAssemblyPath
			{
				get
				{
					return commandAssemblyPath;
				}
				set
				{
					commandAssemblyPath = value;
				}
			}
            
            /// <summary>
            /// RemotePath를 구하거나 설정합니다.
            /// </summary>
            public string RemotePath
            {
            	get
            	{
            		return remotePath;
            	}
            	set
            	{
            		remotePath = value;
            	}
            }
            
            /// <summary>
            /// ToIpAddresses를 구하거나 설정합니다.
            /// </summary>
			public string[] ToIpAddresses
            {
            	get
            	{
            		return toIpAddresses;
            	}
            	set
            	{
            		toIpAddresses = value;
            	}
            }
            
            #endregion
            
		}

		private void OnExecutePutFileCompleted(object sender, TransferInfoEventArgs e)
		{
			// 등록된 event handler는 사용 후 제거한다.
			PutFileCompleted -= new TransferEventHandler(OnExecutePutFileCompleted);
			
			// 실행 파일을 실행 하도록 메세지를 전송한다.
			if (_executeCommandInfo != null)
				_xMessage.ExecuteCommand(_executeCommandInfo.CommandAssemblyPath, 
                    _executeCommandInfo.RemotePath, 
                    _executeCommandInfo.ToIpAddresses);
		}

		#region GetRemoteFileList Test
		/*
        /// <summary>
		/// 원격 Client의 File 목록을 구합니다.
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 remote path 입니다.</param>
		/// <param name="fromIpAddress">구하고자 하는 Client IpAddress입니다.</param>
		/// <returns></returns>
		public List<string> GetRemoteFileList(string remotePath, string fromIpAddress)
		{
			if (CheckConnection == false)
				return null;

			_xMessage.AgentMode = AgentMode;

			return _xMessage.GetRemoteFileList(remotePath, fromIpAddress);
		}
        */
		#endregion

		#region Unidirection Send Message to Server for Logging
		/// <summary>
		/// Exception Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="message">exceptino message</param>
		/// <param name="exception">전달하려는 Exception</param>
		/// <param name="userId">작업중인 userId</param>
		/// <param name="toIpAddress">보내는 Server IpAddress</param>
		public void SendExceptionMessage(string message, Exception exception, string userId, string toIpAddress)
		{
			if (CheckConnection == false)
				return;

			_xMessage.SendExceptionMessage(message, exception, userId, toIpAddress);
		}

		/// <summary>
		/// Log Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="logData"></param>
		/// <param name="userId">작업중인 userId</param>
		/// <param name="additionalInfo">추가 정보</param>
		/// <param name="toIpAddress">보내는 Server IpAddress</param>
		public void SendLogMessage(string logData, string userId, string additionalInfo, string toIpAddress)
		{
			if (CheckConnection == false)
				return;

			_xMessage.SendLogMessage(logData, userId, additionalInfo, toIpAddress);
		}

		/// <summary>
		/// Client Form Action Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="userId">작업중인 userId</param>
		/// <param name="formId">FormId</param>
		/// <param name="formPath">Form Path</param>
		/// <param name="formVersion">Form의 Version</param>
		/// <param name="toIpAddress">보내는 Server IpAddress</param>
		public void SendFormOpenMessage(string userId, string formId, string formPath, string formVersion, string toIpAddress)
		{
			if (CheckConnection == false)
				return;

			_xMessage.SendFormOpenMessage(userId, formId, formPath, formVersion, toIpAddress);
		}
		
		/// <summary>
		/// Client Form Action Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="userId">작업중인 userId</param>
		/// <param name="formId">FormId</param>
		/// <param name="formPath">Form Path</param>
		/// <param name="formVersion">Form의 Version</param>
		/// <param name="toIpAddress">보내는 Server IpAddress</param>
		public void SendFormCloseMessage(string userId, string formId, string formPath, string formVersion, string toIpAddress)
		{
			if (CheckConnection == false)
				return;

			_xMessage.SendFormCloseMessage(userId, formId, formPath, formVersion, toIpAddress);
		}
		
		/// <summary>
		/// Client Form Action Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="userId">작업중인 userId</param>
		/// <param name="formId">FormId</param>
		/// <param name="toIpAddress">보내는 Server IpAddress</param>
		public void SendFormActiveMessage(string userId, string formId, string toIpAddress)
		{
			if (CheckConnection == false)
				return;

			_xMessage.SendFormActiveMessage(userId, formId, toIpAddress);
		}
		#endregion
		#endregion
	}
}
