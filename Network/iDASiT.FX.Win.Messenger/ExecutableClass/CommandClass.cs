﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace iDASiT.FX.Win.Messenger
{
	[Serializable]
	public class CommandClass : ICommand
	{
		public string Description
		{
			get
			{
				return "cmd 실행";
			}
		}

		public void Execute()
		{
			ProcessStartInfo psi = new ProcessStartInfo(@"C:\WINDOWS\system32\cmd.exe");

			psi.ErrorDialog = true;

			Process.Start(psi);
		}
	}
}
