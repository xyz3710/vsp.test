﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.CallbackObject
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 3일 목요일 오후 9:59
/*	Purpose		:	Client에서 처리되어진 Message에 Callback으로 return 받기 위한 class 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Messaging;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// Client에서 처리되어진 Message에 Callback으로 return 받기 위한 class 입니다.
	/// </summary>
	public abstract class CallbackObject : MarshalByRefObject
	{
		#region Properties
		/// <summary>
		/// Callback 받을 Source client의 IpAddress를 구하거나 설정합니다.
		/// </summary>
		public abstract string ClientIpAddress
		{
			get;
		}

		/// <summary>
		/// Callback 받을 Source client의 UserId를 구하거나 설정합니다.
		/// </summary>
		public abstract string UserId
		{
			get;
		}
		#endregion
		
		#region Internal abstract Method		
		#region Message
		/// <summary>
		/// <seealso cref="iDASiT.FX.Win.Messenger.XMessage.SendMessage"/>를 호출할 경우 내부적으로 Message를 처리할 Method입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected abstract void InternalSendMessageCallback(object sender, MessageEventArgs e);
		#endregion
				
		#region PutFile
		/// <summary>
		/// 서버와 접속이 되면 발생합니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected abstract void InternalPutFileConnectedCallback(object sender, ServerInfoEventArgs e);
		/// <summary>
		/// 서버와 접속이 종료되면 발생합니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected abstract void InternalPutFileDisconnectedCallback(object sender, ServerInfoEventArgs e);
		/// <summary>
		/// Header를 전송했을 경우 발생합니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected abstract void InternalPutFileHeaderSendedCallback(object sender, TransferInfoEventArgs e);
		/// <summary>
		/// 데이터를 전송했을 경우 발생합니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected abstract void InternalPutFileSendingCallback(object sender, TransferInfoEventArgs e);
		/// <summary>
		/// 데이터 전송이 완료되면 발생합니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected abstract void InternalPutFileCompletedCallback(object sender, TransferInfoEventArgs e);
		/// <summary>
		/// Error가 발생할 경우 발생합니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected abstract void InternalPutFileErrorCallback(object sender, ErrorEventArgs e);
		#endregion
		#endregion

		#region Public methods
		/// <summary>
		/// 이 인스턴스의 수명 정책을 제어하기 위한 수명 서비스 개체를 가져옵니다.
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}

		#region Message
		/// <summary>
		/// XMessage 객체에 SendMessage를 호출하면 Client에 Callback으로 발생합니다.
		/// <remarks><seealso cref="iDASiT.FX.Win.Messenger.CallbackObject"/>를 직접 구현하지 않은 class에서는 사용할 필요가 없습니다.</remarks>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		[OneWay]
		public void OnSendMessage(object sender, MessageEventArgs e)
		{
			InternalSendMessageCallback(sender, e);
		}
		#endregion
				
		#region PutFile
		/// <summary>
		/// 서버와 접속이 되면 Client에 Callback으로 발생합니다.
		/// <remarks><seealso cref="iDASiT.FX.Win.Messenger.CallbackObject"/>를 직접 구현하지 않은 class에서는 사용할 필요가 없습니다.</remarks>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		[OneWay]
		public void OnPutFileConnected(object sender, ServerInfoEventArgs e)
		{
			InternalPutFileConnectedCallback(sender, e);
		}

		/// <summary>
		/// 서버와 접속이 종료되면 Client에 Callback으로 발생합니다.
		/// <remarks><seealso cref="iDASiT.FX.Win.Messenger.CallbackObject"/>를 직접 구현하지 않은 class에서는 사용할 필요가 없습니다.</remarks>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		[OneWay]
		public void OnPutFileDisconnected(object sender, ServerInfoEventArgs e)
		{
			InternalPutFileDisconnectedCallback(sender, e);
		}

		/// <summary>
		/// Header를 전송하면 Client에 Callback으로 발생합니다.
		/// <remarks><seealso cref="iDASiT.FX.Win.Messenger.CallbackObject"/>를 직접 구현하지 않은 class에서는 사용할 필요가 없습니다.</remarks>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		[OneWay]
		public void OnPutFileHeaderSended(object sender, TransferInfoEventArgs e)
		{
			InternalPutFileHeaderSendedCallback(sender, e);
		}

		/// <summary>
		/// 데이터를 전송하면 Client에 Callback으로 발생합니다.
		/// <remarks><seealso cref="iDASiT.FX.Win.Messenger.CallbackObject"/>를 직접 구현하지 않은 class에서는 사용할 필요가 없습니다.</remarks>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		[OneWay]
		public void OnPutFileSending(object sender, TransferInfoEventArgs e)
		{
			InternalPutFileSendingCallback(sender, e);
		}

		/// <summary>
		/// 데이터 전송이 완료되면 Client에 Callback으로 발생합니다.
		/// <remarks><seealso cref="iDASiT.FX.Win.Messenger.CallbackObject"/>를 직접 구현하지 않은 class에서는 사용할 필요가 없습니다.</remarks>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		[OneWay]
		public virtual void OnPutFileCompleted(object sender, TransferInfoEventArgs e)
		{
			InternalPutFileCompletedCallback(sender, e);
		}

		/// <summary>
		/// 전송 도중 Error가 발생하면 Client에 Callback으로 발생합니다.
		/// <remarks><seealso cref="iDASiT.FX.Win.Messenger.CallbackObject"/>를 직접 구현하지 않은 class에서는 사용할 필요가 없습니다.</remarks>
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		[OneWay]
		public void OnPutFileError(object sender, ErrorEventArgs e)
		{
			InternalPutFileErrorCallback(sender, e);
		}
		#endregion
		#endregion
	}
}
