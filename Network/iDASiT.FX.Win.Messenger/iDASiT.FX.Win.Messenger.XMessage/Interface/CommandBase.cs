﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.CommandBase
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 2일 수요일 오후 9:11
/*	Purpose		:	Client에 Command를 실행할 수 있는 base class 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// Client에 Command를 실행할 수 있는 Interface 입니다.
	/// </summary>
	[Serializable]
	public abstract class CommandBase 
	{
		/// <summary>
		/// CommandBase class의 새 인스턴스를 초기화 합니다.
		/// <remarks>구현하는 class는 <seealso cref="System.SerializableAttribute"/>을 사용 해야 합니다.</remarks>
		/// <exception cref="System.Runtime.Serialization.SerializationException">구현 class에서 <seealso cref="System.SerializableAttribute"/>을 사용하지 않으면 발생합니다.</exception>
		/// </summary>
		public CommandBase()
		{
			foreach (SerializableAttribute attr in this.GetType().GetCustomAttributes(false))
			{
				if (attr == null)
					throw new SerializationException(string.Format("{0}는 System.SerializableAttribute를 사용 해야 합니다.", this.GetType().FullName));
			}
		}

		/// <summary>
		/// 해당 class의 설명을 구합니다.
		/// </summary>
		/// <returns></returns>
		public abstract string Description
		{
			get;
		}

		/// <summary>
		/// 명령을 실행합니다.
		/// </summary>
		/// <returns>성공 여부를 return 합니다.</returns>
		public abstract void Execute();
	}
}
