﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.FormAction
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 2일 수요일 오후 8:15
/*	Purpose		:	Form의 FormAction을 선택합니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// Agent의 Mode을 선택합니다.
	/// </summary>
	public enum FormAction
	{
		/// <summary>
		/// Form을 Open 했을 경우를 선택합니다.
		/// </summary>
		Open = 0,
		/// <summary>
		/// Form을 Close 했을 경우를 선택합니다.
		/// </summary>
		Close,
		/// <summary>
		/// Form을 활성화 했을 경우를 선택합니다.
		/// </summary>
		Active,
	}
}
