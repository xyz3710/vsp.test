/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.RegisterEventArgs
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 2일 수요일 오후 7:54
/*	Purpose		:	SendingMessage Event를 위한 Argument입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// SendingMessage Event를 위한 Argument입니다.
	/// </summary>
	[Serializable]
	public class MessageEventArgs : EventArgs
	{
		#region Fields
		private SendType _sendType;
		private EventSource _eventSource;
		private string _message;
		private string _fromUserId;
		private string _fromIpAddress;
		private string _toIpAddress;
		private object _tag;
		#endregion

		#region Constructors
		/// <summary>
		/// MessageEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="sendType">전송한 message type입니다.</param>
		/// <param name="message">전송한 message입니다.</param>
		/// <param name="fromIpAddress">전송한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public MessageEventArgs(SendType sendType, string message, string fromIpAddress, string toIpAddress)
			: this(EventSource.Message, sendType, message, fromIpAddress, toIpAddress, String.Empty, null)
		{
		}

		/// <summary>
		/// MessageEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="eventSource">Event가 발생한 Source 입니다.</param>
		/// <param name="sendType">전송한 message type입니다.</param>
		/// <param name="message">전송한 message입니다.</param>
		/// <param name="fromIpAddress">전송한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public MessageEventArgs(EventSource eventSource, SendType sendType, string message, string fromIpAddress, string toIpAddress)
			: this(eventSource, sendType, message, fromIpAddress, toIpAddress, String.Empty, null)
		{
		}

		/// <summary>
		/// MessageEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="sendType">전송한 message type입니다.</param>
		/// <param name="message">전송한 message입니다.</param>
		/// <param name="fromIpAddress">전송한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="fromUserId">메세지를 전달한 사용자 Id 입니다.</param>
		/// <param name="tag">Message 전달시 사용될 Tag입니다.</param>
		protected MessageEventArgs(SendType sendType, string message, string fromIpAddress, string toIpAddress, string fromUserId, object tag)
		{
			_eventSource = EventSource.Message;
			_sendType = sendType;
			_message = message;
			_fromIpAddress = fromIpAddress;
			_toIpAddress = toIpAddress;
			_fromUserId = fromUserId;
			_tag = tag;
		}

		/// <summary>
		/// MessageEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="eventSource">Event가 발생한 Source 입니다.</param>
		/// <param name="sendType">전송한 message type입니다.</param>
		/// <param name="message">전송한 message입니다.</param>
		/// <param name="fromIpAddress">전송한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="tag">Message 전달시 사용될 Tag입니다.</param>
		public MessageEventArgs(EventSource eventSource, SendType sendType, string message, string fromIpAddress, string toIpAddress, object tag)
			: this(eventSource, sendType, message, fromIpAddress, toIpAddress, String.Empty, tag)
		{
		}

        /// <summary>
		/// MessageEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="eventSource">Event가 발생한 Source 입니다.</param>
		/// <param name="sendType">전송한 message type입니다.</param>
		/// <param name="message">전송한 message입니다.</param>
		/// <param name="fromIpAddress">전송한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="fromUserId">메세지를 전달한 사용자 Id 입니다.</param>
		/// <param name="tag">Message 전달시 사용될 Tag입니다.</param>
		public MessageEventArgs(EventSource eventSource, SendType sendType, string message, string fromIpAddress, string toIpAddress, string fromUserId, object tag)
		{
			_eventSource = eventSource;
			_sendType = sendType;
			_message = message;
			_fromIpAddress = fromIpAddress;
			_toIpAddress = toIpAddress;
			_fromUserId = fromUserId;
			_tag = tag;
		}
		#endregion

		#region Properties
		/// <summary>
		/// EventSource를 구하거나 설정합니다.
		/// </summary>
		public EventSource EventSource
		{
			get
			{
				return _eventSource;
			}
			set
			{
				_eventSource = value;
			}
		}

		/// <summary>
		/// SendType를 구하거나 설정합니다.
		/// </summary>
		public SendType SendType
		{
			get
			{
				return _sendType;
			}
			set
			{
				_sendType = value;
			}
		}

		/// <summary>
		/// Message를 구하거나 설정합니다.
		/// </summary>
		public string Message
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}

		/// <summary>
		/// UserId를 구하거나 설정합니다.
		/// </summary>
		public string FromUserId
		{
			get
			{
				return _fromUserId;
			}
			set
			{
				_fromUserId = value;
			}
		}

		/// <summary>
        /// FromIpAddress를 구하거나 설정합니다.
        /// </summary>
        public string FromIpAddress
        {
        	get
        	{
        		return _fromIpAddress;
        	}
        	set
        	{
        		_fromIpAddress = value;
        	}
        }

		/// <summary>
        /// ToIpAddress를 구하거나 설정합니다.
        /// </summary>
        public string ToIpAddress
        {
        	get
        	{
        		return _toIpAddress;
        	}
        	set
        	{
        		_toIpAddress = value;
        	}
        }

		/// <summary>
		/// Tag를 구하거나 설정합니다.
		/// </summary>
		public object Tag
		{
			get
			{
				return _tag;
			}
			set
			{
				_tag = value;
			}
		}
		#endregion
	}
}
