﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.TransferInfoEventArgs
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 10일 목요일 오후 5:38
/*	Purpose		:	파일 전송 정보에 대한 EventArgs입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// 파일 전송 정보에 대한 EventArgs입니다.
	/// </summary>
	[Serializable]
	public class TransferInfoEventArgs : ServerInfoEventArgs
	{
		#region Fields
		private string _fileName;
		private long _currentSize;
		private long _totalSize;
		#endregion

		#region Constructors
		/// <summary>
		/// TransferInfoEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="fileName">전송중인 파일 이름 입니다.</param>
		/// <param name="currentSize">현재 전송된 byte 입니다.</param>
		/// <param name="totalSize">전체 전송할 byte 입니다.</param>
		public TransferInfoEventArgs(string fileName, long currentSize, long totalSize)
			: this(fileName, currentSize, totalSize, string.Empty, 0)
		{
		}

		/// <summary>
		/// TransferInfoEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="filePath">전송중인 파일 이름 입니다.</param>
		/// <param name="currentSize">현재 전송된 byte 입니다.</param>
		/// <param name="totalSize">전체 전송할 byte 입니다.</param>
		/// <param name="serverIpAddress">서비스 하는 Server의 IpAddress 입니다.</param>
		/// <param name="port">Socket과 Binding되는 Port 번호 입니다.</param>
		public TransferInfoEventArgs(string filePath, long currentSize, long totalSize, string serverIpAddress, int port)
			: base(serverIpAddress, port)
		{
			_fileName = filePath;
			_currentSize = currentSize;
			_totalSize = totalSize;
		}
		#endregion

		#region Properties
		/// <summary>
		/// FileName를 구하거나 설정합니다.
		/// </summary>
		public string FileName
		{
			get
			{
				return _fileName;
			}
			set
			{
				_fileName = value;
			}
		}

		/// <summary>
		/// CurrentSize를 구하거나 설정합니다.
		/// </summary>
		public long CurrentSize
		{
			get
			{
				return _currentSize;
			}
			set
			{
				_currentSize = value;
			}
		}

		/// <summary>
		/// TotalSize를 구하거나 설정합니다.
		/// </summary>
		public long TotalSize
		{
			get
			{
				return _totalSize;
			}
			set
			{
				_totalSize = value;
			}
		}

		/// <summary>
		/// 현재 전송한 전송율을 구합니다.
		/// </summary>
		public double Percentage
		{
			get
			{
				return ((double)CurrentSize / (double)TotalSize) * 100;
			}
		}
		#endregion
	}
}
