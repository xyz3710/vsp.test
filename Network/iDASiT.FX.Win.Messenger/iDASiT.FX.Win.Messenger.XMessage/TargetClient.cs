﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.TargetClient
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 8월 26일 화요일 오후 4:26
/*	Purpose		:	전송할 Target Client의 정보를 저장한 structure 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.IO;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// 전송할 Target Client의 정보를 저장한 class 입니다.
	/// </summary>
	public class TargetClient
	{
		#region Fields
		private string _clientIpAddress;
		private string _userId;
		#endregion

		#region Constructors
		/// <summary>
		/// TargetClient structure의 새 인스턴스를 초기화 합니다.
		/// </summary>
		static TargetClient()
		{
		}

		/// <summary>
		/// TargetClient structure의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="clientIpAddress"></param>
		/// <param name="userId"></param>
		public TargetClient(string clientIpAddress, string userId)
		{
			_clientIpAddress = clientIpAddress;
			_userId = userId;
		}
		#endregion

		#region Properties
		/// <summary>
		/// ClientIpAddress를 구하거나 설정합니다.
		/// </summary>
		public string ClientIpAddress
		{
			get
			{
				return _clientIpAddress;
			}
			set
			{
				_clientIpAddress = value;
			}
		}
        
        /// <summary>
        /// UserId를 구하거나 설정합니다.
        /// </summary>
		public string UserId
        {
        	get
        	{
        		return _userId;
        	}
        	set
        	{
        		_userId = value;
        	}
        }
        
		/// <summary>
		/// Target Client 정보를 이용한 Key를 구합니다.
		/// </summary>
		public string Key
		{
			get
			{
				return MergeToAgentKey(ClientIpAddress, UserId);
			}
		}
        #endregion

		/// <summary>
		/// 현재 TargetClient를 나타내는 <seealso cref="System.String"/>을 반환 합니다.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{			
			return MergeToAgentKey(ClientIpAddress, UserId);
		}

		/// <summary>
		/// AgentKey를 생성합니다.
		/// </summary>
		/// <param name="clientIpAddress">Client IpAddress</param>
		/// <param name="userId">UserId</param>
		/// <returns></returns>
		public static string MergeToAgentKey(string clientIpAddress, string userId)
		{
			return string.Format("{0}{1}{2}", clientIpAddress, Path.DirectorySeparatorChar, userId);
		}

		/// <summary>
		/// AgentKey에서 Client IpAddress와 UserId를 구합니다.
		/// </summary>
		/// <param name="key">Agent Key</param>
		/// <param name="clientIpAddress">분리된 Client IpAddress</param>
		/// <param name="userId">분리된 UserId</param>
		public static void SplitFromAgentKey(string key, out string clientIpAddress, out string userId)
		{
			string[] result = key.Split(Path.DirectorySeparatorChar);

			clientIpAddress = result[0];
			userId = result[1];
		}
	}
}