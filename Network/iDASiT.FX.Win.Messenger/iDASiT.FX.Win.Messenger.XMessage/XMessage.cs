﻿#define CONSOLE
//#define SERVICE_LOG
/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.XMessage
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 2일 수요일 오후 6:05
/*	Purpose		:	iDASiT Windows Framework Messenger의 Server와 Agent의 공통 Object입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	현재는 한개의 Ip에 여러 Id를 등록할 수는 있다 하지만 메세지는 Ip단위로만 전송한다.
 *					(한 Ip에 등록된 client들은 Id가 틀리더라도 동시에 메세지를 받을 수 있다.)
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Net.Sockets;
using System.IO;
using System.Net;
using System.Threading;
using System.ComponentModel;
using System.Data.OracleClient;
using iDASiT.AppFoundation.Shopfloor;
using System.Data;
using System.Diagnostics;
using System.Configuration;

namespace iDASiT.FX.Win.Messenger
{
    /// <summary>
	/// iDASiT Windows Framework Messenger의 Server와 Agent의 공통 Object입니다.
	/// </summary>
	public class XMessage : MarshalByRefObject
	{
		#region Constants
		/// <summary>
		/// PutFile에 사용되는 Port입니다.
		/// </summary>
		public const int PutFilePort = 19091;
		/// <summary>
		/// GetFile에 사용되는 Port입니다.
		/// </summary>
		public const int GetFilePort = 19090;
		/// <summary>
		/// XMessage의 Event Log에 남길 Source의 이름 입니다.
		/// </summary>
		public const string EventLogName = "XMessage";
		private const int DUMMY_SIGNAL_INTERVAL = 3000;
		#endregion

		#region Fields
		private static DatabaseHelperProxy _databaseHelperProxy;
        private AgentMode _agentMode;
		private string _clientIpAddress;
		private Dictionary<string, string> _agents;		// ClientIpAddress + UserId, UserId로 구성된다.(Login을 해야 생성된다.)
		private int _hashCode;
		#endregion
		
		#region Constructors
		/// <summary>
		/// XMessage class의 새 인스턴스를 <seealso cref="iDASiT.FX.Win.Messenger.AgentMode.Client"/>로 초기화 합니다.
		/// <remarks>Client에 Message BroadCasting등 제어를 하려면 AgentMode를 <seealso cref="iDASiT.FX.Win.Messenger.AgentMode.Controller"/>로 설정해야 합니다.<br/>
		/// 각 Method에서 FromIpAddress를 할당하지 않으려면 ClientIpAddress를 설정해야 합니다.</remarks> 
		/// </summary>
		public XMessage()
		{
			_agentMode = AgentMode.Client;
			_agents = new Dictionary<string, string>();
			
			if (EventLog.SourceExists(EventLogName) == false)
				EventLog.CreateEventSource(EventLogName, EventLogName);

			_hashCode = (int)DateTime.Now.ToFileTimeUtc();

			EventLog.WriteEntry(EventLogName, string.Format("XMessage object created in {0} {1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString()));
			DummySignalSendByInterval();
		}

		private void DummySignalSendByInterval()
		{
			string serverIp = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString();
			DummySignal(serverIp);
		}

		private void DummySignal(string serverIp)
		{
			Thread thread = new Thread(new ParameterizedThreadStart(DummySignalThread));

			thread.IsBackground = true;
			thread.Start(serverIp);
		}

		private void DummySignalThread(object serverIp)
		{
			ClientContext clientContext = new ClientContext(serverIp as string);
			CallContext.SetData(ClientContext.ContextName, clientContext);
			string intervalString = ConfigurationManager.AppSettings["DummySignalInterval"];
			int interval = DUMMY_SIGNAL_INTERVAL;

			if (string.IsNullOrEmpty(intervalString) == false)
				int.TryParse(intervalString, out interval);
            	
			while (true)
			{
				InternalSendMessage(EventSource.DummySignalMessageBroadCasted, string.Empty, null, null);
				
				Thread.Sleep(interval);
			}			
		}
		#endregion

		#region Events
		/// <summary>
		/// Message를 전송하면 발생하는 이벤트 입니다.
		/// <remarks><seealso cref="iDASiT.FX.Win.Messenger.CallbackObject"/>에서 상속받은</remarks>
		/// </summary>
		public event SendMessageEventHandler SendedMessage;

		/// <summary>
		/// 서버와 접속이 되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler PutFileConnected;
		/// <summary>
		/// 서버와 접속이 종료되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler PutFileDisconnected;
		/// <summary>
		/// Header를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileHeaderSended;
		/// <summary>
		/// 데이터를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileSending;
		/// <summary>
		/// 데이터 전송이 완료 되면 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileCompleted;
		/// <summary>
		/// Error가 발생할 경우 발생합니다.
		/// </summary>
		public event ErrorEventHandler PutFileError;
		#endregion

		#region Properties
		/// <summary>
		/// AgentMode를 구하거나 설정합니다.
		/// </summary>
		public AgentMode AgentMode
		{
			get
			{
				return _agentMode;
			}
			set
			{
				_agentMode = value;
			}
		}

		/// <summary>
		/// ClientIpAddress를 구하거나 설정합니다.
		/// <remarks>FromIpAddress 속성으로 사용됩니다.</remarks>
		/// </summary>
		public string ClientIpAddress
		{
			get
			{
				if (string.IsNullOrEmpty(_clientIpAddress) == true)
					throw new InvalidOperationException("ClientIpAddress를 설정해야 합니다.");

				return _clientIpAddress;
			}
			set
			{
				_clientIpAddress = value;
			}
		}

		#region Agents
		/// <summary>
		/// 현재 접속한 Agent 목록을 구하거나 설정합니다.
		/// </summary>
		public Dictionary<string, string> Agents
		{
			get
			{
				return _agents;
			}
			set
			{
				_agents = value;
			}
		}

		private string MergeToAgentKey(string clientIpAddress, string userId)
		{
			return string.Format("{0}{1}{2}", clientIpAddress, Path.DirectorySeparatorChar, userId);
		}

		private void SplitFromAgentKey(string key, out string clientIpAddress, out string userId)
		{
			string[] result = key.Split(Path.DirectorySeparatorChar);

			clientIpAddress = result[0];
			userId = result[1];
		}

		#endregion

		/// <summary>
		/// Agent와의 연결 상태를 확인하는데 사용됩니다.
		/// </summary>
		/// <returns></returns>
		public bool CheckConnection
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// ClientContext에서 ClientIpAddres를 구한다.
		/// </summary>
		private ClientContext ClientContext
		{
			get
			{
				ClientContext clientContext = CallContext.GetData(ClientContext.ContextName) as ClientContext;

				if (clientContext == null)
					throw new InvalidOperationException("CallbackObject를 구현한 class에서 ClientContext를 구할 수 없습니다.");

				return clientContext;
			}
		}
		#endregion

		#region GetHashCode
		/// <summary>
		/// 객체가 생성된 시간으로 HashCode를 Return합니다.
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return _hashCode;
		}
		#endregion

		#region InitializeLifetimeService
		/// <summary>
		/// Service life time은 null을 return 합니다.
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}
		#endregion
		        
		#region Use Callback Messagees
		/// <summary>
		/// 선택된 client로 Message를 전파 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="message">전파할 message를 입력합니다.</param>
		/// <returns>전파한 Client의 개수를 return 합니다.</returns>
		public int BroadCastMessage(string message)
		{
			if (AgentMode == AgentMode.Client)
				throw new InvalidOperationException("AgentMode를 Controller로 설정해야 사용할 수 있습니다.");

			return SendMessage(message, null);
		}

		/// <summary>
		/// 선택된 client로 Message를 전송 합니다.
		/// </summary>
		/// <param name="message">전파할 message를 입력합니다.</param>
		/// <param name="toIpAddresses">Message를 전송할 client IpAddress 입니다.</param>
		/// <returns>전송한 Client의 개수를 return 합니다.</returns>
		public int SendMessage(string message, params string[] toIpAddresses)
		{
			return InternalSendMessage(EventSource.Message, message, null, toIpAddresses);
		}
		
		/// <summary>
		/// 선택된 client로 Message를 전송 합니다.
		/// </summary>
		/// <param name="eventSource">메세지를 전송할 EventSource를 입력합니다.</param>
		/// <param name="message">전파할 message를 입력합니다.</param>
		/// <param name="tag">기타 정보를 입력합니다.</param>
		/// <param name="toIpAddresses">Message를 전송할 client IpAddress 입니다.</param>
		/// <returns>전송한 Client의 개수를 return 합니다.</returns>
		private int InternalSendMessage(EventSource eventSource, string message, object tag, params string[] toIpAddresses)
		{
			int result = 0;
			SendType sendType = toIpAddresses == null || toIpAddresses.Length == 0 ? SendType.BroadCast : SendType.SendTo;

			if (SendedMessage != null)
			{
				if (eventSource == EventSource.Message 
						&& AgentMode == AgentMode.Client && toIpAddresses == null)
					throw new InvalidOperationException("AgentMode가 Client로 설정되어 있으면 toIpAddresses에 null을 할당할 수 없습니다.");

				string fromIpAddress = ClientContext.ClientIpAddress;
				Delegate[] sendingMessageGetInvocationList = SendedMessage.GetInvocationList();
				Dictionary<string, string> successAgents = new Dictionary<string, string>(); // ClientIpAddress + UserId, UserId로 구성된다.

				string toIpAddressString = string.Empty;
				int toIpCount = 0;

				if (toIpAddresses != null)
				{
					StringBuilder toIps = new StringBuilder();

					foreach (string ipAddress in toIpAddresses)
					{
						toIps.AppendFormat("{0};", ipAddress);
						toIpCount++;
					}

					toIpAddressString = toIps.ToString(0, toIps.Length - 1);
				}
				else
					toIpAddressString = SendType.BroadCast.ToString();

				if (eventSource != EventSource.GetFileSendResponse 
					&& eventSource != EventSource.PutFile
					&& eventSource != EventSource.DummySignalMessageBroadCasted
					&& eventSource != EventSource.AbnormalLogout)
				{
					WriteLog(eventSource,
						sendType,
						fromIpAddress,
						toIpCount == 1 ? 
								toIpAddressString : 
								(eventSource == EventSource.GetFileSendRequest ? 
									string.Empty :
									SendType.BroadCast.ToString()),
						ClientContext.UserId,
						message,
						toIpAddressString,
						eventSource == EventSource.GetFileSendRequest ?
								tag : null);
				}

				foreach (Delegate xMessageDelegate in sendingMessageGetInvocationList)
				{
						CallbackObject callbackObjject = xMessageDelegate.Target as CallbackObject;
						string eventHandlerClientIpAddress = string.Empty;
						string userId = string.Empty;

						try
						{
							// 자기 자신의 EventHandler를 호출할 필요는 없다.
							// FileReceiver는 자기 자신이 Receiver를 열어야 하므로 호출할 수 있다.
							if (callbackObjject != null)
							{
								string agentKey = string.Empty;

								lock (this)
								{
									eventHandlerClientIpAddress = callbackObjject.ClientIpAddress;
									userId = callbackObjject.UserId;
									agentKey = MergeToAgentKey(eventHandlerClientIpAddress, userId);
								}
								
								if (((eventSource == EventSource.Login
    										&& fromIpAddress == eventHandlerClientIpAddress)
    									|| eventSource == EventSource.DummySignalMessageBroadCasted)
									&& Agents.ContainsKey(agentKey) == false)
								{
									Agents.Add(agentKey, userId);
								}

								if (((eventSource == EventSource.Logout 
											|| eventSource == EventSource.AppUpdate)
                                		&& fromIpAddress == eventHandlerClientIpAddress
                                		&& userId == message)		// Logout일 경우에는 message가 UserId 이다
									&& Agents.ContainsKey(agentKey) == true)
								{
									Agents.Remove(agentKey);
									SendedMessage = MulticastDelegate.Remove(SendedMessage, xMessageDelegate) as SendMessageEventHandler;
								}

								// Message를 성공적으로 보낸 Agent만 저장하여 비정상 Logout한 Agent를 제거한다.
								if (successAgents.ContainsKey(agentKey) == false)
									successAgents.Add(agentKey, userId);

								if (eventHandlerClientIpAddress == fromIpAddress
									&& eventSource != EventSource.GetFileSendResponse)
									continue;
							}
						}
						catch (SocketException sEx)
						{
							switch (sEx.ErrorCode)
							{
								case 10054:		// 대상 컴퓨터에서 연결을 거부했으므로 연결하지 못했습니다 .
								case 10061:		// 현재 연결은 원격 호스트에 의해 강제로 끊겼습니다.
									SendedMessage = MulticastDelegate.Remove(SendedMessage, xMessageDelegate) as SendMessageEventHandler;

									break;
							}

							continue;
						}

						// 각 EventHandler에 따라서 Invoke를 시킨다.
						SendMessageEventHandler mHandler = xMessageDelegate as SendMessageEventHandler;

						if (mHandler != null)
						{
							if (sendType == SendType.BroadCast)
								mHandler.BeginInvoke(this, new MessageEventArgs(eventSource, sendType, message, fromIpAddress, string.Empty, userId, tag), null, null);
							else if (sendType == SendType.SendTo)
							{
								// 특정 Client에 전송할 경우
								foreach (string toIpAddress in toIpAddresses)
								{
									if (eventHandlerClientIpAddress == toIpAddress)
										// 대상 client에 SendMessage 이벤트를 실행한다.
										mHandler.BeginInvoke(this, new MessageEventArgs(eventSource, sendType, message, fromIpAddress, toIpAddress, userId, tag), null, null);
								}
							}

							result++;
						}
				}

				Dictionary<string, string> failAgent = new Dictionary<string, string>();	// ClientIpAddress + UserId, UserId로 구성된다.
				string clientIp = string.Empty;
				string clientUserId = string.Empty;

				// 실패한 Agent는 제거한다.
				foreach (string key in Agents.Keys)
				{
					if (successAgents.ContainsKey(key) == false)
						failAgent.Add(key, clientUserId);
				}

				// AbnormalLogout은 자기 자신에서 호출하므로 아래 routine을 실행하면 무한 재귀에 빠진다.
				if (eventSource == EventSource.AbnormalLogout)
					return result;

				foreach (string key in failAgent.Keys)
				{
					// 해당 Client가 정상 종료를 하지 않거나 MulticastDelegate에서 제거 하지 않고 종료한 경우이다.
					// 강제로 Logout한 User를 알린다.
					// SendedMessage == null일 경우 Log를 남기지 않아서 미리 처리 한다.
					SplitFromAgentKey(key, out clientIp, out clientUserId);
					WriteLog(EventSource.AbnormalLogout, SendType.BroadCast, clientIp, toIpAddressString, clientUserId, clientUserId);
					InternalSendMessage(EventSource.AbnormalLogout, clientUserId, clientIp, null);

					Agents.Remove(key);
				}
			}
			else
				// server와 연결이 된 뒤 server가 restart 되면 메세지를 다시 등록할 수 있도록 한다.
				result = -1;

#if CONSOLE
			Console.WriteLine("Agents Count : {0}, count : {1}", Agents.Count, SendedMessage == null ? 0 : SendedMessage.GetInvocationList().Length);
			Console.Write("Agents : ");

			foreach (string key in Agents.Keys)
				Console.Write("{0}, ", Agents[key]);

			Console.WriteLine("");
#endif
			return result;
		}
		#endregion

		#region Use Callback Login/Logout Messages
		/// <summary>
		/// 선택된 서버로 Login 합니다.
		/// <remarks>(모든 client에 로그인 message를 BroadCasting 합니다.)</remarks>
		/// </summary>
		/// <param name="userId">Login한 UserId입니다.</param>
		public bool Login(string userId)
		{
			SendType sendType = SendType.BroadCast;
			string toIpAddress = null;
			object tag = null;

			return Login(sendType, userId, toIpAddress, tag);
		}

		/// <summary>
		/// 선택된 서버로 Login 합니다.
		/// <remarks>(특정 client에만 알립니다.)</remarks>
		/// </summary>
		/// <param name="userId">Login한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public bool Login(string userId, string toIpAddress)
		{
			SendType sendType = SendType.SendTo;
			object tag = null;
		
			return Login(sendType, userId, toIpAddress, tag);
		}

        /// <summary>
		/// 선택된 서버로 Login 합니다.
		/// </summary>
		/// <param name="sendType">Login을 전달할 message type입니다.</param>
		/// <param name="userId">Login한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="tag">Login시 전달할 Tag입니다.</param>
		public bool Login(SendType sendType, string userId, string toIpAddress, object tag)
		{
			int result = 0;

			if (sendType == SendType.BroadCast)
			{
				// Login의 BroadCastMessage는 AgentMode와 상관없이 이루어져야 한다.
				AgentMode agentMode = AgentMode;
				AgentMode = AgentMode.Controller;

				result = InternalSendMessage(EventSource.Login, userId, tag, null);

				AgentMode = agentMode;
			}
			else if (sendType == SendType.SendTo)
				result = InternalSendMessage(EventSource.Login, userId, tag, toIpAddress);

			return result > 0;
		}

		/// <summary>
		/// 선택된 서버에서 Logout 합니다.
		/// <remarks>(모든 client에 로그인 message를 BroadCasting 합니다.)</remarks>
		/// </summary>
		/// <param name="userId">Logout한 UserId입니다.</param>
		public bool Logout(string userId)
		{
			SendType sendType = SendType.BroadCast;
			string toIpAddress = null;
			object tag = null;

			return Logout(sendType, userId, toIpAddress, tag);
		}

        /// <summary>
		/// 선택된 서버에서 Logout 합니다.
		/// <remarks>(특정 client에만 알립니다.)</remarks>
		/// </summary>
		/// <param name="userId">Logout한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public bool Logout(string userId, string toIpAddress)
		{
			SendType sendType = SendType.SendTo;
			object tag = null;

			return Logout(sendType, userId, toIpAddress, tag);
		}

        /// <summary>
		/// 선택된 서버에서 Logout 합니다.
		/// </summary>
		/// <param name="sendType">Logout을 전달할 message type입니다.</param>
		/// <param name="userId">Logout한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="tag">Lotout시 전달할 Tag입니다.</param>
		public bool Logout(SendType sendType, string userId, string toIpAddress, object tag)
		{
			int result = 0;

			if (sendType == SendType.BroadCast)
			{
				// Login의 BroadCastMessage는 AgentMode와 상관없이 이루어져야 한다.
				AgentMode agentMode = AgentMode;
				AgentMode = AgentMode.Controller;

				lock (this)
				{
					result = InternalSendMessage(EventSource.Logout, userId, tag, null);
				}

				AgentMode = agentMode;
			}
			else if (sendType == SendType.SendTo)
				result = InternalSendMessage(EventSource.Logout, userId, tag, toIpAddress);

			return result > 0;
		}

		/// <summary>
		/// Loader를 Update 합니다.
		/// </summary>
		/// <param name="sendType">Logout을 전달할 message type입니다.</param>
		/// <param name="userId">Logout한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="tag">Lotout시 전달할 Tag입니다.</param>
		public bool AppUpdate(SendType sendType, string userId, string toIpAddress, object tag)
		{
			int result = 0;

			if (sendType == SendType.BroadCast)
			{
				// Login의 BroadCastMessage는 AgentMode와 상관없이 이루어져야 한다.
				AgentMode agentMode = AgentMode;
				AgentMode = AgentMode.Controller;

				result = InternalSendMessage(EventSource.AppUpdate, userId, tag, null);

				AgentMode = agentMode;
			}
			else if (sendType == SendType.SendTo)
				result = InternalSendMessage(EventSource.AppUpdate, userId, tag, toIpAddress);

			return result > 0;
		}
		#endregion

		#region Object
		/// <summary>
		/// Client에 요청 message 보냅니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="requestObject">요청할 객체를 선택합니다.</param>
		/// <returns>성공 여부를 return 합니다.</returns>
		public bool Request(object requestObject)
		{
			if (AgentMode == AgentMode.Client)
				throw new InvalidOperationException("생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.");

			bool result = false;
			

			return result;
		}
		#endregion

		#region Files
		private delegate void SendFileMessage(string filePath, string[] targetIpAddresses, List<FileInfo> fileInfos);

		#region SendFileMessageData class
		private class SendFileMessageData
		{
			private string _message;
			private string[] _fileList;
			private string[] _targetIpAddresses;

			/// <summary>
			/// SendFileMessageData class의 새 인스턴스를 초기화 합니다.
			/// </summary>
			/// <param name="message"></param>
			/// <param name="targetIpAddresses"></param>
			public SendFileMessageData(string message, string[] targetIpAddresses)
			{
				_message = message;
				_targetIpAddresses = targetIpAddresses;
			}

			/// <summary>
			/// SendPutMessageData class의 새 인스턴스를 초기화 합니다.
			/// </summary>
			/// <param name="fileList">file Message를 보낼 파일 목록 입니다.</param>
			/// <param name="targetIpAddresses">보내려는 대상 입니다.</param>
			public SendFileMessageData(string[] fileList, string[] targetIpAddresses)
			{
				_fileList = fileList;
				_targetIpAddresses = targetIpAddresses;
			}

			#region Properties
			/// <summary>
			/// Message를 구하거나 설정합니다.
			/// </summary>
			public string Message
			{
				get
				{
					return _message;
				}
				set
				{
					_message = value;
				}
			}

			/// <summary>
			/// LocalFileList를 구하거나 설정합니다.
			/// </summary>
			public string[] FileList
			{
				get
				{
					return _fileList;
				}
				set
				{
					_fileList = value;
				}
			}

			/// <summary>
			/// ToIpAddresses를 구하거나 설정합니다.
			/// </summary>
			public string[] TargetIpAddresses
			{
				get
				{
					return _targetIpAddresses;
				}
				set
				{
					_targetIpAddresses = value;
				}
			}
			#endregion
		}
		#endregion

		#region GetFiles
		/// <summary>
		/// Controller에서 File을 가져 옵니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 remote path 입니다.</param>
		/// <param name="filePattern">가져오려는 파일 pattern 입니다.</param>
		/// <param name="localPath">가져온 파일을 저장할 로컬 경로입니다.<br/>경로 아래 IpAddress 형태로 폴더가 생성됩니다.</param>
		/// <param name="fromIpAddresses">데이터를 가져올 Client 입니다.</param>
		public void GetFiles(string remotePath, string filePattern, string localPath, params string[] fromIpAddresses)
		{
			GetFiles(remotePath, new string[] { filePattern }, localPath, fromIpAddresses);			
		}

		/// <summary>
		/// Controller에서 File을 가져 옵니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 local file path 입니다.</param>
		/// <param name="filePatterns">가져오려는 파일 pattern 입니다.</param>
		/// <param name="localPath">가져온 파일을 저장할 로컬 경로입니다.<br/>경로 아래 IpAddress 형태로 폴더가 생성됩니다.</param>
		/// <param name="fromIpAddresses">데이터를 가져올 Client 입니다.</param>
		public void GetFiles(string remotePath, string[] filePatterns, string localPath, params string[] fromIpAddresses)
		{
			if (AgentMode == AgentMode.Client)
				throw new InvalidOperationException("생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.");

			// Client에 파일과 pattern을 보낸다.
			List<string> fileList = new List<string>();
			StringBuilder filePatternList = new StringBuilder();
			
			foreach (string pattern in filePatterns)
				fileList.AddRange(Directory.GetFiles(remotePath, pattern, SearchOption.TopDirectoryOnly));

			// 받는 쪽에서 ; 으로 paring 할 수 있도록 묶는다.
			filePatternList.AppendFormat("{0};", remotePath);

			foreach (string fileName in fileList)
				filePatternList.AppendFormat("{0};", Path.GetFileName(fileName));

			// 호출한 client에서 FileReceiver의 실행 message를 전송한다.
			InternalSendMessage(EventSource.GetFileSendResponse, localPath, null, ClientContext.ClientIpAddress);

			// fromIpAddresses에서 Sender의 실행 message를 전송한다.
			InternalSendMessage(EventSource.GetFileSendRequest, filePatternList.ToString(), fileList.Count, fromIpAddresses);
			#region For Test(Server에서 Receiver 생성)
			/*
			SendFileMessage sendGetFileMessage = new SendFileMessage(SendGetFileMessageToTarget);
			AsyncCallback asyncCallback = new AsyncCallback(CallbackSendGetFileMessage);
			SendFileMessageData sendGetMessageData = new SendFileMessageData(filePatternList.ToString(), fromIpAddresses);

			sendGetFileMessage.BeginInvoke(localPath, fromIpAddresses, asyncCallback, sendGetMessageData);
			*/
			#endregion
		}

		#region For Test(Server에서 Receiver 생성)
		/*
		private void SendGetFileMessageToTarget(string filePath, string[] targetIpAddresses)
		{
			Thread[] threads = new Thread[targetIpAddresses.Length];
			ParameterizedThreadStart threadParameter = new ParameterizedThreadStart(InternalGetFiles);

			for (int i = 0; i < targetIpAddresses.Length; i++)
				threads[i] = new Thread(threadParameter);

			for (int i = 0; i < targetIpAddresses.Length; i++)
				threads[i].Start(string.Format(@"{0}\{1}", filePath, targetIpAddresses[i]));
		}

		private void InternalGetFiles(object receivedPath)
		{
			FileReceiver fileReceiver = new FileReceiver(GetFilePort);
			
			fileReceiver.ReceivedPath = receivedPath as string;
			fileReceiver.CloseAfterReceive = true;

			//fileReceiver.Started += new ServiceEventHandler(OnGetFilesStarted);
			//fileReceiver.Stopped += new ServiceEventHandler(OnGetFilesStopped);
			//fileReceiver.AcceptClient += new AcceptClientEventHandler(OnGetFilesAcceptClient);
			//fileReceiver.HeaderReceived += new TransferEventHandler(OnGetFilesHeaderReceived);
			//fileReceiver.Receiving += new TransferEventHandler(OnGetFilesReceiving);
			fileReceiver.Completed += new TransferEventHandler(OnGetFilesCompleted);
			fileReceiver.Error += new ErrorEventHandler(OnGetFilesError);

			fileReceiver.Start(false, 1);

			while (fileReceiver.Running == false)
				Thread.Sleep(500);
			
			DebugLog("Port : {0}", fileReceiver.Port);
			//DebugLog("{0} : {1}, {2}", e.Message, e.FromIpAddress, e.ToIpAddress);
		}

		private void CallbackSendGetFileMessage(IAsyncResult ar)
		{
			SendFileMessageData data = ar.AsyncState as SendFileMessageData;

			if (data != null)
				InternalSendMessage(EventSource.GetFile, data.Message, data.TargetIpAddresses);
		}
		*/
		#endregion

		#region GetRemoteFileList Test
		/*
		/// <summary>
		/// 원격 Client의 File 목록을 구합니다.
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 remote path 입니다.</param>
		/// <param name="fromIpAddress">구하고자 하는 Client IpAddress입니다.</param>
		/// <returns></returns>
		public List<string> GetRemoteFileList(string remotePath, string fromIpAddress)
		{
			return InternalGetRemoteFileList(remotePath, fromIpAddress, GetListEventSource.Request);
		}

		/// <summary>
		/// 원격 Client의 File 목록을 구합니다.
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 remote path 입니다.</param>
		/// <param name="fromIpAddress">구하고자 하는 Client IpAddress입니다.</param>
		/// <param name="getListEventSource"></param>
		/// <returns></returns>
		private List<string> InternalGetRemoteFileList(string remotePath, string fromIpAddress, GetListEventSource getListEventSource)
		{
			ResponseGetListDelegte responseGetListDelegte = new ResponseGetListDelegte(InternalGetRemoteFileList);

			InternalSendMessage(EventSource.GetFileList, remotePath, responseGetListDelegte, fromIpAddress);

			return null;
		}

		private enum GetListEventSource
		{
			Request,
			Response,
		}

		private delegate List<string> ResponseGetListDelegte(string remotePath, string fromIpAddress, GetListEventSource getListEventSource);
		*/
		#endregion
		#endregion

		#region Use Callback PutFiles
		/// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.<br></br>
		/// 지정된 Folder만 검색합니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePattern">전송하려는 파일 pattern 입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string filePattern, string remotePath, params string[] toIpAddresses)
		{
			PutFiles(localPath, new string[] { filePattern }, remotePath, toIpAddresses);
		}

        /// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.<br></br>
		/// 지정된 Folder만 검색합니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePatterns">전송하려는 파일 pattern 입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string[] filePatterns, string remotePath, params string[] toIpAddresses)
		{
			SearchOption searchOption = SearchOption.TopDirectoryOnly;

			PutFiles(localPath, filePatterns, searchOption, remotePath, toIpAddresses);
		}

        /// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePatterns">전송하려는 파일 pattern 입니다.</param>
		/// <param name="searchOption">로컬 파일을 검색하는 Option입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string[] filePatterns, SearchOption searchOption, string remotePath, params string[] toIpAddresses)
		{
			if (AgentMode == AgentMode.Client)
				throw new InvalidOperationException("생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.");

			//if (toIpAddresses.Length < 1)
			//	throw new InvalidOperationException("ToIpAddress를 설정하십시오.");

			#region Target client를 SendedMessage에서 찾는다.
			// Message를 수신할 수 있어야 파일도 수신 할 수 있다.
			// BroadCast File일 경우 message를 보낼 수 있는 Client list를 구한다.
			List<string> clientList = new List<string>();

			foreach (Delegate xMessageDelegate in SendedMessage.GetInvocationList())
			{
				CallbackObject obj = xMessageDelegate.Target as CallbackObject;
				string eventHandlerClientIpAddress = string.Empty;

				try
				{
					// 자기 자신의 EventHandler를 호출할 필요는 없다.
					if (obj != null)
					{
						eventHandlerClientIpAddress = obj.ClientIpAddress;

						if (eventHandlerClientIpAddress == ClientContext.ClientIpAddress)
							continue;
						else
							clientList.Add(eventHandlerClientIpAddress);
					}
				}
				catch
				{
				}
			}

			List<string> targetClientList = new List<string>();

			// 실제 EventHandler에 등록된 client에만 File을 전송하도록 목록을 다시 구한다.
			foreach (string targetIpAddress in toIpAddresses)
			{
				bool exists = clientList.Exists(delegate(string checkIpAddress)
				{
					if (targetIpAddress == checkIpAddress)
						return true;

					return false;
				});

				if (exists == true)
					targetClientList.Add(targetIpAddress);
			}

			if (targetClientList.Count > 0)
				toIpAddresses = targetClientList.ToArray();
			else
				return;
			#endregion

			List<string> fileList = new List<string>();

			foreach (string searchPattern in filePatterns)
				fileList.AddRange(Directory.GetFiles(localPath, searchPattern, searchOption));

			if (toIpAddresses.Length == 0 || fileList.Count == 0)
				return;

			List<FileInfo> fileInfos = new List<FileInfo>();
			string toIpAddString = string.Empty;
			StringBuilder toAddrSB = new StringBuilder();
            
			foreach(string toAdd in toIpAddresses)
				toAddrSB.AppendFormat("{0};", toAdd);

			toIpAddString = toAddrSB.ToString(0, toAddrSB.Length - 1);

			foreach (string filePath in fileList)
			{
				FileInfo fileInfo = new FileInfo(filePath);

				fileInfos.Add(fileInfo);
				WriteLog(EventSource.PutFile, 
                    SendType.SendTo, 
                    ClientContext.ClientIpAddress, 
                    toIpAddresses.Length == 1 ? toIpAddresses[0] : SendType.BroadCast.ToString(), 
                    ClientContext.UserId,
					string.Format("{0};{1}", filePath, remotePath), 
                    fileInfo.Length,
					toIpAddresses.Length == 1 ? string.Empty : toIpAddString);	
			}			

			SendFileMessage sendPutFileMessage = new SendFileMessage(SendPutFileMessageToTarget);
			AsyncCallback asyncCallback = new AsyncCallback(CallbackSendPutFileMessage);
			SendFileMessageData sendPutMessageData = new SendFileMessageData(fileList.ToArray(), toIpAddresses);

			sendPutFileMessage.BeginInvoke(remotePath, toIpAddresses, fileInfos, asyncCallback, sendPutMessageData);
		}

		private void SendPutFileMessageToTarget(string remotePath, string[] toIpAddresses, List<FileInfo> fileInfos)
		{			
			InternalSendMessage(EventSource.PutFile, remotePath, fileInfos, toIpAddresses);
		}

		private void CallbackSendPutFileMessage(IAsyncResult ar)
		{
			SendFileMessageData data = ar.AsyncState as SendFileMessageData;

			if (data != null)
			{
				Thread[] threads = new Thread[data.TargetIpAddresses.Length];
				ParameterizedThreadStart threadParameter = new ParameterizedThreadStart(InternalPutFiles);

				for (int i = 0; i < data.TargetIpAddresses.Length; i++)
					threads[i] = new Thread(threadParameter);

				for (int i = 0; i < data.TargetIpAddresses.Length; i++)
				{
					PutFilesParameter param = new PutFilesParameter(data.TargetIpAddresses[i], data.FileList);

					threads[i].Start(param);
				}
			}
		}

		private void InternalPutFiles(object putFilesParameter)
		{
			PutFilesParameter param = putFilesParameter as PutFilesParameter;

			if (param != null)
			{
				FileSender fileSender = new FileSender(param.ToIpAddress, PutFilePort);

				fileSender.Connected += new ServiceEventHandler(OnPutFilesConnected);
				fileSender.Disconnected += new ServiceEventHandler(OnPutFilesDisconnected);
				fileSender.HeaderSended += new TransferEventHandler(OnPutFilesHeaderSended);
				fileSender.Sending += new TransferEventHandler(OnPutFilesSending);
				fileSender.Completed += new TransferEventHandler(OnPutFilesCompleted);
				fileSender.Error += new ErrorEventHandler(OnPutFilesError);

				fileSender.SendFile(param.FilePaths);
			}
		}

		/// <summary>
		/// PutFiles의 PutFileConnected 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesConnected(object sender, ServerInfoEventArgs e)
		{
			if (PutFileConnected != null)
			{
				try
				{
					PutFileConnected(sender, e);
				}
				catch
				{
				}
				finally
				{
					// 해당 파일이 완료되면 이벤트를 더이상 수신하지 않는다.
					PutFileConnected = MulticastDelegate.RemoveAll(PutFileConnected, PutFileConnected) as ServiceEventHandler;
				}
			}

			DebugLog("{0}:{1} Connected.", e.ServerIpAddress, e.Port, e.ListenerCount);
		}

		/// <summary>
		/// PutFiles의 PutFileDisconnected 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesDisconnected(object sender, ServerInfoEventArgs e)
		{
			if (PutFileDisconnected != null)
			{
				try
				{
					PutFileDisconnected(sender, e);
				}
				catch
				{
				}
				finally
				{
					// 해당 파일이 완료되면 이벤트를 더이상 수신하지 않는다.
					PutFileDisconnected = MulticastDelegate.RemoveAll(PutFileDisconnected, PutFileDisconnected) as ServiceEventHandler;
				}
			}

			DebugLog("{0}:{1} Disconnected.", e.ServerIpAddress, e.Port, e.ListenerCount);
		}

		/// <summary>
		/// PutFiles의 PutFileHeaderSended 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesHeaderSended(object sender, TransferInfoEventArgs e)
		{
			if (PutFileHeaderSended != null)
			{
				try
				{
					PutFileHeaderSended(sender, e);
				}
				catch
				{
				}
				finally
				{
					PutFileHeaderSended = MulticastDelegate.RemoveAll(PutFileHeaderSended, PutFileHeaderSended) as TransferEventHandler;
				}
			}

			DebugLog("Header Sended\t{0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		/// <summary>
		/// PutFiles의 PutFileSending 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesSending(object sender, TransferInfoEventArgs e)
		{
			if (PutFileSending != null)
			{
				try
				{
					PutFileSending(sender, e);
				}
				catch 
				{
				}
				finally
				{
					PutFileSending = MulticastDelegate.RemoveAll(PutFileSending, PutFileSending) as TransferEventHandler;
				}
			}

			//Console.Write("\rSending\t\t{0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		/// <summary>
		/// PutFiles의 PutFileCompleted 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesCompleted(object sender, TransferInfoEventArgs e)
		{
			if (PutFileCompleted != null)
			{
				// BroadCast PutFiles 일 경우 Socket exception이 발생할 수 있어서 구현
				Delegate[] putFileCompletedGetInvocationList = PutFileCompleted.GetInvocationList();

				foreach (Delegate eventDelegate in putFileCompletedGetInvocationList)
				{
					try
					{
						CallbackObject obj = eventDelegate.Target as CallbackObject;
						string eventHandlerClientIpAddress = string.Empty;
						string userId = string.Empty;

						if (obj != null)
						{
							eventHandlerClientIpAddress = obj.ClientIpAddress as string;
							userId = obj.UserId as string;
						}

						if (eventHandlerClientIpAddress == ClientContext.ClientIpAddress)
							eventDelegate.DynamicInvoke(sender, e);
					}
					catch
					{
						// 해당 이벤트가 불완전 제거 되면 강제로 삭제 한다.
						PutFileCompleted = MulticastDelegate.Remove(PutFileCompleted, eventDelegate) as TransferEventHandler;

						continue;
					}
				}
			}

			DebugLog("Completed\t{0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		/// <summary>
		/// PutFiles의 PutFileError 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesError(object sender, ErrorEventArgs e)
		{
			if (PutFileError != null)
			{
				try
				{
					PutFileError(sender, e);
				}
				catch
				{
				}
				finally
				{
					// 해당 파일이 완료되면 이벤트를 더이상 수신하지 않는다.
					PutFileError = MulticastDelegate.RemoveAll(PutFileError, PutFileError) as ErrorEventHandler;
				}				
			}

			DebugLog(e.Message);
		}

        #region PutFilesParameter class
		private class PutFilesParameter
		{
			#region Fields
			private string _toIpAddress;
			private string[] _filePath;
			#endregion

			#region Constructors
			/// <summary>
			/// PutFilesParameter class의 새 인스턴스를 초기화 합니다.
			/// </summary>
			/// <param name="toIpAddress"></param>
			/// <param name="filePath"></param>
			public PutFilesParameter(string toIpAddress, params string[] filePath)
			{
				_toIpAddress = toIpAddress;
				_filePath = filePath;
			}
			#endregion

			#region Properties
			/// <summary>
			/// ToIpAddress를 구하거나 설정합니다.
			/// </summary>
			public string ToIpAddress
			{
				get
				{
					return _toIpAddress;
				}
				set
				{
					_toIpAddress = value;
				}
			}

			/// <summary>
			/// FilePath를 구하거나 설정합니다.
			/// </summary>
			public string[] FilePaths
			{
				get
				{
					return _filePath;
				}
				set
				{
					_filePath = value;
				}
			}
			#endregion
		}
		#endregion
		#endregion
		#endregion

		#region Execute Command
		/// <summary>
		/// Client <seealso cref="iDASiT.FX.Win.Messenger.ICommand"/> 을 구현한 class의 명령을 실행 합니다.
		/// </summary>
		/// <param name="commandAssemblyPath">ICommand를 구현하여 실행할 명령이 있는 assembly의 fullPath</param>
		/// <param name="remotePath">commandAssembly가 저장될 원격지의 path</param>
		/// <param name="toIpAddresses">명령을 실행할 Client 입니다.</param>
		/// <returns></returns>
		public void ExecuteCommand(string commandAssemblyPath, string remotePath, params string[] toIpAddresses)
		{
			// NOTICE:	1. 생성된 객체를 직접 전송하면 server에도 동일한 객체 파일이 있어야 한다.
			//			2. CommandBase를 구현한 class => serialize => PutStream =(transfer)=> GetStream => deserialize => ExecuteCommand()
			// 1, 2 모두 동일 object를 가지고 있어야 하므로 server/client에서 로드하는 class를 양쪽다 가지고 있어야 한다.
			// 따라서 특정 위치로 파일을 전송한뒤 해당 assembly를 laod해서 ICommand.ExecuteCommand()를 실행하도록 한다.
			// by KIMKIWON\xyz37 in 2008년 8월 27일 수요일 오후 5:42
			InternalSendMessage(EventSource.ExecuteCommand, Path.GetFileName(commandAssemblyPath), remotePath, toIpAddresses);
		}
		#endregion

		#region Unidirection Send Message to Server for Logging
		/// <summary>
		/// Exception Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="message">exceptino message</param>
		/// <param name="exception">전달하려는 Exception</param>
		/// <param name="userId">작업중인 userId</param>
		/// <param name="toIpAddress">보내는 Server IpAddress</param>
		public void SendExceptionMessage(string message, Exception exception, string userId, string toIpAddress)
		{
			WriteLog(EventSource.Exception,
				SendType.SendTo,
				ClientContext.ClientIpAddress,
				toIpAddress,
				userId,
				message,
				exception.ToString());
		}

		/// <summary>
		/// Log Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="logData"></param>
		/// <param name="userId">작업중인 userId</param>
		/// <param name="additionalInfo">추가 정보</param>
		/// <param name="toIpAddress">보내는 Server IpAddress</param>
		public void SendLogMessage(string logData, string userId, string additionalInfo, string toIpAddress)
		{
			WriteLog(EventSource.Log, SendType.SendTo, ClientContext.ClientIpAddress, toIpAddress, userId, string.Format("{0}/{1}", logData, userId), additionalInfo);
		}

		/// <summary>
		/// Client Form Open Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="userId">작업중인 userId</param>
		/// <param name="formId">FormId</param>
		/// <param name="formPath">Form Path</param>
		/// <param name="formVersion">Form의 Version</param>
		/// <param name="toIpAddress">보내는 Server IpAddress</param>
		public void SendFormOpenMessage(string userId, string formId, string formPath, string formVersion, string toIpAddress)
		{
			WriteLog(EventSource.FormOpen, SendType.SendTo, ClientContext.ClientIpAddress, toIpAddress, userId, string.Format(@"{0}\{1}\{2}", formId, formPath, formVersion));
		}

		/// <summary>
		/// Client Form Close Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="userId">작업중인 userId</param>
		/// <param name="formId">FormId</param>
		/// <param name="formPath">Form Path</param>
		/// <param name="formVersion">Form의 Version</param>
		/// <param name="toIpAddress">보내는 Server IpAddress</param>
		public void SendFormCloseMessage(string userId, string formId, string formPath, string formVersion, string toIpAddress)
		{
			WriteLog(EventSource.FormClose, SendType.SendTo, ClientContext.ClientIpAddress, toIpAddress, userId, string.Format(@"{0}\{1}\{2}", formId, formPath, formVersion));
		}

		/// <summary>
		/// Client Form Active Message를 서버로 전달합니다.<remarks>서버로의 단방향 메세지만을 전달합니다.</remarks>
		/// </summary>
		/// <param name="userId">작업중인 userId</param>
		/// <param name="formId">FormId</param>
		/// <param name="toIpAddress">보내는 Server IpAddress</param>
		public void SendFormActiveMessage(string userId, string formId, string toIpAddress)
		{
			WriteLog(EventSource.FormActivation, SendType.SendTo, ClientContext.ClientIpAddress, toIpAddress, userId, formId);
		}
		#endregion

		#region Log
		private void DebugLog(string format, params object[] args)
		{
#if CONSOLE && DEBUG
			Console.WriteLine(format, args);
#endif
		}

		/// <summary>
		/// 비동기 Logging을 지원하기 위한 Delelgate
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="eventSource"></param>
		/// <param name="sendType"></param>
		/// <param name="fromIpAddress"></param>
		/// <param name="toIpAddress"></param>
		/// <param name="message"></param>
		/// <param name="fileSize"></param>
		/// <param name="additionalInfo"></param>
		private delegate void WriteLogDelegate(EventSource eventSource, SendType sendType, string fromIpAddress, string toIpAddress, string userId, string message, long fileSize, params object[] additionalInfo);

		/// <summary>
		/// Log를 특정 양식에 따라서 Write 시킨다.
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="eventSource"></param>
		/// <param name="sendType"></param>
		/// <param name="fromIpAddress"></param>
		/// <param name="toIpAddress"></param>
		/// <param name="message"></param>
		/// <param name="additionalInfo"></param>
		public void WriteLog(EventSource eventSource, SendType sendType, string fromIpAddress, string toIpAddress, string userId, string message, params object[] additionalInfo)
		{
			long fileSize = 0L;

			WriteLog(eventSource, sendType, fromIpAddress, toIpAddress, userId, message, fileSize, additionalInfo);
		}

		/// <summary>
		/// Log를 특정 양식에 따라서 Write 시킨다.
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="eventSource"></param>
		/// <param name="sendType"></param>
		/// <param name="fromIpAddress"></param>
		/// <param name="toIpAddress"></param>
		/// <param name="message"></param>
		/// <param name="fileSize"></param>
		/// <param name="additionalInfo"></param>
		public void WriteLog(EventSource eventSource, SendType sendType, string fromIpAddress, string toIpAddress, string userId, string message, long fileSize, params object[] additionalInfo)
		{
			WriteLogDelegate writeLogDelegate = new WriteLogDelegate(InternalWriteLog);

			// Log 입력시 쓰기 지연을 방지하기 위해 비동기로 실행한다.
			writeLogDelegate.BeginInvoke(eventSource, sendType, fromIpAddress, toIpAddress, userId, message, fileSize, additionalInfo, null, null);
		}

		/// <summary>
		/// Log를 특정 양식에 따라서 Write 시킨다.
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="eventSource"></param>
		/// <param name="sendType"></param>
		/// <param name="fromIpAddress"></param>
		/// <param name="toIpAddress"></param>
		/// <param name="message"></param>
		/// <param name="fileSize"></param>
		/// <param name="additionalInfo"></param>
		private void InternalWriteLog(EventSource eventSource, SendType sendType, string fromIpAddress, string toIpAddress, string userId, string message, long fileSize, params object[] additionalInfo)
		{
#if CONSOLE
			string format = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}";

			for (int i = 0; i < additionalInfo.Length; i++)
			{
				if (additionalInfo[i] == null)
					continue;

				format = string.Format("{0}\t{1}", format, additionalInfo[i].ToString());
			}

			Console.WriteLine(format,
					eventSource,
					sendType,
					fromIpAddress,
					toIpAddress,
					userId,
					message);
#endif

			List<OracleParameter> inParams = new List<OracleParameter>();
			string spName = string.Empty;
			string spName2 = string.Empty;
			int result = -1;

			// toIpAddress는 Not Null type이다.
			if (string.IsNullOrEmpty(toIpAddress) == true)
				toIpAddress = SendType.BroadCast.ToString();

			OracleParameter opFromIp = new OracleParameter();

			opFromIp.DbType = DbType.String;
			opFromIp.OracleType = OracleType.VarChar;
			opFromIp.ParameterName = "P_FromIpAddress";
			opFromIp.Value = fromIpAddress;

			OracleParameter opToIp = new OracleParameter();

			opToIp.DbType = DbType.String;
			opToIp.OracleType = OracleType.VarChar;
			opToIp.ParameterName = "P_ToIpAddress";
			opToIp.Value = toIpAddress;

			OracleParameter opUserId = new OracleParameter();

			opUserId.DbType = DbType.String;
			opUserId.OracleType = OracleType.VarChar;
			opUserId.ParameterName = "P_UserId";

			if (string.IsNullOrEmpty(userId) == true)
				userId = message;
            	
			opUserId.Value = userId;

			OracleParameter opEventSource = new OracleParameter();

			opEventSource.DbType = DbType.String;
			opEventSource.OracleType = OracleType.VarChar;
			opEventSource.ParameterName = "P_EventSource";
			opEventSource.Value = eventSource.ToString();

			OracleParameter opMessage = new OracleParameter();

			opMessage.DbType = DbType.String;
			opMessage.OracleType = OracleType.VarChar;
			opMessage.ParameterName = "P_Message";
			opMessage.Value = message;

			OracleParameter opHostName = new OracleParameter();

			opHostName.DbType = DbType.String;
			opHostName.OracleType = OracleType.VarChar;
			opHostName.ParameterName = "P_HostName";
			opHostName.Value = ClientContext.HostName;

			StringBuilder addInfos = new StringBuilder();
			string addInfo = string.Empty;

			if (additionalInfo != null)
			{
				if (additionalInfo.Length == 1)
					addInfo = Convert.ToString(additionalInfo[0]);
				else if (additionalInfo.Length > 1)
				{
					foreach (object info in additionalInfo)
						addInfos.AppendFormat("{0};", info);

					addInfo = addInfos.ToString(0, addInfos.Length - 1);
				}
			}

			OracleParameter opAddInfo = new OracleParameter();

			opAddInfo.DbType = DbType.String;
			opAddInfo.OracleType = OracleType.VarChar;
			opAddInfo.ParameterName = "P_AdditionalInfo";
			opAddInfo.Value = addInfo;

			try
			{
				if (_databaseHelperProxy == null)
					_databaseHelperProxy = new DatabaseHelperProxy();

				switch (eventSource)
				{
					case EventSource.Message:
					case EventSource.PutFile:
					case EventSource.PutFileCompleted:
					case EventSource.GetFileSendResponse:
					case EventSource.GetFileSendRequest:
					case EventSource.GetFileList:
					case EventSource.ExecuteCommand:
					case EventSource.Deploy:
						spName = "MG_PC_Messages";

						OracleParameter opSendType = new OracleParameter();

						opSendType.DbType = DbType.String;
						opSendType.OracleType = OracleType.VarChar;
						opSendType.ParameterName = "P_SendType";
						opSendType.Value = sendType.ToString();

						OracleParameter opFileSize = new OracleParameter();

						opFileSize.DbType = DbType.Int32;
						opFileSize.OracleType = OracleType.Number;
						opFileSize.ParameterName = "P_FileSize";
						opFileSize.Value = fileSize;

						if (sendType == SendType.BroadCast)
							opAddInfo.Value = string.Empty;

						switch (eventSource)
						{
							case EventSource.GetFileSendResponse:
								opAddInfo.Value = string.Empty;

								break;
							case EventSource.GetFileSendRequest:
								// 추가정보에 여러 Ip에서 가져오면 해당 IpAddress를 입력한다.
								opAddInfo.Value = toIpAddress == SendType.BroadCast.ToString() ? Convert.ToString(additionalInfo[0]) : string.Empty;
								// 가져올 파일의 개수를 저장한다.
								opFileSize.Value = Convert.ToInt32(additionalInfo[1]);

								break;
						}

						inParams.Add(opEventSource);
						inParams.Add(opSendType);
						inParams.Add(opFromIp);
						inParams.Add(opToIp);
						inParams.Add(opMessage);
						inParams.Add(opUserId);
						inParams.Add(opFileSize);
						inParams.Add(opAddInfo);

						if (eventSource == EventSource.PutFileCompleted)
						{
							opAddInfo.Value = string.Empty;
							opFromIp.Value = toIpAddress;
							opToIp.Value = fromIpAddress;

							List<FileInfo> fileInfos = additionalInfo[0] as List<FileInfo>;

							if (fileInfos != null)
							{
								for (int j = 0; j < fileInfos.Count; j++)
								{
									opMessage.Value = string.Format(@"{0}\{1}", message, fileInfos[j].Name);
									opFileSize.Value = fileInfos[j].Length;

									// 마지막 하나는 switch 문 밖에서 실행
									if (j <= fileInfos.Count - 2)
									{
										result = _databaseHelperProxy.ExecuteProcedure(spName, inParams);
										WriteServiceLog(inParams, result, spName);
									}
								}
							}
						}

						break;
					case EventSource.Login:
						spName = "MG_PC_ActivatedLogin";

						inParams.Add(opHostName);
						inParams.Add(opFromIp);
						inParams.Add(opToIp);
						inParams.Add(opUserId);

						break;
					case EventSource.AppUpdate:
					case EventSource.Logout:
					case EventSource.AbnormalLogout:
						spName = "MG_PD_ActivatedLogin";

						inParams.Add(opEventSource);
						inParams.Add(opFromIp);
						inParams.Add(opUserId);

						// Logout시 UserAction table의 이력을 삭제 해야 한다.
						// FormClosing시에 동일한 Routine으로 처리한다.
						break;
					case EventSource.Exception:
					case EventSource.Log:
						spName = "MG_PC_LogMessages";

						inParams.Add(opEventSource);
						inParams.Add(opFromIp);
						inParams.Add(opToIp);
						inParams.Add(opMessage);
						inParams.Add(opUserId);
						inParams.Add(opAddInfo);

						break;
					case EventSource.FormOpen:
					case EventSource.FormClose:
					case EventSource.FormActivation:
						string formId = string.Empty;
						string formPath = string.Empty;
						string formVersion = string.Empty;
						string[] formInfo = message.Split('\\');

						if (formInfo.Length >= 2)
						{
							formId = formInfo[0];
							formPath = formInfo[1];
						}

						if (formInfo.Length == 3)
							formVersion = formInfo[2];

						OracleParameter opFormId = new OracleParameter();

						opFormId.DbType = DbType.String;
						opFormId.OracleType = OracleType.VarChar;
						opFormId.ParameterName = "P_FormId";
						opFormId.Value = formId;

						OracleParameter opFormPath = new OracleParameter();

						opFormPath.DbType = DbType.String;
						opFormPath.OracleType = OracleType.VarChar;
						opFormPath.ParameterName = "P_FormPath";
						opFormPath.Value = formPath;

						OracleParameter opFormVer = new OracleParameter();

						opFormVer.DbType = DbType.String;
						opFormVer.OracleType = OracleType.VarChar;
						opFormVer.ParameterName = "P_FormVersion";
						opFormVer.Value = formVersion;

						OracleParameter opAction = new OracleParameter();

						opAction.DbType = DbType.String;
						opAction.OracleType = OracleType.VarChar;
						opAction.ParameterName = "P_Action";
						
						OracleParameter opActiveFormId = new OracleParameter();

						opActiveFormId.DbType = DbType.String;
						opActiveFormId.OracleType = OracleType.VarChar;
						opActiveFormId.ParameterName = "P_ActiveFormId";

						switch (eventSource)
						{
							case EventSource.FormOpen:
								spName = "MG_PC_UserAction";

								opAction.Value = FormAction.Open.ToString();

								inParams.Add(opFromIp);
								inParams.Add(opToIp);
								inParams.Add(opUserId);
								inParams.Add(opFormId);
								inParams.Add(opFormPath);
								inParams.Add(opFormVer);
								inParams.Add(opAction);
								
								break;
							case EventSource.FormClose:
								spName = "MG_PD_UserAction";

								opActiveFormId.Value = string.Empty;

								inParams.Add(opFromIp);
								inParams.Add(opUserId);
								inParams.Add(opFormId);
								inParams.Add(opActiveFormId);

								break;
							case EventSource.FormActivation:
								spName = "MG_PU_UserAction";

								opActiveFormId.Value = message;

								inParams.Add(opFromIp);
								inParams.Add(opUserId);
								inParams.Add(opActiveFormId);

								break;
						}

						break;
				}

				result = _databaseHelperProxy.ExecuteProcedure(spName, inParams);
				WriteServiceLog(inParams, result, spName);
			}
			catch (Exception ex)
			{
#if CONSOLE
				Console.WriteLine("WriteLog Exception 발생 : {0}", ex.Message);
#endif
#if SERVICE_LOG
				WriteServiceLog(null, "WriteLog Exception 발생 : {0}", ex.Message);
#endif
				StringBuilder param = new StringBuilder();

				foreach (OracleParameter op in inParams)
					param.Append(string.Format("{0}:{1}{2}", op.ParameterName, op.Value, Environment.NewLine));

				string logMessage = string.Format("EventSource : {1}, FromIp : {2}{0}SP : {3}, SP2 : {4}{0}{5}{0}{6}{0}{7}", 
                                     Environment.NewLine, 
									 eventSource.ToString(),
									 fromIpAddress,
									 spName,
									 spName2,
									 param.ToString(),
                                     ex.Message, 
                                     ex.StackTrace);

				EventLog.WriteEntry(EventLogName,
					logMessage,
					EventLogEntryType.Error,
					GetFilePort);
			}
		}

		private void WriteServiceLog(List<OracleParameter> inParams, params object[] args)
		{
			string format = "SP result : {0} with {1}";

			WriteServiceLog(inParams, format, args);
		}

		private void WriteServiceLog(List<OracleParameter> inParams, string format, params object[] args)
		{
#if SERVICE_LOG
			if (inParams != null)
			{
				StringBuilder param = new StringBuilder();

				foreach (OracleParameter op in inParams)
					param.Append(string.Format("{0}:{1},", op.ParameterName, op.Value));

				Console.WriteLine("\t{0}", param.ToString());
			}

			Console.WriteLine(format, args);
#endif
		}
		#endregion
	}
}
