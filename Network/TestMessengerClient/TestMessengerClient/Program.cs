using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;

namespace TestMessengerClient
{
	class Program
	{
		private const int _packSize = 1024;
		private static TcpClient _tcpClient;
		private static byte[] _recvBytes = new byte[_packSize];
				
		static void Main(string[] args)
		{
			try
			{
				_tcpClient = new TcpClient("localhost", 1980);

				Console.WriteLine("서버에 접속됨");

				AsyncCallback asyncCallback = new AsyncCallback(GetStreamMsg);

				_tcpClient.GetStream().BeginRead(_recvBytes, 0, _packSize, asyncCallback, null);

				//NetworkStream networkStream = tcpClient.GetStream();
				SendMessage("xyz37");
				SendMessage(string.Format("메세지 서버에 접속했네{0}", DateTime.Now));

				Console.WriteLine("Type 'q' to quit");

				while (Console.Read() != 'q')
				{
					
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		private static void GetStreamMsg(IAsyncResult ar)
		{
			int intCount = -1;

			lock (_tcpClient.GetStream())
			{
				intCount = _tcpClient.GetStream().EndRead(ar);
			}

			if (intCount < -1)
				_tcpClient.Close();

			string data = Encoding.Default.GetString(_recvBytes, 0, _packSize);
			string[] messages = data.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);

			foreach (string item in messages)
			{
				Console.WriteLine(item);
			}			

			// message가 종료 되기 전까지 계속 해서 수신한다.
			_tcpClient.GetStream().BeginRead(_recvBytes, 0, _packSize, new AsyncCallback(GetStreamMsg), null);
		}

		private static void SendMessage(string msg)
		{
			int packSize = 1024;
			byte[] recvBytes = new byte[packSize];
			Encoding defaultEncoding = Encoding.Default;
			NetworkStream nStream = _tcpClient.GetStream();

			msg += Environment.NewLine;

			byte[] sendBytes = defaultEncoding.GetBytes(msg.ToCharArray());

			nStream.Write(sendBytes, 0, sendBytes.Length);
			nStream.Flush();
		}
	}
}
