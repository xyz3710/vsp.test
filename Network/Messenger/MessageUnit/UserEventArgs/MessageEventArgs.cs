using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
namespace MessageUnit
{
	/// <summary>
	/// Class MessageEventArgs.
	/// </summary>
	[DebuggerDisplay("Token:{Token}, Unit:{Unit}", Name = "MessageEventArgs")]
	[System.Diagnostics.DebuggerStepThrough]
	public class MessageEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the ReceiveMessageEventArgs class.
		/// </summary>
		/// <param name="token">The token.</param>
		/// <param name="unit">The unit.</param>
		public MessageEventArgs(Token token, Unit unit)
		{
			Token = token;
			Unit = unit;
		}

		/// <summary>
		/// Token를 구하거나 설정합니다.
		/// </summary>
		/// <value>Token를 반환합니다.</value>
		public Token Token
		{
			get;
			set;
		}

		/// <summary>
		/// 메세지를 발생시킨 Client를 구하거나 설정합니다.
		/// </summary>
		public Unit Unit
		{
			get;
			set;
		}
	}
}
