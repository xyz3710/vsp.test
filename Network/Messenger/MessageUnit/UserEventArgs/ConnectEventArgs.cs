using System;
using System.Collections.Generic;
using System.Text;

namespace MessageUnit
{
	/// <summary>
	/// 
	/// </summary>
	public class ConnectEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the ReceiveMessageEventArgs class.
		/// </summary>
		/// <param name="unit">The unit.</param>
		public ConnectEventArgs(Unit unit)
		{
			Unit = unit;
		}

		/// <summary>
		/// 접속된 유닛을 구하거나 설정합니다.
		/// </summary>
		public Unit Unit
		{
			get;
			set;
		}
	}
}
