using System;
using System.Collections.Generic;
using System.Text;

namespace MessageUnit
{
	/// <summary>
	/// Class ClientErrorEventArgs.
	/// </summary>
	public class UnitErrorEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the ErrorEventArgs class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="exception">The exception.</param>
		public UnitErrorEventArgs(string message, Exception exception)
		{
			Message = message;
			Exception = exception;
		}

		/// <summary>
		/// Message를 구하거나 설정합니다.
		/// </summary>
		public string Message
		{
			get;
			set;
		}

		/// <summary>
		/// Exception를 구하거나 설정합니다.
		/// </summary>
		public Exception Exception
		{
			get;
			set;
		}
	}
}
