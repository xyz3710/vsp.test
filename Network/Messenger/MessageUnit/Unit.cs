using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
namespace MessageUnit
{
	/// <summary>
	/// Class Unit.
	/// </summary>
	[DebuggerDisplay("Key:{Key}, IpAddress:{IpAddress}IsConnected:{IsConnected}, UserId:{UserId}", Name = "Unit")]
	public class Unit
	{
		#region MessageReceived Event
		/// <summary>
		/// MessageReceived 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="e">이벤트 데이터가 들어 있는 MessageReceived EventArgs 입니다.</param>
		public delegate void MessageReceivedEventHandler(object sender, MessageEventArgs e);

		/// <summary>
		/// MessageReceived를 하면 발생합니다.
		/// </summary>
		public event MessageReceivedEventHandler MessageReceived;

		/// <summary>
		/// MessageReceived 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 MessageReceived EventArgs 입니다.</param>
		protected void OnMessageReceived(MessageEventArgs e)
		{
			if (MessageReceived != null)
			{
				MessageReceived(this, e);
			}
		}
		#endregion

		#region Connected Event
		/// <summary>
		/// Connected 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="e">이벤트 데이터가 들어 있는 Connect EventArgs 입니다.</param>
		public delegate void ConnectedEventHandler(object sender, ConnectEventArgs e);

		/// <summary>
		/// Connected를 하면 발생합니다.
		/// </summary>
		public event ConnectedEventHandler Connected;

		/// <summary>
		/// Connected 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 Connect EventArgs 입니다.</param>
		protected void OnConnected(ConnectEventArgs e)
		{
			if (Connected != null)
			{
				Connected(this, e);
			}
		}
		#endregion

		#region Disconnected Event
		/// <summary>
		/// Disconnected 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="e">이벤트 데이터가 들어 있는 Connect EventArgs 입니다.</param>
		public delegate void DisconnectedEventHandler(object sender, ConnectEventArgs e);

		/// <summary>
		/// Disconnected를 하면 발생합니다.
		/// </summary>
		public event DisconnectedEventHandler Disconnected;

		/// <summary>
		/// Disconnected 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 Connect EventArgs 입니다.</param>
		protected void OnDisconnected(ConnectEventArgs e)
		{
			if (Disconnected != null)
			{
				Disconnected(this, e);
			}
		}
		#endregion

		#region Error Event
		/// <summary>
		/// Error 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="e">이벤트 데이터가 들어 있는 Error EventArgs 입니다.</param>
		public delegate void ErrorEventHandler(object sender, UnitErrorEventArgs e);

		/// <summary>
		/// Error를 하면 발생합니다.
		/// </summary>
		public event ErrorEventHandler Error;

		/// <summary>
		/// Error 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 Error EventArgs 입니다.</param>
		protected void OnError(UnitErrorEventArgs e)
		{
			if (Error != null)
			{
				Error(this, e);
			}
		}
		#endregion

		#region Login Event
		/// <summary>
		/// Login 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="userId">The user identifier.</param>
		public delegate void LoginEventHandler(object sender, string userId);

		/// <summary>
		/// Login를 하면 발생합니다.
		/// </summary>
		public event LoginEventHandler Login;

		/// <summary>
		/// Login 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="userId">The user identifier.</param>
		protected void OnLogin(string userId)
		{
			if (Login != null)
			{
				Login(this, userId);
			}
		}
		#endregion

		#region Logout Event
		/// <summary>
		/// Logout 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="userId">The user identifier.</param>
		public delegate void LogoutEventHandler(object sender, string userId);

		/// <summary>
		/// Logout를 하면 발생합니다.
		/// </summary>
		public event LogoutEventHandler Logout;

		/// <summary>
		/// Logout 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="userId">The user identifier.</param>
		protected void OnLogout(string userId)
		{
			if (Logout != null)
			{
				Logout(this, userId);
			}
		}
		#endregion

		#region Fields
		private TcpClient _tcpClient;
		private Encoding _encoding;
		private int _packSize;
		#endregion

		#region Constructors
		/// <summary>
		/// Client 클래스의 새로운 인스턴스를 초기화 합니다.
		/// </summary>
		public Unit()
		{
			_encoding = Encoding.Default;
			_packSize = 1024;
			Key = Guid.NewGuid().ToString().Replace("-", string.Empty);
		}
		#endregion

		#region Properties
		/// <summary>
		/// 접속된 Key를 구하거나 설정합니다.
		/// </summary>
		public string Key
		{
			get;
			set;
		}

		/// <summary>
		/// 접속된 Client의 IP Address를 구합니다.
		/// </summary>
		public string IpAddress
		{
			get
			{
				string ipAddress = IPAddress.None.ToString();

				if (_tcpClient.Connected == true)
				{
					ipAddress = ((IPEndPoint)_tcpClient.Client.RemoteEndPoint).Address.ToString();
				}

				return ipAddress;
			}
		}

		/// <summary>
		/// 접속된 Client의 TCP Port를 구합니다.
		/// </summary>
		public int Port
		{
			get
			{
				int port = 0;

				if (_tcpClient.Connected == true)
				{
					port = ((IPEndPoint)_tcpClient.Client.RemoteEndPoint).Port;
				}

				return port;
			}
		}

		/// <summary>
		/// 원격 호스트에 연결되어 있는지 여부를 나타내는 값을 가져옵니다.
		/// </summary>
		public bool IsConnected
		{
			get;
			private set;
		}

		/// <summary>
		/// 원격 호스트에 Id를 가지고 로그인 했는지 여부를 나타내는 값을 구합니다.
		/// </summary>
		public bool IsLogin
		{
			get;
			private set;
		}

		/// <summary>
		/// 접속된 UserId를 구하거나 설정합니다.
		/// </summary>
		public string UserId
		{
			get;
			set;
		}

		/// <summary>
		/// Connect/Disconnected 시 event와 함께 Message를 전송하는지 여부를 구하거나 설정합니다.
		/// </summary>
		/// <value>Connect/Disconnected 시 Message 전송 여부를 반환합니다.</value>
		public bool SendConnectMessage
		{
			get;
			set;
		}
		#endregion

		#region ToString
		public override string ToString()
		{
			return string.Format("{0}, {1}", Key, Port);
		}
		#endregion

		#region Connect
		/// <summary>
		/// TcpClient가 연결된 뒤 연결을 설정합니다.
		/// </summary>
		/// <param name="ipAddress">Server IPAddress</param>
		/// <param name="port">Server TCP port</param>
		/// <param name="userId">The userId(Default GUID).</param>
		public void Connect(IPAddress ipAddress, int port, string userId = "")
		{
			Connect(new TcpClient(ipAddress.ToString(), port), userId);
		}

		/// <summary>
		/// TcpClient가 연결된 뒤 연결을 설정합니다.
		/// </summary>
		/// <param name="tcpClient">서버와 연결한 TcpClient</param>
		/// <param name="userId">The userId(Default GUID).</param>
		public void Connect(TcpClient tcpClient, string userId)
		{
			_tcpClient = tcpClient;

			// 비동기 방식으로 메세지 수신 시작
			AsyncCallback getMessageCallback = new AsyncCallback(GetAsyncStreamMessage);
			var token = new Token
			{
				MessageType = MessageTypes.Connect,
				From = userId,
				Value = userId,
			};
			byte[] receiveBytes = new byte[_packSize];
			var buffer = token.GetBuffer();
			buffer.CopyTo(receiveBytes, 0);

			try
			{
				_tcpClient.GetStream().BeginRead(receiveBytes, 0, receiveBytes.Length, getMessageCallback, receiveBytes);
				IsConnected = true;
				OnConnected(new ConnectEventArgs(this));

				if (SendConnectMessage == true)
				{
					token = new Token
					{
						MessageType = MessageTypes.Connect,
						From = Key,
						Value = UserId,
					};

					SendMessage(token);
				}
			}
			catch (Exception ex)
			{
				IsConnected = false;
				OnError(new UnitErrorEventArgs("연결은 되었지만 메세지를 수신할 수 없습니다.\r\n종료 후 다시 시도해 주십시오.", ex));
			}
		}
		#endregion

		#region Disconnect
		/// <summary>
		/// Client를 Disconnect 시킵니다.
		/// </summary>
		public void Disconnect()
		{
			try
			{
				if (_tcpClient != null && _tcpClient.Connected == true)
				{
					OnDisconnected(new ConnectEventArgs(this));

					if (SendConnectMessage == true)
					{
						var token = new Token
						{
							MessageType = MessageTypes.Disconnected,
							From = Key,
							Value = UserId,
						};

						SendMessage(token);
					}

					_tcpClient.Close();
				}
			}
			catch (Exception ex)
			{
				OnError(new UnitErrorEventArgs("Disconnect 도중 에러가 발생하였습니다.", ex));
			}
			finally
			{
				IsConnected = false;
				IsLogin = false;
			}
		}
		#endregion

		#region Login / Logout
		/// <summary>
		/// 접속한 뒤 사용자 계정으로 로그인 합니다.
		/// </summary>
		/// <param name="userId">The user identifier.</param>
		/// <returns></returns>
		public bool LoginByUser(string userId)
		{
			bool result = false;
			var token = new Token
			{
				MessageType = MessageTypes.Login,
				From = Key,
				Value = userId,
			};

			if (SendMessage(token) == true)	// Login한 사용자 계정을 보낸다.
			{
				IsLogin = true;
				result = true;
				UserId = userId;
				OnLogin(userId);
			}

			return result;
		}

		/// <summary>
		/// 연결 종료 전 로그 아웃 합니다.
		/// </summary>
		/// <returns></returns>
		public bool LogoutByUser()
		{
			bool result = false;
			var token = new Token
			{
				MessageType = MessageTypes.Logout,
				From = Key,
				Value = UserId,
			};

			if (SendMessage(token) == true)
			{
				IsLogin = false;
				result = true;
				OnLogout(UserId);
				UserId = string.Empty;
			}

			return result;
		}
		#endregion

		#region SendMessage
		/// <summary>
		/// 현재 TcpClient로 메세지를 전송합니다.
		/// </summary>
		/// <param name="token"></param>
		/// <returns></returns>
		public bool SendMessage(Token token)
		{
			if (IsConnected == false)
			{
				return false;
			}

			bool result = false;

			try
			{
				if (_tcpClient != null && _tcpClient.Connected == true)
				{
					NetworkStream ns = _tcpClient.GetStream();
					byte[] sendBytes = token.GetBuffer();

					ns.Write(sendBytes, 0, sendBytes.Length);
					ns.Flush();
					result = true;
				}
			}
			catch (Exception ex)
			{
				OnError(new UnitErrorEventArgs("메세지 전송 도중 에러가 발생하여 메세지를 전송할 수 없습니다.", ex));
			}

			return result;
		}
		#endregion

		#region Private methods
		private void GetAsyncStreamMessage(IAsyncResult ar)
		{
			if (IsConnected == false)
			{
				return;
			}

			long inCount;

			try
			{
				// Thread간에 충돌 나지 않게 Stream Lock
				lock (_tcpClient.GetStream())
				{
					// Stream 수신이 완료되면 수신된 크기를 intCount에 할당
					inCount = _tcpClient.GetStream().EndRead(ar);
				}

				if (inCount < 1)		// server와 접속이 끊긴 상태
				{
					OnDisconnected(new ConnectEventArgs(this));
					_tcpClient.Close();
				}
				else
				{
					byte[] beforeReceiveBytes = (byte[])ar.AsyncState;
					var token = Token.GetObject<Token>(beforeReceiveBytes);

					if (token.MessageType == MessageTypes.Login)
					{
						Key = token.From;
						UserId = token.Value;
					}

					OnMessageReceived(new MessageEventArgs(token, this));

					lock (_tcpClient.GetStream())
					{
						AsyncCallback getMessageCallback = new AsyncCallback(GetAsyncStreamMessage);
						byte[] receiveBytes = new byte[_packSize];

						_tcpClient.GetStream().BeginRead(receiveBytes, 0, receiveBytes.Length, getMessageCallback, receiveBytes);
					}
				}
			}
			catch (Exception ex)
			{
				bool errorHandled = false;

				if (ex.InnerException != null)
				{
					SocketException sEx = ex.InnerException as SocketException;

					if (sEx != null)
					{
						errorHandled = true;
						OnError(new UnitErrorEventArgs(sEx.Message, ex));
						#region For Test
						/*
						if (sEx.SocketErrorCode == SocketError.ConnectionReset)
						{
							OnError("", ex);
							ConnectionClosed();		// 현재 연결은 원격 호스트에 의해 강제로 끊겼습니다.
						}
						*/
						#endregion
					}
				}

				if (errorHandled == false)
				{
					OnError(new UnitErrorEventArgs("메세지 수신시 오류가 발생하였습니다.", ex));
				}

				ConnectionClosed();
			}
		}

		private void ConnectionClosed()
		{
			try
			{
				if (_tcpClient != null && _tcpClient.Connected == true)
				{
					_tcpClient.Close();
				}

				OnDisconnected(new ConnectEventArgs(this));
			}
			catch (Exception ex)
			{
				OnError(new UnitErrorEventArgs("Disconnect 도중 에러가 발생하였습니다.", ex));
			}
		}
		#endregion
	}
}
