﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Diagnostics;
namespace MessageUnit
{
	[DebuggerDisplay("From:{From}, To:{To}, MessageType:{MessageType}, Value:{Value}, Tag:{Tag}", Name = "Token")]
	[Serializable]
	public class Token : XMessageBase
	{
		/// <summary>
		/// 보낸 Unit Key를 구하거나 설정합니다.
		/// </summary>
		/// <value>보낸 Unit Key를 반환합니다.</value>
		public string From
		{
			get;
			set;
		}
		/// <summary>
		/// 보낼 Unit Key를 구하거나 설정합니다.
		/// </summary>
		/// <value>보낼 Unit Key를 반환합니다.</value>
		public string To
		{
			get;
			set;
		}

		/// <summary>
		/// MessageType를 구하거나 설정합니다.
		/// </summary>
		/// <value>MessageType를 반환합니다.</value>
		public MessageTypes MessageType
		{
			get;
			set;
		}

		/// <summary>
		/// Value를 구하거나 설정합니다.
		/// </summary>
		/// <value>Value를 반환합니다.</value>
		public string Value
		{
			get;
			set;
		}

		/// <summary>
		/// Tag를 구하거나 설정합니다.
		/// </summary>
		/// <value>Tag를 반환합니다.</value>
		public object Tag
		{
			get;
			set;
		}
	}
}
