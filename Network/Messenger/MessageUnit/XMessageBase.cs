﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace MessageUnit
{
	[Serializable]
	public class XMessageBase
	{
		/// <summary>
		/// 전송하려는 객체를 Serialize 시켜서 byte array로 구합니다.
		/// </summary>
		/// <returns>Serialize된 byte array</returns>
		public byte[] GetBuffer()
		{
			IFormatter formatter = new BinaryFormatter();
			byte[] buffer = null;

			using (var ms = new MemoryStream())
			{
				formatter.Serialize(ms, this);
				buffer = ms.ToArray();
			}

			return buffer;
		}

		/// <summary>
		/// Serialized된 객체의 byte 배열을 Deserialize 시켜서 해당 객체를 구합니다.
		/// </summary>
		/// <typeparam name="T">새로운 버퍼를 구할 형식입니다.</typeparam>
		/// <param name="buffer">The buffer.</param>
		/// <returns>지정된 형식으로 캐스트된 T 객체</returns>
		public static T GetObject<T>(byte[] buffer)
			where T : XMessageBase
		{
			IFormatter formatter = new BinaryFormatter();
			var returnObject = default(T);

			using (var ms = new MemoryStream(buffer))
			{
				returnObject = (T)formatter.Deserialize(ms);
			}

			return returnObject;
		}
	}
}
