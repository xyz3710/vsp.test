using System;
using System.Collections.Generic;
using System.Text;

namespace MessageUnit
{
	/// <summary>
	/// 접속된 Client 정보를 관리합니다.
	/// </summary>
	public class Units : IEnumerable<Unit>
	{
		#region Added Event
		/// <summary>
		/// Added 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="e">이벤트 데이터가 들어 있는 Added EventArgs 입니다.</param>
		public delegate void AddedEventHandler(object sender, Unit e);

		/// <summary>
		/// Added를 하면 발생합니다.
		/// </summary>
		public event AddedEventHandler Added;

		/// <summary>
		/// Added 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 Added EventArgs 입니다.</param>
		protected void OnAdded(Unit e)
		{
			if (Added != null)
			{
				Added(this, e);
			}
		}
		#endregion

		#region Removed Event
		/// <summary>
		/// Removed 이벤트를 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">이벤트 소스입니다.</param>
		/// <param name="e">이벤트 데이터가 들어 있는 Removed EventArgs 입니다.</param>
		public delegate void RemovedEventHandler(object sender, Unit e);

		/// <summary>
		/// Removed를 하면 발생합니다.
		/// </summary>
		public event RemovedEventHandler Removed;

		/// <summary>
		/// Removed 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 Removed EventArgs 입니다.</param>
		protected void OnRemoved(Unit e)
		{
			if (Removed != null)
			{
				Removed(this, e);
			}
		}
		#endregion

		/// <summary>
		/// Initializes a new instance of the <see cref="Units"/> class.
		/// </summary>
		public Units()
		{
			Dic = new Dictionary<string, Unit>();
		}

		private Dictionary<string, Unit> Dic
		{
			get;
			set;
		}

		/// <summary>
		/// UserId로 Client 정보를 구합니다.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>Unit.</returns>
		public Unit this[string key]
		{
			get
			{
				if (Dic.ContainsKey(key) == true)
				{
					return Dic[key];
				}

				return null;
			}
		}

		/// <summary>
		/// Unit 사전에 추가합니다.
		/// </summary>
		/// <param name="unit">The unit.</param>
		public void Add(Unit unit)
		{
			if (unit == null)
			{
				return;
			}

			string key = unit.Key;

			if (Dic.ContainsKey(key) == false)
			{
				Dic.Add(key, unit);
				OnAdded(unit);
			}
		}

		/// <summary>
		/// 지정한 Unit 있으면 제거 합니다.
		/// </summary>
		/// <param name="unit">The unit.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool Remove(Unit unit)
		{
			if (unit != null && Dic.ContainsKey(unit.Key) == true)
			{
				OnRemoved(unit);
				return Dic.Remove(unit.Key);
			}

			return false;
		}

		/// <summary>
		/// 지정한 Unit의 Key가 있으면 제거 합니다.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		public bool Remove(string key)
		{
			if (Dic.ContainsKey(key) == true)
			{
				OnRemoved(Dic[key]);
				return Dic.Remove(key);
			}

			return false;
		}

		/// <summary>
		/// Removes all collection.
		/// </summary>
		/// <param name="action">The action.</param>
		public void RemoveAll(Action<Unit> action)
		{
			if (action != null)
			{
				List<string> keys = new List<string>(Dic.Keys);

				for (int i = 0; i < keys.Count; i++)
				{
					action(Dic[keys[i]]);
				}
			}
		}

		#region IEnumerable<Unit> 멤버

		/// <summary>
		/// 컬렉션을 반복하는 열거자를 반환합니다.
		/// </summary>
		/// <returns>컬렉션을 반복하는 데 사용할 수 있는 <see cref="T:System.Collections.Generic.IEnumerator`1" />입니다.</returns>
		public IEnumerator<Unit> GetEnumerator()
		{
			return Dic.Values.GetEnumerator();
		}

		#endregion

		#region IEnumerable 멤버

		/// <summary>
		/// 컬렉션을 반복하는 열거자를 반환합니다.
		/// </summary>
		/// <returns>컬렉션을 반복하는 데 사용할 수 있는 <see cref="T:System.Collections.IEnumerator" /> 개체입니다.</returns>
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return Dic.GetEnumerator();
		}

		#endregion
	}
}
