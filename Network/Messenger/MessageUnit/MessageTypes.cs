﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageUnit
{
	/// <summary>
	/// Enum MessageTypes
	/// </summary>
	public enum MessageTypes
	{
		/// <summary>
		/// 일반적인 메세지 입니다.
		/// </summary>
		Default = 0,
		/// <summary>
		/// The message from server
		/// </summary>
		MessageFromServer,
		/// <summary>
		/// The server force stopped
		/// </summary>
		ServerForceStopped,
		/// <summary>
		/// The connect
		/// </summary>
		Connect,
		/// <summary>
		/// The login
		/// </summary>
		Login,
		/// <summary>
		/// The logout
		/// </summary>
		Logout,
		/// <summary>
		/// The disconnected
		/// </summary>
		Disconnected,
		/// <summary>
		/// The command
		/// </summary>
		Command,
	}
}
