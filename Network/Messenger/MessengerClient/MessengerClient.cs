﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using MessageUnit;

namespace MessengerClient
{
	public partial class MessengerClient : Form
	{
		private const int PORT_NO = 19090;

		#region Fields
		private Unit _unit;
		private IPAddress _ipAddress;
		private Encoding _encoding;
		private System.Timers.Timer _connectionWatcher;
		#endregion

		#region Constructors
		public MessengerClient()
		{
			InitializeComponent();

			string serverIPAddress = LocalIp;

			txtServerIp.Text = serverIPAddress;
			IPAddress = IPAddress.Parse(serverIPAddress);
			_encoding = Encoding.Default;

			InitUnit();
			InitConnectionWatcher();

			SuspendLayout();

			Rectangle workingArea = Screen.GetWorkingArea(this);
			Location = new Point(workingArea.Width - Size.Width, workingArea.Height - Size.Height);

			ResumeLayout();
		}

		private void InitUnit()
		{
			_unit = new Unit();

			_unit.Connected += OnConnected;
			_unit.Disconnected += OnDisconnected;
			_unit.MessageReceived += OnMessageReceived;
			_unit.Error += OnError;
		}

		private void InitConnectionWatcher()
		{
			_connectionWatcher = new System.Timers.Timer
			{
				Interval = 1000,
			};
			_connectionWatcher.Elapsed += OnConnectionWatherTick;
		}

		#endregion

		private IPAddress IPAddress
		{
			get
			{
				_ipAddress = IPAddress.Parse(txtServerIp.Text);

				return _ipAddress;
			}
			set
			{
				_ipAddress = value;
				txtServerIp.Text = _ipAddress.ToString();
			}
		}

		private string UserId
		{
			get
			{
				return txtUserId.Text;
			}
			set
			{
				txtUserId.Text = value;
			}
		}

		private string LocalIp
		{
			get
			{
				// Get local ip address
				IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());
				string localIpAddress = string.Empty;

				foreach (IPAddress ipAddr in ipEntry.AddressList)
				{
					byte ipHeader = ipAddr.GetAddressBytes()[0];

					if (ipHeader == 0 || ipHeader == 127 || ipHeader == 169
						|| ipAddr.AddressFamily != AddressFamily.InterNetwork)
					{
						continue;
					}

					localIpAddress = Convert.ToString(ipAddr);

					if (localIpAddress != string.Empty)
					{
						break;
					}
				}

				if (localIpAddress == string.Empty)
				{
					localIpAddress = "127.0.0.1";
				}

				return localIpAddress;
			}
		}

		protected override void OnShown(EventArgs e)
		{
			UserId = DateTime.Now.ToString("mm_sss");
			ConnectedControl(false);
		}

		protected override void OnFormClosed(FormClosedEventArgs e)
		{
			Disconnect();
		}

		protected override void OnActivated(EventArgs e)
		{
			base.OnActivated(e);
			tbMessage.Focus();
		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			ConnectedControl(Connect());
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Disconnect();
			ConnectedControl(false);
		}

		private void btnSend_Click(object sender, EventArgs e)
		{
			SendMessage();
		}

		private void OnConnectionWatherTick(object sender, EventArgs e)
		{
			Invoke(new MethodInvoker(() =>
			{
				ConnectedControl(Connect(false));
			}));
		}

		private void ConnectedControl(bool connected)
		{
			if (connected == true)
			{
				_connectionWatcher.Stop();
				tbMessage.Select();
			}

			Invoke(new MethodInvoker(() =>
			{
				btnConnect.Enabled = !connected;
				btnClose.Enabled = connected;
				btnSendMessage.Enabled = connected;
			}));
		}

		private void OnConnected(object sender, ConnectEventArgs e)
		{
			Text = string.Format("{0},{1}", e.Unit.Key, e.Unit.UserId);
			ConnectedControl(true);
			DisplayMessage("서버에 접속됨");
		}

		private void OnDisconnected(object sender, ConnectEventArgs e)
		{
			ConnectedControl(false);
			DisplayMessage("서버와 접속 종료");
		}

		private void OnMessageReceived(object sender, MessageEventArgs e)
		{
			switch (e.Token.MessageType)
			{
				case MessageTypes.Default:
					DisplayMessage(string.Format("{0} -> {1}: {2}", e.Token.From, e.Token.To, e.Token.Value));

					break;
				case MessageTypes.MessageFromServer:
					DisplayMessage(string.Format("{0} -> {1}: {2}", "SERVER", e.Token.To, e.Token.Value));

					break;
				case MessageTypes.ServerForceStopped:
					_connectionWatcher.Start();
					DisplayMessage(e.Token.Value);

					break;
				case MessageTypes.Connect:
					DisplayMessage(string.Format("{0} 접속됨", e.Unit.IpAddress));

					break;
				case MessageTypes.Login:
					DisplayMessage(string.Format("{0} 사용자가 로그인 했습니다.", e.Token.Value));

					break;
				case MessageTypes.Logout:
					DisplayMessage(string.Format("{0} 사용자가 로그아웃 했습니다.", e.Token.Value));

					break;
				case MessageTypes.Disconnected:
					DisplayMessage(string.Format("{0} 종료됨", e.Unit.IpAddress));

					break;
				case MessageTypes.Command:

					break;
			}
		}

		private void OnError(object sender, UnitErrorEventArgs e)
		{
			DisplayMessage(string.Format("{0}: {1}", e.Message, e.Exception.Message));
		}

		private bool Connect(bool showExceptionMessage = true)
		{
			bool result = false;

			try
			{
				_unit.Connect(IPAddress, PORT_NO, UserId);
				var token = new Token
				{
					MessageType = MessageTypes.Login,
					Value = UserId,
				};

				_unit.LoginByUser(UserId);		// 접속 후 접속한 Client 정보를 전송한다.
				result = true;
			}
			catch (SocketException sEx)
			{
				if (sEx.ErrorCode == 10061)
				{
					if (showExceptionMessage == true)
					{
						// 대상 컴퓨터에서 연결을 거부했으므로 연결하지 못했습니다
						DisplayMessage(sEx.Message);
					}
				}
			}
			catch (Exception ex)
			{
				if (showExceptionMessage == true)
				{
					DisplayMessage(ex.Message);
				}
			}

			return result;
		}

		private void SendMessage()
		{
			string message = tbMessage.Text;

			if (message == string.Empty)
			{
				return;
			}

			var token = new Token
			{
				From = _unit.Key,
				Value = message,
			};

			if (_unit != null && _unit.SendMessage(token) == true)
			{
				DisplayMessage(message);
			}

			tbMessage.Clear();
		}

		private bool Disconnect(bool showExceptionMessage = true)
		{
			bool result = false;

			try
			{
				var token = new Token
				{
					MessageType = MessageTypes.Logout,
					Value = UserId,
				};

				_unit.LogoutByUser();
				_unit.Disconnect();
				result = true;

			}
			catch (SocketException sEx)
			{
				if (sEx.ErrorCode == 10061)
				{
					if (showExceptionMessage == true)
					{
						// 대상 컴퓨터에서 연결을 거부했으므로 연결하지 못했습니다
						DisplayMessage(sEx.Message);
					}
				}
			}
			catch (Exception ex)
			{
				if (showExceptionMessage == true)
				{
					DisplayMessage(ex.Message);
				}
			}

			return result;
		}

		private void DisplayMessage(string message)
		{
			Invoke(new MethodInvoker(() =>
			{
				lstMessage.Items.Insert(0, message);
			}));
		}
	}
}