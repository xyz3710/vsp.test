/**********************************************************************************************************************/
/*	Domain		:	MessengerServer.UserEventArgs.ErrorEventArgs
/*	Creator		:	MOBILE\xyz37
/*	Create		:	2008년 1월 12일 토요일 오전 1:04
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerServer.UserEventArgs
{
	/// <summary>
	/// 
	/// </summary>
	public class ClientErrorEventArgs : EventArgs
	{
		#region Fields
		private string _message;
		private Exception _exception;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the ErrorEventArgs class.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="exception"></param>
		public ClientErrorEventArgs(string message, Exception exception)
		{
			_message = message;
			_exception = exception;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Message를 구하거나 설정합니다.
		/// </summary>
		public string Message
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}

		/// <summary>
		/// Exception를 구하거나 설정합니다.
		/// </summary>
		public Exception Exception
		{
			get
			{
				return _exception;
			}
			set
			{
				_exception = value;
			}
		}
		#endregion        
	}
}
