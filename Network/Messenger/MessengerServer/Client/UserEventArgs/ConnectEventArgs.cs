/**********************************************************************************************************************/
/*	Domain		:	MessengerServer.UserEventArgs.ConnectEventArgs
/*	Creator		:	MOBILE\xyz37
/*	Create		:	2008년 1월 12일 토요일 오전 1:04
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerServer.UserEventArgs
{
	/// <summary>
	/// 
	/// </summary>
	public class ConnectEventArgs : EventArgs
	{
		#region Fields
		private Client _client;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the ReceiveMessageEventArgs class.
		/// </summary>
		/// <param name="client"></param>
		public ConnectEventArgs(Client client)
		{
			_client = client;
		}
		#endregion

		#region Properties
		/// <summary>
		/// 접속된 클라이언트를 구하거나 설정합니다.
		/// </summary>
		public Client Client
		{
			get
			{
				return _client;
			}
			set
			{
				_client = value;
			}
		}
		#endregion
	}
}
