using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using MessengerServer;
using MessageUnit;

namespace Messenger
{
	/// <summary>
	/// Messenger Server 클래스 입니다.
	/// </summary>
	public partial class MessengerServer : Form
	{
		private const int PORT_NO = 19090;

		#region Fields
		private TcpListener _tcpListener;
		private IPAddress _ipAddress;
		private Units _units;
		#endregion

		/// <summary>
		/// MessengerServer 클래스 인스턴스를 생성합니다.
		/// </summary>
		public MessengerServer()
		{
			InitializeComponent();

			_ipAddress = IPAddress.Any;
			_units = new Units();

			_units.Added += OnAdded;
			_units.Removed += OnRemoved;
		}

		private void OnAdded(object sender, Unit e)
		{
			AddClient(e);
		}

		private void OnRemoved(object sender, Unit e)
		{
			RemoveClient(e);
		}

		protected override void OnShown(EventArgs e)
		{
			btnStart.PerformClick();
			btnBroadCasting.Click += (senderObject, ea) =>
			{
				lstClients.SelectedIndex = -1;
			};
			btnBroadCasting.Click += (senderObject, ea) =>
			{
				tbMessage.Focus();
			};
			lstClients.SelectedIndexChanged += (senderObject, ea) =>
			{
				tbMessage.Focus();
			};
		}

		protected override void OnFormClosed(FormClosedEventArgs e)
		{
			StopServer();
		}

		protected override void OnActivated(EventArgs e)
		{
			base.OnActivated(e);
			tbMessage.Focus();
		}

		private void btnStart_Click(object sender, EventArgs e)
		{
			tbMessage.Select();

			Thread startThread = new Thread(new ThreadStart(StartServer));

			startThread.Start();
		}

		private void btnStop_Click(object sender, EventArgs e)
		{
			StopServer();
		}

		private void btnSendMessage_Click(object sender, EventArgs e)
		{
			string message = tbMessage.Text;
			string targetId = lstClients.SelectedItem as string;

			if (string.IsNullOrEmpty(targetId) == false)
			{
				SendMessage(message, targetId);
				DisplayMessage(string.Format("{0} => {1}", targetId, message));
			}
			else
			{
				MessageBroadCasting(message);
				DisplayMessage(message);
			}

			tbMessage.Clear();
		}

		private void StartServer()
		{
			try
			{
				IPEndPoint iPEndPoint = new IPEndPoint(_ipAddress, PORT_NO);

				_tcpListener = new TcpListener(iPEndPoint);
				_tcpListener.Start();

				DisplayMessage("서버 시작됨");

				bool startServer = true;

				do
				{
					// 새로운 User가 접속할 때 마다 새로운 User를 생성한다.
					TcpClient tcpClient = _tcpListener.AcceptTcpClient();
					Unit unit = new Unit();

					unit.Connected += OnConnected;
					unit.Disconnected += OnDisconnected;
					unit.MessageReceived += OnMessageReceived;

					unit.Connect(tcpClient, string.Empty);
				}
				while (startServer == true);
			}
			catch (Exception ex)
			{
				Debug.Print(ex.Message);
				if (_tcpListener != null)
				{
					_tcpListener.Stop();
					DisplayMessage("서버 종료됨");
				}
			}
		}

		private void StopServer()
		{
			_units.RemoveAll(unit =>
			{
				SendMessage("서버가 종료되었습니다.", unit.UserId, MessageTypes.ServerForceStopped);
				unit.Disconnect();
			});

			ConnectionClosed();
			lstClients.Items.Clear();
		}

		private void OnConnected(object sender, ConnectEventArgs e)
		{
			// 메세지로 전파 되지 않으므로 OnMessageReceived로 발생되지 않음
			DisplayMessage(string.Format("{0}({1}) 접속됨", e.Unit.IpAddress, e.Unit.UserId));
		}

		private void OnDisconnected(object sender, ConnectEventArgs e)
		{
			DisplayMessage(string.Format("{0}({1}) 종료됨", e.Unit.IpAddress, e.Unit.UserId));
			_units.Remove(e.Unit);
		}

		private void OnMessageReceived(object sender, MessageEventArgs e)
		{
			string key = e.Unit.Key;
			Token token = null;

			switch (e.Token.MessageType)
			{
				case MessageTypes.Default:
					DisplayMessage(string.Format("{0} <- {1}: {2}", e.Unit.UserId, e.Token.To, e.Token.Value));

					if (cbBroadCasting.Checked == true)
					{
						token = new Token
						{
							MessageType = MessageTypes.Default,
							From = e.Unit.UserId,
							Value = e.Token.Value,
						};

						MessageBroadCasting(token, e.Unit.UserId);
					}

					break;
				case MessageTypes.Connect:
					// NOTICE: Unit.Connect에서 SendMessage를 주석 처리 해서 메세징 안함
					// by X10-MOBILE\xyz37(김기원) in 2015년 3월 20일 금요일 오후 11:41
					DisplayMessage(string.Format("{0} 접속됨", e.Unit.IpAddress));

					break;
				case MessageTypes.Login:
					_units.Add(e.Unit);
					DisplayMessage(string.Format("{0} 사용자가 로그인 했습니다.", e.Token.Value));

					break;
				case MessageTypes.Logout:
					_units.Remove(e.Unit);
					string message = string.Format("{0} 사용자가 로그아웃 했습니다.", e.Token.Value);

					DisplayMessage(message);

					if (cbBroadCasting.Checked == true)
					{
						token = new Token
						{
							MessageType = MessageTypes.Default,
							From = e.Unit.UserId,
							Value = message,
						};

						MessageBroadCasting(token, e.Unit.UserId);
					}

					break;
				case MessageTypes.Disconnected:
					// NOTICE: Unit.Connect에서 SendMessage를 주석 처리 해서 메세징 안함
					// by X10-MOBILE\xyz37(김기원) in 2015년 3월 20일 금요일 오후 11:41
					DisplayMessage(string.Format("{0} 종료됨", e.Unit.IpAddress));

					break;
				case MessageTypes.Command:

					break;
			}
		}

		private void ConnectionClosed()
		{
			if (_tcpListener != null)
			{
				_tcpListener.Stop();
			}
		}

		private void MessageBroadCasting(string message)
		{
			var token = new Token
			{
				MessageType = MessageTypes.MessageFromServer,
				Value = message,
			};

			foreach (Unit unit in _units)
			{
				unit.SendMessage(token);
			}
		}

		private void MessageBroadCasting(Token token, string exceptId = "")
		{
			foreach (Unit unit in _units)
			{
				if (unit.UserId == exceptId)
				{
					continue;		// 자기 자신은 BroadCasting 하지 않는다.
				}

				unit.SendMessage(token);
			}
		}

		private void SendMessage(
			string message,
			string targetId,
			MessageTypes messageType = MessageTypes.MessageFromServer)
		{
			var token = new Token
			{
				MessageType = messageType,
				To = targetId,
				Value = message,
			};

			if (string.IsNullOrEmpty(targetId) == false)
			{
				foreach (Unit unit in _units)
				{
					if (unit.UserId == targetId)
					{
						unit.SendMessage(token);

						break;
					}
				}
			}
		}

		#region Cross Thread Handler

		private delegate void SetTextCallback(string text);

		private void DisplayMessage(string message)
		{
			Invoke(new MethodInvoker(() =>
			{
				lstMessage.Items.Insert(0, message);
			}));
		}

		private void AddClient(Unit unit)
		{
			Invoke(new MethodInvoker(() =>
			{
				lstClients.Items.Insert(0, unit.UserId);
			}));
		}

		private void RemoveClient(Unit unit)
		{
			Invoke(new MethodInvoker(() =>
			{
				var userId = unit.UserId;

				if (lstClients.Items.Contains(userId) == true)
				{
					lstClients.Items.Remove(userId);
				}
			}));
		}
		#endregion
	}
}