﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mail;
using System.Net.Mail;

namespace SmtpTestDriver
{
	class Program
	{
		static void Main(string[] args)
		{
			const string FROM_MAIL = "xyz37@hanmail.net";
			const string TO_MAIL = "kwkim@ibslab.com";
			string project = "Turn Off SMTP final Test";
			StringBuilder sb = new StringBuilder();

			sb.Append("<HTML> ");
			sb.Append("<HEAD> ");
			sb.Append("<TITLE> New Document </TITLE> ");
			sb.Append("</HEAD> ");
			sb.Append(" ");
			sb.Append("<BODY> ");
			sb.Append("HTML 테스트 입니다. ");
			sb.AppendFormat("현재 시간은 {0} 입니다. ", DateTime.Now.ToString());
			sb.Append("</BODY> ");
			sb.Append("</HTML> ");

			SmtpMail.Send(FROM_MAIL, TO_MAIL, project, sb.ToString());

		}
	}
}
