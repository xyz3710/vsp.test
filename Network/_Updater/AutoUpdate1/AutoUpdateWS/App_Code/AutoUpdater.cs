using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;

[WebService(Namespace = "http://microsoft.com/webservices/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class AutoUpdater : System.Web.Services.WebService
{
    public AutoUpdater () {

        //디자인된 구성 요소를 사용하는 경우 다음 줄의 주석 처리를 제거합니다. 
        //InitializeComponent(); 
    }

    [WebMethod]
	public string UpdateCheckDAL()
	{
		XmlTextReader reader = new XmlTextReader(@"D:\VSP\AutoUpdate\Files\filelist.xml");
		String list ="";
		if (!reader.EOF)
		{
			while (reader.Read())
			{
				switch (reader.NodeType)
				{
					case XmlNodeType.Element: // The node is an Element
						while (reader.MoveToNextAttribute()) // Read attributes
						{
							list = list + reader.Value+",";
						}
						break;
					case XmlNodeType.DocumentType: // The node is a DocumentType
						break;
					default:
						break;
				}
			}
			reader.Close();
		}
		return list;
	}
    
}
