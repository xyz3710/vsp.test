using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace HttpAsyncDownloadTester
{
	/// <summary>
	/// Form1에 대한 요약 설명입니다.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private HttpAsyncDownload.HttpAsyncDownloadControl httpAsyncDownloadControl1;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Windows Form 디자이너 지원에 필요합니다.
			//
			InitializeComponent();

			//
			// TODO: InitializeComponent를 호출한 다음 생성자 코드를 추가합니다.
			//
		}

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.httpAsyncDownloadControl1 = new HttpAsyncDownload.HttpAsyncDownloadControl();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// httpAsyncDownloadControl1
			// 
			this.httpAsyncDownloadControl1.BackColor = System.Drawing.SystemColors.Window;
			this.httpAsyncDownloadControl1.EnableExit = false;
			this.httpAsyncDownloadControl1.Location = new System.Drawing.Point(16, 48);
			this.httpAsyncDownloadControl1.Name = "httpAsyncDownloadControl1";
			this.httpAsyncDownloadControl1.Size = new System.Drawing.Size(304, 248);
			this.httpAsyncDownloadControl1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(129)));
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.label1.Location = new System.Drawing.Point(16, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(288, 24);
			this.label1.TabIndex = 1;
			this.label1.Text = "최신버전으로 업그레이드 합니다.";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(328, 301);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.httpAsyncDownloadControl1);
			this.Name = "Form1";
			this.Text = "자동업그레이드";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}
		private void Form1_Load(object sender, System.EventArgs e)
		{
			httpAsyncDownloadControl1.GetFileList();
		}
	}
}
