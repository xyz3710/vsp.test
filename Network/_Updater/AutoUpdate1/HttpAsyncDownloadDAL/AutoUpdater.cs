using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Xml;

namespace HttpAsyncDownloadDAL
{
	/// <summary>
	/// Class1에 대한 요약 설명입니다.
	/// </summary>
	public class AutoUpdater
	{
		public AutoUpdater()
		{
			//
			// TODO: 여기에 생성자 논리를 추가합니다.
			//
		}

		//업데이트검색
		public string UpdateCheckDAL()
		{
			XmlTextReader reader = new XmlTextReader(@"D:\VSP\AutoUpdate\Files\filelist.xml");
			String list ="";
			if(!reader.EOF)
			{
				while (reader.Read())
				{
					switch (reader.NodeType)
					{
						case XmlNodeType.Element: // The node is an Element
							while (reader.MoveToNextAttribute()) // Read attributes
							{
								list = list + reader.Value+",";
							}
							break;
						case XmlNodeType.DocumentType: // The node is a DocumentType
							break;
						default:
							break;
					}
				}
				reader.Close();
			}
			return list;
		}
	}
}
