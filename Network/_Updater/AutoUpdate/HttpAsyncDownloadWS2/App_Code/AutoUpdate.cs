using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using HttpAsyncDownloadDAL;
using System.Collections.Generic;

namespace iDASiT.Framework.Win.Updater.WS
{
	[WebService(Namespace="http://microsoft.com/webservices/")]
	[WebServiceBinding(ConformsTo=WsiProfiles.BasicProfile1_1)]
	public class AutoUpdate : System.Web.Services.WebService
	{
		public AutoUpdate()
		{

			//디자인된 구성 요소를 사용하는 경우 다음 줄의 주석 처리를 제거합니다. 
			//InitializeComponent(); 
		}

		[WebMethod]
		public string UpdateCheckWS()
		{
			AutoUpdater autoupdater = new AutoUpdater();
			string list = autoupdater.UpdateCheckDAL();

			return list;
		}
	}
}