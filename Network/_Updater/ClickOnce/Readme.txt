Trustworthy Rich Client Deployment with ClickOnce
Brian Noyes
MSDN Magazine
May 2004

This download code accompanies the subject article and allows you to experiment with 
various forms of ClickOnce deployment. The projects were all created initially with the 
PDC Tech Preview verion of Visual Studio .NET Whidbey, but have been updated to work
with the Community Drop Tech Preview of Visual Studio 2005 that was released late-March
2004. You will need to follow  through the article to understand the intent of each 
version of the application in terms of demonstrating different forms of ClickOnce 
deployment. However, feel free to experiment on your own.

To get version 2.0 to run correctly, you will need to create a database using the 
enclosed TimeManager.sql script. You will also need to update the app.config file 
for the application to include the appropriate connection string for where you place 
the database.

If you have any questions or problems, feel free to contact me at 
brian.noyes@idesign.net.

Thanks and enjoy!