
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Imports System.IO.IsolatedStorage
Imports System.IO



'/ <summary>
'/ Summary description for Form1.
'/ </summary>

Public Class TimeManagerForm
   Inherits Form
   Private WithEvents btnEdit As Button
   Private WithEvents btnAdd As Button
   Private WithEvents btnDelete As Button
   Private m_dsTimeEntries As TimeEntriesDS
   Private dgTimeEntries As DataGrid
   '/ <summary>
   '/ Required designer variable.
   '/ </summary>
   Private components As System.ComponentModel.Container = Nothing
   
   
   Public Sub New()
      '
      ' Required for Windows Form Designer support
      '
      InitializeComponent()
      
      ' Load the time entries
      LoadTimeEntries()
   End Sub 'New
   
   
   '/ <summary>
   '/ Clean up any resources being used.
   '/ </summary>
    Protected Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub 'Dispose

#Region "Windows Form Designer generated code"

    '/ <summary>
    '/ Required method for Designer support - do not modify
    '/ the contents of this method with the code editor.
    '/ </summary>
    Private Sub InitializeComponent()
        Me.dgTimeEntries = New System.Windows.Forms.DataGrid
        Me.m_dsTimeEntries = New TimecardManager.TimeEntriesDS
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        CType(Me.dgTimeEntries, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.m_dsTimeEntries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgTimeEntries
        '
        Me.dgTimeEntries.AccessibleName = "DataGrid"
        Me.dgTimeEntries.AccessibleRole = System.Windows.Forms.AccessibleRole.Table
        Me.dgTimeEntries.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgTimeEntries.DataMember = ""
        Me.dgTimeEntries.DataSource = Me.m_dsTimeEntries.TimeEntries
        Me.dgTimeEntries.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dgTimeEntries.Location = New System.Drawing.Point(16, 16)
        Me.dgTimeEntries.Name = "dgTimeEntries"
        Me.dgTimeEntries.ReadOnly = True
        Me.dgTimeEntries.Size = New System.Drawing.Size(600, 288)
        Me.dgTimeEntries.TabIndex = 0
        '
        'm_dsTimeEntries
        '
        Me.m_dsTimeEntries.DataSetName = "TimeEntriesDS"
        Me.m_dsTimeEntries.Locale = New System.Globalization.CultureInfo("en-US")
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnEdit.Location = New System.Drawing.Point(248, 312)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(136, 23)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "Edit Selected Entry..."
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Location = New System.Drawing.Point(16, 312)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(136, 23)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.Text = "Add New Entry..."
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.Location = New System.Drawing.Point(480, 312)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(136, 23)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "Delete Selected Entry"
        '
        'TimeManagerForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(632, 342)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.dgTimeEntries)
        Me.Name = "TimeManagerForm"
        Me.Text = "Time Manager v1.1"
        CType(Me.dgTimeEntries, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.m_dsTimeEntries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub 'InitializeComponent 
#End Region


    '/ <summary>
    '/ The main entry point for the application.
    '/ </summary>
    <STAThread()> _
    Shared Sub Main()
        Application.Run(New TimeManagerForm)
    End Sub 'Main


    '/ <summary>
    '/ Add new time entry items
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ' Show the form
        Dim form As New AddEditTimeEntryForm
        If form.ShowDialog() = DialogResult.OK Then
            ' Insert the results
            m_dsTimeEntries.TimeEntries.Rows.Add(form.TimeEntries.TimeEntries(0).ItemArray)
        End If
    End Sub 'btnAdd_Click


    '/ <summary>
    '/ Edit a selected time entry item
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        ' Create the form
        Dim form As New AddEditTimeEntryForm
        ' Get the current row
        Dim row As TimeEntriesDS.TimeEntriesRow = CType(CType(BindingContext(m_dsTimeEntries.TimeEntries).Current, DataRowView).Row, TimeEntriesDS.TimeEntriesRow)
        ' Push the current row into the dataset used by the add/edit form
        form.TimeEntries.Merge(New DataRow() {row})
        ' show it
        If form.ShowDialog() = DialogResult.OK Then
            ' Write the results back in
            row.ItemArray = form.TimeEntries.TimeEntries(0).ItemArray
        End If
    End Sub 'btnEdit_Click


    '/ <summary>
    '/ Delete the selected entry
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        CType(BindingContext(m_dsTimeEntries.TimeEntries).Current, DataRowView).Row.Delete()
    End Sub 'btnDelete_Click


    '/ <summary>
    '/ When closing, prompt the user to save or discard changes
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub OnFormClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim dr As DialogResult = MessageBox.Show("Do you want to save changes to your time entries?", "Time Manager", MessageBoxButtons.YesNoCancel)
        ' If they selected cancel, just stop the closing
        If dr = DialogResult.Cancel Then
            e.Cancel = True

            ' If they said Yes, save before closing
        ElseIf dr = DialogResult.Yes Then
            SaveTimeEntries()
        End If
    End Sub 'OnFormClosing

    ' Otherwise allow closing to proceed

    '/ <summary>
    '/ Load the entries from isolated storage for the domain,
    '/ safe for internet zone access
    '/ </summary>
    Private Sub LoadTimeEntries()
        ' Declare our resources
        Dim isoStoreFile As IsolatedStorageFile = Nothing
        Dim isoStream As IsolatedStorageFileStream = Nothing

        Try
            ' Get the isolated storage file instance
            ' Note: in the Whidbey tech preview, domain storage files in the debugger, use assembly storage if running in the debugger
            isoStoreFile = IsolatedStorageFile.GetUserStoreForAssembly()
            'isoStoreFile = IsolatedStorageFile.GetUserStoreForDomain()

            ' Check for the existence of the time entries file
            Dim fnames As String() = isoStoreFile.GetFileNames("TimeEntries.xml")

            If fnames.Length = 1 Then ' We have one
                ' Read in the existing time entries
                isoStream = New IsolatedStorageFileStream("TimeEntries.xml", FileMode.Open, isoStoreFile)
                m_dsTimeEntries.ReadXml(isoStream)
                ' Make them show as unchanged
                m_dsTimeEntries.AcceptChanges()
            End If
        Catch ex As Exception
            MessageBox.Show(("Unable to load time entries. Exception: " + ex.GetType().ToString() + ": " + ex.Message))
        Finally
            ' Clean up
            If Not (isoStream Is Nothing) Then
                isoStream.Close()
            End If
            If Not (isoStoreFile Is Nothing) Then
                isoStoreFile.Close()
            End If
        End Try
    End Sub 'LoadTimeEntries


    '/ <summary>
    '/ Save the time entries to isolated storage. Safe for internet security zone.
    '/ </summary>
    Private Sub SaveTimeEntries()
        ' Declare the resources
        Dim isoStoreFile As IsolatedStorageFile = Nothing
        Dim isoStream As IsolatedStorageFileStream = Nothing

        Try
            ' Get the isolated storage file instance
            ' Note: in the Whidbey tech preview, domain storage files in the debugger, use assembly storage if running in the debugger
            isoStoreFile = IsolatedStorageFile.GetUserStoreForAssembly()
            'isoStoreFile = IsolatedStorageFile.GetUserStoreForDomain()

            ' Save the contents of the data set to storage
            isoStream = New IsolatedStorageFileStream("TimeEntries.xml", FileMode.Create, isoStoreFile)
            m_dsTimeEntries.WriteXml(isoStream)
        Catch ex As Exception
            MessageBox.Show(("Unable to save time entries. Exception: " + ex.GetType().ToString() + ": " + ex.Message))
        Finally
            ' Clean up
            If Not (isoStream Is Nothing) Then
                isoStream.Close()
            End If
            If Not (isoStoreFile Is Nothing) Then
                isoStoreFile.Close()
            End If
        End Try
    End Sub 'SaveTimeEntries
End Class 'TimeManagerForm 