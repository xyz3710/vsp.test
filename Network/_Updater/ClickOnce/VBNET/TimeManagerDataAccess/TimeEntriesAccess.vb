
Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient


'/ <summary>
'/ Summary description for TimeEntriesAccess.
'/ </summary>

Public Class TimeEntriesAccess
    Private Shared m_connStr As String


    ' Prevent construction - static only access
    Private Sub New()
    End Sub 'New


    Shared Sub New()
        m_connStr = ConfigurationSettings.AppSettings("connStr")
    End Sub 'New


    Private Sub InitCommands()
    End Sub 'InitCommands


    Public Shared Function GetEntries() As TimeEntriesDS
        Dim ds As New TimeEntriesDS
        SqlHelper.FillDataset(m_connStr, CommandType.Text, "SELECT Id, StartTime, StopTime, Duration, Task, Customer, Description, Comments FROM TimeEntries", ds, New String() {ds.TimeEntries.TableName})
        Return ds
    End Function 'GetEntries


    Public Shared Sub UpdateEntries(ByVal entries As TimeEntriesDS)
        Dim conn As New SqlConnection(m_connStr)
        Dim cmdInsert As New SqlCommand
        Dim cmdUpdate As New SqlCommand
        Dim cmdDelete As New SqlCommand

        ' Asssociate connections
        cmdInsert.Connection = conn
        cmdDelete.Connection = conn
        cmdUpdate.Connection = conn

        ' 
        ' cmdInsert params and query
        ' 
        cmdInsert.CommandText = "INSERT INTO TimeEntries(Id, StartTime, StopTime, Duration, Task, Customer, Description, Comments) VALUES (@Id, @StartTime, @StopTime, @Duration, @Task, @Customer, @Description, @Comments); SELECT Id, StartTime, StopTime, Duration, Task, Customer, Description, Comments FROM TimeEntries WHERE (Id = @Id)"
        cmdInsert.Parameters.Add(New SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier, 16, "Id"))
        cmdInsert.Parameters.Add(New SqlParameter("@StartTime", System.Data.SqlDbType.DateTime, 8, "StartTime"))
        cmdInsert.Parameters.Add(New SqlParameter("@StopTime", System.Data.SqlDbType.DateTime, 8, "StopTime"))
        cmdInsert.Parameters.Add(New SqlParameter("@Duration", System.Data.SqlDbType.Float, 8, "Duration"))
        cmdInsert.Parameters.Add(New SqlParameter("@Task", System.Data.SqlDbType.VarChar, 100, "Task"))
        cmdInsert.Parameters.Add(New SqlParameter("@Customer", System.Data.SqlDbType.VarChar, 100, "Customer"))
        cmdInsert.Parameters.Add(New SqlParameter("@Description", System.Data.SqlDbType.VarChar, 2147483647, "Description"))
        cmdInsert.Parameters.Add(New SqlParameter("@Comments", System.Data.SqlDbType.VarChar, 2147483647, "Comments"))
        ' 
        ' cmdUpdate  params and query
        ' 
        cmdUpdate.CommandText = "UPDATE TimeEntries SET Id = @Id, StartTime = @StartTime, StopTime = @StopTime, Duration = @Duration, Task = @Task, Customer = @Customer, Description = @Description, Comments = @Comments WHERE (Id = @Original_Id) AND (Customer = @Original_Customer OR @Original_Customer IS NULL AND Customer IS NULL) AND (Duration = @Original_Duration) AND (StartTime = @Original_StartTime OR @Original_StartTime IS NULL AND StartTime IS NULL) AND (StopTime = @Original_StopTime OR @Original_StopTime IS NULL AND StopTime IS NULL) AND (Task = @Original_Task); SELECT Id, StartTime, StopTime, Duration, Task, Customer, Description, Comments FROM TimeEntries WHERE (Id = @Id)"
        cmdUpdate.Parameters.Add(New SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier, 16, "Id"))
        cmdUpdate.Parameters.Add(New SqlParameter("@StartTime", System.Data.SqlDbType.DateTime, 8, "StartTime"))
        cmdUpdate.Parameters.Add(New SqlParameter("@StopTime", System.Data.SqlDbType.DateTime, 8, "StopTime"))
        cmdUpdate.Parameters.Add(New SqlParameter("@Duration", System.Data.SqlDbType.Float, 8, "Duration"))
        cmdUpdate.Parameters.Add(New SqlParameter("@Task", System.Data.SqlDbType.VarChar, 100, "Task"))
        cmdUpdate.Parameters.Add(New SqlParameter("@Customer", System.Data.SqlDbType.VarChar, 100, "Customer"))
        cmdUpdate.Parameters.Add(New SqlParameter("@Description", System.Data.SqlDbType.VarChar, 2147483647, "Description"))
        cmdUpdate.Parameters.Add(New SqlParameter("@Comments", System.Data.SqlDbType.VarChar, 2147483647, "Comments"))
        cmdUpdate.Parameters.Add(New SqlParameter("@Original_Id", System.Data.SqlDbType.UniqueIdentifier, 16, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        cmdUpdate.Parameters.Add(New SqlParameter("@Original_Customer", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "Customer", System.Data.DataRowVersion.Original, Nothing))
        cmdUpdate.Parameters.Add(New SqlParameter("@Original_Duration", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "Duration", System.Data.DataRowVersion.Original, Nothing))
        cmdUpdate.Parameters.Add(New SqlParameter("@Original_StartTime", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "StartTime", System.Data.DataRowVersion.Original, Nothing))
        cmdUpdate.Parameters.Add(New SqlParameter("@Original_StopTime", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "StopTime", System.Data.DataRowVersion.Original, Nothing))
        cmdUpdate.Parameters.Add(New SqlParameter("@Original_Task", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "Task", System.Data.DataRowVersion.Original, Nothing))
        ' 
        ' cmdDelete params and query
        ' 
        cmdDelete.CommandText = "DELETE FROM TimeEntries WHERE (Id = @Original_Id) AND (Customer = @Original_Customer OR @Original_Customer IS NULL AND Customer IS NULL) AND (Duration = @Original_Duration) AND (StartTime = @Original_StartTime OR @Original_StartTime IS NULL AND StartTime IS NULL) AND (StopTime = @Original_StopTime OR @Original_StopTime IS NULL AND StopTime IS NULL) AND (Task = @Original_Task)"
        cmdDelete.Parameters.Add(New SqlParameter("@Original_Id", System.Data.SqlDbType.UniqueIdentifier, 16, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        cmdDelete.Parameters.Add(New SqlParameter("@Original_Customer", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "Customer", System.Data.DataRowVersion.Original, Nothing))
        cmdDelete.Parameters.Add(New SqlParameter("@Original_Duration", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "Duration", System.Data.DataRowVersion.Original, Nothing))
        cmdDelete.Parameters.Add(New SqlParameter("@Original_StartTime", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "StartTime", System.Data.DataRowVersion.Original, Nothing))
        cmdDelete.Parameters.Add(New SqlParameter("@Original_StopTime", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "StopTime", System.Data.DataRowVersion.Original, Nothing))
        cmdDelete.Parameters.Add(New SqlParameter("@Original_Task", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, System.Byte), CType(0, System.Byte), "Task", System.Data.DataRowVersion.Original, Nothing))

        ' Perform the update
        SqlHelper.UpdateDataset(cmdInsert, cmdDelete, cmdUpdate, entries, entries.TimeEntries.TableName)
    End Sub 'UpdateEntries 
End Class 'TimeEntriesAccess