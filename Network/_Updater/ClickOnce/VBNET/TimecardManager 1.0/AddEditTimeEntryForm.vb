
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms


'/ <summary>
'/ Summary description for AddEditTimeEntryForm.
'/ </summary>

Public Class AddEditTimeEntryForm
    Inherits Form
    Private label1 As Label
    Private label2 As Label
    Private label3 As Label
    Private lblTask As Label
    Private label5 As Label
    Private lblDescription As Label
    Private lblComments As Label
    Private txtDuration As TextBox
    Private txtTask As TextBox
    Private txtCustomer As TextBox
    Private txtDescription As TextBox
    Private WithEvents btnSave As Button
    Private btnCancel As Button
    Private m_dsTimeEntries As TimeEntriesDS
    Private txtStartTime As TextBox
    Private txtStopTime As TextBox
    Private label4 As Label
    Private txtComments As TextBox
    Private txtDate As TextBox
    '/ <summary>
    '/ Required designer variable.
    '/ </summary>
    Private components As System.ComponentModel.Container = Nothing


    Public Sub New()
        '
        ' Required for Windows Form Designer support
        '
        InitializeComponent()

        ' Set the dialog buttons
        Me.CancelButton = btnCancel
        Me.AcceptButton = btnSave

        ' Set up the custom bindings for the date and time fields
        BindDateTime()
    End Sub 'New

    '/ <summary>
    '/ The DataSet used to populate the form and get updated values
    '/ </summary>

    Public Property TimeEntries() As TimeEntriesDS
        Get
            Return m_dsTimeEntries
        End Get
        Set(ByVal Value As TimeEntriesDS)
            ' Merge in the values
            m_dsTimeEntries.Merge(value)
            ' Make sure someone didn't give us something unexpected
            If m_dsTimeEntries.TimeEntries.Count > 1 OrElse m_dsTimeEntries.TimeEntries.Count = 0 Then
                Throw New ArgumentException("You can only provide a data set with a single row in the TimeEntries table to this form.")
            End If
        End Set
    End Property


    '/ <summary>
    '/ Clean up any resources being used.
    '/ </summary>
    Protected Overloads Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub 'Dispose

#Region "Windows Form Designer generated code"

    '/ <summary>
    '/ Required method for Designer support - do not modify
    '/ the contents of this method with the code editor.
    '/ </summary>
    Private Sub InitializeComponent()
        Me.label1 = New System.Windows.Forms.Label
        Me.label2 = New System.Windows.Forms.Label
        Me.label3 = New System.Windows.Forms.Label
        Me.lblTask = New System.Windows.Forms.Label
        Me.label5 = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblComments = New System.Windows.Forms.Label
        Me.m_dsTimeEntries = New TimecardManager.TimeEntriesDS
        Me.txtDuration = New System.Windows.Forms.TextBox
        Me.txtTask = New System.Windows.Forms.TextBox
        Me.txtCustomer = New System.Windows.Forms.TextBox
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.txtComments = New System.Windows.Forms.TextBox
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.txtStartTime = New System.Windows.Forms.TextBox
        Me.txtStopTime = New System.Windows.Forms.TextBox
        Me.txtDate = New System.Windows.Forms.TextBox
        Me.label4 = New System.Windows.Forms.Label
        CType(Me.m_dsTimeEntries, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        ' 
        ' label1
        ' 
        Me.label1.Location = New System.Drawing.Point(8, 32)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(72, 16)
        Me.label1.TabIndex = 2
        Me.label1.Text = "Start"
        ' 
        ' label2
        ' 
        Me.label2.Location = New System.Drawing.Point(8, 56)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(72, 16)
        Me.label2.TabIndex = 4
        Me.label2.Text = "Stop"
        ' 
        ' label3
        ' 
        Me.label3.Location = New System.Drawing.Point(8, 80)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(72, 16)
        Me.label3.TabIndex = 6
        Me.label3.Text = "Duration"
        ' 
        ' lblTask
        ' 
        Me.lblTask.Location = New System.Drawing.Point(8, 104)
        Me.lblTask.Name = "lblTask"
        Me.lblTask.Size = New System.Drawing.Size(72, 16)
        Me.lblTask.TabIndex = 8
        Me.lblTask.Text = "Task"
        ' 
        ' label5
        ' 
        Me.label5.Location = New System.Drawing.Point(8, 128)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(72, 16)
        Me.label5.TabIndex = 10
        Me.label5.Text = "Customer"
        ' 
        ' lblDescription
        ' 
        Me.lblDescription.Location = New System.Drawing.Point(8, 152)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(72, 16)
        Me.lblDescription.TabIndex = 12
        Me.lblDescription.Text = "Description"
        ' 
        ' lblComments
        ' 
        Me.lblComments.Location = New System.Drawing.Point(8, 216)
        Me.lblComments.Name = "lblComments"
        Me.lblComments.Size = New System.Drawing.Size(72, 16)
        Me.lblComments.TabIndex = 14
        Me.lblComments.Text = "Comments"
        ' 
        ' m_dsTimeEntries
        ' 
        Me.m_dsTimeEntries.DataSetName = "TimeEntriesDS"
        Me.m_dsTimeEntries.Locale = New System.Globalization.CultureInfo("en-US")
        ' 
        ' txtDuration
        ' 
        Me.txtDuration.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.m_dsTimeEntries, "TimeEntries.Duration"))
        Me.txtDuration.Location = New System.Drawing.Point(80, 80)
        Me.txtDuration.Name = "txtDuration"
        Me.txtDuration.TabIndex = 7
        Me.txtDuration.Text = ""
        ' 
        ' txtTask
        ' 
        Me.txtTask.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.m_dsTimeEntries, "TimeEntries.Task"))
        Me.txtTask.Location = New System.Drawing.Point(80, 104)
        Me.txtTask.Name = "txtTask"
        Me.txtTask.TabIndex = 9
        Me.txtTask.Text = ""
        ' 
        ' txtCustomer
        ' 
        Me.txtCustomer.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.m_dsTimeEntries, "TimeEntries.Customer"))
        Me.txtCustomer.Location = New System.Drawing.Point(80, 128)
        Me.txtCustomer.Name = "txtCustomer"
        Me.txtCustomer.Size = New System.Drawing.Size(152, 20)
        Me.txtCustomer.TabIndex = 11
        Me.txtCustomer.Text = ""
        ' 
        ' txtDescription
        ' 
        Me.txtDescription.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.m_dsTimeEntries, "TimeEntries.Description"))
        Me.txtDescription.Location = New System.Drawing.Point(80, 152)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(328, 56)
        Me.txtDescription.TabIndex = 13
        Me.txtDescription.Text = ""
        ' 
        ' txtComments
        ' 
        Me.txtComments.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.m_dsTimeEntries, "TimeEntries.Comments"))
        Me.txtComments.Location = New System.Drawing.Point(80, 216)
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Size = New System.Drawing.Size(328, 56)
        Me.txtComments.TabIndex = 15
        Me.txtComments.Text = ""
        ' 
        ' btnSave
        ' 
        Me.btnSave.Location = New System.Drawing.Point(16, 288)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.TabIndex = 16
        Me.btnSave.Text = "Save"
        ' 
        ' btnCancel
        ' 
        Me.btnCancel.Location = New System.Drawing.Point(352, 288)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.TabIndex = 17
        Me.btnCancel.Text = "Cancel"
        ' 
        ' txtStartTime
        ' 
        Me.txtStartTime.Location = New System.Drawing.Point(80, 32)
        Me.txtStartTime.Name = "txtStartTime"
        Me.txtStartTime.TabIndex = 3
        Me.txtStartTime.Text = ""
        ' 
        ' txtStopTime
        ' 
        Me.txtStopTime.Location = New System.Drawing.Point(80, 56)
        Me.txtStopTime.Name = "txtStopTime"
        Me.txtStopTime.TabIndex = 5
        Me.txtStopTime.Text = ""
        ' 
        ' txtDate
        ' 
        Me.txtDate.Location = New System.Drawing.Point(80, 8)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.TabIndex = 1
        Me.txtDate.Text = ""
        ' 
        ' label4
        ' 
        Me.label4.Location = New System.Drawing.Point(8, 8)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(72, 16)
        Me.label4.TabIndex = 0
        Me.label4.Text = "Date"
        ' 
        ' AddEditTimeEntryForm
        ' 
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(440, 318)
        Me.Controls.Add(txtDate)
        Me.Controls.Add(label4)
        Me.Controls.Add(txtStopTime)
        Me.Controls.Add(txtStartTime)
        Me.Controls.Add(btnCancel)
        Me.Controls.Add(btnSave)
        Me.Controls.Add(txtComments)
        Me.Controls.Add(txtDescription)
        Me.Controls.Add(txtCustomer)
        Me.Controls.Add(txtTask)
        Me.Controls.Add(txtDuration)
        Me.Controls.Add(lblComments)
        Me.Controls.Add(lblDescription)
        Me.Controls.Add(label5)
        Me.Controls.Add(lblTask)
        Me.Controls.Add(label3)
        Me.Controls.Add(label2)
        Me.Controls.Add(label1)
        Me.Name = "AddEditTimeEntryForm"
        Me.Text = "Add/Edit Time Entry"
        CType(Me.m_dsTimeEntries, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
    End Sub 'InitializeComponent 
#End Region


    '/ <summary>
    '/ Set up custom bindings for the date and time fields
    '/ </summary>
    Private Sub BindDateTime()
        ' Date field binding
        Dim bDate As New Binding("Text", m_dsTimeEntries, "TimeEntries.StartTime")
        AddHandler bDate.Format, AddressOf FormatDate
        AddHandler bDate.Parse, AddressOf ParseDate

        ' Start time binding
        Dim bStartTime As New Binding("Text", m_dsTimeEntries, "TimeEntries.StartTime")
        AddHandler bStartTime.Format, AddressOf FormatStartTime
        AddHandler bStartTime.Parse, AddressOf ParseStartTime

        ' Stop time binding
        Dim bStopTime As New Binding("Text", m_dsTimeEntries, "TimeEntries.StopTime")
        AddHandler bStopTime.Format, AddressOf FormatStopTime
        AddHandler bStopTime.Parse, AddressOf ParseStopTime

        ' Add the bindings to their respective controls
        txtDate.DataBindings.Add(bDate)
        txtStartTime.DataBindings.Add(bStartTime)
        txtStopTime.DataBindings.Add(bStopTime)
    End Sub 'BindDateTime


    '/ <summary>
    '/ Make the changes current in the dataset so that they can be merged on
    '/ the other end, then close the dialog with an OK result
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        m_dsTimeEntries.AcceptChanges()
        DialogResult = DialogResult.OK
        Close()
    End Sub 'btnSave_Click


    '/ <summary>
    '/ Initialize the dataset if needed for add scenario,
    '/ and set the currency manager to the one and only record we
    '/ care about.
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub OnFormLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' If there is nothing in the data set, add something!
        If m_dsTimeEntries.TimeEntries.Count < 1 Then
            ' Add a new row
            Dim newrow As TimeEntriesDS.TimeEntriesRow = m_dsTimeEntries.TimeEntries.NewTimeEntriesRow()
            ' Initialize the values
            newrow.Id = Guid.NewGuid()
            newrow.StartTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0)
            newrow.StopTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0)
            newrow.Duration = 0
            newrow.Task = String.Empty
            newrow.Description = String.Empty
            newrow.Comments = String.Empty
            ' Add it to the dataset
            m_dsTimeEntries.TimeEntries.AddTimeEntriesRow(newrow)
        End If

        ' Get the currency manager for the dataset
        Dim cm As CurrencyManager = BindingContext(m_dsTimeEntries) '
        'ToDo: Error processing original source shown below
        '   // Get the currency manager for the dataset
        '   CurrencyManager cm = BindingContext[m_dsTimeEntries] as CurrencyManager;
        '---------------------------------------------------------^--- Syntax error: ';' expected
        If Not (cm Is Nothing) Then
            ' Set the position to zero to be sure
            cm.Position = 0
        End If
    End Sub 'OnFormLoad


    '/ <summary>
    '/ Format the date field to just the short date for display
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub FormatDate(ByVal sender As Object, ByVal e As ConvertEventArgs)
        e.Value = CType(e.Value, DateTime).ToShortDateString()
    End Sub 'FormatDate


    '/ <summary>
    '/ Parse changed values in the date field into the date for
    '/ both the start time and stop time
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub ParseDate(ByVal sender As Object, ByVal e As ConvertEventArgs)
        e.Value = DateTime.Parse((m_dsTimeEntries.TimeEntries(0).StartTime.ToShortTimeString() + " " + CStr(e.Value)))
        m_dsTimeEntries.TimeEntries(0).StopTime = DateTime.Parse((m_dsTimeEntries.TimeEntries(0).StopTime.ToShortTimeString() + " " + m_dsTimeEntries.TimeEntries(0).StartTime.ToShortDateString()))
    End Sub 'ParseDate


    '/ <summary>
    '/ Format the start time to a short time format for display
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub FormatStartTime(ByVal sender As Object, ByVal e As ConvertEventArgs)
        e.Value = CType(e.Value, DateTime).ToShortTimeString()
    End Sub 'FormatStartTime


    '/ <summary>
    '/ Parse changed values in the start time field into a
    '/ full date time based on the date field and the entered time
    '/ and update the duration
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub ParseStartTime(ByVal sender As Object, ByVal e As ConvertEventArgs)
        Dim newStart As DateTime = DateTime.Parse((CStr(e.Value) + " " + m_dsTimeEntries.TimeEntries(0).StartTime.ToShortDateString()))
        e.Value = newStart
        UpdateDuration(m_dsTimeEntries.TimeEntries(0).StopTime, newStart)
    End Sub 'ParseStartTime


    '/ <summary>
    '/ Format the stop time for display
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub FormatStopTime(ByVal sender As Object, ByVal e As ConvertEventArgs)
        e.Value = CType(e.Value, DateTime).ToShortTimeString()
    End Sub 'FormatStopTime


    '/ <summary>
    '/ Parse changed values in the stop time field to include 
    '/ the date from the date field and update the duration.
    '/ </summary>
    '/ <param name="sender"></param>
    '/ <param name="e"></param>
    Private Sub ParseStopTime(ByVal sender As Object, ByVal e As ConvertEventArgs)
        Dim newStop As DateTime = DateTime.Parse((CStr(e.Value) + " " + m_dsTimeEntries.TimeEntries(0).StartTime.ToShortDateString()))
        e.Value = newStop
        UpdateDuration(newStop, m_dsTimeEntries.TimeEntries(0).StartTime)
    End Sub 'ParseStopTime


    '/ <summary>
    '/ Update the duration automatically based on the entered values
    '/ in the start and stop time fields
    '/ </summary>
    '/ <param name="stopTime"></param>
    '/ <param name="startTime"></param>
    Private Sub UpdateDuration(ByVal stopTime As DateTime, ByVal startTime As DateTime)
        Dim ts As TimeSpan = stopTime.Subtract(startTime)
        txtDuration.Text = (ts.Hours + ts.Minutes / 60.0F).ToString()
    End Sub 'UpdateDuration
End Class 'AddEditTimeEntryForm 
