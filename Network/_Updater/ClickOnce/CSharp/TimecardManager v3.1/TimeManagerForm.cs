using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO.IsolatedStorage;
using System.IO;
using System.Security;
using TimeManagerDataAccess;
using System.Deployment;

namespace TimecardManager
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class TimeManagerForm : Form
	{
		private Button btnEdit;
		private Button btnAdd;
		private Button btnDelete;
		private TimeEntriesDS m_dsTimeEntries;
        private DataGrid dgTimeEntries;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem fileMenuItem;
        private System.Windows.Forms.MenuItem checkForUpdatesMenuItem;
        private System.ComponentModel.IContainer components;
		/// <summary>
		/// Required designer variable.
		/// </summary>

		public TimeManagerForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// Load the time entries
			LoadTimeEntries();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.dgTimeEntries = new System.Windows.Forms.DataGrid();
            this.m_dsTimeEntries = new TimeManagerDataAccess.TimeEntriesDS();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.fileMenuItem = new System.Windows.Forms.MenuItem();
            this.checkForUpdatesMenuItem = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgTimeEntries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsTimeEntries)).BeginInit();
            this.SuspendLayout();

            // 
            // dgTimeEntries
            // 
            this.dgTimeEntries.AccessibleName = "DataGrid";
            this.dgTimeEntries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTimeEntries.DataMember = "";
            this.dgTimeEntries.DataSource = this.m_dsTimeEntries.TimeEntries;
            this.dgTimeEntries.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgTimeEntries.Location = new System.Drawing.Point(16, 10);
            this.dgTimeEntries.Name = "dgTimeEntries";
            this.dgTimeEntries.ReadOnly = true;
            this.dgTimeEntries.Size = new System.Drawing.Size(600, 294);
            this.dgTimeEntries.TabIndex = 0;

            // 
            // m_dsTimeEntries
            // 
            this.m_dsTimeEntries.DataSetName = "TimeEntriesDS";
            this.m_dsTimeEntries.Locale = new System.Globalization.CultureInfo("en-US");

            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnEdit.Location = new System.Drawing.Point(248, 312);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(136, 23);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Edit Selected Entry...";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);

            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAdd.Location = new System.Drawing.Point(16, 312);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(136, 23);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add New Entry...";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);

            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Location = new System.Drawing.Point(480, 312);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(136, 23);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete Selected Entry";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);

            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
                this.fileMenuItem
            });
            this.mainMenu1.Name = "mainMenu1";

            // 
            // fileMenuItem
            // 
            this.fileMenuItem.Index = 0;
            this.fileMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
                this.checkForUpdatesMenuItem
            });
            this.fileMenuItem.Name = "menuItem1";
            this.fileMenuItem.Text = "File";

            // 
            // checkForUpdatesMenuItem
            // 
            this.checkForUpdatesMenuItem.Index = 0;
            this.checkForUpdatesMenuItem.Name = "menuItem1";
            this.checkForUpdatesMenuItem.Text = "Check For Updates";
            this.checkForUpdatesMenuItem.Click += new System.EventHandler(this.checkForUpdatesMenuItem_Click);

            // 
            // TimeManagerForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(632, 342);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.dgTimeEntries);
            this.Menu = this.mainMenu1;
            this.Name = "TimeManagerForm";
            this.Text = "Time Manager v3.1";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgTimeEntries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsTimeEntries)).EndInit();
            this.ResumeLayout(false);
        }
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new TimeManagerForm());
		}

		/// <summary>
		/// Add new time entry items
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			// Show the form
			AddEditTimeEntryForm form = new AddEditTimeEntryForm();
			if (form.ShowDialog() == DialogResult.OK)
			{
				// Insert the results
				m_dsTimeEntries.TimeEntries.Rows.Add(form.TimeEntries.TimeEntries[0].ItemArray);
			}
		}

		/// <summary>
		/// Edit a selected time entry item
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnEdit_Click(object sender, System.EventArgs e)
		{
			// Create the form
			AddEditTimeEntryForm form = new AddEditTimeEntryForm();
			// Get the current row
			TimeEntriesDS.TimeEntriesRow row = (TimeEntriesDS.TimeEntriesRow)((DataRowView)BindingContext[m_dsTimeEntries.TimeEntries].Current).Row;
			// Push the current row into the dataset used by the add/edit form
			form.TimeEntries.Merge(new DataRow[] {row});
			// show it
			if (form.ShowDialog() == DialogResult.OK)
			{
				// Write the results back in
				row.ItemArray = form.TimeEntries.TimeEntries[0].ItemArray;
			}
		}

		/// <summary>
		/// Delete the selected entry
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			((DataRowView)BindingContext[m_dsTimeEntries.TimeEntries].Current).Row.Delete();
		}

		/// <summary>
		/// When closing, prompt the user to save or discard changes
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFormClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			DialogResult dr = MessageBox.Show("Do you want to save changes to your time entries?","Time Manager", MessageBoxButtons.YesNoCancel);
			// If they selected cancel, just stop the closing
			if (dr == DialogResult.Cancel)
			{
				e.Cancel = true;
			}

			// If they said Yes, save before closing
			else if (dr == DialogResult.Yes)
			{
				SaveTimeEntries();
			}

			// Otherwise allow closing to proceed
		}

		/// <summary>
		/// Load the entries from the database if connected, otherwise from 
		/// isolated storage for the assembly.
		/// </summary>
		private void LoadTimeEntries()
		{
			// First try to load from DB
            try
            {
                m_dsTimeEntries = TimeEntriesAccess.GetEntries();
                dgTimeEntries.DataSource = m_dsTimeEntries.TimeEntries;
                return;
            }
            catch (SecurityException ex)
            {
                MessageBox.Show("A security exception occured. " + ex.Message);
            }
            catch { } // Treat as disconnected and continue

			// Declare our resources
			IsolatedStorageFile isoStoreFile = null;
			IsolatedStorageFileStream isoStream = null;

			try
			{
				// Get the isolated storage file instance
				// Note: in the Whidbey tech preview, domain storage files in the debugger, use assembly storage if running in the debugger
				isoStoreFile = IsolatedStorageFile.GetUserStoreForApplication();

				// Check for the existence of the time entries file
				string[] fnames = isoStoreFile.GetFileNames("TimeEntries.xml");

				if (fnames.Length == 1) // We have one
				{
					// Read in the existing time entries
					isoStream = new IsolatedStorageFileStream("TimeEntries.xml", FileMode.Open, isoStoreFile);
					m_dsTimeEntries.ReadXml(isoStream);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Unable to load time entries. Exception: " + ex.GetType().ToString() + ": " + ex.Message);
			}
			finally
			{
				// Clean up
				if (isoStream != null)
				{
					isoStream.Close();
				}
				if (isoStoreFile != null)
				{
					isoStoreFile.Close();
				}
			}
		}

		/// <summary>
		/// Save the time entries to the database if connected, 
		/// and isolated storage regardless. 
		/// </summary>
		private void SaveTimeEntries()
		{
			// Try to save changes off to DB if connected
			try
			{
				TimeEntriesAccess.UpdateEntries(m_dsTimeEntries);
			}
            catch (SecurityException ex)
            {
                MessageBox.Show("A security exception occured. " + ex.Message);
            }
            catch { } // treat as disconnected

			// Declare the resources
			IsolatedStorageFile isoStoreFile = null;
			IsolatedStorageFileStream isoStream = null;

			try
			{
				// Get the isolated storage file instance
				// Note: in the Whidbey tech preview, domain storage files in the debugger, use assembly storage if running in the debugger
				isoStoreFile = IsolatedStorageFile.GetUserStoreForApplication();

				// Save the contents of the data set to storage
				isoStream = new IsolatedStorageFileStream("TimeEntries.xml", FileMode.Create, isoStoreFile);
				m_dsTimeEntries.WriteXml(isoStream);
			}
			catch (Exception ex)
			{
				MessageBox.Show("Unable to save time entries. Exception: " + ex.GetType().ToString() + ": " + ex.Message);
			}
			finally
			{
				// Clean up
				if (isoStream != null)
				{
					isoStream.Close();
				}
				if (isoStoreFile != null)
				{
					isoStoreFile.Close();
				}
			}
        }
        private void checkForUpdatesMenuItem_Click(object sender, System.EventArgs e)
        {
			ApplicationDeployment updater = ApplicationDeployment.CurrentDeployment;
			if (updater.CheckForUpdate()) // Update available
            {
                DialogResult res = MessageBox.Show("A newer version of the TimecardManager application is available. Do you wish to update the application now?", "TimecardManager Updater", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    updater.Update();
                    MessageBox.Show("Please shutdown and restart the application to start using the new version.");
                }
            }
            else
            {
                MessageBox.Show("No updates available.");
            }
        }

	}
}
