using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace TimeManagerDataAccess
{
	/// <summary>
	/// Summary description for TimeEntriesAccess.
	/// </summary>
	public class TimeEntriesAccess
	{
		private static string m_connStr;

		// Prevent construction - static only access
		private TimeEntriesAccess()
		{
		}

		static TimeEntriesAccess()
		{
			m_connStr = ConfigurationSettings.AppSettings["connStr"];
		}

		private void InitCommands()
		{
		}

		public static TimeEntriesDS GetEntries()
		{
			TimeEntriesDS ds = new TimeEntriesDS();
			SqlHelper.FillDataset(m_connStr,CommandType.Text, "SELECT Id, StartTime, StopTime, Duration, Task, Customer, Description, Comments FROM TimeEntries",ds,new string[]{ds.TimeEntries.TableName});
			return ds;
		}

		public static void UpdateEntries(TimeEntriesDS entries)
		{
			SqlConnection conn = new SqlConnection(m_connStr);
			SqlCommand cmdInsert = new SqlCommand();
			SqlCommand cmdUpdate = new SqlCommand();
			SqlCommand cmdDelete = new SqlCommand();

			// Asssociate connections
			cmdInsert.Connection = conn;
			cmdDelete.Connection = conn;
			cmdUpdate.Connection = conn;

			// 
			// cmdInsert params and query
			// 
			cmdInsert.CommandText = @"INSERT INTO TimeEntries(Id, StartTime, StopTime, Duration, Task, Customer, Description, Comments) VALUES (@Id, @StartTime, @StopTime, @Duration, @Task, @Customer, @Description, @Comments); SELECT Id, StartTime, StopTime, Duration, Task, Customer, Description, Comments FROM TimeEntries WHERE (Id = @Id)";
			cmdInsert.Parameters.Add(new SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier, 16, "Id"));
			cmdInsert.Parameters.Add(new SqlParameter("@StartTime", System.Data.SqlDbType.DateTime, 8, "StartTime"));
			cmdInsert.Parameters.Add(new SqlParameter("@StopTime", System.Data.SqlDbType.DateTime, 8, "StopTime"));
			cmdInsert.Parameters.Add(new SqlParameter("@Duration", System.Data.SqlDbType.Float, 8, "Duration"));
			cmdInsert.Parameters.Add(new SqlParameter("@Task", System.Data.SqlDbType.VarChar, 100, "Task"));
			cmdInsert.Parameters.Add(new SqlParameter("@Customer", System.Data.SqlDbType.VarChar, 100, "Customer"));
			cmdInsert.Parameters.Add(new SqlParameter("@Description", System.Data.SqlDbType.VarChar, 2147483647, "Description"));
			cmdInsert.Parameters.Add(new SqlParameter("@Comments", System.Data.SqlDbType.VarChar, 2147483647, "Comments"));
			// 
			// cmdUpdate  params and query
			// 
			cmdUpdate.CommandText = @"UPDATE TimeEntries SET Id = @Id, StartTime = @StartTime, StopTime = @StopTime, Duration = @Duration, Task = @Task, Customer = @Customer, Description = @Description, Comments = @Comments WHERE (Id = @Original_Id) AND (Customer = @Original_Customer OR @Original_Customer IS NULL AND Customer IS NULL) AND (Duration = @Original_Duration) AND (StartTime = @Original_StartTime OR @Original_StartTime IS NULL AND StartTime IS NULL) AND (StopTime = @Original_StopTime OR @Original_StopTime IS NULL AND StopTime IS NULL) AND (Task = @Original_Task); SELECT Id, StartTime, StopTime, Duration, Task, Customer, Description, Comments FROM TimeEntries WHERE (Id = @Id)";
			cmdUpdate.Parameters.Add(new SqlParameter("@Id", System.Data.SqlDbType.UniqueIdentifier, 16, "Id"));
			cmdUpdate.Parameters.Add(new SqlParameter("@StartTime", System.Data.SqlDbType.DateTime, 8, "StartTime"));
			cmdUpdate.Parameters.Add(new SqlParameter("@StopTime", System.Data.SqlDbType.DateTime, 8, "StopTime"));
			cmdUpdate.Parameters.Add(new SqlParameter("@Duration", System.Data.SqlDbType.Float, 8, "Duration"));
			cmdUpdate.Parameters.Add(new SqlParameter("@Task", System.Data.SqlDbType.VarChar, 100, "Task"));
			cmdUpdate.Parameters.Add(new SqlParameter("@Customer", System.Data.SqlDbType.VarChar, 100, "Customer"));
			cmdUpdate.Parameters.Add(new SqlParameter("@Description", System.Data.SqlDbType.VarChar, 2147483647, "Description"));
			cmdUpdate.Parameters.Add(new SqlParameter("@Comments", System.Data.SqlDbType.VarChar, 2147483647, "Comments"));
			cmdUpdate.Parameters.Add(new SqlParameter("@Original_Id", System.Data.SqlDbType.UniqueIdentifier, 16, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Id", System.Data.DataRowVersion.Original, null));
			cmdUpdate.Parameters.Add(new SqlParameter("@Original_Customer", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Customer", System.Data.DataRowVersion.Original, null));
			cmdUpdate.Parameters.Add(new SqlParameter("@Original_Duration", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Duration", System.Data.DataRowVersion.Original, null));
			cmdUpdate.Parameters.Add(new SqlParameter("@Original_StartTime", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "StartTime", System.Data.DataRowVersion.Original, null));
			cmdUpdate.Parameters.Add(new SqlParameter("@Original_StopTime", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "StopTime", System.Data.DataRowVersion.Original, null));
			cmdUpdate.Parameters.Add(new SqlParameter("@Original_Task", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Task", System.Data.DataRowVersion.Original, null));
			// 
			// cmdDelete params and query
			// 
			cmdDelete.CommandText = @"DELETE FROM TimeEntries WHERE (Id = @Original_Id) AND (Customer = @Original_Customer OR @Original_Customer IS NULL AND Customer IS NULL) AND (Duration = @Original_Duration) AND (StartTime = @Original_StartTime OR @Original_StartTime IS NULL AND StartTime IS NULL) AND (StopTime = @Original_StopTime OR @Original_StopTime IS NULL AND StopTime IS NULL) AND (Task = @Original_Task)";
			cmdDelete.Parameters.Add(new SqlParameter("@Original_Id", System.Data.SqlDbType.UniqueIdentifier, 16, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Id", System.Data.DataRowVersion.Original, null));
			cmdDelete.Parameters.Add(new SqlParameter("@Original_Customer", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Customer", System.Data.DataRowVersion.Original, null));
			cmdDelete.Parameters.Add(new SqlParameter("@Original_Duration", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Duration", System.Data.DataRowVersion.Original, null));
			cmdDelete.Parameters.Add(new SqlParameter("@Original_StartTime", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "StartTime", System.Data.DataRowVersion.Original, null));
			cmdDelete.Parameters.Add(new SqlParameter("@Original_StopTime", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "StopTime", System.Data.DataRowVersion.Original, null));
			cmdDelete.Parameters.Add(new SqlParameter("@Original_Task", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Task", System.Data.DataRowVersion.Original, null));

			// Perform the update
			SqlHelper.UpdateDataset(cmdInsert,cmdDelete,cmdUpdate,entries,entries.TimeEntries.TableName);

		}
	}
}
