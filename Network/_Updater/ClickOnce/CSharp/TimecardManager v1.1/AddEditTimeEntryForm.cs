using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace TimecardManager
{
	/// <summary>
	/// Summary description for AddEditTimeEntryForm.
	/// </summary>
	public class AddEditTimeEntryForm : Form
	{
		private Label label1;
		private Label label2;
		private Label label3;
		private Label lblTask;
		private Label label5;
		private Label lblDescription;
		private Label lblComments;
		private TextBox txtDuration;
		private TextBox txtTask;
		private TextBox txtCustomer;
		private TextBox txtDescription;
		private Button btnSave;
		private Button btnCancel;
		private TimeEntriesDS m_dsTimeEntries;
		private TextBox txtStartTime;
		private TextBox txtStopTime;
		private Label label4;
		private TextBox txtComments;
		private TextBox txtDate;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AddEditTimeEntryForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// Set the dialog buttons
			this.CancelButton = btnCancel;
			this.AcceptButton = btnSave;

			// Set up the custom bindings for the date and time fields
			BindDateTime();
		}

		/// <summary>
		/// The DataSet used to populate the form and get updated values
		/// </summary>
		public TimeEntriesDS TimeEntries
		{
			get
			{
				return m_dsTimeEntries;
			}
			set
			{
				// Merge in the values
				m_dsTimeEntries.Merge(value);
				// Make sure someone didn't give us something unexpected
				if (m_dsTimeEntries.TimeEntries.Count > 1 || m_dsTimeEntries.TimeEntries.Count == 0)
				{
					throw new ArgumentException("You can only provide a data set with a single row in the TimeEntries table to this form.");
				}
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.lblTask = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.lblDescription = new System.Windows.Forms.Label();
			this.lblComments = new System.Windows.Forms.Label();
			this.m_dsTimeEntries = new TimecardManager.TimeEntriesDS();
			this.txtDuration = new System.Windows.Forms.TextBox();
			this.txtTask = new System.Windows.Forms.TextBox();
			this.txtCustomer = new System.Windows.Forms.TextBox();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.txtComments = new System.Windows.Forms.TextBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.txtStartTime = new System.Windows.Forms.TextBox();
			this.txtStopTime = new System.Windows.Forms.TextBox();
			this.txtDate = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.m_dsTimeEntries)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "Start";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 16);
			this.label2.TabIndex = 4;
			this.label2.Text = "Stop";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 80);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 16);
			this.label3.TabIndex = 6;
			this.label3.Text = "Duration";
			// 
			// lblTask
			// 
			this.lblTask.Location = new System.Drawing.Point(8, 104);
			this.lblTask.Name = "lblTask";
			this.lblTask.Size = new System.Drawing.Size(72, 16);
			this.lblTask.TabIndex = 8;
			this.lblTask.Text = "Task";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 128);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 10;
			this.label5.Text = "Customer";
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(8, 152);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(72, 16);
			this.lblDescription.TabIndex = 12;
			this.lblDescription.Text = "Description";
			// 
			// lblComments
			// 
			this.lblComments.Location = new System.Drawing.Point(8, 216);
			this.lblComments.Name = "lblComments";
			this.lblComments.Size = new System.Drawing.Size(72, 16);
			this.lblComments.TabIndex = 14;
			this.lblComments.Text = "Comments";
			// 
			// m_dsTimeEntries
			// 
			this.m_dsTimeEntries.DataSetName = "TimeEntriesDS";
			this.m_dsTimeEntries.Locale = new System.Globalization.CultureInfo("en-US");
			// 
			// txtDuration
			// 
			this.txtDuration.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.m_dsTimeEntries, "TimeEntries.Duration"));
			this.txtDuration.Location = new System.Drawing.Point(80, 80);
			this.txtDuration.Name = "txtDuration";
			this.txtDuration.TabIndex = 7;
			this.txtDuration.Text = "";
			// 
			// txtTask
			// 
			this.txtTask.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.m_dsTimeEntries, "TimeEntries.Task"));
			this.txtTask.Location = new System.Drawing.Point(80, 104);
			this.txtTask.Name = "txtTask";
			this.txtTask.TabIndex = 9;
			this.txtTask.Text = "";
			// 
			// txtCustomer
			// 
			this.txtCustomer.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.m_dsTimeEntries, "TimeEntries.Customer"));
			this.txtCustomer.Location = new System.Drawing.Point(80, 128);
			this.txtCustomer.Name = "txtCustomer";
			this.txtCustomer.Size = new System.Drawing.Size(152, 20);
			this.txtCustomer.TabIndex = 11;
			this.txtCustomer.Text = "";
			// 
			// txtDescription
			// 
			this.txtDescription.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.m_dsTimeEntries, "TimeEntries.Description"));
			this.txtDescription.Location = new System.Drawing.Point(80, 152);
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(328, 56);
			this.txtDescription.TabIndex = 13;
			this.txtDescription.Text = "";
			// 
			// txtComments
			// 
			this.txtComments.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.m_dsTimeEntries, "TimeEntries.Comments"));
			this.txtComments.Location = new System.Drawing.Point(80, 216);
			this.txtComments.Multiline = true;
			this.txtComments.Name = "txtComments";
			this.txtComments.Size = new System.Drawing.Size(328, 56);
			this.txtComments.TabIndex = 15;
			this.txtComments.Text = "";
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(16, 288);
			this.btnSave.Name = "btnSave";
			this.btnSave.TabIndex = 16;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(352, 288);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 17;
			this.btnCancel.Text = "Cancel";
			// 
			// txtStartTime
			// 
			this.txtStartTime.Location = new System.Drawing.Point(80, 32);
			this.txtStartTime.Name = "txtStartTime";
			this.txtStartTime.TabIndex = 3;
			this.txtStartTime.Text = "";
			// 
			// txtStopTime
			// 
			this.txtStopTime.Location = new System.Drawing.Point(80, 56);
			this.txtStopTime.Name = "txtStopTime";
			this.txtStopTime.TabIndex = 5;
			this.txtStopTime.Text = "";
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(80, 8);
			this.txtDate.Name = "txtDate";
			this.txtDate.TabIndex = 1;
			this.txtDate.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 8);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(72, 16);
			this.label4.TabIndex = 0;
			this.label4.Text = "Date";
			// 
			// AddEditTimeEntryForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(440, 318);
			this.Controls.Add(this.txtDate);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtStopTime);
			this.Controls.Add(this.txtStartTime);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.txtComments);
			this.Controls.Add(this.txtDescription);
			this.Controls.Add(this.txtCustomer);
			this.Controls.Add(this.txtTask);
			this.Controls.Add(this.txtDuration);
			this.Controls.Add(this.lblComments);
			this.Controls.Add(this.lblDescription);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.lblTask);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "AddEditTimeEntryForm";
			this.Text = "Add/Edit Time Entry";
			this.Load += new System.EventHandler(this.OnFormLoad);
			((System.ComponentModel.ISupportInitialize)(this.m_dsTimeEntries)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// Set up custom bindings for the date and time fields
		/// </summary>
		private void BindDateTime()
		{
			// Date field binding
			Binding bDate = new Binding("Text",m_dsTimeEntries,"TimeEntries.StartTime");
			bDate.Format += new ConvertEventHandler(FormatDate);
			bDate.Parse +=new ConvertEventHandler(ParseDate);

			// Start time binding
			Binding bStartTime = new Binding("Text",m_dsTimeEntries,"TimeEntries.StartTime");
			bStartTime.Format +=new ConvertEventHandler(FormatStartTime);
			bStartTime.Parse += new ConvertEventHandler(ParseStartTime);

			// Stop time binding
			Binding bStopTime = new Binding("Text",m_dsTimeEntries,"TimeEntries.StopTime");
			bStopTime.Format += new ConvertEventHandler(FormatStopTime);
			bStopTime.Parse += new ConvertEventHandler(ParseStopTime);

			// Add the bindings to their respective controls
			txtDate.DataBindings.Add(bDate);
			txtStartTime.DataBindings.Add(bStartTime);
			txtStopTime.DataBindings.Add(bStopTime);
		}

		/// <summary>
		/// Make the changes current in the dataset so that they can be merged on
		/// the other end, then close the dialog with an OK result
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			m_dsTimeEntries.AcceptChanges();
			DialogResult = DialogResult.OK;
			Close();
		}

		/// <summary>
		/// Initialize the dataset if needed for add scenario,
		/// and set the currency manager to the one and only record we
		/// care about.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFormLoad(object sender, System.EventArgs e)
		{
			// If there is nothing in the data set, add something!
			if (m_dsTimeEntries.TimeEntries.Count < 1)
			{
				// Add a new row
				TimeEntriesDS.TimeEntriesRow newrow = m_dsTimeEntries.TimeEntries.NewTimeEntriesRow();
				// Initialize the values
				newrow.Id = Guid.NewGuid();
				newrow.StartTime = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,DateTime.Now.Hour,0,0);
				newrow.StopTime = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,DateTime.Now.Hour,0,0);
				newrow.Duration = 0;
				newrow.Task = string.Empty;
				newrow.Description  = string.Empty;
				newrow.Comments = string.Empty;
				// Add it to the dataset
				m_dsTimeEntries.TimeEntries.AddTimeEntriesRow(newrow);
			}

			// Get the currency manager for the dataset
			CurrencyManager cm = BindingContext[m_dsTimeEntries] as CurrencyManager;
			if (cm != null)
			{
				// Set the position to zero to be sure
				cm.Position = 0;
			}
		}

		/// <summary>
		/// Format the date field to just the short date for display
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FormatDate(object sender, ConvertEventArgs e)
		{
			e.Value = ((DateTime) e.Value).ToShortDateString();
		}

		/// <summary>
		/// Parse changed values in the date field into the date for
		/// both the start time and stop time
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ParseDate(object sender, ConvertEventArgs e)
		{
			e.Value = DateTime.Parse(m_dsTimeEntries.TimeEntries[0].StartTime.ToShortTimeString() + " " + (string)e.Value);
			m_dsTimeEntries.TimeEntries[0].StopTime = DateTime.Parse(m_dsTimeEntries.TimeEntries[0].StopTime.ToShortTimeString() + " " + m_dsTimeEntries.TimeEntries[0].StartTime.ToShortDateString());
		}

		/// <summary>
		/// Format the start time to a short time format for display
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FormatStartTime(object sender, ConvertEventArgs e)
		{
			e.Value = ((DateTime) e.Value).ToShortTimeString();
		}

		/// <summary>
		/// Parse changed values in the start time field into a
		/// full date time based on the date field and the entered time
		/// and update the duration
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ParseStartTime(object sender, ConvertEventArgs e)
		{
			DateTime newStart = DateTime.Parse((string)e.Value + " " + m_dsTimeEntries.TimeEntries[0].StartTime.ToShortDateString());
			e.Value = newStart;
			UpdateDuration(m_dsTimeEntries.TimeEntries[0].StopTime,newStart);
		}

		/// <summary>
		/// Format the stop time for display
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FormatStopTime(object sender, ConvertEventArgs e)
		{
			e.Value = ((DateTime) e.Value).ToShortTimeString();

		}

		/// <summary>
		/// Parse changed values in the stop time field to include 
		/// the date from the date field and update the duration.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ParseStopTime(object sender, ConvertEventArgs e)
		{
			DateTime newStop = DateTime.Parse((string)e.Value + " " + m_dsTimeEntries.TimeEntries[0].StartTime.ToShortDateString());
			e.Value = newStop;
			UpdateDuration(newStop, m_dsTimeEntries.TimeEntries[0].StartTime);
		}

		/// <summary>
		/// Update the duration automatically based on the entered values
		/// in the start and stop time fields
		/// </summary>
		/// <param name="stopTime"></param>
		/// <param name="startTime"></param>
		private void UpdateDuration(DateTime stopTime, DateTime startTime)
		{
			TimeSpan ts = stopTime - startTime;
			txtDuration.Text = (ts.Hours + (ts.Minutes/60.0f)).ToString();
		}


	}
}
