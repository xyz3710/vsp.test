IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'TimeManager')
	DROP DATABASE [TimeManager]
GO

CREATE DATABASE [TimeManager]  ON (NAME = N'TimeManager_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL\data\TimeManager_Data.MDF' , SIZE = 1, FILEGROWTH = 10%) LOG ON (NAME = N'TimeManager_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL\data\TimeManager_Log.LDF' , SIZE = 1, FILEGROWTH = 10%)
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO

exec sp_dboption N'TimeManager', N'autoclose', N'false'
GO

exec sp_dboption N'TimeManager', N'bulkcopy', N'false'
GO

exec sp_dboption N'TimeManager', N'trunc. log', N'false'
GO

exec sp_dboption N'TimeManager', N'torn page detection', N'true'
GO

exec sp_dboption N'TimeManager', N'read only', N'false'
GO

exec sp_dboption N'TimeManager', N'dbo use', N'false'
GO

exec sp_dboption N'TimeManager', N'single', N'false'
GO

exec sp_dboption N'TimeManager', N'autoshrink', N'false'
GO

exec sp_dboption N'TimeManager', N'ANSI null default', N'false'
GO

exec sp_dboption N'TimeManager', N'recursive triggers', N'false'
GO

exec sp_dboption N'TimeManager', N'ANSI nulls', N'false'
GO

exec sp_dboption N'TimeManager', N'concat null yields null', N'false'
GO

exec sp_dboption N'TimeManager', N'cursor close on commit', N'false'
GO

exec sp_dboption N'TimeManager', N'default to local cursor', N'false'
GO

exec sp_dboption N'TimeManager', N'quoted identifier', N'false'
GO

exec sp_dboption N'TimeManager', N'ANSI warnings', N'false'
GO

exec sp_dboption N'TimeManager', N'auto create statistics', N'true'
GO

exec sp_dboption N'TimeManager', N'auto update statistics', N'true'
GO

if( ( (@@microsoftversion / power(2, 24) = 8) and (@@microsoftversion & 0xffff >= 724) ) or ( (@@microsoftversion / power(2, 24) = 7) and (@@microsoftversion & 0xffff >= 1082) ) )
	exec sp_dboption N'TimeManager', N'db chaining', N'false'
GO

use [TimeManager]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTimeEntries]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetTimeEntries]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SetTimeEntry]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SetTimeEntry]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TimeEntries]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TimeEntries]
GO

CREATE TABLE [dbo].[TimeEntries] (
	[Id] [uniqueidentifier] NOT NULL ,
	[StartTime] [datetime] NULL ,
	[StopTime] [datetime] NULL ,
	[Duration] [float] NOT NULL ,
	[Task] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Customer] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Description] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Comments] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE GetTimeEntries
AS
	SELECT Id, StartTime, StopTime, Duration, Task, Customer, Description, Comments FROM TimeEntries
	RETURN 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE SetTimeEntry
	@Id uniqueidentifier OUTPUT,
	@StartTime datetime = NULL,
	@StopTime datetime = NULL,
	@Duration float,
	@Task varchar(100),
	@Customer varchar(100) = NULL,
	@Description text,
	@Comments text = NULL
AS
	IF NOT EXISTS(SELECT Id FROM TimeEntries WHERE Id = @Id)
	BEGIN -- Doing an insert
		INSERT INTO TimeEntries (Id, StartTime, StopTime, Duration, Task, Customer, Description, Comments)
			VALUES (@Id, @StartTime, @StopTime, @Duration, @Task, @Customer, @Description, @Comments)
	END -- Doing an update
	ELSE
	BEGIN
		UPDATE TimeEntries SET
			StartTime = @StartTime,
			StopTime = @StopTime,
			Duration = @Duration,
			Task = @Task,
			Customer = @Customer,
			Description = @Description,
			Comments = @Comments
		WHERE Id = @Id	
	END
	
	RETURN 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

