using System;
using System.Xml.Serialization;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Configuration;

namespace Katersoft.HttpDownloader
{
	/// <summary>
	/// Contains configuration for the <see cref="HttpDownloader"/>.
	/// </summary>
	[XmlRoot("downloader", Namespace = ApplicationUpdaterSettings.ConfigurationNamespace)]
	public class HttpDownloaderProviderData : DownloadProviderData
	{
		/// <summary>
		/// See <see cref="DownloadBufferSize"/>.
		/// </summary>
		private int _downloadBufferSize;

		/// <summary>
		/// See <see cref="DownloadProgressMaximumFrequency"/>
		/// </summary>
		private int _downloadProgressMaximumProgress;

		/// <summary>
		/// The minimum value that can be assigned to <see cref="DownloadBufferSize"/>.
		/// </summary>
		private const int DOWNLOAD_BUFFER_SIZE_MIN = 256;

		/// <summary>
		/// The maximum value that can be assigned to <see cref="DownloadBufferSize"/>.
		/// </summary>
		private const int DOWNLOAD_BUFFER_SIZE_MAX = 10240;

		/// <summary>
		/// The minimum value that can be assigned to <see cref="DownloadProgressMaximumFrequency"/>.
		/// </summary>
		private const int DOWNLOAD_PROGRESS_MAXIMUM_FREQUENCY_MIN = 0;
		
		/// <summary>
		/// The maximum value that can be assigned to <see cref="DownloadProgressMaximumFrequency"/>.
		/// </summary>
		private const int DOWNLOAD_PROGRESS_MAXIMUM_FREQUENCY_MAX = 5000;

		/// <summary>
		/// Gets or sets the associated type name.
		/// </summary>
		[XmlIgnore]
		public override string TypeName
		{
			get
			{
				return typeof(HttpDownloader).AssemblyQualifiedName;
			}
			set
			{
				//nothing to do
			}
		}

		/// <summary>
		/// Gets or sets the buffer size, in bytes, used whilst downloading files.
		/// </summary>
		/// <remarks>
		/// The HTTP downloader reports progress each time it fills the download buffer. Therefore, larger buffer sizes will result
		/// in fewer progress reports.
		/// </remarks>
		[XmlElement("downloadBufferSize")]
		public int DownloadBufferSize
		{
			get
			{
				return _downloadBufferSize;
			}
			set
			{
				if ((value < DOWNLOAD_BUFFER_SIZE_MIN) || (value > DOWNLOAD_BUFFER_SIZE_MAX))
				{
					throw new ArgumentException(string.Format("DownloadBufferSize must be between {0} and {1}", DOWNLOAD_BUFFER_SIZE_MIN, DOWNLOAD_BUFFER_SIZE_MAX));
				}

				_downloadBufferSize = value;
			}
		}

		/// <summary>
		/// Gets or sets the maximum frequency, in milliseconds, that progress will be reported by the HTTP downloader.
		/// </summary>
		/// <remarks>
		/// <para>
		/// This property allows you to limit the number of progress reports issued by the HTTP downloader. The downloader will ensure that
		/// the amount of time specified by this property has elapsed prior to issuing another progress report. If enough time has not elapsed
		/// then the downloader will not issue a progress report at that time.
		/// </para>
		/// <para>
		/// Setting this property to <c>0</c> ensures that the downloader always reports progress, regardless of the amount of time that has
		/// elapsed since the last progress report.
		/// </para>
		/// </remarks>
		[XmlElement("downloadProgressMaximumFrequency")]
		public int DownloadProgressMaximumFrequency
		{
			get
			{
				return _downloadProgressMaximumProgress;
			}
			set
			{
				if ((value < DOWNLOAD_PROGRESS_MAXIMUM_FREQUENCY_MIN) || (value > DOWNLOAD_PROGRESS_MAXIMUM_FREQUENCY_MAX))
				{
					throw new ArgumentException(string.Format("DownloadProgressMaximumFrequency must be between {0} and {1}", DOWNLOAD_PROGRESS_MAXIMUM_FREQUENCY_MIN, DOWNLOAD_PROGRESS_MAXIMUM_FREQUENCY_MAX));
				}

				_downloadProgressMaximumProgress = value;
			}
		}

		/// <summary>
		/// Default constructor.
		/// </summary>
		public HttpDownloaderProviderData()
		{
			DownloadBufferSize = 1024;
		}
	}
}
