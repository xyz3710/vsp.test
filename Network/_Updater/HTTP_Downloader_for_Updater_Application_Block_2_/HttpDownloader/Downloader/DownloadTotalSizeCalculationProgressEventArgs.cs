using System;

namespace Katersoft.HttpDownloader
{
	/// <summary>
	/// Summary description for DownloadTotalSizeCalculationProgressEventArgs.
	/// </summary>
	public sealed class DownloadTotalSizeCalculationProgressEventArgs : EventArgs
	{
		/// <summary>
		/// See <see cref="TotalFiles"/>.
		/// </summary>
		private int _totalFiles;

		/// <summary>
		/// See <see cref="CalculatedFiles"/>.
		/// </summary>
		private int _calculatedFiles;

		/// <summary>
		/// Gets the total number of files whose sizes are to be calculated.
		/// </summary>
		public int TotalFiles
		{
			get
			{
				return _totalFiles;
			}
		}

		/// <summary>
		/// Gets the number of files whose size has been calculated thus far.
		/// </summary>
		public int CalculatedFiles
		{
			get
			{
				return _calculatedFiles;
			}
		}

		/// <summary>
		/// Constructs an instance of <c>DownloadTotalSizeCalculationProgressEventArgs</c> with the specified information.
		/// </summary>
		public DownloadTotalSizeCalculationProgressEventArgs(int totalFiles, int calculatedFiles)
		{
			_totalFiles = totalFiles;
			_calculatedFiles = calculatedFiles;
		}
	}
}
