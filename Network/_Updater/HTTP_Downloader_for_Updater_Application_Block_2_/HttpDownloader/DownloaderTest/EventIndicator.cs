using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace DownloaderTest
{
	/// <summary>
	/// Summary description for EventIndicator.
	/// </summary>
	public class EventIndicator : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Timer timer;
		private System.Windows.Forms.Panel fireIndicator;
		private System.Windows.Forms.Label eventNameLabel;
		private System.ComponentModel.IContainer components;

		public EventIndicator()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		[Browsable(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		public override string Text
		{
			get
			{
				return eventNameLabel.Text;
			}
			set
			{
				eventNameLabel.Text = value;
			}
		}

		public void EventFired()
		{
			fireIndicator.BackColor = Color.FromArgb(100, 255, 100);
			timer.Enabled = true;
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.eventNameLabel = new System.Windows.Forms.Label();
			this.fireIndicator = new System.Windows.Forms.Panel();
			this.timer = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// eventNameLabel
			// 
			this.eventNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.eventNameLabel.Location = new System.Drawing.Point(24, 0);
			this.eventNameLabel.Name = "eventNameLabel";
			this.eventNameLabel.TabIndex = 0;
			this.eventNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fireIndicator
			// 
			this.fireIndicator.BackColor = System.Drawing.Color.Black;
			this.fireIndicator.Location = new System.Drawing.Point(0, 0);
			this.fireIndicator.Name = "fireIndicator";
			this.fireIndicator.Size = new System.Drawing.Size(24, 24);
			this.fireIndicator.TabIndex = 1;
			// 
			// timer
			// 
			this.timer.Tick += new System.EventHandler(this.timer_Tick);
			// 
			// EventIndicator
			// 
			this.Controls.Add(this.fireIndicator);
			this.Controls.Add(this.eventNameLabel);
			this.Name = "EventIndicator";
			this.Size = new System.Drawing.Size(128, 24);
			this.ResumeLayout(false);

		}
		#endregion

		private void timer_Tick(object sender, System.EventArgs e)
		{
			int delta = 15;
			int r = Math.Max(0, fireIndicator.BackColor.R - delta);
			int g = Math.Max(0, fireIndicator.BackColor.G - delta);
			int b = Math.Max(0, fireIndicator.BackColor.B - delta);
			fireIndicator.BackColor = Color.FromArgb(r, g, b);

			if ((r == 0) && (g == 0) && (b == 0))
			{
				timer.Enabled = false;
			}
		}
	}
}
