using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Text;
using System.Data;
using Microsoft.ApplicationBlocks.Updater;
using Katersoft.HttpDownloader;

namespace DownloaderTest
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button beginDownloadButton;
		private DownloaderTest.EventIndicator downloadTotalSizeCalculationStartedEventIndicator;
		private DownloaderTest.EventIndicator downloadTotalSizeCalculationCompletedEventIndicator;
		private DownloaderTest.EventIndicator downloadStartedEventIndicator;
		private DownloaderTest.EventIndicator downloadProgressEventIndicator;
		private DownloaderTest.EventIndicator downloadCompletedEventIndicator;
		private DownloaderTest.EventIndicator downloadErrorEventIndicator;
		private System.Windows.Forms.Label eventsLabel;
		private DownloaderTest.EventIndicator downloadTotalSizeCalculationProgressEventIndicator;
		private System.Windows.Forms.Button clearCacheButton;

		/// <summary>
		/// Manages updates.
		/// </summary>
		private readonly ApplicationUpdaterManager UPDATE_MANAGER;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//manages updates
			UPDATE_MANAGER = ApplicationUpdaterManager.GetUpdater();

			//listen to events
			HttpDownloader.DownloadTotalSizeCalculationStarted += new Katersoft.HttpDownloader.HttpDownloader.DownloadTotalSizeCalculationStartedEventHandler(HttpDownloader_DownloadTotalSizeCalculationStarted);
			HttpDownloader.DownloadTotalSizeCalculationProgress += new Katersoft.HttpDownloader.HttpDownloader.DownloadTotalSizeCalculationProgressEventHandler(HttpDownloader_DownloadTotalSizeCalculationProgress);
			HttpDownloader.DownloadTotalSizeCalculationCompleted += new Katersoft.HttpDownloader.HttpDownloader.DownloadTotalSizeCalculationCompletedEventHandler(HttpDownloader_DownloadTotalSizeCalculationCompleted);
			UPDATE_MANAGER.DownloadStarted += new DownloadStartedEventHandler(UPDATE_MANAGER_DownloadStarted);
			UPDATE_MANAGER.DownloadProgress += new DownloadProgressEventHandler(UPDATE_MANAGER_DownloadProgress);
			UPDATE_MANAGER.DownloadCompleted += new DownloadCompletedEventHandler(UPDATE_MANAGER_DownloadCompleted);
			UPDATE_MANAGER.DownloadError += new DownloadErrorEventHandler(UPDATE_MANAGER_DownloadError);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.downloadTotalSizeCalculationStartedEventIndicator = new DownloaderTest.EventIndicator();
			this.beginDownloadButton = new System.Windows.Forms.Button();
			this.downloadTotalSizeCalculationProgressEventIndicator = new DownloaderTest.EventIndicator();
			this.downloadTotalSizeCalculationCompletedEventIndicator = new DownloaderTest.EventIndicator();
			this.downloadStartedEventIndicator = new DownloaderTest.EventIndicator();
			this.downloadProgressEventIndicator = new DownloaderTest.EventIndicator();
			this.downloadCompletedEventIndicator = new DownloaderTest.EventIndicator();
			this.downloadErrorEventIndicator = new DownloaderTest.EventIndicator();
			this.eventsLabel = new System.Windows.Forms.Label();
			this.clearCacheButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// downloadTotalSizeCalculationStartedEventIndicator
			// 
			this.downloadTotalSizeCalculationStartedEventIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.downloadTotalSizeCalculationStartedEventIndicator.Location = new System.Drawing.Point(10, 87);
			this.downloadTotalSizeCalculationStartedEventIndicator.Name = "downloadTotalSizeCalculationStartedEventIndicator";
			this.downloadTotalSizeCalculationStartedEventIndicator.Size = new System.Drawing.Size(396, 26);
			this.downloadTotalSizeCalculationStartedEventIndicator.TabIndex = 3;
			this.downloadTotalSizeCalculationStartedEventIndicator.Text = "DownloadTotalSizeCalculationStarted";
			// 
			// beginDownloadButton
			// 
			this.beginDownloadButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.beginDownloadButton.Location = new System.Drawing.Point(10, 43);
			this.beginDownloadButton.Name = "beginDownloadButton";
			this.beginDownloadButton.Size = new System.Drawing.Size(396, 25);
			this.beginDownloadButton.TabIndex = 1;
			this.beginDownloadButton.Text = "Download and Run Client App";
			this.beginDownloadButton.Click += new System.EventHandler(this.button1_Click);
			// 
			// downloadTotalSizeCalculationProgressEventIndicator
			// 
			this.downloadTotalSizeCalculationProgressEventIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.downloadTotalSizeCalculationProgressEventIndicator.Location = new System.Drawing.Point(10, 121);
			this.downloadTotalSizeCalculationProgressEventIndicator.Name = "downloadTotalSizeCalculationProgressEventIndicator";
			this.downloadTotalSizeCalculationProgressEventIndicator.Size = new System.Drawing.Size(396, 26);
			this.downloadTotalSizeCalculationProgressEventIndicator.TabIndex = 4;
			this.downloadTotalSizeCalculationProgressEventIndicator.Text = "DownloadTotalSizeCalculationProgress";
			// 
			// downloadTotalSizeCalculationCompletedEventIndicator
			// 
			this.downloadTotalSizeCalculationCompletedEventIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.downloadTotalSizeCalculationCompletedEventIndicator.Location = new System.Drawing.Point(10, 156);
			this.downloadTotalSizeCalculationCompletedEventIndicator.Name = "downloadTotalSizeCalculationCompletedEventIndicator";
			this.downloadTotalSizeCalculationCompletedEventIndicator.Size = new System.Drawing.Size(396, 26);
			this.downloadTotalSizeCalculationCompletedEventIndicator.TabIndex = 5;
			this.downloadTotalSizeCalculationCompletedEventIndicator.Text = "DownloadTotalSizeCalculationCompleted";
			// 
			// downloadStartedEventIndicator
			// 
			this.downloadStartedEventIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.downloadStartedEventIndicator.Location = new System.Drawing.Point(10, 190);
			this.downloadStartedEventIndicator.Name = "downloadStartedEventIndicator";
			this.downloadStartedEventIndicator.Size = new System.Drawing.Size(396, 26);
			this.downloadStartedEventIndicator.TabIndex = 6;
			this.downloadStartedEventIndicator.Text = "DownloadStarted";
			// 
			// downloadProgressEventIndicator
			// 
			this.downloadProgressEventIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.downloadProgressEventIndicator.Location = new System.Drawing.Point(10, 225);
			this.downloadProgressEventIndicator.Name = "downloadProgressEventIndicator";
			this.downloadProgressEventIndicator.Size = new System.Drawing.Size(396, 26);
			this.downloadProgressEventIndicator.TabIndex = 7;
			this.downloadProgressEventIndicator.Text = "DownloadProgress";
			// 
			// downloadCompletedEventIndicator
			// 
			this.downloadCompletedEventIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.downloadCompletedEventIndicator.Location = new System.Drawing.Point(10, 259);
			this.downloadCompletedEventIndicator.Name = "downloadCompletedEventIndicator";
			this.downloadCompletedEventIndicator.Size = new System.Drawing.Size(396, 26);
			this.downloadCompletedEventIndicator.TabIndex = 8;
			this.downloadCompletedEventIndicator.Text = "DownloadCompleted";
			// 
			// downloadErrorEventIndicator
			// 
			this.downloadErrorEventIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.downloadErrorEventIndicator.Location = new System.Drawing.Point(10, 294);
			this.downloadErrorEventIndicator.Name = "downloadErrorEventIndicator";
			this.downloadErrorEventIndicator.Size = new System.Drawing.Size(396, 26);
			this.downloadErrorEventIndicator.TabIndex = 9;
			this.downloadErrorEventIndicator.Text = "DownloadError";
			// 
			// eventsLabel
			// 
			this.eventsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.eventsLabel.AutoSize = true;
			this.eventsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.eventsLabel.Location = new System.Drawing.Point(10, 61);
			this.eventsLabel.Name = "eventsLabel";
			this.eventsLabel.Size = new System.Drawing.Size(104, 13);
			this.eventsLabel.TabIndex = 2;
			this.eventsLabel.Text = "Event Indicators:";
			// 
			// clearCacheButton
			// 
			this.clearCacheButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.clearCacheButton.Location = new System.Drawing.Point(11, 9);
			this.clearCacheButton.Name = "clearCacheButton";
			this.clearCacheButton.Size = new System.Drawing.Size(396, 24);
			this.clearCacheButton.TabIndex = 0;
			this.clearCacheButton.Text = "Clear Download Cache (force re-download)";
			this.clearCacheButton.Click += new System.EventHandler(this.button1_Click_1);
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(418, 327);
			this.Controls.Add(this.clearCacheButton);
			this.Controls.Add(this.eventsLabel);
			this.Controls.Add(this.downloadErrorEventIndicator);
			this.Controls.Add(this.downloadCompletedEventIndicator);
			this.Controls.Add(this.downloadProgressEventIndicator);
			this.Controls.Add(this.downloadStartedEventIndicator);
			this.Controls.Add(this.downloadTotalSizeCalculationCompletedEventIndicator);
			this.Controls.Add(this.downloadTotalSizeCalculationProgressEventIndicator);
			this.Controls.Add(this.beginDownloadButton);
			this.Controls.Add(this.downloadTotalSizeCalculationStartedEventIndicator);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MainForm";
			this.Text = "HTTP Downloader Test";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

		private void GetErrorMessage(StringBuilder sb, Exception e)
		{
			sb.Append(e.GetType().FullName).Append(" : ").Append(e.Message).Append(Environment.NewLine);
			sb.Append(e.StackTrace);

			if (e.InnerException != null)
			{
				GetErrorMessage(sb, e.InnerException);
			}
		}

		private void HandleError(Exception e)
		{
			StringBuilder sb = new StringBuilder();
			GetErrorMessage(sb, e);
			MessageBox.Show(sb.ToString());
		}

		private void RunClient()
		{
			//run the downloaded app
			try
			{
				string exe = Path.Combine(Environment.CurrentDirectory, @"files\Client.exe");
				ProcessStartInfo startInfo = new ProcessStartInfo(exe);
				startInfo.WorkingDirectory = Path.GetDirectoryName(exe);
				Process.Start(startInfo);
			} 
			catch (Exception ex)
			{
				HandleError(ex);
			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			//clearing the cache after downloaded causes the updater to die because it expects the directory to exist
			clearCacheButton.Enabled = false;

			try
			{
				UPDATE_MANAGER.CancelPendingUpdates();
				//check for any updates
				Manifest[] manifests = UPDATE_MANAGER.CheckForUpdates();

				if ((manifests != null) && (manifests.Length != 0))
				{
					//ensure all manifests are downloaded
					foreach (Manifest manifest in manifests)
					{
						manifest.Apply = true;
					}

					//start the download
					UPDATE_MANAGER.BeginDownload(manifests);
				}
				else
				{
					//nothing to download - just start the app coz it must have downloaded already
					RunClient();
				}
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
		}

		private void EventFired(EventIndicator eventIndicator)
		{
			eventIndicator.EventFired();
		}

		private delegate void SimpleDelegate();

		private void DownloadTotalSizeCalculationStarted()
		{
			EventFired(downloadTotalSizeCalculationStartedEventIndicator);
		}

		private delegate void DownloadTotalSizeCalculationProgressDelegate(DownloadTotalSizeCalculationProgressEventArgs e);
		private void DownloadTotalSizeCalculationProgress(DownloadTotalSizeCalculationProgressEventArgs e)
		{
			EventFired(downloadTotalSizeCalculationProgressEventIndicator);
			downloadTotalSizeCalculationProgressEventIndicator.Text = string.Format("DownloadTotalSizeCalculationProgress ({0:P})", e.CalculatedFiles / (double) e.TotalFiles);
		}

		private void DownloadTotalSizeCalculationCompleted()
		{
			EventFired(downloadTotalSizeCalculationCompletedEventIndicator);
		}

		private void DownloadStarted()
		{
			EventFired(downloadStartedEventIndicator);
		}

		private delegate void DownloadProgressDelegate(DownloadProgressEventArgs e);
		private void DownloadProgress(DownloadProgressEventArgs e)
		{
			EventFired(downloadProgressEventIndicator);
			downloadProgressEventIndicator.Text = string.Format("DownloadProgress ({0:P})", e.BytesTransferred / (double) e.BytesTotal);
		}

		private delegate void DownloadCompletedDelegate(ManifestEventArgs e);
		private void DownloadCompleted(ManifestEventArgs e)
		{
			EventFired(downloadCompletedEventIndicator);
			//activate the downloaded manifests
			UPDATE_MANAGER.Activate(new Manifest[] {e.Manifest});
			//run the downloaded app
			RunClient();
		}

		private delegate void DownloadErrorDelegate(Exception e);
		private void DownloadError(Exception e)
		{
			EventFired(downloadErrorEventIndicator);
			HandleError(e);
		}







		//all these events are firing on a non-UI thread so we just Invoke onto a UI thread

		private void HttpDownloader_DownloadTotalSizeCalculationStarted(object sender, Microsoft.ApplicationBlocks.Updater.TaskEventArgs e)
		{
			Invoke(new SimpleDelegate(DownloadTotalSizeCalculationStarted), null);
		}

		private void HttpDownloader_DownloadTotalSizeCalculationProgress(object sender, DownloadTotalSizeCalculationProgressEventArgs e)
		{
			Invoke(new DownloadTotalSizeCalculationProgressDelegate(DownloadTotalSizeCalculationProgress), new object[] {e});
		}

		private void HttpDownloader_DownloadTotalSizeCalculationCompleted(object sender, Microsoft.ApplicationBlocks.Updater.TaskEventArgs e)
		{
			Invoke(new SimpleDelegate(DownloadTotalSizeCalculationCompleted), null);
		}

		private void UPDATE_MANAGER_DownloadStarted(object sender, DownloadStartedEventArgs e)
		{
			Invoke(new SimpleDelegate(DownloadStarted), null);
		}

		private void UPDATE_MANAGER_DownloadProgress(object sender, DownloadProgressEventArgs e)
		{
			Invoke(new DownloadProgressDelegate(DownloadProgress), new object[] {e});
		}

		private void UPDATE_MANAGER_DownloadCompleted(object sender, ManifestEventArgs e)
		{
			Invoke(new DownloadCompletedDelegate(DownloadCompleted), new object[] {e});
		}

		private void UPDATE_MANAGER_DownloadError(object sender, ManifestErrorEventArgs e)
		{
			Invoke(new DownloadErrorDelegate(DownloadError), new object[] {e.Exception});
		}

		private void button1_Click_1(object sender, System.EventArgs e)
		{
			try
			{
				string dir = @"C:\Temp\HttpDownloaderTest";

				if (Directory.Exists(dir))
				{
					Directory.Delete(dir, true);
				}

				MessageBox.Show("Cache cleared.");
			}
			catch (Exception ex)
			{
				HandleError(ex);
			}
		}
	}
}
