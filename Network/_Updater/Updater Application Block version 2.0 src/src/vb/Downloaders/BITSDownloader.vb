 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' BitsDownloader.vb
'
' Contains the implementation of the BITS downloader.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Collections.Specialized
Imports System.Configuration
Imports System.Globalization
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Security.Permissions
Imports System.Threading
Imports Microsoft.ApplicationBlocks.Updater.Configuration
Imports Microsoft.ApplicationBlocks.Updater.Utilities
Imports Microsoft.Practices.EnterpriseLibrary.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.Downloaders

    ' <summary>
    ' This downloader uses BITS technology to download files.
    ' </summary>

    Friend NotInheritable Class BitsDownloader
        Implements IDownloader, IBackgroundCopyCallback, IDisposable
#Region "Declarations"

        ' <summary>
        ' This is used to wait for some time between checking the download status to avoid CPU consumtion.
        ' </summary>
        Private Const TimeToWaitDuringSynchronousDownload As Integer = 200 ' milliseconds
        ' <summary>
        ' Maximum time to wait for a pregress event.
        ' </summary>
        Private Const BitsNoProgressTimeout As Integer = 5 'seconds
        ' <summary>
        ' The delay between reties.
        ' </summary>
        Private Const BitsMinimumRetryDelay As Integer = 0 'immediate retry
        ' <summary>
        ' Constant for the COM error when an error is requested and no error have been raised.
        ' </summary>
        Private Const ExceptionCodeNotAnError As Integer = -2145386481

        ' <summary>
        ' The culture Id.
        ' </summary>
        Private CultureIdForGettingComErrorMessages As Integer = Int32.Parse(Resource.ResourceManager(Resource.MessageKey.CultureIdToGetComErrorStringsFormatted), CultureInfo.CurrentUICulture)

        ' <summary>
        ' Keeps all the pending downloader jobs.
        ' </summary>
        Private bitsDownloaderJobs As HybridDictionary = Nothing

        ' <summary>
        ' Enables access to the downloader configuration.
        ' </summary>
        Private updaterConfigurationView As UpdaterConfigurationView

        ' <summary>
        ' Name of the downloader.
        ' </summary>
        Private downloaderName As String

        ' <summary>
        ' The error message.
        ' </summary>
        Private cumulativeErrorMessage As String = String.Empty

        ' <summary>
        ' The key to the job Id to be stored in the task state
        ' </summary>
        Private Const TASK_JOBID_KEY As String = "jobId"

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "IDownloader Members"

#Region "Downloader events"

        ' <summary>
        ' Notifies the application of the download progress. 
        ' </summary>
        Public Event DownloadProgress As DownloadTaskProgressEventHandler Implements IDownloader.DownloadProgress


        ' <summary>
        ' Notifies the application when the download is complete.
        ' </summary>
        Public Event DownloadCompleted As DownloadTaskCompletedEventHandler Implements IDownloader.DownloadCompleted

        ' <summary>
        ' Notifies the application when there is a download error. 
        ' </summary>
        Public Event DownloadError As DownloadTaskErrorEventHandler Implements IDownloader.DownloadError


        ' <summary>
        ' Notifies the application that the download has started. 
        ' </summary>
        Public Event DownloadStarted As DownloadTaskStartedEventHandler Implements IDownloader.DownloadStarted


        ' <summary>
        ' Helper method to fire the event.
        ' </summary>
        ' <param name="e">The event information.</param>
        Private Sub OnDownloadStarted(ByVal e As TaskEventArgs)
            RaiseEvent DownloadStarted(Me, e)
        End Sub 'OnDownloadStarted


        ' <summary>
        ' Helper method to fire the event.
        ' </summary>
        ' <param name="e">The event information.</param>
        Private Sub OnDownloadProgress(ByVal e As DownloadTaskProgressEventArgs)
            RaiseEvent DownloadProgress(Me, e)
        End Sub 'OnDownloadProgress


        ' <summary>
        ' Helper method to fire the event.
        ' </summary>
        ' <param name="e">The event information.</param>
        Private Sub OnDownloadCompleted(ByVal e As TaskEventArgs)
            RaiseEvent DownloadCompleted(Me, e)
        End Sub 'OnDownloadCompleted


        ' <summary>
        ' Helper method to fire the event.
        ' </summary>
        ' <param name="e">The event information.</param>
        Private Sub OnDownloadError(ByVal e As DownloadTaskErrorEventArgs)
            RaiseEvent DownloadError(Me, e)
        End Sub 'OnDownloadError

#End Region


        ' <summary>
        ' Synchronous download method implementation.
        ' </summary>
        ' <param name="task">The UpdaterTask to process.</param>
        ' <param name="maxWaitTime">The maximum wait time.</param>
        <FileIOPermission(SecurityAction.Demand)> _
        Public Sub Download(ByVal task As UpdaterTask, ByVal maxWaitTime As TimeSpan) Implements IDownloader.Download
            Dim backGroundCopyManager As IBackgroundCopyManager = Nothing
            Dim backgroundCopyJob As IBackgroundCopyJob = Nothing
            Dim jobID As Guid = Guid.Empty

            Try
                '  create the manager
                backGroundCopyManager = BackgroundCopyManagerFactory.Create()

                ' If the job is already finished, just return
                If CheckForResumeAndProceed(backGroundCopyManager, task, backgroundCopyJob) Then
                    Return
                End If

                If backgroundCopyJob Is Nothing Then

                    '  use utility function to create manager, job, get back jobid etc.; uses 'out' semantics
                    CreateCopyJob(backGroundCopyManager, backgroundCopyJob, jobID, Resource.MessageKey.BitsJobName, Resource.MessageKey.BitsDescription)

                    task(TASK_JOBID_KEY) = jobID

                    PrepareJob(backgroundCopyJob, task)

                End If

                WaitForDownload(task, backgroundCopyJob, maxWaitTime)

            Catch e As Exception
                '  if exception, clean up job
                OnJobError(task, backgroundCopyJob, Nothing, e)
            Finally
                If Not (backgroundCopyJob Is Nothing) Then
                    Marshal.ReleaseComObject(backgroundCopyJob)
                End If
                If Not (backGroundCopyManager Is Nothing) Then
                    Marshal.ReleaseComObject(backGroundCopyManager)
                End If
            End Try

        End Sub 'Download


        ' <summary>
        ' Asynchronous download method implementation.
        ' </summary>
        ' <param name="task">The UpdaterTask to process.</param>
        <FileIOPermission(SecurityAction.Demand)> _
        Public Sub BeginDownload(ByVal task As UpdaterTask) Implements IDownloader.BeginDownload
            Dim backGroundCopyManager As IBackgroundCopyManager = Nothing
            Dim backgroundCopyJob As IBackgroundCopyJob = Nothing
            Dim jobID As Guid = Guid.Empty

            Try
                '  create the manager
                backGroundCopyManager = BackgroundCopyManagerFactory.Create()

                ' If the job is already finished, just return
                If CheckForResumeAndProceed(backGroundCopyManager, task, backgroundCopyJob) Then
                    Return
                End If

                If Not backgroundCopyJob Is Nothing Then
                    ' if CheckForResumeAndProceed connected to an ongoing BITS job
                    ' wire up our notify interface to forward events to the client

                    backgroundCopyJob.SetNotifyInterface(Me)

                    backgroundCopyJob.SetNotifyFlags(System.Convert.ToUInt32(BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_ERROR Or BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_MODIFICATION Or BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_TRANSFERRED)) 'ToDo: Unsigned Integers not supported

                Else

                    '  use utility function to create the job. 
                    CreateCopyJob(backGroundCopyManager, backgroundCopyJob, jobID, Resource.MessageKey.BitsFilesDownloadJobName, Resource.MessageKey.BitsFilesDownloadDescription)

                    task(TASK_JOBID_KEY) = jobID

                    PrepareJob(backgroundCopyJob, task)

                    backgroundCopyJob.SetNotifyInterface(Me)

                    backgroundCopyJob.SetNotifyFlags(System.Convert.ToUInt32(BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_ERROR Or BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_MODIFICATION Or BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_TRANSFERRED)) 'ToDo: Unsigned Integers not supported

                    OnDownloadStarted(New TaskEventArgs(task))
                    backgroundCopyJob.Resume()
                End If
            Catch e As Exception
                '  if exception, clean up job
                OnJobError(task, backgroundCopyJob, Nothing, e)
            Finally
                If Not (backgroundCopyJob Is Nothing) Then
                    Marshal.ReleaseComObject(backgroundCopyJob)
                End If
                If Not (backGroundCopyManager Is Nothing) Then
                    Marshal.ReleaseComObject(backGroundCopyManager)
                End If
            End Try

        End Sub 'BeginDownload


        ' <summary>
        ' Cancels an asynhronous download operation.
        ' </summary>
        ' <param name="task">The <see cref="UpdaterTask"/> for the operation.</param>
        ' <returns>Indicates whether the operation was canceled.</returns>
        Public Function CancelDownload(ByVal task As UpdaterTask) As Boolean Implements IDownloader.CancelDownload
            Dim copyManager As IBackgroundCopyManager = Nothing
            Dim pJob As IBackgroundCopyJob = Nothing

            If Not (task(TASK_JOBID_KEY) Is Nothing) Then
                Try
                    Dim jobID As Guid = CType(task(TASK_JOBID_KEY), Guid)
                    copyManager = BackgroundCopyManagerFactory.Create()
                    copyManager.GetJob(jobID, pJob)

                    If Not (pJob Is Nothing) Then
                        pJob.Cancel()
                    End If
                Finally
                    If Not (copyManager Is Nothing) Then
                        Marshal.ReleaseComObject(copyManager)
                    End If

                    If Not (pJob Is Nothing) Then
                        Marshal.ReleaseComObject(pJob)
                    End If
                End Try
            End If
            Return True

        End Function 'CancelDownload

#End Region

#Region "Private helper methods"


        ' <summary>
        ' Verifies if the task has a download job assigned, meaning this is a retry.
        ' If a transferred job is detected, the job is completed and the event
        ' OnDownloadCompleted is raised.
        ' </summary>
        ' <param name="copyManager">The BITS background copy manager to use</param>
        ' <param name="task">The UpdaterTask to get the data from</param>
        ' <param name="copyJob">If a on progress BITS job is found for this task, this job is returned on this parameter</param>
        ' <returns>A Boolean value indicating whether the job is completed or not.
        ' A True value means that the job has been completed by BITS while a False value
        ' means that the job doesn't exists or can be resumed.
        ' </returns>
        Private Function CheckForResumeAndProceed(ByVal copyManager As IBackgroundCopyManager, ByVal task As UpdaterTask, ByRef copyJob As IBackgroundCopyJob) As Boolean
            copyJob = Nothing
            If Not (task(TASK_JOBID_KEY) Is Nothing) Then
                Dim jobId As Guid = CType(task(TASK_JOBID_KEY), Guid)
                Dim jobState As BG_JOB_STATE

                Try
                    copyManager.GetJob(jobId, copyJob)
                    If Not (copyJob Is Nothing) Then
                        copyJob.GetState(jobState)
                        If jobState = BG_JOB_STATE.BG_JOB_STATE_TRANSFERRED Then
                            OnJobTransferred(task, copyJob)
                            Return True
                        End If

                    End If
                Catch ex As Exception
                    Logger.LogException(New ApplicationUpdaterException(Resource.ResourceManager(Resource.MessageKey.BITSCannotConnectToJob, jobId, task.TaskId), ex))
                End Try
            End If
            Return False
        End Function 'CheckForResumeAndProceed


        ' <summary>
        ' Locate the UpdaterTask associated with the given background job.
        ' </summary>
        ' <param name="pJob">The job reference.</param>
        ' <returns>The UpdaterTask for that job.</returns>
        Private Function FindTask(ByVal pJob As IBackgroundCopyJob) As UpdaterTask
            Dim jobID As Guid = Guid.Empty
            pJob.GetId(jobID)

            Dim task As UpdaterTask
            For Each task In ApplicationUpdaterManager.GetTasks()
                If Guid.Equals(task(TASK_JOBID_KEY), jobID) Then
                    Return task
                End If
            Next task

            Return Nothing

        End Function 'FindTask


        ' <summary>
        ' Waits for the download to complete, for the synchronous usage of the downloader.
        ' </summary>
        ' <param name="backgroundCopyJob">The job instance reference.</param>
        ' <param name="maxWaitTime">The maximum wait time.</param>
        ' <param name="task">The updater task instance.</param>
        Private Sub WaitForDownload(ByVal task As UpdaterTask, ByVal backgroundCopyJob As IBackgroundCopyJob, ByVal maxWaitTime As TimeSpan)
            Dim jobID As Guid = Guid.Empty

            Dim isCompleted As Boolean = False
            Dim isSuccessful As Boolean = False
            Dim state As BG_JOB_STATE

            Try

                backgroundCopyJob.GetId(jobID)

                '  set endtime to current tickcount + allowable # milliseconds to wait for job
                Dim endTime As Double = Environment.TickCount + maxWaitTime.TotalMilliseconds

                While Not isCompleted
                    backgroundCopyJob.GetState(state)
                    Select Case state
                        Case BG_JOB_STATE.BG_JOB_STATE_SUSPENDED
                            OnDownloadStarted(New TaskEventArgs(task))
                            backgroundCopyJob.Resume()
                            Exit Select
                        Case BG_JOB_STATE.BG_JOB_STATE_ERROR
                            '  use utility to:
                            '  a)  get error info 
                            '  b)  report it
                            '  c)  cancel and remove copy job
                            OnJobError(task, backgroundCopyJob, Nothing, Nothing)

                            '  complete loop, but DON'T say it's successful
                            isCompleted = True
                            Exit Select
                        Case BG_JOB_STATE.BG_JOB_STATE_TRANSIENT_ERROR
                            '  NOTE:  during debugging + test, transient errors resulted in complete job failure about 90%
                            '  of the time.  Therefore we treat transients just like full errors, and CANCEL the job
                            '  use utility to manage error etc.
                            OnJobError(task, backgroundCopyJob, Nothing, Nothing)

                            '  stop the loop, set completed to true but not successful
                            isCompleted = True
                            Exit Select
                        Case BG_JOB_STATE.BG_JOB_STATE_TRANSFERRING
                            OnJobModification(task, backgroundCopyJob)
                            Exit Select
                        Case BG_JOB_STATE.BG_JOB_STATE_TRANSFERRED
                            OnJobTransferred(task, backgroundCopyJob)

                            isCompleted = True
                            isSuccessful = True
                            Exit Select
                        Case Else
                            Exit Select
                    End Select

                    If isCompleted Then
                        Exit While
                    End If

                    If endTime < Environment.TickCount Then
                        Dim ex As New ApplicationUpdaterException(Resource.ResourceManager(Resource.MessageKey.DownloadTimeoutExceeded))
                        OnJobError(task, backgroundCopyJob, Nothing, ex)
                        Exit While
                    End If

                    '  Avoid 100% CPU utilisation with too tight a loop, let download happen.
                    Thread.Sleep(TimeToWaitDuringSynchronousDownload)
                End While

                If Not isSuccessful Then
                    '  create message + error, package it, publish 
                    Dim ex As New ApplicationUpdaterException(Resource.ResourceManager(Resource.MessageKey.BitsDownloaderFailedDownload, task.Manifest.ManifestId))
                    OnJobError(task, backgroundCopyJob, Nothing, ex)
                End If
            Catch tie As ThreadInterruptedException
                '  if interrupted, clean up job
                OnJobError(task, backgroundCopyJob, Nothing, tie)
            End Try

        End Sub 'WaitForDownload


        ' <summary>
        ' Prepares a BITS job adding the files and creating the required folders.
        ' </summary>
        ' <param name="backgroundCopyJob">The BITS job information.</param>
        ' <param name="task">The UpdaterTask instace.</param>
        Private Sub PrepareJob(ByVal backgroundCopyJob As IBackgroundCopyJob, ByVal task As UpdaterTask)
            Dim jobID As Guid = Guid.Empty

            backgroundCopyJob.GetId(jobID)
            task(TASK_JOBID_KEY) = jobID

            Dim sourceFile As FileManifest
            For Each sourceFile In task.Manifest.Files
                Dim src As String = task.Manifest.Files.Base + sourceFile.Source
                Dim dest As String = Path.Combine(task.DownloadFilesBase, sourceFile.Source)

                '  to defend against config errors, check to see if the path given is UNC;
                '  if so, throw immediately there's a misconfiguration.  Paths to BITS must be HTTP or HTTPS
                If FileUtility.IsUncPath(src) Then
                    Logger.LogAndThrowException(New ApplicationUpdaterException(Resource.ResourceManager(Resource.MessageKey.UncPathUnsupported, src)))
                End If

                If Not Directory.Exists(Path.GetDirectoryName(dest)) Then
                    Directory.CreateDirectory(Path.GetDirectoryName(dest))
                End If

                '  add this file to the job
                backgroundCopyJob.AddFile(src, dest)
            Next sourceFile

        End Sub 'PrepareJob


        ' <summary>
        ' Internal copy-job factory method.  Used to coordinate all aspects of a job set-up, 
        ' which includes creating a copy manager, creating a job within it, setting download
        ' parameters, and adding the job to our tracking collection for cleanup later
        ' </summary>
        ' <param name="copyManager">null reference to copy manager</param>
        ' <param name="copyJob">null reference to copy job</param>
        ' <param name="jobID">null reference to job id guid</param>
        ' <param name="jobNameKey">the key used to look up the job name in the resource file</param>
        ' <param name="jobDescriptionKey">the key used to look up the job description in the resource file</param>
        Private Sub CreateCopyJob(ByVal copyManager As IBackgroundCopyManager, ByRef copyJob As IBackgroundCopyJob, ByRef jobID As Guid, ByVal jobNameKey As Resource.MessageKey, ByVal jobDescriptionKey As Resource.MessageKey)

            Dim jobName As String = Resource.ResourceManager(jobNameKey)
            Dim jobDesc As String = Resource.ResourceManager(jobDescriptionKey)

            ' create the job, set its description, use "out" semantics to get jobid and the actual job object
            copyManager.CreateJob(jobName, BG_JOB_TYPE.BG_JOB_TYPE_DOWNLOAD, jobID, copyJob)

            '  set useful description
            copyJob.SetDescription(jobDesc)

            '  ***
            '      SET UP BITS JOB SETTINGS--TIMEOUTS/RETRY ETC           
            '      SEE THE FOLLOWING REFERENCES:
            '  **  http://msdn.microsoft.com/library/default.asp?url=/library/en-us/bits/bits/IBackgroundCopyJob2_setminimumretrydelay.asp?frame=true
            '  **  http://msdn.microsoft.com/library/default.asp?url=/library/en-us/bits/bits/IBackgroundCopyJob2_setnoprogresstimeout.asp?frame=true
            '  **  http://msdn.microsoft.com/library/default.asp?url=/library/en-us/bits/bits/bg_job_priority.asp
            '  ***
            '  in constant set to 0; this makes BITS retry as soon as possible after an error
            copyJob.SetMinimumRetryDelay(System.Convert.ToUInt32(BitsMinimumRetryDelay))
            '  in constant set to 5 seconds; BITS will set job to Error status if exceeded
            copyJob.SetNoProgressTimeout(System.Convert.ToUInt32(BitsNoProgressTimeout))
            '  make this job the highest (but still background) priority--
            copyJob.SetPriority(BG_JOB_PRIORITY.BG_JOB_PRIORITY_HIGH)
            '  ***
            '----------------------------------------------------------------------
            '-- Data Management and Research, Inc. - Paul Cox - 8/13/2004
            '-- ADDED the following lines to verify credentials of file copy job
            '----------------------------------------------------------------------
            ' Set credentials on the job
            VerifyAndSetBackgroundCopyJobCredentials(copyJob)

            '----------------------------------------------------------------------
            '-- End ADDED
            '----------------------------------------------------------------------
            '  lock our internal collection of jobs, and add this job--we use this collection in Dispose()
            '  to tell BITS to Cancel() jobs--and remove them from the queue
            '  if we did not do this, BITS would continue for (by default) two weeks to download what we asked!
            SyncLock bitsDownloaderJobs.SyncRoot
                bitsDownloaderJobs.Add(jobID, jobName)
            End SyncLock

        End Sub 'CreateCopyJob


        ' <summary>
        ' Method responsible for checking the authentication type and setting the 
        ' appropriate credentials. If the NTLM authentication is used then 
        ' if the username and password are not provided then we use null values. For
        ' all other authentication schemes we need a username and password.
        ' </summary>
        ' <param name="backgroundCopyJob">BackgroundJob on which we need to set the credentials.</param>
        Private Sub VerifyAndSetBackgroundCopyJobCredentials(ByVal backgroundCopyJob As IBackgroundCopyJob)
            Dim userName As String = CType(updaterConfigurationView.GetDownloadProviderData(Me.ConfigurationName), BitsDownloaderProviderData).UserName
            Dim password As String = CType(updaterConfigurationView.GetDownloadProviderData(Me.ConfigurationName), BitsDownloaderProviderData).Password
            If CType(updaterConfigurationView.GetDownloadProviderData(Me.ConfigurationName), BitsDownloaderProviderData).AuthenticationScheme = BG_AUTH_SCHEME.BG_AUTH_SCHEME_NTLM Then
                If Not (userName Is Nothing) AndAlso userName.Length = 0 Then
                    userName = Nothing
                End If
                If Not (password Is Nothing) AndAlso password.Length = 0 Then
                    password = Nothing
                End If

                SetBackgroundJobCredentials(backgroundCopyJob)
            ElseIf Not (userName Is Nothing) AndAlso userName.Length > 0 AndAlso Not (password Is Nothing) Then
                SetBackgroundJobCredentials(backgroundCopyJob)
            End If

        End Sub 'VerifyAndSetBackgroundCopyJobCredentials



        ' <summary>
        ' Method responsible for setting the credentials on the BackgroundJob.
        ' </summary>
        ' <param name="backgroundCopyJob">BackgroundJob to set the credentials on.</param>
        Private Sub SetBackgroundJobCredentials(ByVal backgroundCopyJob As IBackgroundCopyJob)
            Dim copyJob As IBackgroundCopyJob2 = CType(backgroundCopyJob, IBackgroundCopyJob2)
            Dim credentials As New BG_AUTH_CREDENTIALS
            credentials.Credentials.Basic.UserName = CType(updaterConfigurationView.GetDownloadProviderData(Me.ConfigurationName), BitsDownloaderProviderData).UserName
            credentials.Credentials.Basic.Password = CType(updaterConfigurationView.GetDownloadProviderData(Me.ConfigurationName), BitsDownloaderProviderData).Password
            credentials.Scheme = CType(updaterConfigurationView.GetDownloadProviderData(Me.ConfigurationName), BitsDownloaderProviderData).AuthenticationScheme
            credentials.Target = CType(updaterConfigurationView.GetDownloadProviderData(Me.ConfigurationName), BitsDownloaderProviderData).TargetServerType
            copyJob.SetCredentials(credentials)

        End Sub 'SetBackgroundJobCredentials


        ' <summary>
        ' Removes a copy job from the internal lookup collection.
        ' </summary>
        ' <param name="jobID">GUID identifies of a job (job id).</param>
        Private Sub RemoveCopyJobEntry(ByVal jobID As Guid)
            '  lock our collection of running jobs; remove it from the job collection
            SyncLock bitsDownloaderJobs.SyncRoot
                bitsDownloaderJobs.Remove(jobID)
            End SyncLock

        End Sub 'RemoveCopyJobEntry


        ' <summary>
        ' Method called by BITS when the job is modified, this method is used to notify progress.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <param name="pJob">The BITS job reference.</param>
        Private Sub OnJobModification(ByVal task As UpdaterTask, ByVal pJob As IBackgroundCopyJob)
            Dim progress As _BG_JOB_PROGRESS
            pJob.GetProgress(progress)

            Dim args As New DownloadTaskProgressEventArgs(Fix(progress.BytesTotal.ToString()), Fix(progress.BytesTransferred.ToString()), Fix(progress.FilesTotal.ToString()), Fix(progress.FilesTransferred.ToString()), task)

            OnDownloadProgress(args)

        End Sub 'OnJobModification


        ' <summary>
        ' Centralizes all chores related to stopping and cancelling a copy job, and getting back
        ' from BITS the errors incurred during the job.
        ' </summary>
        ' <param name="task">reference to the job associated task</param>
        ' <param name="pJob">reference to the copy job object (not job id)</param>
        ' <param name="pError">reference to the COM error reported by bits (might be null)</param>
        ' <param name="ex">reference to an exception cosidered as an error (might be null)</param>
        Private Sub OnJobError(ByVal task As UpdaterTask, ByVal pJob As IBackgroundCopyJob, ByVal pError As IBackgroundCopyError, ByVal ex As Exception)
            Dim jobDesc As String = ""
            Dim jobName As String = ""
            Dim jobID As Guid = Guid.Empty

            Dim finalException As Exception = ex
            If Not (pJob Is Nothing) Then
                '  get information about this job
                pJob.GetDescription(jobDesc)
                pJob.GetDisplayName(jobName)
                pJob.GetId(jobID)

                Try
                    ' if the error hasn't been reported, try to get it
                    If pError Is Nothing Then
                        pJob.GetError(pError)
                    End If
                Catch e As COMException
                    Logger.LogException(e)
                    If e.ErrorCode <> ExceptionCodeNotAnError Then
                        Throw e
                    End If
                End Try

                ' If we've got the native error, wrap into a nicer exception
                If Not (pError Is Nothing) Then
                    Dim BitsEx As New BitsDownloadErrorException(pError, System.Convert.ToUInt32(CultureIdForGettingComErrorMessages)) 'ToDo: Unsigned Integers not supported
                    cumulativeErrorMessage += BitsEx.Message + Environment.NewLine
                    finalException = BitsEx
                End If


                Dim state As BG_JOB_STATE
                pJob.GetState(state)
                If state <> BG_JOB_STATE.BG_JOB_STATE_ACKNOWLEDGED AndAlso state <> BG_JOB_STATE.BG_JOB_STATE_CANCELLED Then
                    pJob.Cancel()
                End If
                RemoveCopyJobEntry(jobID)
            End If

            OnDownloadError(New DownloadTaskErrorEventArgs(task, finalException))
            Logger.LogAndThrowException(finalException)

        End Sub 'OnJobError


        ' <summary>
        ' Method called by BITS when the job is completed.
        ' </summary>
        ' <param name="task">The Updater task instance.</param>
        ' <param name="pJob">The BITS job reeference.</param>
        Private Sub OnJobTransferred(ByVal task As UpdaterTask, ByVal pJob As IBackgroundCopyJob)
            pJob.Complete()
            OnDownloadCompleted(New TaskEventArgs(task))

        End Sub 'OnJobTransferred

#End Region

#Region "IDisposable Implementation"


        '  take care of IDisposable too   
        ' <summary>
        ' Allows graceful cleanup of hard resources
        ' </summary>
        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)

        End Sub 'Dispose


        ' <summary>
        ' used by externally visible overload.
        ' </summary>
        ' <param name="isDisposing">whether or not to clean up managed + unmanaged/large (true) or just unmanaged(false)</param>
        Private Overloads Sub Dispose(ByVal isDisposing As Boolean)
            Dim BG_JOB_ENUM_ALL_USERS As System.UInt32 = System.Convert.ToUInt32(&H1)
            Dim numJobs As UInt32 'ToDo: Unsigned Integers not supported
            Dim fetched As System.UInt32 'ToDo: Unsigned Integers not supported
            Dim jobID As Guid
            Dim mgr As IBackgroundCopyManager = Nothing
            Dim jobs As IEnumBackgroundCopyJobs = Nothing
            Dim job As IBackgroundCopyJob = Nothing
            If isDisposing Then
                Try
                    mgr = BackgroundCopyManagerFactory.Create()

                    mgr.EnumJobs(BG_JOB_ENUM_ALL_USERS, jobs)

                    jobs.GetCount(numJobs)

                    '  lock the jobs collection for duration of this operation
                    SyncLock bitsDownloaderJobs.SyncRoot
                        Dim i As Integer
                        For i = 0 To System.Convert.ToInt32(numJobs) - 1
                            '  use jobs interface to walk through getting each job
                            jobs.Next(System.Convert.ToUInt32(1), job, fetched) 'ToDo: Unsigned Integers not supported

                            '  get jobid guid
                            job.GetId(jobID)

                            '  check if the job is in OUR collection; if so cancel it.  we obviously don't want to get
                            '  jobs from other Updater threads/processes, or other BITS jobs on the machine!
                            If bitsDownloaderJobs.Contains(jobID) Then
                                '  take ownership just in case, and cancel() it
                                job.TakeOwnership()
                                job.Cancel()
                                ' remove from our collection
                                bitsDownloaderJobs.Remove(jobID)
                            End If
                        Next i
                    End SyncLock
                Finally
                    If Not (mgr Is Nothing) Then
                        Marshal.ReleaseComObject(mgr)
                        mgr = Nothing
                    End If
                    If Not (jobs Is Nothing) Then
                        Marshal.ReleaseComObject(jobs)
                        jobs = Nothing
                    End If
                    If Not (job Is Nothing) Then
                        Marshal.ReleaseComObject(job)
                        job = Nothing
                    End If
                End Try
            End If

        End Sub 'Dispose



        ' <summary>
        ' Destructor/Finalizer
        ' </summary>
        Protected Overloads Overrides Sub Finalize()
            ' Simply call Dispose(false).
            Dispose(False)
        End Sub 'Finalize

#End Region

#Region "IBackgroundCopyCallback Members"


        ' <summary>
        ' BITS notifies about job finished using this method.
        ' </summary>
        ' <param name="pJob">The BITS job reference.</param>
        Sub JobTransferred(ByVal pJob As IBackgroundCopyJob) Implements IBackgroundCopyCallback.JobTransferred
            OnJobTransferred(FindTask(pJob), pJob)

        End Sub 'IBackgroundCopyCallback.JobTransferred


        ' <summary>
        ' BITS notifies about job error using this method.
        ' </summary>
        ' <param name="pJob">The BITS job reference.</param>
        ' <param name="pError">The error information.</param>
        Sub JobError(ByVal pJob As IBackgroundCopyJob, ByVal pError As IBackgroundCopyError) Implements IBackgroundCopyCallback.JobError
            OnJobError(FindTask(pJob), pJob, pError, Nothing)

        End Sub 'IBackgroundCopyCallback.JobError


        ' <summary>
        ' BITS notifies about job finished using this method.
        ' </summary>
        ' <param name="pJob">The BITS job reference.</param>
        ' <param name="dwReserved">Reserved for BITS.</param>
        Sub JobModification(ByVal pJob As IBackgroundCopyJob, ByVal dwReserved As System.UInt32) Implements IBackgroundCopyCallback.JobModification 'ToDo: Unsigned Integers not supported
            OnJobModification(FindTask(pJob), pJob)


        End Sub 'IBackgroundCopyCallback.JobModification

#End Region

#Region "IConfigurationProvider Members"

        ' <summary>
        ' The name of the configuration provider.
        ' </summary>

        Public Property ConfigurationName() As String Implements IConfigurationProvider.ConfigurationName
            Get
                Return downloaderName
            End Get
            Set(ByVal Value As String)
                downloaderName = Value
            End Set
        End Property


        ' <summary>
        ' Method called by Enterprise Library provider model; it is not used because the IDownloader interface also provides this feature. 
        ' </summary>
        ' <param name="configurationView">The configuration view instance.</param>
        Public Sub Initialize(ByVal configurationView As ConfigurationView) Implements IConfigurationProvider.Initialize
            Try
                updaterConfigurationView = CType(configurationView, UpdaterConfigurationView)
            Catch ex As Exception
                Logger.LogAndThrowException(ex)
            Finally
                bitsDownloaderJobs = New HybridDictionary(10)
            End Try

        End Sub 'Initialize

#End Region
    End Class 'BitsDownloader


End Namespace
