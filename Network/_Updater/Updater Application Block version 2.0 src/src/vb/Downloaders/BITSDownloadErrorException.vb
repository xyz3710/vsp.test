 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' BitsDownloaderErrorException.vb
'
' Contains the implementation of the exception thrown by BITS when an error occurs.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Security.Permissions
Imports System.Runtime.Serialization

Namespace Microsoft.ApplicationBlocks.Updater.Downloaders
    ' <summary>
    ' Exception thrown by BITS downloader when an error is found.
    ' </summary>
    <Serializable()> _
    Public Class BitsDownloadErrorException
        Inherits Exception
#Region "Private members"

        ' <summary>
        ' The context for the error.
        ' </summary>
        Private contextForError As BG_ERROR_CONTEXT

        ' <summary>
        ' The error code detected.
        ' </summary>
        Private errorCode As Integer

        ' <summary>
        ' The description of the context.
        ' </summary>
        Private _contextDescription As String

        ' <summary>
        ' The description of the error.
        ' </summary>
        Private errorDescription As String

        ' <summary>
        ' The protocol name.
        ' </summary>
        Private _protocol As String

        ' <summary>
        ' The file name where the file will be copied.
        ' </summary>
        Private _fileLocalName As String

        ' <summary>
        ' The remote file name that was downloaded.
        ' </summary>
        Private _fileRemoteName As String

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New


        ' <summary>
        ' Creates an exception with the BITS error reference and a language id.
        ' </summary>
        ' <param name="error">The BITS error reference.</param>
        ' <param name="langID">The language Id.</param>
        Friend Sub New(ByVal [error] As IBackgroundCopyError, ByVal langID As System.UInt32)  'ToDo: Unsigned Integers not supported
            Dim file As IBackgroundCopyFile

            [error].GetError(contextForError, errorCode)

            [error].GetErrorContextDescription(langID, _contextDescription)
            [error].GetErrorDescription(langID, errorDescription)
            [error].GetFile(file)
            [error].GetProtocol(_protocol)

            file.GetLocalName(_fileLocalName)
            file.GetRemoteName(_fileRemoteName)

        End Sub 'New


        ' <summary>
        ' Creates an exception with the specified message.
        ' </summary>
        ' <param name="message">The message of the exception.</param>
        Public Sub New(ByVal message As String)
            MyBase.New(message)
            errorDescription = message

        End Sub 'New


        ' <summary>
        ' Creates an exception with the specified message and the inner exception detected.
        ' </summary>
        ' <param name="message">The message string.</param>
        ' <param name="innerException">The inner exception reference.</param>
        Public Sub New(ByVal message As String, ByVal innerException As Exception)
            MyBase.New(message, innerException)
            errorDescription = message

        End Sub 'New


        ' <summary>
        ' Constructor used by the serialization infrastructure.
        ' </summary>
        ' <param name="info">The serialization information for the object.</param>
        ' <param name="context">The context for the serialization.</param>
        <SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter:=True)> _
        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)

        End Sub 'New

#End Region

#Region "Public members"

        ' <summary>
        ' The error code.
        ' </summary>

        Public ReadOnly Property Code() As Integer
            Get
                Return errorCode
            End Get
        End Property
        ' <summary>
        ' The error context.
        ' </summary>

        Public ReadOnly Property Context() As Integer
            Get
                Return Fix(contextForError)
            End Get
        End Property
        ' <summary>
        ' The context description.
        ' </summary>

        Public ReadOnly Property ContextDescription() As String
            Get
                Return _contextDescription
            End Get
        End Property
        ' <summary>
        ' The error message.
        ' </summary>

        Public Overrides ReadOnly Property Message() As String
            Get
                Return errorDescription
            End Get
        End Property
        ' <summary>
        ' The protocol used.
        ' </summary>

        Public ReadOnly Property Protocol() As String
            Get
                Return _protocol
            End Get
        End Property
        ' <summary>
        ' The local file name.
        ' </summary>

        Public ReadOnly Property LocalFileName() As String
            Get
                Return _fileLocalName
            End Get
        End Property
        ' <summary>
        ' The remote file name.
        ' </summary>

        Public ReadOnly Property RemoteFileName() As String
            Get
                Return _fileRemoteName
            End Get
        End Property
#End Region

#Region "Public methods"


        ' <summary>
        ' Used by the serialization infrastructure.
        ' </summary>
        ' <param name="info">The serialization information.</param>
        ' <param name="context">The serialization context.</param>
        <SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter:=True)> _
        Public Overrides Sub GetObjectData(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.GetObjectData(info, context)

        End Sub 'GetObjectData

#End Region
    End Class 'BitsDownloadErrorException


End Namespace
