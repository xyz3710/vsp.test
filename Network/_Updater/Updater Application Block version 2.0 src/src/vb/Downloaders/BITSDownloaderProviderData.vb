 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' BitsDownloaderProviderData.vb
'
' Contains the implementation of the configuration for BITS downloader.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Xml.Serialization
Imports Microsoft.ApplicationBlocks.Updater.Configuration
Imports Microsoft.ApplicationBlocks.Updater.Downloaders

Namespace Microsoft.ApplicationBlocks.Updater.Configuration

    ' <summary>
    ' Defines the configuration in the local computer for Background Intelligent Trasnfer Service (BITS).
    ' </summary>
    <XmlRoot("downloader", [Namespace]:=ApplicationUpdaterSettings.ConfigurationNamespace)> _
    Public NotInheritable Class BitsDownloaderProviderData
        Inherits DownloadProviderData
#Region "Private members"

        ' <summary>
        ' The name of the user which will be used by the downloader.
        ' </summary>
        Private _userName As String = Nothing

        ' <summary>
        ' The password for the user used by the downloader.
        ' </summary>
        Private _password As String = Nothing

        ' <summary>
        ' The authentication scheme used by BITS for the download process.
        ' </summary>
        Private _authenticationScheme As BG_AUTH_SCHEME = BG_AUTH_SCHEME.BG_AUTH_SCHEME_NTLM

        ' <summary>
        ' The type of the server for the BITS download.
        ' </summary>
        Private _targetServerType As BG_AUTH_TARGET = BG_AUTH_TARGET.BG_AUTH_TARGET_SERVER

#End Region

#Region "Constructor"


        ' <summary>
        ' Default construcor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "Public properties"

        ' <summary>
        ' The name of the user.
        ' </summary>

        <XmlElement("userName")> _
        Public Property UserName() As String
            Get
                Return _userName
            End Get
            Set(ByVal Value As String)
                _userName = Value
            End Set
        End Property
        ' <summary>
        ' The password for the user.
        ' </summary>

        <XmlElement("password")> _
        Public Property Password() As String
            Get
                Return _password
            End Get
            Set(ByVal Value As String)
                _password = Value
            End Set
        End Property
        ' <summary>
        ' The authentication scheme.
        ' </summary>

        <XmlElement("authenticationScheme")> _
        Public Property AuthenticationScheme() As BG_AUTH_SCHEME
            Get
                Return _authenticationScheme
            End Get
            Set(ByVal Value As BG_AUTH_SCHEME)
                _authenticationScheme = Value
            End Set
        End Property
        ' <summary>
        ' The target server type.
        ' </summary>

        <XmlElement("targetServerType")> _
        Public Property TargetServerType() As BG_AUTH_TARGET
            Get
                Return _targetServerType
            End Get
            Set(ByVal Value As BG_AUTH_TARGET)
                _targetServerType = Value
            End Set
        End Property
        ' <summary>
        ' The name of the type.
        ' </summary>

        <XmlIgnore()> _
        Public Overrides Property TypeName() As String
            Get
                Return GetType(BitsDownloader).AssemblyQualifiedName
            End Get
            Set(ByVal Value As String)
            End Set
        End Property
#End Region
    End Class 'BitsDownloaderProviderData

End Namespace
