 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' GlobalAssemblyInfo.vb
'
' Contains the common information for all the assemblies.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions



<assembly: CLSCompliantAttribute(True)> 

<assembly: AssemblyCompany("Microsoft Corp.")>

<assembly: AssemblyProduct("Updater Application Block")>

<assembly: AssemblyCopyright("2004 Microsoft Corp.")>

<assembly: AssemblyTrademark("")> 

<assembly: AssemblyVersion("2.0.0.0")> 

<assembly: AssemblyDelaySign(False)>

<Assembly: AssemblyKeyFile("")> 

<assembly: AssemblyKeyName("")> 

<assembly: ComVisible(False)> 

<assembly: SecurityPermission(SecurityAction.RequestMinimum, Flags := SecurityPermissionFlag.Assertion Or SecurityPermissionFlag.Execution)>
