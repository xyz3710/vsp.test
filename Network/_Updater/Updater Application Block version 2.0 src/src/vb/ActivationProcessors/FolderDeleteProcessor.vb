'============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' FolderDeleteProcessor.vb
'
' Contains the implementation of the FolderDeleteProcessor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports System.Globalization
Imports System.Xml
Imports System.IO
Imports Microsoft.ApplicationBlocks.Updater.Configuration


Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
    ' <summary>
    ' This processor deletes all the files and subfolders of the specified folder in its configuration.
    ' </summary>
    ' <remarks>
    ' These attributes are defined to configure this processor. They are mandatory except where noted.
    ' <list type="table">
    ' <listheader><term>Attribute</term><description>Description</description></listheader>
    ' <item><term>type</term><description>The type name for this processor</description></item>
    ' <item><term>path</term><description>The path to the folder to delete. Relatives paths are considered to be under the application location folder</description></item>
    ' <item><term>recursive</term><description>Optional. Indicates wheter the subfolders should be recursively deleted. Default is True.</description></item>
    ' </list>
    ' </remarks>
    ' <example>
    ' The following XML excerpt demonstrates how to configure this processor in the manifest file.
    ' <code>
    ' &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
    '		...
    '		&lt;activation&gt;
    '			&lt;tasks&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FolderDeleteProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					path="samples"
    '					recursive="False"
    '				/&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FolderDeleteProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					path="temp"
    '				/&gt;
    '			&lt;/tasks&gt;
    '		&lt;/activation&gt;
    '	&lt;/manifest&gt;
    ' </code>
    ' </example>

    Public Class FolderDeleteProcessor
        Implements IActivationProcessor
#Region "Private members"

        ' <summary>
        ' The UpdaterTask provided in the Init method.
        ' </summary>
        Private task As UpdaterTask

        ' <summary>
        ' The folder specification to delete.
        ' </summary>
        Private folderSpec As String = String.Empty

        ' <summary>
        ' Whether the delete operation is recursive or not.
        ' </summary>
        Private isRecursive As Boolean = True

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "IActivationProcessor Members"


        ' <summary>
        ' Executes the processor.
        ' </summary>
        ' <remarks>Uses the base class library to delete the contents of a folder.</remarks>
        Public Sub Execute() Implements IActivationProcessor.Execute
            If Not Path.IsPathRooted(folderSpec) Then
                folderSpec = Path.Combine(task.Manifest.Application.Location, folderSpec)
            End If

            Directory.Delete(folderSpec, isRecursive)

        End Sub 'Execute


        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <remarks>Validates the correctness of the configuration and aborts the activation if it is not valid.</remarks>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal task As UpdaterTask) Implements IActivationProcessor.Init
            Me.task = task

            If config.AnyAttributes Is Nothing Then
                Throw New ConfigurationException(Resource.ResourceManager(Resource.MessageKey.ProcessorNotConfigured))
            End If

            Dim attr As XmlAttribute
            For Each attr In config.AnyAttributes
                If String.Compare(attr.Name, "path", False, CultureInfo.InvariantCulture) = 0 Then
                    folderSpec = attr.Value
                End If
                If String.Compare(attr.Name, "recursive", False, CultureInfo.InvariantCulture) = 0 Then
                    isRecursive = Boolean.Parse(attr.Value)
                End If
            Next attr

            If Not (folderSpec Is Nothing) AndAlso folderSpec.Length = 0 Then
                Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.PathExpected))
            End If

        End Sub 'Init


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        ' <remarks>No error hadling implemented for this provider.</remarks>
        Public Sub OnError() Implements IActivationProcessor.OnError

        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        ' <remarks>This method is not implemented because there is no validation process required for this processor.</remarks>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution

        End Sub 'PrepareExecution

#End Region
    End Class 'FolderDeleteProcessor
End Namespace