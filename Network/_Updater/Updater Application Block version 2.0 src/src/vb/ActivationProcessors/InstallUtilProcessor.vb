 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' InstallUtilProcessor.vb
'
' Contains the implementation of the InstallUtil processor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports System.Globalization
Imports Microsoft.Win32
Imports System.Xml
Imports System.IO
Imports System.Text
Imports System.Diagnostics
Imports Microsoft.ApplicationBlocks.Updater.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
    '  <summary>
    '  This processor executes the InstallUtil tool included with the .NET Framework to perform the installer actions defined in the specified asemblies.
    '  </summary>
    '  <remarks>
    '  These attributes are defined to configure this processor. They are mandatory except where noted.
    '  <list type="table">
    '  <listheader><term>Attribute</term><description>Description</description></listheader>
    '  <item><term>type</term><description>The type name for this processor</description></item>
    '  <item><term>action</term><description>It can be "Install" or "Uninstall"</description></item>
    '  </list>	
    '  Child elements
    '  <list type="table">
    '  <listheader><term>Element</term><description>Description</description></listheader>
    '  <item><term>assemblies</term><description>The list of assemblies to process. Contains a list of assembly elements.</description></item>
    '  </list>	
    '  assembly element attributes
    '  <list type="table">
    '  <listheader><term>Attribute</term><description>Description</description></listheader>
    '  <item><term>path</term><description>Path to the assembly file to process, relative to the application location</description></item>
    '  <item><term>options</term><description>List of options to be passed to the InstallUtil for processing this assembly</description></item>
    '  </list>	
    '  </remarks>
    '  <example>
    '  The following XML excerpt demonstrates how to configure this processor in the manifest file.
    '  <code>
    '  &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
    ' 		...
    ' 		&lt;files base="http://some-server/manifests/" &gt;
    ' 			&lt;file source="myCustomAction.dll"&gt;
    ' 		&lt;/files&gt;
    ' 		&lt;activation&gt;
    ' 			&lt;tasks&gt;
    ' 				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    ' 					source="myCustomAction.dll"
    ' 					destination="installers\myCustomAction.dll"
    ' 				/&gt;
    ' 				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.InstallUtilProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    ' 					action="Install" &gt;
    ' 					&lt;assemblies&lt;
    ' 						&lt;assembly path="installers\myCustomAction.dll" options="/LogFile=myCustomAction.log" &gt;
    ' 					&lt;\assemblies&gt;
    ' 				&lt;\task&gt;
    ' 			&lt;/tasks&gt;
    ' 		&lt;/activation&gt;
    ' 	&lt;/manifest&gt;
    ' 	</code>
    '  </example>

    Public Class InstallUtilProcessor
        Implements IActivationProcessor
        ' <summary>
        ' The parameter provided to the tool in order to select the InstallUtil mode.
        ' </summary>

        Friend Enum InstallUtilAction
            ' <summary>
            ' Install an assembly.
            ' </summary>
            Install

            ' <summary>
            ' Uninstall an assembly.
            ' </summary>
            Uninstall
        End Enum 'InstallUtilAction

#Region "Private members"

        ' <summary>
        ' The UpdaterTask provided in the Init method.
        ' </summary>
        Private task As UpdaterTask

        ' <summary>
        ' The arguments for the processor in the manifest file.
        ' </summary>
        Private arguments As String = String.Empty

        ' <summary>
        ' The path to the .net tool.
        ' </summary>
        Private utilPath As String = String.Empty

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "IActivationProcessor Members"


        ' <summary>
        ' Executes the processor.
        ' </summary>
        Public Sub Execute() Implements IActivationProcessor.Execute
            Dim installUtilProcess As New Process
            Try
                installUtilProcess.StartInfo.FileName = utilPath
                installUtilProcess.StartInfo.Arguments = arguments
                installUtilProcess.Start()
                installUtilProcess.WaitForExit()

                If installUtilProcess.ExitCode <> 0 Then
                    Throw New Exception(Resource.ResourceManager(Resource.MessageKey.ToolGeneratedError, "installutil.exe", installUtilProcess.ExitCode))
                End If
            Finally
                If Not installUtilProcess Is Nothing Then
                    installUtilProcess.Dispose()
                End If
            End Try

        End Sub 'Execute


        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal task As UpdaterTask) Implements IActivationProcessor.Init
            Me.task = task

            If config.AnyAttributes Is Nothing OrElse config.AnyElement Is Nothing Then
                Throw New ConfigurationException(Resource.ResourceManager(Resource.MessageKey.ProcessorNotConfigured))
            End If

            Dim action As InstallUtilAction = InstallUtilAction.Install

            Dim sb As New StringBuilder

            Dim attr As XmlAttribute
            For Each attr In config.AnyAttributes
                If String.Compare(attr.Name, "action", False, CultureInfo.InvariantCulture) = 0 Then
                    action = CType([Enum].Parse(GetType(InstallUtilAction), attr.Value, False), InstallUtilAction)
                End If
            Next attr

            If action = InstallUtilAction.Uninstall Then
                sb.Append("/uninstall")
            End If
            sb.Append(" ")

            Dim assemblyPath As String = Nothing
            Dim elm As XmlNode
            For Each elm In config.AnyElement.ChildNodes
                If String.Compare(elm.Name, "assembly", False, CultureInfo.InvariantCulture) = 0 Then
                    assemblyPath = elm.Attributes("path").Value

                    If Not Path.IsPathRooted(assemblyPath) Then
                        assemblyPath = Path.Combine(Me.task.Manifest.Application.Location, assemblyPath)
                    End If

                    sb.Append(elm.Attributes("options").Value)
                    sb.AppendFormat(" ""{0}""", assemblyPath)
                    sb.Append(" ")
                End If
            Next elm

            arguments = sb.ToString()

        End Sub 'Init


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        Public Sub OnError() Implements IActivationProcessor.OnError

        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution
            ' Get the .Net Framework folder
            utilPath = Utilities.FileUtility.GetLatestDotNetFrameworkPath()
            If Not utilPath Is Nothing AndAlso utilPath.Length = 0 Then
                Throw New InvalidOperationException(Resource.ResourceManager(Resource.MessageKey.CannotFindDotNetFolder))
            End If

            utilPath = Path.Combine(utilPath, "InstallUtil.exe")
            ' Validate the existence of the file.
            If Not File.Exists(utilPath) Then
                Throw New InvalidOperationException(Resource.ResourceManager(Resource.MessageKey.CannotFindTool, "installutil.exe"))
            End If
        End Sub 'PrepareExecution

#End Region
    End Class 'InstallUtilProcessor
End Namespace