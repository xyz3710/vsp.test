 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' FileCopyProcessor.vb
'
' Contains the FileCopy processor implementation.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports System.Globalization
Imports System.IO
Imports System.Xml
Imports Microsoft.ApplicationBlocks.Updater.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
    ' <summary>
    ' This processor copies one downloaded file to the specified location
    ' </summary>
    ' <remarks>
    ' These attributes are defined to configure this processor. They are mandatory except where noted.
    ' <list type="table">
    ' <listheader><term>Attribute</term><description>Description</description></listheader>
    ' <item><term>type</term><description>The type name for this processor</description></item>
    ' <item><term>source</term><description>A reference to a downloaded file as defined by the source attribute in the files list</description></item>
    ' <item><term>destination</term><description>The directory or filename for the copied file. This path is relative to the application location, if a full path is not specified</description></item>
    ' <item><term>overwrite</term><description>Optional. Indicates whether to overwrite the file if the destination exists. Default is True.</description></item>
    ' </list>
    ' </remarks>
    ' <example>
    ' The following XML excerpt demonstrates how to configure this processor in the manifest file.
    ' <code>
    ' &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
    '		...
    '		&lt;files base="http://some-server/manifests/" &gt;
    '			&lt;file source="file1"&gt;
    '			&lt;file source="file2"&gt;
    '			&lt;file source="temp\file3"&gt;
    '		&lt;/files&gt;
    '		&lt;activation&gt;
    '			&lt;tasks&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					source="file1"
    '					destination="file1"
    '				/&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					source="file2"
    '					destination="samples\file"
    '				/&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					source="temp\file3"
    '					destination="file2"
    '					overwrite="False"
    '				/&gt;
    '			&lt;/tasks&gt;
    '		&lt;/activation&gt;
    '	&lt;/manifest&gt;
    ' </code>
    ' </example>

    Public Class FileCopyProcessor
        Implements IActivationProcessor
#Region "Private members"

        ' <summary>
        ' The UpdaterTask provided in the Init method.
        ' </summary>
        Private taskToProcess As UpdaterTask

        ' <summary>
        ' The source file specification placed in the configuration of the processor in the manifest.
        ' </summary>
        Private sourceFileSpec As String


        ' <summary>
        ' The target file specification placed in the configuration of the processor in the manifest.
        ' </summary>
        Private destFileSpec As String

        ' <summary>
        ' Whether the destination files can be overritten or not.
        ' </summary>
        Private enableOverwrite As Boolean = True

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "IActivationProcessor Members"


        ' <summary>
        ' Executes the processor.
        ' </summary>
        ' <remarks>Copy a file or a list of files from the source to the target.</remarks>
        Public Sub Execute() Implements IActivationProcessor.Execute
            ' Get the destination file full path if needed
            If Not Path.IsPathRooted(destFileSpec) Then
                destFileSpec = Path.Combine(taskToProcess.Manifest.Application.Location, destFileSpec)
            End If

            ' If destination is a folder ( anything ending with a '\' will be considered as a folder )
            ' just copy the file to that folder
            If destFileSpec.EndsWith(Path.DirectorySeparatorChar.ToString(CultureInfo.InvariantCulture)) Then
                ' Verify the existence of the required folder, if not, create it
                If Not Directory.Exists(destFileSpec) Then
                    Directory.CreateDirectory(destFileSpec)
                End If

                destFileSpec = Path.Combine(destFileSpec, Path.GetFileName(sourceFileSpec))
            Else
                ' Verify the existence of the target folder, and create it if not exists
                If Not Directory.Exists(Path.GetDirectoryName(destFileSpec)) Then
                    Directory.CreateDirectory(Path.GetDirectoryName(destFileSpec))
                End If
            End If

            ' Finally, copy the files
            File.Copy(sourceFileSpec, destFileSpec, enableOverwrite)

        End Sub 'Execute


        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <remarks>Validates the configuration and aborts the activation if it is not correct.</remarks>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal task As UpdaterTask) Implements IActivationProcessor.Init
            taskToProcess = task

            If config.AnyAttributes Is Nothing Then
                Throw New ConfigurationException(Resource.ResourceManager(Resource.MessageKey.ProcessorNotConfigured))
            End If

            ' Validate all the copy parameters.
            Dim attr As XmlAttribute
            For Each attr In config.AnyAttributes
                If String.Compare(attr.Name, "source", False, CultureInfo.InvariantCulture) = 0 Then
                    sourceFileSpec = attr.Value
                End If
                If String.Compare(attr.Name, "destination", False, CultureInfo.InvariantCulture) = 0 Then
                    destFileSpec = attr.Value
                End If
                If String.Compare(attr.Name, "overwrite", False, CultureInfo.InvariantCulture) = 0 Then
                    enableOverwrite = Boolean.Parse(attr.Value)
                End If
            Next attr

            If sourceFileSpec Is Nothing OrElse destFileSpec Is Nothing Then
                Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.SourceAndDestinationExpected))
            End If

        End Sub 'Init


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        ' <remarks>If the activation returned an error, this processor will delete the copied files.</remarks>
        Public Sub OnError() Implements IActivationProcessor.OnError
            If File.Exists(destFileSpec) Then
                File.Delete(destFileSpec)
            End If

        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution
            ' Get the source file full path  is needed
            If Not Path.IsPathRooted(sourceFileSpec) Then
                sourceFileSpec = Path.Combine(taskToProcess.DownloadFilesBase, sourceFileSpec)
            End If

            ' This processor only copies files, so fail if no valid source file was specified
            If Not File.Exists(sourceFileSpec) Then
                Dim ex As New InvalidOperationException(Resource.ResourceManager(Resource.MessageKey.FileCopyProcessorSourceFileNotExists, sourceFileSpec))
                Logger.LogException(ex)
                Throw ex
            End If

        End Sub 'PrepareExecution

#End Region
    End Class 'FileCopyProcessor
End Namespace