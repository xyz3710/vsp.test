 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' FlderCopyProcessor.vb
'
' Contains the implementation of the FolderCopy processor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports System.Globalization
Imports System.IO
Imports System.Xml
Imports Microsoft.ApplicationBlocks.Updater.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
    ' <summary>
    ' This processor recursively copies the contents of the specified folder to another folder.
    ' </summary>
    ' <remarks>
    ' These attributes are defined to configure this processor. They are mandatory except where noted.
    ' <list type="table">
    ' <listheader><term>Attribute</term><description>Description</description></listheader>
    ' <item><term>type</term><description>The type name for this processor</description></item>
    ' <item><term>source</term><description>A reference to a source folder as defined by the source attribute in the files list</description></item>
    ' <item><term>destination</term><description>The destination directory. This path is relative to the application location, if a full path is not specified</description></item>
    ' <item><term>overwrite</term><description>Optional. Indicates whether to overwrite existing files under the destination directory. A False value indicates to skip overwriting the existing files. Default is True.</description></item>
    ' </list>
    ' </remarks>
    ' <example>
    ' The following XML excerpt demonstrates how to configure this processor in the manifest file.
    ' <code>
    ' &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
    '		...
    '		&lt;files base="http://some-server/manifests/" &gt;
    '			&lt;file source="file1"&gt;
    '			&lt;file source="file2"&gt;
    '			&lt;file source="temp\file3"&gt;
    '			&lt;file source="doc\help"&gt;
    '		&lt;/files&gt;
    '		&lt;activation&gt;
    '			&lt;tasks&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FolderCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					source="."
    '					destination="."
    '					overwrite="False"
    '				/&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FolderCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					source="doc"
    '					destination="doc"
    '				/&gt;
    '			&lt;/tasks&gt;
    '		&lt;/activation&gt;
    '	&lt;/manifest&gt;
    ' </code>
    ' </example>

    Public Class FolderCopyProcessor
        Implements IActivationProcessor
#Region "Private members"

        ' <summary>
        ' The UpdaterTask provided in the Init method.
        ' </summary>
        Private task As UpdaterTask

        ' <summary>
        ' The source folder specification placed in the manifest file.
        ' </summary>
        Private sourceFolderSpec As String = String.Empty

        ' <summary>
        ' The target folder specification placed in the manifest file.
        ' </summary>
        Private destFolderSpec As String = String.Empty

        ' <summary>
        ' Whether the overwite of the target files is enabled or not.
        ' </summary>
        Private enableOverwrite As Boolean = True

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "IActivationProcessor Members"


        ' <summary>
        ' Executes the processor.
        ' </summary>
        ' <remarks>Uses the helper function to recursively copy the contents of a folder into a new one.</remarks>
        Public Sub Execute() Implements IActivationProcessor.Execute
            If Not Path.IsPathRooted(sourceFolderSpec) Then
                sourceFolderSpec = Path.Combine(task.DownloadFilesBase, sourceFolderSpec)
            End If

            If Not Path.IsPathRooted(destFolderSpec) Then
                destFolderSpec = Path.Combine(task.Manifest.Application.Location, destFolderSpec)
            End If

            Utilities.FileUtility.CopyDirectory(sourceFolderSpec, destFolderSpec, enableOverwrite)

        End Sub 'Execute


        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <remarks>This method is validating the correctness of the validation and will abort the activation if the 
        ' configuration is not correct.</remarks>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal task As UpdaterTask) Implements IActivationProcessor.Init
            Me.task = task

            If config.AnyAttributes Is Nothing Then
                Throw New ConfigurationException(Resource.ResourceManager(Resource.MessageKey.ProcessorNotConfigured))
            End If

            Dim attr As XmlAttribute
            For Each attr In config.AnyAttributes
                If String.Compare(attr.Name, "source", False, CultureInfo.InvariantCulture) = 0 Then
                    sourceFolderSpec = attr.Value
                End If
                If String.Compare(attr.Name, "destination", False, CultureInfo.InvariantCulture) = 0 Then
                    destFolderSpec = attr.Value
                End If
                If String.Compare(attr.Name, "overwrite", False, CultureInfo.InvariantCulture) = 0 Then
                    enableOverwrite = Boolean.Parse(attr.Value)
                End If
            Next attr

            If Not (sourceFolderSpec Is Nothing) AndAlso sourceFolderSpec.Length = 0 OrElse (Not (destFolderSpec Is Nothing) AndAlso destFolderSpec.Length = 0) Then
                Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.SourceAndDestinationExpected))
            End If

        End Sub 'Init


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        ' <remarks>No error handling implemented in this processor.</remarks>
        Public Sub OnError() Implements IActivationProcessor.OnError

        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        ' <remarks>This method is not implemented because there is no validation process required for this processor.</remarks>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution

        End Sub 'PrepareExecution

#End Region
    End Class 'FolderCopyProcessor
End Namespace