 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ValidateHashProcessor.vb
'
' Contains the implementation of the ValidateHash processor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports System.Globalization
Imports System.Xml
Imports System.IO
Imports Microsoft.ApplicationBlocks.Updater.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Security.Cryptography

Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
    ' <summary>
    ' Validate the hash of the file specified in the manifes and the hash of the downloaded file. 
    ' If they do not match a ValidateHashException is thrown.
    ' </summary>
    ' <remarks>
    ' It is important to note that to use this processor, you must configure the hash 
    ' usage in the files element of the manifest. Also, the application configuration 
    ' must have defined the hashing provider specified by the hashProvider attribute of 
    ' the files element. 
    ' 
    ' These attributes are defined to configure this processor and they are mandatory 
    ' except where noted. 
    ' <list type="table">
    ' <listheader><term>Attribute</term><description>Description</description></listheader>
    ' <item><term>type</term><description>The type name for this processor</description></item>
    ' <item><term>source</term><description>A reference to a downloaded file as defined by the source attribute in the files list</description></item>
    ' </list>	
    ' </remarks>
    ' <example>
    ' The following XML excerpt demonstrates how to configure this processor in the manifest file.
    ' <code>
    ' &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
    '		...
    '		&lt;files base="http://some-server/manifests/" hashComparison="True" hashProvider="hmac1" &gt;
    '			&lt;file source="hashedFile.txt" hash="a5aqUnlIyzu670VDJW8lSwXJdJM=" /&gt;
    '		&lt;/files&gt;
    '		&lt;activation&gt;
    '			&lt;tasks&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.ValidateHashProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					source="hashedFile.txt"
    '				/&gt;
    '			&lt;/tasks&gt;
    '		&lt;/activation&gt;
    '	&lt;/manifest&gt;
    '	</code>
    ' </example>	

    Public Class ValidateHashProcessor
        Implements IActivationProcessor
#Region "Private members"

        ' <summary>
        ' The UpdaterTask provided in the Init method.
        ' </summary>
        Private task As UpdaterTask

        ' <summary>
        ' The source file to validate specified in the processor configuration.
        ' </summary>
        Private sourceFile As String = String.Empty

#End Region

#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "IActivationProcessor Members"


        ' <summary>
        ' Executes the processor.
        ' </summary>
        ' <remarks>If the hash does not match, an exception is thrown.</remarks>
        Public Sub Execute() Implements IActivationProcessor.Execute

            Dim targetHash As String = Nothing

            Dim _file As FileManifest
            For Each _file In task.Manifest.Files
                If String.Compare(_file.Source, sourceFile, True, System.Globalization.CultureInfo.CurrentCulture) = 0 Then
                    targetHash = _file.Hash
                    Exit For
                End If
            Next _file

            If targetHash Is Nothing Then
                Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.ArgumentMissing1, sourceFile), "source")
            End If

            sourceFile = Path.Combine(task.DownloadFilesBase, sourceFile)
            If Not File.Exists(sourceFile) Then
                Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.FileNotExists, sourceFile), "source")
            End If

            Dim fs As New FileStream(sourceFile, FileMode.Open, FileAccess.Read, FileShare.Read)
            Try
                Dim fileBytes(fs.Length) As Byte
                fs.Read(fileBytes, 0, fileBytes.Length)
                If Not Cryptographer.CompareHash(task.Manifest.Files.HashProvider, fileBytes, Convert.FromBase64String(targetHash)) Then
                    Throw New ValidateHashException(sourceFile, targetHash)
                End If
            Finally
                fs.Close()
                fs = Nothing
            End Try

        End Sub 'Execute


        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal task As UpdaterTask) Implements IActivationProcessor.Init
            Me.task = task

            If config.AnyAttributes Is Nothing Then
                Throw New ConfigurationException(Resource.ResourceManager(Resource.MessageKey.ProcessorNotConfigured))
            End If

            Dim attr As XmlAttribute
            For Each attr In config.AnyAttributes
                If String.Compare(attr.Name, "source", False, CultureInfo.InvariantCulture) = 0 Then
                    sourceFile = attr.Value
                End If
            Next attr

            If Not (sourceFile Is Nothing) AndAlso sourceFile.Length = 0 Then
                Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.SourceExpected))
            End If

        End Sub 'Init


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        Public Sub OnError() Implements IActivationProcessor.OnError

        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        ' <remarks>
        ' Uses a mutex to validate that no other WaitForApplication method is already 
        ' executing. It also checks whether the application is already executing as a 
        ' process. If the application is running, a new process is started to wait for 
        ' the application and an exception is thrown to abort the execution. If the 
        ' process is not running, the activation continues.
        ' </remarks>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution

        End Sub 'PrepareExecution

#End Region
    End Class 'ValidateHashProcessor
End Namespace