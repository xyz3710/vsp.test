 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' WaitForApplicationExitProcessor.vb
'
' This file contains the WaitForApplicationExit processor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports System.Diagnostics
Imports System.Globalization
Imports System.IO
Imports System.Reflection
Imports System.Threading
Imports System.Xml
Imports Microsoft.ApplicationBlocks.Updater.Configuration

Imports Microsoft.ApplicationBlocks.Updater.Activator

Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
    ' <summary>
    ' Checks whether the application is already executing and postpones the activation until it is closed.
    ' </summary>
    ' <remarks>
    ' As the updating application is running, some of its files may be locked. 
    ' When this processor is called from the updating application, it starts a new process
    ' that waits for the application to be closed to resume the activation process.
    ' 
    ' These attributes are defined to configure this processor. They are mandatory except where noted.
    ' <list type="table">
    ' <listheader><term>Attribute</term><description>Description</description></listheader>
    ' <item><term>type</term><description>The type name for this processor</description></item>
    ' <item><term>processName</term><description>Optional. The name of the process to wait for. When used, the processor will wait for the termination of the specified process instead of the updatiing application</description></item>
    ' </list>	
    ' </remarks>
    ' <example>
    ' The following XML excerpt demonstrates how to configure this processor in the manifest file.
    ' <code>
    ' &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
    '		...
    '		&lt;activation&gt;
    '			&lt;tasks&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.WaitForApplicationExitProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" /&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.ApplicationDeployProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" /&gt;
    '			&lt;/tasks&gt;
    '		&lt;/activation&gt;
    '	&lt;/manifest&gt;
    '	</code>
    '	This example delays the application deploy until the application being updated is closed.
    ' </example>	

    Public Class WaitForApplicationExitProcessor
        Implements IActivationProcessor
#Region "Private members"

        ' <summary>
        ' The UpdaterTask instance provided in the Init method.
        ' </summary>
        Private taskToProcess As UpdaterTask

        ' <summary>
        ' The name of the process that must be waited for.
        ' </summary>
        Private processName As String

        ' <summary>
        ' The name of the process that will wait for the application.
        ' </summary>
        Private Const waiterProcessName As String = "PostApplicationExitActivationProcess"

#End Region

#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "IActivationProcessor Members"


        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal task As UpdaterTask) Implements IActivationProcessor.Init
            taskToProcess = task

            If Not (config.AnyAttributes Is Nothing) Then
                Dim attr As XmlAttribute
                For Each attr In config.AnyAttributes
                    If String.Compare(attr.Name, "processName", False, CultureInfo.InvariantCulture) = 0 Then
                        processName = attr.Value
                    End If
                Next attr
            End If

        End Sub 'Init


        ' <summary>
        ' Executes the processor.
        ' </summary>
        ' <remarks>This processor does not implement this method because all the operations are performed in the 
        ' PrepareExecution method.</remarks>
        Public Sub Execute() Implements IActivationProcessor.Execute

        End Sub 'Execute


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        ' <remarks>Method not implemented because no error handling is required.</remarks>
        Public Sub OnError() Implements IActivationProcessor.OnError

        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        ' <remarks>Uses a Mutex to validate no other WaitForApplication is already executing. And checks whether the
        ' application is already executing as a process. If the application is running a new process is started to 
        ' wait for the application and and exception is throw to abort the execution. If the process is not running, 
        ' the activation continues.</remarks>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution
            ' Use a mutex to avoid running twice or more in a single application
            ' we don't own the mutex here, since ownership is taken by the waiting process
            Dim createdNew As Boolean = False
            Dim mutexName As String = String.Format(CultureInfo.InvariantCulture, "WaitApplication_{0}", taskToProcess.Manifest.Application.ApplicationId)
            Dim waitMutex As New Mutex(False, mutexName, createdNew)
            If Not createdNew Then
                Return
            End If

            ' Look out for the process id to wait for based on the configuration
            Dim processId As Integer = GetProcessIdToWaitFor()

            ' If we can't find the running process, we can continue with the activation.
            If processId < 0 Then
                Return
            End If

            ' Copy the configuration file for the WaitApplication process
            Dim newConfig As String = Path.Combine(Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile), waiterProcessName + ".exe.config")

            File.Copy(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile, newConfig, True)

            ' Extract the embedded executables
            ExtractResourceStream("PostApplicationExitActivationProcess.exe", waiterProcessName + ".exe")
            ExtractResourceStream("wait.bat", "wait.bat")

            ' Spawn the wait process
            Dim psi As New ProcessStartInfo("wait.bat")

            ' The process Id the processor must wait to end
            psi.Arguments = String.Format(CultureInfo.InvariantCulture, "{0} {1}", processId, taskToProcess.Manifest.Application.ApplicationId)
            psi.CreateNoWindow = True
            Process.Start(psi)

            ' Throw this exception to say "no, we won't run now"
            Throw New ActivationPausedException

        End Sub 'PrepareExecution

#End Region

#Region "Private members"


        ' <summary>
        ' Extracts the files needed for waiting for a new process (which are included as embedded resources) in the 
        ' local folder.
        ' </summary>
        ' <param name="resourceName">The name of the resource to extract.</param>
        ' <param name="targetFile">The name of the file where the embedded resource will be saved.</param>
        ' <remarks>This method is a helper used to extract the two files included as embedded resources.</remarks>
        Private Sub ExtractResourceStream(ByVal resourceName As String, ByVal targetFile As String)
            ' Get the assembly resources
            Dim manifestResourceNames As String() = [Assembly].GetExecutingAssembly().GetManifestResourceNames()
            Dim res As Stream = [Assembly].GetExecutingAssembly().GetManifestResourceStream(resourceName)
            Try
                Dim fs As New FileStream(targetFile, FileMode.Create, FileAccess.Write)
                Try
                    Dim bw As New BinaryWriter(fs)
                    Try
                        Dim buffer(1023) As Byte
                        Dim cbRead As Integer = 0
                        Do
                            cbRead = res.Read(buffer, 0, 1024)
                            bw.Write(buffer, 0, cbRead)
                        Loop While cbRead >= 1024
                    Finally
                        bw.Close()
                        bw = Nothing
                    End Try
                Finally
                    fs.Close()
                    fs = Nothing
                End Try
            Finally
                res.Close()
                res = Nothing
            End Try

        End Sub 'ExtractResourceStream


        ' <summary>
        ' Finds the process id using the name of the process.
        ' </summary>
        ' <returns>The id of the process.</returns>
        Private Function GetProcessIdToWaitFor() As Integer
            If processName Is Nothing Then
                ' Assumes the application that must be waited is the current application.
                Return Process.GetCurrentProcess().Id
            Else
                ' Finds the process in the operating system information.
                Dim processes As Process() = Process.GetProcessesByName(processName)
                If processes.Length > 0 Then
                    Return processes(0).Id
                End If
            End If
            Return -1

        End Function 'GetProcessIdToWaitFor

#End Region
    End Class 'WaitForApplicationExitProcessor
End Namespace