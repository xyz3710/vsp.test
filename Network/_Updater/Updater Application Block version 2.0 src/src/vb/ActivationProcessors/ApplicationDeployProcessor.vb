 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ApplicationDeploy.vb
'
' Defines the ApplicationDeploy processor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.IO
Imports Microsoft.ApplicationBlocks.Updater.Configuration


Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
    ' <summary>
    ' This processor copies all the downloaded files from the temporary folder to the application location.
    ' </summary>
    ' <remarks>
    ' All the downloaded files that are not marked as transient are copied from the temporary dowload location to the application location.
    ' To mark a file as transient, you need to add the transient="True" attribute to the file element in the manifest files list.
    ' </remarks>

    Public Class ApplicationDeployProcessor
        Implements IActivationProcessor
#Region "Private members"

        ' <summary>
        ' Keeps the task as a member during the lifetime of this instance.
        ' </summary>
        Private taskToProcess As UpdaterTask

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New 
#End Region

#Region "IActivationProcessor Members"


        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal task As UpdaterTask) Implements IActivationProcessor.Init
            taskToProcess = task

        End Sub 'Init


        ' <summary>
        ' Executes the processor.
        ' </summary>
        ' <remarks>Copies the files to the application folder. If the folder does not exist it is created. This 
        ' method copies all the files marked as non-transient in the manifest.</remarks>
        Public Sub Execute() Implements IActivationProcessor.Execute
            ' Check the existence of the folder.
            If Not Directory.Exists(taskToProcess.Manifest.Application.Location) Then
                Directory.CreateDirectory(taskToProcess.Manifest.Application.Location)
            End If

            ' Copy all the non-transient files to the app folder.
            Dim _file As FileManifest
            For Each _file In taskToProcess.Manifest.Files
                If Not _file.Transient Then
                    ' Prepare the local paths.
                    Dim sourceFile As String = Path.Combine(taskToProcess.DownloadFilesBase, _file.Source)
                    Dim targetFile As String = Path.Combine(taskToProcess.Manifest.Application.Location, _file.Source)
                    Dim targetDir As String = Path.GetDirectoryName(targetFile)

                    ' Since the file may be in a folder, also checks if the folder exists.
                    If Not Directory.Exists(targetDir) Then
                        Directory.CreateDirectory(targetDir)
                    End If

                    ' Copy the file.
                    File.Copy(sourceFile, targetFile, True)
                End If
            Next _file

        End Sub 'Execute


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        ' <remarks>
        ' This method is deleting the files that were already copied. It also checks wether the file exists. If 
        ' there is another processor that has renamed the files this processor will fail.
        ' </remarks>
        Public Sub OnError() Implements IActivationProcessor.OnError
            ' Check the directory exists.
            If Not Directory.Exists(taskToProcess.Manifest.Application.Location) Then
                Return
            End If

            ' Remove any previously copied file
            Dim _file As FileManifest
            For Each _file In taskToProcess.Manifest.Files
                If Not _file.Transient Then
                    Dim targetFile As String = Path.Combine(taskToProcess.Manifest.Application.Location, _file.Source)
                    If File.Exists(targetFile) Then
                        File.Delete(targetFile)
                    End If
                End If
            Next _file

        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        ' <remarks>This method is not implemented because there is no validation process required for this processor.</remarks>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution

        End Sub 'PrepareExecution

#End Region
    End Class 'ApplicationDeployProcessor
End Namespace