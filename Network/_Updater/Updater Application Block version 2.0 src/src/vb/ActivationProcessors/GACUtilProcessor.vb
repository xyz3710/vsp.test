 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' GacUtilProcessor.vb
'
' Contains the implementation of the GacUtil processor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports System.Diagnostics
Imports System.Globalization
Imports System.IO
Imports System.Xml
Imports Microsoft.Win32
Imports Microsoft.ApplicationBlocks.Updater.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
    ' <summary>
    ' This processor executes the GacUtil tool included with the .NET Framework 
    ' to install files in the global assembly cache.
    ' </summary>
    ' <remarks>
    ' These attributes are defined to configure this processor. They are mandatory except where noted.
    ' <list type="table">
    ' <listheader><term>Attribute</term><description>Description</description></listheader>
    ' <item><term>type</term><description>The type name for this processor</description></item>
    ' <item><term>options</term><description>The list of options to be passed to the gacutil utility</description></item>
    ' <item><term>assemblyFile</term><description>Optional. The path (relative to the application location) to an assembly dll to process</description></item>
    ' <item><term>assemblyName</term><description>Optional. The name of an assembly to process</description></item>
    ' </list>	
    ' One of the assemblyFile or assemblyName attributes must be specified.
    ' </remarks>
    ' <example>
    ' The following XML excerpt demonstrates how to configure this processor in the manifest file.
    ' <code>
    ' &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
    '		...
    '		&lt;files base="http://some-server/manifests/" &gt;
    '			&lt;file source="myAssembly.dll"&gt;
    '		&lt;/files&gt;
    '		&lt;activation&gt;
    '			&lt;tasks&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					source="myAssembly.dll"
    '					destination="bin\myAssembly.dll"
    '				/&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.GacUtilProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					options="/i"
    '					assemblyFile="bin\myAssembly.dll"
    '				/&gt;
    '			&lt;/tasks&gt;
    '		&lt;/activation&gt;
    '	&lt;/manifest&gt;
    ' </code>
    ' This manifest indicates the updater to download the file myAssembly.dll, to copy it to the application folder bin subfolder
    ' and to register the assembly in the global assembly cache.
    ' </example>

    Public Class GacUtilProcessor
        Implements IActivationProcessor
#Region "Private members"

        ' <summary>
        ' The UpdaterTask provided in the Init method.
        ' </summary>
        Private task As UpdaterTask

        ' <summary>
        ' Options passed to the GacUtil tool.
        ' </summary>
        Private options As String = String.Empty

        ' <summary>
        ' The assembly specification placed in the manifest file.
        ' </summary>
        Private assemblySpec As String = String.Empty

        ' <summary>
        ' The assembly specification placed in the manifest file.
        ' </summary>
        Private assemblyName As String = String.Empty

        ' <summary>
        ' Stores the path to the .net tool.
        ' </summary>
        Private utilPath As String = String.Empty

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "IActivationProcessor Members"


        ' <summary>
        ' Executes the processor.
        ' </summary>
        Public Sub Execute() Implements IActivationProcessor.Execute
            If Not (assemblySpec Is Nothing) AndAlso assemblySpec.Length <> 0 AndAlso Not Path.IsPathRooted(assemblySpec) Then
                assemblySpec = Path.Combine(task.Manifest.Application.Location, assemblySpec)
            End If

            ' Create the process and run the tool.
            Dim gacProcess As New Process
            Try
                gacProcess.StartInfo.FileName = utilPath
                gacProcess.StartInfo.Arguments = String.Format(CultureInfo.InvariantCulture, "{0} {1}", options, IIf(assemblySpec.Length > 0, assemblySpec, assemblyName)) 'TODO: For performance reasons this should be changed to nested IF statements
                gacProcess.Start()
                gacProcess.WaitForExit()
                If gacProcess.ExitCode <> 0 Then
                    Throw New Exception(Resource.ResourceManager(Resource.MessageKey.ToolGeneratedError, "gacutil.exe", gacProcess.ExitCode))
                End If
            Finally
                If Not gacProcess Is Nothing Then
                    gacProcess.Dispose()
                End If
            End Try

        End Sub 'Execute


        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <remarks>Validate the manifest configuration for correctness.</remarks>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal task As UpdaterTask) Implements IActivationProcessor.Init
            Me.task = task

            If config.AnyAttributes Is Nothing Then
                Throw New ConfigurationException(Resource.ResourceManager(Resource.MessageKey.ProcessorNotConfigured))
            End If

            Dim attr As XmlAttribute
            For Each attr In config.AnyAttributes
                If String.Compare(attr.Name, "options", False, CultureInfo.InvariantCulture) = 0 Then
                    options = attr.Value
                End If
                If String.Compare(attr.Name, "assemblyFile", False, CultureInfo.InvariantCulture) = 0 Then
                    assemblySpec = attr.Value
                End If
                If String.Compare(attr.Name, "assemblyName", False, CultureInfo.InvariantCulture) = 0 Then
                    assemblyName = attr.Value
                End If
            Next attr

            If Not (options Is Nothing) AndAlso options.Length = 0 OrElse (Not (assemblySpec Is Nothing) AndAlso assemblySpec.Length = 0 AndAlso (Not (assemblyName Is Nothing) AndAlso assemblyName.Length = 0)) Then
                Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.AssemblyReferenceExpected))
            End If

        End Sub 'Init


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        Public Sub OnError() Implements IActivationProcessor.OnError

        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution

            ' Get the .Net Framework folder
            utilPath = Utilities.FileUtility.GetLatestDotNetFrameworkPath()
            If Not utilPath Is Nothing AndAlso utilPath.Length = 0 Then
                Throw New InvalidOperationException(Resource.ResourceManager(Resource.MessageKey.CannotFindDotNetFolder))
            End If

            utilPath = Path.Combine(utilPath, "GacUtil.exe")

            ' Validate the existence of the file.
            If Not File.Exists(utilPath) Then
                Throw New InvalidOperationException(Resource.ResourceManager(Resource.MessageKey.CannotFindTool, "gactuil.exe"))
            End If
        End Sub 'PrepareExecution

#End Region
    End Class 'GacUtilProcessor
End Namespace