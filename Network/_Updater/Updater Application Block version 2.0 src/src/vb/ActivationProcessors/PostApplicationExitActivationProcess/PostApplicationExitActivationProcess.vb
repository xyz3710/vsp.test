 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' PostApplicationExitActivationProcess.vb
'
' Contains the implementation of the post application exit update process.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Diagnostics
Imports System.Globalization
Imports System.Threading
Imports Microsoft.ApplicationBlocks.Updater


' <summary>
' This process waits for another process specified as a command line parameter and executes Updater when the 
' process has finished. This is used for the WaitForApplication processor when the application is detected as
' running when the update is perfromed.
' </summary>

Public Class PostApplicationExitActivationProcess

    ' <summary>
    ' The main entry point for the application.
    ' </summary>
    <STAThread()> _
    Shared Sub Main(ByVal args() As String)
        If args.Length <> 2 Then
            Return
        End If

        Dim processToWait As Process = Nothing
        Dim pid As Integer = Integer.Parse(args(0))
        Dim applicationId As String = args(1)

        Dim waitMutex As New Mutex(True, String.Format(CultureInfo.InvariantCulture, "WaitApplication_{0}", applicationId))

        waitMutex.WaitOne()

        Try
            processToWait = Process.GetProcessById(pid)
        Catch
            Return
        End Try

        processToWait.WaitForExit()

        ' This will get the updater and perform all the pending updates automatically
        Dim updater As ApplicationUpdaterManager = ApplicationUpdaterManager.GetUpdater(applicationId)
        updater.ResumePendingUpdates()

        waitMutex.ReleaseMutex()

    End Sub 'Main
End Class 'PostApplicationExitActivationProcess