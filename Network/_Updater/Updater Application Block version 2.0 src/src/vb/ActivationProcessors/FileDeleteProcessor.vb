 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' FileDeleteProcessor.vb
'
' Contains the implementation of the FileDelete processor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports System.IO
Imports System.Globalization
Imports System.Xml
Imports Microsoft.ApplicationBlocks.Updater.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors

    '  <summary>
    '  This processor deletes a file using the its manifest configuration.
    '  </summary>
    '  <remarks>
    '  These attributes are defined to configure this processor. They are mandatory except where noted.
    '  <list type="table">
    '  <listheader><term>Attribute</term><description>Description</description></listheader>
    '  <item><term>type</term><description>The type name for this processor</description></item>
    '  <item><term>path</term><description>The path to the file or pattern to delete under the application location</description></item>
    '  </list>
    '  </remarks>
    '  <example>
    '  The following XML excerpt demonstrates how to configure this processor in the manifest file.
    '  <code>
    '  &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
    ' 		...
    ' 		&lt;activation&gt;
    ' 			&lt;tasks&gt;
    ' 				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileDeleteProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    ' 					path="samples\*.txt"
    ' 					destination="file1"
    ' 				/&gt;
    ' 				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileDeleteProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    ' 					path="file2"
    ' 				/&gt;
    ' 			&lt;/tasks&gt;
    ' 		&lt;/activation&gt;
    ' 	&lt;/manifest&gt;
    '  </code>
    '  </example>    
    Public Class FileDeleteProcessor
        Implements IActivationProcessor
#Region "Private members"

        ' <summary>
        ' The UpdaterTask provided in the Init method.
        ' </summary>
        Private taskToProcess As UpdaterTask

        ' <summary>
        ' The file specification that must be deleted.
        ' </summary>
        Private fileSpec As String

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "IActivationProcessor Members"


        ' <summary>
        ' Executes the processor.
        ' </summary>
        ' <remarks>Uses the configuration to find a folder and deletes all the files within the folder that applies
        ' to the pattern provided.</remarks>
        Public Sub Execute() Implements IActivationProcessor.Execute
            ' Combine the path to make it absolute.
            If Not Path.IsPathRooted(fileSpec) Then
                fileSpec = Path.Combine(taskToProcess.Manifest.Application.Location, fileSpec)
            End If

            ' 
            Dim pattern As String = Path.GetFileName(fileSpec)
            Dim _path As String = Path.GetDirectoryName(fileSpec)
            Dim fileName As String
            For Each fileName In Directory.GetFiles(_path, pattern)
                File.Delete(fileName)
            Next fileName

        End Sub 'Execute


        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="taskToProcess">The UpdaterTask instance.</param>
        ' <remarks>Checks whether the configuration is valid.</remarks>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal taskToProcess As UpdaterTask) Implements IActivationProcessor.Init
            Me.taskToProcess = taskToProcess

            If config.AnyAttributes Is Nothing Then
                Throw New ConfigurationException(Resource.ResourceManager(Resource.MessageKey.ProcessorNotConfigured))
            End If
            Dim attr As XmlAttribute
            For Each attr In config.AnyAttributes
                If String.Compare(attr.Name, "path", False, CultureInfo.InvariantCulture) = 0 Then
                    fileSpec = attr.Value
                End If
            Next attr

            If fileSpec Is Nothing Then
                Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.PathExpected))
            End If

        End Sub 'Init


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        ' <remarks>No implementation for this method because does not make sense undeleting the deleted files.</remarks>
        Public Sub OnError() Implements IActivationProcessor.OnError

        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        ' <remarks>This method is not implemented because there is no validation process required for this processor.</remarks>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution

        End Sub 'PrepareExecution

#End Region
    End Class 'FileDeleteProcessor
End Namespace