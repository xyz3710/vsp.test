 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' MsiProcessor.vb
'
' Contains the implementation of the MSI processor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Xml
Imports Microsoft.ApplicationBlocks.Updater.Configuration
Imports WindowsInstaller

Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
    ' <summary>
    ' This processor uses Windows Installer to execute installation databases in the local system.
    ' </summary>
    ' <remarks>
    ' These attributes are defined to configure this processor. They are mandatory except where noted.
    ' <list type="table">
    ' <listheader><term>Attribute</term><description>Description</description></listheader>
    ' <item><term>type</term><description>The type name for this processor</description></item>
    ' </list>	
    ' The config element is defined to configure this processor, and has these child elements
    ' <list type="table">
    ' <listheader><term>Element</term><description>Description</description></listheader>
    ' <item><term>installType</term><description>The installation action to perform. Can be "Install", "Remove" or "Patch"</description></item>
    ' <item><term>packagePath</term><description>A reference to the msi to process as specified on the files list</description></item>
    ' <item><term>propertyValues</term><description>A list of pairs of property=value terms to initialize the installer</description></item>
    ' <item><term>uiLevel</term><description>Indicates the type of user interface to be used when opening and processing the package</description></item>
    ' <item><term>productCode</term><description>The GUID for the product. Can be used to uninstall the product without specifying the packagePath</description></item>
    ' </list>	
    ' </remarks>
    ' <example>
    ' <code>
    ' &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
    '		...
    '		&lt;files base="http://some-server/manifests/" &gt;
    '			&lt;file source="setup.msi"&gt;
    '		&lt;/files&gt;
    '		&lt;activation&gt;
    '			&lt;tasks&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.MSIProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" &gt;
    '					&lt;config&gt;
    '						&lt;installType&gt;Install&lt;/installType&gt;
    '						&lt;packagePath&gt;setup.msi&lt;/packagePath&gt;
    '						&lt;propertyValues&gt;ACTION=INSTALL&gt;/propertyValues&lt;
    '						&lt;uiLevel&gt;msiUILevelFull&lt;/uiLevel&gt;
    '					&lt;/config&gt;
    '				&lt;/task&gt;
    '			&lt;/tasks&gt;
    '		&lt;/activation&gt;
    '	&lt;/manifest&gt;	
    ' </code>
    '  This code sample tells the updater to download the Setup.msi file and run 
    '  Windows Installer on it to perform the product installation and showing the 
    '  full user interface support.     
    ' </example>

    Public Class MsiProcessor
        Implements IActivationProcessor
        ' <summary>
        ' The type of installation included in the processor configuration in the manifest file.
        ' </summary>
        Private installer As Installer
        Friend Enum InstallType
            ' <summary>
            ' The file specified is an MSI file so the processor will install the database in the local system.
            ' </summary>
            Install

            ' <summary>
            ' The file specified is an MSP file so the processor will apply the patch to an existing database.
            ' </summary>
            Patch

            ' <summary>
            ' Remove a product id from the system.
            ' </summary>
            Remove
        End Enum 'InstallType

#Region "Private fields"

        ' <summary>
        ' The UpdaterTask provided in the Init method.
        ' </summary>
        Private taskToProcess As UpdaterTask

        ' <summary>
        ' The type of the installation specified in the processor configuration.
        ' </summary>
        Private msiPackageInstallType As InstallType

        ' <summary>
        ' The path to the Windows Installer database.
        ' </summary>
        Private msiPackagePath As String = String.Empty

        ' <summary>
        ' The properties specified for the selected operation.
        ' </summary>
        Private msiPropertyValues As String = String.Empty

        ' <summary>
        ' The code of the product that must be uninstalled.
        ' </summary>
        Private msiProductCode As String = String.Empty

        ' <summary>
        ' The level of UI provided to Windows Installer so no UI is displayed.
        ' </summary>
        Private uiLevelForTheMSI As MsiUILevel = MsiUILevel.msiUILevelProgressOnly

#End Region

#Region "IActivationProcessor Members"
        ' <summary>
        ' Executes the processor.
        ' </summary>
        Public Sub Execute() Implements IActivationProcessor.Execute
            CreateInstallerInstance()
            ExecuteInstaller()
        End Sub
        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal task As UpdaterTask) Implements IActivationProcessor.Init
            taskToProcess = task

            Dim node As XmlNode
            For Each node In config.AnyElement.ChildNodes
                Select Case node.LocalName
                    Case "installType"
                        msiPackageInstallType = CType([Enum].Parse(GetType(InstallType), node.InnerText), InstallType)
                    Case "packagePath"
                        msiPackagePath = node.InnerText
                        If Not Path.IsPathRooted(msiPackagePath) Then
                            msiPackagePath = Path.Combine(taskToProcess.DownloadFilesBase, msiPackagePath)
                        End If
                    Case "propertyValues"
                        msiPropertyValues = node.InnerText
                    Case "uiLevel"
                        uiLevelForTheMSI = CType([Enum].Parse(GetType(MsiUILevel), node.InnerText), MsiUILevel)
                    Case "productCode"
                        msiProductCode = node.InnerText
                End Select
            Next node

        End Sub 'Init


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        Public Sub OnError() Implements IActivationProcessor.OnError


        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        ' <remarks>This method is not implemented because there is no validation process required for this processor.</remarks>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution

        End Sub 'PrepareExecution

#End Region

#Region "Private members"


        ' <summary>
        ' Helper method to remove a product from the local system.
        ' </summary>
        ' <param name="installer">The Windows Installer instance.</param>
        ' <param name="productCode">The producr code to uninstall.</param>
        Private Sub RemoveProduct(ByVal installer As Installer, ByVal productCode As String)
            Dim state As MsiInstallState = installer.ProductState(productCode)
            If state = MsiInstallState.msiInstallStateDefault Then
                Dim session As session = Nothing
                Try
                    session = installer.OpenProduct(productCode)
                    session.Property("REMOVE") = "ALL"
                    session.DoAction("INSTALL")
                Finally
                    If Not (session Is Nothing) Then
                        Marshal.ReleaseComObject(session)
                    End If
                End Try
            End If

        End Sub 'RemoveProduct
        ' <summary>
        ' Helper method to create Installer instance.
        ' </summary>
        Public Sub CreateInstallerInstance()
            Dim winInstObj As Object = CreateObject("WindowsInstaller.Installer")
            AssignInstaller(winInstObj)
        End Sub 'CreateInstallerInstance

        ' <summary>
        ' Helper method to Assign Installer instance.
        ' </summary>
        Sub AssignInstaller(ByVal refInstaller As Object)
            installer = refInstaller
        End Sub 'AssignInstaller

        ' <summary>
        ' Helper method to execute install action as per the configuration.
        ' </summary>
        Sub ExecuteInstaller()
            Try
                installer.UILevel = uiLevelForTheMSI
                If msiPackageInstallType = InstallType.Patch Then
                    If msiPackagePath Is Nothing OrElse msiPackagePath.Length = 0 OrElse Not File.Exists(msiPackagePath) Then
                        Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.ArgumentMissing1, "packagePath"))
                    End If

                    installer.ApplyPatch(msiPackagePath, "", MsiInstallType.msiInstallTypeDefault, msiPropertyValues)
                End If
                If msiPackageInstallType = InstallType.Install Then
                    If msiPackagePath Is Nothing OrElse msiPackagePath.Length = 0 OrElse Not File.Exists(msiPackagePath) Then
                        Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.ArgumentMissing1, "packagePath"))
                    End If
                    installer.InstallProduct(msiPackagePath, msiPropertyValues)
                End If
                If msiPackageInstallType = InstallType.Remove Then
                    If msiProductCode Is Nothing OrElse msiProductCode.Length = 0 Then
                        Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.ArgumentMissing), "productCode")
                    End If
                    RemoveProduct(installer, msiProductCode)
                End If
            Finally
                If Not (installer Is Nothing) Then
                    Marshal.ReleaseComObject(installer)
                End If
            End Try

        End Sub 'Execute
#End Region
    End Class 'MsiProcessor 

End Namespace