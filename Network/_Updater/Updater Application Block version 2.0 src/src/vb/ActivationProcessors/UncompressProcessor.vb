 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' UncompressProcessor.vb
'
' Contains the implementation of the Uncompress processor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports System.Globalization
Imports System.IO
Imports System.Xml
Imports System.Diagnostics
Imports Microsoft.ApplicationBlocks.Updater.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
    '  <summary>
    '  Uses the MS-ZIP algorithm to uncompress files
    '  which is available on any Windows installation.
    '  </summary>
    '  <remarks>
    '  You can use the Compress.exe tool (from the platform SDK) to create your compressed files.
    '  These attributes are defined to configure this processor. They are mandatory except where noted.
    ' <list type="table">
    ' <listheader><term>Attribute</term><description>Description</description></listheader>
    ' <item><term>type</term><description>The type name for this processor</description></item>
    ' <item><term>source</term><description>A reference to a downloaded file as defined by the source attribute in the files list</description></item>
    ' <item><term>destination</term><description>Optional. The filename for the uncompressed file. If not specified the file is not renamed. The path to this file is relative to the application location</description></item>
    ' </list>	
    ' </remarks>
    ' <example>
    ' The following XML excerpt demonstrates how to configure this processor in the manifest file.
    ' <code>
    ' &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
    '		...
    '		&lt;files base="http://some-server/manifests/" &gt;
    '			&lt;file source="largeFile.re_"&gt;
    '		&lt;/files&gt;
    '		&lt;activation&gt;
    '			&lt;tasks&gt;
    '				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.UncompressProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
    '					source="largeFile.res_"
    '					destination="resources\largeFile.res"
    '				/&gt;
    '			&lt;/tasks&gt;
    '		&lt;/activation&gt;
    '	&lt;/manifest&gt;
    '	</code>
    ' </example>

    Public Class UncompressProcessor
        Implements IActivationProcessor
#Region "Private members"

        ' <summary>
        ' The UpdaterTask provided in the Init method.
        ' </summary>
        Private task As UpdaterTask

        ' <summary>
        ' The source file that will be uncompressed.
        ' </summary>
        Private [source] As String

        ' <summary>
        ' The target file after uncompression.
        ' </summary>
        Private destination As String

        ' <summary>
        ' The path to the windows tool.
        ' </summary>
        Private utilPath As String = String.Empty

#End Region

#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "IActivationProcessor Members"


        ' <summary>
        ' Executes the processor.
        ' </summary>
        Public Sub Execute() Implements IActivationProcessor.Execute
            If Not (destination Is Nothing) AndAlso destination.Length > 0 AndAlso Not Path.IsPathRooted(destination) Then
                destination = Path.Combine(task.Manifest.Application.Location, destination)
            End If

            If Not Path.IsPathRooted([source]) Then
                [source] = Path.Combine(task.DownloadFilesBase, [source])
            End If

            Dim expandProcess As Process
            Try
                expandProcess = New Process
                expandProcess.StartInfo.FileName = utilPath
                If Not (destination Is Nothing) AndAlso destination.Length > 0 Then
                    expandProcess.StartInfo.Arguments = String.Format(CultureInfo.InvariantCulture, """{0}"" ""{1}""", [source], destination)
                Else
                    expandProcess.StartInfo.Arguments = String.Format(CultureInfo.InvariantCulture, "-r {0} {1}", [source], task.Manifest.Application.Location)
                End If
                expandProcess.Start()
                expandProcess.WaitForExit()

                If expandProcess.ExitCode <> 0 Then
                    Throw New Exception(Resource.ResourceManager(Resource.MessageKey.ToolGeneratedError, "expand.exe", expandProcess.ExitCode))
                End If
            Finally
                If Not expandProcess Is Nothing Then
                    expandProcess.Dispose()
                End If
            End Try

        End Sub 'Execute


        ' <summary>
        ' Initializes the processor using the manifest configuration and the UpdaterTask instance.
        ' </summary>
        ' <param name="config">The configuration for the processor in the manifest file.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub Init(ByVal config As ActivationProcessorProviderData, ByVal task As UpdaterTask) Implements IActivationProcessor.Init
            Me.task = task

            If config.AnyAttributes Is Nothing Then
                Throw New ConfigurationException(Resource.ResourceManager(Resource.MessageKey.ProcessorNotConfigured))
            End If

            Dim attr As XmlAttribute
            For Each attr In config.AnyAttributes
                If String.Compare(attr.Name, "source", False, CultureInfo.InvariantCulture) = 0 Then
                    [source] = attr.Value
                End If
                If String.Compare(attr.Name, "destination", False, CultureInfo.InvariantCulture) = 0 Then
                    destination = attr.Value
                End If
            Next attr

            If Not ([source] Is Nothing) AndAlso [source].Length = 0 Then
                Throw New ArgumentException(Resource.ResourceManager(Resource.MessageKey.SourceAndDestinationExpected))
            End If

        End Sub 'Init


        ' <summary>
        ' If the activation fails this method is called to revert the operations performed by the processor.
        ' </summary>
        Public Sub OnError() Implements IActivationProcessor.OnError

        End Sub 'OnError


        ' <summary>
        ' Prepares the execution and throws an exception if the execution is not possible.
        ' </summary>
        Public Sub PrepareExecution() Implements IActivationProcessor.PrepareExecution
            utilPath = Path.Combine(Environment.SystemDirectory, "Expand.exe")

            If Not File.Exists(utilPath) Then
                Throw New InvalidOperationException(Resource.ResourceManager(Resource.MessageKey.CannotFindTool, "expand.exe"))
            End If

        End Sub 'PrepareExecution 

#End Region
    End Class 'UncompressProcessor
End Namespace
