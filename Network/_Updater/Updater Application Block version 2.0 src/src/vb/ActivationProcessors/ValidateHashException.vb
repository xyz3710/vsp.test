 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ValidateHashException.vb
'
' Contains the implementation of the exception thrown by the validate hash processor.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Runtime.Serialization
Imports System.Security.Permissions

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Exception thrown by the validation hash processor when the hashes do not match.
    ' </summary>
    <Serializable()> _
    Public Class ValidateHashException
        Inherits Exception
#Region "Private members"

        ' <summary>
        ' The source file whose hash was computed.
        ' </summary>
        Private sourceFileFile As String

        ' <summary>
        ' The computed hash for the file.
        ' </summary>
        Private computedHash As String

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New


        ' <summary>
        ' Creates a ValidateHashException with a specified message.
        ' </summary>
        ' <param name="message">The exception message string.</param>
        Public Sub New(ByVal message As String)
            MyBase.New(message)

        End Sub 'New


        ' <summary>
        ' Creates a ValidateHashException with a specified message.
        ' </summary>
        ' <param name="message">The exception message string.</param>
        ' <param name="innerException">The inner exception detected.</param>
        Public Sub New(ByVal message As String, ByVal innerException As Exception)
            MyBase.New(message, innerException)

        End Sub 'New


        ' <summary>
        ' Creates the exception using the name of the file and the computed hash.
        ' </summary>
        ' <param name="sourceFile">The file used to compute the hash.</param>
        ' <param name="computedHash">The computed hash.</param>
        Friend Sub New(ByVal sourceFile As String, ByVal computedHash As String)
            MyBase.New(Resource.ResourceManager(Resource.MessageKey.ValidateHashExceptionMessage))
            sourceFileFile = sourceFile
            Me.computedHash = computedHash

        End Sub 'New


        ' <summary>
        ' Constructor used by the serialization infrastructure.
        ' </summary>
        ' <param name="info">The serialization information for the object.</param>
        ' <param name="context">The context for the serialization.</param>
        <SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter:=True)> _
        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
            sourceFileFile = info.GetString("_sourceFileFile")
            computedHash = info.GetString("_computedHash")

        End Sub 'New

#End Region

#Region "Public properties"

        ' <summary>
        ' The source file the hash was computed for.
        ' </summary>

        Public ReadOnly Property SourceFile() As String
            Get
                Return sourceFileFile
            End Get
        End Property
        ' <summary>
        ' The computed hash for the file.
        ' </summary>

        Public ReadOnly Property TargetHash() As String
            Get
                Return computedHash
            End Get
        End Property
#End Region

#Region "Public methods"


        ' <summary>
        ' Used by the serialization infrastructure.
        ' </summary>
        ' <param name="info">The serialization information.</param>
        ' <param name="context">The serialization context.</param>
        <SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter:=True)> _
        Public Overrides Sub GetObjectData(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            info.AddValue("_sourceFileFile", sourceFileFile)
            info.AddValue("_computedHash", computedHash)
            MyBase.GetObjectData(info, context)

        End Sub 'GetObjectData

#End Region
    End Class 'ValidateHashException
End Namespace