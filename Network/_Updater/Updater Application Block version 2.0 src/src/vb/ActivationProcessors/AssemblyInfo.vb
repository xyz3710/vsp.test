 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' AssemblyInfo.vb
'
' 
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Reflection
Imports System.Security.Permissions



<assembly: AssemblyTitle("")>

<assembly: AssemblyDescription("")>

<assembly: AssemblyConfiguration("")>

<assembly: AssemblyCulture("")> 

<assembly: SecurityPermission(SecurityAction.RequestMinimum, Flags := SecurityPermissionFlag.UnmanagedCode)>

<assembly: FileIOPermission(SecurityAction.RequestMinimum)>

<assembly: RegistryPermission(SecurityAction.RequestMinimum)>
 