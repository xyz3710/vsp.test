 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' FileUtility.vb
'
' Contains the implementation of the FileUtility helper class.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.IO
Imports System.Runtime.InteropServices

Namespace Microsoft.ApplicationBlocks.Updater.Utilities
    ' <summary>
    ' Indicates how to proceed with the move file operation. 
    ' </summary>
    <Flags()> _
    Public Enum MoveFileFlag As Integer
        ' <summary>Perform a default move funtion.</summary>
        None = &H0
        ' <summary>If the target file exists, the move function will replace it.</summary>
        ReplaceExisting = &H1
        ' <summary>If the file is to be moved to a different volume, the function simulates the move by using the CopyFile and DeleteFile functions. </summary>
        CopyAllowed = &H2
        ' <summary>
        ' The system does not move the file until the operating system is restarted. 
        ' The system moves the file immediately after AUTOCHK is executed, but before 
        ' creating any paging files. Consequently, this parameter enables the function 
        ' to delete paging files from previous startups. 
        ' </summary>
        DelayUntilReboot = &H4
        ' <summary>
        ' The function does not return until the file has actually been moved on the disk. 
        ' </summary>
        WriteThrough = &H8
        ' <summary>
        ' Reserved for future use.
        ' </summary>
        CreateHardLink = &H10
        ' <summary>
        ' The function fails if the source file is a link source, but the file cannot be tracked after the move. This situation can occur if the destination is a volume formatted with the FAT file system.
    ' </summary>
        FailIfNotTrackable = &H20
    End Enum



    ' <summary>
    ' Provides certain utilities used by configuration processors, such as correcting file paths.
    ' </summary>

    Public NotInheritable Class FileUtility
#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Private Sub New()

        End Sub 'New

#End Region

#Region "Public members"


        ' <summary>
        ' Returns whether the path is a UNC path.
        ' </summary>
        ' <param name="path">The path string.</param>
        ' <returns><c>true</c> if the path is a UNC path.</returns>
        Public Shared Function IsUncPath(ByVal path As String) As Boolean
            '  FIRST, check if this is a URL or a UNC path; do this by attempting to construct uri object from it
            Dim url As New Uri(path)

            If url.IsUnc Then
                '  it is a unc path, return true
                Return True
            Else
                Return False
            End If

        End Function 'IsUncPath


        ' <summary>
        ' Takes a UNC or URL path, determines which it is (NOT hardened against bad strings, assumes one or the other is present)
        ' and returns the path with correct trailing slash: backslash for UNC or
        ' slash mark for URL.
        ' </summary>
        ' <param name="path">The URL or UNC string.</param>
        ' <returns>Path with correct terminal slash.</returns>
        Public Shared Function AppendSlashUrlOrUnc(ByVal path As String) As String
            If IsUncPath(path) Then
                '  it is a unc path, so decorate the end with a back-slash (to correct misconfigurations, defend against trivial errors)
                Return AppendTerminalBackslash(path)
            Else
                '  assume URL here
                Return AppendTerminalForwardSlash(path)
            End If

        End Function 'AppendSlashUrlOrUnc


        ' <summary>
        ' If not present appends terminal backslash to paths.
        ' </summary>
        ' <param name="path">A path string; for example, "C:\AppUpdaterClient".</param>
        ' <returns>A path string with trailing backslash; for example, "C:\AppUpdaterClient\".</returns>
        Public Shared Function AppendTerminalBackslash(ByVal _path As String) As String
            If _path.IndexOf(Path.DirectorySeparatorChar, _path.Length - 1) = -1 Then
                Return _path + Path.DirectorySeparatorChar
            Else
                Return _path
            End If

        End Function 'AppendTerminalBackslash


        ' <summary>
        ' Appends a terminal slash mark if there is not already one; returns corrected path.
        ' </summary>
        ' <param name="path">The path that may be missing a terminal slash mark.</param>
        ' <returns>The corrected path with terminal slash mark.</returns>
        Public Shared Function AppendTerminalForwardSlash(ByVal _path As String) As String
            If _path.IndexOf(Path.AltDirectorySeparatorChar, _path.Length - 1) = -1 Then
                Return _path + Path.AltDirectorySeparatorChar
            Else
                Return _path
            End If

        End Function 'AppendTerminalForwardSlash


        ' <summary>
        ' Creates a new temporary folder under the system temp folder
        ' and returns its full pathname.
        ' </summary>
        ' <returns>The full temp path string.</returns>
        Public Shared Function CreateTemporaryFolder() As String
            Return Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetTempFileName()))

        End Function 'CreateTemporaryFolder


        ' <summary>
        ' Copies files from the source to destination directories. Directory.Move is not 
        ' suitable here because the downloader may still have the temporary 
        ' directory locked. 
        ' </summary>
        ' <param name="sourcePath">The source path.</param>
        ' <param name="destinationPath">The destination path.</param>
        Public Overloads Shared Sub CopyDirectory(ByVal sourcePath As String, ByVal destinationPath As String)
            CopyDirectory(sourcePath, destinationPath, True)

        End Sub 'CopyDirectory


        ' <summary>
        ' Copies files from the source to destination directories. Directory.Move is not 
        ' suitable here because the downloader may still have the temporary 
        ' directory locked. 
        ' </summary>
        ' <param name="sourcePath">The source path.</param>
        ' <param name="destinationPath">The destination path.</param>
        ' <param name="overwrite">Indicates whether the destination files should be overwritten.</param>
        Public Overloads Shared Sub CopyDirectory(ByVal sourcePath As String, ByVal destinationPath As String, ByVal overwrite As Boolean)
            CopyDirRecurse(sourcePath, destinationPath, destinationPath, overwrite)

        End Sub 'CopyDirectory


        ' <summary>
        ' Move a file from a folder to a new one.
        ' </summary>
        ' <param name="existingFileName">The original file name.</param>
        ' <param name="newFileName">The new file name.</param>
        ' <param name="flags">Flags about how to move the files.</param>
        ' <returns>indicates whether the file was moved.</returns>
        Public Shared Function MoveFile(ByVal existingFileName As String, ByVal newFileName As String, ByVal flags As MoveFileFlag) As Boolean
            Return MoveFileEx(existingFileName, newFileName, Fix(flags))

        End Function 'MoveFile

        ' <summary>
        ' Deletes a folder. If the folder cannot be deleted at the time this method is called,
        ' the deletion operation is delayed until the next system boot.
        ' </summary>
        ' <param name="folderPath">The directory to be removed</param>
        Public Shared Function DestroyFolder(ByVal folderPath As String)
            Try

                If (Directory.Exists(folderPath)) Then

                    Directory.Delete(folderPath, True)
                End If

            Catch

                ' If we couldn't remove the files, postpone it to the next system reboot
                If (Directory.Exists(folderPath)) Then

                    FileUtility.MoveFile(folderPath, Nothing, MoveFileFlag.DelayUntilReboot)
                End If
            End Try
        End Function

        ' <summary>
        ' Deletes a file. If the file cannot be deleted at the time this method is called,
        ' the deletion operation is delayed until the next system boot.
        ' </summary>
        ' <param name="filePath">The file to be removed</param>
        Public Shared Function DestroyFile(ByVal filePath As String)
            Try

                If (File.Exists(filePath)) Then

                    File.Delete(filePath)
                End If
            Catch

                If (File.Exists(filePath)) Then

                    FileUtility.MoveFile(filePath, Nothing, MoveFileFlag.DelayUntilReboot)
                End If
            End Try
        End Function

        ' <summary>
        ' Returns the path to the newer version of the .NET Framework installed on the system.
        ' </summary>
        ' <returns>A string containig the full path to the newer .Net Framework location</returns>
        Public Shared Function GetLatestDotNetFrameworkPath() As String
            Dim latestVersion As Version
            Dim fwkPath As String = Path.GetFullPath(Path.Combine(Environment.SystemDirectory, "..\Microsoft.NET\Framework"))

            Dim _path As String, candidateVersion As String
            For Each _path In Directory.GetDirectories(fwkPath, "v*")
                candidateVersion = Path.GetFileName(_path).TrimStart("v")
                Try
                    Dim curVersion As Version = New Version(candidateVersion)
                    If latestVersion Is Nothing OrElse (Not latestVersion Is Nothing AndAlso latestVersion.CompareTo(curVersion) < 0) Then
                        latestVersion = curVersion
                    End If

                Catch
                End Try
            Next

            Return Path.Combine(fwkPath, "v" + latestVersion.ToString())
        End Function   'GetLatestDotNetFrameworkPath

#End Region

#Region "Private members"


            ' <summary>
            ' API declaration of the Win32 function.
            ' </summary>
            ' <param name="lpExistingFileName">Existing file path.</param>
            ' <param name="lpNewFileName">The file path.</param>
            ' <param name="dwFlags">Move file flags.</param>
            ' <returns>Whether the file was moved or not.</returns>
        <DllImport("KERNEL32.DLL")> _
        Private Shared Function MoveFileEx(ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal dwFlags As Long) As Boolean

        End Function


        ' <summary>
        ' Utility function that recursively copies directories and files.
        ' Again, we could use Directory.Move but we need to preserve the original.
        ' </summary>
        ' <param name="sourcePath">The source path to copy.</param>
        ' <param name="destinationPath">The destination path to copy to.</param>
        ' <param name="originalDestination">The original dstination path.</param>
        ' <param name="overwrite">Whether the folders should be copied recursively.</param>
        Private Shared Sub CopyDirRecurse(ByVal sourcePath As String, ByVal destinationPath As String, ByVal originalDestination As String, ByVal overwrite As Boolean)
            '  ensure terminal backslash
            sourcePath = FileUtility.AppendTerminalBackslash(sourcePath)
            destinationPath = FileUtility.AppendTerminalBackslash(destinationPath)

            If Not Directory.Exists(destinationPath) Then
                Directory.CreateDirectory(destinationPath)
            End If

            '  get dir info which may be file or dir info object
            Dim dirInfo As New DirectoryInfo(sourcePath)

            Dim destFileName As String = Nothing

            Dim fsi As FileSystemInfo
            For Each fsi In dirInfo.GetFileSystemInfos()
                If TypeOf fsi Is FileInfo Then
                    destFileName = Path.Combine(destinationPath, fsi.Name)

                    '  if file object just copy when overwrite is allowed
                    If File.Exists(destFileName) Then
                        If overwrite Then
                            File.Copy(fsi.FullName, destFileName, True)
                        End If
                    Else
                        File.Copy(fsi.FullName, destFileName)
                    End If
                Else
                    ' avoid this recursion path, otherwise copying directories as child directories
                    ' would be an endless recursion (up to an stack-overflow exception).
                    If fsi.FullName <> originalDestination Then
                        '  must be a directory, create destination sub-folder and recurse to copy files
                        'Directory.CreateDirectory( destinationPath + fsi.Name );
                        CopyDirRecurse(fsi.FullName, destinationPath + fsi.Name, originalDestination, overwrite)
                    End If
                End If
            Next fsi

        End Sub 'CopyDirRecurse
#End Region
    End Class 'FileUtility
End Namespace