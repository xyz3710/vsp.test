 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' Logger.vb
'
' Contains the implementation of the logger helper.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Diagnostics
Imports System.Globalization
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common.Instrumentation

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Helper class for logging and exception reporting.
    ' </summary>

    Public NotInheritable Class Logger
#Region "Declarations"

        ' <summary>
        ' The source name for the events.
        ' </summary>
        Private Const eventLoggerSourceName As String = "ApplicationUpdater"

        ' <summary>
        ' The type of the event entry.
        ' </summary>
        Private Const defaultLogEntryType As EventLogEntryType = EventLogEntryType.Information

        ' <summary>
        ' The default entry id.
        ' </summary>
        Private Const defaultLogEntryId As Integer = 1000

        ' <summary>
        ' The singleton instance of the helper class.
        ' </summary>
        Private Shared instance As New logger

        ' <summary>
        ' The EntLib event logger.
        ' </summary>
        Private logger As EventLogger

#End Region

#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Private Sub New()
            logger = New EventLogger(eventLoggerSourceName, defaultLogEntryType, defaultLogEntryId)

        End Sub 'New

#End Region

#Region "Singleton implementation"

        ' <summary>
        ' Returns the singleton instance.
        ' </summary>

        Private Shared ReadOnly Property Current() As logger
            Get
                Return instance
            End Get
        End Property

#End Region

#Region "Public static inteface"


        ' <summary>
        ' Log an error message.
        ' </summary>
        ' <param name="message">The message string.</param>
        Public Shared Sub LogError(ByVal message As String)
            Current.WriteLog(message, EventLogEntryType.Error)

        End Sub 'LogError


        ' <summary>
        ' Log a warning message.
        ' </summary>
        ' <param name="message">The message string.</param>
        Public Shared Sub LogWarning(ByVal message As String)
            Current.WriteLog(message, EventLogEntryType.Warning)

        End Sub 'LogWarning


        ' <summary>
        ' Log an information message.
        ' </summary>
        ' <param name="message">The message string.</param>
        Public Shared Sub LogInformation(ByVal message As String)
            Current.WriteLog(message, EventLogEntryType.Information)

        End Sub 'LogInformation


        ' <summary>
        ' Log an exception.
        ' </summary>
        ' <param name="ex">The exception instance.</param>
        Public Overloads Shared Sub LogException(ByVal ex As Exception)
            LogException(ex, Nothing)

        End Sub 'LogException


        ' <summary>
        ' Log an exception and a message.
        ' </summary>
        ' <param name="ex">The exception instance.</param>
        ' <param name="message">The message string.</param>
        Public Overloads Shared Sub LogException(ByVal ex As Exception, ByVal message As String)
            Dim formatter As New ExceptionFormatter
            If Not (message Is Nothing) AndAlso message.Length > 0 Then
                LogError((message + vbLf + formatter.GetMessage(ex)))
            Else
                LogError(formatter.GetMessage(ex))
            End If

        End Sub 'LogException


        ' <summary>
        ' Log and throw the exception specified.
        ' </summary>
        ' <param name="ex">The exception instance.</param>
        Public Overloads Shared Sub LogAndThrowException(ByVal ex As Exception)
            LogAndThrowException(Nothing, ex)

        End Sub 'LogAndThrowException


        ' <summary>
        ' Log and throw the exception specified with a message string.
        ' </summary>
        ' <param name="ex">The exception instance.</param>
        ' <param name="message">The message string.</param>
        Public Overloads Shared Sub LogAndThrowException(ByVal message As String, ByVal ex As Exception)
            Dim formatter As New ExceptionFormatter
            If Not (message Is Nothing) AndAlso message.Length > 0 Then
                LogError((message + vbLf + formatter.GetMessage(ex)))
            Else
                LogError(formatter.GetMessage(ex))
            End If
            Throw ex

        End Sub 'LogAndThrowException

#End Region

#Region "Instance members"


        ' <summary>
        ' Write the log to EntLib logger.
        ' </summary>
        ' <param name="message">The message string.</param>
        ' <param name="entryType">The event type.</param>
        Private Sub WriteLog(ByVal message As String, ByVal entryType As EventLogEntryType)
            logger.Log(message, entryType)

        End Sub 'WriteLog

#End Region
    End Class 'Logger
End Namespace