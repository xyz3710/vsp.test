 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' SchemaValidator.vb
'
' Contains the implementation of the schema validator.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Collections
Imports System.IO
Imports System.Xml
Imports System.Xml.Schema

Namespace Microsoft.ApplicationBlocks.Updater.Utilities
    ' <summary>
    ' Helper class to perform schema validations.
    ' </summary>

    Public NotInheritable Class SchemaValidator
#Region "Private members"

        ' <summary>
        ' The schemas that will be used for validation.
        ' </summary>
        Private _schemas As New ArrayList(2)

        ' <summary>
        ' The errors detected during the validation.
        ' </summary>
        Private _errors As New ArrayList(5)

        ' <summary>
        ' Whether the document is valid or not.
        ' </summary>
        Private _isValid As Boolean

#End Region

#Region "Constructors"


        ' <summary>
        ' Creates an instance of the SchemaValidator using the document and the schemas.
        ' </summary>
        ' <param name="document">The document to validate.</param>
        ' <param name="schemas">A list of schemas to validate the document against.</param>
        Public Sub New(ByVal document As String, ByVal ParamArray schemas() As Stream)
            Me._schemas.Add(document)
            Dim s As Stream
            For Each s In schemas
                Me._schemas.Add(s)
            Next s
            _isValid = True

        End Sub 'New

#End Region

#Region "Public members"


        ' <summary>
        ' Validates the document and returns the result of the validation.
        ' </summary>
        ' <returns><c>true</c> if the document have succeeded the validation, otherwise <c>false</c>.</returns>
        Public Function Validate() As Boolean
            _errors.Clear()
            Dim vr As XmlValidatingReader = Nothing
            Dim doc As Object = _schemas(0)
            If TypeOf doc Is Stream Then
                vr = New XmlValidatingReader(CType(doc, Stream), XmlNodeType.Element, Nothing)
            ElseIf TypeOf doc Is String Then
                vr = New XmlValidatingReader(CStr(doc), XmlNodeType.Element, Nothing)
            End If

            Try
                Dim i As Integer
                For i = 1 To _schemas.Count
                    If i = _schemas.Count Then
                        Exit For
                    End If
                    vr.Schemas.Add(Nothing, New XmlTextReader(CType(_schemas(i), Stream)))
                Next i

                AddHandler vr.ValidationEventHandler, AddressOf ValidationEventHandler

                While vr.Read()
                End While
                Return _isValid
            Finally
                vr.Close()
            End Try

        End Function 'Validate

        ' <summary>
        ' Contains the validation errors encountered in the last validation operation.
        ' </summary>

        Public ReadOnly Property Errors() As ValidationEventArgs()
            Get
                Return CType(_errors.ToArray(GetType(ValidationEventArgs)), ValidationEventArgs())
            End Get
        End Property


        ' <summary>
        ' Helper method to capture the event handlers.
        ' </summary>
        ' <param name="sender">The sender of the event.</param>
        ' <param name="e">The information about the event.</param>
        Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As System.Xml.Schema.ValidationEventArgs)
            _errors.Add(e)
            If e.Severity = XmlSeverityType.Warning Then
                Return
            End If
            _isValid = False

        End Sub 'ValidationEventHandler

#End Region
    End Class 'SchemaValidator
End Namespace