 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ProcessorFactory.vb
'
' Contains the implementation of the activation processor factory.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports Microsoft.Practices.EnterpriseLibrary.Configuration


Namespace Microsoft.ApplicationBlocks.Updater.Utilities

    ' <summary>
    ' Helper class to create instances of activation processors.
    ' </summary>

    Public NotInheritable Class ProcessorFactory
#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Private Sub New()

        End Sub 'New

#End Region

#Region "Static members"


        ' <summary>
        ' Creates an instance of a processor using the specified processor type.
        ' </summary>
        ' <param name="typeName">The name of the processor type.</param>
        ' <returns>The activation processor.</returns>
        Public Shared Function Create(ByVal typeName As String) As IActivationProcessor
            Dim type As type = type.GetType(typeName, True)
            Return System.Activator.CreateInstance(type)
            '
            'ToDo: Error processing original source shown below
            '   Type type = Type.GetType( typeName, true );
            '   return System.Activator.CreateInstance( type ) as IActivationProcessor;
            '---------------------------------------------------^--- Syntax error: ';' expected
        End Function 'Create

#End Region
    End Class 'ProcessorFactory
End Namespace