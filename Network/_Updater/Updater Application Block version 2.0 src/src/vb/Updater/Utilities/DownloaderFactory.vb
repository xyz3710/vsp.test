 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' DownloaderFactory.vb
'
' Contains the implementation of the downloader factory.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports Microsoft.ApplicationBlocks.Updater.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.Utilities
    ' <summary>
    ' EntLib factory provider implementation.
    ' </summary>

    Public NotInheritable Class DownloaderFactory
        Inherits ProviderFactory
#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()
            MyBase.New("Downloader factory", ConfigurationManager.GetCurrentContext(), GetType(IDownloader))

        End Sub 'New

#End Region

#Region "Public methods"


        ' <summary>
        ' Creates an instance of the downloader using its type.
        ' </summary>
        ' <param name="downloaderProviderName">Name of the downloader to create.</param>
        ' <returns>The instance of the new downloader.</returns>
        Public Function CreateDownloader(ByVal downloaderProviderName As String) As IDownloader
            Return CType(MyBase.CreateInstance(downloaderProviderName), IDownloader)

        End Function 'CreateDownloader

#End Region

#Region "Protected members"


        ' <summary/>
        Protected Overrides Function CreateConfigurationView() As ConfigurationView
            Return New UpdaterConfigurationView(ConfigurationContext)

        End Function 'CreateConfigurationView


        ' <summary/>
        Protected Overrides Function GetConfigurationType(ByVal downloadProviderName As String) As Type
            Dim updaterConfigurationView As updaterConfigurationView = CType(CreateConfigurationView(), updaterConfigurationView)
            Dim downloadProviderData As downloadProviderData = updaterConfigurationView.GetDownloadProviderData(downloadProviderName)
            Return [GetType](downloadProviderData.TypeName)

        End Function 'GetConfigurationType

#End Region
    End Class 'DownloaderFactory
End Namespace

