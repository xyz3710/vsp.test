 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ActivationManger.vb
' 
' Contains the implementatin of the activation manager.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports Microsoft.ApplicationBlocks.Updater.Utilities

Namespace Microsoft.ApplicationBlocks.Updater.Activator

    ' <summary>
    ' The activation manager orchestrates the activation process.
    ' </summary>

    Friend NotInheritable Class ActivationManager
#Region "Constructor"


        ' <summary>
        ' Default constructor
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "Public methods"


        ' <summary>
        ' Start the activation of an UpdaterTask.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub SubmitTask(ByVal task As UpdaterTask)
            If Not (task.Manifest.ActivationProcessors Is Nothing) AndAlso task.Manifest.ActivationProcessors.Count > 0 Then
                AppDomain.CurrentDomain.AppendPrivatePath(task.DownloadFilesBase)
                Activate(task)
            Else
                OnActivationCompleted(task, True)
            End If

        End Sub 'SubmitTask

#End Region

#Region "Private members"


        ' <summary>
        ' Starts the activation of an UpdaterTask.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        Private Sub Activate(ByVal task As UpdaterTask)
            Dim current As Integer = 0

            ' Initialization phase
            OnActivationInitializing(task)
            Dim activationProcessors(task.Manifest.ActivationProcessors.Count - 1) As IActivationProcessor
            For current = 0 To activationProcessors.Length - 1
                activationProcessors(current) = ProcessorFactory.Create(task.Manifest.ActivationProcessors(current).TypeName)
                Try
                    activationProcessors(current).Init(task.Manifest.ActivationProcessors(current), task)
                    activationProcessors(current).PrepareExecution()
                Catch ex As ActivationPausedException
                    Logger.LogException(ex)
                    OnActivationInitializationAborted(task, activationProcessors(current), ex)
                    Return
                Catch ex As Exception
                    Logger.LogException(ex)
                    OnActivationError(task, activationProcessors(current), ex)
                    Return
                End Try
            Next current

            ' Execution phase
            OnActivationStarted(task)
            Dim _error As Boolean = False
            Try
                For current = 0 To activationProcessors.Length - 1
                    activationProcessors(current).Execute()
                Next current
            Catch ex As Exception
                _error = True
                Logger.LogException(ex)
                OnActivationError(task, activationProcessors(current), ex)
            Finally
                ' Error handling phase
                If _error Then
                    Dim i As Integer
                    For i = current - 1 To 0 Step -1
                        Try
                            activationProcessors(i).OnError()
                        Catch ex As Exception
                            ' If an OnError method fails, just log the exception and
                            ' continue with the processing, giving the remaining processors
                            ' an opportunity to handle this situation
                            Logger.LogException(ex, Resource.ResourceManager(Resource.MessageKey.ProcessorOnErrorException, task.Manifest.ActivationProcessors(i).Name))
                        End Try
                    Next i
                End If
                OnActivationCompleted(task, Not _error)
            End Try

        End Sub 'Activate

#End Region

#Region "Events"

        ' <summary>
        ' Notifies about the initialization of the activation process.
        ' </summary>
        Public Event ActivationInitializing As ActivationTaskInitializingEventHandler

        ' <summary>
        ' Notifies about the initialization of the activation process was aborted.
        ' </summary>
        Public Event ActivationInitializationAborted As ActivationTaskInitializationAbortedEventHandler

        ' <summary>
        ' Notifies about the start of the activation process.
        ' </summary>
        Public Event ActivationStarted As ActivationTaskStartedEventHandler

        ' <summary>
        ' Notifies about an error during the activation process.
        ' </summary>
        Public Event ActivationError As ActivationTaskErrorEventHandler

        ' <summary>
        ' Notifies about the completion of the activation process.
        ' </summary>
        Public Event ActivationCompleted As ActivationTaskCompletedEventHandler


        ' <summary>
        ' Method used to fre the event.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        Private Sub OnActivationStarted(ByVal task As UpdaterTask)
            task.State = UpdaterTaskState.Activating
            Dim args As New TaskEventArgs(task)
            RaiseEvent ActivationStarted(Me, args)
            'End If

        End Sub 'OnActivationStarted


        ' <summary>
        ' Method used to fre the event.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        Private Sub OnActivationInitializing(ByVal task As UpdaterTask)
            task.State = UpdaterTaskState.ActivationInitializing
            Dim args As New TaskEventArgs(task)
            RaiseEvent ActivationInitializing(Me, args)

        End Sub 'OnActivationInitializing


        ' <summary>
        ' Method used to fre the event.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <param name="processor">The processor instance.</param>
        ' <param name="ex">The exception instance.</param>
        Private Sub OnActivationInitializationAborted(ByVal task As UpdaterTask, ByVal processor As IActivationProcessor, ByVal ex As Exception)
            task.State = UpdaterTaskState.ActivationInitializationAborted
            Dim args As New ActivationTaskErrorEventArgs(task, processor, ex)
            RaiseEvent ActivationInitializationAborted(Me, args)

        End Sub 'OnActivationInitializationAborted


        ' <summary>
        ' Method used to fre the event.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <param name="processor">The processor instance.</param>
        ' <param name="ex">The exception instance.</param>
        Private Sub OnActivationError(ByVal task As UpdaterTask, ByVal processor As IActivationProcessor, ByVal ex As Exception)
            task.State = UpdaterTaskState.ActivationError
            Dim args As New ActivationTaskErrorEventArgs(task, processor, ex)
            RaiseEvent ActivationError(Me, args)

        End Sub 'OnActivationError


        ' <summary>
        ' Method used to fre the event.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <param name="success">The success status.</param>
        Private Sub OnActivationCompleted(ByVal task As UpdaterTask, ByVal success As Boolean)
            If success Then
                task.State = UpdaterTaskState.Activated
            End If
            Dim args As New ActivationTaskCompleteEventArgs(task, success)
            RaiseEvent ActivationCompleted(Me, args)

        End Sub 'OnActivationCompleted

#End Region
    End Class 'ActivationManager
End Namespace
