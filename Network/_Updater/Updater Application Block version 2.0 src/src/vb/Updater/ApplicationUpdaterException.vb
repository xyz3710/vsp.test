 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ApplicationUpdaterException.vb
'
' Contains the implementation of the generic application updater exception.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Security.Permissions
Imports System.Runtime.Serialization

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' This is a generic exception thrown by the updater.
    ' </summary>
    <Serializable()> _
    Public Class ApplicationUpdaterException
        Inherits Exception
#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New


        ' <summary>
        ' Creates an ApplicationUpdaterException with a specified message.
        ' </summary>
        ' <param name="message">The exception message string.</param>
        Public Sub New(ByVal message As String)
            MyBase.New(message)

        End Sub 'New


        ' <summary>
        ' Creates an ApplicationUpdaterException with a specified message and an inner exception.
        ' </summary>
        ' <param name="message">The exception message string.</param>
        ' <param name="innerException">The inner exception detected.</param>
        Public Sub New(ByVal message As String, ByVal innerException As Exception)
            MyBase.New(message, innerException)

        End Sub 'New


        ' <summary>
        ' Serialization constructor used for the serialization of the exception.
        ' </summary>
        ' <param name="info">The serialization information.</param>
        ' <param name="context">The serialization context.</param>
        <SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter:=True)> _
        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)

        End Sub 'New

#End Region
    End Class 'ApplicationUpdaterException
End Namespace
