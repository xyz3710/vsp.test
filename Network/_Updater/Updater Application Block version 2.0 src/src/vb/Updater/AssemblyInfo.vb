 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' AssemblyInfo.vb
'
' Contains the information for the assembly.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Reflection
Imports System.Security.Permissions
Imports System.Net



<assembly: AssemblyTitle("")>

<assembly: AssemblyDescription("")>

<assembly: AssemblyConfiguration("")>

<assembly: AssemblyCulture("")> 

<assembly: FileIOPermission(SecurityAction.RequestMinimum)>

<assembly: SecurityPermission(SecurityAction.RequestMinimum, Flags := SecurityPermissionFlag.UnmanagedCode)>

<assembly: SecurityPermission(SecurityAction.RequestMinimum, ControlThread := True)>

<assembly: SecurityPermission(SecurityAction.RequestMinimum, Flags := SecurityPermissionFlag.SerializationFormatter)>

<assembly: WebPermission(SecurityAction.RequestMinimum)>
