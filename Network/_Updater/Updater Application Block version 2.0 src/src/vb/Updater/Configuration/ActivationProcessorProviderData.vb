 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ActivationProcessorProviderData.vb
'
' Contains the implementation of the activation processor configuration data.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Configuration


Namespace Microsoft.ApplicationBlocks.Updater.Configuration

    ' <summary>
    ' Defines the configuration for the activation processor within the manifest.
    ' </summary>

    Public Class ActivationProcessorProviderData
        Inherits ProviderData
#Region "Private members"

        ' <summary>
        ' The deserialized activation data.
        ' </summary>
        Private data As Xsd.ActivationProcessorProviderData

#End Region

#Region "Constructors"


        ' <summary>
        ' Creates an instance of a ActivationProcessorProviderData using the deserialized class.
        ' </summary>
        ' <param name="data">The deserialized data.</param>
        Public Sub New(ByVal data As Xsd.ActivationProcessorProviderData)
            Me.data = data

        End Sub 'New


        ' <summary>
        ' Creates an instance of a ActivationProcessorProviderData using the deserialized 
        ' class and the processor name.
        ' </summary>
        ' <param name="data">The deserialized data.</param>
        ' <param name="name">The processor name.</param>
        Public Sub New(ByVal name As String, ByVal data As Xsd.ActivationProcessorProviderData)
            MyBase.New(name)
            Me.data = data

        End Sub 'New

#End Region

#Region "Public Properties"

        ' <summary>
        ' The name of the type.
        ' </summary>

        <XmlAttributeAttribute("type")> _
        Public Overrides Property TypeName() As String
            Get
                Return data.Type
            End Get
            Set(ByVal Value As String)
                data.Type = Value
            End Set
        End Property
        ' <summary>
        ' The elements included in the configuration.
        ' </summary>

        <XmlAnyElement()> _
        Public ReadOnly Property AnyElement() As XmlElement
            Get
                Return data.Any
            End Get
        End Property
        ' <summary>
        ' The attributes included in the configuration.
        ' </summary>

        <XmlAnyAttributeAttribute()> _
        Public ReadOnly Property AnyAttributes() As XmlAttribute()
            Get
                Return data.AnyAttr
            End Get
        End Property
#End Region
    End Class 'ActivationProcessorProviderData

End Namespace