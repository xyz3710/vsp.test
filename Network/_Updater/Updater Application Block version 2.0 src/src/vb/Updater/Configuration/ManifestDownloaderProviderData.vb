 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ManifestDownloaderProviderData.vb
'
' Contains the implementation of the configuration for the downloader within the manifest.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.Configuration
    ' <summary>
    ' Defines the configuration for the downloader in the manifest.
    ' </summary>

    Public NotInheritable Class ManifestDownloaderProviderData

#Region "Private members"

        ' <summary>
        ' The downloader configuration within the manifest.
        ' </summary>
        Private _data As Xsd.ManifestDownloaderProviderData

        Private _name As String

#End Region

#Region "Constructors"

        ' <summary>
        ' Creates an instance of a ManifestDownloaderProviderData using the deserialized configuration 
        ' and the name of the downloader provider.
        ' </summary>
        ' <param name="name">The name of the download provider.</param>
        ' <param name="data">The downloader configuration within the manifest.</param>
        Public Sub New(ByVal name As String, ByVal data As Xsd.ManifestDownloaderProviderData)
            Me._name = name
            Me._data = data

        End Sub 'New

#End Region

#Region "Public properties"
        ' <summary>
        ' Tne downloader name.
        ' </summary>

        Public ReadOnly Property Name() As String
            Get
                Return Me._name
            End Get
        End Property

        ' <summary>
        ' Any element defined in the manifest for the downloader.
        ' </summary>

        <XmlAnyElement()> _
        Public ReadOnly Property AnyElement() As XmlElement
            Get
                Return _data.Any
            End Get
        End Property
        ' <summary>
        ' Any attribute defined in the downloader node.
        ' </summary>

        <XmlAnyAttributeAttribute()> _
        Public ReadOnly Property AnyAttributes() As XmlAttribute()
            Get
                Return _data.AnyAttr
            End Get
        End Property
#End Region
    End Class 'ManifestDownloaderProviderData
End Namespace
