 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' AuthenticationType.vb
'
' Contains the implementation of the authentication types.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================


Namespace Microsoft.ApplicationBlocks.Updater.Configuration
    ' <summary>
    ' Used by the downloader configuration to determine the authentication type.
    ' </summary>

    Public Enum AuthenticationType
        ' <summary>
        ' Using SSPI integrated security.
        ' </summary>
        Integrated = 0

        ' <summary>
        ' Using basic security authentication.
        ' </summary>
        Basic = 1

        ' <summary>
        ' Using anonymous access.
        ' </summary>
        Anonymous = 2
    End Enum 'AuthenticationType
End Namespace
