 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ActivationProcessorProviderDataCollection.vb
'
' Contains the implementation of the processor collection.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports Microsoft.Practices.EnterpriseLibrary.Configuration


Namespace Microsoft.ApplicationBlocks.Updater.Configuration

    ' <summary>
    ' Defines a collection of activation provider data items.
    ' </summary>

    Public Class ActivationProcessorProviderDataCollection
        Inherits ProviderDataCollection
#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "Public properties"

        ' <summary>
        ' Returns the activation processor configuration at the specified index.
        ' </summary>

        Default Public Property Item(ByVal index As Integer) As ActivationProcessorProviderData
            Get
                Return CType(GetProvider(index), ActivationProcessorProviderData)
            End Get
            Set(ByVal Value As ActivationProcessorProviderData)
                SetProvider(index, Value)
            End Set
        End Property
        ' <summary>
        ' Returns the activation processor configuration for the processor that matches the given name.
        ' </summary>

        Default Public Property Item(ByVal name As String) As ActivationProcessorProviderData
            Get
                Return CType(GetProvider(name), ActivationProcessorProviderData)
            End Get
            Set(ByVal Value As ActivationProcessorProviderData)
                SetProvider(name, Value)
            End Set
        End Property
#End Region

#Region "Public methods"


        ' <summary>
        ' Adds a provider to the collection.
        ' </summary>
        ' <param name="providerData">The provider configuration data.</param>
        Public Sub Add(ByVal providerData As ActivationProcessorProviderData)
            AddProvider(providerData)

        End Sub 'Add

#End Region
    End Class 'ActivationProcessorProviderDataCollection

End Namespace