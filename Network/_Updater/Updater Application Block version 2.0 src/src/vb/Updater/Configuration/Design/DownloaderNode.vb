 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' DownloaderNode.cs
'
' Contains the implementation of the downloader node.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.ComponentModel
Imports Microsoft.ApplicationBlocks.Updater
Imports Microsoft.Practices.EnterpriseLibrary.Configuration.Design


'/ <summary>
'/ Defines the configuration node for the downloaders.
'/ </summary>
<Image(GetType(DownloaderNode))>  _
Public Class DownloaderNode
    Inherits ConfigurationNode
    #Region "Private members"
    
    '/ <summary>
    '/ The generic downloader configuration.
    '/ </summary>
    Private data As DownloadProviderData
    
    #End Region
    
    #Region "Constructors"
    
    
    '/ <summary>
    '/ Creates a DownloaderNode with the downloader data.
    '/ </summary>
    '/ <param name="data">The downloader configuration instance.</param>
    Public Sub New(ByVal data As DownloadProviderData) 
        Me.data = data
    
    End Sub 'New
    
    #End Region
    
    #Region "Public properties"
    
    '/ <summary>
    '/ Gets the downloader provider information.
    '/ </summary>
    
    <Browsable(False)>  _
    Public Overridable ReadOnly Property DownloadProviderData() As DownloadProviderData 
        Get
            Return data
        End Get
    End Property 
    #End Region
    
    #Region "Protected members "
    
    
    '/ <summary>
    '/ Set the name of the node.
    '/ </summary>
    Protected Overrides Sub OnSited() 
        MyBase.OnSited()
        Site.Name = Resource.ResourceManager(Resource.MessageKey.DownloaderMenuItem)
    
    End Sub 'OnSited
    
    #End Region
End Class 'DownloaderNode