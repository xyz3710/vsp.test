 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ServiceConfigNode.cs
'
' Contains the implementation of the service configuration node.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.ComponentModel
Imports Microsoft.ApplicationBlocks.Updater
Imports Microsoft.Practices.EnterpriseLibrary.Configuration.Design
Imports Microsoft.Practices.EnterpriseLibrary.Configuration.Design.Validation


'/ <summary>
'/ Defines the configuration for the service.
'/ </summary>
<Image(GetType(ServiceConfigNode))>  _
Public Class ServiceConfigNode
    Inherits ConfigurationNode
    #Region "Private members"
    
    '/ <summary>
    '/ The service configuration instance.
    '/ </summary>
    Private data As ServiceSettings
    
    #End Region
    
    #Region "Constructor"
    
    
    '/ <summary>
    '/ Default constructor.
    '/ </summary>
    Public Sub New() 
        MyClass.New(New ServiceSettings())
    
    End Sub 'New
    
    
    '/ <summary>
    '/ Creates a ServiceConfigNode with the service configuration.
    '/ </summary>
    '/ <param name="data">The service configuration instance.</param>
    Public Sub New(ByVal data As ServiceSettings) 
        Me.data = data
    
    End Sub 'New
    
    #End Region
    
    #Region "Public properties"
    
    '/ <summary>
    '/ The name of the node.
    '/ </summary>
    
    <[ReadOnly](True)>  _
    Public Overrides Property Name() As String 
        Get
            Return MyBase.Name
        End Get
        Set
            MyBase.Name = value
        End Set
    End Property
    
    '/ <summary>
    '/ The updater service interval.
    '/ </summary>
    
    <ResourceDescription(Resource.MessageKey.GetSetUpdaterServiceInterval), Required()>  _
    Public Property UpdateInterval() As String 
        Get
            Return data.Interval
        End Get
        Set
            data.Interval = value
        End Set
    End Property 
    '/ <summary>
    '/ The service settings.
    '/ </summary>
    
    <Browsable(False)>  _
    Public ReadOnly Property ServiceSettings() As ServiceSettings 
        Get
            Return data
        End Get
    End Property
     
    '/ <summary>
    '/ Set the node name.
    '/ </summary>
    Protected Overrides Sub OnSited() 
        MyBase.OnSited()
        Site.Name = Resource.ResourceManager(Resource.MessageKey.ServiceConfigurationNode)
    
    End Sub 'OnSited
    
#End Region

#Region "Protected members"
    Protected Overrides Sub AddDefaultChildNodes()
        MyBase.AddDefaultChildNodes()
        Dim appNode As ApplicationUpdaterNode = CType(Parent, ApplicationUpdaterNode)
        appNode.ServiceNode = Me
    End Sub
#End Region

End Class 'ServiceConfigNode
