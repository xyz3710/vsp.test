 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ManifestManagerNode.cs
'
' Contains the implementation of the manifest manager node.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.ComponentModel
Imports System.Drawing.Design
Imports Microsoft.ApplicationBlocks.Updater
Imports Microsoft.Practices.EnterpriseLibrary.Configuration.Design
Imports Microsoft.Practices.EnterpriseLibrary.Configuration.Design.Validation


'/ <summary>
'/ Defines the confiuration node for the manifest manager.
'/ </summary>
<Image(GetType(ManifestManagerNode))>  _
Public Class ManifestManagerNode
    Inherits ConfigurationNode
    #Region "Private members"
    
    '/ <summary>
    '/ The manifest manager configuration.
    '/ </summary>
    Private managerSettings As ManifestManagerSettings
    
    #End Region
    
    #Region "Constructors"
    
    
    '/ <summary>
    '/ Default constructor.
    '/ </summary>
    Public Sub New() 
        MyClass.New(New ManifestManagerSettings())
    
    End Sub 'New
    
    
    '/ <summary>
    '/ Creates a ManifestManagerNode with the specified manifest manager settings.
    '/ </summary>
    '/ <param name="managerSettings">The manifest manager setting.</param>
    Public Sub New(ByVal managerSettings As ManifestManagerSettings)
        Me.managerSettings = managerSettings

    End Sub 'New

#End Region
    
    #Region "Public properties"
    
    '/ <summary>
    '/ The name of the node.
    '/ </summary>
    
    <[ReadOnly](True)>  _
    Public Overrides Property Name() As String 
        Get
            Return MyBase.Name
        End Get
        Set
            MyBase.Name = value
        End Set
    End Property
    
    '/ <summary>
    '/ The manifest manager settings.
    '/ </summary>
    
    <Browsable(False), ResourceDescription(Resource.MessageKey.GetSetManifestSettings)>  _
    Public ReadOnly Property ManifestManagerSettings() As ManifestManagerSettings 
        Get
            Return managerSettings
        End Get
    End Property 
    '/ <summary>
    '/ The authentication type.
    '/ </summary>
    
    <Required(), ResourceDescription(Resource.MessageKey.GetSetAuthenticationType)>  _
    Public Property AuthenticationType() As AuthenticationType 
        Get
            Return managerSettings.AuthenticationType
        End Get
        Set
            managerSettings.AuthenticationType = Value
        End Set
    End Property 
    '/ <summary>
    '/ The user name credential.
    '/ </summary>
    
    <Required(), ResourceDescription(Resource.MessageKey.GetSetUserName)>  _
    Public Property UserName() As String 
        Get
            Return managerSettings.UserName
        End Get
        Set
            managerSettings.UserName = Value
        End Set
    End Property 
    '/ <summary>
    '/ The password credential.
    '/ </summary>
    
    <Required(), ResourceDescription(Resource.MessageKey.GetSetPassword), _
    Editor(GetType(PasswordEditor), GetType(UITypeEditor))> _
    Public Property Password() As Password
        Get
            Return New Password(managerSettings.Password)
        End Get
        Set(ByVal Value As Password)
            managerSettings.Password = Value.PasswordText
        End Set
    End Property
#End Region
    
    #Region "Public methods"
    
    
    '/ <summary>
    '/ Prepare the context of execution.
    '/ </summary>
    Protected Overrides Sub OnSited() 
        MyBase.OnSited()
        Site.Name = Resource.ResourceManager(Resource.MessageKey.ManifestManagerNode)
    
    End Sub 'OnSited
    
#End Region

#Region "Protected members"
    Protected Overrides Sub AddDefaultChildNodes()
        MyBase.AddDefaultChildNodes()

        Dim appNode As ApplicationUpdaterNode = CType(parent, ApplicationUpdaterNode)
        appNode.ManagerNode = Me
    End Sub
#End Region

End Class 'ManifestManagerNode