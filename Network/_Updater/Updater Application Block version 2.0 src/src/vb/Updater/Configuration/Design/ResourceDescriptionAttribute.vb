 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ResourceDescriptionAttribute.cs
'
' Contains the implementation of the resource description attribute.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.ComponentModel


'/ <devdoc>
'/ Represents a localized version of a DescriptionAttribute. It uses the local strings resouce to get its 
'/ description. This class cannot be inherited.
'/ </devdoc>
<AttributeUsage(AttributeTargets.All)>  _
NotInheritable Friend Class ResourceDescriptionAttribute
    Inherits DescriptionAttribute
    #Region "Private members"
    
    '/ <summary>
    '/ Wether the string has been replaced.
    '/ </summary>
    Private replaced As Boolean
    
    #End Region
    
    #Region "Constructors"
    
    
    '/ <summary>
    '/ Creates a ResourceDescriptionAttribute with the message string.
    '/ </summary>
    '/ <param name="description">The message string.</param>
    Public Sub New(ByVal description As Resource.MessageKey) 
        MyBase.New(description.ToString())
    
    End Sub 'New
    
    #End Region
    
    #Region "Public properties"
    
    '/ <summary>
    '/ The node description.
    '/ </summary>
    
    Public Overrides ReadOnly Property Description() As String 
        Get
            If Not Me.replaced Then
                Me.replaced = True
                MyBase.DescriptionValue = Resource.ResourceManager(MyBase.Description)
            End If
            Return MyBase.Description
        End Get
    End Property
    
    #End Region
End Class 'ResourceDescriptionAttribute