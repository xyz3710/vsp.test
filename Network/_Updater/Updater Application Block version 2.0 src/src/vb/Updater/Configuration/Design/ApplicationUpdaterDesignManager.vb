 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ApplicationUpdaterDesignManager.cs
'
' Contains the implementation of the application updater design manager.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Configuration
Imports System.Windows.Forms
Imports System.Diagnostics
Imports Microsoft.Practices.EnterpriseLibrary.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Configuration.Design


'/ <summary>
'/ Defines the root class for the configuration manager on this assembly.
'/ </summary>

Public Class ApplicationUpdaterDesignManager
    Implements IConfigurationDesignManager
    #Region "Private members"
    
    '/ <summary>
    '/ The name of the section.
    '/ </summary>
    Private Const SectionName As String = "UpdaterConfiguration"
    
    #End Region
    
    #Region "Constructors"
    
    
    '/ <summary>
    '/ Default constructor.
    '/ </summary>
    Public Sub New() 
    
    End Sub 'New
    
    #End Region
    
    #Region "Public methods"
    
    
    '/ <summary>
    '/ Register the nodes in the tool.
    '/ </summary>
    '/ <param name="serviceProvider">The service provider instance.</param>
    Public Sub Register(ByVal serviceProvider As IServiceProvider) Implements IConfigurationDesignManager.Register
        RegisterNodeMaps(serviceProvider)
        CreateCommands(serviceProvider)
        RegisterXmlIncludeTypes(serviceProvider)

    End Sub 'Register


    '/ <summary>
    '/ Open the configuration section.
    '/ </summary>
    '/ <param name="serviceProvider">The service provider instance.</param>
    Public Sub Open(ByVal serviceProvider As IServiceProvider) Implements IConfigurationDesignManager.Open

        Dim configurationContext As configurationContext = ServiceHelper.GetCurrentConfigurationContext(serviceProvider)
        If configurationContext.IsValidSection(SectionName) Then
            Dim applicationUpdaterNode As applicationUpdaterNode = Nothing
            Try
                Dim settings As ApplicationUpdaterSettings = CType(configurationContext.GetConfiguration(SectionName), ApplicationUpdaterSettings)
                If Not (settings Is Nothing) Then
                    applicationUpdaterNode = New applicationUpdaterNode(settings)
                    Dim configurationNode As configurationNode = ServiceHelper.GetCurrentRootNode(serviceProvider)
                    configurationNode.Nodes.Add(applicationUpdaterNode)
                End If
            Catch e As ConfigurationException
                ServiceHelper.LogError(serviceProvider, applicationUpdaterNode, e)
            End Try
        End If

    End Sub 'Open


    '/ <summary>
    '/ Called when the configuration will be saved.
    '/ </summary>
    '/ <param name="serviceProvider">The service provider.</param>
    Public Sub Save(ByVal serviceProvider As IServiceProvider) Implements IConfigurationDesignManager.Save
        Dim configurationContext As configurationContext = ServiceHelper.GetCurrentConfigurationContext(serviceProvider)
        If configurationContext.IsValidSection(SectionName) Then
            Dim applicationUpdaterNode As applicationUpdaterNode = Nothing
            Try
                Dim hierarchy As IUIHierarchy = ServiceHelper.GetCurrentHierarchy(serviceProvider)
                applicationUpdaterNode = CType(hierarchy.FindNodeByType(GetType(applicationUpdaterNode)), applicationUpdaterNode)
                If applicationUpdaterNode Is Nothing Then
                    Return
                End If
                Dim settings As ApplicationUpdaterSettings = applicationUpdaterNode.Settings
                If Not (settings Is Nothing) Then
                    configurationContext.WriteConfiguration(SectionName, settings)
                End If
            Catch e As ConfigurationException
                ServiceHelper.LogError(serviceProvider, applicationUpdaterNode, e)
            Catch e As InvalidOperationException
                ServiceHelper.LogError(serviceProvider, applicationUpdaterNode, e)
            End Try
        End If

    End Sub 'Save


    '/ <summary>
    '/ Called when the configuration tool is preparing the configuration context.
    '/ </summary>
    '/ <param name="serviceProvider">The service provider instance.</param>
    '/ <param name="configurationDictionary">The configuration dictionary.</param>
    Public Sub BuildContext(ByVal serviceProvider As IServiceProvider, ByVal configurationDictionary As ConfigurationDictionary) Implements IConfigurationDesignManager.BuildContext
        Dim updaterNode As ApplicationUpdaterNode = CType(ServiceHelper.GetCurrentHierarchy(serviceProvider).FindNodeByType(GetType(ApplicationUpdaterNode)), ApplicationUpdaterNode)
        If updaterNode Is Nothing Then
            Return
        End If
        configurationDictionary(SectionName) = updaterNode

    End Sub 'BuildContext

#End Region
    
    #Region "Private members"
    
    
    '/ <summary>
    '/ Create the menu commands on the configuratin tool host.
    '/ </summary>
    '/ <param name="provider"></param>
    Private Shared Sub CreateCommands(ByVal provider As IServiceProvider) 
        Dim hierarchyService As IUIHierarchyService = CType(provider.GetService(GetType(IUIHierarchyService)), IUIHierarchyService)
        Debug.Assert( Not (hierarchyService Is Nothing), "Could not get the IUIHierarchyService")
        
        Dim currentHierarchy As IUIHierarchy = hierarchyService.SelectedHierarchy
        Dim containsNode As Boolean = currentHierarchy.ContainsNodeType(GetType(ConfigurationSectionCollectionNode))
        Dim menuService As IMenuContainerService = CType(provider.GetService(GetType(IMenuContainerService)), IMenuContainerService)
        Debug.Assert( Not (menuService Is Nothing), "Could not get the IMenuContainerService")
        
        Dim item As New ConfigurationMenuItem(Resource.ResourceManager(Resource.MessageKey.UpdaterApplicationBlockConfiguration), New AddConfigurationSectionCommand(provider, GetType(ApplicationUpdaterNode), SectionName), ServiceHelper.GetCurrentRootNode(provider), Shortcut.None, Resource.ResourceManager(Resource.MessageKey.UpdaterApplicationBlockConfiguration), InsertionPoint.[New])
        item.Enabled = Not containsNode
        menuService.MenuItems.Add(item)
    
    End Sub 'CreateCommands
    
    
    '/ <summary>
    '/ Register the node mapping on the host tool.
    '/ </summary>
    '/ <param name="serviceProvider">The service provider.</param>
    Private Shared Sub RegisterNodeMaps(ByVal serviceProvider As IServiceProvider) 
        Dim nodeCreationService As INodeCreationService = ServiceHelper.GetNodeCreationService(serviceProvider)
        
        Dim nodeType As Type = GetType(BitsDownloaderNode)
        Dim entry As NodeCreationEntry = NodeCreationEntry.CreateNodeCreationEntryNoMultiples(New AddChildNodeCommand(serviceProvider, nodeType), nodeType, GetType(BitsDownloaderProviderData), "BitsDownloaderNodeMap")
        nodeCreationService.AddNodeCreationEntry(entry)
    
    End Sub 'RegisterNodeMaps
     
    
    '/ <summary>
    '/ Register the types that must be included for correct Xml Serialization.
    '/ </summary>
    '/ <param name="serviceProvider">The service provider instance.</param>
    Private Shared Sub RegisterXmlIncludeTypes(ByVal serviceProvider As IServiceProvider) 
        Dim xmlIncludeTypeService As IXmlIncludeTypeService = CType(serviceProvider.GetService(GetType(IXmlIncludeTypeService)), IXmlIncludeTypeService)
        Debug.Assert( Not (xmlIncludeTypeService Is Nothing), "Could not find the IXmlIncludeTypeService")
        xmlIncludeTypeService.AddXmlIncludeType(ApplicationUpdaterSettings.SectionName, GetType(BitsDownloaderProviderData))
    End Sub 'RegisterXmlIncludeTypes 
    
    
    #End Region
End Class 'ApplicationUpdaterDesignManager