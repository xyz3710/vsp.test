 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' BitsDownloaderNode.cs
'
' Contains the implementation of the Bits downloader configuration node.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.ComponentModel
Imports System.Drawing.Design
Imports Microsoft.ApplicationBlocks.Updater
Imports Microsoft.ApplicationBlocks.Updater.Downloaders
Imports Microsoft.Practices.EnterpriseLibrary.Configuration.Design


'/ <summary>
'/ Defines the configuration options for Bits downloader.
'/ </summary>
<ServiceDependency(GetType(IXmlIncludeTypeService)), ServiceDependency(GetType(INodeCreationService))>  _
Public Class BitsDownloaderNode
    Inherits DownloaderNode
    #Region "Private members"
    
    '/ <summary>
    '/ The BITS downloader data instance.
    '/ </summary>
    Private data As BitsDownloaderProviderData
    
    #End Region
    
    #Region "Constructors"
    
    
    '/ <summary>
    '/ Default constructor.
    '/ </summary>
    Public Sub New() 
        MyClass.New(New BitsDownloaderProviderData())
    
    End Sub 'New
    
    
    '/ <summary>
    '/ Creates a BitsDownloaderNode using the provider data.
    '/ </summary>
    '/ <param name="data">The Bits downloader data in th local configuration file.</param>
    Public Sub New(ByVal data As BitsDownloaderProviderData) 
        MyBase.New(data)
        Me.data = data
    
    End Sub 'New
    
    #End Region
    
    #Region "Public properties"
    
    '/ <summary>
    '/ The provider name.
    '/ </summary>
    
    <[ReadOnly](True)>  _
    Public Overrides Property Name() As String 
        Get
            Return MyBase.Name
        End Get
        Set
            MyBase.Name = value
        End Set
    End Property 
    '/ <summary>
    '/ The provider data.
    '/ </summary>
    
    <Browsable(False)>  _
    Public Overrides ReadOnly Property DownloadProviderData() As DownloadProviderData 
        Get
            data.Name = MyBase.Name
            Return data
        End Get
    End Property
    
    '/ <summary>
    '/ The user name credential.
    '/ </summary>
    
    <ResourceDescription(Resource.MessageKey.GetSetUserName)>  _
    Public Property UserName() As String 
        Get
            Return data.UserName
        End Get
        Set
            data.UserName = value
        End Set
    End Property 
    '/ <summary>
    '/ The password credential.
    '/ </summary>
    
    <ResourceDescription(Resource.MessageKey.GetSetPassword), _
    Editor(GetType(PasswordEditor), GetType(UITypeEditor))> _
    Public Property Password() As Password
        Get
            Return New Password(data.Password)
        End Get
        Set(ByVal Value As Password)
            data.Password = Value.PasswordText
        End Set
    End Property
    '/ <summary>
    '/ The authentication scheme.
    '/ </summary>

    <ResourceDescription(Resource.MessageKey.GetSetAuthenticationScheme)> _
    Public Property AuthenticationScheme() As BG_AUTH_SCHEME
        Get
            Return data.AuthenticationScheme
        End Get
        Set
            data.AuthenticationScheme = value
        End Set
    End Property
    '/ <summary>
    '/ The type of the target server.
    '/ </summary>

    <ResourceDescription(Resource.MessageKey.GetSetServerType)> _
    Public Property TargetServerType() As BG_AUTH_TARGET
        Get
            Return data.TargetServerType
        End Get
        Set
            data.TargetServerType = value
        End Set
    End Property
#End Region
    
    #Region "Public methods"
    
    
    '/ <summary>
    '/ Overrides the base method.
    '/ </summary>
    Protected Overrides Sub OnSited() 
        MyBase.OnSited()
        Site.Name = Resource.ResourceManager(Resource.MessageKey.BitsDownloaderNode)
    
    End Sub 'OnSited
    
    #End Region
End Class 'BitsDownloaderNode