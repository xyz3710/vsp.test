 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ApplicationUpdaterNode.cs
'
' Contains the implementation of the application updater node.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Drawing.Design
Imports System.Windows.Forms
Imports Microsoft.ApplicationBlocks.Updater
Imports Microsoft.Practices.EnterpriseLibrary.Configuration.Design
Imports Microsoft.Practices.EnterpriseLibrary.Configuration.Design.Validation
Imports Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration.Design


'/ <summary>
'/ </summary>
<Image(GetType(ApplicationUpdaterNode))>  _
Public Class ApplicationUpdaterNode
    Inherits ConfigurationNode
    #Region "Private members"
    
    '/ <summary>
    '/ The application updater settings.
    '/ </summary>
    Private _settings As ApplicationUpdaterSettings

    Private _nodeRemoveHandler As ConfigurationNodeChangedEventHandler
    Private _nodeRenameHandler As ConfigurationNodeChangedEventHandler

    Private _managerNode As ManifestManagerNode
    Private _serviceNode As ServiceConfigNode

    #End Region
    
    #Region "Constructors"
    
    
    '/ <summary>
    '/ Default constructor.
    '/ </summary>
    Public Sub New() 
        MyClass.New(New ApplicationUpdaterSettings())
    
    End Sub 'New
    
    
    '/ <summary>
    '/ Creates a ApplicationUpdaterNode with the provided application updater settings.
    '/ </summary>
    '/ <param name="settings">The application updater settings.</param>
    Public Sub New(ByVal settings As ApplicationUpdaterSettings)
        Me._nodeRemoveHandler = New ConfigurationNodeChangedEventHandler(AddressOf OnNodeRemoved)
        Me._nodeRenameHandler = New ConfigurationNodeChangedEventHandler(AddressOf OnNodeRenamed)
        Me._settings = settings
    End Sub 'New

#End Region
    
    #Region "Public properties"
    
    '/ <summary>
    '/ The name of the node.
    '/ </summary>
    
    <[ReadOnly](True)>  _
    Public Overrides Property Name() As String 
        Get
            Return MyBase.Name
        End Get
        Set
            MyBase.Name = value
        End Set
    End Property 
    '/ <summary>
    '/ The base path for the application.
    '/ </summary>
    
    <ResourceDescription(Resource.MessageKey.GetSetApplicationBasePath)>  _
    Public Property BasePath() As String 
        Get
            Return _settings.BasePath
        End Get
        Set
            _settings.BasePath = Value
        End Set
    End Property 
    '/ <summary>
    '/ The application Id.
    '/ </summary>
    
    <ResourceDescription(Resource.MessageKey.GetSetApplicationIdentifier), Required()>  _
    Public Property ApplicationId() As String 
        Get
            Return _settings.ApplicationId
        End Get
        Set
            _settings.ApplicationId = Value
        End Set
    End Property 
    '/ <summary>
    '/ The manifest Url.
    '/ </summary>
    
    <ResourceDescription(Resource.MessageKey.GetSetManifestLocation), Required()>  _
    Public Property ManifestUri() As String 
        Get
            Return _settings.ManifestUri
        End Get
        Set
            _settings.ManifestUri = Value
        End Set
    End Property 
    '/ <summary>
    '/ The applicatoin updater settings.
    '/ </summary>
    
    <Browsable(False)>  _
    Public ReadOnly Property Settings() As ApplicationUpdaterSettings 
        Get
            GetManifestManager()
            GetDownloaders()
            GetServiceConfiguration()
            
            Return _settings
        End Get
    End Property



    '/ <summary>
    '/ The manifest manager node
    '/ </summary>
    <Browsable(False)> _
    Public Property ManagerNode() As ManifestManagerNode
        Get
            Return _managerNode
        End Get
        Set(ByVal Value As ManifestManagerNode)
            Dim svc As ILinkNodeService = CType(GetService(GetType(ILinkNodeService)), ILinkNodeService)
            Debug.Assert(Not svc Is Nothing, "Could not get the ILinkNodeService")
            _managerNode = CType(svc.CreateReference(ManagerNode, Value, _nodeRemoveHandler, _nodeRemoveHandler), ManifestManagerNode)
        End Set
    End Property

    '/ <summary>
    '/ The service node
    '/ </summary>
    <Browsable(False)> _
    Public Property ServiceNode() As ServiceConfigNode
        Get
            Return _serviceNode
        End Get
        Set(ByVal Value As ServiceConfigNode)

            Dim svc As ILinkNodeService = CType(GetService(GetType(ILinkNodeService)), ILinkNodeService)

            Debug.Assert(Not svc Is Nothing, "Could not get the ILinkNodeService")
            _serviceNode = CType(svc.CreateReference(ServiceNode, Value, _nodeRemoveHandler, _nodeRenameHandler), ServiceConfigNode)
        End Set
    End Property

#End Region

    #Region "Private methods"


            '/ <summary>
            '/ Get the manifest manager configuration.
            '/ </summary>
    Private Sub GetManifestManager() 
        Dim node As ConfigurationNode = Hierarchy.FindNodeByType(Me, GetType(ManifestManagerNode))
        If Not node Is Nothing Then
            _settings.ManifestManager = CType(node, ManifestManagerNode).ManifestManagerSettings
        End If
    End Sub 'GetManifestManager


    '/ <summary>
    '/ Get the configuration for the service.
    '/ </summary>
    Private Sub GetServiceConfiguration()
        Dim node As ConfigurationNode = Hierarchy.FindNodeByType(Me, GetType(ServiceConfigNode))
        If Not node Is Nothing Then
            _settings.Service = CType(node, ServiceConfigNode).ServiceSettings
        End If
    End Sub 'GetServiceConfiguration


    '/ <summary>
    '/ Get the downloaders configuration.
    '/ </summary>
    Private Sub GetDownloaders()
        Dim node As ConfigurationNode = Hierarchy.FindNodeByType(Me, GetType(DownloaderNodeCollection))
        If Not node Is Nothing Then
            _settings.Downloaders = CType(node, DownloaderNodeCollection).DownloaderDataCollection
        End If
    End Sub 'GetDownloaders


    '/ <summary>
    '/ When the user right clicks the node 3 nodes will be added.
    '/ </summary>
    Protected Overrides Sub OnAddMenuItems()
        MyBase.OnAddMenuItems()
        AddMenuItem(GetType(ManifestManagerNode), Resource.ResourceManager(Resource.MessageKey.ManifestProviderMenuItem))
        AddMenuItem(GetType(ServiceConfigNode), Resource.ResourceManager(Resource.MessageKey.ServiceConfigurationMenuItem))
        AddMenuItem(GetType(DownloaderNodeCollection), Resource.ResourceManager(Resource.MessageKey.DownloadersMenuItem))

    End Sub 'OnAddMenuItems


    '/ <summary>
    '/ Set the name of the node 
    '/ </summary>
    Protected Overrides Sub OnSited()
        MyBase.OnSited()
        Site.Name = Resource.ResourceManager(Resource.MessageKey.UpdaterApplicationBlock)

        If Not (_settings.ManifestManager Is Nothing) Then
            Me.Nodes.Add(New ManifestManagerNode(_settings.ManifestManager))
        End If
        If Not (_settings.Service Is Nothing) Then
            Me.Nodes.Add(New ServiceConfigNode(_settings.Service))
        End If
        If Not (_settings.Downloaders Is Nothing) AndAlso _settings.Downloaders.Count > 0 Then
            Me.Nodes.Add(New DownloaderNodeCollection(_settings.Downloaders))
        End If

    End Sub 'OnSited

    '/ <summary>
    '/ Add the default child nodes.
    '/ </summary>
    Protected Overrides Sub AddDefaultChildNodes()
        MyBase.AddDefaultChildNodes()
        CreateCryptographySettingsNode()

    End Sub 'AddDefaultChildNodes

    '/ <summary>
    '/ Allows child nodes to resolve references to sibling nodes in configuration.
    '/ </summary>
    Public Overrides Sub ResolveNodeReferences()
        MyBase.ResolveNodeReferences()

        Me.ManagerNode = CType(Hierarchy.FindNodeByType(Me, GetType(ManifestManagerNode)), ManifestManagerNode)
        Me.ServiceNode = CType(Hierarchy.FindNodeByType(Me, GetType(ServiceConfigNode)), ServiceConfigNode)
    End Sub 'ResolveNodeReferences


    Private Sub OnNodeRemoved(ByVal sender As Object, ByVal e As ConfigurationNodeChangedEventArgs)
        If TypeOf e.Node Is ManifestManagerNode Then
            _settings.ManifestManager = Nothing
            _managerNode = Nothing
        End If

        If TypeOf e.Node Is ServiceConfigNode Then
            _settings.Service = Nothing
            _serviceNode = Nothing
        End If
    End Sub 'OnNodeRemoved

    Private Sub OnNodeRenamed(ByVal sender As Object, ByVal e As ConfigurationNodeChangedEventArgs)

    End Sub 'OnNodeRenamed

#End Region

#Region "Private members"


    '/ <summary>
    '/ Add a menu entry.
    '/ </summary>
    '/ <param name="type">The type of the child node.</param>
    '/ <param name="text">The menu item text.</param>
    Private Overloads Sub AddMenuItem(ByVal type As Type, ByVal [text] As String)
        Dim item As New ConfigurationMenuItem([text], New AddChildNodeCommand(Site, type), Me, Shortcut.None, Resource.ResourceManager(Resource.MessageKey.CreateNewText) + [text], InsertionPoint.[New])
        Dim contains As Boolean = Hierarchy.ContainsNodeType(Me, type)
        item.Enabled = Not contains
        MyBase.AddMenuItem(item)

    End Sub 'AddMenuItem


    '/ <summary>
    '/ Checks whether there is a child node already created.
    '/ </summary>
    '/ <returns><c>true</c> if the child node is found, otherwise, <c>false</c>.</returns>
    Private Function CryptographySettingsNodeExists() As Boolean
        Dim node As CryptographySettingsNode = CType(Hierarchy.FindNodeByType(GetType(CryptographySettingsNode)), CryptographySettingsNode) '        
        Return Not (node Is Nothing)

    End Function 'CryptographySettingsNodeExists


    '/ <summary>
    '/ Creates the cryptograohy child node.
    '/ </summary>
    Private Sub CreateCryptographySettingsNode()
        If Not CryptographySettingsNodeExists() Then
            Dim cmd As New AddConfigurationSectionCommand(Site, GetType(CryptographySettingsNode), CryptographySettings.SectionName)
            cmd.Execute(Hierarchy.RootNode)
        End If

    End Sub 'CreateCryptographySettingsNode

#End Region
End Class 'ApplicationUpdaterNode