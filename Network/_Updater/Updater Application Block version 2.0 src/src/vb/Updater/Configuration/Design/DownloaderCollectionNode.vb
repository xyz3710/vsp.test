 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' DownloaderCollectionNode.cs
'
' Contains the implementation of the downloader collection node.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.ComponentModel
Imports System.Windows.Forms
Imports Microsoft.Practices.EnterpriseLibrary.Configuration.Design
Imports Microsoft.ApplicationBlocks.Updater
Imports Microsoft.ApplicationBlocks.Updater.Downloaders


'/ <summary>
'/ Defines the collection of downloader nodes.
'/ </summary>
<Image(GetType(DownloaderNodeCollection))>  _
Public Class DownloaderNodeCollection
    Inherits ConfigurationNode
    #Region "Private members"
    
    '/ <summary>
    '/ Mantains the download provider collection.
    '/ </summary>
    Private collection As DownloadProviderDataCollection
    
    #End Region
    
    #Region "Constructors"
    
    
    '/ <summary>
    '/ Default constructor.
    '/ </summary>
    Public Sub New() 
        MyClass.New(New DownloadProviderDataCollection())
    
    End Sub 'New
    
    
    '/ <summary>
    '/ Creates a DownloaderNodeCollection with the deserialized collection.
    '/ </summary>
    '/ <param name="collection"></param>
    Public Sub New(ByVal collection As DownloadProviderDataCollection) 
        Me.collection = collection
    
    End Sub 'New
    
    #End Region
    
    #Region "Public properties"
    
    '/ <summary>
    '/ The name of the collection.
    '/ </summary>
    
    <[ReadOnly](True)>  _
    Public Overrides Property Name() As String 
        Get
            Return MyBase.Name
        End Get
        Set
            MyBase.Name = value
        End Set
    End Property 
    '/ <summary>
    '/ Gets the provider data collection configuration.
    '/ </summary>
    
    <Browsable(False)>  _
    Public Overridable ReadOnly Property DownloaderDataCollection() As DownloadProviderDataCollection 
        Get
            collection = New DownloadProviderDataCollection()
            Dim node As DownloaderNode
            For Each node In  Nodes
                collection.Add(node.DownloadProviderData)
            Next node
            
            Return collection
        End Get
    End Property
    
    #End Region
    
    #Region "Protected members"
    
    
    '/ <summary>
    '/ Set the name of the current node selected.
    '/ </summary>
    Protected Overrides Sub OnSited() 
        MyBase.OnSited()
        Site.Name = Resource.ResourceManager(Resource.MessageKey.DownloadersMenuItem)
        
        Dim downloader As DownloadProviderData
        For Each downloader In  collection
            If TypeOf downloader Is BitsDownloaderProviderData Then
                Me.Nodes.Add(New BitsDownloaderNode(CType(downloader, BitsDownloaderProviderData)))
            Else
                Me.Nodes.Add(New DownloaderNode(downloader))
            End If
        Next downloader
    
    End Sub 'OnSited
     
    '/ <summary>
    '/ Add a menu item when the menu is required.
    '/ </summary>
    Protected Overrides Sub OnAddMenuItems() 
        MyBase.OnAddMenuItems()
        AddMenuItem(GetType(BitsDownloaderNode), Resource.ResourceManager(Resource.MessageKey.BitsDownloaderNode))
    
    End Sub 'OnAddMenuItems
    
    #End Region
    
    #Region "Private members"
    '/ <summary>
    '/ Adds a menu item using the base class methods.
    '/ </summary>
    '/ <param name="type">The type for the configuration information.</param>
    '/ <param name="text">The text for the menu.</param>
    Private Overloads Sub AddMenuItem(ByVal type As Type, ByVal [text] As String)
        Dim item As New ConfigurationMenuItem([text], New AddChildNodeCommand(Site, type), Me, Shortcut.None, Resource.ResourceManager(Resource.MessageKey.CreateNewText) + [text], InsertionPoint.[New])
        Dim contains As Boolean = Hierarchy.ContainsNodeType(Me, type)
        item.Enabled = Not contains
        MyBase.AddMenuItem(item)
    
    End Sub 'AddMenuItem
    
    #End Region
End Class 'DownloaderNodeCollection