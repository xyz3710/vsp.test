 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ApplicationUpdaterSettings.vb
'
' Contains the implementation of the application updater settings in the local configuration file.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Xml.Serialization

Namespace Microsoft.ApplicationBlocks.Updater.Configuration
    ' <summary>
    ' Defines the block configuration in the local configuration file.
    ' </summary>
    <XmlRoot("applicationUpdater", [Namespace]:=ApplicationUpdaterSettings.ConfigurationNamespace)> _
    Public Class ApplicationUpdaterSettings
#Region "Private members"

        ' <summary>
        ' The base path for the temp folders.
        ' </summary>
        Private _basePath As String

        ' <summary>
        ' The application id.
        ' </summary>
        Private _applicationId As String

        ' <summary>
        ' The manifest Uri.
        ' </summary>
        Private _manifestUri As String

        ' <summary>
        ' The service configuration data.
        ' </summary>
        Private _service As ServiceSettings

        ' <summary>
        ' The collection of downloader providers.
        ' </summary>
        Private _downloaders As DownloadProviderDataCollection

        ' <summary>
        ' The settings for the manifest manager.
        ' </summary>
        Private _manifestManager As ManifestManagerSettings

#End Region

#Region "Public constants "

        ' <summary>
        ' The namespace for the XML configuration file.
        ' </summary>
        Public Const ConfigurationNamespace As String = "urn:schemas-microsoft-com:PAG:updater-application-block:v2"

        ' <summary>
        ' Configuration key for application updater manager settings.
        ' </summary>
        Public Const SectionName As String = "UpdaterConfiguration"

#End Region

#Region "Public properties "

        ' <summary>
        ' The base path for the temporary folders.
        ' </summary>

        <XmlAttributeAttribute("basePath")> _
        Public Property BasePath() As String
            Get
                Return _basePath
            End Get
            Set(ByVal Value As String)
                _basePath = Value
            End Set
        End Property
        ' <summary>
        ' The application ID.
        ' </summary>

        <XmlAttributeAttribute("applicationId")> _
        Public Property ApplicationId() As String
            Get
                Return _applicationId
            End Get
            Set(ByVal Value As String)
                _applicationId = Value
            End Set
        End Property
        ' <summary>
        ' The manifest URI.
        ' </summary>

        <XmlAttributeAttribute("manifestUri")> _
        Public Property ManifestUri() As String
            Get
                Return _manifestUri
            End Get
            Set(ByVal Value As String)
                _manifestUri = Value
            End Set
        End Property
        ' <summary>
        ' The service configuration data.
        ' </summary>

        <XmlElement("service")> _
        Public Property Service() As ServiceSettings
            Get
                Return _service
            End Get
            Set(ByVal Value As ServiceSettings)
                _service = Value
            End Set
        End Property
        ' <summary>
        ' The collection of downloader providers.
        ' </summary>

        <XmlArray("downloaders"), XmlArrayItem("downloader", GetType(DownloadProviderData))> _
        Public Property Downloaders() As DownloadProviderDataCollection
            Get
                Return _downloaders
            End Get
            Set(ByVal Value As DownloadProviderDataCollection)
                _downloaders = Value
            End Set
        End Property
        ' <summary>
        ' The settings for the manifest manager.
        ' </summary>

        <XmlElement("manifestManager")> _
        Public Property ManifestManager() As ManifestManagerSettings
            Get
                Return _manifestManager
            End Get
            Set(ByVal Value As ManifestManagerSettings)
                _manifestManager = Value
            End Set
        End Property
#End Region
    End Class 'ApplicationUpdaterSettings
End Namespace
