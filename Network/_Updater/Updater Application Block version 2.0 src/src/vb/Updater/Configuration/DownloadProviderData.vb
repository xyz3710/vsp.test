 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' DownloadProviderData.vb
'
' Contains the implementation of the downloader provider configuration data.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.Configuration
    ' <summary>
    ' Defines abstract base class for the downloader provider configuration.
    ' </summary>
    <XmlRoot("downloader", [Namespace]:=ApplicationUpdaterSettings.ConfigurationNamespace)> _
                Public MustInherit Class DownloadProviderData
        Inherits ProviderData
#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Protected Sub New()

        End Sub 'New


        ' <summary>
        ' Creates a DownloadProviderData specifying the name.
        ' </summary>
        ' <param name="name"></param>
        Protected Sub New(ByVal name As String)
            MyBase.New(name)

        End Sub 'New

#End Region
    End Class 'DownloadProviderData
End Namespace