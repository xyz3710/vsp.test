'============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ServiceSettings.vb
'
' Contains the implementation of the service configuration.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Xml.Serialization

Namespace Microsoft.ApplicationBlocks.Updater.Configuration
    ' <summary>
    ' Defines the configuration required by the service to process updates.
    ' </summary>
    <XmlRoot("service", [Namespace]:=ApplicationUpdaterSettings.ConfigurationNamespace)> _
    Public Class ServiceSettings
#Region "Private members"

        ' <summary>
        ' The polling interval of the service.
        ' </summary>
        Private _interval As String

#End Region

#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "Public properties"

        ' <summary>
        ' The polling interval.
        ' </summary>

        <XmlAttributeAttribute("updateInterval")> _
        Public Property Interval() As String
            Get
                Return _interval
            End Get
            Set(ByVal Value As String)
                _interval = Value
            End Set
        End Property
#End Region
    End Class 'ServiceSettings
End Namespace