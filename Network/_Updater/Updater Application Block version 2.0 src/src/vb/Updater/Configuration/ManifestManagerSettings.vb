 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ManifestManagerSettings.vb
'
' Contains the implementation of the manifest manager.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Xml.Serialization

Namespace Microsoft.ApplicationBlocks.Updater.Configuration
    ' <summary>
    ' Defines the configuration for the manifest manager.
    ' </summary>
    <XmlRoot("manifestManager", [Namespace]:=ApplicationUpdaterSettings.ConfigurationNamespace)> _
    Public Class ManifestManagerSettings
#Region "Private members"

        ' <summary>
        ' The athentication type for getting the manifest.
        ' </summary>
        Private _authenticationMethod As AuthenticationType = AuthenticationType.Anonymous

        ' <summary>
        ' The user name used as a credential.
        ' </summary>
        Private _userName As String

        ' <summary>
        ' The password used as a credential.
        ' </summary>
        Private _password As String

#End Region

#Region "Public properties"

        ' <summary>
        ' The authentication type.
        ' </summary>

        <XmlAttributeAttribute("authenticationMethod")> _
        Public Property AuthenticationType() As AuthenticationType
            Get
                Return _authenticationMethod
            End Get
            Set(ByVal Value As AuthenticationType)
                _authenticationMethod = Value
            End Set
        End Property
        ' <summary>
        ' The user name credential.
        ' </summary>

        <XmlAttributeAttribute("userName")> _
        Public Property UserName() As String
            Get
                Return _userName
            End Get
            Set(ByVal Value As String)
                _userName = Value
            End Set
        End Property
        ' <summary>
        ' The password credential.
        ' </summary>

        <XmlAttributeAttribute("password")> _
        Public Property Password() As String
            Get
                Return _password
            End Get
            Set(ByVal Value As String)
                _password = Value
            End Set
        End Property
#End Region
    End Class 'ManifestManagerSettings
End Namespace
