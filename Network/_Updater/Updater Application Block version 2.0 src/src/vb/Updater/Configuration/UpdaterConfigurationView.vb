'============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' UpdaterConfigurationView.vb
'
' Contains the implementation of the updater configuration view.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================

Imports System
Imports System.IO
Imports System.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.Configuration
    ' <summary>
    ' Represents a view into configuration data.
    ' </summary>

    Public Class UpdaterConfigurationView
        Inherits ConfigurationView


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()
            MyBase.New(ConfigurationManager.GetCurrentContext())
        End Sub 'New


        ' <summary>
        ' <para>Initialize a new instance of the <see cref="UpdaterConfigurationView"/> with a <see cref="ConfigurationContext"/>.</para>
        ' </summary>
        ' <param name="context">
        ' <para>A <see cref="ConfigurationContext"/> object.</para>
        ' </param>
        Public Sub New(ByVal context As ConfigurationContext)
            MyBase.New(context)

        End Sub 'New


        ' <summary>
        ' Returns the <see cref="DownloadProviderData"/> for the specified downloader name.
        ' </summary>
        ' <param name="downloadProviderName">The name of the downloader</param>
        ' <returns>A <see cref="DownloadProviderData"/> instance</returns>
        Public Overridable Function GetDownloadProviderData(ByVal downloadProviderName As String) As DownloadProviderData
            Return GetApplicationUpdaterSettings().Downloaders(downloadProviderName)

        End Function 'GetDownloadProviderData


        ' <summary>
        ' Returns the <see cref="ApplicationUpdaterSettings"/> configured for the updater.
        ' </summary>
        ' <returns>An <see cref="ApplicationUpdaterSettings"/> instance</returns>

        Public Overridable Function GetApplicationUpdaterSettings() As ApplicationUpdaterSettings
            Return CType(ConfigurationContext.GetConfiguration(ApplicationUpdaterSettings.SectionName), ApplicationUpdaterSettings)
        End Function 'GetApplicationUpdaterSettings


        ' <summary>
        ' Gets the default URI to get the manifest from.
        ' </summary>


        Public Overridable ReadOnly Property DefaultManifestUriLocation() As Uri
            Get
                Dim manifestUri As Uri = Nothing
                Dim location As String = IIf(Not (GetApplicationUpdaterSettings() Is Nothing), GetApplicationUpdaterSettings().ManifestUri, Nothing)  'TODO: For performance reasons this should be changed to nested IF statements
                If Not (location Is Nothing) Then
                    manifestUri = New Uri(location)
                Else
                    Dim manifestUriConfigurationException As New ConfigurationException(Resource.ResourceManager(Resource.MessageKey.DefaultManifestUriMissing))
                    Logger.LogException(manifestUriConfigurationException)
                    Throw manifestUriConfigurationException
                End If
                Return manifestUri
            End Get
        End Property

        ' <summary>
        ' Gets the application ID for the configured application.
        ' </summary>

        Public Overridable ReadOnly Property ApplicationId() As String
            Get
                Dim _applicationId As String = Nothing

                If Not (GetApplicationUpdaterSettings() Is Nothing) AndAlso (Not (GetApplicationUpdaterSettings().ApplicationId Is Nothing) AndAlso GetApplicationUpdaterSettings().ApplicationId.Length <> 0) Then
                    _applicationId = GetApplicationUpdaterSettings().ApplicationId
                End If

                Return _applicationId
            End Get
        End Property


        ' <summary>
        ' The base path for the temporary folders.
        ' </summary>

        Public ReadOnly Property BasePath() As String
            Get
                Dim applicationBasePath As String = Nothing

                If Not (GetApplicationUpdaterSettings() Is Nothing) Then
                    applicationBasePath = GetApplicationUpdaterSettings().BasePath
                End If
                If applicationBasePath Is Nothing OrElse (Not (applicationBasePath Is Nothing) AndAlso applicationBasePath.Length = 0) Then
                    applicationBasePath = Path.Combine(Environment.CurrentDirectory, "UAB")
                End If

                If Not Directory.Exists(applicationBasePath) Then
                    Dim info As DirectoryInfo = Directory.CreateDirectory(applicationBasePath)
                    applicationBasePath = info.FullName
                Else
                    applicationBasePath = Path.GetFullPath(applicationBasePath)
                End If

                Return applicationBasePath
            End Get
        End Property

        ' <summary>
        ' Indicates whether the application is a service.
        ' </summary>

        Public ReadOnly Property IsService() As Boolean
            Get
                Return Not (GetApplicationUpdaterSettings().Service Is Nothing)
            End Get
        End Property

        ' <summary>
        ' The polling interval between updates.
        ' </summary>

        Public ReadOnly Property ServiceQueryInterval() As TimeSpan
            Get
                If IsService Then
                    Return TimeSpan.Parse(GetApplicationUpdaterSettings().Service.Interval)
                End If
                Return TimeSpan.MaxValue
            End Get
        End Property

        ' <summary>
        ' The manifest manager configuration.
        ' </summary>

        Friend ReadOnly Property ManifestManager() As ManifestManagerSettings
            Get
                Return GetApplicationUpdaterSettings().ManifestManager
            End Get
        End Property

        ' <summary>
        ' The configuration for the downloaders.
        ' </summary>

        Friend ReadOnly Property Downloaders() As DownloadProviderDataCollection
            Get
                Return GetApplicationUpdaterSettings().Downloaders
            End Get
        End Property
    End Class 'UpdaterConfigurationView 
End Namespace
