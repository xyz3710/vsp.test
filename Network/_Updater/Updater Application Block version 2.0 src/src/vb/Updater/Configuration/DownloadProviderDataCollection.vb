 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' DownloadProviderDataCollection.vb
'
' Contains the implementation of the collection of downloader data.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Collections
Imports Microsoft.Practices.EnterpriseLibrary.Configuration

Namespace Microsoft.ApplicationBlocks.Updater.Configuration
    ' <summary>
    ' Defines a collection of downloader configuration in the loca configuration file.
    ' </summary>

    Public Class DownloadProviderDataCollection
        Inherits ProviderDataCollection
#Region "Constructors"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "Public properties"

        ' <summary>
        ' Returns the downloader data by its position in the collection.
        ' </summary>

        Default Public Property Item(ByVal index As Integer) As DownloadProviderData
            Get
                Return CType(GetProvider(index), DownloadProviderData)
            End Get
            Set(ByVal Value As DownloadProviderData)
                SetProvider(index, Value)
            End Set
        End Property
        ' <summary>
        ' Returns the downloader data by its name in the collection.
        ' </summary>

        Default Public Property Item(ByVal name As String) As DownloadProviderData
            Get
                Return CType(GetProvider(name), DownloadProviderData)
            End Get
            Set(ByVal Value As DownloadProviderData)
                SetProvider(name, Value)
            End Set
        End Property
#End Region

#Region "Public methods"


        ' <summary>
        ' Adds downloader data to the collection.
        ' </summary>
        ' <param name="providerData">The downloader configuration data.</param>
        Public Sub Add(ByVal providerData As DownloadProviderData)
            AddProvider(providerData.Name, providerData)

        End Sub 'Add

#End Region
    End Class 'DownloadProviderDataCollection
End Namespace
