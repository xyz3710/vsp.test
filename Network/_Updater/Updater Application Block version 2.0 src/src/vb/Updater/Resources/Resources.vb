 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' Resources.vb
'
' Helper class to provide access to .NET resource files simply in other Updater classes.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Globalization
Imports System.IO
Imports System.Reflection
Imports System.Resources

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Helper class used to manage application resources.
    ' </summary>
    ' <exclude/>

    Public NotInheritable Class Resource

#Region "MessageKey enum"

        ' <summary>
        ' The message keys defined in an enumeration to avoid message synchronization problems.
        ' </summary>

        Public Enum MessageKey

#Region "General Messages"
            ArgumentMissing
            ArgumentMissing1
            FileNotExists
#End Region

#Region "Updater Messages"
            CannotFindActivationTaskForManifest
            ActivationTaskForManifestInInvalidState
            CallerIsNotUpdaterService
            MissingManifestUri
            UpdaterManagerOnlyToBeUsedByWindows
            UpdaterManagerUpdateCannotBeCanceled
            ApplicationIdCannotBeNullOrEmpty
            LocationCannotBeNullOrEmpty
            NoConfigurationToGetApplicationId
            ManifestManagerMustBeValidId
            CultureIdToGetComErrorStringsFormatted
#End Region

#Region "Manifest Manager"
            FileHashIsNull
            ManifestSchemaError
            DuplicatedManifestId
            SourceAndDestinationExpected
            PathExpected
            ProcessorNotConfigured
            AssemblyReferenceExpected
            CannotFindDotNetFolder
            CannotFindTool
            ToolGeneratedError
            SourceExpected
            ValidateHashExceptionMessage
            ActivationPausedMessage
            FileCopyProcessorSourceFileNotExists
            CantCreateAlgorithmProvider
#End Region

#Region "Downloader Messages"
            UncPathUnsupported
            BitsDownloaderFailedDownload
            BitsDownloadedNotConfigured
            DownloadTimeoutExceeded
            BitsJobName
            BitsDescription
            BitsFilesDownloadJobName
            BitsFilesDownloadDescription
            CannotCreateDefaultDownloader
            CannotCreateDownloader
            BITSCannotConnectToJob
#End Region

#Region "Activation Messages"
            ProcessorOnErrorException
#End Region

#Region "Manifest Provider"
            DefaultManifestUriMissing
#End Region

#Region "Manifest tool"
            ManifestAlreadyIncluded
            ProcessorAlreadyIncluded
            IncompleteCommandWasIssued
            MustSpecifyValidValueForArgument
            WelcomeToTheManifestTool
            TypeHelpToGetUsageInformation
            ReadyString
            CommandUnsupported
            UnsavedChangesProceedQuestion
            TheManifestIsValid
            TheManifestIsInvalid
            AddFileLabel
            OpenCryptographyConfigurationLabel
            OpenCryptographyConfigurationFilter
            ManifestFilesFilter
            YesString
            NoString
            ConfirmOperationTitle
            OpenManifestTitle
            ManifestValidationTitle
            PathDoesNotExists
            ApplicationDeployProcessor
            FileCopyProcessor
            FileDeleteProcessor
            FolderCopyProcessor
            FolderDeleteProcessor
            GacUtilProcessor
            InstallUtilProcessor
            MSIProcessor
            UncompressProcessor
            HashValidationProcessor
            WaitForExitProcessor
            UpdaterApplicationBlockConfiguration
            UpdaterApplicationBlock
            GetSetApplicationBasePath
            GetSetApplicationIdentifier
            GetSetManifestLocation
            ManifestProviderMenuItem
            ServiceConfigurationMenuItem
            DownloadersMenuItem
            DownloaderMenuItem
            CreateNewText
            ServiceConfigurationNode
            GetSetUpdaterServiceInterval
            BitsDownloaderNode
            GetSetUserName
            GetSetPassword
            GetSetAuthenticationScheme
            GetSetServerType
            ManifestManagerNode
            GetSetDownloaderTypeName
            GetSetAuthenticationType
            GetSetManifestSettings
            EditorControlNotConfigured
            InvalidProcessorItemForEdition
            MissingManifestEditorConfiguration
            MissingManifestEditorConfigurationTitle
            ManifestEditorFilesNotAdded
            ManifestEditorCannotChangeFolder
            EditorValidationErrorCaption
            EditorMissingApplicationId
            EditorMissingManifestDescription
            EditorMissingManifestId
            EditorMissingApplicationLocation
            BrowseForWorkingFolderDialogText
			BrowseForSourceFolderDescription
			InvalidSourceFolderMessage
			InvalidSourceFolderCaption
            ToolCannotLoadManifestSchema
#End Region

        End Enum 'MessageKey
#End Region

#Region "Static part"

        ' <summary>
        ' Implement Singleton --.NET static initialization is guaranteed to be thread-safe
        ' </summary>
        Private Shared resource As New resource

        ' <summary>
        ' Returns the singleton instance of the resource manager
        ' </summary>

        Public Shared ReadOnly Property ResourceManager() As resource
            Get
                Return resource
            End Get
        End Property

#End Region

#Region "Instance part"

        ' <summary>
        ' This is the ACTUAL resource manager, for which this class is just a convenience wrapper
        ' </summary>
        Private _resourceManager As ResourceManager = Nothing


        ' <summary>
        ' Make constructor private so noone can directly create an instance of Resource, only use the Static Property ResourceManager
        ' </summary>
        Private Sub New()
            _resourceManager = New ResourceManager("ApplicationUpdaterText", [Assembly].GetExecutingAssembly())

        End Sub 'New


        ' <summary>
        ' A convenience indexer that accesses the internal resource manager
        ' </summary>

        Default Public ReadOnly Property Item(ByVal key As String) As String
            Get
                ' try to retreive the message, if not, at least return the key
                Dim msg As String = _resourceManager.GetString(key, CultureInfo.CurrentCulture)
                If Not (msg Is Nothing) Then
                    Return msg
                Else
                    Return key
                End If
            End Get
        End Property

        ' <summary>
        ' Returns the localized string for a string key and a set of parameters.
        ' </summary>

        Default Public ReadOnly Property Item(ByVal key As String, ByVal ParamArray par() As Object) As String
            Get
                Return String.Format(CultureInfo.CurrentUICulture, _resourceManager.GetString(key, CultureInfo.CurrentCulture), par)
            End Get
        End Property

        ' <summary>
        ' Returns the localized string for a message key.
        ' </summary>

        Default Public ReadOnly Property Item(ByVal key As MessageKey) As String
            Get
                Return Me(key.ToString(CultureInfo.InvariantCulture))
            End Get
        End Property

        ' <summary>
        ' Returns the localized message for a string key and a set of parameters.
        ' </summary>

        Default Public ReadOnly Property Item(ByVal key As MessageKey, ByVal ParamArray par() As Object) As String
            Get
                Return String.Format(CultureInfo.CurrentUICulture, Me(key), par)
            End Get
        End Property


        ' <summary>
        ' Returns the resource stream using the name of the embedded resource.
        ' </summary>
        Public Function GetStream(ByVal name As String) As Stream
            Return [Assembly].GetExecutingAssembly().GetManifestResourceStream(name)

        End Function 'GetStream
#End Region

    End Class
End Namespace

