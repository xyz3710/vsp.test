 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' FilesCollection.vb
'
' This files contains the implementation of the files collection for the manifest.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Collections


Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Defines a collection of files with its own configuration.
    ' </summary>

    Public Class FilesCollection
        Inherits CollectionBase
#Region "Private members"

        ' <summary>
        ' The base path for all the files.
        ' </summary>
        Private basePath As String

        ' <summary>
        ' Whether hash comparison is enabled or not.
        ' </summary>
        Private hashComparison As Boolean

        ' <summary>
        ' The EntLib hash provider reference.
        ' </summary>
        Private hashProviderName As String

#End Region

#Region "Constructors"


        ' <summary>
        ' Creates a FilesCollection using the information in the serialized manifest.
        ' </summary>
        ' <param name="files">The files collection in the manifest.</param>
        Public Sub New(ByVal files As Xsd.ManifestFiles)
            If Not (files Is Nothing) AndAlso Not (files.Files Is Nothing) AndAlso files.Files.Length > 0 Then
                basePath = Utilities.FileUtility.AppendSlashUrlOrUnc(files.Base)
                hashComparison = files.HashComparison = Xsd.YesNoBoolType.Yes OrElse files.HashComparison = Xsd.YesNoBoolType.True
                hashProviderName = files.HashProviderName
                Dim i As Integer
                For i = 0 To files.Files.Length
                    If i = files.Files.Length Then
                        Exit For
                    End If
                    List.Add(New FileManifest(files.Files(i)))
                Next i
            End If

        End Sub 'New

#End Region

#Region "Public properties"

        ' <summary>
        ' The base folder path.
        ' </summary>

        Public ReadOnly Property Base() As String
            Get
                Return basePath
            End Get
        End Property

        ' <summary>
        ' Indicates whether the hash comparison is enabled for all the files.
        ' </summary>

        Public ReadOnly Property IsHashComparisonEnabled() As Boolean
            Get
                Return hashComparison
            End Get
        End Property

        ' <summary>
        ' The reference to the EntLib hash provider.
        ' </summary>

        Public ReadOnly Property HashProvider() As String
            Get
                Return hashProviderName
            End Get
        End Property

        ' <summary>
        ' Indexer that returns a file for a given index.
        ' </summary>
        ' <param name="index">The index of the file to locate.</param>

        Default Public Property Item(ByVal index As Integer) As FileManifest
            Get
                Return CType(List(index), FileManifest)
            End Get
            Set(ByVal Value As FileManifest)
                List(index) = Value
            End Set
        End Property

#End Region

#Region "Public methods"


        ' <summary>
        ' Allows determining if the file is already contained in the collection.
        ' </summary>
        ' <param name="value">The FileManiest instance.</param>
        ' <returns>A boolean value indicating whether file is in the collection.</returns>
        Public Function Contains(ByVal value As FileManifest) As Boolean
            Return List.Contains(value)

        End Function 'Contains


        ' <summary>
        ' Adds a file to the file collection.
        ' </summary>
        ' <param name="value">The FileManiest instance.</param>
        Public Sub Add(ByVal value As FileManifest)
            List.Add(value)

        End Sub 'Add


        ' <summary>
        ' Removes the file from the file collection.
        ' </summary>
        ' <param name="value">The FileManiest instance.</param>
        Public Sub Remove(ByVal value As FileManifest)
            List.Remove(value)

        End Sub 'Remove


        ' <summary>
        ' Insert the file at a specific index in the collection.
        ' </summary>
        ' <param name="index">The index to insert the file.</param>
        ' <param name="value">The FileManiest instance.</param>
        Public Sub Insert(ByVal index As Integer, ByVal value As FileManifest)
            List.Insert(index, value)

        End Sub 'Insert

#End Region
    End Class 'FilesCollection
End Namespace