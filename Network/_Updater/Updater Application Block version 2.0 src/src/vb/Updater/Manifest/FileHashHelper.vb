 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' FileHashHelper.vb
'
' Contains the implementation of the file hash helper.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.IO
Imports System.Security.Cryptography

Imports Microsoft.Practices.EnterpriseLibrary.Security.Cryptography

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Allows calculating and comparing hashes of existing files.
    ' </summary>

    Friend NotInheritable Class FileHashHelper
#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Private Sub New()

        End Sub 'New

#End Region

#Region "Public methods"


        ' <summary>
        ' Calculares the hash of a specific file and compares the resultng hash with the provided one.
        ' </summary>
        ' <param name="hashProviderName">The name of the EntLib hash provider.</param>
        ' <param name="filePath">The path of the file to calculate the hash.</param>
        ' <param name="hash">The calculated hash provided in the manifest.</param>
        ' <returns><c>true</c> if the hash results the same, otherwise <c>false</c>.</returns>
        Public Shared Function CompareHash(ByVal hashProviderName As String, ByVal filePath As String, ByVal hash As String) As Boolean
            Dim filestream As New filestream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read)
            Dim count As Integer
            Try
                Dim fileBytesTemp(fileStream.Length) As Byte
                Dim fileBytes(fileStream.Length - 1) As Byte
                fileStream.Read(fileBytesTemp, 0, fileStream.Length)

                For count = 0 To filestream.Length - 1
                    fileBytes(count) = fileBytesTemp(count)
                Next count

                Return Cryptographer.CompareHash(hashProviderName, fileBytes, Convert.FromBase64String(hash))
            Finally
                fileStream.Close()
                fileStream = Nothing
            End Try

        End Function 'CompareHash

#End Region
    End Class 'FileHashHelper
End Namespace