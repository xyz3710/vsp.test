 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ManifestManager.vb
'
' Contains the implementation of the manifest manager.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Collections
Imports System.Globalization
Imports System.IO
Imports System.Net
Imports System.Reflection
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Security.Permissions
Imports System.Security.Principal
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.ApplicationBlocks.Updater.Configuration
Imports Microsoft.ApplicationBlocks.Updater.Utilities
Imports Microsoft.ApplicationBlocks.Updater.Xsd

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' This class is responsible for fetching the manifest from the specified location, 
    ' checking if the manifest has been applied and returning a list of valid manifests to
    ' apply.
    ' This class can be used as an instance member or as a static member. It is used as a static 
    ' member variable in the UpdaterManager. In order to support testability of the class we allow
    ' classes to use this as an instance member.
    ' </summary>

    Friend NotInheritable Class ManifestManager
#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New
#End Region

#Region "Public Methods"


        ' <summary>
        ' Return all the manifests for the Uri and the application id.
        ' </summary>
        ' <param name="location">The Uri location for the manifest.</param>
        ' <param name="applicationId">The application id.</param>
        ' <returns>The manifest list.</returns>
        Public Function GetManifests(ByVal location As Uri, ByVal applicationId As String) As Manifest()
            If applicationId Is Nothing OrElse (Not (applicationId Is Nothing) AndAlso applicationId.Length = 0) Then
                Logger.LogAndThrowException(New ArgumentException(Resource.ResourceManager(Resource.MessageKey.ManifestManagerMustBeValidId), "applicationId"))
            End If

            Return Me.GetValidManifests(location, applicationId)

        End Function 'GetManifests


        ' <summary>
        ' Save the manifest in the manifest registry so they can be retrieved in the next update.
        ' </summary>
        ' <param name="manifest">The Manifest instance.</param>
        <SecurityPermission(SecurityAction.Demand, SerializationFormatter:=True)> _
        Public Sub CommitUpdate(ByVal manifest As Manifest)
            Dim manifestPath As String = Path.Combine(New UpdaterConfigurationView().BasePath, "applications")

            manifestPath = Path.Combine(manifestPath, manifest.Application.ApplicationId)
            If Not Directory.Exists(manifestPath) Then
                Directory.CreateDirectory(manifestPath)
            End If
            manifestPath = Path.Combine(manifestPath, manifest.ManifestId.ToString() + ".appliedmanifest")

            Try
                Dim stream = New FileStream(manifestPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read)
                Try
                    Dim formatter As New BinaryFormatter
                    formatter.Serialize(stream, manifest)
                Finally
                    stream.Flush()
                    stream.Close()
                End Try
            Catch ex As Exception
                File.Delete(manifestPath)
                Logger.LogAndThrowException(ex)
            End Try

        End Sub 'CommitUpdate

#End Region

#Region "Private Members"


        ' <summary>
        ' Return the Manifest instance for the application id using the remote location.
        ' </summary>
        ' <param name="location">The Uri to the location of the manifest.</param>
        ' <param name="applicationId">The application id.</param>
        ' <returns></returns>
        Private Function GetValidManifests(ByVal location As Uri, ByVal applicationId As String) As Manifest()
            If location Is Nothing Then
                Logger.LogAndThrowException(New ArgumentNullException(Resource.ResourceManager(Resource.MessageKey.ArgumentMissing1, "location")))
            End If

            ' Donwload and validate the primary manifest
            Dim primaryManifest As Xsd.Manifest = CType(ValidateAndDeserialize(GetType(Xsd.Manifest), location, "Manifest.xsd"), Xsd.Manifest)

            ' Download, validate and process each manifest included by the primary manifest
            Dim count As Integer
            If (primaryManifest.IncludedManifests Is Nothing) Then
                count = 0
            Else
                count = primaryManifest.IncludedManifests.Manifest.Length
            End If

            ' Allocate an array for holding the primary + included manifests
            Dim validManifests As New ArrayList(count)

            ' Create the manifest from the Xsd class
            Dim primaryManifestTemp As New Manifest(primaryManifest)

            If String.Compare(primaryManifestTemp.Application.ApplicationId, applicationId, True, CultureInfo.InvariantCulture) = 0 Then
                PopulateValidManifestList(primaryManifestTemp, primaryManifest, validManifests)

                ' Checking for duplicate included manifests.

                ' Checking for duplicate included manifests.
                If (count > 0) Then
                    Dim includedManifestIds As New ArrayList(count - 1)
                    Dim index As Integer
                    For index = 0 To count - 1
                        If includedManifestIds.Contains(primaryManifest.IncludedManifests.Manifest(index).ManifestId) Then
                            Logger.LogAndThrowException(New ApplicationUpdaterException(Resource.ResourceManager(Resource.MessageKey.DuplicatedManifestId)))
                        Else
                            includedManifestIds.Add(primaryManifest.IncludedManifests.Manifest(index).ManifestId)
                        End If
                    Next index
                End If


                If count > 0 Then
                    Dim i As Integer
                    For i = 0 To count - 1
                        Try
                            Dim ub As New UriBuilder(location)
                            ub.Path = Path.Combine(Path.GetDirectoryName(ub.Path), primaryManifest.IncludedManifests.Manifest(i).Location)

                            Dim xsdManifest As Xsd.Manifest = CType(ValidateAndDeserialize(GetType(Xsd.Manifest), ub.Uri, "Manifest.xsd"), Xsd.Manifest)
                            Dim includedManifest As New Manifest(xsdManifest)
                            If String.Compare(includedManifest.Application.ApplicationId, applicationId, True, CultureInfo.InvariantCulture) = 0 Then
                                PopulateValidManifestList(includedManifest, primaryManifest, validManifests)
                            End If
                        Catch ex As Exception
                            Logger.LogAndThrowException(ex)
                        End Try
                    Next i
                End If
            End If

            Return CType(validManifests.ToArray(GetType(Manifest)), Manifest())

        End Function 'GetValidManifests


        ' <summary>
        ' Check wether the manifest is applied locally and add the manifest to a list or not.
        ' </summary>
        ' <param name="manifestToApply">The instance of the Manifest to apply.</param>
        ' <param name="primaryManifest">The primary manifest instance.</param>
        ' <param name="validManifests">The valid manifest list.</param>
        Private Sub PopulateValidManifestList(ByVal manifestToApply As Manifest, ByVal primaryManifest As Xsd.Manifest, ByVal validManifests As ArrayList)
            If Not ManifestApplied(manifestToApply.Application.ApplicationId, manifestToApply.ManifestId) Then
                manifestToApply.Apply = primaryManifest.Mandatory = YesNoBoolType.True OrElse primaryManifest.Mandatory = YesNoBoolType.Yes

                ' Make a hash comparison
                If manifestToApply.Files.IsHashComparisonEnabled Then
                    RemoveAppliedFiles(manifestToApply)
                End If

                validManifests.Add(manifestToApply)
            End If

        End Sub 'PopulateValidManifestList


        ' <summary>
        ' Removes the manifest from the list of applied manifests.
        ' </summary>
        ' <param name="manifest">The instance to the Manifest.</param>
        Private Sub RemoveAppliedFiles(ByVal manifest As Manifest)
            Dim rootPath As String = manifest.Application.Location

            Dim index As Integer
            For index = 0 To manifest.Files.Count - 1
                If index = manifest.Files.Count Then
                    Exit For
                End If
                Dim file As FileManifest = manifest.Files(index)

                Dim filePath As String = Path.Combine(rootPath, file.Source)

                If file.Hash Is Nothing Then
                    Logger.LogAndThrowException(New ApplicationUpdaterException(Resource.ResourceManager(Resource.MessageKey.FileHashIsNull, file.Source)))
                End If

                If FileNotChanged(manifest.Files.HashProvider, file.Hash, filePath) Then
                    manifest.Files.RemoveAt(index)
                    index -= 1
                End If
            Next index

        End Sub 'RemoveAppliedFiles


        ' <summary>
        ' Check the manifest file hash with the calculated file hash to detect file changes.
        ' </summary>
        ' <param name="hashProviderName">The EntLib hash provider name.</param>
        ' <param name="hash">The calculated hash in the manifest.</param>
        ' <param name="file">The flie that will be checked.</param>
        ' <returns><c>true</c>, if the file have not changed, otherwise returns <c>true</c>,</returns>
        Private Function FileNotChanged(ByVal hashProviderName As String, ByVal hash As String, ByVal _file As String) As Boolean
            If File.Exists(_file) Then
                Return FileHashHelper.CompareHash(hashProviderName, _file, hash)
            Else
                Return False
            End If

        End Function 'FileNotChanged


        ' <summary>
        ' Download and validates the manifest using the Uri location.
        ' </summary>
        ' <param name="type">The type that represents the manifest class.</param>
        ' <param name="location">The location of the manifest.</param>
        ' <param name="schemaResource">The resource where the embedded XSD resides in the assembly.</param>
        ' <returns>The Manifest instance.</returns>
        <SecurityPermission(SecurityAction.Demand, SerializationFormatter:=True)> _
        Private Function ValidateAndDeserialize(ByVal type As Type, ByVal location As Uri, ByVal schemaResource As String) As Object
            Dim result As Object = Nothing
            Try
                Dim doc As String = DownloadFile(location)
                Dim schema As Stream = [Assembly].GetExecutingAssembly().GetManifestResourceStream(schemaResource)
                Try
                    Dim validator As New SchemaValidator(doc, schema)
                    If Not validator.Validate() Then
                        Logger.LogAndThrowException(New ApplicationUpdaterException(Resource.ResourceManager(Resource.MessageKey.ManifestSchemaError, location, schemaResource)))
                    End If

                    Dim xser As New XmlSerializer(type)
                    result = xser.Deserialize(New XmlTextReader(doc, XmlNodeType.Document, Nothing))
                Finally
                    schema.Close()
                    schema = Nothing
                End Try
            Catch ex As Exception
                Logger.LogAndThrowException(ex)
            End Try

            Return result

        End Function 'ValidateAndDeserialize


        ' <summary>
        ' Used to detect whether the manifest is applied or not.
        ' </summary>
        ' <param name="applicationID">The application id.</param>
        ' <param name="manifestID">The manifest id.</param>
        ' <returns><c>true</c>, if the manifest is applied, otherwise returns <c>false</c>.</returns>
        Private Function ManifestApplied(ByVal applicationID As String, ByVal manifestID As Guid) As Boolean
            ' Check if the manifest has already been applied
            Dim manifestPath As String = Path.Combine(New UpdaterConfigurationView().BasePath, "applications")
            manifestPath = Path.Combine(manifestPath, applicationID)
            manifestPath = Path.Combine(manifestPath, manifestID.ToString() + ".appliedmanifest")

            ' If the manifest file exists, the manifest has been applied
            ' Or it's being applied if there's an associated task related to the manifest
            Return File.Exists(manifestPath) OrElse Not (RegistryManager.Current.GetByManifestID(manifestID) Is Nothing)

        End Function 'ManifestApplied


        ' <summary>
        ' Download a file using HTTP by a given Uri.
        ' </summary>
        ' <param name="uri">The uri of the file to download.</param>
        ' <returns>The contents of the file.</returns>
        Private Function DownloadFile(ByVal uri As Uri) As String
            Dim request As WebRequest = WebRequest.Create(uri)

            Dim method As AuthenticationType = AuthenticationType.Anonymous
            If Not (New UpdaterConfigurationView().ManifestManager Is Nothing) Then
                method = New UpdaterConfigurationView().ManifestManager.AuthenticationType
            End If

            Select Case method
                Case AuthenticationType.Integrated
                    request.ConnectionGroupName = WindowsIdentity.GetCurrent().Name
                    request.Credentials = CredentialCache.DefaultCredentials
                Case AuthenticationType.Basic
                    Dim windowsAuthType As String = "Basic"

                    Dim cache As New CredentialCache
                    cache.Add(uri, windowsAuthType, New NetworkCredential(New UpdaterConfigurationView().ManifestManager.UserName, New UpdaterConfigurationView().ManifestManager.Password))

                    request.Credentials = cache
                    request.PreAuthenticate = True
                Case AuthenticationType.Anonymous
                    request.Credentials = CredentialCache.DefaultCredentials
            End Select


            Dim response As String = String.Empty

            Dim res As WebResponse = request.GetResponse()
            Try
                Dim reader As New StreamReader(res.GetResponseStream(), True)
                Try
                    response = reader.ReadToEnd()
                Finally
                    reader.Close()
                    reader = Nothing
                End Try
            Finally
                res.Close()
                res = Nothing
            End Try
            Return response

        End Function 'DownloadFile

#End Region
    End Class 'ManifestManager
End Namespace