 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' FileManifest.vb
'
' Contains the implementation of the file information in the manifest.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Xml

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Defines the information of a file in the manifest.
    ' </summary>

    Public Class FileManifest
#Region "Private members"

        ' <summary>
        ' The source location for the file.
        ' </summary>
        Private sourceLocation As String

        ' <summary>
        ' Whether the file is transient or not.
        ' </summary>
        Private _transient As Boolean

        ' <summary>
        ' The calculated hash value for the file.
        ' </summary>
        Private fileHashValue As String

        ' <summary>
        ' The list of attributes defined in the file element.
        ' </summary>
        Private otherAttributes() As XmlAttribute

#End Region

#Region "Constructor"


        ' <summary>
        ' Creates a new FileManifest with the desrialized manifest file information.
        ' </summary>
        ' <param name="file">The FileManifest instance.</param>
        Public Sub New(ByVal file As Xsd.ManifestFile)
            otherAttributes = file.AnyAttr
            sourceLocation = file.Source
            fileHashValue = file.Hash
            _transient = file.Transient = Xsd.YesNoBoolType.True OrElse file.Transient = Xsd.YesNoBoolType.Yes

        End Sub 'New

#End Region

#Region "Public properties"

        ' <summary>
        ' The source location of the file.
        ' </summary>

        Public ReadOnly Property [Source]() As String
            Get
                Return sourceLocation
            End Get
        End Property

        ' <summary>
        ' Indicates whether the file is transient.
        ' </summary>

        Public ReadOnly Property Transient() As Boolean
            Get
                Return _transient
            End Get
        End Property

        ' <summary>
        ' The calculated hash for the file.
        ' </summary>

        Public ReadOnly Property Hash() As String
            Get
                Return fileHashValue
            End Get
        End Property

        ' <summary>
        ' The attributes specified for the file.
        ' </summary>

        Public ReadOnly Property Attributes() As XmlAttribute()
            Get
                Return otherAttributes
            End Get
        End Property
#End Region
    End Class 'FileManifest
End Namespace