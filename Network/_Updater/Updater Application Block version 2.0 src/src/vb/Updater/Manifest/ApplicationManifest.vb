 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ApplicationManifest.vb
'
' Contains the implementation of the information about the application within the manifest.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Xml

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' The information about the application within the manifest.
    ' </summary>

    Public Class ApplicationManifest
#Region "Declarations"

        ' <summary>
        ' The deserialized class for the manifest.
        ' </summary>
        Private manifestOfApplicationToUpdate As Xsd.ManifestApplication

        ' <summary>
        ' The application Id.
        ' </summary>
        Private _applicationId As String

        ' <summary>
        ' The application location folder.
        ' </summary>
        Private applicationUpdateLocation As String

#End Region

#Region "Constructor"


        ' <summary>
        ' Creates an ApplicationManifest using the deserialized manifest application information.
        ' </summary>
        ' <param name="application">The information about the application in the manifest.</param>
        Public Sub New(ByVal application As Xsd.ManifestApplication)
            manifestOfApplicationToUpdate = application
            applicationUpdateLocation = application.Location
            _applicationId = application.ApplicationId

        End Sub 'New

#End Region

#Region "ApplicationManifest Members"

        ' <summary>
        ' The application ID.
        ' </summary>

        Public ReadOnly Property ApplicationId() As String
            Get
                Return _applicationId
            End Get
        End Property
        ' <summary>
        ' The entry point for the application.
        ' </summary>

        Public ReadOnly Property StartCommand() As String
            Get
                Return manifestOfApplicationToUpdate.EntryPoint.File
            End Get
        End Property
        ' <summary>
        ' The parameters for the application.
        ' </summary>

        Public ReadOnly Property StartParameters() As String
            Get
                Return manifestOfApplicationToUpdate.EntryPoint.Parameters
            End Get
        End Property
        ' <summary>
        ' The folder location for the application.
        ' </summary>

        Public Property Location() As String
            Get
                Return applicationUpdateLocation
            End Get
            Set(ByVal Value As String)
                applicationUpdateLocation = Value
            End Set
        End Property
        ' <summary>
        ' The element defined in the XML manifest.
        ' </summary>

        Public ReadOnly Property Any() As XmlElement
            Get
                Return manifestOfApplicationToUpdate.Any
            End Get
        End Property
        ' <summary>
        ' The attributes defined in the XML manifest.
        ' </summary>

        Public ReadOnly Property Attributes() As XmlAttribute()
            Get
                Return manifestOfApplicationToUpdate.AnyAttr
            End Get
        End Property
#End Region
    End Class 'ApplicationManifest 
End Namespace