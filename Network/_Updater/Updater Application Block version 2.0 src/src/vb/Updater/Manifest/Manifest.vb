 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' Manifest.vb
'
' Contains the implementation of the manifest class.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Runtime.Serialization
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text
Imports System.Globalization
Imports System.Security.Permissions
Imports Microsoft.ApplicationBlocks.Updater.Configuration

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' This class mantains all the information of the manifest that was retrieved using the URI.
    ' </summary>
    <Serializable()> _
    Public Class Manifest
        Implements ISerializable
#Region "Private fields"

        ' <summary>
        ' The manifest class that was deserialized.
        ' </summary>
        Private updateManifest As Xsd.Manifest

        ' <summary>
        ' The manifest id.
        ' </summary>
        Private updaterManifestId As Guid

        ' <summary>
        ' The collection of files that was downloaded.
        ' </summary>
        Private filesToUpdate As FilesCollection

        ' <summary>
        ' The configuration for the downloader included in the manifest.
        ' </summary>
        Private downloaderConfigInformation As ManifestDownloaderProviderData

        ' <summary>
        ' The application information for the manifest.
        ' </summary>
        Private manifestOfApplicationToUpdate As ApplicationManifest

        ' <summary>
        ' The information about the activation processors configured in the manifest.
        ' </summary>
        Private activationProcessorsConfigInformation As ActivationProcessorProviderDataCollection

        ' <summary>
        ' Indicates whether the manifest will be applied.
        ' </summary>
        Private _apply As Boolean

#End Region

#Region "Public Constructors"


        ' <summary>
        ' Creates a manifest using the deserialized manifest instance.
        ' </summary>
        ' <param name="manifest">The XSD manifest to wrap.</param>
        Public Sub New(ByVal manifest As Xsd.Manifest)
            updateManifest = manifest

        End Sub 'New

#End Region

#Region "Manifest Members"

        ' <summary>
        ' The manifest ID.
        ' </summary>

        Public ReadOnly Property ManifestId() As Guid
            Get
                If updaterManifestId.CompareTo(Guid.Empty) = 0 Then
                    updaterManifestId = New Guid(updateManifest.ManifestId)
                End If
                Return updaterManifestId
            End Get
        End Property


        ' <summary>
        ' Initalizes the manifest by setting the XmlNode for the configuration.
        ' </summary>
        ' <param name="config">The node containinf the manifest representation</param>

        <SecurityPermission(SecurityAction.Demand, SerializationFormatter:=True)> _
        Public Sub Init(ByVal config As XmlNode)
            Dim xser As New XmlSerializer(GetType(Xsd.Manifest))
            updateManifest = CType(xser.Deserialize(New XmlNodeReader(config)), Xsd.Manifest)

        End Sub 'Init

        ' <summary>
        ' The description of the manifest.
        ' </summary>

        Public ReadOnly Property Description() As String
            Get
                Return updateManifest.Description
            End Get
        End Property

        ' <summary>
        ' Indicates whether the manifest will be applied or not.
        ' </summary>

        Public Property Apply() As Boolean
            Get
                Return _apply
            End Get
            Set(ByVal Value As Boolean)
                _apply = Value
            End Set
        End Property

        ' <summary>
        ' Indicates whether the manifest is mandatory or not.
        ' </summary>

        Public ReadOnly Property Mandatory() As Boolean
            Get
                Return updateManifest.Mandatory = Xsd.YesNoBoolType.True OrElse updateManifest.Mandatory = Xsd.YesNoBoolType.Yes
            End Get
        End Property

        ' <summary>
        ' The collection of files defined in the manifest.
        ' </summary>

        Public ReadOnly Property Files() As FilesCollection
            Get
                If filesToUpdate Is Nothing Then
                    filesToUpdate = New FilesCollection(updateManifest.Files)
                End If
                Return filesToUpdate
            End Get
        End Property

        ' <summary>
        ' The configuration for the downloader defined in the manifest.
        ' </summary>

        Public ReadOnly Property Downloader() As ManifestDownloaderProviderData
            Get
                If downloaderConfigInformation Is Nothing Then
                    If Not (updateManifest.Downloader Is Nothing) Then
                        downloaderConfigInformation = New ManifestDownloaderProviderData(updateManifest.Downloader.Name, updateManifest.Downloader)
                    End If
                End If
                Return downloaderConfigInformation
            End Get
        End Property

        ' <summary>
        ' The information for the application.
        ' </summary>

        Public ReadOnly Property Application() As ApplicationManifest
            Get
                If manifestOfApplicationToUpdate Is Nothing Then
                    manifestOfApplicationToUpdate = New ApplicationManifest(updateManifest.Application)
                End If
                Return manifestOfApplicationToUpdate
            End Get
        End Property

        ' <summary>
        ' The activation processors configuration.
        ' </summary>

        Public ReadOnly Property ActivationProcessors() As ActivationProcessorProviderDataCollection
            Get
                If activationProcessorsConfigInformation Is Nothing Then
                    If Not (updateManifest.ActivationProcess Is Nothing) Then
                        If updateManifest.ActivationProcess.Tasks Is Nothing Then
                            activationProcessorsConfigInformation = New ActivationProcessorProviderDataCollection
                        Else
                            activationProcessorsConfigInformation = New ActivationProcessorProviderDataCollection
                            Dim activationProcessor As Xsd.ActivationProcessorProviderData
                            For Each activationProcessor In updateManifest.ActivationProcess.Tasks
                                activationProcessorsConfigInformation.Add(New ActivationProcessorProviderData(activationProcessor.Name, activationProcessor))
                            Next activationProcessor
                        End If
                    End If
                End If
                Return activationProcessorsConfigInformation
            End Get
        End Property

#End Region

#Region "ISerializable Members"


        ' <summary>
        ' Constructor used by the serialization infrastructure.
        ' </summary>
        ' <param name="info">The serialization information.</param>
        ' <param name="context">The serialization context.</param>
        <SecurityPermission(SecurityAction.Demand, SerializationFormatter:=True)> _
        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            Dim xser As New XmlSerializer(GetType(Xsd.Manifest))
            updateManifest = CType(xser.Deserialize(New StringReader(info.GetString("_manifest"))), Xsd.Manifest)
            _apply = info.GetBoolean("_apply")

        End Sub 'New


        ' <summary>
        ' Method used by the serialization infrastructure.
        ' </summary>
        ' <param name="info">The serialization information.</param>
        ' <param name="context">The serialization context.</param>
        <SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter:=True)> _
        Public Sub GetObjectData(ByVal info As SerializationInfo, ByVal context As StreamingContext) Implements ISerializable.GetObjectData
            Dim sb As New StringBuilder(300)
            Dim sw As New StringWriter(sb, CultureInfo.InvariantCulture)
            Dim xser As New XmlSerializer(GetType(Xsd.Manifest))
            xser.Serialize(sw, updateManifest)
            info.AddValue("_manifest", sb.ToString())
            info.AddValue("_apply", _apply)

        End Sub 'GetObjectData

#End Region
    End Class 'Manifest
End Namespace
