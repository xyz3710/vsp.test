 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ClientEvents.vb
'
' Contains the information about all the events that client application will use.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports Microsoft.ApplicationBlocks.Updater

Namespace Microsoft.ApplicationBlocks.Updater

#Region "EventArgs classes"

    ' <summary>
    ' Used to notify events about the manifest.
    ' </summary>

    Public Class ManifestEventArgs
        Inherits EventArgs
        ' <summary>
        ' The manifest reference.
        ' </summary>
        Private manifestInEventsArgs As Manifest


        ' <summary>
        ' Creates an instance of a ManifestEventArgs using the Manifest instance.
        ' </summary>
        ' <param name="manifest">The manigest reference.</param>
        Public Sub New(ByVal manifest As Manifest)
            manifestInEventsArgs = manifest

        End Sub 'New

        ' <summary>
        ' The manifest reference.
        ' </summary>

        Public ReadOnly Property Manifest() As Manifest
            Get
                Return manifestInEventsArgs
            End Get
        End Property
    End Class 'ManifestEventArgs 
    ' <summary>
    ' Used to notify events about the activation completed.
    ' </summary>

    Public Class ActivationCompleteEventArgs
        Inherits ManifestEventArgs
        ' <summary>
        ' Whether the task has been sucessfully completed or not.
        ' </summary>
        Private _success As Boolean


        ' <summary>
        ' Creates an instance of the ActivationCompleteEventArgs using the mainfest reference and the success status.
        ' </summary>
        ' <param name="manifest">The manifest instance.</param>
        ' <param name="success">The success status.</param>
        Public Sub New(ByVal manifest As Manifest, ByVal success As Boolean)
            MyBase.New(manifest)
            Me._success = success

        End Sub 'New

        ' <summary>
        ' Indicates whether the update has been sucessfully activated.
        ' </summary>

        Public ReadOnly Property Success() As Boolean
            Get
                Return _success
            End Get
        End Property
    End Class 'ActivationCompleteEventArgs 
    ' <summary>
    ' Used to notify information about manifest download errors.
    ' </summary>

    Public Class ManifestErrorEventArgs
        Inherits ManifestEventArgs
        ' <summary>
        ' The exception detected.
        ' </summary>
        Private exceptionContainedInManifestErrorEventArgs As Exception


        ' <summary>
        ' Creates an instance of the ManifestErrorEventArgs using the manifest instance and the exception information.
        ' </summary>
        ' <param name="manifest">The manifest reference.</param>
        ' <param name="exception">The exception information.</param>
        Public Sub New(ByVal manifest As Manifest, ByVal exception As Exception)
            MyBase.New(manifest)
            exceptionContainedInManifestErrorEventArgs = exception

        End Sub 'New

        ' <summary>
        ' The exception detected.
        ' </summary>

        Public ReadOnly Property Exception() As Exception
            Get
                Return exceptionContainedInManifestErrorEventArgs
            End Get
        End Property
    End Class 'ManifestErrorEventArgs 
    ' <summary>
    ' Used to notify information about download progress.
    ' </summary>

    Public Class DownloadProgressEventArgs
        Inherits BaseDownloadProgressEventArgs
        ' <summary>
        ' The manifest instance.
        ' </summary>
        Private updateManifest As Manifest


        ' <summary>
        ' Creates an instance of the DownloadProgressEventArgs with the information about the progress.
        ' </summary>
        ' <param name="bytesTotal">The total bytes that will be transfered.</param>
        ' <param name="bytesTransferred">How many bytes that have been transfered.</param>
        ' <param name="filesTotal">The total bytes that will be transfered.</param>
        ' <param name="filesTransferred">How many bytes have been transfered.</param>
        ' <param name="manifest">The manifest instance.</param>
        Public Sub New(ByVal bytesTotal As Long, ByVal bytesTransferred As Long, ByVal filesTotal As Integer, ByVal filesTransferred As Integer, ByVal manifest As Manifest)
            MyBase.New(bytesTotal, bytesTransferred, filesTotal, filesTransferred)
            updateManifest = manifest

        End Sub 'New

        ' <summary>
        ' The manifest instance.
        ' </summary>

        Public ReadOnly Property Manifest() As Manifest
            Get
                Return updateManifest
            End Get
        End Property
    End Class 'DownloadProgressEventArgs 
    ' <summary>
    ' Used to notify information aobut download started.
    ' </summary>

    Public Class DownloadStartedEventArgs
        Inherits ManifestEventArgs
        ' <summary>
        ' Indicates whether the download has been canceled.
        ' </summary>
        Private _cancel As Boolean


        ' <summary>
        ' Creates an instance of DownloadStartedEventArgs using the manifest instance.
        ' </summary>
        ' <param name="manifest">The manifest instance.</param>
        Public Sub New(ByVal manifest As Manifest)
            MyBase.New(manifest)
            _cancel = False

        End Sub 'New

        ' <summary>
        ' Indicates whether the download has been canceled.
        ' </summary>

        Public Property Cancel() As Boolean
            Get
                Return _cancel
            End Get
            Set(ByVal Value As Boolean)
                _cancel = _cancel OrElse Value
            End Set
        End Property
    End Class 'DownloadStartedEventArgs 
    ' <summary>
    ' Used to notify events about pending updates in registry.
    ' </summary>

    Public Class PendingUpdatesDetectedEventArgs
        Inherits EventArgs
        ' <summary>
        ' The manifests that are pending.
        ' </summary>
        Private pendingManifests() As Manifest


        ' <summary>
        ' Creates an instance of the PendingUpdatesDetectedEventArgs using the manifest instance array.
        ' </summary>
        ' <param name="manifests">The manifest instance array.</param>
        Friend Sub New(ByVal manifests() As Manifest)
            pendingManifests = manifests

        End Sub 'New

        ' <summary>
        ' The manifests that are pending.
        ' </summary>

        Public ReadOnly Property Manifests() As Manifest()
            Get
                Return pendingManifests
            End Get
        End Property
    End Class 'PendingUpdatesDetectedEventArgs 
#End Region

#Region "ApplicationUpdater Events"


    ' <summary>
    ' Delegate for the PendingUpdatesDetected event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub PendingUpdatesDetectedEventHandler(ByVal sender As Object, ByVal e As PendingUpdatesDetectedEventArgs)

#End Region

#Region "Download Events"

    ' <summary>
    ' Delegate for the DownloadProgress event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub DownloadProgressEventHandler(ByVal sender As Object, ByVal e As DownloadProgressEventArgs)


    ' <summary>
    ' Delegate for the DownloadStarted event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub DownloadStartedEventHandler(ByVal sender As Object, ByVal e As DownloadStartedEventArgs)


    ' <summary>
    ' Delegate for the DownloadCompleted event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub DownloadCompletedEventHandler(ByVal sender As Object, ByVal e As ManifestEventArgs)


    ' <summary>
    ' Delegate for the DownloadError event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub DownloadErrorEventHandler(ByVal sender As Object, ByVal e As ManifestErrorEventArgs)

#End Region

#Region "Activation Events"


    ' <summary>
    ' Delegate for the ActivationInitializing event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub ActivationInitializingEventHandler(ByVal sender As Object, ByVal e As ManifestEventArgs)


    ' <summary>
    ' Delegate for the ActivationInitializationAborted event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub ActivationInitializationAbortedEventHandler(ByVal sender As Object, ByVal e As ManifestEventArgs)


    ' <summary>
    ' Delegate for the ActivationStarted event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub ActivationStartedEventHandler(ByVal sender As Object, ByVal e As ManifestEventArgs)


    ' <summary>
    ' Delegate for the ActivationCompleted event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub ActivationCompletedEventHandler(ByVal sender As Object, ByVal e As ActivationCompleteEventArgs)


    ' <summary>
    ' Delegate for the ActivationError event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub ActivationErrorEventHandler(ByVal sender As Object, ByVal e As ManifestErrorEventArgs)

#End Region

End Namespace
