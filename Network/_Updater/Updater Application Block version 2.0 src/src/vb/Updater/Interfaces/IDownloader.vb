 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' IDownloader.vb
'
' Contains the definition if the IDownloader interface.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports Microsoft.Practices.EnterpriseLibrary.Configuration

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Defines the contract that all downloaders must implement
    ' to be used as an updater downloader.
    ' </summary>

    Public Interface IDownloader
        Inherits IConfigurationProvider
        ' <summary>
        ' Performs the syncronous download of the files specified on the manifest.
        ' </summary>
        ' <param name="task">The associated <see cref="UpdaterTask"/> that holds a reference to the manifest to process</param>
        ' <param name="maxWaitTime">A time span indicating the maximum period of time to wait for a download. 
        Sub Download(ByVal task As UpdaterTask, ByVal maxWaitTime As TimeSpan)

        ' <summary>
        ' Initiates the asynchronous download of the files specified on the manifests.
        ' </summary>
        ' <param name="task">The associated <see cref="UpdaterTask"/> that holds a reference to the manifest to process.</param>
        Sub BeginDownload(ByVal task As UpdaterTask)

        ' <summary>
        ' Terminates o cancels an unfinifhed asyncronous download.
        ' </summary>
        ' <param name="task">The associated <see cref="UpdaterTask"/> that holds a reference to the manifest to process</param>
        ' <returns>Returns true if the task was canceled.</returns>
        Function CancelDownload(ByVal task As UpdaterTask) As Boolean

        ' <summary>
        ' Informs the download progress for the task.
        ' </summary>
        Event DownloadProgress As DownloadTaskProgressEventHandler

        ' <summary>
        ' Notifies that the downloading for an UpdaterTask has started
        ' </summary>
        Event DownloadStarted As DownloadTaskStartedEventHandler


        ' <summary>
        ' Notifies that the downloading for an UpdaterTask has finished
        ' </summary>
        Event DownloadCompleted As DownloadTaskCompletedEventHandler

        ' <summary>
        ' Notifies that an error ocurred while downloading the files for an UpdaterTask.
        ' </summary>
        Event DownloadError As DownloadTaskErrorEventHandler
    End Interface 'IDownloader 
End Namespace
