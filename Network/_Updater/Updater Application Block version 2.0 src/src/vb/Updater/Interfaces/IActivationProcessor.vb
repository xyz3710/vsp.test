 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' IActivationProcessor.vb
'
' Contains the definition if the IActivationProcessor interface.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports Microsoft.ApplicationBlocks.Updater.Configuration

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Defines the contract all activation processors must implement
    ' to participate in the activation process.
    ' </summary>

    Public Interface IActivationProcessor
        ' <summary>
        ' Performs the processor initialization. This method is called immediately
        ' after the activation processor creation.
        ' </summary>
        ' <param name="data">The configuration data specified for the processor in the manifest.</param>
        ' <param name="task">The UpdaterTask associated with this activation</param>
        Sub Init(ByVal data As ActivationProcessorProviderData, ByVal task As UpdaterTask)

        ' <summary>
        ' Called to signal the initial activation phase. On this phase the processor
        ' must ensure that it can execute without problems. It it cannot, it should
        ' raise an exception signaling and describing the problem.
        ' </summary>
        Sub PrepareExecution()

        ' <summary>
        ' Actually perform the activation processor tasks.
        ' </summary>
        Sub Execute()

        ' <summary>
        ' Called when one of the activation processor on the activation chain failed.
        ' </summary>
        Sub OnError()
    End Interface 'IActivationProcessor
End Namespace