 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ApplicationUpdaterManager.vb
'
' Contains the implementation of the public API for Updater.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Collections
Imports System.Configuration
Imports System.IO
Imports System.Threading
Imports Microsoft.ApplicationBlocks.Updater.Activator
Imports Microsoft.ApplicationBlocks.Updater.Configuration
Imports Microsoft.ApplicationBlocks.Updater.Downloader
Imports Microsoft.ApplicationBlocks.Updater.Utilities


Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Public class that client application uses to operate with the updater.
    ' </summary>

    Public Class ApplicationUpdaterManager
#Region "Private fields"

        ' <summary>
        ' The activation manager instance.
        ' </summary>
        Private activationManagerInstance As ActivationManager

        ' <summary>
        ' The download manager instance.
        ' </summary>
        Private downloadManagerInstance As DownloadManager

        ' <summary>
        ' The manifest provider to use.
        ' </summary>
        Private Shared manifestManagerInstance As New ManifestManager

        ' <summary>
        ' The list of created updaters.
        ' </summary>
        Private Shared updaters As New Hashtable

        ' <summary>
        ' Key in the task context.
        ' </summary>
        Private Const AsyncTaskKey As String = "AsyncTask"

        ' <summary>
        ' The last time the CheckForUpdates was called.
        ' </summary>
        Private _lastCheck As DateTime = DateTime.MinValue

        ' <summary>
        ' The applicationID the updater instance is for.
        ' </summary>
        Private applicationIdOfApplicationToUpdate As String

        ' <summary>
        ' Control whether the updater instance is hooked to the DownloaderManager and ActivationManager events.
        ' </summary>
        Private eventsHooked As Boolean

        ' <summary>
        ' Flag indicating if the client has resumed the pending updates
        ' </summary>
        Private pendingUpdatesResumed As Boolean = False

        ' <summary>
        ' Maximum time to wait when resuming a pending update
        ' </summary>
        Private downloadResumeMaxWaitTime As TimeSpan = TimeSpan.FromMinutes(10)

#End Region

#Region "Events"

        ' <summary>
        ' Notifies that there are pending updates.
        ' </summary>
        Public Event PendingUpdatesDetected As PendingUpdatesDetectedEventHandler

        ' <summary>
        ' Notifies download started.
        ' </summary>
        Public Event DownloadStarted As DownloadStartedEventHandler

        ' <summary>
        ' Notifies download progress.
        ' </summary>
        Public Event DownloadProgress As DownloadProgressEventHandler

        ' <summary>
        ' Notifies download errors.
        ' </summary>
        Public Event DownloadError As DownloadErrorEventHandler

        ' <summary>
        ' Notifies that download is complete.
        ' </summary>
        Public Event DownloadCompleted As DownloadCompletedEventHandler

        ' <summary>
        ' Notifies that activation is initializing.
        ' </summary>
        Public Event ActivationInitializing As ActivationInitializingEventHandler

        ' <summary>
        ' Notifies that the activation initialization is aborted.
        ' </summary>
        Public Event ActivationInitializationAborted As ActivationInitializationAbortedEventHandler

        ' <summary>
        ' Notifies that the activation is started.
        ' </summary>
        Public Event ActivationStarted As ActivationStartedEventHandler

        ' <summary>
        ' Notifies that activation is complete.
        ' </summary>
        Public Event ActivationCompleted As ActivationCompletedEventHandler

        ' <summary>
        ' Notifies that an activation error ocurred.
        ' </summary>
        Public Event ActivationError As ActivationErrorEventHandler

#End Region

#Region "Constructors"


        ' <summary>
        ' Constructor used by a non Windows service app.
        ' </summary>
        ' <param name="applicationId">Id of the application being updated.</param>
        Private Sub New(ByVal applicationId As String)
            applicationIdOfApplicationToUpdate = applicationId

            activationManagerInstance = New ActivationManager
            downloadManagerInstance = New DownloadManager

            Me.HookUpEvents()

        End Sub 'New


        ' <summary>
        ' Release this instance of the updater, and unregister the event handlers.
        ' </summary>
        Protected Overloads Overrides Sub Finalize()
            SyncLock updaters.SyncRoot
                updaters.Remove(applicationIdOfApplicationToUpdate)
            End SyncLock
            UnHookEvents()
            MyBase.Finalize()

        End Sub 'Finalize


#End Region

#Region "Public Properties"

        ' <summary>
        ' Gets the last time CheckForUpdates() was called.
        ' </summary>

        Public ReadOnly Property LastCheck() As DateTime
            Get
                Return _lastCheck
            End Get
        End Property

#End Region

#Region "Registry Management Methods"


        ' <summary>
        ' Returns the list of registered tasks from the RegistryManager.
        ' </summary>
        ' <returns>An array of UpdaterTasks.</returns>
        Public Shared Function GetTasks() As UpdaterTask()
            Return RegistryManager.Current.GetTasks()

        End Function 'GetTasks

#End Region

#Region "Static Members"

        ' <summary>
        ' The manifest manager instance.
        ' </summary>

        Private Shared ReadOnly Property ManifestManager() As ManifestManager
            Get
                Return manifestManagerInstance
            End Get
        End Property

#End Region

#Region "Factory implementation"


        ' <summary>
        ' Returns an <see cref="ApplicationUpdaterManager"/> instance for the configured application. 
        ' The client application will manage its updates through this instance.
        ' Can be used by both Windows service and non-Windows service clients for getting the <see cref="ApplicationUpdaterManager"/>.
        ' </summary>
        ' <returns>An ApplicationUpdaterManager instance.</returns>
        Public Overloads Shared Function GetUpdater() As ApplicationUpdaterManager
            If (New UpdaterConfigurationView().ApplicationId Is Nothing) OrElse (Not (New UpdaterConfigurationView().ApplicationId Is Nothing) AndAlso New UpdaterConfigurationView().ApplicationId.Length = 0) Then
                Logger.LogAndThrowException(New ConfigurationException(Resource.ResourceManager(Resource.MessageKey.NoConfigurationToGetApplicationId)))
            End If
            Return ApplicationUpdaterManager.GetUpdater(New UpdaterConfigurationView().ApplicationId)

        End Function 'GetUpdater


        ' <summary>
        ' Returns an <see cref="ApplicationUpdaterManager"/> instance for the specified application. 
        ' The client application will manage its updates through this instance. 
        ' </summary>
        ' <param name="applicationId">The ID for an application updates will be performed for.</param>
        ' <returns>An ApplicationUpdaterManager instance.</returns>
        Public Overloads Shared Function GetUpdater(ByVal applicationId As String) As ApplicationUpdaterManager
            If (applicationId Is Nothing) OrElse (Not applicationId Is Nothing AndAlso applicationId.Length = 0) Then
                Logger.LogAndThrowException(New ArgumentException(Resource.ResourceManager(Resource.MessageKey.ApplicationIdCannotBeNullOrEmpty), "applicationId"))
            End If

            If updaters.ContainsKey(applicationId) Then
                Return CType(updaters(applicationId), ApplicationUpdaterManager)
            End If

            SyncLock updaters.SyncRoot
                If updaters.ContainsKey(applicationId) Then
                    Return CType(updaters(applicationId), ApplicationUpdaterManager)
                End If
                Dim instance As New ApplicationUpdaterManager(applicationId)
                updaters.Add(applicationId, instance)
                Return instance
            End SyncLock

        End Function 'GetUpdater

#End Region

#Region "Public Methods"


        ' <summary>
        ' Returns a list of available update manifests for the application from the default manifest URI location.
        ' </summary>
        ' <returns>The list of available manifests to apply.</returns>
        Public Overloads Function CheckForUpdates() As Manifest()
            Return CheckForUpdates(New UpdaterConfigurationView().DefaultManifestUriLocation)

        End Function 'CheckForUpdates


        ' <summary>
        ' Returns a list of available update manifests for the application from the specified manifest URI location.
        ' </summary>
        ' <param name="location">The target URI to get the manifest from.</param>
        ' <returns>The list of available manifests to apply.</returns>
        Public Overloads Function CheckForUpdates(ByVal location As Uri) As Manifest()
            If location Is Nothing OrElse (Not (location Is Nothing) AndAlso location.AbsoluteUri.Length = 0) Then
                Logger.LogAndThrowException(New ArgumentException(Resource.ResourceManager(Resource.MessageKey.LocationCannotBeNullOrEmpty), "location"))
            End If

            ' Checks for pending updates and notifies the client
            CheckForPendingUpdates()

            ' If the client didn't resume the pending updates, clear them
            ' to allow the change of getting a fixed/renewed manifest
            If Not pendingUpdatesResumed Then
                CancelPendingUpdates() ' If there are no pending updates this method does nothing
            End If

            _lastCheck = DateTime.UtcNow
            Return ApplicationUpdaterManager.ManifestManager.GetManifests(location, applicationIdOfApplicationToUpdate)

        End Function 'CheckForUpdates


        ' <summary>
        ' Continues with any pending updater task the application didn't complete 
        ' in a previous session.
        ' </summary>
        Public Sub ResumePendingUpdates()
            ' If there are pending tasks for this application, resume them
            ' accordingly to their state
            Dim task As UpdaterTask
            For Each task In RegistryManager.Current.GetByApplicationId(applicationIdOfApplicationToUpdate)
                Select Case task.State
                    Case UpdaterTaskState.DownloadError, UpdaterTaskState.Downloading
                        downloadManagerInstance.SubmitTask(task, downloadResumeMaxWaitTime)
                    Case UpdaterTaskState.Downloaded, UpdaterTaskState.Activating, UpdaterTaskState.ActivationInitializing, UpdaterTaskState.ActivationInitializationAborted, UpdaterTaskState.ActivationError
                        activationManagerInstance.SubmitTask(task)
                        If task.State = UpdaterTaskState.Activated Then
                            ' Finally, unregister the task
                            RegistryManager.Current.UnRegisterTask(task.TaskId)
                        End If
                End Select
            Next task

            pendingUpdatesResumed = True

        End Sub 'ResumePendingUpdates



        ' <summary>
        ' Cancels pending updates.
        ' </summary>
        Public Sub CancelPendingUpdates()
            Dim pendingTasks As UpdaterTask() = RegistryManager.Current.GetByApplicationId(applicationIdOfApplicationToUpdate)
            Dim task As UpdaterTask
            For Each task In pendingTasks
                RegistryManager.Current.UnRegisterTask(task.TaskId)
                FileUtility.DestroyFolder(task.DownloadFilesBase)
            Next task

        End Sub 'CancelPendingUpdates


        ' <summary>
        ' Synchronously downloads the specified files in selected manifests.
        ' </summary>
        ' <param name="selectedManifests">The list of manifests to process.</param>
        ' <param name="maxWaitTime">The maximum time given the downloader to take.</param>
        Public Sub Download(ByVal selectedManifests() As Manifest, ByVal maxWaitTime As TimeSpan)
            Dim manifest As manifest
            For Each manifest In selectedManifests
                If manifest.Apply Then
                    Dim task As New UpdaterTask(manifest)
                    RegistryManager.Current.RegisterTask(task)
                    task(AsyncTaskKey) = False
                    downloadManagerInstance.SubmitTask(task, maxWaitTime)
                End If
            Next manifest

        End Sub 'Download


        ' <summary>
        ' Asynchronously begins a download of the files specified in selected manifests.
        ' </summary>
        ' <param name="selectedManifests">The list of Manifest to process.</param>
        Public Sub BeginDownload(ByVal selectedManifests() As Manifest)
            Dim manifest As manifest
            For Each manifest In selectedManifests
                If manifest.Apply Then
                    Dim task As New UpdaterTask(manifest)
                    RegistryManager.Current.RegisterTask(task)
                    task(AsyncTaskKey) = True
                    downloadManagerInstance.SubmitTaskAsync(task)
                End If
            Next manifest

        End Sub 'BeginDownload


        ' <summary>
        ' Cancels the asynchronous download operation associated with the specified manifest. 
        ' </summary>
        ' <param name="manifest">The manifest instance to cancel.</param>
        Public Sub CancelDownload(ByVal manifest As Manifest)
            Dim task As UpdaterTask = RegistryManager.Current.GetByManifestID(manifest.ManifestId)
            If Not (task Is Nothing) Then
                Select Case task.State
                    Case UpdaterTaskState.None
                        RegistryManager.Current.UnRegisterTask(task.TaskId)
                    Case UpdaterTaskState.DownloadError, UpdaterTaskState.Downloading
                        downloadManagerInstance.EndTask(task)
                        RegistryManager.Current.UnRegisterTask(task.TaskId)
                    Case UpdaterTaskState.Downloaded, UpdaterTaskState.ActivationInitializing, UpdaterTaskState.ActivationInitializationAborted
                        RegistryManager.Current.UnRegisterTask(task.TaskId)
                    Case UpdaterTaskState.Activating, UpdaterTaskState.ActivationError
                        Logger.LogAndThrowException(New InvalidOperationException(Resource.ResourceManager(Resource.MessageKey.UpdaterManagerUpdateCannotBeCanceled)))
                End Select

                SyncLock task.SyncRoot
                    task.State = UpdaterTaskState.Cancelled
                End SyncLock
            End If

        End Sub 'CancelDownload



        ' <summary>
        ' Initiates the activation for the selected manifests.
        ' </summary>
        ' <param name="selectedManifests">The list of manifests describin the updates to apply.</param>
        Public Sub Activate(ByVal selectedManifests() As Manifest)
            Dim manifest As manifest
            For Each manifest In selectedManifests
                If manifest.Apply Then
                    ' Get the task associated with the manifest
                    Dim task As UpdaterTask = GetManifestActivationTask(manifest)

                    ' Start the activation process
                    activationManagerInstance.SubmitTask(task)

                    If task.State = UpdaterTaskState.Activated Then
                        ' Finally, unregister the task
                        RegistryManager.Current.UnRegisterTask(task.TaskId)
                    End If
                End If
            Next manifest

        End Sub 'Activate

#End Region

#Region "Event subscription, handling and forwarding methods"


        ' <summary>
        ' Registers the events of the downloader and the activator manager.
        ' </summary>
        Private Sub HookUpEvents()
            AddHandler downloadManagerInstance.DownloadCompleted, AddressOf OnDownloadCompleted
            AddHandler downloadManagerInstance.DownloadStarted, AddressOf OnDownloadStarted
            AddHandler downloadManagerInstance.DownloadProgress, AddressOf OnDownloadProgress
            AddHandler downloadManagerInstance.DownloadError, AddressOf OnDownloadError

            AddHandler activationManagerInstance.ActivationInitializationAborted, AddressOf OnActivationInitializationAborted
            AddHandler activationManagerInstance.ActivationError, AddressOf OnActivationError
            AddHandler activationManagerInstance.ActivationInitializing, AddressOf OnActivationInitializing
            AddHandler activationManagerInstance.ActivationStarted, AddressOf OnActivationStarted
            AddHandler activationManagerInstance.ActivationCompleted, AddressOf OnActivationCompleted

            eventsHooked = True

        End Sub 'HookUpEvents


        ' <summary>
        ' Unregisters the events of the downloader and the activator manager.
        ' </summary>
        Private Sub UnHookEvents()
            If eventsHooked Then
                RemoveHandler downloadManagerInstance.DownloadCompleted, AddressOf OnDownloadCompleted
                RemoveHandler downloadManagerInstance.DownloadStarted, AddressOf OnDownloadStarted
                RemoveHandler downloadManagerInstance.DownloadProgress, AddressOf OnDownloadProgress
                RemoveHandler downloadManagerInstance.DownloadError, AddressOf OnDownloadError

                RemoveHandler activationManagerInstance.ActivationInitializationAborted, AddressOf OnActivationInitializationAborted
                RemoveHandler activationManagerInstance.ActivationError, AddressOf OnActivationError
                RemoveHandler activationManagerInstance.ActivationInitializing, AddressOf OnActivationInitializing
                RemoveHandler activationManagerInstance.ActivationStarted, AddressOf OnActivationStarted
                RemoveHandler activationManagerInstance.ActivationCompleted, AddressOf OnActivationCompleted
                eventsHooked = False
            End If

        End Sub 'UnHookEvents


        ' <summary>
        ' Method used to handle the event.
        ' </summary>
        ' <param name="tasks">The UpdaterTask instance array.</param>
        Private Sub OnPendingUpdatesDetected(ByVal tasks() As UpdaterTask)
            'If Not (PendingUpdatesDetected Is Nothing) Then
            ' Notify the client pending tasks has been found
            Dim manifests(tasks.Length) As Manifest
            Dim i As Integer

            While i < tasks.Length
                manifests(i) = tasks(i).Manifest
                i += 1
            End While
            Dim args As New PendingUpdatesDetectedEventArgs(manifests)
            RaiseEvent PendingUpdatesDetected(Me, args)

        End Sub 'OnPendingUpdatesDetected


        ' <summary>
        ' Method used to handle the event.
        ' </summary>
        ' <param name="sender">The sender of the event.</param>
        ' <param name="e">The task information.</param>
        Private Sub OnDownloadCompleted(ByVal sender As Object, ByVal e As TaskEventArgs)
            RegistryManager.Current.UpdateTask(e.Task)
            RaiseEvent DownloadCompleted(Me, New ManifestEventArgs(e.Task.Manifest))

        End Sub 'OnDownloadCompleted


        ' <summary>
        ' Method used to handle the event.
        ' </summary>
        ' <param name="sender">The sender of the event.</param>
        ' <param name="e">The download error information.</param>
        Private Sub OnDownloadError(ByVal sender As Object, ByVal e As DownloadTaskErrorEventArgs)
            RegistryManager.Current.UpdateTask(e.Task)
            RaiseEvent DownloadError(Me, New ManifestErrorEventArgs(e.Task.Manifest, e.Exception))

        End Sub 'OnDownloadError


        ' <summary>
        ' Method used to handle the event.
        ' </summary>
        ' <param name="sender">The sender of the event.</param>
        ' <param name="e">The download progress task information.</param>
        Private Sub OnDownloadProgress(ByVal sender As Object, ByVal e As DownloadTaskProgressEventArgs)
            Dim eventArgs As New DownloadProgressEventArgs(e.BytesTotal, e.BytesTransferred, e.FilesTotal, e.FilesTransferred, e.Task.Manifest)
            RaiseEvent DownloadProgress(Me, eventArgs)
            If eventArgs.Cancel Then
                CancelDownload(e.Task.Manifest)
            End If

        End Sub 'OnDownloadProgress


        ' <summary>
        ' Method used to handle the event.
        ' </summary>
        ' <param name="sender">The sender of the event.</param>
        ' <param name="e">The task information.</param>
        Private Sub OnDownloadStarted(ByVal sender As Object, ByVal e As TaskEventArgs)
            RegistryManager.Current.UpdateTask(e.Task)
            'If Not (DownloadStarted Is Nothing) Then
            Dim eventArgs As New DownloadStartedEventArgs(e.Task.Manifest)
            RaiseEvent DownloadStarted(Me, eventArgs)
            If eventArgs.Cancel Then
                CancelDownload(eventArgs.Manifest)
            End If

        End Sub 'OnDownloadStarted


        ' <summary>
        ' Method used to handle the event.
        ' </summary>
        ' <param name="sender">The sender of the event.</param>
        ' <param name="e">The activatin completed information.</param>
        Private Sub OnActivationCompleted(ByVal sender As Object, ByVal e As ActivationTaskCompleteEventArgs)
            Dim task As UpdaterTask = e.Task

            If e.Success Then
                ' Clean up temporary files
                FileUtility.DestroyFolder(task.DownloadFilesBase)

                ' Tell the ManifestManager we are done with this update
                ApplicationUpdaterManager.ManifestManager.CommitUpdate(task.Manifest)
            End If

            RaiseEvent ActivationCompleted(Me, New ActivationCompleteEventArgs(task.Manifest, e.Success))

        End Sub 'OnActivationCompleted


        ' <summary>
        ' Method used to handle the event.
        ' </summary>
        ' <param name="sender">The sende of the event.</param>
        ' <param name="e">The activation task error information.</param>
        Private Sub OnActivationInitializationAborted(ByVal sender As Object, ByVal e As ActivationTaskErrorEventArgs)
            RegistryManager.Current.UpdateTask(e.Task)
            RaiseEvent ActivationInitializationAborted(Me, New ManifestEventArgs(e.Task.Manifest))

        End Sub 'OnActivationInitializationAborted


        ' <summary>
        ' Method used to handle the event.
        ' </summary>
        ' <param name="sender">The sender of the event.</param>
        ' <param name="e">The activation error information.</param>
        Private Sub OnActivationError(ByVal sender As Object, ByVal e As ActivationTaskErrorEventArgs)
            RegistryManager.Current.UpdateTask(e.Task)
            RaiseEvent ActivationError(Me, New ManifestErrorEventArgs(e.Task.Manifest, e.Exception))

        End Sub 'OnActivationError


        ' <summary>
        ' Method used to handle the event.
        ' </summary>
        ' <param name="sender">The sender of the event.</param>
        ' <param name="e">Tehe task informaion.</param>
        Private Sub OnActivationInitializing(ByVal sender As Object, ByVal e As TaskEventArgs)
            RegistryManager.Current.UpdateTask(e.Task)
            RaiseEvent ActivationInitializing(Me, New ManifestEventArgs(e.Task.Manifest))

        End Sub 'OnActivationInitializing


        ' <summary>
        ' Method used to handle the event.
        ' </summary>
        ' <param name="sender">The sender of the event.</param>
        ' <param name="e">The task information.</param>
        Private Sub OnActivationStarted(ByVal sender As Object, ByVal e As TaskEventArgs)
            RegistryManager.Current.UpdateTask(e.Task)
            RaiseEvent ActivationStarted(Me, New ManifestEventArgs(e.Task.Manifest))

        End Sub 'OnActivationStarted

#End Region

#Region "Private members"


        ' <summary>
        ' Check for pending updates in the registry manager.
        ' </summary>
        Private Sub CheckForPendingUpdates()
            Dim pendingTasks As UpdaterTask() = RegistryManager.Current.GetByApplicationId(applicationIdOfApplicationToUpdate)
            If pendingTasks.Length > 0 Then
                OnPendingUpdatesDetected(pendingTasks)
            End If

        End Sub 'CheckForPendingUpdates


        ' <summary>
        ' Return the UpdaterTask for a Manifest and validates the it's state.
        ' </summary>
        ' <param name="manifest">The manifest instance.</param>
        ' <returns>The UpdateTask instance.</returns>
        Private Function GetManifestActivationTask(ByVal manifest As Manifest) As UpdaterTask
            Dim task As UpdaterTask = RegistryManager.Current.GetByManifestID(manifest.ManifestId)
            If task Is Nothing Then
                Logger.LogAndThrowException(New ApplicationUpdaterException(Resource.ResourceManager(Resource.MessageKey.CannotFindActivationTaskForManifest, manifest.ManifestId)))
            End If
            ' Get the task to activate if the state is Downloaded, 
            ' ActivationInitializationAborted and ActivationError 
            If task.State = UpdaterTaskState.Downloaded OrElse task.State = UpdaterTaskState.ActivationInitializationAborted OrElse task.State = UpdaterTaskState.ActivationError Then
                Return task
            Else
                Logger.LogAndThrowException(New ApplicationUpdaterException(Resource.ResourceManager(Resource.MessageKey.ActivationTaskForManifestInInvalidState, manifest.ManifestId, task.State)))
            End If
            Return task

        End Function 'GetManifestActivationTask

#End Region
    End Class 'ApplicationUpdaterManager
End Namespace