 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' DownloadManager.vb
'
' Contains the implementation of the downloader manager.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Reflection
Imports Microsoft.ApplicationBlocks.Updater.Configuration
Imports Microsoft.ApplicationBlocks.Updater.Utilities

Namespace Microsoft.ApplicationBlocks.Updater.Downloader
    ' <summary>
    ' Performs the download operations by instancing and managing the configured downloader.
    ' Also channels and raises the downloader events.
    ' </summary>

    Friend NotInheritable Class DownloadManager
#Region "Constructors "


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "Public methods"


        ' <summary>
        ' Synchronously submits a task to the downloader.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <param name="maxWaitTime">The maximum wait time for the task.</param>
        Public Sub SubmitTask(ByVal task As UpdaterTask, ByVal maxWaitTime As TimeSpan)
            ' If there's no files to download
            ' Consider the download as done
            If task.Manifest.Files.Count = 0 Then
                OnDownloadCompleted(Nothing, New TaskEventArgs(task))
                Return
            End If

            Dim downloader As IDownloader = GetDownloader(task)
            Try
                downloader.Download(task, maxWaitTime)
            Catch e As Exception
                Throw e
            Finally
                UnregisterEvents(downloader)
            End Try

        End Sub 'SubmitTask


        ' <summary>
        ' Asynchronously submits a task to the downloader.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub SubmitTaskAsync(ByVal task As UpdaterTask)
            ' If there's no files to download
            ' Consider the download as done
            If task.Manifest.Files.Count = 0 Then
                OnDownloadCompleted(Nothing, New TaskEventArgs(task))
                Return
            End If

            Dim downloader As IDownloader = GetDownloader(task)
            downloader.BeginDownload(task)

        End Sub 'SubmitTaskAsync


        ' <summary>
        ' Cancels a pending task.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <returns></returns>
        Public Function EndTask(ByVal task As UpdaterTask) As Boolean
            Dim downloader As IDownloader = GetDownloader(task)
            Try
                Return downloader.CancelDownload(task)
            Finally
                UnregisterEvents(downloader)
            End Try

        End Function 'EndTask

#End Region

#Region "Private methods"


        ' <summary>
        ' Return the downloader instance for a specific UpdaterTask.
        ' </summary>
        ' <param name="task">The UpdaterTask.</param>
        ' <returns>The Downloader instance.</returns>
        Private Function GetDownloader(ByVal task As UpdaterTask) As IDownloader
            Dim df As New DownloaderFactory
            Dim downloader As IDownloader = Nothing

            Try
                If task.Manifest.Downloader Is Nothing OrElse task.Manifest.Downloader.Name Is Nothing Then
                    downloader = df.CreateDownloader(Resource.ResourceManager(Resource.MessageKey.BitsDownloaderNode))
                Else
                    downloader = df.CreateDownloader(task.Manifest.Downloader.Name)
                End If

            Catch
                Throw
            Catch ex As Exception
                Throw New ApplicationUpdaterException(Resource.ResourceManager(Resource.MessageKey.CannotCreateDownloader), ex)
            End Try

            AddHandler downloader.DownloadStarted, AddressOf OnDownloadStarted
            AddHandler downloader.DownloadProgress, AddressOf OnDownloadProgress
            AddHandler downloader.DownloadCompleted, AddressOf OnDownloadCompleted
            AddHandler downloader.DownloadError, AddressOf OnDownloadError

            Return downloader

        End Function 'GetDownloader


        ' <summary>
        ' Returns the Downloader configuration data for the downloader type especified.
        ' </summary>
        ' <param name="downloaderType">The downloader type.</param>
        ' <returns>The downloader configuration data.</returns>
        Private Function GetApplicationConfig(ByVal downloaderType As String) As DownloadProviderData
            Dim config As DownloadProviderData
            For Each config In New UpdaterConfigurationView().Downloaders
                If config.TypeName = downloaderType Then
                    Return config
                End If
            Next config
            Return Nothing

        End Function 'GetApplicationConfig

#End Region

#Region "Event handling and forwarding"

        ' <summary>
        ' Notifies about downloader progress.
        ' </summary>
        Public Event DownloadProgress As DownloadTaskProgressEventHandler

        ' <summary>
        ' Notifies about a started downloading task.
        ' </summary>
        Public Event DownloadStarted As DownloadTaskStartedEventHandler

        ' <summary>
        ' Notifies about completion of the download tasks.
        ' </summary>
        Public Event DownloadCompleted As DownloadTaskCompletedEventHandler

        ' <summary>
        ' Notifies about errors in downloader tasks.
        ' </summary>
        Public Event DownloadError As DownloadTaskErrorEventHandler


        ' <summary>
        ' Private method used to handle the downloader event.
        ' </summary>
        ' <param name="sender">The downloader instance.</param>
        ' <param name="e">The information for the event.</param>
        Private Sub OnDownloadCompleted(ByVal sender As Object, ByVal e As TaskEventArgs)
            e.Task.State = UpdaterTaskState.Downloaded
            RaiseEvent DownloadCompleted(Me, e)

            If TypeOf sender Is IDownloader Then
                Dim downloader As IDownloader = CType(sender, IDownloader)
                UnregisterEvents(downloader)
            End If
        End Sub 'OnDownloadCompleted


        ' <summary>
        ' Private method used to handle the downloader event.
        ' </summary>
        ' <param name="sender">The downloader instance.</param>
        ' <param name="e">The information for the event.</param>
        Private Sub OnDownloadStarted(ByVal sender As Object, ByVal e As TaskEventArgs)
            e.Task.State = UpdaterTaskState.Downloading
            RaiseEvent DownloadStarted(Me, e)

        End Sub 'OnDownloadStarted


        ' <summary>
        ' Private method used to handle the downloader event.
        ' </summary>
        ' <param name="sender">The downloader instance.</param>
        ' <param name="e">The information for the event.</param>
        Private Sub OnDownloadProgress(ByVal sender As Object, ByVal e As DownloadTaskProgressEventArgs)
            RaiseEvent DownloadProgress(Me, e)

        End Sub 'OnDownloadProgress


        ' <summary>
        ' Private method used to handle the downloader event.
        ' </summary>
        ' <param name="sender">The downloader instance.</param>
        ' <param name="e">The information for the event.</param>
        Private Sub OnDownloadError(ByVal sender As Object, ByVal e As DownloadTaskErrorEventArgs)
            e.Task.State = UpdaterTaskState.DownloadError
            RaiseEvent DownloadError(Me, e)
            Dim downloader As IDownloader = CType(sender, IDownloader)
            UnregisterEvents(downloader)

        End Sub 'OnDownloadError

        '/ <summary>
        '/ Method to unregister Downloader events.
        '/ </summary>
        '/ <param name="downloader">
        '/ Downloader instance to unregister events from.
        '/ </param>
        Private Sub UnregisterEvents(ByVal downloader As IDownloader)
            RemoveHandler downloader.DownloadStarted, AddressOf OnDownloadStarted
            RemoveHandler downloader.DownloadProgress, AddressOf OnDownloadProgress
            RemoveHandler downloader.DownloadCompleted, AddressOf OnDownloadCompleted
            RemoveHandler downloader.DownloadError, AddressOf OnDownloadError
        End Sub 'UnregisterEvents

#End Region
    End Class 'DownloadManager
End Namespace