 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' RegistryManager.vb
'
' Contains the implementation of the registry manager.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Collections
Imports System.Globalization
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Security.Permissions
Imports Microsoft.ApplicationBlocks.Updater.Configuration
Imports Microsoft.ApplicationBlocks.Updater.Utilities

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Manages the list of tasks under progress, from its initial registration when they
    ' are submitted to download, to their final state when they are successfully activated
    ' or cancelled.
    ' </summary>
    <Serializable()> _
                   Friend NotInheritable Class RegistryManager
#Region "Private fields"

        ' <summary>
        ' The singleton instance stored.
        ' </summary>
        Private Shared instance As New RegistryManager

        ' <summary>
        ' Root folder name for the registry.
        ' </summary>
        Private Const root As String = "registry"

        ' <summary>
        ' Helper class for directory information.
        ' </summary>
        Private rootDir As DirectoryInfo

        ' <summary>
        ' The in memory registry storage.
        ' </summary>
        Private registry As New Hashtable(10)

        ' <summary>
        ' Indicates if the list of tasks is loaded
        ' </summary>
        Private loaded As Boolean = False

#End Region

#Region "Singleton implementation"

        ' <summary>
        ' Singleton instance.
        ' </summary>

        Public Shared ReadOnly Property Current() As RegistryManager
            Get
                Return instance
            End Get
        End Property

        ' <summary>
        ' Default constructor disable because there is a singleton implementation.
        ' </summary>
        Private Sub New()
            Dim _path As String = Path.Combine(New UpdaterConfigurationView().BasePath, root)
            If Not Directory.Exists(_path) Then
                rootDir = Directory.CreateDirectory(_path)
            Else
                rootDir = New DirectoryInfo(_path)
            End If

        End Sub 'New

#End Region

#Region "Public Methods"


        ' <summary>
        ' Loads all the pending tasks.
        ' </summary>
        Public Sub Load()
            Dim fi As FileInfo
            For Each fi In rootDir.GetFiles()
                Me.LoadTask(fi.FullName)
            Next fi

        End Sub 'Load


        ' <summary>
        ' Updates the information of an existing stored task.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub UpdateTask(ByVal task As UpdaterTask)
            SaveTask(task)

        End Sub 'UpdateTask


        ' <summary>
        ' Registers the task in the storage.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub RegisterTask(ByVal task As UpdaterTask)
            SyncLock Tasks.SyncRoot
                If Not Tasks.ContainsKey(task.TaskId) Then
                    Tasks.Add(task.TaskId, task)
                End If
            End SyncLock
            SaveTask(task)

        End Sub 'RegisterTask


        ' <summary>
        ' Removes the task from the storage.
        ' </summary>
        ' <param name="taskId">The task id.</param>
        Public Sub UnRegisterTask(ByVal taskId As Guid)
            SyncLock Tasks.SyncRoot
                Tasks.Remove(taskId)
            End SyncLock
            Dim fileName As String = Path.Combine(rootDir.FullName, taskId.ToString() + ".task")
            FileUtility.DestroyFile(fileName)

        End Sub 'UnRegisterTask


        ' <summary>
        ' Return all the tasks stored in memory.
        ' </summary>
        ' <returns>An array of UpdaterTask instances.</returns>
        Public Function GetTasks() As UpdaterTask()
            Dim result(Tasks.Count - 1) As UpdaterTask
            Tasks.Values.CopyTo(result, 0)
            Return result

        End Function 'GetTasks


        ' <summary>
        ' Returns all the stored tasks by a given application id.
        ' </summary>
        ' <param name="applicationId">The application id.</param>
        ' <returns>An array of UpdaterTask instances.</returns>
        Public Function GetByApplicationId(ByVal applicationId As String) As UpdaterTask()
            Dim _tasks As New ArrayList
            SyncLock Tasks.SyncRoot
                Dim task As UpdaterTask
                For Each task In Tasks.Values
                    If task.Manifest.Application.ApplicationId = applicationId Then
                        _tasks.Add(task)
                    End If
                Next task
            End SyncLock
            Return CType(_tasks.ToArray(GetType(UpdaterTask)), UpdaterTask())
        End Function 'GetByApplicationId


        ' <summary>
        ' Return the UpdaterTask for a given manifest id.
        ' </summary>
        ' <param name="manifestId">The manifest id.</param>
        ' <returns>An UpdaterTask instance.</returns>
        Public Function GetByManifestID(ByVal manifestId As Guid) As UpdaterTask
            SyncLock Tasks.SyncRoot
                Dim task As UpdaterTask
                For Each task In Tasks.Values
                    If task.Manifest.ManifestId.CompareTo(manifestId) = 0 Then
                        Return task
                    End If
                Next task
            End SyncLock
            Return Nothing

        End Function 'GetByManifestID

#End Region

#Region "Private Methods"


        ' <summary>
        ' Load the tasks stored in a specified path.
        ' </summary>
        ' <param name="taskFilePath">The base path for the registry storage.</param>
        ' <returns>An UpdaterTask instance.</returns>
        <SecurityPermission(SecurityAction.Demand, SerializationFormatter:=True)> _
        Private Function LoadTask(ByVal taskFilePath As String) As UpdaterTask
            Dim task As UpdaterTask = Nothing
            Dim formatter As New BinaryFormatter
            Dim stream As New FileStream(taskFilePath, FileMode.Open, FileAccess.Read, FileShare.Read)
            Try
                task = CType(formatter.Deserialize(stream), UpdaterTask)
                SyncLock registry.SyncRoot
                    If task.State <> UpdaterTaskState.Activated AndAlso Not registry.ContainsKey(task.TaskId) Then
                        registry.Add(task.TaskId, task)
                    End If
                End SyncLock
            Finally
                stream.Close()
                stream = Nothing
            End Try
            Return task

        End Function 'LoadTask


        ' <summary>
        ' Stores a task in the registry storage.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        <SecurityPermission(SecurityAction.Demand, SerializationFormatter:=True)> _
        Private Sub SaveTask(ByVal task As UpdaterTask)
            Dim filename As String = Path.Combine(rootDir.FullName, String.Format(CultureInfo.InvariantCulture, "{0}.task", task.TaskId.ToString()))
            Try
                Dim stream = New FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read)
                Try
                    Dim formatter As New BinaryFormatter
                    formatter.Serialize(stream, task)
                Finally
                    stream.Flush()
                    stream.Close()
                End Try
            Catch ex As Exception
                File.Delete(filename)
                Logger.LogAndThrowException(ex)
            End Try

        End Sub 'SaveTask


        ' <summary>
        ' Gets the list of registered tasks, ensuring the list is loaded
        ' </summary>

        Private ReadOnly Property Tasks() As Hashtable
            Get
                If Not loaded Then
                    Load()
                    loaded = True
                End If
                Return registry
            End Get
        End Property

#End Region
    End Class 'RegistryManager 
End Namespace
