 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ActivationPausedException.vb
'
' This class is used for notification of a pause during the execution of an activator.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Runtime.Serialization
Imports System.Security.Permissions

Namespace Microsoft.ApplicationBlocks.Updater.Activator
    ' <summary>
    ' Exception thrown by activatoris when the activation process should be paused.
    ' </summary>
    <Serializable()> _
    Public Class ActivationPausedException
        Inherits Exception
#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()
            MyBase.New(Resource.ResourceManager(Resource.MessageKey.ActivationPausedMessage))

        End Sub 'New


        ' <summary>
        ' Creates an ActivationPausedException with a specified message.
        ' </summary>
        ' <param name="message">The exception message string.</param>
        Public Sub New(ByVal message As String)
            MyBase.New(message)

        End Sub 'New


        ' <summary>
        ' Creates an ActivationPausedException with a specified message.
        ' </summary>
        ' <param name="message">The exception message string.</param>
        ' <param name="innerException">The inner exception detected.</param>
        Public Sub New(ByVal message As String, ByVal innerException As Exception)
            MyBase.New(message, innerException)

        End Sub 'New


        ' <summary>
        ' Constructor used by the serialization infrastructure.
        ' </summary>
        ' <param name="info">The serialization information for the object.</param>
        ' <param name="context">The context for the serialization.</param>
        <SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter:=True)> _
        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)

        End Sub 'New

#End Region

#Region "Public members"


        ' <summary>
        ' Used by the serialization infrastructure.
        ' </summary>
        ' <param name="info">The serialization information.</param>
        ' <param name="context">The serialization context.</param>
        <SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter:=True)> _
        Public Overrides Sub GetObjectData(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.GetObjectData(info, context)

        End Sub 'GetObjectData

#End Region
    End Class 'ActivationPausedException
End Namespace
