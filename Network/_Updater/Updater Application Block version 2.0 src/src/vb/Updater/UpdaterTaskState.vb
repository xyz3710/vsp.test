 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' UpdaterTaskState.vb
'
' Contains the implementation of the UpdaterTaskState enumeration.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' The different states an UpdaterTask can be.
    ' </summary>

    Public Enum UpdaterTaskState
        ' <summary>
        ' No state specified.
        ' </summary>
        None

        ' <summary>
        ' The task is starting the download process.
        ' </summary>
        Downloading

        ' <summary>
        ' The task is being downloaded.
        ' </summary>
        Downloaded

        ' <summary>
        ' There is a download error while processing the task.
        ' </summary>
        DownloadError

        ' <summary>
        ' The activation is being initialized.
        ' </summary>
        ActivationInitializing

        ' <summary>
        ' The activation initialization has been aborted.
        ' </summary>
        ActivationInitializationAborted

        ' <summary>
        ' The task is being activated.
        ' </summary>
        Activating

        ' <summary>
        ' The activation detects an error during activation.
        ' </summary>
        ActivationError

        ' <summary>
        ' The task has been activated.
        ' </summary>
        Activated

        ' <summary>
        ' The task has been cancelled.
        ' </summary>
        Cancelled
    End Enum 'UpdaterTaskState
End Namespace