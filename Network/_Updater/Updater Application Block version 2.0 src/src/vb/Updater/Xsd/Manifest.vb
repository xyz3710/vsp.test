 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' Manifest.vb
'
' 
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System.Xml.Serialization

Namespace Microsoft.ApplicationBlocks.Updater.Xsd
    ' <summary/>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest"), XmlRoot("manifest", [Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest", IsNullable:=True)> _
    Public Class Manifest
        ' <summary>
        ' The manifest schema defines the required structure of the manifest files.
        ' </summary>
        Private _description As String
        ' <summary/>
        Private _application As ManifestApplication
        ' <summary/>
        Private _files As ManifestFiles
        ' <summary/>
        Private _downloader As ManifestDownloaderProviderData
        ' <summary/>
        Private _activationProcess As ActivationProcess
        ' <summary/>
        Private _manifestId As String
        ' <summary/>
        Private _mandatory As YesNoBoolType
        ' <summary/>
        Private _includedManifests As IncludedManifests

        ' <summary>
        ' A string that contains a simple description of the manifest. This can be used by a client application to inform the user of information about the update. 
        '</summary>
        <XmlElement("description")> _
        Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal Value As String)
                _description = Value
            End Set
        End Property

        ' <summary>
        ' A complex type that describes the client application.
        ' </summary>
        <XmlElement("application")> _
        Public Property Application() As ManifestApplication
            Get
                Return _application
            End Get
            Set(ByVal Value As ManifestApplication)
                _application = Value
            End Set
        End Property

        ' <summary>
        ' A complex type that contains the list of files to download.
        '</summary>
        <XmlElement("files")> _
        Public Property Files() As ManifestFiles
            Get
                Return _files
            End Get
            Set(ByVal Value As ManifestFiles)
                _files = Value
            End Set
        End Property

        ' <summary>
        ' A complex type that specifies which downloader implementation to use.
        '</summary>
        <XmlElement("downloader")> _
        Public Property Downloader() As ManifestDownloaderProviderData
            Get
                Return _downloader
            End Get
            Set(ByVal Value As ManifestDownloaderProviderData)
                _downloader = Value
            End Set
        End Property

        ' <summary>
        ' A complex type that specifies which activation processors to use. 
        '</summary>
        <XmlElement("activation")> _
        Public Property ActivationProcess() As ActivationProcess
            Get
                Return _activationProcess
            End Get
            Set(ByVal Value As ActivationProcess)
                _activationProcess = Value
            End Set
        End Property

        ' <summary>
        ' A complex type that defines any other manifests that should be included.
        ' </summary>
        <XmlElement("includedManifests")> _
        Public Property IncludedManifests() As IncludedManifests
            Get
                Return _includedManifests
            End Get
            Set(ByVal Value As IncludedManifests)
                _includedManifests = Value
            End Set
        End Property

        ' <summary>
        ' A string that specifies the unique identifier of the manifest. 
        '</summary>
        <XmlAttributeAttribute("manifestId")> _
        Public Property ManifestId() As String
            Get
                Return _manifestId
            End Get
            Set(ByVal Value As String)
                _manifestId = Value
            End Set
        End Property

        ' <summary>
        ' Specifies whether this is a mandatory update.
        ' </summary>
        <XmlAttributeAttribute("mandatory")> _
        Public Property Mandatory() As YesNoBoolType
            Get
                Return _mandatory
            End Get
            Set(ByVal Value As YesNoBoolType)
                _mandatory = Value
            End Set
        End Property
    End Class 'Manifest 

    ' <summary>
    ' Type that describes the client application.
    ' </summary>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")> _
    Public Class ManifestApplication
        ' <summary/>
        Private _entryPoint As EntryPoint
        ' <summary/>
        Private _location As String
        ' <summary/>
        Private _applicationId As String
        ' <summary/>
        Private _any As System.Xml.XmlElement
        ' <summary/>
        Private _anyAttr() As System.Xml.XmlAttribute


        ' <summary>
        ' A complex type that identifies the entry point for the application. For example, when using AppStart to start an application.
        ' </summary>
        <XmlElement("entryPoint")> _
        Public Property EntryPoint() As EntryPoint
            Get
                Return _entryPoint
            End Get
            Set(ByVal Value As EntryPoint)
                _entryPoint = Value
            End Set
        End Property

        ' <summary>
        ' A string that provides information about the path on the client computer where the updates should be copied.
        ' </summary>
        <XmlElement("location")> _
        Public Property Location() As String
            Get
                Return _location
            End Get
            Set(ByVal Value As String)
                _location = Value
            End Set
        End Property

        ' <summary>
        ' A string that specifies the unique identifier of this application
        ' </summary>
        <XmlAttributeAttribute("applicationId")> _
        Public Property ApplicationId() As String
            Get
                Return _applicationId
            End Get
            Set(ByVal Value As String)
                _applicationId = Value
            End Set
        End Property

        ' <summary>
        ' A reference to an extra element that might be added to the configuration
        ' </summary>
        <XmlAnyElement()> _
        Public Property Any() As System.Xml.XmlElement
            Get
                Return _any
            End Get
            Set(ByVal Value As System.Xml.XmlElement)
                _any = Value
            End Set
        End Property

        ' <summary>
        ' An array containing the extra attributes that might be added to the configuration
        ' </summary>
        <XmlAnyAttributeAttribute()> _
        Public Property AnyAttr() As System.Xml.XmlAttribute()
            Get
                Return _anyAttr
            End Get
            Set(ByVal Value As System.Xml.XmlAttribute())
                _anyAttr = Value
            End Set
        End Property
    End Class 'ManifestApplication 

    ' <summary>
    ' A type that describes information about activation processors.
    ' </summary>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")> _
    Public Class ActivationProcess
        ' <summary/>
        Private _tasks() As ActivationProcessorProviderData

        ' <summary>
        ' A complex type that contains a list of tasks to process.
        ' </summary>
        <XmlArray("tasks"), XmlArrayItem("task")> _
        Public Property Tasks() As ActivationProcessorProviderData()
            Get
                Return _tasks
            End Get
            Set(ByVal Value As ActivationProcessorProviderData())
                _tasks = Value
            End Set
        End Property
    End Class 'ActivationProcess 

    ' <summary>
    ' A complex type that specifies which activation processors to use.
    ' </summary>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")> _
    Public Class ActivationProcessorProviderData
        ' <summary/>
        Private _any As System.Xml.XmlElement
        ' <summary/>
        Private _type As String
        ' <summary/>
        Private _name As String
        ' <summary/>
        Private _anyAttr() As System.Xml.XmlAttribute


        ' <summary>
        ' A reference to an extra element that might be added to the configuration
        ' </summary>
        <XmlAnyElement()> _
        Public Property Any() As System.Xml.XmlElement
            Get
                Return _any
            End Get
            Set(ByVal Value As System.Xml.XmlElement)
                _any = Value
            End Set
        End Property

        ' <summary>
        ' A string that defines the fully qualified type of the processor.
        ' </summary>
        <XmlAttributeAttribute("type")> _
        Public Property Type() As String
            Get
                Return _type
            End Get
            Set(ByVal Value As String)
                _type = Value
            End Set
        End Property

        ' <summary>
        ' A string that defines the name of the processor.
        ' </summary>
        <XmlAttributeAttribute("name")> _
        Public Property Name() As String
            Get
                Return _name
            End Get
            Set(ByVal Value As String)
                _name = Value
            End Set
        End Property

        ' <summary>
        ' An array containing the extra attributes that might be added to the configuration
        ' </summary>
        <XmlAnyAttributeAttribute()> _
        Public Property AnyAttr() As System.Xml.XmlAttribute()
            Get
                Return _anyAttr
            End Get
            Set(ByVal Value As System.Xml.XmlAttribute())
                _anyAttr = Value
            End Set
        End Property
    End Class 'ActivationProcessorProviderData 

    ' <summary>
    ' A complex type that specifies which downloader implementation to use.
    ' </summary>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")> _
    Public Class ManifestDownloaderProviderData
        ' <summary/>
        Private _any As System.Xml.XmlElement
        ' <summary/>
        Private _name As String
        ' <summary/>
        Private _anyAttr() As System.Xml.XmlAttribute


        ' <summary>
        ' A reference to an extra element that might be added to the configuration
        ' </summary>
        <XmlAnyElement()> _
        Public Property Any() As System.Xml.XmlElement
            Get
                Return _any
            End Get
            Set(ByVal Value As System.Xml.XmlElement)
                _any = Value
            End Set
        End Property

        ' <summary>
        ' A string that defines the name of the downloader.
        ' </summary>

        <XmlAttributeAttribute("name")> _
        Public Property Name() As String
            Get
                Return _name
            End Get
            Set(ByVal Value As String)
                _name = Value
            End Set
        End Property

        ' <summary>
        ' An array containing the extra attributes that might be added to the configuration
        ' </summary>
        <XmlAnyAttributeAttribute()> _
        Public Property AnyAttr() As System.Xml.XmlAttribute()
            Get
                Return _anyAttr
            End Get
            Set(ByVal Value As System.Xml.XmlAttribute())
                _anyAttr = Value
            End Set
        End Property
    End Class 'ManifestDownloaderProviderData 

    ' <summary>
    ' A complex type that identifies the entry point for the application. For example, when using AppStart to start an application.
    ' </summary>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")> _
    Public Class EntryPoint
        ' <summary/>
        Private _file As String
        ' <summary/>
        Private _parameters As String


        ' <summary>
        ' A string that contains the file to use as the entry point for the application.
        ' </summary>
        <XmlAttributeAttribute("file")> _
        Public Property File() As String
            Get
                Return _file
            End Get
            Set(ByVal Value As String)
                _file = Value
            End Set
        End Property

        ' <summary>
        ' A string that contains any parameters the entry point requires.
        ' </summary>
        <XmlAttributeAttribute("parameters")> _
        Public Property Parameters() As String
            Get
                Return _parameters
            End Get
            Set(ByVal Value As String)
                _parameters = Value
            End Set
        End Property
    End Class 'EntryPoint 

    ' <summary>
    ' A complex type that contains the list of files to download.
    ' </summary>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")> _
    Public Class ManifestFiles
        ' <summary/>
        Private _basePath As String
        ' <summary/>
        Private _hashComparison As YesNoBoolType = YesNoBoolType.No
        ' <summary/>
        Private _files() As ManifestFile
        ' <summary/>
        Private _hashProviderName As String


        ' <summary>
        ' A string that specifies the location on the server from where the files should be downloaded.
        ' </summary>
        <XmlAttributeAttribute("base")> _
        Public Property Base() As String
            Get
                Return _basePath
            End Get
            Set(ByVal Value As String)
                _basePath = Value
            End Set
        End Property

        ' <summary>
        ' A Boolean that defines whether a hash comparison should be made.
        ' </summary>
        <XmlAttributeAttribute("hashComparison")> _
        Public Property HashComparison() As YesNoBoolType
            Get
                Return _hashComparison
            End Get
            Set(ByVal Value As YesNoBoolType)
                _hashComparison = Value
            End Set
        End Property

        ' <summary>
        ' A string that defines the type of hashing mechanism to be used.
        ' </summary>
        <XmlAttributeAttribute("hashProvider")> _
        Public Property HashProviderName() As String
            Get
                Return _hashProviderName
            End Get
            Set(ByVal Value As String)
                _hashProviderName = Value
            End Set
        End Property

        ' <summary>
        ' A complex type that describes the files to download.
        ' </summary>
        <XmlElement("file")> _
        Public Property Files() As ManifestFile()
            Get
                Return _files
            End Get
            Set(ByVal Value As ManifestFile())
                _files = Value
            End Set
        End Property
    End Class 'ManifestFiles 

    ' <summary>
    ' Class that represents a list of included manifests
    ' </summary>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")> _
    Public Class IncludedManifests
        ' <summary/>
        Private _manifest() As IncludedManifest


        ' <summary>
        ' An array of types that defines any other manifests that should be included.
        ' </summary>
        <XmlElement("manifest")> _
        Public Property Manifest() As IncludedManifest()
            Get
                Return _manifest
            End Get
            Set(ByVal Value As IncludedManifest())
                _manifest = Value
            End Set
        End Property
    End Class 'IncludedManifests 

    ' <summary>
    ' A type that defines any other manifests that should be included.
    ' </summary>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")> _
    Public Class IncludedManifest
        ' <summary/>
        Private _location As String
        ' <summary/>
        Private _manifestId As String


        ' <summary>
        ' A string that specifies the location of the manifest.
        ' </summary>
        <XmlAttributeAttribute("location")> _
        Public Property Location() As String
            Get
                Return _location
            End Get
            Set(ByVal Value As String)
                _location = Value
            End Set
        End Property

        ' <summary>
        ' A string that specifies a unique identifier for the manifest.
        ' </summary>
        <XmlAttributeAttribute("manifestId")> _
        Public Property ManifestId() As String
            Get
                Return _manifestId
            End Get
            Set(ByVal Value As String)
                _manifestId = Value
            End Set
        End Property
    End Class 'IncludedManifest 

    ' <summary>
    ' A type that contains the list of files to download.
    ' </summary>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")> _
    Public Class ManifestFile
        ' <summary/>
        Private _any As System.Xml.XmlElement
        ' <summary/>
        Private _source As String
        ' <summary/>
        Private _hash As String
        ' <summary/>
        Private _transient As YesNoBoolType = YesNoBoolType.False
        ' <summary/>
        Private _anyAttr() As System.Xml.XmlAttribute


        ' <summary>
        ' A reference to an extra element that might be added to the configuration
        ' </summary>
        <XmlAnyElement()> _
        Public Property Any() As System.Xml.XmlElement
            Get
                Return _any
            End Get
            Set(ByVal Value As System.Xml.XmlElement)
                _any = Value
            End Set
        End Property

        ' <summary>
        ' A string that specifies the file name relative to the location specified in the base attribute.
        ' </summary>
        <XmlAttributeAttribute("source")> _
        Public Property [Source]() As String
            Get
                Return _source
            End Get
            Set(ByVal Value As String)
                _source = Value
            End Set
        End Property

        ' <summary>
        ' A string that defines the computed hash value of the file.
        ' </summary>
        <XmlAttributeAttribute("hash")> _
        Public Property Hash() As String
            Get
                Return _hash
            End Get
            Set(ByVal Value As String)
                _hash = Value
            End Set
        End Property

        ' <summary>
        ' A Boolean that identifies if a file is required only for the installation process and can be deleted when the installation is complete.
        ' </summary>
        <XmlAttributeAttribute("transient")> _
        Public Property Transient() As YesNoBoolType
            Get
                Return _transient
            End Get
            Set(ByVal Value As YesNoBoolType)
                _transient = Value
            End Set
        End Property

        ' <summary>
        ' An array containing the extra attributes that might be added to the configuration
        ' </summary>
        <XmlAnyAttributeAttribute()> _
        Public Property AnyAttr() As System.Xml.XmlAttribute()
            Get
                Return _anyAttr
            End Get
            Set(ByVal Value As System.Xml.XmlAttribute())
                _anyAttr = Value
            End Set
        End Property
    End Class 'ManifestFile 

    ' <summary>
    ' Enumerator for Yes-No choices in manifest. 
    ' </summary>
    <XmlType([Namespace]:="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")> _
    Public Enum YesNoBoolType
        ' <summary>A <c>yes</c> choice.</summary>
        Yes
        ' <summary>A <c>no</c> choice.</summary>
        No
        ' <summary>A <c>true</c> choice.</summary>
        [True]
        ' <summary>A <c>false</c> choice.</summary>
        [False]
    End Enum 'YesNoBoolType
End Namespace
