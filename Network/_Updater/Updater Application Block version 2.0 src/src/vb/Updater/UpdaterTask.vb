 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' UpdaterTask.vb
'
' Contains the implementation of the updater task.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Security.Permissions
Imports Microsoft.ApplicationBlocks.Updater.Configuration

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' Holds the state information about an ongoing update.
    ' </summary>
    <Serializable()> _
    Public Class UpdaterTask
        Implements ISerializable
#Region "Private fields"

        ' <summary>
        ' The task Id.
        ' </summary>
        Private _id As Guid

        ' <summary>
        ' The base folder where files will be downloaded.
        ' </summary>
        Private _downloadFilesBase As String

        ' <summary>
        ' The manifest that is being processed.
        ' </summary>
        Private _manifest As Manifest

        ' <summary>
        ' The task context where all the components can set information for later retrieval.
        ' </summary>
        Private _context As StateBag

        ' <summary>
        ' The status of the updater task.
        ' </summary>
        Private _state As UpdaterTaskState = UpdaterTaskState.None

        ' <summary>
        ' An object that can be used to synchronize access to the UpdaterTask.
        ' </summary>
        Private _syncRoot As New Object

#End Region

#Region "Constructors"


        ' <summary>
        ' Creates an instance of the updater task using the manifest retrieved.
        ' </summary>
        ' <param name="manifest">The manifes reference for the task.</param>
        Public Sub New(ByVal _manifest As Manifest)
            _id = Guid.NewGuid()
            _context = New StateBag
            Init(_manifest)

        End Sub 'New

#End Region

#Region "UpdaterTask Members"

        ' <summary>
        ' The task context where all the components can set information for later retrieval.
        ' </summary>
        ' <param name="key">The index key of the item to locate.</param>

        Default Public Property Item(ByVal key As String) As Object
            Get
                Return _context(key)
            End Get
            Set(ByVal Value As Object)
                _context(key) = Value
            End Set
        End Property

        ' <summary>
        ' Gets an object that can be used to synchronize access to the UpdaterTask.
        ' </summary>

        Public ReadOnly Property SyncRoot() As Object
            Get
                Return _syncRoot
            End Get
        End Property
        ' <summary>
        ' The base folder where files will be downloaded.
        ' </summary>

        Public ReadOnly Property DownloadFilesBase() As String
            Get
                Return _downloadFilesBase
            End Get
        End Property


        ' <summary>
        ' Initializes an existing UpdaterTask with the manifes provided.
        ' </summary>
        ' <param name="manifest"></param>
        Public Sub Init(ByVal _manifest As Manifest)
            Me._manifest = _manifest
            _downloadFilesBase = Path.Combine(New UpdaterConfigurationView().BasePath, "downloader")
            _downloadFilesBase = Path.Combine(_downloadFilesBase, Me._manifest.Application.ApplicationId)
            _downloadFilesBase = Path.Combine(_downloadFilesBase, _id.ToString())

        End Sub 'Init

        ' <summary>
        ' The task Id.
        ' </summary>

        Public ReadOnly Property TaskId() As Guid
            Get
                Return _id
            End Get
        End Property

        ' <summary>
        ' The status of the updater task.
        ' </summary>

        Public Property State() As UpdaterTaskState
            Get
                Return _state
            End Get
            Set(ByVal Value As UpdaterTaskState)
                _state = Value
            End Set
        End Property

        ' <summary>
        ' The manifest that is being processed.
        ' </summary>

        Public ReadOnly Property Manifest() As Manifest
            Get
                Return _manifest
            End Get
        End Property

#End Region

#Region "ISerializable Members"


        ' <summary>
        ' Serialization constructor, required by the RegistryManager to store the task.
        ' </summary>
        ' <param name="info">The serialization information.</param>
        ' <param name="context">The serialization context.</param>
        <SecurityPermission(SecurityAction.LinkDemand)> _
        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)

            _manifest = CType(info.GetValue("_manifest", GetType(Manifest)), Manifest)
            _state = CType(info.GetValue("_state", GetType(Integer)), UpdaterTaskState)
            _id = CType(info.GetValue("_id", GetType(Guid)), Guid)
            Me._context = CType(info.GetValue("_context", GetType(StateBag)), StateBag)
            _downloadFilesBase = CStr(info.GetValue("_downloadFilesBase", GetType(String)))

        End Sub 'New


        ' <summary>
        ' Method used by the seralization mechanism to retrieve the serialized information.
        ' </summary>
        ' <param name="info"></param>
        ' <param name="context"></param>
        <SecurityPermission(SecurityAction.Demand, SerializationFormatter:=True)> _
        Public Overridable Sub GetObjectData(ByVal info As SerializationInfo, ByVal context As StreamingContext) Implements ISerializable.GetObjectData
            info.AddValue("_manifest", _manifest)
            info.AddValue("_state", _state)
            info.AddValue("_id", _id)
            info.AddValue("_context", Me._context)
            info.AddValue("_downloadFilesBase", _downloadFilesBase)

        End Sub 'GetObjectData

#End Region
    End Class 'UpdaterTask 
End Namespace
