 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ActivationProcessTestFixture.cs
'
' 
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports NUnit.Framework
Imports xsd = Microsoft.ApplicationBlocks.Updater.Xsd


'/ <summary>
'/ Summary description for NewTests.
'/ </summary>
<TestFixture()>  _
Public Class ActivationProcessTestFixture
    
    '/ <summary/>
    Public Sub New() 
    
    End Sub 'New
    
    
    '/ <summary/>
    <Test()>  _
    Public Sub ActivationProcessTest() 
    
    End Sub 'ActivationProcessTest
End Class 'ActivationProcessTestFixture
'			xsd.Manifest man = new xsd.Manifest();
'			man.Mandatory = xsd.YesNoBoolType.True;
'			man.Action = ManifestAction.Deploy;
'			man.Description = "a manifest";
'			man.Application = new xsd.ManifestApplication();
'			man.Application.ApplicationId = "518a03e0-c042-4be9-a704-7b1506e0a187";
'			man.Application.EntryPoint = new Microsoft.ApplicationBlocks.ApplicationUpdater.Xsd.EntryPoint();
'			man.Application.EntryPoint.File = "";
'			man.Application.EntryPoint.Parameters = "";
'			man.Application.Location = "";
'			man.ActivationProcess = new Microsoft.ApplicationBlocks.ApplicationUpdater.Xsd.ActivationProcess();
'			man.ActivationProcess.Type = "NUnit.Tests.MockActivationProcess, NUnit.Tests";
'			man.ActivationProcess.Tasks = new xsd.GenericTypeConfig[] { new xsd.GenericTypeConfig() };
'			man.ActivationProcess.Tasks[0].Type = "NUnit.Tests.MockProcessor, NUnit.Tests";
'			Manifest m = new Manifest( man );
'			UpdaterTask task = new UpdaterTask( m );
'
'			ActivationProcess process = new ActivationProcess();
'			process.Activate( task );

