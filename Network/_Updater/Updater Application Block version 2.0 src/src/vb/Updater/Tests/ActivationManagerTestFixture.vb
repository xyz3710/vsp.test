 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' ActivationManagerTestFixture.cs
'
' 
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.IO
Imports NUnit.Framework
Imports Microsoft.ApplicationBlocks.Updater
Imports Microsoft.ApplicationBlocks.Updater.Downloader
Imports Microsoft.ApplicationBlocks.Updater.Activator
Imports xsd = Microsoft.ApplicationBlocks.Updater.Xsd


'/ <summary>
'/ Summary description for ActivationManagerTests.
'/ </summary>
<TestFixture()>  _
Public Class ActivationManagerTestFixture
    #Region "Private members"
    
    Private applicationPath As String = ""
    
    #End Region
    
    
    '/ <summary/>
    <TestFixtureSetUp()>  _
    Public Sub SetUp() 
        AppDomain.CurrentDomain.AppendPrivatePath(Path.GetFullPath("..\..\..\..\Tests\Tests\bin\Debug"))
    
    End Sub 'SetUp
    
    
    '/ <summary/>
    <TestFixtureTearDown()>  _
    Public Sub TearDown() 
    
    End Sub 'TearDown
    
    
    '/ <summary/>
    Private Function CreateManifest() As xsd.Manifest 
        Dim man As New xsd.Manifest()
        man.Mandatory = xsd.YesNoBoolType.True
        man.Description = "a manifest"
        man.Application = New xsd.ManifestApplication()
        man.Application.ApplicationId = "518a03e0-c042-4be9-a704-7b1506e0a187"
        man.Application.EntryPoint = New Microsoft.ApplicationBlocks.Updater.Xsd.EntryPoint()
        man.Application.EntryPoint.File = ""
        man.Application.EntryPoint.Parameters = ""
        man.Application.Location = applicationPath
        man.ActivationProcess = New Microsoft.ApplicationBlocks.Updater.Xsd.ActivationProcess()
        
        Return man
    
    End Function 'CreateManifest
    
    
    '/ <summary/>
    <Test()>  _
    Public Sub ActivationManagerTest() 
        Dim man As Xsd.Manifest = CreateManifest()
        
        man.ActivationProcess.Tasks = New xsd.ActivationProcessorProviderData() {New xsd.ActivationProcessorProviderData()}
        man.ActivationProcess.Tasks(0).Type = "Microsoft.ApplicationBlocks.Updater.UnitTests.MockProcessor, Microsoft.ApplicationBlocks.Updater.UnitTests"
        Dim m As New Manifest(man)
        Dim task As New UpdaterTask(m)
        
        Dim instance As New ActivationManager()
        instance.SubmitTask(task)
    
    End Sub 'ActivationManagerTest
    
    
    '/ <summary/>
    <Test()>  _
    Public Sub CannotActivateTest() 
        Dim man As Xsd.Manifest = CreateManifest()
        
        man.ActivationProcess.Tasks = New xsd.ActivationProcessorProviderData() {New xsd.ActivationProcessorProviderData()}
        man.ActivationProcess.Tasks(0).Type = "Microsoft.ApplicationBlocks.Updater.UnitTests.MockNoExecuteProcessor, Microsoft.ApplicationBlocks.Updater.UnitTests"
        Dim m As New Manifest(man)
        Dim task As New UpdaterTask(m)
        
        Dim instance As New ActivationManager()
        instance.SubmitTask(task)
    
    End Sub 'CannotActivateTest
    
    
    '/ <summary/>
    <Test()>  _
    Public Sub RollbackActivationTest() 
        Dim man As Xsd.Manifest = CreateManifest()
        
        man.ActivationProcess.Tasks = New xsd.ActivationProcessorProviderData() {New xsd.ActivationProcessorProviderData(), New xsd.ActivationProcessorProviderData()}
        man.ActivationProcess.Tasks(0).Type = "Microsoft.ApplicationBlocks.Updater.UnitTests.MockRollBackProcessor, Microsoft.ApplicationBlocks.Updater.UnitTests"
        man.ActivationProcess.Tasks(1).Type = "Microsoft.ApplicationBlocks.Updater.UnitTests.MockFailProcessor, Microsoft.ApplicationBlocks.Updater.UnitTests"
        Dim m As New Manifest(man)
        Dim task As New UpdaterTask(m)
        
        Dim instance As New ActivationManager()
        instance.SubmitTask(task)
    
    End Sub 'RollbackActivationTest
    
    
    '/ <summary/>
    <Test()>  _
    Public Sub ActivationFailTest() 
        Dim man As Xsd.Manifest = CreateManifest()
        
        man.ActivationProcess.Tasks = New xsd.ActivationProcessorProviderData() {New xsd.ActivationProcessorProviderData()}
        man.ActivationProcess.Tasks(0).Type = "Microsoft.ApplicationBlocks.Updater.UnitTests.MockFailProcessor, Microsoft.ApplicationBlocks.Updater.UnitTests"
        Dim m As New Manifest(man)
        Dim task As New UpdaterTask(m)
        
        Dim instance As New ActivationManager()
        instance.SubmitTask(task)
    
    End Sub 'ActivationFailTest
End Class 'ActivationManagerTestFixture