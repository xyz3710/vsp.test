 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' StateBag.vb
'
' Contains the implementation of a state bag.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports System.Collections

Namespace Microsoft.ApplicationBlocks.Updater
    ' <summary>
    ' A helper class to hold any state information needed in the UpdaterTask.
    ' </summary>
    <Serializable()> _
    Friend Class StateBag
        Inherits DictionaryBase
#Region "Constructor"


        ' <summary>
        ' Default constructor.
        ' </summary>
        Public Sub New()

        End Sub 'New

#End Region

#Region "Public properties"

        ' <summary>
        ' Gets or sets and object value associated with a string key.
        ' </summary>

        Default Public Property Item(ByVal key As String) As Object
            Get
                Return Dictionary(key)
            End Get
            Set(ByVal Value As Object)
                Dictionary(key) = Value
            End Set
        End Property

#End Region
    End Class 'StateBag
End Namespace