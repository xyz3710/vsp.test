 '============================================================================================================
' Microsoft Updater Application Block for .NET
'  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
'	
' Events.vb
'
' Contains the definition of all the delegates and EventArgs derived classes.
' 
' For more information see the Updater Application Block Implementation Overview. 
' 
'============================================================================================================
' Copyright � Microsoft Corporation.  All rights reserved.
' All rights reserved.
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
' OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
' FITNESS FOR A PARTICULAR PURPOSE.
'============================================================================================================
Imports System
Imports Microsoft.ApplicationBlocks.Updater

Namespace Microsoft.ApplicationBlocks.Updater

#Region "EventArgs classes"


#Region "TaskEventArgs class"

    ' <summary>
    ' Used to norify information about an updater task.
    ' </summary>

    Public Class TaskEventArgs
        Inherits EventArgs
        Private currentUpdaterTask As UpdaterTask


        ' <summary>
        ' Creates an instance of the TaskEventArgsusing an UpdaterTask reference.
        ' </summary>
        ' <param name="task">The UpdaterTask information.</param>
        Public Sub New(ByVal task As UpdaterTask)
            currentUpdaterTask = task

        End Sub 'New

        ' <summary>
        ' Get's the updater task.
        ' </summary>

        Public ReadOnly Property Task() As UpdaterTask
            Get
                Return currentUpdaterTask
            End Get
        End Property
    End Class 'TaskEventArgs 
#End Region

#Region "ActivationProcessorEventArgs class"

    ' <summary>
    ' Used to notify events occurring during activation.
    ' </summary>

    Public Class ActivationProcessorEventArgs
        Inherits TaskEventArgs
        ' <summary>
        ' The processor instance.
        ' </summary>
        Private _processor As IActivationProcessor


        ' <summary>
        ' Creates an instance of the ActivationProcessorEventArgs using an UpdaterTask and an activcator processor.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <param name="processor">The IActivationProcessor instance.</param>
        Public Sub New(ByVal task As UpdaterTask, ByVal processor As IActivationProcessor)
            MyBase.New(task)
            Me._processor = processor

        End Sub 'New

        ' <summary>
        ' The processor instance.
        ' </summary>

        Public ReadOnly Property Processor() As IActivationProcessor
            Get
                Return _processor
            End Get
        End Property
    End Class 'ActivationProcessorEventArgs 
#End Region

#Region "ActivationTaskErrorEventArgs class"

    ' <summary>
    ' Used to notify events about activation task errors.
    ' </summary>

    Public Class ActivationTaskErrorEventArgs
        Inherits ActivationProcessorEventArgs
        ' <summary>
        ' Exception detected during the activation.
        ' </summary>
        Private _exception As Exception


        ' <summary>
        ' Creates a new instance of the ActivationTaskErrorEventArgs using an UpdaterTask an IActivatorProcessor and an Exception.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <param name="processor">The IActivationProcessor instance.</param>
        ' <param name="exception">The Exception instance.</param>
        Public Sub New(ByVal task As UpdaterTask, ByVal processor As IActivationProcessor, ByVal exception As Exception)
            MyBase.New(task, processor)
            Me._exception = exception

        End Sub 'New

        ' <summary>
        ' Exception detected during the activation.
        ' </summary>

        Public ReadOnly Property Exception() As Exception
            Get
                Return _exception
            End Get
        End Property
    End Class 'ActivationTaskErrorEventArgs 
#End Region

#Region "ActivationTaskCompleteEventArgs class"

    ' <summary>
    ' Used to notify an event about the UpdaterTask completion.
    ' </summary>

    Public Class ActivationTaskCompleteEventArgs
        Inherits TaskEventArgs
        ' <summary>
        ' Indicates whether the activation was successful.
        ' </summary>
        Private _success As Boolean


        ' <summary>
        ' Constructor for the ActivationTaskCompleteEventArgs.
        ' </summary>
        ' <param name="task">The <see cref="UpdaterTask"/> instance.</param>
        ' <param name="success">Result of the activation process</param>
        Public Sub New(ByVal task As UpdaterTask, ByVal success As Boolean)
            MyBase.New(task)
            Me._success = success

        End Sub 'New

        ' <summary>
        ' Indicates whether the activation was successful.
        ' </summary>

        Public ReadOnly Property Success() As Boolean
            Get
                Return _success
            End Get
        End Property
    End Class 'ActivationTaskCompleteEventArgs 
#End Region

#Region "BaseDownloadProgrssEventArgs class"

    ' <summary>
    ' Used to notify information about the download stage progress.
    ' </summary>

    Public Class BaseDownloadProgressEventArgs
        Inherits EventArgs
        ' <summary>
        ' The total bystes that will be transfered.
        ' </summary>
        Private _bytesTotal As Long

        ' <summary>
        ' The amount of bytes that have been transfered.
        ' </summary>
        Private _bytesTransferred As Long

        ' <summary>
        ' The total files that will be transfered.
        ' </summary>
        Private _filesTotal As Integer

        ' <summary>
        ' The files that have been transfered.
        ' </summary>
        Private _filesTransferred As Integer

        ' <summary>
        ' Indicates whether the operation was canceled.
        ' </summary>
        Private _cancel As Boolean = False


        ' <summary>
        ' Creates an instace of BaseDownloadProgressEventArgs specfying all the information for event.
        ' </summary>
        ' <param name="bytesTotal">The total bytes that will be transfered.</param>
        ' <param name="bytesTransferred">How many bytes have been transfered.</param>
        ' <param name="filesTotal">The total files that will be trnasfered.</param>
        ' <param name="filesTransferred">How many files have been transfered.</param>
        Public Sub New(ByVal bytesTotal As Long, ByVal bytesTransferred As Long, ByVal filesTotal As Integer, ByVal filesTransferred As Integer)
            Me._bytesTotal = bytesTotal
            Me._bytesTransferred = bytesTransferred
            Me._filesTotal = filesTotal
            Me._filesTransferred = filesTransferred

        End Sub 'New

        ' <summary>
        ' The total bystes that will be transfered.
        ' </summary>

        Public ReadOnly Property BytesTotal() As Long
            Get
                Return _bytesTotal
            End Get
        End Property
        ' <summary>
        ' The amount of bytes that have been transfered.
        ' </summary>

        Public ReadOnly Property BytesTransferred() As Long
            Get
                Return _bytesTransferred
            End Get
        End Property
        ' <summary>
        ' The total files that will be transfered.
        ' </summary>

        Public ReadOnly Property FilesTotal() As Integer
            Get
                Return _filesTotal
            End Get
        End Property
        ' <summary>
        ' The files that have been transfered.
        ' </summary>

        Public ReadOnly Property FilesTransferred() As Integer
            Get
                Return _filesTransferred
            End Get
        End Property
        ' <summary>
        ' Indicates whether the operation was canceled.
        ' </summary>

        Public Property Cancel() As Boolean
            Get
                Return _cancel
            End Get
            Set(ByVal Value As Boolean)
                _cancel = _cancel OrElse Value
            End Set
        End Property
    End Class 'BaseDownloadProgressEventArgs 
#End Region

#Region "DownloadTaskProgressEventArgs class"

    ' <summary>
    ' Used to notify events about download progess.
    ' </summary>

    Public Class DownloadTaskProgressEventArgs
        Inherits BaseDownloadProgressEventArgs
        ' <summary>
        ' The updater task.
        ' </summary>
        Private _task As UpdaterTask


        ' <summary>
        ' Creates an instance of DownloadTaskProgressEventArgs specifying all the progress information.
        ' </summary>
        ' <param name="bytesTotal">The total bytes that will be transfered.</param>
        ' <param name="bytesTransferred">How many bytes have been transfered.</param>
        ' <param name="filesTotal">The total files that will be trnasfered.</param>
        ' <param name="filesTransferred">How many files have been transfered.</param>
        ' <param name="task">The UpdaterTask instance.</param>
        Public Sub New(ByVal bytesTotal As Long, ByVal bytesTransferred As Long, ByVal filesTotal As Integer, ByVal filesTransferred As Integer, ByVal task As UpdaterTask)
            MyBase.New(bytesTotal, bytesTransferred, filesTotal, filesTransferred)
            Me._task = task

        End Sub 'New

        ' <summary>
        ' The updater task.
        ' </summary>

        Public ReadOnly Property Task() As UpdaterTask
            Get
                Return _task
            End Get
        End Property
    End Class 'DownloadTaskProgressEventArgs 
#End Region

#Region "DownloadTaskErrorEventArgs class"

    ' <summary>
    ' Used to notify information about download errors.
    ' </summary>

    Public Class DownloadTaskErrorEventArgs
        Inherits TaskEventArgs
        ' <summary>
        ' The exception received.
        ' </summary>
        Private _exception As Exception


        ' <summary>
        ' Creates an instasnce of the DownloadTaskErrorEventArgs with the UpdaterTask and the exception information.
        ' </summary>
        ' <param name="task">The UpdaterTask instance.</param>
        ' <param name="exception">The exception information.</param>
        Public Sub New(ByVal task As UpdaterTask, ByVal exception As Exception)
            MyBase.New(task)
            Me._exception = exception

        End Sub 'New

        ' <summary>
        ' The exception received.
        ' </summary>

        Public ReadOnly Property Exception() As Exception
            Get
                Return _exception
            End Get
        End Property
    End Class 'DownloadTaskErrorEventArgs 
#End Region

#End Region

#Region "Download events"


    ' <summary>
    ' Event handler for the DownloadTaskProgressEvent event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub DownloadTaskProgressEventHandler(ByVal sender As Object, ByVal e As DownloadTaskProgressEventArgs)


    ' <summary>
    ' Event handler for the DownloadTaskStarted event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub DownloadTaskStartedEventHandler(ByVal sender As Object, ByVal e As TaskEventArgs)


    ' <summary>
    ' Event handler for the DownloadTaskCompleted event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub DownloadTaskCompletedEventHandler(ByVal sender As Object, ByVal e As TaskEventArgs)


    ' <summary>
    ' Event handler for the DownloadTaskError event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub DownloadTaskErrorEventHandler(ByVal sender As Object, ByVal e As DownloadTaskErrorEventArgs)

#End Region

#Region "Activation events"


    ' <summary>
    ' Event handler for the ActivationTaskInitializing event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub ActivationTaskInitializingEventHandler(ByVal sender As Object, ByVal e As TaskEventArgs)


    ' <summary>
    ' Event handler for the ActivationTaskInitializationAborted event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub ActivationTaskInitializationAbortedEventHandler(ByVal sender As Object, ByVal e As ActivationTaskErrorEventArgs)


    ' <summary>
    ' Event handler for the ActivationTaskStarted event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub ActivationTaskStartedEventHandler(ByVal sender As Object, ByVal e As TaskEventArgs)


    ' <summary>
    ' Event handler for the ActivationTaskCompleted event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub ActivationTaskCompletedEventHandler(ByVal sender As Object, ByVal e As ActivationTaskCompleteEventArgs)


    ' <summary>
    ' Event handler for the ActivationTaskError event.
    ' </summary>
    ' <param name="sender">The sender of the event.</param>
    ' <param name="e">The information about the event.</param>
    Public Delegate Sub ActivationTaskErrorEventHandler(ByVal sender As Object, ByVal e As ActivationTaskErrorEventArgs)

#End Region

End Namespace
