//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// BitsDownloaderProviderData.cs
//
// Contains the implementation of the configuration for BITS downloader.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System.Xml.Serialization;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.ApplicationBlocks.Updater.Downloaders;

namespace Microsoft.ApplicationBlocks.Updater.Configuration
{
	/// <summary>
	/// Defines the configuration in the local computer for Background Intelligent Trasnfer Service (BITS).
	/// </summary>
	[XmlRoot("downloader", Namespace=ApplicationUpdaterSettings.ConfigurationNamespace )]
	public sealed class BitsDownloaderProviderData : DownloadProviderData
	{
		#region Private members

		/// <summary>
		/// The name of the user which will be used by the downloader.
		/// </summary>
		private string userName = null;

		/// <summary>
		/// The password for the user used by the downloader.
		/// </summary>
		private string password = null;

		/// <summary>
		/// The authentication scheme used by BITS for the download process.
		/// </summary>
		private BG_AUTH_SCHEME authenticationScheme = BG_AUTH_SCHEME.BG_AUTH_SCHEME_NTLM;

		/// <summary>
		/// The type of the server for the BITS download.
		/// </summary>
		private BG_AUTH_TARGET targetServerType = BG_AUTH_TARGET.BG_AUTH_TARGET_SERVER;

		#endregion

		#region Constructor

		/// <summary>
		/// Default construcor.
		/// </summary>
		public BitsDownloaderProviderData() 
		{
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The name of the user.
		/// </summary>
		[XmlElement("userName")]
		public string UserName
		{
			get{ return userName; }
			set{ userName = value; }
		}

		/// <summary>
		/// The password for the user.
		/// </summary>
		[XmlElement("password")]
		public string Password
		{
			get { return password; }
			set { password = value; }
		}

		/// <summary>
		/// The authentication scheme.
		/// </summary>
		[XmlElement("authenticationScheme")]
		public BG_AUTH_SCHEME AuthenticationScheme
		{
			get { return authenticationScheme; }
			set { authenticationScheme = value; }
		}

		/// <summary>
		/// The target server type.
		/// </summary>
		[XmlElement("targetServerType")]
		public BG_AUTH_TARGET TargetServerType
		{
			get { return targetServerType; }
			set { targetServerType = value; }
		}

		/// <summary>
		/// The name of the type.
		/// </summary>
		[XmlIgnore()]
		public override string TypeName
		{
			get { return typeof( BitsDownloader ).AssemblyQualifiedName; }
			set {}
		}

		#endregion
	}
}
