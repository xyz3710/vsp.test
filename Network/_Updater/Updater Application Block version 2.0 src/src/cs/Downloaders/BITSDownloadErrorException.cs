//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// BitsDownloaderErrorException.cs
//
// Contains the implementation of the exception thrown by BITS when an error occurs.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Security.Permissions;
using System.Runtime.Serialization;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Downloaders
{
	/// <summary>
	/// Exception thrown by BITS downloader when an error is found.
	/// </summary>
	[Serializable]
	public class BitsDownloadErrorException : Exception
	{
		#region Private members

		/// <summary>
		/// The context for the error.
		/// </summary>
		private BG_ERROR_CONTEXT contextForError;

		/// <summary>
		/// The error code detected.
		/// </summary>
		private int errorCode;

		/// <summary>
		/// The description of the context.
		/// </summary>
		private string contextDescription;
		
		/// <summary>
		/// The description of the error.
		/// </summary>
		private string errorDescription;

		/// <summary>
		/// The protocol name.
		/// </summary>
		private string protocol;

		/// <summary>
		/// The file name where the file will be copied.
		/// </summary>
		private string fileLocalName;

		/// <summary>
		/// The remote file name that was downloaded.
		/// </summary>
		private string fileRemoteName;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public BitsDownloadErrorException() : base()
		{
		}

		/// <summary>
		/// Creates an exception with the BITS error reference and a language id.
		/// </summary>
		/// <param name="error">The BITS error reference.</param>
		/// <param name="langID">The language Id.</param>
		internal BitsDownloadErrorException( IBackgroundCopyError error, uint langID )
		{
			IBackgroundCopyFile file;

			error.GetError(out contextForError, out errorCode );
			
			error.GetErrorContextDescription( langID, out contextDescription );
			error.GetErrorDescription( langID, out errorDescription );
			error.GetFile( out file );
			error.GetProtocol( out protocol );

			file.GetLocalName( out fileLocalName );
			file.GetRemoteName( out fileRemoteName );
		}

		/// <summary>
		/// Creates an exception with the specified message.
		/// </summary>
		/// <param name="message">The message of the exception.</param>
		public BitsDownloadErrorException( string message ) : base( message )
		{
			errorDescription = message;
		}

		/// <summary>
		/// Creates an exception with the specified message and the inner exception detected.
		/// </summary>
		/// <param name="message">The message string.</param>
		/// <param name="innerException">The inner exception reference.</param>
		public BitsDownloadErrorException( string message, Exception innerException ) : base( message, innerException )
		{
			errorDescription = message;
		}

		/// <summary>
		/// Constructor used by the serialization infrastructure.
		/// </summary>
		/// <param name="info">The serialization information for the object.</param>
		/// <param name="context">The context for the serialization.</param>
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		protected BitsDownloadErrorException(SerializationInfo info, StreamingContext context) : base( info, context ) 
		{
		}

		#endregion

		#region Public members

		/// <summary>
		/// The error code.
		/// </summary>
		public int Code
		{ 
			get { return errorCode; } 
		}

		/// <summary>
		/// The error context.
		/// </summary>
		public int Context 
		{ 
			get { return (int)contextForError; } 
		}

		/// <summary>
		/// The context description.
		/// </summary>
		public string ContextDescription
		{
			get { return contextDescription; }
		}

		/// <summary>
		/// The error message.
		/// </summary>
		public override string Message
		{
			get{ return errorDescription; }
		}

		/// <summary>
		/// The protocol used.
		/// </summary>
		public string Protocol
		{
			get { return protocol; }
		}

		/// <summary>
		/// The local file name.
		/// </summary>
		public string LocalFileName
		{
			get { return fileLocalName; }
		}

		/// <summary>
		/// The remote file name.
		/// </summary>
		public string RemoteFileName
		{
			get  { return fileRemoteName; }
		}

		#endregion
		
		#region Public methods

		/// <summary>
		/// Used by the serialization infrastructure.
		/// </summary>
		/// <param name="info">The serialization information.</param>
		/// <param name="context">The serialization context.</param>
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context) 
		{
			 base.GetObjectData ( info, context );
		}

		#endregion
	}
}
