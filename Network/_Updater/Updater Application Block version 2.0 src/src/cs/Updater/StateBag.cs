//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// StateBag.cs
//
// Contains the implementation of a state bag.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Collections;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// A helper class to hold any state information needed in the UpdaterTask.
	/// </summary>
	[Serializable]
	internal class StateBag : DictionaryBase 
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		public StateBag()
		{
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Gets or sets and object value associated with a string key.
		/// </summary>
		public object this [ string key ]
		{
			get
			{
				return Dictionary[ key ];
			}
			set
			{
				Dictionary[ key ] = value;
			}
		}

		#endregion
	}
}
