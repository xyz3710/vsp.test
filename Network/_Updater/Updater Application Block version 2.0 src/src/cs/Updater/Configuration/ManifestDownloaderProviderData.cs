//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ManifestDownloaderProviderData.cs
//
// Contains the implementation of the configuration for the downloader within the manifest.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System.Xml;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Configuration
{
	/// <summary>
	/// Defines the configuration for the downloader in the manifest.
	/// </summary>
	public sealed class ManifestDownloaderProviderData 
	{
		#region Private members

		/// <summary>
		/// The downloader configuration within the manifest.
		/// </summary>
		private Xsd.ManifestDownloaderProviderData data;

		private string name;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates an instance of a ManifestDownloaderProviderData using the deserialized configuration 
		/// and the name of the downloader provider.
		/// </summary>
		/// <param name="name">The name of the download provider.</param>
		/// <param name="data">The downloader configuration in the manifest.</param>
		public ManifestDownloaderProviderData( string name, Xsd.ManifestDownloaderProviderData data )
		{
			this.data = data;
			this.name = name;
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Tne downloader name.
		/// </summary>
		public string Name
		{
			get
			{
				return this.name;			
			}
		}

		/// <summary>
		/// Any element defined in the manifest for the downloader.
		/// </summary>
		[XmlAnyElement()]
		public XmlElement AnyElement
		{
			get	{ return data.Any; }
		}

		/// <summary>
		/// Any attribute defined in the downloader node.
		/// </summary>
		[XmlAnyAttribute()] 
		public XmlAttribute[] AnyAttributes
		{
			get	{ return data.AnyAttr;	}
		}

		#endregion
	}
}
