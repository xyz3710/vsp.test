//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ManifestManagerSettings.cs
//
// Contains the implementation of the manifest manager.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System.Xml.Serialization;

namespace Microsoft.ApplicationBlocks.Updater.Configuration
{
	/// <summary>
	/// Defines the configuration for the manifest manager.
	/// </summary>
	[XmlRoot("manifestManager", Namespace=ApplicationUpdaterSettings.ConfigurationNamespace)]
	public class ManifestManagerSettings
	{
		#region Private members

		/// <summary>
		/// The athentication type for getting the manifest.
		/// </summary>
		private AuthenticationType authenticationMethod =  AuthenticationType.Anonymous;

		/// <summary>
		/// The user name used as a credential.
		/// </summary>
		private string userName;

		/// <summary>
		/// The password used as a credential.
		/// </summary>
		private string password;

		#endregion

		#region Public properties

		/// <summary>
		/// The authentication type.
		/// </summary>
		[XmlAttribute("authenticationMethod")]
		public AuthenticationType AuthenticationType
		{
			get { return authenticationMethod; }
			set { authenticationMethod = value; }
		}

		/// <summary>
		/// The user name credential.
		/// </summary>
		[XmlAttribute("userName")]
		public string UserName
		{
			get { return userName; }
			set { userName = value; }
		}

		/// <summary>
		/// The password credential.
		/// </summary>
		[XmlAttribute("password")]
		public string Password
		{
			get { return password; }
			set { password = value; }
		}

		#endregion
	}
}
