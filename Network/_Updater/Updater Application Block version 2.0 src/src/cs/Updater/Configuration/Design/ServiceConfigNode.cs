//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ServiceConfigNode.cs
//
// Contains the implementation of the service configuration node.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.ComponentModel;
using Microsoft.ApplicationBlocks.Updater;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design.Validation;

namespace Microsoft.ApplicationBlocks.Updater.Configuration.Design
{
	/// <summary>
	/// Defines the configuration for the service.
	/// </summary>
	[Image(typeof(ServiceConfigNode))]
	public class ServiceConfigNode : ConfigurationNode
	{
		#region Private members

		/// <summary>
		/// The service configuration instance.
		/// </summary>
		private ServiceSettings data;

		#endregion

		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ServiceConfigNode() : this( new ServiceSettings() )
		{
		}

		/// <summary>
		/// Creates a ServiceConfigNode with the service configuration.
		/// </summary>
		/// <param name="data">The service configuration instance.</param>
		public ServiceConfigNode( ServiceSettings data ): base()
		{
			this.data = data;
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The name of the node.
		/// </summary>
		[ReadOnly(true)]
		public override string Name
		{
			get
			{
				return base.Name;
			}
			set
			{
				base.Name = value;
			}
		}

		/// <summary>
		/// The updater service interval.
		/// </summary>
		[ResourceDescription( Resource.MessageKey.GetSetUpdaterServiceInterval )]
		[Required]
		public string UpdateInterval
		{
			get { return data.Interval; }
			set { data.Interval = value; }
		}

		/// <summary>
		/// The service settings.
		/// </summary>
		[Browsable(false)]
		public ServiceSettings ServiceSettings
		{
			get { return data; }
		}

		/// <summary>
		/// Set the node name.
		/// </summary>
		protected override void OnSited()
		{
			base.OnSited();
			Site.Name = Resource.ResourceManager[ Resource.MessageKey.ServiceConfigurationNode ]; 
		}

		#endregion

		protected override void AddDefaultChildNodes()
		{
			base.AddDefaultChildNodes ();
			ApplicationUpdaterNode appNode = Parent as ApplicationUpdaterNode;
			appNode.ServiceNode = this;
		}

	}
}

