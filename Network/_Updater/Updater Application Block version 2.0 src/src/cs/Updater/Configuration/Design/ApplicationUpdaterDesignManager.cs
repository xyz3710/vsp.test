//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ApplicationUpdaterDesignManager.cs
//
// Contains the implementation of the application updater design manager.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design;

using Microsoft.ApplicationBlocks.Updater.Downloaders;

namespace Microsoft.ApplicationBlocks.Updater.Configuration.Design
{
	/// <summary>
	/// Defines the root class for the configuration manager on this assembly.
	/// </summary>
	public class ApplicationUpdaterDesignManager : IConfigurationDesignManager
	{
		#region Private members

		/// <summary>
		/// The name of the section.
		/// </summary>
		private const string SectionName = "UpdaterConfiguration";

		#endregion
		
		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ApplicationUpdaterDesignManager()
		{
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Register the nodes in the tool.
		/// </summary>
		/// <param name="serviceProvider">The service provider instance.</param>
		public void Register(IServiceProvider serviceProvider)
		{
			RegisterNodeMaps(serviceProvider);
			CreateCommands(serviceProvider);
			RegisterXmlIncludeTypes(serviceProvider);
		}

		/// <summary>
		/// Open the configuration section.
		/// </summary>
		/// <param name="serviceProvider">The service provider instance.</param>
		public void Open(IServiceProvider serviceProvider)
		{
			ConfigurationContext configurationContext = ServiceHelper.GetCurrentConfigurationContext(serviceProvider);
			if (configurationContext.IsValidSection(SectionName))
			{
				ApplicationUpdaterNode applicationUpdaterNode = null;
				try
				{
					ApplicationUpdaterSettings settings = configurationContext.GetConfiguration(SectionName) as ApplicationUpdaterSettings;
					if (settings != null)
					{
						applicationUpdaterNode = new ApplicationUpdaterNode(settings);
						ConfigurationNode configurationNode = ServiceHelper.GetCurrentRootNode(serviceProvider);
						configurationNode.Nodes.Add(applicationUpdaterNode);
					}
				}
				catch (ConfigurationException e)
				{
					ServiceHelper.LogError(serviceProvider, applicationUpdaterNode, e);
				}
			}
		}
		
		/// <summary>
		/// Called when the configuration will be saved.
		/// </summary>
		/// <param name="serviceProvider">The service provider.</param>
		public void Save(IServiceProvider serviceProvider)
		{
			ConfigurationContext configurationContext = ServiceHelper.GetCurrentConfigurationContext(serviceProvider);
			if (configurationContext.IsValidSection(SectionName))
			{
				ApplicationUpdaterNode applicationUpdaterNode = null;
				try
				{
					IUIHierarchy hierarchy = ServiceHelper.GetCurrentHierarchy(serviceProvider);
					applicationUpdaterNode = hierarchy.FindNodeByType(typeof(ApplicationUpdaterNode)) as ApplicationUpdaterNode;
					if ( applicationUpdaterNode == null ) 
					{ 
						return; 
					}
					ApplicationUpdaterSettings settings = applicationUpdaterNode.Settings;
					if ( settings != null )
					{
						configurationContext.WriteConfiguration( SectionName, settings );
					}
				}
				catch (ConfigurationException e)
				{
					ServiceHelper.LogError(serviceProvider, applicationUpdaterNode, e);
				}
				catch (InvalidOperationException e)
				{
					ServiceHelper.LogError(serviceProvider, applicationUpdaterNode, e);
				}
			}
		}

		/// <summary>
		/// Called when the configuration tool is preparing the configuration context.
		/// </summary>
		/// <param name="serviceProvider">The service provider instance.</param>
		/// <param name="configurationDictionary">The configuration dictionary.</param>
		public void BuildContext(IServiceProvider serviceProvider, ConfigurationDictionary configurationDictionary)
		{
			ApplicationUpdaterNode updaterNode = ServiceHelper.GetCurrentHierarchy(serviceProvider).FindNodeByType(typeof(ApplicationUpdaterNode)) as ApplicationUpdaterNode;
			if (updaterNode == null) { return; }            
			configurationDictionary[SectionName] = updaterNode; 
		}

		#endregion

		#region Private members

		/// <summary>
		/// Create the menu commands on the configuratin tool host.
		/// </summary>
		/// <param name="provider"></param>
		private static void CreateCommands(IServiceProvider provider)
		{
			IUIHierarchyService hierarchyService = provider.GetService(typeof(IUIHierarchyService)) as IUIHierarchyService;
			Debug.Assert(hierarchyService != null, "Could not get the IUIHierarchyService");
			
			IUIHierarchy currentHierarchy = hierarchyService.SelectedHierarchy;
			bool containsNode = currentHierarchy.ContainsNodeType(typeof(ConfigurationSectionCollectionNode));
			IMenuContainerService menuService = provider.GetService(typeof(IMenuContainerService)) as IMenuContainerService;
			Debug.Assert(menuService != null, "Could not get the IMenuContainerService");
			
			ConfigurationMenuItem item = new ConfigurationMenuItem( Resource.ResourceManager[ Resource.MessageKey.UpdaterApplicationBlockConfiguration ],
				new AddConfigurationSectionCommand(provider, typeof(ApplicationUpdaterNode), SectionName), 
				ServiceHelper.GetCurrentRootNode(provider), 
				Shortcut.None, Resource.ResourceManager[ Resource.MessageKey.UpdaterApplicationBlockConfiguration ], 
				InsertionPoint.New);
			item.Enabled = !containsNode;
			menuService.MenuItems.Add(item);
		}

		/// <summary>
		/// Register the node mapping on the host tool.
		/// </summary>
		/// <param name="serviceProvider">The service provider.</param>
		private static void RegisterNodeMaps(IServiceProvider serviceProvider)
		{
			INodeCreationService nodeCreationService = ServiceHelper.GetNodeCreationService(serviceProvider);
            
			Type nodeType = typeof(BitsDownloaderNode);
			NodeCreationEntry entry = NodeCreationEntry.CreateNodeCreationEntryNoMultiples(new AddChildNodeCommand(serviceProvider, nodeType), nodeType, typeof(BitsDownloaderProviderData), "BitsDownloaderNodeMap");
			nodeCreationService.AddNodeCreationEntry(entry);

		}

		/// <summary>
		/// Register the types that must be included for correct Xml Serialization.
		/// </summary>
		/// <param name="serviceProvider">The service provider instance.</param>
		private static void RegisterXmlIncludeTypes(IServiceProvider serviceProvider)
		{
			IXmlIncludeTypeService xmlIncludeTypeService = serviceProvider.GetService(typeof(IXmlIncludeTypeService)) as IXmlIncludeTypeService;
			Debug.Assert(xmlIncludeTypeService != null, "Could not find the IXmlIncludeTypeService");
			xmlIncludeTypeService.AddXmlIncludeType( ApplicationUpdaterSettings.SectionName, typeof(Configuration.BitsDownloaderProviderData));


		}

		#endregion
	}
}
