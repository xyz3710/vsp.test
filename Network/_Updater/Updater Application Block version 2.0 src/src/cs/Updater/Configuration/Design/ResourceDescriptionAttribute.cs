//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ResourceDescriptionAttribute.cs
//
// Contains the implementation of the resource description attribute.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.ComponentModel;

namespace Microsoft.ApplicationBlocks.Updater.Configuration.Design
{
	/// <devdoc>
	/// Represents a localized version of a DescriptionAttribute. It uses the local strings resouce to get its 
	/// description. This class cannot be inherited.
	/// </devdoc>
	[AttributeUsage(AttributeTargets.All)]
	internal sealed class ResourceDescriptionAttribute : DescriptionAttribute
	{
		#region Private members

		/// <summary>
		/// Wether the string has been replaced.
		/// </summary>
		private bool replaced;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a ResourceDescriptionAttribute with the message string.
		/// </summary>
		/// <param name="description">The message string.</param>
		public ResourceDescriptionAttribute(Resource.MessageKey description) : base(description.ToString())
		{
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The node description.
		/// </summary>
		public override string Description
		{
			get
			{
				if (!this.replaced)
				{
					this.replaced = true;
					base.DescriptionValue = Resource.ResourceManager[ base.Description ];
				}
				return base.Description;
			}
		}

		#endregion
	}
}