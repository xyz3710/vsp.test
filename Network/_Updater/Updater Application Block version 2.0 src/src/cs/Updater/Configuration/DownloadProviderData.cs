//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// DownloadProviderData.cs
//
// Contains the implementation of the downloader provider configuration data.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Configuration
{
	/// <summary>
	/// Defines abstract base class for the downloader provider configuration.
	/// </summary>
	[XmlRoot("downloader", Namespace=ApplicationUpdaterSettings.ConfigurationNamespace )]
	public abstract class DownloadProviderData : ProviderData
	{
		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		protected DownloadProviderData() : base()
		{
		}

		/// <summary>
		/// Creates a DownloadProviderData specifying the name.
		/// </summary>
		/// <param name="name"></param>
		protected DownloadProviderData( string name ) : base( name )
		{
		}

		#endregion
	}
}
