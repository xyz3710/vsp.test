//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// FilesCollection.cs
//
// This files contains the implementation of the files collection for the manifest.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Collections;


namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// Defines a collection of files with its own configuration.
	/// </summary>
	public class FilesCollection : CollectionBase 
	{
		#region Private members

		/// <summary>
		/// The base path for all the files.
		/// </summary>
		private string basePath;

		/// <summary>
		/// Whether hash comparison is enabled or not.
		/// </summary>
		private bool hashComparison;

		/// <summary>
		/// The EntLib hash provider reference.
		/// </summary>
		private string hashProviderName;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a FilesCollection using the information in the serialized manifest.
		/// </summary>
		/// <param name="files">The files collection in the manifest.</param>
		public FilesCollection( Xsd.ManifestFiles files )
		{
			if ( files != null && files.Files != null && files.Files.Length > 0 )
			{
				basePath = Utilities.FileUtility.AppendSlashUrlOrUnc( files.Base );
				hashComparison = ( files.HashComparison == Xsd.YesNoBoolType.Yes || files.HashComparison == Xsd.YesNoBoolType.True );
				hashProviderName = files.HashProviderName;
				for(int i=0;i<files.Files.Length;i++)
				{
					List.Add( new FileManifest( files.Files[i] ) );
				}
			}
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The base folder path.
		/// </summary>
		public string Base
		{
			get
			{
				return basePath;
			}
		}

		/// <summary>
		/// Indicates whether the hash comparison is enabled for all the files.
		/// </summary>
		public bool IsHashComparisonEnabled
		{
			get
			{
				return hashComparison;
			}
		}

		/// <summary>
		/// The reference to the EntLib hash provider.
		/// </summary>
		public string HashProvider
		{
			get 
			{
				return  hashProviderName;
			}
		}

		/// <summary>
		/// Indexer that returns a file for a given index.
		/// </summary>
		/// <param name="index">The index of the file to locate.</param>
		public FileManifest this[ int index ]
		{
			get
			{
				return (FileManifest)List[ index ];
			}
			set
			{
				List[ index ] = value;
			}
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Allows determining if the file is already contained in the collection.
		/// </summary>
		/// <param name="value">The FileManiest instance.</param>
		/// <returns>A boolean value indicating whether file is in the collection.</returns>
		public bool Contains( FileManifest value)
		{
			return List.Contains( value );
		}

		/// <summary>
		/// Adds a file to the file collection.
		/// </summary>
		/// <param name="value">The FileManiest instance.</param>
		public void Add( FileManifest value )
		{
			List.Add( value );
		}

		/// <summary>
		/// Removes the file from the file collection.
		/// </summary>
		/// <param name="value">The FileManiest instance.</param>
		public void Remove( FileManifest value )
		{
			List.Remove( value );
		}

		/// <summary>
		/// Insert the file at a specific index in the collection.
		/// </summary>
		/// <param name="index">The index to insert the file.</param>
		/// <param name="value">The FileManiest instance.</param>
		public void Insert( int index, FileManifest value )
		{
			List.Insert( index, value );
		}

		#endregion
	}	
}
