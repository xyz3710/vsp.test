//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ApplicationManifest.cs
//
// Contains the implementation of the information about the application within the manifest.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Xml;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// The information about the application within the manifest.
	/// </summary>
	public class ApplicationManifest
	{
		#region Declarations

		/// <summary>
		/// The deserialized class for the manifest.
		/// </summary>
		private Xsd.ManifestApplication manifestOfApplicationToUpdate;

		/// <summary>
		/// The application Id.
		/// </summary>
		private string applicationId;

		/// <summary>
		/// The application location folder.
		/// </summary>
		private string applicationUpdateLocation;

		#endregion

		#region Constructor

		/// <summary>
		/// Creates an ApplicationManifest using the deserialized manifest application information.
		/// </summary>
		/// <param name="application">The information about the application in the manifest.</param>
		public ApplicationManifest( Xsd.ManifestApplication application )
		{
			manifestOfApplicationToUpdate = application;
			applicationUpdateLocation = application.Location;
			applicationId = application.ApplicationId;
		}

		#endregion

		#region ApplicationManifest Members

		/// <summary>
		/// The application ID.
		/// </summary>
		public string ApplicationId
		{ 
			get { return applicationId  ; }
		}

		/// <summary>
		/// The entry point for the application.
		/// </summary>
		public string StartCommand 
		{
			get{ return manifestOfApplicationToUpdate.EntryPoint.File; }
		}

		/// <summary>
		/// The parameters for the application.
		/// </summary>
		public string StartParameters 
		{ 
			get { return manifestOfApplicationToUpdate.EntryPoint.Parameters; }
		}

		/// <summary>
		/// The folder location for the application.
		/// </summary>
		public string Location
		{
			get { return applicationUpdateLocation; }
			set { applicationUpdateLocation = value; }
		}

		/// <summary>
		/// The element defined in the XML manifest.
		/// </summary>
		public XmlElement Any
		{
			get { return manifestOfApplicationToUpdate.Any; }
		}

		/// <summary>
		/// The attributes defined in the XML manifest.
		/// </summary>
		public XmlAttribute[] Attributes
		{
			get { return manifestOfApplicationToUpdate.AnyAttr; }
		}
		#endregion

	}
}
