//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ManifestManager.cs
//
// Contains the implementation of the manifest manager.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Permissions;
using System.Security.Principal;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.ApplicationBlocks.Updater.Utilities;
using Microsoft.ApplicationBlocks.Updater.Xsd;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// This class is responsible for fetching the manifest from the specified location, 
	/// checking if the manifest has been applied and returning a list of valid manifests to
	/// apply.
	/// This class can be used as an instance member or as a static member. It is used as a static 
	/// member variable in the UpdaterManager. In order to support testability of the class we allow
	/// classes to use this as an instance member.
	/// </summary>
	internal sealed class ManifestManager
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ManifestManager()
		{
		}
		#endregion

		#region Public Methods

		/// <summary>
		/// Return all the manifests for the Uri and the application id.
		/// </summary>
		/// <param name="location">The Uri location for the manifest.</param>
		/// <param name="applicationId">The application id.</param>
		/// <returns>The manifest list.</returns>
		public Manifest[] GetManifests( Uri location, string applicationId )
		{
			if ( applicationId == null || ( applicationId != null && applicationId.Length == 0 ) )
			{
				Logger.LogAndThrowException( new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.ManifestManagerMustBeValidId ], "applicationId" ) );
			}

			return this.GetValidManifests( location, applicationId );
		}

		/// <summary>
		/// Save the manifest in the manifest registry so they can be retrieved in the next update.
		/// </summary>
		/// <param name="manifest">The Manifest instance.</param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter=true)]
		public void CommitUpdate( Manifest manifest )
		{
			string manifestPath = Path.Combine( new UpdaterConfigurationView().BasePath, "applications" );
			
			manifestPath = Path.Combine( manifestPath, manifest.Application.ApplicationId );
			if ( !Directory.Exists( manifestPath ) )
			{
				Directory.CreateDirectory( manifestPath );
			}
			manifestPath = Path.Combine( manifestPath, manifest.ManifestId.ToString() + ".appliedmanifest" );

			try
			{
				using(Stream stream = new FileStream( manifestPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read ) )
				{
					BinaryFormatter formatter = new BinaryFormatter();
					formatter.Serialize( stream, manifest );
				}
			}
			catch( Exception ex)
			{
				File.Delete( manifestPath );
				Logger.LogAndThrowException( ex );
			}
		}

		#endregion

		#region Private Members

		/// <summary>
		/// Return the Manifest instance for the application id using the remote location.
		/// </summary>
		/// <param name="location">The Uri to the location of the manifest.</param>
		/// <param name="applicationId">The application id.</param>
		/// <returns></returns>
		private Manifest[] GetValidManifests( Uri location, string applicationId )
		{
			if( location == null ) 
			{
				Logger.LogAndThrowException( new ArgumentNullException( Resource.ResourceManager[ Resource.MessageKey.ArgumentMissing1, "location" ] ) );
			}

			// Donwload and validate the primary manifest
			Xsd.Manifest primaryManifest = (Xsd.Manifest)ValidateAndDeserialize( typeof( Xsd.Manifest ), location, "Microsoft.ApplicationBlocks.Updater.Xsd.Manifest.xsd" );

			// Download, validate and process each manifest included by the primary manifest
			int count = ( primaryManifest.IncludedManifests != null ) ? primaryManifest.IncludedManifests.Manifest.Length : 0;
			
			// Allocate an array for holding the primary + included manifests
			ArrayList validManifests = new ArrayList( count + 1 );
			
			// Create the manifest from the Xsd class
			Manifest primaryManifestTemp = new Manifest( primaryManifest );
							
			if ( String.Compare( primaryManifestTemp.Application.ApplicationId, applicationId, true, CultureInfo.InvariantCulture ) == 0 )
			{
				PopulateValidManifestList(primaryManifestTemp, primaryManifest, validManifests);

				// Checking for duplicate included manifests.
				ArrayList includedManifestIds = new ArrayList(count);
				for( int index = 0; index < count; index++ )
				{
					if( includedManifestIds.Contains( primaryManifest.IncludedManifests.Manifest[index].ManifestId ) )
					{
						Logger.LogAndThrowException( new ApplicationUpdaterException( Resource.ResourceManager[ Resource.MessageKey.DuplicatedManifestId ] ) );
					}
					else
					{
						includedManifestIds.Add(primaryManifest.IncludedManifests.Manifest[index].ManifestId);
					}
				}

				if( count > 0 )
				{
					for( int i = 0; i < count; i++ )
					{
						try
						{
							UriBuilder ub = new UriBuilder( location );
							ub.Path = Path.Combine( Path.GetDirectoryName( ub.Path ), primaryManifest.IncludedManifests.Manifest[i].Location );
					
							Xsd.Manifest xsdManifest = (Xsd.Manifest)ValidateAndDeserialize( typeof( Xsd.Manifest), ub.Uri, "Microsoft.ApplicationBlocks.Updater.Xsd.Manifest.xsd" );
							Manifest includedManifest = new Manifest( xsdManifest );
							if( String.Compare( includedManifest.Application.ApplicationId, applicationId, true, CultureInfo.InvariantCulture ) == 0 )
							{
								PopulateValidManifestList(includedManifest, primaryManifest, validManifests);
							}
						}
						catch( Exception ex )
						{
							Logger.LogAndThrowException( ex );
						}
					}
				}
			}

			return (Manifest[])validManifests.ToArray( typeof( Manifest ) );
		}

		/// <summary>
		/// Check wether the manifest is applied locally and add the manifest to a list or not.
		/// </summary>
		/// <param name="manifestToApply">The instance of the Manifest to apply.</param>
		/// <param name="primaryManifest">The primary manifest instance.</param>
		/// <param name="validManifests">The valid manifest list.</param>
		private void PopulateValidManifestList(Manifest manifestToApply, Xsd.Manifest primaryManifest, ArrayList validManifests)
		{
			if ( !ManifestApplied( manifestToApply.Application.ApplicationId, manifestToApply.ManifestId ) )
			{
				manifestToApply.Apply = 
					(primaryManifest.Mandatory == YesNoBoolType.True) || 
						(primaryManifest.Mandatory == YesNoBoolType.Yes);
							
				// Make a hash comparison
				if( manifestToApply.Files.IsHashComparisonEnabled )
				{
					RemoveAppliedFiles( manifestToApply );
				}
							
				validManifests.Add( manifestToApply );
			}
		}

		/// <summary>
		/// Removes the manifest from the list of applied manifests.
		/// </summary>
		/// <param name="manifest">The instance to the Manifest.</param>
		private void RemoveAppliedFiles( Manifest manifest )
		{
			string rootPath = manifest.Application.Location;

			for( int index = 0; index < manifest.Files.Count; index++ )
			{
				FileManifest file = manifest.Files[index];
					
				string filePath = Path.Combine( rootPath, file.Source );

				if( file.Hash == null )
				{
					Logger.LogAndThrowException( new ApplicationUpdaterException( Resource.ResourceManager[ Resource.MessageKey.FileHashIsNull, file.Source ] ) );
				}

				if( FileNotChanged( manifest.Files.HashProvider, file.Hash, filePath ) )
				{
					manifest.Files.RemoveAt( index ); 
					index--;
				}
			}
		}

		/// <summary>
		/// Check the manifest file hash with the calculated file hash to detect file changes.
		/// </summary>
		/// <param name="hashProviderName">The EntLib hash provider name.</param>
		/// <param name="hash">The calculated hash in the manifest.</param>
		/// <param name="file">The flie that will be checked.</param>
		/// <returns><c>true</c>, if the file have not changed, otherwise returns <c>true</c>,</returns>
		private bool FileNotChanged( string hashProviderName, string hash, string file )
		{
			if( File.Exists( file ) )
			{
				return FileHashHelper.CompareHash( hashProviderName, file, hash );			
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Download and validates the manifest using the Uri location.
		/// </summary>
		/// <param name="type">The type that represents the manifest class.</param>
		/// <param name="location">The location of the manifest.</param>
		/// <param name="schemaResource">The resource where the embedded XSD resides in the assembly.</param>
		/// <returns>The Manifest instance.</returns>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter=true)]
		private object ValidateAndDeserialize( Type type, Uri location, string schemaResource )
		{
			object result = null;
			try
			{
				string doc = DownloadFile( location );
				using ( Stream schema = Assembly.GetExecutingAssembly().GetManifestResourceStream( schemaResource ) )
				{
					SchemaValidator validator = new SchemaValidator( doc, schema );
					if ( !validator.Validate() )
					{
						Logger.LogAndThrowException( new ApplicationUpdaterException( Resource.ResourceManager[ Resource.MessageKey.ManifestSchemaError, location, schemaResource ] ) );
					}

					XmlSerializer xser = new XmlSerializer( type );
					result = xser.Deserialize( new XmlTextReader( doc, XmlNodeType.Document, null) );
				}
			}
			catch( Exception ex )
			{
				Logger.LogAndThrowException( ex );
			}

			return result;
		}

		/// <summary>
		/// Used to detect whether the manifest is applied or not.
		/// </summary>
		/// <param name="applicationID">The application id.</param>
		/// <param name="manifestID">The manifest id.</param>
		/// <returns><c>true</c>, if the manifest is applied, otherwise returns <c>false</c>.</returns>
		private bool ManifestApplied( string applicationID, Guid manifestID )
		{
			// Check if the manifest has already been applied
			string manifestPath = Path.Combine( new UpdaterConfigurationView().BasePath, "applications" );
			manifestPath = Path.Combine( manifestPath, applicationID );
			manifestPath = Path.Combine( manifestPath, manifestID.ToString() + ".appliedmanifest" );

			// If the manifest file exists, the manifest has been applied
			// Or it's being applied if there's an associated task related to the manifest
			return File.Exists( manifestPath ) || RegistryManager.Current.GetByManifestID( manifestID ) != null;
		}

		/// <summary>
		/// Download a file using HTTP by a given Uri.
		/// </summary>
		/// <param name="uri">The uri of the file to download.</param>
		/// <returns>The contents of the file.</returns>
		private string DownloadFile( Uri uri)
		{
			WebRequest request = WebRequest.Create( uri );

			AuthenticationType method = AuthenticationType.Anonymous;
			if( new UpdaterConfigurationView().ManifestManager != null )
			{
				method = new UpdaterConfigurationView().ManifestManager.AuthenticationType;
			}

			switch( method )
			{
				case AuthenticationType.Integrated:
				{
					request.ConnectionGroupName = WindowsIdentity.GetCurrent().Name;
					request.Credentials = CredentialCache.DefaultCredentials;
					break;
				}
				case AuthenticationType.Basic:
				{
					string windowsAuthType = "Basic";

					CredentialCache cache = new CredentialCache();
					cache.Add( 
						uri, 
						windowsAuthType, 
						new NetworkCredential( 
						new UpdaterConfigurationView().ManifestManager.UserName,
						new UpdaterConfigurationView().ManifestManager.Password ) );
					
					request.Credentials = cache;
					request.PreAuthenticate = true;
					break;
				}
				case AuthenticationType.Anonymous:
				{
					request.Credentials = CredentialCache.DefaultCredentials;
					break;
				}

			}
			
			string response = String.Empty;
			
			using(WebResponse res = request.GetResponse())
			{
				using( StreamReader reader = new StreamReader( res.GetResponseStream(), true ) )
				{
					response = reader.ReadToEnd();
				}
			}
			return response;
		}

		#endregion
	}
}