//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// Manifest.cs
//
// Contains the implementation of the manifest class.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Globalization;
using System.Security.Permissions;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// This class mantains all the information of the manifest that was retrieved using the URI.
	/// </summary>
	[Serializable]
	public class Manifest : ISerializable
	{
		#region Private fields

		/// <summary>
		/// The manifest class that was deserialized.
		/// </summary>
		private Xsd.Manifest updateManifest;

		/// <summary>
		/// The manifest id.
		/// </summary>
		private Guid updaterManifestId;

		/// <summary>
		/// The collection of files that was downloaded.
		/// </summary>
		private FilesCollection filesToUpdate;

		/// <summary>
		/// The configuration for the downloader included in the manifest.
		/// </summary>
		private ManifestDownloaderProviderData downloaderConfigInformation;

		/// <summary>
		/// The application information for the manifest.
		/// </summary>
		private ApplicationManifest manifestOfApplicationToUpdate;

		/// <summary>
		/// The information about the activation processors configured in the manifest.
		/// </summary>
		private ActivationProcessorProviderDataCollection activationProcessorsConfigInformation;

		/// <summary>
		/// Indicates whether the manifest will be applied.
		/// </summary>
		private bool apply;

		#endregion

		#region Public Constructors

		/// <summary>
		/// Creates a manifest using the deserialized manifest instance.
		/// </summary>
		/// <param name="manifest">The XSD manifest to wrap.</param>
		public Manifest( Xsd.Manifest manifest )
		{
			updateManifest = manifest;
		}

		#endregion

		#region Manifest Members

		/// <summary>
		/// The manifest ID.
		/// </summary>
		public Guid ManifestId
		{
			get
			{
				if ( updaterManifestId == Guid.Empty )
				{
					updaterManifestId = new Guid( updateManifest.ManifestId );
				}
				return updaterManifestId;
			}
		} 

		/// <summary>
		/// Initalizes the manifest by setting the XmlNode for the configuration.
		/// </summary>
		/// <param name="config">The node containinf the manifest representation</param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter=true)]
		public void Init(XmlNode config)
		{
			XmlSerializer xser = new XmlSerializer( typeof( Xsd.Manifest ) );
			updateManifest = (Xsd.Manifest)xser.Deserialize( new XmlNodeReader( config ) );
		}

		/// <summary>
		/// The description of the manifest.
		/// </summary>
		public string Description
		{
			get
			{
				return updateManifest.Description;
			}

		}

		/// <summary>
		/// Indicates whether the manifest will be applied.
		/// </summary>
		public bool Apply
		{
			get
			{
				return apply;
			}
			set
			{
				apply = value;
			}
		} 

		/// <summary>
		/// Indicates whether the manifest is mandatory.
		/// </summary>
		public bool Mandatory
		{
			get
			{
				return updateManifest.Mandatory == Xsd.YesNoBoolType.True || updateManifest.Mandatory == Xsd.YesNoBoolType.Yes;
			}
		}

		/// <summary>
		/// The collection of files defined in the manifest.
		/// </summary>
		public FilesCollection Files
		{
			get
			{
				if ( filesToUpdate == null )
				{
					filesToUpdate = new FilesCollection( updateManifest.Files );
				}
				return filesToUpdate;
			}
		} 

		/// <summary>
		/// The configuration for the downloader defined in the manifest.
		/// </summary>
		public ManifestDownloaderProviderData Downloader
		{
			get
			{
				if ( downloaderConfigInformation == null )
				{
					if ( updateManifest.Downloader != null )
					{
						downloaderConfigInformation = new ManifestDownloaderProviderData( updateManifest.Downloader.Name, updateManifest.Downloader );
					}
				}
				return downloaderConfigInformation;
			}
		} 

		/// <summary>
		/// The information for the application.
		/// </summary>
		public ApplicationManifest Application
		{
			get
			{
				if ( manifestOfApplicationToUpdate == null )
				{
					manifestOfApplicationToUpdate = new ApplicationManifest( updateManifest.Application );
				}
				return manifestOfApplicationToUpdate;
			}
		} 

		/// <summary>
		/// The activation processors configuration.
		/// </summary>
		public ActivationProcessorProviderDataCollection ActivationProcessors
		{
			get
			{
				if ( activationProcessorsConfigInformation == null )
				{
					if ( updateManifest.ActivationProcess != null )
					{
						if( updateManifest.ActivationProcess.Tasks == null )
						{
							activationProcessorsConfigInformation = new ActivationProcessorProviderDataCollection();
						}
						else
						{
							activationProcessorsConfigInformation = new ActivationProcessorProviderDataCollection();
							foreach( Xsd.ActivationProcessorProviderData activationProcessor in updateManifest.ActivationProcess.Tasks )
						{
									activationProcessorsConfigInformation.Add( 
										new ActivationProcessorProviderData( activationProcessor.Name, activationProcessor ) );
						}
					}
				}
				}
				return activationProcessorsConfigInformation;
			}
		} 

		#endregion

		#region ISerializable Members

		/// <summary>
		/// Constructor used by the serialization infrastructure.
		/// </summary>
		/// <param name="info">The serialization information.</param>
		/// <param name="context">The serialization context.</param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter=true)]
		protected Manifest(SerializationInfo info, StreamingContext context)
		{
			XmlSerializer xser = new XmlSerializer( typeof( Xsd.Manifest ) );
			updateManifest = (Xsd.Manifest)xser.Deserialize( new StringReader( info.GetString( "_manifest" ) ) );
			apply = info.GetBoolean( "_apply" );
		}

		/// <summary>
		/// Method used by the serialization infrastructure.
		/// </summary>
		/// <param name="info">The serialization information.</param>
		/// <param name="context">The serialization context.</param>
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			StringBuilder sb = new StringBuilder( 300 );
			StringWriter sw = new StringWriter( sb, CultureInfo.InvariantCulture );
			XmlSerializer xser = new XmlSerializer( typeof( Xsd.Manifest ) );
			xser.Serialize( sw, updateManifest );
			info.AddValue( "_manifest", sb.ToString() );
			info.AddValue("_apply", apply );
		}

		#endregion
	}
}
