//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// Events.cs
//
// Contains the definition of all the delegates and EventArgs derived classes.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using Microsoft.ApplicationBlocks.Updater;

namespace Microsoft.ApplicationBlocks.Updater
{
	#region EventArgs classes

	#region TaskEventArgs class

	/// <summary>
	/// Used to notify information about an UpdaterTask.
	/// </summary>
	public class TaskEventArgs : EventArgs
	{
		private UpdaterTask currentUpdaterTask;

		/// <summary>
		/// Constructor for a TaskEventArgs.
		/// </summary>
		/// <param name="task">The UpdaterTask for initialization.</param>
		public TaskEventArgs( UpdaterTask task ) : base()
		{
			currentUpdaterTask = task;
		}

		/// <summary>
		/// Returns the updater task.
		/// </summary>
		public UpdaterTask Task
		{
			get { return currentUpdaterTask; }
		}
	}

	#endregion

	#region ActivationProcessorEventArgs class
	
	/// <summary>
	/// Used to notify events occurring during activation.
	/// </summary>
	public class ActivationProcessorEventArgs: TaskEventArgs
	{
		/// <summary>
		/// The processor instance.
		/// </summary>
		private IActivationProcessor processor;

		/// <summary>
		/// Constructor for ActivationProcessorEventArgs.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <param name="processor">The <see cref="IActivationProcessor"/> instance.</param>
		public ActivationProcessorEventArgs( UpdaterTask task, IActivationProcessor processor ) : base( task )
		{
			this.processor = processor;
		}

		/// <summary>
		/// The ActivationProcessor instance raising the event.
		/// </summary>
		public IActivationProcessor Processor
		{
			get { return processor; }
		}
	}

	#endregion

	#region ActivationTaskErrorEventArgs class

	/// <summary>
	/// Used to notify events about activation errors.
	/// </summary>
	public class ActivationTaskErrorEventArgs : ActivationProcessorEventArgs
	{
		/// <summary>
		/// Exception detected during the activation.
		/// </summary>
		private Exception exception;

		/// <summary>
		/// Constructor for the ActivationTaskErrorEventArgs.
		/// </summary>
		/// <param name="task">The <see cref="UpdaterTask"/> instance.</param>
		/// <param name="processor">The <see cref="IActivationProcessor"/> instance.</param>
		/// <param name="exception">The Exception instance.</param>
		public ActivationTaskErrorEventArgs( UpdaterTask task, IActivationProcessor processor, Exception exception ) : base( task, processor )
		{
			this.exception = exception;
		}

		/// <summary>
		/// Exception thrown during the activation.
		/// </summary>
		public Exception Exception
		{
			get { return exception; }
		}
	}

	#endregion

	#region ActivationTaskCompleteEventArgs class

	/// <summary>
	/// Used to notify activation completion.
	/// </summary>
	public class ActivationTaskCompleteEventArgs : TaskEventArgs
	{
		/// <summary>
		/// Indicates whether the activation was successful.
		/// </summary>
		private bool success;

		/// <summary>
		/// Constructor for the ActivationTaskCompleteEventArgs.
		/// </summary>
		/// <param name="task">The <see cref="UpdaterTask"/> instance.</param>
		/// <param name="success">Result of the activation process</param>
		public ActivationTaskCompleteEventArgs( UpdaterTask task, bool success ) : base( task )
		{
			this.success = success;
		}

		/// <summary>
		/// Indicates whether the activation was successful.
		/// </summary>
		public bool Success
		{
			get { return success; }
		} 
	}
	
	#endregion

	#region BaseDownloadProgrssEventArgs class

	/// <summary>
	/// Base class used to provide information about the download progress.
	/// </summary>
	public class BaseDownloadProgressEventArgs : EventArgs	
	{
		/// <summary>
		/// The total bystes that will be transfered.
		/// </summary>
		private long bytesTotal;

		/// <summary>
		/// The amount of bytes that have been transfered.
		/// </summary>
		private long bytesTransferred;

		/// <summary>
		/// The total files that will be transfered.
		/// </summary>
		private int filesTotal;
		
		/// <summary>
		/// The files that have been transfered.
		/// </summary>
		private int filesTransferred;

		/// <summary>
		/// Indicates whether the operation was canceled.
		/// </summary>
		private bool cancel = false;

		/// <summary>
		/// Constructor for the BaseDownloadProgressEventArgs.
		/// </summary>
		/// <param name="bytesTotal">The total bytes to be transferred.</param>
		/// <param name="bytesTransferred">Number of bytes that have been transferred.</param>
		/// <param name="filesTotal">The total number of files to be transferred.</param>
		/// <param name="filesTransferred">Number of files that have been transferred.</param>
		public BaseDownloadProgressEventArgs( long bytesTotal, long bytesTransferred, int filesTotal, int filesTransferred )
		{
			this.bytesTotal = bytesTotal;
			this.bytesTransferred = bytesTransferred;
			this.filesTotal = filesTotal;
			this.filesTransferred = filesTransferred;
		}

		/// <summary>
		/// The total bytes to be transferred.
		/// </summary>
		public long BytesTotal
		{
			get { return bytesTotal; }
		}

		/// <summary>
		/// Number of bytes that have been transferred.
		/// </summary>
		public long BytesTransferred
		{
			get { return bytesTransferred; }
		}

		/// <summary>
		/// The total number of files to be transferred.
		/// </summary>
		public int FilesTotal
		{
			get { return filesTotal; }
		}

		/// <summary>
		/// Number of files that have been transferred.
		/// </summary>
		public int FilesTransferred
		{
			get { return filesTransferred; }
		}
		
		/// <summary>
		/// Indicates whether the operation was cancelled or not.
		/// </summary>
		public bool Cancel
		{
			get { return cancel; }
			set { cancel = cancel || value; }
		}
	}

	#endregion

	#region DownloadTaskProgressEventArgs class

	/// <summary>
	/// Used to notify events about download progess.
	/// </summary>
	public class DownloadTaskProgressEventArgs : BaseDownloadProgressEventArgs	
	{
		/// <summary>
		/// The updater task.
		/// </summary>
		private UpdaterTask task;

		/// <summary>
		/// Constructor for the DownloadTaskProgressEventArgs.
		/// </summary>
		/// <param name="bytesTotal">The total bytes to be transferred.</param>
		/// <param name="bytesTransferred">Number of bytes that have been transferred.</param>
		/// <param name="filesTotal">The total number of files to be transferred.</param>
		/// <param name="filesTransferred">Number of files that have been transferred.</param>
		/// <param name="task">The <see cref="UpdaterTask"/> instance.</param>
		public DownloadTaskProgressEventArgs( long bytesTotal, long bytesTransferred, int filesTotal, int filesTransferred, 
			UpdaterTask task ) : base( bytesTotal, bytesTransferred, filesTotal, filesTransferred )
		{
			this.task = task;
		}

		/// <summary>
		/// Returns the UpdaterTask.
		/// </summary>
		public UpdaterTask Task
		{
			get { return task; }
		}
	}

	#endregion

	#region DownloadTaskErrorEventArgs class

	/// <summary>
	/// Used to provide information about download errors.
	/// </summary>
	public class DownloadTaskErrorEventArgs: TaskEventArgs
	{
		/// <summary>
		/// The exception received.
		/// </summary>
		private Exception exception;

		/// <summary>
		/// Constructor for the DownloadTaskErrorEventArgs.
		/// </summary>
		/// <param name="task">The <see cref="UpdaterTask"/> instance.</param>
		/// <param name="exception">The exception information.</param>
		public DownloadTaskErrorEventArgs( UpdaterTask task, Exception exception ) : base( task )
		{
			this.exception = exception;
		}

		/// <summary>
		/// The exception received.
		/// </summary>
		public Exception Exception
		{
			get { return exception; }
		}
	}

	#endregion
	
	#endregion
    
	#region Download events

	/// <summary>
	/// Event handler for the DownloadTaskProgressEvent event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void DownloadTaskProgressEventHandler( object sender, DownloadTaskProgressEventArgs e );

	/// <summary>
	/// Event handler for the DownloadTaskStarted event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void DownloadTaskStartedEventHandler( object sender, TaskEventArgs e );

	/// <summary>
	/// Event handler for the DownloadTaskCompleted event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void DownloadTaskCompletedEventHandler( object sender, TaskEventArgs e );

	/// <summary>
	/// Event handler for the DownloadTaskError event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void DownloadTaskErrorEventHandler( object sender, DownloadTaskErrorEventArgs e );

	#endregion

	#region Activation events

	/// <summary>
	/// Event handler for the ActivationTaskInitializing event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void ActivationTaskInitializingEventHandler( object sender, TaskEventArgs e );

	/// <summary>
	/// Event handler for the ActivationTaskInitializationAborted event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void ActivationTaskInitializationAbortedEventHandler( object sender, ActivationTaskErrorEventArgs e );
	
	/// <summary>
	/// Event handler for the ActivationTaskStarted event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void ActivationTaskStartedEventHandler( object sender, TaskEventArgs e );
	
	/// <summary>
	/// Event handler for the ActivationTaskCompleted event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void ActivationTaskCompletedEventHandler( object sender, ActivationTaskCompleteEventArgs e );
	
	/// <summary>
	/// Event handler for the ActivationTaskError event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void ActivationTaskErrorEventHandler( object sender, ActivationTaskErrorEventArgs e );

	#endregion
}
