//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ProcessorFactory.cs
//
// Contains the implementation of the activation processor factory.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using Microsoft.Practices.EnterpriseLibrary.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Utilities
{
	/// <summary>
	/// Helper class to create instances of activation processors.
	/// </summary>
	public sealed class ProcessorFactory
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		private ProcessorFactory()
		{
		}

		#endregion

		#region Static members

		/// <summary>
		/// Creates an instance of a processor using the specified processor type.
		/// </summary>
		/// <param name="typeName">The name of the processor type.</param>
		/// <returns>The activation processor.</returns>
		public static IActivationProcessor Create( string typeName )
		{
			Type type = Type.GetType( typeName, true );
			return System.Activator.CreateInstance( type ) as IActivationProcessor;
		}

		#endregion
	}
}
