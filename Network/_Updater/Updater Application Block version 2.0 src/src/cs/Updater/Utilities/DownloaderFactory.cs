//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// DownloaderFactory.cs
//
// Contains the implementation of the downloader factory.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Utilities
{
	/// <summary>
	/// EntLib factory provider implementation.
	/// </summary>
	public sealed class DownloaderFactory : ProviderFactory
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		public DownloaderFactory() : 
			base( "Downloader factory", ConfigurationManager.GetCurrentContext(), typeof(IDownloader)  )
		{
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Creates an instance of the downloader using its type.
		/// </summary>
		/// <param name="downloaderProviderName">Name of the downloader to create.</param>
		/// <returns>The instance of the new downloader.</returns>
		public IDownloader CreateDownloader( string downloaderProviderName )
		{
			return (IDownloader)base.CreateInstance(downloaderProviderName);
		}

		#endregion

		#region Protected members

		/// <summary/>
		protected override ConfigurationView CreateConfigurationView()
		{
			return new UpdaterConfigurationView(ConfigurationContext);
		}

		/// <summary/>
		protected override Type GetConfigurationType( string downloadProviderName ) 
		{
			UpdaterConfigurationView updaterConfigurationView = (UpdaterConfigurationView)CreateConfigurationView();
			DownloadProviderData downloadProviderData = updaterConfigurationView.GetDownloadProviderData(downloadProviderName);
			return GetType(downloadProviderData.TypeName);
		}

		#endregion
	}

}
