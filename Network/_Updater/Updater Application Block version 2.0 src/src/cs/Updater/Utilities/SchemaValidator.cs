//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// SchemaValidator.cs
//
// Contains the implementation of the schema validator.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace Microsoft.ApplicationBlocks.Updater.Utilities
{
	/// <summary>
	/// Helper class to perform schema validations.
	/// </summary>
	public sealed class SchemaValidator
	{
		#region Private members
        
		/// <summary>
		/// The schemas that will be used for validation.
		/// </summary>
		private ArrayList schemas = new ArrayList(2);

		/// <summary>
		/// The errors detected during the validation.
		/// </summary>
		private ArrayList errors = new ArrayList(5);

		/// <summary>
		/// Whether the document is valid or not.
		/// </summary>
		private bool isValid;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates an instance of the SchemaValidator using the document and the schemas.
		/// </summary>
		/// <param name="document">The document to validate.</param>
		/// <param name="schemas">A list of schemas to validate the document against.</param>
		public SchemaValidator( string document, params Stream[] schemas )
		{
			this.schemas.Add( document );
			foreach(Stream s in schemas)
			{
				this.schemas.Add( s );
			}
			isValid = true;
		}

		#endregion

		#region Public members

		/// <summary>
		/// Validates the document and returns the result of the validation.
		/// </summary>
		/// <returns><c>true</c> if the document have succeeded the validation, otherwise <c>false</c>.</returns>
		public bool Validate()
		{
			errors.Clear();
			XmlValidatingReader vr = null;
			object doc = schemas[0];
			if ( doc is Stream)
			{
				vr = new XmlValidatingReader( (Stream)doc, XmlNodeType.Element, null );
			}
			else if ( doc is String )
			{
				vr = new XmlValidatingReader( (string)doc, XmlNodeType.Element, null );
			}

			try
			{
				for(int i=1;i<schemas.Count;i++)
				{
					vr.Schemas.Add( null, new XmlTextReader( (Stream)schemas[i] ) );
				}

				vr.ValidationEventHandler += new System.Xml.Schema.ValidationEventHandler(ValidationEventHandler);
			
				while( vr.Read() );
				return isValid;
			}
                        finally
			{
				vr.Close();
			}
		}

		/// <summary>
		/// Contains the validation errors encountered in the last validation operation.
		/// </summary>
		public ValidationEventArgs[] Errors
		{
			get 
			{ 
				return (ValidationEventArgs[])errors.ToArray(typeof(ValidationEventArgs) ); 
			}
		}

		/// <summary>
		/// Helper method to capture the event handlers.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The information about the event.</param>
		private void ValidationEventHandler(object sender, System.Xml.Schema.ValidationEventArgs e)
		{
			errors.Add( e );
			if ( e.Severity == XmlSeverityType.Warning )
			{
				return;
			}
			isValid = false;
		}

		#endregion
	}
}
