//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// DownloadManager.cs
//
// Contains the implementation of the downloader manager.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Reflection;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.ApplicationBlocks.Updater.Utilities;

namespace Microsoft.ApplicationBlocks.Updater.Downloader
{
	/// <summary>
	/// Performs the download operations by instancing and managind the configured downloader.
	/// Also channels and raises the downloader events.
	/// </summary>
	internal sealed class DownloadManager
	{
		#region Constructors 

		/// <summary>
		/// Default constructor.
		/// </summary>
		public DownloadManager()
		{
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Synchronously submits a task to the downloader.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <param name="maxWaitTime">The maximum wait time for the task.</param>
		public void SubmitTask(UpdaterTask task, TimeSpan maxWaitTime)
		{
			// If there's no files to download
			// Consider the download as done
			if ( task.Manifest.Files.Count == 0 )
			{
				OnDownloadCompleted( null, new TaskEventArgs( task ) );
				return;
			}

			IDownloader downloader = GetDownloader( task );
			try
			{
				downloader.Download( task, maxWaitTime );
			}
			finally
			{
				UnregisterEvents(downloader);
			}
		}

		/// <summary>
		/// Asynchronously submits a task to the downloader.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		public void SubmitTaskAsync( UpdaterTask task )
		{
			// If there's no files to download
			// Consider the download as done
			if ( task.Manifest.Files.Count == 0 )
			{
				OnDownloadCompleted( null, new TaskEventArgs( task ) );
				return;
			}

			IDownloader downloader = GetDownloader( task );
			downloader.BeginDownload( task );
		}

		/// <summary>
		/// Cancels a pending task.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <returns></returns>
		public bool EndTask( UpdaterTask task )
		{
			IDownloader downloader = GetDownloader( task );
			try
			{
				return downloader.CancelDownload( task );
			}
			finally
			{
				UnregisterEvents(downloader);
			}
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Return the downloader instance for a specific UpdaterTask.
		/// </summary>
		/// <param name="task">The UpdaterTask.</param>
		/// <returns>The Downloader instance.</returns>
		private IDownloader GetDownloader( UpdaterTask task )
		{
			DownloaderFactory df = new DownloaderFactory();
			IDownloader downloader = null; 

			try
			{
				if ( task.Manifest.Downloader == null || task.Manifest.Downloader.Name == null) 
				{
					downloader = df.CreateDownloader( Resource.ResourceManager[Resource.MessageKey.BitsDownloaderNode] );
				}
				else
				{
					downloader = df.CreateDownloader( task.Manifest.Downloader.Name ) ;
				}

			}
			catch( ApplicationUpdaterException)
			{
				throw;
			}
			catch( Exception ex)
			{
				throw new ApplicationUpdaterException( Resource.ResourceManager[ Resource.MessageKey.CannotCreateDownloader ], ex );
			}

			downloader.DownloadStarted += new DownloadTaskStartedEventHandler( OnDownloadStarted );
			downloader.DownloadProgress += new DownloadTaskProgressEventHandler( OnDownloadProgress );
			downloader.DownloadCompleted += new DownloadTaskCompletedEventHandler( OnDownloadCompleted );
			downloader.DownloadError += new DownloadTaskErrorEventHandler( OnDownloadError );

			return downloader;
		}

		/// <summary>
		/// Returns the Downloader configuration data for the downloader type especified.
		/// </summary>
		/// <param name="downloaderType">The downloader type.</param>
		/// <returns>The downloader configuration data.</returns>
		private DownloadProviderData GetApplicationConfig( string downloaderType )
		{
			foreach( DownloadProviderData config in new UpdaterConfigurationView().Downloaders )
			{
				if( config.TypeName == downloaderType )
					return config;
			}

			return null;
		}

		#endregion

		#region Event handling and forwarding

		/// <summary>
		/// Notifies about downloader progress.
		/// </summary>
		public event DownloadTaskProgressEventHandler DownloadProgress;

		/// <summary>
		/// Notifies about a started downloading task.
		/// </summary>
		public event DownloadTaskStartedEventHandler DownloadStarted;

		/// <summary>
		/// Notifies about completion of the download tasks.
		/// </summary>
		public event DownloadTaskCompletedEventHandler DownloadCompleted;

		/// <summary>
		/// Notifies about errors in downloader tasks.
		/// </summary>
		public event DownloadTaskErrorEventHandler DownloadError;

		/// <summary>
		/// Private method used to handle the downloader event.
		/// </summary>
		/// <param name="sender">The downloader instance.</param>
		/// <param name="e">The information for the event.</param>
		private void OnDownloadCompleted( object sender, TaskEventArgs e)
		{
			e.Task.State = UpdaterTaskState.Downloaded;
			if ( DownloadCompleted != null )
			{
				DownloadCompleted( this, e );
			}
			if ( sender is IDownloader )
			{
				IDownloader downloader = (IDownloader)sender;
				UnregisterEvents(downloader);
			}
		}

		/// <summary>
		/// Private method used to handle the downloader event.
		/// </summary>
		/// <param name="sender">The downloader instance.</param>
		/// <param name="e">The information for the event.</param>
		private void OnDownloadStarted( object sender, TaskEventArgs e)
		{
			e.Task.State = UpdaterTaskState.Downloading;
			if ( DownloadStarted != null)
			{
				DownloadStarted( this, e );
			}
		}

		/// <summary>
		/// Private method used to handle the downloader event.
		/// </summary>
		/// <param name="sender">The downloader instance.</param>
		/// <param name="e">The information for the event.</param>
		private void OnDownloadProgress( object sender, DownloadTaskProgressEventArgs e)
		{
			if ( DownloadProgress != null )
			{
				DownloadProgress( this, e );
			}
		}

		/// <summary>
		/// Private method used to handle the downloader event.
		/// </summary>
		/// <param name="sender">The downloader instance.</param>
		/// <param name="e">The information for the event.</param>
		private void OnDownloadError( object sender, DownloadTaskErrorEventArgs e)
		{
			e.Task.State = UpdaterTaskState.DownloadError;
			if ( DownloadError != null )
			{
				DownloadError( this, e );
			}
			IDownloader downloader = (IDownloader)sender;
			UnregisterEvents(downloader);
		}

		/// <summary>
		/// Method to unregister Downloader events.
		/// </summary>
		/// <param name="downloader">
		/// Downloader instance to unregister events from.
		/// </param>
		private void UnregisterEvents(IDownloader downloader)
		{
			downloader.DownloadStarted -= new DownloadTaskStartedEventHandler( OnDownloadStarted );
			downloader.DownloadProgress -= new DownloadTaskProgressEventHandler( OnDownloadProgress );
			downloader.DownloadCompleted -= new DownloadTaskCompletedEventHandler( OnDownloadCompleted );
			downloader.DownloadError -= new DownloadTaskErrorEventHandler( OnDownloadError );
		}

		#endregion
	}
}
