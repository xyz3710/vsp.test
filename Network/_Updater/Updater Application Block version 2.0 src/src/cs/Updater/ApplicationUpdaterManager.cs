//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ApplicationUpdaterManager.cs
//
// Contains the implementation of the public API for Updater.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Threading;
using Microsoft.ApplicationBlocks.Updater.Activator;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.ApplicationBlocks.Updater.Downloader;
using Microsoft.ApplicationBlocks.Updater.Utilities;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// Public class that client application uses to operate with the updater.
	/// </summary>
	public class ApplicationUpdaterManager
	{
		#region Private fields

		/// <summary>
		/// The activation manager instance.
		/// </summary>
		private ActivationManager activationManagerInstance;

		/// <summary>
		/// The download manager instance.
		/// </summary>
		private DownloadManager downloadManagerInstance;
		
		/// <summary>
		/// The manifest provider to use.
		/// </summary>
		private static ManifestManager manifestManagerInstance =  new ManifestManager();

		/// <summary>
		/// The list of created updaters.
		/// </summary>
		private static Hashtable updaters = new Hashtable();

		/// <summary>
		/// Key in the task context.
		/// </summary>
		private const string AsyncTaskKey = "AsyncTask";

		/// <summary>
		/// The last time the CheckForUpdates was called.
		/// </summary>
		private DateTime lastCheck = DateTime.MinValue;

		/// <summary>
		/// The applicationID the updater instance is for.
		/// </summary>
		private string applicationIdOfApplicationToUpdate;

		/// <summary>
		/// Control whether the updater instance is hooked to the DownloaderManager and ActivationManager events.
		/// </summary>
		private bool eventsHooked;

		/// <summary>
		/// Flag indicating if the client has resumed the pending updates
		/// </summary>
		private bool pendingUpdatesResumed = false;


		/// <summary>
		/// Maximum time to wait when resuming a pending update
		/// </summary>
		private TimeSpan downloadResumeMaxWaitTime = TimeSpan.FromMinutes( 10 );

		#endregion

		#region Events

		/// <summary>
		/// Notifies that there are pending updates.
		/// </summary>
		public event PendingUpdatesDetectedEventHandler PendingUpdatesDetected;
		
		/// <summary>
		/// Notifies downloads have started.
		/// </summary>
		public event DownloadStartedEventHandler DownloadStarted;

		/// <summary>
		/// Notifies download progress.
		/// </summary>
		public event DownloadProgressEventHandler DownloadProgress;

		/// <summary>
		/// Notifies download errors.
		/// </summary>
		public event DownloadErrorEventHandler DownloadError;

		/// <summary>
		/// Notifies that download is complete.
		/// </summary>
		public event DownloadCompletedEventHandler DownloadCompleted;
		
		/// <summary>
		/// Notifies that activation is initializing.
		/// </summary>
		public event ActivationInitializingEventHandler ActivationInitializing;

		/// <summary>
		/// Notifies that the activation initialization is aborted.
		/// </summary>
		public event ActivationInitializationAbortedEventHandler ActivationInitializationAborted;

		/// <summary>
		/// Notifies that the activation is started.
		/// </summary>
		public event ActivationStartedEventHandler ActivationStarted;

		/// <summary>
		/// Notifies that activation is complete.
		/// </summary>
		public event ActivationCompletedEventHandler ActivationCompleted;

		/// <summary>
		/// Notifies that an activation error ocurred.
		/// </summary>
		public event ActivationErrorEventHandler ActivationError;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructor used by a non Windows service app.
		/// </summary>
		/// <param name="applicationId">Id of the application being updated.</param>
		private ApplicationUpdaterManager( string applicationId )
		{
			applicationIdOfApplicationToUpdate = applicationId;

			activationManagerInstance = new ActivationManager();
			downloadManagerInstance = new DownloadManager();

			this.HookUpEvents();
		}

		/// <summary>
		/// Release this instance of the updater, and unregister the event handlers.
		/// </summary>
		~ApplicationUpdaterManager()
		{
			lock( updaters.SyncRoot )
			{
				updaters.Remove( applicationIdOfApplicationToUpdate );
			}
			UnHookEvents();
		}


		#endregion

		#region Public Properties

		/// <summary>
		/// Gets the last time CheckForUpdates() was called.
		/// </summary>
		public DateTime LastCheck
		{
			get
			{
				return lastCheck;
			}
		}

		#endregion

		#region Registry Management Methods

		/// <summary>
		/// Returns the list of registered tasks.
		/// </summary>
		/// <returns>A list of tasks corresponding to updates currently in progress.</returns>
		public static UpdaterTask[] GetTasks()
		{
			return RegistryManager.Current.GetTasks();
		}

		#endregion

		#region Static Members

		/// <summary>
		/// The manifest manager instance.
		/// </summary>
		private static ManifestManager ManifestManager
		{
			get
			{
				return manifestManagerInstance;
			}
		}

		#endregion

		#region Factory implementation

		/// <summary>
		/// Returns an <see cref="ApplicationUpdaterManager"/> instance for the configured application. 
		/// The client application will manage its updates through this instance.
		/// Can be used by both Windows service and non-Windows service clients for getting the <see cref="ApplicationUpdaterManager"/>.
		/// </summary>
		/// <returns>An ApplicationUpdaterManager instance.</returns>
		public static ApplicationUpdaterManager GetUpdater()
		{
			if ( new UpdaterConfigurationView().ApplicationId == null || (new UpdaterConfigurationView().ApplicationId != null && new UpdaterConfigurationView().ApplicationId.Length == 0) )
			{
				Logger.LogAndThrowException( new ConfigurationException( Resource.ResourceManager[ Resource.MessageKey.NoConfigurationToGetApplicationId ] ) );
			}
			return ApplicationUpdaterManager.GetUpdater( new UpdaterConfigurationView().ApplicationId );
		}

		/// <summary>
		/// Returns an <see cref="ApplicationUpdaterManager"/> instance for the specified application. 
		/// The client application will manage its updates through this instance. 
		/// </summary>
		/// <param name="applicationId">The ID for an application updates will be performed for.</param>
		/// <returns>An ApplicationUpdaterManager instance.</returns>
		public static ApplicationUpdaterManager GetUpdater( string applicationId )
		{
			if ( applicationId == null || ( applicationId != null && applicationId.Length == 0 ) )
			{
				Logger.LogAndThrowException( new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.ApplicationIdCannotBeNullOrEmpty ], "applicationId" ) );
			}

			if ( updaters.ContainsKey( applicationId ) )
			{
				return (ApplicationUpdaterManager)updaters[ applicationId ];
			}

			lock( updaters.SyncRoot )
			{
				if ( updaters.ContainsKey( applicationId ) )
				{
					return (ApplicationUpdaterManager)updaters[ applicationId ];
				}
				ApplicationUpdaterManager instance = new ApplicationUpdaterManager( applicationId );
				updaters.Add( applicationId, instance );
				return instance;
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Returns a list of available update manifests for the application from the default manifest URI location.
		/// </summary>
		/// <returns>The list of available manifests.</returns>
		public Manifest[] CheckForUpdates()
		{
			return CheckForUpdates( new UpdaterConfigurationView().DefaultManifestUriLocation );
		}

		/// <summary>
		/// Returns a list of available update manifests for the application from the specified manifest URI location.
		/// </summary>
		/// <param name="location">The target URI to get the manifest from.</param>
		/// <returns>The list of available manifests to apply.</returns>
		public Manifest[] CheckForUpdates( Uri location )
		{
			if ( location == null || (location != null && location.AbsoluteUri.Length == 0) )
			{
				Logger.LogAndThrowException( new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.LocationCannotBeNullOrEmpty ], "location" ) );
			}

			// Checks for pending updates and notifies the client
			CheckForPendingUpdates();

			// If the client didn't resume the pending updates, clear them
			// to allow the change of getting a fixed/renewed manifest
			if ( !pendingUpdatesResumed )
			{
				CancelPendingUpdates();	// If there are no pending updates this method does nothing
			}

			lastCheck = DateTime.UtcNow;
			return ApplicationUpdaterManager.ManifestManager.GetManifests( location, applicationIdOfApplicationToUpdate );
		}
		
		/// <summary>
		/// Resumes pending updates that were not completed in a previous session. 
		/// </summary>
		public void ResumePendingUpdates()
		{
			// If there are pending tasks for this application, resume them
			// accordingly to their state
			foreach(UpdaterTask task in RegistryManager.Current.GetByApplicationId( applicationIdOfApplicationToUpdate ) )
			{
				switch( task.State )
				{
					case UpdaterTaskState.DownloadError:
					case UpdaterTaskState.Downloading:
						downloadManagerInstance.SubmitTask( task, downloadResumeMaxWaitTime );
						break;
					case UpdaterTaskState.Downloaded:
					case UpdaterTaskState.Activating:
					case UpdaterTaskState.ActivationInitializing:
					case UpdaterTaskState.ActivationInitializationAborted:
					case UpdaterTaskState.ActivationError:
						activationManagerInstance.SubmitTask( task );
						if( task.State == UpdaterTaskState.Activated )
						{
							// Finally, unregister the task
							RegistryManager.Current.UnRegisterTask( task.TaskId );
						}
						break;
				}
			}

			pendingUpdatesResumed = true;
		}


		/// <summary>
		/// Cancels pending updates.
		/// </summary>
		public void CancelPendingUpdates()
		{
			UpdaterTask[] pendingTasks = RegistryManager.Current.GetByApplicationId( applicationIdOfApplicationToUpdate );
			foreach(UpdaterTask task in pendingTasks)
			{
				RegistryManager.Current.UnRegisterTask( task.TaskId );
				FileUtility.DestroyFolder( task.DownloadFilesBase );
			}
		}

		/// <summary>
		/// Synchronously downloads the specified files in selected manifests.
		/// </summary>
		/// <param name="selectedManifests">The list of manifests to process.</param>
		/// <param name="maxWaitTime">The maximum amount of time for download to complete before timeout.</param>
		public void Download( Manifest[] selectedManifests, TimeSpan maxWaitTime )
		{
			foreach(Manifest manifest in selectedManifests)
			{
				if ( manifest.Apply )
				{
					UpdaterTask task = new UpdaterTask( manifest );
					RegistryManager.Current.RegisterTask( task );
					task[ AsyncTaskKey ] = false;
					downloadManagerInstance.SubmitTask( task, maxWaitTime );
				}
			}		
		}

		/// <summary>
		/// Asynchronously begins a download of the files specified in selected manifests.
		/// </summary>
		/// <param name="selectedManifests">The list of Manifests to process.</param>
		public void BeginDownload( Manifest[] selectedManifests )
		{
			foreach(Manifest manifest in selectedManifests)
			{
				if ( manifest.Apply )
				{
					UpdaterTask task = new UpdaterTask( manifest );
					RegistryManager.Current.RegisterTask( task );
					task[ AsyncTaskKey ] = true;
					downloadManagerInstance.SubmitTaskAsync( task );
				}
			}		
		}

		/// <summary>
		/// Cancels the asynchronous download operation associated with the specified manifest. 
		/// </summary>
		/// <exception cref="InvalidOperationException">Thrown if the related update 
		/// task is in a state that is not cancelable.</exception>
		/// <param name="manifest">The manifest instance to cancel.</param>
		public void CancelDownload( Manifest manifest )
		{
			UpdaterTask task = RegistryManager.Current.GetByManifestID( manifest.ManifestId );
			if ( task != null )
			{
				switch(	task.State )
				{
					case UpdaterTaskState.None:
					{
						RegistryManager.Current.UnRegisterTask( task.TaskId );
						break;
					}
					case UpdaterTaskState.DownloadError:
					case UpdaterTaskState.Downloading:
					{
						downloadManagerInstance.EndTask( task );
						RegistryManager.Current.UnRegisterTask( task.TaskId );
						break;
					}
					case UpdaterTaskState.Downloaded:
					case UpdaterTaskState.ActivationInitializing:
					case UpdaterTaskState.ActivationInitializationAborted:
					{
						RegistryManager.Current.UnRegisterTask( task.TaskId );
						break;
					}
					case UpdaterTaskState.Activating:
					case UpdaterTaskState.ActivationError:
					{
						Logger.LogAndThrowException( new InvalidOperationException( Resource.ResourceManager[ Resource.MessageKey.UpdaterManagerUpdateCannotBeCanceled ] ) );
						break;
					}
				}

				lock(task.SyncRoot)
				{
					task.State = UpdaterTaskState.Cancelled;
				}
			}
		}


		/// <summary>
		/// Initiates the activation for the selected manifests.
		/// </summary>
		/// <param name="selectedManifests">The list of manifests describing the updates to apply.</param>
		public void Activate( Manifest[] selectedManifests )
		{
			foreach(Manifest manifest in selectedManifests)
			{
				if ( manifest.Apply )
				{
					// Get the task associated with the manifest
					UpdaterTask task = GetManifestActivationTask( manifest );

					// Start the activation process
					activationManagerInstance.SubmitTask( task );

					if( task.State == UpdaterTaskState.Activated )
					{
						// Finally, unregister the task
						RegistryManager.Current.UnRegisterTask( task.TaskId );
					}
				}
			}
		}

		#endregion

		#region Event subscription, handling and forwarding methods

		/// <summary>
		/// Registers the events of the downloader and the activator manager.
		/// </summary>
		private void HookUpEvents()
		{
			downloadManagerInstance.DownloadCompleted += new DownloadTaskCompletedEventHandler(OnDownloadCompleted);
			downloadManagerInstance.DownloadStarted += new DownloadTaskStartedEventHandler(OnDownloadStarted);
			downloadManagerInstance.DownloadProgress += new DownloadTaskProgressEventHandler(OnDownloadProgress);
			downloadManagerInstance.DownloadError += new DownloadTaskErrorEventHandler(OnDownloadError);
			
			activationManagerInstance.ActivationInitializationAborted += new ActivationTaskInitializationAbortedEventHandler(OnActivationInitializationAborted);
			activationManagerInstance.ActivationError += new ActivationTaskErrorEventHandler(OnActivationError);
			activationManagerInstance.ActivationInitializing += new ActivationTaskInitializingEventHandler(OnActivationInitializing);
			activationManagerInstance.ActivationStarted += new ActivationTaskStartedEventHandler(OnActivationStarted);
			activationManagerInstance.ActivationCompleted += new ActivationTaskCompletedEventHandler(OnActivationCompleted);
		
			eventsHooked = true;
		}

		/// <summary>
		/// Unregisters the events of the downloader and the activator manager.
		/// </summary>
		private void UnHookEvents()
		{
			if ( eventsHooked )
			{
				downloadManagerInstance.DownloadCompleted -= new DownloadTaskCompletedEventHandler(OnDownloadCompleted);
				downloadManagerInstance.DownloadStarted -= new DownloadTaskStartedEventHandler(OnDownloadStarted);
				downloadManagerInstance.DownloadProgress -= new DownloadTaskProgressEventHandler(OnDownloadProgress);
				downloadManagerInstance.DownloadError -= new DownloadTaskErrorEventHandler(OnDownloadError);
         
				activationManagerInstance.ActivationInitializationAborted -= new ActivationTaskInitializationAbortedEventHandler(OnActivationInitializationAborted);
				activationManagerInstance.ActivationError -= new ActivationTaskErrorEventHandler(OnActivationError);
				activationManagerInstance.ActivationInitializing -= new ActivationTaskInitializingEventHandler(OnActivationInitializing);
				activationManagerInstance.ActivationStarted -= new ActivationTaskStartedEventHandler(OnActivationStarted);
				activationManagerInstance.ActivationCompleted -= new ActivationTaskCompletedEventHandler(OnActivationCompleted);

				eventsHooked = false;
			}
		}

		/// <summary>
		/// Method used to handle the event.
		/// </summary>
		/// <param name="tasks">The UpdaterTask instance array.</param>
		private void OnPendingUpdatesDetected( UpdaterTask[] tasks )
		{
			if ( PendingUpdatesDetected != null )
			{
				// Notify the client pending tasks has been found
				Manifest[] manifests = new Manifest[ tasks.Length ];
				for(int i=0;i<tasks.Length;manifests[i] = tasks[i++].Manifest);
				PendingUpdatesDetectedEventArgs args = new PendingUpdatesDetectedEventArgs( manifests );
				PendingUpdatesDetected( this, args );
			}
		}

		/// <summary>
		/// Method used to handle the event.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The task information.</param>
		private void OnDownloadCompleted(object sender, TaskEventArgs e)
		{
			RegistryManager.Current.UpdateTask( e.Task );

			if ( DownloadCompleted != null )
			{
				DownloadCompleted( this, new ManifestEventArgs( e.Task.Manifest ) );
			}
		}

		/// <summary>
		/// Method used to handle the event.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The download error information.</param>
		private void OnDownloadError( object sender, DownloadTaskErrorEventArgs e )
		{
			RegistryManager.Current.UpdateTask( e.Task );
			if ( DownloadError != null )
			{
				DownloadError( this, new ManifestErrorEventArgs( e.Task.Manifest, e.Exception ) );
			}
		}

		/// <summary>
		/// Method used to handle the event.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The download progress task information.</param>
		private void OnDownloadProgress(object sender, DownloadTaskProgressEventArgs e)
		{
			if ( DownloadProgress != null )
			{

				DownloadProgressEventArgs eventArgs = new DownloadProgressEventArgs( 
					e.BytesTotal,
					e.BytesTransferred, 
					e.FilesTotal, 
					e.FilesTransferred, 
					e.Task.Manifest );
				DownloadProgress( this, eventArgs );
				if ( eventArgs.Cancel )
				{
					CancelDownload( e.Task.Manifest );
				}
			}
		}

		/// <summary>
		/// Method used to handle the event.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The task information.</param>
		private void OnDownloadStarted(object sender, TaskEventArgs e )
		{
			RegistryManager.Current.UpdateTask( e.Task );
			if ( DownloadStarted != null )
			{
				DownloadStartedEventArgs eventArgs = new DownloadStartedEventArgs( e.Task.Manifest );
				DownloadStarted( this, eventArgs );
				if ( eventArgs.Cancel )
				{
					CancelDownload( eventArgs.Manifest );
				}
			}
		}
		
		/// <summary>
		/// Method used to handle the event.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The activatin completed information.</param>
		private void OnActivationCompleted( object sender, ActivationTaskCompleteEventArgs e )
		{
			UpdaterTask task = e.Task;

			if ( e.Success )
			{
				// Clean up temporary files
				FileUtility.DestroyFolder( task.DownloadFilesBase );

				// Tell the ManifestManager we are done with this update
				ApplicationUpdaterManager.ManifestManager.CommitUpdate( task.Manifest );
			}

			if ( ActivationCompleted != null )
			{
				ActivationCompleted( this, new ActivationCompleteEventArgs( task.Manifest, e.Success ) );
			}
		}

		/// <summary>
		/// Method used to handle the event.
		/// </summary>
		/// <param name="sender">The sende of the event.</param>
		/// <param name="e">The activation task error information.</param>
		private void OnActivationInitializationAborted( object sender, ActivationTaskErrorEventArgs e )
		{
			RegistryManager.Current.UpdateTask( e.Task );
			if ( ActivationInitializationAborted != null )
			{
				ActivationInitializationAborted( this, new ManifestEventArgs( e.Task.Manifest ) );
			}
		}

		/// <summary>
		/// Method used to handle the event.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The activation error information.</param>
		private void OnActivationError( object sender, ActivationTaskErrorEventArgs e)
		{
			RegistryManager.Current.UpdateTask( e.Task );
			if ( ActivationError != null )
			{
				ActivationError( this, new ManifestErrorEventArgs( e.Task.Manifest, e.Exception ) );
			}
		}

		/// <summary>
		/// Method used to handle the event.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">Tehe task informaion.</param>
		private void OnActivationInitializing( object sender, TaskEventArgs e )
		{
			RegistryManager.Current.UpdateTask( e.Task );
			if ( ActivationInitializing != null )
			{
				ActivationInitializing( this, new ManifestEventArgs( e.Task.Manifest ) );
			}
		}

		/// <summary>
		/// Method used to handle the event.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The task information.</param>
		private void OnActivationStarted( object sender, TaskEventArgs e )
		{
			RegistryManager.Current.UpdateTask( e.Task );
			if ( ActivationStarted != null )
			{
				ActivationStarted( this, new ManifestEventArgs( e.Task.Manifest ) );
			}
		}

		#endregion

		#region Private members

		/// <summary>
		/// Check for pending updates in the registry manager.
		/// </summary>
		private void CheckForPendingUpdates()
		{
			UpdaterTask[] pendingTasks = RegistryManager.Current.GetByApplicationId( applicationIdOfApplicationToUpdate );
			if ( pendingTasks.Length > 0 )
			{
				OnPendingUpdatesDetected( pendingTasks );
			}
		}

		/// <summary>
		/// Return the UpdaterTask for a Manifest and validates the it's state.
		/// </summary>
		/// <param name="manifest">The manifest instance.</param>
		/// <returns>The UpdateTask instance.</returns>
		private UpdaterTask GetManifestActivationTask( Manifest manifest )
		{
			UpdaterTask task = RegistryManager.Current.GetByManifestID( manifest.ManifestId );
			if ( task == null )
			{
				Logger.LogAndThrowException( new ApplicationUpdaterException( Resource.ResourceManager[ Resource.MessageKey.CannotFindActivationTaskForManifest, manifest.ManifestId ] ) );
			}
			// Get the task to activate if the state is Downloaded, 
			// ActivationInitializationAborted and ActivationError 
			if( task.State == UpdaterTaskState.Downloaded
				|| task.State == UpdaterTaskState.ActivationInitializationAborted 
				|| task.State == UpdaterTaskState.ActivationError )
			{
				return task;
			}
			else
			{
				Logger.LogAndThrowException( new ApplicationUpdaterException( Resource.ResourceManager[ Resource.MessageKey.ActivationTaskForManifestInInvalidState, manifest.ManifestId, task.State ] ) );
			}
			return task;
		}

		#endregion
	}
}

