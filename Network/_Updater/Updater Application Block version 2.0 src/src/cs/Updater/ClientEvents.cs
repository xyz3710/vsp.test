//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ClientEvents.cs
//
// Contains the information about all the events that client application will use.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using Microsoft.ApplicationBlocks.Updater;

namespace Microsoft.ApplicationBlocks.Updater
{
	#region EventArgs classes

	/// <summary>
	/// Used to provide event information about the manifest.
	/// </summary>
	public class ManifestEventArgs : EventArgs
	{
		/// <summary>
		/// The manifest reference.
		/// </summary>
		private Manifest manifestInEventsArgs;

		/// <summary>
		/// Constructor for a ManifestEventArgs.
		/// </summary>
		/// <param name="manifest">The <see cref="Manifest"/> reference.</param>
		public ManifestEventArgs( Manifest manifest )
		{
			manifestInEventsArgs = manifest;
		}

		/// <summary>
		/// Returns the manifest reference.
		/// </summary>
		public Manifest Manifest
		{
			get { return manifestInEventsArgs; }
		}
	}

	/// <summary>
	/// Used to provide event information about the activation completion.
	/// </summary>
	public class ActivationCompleteEventArgs : ManifestEventArgs
	{
		/// <summary>
		/// Whether the task has been sucessfully completed or not.
		/// </summary>
		private bool success;

		/// <summary>
		/// Constructor for the ActivationCompleteEventArgs.
		/// </summary>
		/// <param name="manifest">The <see cref="Manifest"/> instance.</param>
		/// <param name="success">The success status.</param>
		public ActivationCompleteEventArgs( Manifest manifest, bool success ) : base( manifest )
		{
			this.success = success;
		}

		/// <summary>
		/// Indicates whether the update has been sucessfully activated.
		/// </summary>
		public bool Success
		{
			get { return success; }
		}
	}

	/// <summary>
	/// Used to provide information about manifest download errors.
	/// </summary>
	public class ManifestErrorEventArgs : ManifestEventArgs
	{
		/// <summary>
		/// The exception detected.
		/// </summary>
		private Exception exceptionContainedInManifestErrorEventArgs;

		/// <summary>
		/// Constructor for the ManifestErrorEventArgs.
		/// </summary>
		/// <param name="manifest">The <see cref="Manifest"/> reference.</param>
		/// <param name="exception">The exception information.</param>
		public ManifestErrorEventArgs( Manifest manifest, Exception exception ) : base( manifest )
		{
			exceptionContainedInManifestErrorEventArgs = exception;
		}

		/// <summary>
		/// The thrown exception.
		/// </summary>
		public Exception Exception
		{
			get { return exceptionContainedInManifestErrorEventArgs; }
		}
	}

	/// <summary>
	/// Used to provide information about the download progress.
	/// </summary>
	public class DownloadProgressEventArgs : BaseDownloadProgressEventArgs	
	{
		/// <summary>
		/// The manifest instance.
		/// </summary>
		private Manifest updateManifest;

		/// <summary>
		/// Constructor for the DownloadProgressEventArgs.
		/// </summary>
		/// <param name="bytesTotal">The total bytes to be transferred.</param>
		/// <param name="bytesTransferred">Number of bytes that have been transferred.</param>
		/// <param name="filesTotal">The total number of files to be transferred.</param>
		/// <param name="filesTransferred">Number of files that have been transferred.</param>
		/// <param name="manifest">The <see cref="Manifest"/> instance.</param>
		public DownloadProgressEventArgs( long bytesTotal, long bytesTransferred, int filesTotal, int filesTransferred, 
			Manifest manifest ) : base( bytesTotal, bytesTransferred, filesTotal, filesTransferred )
		{
			updateManifest = manifest;
		}

		/// <summary>
		/// The manifest instance.
		/// </summary>
		public Manifest Manifest
		{
			get { return updateManifest; }
		}
	}

	/// <summary>
	/// Used to provide information about download started.
	/// </summary>
	public class DownloadStartedEventArgs : ManifestEventArgs 
	{
		/// <summary>
		/// Indicates whether the download has been canceled.
		/// </summary>
		private bool cancel;

		/// <summary>
		/// Constructor for DownloadStartedEventArgs.
		/// </summary>
		/// <param name="manifest">The <see cref="Manifest"/> instance.</param>
		public DownloadStartedEventArgs( Manifest manifest ) : base( manifest )
		{
			cancel = false;
		}

		/// <summary>
		/// Indicates whether the download has been canceled.
		/// </summary>
		public bool Cancel
		{
			get { return cancel; }
			set { cancel = cancel || value; }
		}
	}

	/// <summary>
	/// Used to provide information about pending updates.
	/// </summary>
	public class PendingUpdatesDetectedEventArgs : EventArgs
	{
		/// <summary>
		/// The manifests that are pending.
		/// </summary>
		private Manifest[] pendingManifests;

		/// <summary>
		/// Constructor for the PendingUpdatesDetectedEventArgs.
		/// </summary>
		/// <param name="manifests">The <see cref="Manifest"/> instance array.</param>
		internal PendingUpdatesDetectedEventArgs( Manifest[] manifests )
		{
			pendingManifests = manifests;
		}

		/// <summary>
		/// The manifests that are pending.
		/// </summary>
		public Manifest[] Manifests
		{
			get { return pendingManifests; }
		}
	}

	#endregion

	#region ApplicationUpdater Events

	/// <summary>
	/// Delegate for the PendingUpdatesDetected event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void PendingUpdatesDetectedEventHandler( object sender, PendingUpdatesDetectedEventArgs e );

	#endregion

	#region Download Events
	/// <summary>
	/// Delegate for the DownloadProgress event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void DownloadProgressEventHandler( object sender, DownloadProgressEventArgs e );

	/// <summary>
	/// Delegate for the DownloadStarted event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void DownloadStartedEventHandler( object sender, DownloadStartedEventArgs e );
	
	/// <summary>
	/// Delegate for the DownloadCompleted event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void DownloadCompletedEventHandler( object sender, ManifestEventArgs e );
	
	/// <summary>
	/// Delegate for the DownloadError event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void DownloadErrorEventHandler( object sender, ManifestErrorEventArgs e );
	
	#endregion

	#region Activation Events
	
	/// <summary>
	/// Delegate for the ActivationInitializing event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void ActivationInitializingEventHandler( object sender, ManifestEventArgs e );
	
	/// <summary>
	/// Delegate for the ActivationInitializationAborted event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void ActivationInitializationAbortedEventHandler( object sender, ManifestEventArgs e );
	
	/// <summary>
	/// Delegate for the ActivationStarted event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void ActivationStartedEventHandler( object sender, ManifestEventArgs e );
	
	/// <summary>
	/// Delegate for the ActivationCompleted event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void ActivationCompletedEventHandler( object sender, ActivationCompleteEventArgs e );
	
	/// <summary>
	/// Delegate for the ActivationError event.
	/// </summary>
	/// <param name="sender">The sender of the event.</param>
	/// <param name="e">The information about the event.</param>
	public delegate void ActivationErrorEventHandler( object sender, ManifestErrorEventArgs e );
	
	#endregion
}