//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// PostApplicationExitActivationProcess.cs
//
// Contains the implementation of the post application exit update process.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using Microsoft.ApplicationBlocks.Updater;

namespace Microsoft.Updater.Tools
{
	/// <summary>
	/// This process waits for another process specified as a command line parameter and executes Updater when the 
	/// process has finished. This is used for the WaitForApplication processor when the application is detected as
	/// running when the update is perfromed.
	/// </summary>
	public class PostApplicationExitActivationProcess
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main( string[] args) 
		{
			if ( args.Length != 2 )
			{
				return;
			}

			Process processToWait = null;
			int pid = int.Parse( args[0] );
			string applicationId = args[1];
			
			Mutex waitMutex = new Mutex( true, String.Format( CultureInfo.InvariantCulture,
				"WaitApplication_{0}", applicationId ) ); 

			waitMutex.WaitOne();
			
			try
			{
				processToWait = Process.GetProcessById( pid );
			}
			catch
			{
				return;
			}

			processToWait.WaitForExit();

			// This will get the updater and perform all the pending updates automatically
			ApplicationUpdaterManager updater = ApplicationUpdaterManager.GetUpdater(applicationId);
			updater.ResumePendingUpdates();

			waitMutex.ReleaseMutex();

		}
	}
}