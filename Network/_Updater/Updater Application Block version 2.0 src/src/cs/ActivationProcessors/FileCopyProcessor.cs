//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// FileCopyProcessor.cs
//
// Contains the FileCopy processor implementation.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Xml;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
{
	/// <summary>
	/// This processor copies one downloaded file to the specified location
	/// </summary>
	/// <remarks>
	/// These attributes are defined to configure this processor. They are mandatory except where noted.
	/// <list type="table">
	/// <listheader><term>Attribute</term><description>Description</description></listheader>
	/// <item><term>type</term><description>The type name for this processor</description></item>
	/// <item><term>source</term><description>A reference to a downloaded file as defined by the source attribute in the files list</description></item>
	/// <item><term>destination</term><description>The directory or filename for the copied file. This path is relative to the application location, if a full path is not specified</description></item>
	/// <item><term>overwrite</term><description>Optional. Indicates whether to overwrite the file if the destination exists. Default is True.</description></item>
	/// </list>
	/// </remarks>
	/// <example>
	/// The following XML excerpt demonstrates how to configure this processor in the manifest file.
	/// <code>
	/// &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
	///		...
	///		&lt;files base="http://some-server/manifests/" &gt;
	///			&lt;file source="file1"&gt;
	///			&lt;file source="file2"&gt;
	///			&lt;file source="temp\file3"&gt;
	///		&lt;/files&gt;
	///		&lt;activation&gt;
	///			&lt;tasks&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					source="file1"
	///					destination="file1"
	///				/&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					source="file2"
	///					destination="samples\file"
	///				/&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					source="temp\file3"
	///					destination="file2"
	///					overwrite="False"
	///				/&gt;
	///			&lt;/tasks&gt;
	///		&lt;/activation&gt;
	///	&lt;/manifest&gt;
	/// </code>
	/// </example>
	public class FileCopyProcessor : IActivationProcessor
	{
		#region Private members

		/// <summary>
		/// The UpdaterTask provided in the Init method.
		/// </summary>
		UpdaterTask taskToProcess;

		/// <summary>
		/// The source file specification placed in the configuration of the processor in the manifest.
		/// </summary>
		private string sourceFileSpec;

		
		/// <summary>
		/// The target file specification placed in the configuration of the processor in the manifest.
		/// </summary>
		private string destFileSpec;

		/// <summary>
		/// Whether the destination files can be overritten or not.
		/// </summary>
		private bool enableOverwrite = true;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public FileCopyProcessor()
		{
		}

		#endregion

		#region IActivationProcessor Members

		/// <summary>
		/// Executes the processor.
		/// </summary>
		/// <remarks>Copy a file or a list of files from the source to the target.</remarks>
		public void Execute()
		{
			// Get the destination file full path if needed
			if ( !Path.IsPathRooted( destFileSpec ) )
			{
				destFileSpec = Path.Combine( taskToProcess.Manifest.Application.Location, destFileSpec );
			}

			// If destination is a folder ( anything ending with a '\' will be considered as a folder )
			// just copy the file to that folder
			if ( destFileSpec.EndsWith( Path.DirectorySeparatorChar.ToString( CultureInfo.InvariantCulture ) ) )
			{
				// Verify the existence of the required folder, if not, create it
				if ( !Directory.Exists( destFileSpec ) )
				{
					Directory.CreateDirectory( destFileSpec );
				}

				destFileSpec = Path.Combine( destFileSpec, Path.GetFileName( sourceFileSpec ) );
			}
			else
			{
				// Verify the existence of the target folder, and create it if not exists
				if ( !Directory.Exists( Path.GetDirectoryName( destFileSpec ) ) )
				{
					Directory.CreateDirectory( Path.GetDirectoryName( destFileSpec ) );
				}
			}

			// Finally, copy the files
			File.Copy( sourceFileSpec, destFileSpec, enableOverwrite );
		}

		/// <summary>
		/// Initializes the processor using the manifest configuration and the UpdaterTask instance.
		/// </summary>
		/// <param name="config">The configuration for the processor in the manifest file.</param>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <remarks>Validates the configuration and aborts the activation if it is not correct.</remarks>
		public void Init(ActivationProcessorProviderData config, UpdaterTask task)
		{
			taskToProcess = task;

			if ( config.AnyAttributes == null )
			{
				throw new ConfigurationException( Resource.ResourceManager[ Resource.MessageKey.ProcessorNotConfigured ] );
			}

			// Validate all the copy parameters.
			foreach(XmlAttribute attr in config.AnyAttributes)
			{
				if ( String.Compare( attr.Name, "source", false, CultureInfo.InvariantCulture ) == 0 )
				{
					sourceFileSpec = attr.Value;
				}
				if ( String.Compare( attr.Name, "destination", false, CultureInfo.InvariantCulture ) == 0 )
				{
					destFileSpec = attr.Value;
				}
				if ( String.Compare( attr.Name, "overwrite", false, CultureInfo.InvariantCulture ) == 0 )
				{
					enableOverwrite = bool.Parse( attr.Value );
				}
			}

			if ( sourceFileSpec == null || destFileSpec == null )
			{
				throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.SourceAndDestinationExpected ] );
			}
		}

		/// <summary>
		/// If the activation fails this method is called to revert the operations performed by the processor.
		/// </summary>
		/// <remarks>If the activation returned an error, this processor will delete the copied files.</remarks>
		public void OnError()
		{
			if ( File.Exists( destFileSpec ) )
			{
				File.Delete( destFileSpec );
			}
		}

		/// <summary>
		/// Prepares the execution and throws an exception if the execution is not possible.
		/// </summary>
		public void PrepareExecution()
		{
			// Get the source file full path  is needed
			if ( !Path.IsPathRooted( sourceFileSpec ) )
			{
				sourceFileSpec = Path.Combine( taskToProcess.DownloadFilesBase, sourceFileSpec );
			}

			// This processor only copies files, so fail if no valid source file was specified
			if ( !File.Exists( sourceFileSpec ) )
			{
				InvalidOperationException ex = 
					new InvalidOperationException( Resource.ResourceManager[ Resource.MessageKey.FileCopyProcessorSourceFileNotExists, sourceFileSpec ] );
				Logger.LogException( ex );
				throw ex;
			}
		}

		#endregion
	}
}
