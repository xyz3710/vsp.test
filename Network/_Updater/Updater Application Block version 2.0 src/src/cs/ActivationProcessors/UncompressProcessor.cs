//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// UncompressProcessor.cs
//
// Contains the implementation of the Uncompress processor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
{
	/// <summary>
	/// Uses the MS-ZIP algorithm to uncompress files
	/// which is available on any Windows installation.
	/// </summary>
	/// <remarks>
	/// You can use the Compress.exe tool (from the platform SDK) to create your compressed files.
	/// These attributes are defined to configure this processor. They are mandatory except where noted.
	/// <list type="table">
	/// <listheader><term>Attribute</term><description>Description</description></listheader>
	/// <item><term>type</term><description>The type name for this processor</description></item>
	/// <item><term>source</term><description>A reference to a downloaded file as defined by the source attribute in the files list</description></item>
	/// <item><term>destination</term><description>Optional. The filename for the uncompressed file. If not specified the file is not renamed. The path to this file is relative to the application location</description></item>
	/// </list>	
	/// </remarks>
	/// <example>
	/// The following XML excerpt demonstrates how to configure this processor in the manifest file.
	/// <code>
	/// &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
	///		...
	///		&lt;files base="http://some-server/manifests/" &gt;
	///			&lt;file source="largeFile.re_"&gt;
	///		&lt;/files&gt;
	///		&lt;activation&gt;
	///			&lt;tasks&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.UncompressProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					source="largeFile.res_"
	///					destination="resources\largeFile.res"
	///				/&gt;
	///			&lt;/tasks&gt;
	///		&lt;/activation&gt;
	///	&lt;/manifest&gt;
	///	</code>
	/// </example>
	public class UncompressProcessor : IActivationProcessor
	{
		#region Private members

		/// <summary>
		/// The UpdaterTask provided in the Init method.
		/// </summary>
		private UpdaterTask task;

		/// <summary>
		/// The source file that will be uncompressed.
		/// </summary>
		private string source;

		/// <summary>
		/// The target file after uncompression.
		/// </summary>
		private string destination;

		/// <summary>
		/// The path to the windows tool.
		/// </summary>
		private string utilPath = String.Empty;

		#endregion

		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		public UncompressProcessor()
		{
		}

		#endregion

		#region IActivationProcessor Members

		/// <summary>
		/// Executes the processor.
		/// </summary>
		public void Execute()
		{
			if ( destination != null && destination.Length > 0 && !Path.IsPathRooted( destination ) )
			{
				destination = Path.Combine( task.Manifest.Application.Location, destination );
			}

			if ( !Path.IsPathRooted( source ) )
			{
				source = Path.Combine( task.DownloadFilesBase, source );
			}

			using(Process expandProcess = new Process())
			{
				expandProcess.StartInfo.FileName = utilPath;
				if ( destination != null && destination.Length > 0 )
				{
					expandProcess.StartInfo.Arguments = String.Format( CultureInfo.InvariantCulture, "\"{0}\" \"{1}\"", source, destination );
				}
				else
				{
					expandProcess.StartInfo.Arguments = String.Format( CultureInfo.InvariantCulture, "-r {0} {1}", source, task.Manifest.Application.Location );
				}
				expandProcess.Start();
				expandProcess.WaitForExit();
				if ( expandProcess.ExitCode != 0 )
				{
					throw new Exception( Resource.ResourceManager[ Resource.MessageKey.ToolGeneratedError, "expand.exe" , expandProcess.ExitCode ] );
				}
			}
		}

		/// <summary>
		/// Initializes the processor using the manifest configuration and the UpdaterTask instance.
		/// </summary>
		/// <param name="config">The configuration for the processor in the manifest file.</param>
		/// <param name="task">The UpdaterTask instance.</param>
		public void Init(ActivationProcessorProviderData config, UpdaterTask task)
		{
			this.task = task;

			if ( config.AnyAttributes == null )
			{
				throw new ConfigurationException( Resource.ResourceManager[ Resource.MessageKey.ProcessorNotConfigured ] );
			}

			foreach( XmlAttribute attr in config.AnyAttributes )
			{
				if ( String.Compare( attr.Name, "source", false, CultureInfo.InvariantCulture ) == 0 )
				{
					source = attr.Value;
				}
				if ( String.Compare( attr.Name, "destination", false, CultureInfo.InvariantCulture ) == 0 )
				{
					destination = attr.Value;
				}
			}

			if ( source != null && source.Length == 0 )
			{
				throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.SourceAndDestinationExpected ] );
			}
		}

		/// <summary>
		/// If the activation fails this method is called to revert the operations performed by the processor.
		/// </summary>
		public void OnError()
		{
		}

		/// <summary>
		/// Prepares the execution and throws an exception if the execution is not possible.
		/// </summary>
		public void PrepareExecution()
		{
			utilPath = Path.Combine( Environment.SystemDirectory, "Expand.exe" );

			if ( !File.Exists( utilPath ) )
			{
				throw new InvalidOperationException( Resource.ResourceManager[ Resource.MessageKey.CannotFindTool, "expand.exe" ] );
			}

		}

		#endregion
	}

}
