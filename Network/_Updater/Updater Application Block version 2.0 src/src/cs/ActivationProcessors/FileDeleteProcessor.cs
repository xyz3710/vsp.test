//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// FileDeleteProcessor.cs
//
// Contains the implementation of the FileDelete processor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Xml;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
{
	/// <summary>
	/// This processor deletes a file using the its manifest configuration.
	/// </summary>
	/// <remarks>
	/// These attributes are defined to configure this processor. They are mandatory except where noted.
	/// <list type="table">
	/// <listheader><term>Attribute</term><description>Description</description></listheader>
	/// <item><term>type</term><description>The type name for this processor</description></item>
	/// <item><term>path</term><description>The path to the file or pattern to delete under the application location</description></item>
	/// </list>
	/// </remarks>
	/// <example>
	/// The following XML excerpt demonstrates how to configure this processor in the manifest file.
	/// <code>
	/// &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
	///		...
	///		&lt;activation&gt;
	///			&lt;tasks&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileDeleteProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					path="samples\*.txt"
	///					destination="file1"
	///				/&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileDeleteProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					path="file2"
	///				/&gt;
	///			&lt;/tasks&gt;
	///		&lt;/activation&gt;
	///	&lt;/manifest&gt;
	/// </code>
	/// </example>
	public class FileDeleteProcessor : IActivationProcessor
	{
		#region Private members

		/// <summary>
		/// The UpdaterTask provided in the Init method.
		/// </summary>
		private UpdaterTask taskToProcess;

		/// <summary>
		/// The file specification that must be deleted.
		/// </summary>
		private string fileSpec;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public FileDeleteProcessor()
		{
		}

		#endregion

		#region IActivationProcessor Members

		/// <summary>
		/// Executes the processor.
		/// </summary>
		/// <remarks>Uses the configuration to find a folder and deletes all the files within the folder that applies
		/// to the pattern provided.</remarks>
		public void Execute()
		{
			// Combine the path to make it absolute.
			if ( !Path.IsPathRooted( fileSpec ) )
			{
				fileSpec = Path.Combine( taskToProcess.Manifest.Application.Location, fileSpec );
			}

			string pattern = Path.GetFileName( fileSpec );
			string path = Path.GetDirectoryName( fileSpec );
			foreach( string fileName in Directory.GetFiles( path, pattern ) )
			{
				File.Delete( fileName );
			}
		}

		/// <summary>
		/// Initializes the processor using the manifest configuration and the UpdaterTask instance.
		/// </summary>
		/// <param name="config">The configuration for the processor in the manifest file.</param>
		/// <param name="taskToProcess">The UpdaterTask instance.</param>
		/// <remarks>Checks whether the configuration is valid.</remarks>
		public void Init(ActivationProcessorProviderData config, UpdaterTask taskToProcess)
		{
			this.taskToProcess = taskToProcess;

			if ( config.AnyAttributes == null )
			{
				throw new ConfigurationException( Resource.ResourceManager[ Resource.MessageKey.ProcessorNotConfigured ] );
			}
			foreach(XmlAttribute attr in config.AnyAttributes)
			{
				if ( String.Compare( attr.Name, "path", false, CultureInfo.InvariantCulture ) == 0 )
				{
					fileSpec = attr.Value;
				}
			}

			if ( fileSpec == null )
			{
				throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.PathExpected ] ); 
			}
		}

		/// <summary>
		/// If the activation fails this method is called to revert the operations performed by the processor.
		/// </summary>
		/// <remarks>No implementation for this method because does not make sense undeleting the deleted files.</remarks>
		public void OnError()
		{
		}

		/// <summary>
		/// Prepares the execution and throws an exception if the execution is not possible.
		/// </summary>
		/// <remarks>This method is not implemented because there is no validation process required for this processor.</remarks>
		public void PrepareExecution()
		{
		}

		#endregion
	}
}
