//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// FolderDeleteProcessor.cs
//
// Contains the implementation of the FolderDeleteProcessor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using System.Globalization;
using System.Xml;
using System.IO;
using Microsoft.ApplicationBlocks.Updater.Configuration;


namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
{
	/// <summary>
	/// This processor deletes all the files and subfolders of the specified folder in its configuration.
	/// </summary>
	/// <remarks>
	/// These attributes are defined to configure this processor. They are mandatory except where noted.
	/// <list type="table">
	/// <listheader><term>Attribute</term><description>Description</description></listheader>
	/// <item><term>type</term><description>The type name for this processor</description></item>
	/// <item><term>path</term><description>The path to the folder to delete. Relatives paths are considered to be under the application location folder</description></item>
	/// <item><term>recursive</term><description>Optional. Indicates wheter the subfolders should be recursively deleted. Default is True.</description></item>
	/// </list>
	/// </remarks>
	/// <example>
	/// The following XML excerpt demonstrates how to configure this processor in the manifest file.
	/// <code>
	/// &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
	///		...
	///		&lt;activation&gt;
	///			&lt;tasks&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FolderDeleteProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					path="samples"
	///					recursive="False"
	///				/&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FolderDeleteProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					path="temp"
	///				/&gt;
	///			&lt;/tasks&gt;
	///		&lt;/activation&gt;
	///	&lt;/manifest&gt;
	/// </code>
	/// </example>
	public class FolderDeleteProcessor : IActivationProcessor
	{
		#region Private members

		/// <summary>
		/// The UpdaterTask provided in the Init method.
		/// </summary>
		private UpdaterTask task;

		/// <summary>
		/// The folder specification to delete.
		/// </summary>
		private string folderSpec = String.Empty;

		/// <summary>
		/// Whether the delete operation is recursive or not.
		/// </summary>
		private bool isRecursive = true;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public FolderDeleteProcessor()
		{
		}

		#endregion

		#region IActivationProcessor Members

		/// <summary>
		/// Executes the processor.
		/// </summary>
		/// <remarks>Uses the base class library to delete the contents of a folder.</remarks>
		public void Execute()
		{
			if ( !Path.IsPathRooted( folderSpec ) )
			{
				folderSpec = Path.Combine( task.Manifest.Application.Location, folderSpec );
			}

			Directory.Delete( folderSpec, isRecursive );
		}

		/// <summary>
		/// Initializes the processor using the manifest configuration and the UpdaterTask instance.
		/// </summary>
		/// <param name="config">The configuration for the processor in the manifest file.</param>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <remarks>Validates the correctness of the configuration and aborts the activation if it is not valid.</remarks>
		public void Init(ActivationProcessorProviderData config, UpdaterTask task)
		{
			this.task = task;

			if ( config.AnyAttributes == null )
			{
				throw new ConfigurationException( Resource.ResourceManager[ Resource.MessageKey.ProcessorNotConfigured ] );
			}

			foreach(XmlAttribute attr in config.AnyAttributes)
			{
				if ( String.Compare( attr.Name, "path", false, CultureInfo.InvariantCulture ) == 0 )
				{
					folderSpec = attr.Value;
				}
				if ( String.Compare( attr.Name, "recursive", false, CultureInfo.InvariantCulture ) == 0 )
				{
					isRecursive = bool.Parse( attr.Value );
				}
			}

			if ( ( folderSpec != null && folderSpec.Length == 0 ) )
			{
				throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.PathExpected ] );
			}
		}

		/// <summary>
		/// If the activation fails this method is called to revert the operations performed by the processor.
		/// </summary>
		/// <remarks>No error hadling implemented for this provider.</remarks>
		public void OnError()
		{
		}

		/// <summary>
		/// Prepares the execution and throws an exception if the execution is not possible.
		/// </summary>
		/// <remarks>This method is not implemented because there is no validation process required for this processor.</remarks>
		public void PrepareExecution()
		{
		}

		#endregion
	}
}
