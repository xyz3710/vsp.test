//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ValidateHashException.cs
//
// Contains the implementation of the exception thrown by the validate hash processor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// Exception thrown by the validation hash processor when the hashes do not match.
	/// </summary>
	[Serializable]
	public class ValidateHashException : Exception
	{
		#region Private members

		/// <summary>
		/// The source file whose hash was computed.
		/// </summary>
		private string sourceFileFile;

		/// <summary>
		/// The computed hash for the file.
		/// </summary>
		private string computedHash;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ValidateHashException() : base()
		{
		}

		/// <summary>
		/// Creates a ValidateHashException with a specified message.
		/// </summary>
		/// <param name="message">The exception message string.</param>
		public ValidateHashException(string message) : base( message )
		{
		}

		/// <summary>
		/// Creates a ValidateHashException with a specified message.
		/// </summary>
		/// <param name="message">The exception message string.</param>
		/// <param name="innerException">The inner exception detected.</param>
		public ValidateHashException( string message, Exception innerException ) : base( message, innerException ) 
		{
		}

		/// <summary>
		/// Creates the exception using the name of the file and the computed hash.
		/// </summary>
		/// <param name="sourceFile">The file used to compute the hash.</param>
		/// <param name="computedHash">The computed hash.</param>
		internal ValidateHashException( string sourceFile, string computedHash )
			: base( Resource.ResourceManager[ Resource.MessageKey.ValidateHashExceptionMessage ] )
		{
			sourceFileFile = sourceFile;
			this.computedHash = computedHash;
		}

		/// <summary>
		/// Constructor used by the serialization infrastructure.
		/// </summary>
		/// <param name="info">The serialization information for the object.</param>
		/// <param name="context">The context for the serialization.</param>
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		protected ValidateHashException(SerializationInfo info, StreamingContext context) : base( info, context ) 
		{
			sourceFileFile = info.GetString( "_sourceFileFile" );
			computedHash = info.GetString( "_computedHash" );
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The source file the hash was computed for.
		/// </summary>
		public string SourceFile
		{
			get { return sourceFileFile; }
		}

		/// <summary>
		/// The computed hash for the file.
		/// </summary>
		public string TargetHash
		{
			get { return computedHash; }
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Used by the serialization infrastructure.
		/// </summary>
		/// <param name="info">The serialization information.</param>
		/// <param name="context">The serialization context.</param>
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context) 
		{
			info.AddValue("_sourceFileFile", sourceFileFile );
			info.AddValue("_computedHash", computedHash );
			base.GetObjectData( info, context );
		}

		#endregion
	}
}
