//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ValidateHashProcessor.cs
//
// Contains the implementation of the ValidateHash processor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using System.Globalization;
using System.Xml;
using System.IO;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography;

namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
{
	/// <summary>
	/// Validate the hash of the file specified in the manifes and the hash of the downloaded file. 
	/// If they do not match a ValidateHashException is thrown.
	/// </summary>
	/// <remarks>
	/// It is important to note that to use this processor, you must configure the hash 
	/// usage in the files element of the manifest. Also, the application configuration 
	/// must have defined the hashing provider specified by the hashProvider attribute of 
	/// the files element. 
	/// 
	/// These attributes are defined to configure this processor and they are mandatory 
	/// except where noted. 
	/// <list type="table">
	/// <listheader><term>Attribute</term><description>Description</description></listheader>
	/// <item><term>type</term><description>The type name for this processor</description></item>
	/// <item><term>source</term><description>A reference to a downloaded file as defined by the source attribute in the files list</description></item>
	/// </list>	
	/// </remarks>
	/// <example>
	/// The following XML excerpt demonstrates how to configure this processor in the manifest file.
	/// <code>
	/// &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
	///		...
	///		&lt;files base="http://some-server/manifests/" hashComparison="True" hashProvider="hmac1" &gt;
	///			&lt;file source="hashedFile.txt" hash="a5aqUnlIyzu670VDJW8lSwXJdJM=" /&gt;
	///		&lt;/files&gt;
	///		&lt;activation&gt;
	///			&lt;tasks&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.ValidateHashProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					source="hashedFile.txt"
	///				/&gt;
	///			&lt;/tasks&gt;
	///		&lt;/activation&gt;
	///	&lt;/manifest&gt;
	///	</code>
	/// </example>	
	public class ValidateHashProcessor : IActivationProcessor
	{
		#region Private members

		/// <summary>
		/// The UpdaterTask provided in the Init method.
		/// </summary>
		private UpdaterTask task;

		/// <summary>
		/// The source file to validate specified in the processor configuration.
		/// </summary>
		private string sourceFile = String.Empty;
			
		#endregion
			
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ValidateHashProcessor()
		{
		}

		#endregion

		#region IActivationProcessor Members

		/// <summary>
		/// Executes the processor.
		/// </summary>
		/// <remarks>If the hash does not match, an exception is thrown.</remarks>
		public void Execute()
		{

			string targetHash = null;

			foreach( FileManifest file in task.Manifest.Files )
			{
				if ( String.Compare( file.Source, sourceFile, true, System.Globalization.CultureInfo.CurrentCulture ) == 0 )
				{
					targetHash = file.Hash;
					break;
				}
			}

			if ( targetHash == null )
			{
				throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.ArgumentMissing1, sourceFile], "source" );
			}

			sourceFile = Path.Combine( task.DownloadFilesBase, sourceFile );
			if  ( !File.Exists( sourceFile ) )
			{
				throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.FileNotExists, sourceFile ],  "source");
			}

			using( FileStream fs = new FileStream( sourceFile, FileMode.Open, FileAccess.Read, FileShare.Read ) )
			{
				byte[] fileBytes = new byte[ fs.Length ];
				fs.Read( fileBytes, 0, fileBytes.Length );
				if ( !Cryptographer.CompareHash( task.Manifest.Files.HashProvider, fileBytes, Convert.FromBase64String( targetHash ) ) )
				{
					throw new ValidateHashException( sourceFile, targetHash );
				}
			}
		}

		/// <summary>
		/// Initializes the processor using the manifest configuration and the UpdaterTask instance.
		/// </summary>
		/// <param name="config">The configuration for the processor in the manifest file.</param>
		/// <param name="task">The UpdaterTask instance.</param>
		public void Init(ActivationProcessorProviderData config, UpdaterTask task)
		{
			this.task = task;

			if ( config.AnyAttributes == null )
			{
				throw new ConfigurationException( Resource.ResourceManager[ Resource.MessageKey.ProcessorNotConfigured ] );
			}

			foreach(XmlAttribute attr in config.AnyAttributes)
			{
				if ( String.Compare( attr.Name, "source", false, CultureInfo.InvariantCulture ) == 0 )
				{
					sourceFile = attr.Value;
				}
			}

			if ( sourceFile != null && sourceFile.Length == 0 )
			{
				throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.SourceExpected ] );
			}		
		}

		/// <summary>
		/// If the activation fails this method is called to revert the operations performed by the processor.
		/// </summary>
		public void OnError()
		{
		}

		/// <summary>
		/// Prepares the execution and throws an exception if the execution is not possible.
		/// </summary>
		/// <remarks>This method is not implemented because there is no validation process required for this processor.</remarks>
		public void PrepareExecution()
		{
		}

		#endregion
	}
}
