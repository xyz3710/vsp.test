//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ApplicationDeploy.cs
//
// Defines the ApplicationDeploy processor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.IO;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
{
	/// <summary>
	/// This processor copies all the downloaded files from the temporary folder to the application location.
	/// </summary>
	/// <remarks>
	/// All the downloaded files that are not marked as transient are copied from the temporary dowload location to the application location.
	/// To mark a file as transient, you need to add the transient="True" attribute to the file element in the manifest files list.
	/// </remarks>
	public class ApplicationDeployProcessor : IActivationProcessor	
	{
		#region Private members

		/// <summary>
		/// Keeps the task as a member during the lifetime of this instance.
		/// </summary>
		private UpdaterTask taskToProcess;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ApplicationDeployProcessor() { }

		#endregion

		#region IActivationProcessor Members

		/// <summary>
		/// Initializes the processor using the manifest configuration and the UpdaterTask instance.
		/// </summary>
		/// <param name="config">The configuration for the processor in the manifest file.</param>
		/// <param name="task">The UpdaterTask instance.</param>
		public void Init( ActivationProcessorProviderData config, UpdaterTask task )
		{
			taskToProcess = task;
		}

		/// <summary>
		/// Executes the processor.
		/// </summary>
		/// <remarks>Copies the files to the application folder. If the folder does not exist it is created. This 
		/// method copies all the files marked as non-transient in the manifest.</remarks>
		public void Execute()
		{
			// Check the existence of the folder.
			if( !Directory.Exists ( taskToProcess.Manifest.Application.Location ) )
			{
				Directory.CreateDirectory( taskToProcess.Manifest.Application.Location );
			}
					
			// Copy all the non-transient files to the app folder.
			foreach( FileManifest file in  taskToProcess.Manifest.Files )
			{
				if ( !file.Transient )
				{
					// Prepare the local paths.
					string sourceFile = Path.Combine( taskToProcess.DownloadFilesBase, file.Source );
					string targetFile = Path.Combine( taskToProcess.Manifest.Application.Location, file.Source );
					string targetDir = Path.GetDirectoryName( targetFile );

					// Since the file may be in a folder, also checks if the folder exists.
					if ( !Directory.Exists( targetDir ) )
					{
						Directory.CreateDirectory( targetDir );
					}
					
					// Copy the file.
					File.Copy( sourceFile, targetFile, true );
				}
			}
		}

		/// <summary>
		/// If the activation fails this method is called to revert the operations performed by the processor.
		/// </summary>
		/// <remarks>
		/// This method is deleting the files that were already copied. It also checks wether the file exists. If 
		/// there is another processor that has renamed the files this processor will fail.
		/// </remarks>
		public void OnError()
		{
			// Check the directory exists.
			if ( !Directory.Exists( taskToProcess.Manifest.Application.Location ) )
			{
				return;
			}

			// Remove any previously copied file
			foreach( FileManifest file in  taskToProcess.Manifest.Files )
			{
				if ( !file.Transient )
				{
					string targetFile = Path.Combine( taskToProcess.Manifest.Application.Location, file.Source );
					if ( File.Exists( targetFile ) )
					{
						File.Delete( targetFile );
					}
				}
			}
		}

		/// <summary>
		/// Prepares the execution and throws an exception if the execution is not possible.
		/// </summary>
		/// <remarks>This method is not implemented because there is no validation process required for this processor.</remarks>
		public void PrepareExecution()
		{
		}

		#endregion
	}
}
