using System;
using System.IO;

namespace SimpleAppStart
{
	/// <summary>
	/// Provides encapsulated view of AppStart configuration information.
	/// </summary>
	internal class AppStartConfiguration
	{
		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public AppStartConfiguration()
		{
		}

		#endregion

		#region Private members

		/// <summary>
		/// The name of the executable file for the application.
		/// </summary>
		private string applicationExecutableName = String.Empty;

		/// <summary>
		/// The name of the folder where the application was installed.
		/// </summary>
		private string applicationFolderName = String.Empty;

		/// <summary>
		/// Specifes when the application should be run.
		/// </summary>
		private UpdateTimeEnum whenToRunApplicationUpdate = UpdateTimeEnum.BeforeStart;

		/// <summary>
		/// The id of the application.
		/// </summary>
		private string applicationID;

		/// <summary>
		/// The URI of the manifest.
		/// </summary>
		private Uri manifestUri;

		/// <summary>
		/// The full path for the executable of the application.
		/// </summary>
		private string applicationExecutablePath = String.Empty;

		#endregion

		#region Public Properties

		/// <summary>
		/// The id of the application.
		/// </summary>
		public string ApplicationID
		{
			get { return applicationID; }
			set { applicationID = value; }
		}
		
		/// <summary>
		/// The name of the executable file for the application.
		/// </summary>
		public string ExecutableName
		{
			get{ return applicationExecutableName; }
			set{ applicationExecutableName = value; }
		}

		/// <summary>
		/// The name of the folder where the application was installed.
		/// </summary>
		public string FolderName
		{
			get{ return applicationFolderName; }
			set{ applicationFolderName = value; }
		}

		/// <summary>
		/// Specifes when the application should be run.
		/// </summary>
		public UpdateTimeEnum UpdateTime
		{
			get { return whenToRunApplicationUpdate; }
			set { whenToRunApplicationUpdate =  value; }
		}

		/// <summary>
		/// The URI of the manifest.
		/// </summary>
		public Uri ManifestUri
		{
			get { return manifestUri; }
			set { manifestUri = value; }
		}

		/// <summary>
		/// The full path for the executable of the application.
		/// </summary>
		public string ExecutablePath
		{
			get
			{
				if ( applicationExecutablePath == String.Empty )
				{
					if ( Path.IsPathRooted( this.FolderName ) )
					{
						applicationExecutablePath = Path.Combine( this.FolderName, this.ExecutableName );
					}
					else
					{
						applicationExecutablePath = Path.Combine( Environment.CurrentDirectory, this.FolderName );
						applicationExecutablePath = Path.GetFullPath( Path.Combine( applicationExecutablePath, this.ExecutableName ) );
					}
				}
				return applicationExecutablePath;
			}
		}

		#endregion
	}

	/// <summary>
	/// Specifies the options for the execution of the Updater before of after the application.
	/// </summary>
	internal enum UpdateTimeEnum
	{
		/// <summary>
		/// Check for updates before the application is executed.
		/// </summary>
		BeforeStart,

		/// <summary>
		/// Check for updates after the application is shutdown.
		/// </summary>
		AfterShutdown,
	}
}
