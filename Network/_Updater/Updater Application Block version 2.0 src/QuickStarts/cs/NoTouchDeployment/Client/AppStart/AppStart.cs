using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

using Microsoft.ApplicationBlocks.Updater;

namespace AppStart
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class AppStart
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			try
			{
				AppStart app = new AppStart();
				app.CheckAndUpdate();
				ProcessStartInfo psi = new ProcessStartInfo(Path.Combine(Directory.GetCurrentDirectory(),@"SimpleApplication\SimpleApplication.exe"));
				Process.Start( psi );
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				Console.ReadLine();
			}
		}

		private int manifestDownloaded = 0;

		private void CheckAndUpdate()
		{
			
			// Get the Updater instance
			ApplicationUpdaterManager updater = ApplicationUpdaterManager.GetUpdater();
			updater.DownloadStarted +=new DownloadStartedEventHandler(updater_DownloadStarted);
			updater.DownloadProgress += new DownloadProgressEventHandler(updater_DownloadProgress);
			updater.DownloadError += new DownloadErrorEventHandler(updater_DownloadError);
			updater.DownloadCompleted += new DownloadCompletedEventHandler(updater_DownloadCompleted);
			updater.ActivationStarted += new ActivationStartedEventHandler(updater_ActivationStarted);
			updater.ActivationInitializing += new ActivationInitializingEventHandler(updater_ActivationInitializing);
			updater.ActivationInitializationAborted += new ActivationInitializationAbortedEventHandler(updater_ActivationInitializationAborted);
			updater.ActivationError += new ActivationErrorEventHandler(updater_ActivationError);
			updater.ActivationCompleted += new ActivationCompletedEventHandler(updater_ActivationCompleted);

			// Check for the avilable Updates
			Manifest[] manifests = updater.CheckForUpdates();

			if ( manifests.Length > 0 )
			{
				// Prompt user if he wants to apply the updates
				Console.WriteLine("Update for Simple Application is available, do you want to apply the update? Please print Y or N and enter.");
				string result = Console.ReadLine();
				if( result == "Y" || result == "y" )
				{
					foreach(Manifest m in manifests)
					{
						m.Apply = true;
					}
					
					// update the application as per manifest details.
					updater.Download( manifests, TimeSpan.MaxValue );
					if(manifestDownloaded == manifests.Length)
					{
						updater.Activate(manifests);
						manifestDownloaded = 0;
					}
				}
			}
		}

		private void updater_DownloadStarted(object sender, DownloadStartedEventArgs e)
		{
			Console.WriteLine( "Download Started : {0}", e.Manifest.ManifestId );   
		}

		private void updater_DownloadProgress(object sender, DownloadProgressEventArgs e)
		{
			Console.WriteLine( "Download Progress for manifest: "+ e.Manifest.ManifestId +
				"- Files: "+e.FilesTransferred+"/"+e.FilesTotal+
				" - Bytes: "+e.BytesTransferred+"/"+e.BytesTotal);
		}

		private void updater_DownloadError(object sender, ManifestErrorEventArgs e)
		{
			Console.WriteLine( "Download Error : {0}", e.Manifest.ManifestId );
		}

		private void updater_DownloadCompleted(object sender, ManifestEventArgs e)
		{
			Console.WriteLine( "Download Complete : {0}", e.Manifest.ManifestId );
			manifestDownloaded++;
		}

		private void updater_ActivationStarted(object sender, ManifestEventArgs e)
		{
			Console.WriteLine( "Activation Initializing : {0}", e.Manifest.ManifestId );
		}

		private void updater_ActivationInitializing(object sender, ManifestEventArgs e)
		{
			Console.WriteLine( "Activation Started : {0}", e.Manifest.ManifestId );
		}

		private void updater_ActivationInitializationAborted(object sender, ManifestEventArgs e)
		{
			Console.WriteLine( "Activation Initialization Aborted : {0}", e.Manifest.ManifestId );
		}

		private void updater_ActivationError(object sender, ManifestErrorEventArgs e)
		{
			Console.WriteLine( "Activation Error : {0}", e.Manifest.ManifestId );
		}

		private void updater_ActivationCompleted(object sender, ActivationCompleteEventArgs e)
		{
			Console.WriteLine( "Activation Complete : {0}", e.Manifest.ManifestId );
		}
	}
}
