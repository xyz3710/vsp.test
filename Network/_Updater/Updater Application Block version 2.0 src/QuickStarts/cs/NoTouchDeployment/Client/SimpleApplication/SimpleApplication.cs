using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace SimpleApplication
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class SimpleApplication : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label versionLabel;
		private System.Windows.Forms.Button exitButton;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SimpleApplication()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.versionLabel = new System.Windows.Forms.Label();
			this.exitButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// versionLabel
			// 
			this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.versionLabel.Location = new System.Drawing.Point(32, 88);
			this.versionLabel.Name = "versionLabel";
			this.versionLabel.Size = new System.Drawing.Size(216, 48);
			this.versionLabel.TabIndex = 0;
			this.versionLabel.Text = "This is a Simple Application Version 1.0";
			// 
			// exitButton
			// 
			this.exitButton.Location = new System.Drawing.Point(96, 168);
			this.exitButton.Name = "exitButton";
			this.exitButton.TabIndex = 1;
			this.exitButton.Text = "Exit";
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// SimpleApplication
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Controls.Add(this.exitButton);
			this.Controls.Add(this.versionLabel);
			this.Name = "SimpleApplication";
			this.Text = "SimpleApplication";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new SimpleApplication());
		}

		private void exitButton_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}
	}
}
