using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace NoTouchDeploymentStub
{
	/// <summary>
	/// Summary description for NoTouchDeploymentStub.
	/// </summary>
	public class LaunchStub
	{
		public LaunchStub()
		{
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			string foderName = "AppStart";
			string appStartPath = Path.Combine(Directory.GetCurrentDirectory(),foderName );

			if( !Directory.Exists(appStartPath) )
			{
				Directory.CreateDirectory(appStartPath);
			}

			LaunchStub stub = new LaunchStub();
			stub.ExtractAppStartFiles(appStartPath);
			ProcessStartInfo psi = new ProcessStartInfo(Path.Combine(appStartPath, "AppStart.exe"));
			psi.UseShellExecute = false;
			psi.WorkingDirectory = appStartPath;
			Process.Start( psi );
		}

		private void ExtractAppStartFiles(string appStartPath)
		{
			ExtractResourceStream( appStartPath, "NoTouchDeploymentStub.Resources.AppStart.exe", "AppStart.exe" );
			ExtractResourceStream( appStartPath, "NoTouchDeploymentStub.Resources.updaterconfiguration.config", "updaterconfiguration.config" );
			ExtractResourceStream( appStartPath, "NoTouchDeploymentStub.Resources.AppStart.exe.config", "AppStart.exe.config" );
			ExtractResourceStream( appStartPath, "NoTouchDeploymentStub.Resources.Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.dll", "Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.dll" );
			ExtractResourceStream( appStartPath, "NoTouchDeploymentStub.Resources.Microsoft.Practices.EnterpriseLibrary.Configuration.dll", "Microsoft.Practices.EnterpriseLibrary.Configuration.dll" );
			ExtractResourceStream( appStartPath, "NoTouchDeploymentStub.Resources.Microsoft.Practices.EnterpriseLibrary.Common.dll", "Microsoft.Practices.EnterpriseLibrary.Common.dll" );
			ExtractResourceStream( appStartPath, "NoTouchDeploymentStub.Resources.Microsoft.ApplicationBlocks.Updater.Downloaders.dll", "Microsoft.ApplicationBlocks.Updater.Downloaders.dll" );
			ExtractResourceStream( appStartPath, "NoTouchDeploymentStub.Resources.Microsoft.ApplicationBlocks.Updater.dll", "Microsoft.ApplicationBlocks.Updater.dll" );
			ExtractResourceStream( appStartPath, "NoTouchDeploymentStub.Resources.Microsoft.ApplicationBlocks.Updater.ActivationProcessors.dll", "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.dll" );
		}

		private void ExtractResourceStream( string appStartPath, string resourceName, string targetFile )
		{
			string[] manifestResourceNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();
			string targetFileFullPath = Path.Combine(appStartPath, targetFile);
			using(Stream res = Assembly.GetExecutingAssembly().GetManifestResourceStream( resourceName ) )
			{
				using( FileStream fs = new FileStream( targetFileFullPath, FileMode.Create, FileAccess.Write ) )
				{
					using( BinaryWriter bw = new BinaryWriter( fs ) )
					{
						byte[] buffer = new byte[1024];
						int cbRead = 0;
						do
						{
							cbRead = res.Read( buffer, 0, 1024 );
							bw.Write( buffer, 0, cbRead );
						} while ( cbRead >= 1024 );
					}
				}
			}
		}
	}
}
