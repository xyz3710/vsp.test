
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data


'/ <summary>
'/ Summary description for Form1.
'/ </summary>

Public Class SimpleApplication
    Inherits System.Windows.Forms.Form
    Private versionLabel As System.Windows.Forms.Label
    Private WithEvents exitButton As System.Windows.Forms.Button
    '/ <summary>
    '/ Required designer variable.
    '/ </summary>
    Private components As System.ComponentModel.Container = Nothing
    
    
    Public Sub New() 
        '
        ' Required for Windows Form Designer support
        '
        InitializeComponent()
    
    End Sub 'New
     
    '
    ' TODO: Add any constructor code after InitializeComponent call
    '
    
    '/ <summary>
    '/ Clean up any resources being used.
    '/ </summary>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)

    End Sub 'Dispose

#Region "Windows Form Designer generated code"

    '/ <summary>
    '/ Required method for Designer support - do not modify
    '/ the contents of this method with the code editor.
    '/ </summary>
    Private Sub InitializeComponent()
        Me.versionLabel = New System.Windows.Forms.Label
        Me.exitButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        ' 
        ' versionLabel
        ' 
        Me.versionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, System.Byte))
        Me.versionLabel.Location = New System.Drawing.Point(32, 88)
        Me.versionLabel.Name = "versionLabel"
        Me.versionLabel.Size = New System.Drawing.Size(216, 48)
        Me.versionLabel.TabIndex = 0
        Me.versionLabel.Text = "This is a Simple Application Version 1.0"
        ' 
        ' exitButton
        ' 
        Me.exitButton.Location = New System.Drawing.Point(96, 168)
        Me.exitButton.Name = "exitButton"
        Me.exitButton.TabIndex = 1
        Me.exitButton.Text = "Exit"
        ' 
        ' SimpleApplication
        ' 
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(exitButton)
        Me.Controls.Add(versionLabel)
        Me.Name = "SimpleApplication"
        Me.Text = "SimpleApplication"
        Me.ResumeLayout(False)

    End Sub 'InitializeComponent 
#End Region


    '/ <summary>
    '/ The main entry point for the application.
    '/ </summary>
    <STAThread()> _
    Shared Sub Main()
        Application.Run(New SimpleApplication)

    End Sub 'Main


    Private Sub exitButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles exitButton.Click
        Application.Exit()

    End Sub 'exitButton_Click
End Class 'SimpleApplication