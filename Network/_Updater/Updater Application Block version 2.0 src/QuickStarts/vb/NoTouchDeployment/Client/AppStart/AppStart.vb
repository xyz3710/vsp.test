
Imports System
Imports System.Diagnostics
Imports System.IO
Imports System.Reflection

Imports Microsoft.ApplicationBlocks.Updater


' <summary>
' Summary description for Class1.
' </summary>

Class AppStart
    
    ' <summary>
    ' The main entry point for the application.
    ' </summary>
    <STAThread()>  _
    Shared Sub Main(ByVal args() As String) 
        Try
            Dim app As New AppStart()
            app.CheckAndUpdate()
            Dim psi As New ProcessStartInfo(Path.Combine(Directory.GetCurrentDirectory(), "SimpleApplication\SimpleApplication.exe"))
            Process.Start(psi)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Console.ReadLine()
        End Try
    
    End Sub 'Main
    
    Private manifestDownloaded As Integer = 0
    
    
    Private Sub CheckAndUpdate() 
        
        ' Get the Updater instance
        Dim updater As ApplicationUpdaterManager = ApplicationUpdaterManager.GetUpdater()
        AddHandler updater.DownloadStarted, AddressOf updater_DownloadStarted
        AddHandler updater.DownloadProgress, AddressOf updater_DownloadProgress
        AddHandler updater.DownloadError, AddressOf updater_DownloadError
        AddHandler updater.DownloadCompleted, AddressOf updater_DownloadCompleted
        AddHandler updater.ActivationStarted, AddressOf updater_ActivationStarted
        AddHandler updater.ActivationInitializing, AddressOf updater_ActivationInitializing
        AddHandler updater.ActivationInitializationAborted, AddressOf updater_ActivationInitializationAborted
        AddHandler updater.ActivationError, AddressOf updater_ActivationError
        AddHandler updater.ActivationCompleted, AddressOf updater_ActivationCompleted
        
        ' Check for the avilable Updates
        Dim manifests As Manifest() = updater.CheckForUpdates()
        
        If manifests.Length > 0 Then
            ' Prompt user if he wants to apply the updates
            Console.WriteLine("Update for Simple Application is available, do you want to apply the update? Please print Y or N and enter.")
            Dim result As String = Console.ReadLine()
            If result = "Y" OrElse result = "y" Then
                Dim m As Manifest
                For Each m In  manifests
                    m.Apply = True
                Next m
                
                ' update the application as per manifest details.
                updater.Download(manifests, TimeSpan.MaxValue)
                If manifestDownloaded = manifests.Length Then
                    updater.Activate(manifests)
                    manifestDownloaded = 0
                End If
            End If
        End If
    
    End Sub 'CheckAndUpdate
    
    
    Private Sub updater_DownloadStarted(ByVal sender As Object, ByVal e As DownloadStartedEventArgs) 
        Console.WriteLine("Download Started : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_DownloadStarted
    
    
    Private Sub updater_DownloadProgress(ByVal sender As Object, ByVal e As DownloadProgressEventArgs) 
        Console.WriteLine("Download Progress for manifest: " + e.Manifest.ManifestId.ToString() + "- Files: " + e.FilesTransferred.ToString() + _
            "/" + e.FilesTotal.ToString() + " - Bytes: " + e.BytesTransferred.ToString() + "/" + e.BytesTotal.ToString())
    
    End Sub 'updater_DownloadProgress
    
    
    Private Sub updater_DownloadError(ByVal sender As Object, ByVal e As ManifestErrorEventArgs) 
        Console.WriteLine("Download Error : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_DownloadError
    
    
    Private Sub updater_DownloadCompleted(ByVal sender As Object, ByVal e As ManifestEventArgs) 
        Console.WriteLine("Download Complete : {0}", e.Manifest.ManifestId)
        manifestDownloaded += 1
    
    End Sub 'updater_DownloadCompleted
    
    
    Private Sub updater_ActivationStarted(ByVal sender As Object, ByVal e As ManifestEventArgs) 
        Console.WriteLine("Activation Initializing : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_ActivationStarted
    
    
    Private Sub updater_ActivationInitializing(ByVal sender As Object, ByVal e As ManifestEventArgs) 
        Console.WriteLine("Activation Started : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_ActivationInitializing
    
    
    Private Sub updater_ActivationInitializationAborted(ByVal sender As Object, ByVal e As ManifestEventArgs) 
        Console.WriteLine("Activation Initialization Aborted : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_ActivationInitializationAborted
    
    
    Private Sub updater_ActivationError(ByVal sender As Object, ByVal e As ManifestErrorEventArgs) 
        Console.WriteLine("Activation Error : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_ActivationError
    
    
    Private Sub updater_ActivationCompleted(ByVal sender As Object, ByVal e As ActivationCompleteEventArgs) 
        Console.WriteLine("Activation Complete : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_ActivationCompleted
End Class 'AppStart