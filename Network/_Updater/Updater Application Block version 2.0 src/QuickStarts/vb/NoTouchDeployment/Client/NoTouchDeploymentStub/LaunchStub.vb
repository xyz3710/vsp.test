
Imports System
Imports System.Diagnostics
Imports System.IO
Imports System.Reflection


' <summary>
' Summary description for NoTouchDeploymentStub.
' </summary>

Public Class LaunchStub
    
    Public Sub New() 
    
    End Sub 'New
    
    
    ' <summary>
    ' The main entry point for the application.
    ' </summary>
    <STAThread()>  _
    Shared Sub Main() 
        Dim folderName As String = "AppStart"
        Dim appStartPath As String = Path.Combine(Directory.GetCurrentDirectory(), folderName)

        If Not Directory.Exists(appStartPath) Then
            Directory.CreateDirectory(appStartPath)
        End If

        Dim stub As New LaunchStub
        stub.ExtractAppStartFiles(appStartPath)
        Dim psi As New ProcessStartInfo(Path.Combine(appStartPath, "AppStart.exe"))
        psi.UseShellExecute = False
        psi.WorkingDirectory = appStartPath
        Process.Start(psi)
    
    End Sub 'Main
    
    
    Private Sub ExtractAppStartFiles(ByVal appStartPath As String) 
        ExtractResourceStream(appStartPath, "NoTouchDeploymentStub.AppStart.exe", "AppStart.exe")
        ExtractResourceStream(appStartPath, "NoTouchDeploymentStub.updaterconfiguration.config", "updaterconfiguration.config")
        ExtractResourceStream(appStartPath, "NoTouchDeploymentStub.AppStart.exe.config", "AppStart.exe.config")
        ExtractResourceStream(appStartPath, "NoTouchDeploymentStub.Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.dll", "Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.dll")
        ExtractResourceStream(appStartPath, "NoTouchDeploymentStub.Microsoft.Practices.EnterpriseLibrary.Configuration.dll", "Microsoft.Practices.EnterpriseLibrary.Configuration.dll")
        ExtractResourceStream(appStartPath, "NoTouchDeploymentStub.Microsoft.Practices.EnterpriseLibrary.Common.dll", "Microsoft.Practices.EnterpriseLibrary.Common.dll")
        ExtractResourceStream(appStartPath, "NoTouchDeploymentStub.Microsoft.ApplicationBlocks.Updater.Downloaders.dll", "Microsoft.ApplicationBlocks.Updater.Downloaders.dll")
        ExtractResourceStream(appStartPath, "NoTouchDeploymentStub.Microsoft.ApplicationBlocks.Updater.dll", "Microsoft.ApplicationBlocks.Updater.dll")
        ExtractResourceStream(appStartPath, "NoTouchDeploymentStub.Microsoft.ApplicationBlocks.Updater.ActivationProcessors.dll", "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.dll")
    
    End Sub 'ExtractAppStartFiles
    
    
    Private Sub ExtractResourceStream(ByVal appStartPath As String, ByVal resourceName As String, ByVal targetFile As String) 
        Dim fs As FileStream
        Dim bw As BinaryWriter
        Dim manifestResourceNames As String() = [Assembly].GetExecutingAssembly().GetManifestResourceNames()
        Dim targetFileFullPath As String = Path.Combine(appStartPath, targetFile)
        Dim res As Stream = [Assembly].GetExecutingAssembly().GetManifestResourceStream(resourceName)
        Try
            fs = New FileStream(targetFileFullPath, FileMode.Create, FileAccess.Write)
            Try
                bw = New BinaryWriter(fs)
                Try
                    Dim buffer(1023) As Byte
                    Dim cbRead As Integer = 0
                    Do
                        cbRead = res.Read(buffer, 0, 1024)
                        bw.Write(buffer, 0, cbRead)
                    Loop While cbRead >= 1024
                Finally
                    bw.Flush()
                End Try
            Finally
                fs.Flush()
            End Try
        Finally
            res.Flush()
            bw.Close()
            fs.Close()
            res.Close()
        End Try
    
    End Sub 'ExtractResourceStream
End Class 'LaunchStub