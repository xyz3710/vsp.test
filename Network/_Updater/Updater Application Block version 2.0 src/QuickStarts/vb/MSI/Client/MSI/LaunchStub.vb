
Imports System
Imports System.IO
Imports System.Diagnostics
Imports System.Threading
Imports Microsoft.ApplicationBlocks.Updater


' <summary>
' Summary description for LaunchStub.
' </summary>

Class LaunchStub
    Private manifestDownloaded As Boolean = False
    
    ' <summary>
    ' The main entry point for the application.
    ' </summary>
    <STAThread()>  _
    Shared Sub Main(ByVal args() As String) 
        Dim launch As New LaunchStub()
        launch.CheckAndUpdate()
    
    End Sub 'Main
    
    
    Sub New() 
    
    End Sub 'New
    
    Private Sub CheckAndUpdate() 
        Try
            Dim updater As ApplicationUpdaterManager = ApplicationUpdaterManager.GetUpdater()
            Dim manifests As Manifest() = updater.CheckForUpdates()
            If manifests.Length > 0 Then
                ' Here we are showcasing the included manifest functionality as well but
                ' as the manifests are for Install, Update and Uninstall, we should apply 
                ' only one manifest at a time, also as the order is same as described above
                ' apply always the first manifest, so first time the application will be 
                ' installed, second time the application will be updated and third time the 
                ' application will be uninstalled.
                manifests(0).Apply = True
                
                ' subscribe to various events.
                AddHandler updater.DownloadStarted, AddressOf updater_DownloadStarted
                AddHandler updater.DownloadProgress, AddressOf updater_DownloadProgress
                AddHandler updater.DownloadError, AddressOf updater_DownloadError
                AddHandler updater.DownloadCompleted, AddressOf ApplicationUpdater_DownloadCompleted
                AddHandler updater.ActivationStarted, AddressOf updater_ActivationStarted
                AddHandler updater.ActivationInitializing, AddressOf updater_ActivationInitializing
                AddHandler updater.ActivationInitializationAborted, AddressOf updater_ActivationInitializationAborted
                AddHandler updater.ActivationError, AddressOf updater_ActivationError
                AddHandler updater.ActivationCompleted, AddressOf updater_ActivationCompleted
                
                updater.Download(manifests, TimeSpan.MaxValue)
                
                ' we are applying only one manifest at a time 
                ' hence just check the if the manifest has been correctly 
                ' downloaded.
                If manifestDownloaded Then
                    updater.Activate(manifests)
                    manifestDownloaded = False
                End If
            End If
            
            'Start the installed / updated application.
            Dim appInfo As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            appInfo += "\UpdaterQuickStarts\SimpleApplication\SimpleApplication.exe"
            If File.Exists(appInfo) Then
                Dim psi As New ProcessStartInfo(appInfo)
                Process.Start(psi)
            Else
                Console.WriteLine("The Application has been uninstalled ")
            End If
        Catch ex As Exception
            Console.WriteLine(String.Format("Following error has occured: {0}", ex.Message))
        End Try
        
        Console.WriteLine("Hit ENTER to end.")
        Console.ReadLine()
    
    End Sub 'CheckAndUpdate
    
    
    Private Sub ApplicationUpdater_DownloadCompleted(ByVal sender As Object, ByVal e As ManifestEventArgs) 
        Console.WriteLine("Download Complete : {0}", e.Manifest.ManifestId)
        manifestDownloaded = True
    
    End Sub 'ApplicationUpdater_DownloadCompleted
    
    
    Private Sub updater_ActivationCompleted(ByVal sender As Object, ByVal e As ActivationCompleteEventArgs) 
        Console.WriteLine("Activation Complete : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_ActivationCompleted
    
    
    Private Sub updater_DownloadError(ByVal sender As Object, ByVal e As ManifestErrorEventArgs) 
        Console.WriteLine("Download Error : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_DownloadError
    
    
    Private Sub updater_DownloadProgress(ByVal sender As Object, ByVal e As DownloadProgressEventArgs) 
        Console.WriteLine("Download Progress for manifest: " + e.Manifest.ManifestId.ToString() + "- Files: " + e.FilesTransferred.ToString() + _
            "/" + e.FilesTotal.ToString() + " - Bytes: " + e.BytesTransferred.ToString() + "/" + e.BytesTotal.ToString())
    
    End Sub 'updater_DownloadProgress
    
    
    
    Private Sub updater_DownloadStarted(ByVal sender As Object, ByVal e As DownloadStartedEventArgs) 
        Console.WriteLine("Download Started : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_DownloadStarted
    
    
    Private Sub updater_ActivationError(ByVal sender As Object, ByVal e As ManifestErrorEventArgs) 
        Console.WriteLine("Activation Error : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_ActivationError
    
    
    Private Sub updater_ActivationInitializationAborted(ByVal sender As Object, ByVal e As ManifestEventArgs) 
        Console.WriteLine("Activation Initialization Aborted : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_ActivationInitializationAborted
    
    
    Private Sub updater_ActivationInitializing(ByVal sender As Object, ByVal e As ManifestEventArgs) 
        Console.WriteLine("Activation Initializing : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_ActivationInitializing
    
    
    Private Sub updater_ActivationStarted(ByVal sender As Object, ByVal e As ManifestEventArgs) 
        Console.WriteLine("Activation Started : {0}", e.Manifest.ManifestId)
    
    End Sub 'updater_ActivationStarted
End Class 'LaunchStub