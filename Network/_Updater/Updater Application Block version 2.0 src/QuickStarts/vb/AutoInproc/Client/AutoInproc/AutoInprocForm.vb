
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Threading
Imports Microsoft.ApplicationBlocks.Updater


' <summary>
' Summary description for ManualInprocForm.
' </summary>

Public Class AutoInprocForm
    Inherits System.Windows.Forms.Form
    Private label1 As System.Windows.Forms.Label
    Private WithEvents exitButton As System.Windows.Forms.Button
    Private eventList As System.Windows.Forms.ListBox
    Private pollThread As Thread = Nothing
    
    Private manifestDownloaded As Integer = 0
    
    ' <summary>
    ' Required designer variable.
    ' </summary>
    Private components As System.ComponentModel.Container = Nothing
    
    
    Public Sub New() 
        '
        ' Required for Windows Form Designer support
        '
        InitializeComponent()
    
    End Sub 'New
     
    
    ' <summary>
    ' Clean up any resources being used.
    ' </summary>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If Not (pollThread Is Nothing) Then
            pollThread.Abort()
        End If
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)

    End Sub 'Dispose

#Region "Windows Form Designer generated code"

    ' <summary>
    ' Required method for Designer support - do not modify
    ' the contents of this method with the code editor.
    ' </summary>
    Private Sub InitializeComponent()
        Me.label1 = New System.Windows.Forms.Label
        Me.exitButton = New System.Windows.Forms.Button
        Me.eventList = New System.Windows.Forms.ListBox
        Me.SuspendLayout()
        ' 
        ' label1
        ' 
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, System.Byte))
        Me.label1.Location = New System.Drawing.Point(24, 32)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(224, 23)
        Me.label1.TabIndex = 0
        Me.label1.Text = "This is the base version 1.0"
        ' 
        ' exitButton
        ' 
        Me.exitButton.Location = New System.Drawing.Point(40, 184)
        Me.exitButton.Name = "exitButton"
        Me.exitButton.TabIndex = 3
        Me.exitButton.Text = "Exit"
        ' 
        ' eventList
        ' 
        Me.eventList.HorizontalScrollbar = True
        Me.eventList.Location = New System.Drawing.Point(24, 64)
        Me.eventList.Name = "eventList"
        Me.eventList.Size = New System.Drawing.Size(240, 95)
        Me.eventList.TabIndex = 4
        ' 
        ' AutoInprocForm
        ' 
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(eventList)
        Me.Controls.Add(exitButton)
        Me.Controls.Add(label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "AutoInprocForm"
        Me.Text = "AutoInprocForm"
        Me.ResumeLayout(False)

    End Sub 'InitializeComponent 
#End Region


    ' <summary>
    ' The main entry point for the application.
    ' </summary>
    'Sub Main()
    '    Application.Run(New AutoInprocForm)

    'End Sub 'Main


    Private Sub CheckAndUpdate()
        Try
            ' Get the updater manager
            Dim updater As ApplicationUpdaterManager = ApplicationUpdaterManager.GetUpdater()

            ' Subscribe for various events
            AddHandler updater.DownloadStarted, AddressOf updater_DownloadStarted
            AddHandler updater.DownloadProgress, AddressOf updater_DownloadProgress
            AddHandler updater.DownloadCompleted, AddressOf updater_DownloadCompleted
            AddHandler updater.DownloadError, AddressOf updater_DownloadError
            AddHandler updater.ActivationInitializing, AddressOf updater_ActivationInitializing
            AddHandler updater.ActivationStarted, AddressOf updater_ActivationStarted
            AddHandler updater.ActivationInitializationAborted, AddressOf updater_ActivationInitializationAborted
            AddHandler updater.ActivationError, AddressOf updater_ActivationError
            AddHandler updater.ActivationCompleted, AddressOf updater_ActivationCompleted

            ' Loop till the updates are available
            Dim manifests As Manifest() = Nothing
            While True
                manifests = updater.CheckForUpdates()
                If manifests.Length > 0 Then
                    ' Prompt user if he wants to apply the updates
                    If MessageBox.Show(Me, "Update for Auto Inproc Application is available, do you want to apply the update?", "Update", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                        Dim m As Manifest
                        For Each m In manifests
                            m.Apply = True
                        Next m
                        ' update the application as per manifest details.
                        updater.Download(manifests, TimeSpan.MaxValue)
                        If manifestDownloaded = manifests.Length Then
                            updater.Activate(manifests)
                            manifestDownloaded = 0
                        End If
                        Exit While
                    Else
                        Thread.Sleep(10000)
                    End If
                Else
                    Thread.Sleep(10000)
                End If
            End While
        Catch ex As ThreadAbortException
            ' Do nothing if the thread is being aborted, as we are explicitly doing it
        Catch ex As Exception
            MessageBox.Show(Me, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub 'CheckAndUpdate


    Private Sub exitButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles exitButton.Click
        Application.Exit()

    End Sub 'exitButton_Click


    Private Sub updater_DownloadStarted(ByVal sender As Object, ByVal e As DownloadStartedEventArgs)
        UpdateList(("DownloadStarted for manifest: " + e.Manifest.ManifestId.ToString()))

    End Sub 'updater_DownloadStarted


    Private Sub updater_DownloadProgress(ByVal sender As Object, ByVal e As DownloadProgressEventArgs)
        UpdateList(("DownloadProgress for manifest: " + e.Manifest.ManifestId.ToString() + "- Files: " + _
                e.FilesTransferred.ToString() + "/" + e.FilesTotal.ToString() + " - Bytes: " + e.BytesTransferred.ToString() + "/" + e.BytesTotal.ToString()))

    End Sub 'updater_DownloadProgress


    Private Sub updater_DownloadCompleted(ByVal sender As Object, ByVal e As ManifestEventArgs)
        UpdateList(("DownloadCompleted for manifest: " + e.Manifest.ManifestId.ToString()))
        manifestDownloaded += 1

    End Sub 'updater_DownloadCompleted


    Private Sub updater_DownloadError(ByVal sender As Object, ByVal e As ManifestErrorEventArgs)
        UpdateList(("DownloadError for manifest: " + e.Manifest.ManifestId.ToString() + vbLf + e.Exception.Message))

    End Sub 'updater_DownloadError


    Private Sub updater_ActivationInitializing(ByVal sender As Object, ByVal e As ManifestEventArgs)
        UpdateList(("ActivationInitializing for manifest: " + e.Manifest.ManifestId.ToString()))

    End Sub 'updater_ActivationInitializing


    Private Sub updater_ActivationStarted(ByVal sender As Object, ByVal e As ManifestEventArgs)
        UpdateList(("ActivationStarted for manifest: " + e.Manifest.ManifestId.ToString()))

    End Sub 'updater_ActivationStarted


    Private Sub updater_ActivationInitializationAborted(ByVal sender As Object, ByVal e As ManifestEventArgs)
        UpdateList(("ActivationInitializationAborted for manifest: " + e.Manifest.ManifestId.ToString()))
        MessageBox.Show(Me, "The Application needs to restart for applying the updates, please restart the application.", "Auto Inproc Updates", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub 'updater_ActivationInitializationAborted


    Private Sub updater_ActivationError(ByVal sender As Object, ByVal e As ManifestErrorEventArgs)
        UpdateList(("ActivationError for manifest: " + e.Manifest.ManifestId.ToString() + vbLf + e.Exception.Message))

    End Sub 'updater_ActivationError


    Private Sub updater_ActivationCompleted(ByVal sender As Object, ByVal e As ActivationCompleteEventArgs)
        UpdateList(("ActivationCompleted for manifest: " + e.Manifest.ManifestId.ToString()))

    End Sub 'updater_ActivationCompleted


    Private Sub UpdateList(ByVal displayString As String)
        eventList.Items.Add(displayString)
        eventList.Update()

    End Sub 'UpdateList


    Private Sub AutoInprocForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Seperate thread is spun to keep polling for updates
        Dim checkUpdateThreadStart As New ThreadStart(AddressOf CheckAndUpdate)
        pollThread = New Thread(checkUpdateThreadStart)
        pollThread.Start()

    End Sub 'AutoInprocForm_Load
End Class 'AutoInprocForm 
