
Imports System
Imports System.Configuration
Imports System.Diagnostics
Imports System.Globalization
Imports System.IO
Imports System.Threading
Imports System.Windows.Forms
Imports Microsoft.ApplicationBlocks.Updater


'<summary>
'This is the main class for AppStart.exe.
'</summary>

NotInheritable Public Class AppStart
    #Region "Private members"
    
    ' <summary>
    ' The configuration for the AppStart process.
    ' </summary>
    Private Shared config As AppStartConfiguration = Nothing
    
    ' <summary>
    ' The process of the running application.
    ' </summary>
    Private Shared applicationProcess As Process
    
    ' <summary>
    ' The mutex name for the AppStart.
    ' </summary>
    Private Shared appStartMutexGuid As New Guid(New Byte() {&H5F, &H4D, &H69, &H63, &H68, &H61, &H65, &H6C, &H20, &H53, &H74, &H75, &H61, &H72, &H74, &H5F})
    
    #End Region
    
    #Region "Constructors"
    
    
    ' <summary>
    ' Static constructor.
    ' </summary>
    Shared Sub New() 
        '  Grab our config instance which we use to read app.config params, figure out
        '  WHERE our target app is and WHAT version
        config = CType(ConfigurationSettings.GetConfig("appStart"), AppStartConfiguration)
    
    End Sub 'New
    
    
    ' <summary>
    ' No reason to ever "construct" this from outside, meant purely as invisible shim.
    ' </summary>
    Private Sub New() 
    
    End Sub 'New 
    #End Region
    
    #Region "Static members"
    
    
    ' <summary>
    ' Main entry point to process.  Checks to see if another instance is running already, disallows if so.
    ' </summary>
    ' <param name="args">Arguments are ignored.</param>
    <STAThread()>  _
    Shared Sub Main(ByVal args() As String) 
        'Check to see if AppStart is already running FOR the particular versioned folder of the target application
        Dim isOwned As Boolean = False
        
        Dim appStartMutex As New Mutex(True, config.ExecutableName + appStartMutexGuid.ToString(), isOwned)
        
        If Not isOwned Then
            MessageBox.Show(String.Format(CultureInfo.CurrentCulture, "There is already a copy of the application '{0}' running.  Please close that application before starting a new one.", config.ExecutableName))
            
            Environment.Exit(1)
        End If
        
        StartAppProcess()
    
    End Sub 'Main
    
    
    ' <summary>
    ' Main process code.
    ' </summary>
    Private Shared Sub StartAppProcess() 
        Dim processStarted As Boolean = False
        
        If config.UpdateTime = UpdateTimeEnum.BeforeStart Then
            AppStart.UpdateApplication()
        End If
        
        'Start the application
        Try
            Dim p As New ProcessStartInfo(config.ExecutablePath)
            p.WorkingDirectory = Path.GetDirectoryName(config.ExecutablePath)
            applicationProcess = Process.Start(p)
            processStarted = True
            Debug.WriteLine("APPLICATION STARTER:  Started app:  " + config.ExecutablePath)
        Catch e As Exception
            Debug.WriteLine("APPLICATION STARTER:  Failed to start process at:  " + config.ExecutablePath)
            HandleTerminalError(e)
        End Try
        
        If processStarted AndAlso config.UpdateTime = UpdateTimeEnum.AfterShutdown Then
            applicationProcess.WaitForExit()
            AppStart.UpdateApplication()
        End If
    
    End Sub 'StartAppProcess
    
    
    Private Shared Sub UpdateApplication() 
        Dim updaterManager As ApplicationUpdaterManager = ApplicationUpdaterManager.GetUpdater(config.GetApplicationID)
        Dim manifests As Manifest() = updaterManager.CheckForUpdates(config.GetManifestUri)
        
        If manifests.Length > 0 Then
            If DialogResult.Yes = MessageBox.Show(Nothing, "Updates available do you want to apply?", "SimpleAppStart", MessageBoxButtons.YesNo, MessageBoxIcon.Question) Then
                Dim manifest As Manifest
                For Each manifest In  manifests
                    manifest.Application.Location = Path.GetDirectoryName(config.ExecutablePath)
                    manifest.Apply = True
                Next manifest
                updaterManager.Download(manifests, TimeSpan.MaxValue)
                updaterManager.Activate(manifests)
            End If
        Else
            MessageBox.Show(Nothing, "No updates available", "SimpleAppStart", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    
    End Sub 'UpdateApplication
    
    
    ' <summary>
    ' Prints out the console exception &amp; shuts down the application.
    ' </summary>
    Private Shared Sub HandleTerminalError(ByVal e As Exception) 
        Debug.WriteLine("APPLICATION STARTER: Terminal error encountered.")
        Debug.WriteLine("APPLICATION STARTER: The following exception was encoutered:")
        Debug.WriteLine(e.ToString())
        Debug.WriteLine("APPLICATION STARTER: Shutting down")
        
        MessageBox.Show(String.Format(CultureInfo.CurrentCulture, "There was an error when trying to start the target application: {0}", e.Message))
        Environment.Exit(0)
    
    End Sub 'HandleTerminalError
    #End Region
End Class 'AppStart