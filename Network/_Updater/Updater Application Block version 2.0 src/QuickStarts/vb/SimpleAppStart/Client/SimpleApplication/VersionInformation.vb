
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms


' <summary>
' Summary description for VersionInformation.
' </summary>

Public Class VersionInformation
    Inherits System.Windows.Forms.Form
    Private label1 As System.Windows.Forms.Label
    Private WithEvents button1 As System.Windows.Forms.Button
    ' <summary>
    ' Required designer variable.
    ' </summary>
    Private components As System.ComponentModel.Container = Nothing
    
    
    Public Sub New() 
        '
        ' Required for Windows Form Designer support
        '
        InitializeComponent()
    
    End Sub 'New
     
    '
    ' TODO: Add any constructor code after InitializeComponent call
    '
    
    ' <summary>
    ' Clean up any resources being used.
    ' </summary>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)

    End Sub 'Dispose

#Region "Windows Form Designer generated code"

    ' <summary>
    ' Required method for Designer support - do not modify
    ' the contents of this method with the code editor.
    ' </summary>
    Private Sub InitializeComponent()
        Me.label1 = New System.Windows.Forms.Label
        Me.button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'label1
        '
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(32, 64)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(208, 40)
        Me.label1.TabIndex = 0
        Me.label1.Text = "This is Simple Application base Version 1.0"
        '
        'button1
        '
        Me.button1.Location = New System.Drawing.Point(96, 152)
        Me.button1.Name = "button1"
        Me.button1.TabIndex = 1
        Me.button1.Text = "Exit"
        '
        'VersionInformation
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.button1)
        Me.Controls.Add(Me.label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "VersionInformation"
        Me.Text = "VersionInformation"
        Me.ResumeLayout(False)

    End Sub 'InitializeComponent 
#End Region


    ' <summary>
    ' The main entry point for the application.
    ' </summary>
    <STAThread()> _
    Shared Sub Main()
        Application.Run(New VersionInformation)

    End Sub 'Main


    Private Sub button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles button1.Click
        Application.Exit()

    End Sub 'button1_Click
End Class 'VersionInformation