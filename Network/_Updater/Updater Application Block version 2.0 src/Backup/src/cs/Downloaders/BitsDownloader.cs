//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// BitsDownloader.cs
//
// Contains the implementation of the BITS downloader.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.ApplicationBlocks.Updater.Utilities;
using Microsoft.Practices.EnterpriseLibrary.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Downloaders
{
	/// <summary>
	/// This downloader uses BITS technology to download files.
	/// </summary>
	public sealed class BitsDownloader : 
		IDownloader, IBackgroundCopyCallback, IDisposable
	{
		#region Declarations

		/// <summary>
		/// This is used to wait for some time between checking the download status to avoid CPU consumtion.
		/// </summary>
		private const int TimeToWaitDuringSynchronousDownload = 200; // milliseconds

		/// <summary>
		/// Maximum time to wait for a pregress event.
		/// </summary>
		private const int BitsNoProgressTimeout = 5; //seconds

		/// <summary>
		/// The delay between reties.
		/// </summary>
		private const int BitsMinimumRetryDelay = 0; //immediate retry

		/// <summary>
		/// Constant for the COM error when an error is requested and no error have been raised.
		/// </summary>
		private const int ExceptionCodeNotAnError = -2145386481;

		/// <summary>
		/// The culture Id.
		/// </summary>
		private readonly int CultureIdForGettingComErrorMessages = Int32.Parse( Resource.ResourceManager[ Resource.MessageKey.CultureIdToGetComErrorStringsFormatted ], CultureInfo.CurrentUICulture );

		/// <summary>
		/// Keeps all the pending downloader jobs.
		/// </summary>
		private HybridDictionary bitsDownloaderJobs = null;

		/// <summary>
		/// Enables access to the downloader configuration.
		/// </summary>
		private UpdaterConfigurationView updaterConfigurationView;

		/// <summary>
		/// Name of the downloader.
		/// </summary>
		private string downloaderName;

		/// <summary>
		/// The error message.
		/// </summary>
		private string cumulativeErrorMessage = String.Empty;

		/// <summary>
		/// The key to the job Id to be stored in the task state
		/// </summary>
		private const string TASK_JOBID_KEY = "jobId";

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public BitsDownloader()
		{
		}

		#endregion

		#region IDownloader Members

		#region Downloader events

		/// <summary>
		/// Notifies the application of the download progress. 
		/// </summary>
		public event DownloadTaskProgressEventHandler DownloadProgress;


		/// <summary>
		/// Notifies the application when the download is complete.
		/// </summary>
		public event DownloadTaskCompletedEventHandler DownloadCompleted;

		/// <summary>
		/// Notifies the application when there is a download error. 
		/// </summary>
		public event DownloadTaskErrorEventHandler DownloadError;


		/// <summary>
		/// Notifies the application that the download has started. 
		/// </summary>
		public event DownloadTaskStartedEventHandler DownloadStarted;

		/// <summary>
		/// Helper method to fire the event.
		/// </summary>
		/// <param name="e">The event information.</param>
		private void OnDownloadStarted( TaskEventArgs e )
		{
			if ( DownloadStarted != null )
			{
				DownloadStarted( this, e );
			}
		}

		/// <summary>
		/// Helper method to fire the event.
		/// </summary>
		/// <param name="e">The event information.</param>
		private void OnDownloadProgress( DownloadTaskProgressEventArgs e )
		{
			if ( DownloadProgress != null )
			{
				DownloadProgress( this, e );
			}
		}

		/// <summary>
		/// Helper method to fire the event.
		/// </summary>
		/// <param name="e">The event information.</param>
		private void OnDownloadCompleted( TaskEventArgs e )
		{
			if ( DownloadCompleted != null )
			{
				DownloadCompleted( this, e );
			}
		}

		/// <summary>
		/// Helper method to fire the event.
		/// </summary>
		/// <param name="e">The event information.</param>
		private void OnDownloadError( DownloadTaskErrorEventArgs e )
		{
			if ( DownloadError != null )
			{
				DownloadError( this, e );
			}
		}

		#endregion

		/// <summary>
		/// Synchronous download method implementation.
		/// </summary>
		/// <param name="task">The UpdaterTask to process.</param>
		/// <param name="maxWaitTime">The maximum wait time.</param>
		[FileIOPermission( SecurityAction.Demand )]
		public void Download(UpdaterTask task, TimeSpan maxWaitTime)
		{
			IBackgroundCopyManager backGroundCopyManager = null;
			IBackgroundCopyJob backgroundCopyJob = null;
			Guid jobID = Guid.Empty;

			try 
			{
				//  create the manager
				backGroundCopyManager = (IBackgroundCopyManager) new BackgroundCopyManager();

				// If the job is already finished, just return
				if ( CheckForResumeAndProceed( backGroundCopyManager, task, out backgroundCopyJob ) )
				{
					return;
				}

				if ( backgroundCopyJob == null )
				{
					//  use utility function to create manager, job, get back jobid etc.; uses 'out' semantics
					CreateCopyJob( 
						backGroundCopyManager, 
						out backgroundCopyJob, 
						ref jobID, 
						Resource.MessageKey.BitsJobName, 
						Resource.MessageKey.BitsDescription );

					// Save the jobId in the task
					task[TASK_JOBID_KEY] = jobID;

					// Prepare the job to download the manifest files
					PrepareJob( backgroundCopyJob, task );
				}

				WaitForDownload( task,  backgroundCopyJob, maxWaitTime );

			}
			catch( Exception e )
			{
				//  if exception, clean up job
				OnJobError( task, backgroundCopyJob, null, e );
			}
			finally
			{
				if( null != backgroundCopyJob ) 
				{
					Marshal.ReleaseComObject( backgroundCopyJob );
				}
				if( null !=  backGroundCopyManager )
				{
					Marshal.ReleaseComObject( backGroundCopyManager );
				}
			}		
		}

		/// <summary>
		/// Asynchronous download method implementation.
		/// </summary>
		/// <param name="task">The UpdaterTask to process.</param>
		[FileIOPermission( SecurityAction.Demand )]
		public void BeginDownload(UpdaterTask task)
		{
			IBackgroundCopyManager backGroundCopyManager = null;
			IBackgroundCopyJob backgroundCopyJob = null;
			Guid jobID = Guid.Empty;

			try
			{
				//  create the manager
				backGroundCopyManager = (IBackgroundCopyManager) new BackgroundCopyManager();

				// If the job is already finished, just return
				if ( CheckForResumeAndProceed( backGroundCopyManager, task, out backgroundCopyJob ) )
				{
					return;
				}

				if ( backgroundCopyJob != null )
				{
					// if CheckForResumeAndProceed connected to an ongoing BITS job
					// wire up our notify interface to forward events to the client

					backgroundCopyJob.SetNotifyInterface( this );
			
					backgroundCopyJob.SetNotifyFlags( (uint)( 
						BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_ERROR | 
						BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_MODIFICATION | 
						BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_TRANSFERRED )
						);
				}
				else
				{
					//  use utility function to create the job. 
					CreateCopyJob( 
						backGroundCopyManager, 
						out backgroundCopyJob, 
						ref jobID, 
						Resource.MessageKey.BitsFilesDownloadJobName,
						Resource.MessageKey.BitsFilesDownloadDescription );

					// Save the jobId in the task
					task[TASK_JOBID_KEY] = jobID;

					// Prepare the job to download the manifest files
					PrepareJob( backgroundCopyJob, task );

					// Set the notify interface to get BITS events
					backgroundCopyJob.SetNotifyInterface( this );

					backgroundCopyJob.SetNotifyFlags( (uint)( 
						BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_ERROR | 
						BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_MODIFICATION | 
						BG_JOB_NOTIFICATION_TYPE.BG_NOTIFY_JOB_TRANSFERRED )
						);

					// Fire our download start event
					OnDownloadStarted( new TaskEventArgs( task ) );

					// Initiate the BITS Job
					backgroundCopyJob.Resume();
				}
			}
			catch( Exception e )
			{
				//  if exception, clean up job
				OnJobError( task, backgroundCopyJob, null, e );
			}
			finally
			{
				if( null != backgroundCopyJob ) 
				{
					Marshal.ReleaseComObject( backgroundCopyJob );
				}
				if( null !=  backGroundCopyManager )
				{
					Marshal.ReleaseComObject( backGroundCopyManager );
				}
			}	
		}

		/// <summary>
		/// Cancels an asynhronous download operation.
		/// </summary>
		/// <param name="task">The <see cref="UpdaterTask"/> for the operation.</param>
		/// <returns>Indicates whether the operation was canceled.</returns>
		public bool CancelDownload(UpdaterTask task)
		{
			IBackgroundCopyManager copyManager = null;
			IBackgroundCopyJob pJob = null;

			if ( task[TASK_JOBID_KEY] != null )
			{
				try
				{
					Guid jobID = (Guid)task[ TASK_JOBID_KEY ];
					copyManager = (IBackgroundCopyManager)new BackgroundCopyManager();
					copyManager.GetJob( ref jobID, out pJob );

					if ( pJob != null )
					{
						pJob.Cancel();
					}
				}
				finally
				{
					if ( copyManager != null )
					{
						Marshal.ReleaseComObject( copyManager );
					}

					if ( pJob != null )
					{
						Marshal.ReleaseComObject( pJob );
					}
				}
			}
			return true;
		}
		
		#endregion

		#region Private helper methods

		/// <summary>
		/// Verifies if the task has a download job assigned, meaning this is a retry.
		/// If a transferred job is detected, the job is completed and the event
		/// OnDownloadCompleted is raised.
		/// </summary>
		/// <param name="copyManager">The BITS background copy manager to use</param>
		/// <param name="task">The UpdaterTask to get the data from</param>
		/// <param name="copyJob">If a on progress BITS job is found for this task, this job is returned on this parameter</param>
		/// <returns>A Boolean value indicating whether the job is completed or not.
		/// A True value means that the job has been completed by BITS while a False value
		/// means that the job doesn't exists or can be resumed.
		/// </returns>
		private bool CheckForResumeAndProceed( IBackgroundCopyManager copyManager, UpdaterTask task, out IBackgroundCopyJob copyJob )
		{
			copyJob = null;
			if ( task[TASK_JOBID_KEY] != null )
			{
				Guid jobId = (Guid)task[TASK_JOBID_KEY];
				BG_JOB_STATE jobState;

				try
				{
					copyManager.GetJob( ref jobId, out copyJob );
					if ( copyJob != null )
					{
						copyJob.GetState( out jobState );
						if ( jobState == BG_JOB_STATE.BG_JOB_STATE_TRANSFERRED )
						{
							OnJobTransferred( task, copyJob );
							return true;
						}
					}
				}
				catch(Exception ex)
				{
					Logger.LogException( new ApplicationUpdaterException( Resource.ResourceManager[ Resource.MessageKey.BITSCannotConnectToJob, jobId, task.TaskId ], ex ) );
				}
			}
			return false;
		}

		/// <summary>
		/// Locate the UpdaterTask associated with the given background job.
		/// </summary>
		/// <param name="pJob">The job reference.</param>
		/// <returns>The UpdaterTask for that job.</returns>
		private UpdaterTask FindTask(IBackgroundCopyJob pJob)
		{
			Guid jobID = Guid.Empty;
			pJob.GetId( out jobID );

			foreach( UpdaterTask task in ApplicationUpdaterManager.GetTasks() )
			{
				if ( Guid.Equals( task[ TASK_JOBID_KEY ], jobID ) )
				{
					return task;
				}
			}

			return null;
		}

		/// <summary>
		/// Waits for the download to complete, for the synchronous usage of the downloader.
		/// </summary>
		/// <param name="backgroundCopyJob">The job instance reference.</param>
		/// <param name="maxWaitTime">The maximum wait time.</param>
		/// <param name="task">The updater task instance.</param>
		private void WaitForDownload( UpdaterTask task, IBackgroundCopyJob backgroundCopyJob, TimeSpan maxWaitTime )
		{
			Guid jobID = Guid.Empty;
			
			bool isCompleted = false;
			bool isSuccessful = false;
			BG_JOB_STATE state;

			try
			{
				backgroundCopyJob.GetId( out jobID );

				//  set endtime to current tickcount + allowable # milliseconds to wait for job
				double endTime = Environment.TickCount + maxWaitTime.TotalMilliseconds;

				while ( !isCompleted )
				{
					backgroundCopyJob.GetState( out state );
					switch( state )
					{
						case BG_JOB_STATE.BG_JOB_STATE_SUSPENDED:
						{
							OnDownloadStarted( new TaskEventArgs( task ) );
							backgroundCopyJob.Resume();
							break;
						}
						case BG_JOB_STATE.BG_JOB_STATE_ERROR:	
						{
							//  use utility to:
							//  a)  get error info 
							//  b)  report it
							//  c)  cancel and remove copy job
							OnJobError( task, backgroundCopyJob, null, null );
							
							//  complete loop, but DON'T say it's successful
							isCompleted = true;
							break;
						}
						case BG_JOB_STATE.BG_JOB_STATE_TRANSIENT_ERROR:
						{							
							//  NOTE:  during debugging + test, transient errors resulted in complete job failure about 90%
							//  of the time.  Therefore we treat transients just like full errors, and CANCEL the job
							//  use utility to manage error etc.
							OnJobError( task, backgroundCopyJob, null, null );
							
							//  stop the loop, set completed to true but not successful
							isCompleted = true;
							break;
						}
						case BG_JOB_STATE.BG_JOB_STATE_TRANSFERRING:
						{
							OnJobModification( task, backgroundCopyJob );
							break;
						}
						case BG_JOB_STATE.BG_JOB_STATE_TRANSFERRED:
						{
							OnJobTransferred( task, backgroundCopyJob );

							isCompleted = true;
							isSuccessful = true;
							break;
						}
						default:
							break;
					}

					if ( isCompleted )
					{
						break;
					}

					if( endTime < Environment.TickCount )
					{
						ApplicationUpdaterException ex = new ApplicationUpdaterException(
							Resource.ResourceManager[ Resource.MessageKey.DownloadTimeoutExceeded ]
							);
						OnJobError( task, backgroundCopyJob, null, ex );
						break;
					}

					//  Avoid 100% CPU utilisation with too tight a loop, let download happen.
					Thread.Sleep( TimeToWaitDuringSynchronousDownload );
				}

				if( !isSuccessful )
				{
					//  create message + error, package it, publish 
					ApplicationUpdaterException ex = new ApplicationUpdaterException( Resource.ResourceManager[ Resource.MessageKey.BitsDownloaderFailedDownload, task.Manifest.ManifestId ] );
					OnJobError( task, backgroundCopyJob, null, ex );
				}
			}
			catch( ThreadInterruptedException tie )
			{
				//  if interrupted, clean up job
				OnJobError( task, backgroundCopyJob, null, tie );
			}
		}

		/// <summary>
		/// Prepares a BITS job adding the files and creating the required folders.
		/// </summary>
		/// <param name="backgroundCopyJob">The BITS job information.</param>
		/// <param name="task">The UpdaterTask instace.</param>
		private void PrepareJob( IBackgroundCopyJob backgroundCopyJob, UpdaterTask task)
		{
			Guid jobID = Guid.Empty;

			backgroundCopyJob.GetId( out jobID );
			task[ TASK_JOBID_KEY ] = jobID;

			foreach( FileManifest sourceFile in task.Manifest.Files )
			{
				string src = task.Manifest.Files.Base + sourceFile.Source;
				string dest = Path.Combine( task.DownloadFilesBase, sourceFile.Source );

				//  to defend against config errors, check to see if the path given is UNC;
				//  if so, throw immediately there's a misconfiguration.  Paths to BITS must be HTTP or HTTPS
				if( FileUtility.IsUncPath( src ) )
				{
					Logger.LogAndThrowException( new ApplicationUpdaterException( Resource.ResourceManager[ Resource.MessageKey.UncPathUnsupported, src ] ) );
				}

				if ( !Directory.Exists( Path.GetDirectoryName( dest ) ) )
				{
					Directory.CreateDirectory( Path.GetDirectoryName( dest ) );
				}

				//  add this file to the job
				backgroundCopyJob.AddFile( src, dest );
			}
		}

		/// <summary>
		/// Internal copy-job factory method.  Used to coordinate all aspects of a job set-up, 
		/// which includes creating a copy manager, creating a job within it, setting download
		/// parameters, and adding the job to our tracking collection for cleanup later
		/// </summary>
		/// <param name="copyManager">null reference to copy manager</param>
		/// <param name="copyJob">null reference to copy job</param>
		/// <param name="jobID">null reference to job id guid</param>
		/// <param name="jobNameKey">the key used to look up the job name in the resource file</param>
		/// <param name="jobDescriptionKey">the key used to look up the job description in the resource file</param>
		private void CreateCopyJob( 
			IBackgroundCopyManager copyManager, 
			out IBackgroundCopyJob copyJob, 
			ref Guid jobID, 
			Resource.MessageKey jobNameKey, 
			Resource.MessageKey jobDescriptionKey )
		{
			
			string jobName = Resource.ResourceManager[ jobNameKey ];
			string jobDesc = Resource.ResourceManager[ jobDescriptionKey ];
						
			// create the job, set its description, use "out" semantics to get jobid and the actual job object
			copyManager.CreateJob( 
				jobName,
				BG_JOB_TYPE.BG_JOB_TYPE_DOWNLOAD,
				out jobID,
				out copyJob );

			//  set useful description
			copyJob.SetDescription( jobDesc );

			//  ***
			//      SET UP BITS JOB SETTINGS--TIMEOUTS/RETRY ETC           
			//      SEE THE FOLLOWING REFERENCES:
			//  **  http://msdn.microsoft.com/library/default.asp?url=/library/en-us/bits/bits/IBackgroundCopyJob2_setminimumretrydelay.asp?frame=true
			//  **  http://msdn.microsoft.com/library/default.asp?url=/library/en-us/bits/bits/IBackgroundCopyJob2_setnoprogresstimeout.asp?frame=true
			//  **  http://msdn.microsoft.com/library/default.asp?url=/library/en-us/bits/bits/bg_job_priority.asp
			//  ***
			
			//  in constant set to 0; this makes BITS retry as soon as possible after an error
			copyJob.SetMinimumRetryDelay( (uint)BitsMinimumRetryDelay );
			//  in constant set to 5 seconds; BITS will set job to Error status if exceeded
			copyJob.SetNoProgressTimeout( (uint)BitsNoProgressTimeout );
			//  make this job the highest (but still background) priority--
			copyJob.SetPriority( BG_JOB_PRIORITY.BG_JOB_PRIORITY_HIGH );
			//  ***

			//----------------------------------------------------------------------
			//-- Data Management and Research, Inc. - Paul Cox - 8/13/2004
			//-- ADDED the following lines to verify credentials of file copy job
			//----------------------------------------------------------------------

			// Set credentials on the job
			VerifyAndSetBackgroundCopyJobCredentials(copyJob);
			
			//----------------------------------------------------------------------
			//-- End ADDED
			//----------------------------------------------------------------------

			//  lock our internal collection of jobs, and add this job--we use this collection in Dispose()
			//  to tell BITS to Cancel() jobs--and remove them from the queue
			//  if we did not do this, BITS would continue for (by default) two weeks to download what we asked!
			lock( bitsDownloaderJobs.SyncRoot )
			{
				bitsDownloaderJobs.Add( jobID, jobName );
			}
		}

		/// <summary>
		/// Method responsible for checking the authentication type and setting the 
		/// appropriate credentials. If the NTLM authentication is used then 
		/// if the username and password are not provided then we use null values. For
		/// all other authentication schemes we need a username and password.
		/// </summary>
		/// <param name="backgroundCopyJob">BackgroundJob on which we need to set the credentials.</param>
		private void VerifyAndSetBackgroundCopyJobCredentials(IBackgroundCopyJob backgroundCopyJob)
		{
			string userName = ((BitsDownloaderProviderData)updaterConfigurationView.GetDownloadProviderData(this.ConfigurationName)).UserName;
			string password = ((BitsDownloaderProviderData)updaterConfigurationView.GetDownloadProviderData(this.ConfigurationName)).Password;
			if( ((BitsDownloaderProviderData)updaterConfigurationView.GetDownloadProviderData(this.ConfigurationName)).AuthenticationScheme == BG_AUTH_SCHEME.BG_AUTH_SCHEME_NTLM )
			{
				if( userName != null && userName.Length == 0 )
				{
					userName = null;
				}
				if( password != null && password.Length == 0 )
				{
					password = null;
				}

				SetBackgroundJobCredentials(backgroundCopyJob);
			}
			else if ( userName != null && userName.Length > 0 && password != null )
			{
				SetBackgroundJobCredentials(backgroundCopyJob);
			}
		}


		/// <summary>
		/// Method responsible for setting the credentials on the BackgroundJob.
		/// </summary>
		/// <param name="backgroundCopyJob">BackgroundJob to set the credentials on.</param>
		private void SetBackgroundJobCredentials(IBackgroundCopyJob backgroundCopyJob)
		{
			IBackgroundCopyJob2 copyJob = (IBackgroundCopyJob2) backgroundCopyJob;
			BG_AUTH_CREDENTIALS credentials = new BG_AUTH_CREDENTIALS();
			credentials.Credentials.Basic.UserName = ((BitsDownloaderProviderData)updaterConfigurationView.GetDownloadProviderData(this.ConfigurationName)).UserName;
			credentials.Credentials.Basic.Password = ((BitsDownloaderProviderData)updaterConfigurationView.GetDownloadProviderData(this.ConfigurationName)).Password;
			credentials.Scheme = ((BitsDownloaderProviderData)updaterConfigurationView.GetDownloadProviderData(this.ConfigurationName)).AuthenticationScheme;
			credentials.Target = ((BitsDownloaderProviderData)updaterConfigurationView.GetDownloadProviderData(this.ConfigurationName)).TargetServerType;
			copyJob.SetCredentials(ref credentials);
		}

		/// <summary>
		/// Removes a copy job from the internal lookup collection.
		/// </summary>
		/// <param name="jobID">GUID identifies of a job (job id).</param>
		private void RemoveCopyJobEntry( Guid jobID )
		{
			//  lock our collection of running jobs; remove it from the job collection
			lock( bitsDownloaderJobs.SyncRoot )
			{
				bitsDownloaderJobs.Remove( jobID );
			}	
		}

		/// <summary>
		/// Method called by BITS when the job is modified, this method is used to notify progress.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <param name="pJob">The BITS job reference.</param>
		private void OnJobModification( UpdaterTask task, IBackgroundCopyJob pJob )
		{
			_BG_JOB_PROGRESS progress;
			pJob.GetProgress( out progress );
			
			DownloadTaskProgressEventArgs args = new DownloadTaskProgressEventArgs( (long)progress.BytesTotal, 
				(long)progress.BytesTransferred, (int)progress.FilesTotal, (int)progress.FilesTransferred, task );

			OnDownloadProgress( args );
		}

		/// <summary>
		/// Centralizes all chores related to stopping and cancelling a copy job, and getting back
		/// from BITS the errors incurred during the job.
		/// </summary>
		/// <param name="task">reference to the job associated task</param>
		/// <param name="pJob">reference to the copy job object (not job id)</param>
		/// <param name="pError">reference to the COM error reported by bits (might be null)</param>
		/// <param name="ex">reference to an exception cosidered as an error (might be null)</param>
		private void OnJobError( UpdaterTask task, IBackgroundCopyJob pJob, IBackgroundCopyError pError, Exception ex )
		{
			string jobDesc = "";
			string jobName = "";
			Guid jobID = Guid.Empty;

			Exception finalException = ex;
			if ( pJob != null )
			{
				//  get information about this job
				pJob.GetDescription( out jobDesc );
				pJob.GetDisplayName( out jobName );
				pJob.GetId( out jobID );

				try
				{
					// if the error hasn't been reported, try to get it
					if ( pError == null )
					{
						pJob.GetError( out pError );
					}
				}
				catch( COMException e )
				{
					Logger.LogException( e );
					if( e.ErrorCode != ExceptionCodeNotAnError	)
					{
						throw e;
					}
				}
				
				// If we've got the native error, wrap into a nicer exception
				if ( pError != null )
				{
					BitsDownloadErrorException BitsEx = new BitsDownloadErrorException( pError, (uint)CultureIdForGettingComErrorMessages );
					cumulativeErrorMessage += BitsEx.Message + Environment.NewLine;
					finalException = BitsEx;
				}
				

				BG_JOB_STATE state;
				pJob.GetState(out state);
				if( state != BG_JOB_STATE.BG_JOB_STATE_ACKNOWLEDGED 
					&& state != BG_JOB_STATE.BG_JOB_STATE_CANCELLED )
				{
					pJob.Cancel();
				}
				RemoveCopyJobEntry( jobID );
			}

			OnDownloadError( new DownloadTaskErrorEventArgs( task, finalException ) );
			Logger.LogAndThrowException( finalException );
		}

		/// <summary>
		/// Method called by BITS when the job is completed.
		/// </summary>
		/// <param name="task">The Updater task instance.</param>
		/// <param name="pJob">The BITS job reeference.</param>
		private void OnJobTransferred( UpdaterTask task, IBackgroundCopyJob pJob )
		{
			pJob.Complete();
			OnDownloadCompleted( new TaskEventArgs( task ) );
		}

		#endregion

		#region IDisposable Implementation

		//  take care of IDisposable too   
		/// <summary>
		/// Allows graceful cleanup of hard resources
		/// </summary>
		public void Dispose() 
		{
			Dispose(true);
			GC.SuppressFinalize(this); 
		}
		
		/// <summary>
		/// used by externally visible overload.
		/// </summary>
		/// <param name="isDisposing">whether or not to clean up managed + unmanaged/large (true) or just unmanaged(false)</param>
		private void Dispose(bool isDisposing) 
		{				
			uint BG_JOB_ENUM_ALL_USERS  = 0x0001;
			uint numJobs;
			uint fetched;
			Guid jobID;
			IBackgroundCopyManager mgr = null;
			IEnumBackgroundCopyJobs jobs = null;
			IBackgroundCopyJob job = null;
			if (isDisposing) 
			{
				try
				{
					mgr = (IBackgroundCopyManager)( new BackgroundCopyManager() );

					mgr.EnumJobs( BG_JOB_ENUM_ALL_USERS, out jobs );

					jobs.GetCount( out numJobs );

					//  lock the jobs collection for duration of this operation
					lock( bitsDownloaderJobs.SyncRoot)
					{
						for( int i = 0; i < numJobs; i++ )
						{
							//  use jobs interface to walk through getting each job
							jobs.Next( (uint)1, out job, out fetched );
						
							//  get jobid guid
							job.GetId( out jobID );

							//  check if the job is in OUR collection; if so cancel it.  we obviously don't want to get
							//  jobs from other Updater threads/processes, or other BITS jobs on the machine!
							if( bitsDownloaderJobs.Contains( jobID ) )
							{
								//  take ownership just in case, and cancel() it
								job.TakeOwnership();
								job.Cancel();	
								// remove from our collection
								bitsDownloaderJobs.Remove( jobID );					
							}
						}
					}
				}
				finally
				{
					if( null != mgr )
					{
						Marshal.ReleaseComObject( mgr );
						mgr = null;
					}
					if( null != jobs )
					{
						Marshal.ReleaseComObject( jobs );
						jobs = null;
					}
					if( null != job )
					{
						Marshal.ReleaseComObject( job );
						job = null;
					}
				}
			}
		}

		
		/// <summary>
		/// Destructor/Finalizer
		/// </summary>
		~BitsDownloader()
		{
			// Simply call Dispose(false).
			Dispose(false);
		}

		#endregion

		#region IBackgroundCopyCallback Members

		/// <summary>
		/// BITS notifies about job finished using this method.
		/// </summary>
		/// <param name="pJob">The BITS job reference.</param>
		void IBackgroundCopyCallback.JobTransferred(IBackgroundCopyJob pJob)
		{
			OnJobTransferred( FindTask( pJob ), pJob );
		}

		/// <summary>
		/// BITS notifies about job error using this method.
		/// </summary>
		/// <param name="pJob">The BITS job reference.</param>
		/// <param name="pError">The error information.</param>
		void IBackgroundCopyCallback.JobError(IBackgroundCopyJob pJob, IBackgroundCopyError pError)
		{
			OnJobError( FindTask( pJob ), pJob, pError, null );
		}

		/// <summary>
		/// BITS notifies about job finished using this method.
		/// </summary>
		/// <param name="pJob">The BITS job reference.</param>
		/// <param name="dwReserved">Reserved for BITS.</param>
		void IBackgroundCopyCallback.JobModification(IBackgroundCopyJob pJob, uint dwReserved)
		{
			OnJobModification( FindTask( pJob ), pJob );
		}

		#endregion

		#region IConfigurationProvider Members

		/// <summary>
		/// The name of the configuration provider.
		/// </summary>
		public string ConfigurationName
		{
			get
			{
				return downloaderName;
			}
			set
			{
				downloaderName = value;
			}
		}

		/// <summary>
		/// Method called by Enterprise Library provider model; it is not used because the IDownloader interface also provides this feature. 
		/// </summary>
		/// <param name="configurationView">The configuration view instance.</param>
		public void Initialize(ConfigurationView configurationView)
		{
			try
			{
				updaterConfigurationView = (UpdaterConfigurationView)configurationView;
			}
			catch(Exception ex)
			{
				Logger.LogAndThrowException( ex );
			}
			finally
			{
				bitsDownloaderJobs = new HybridDictionary( 10 );
			}
		}

		#endregion
	}
}
