//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// GlobalAssemblyInfo.cs
//
// Contains the common information for all the assemblies.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: CLSCompliantAttribute( true )]

[assembly: AssemblyCompany("Microsoft Corp.")]
[assembly: AssemblyProduct("Updater Application Block")]
[assembly: AssemblyCopyright("2004 Microsoft Corp.")]
[assembly: AssemblyTrademark("")]

[assembly: AssemblyVersion("2.0.0.0")]

[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyName("")]

[assembly: ComVisible(false)]

[assembly: SecurityPermission(
	SecurityAction.RequestMinimum, 
	Flags = SecurityPermissionFlag.Assertion | SecurityPermissionFlag.Execution )]
