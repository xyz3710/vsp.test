//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// InstallUtilProcessor.cs
//
// Contains the implementation of the InstallUtil processor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using System.Globalization;
using Microsoft.Win32;
using System.Xml;
using System.IO;
using System.Text;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
{
	/// <summary>
	/// This processor executes the InstallUtil tool included with the .NET Framework to perform the installer actions defined in the specified asemblies.
	/// </summary>
	/// <remarks>
	/// These attributes are defined to configure this processor. They are mandatory except where noted.
	/// <list type="table">
	/// <listheader><term>Attribute</term><description>Description</description></listheader>
	/// <item><term>type</term><description>The type name for this processor</description></item>
	/// <item><term>action</term><description>It can be "Install" or "Uninstall"</description></item>
	/// </list>	
	/// Child elements
	/// <list type="table">
	/// <listheader><term>Element</term><description>Description</description></listheader>
	/// <item><term>assemblies</term><description>The list of assemblies to process. Contains a list of assembly elements.</description></item>
	/// </list>	
	/// assembly element attributes
	/// <list type="table">
	/// <listheader><term>Attribute</term><description>Description</description></listheader>
	/// <item><term>path</term><description>Path to the assembly file to process, relative to the application location</description></item>
	/// <item><term>options</term><description>List of options to be passed to the InstallUtil for processing this assembly</description></item>
	/// </list>	
	/// </remarks>
	/// <example>
	/// The following XML excerpt demonstrates how to configure this processor in the manifest file.
	/// <code>
	/// &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
	///		...
	///		&lt;files base="http://some-server/manifests/" &gt;
	///			&lt;file source="myCustomAction.dll"&gt;
	///		&lt;/files&gt;
	///		&lt;activation&gt;
	///			&lt;tasks&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					source="myCustomAction.dll"
	///					destination="installers\myCustomAction.dll"
	///				/&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.InstallUtilProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					action="Install" &gt;
	///					&lt;assemblies&lt;
	///						&lt;assembly path="installers\myCustomAction.dll" options="/LogFile=myCustomAction.log" &gt;
	///					&lt;\assemblies&gt;
	///				&lt;\task&gt;
	///			&lt;/tasks&gt;
	///		&lt;/activation&gt;
	///	&lt;/manifest&gt;
	///	</code>
	/// </example>
	public class InstallUtilProcessor : IActivationProcessor
	{
		/// <summary>
		/// The parameter provided to the tool in order to select the InstallUtil mode.
		/// </summary>
		internal enum InstallUtilAction
		{
			/// <summary>
			/// Install an assembly.
			/// </summary>
			Install,

			/// <summary>
			/// Uninstall an assembly.
			/// </summary>
			Uninstall
		}

		#region Private members

		/// <summary>
		/// The UpdaterTask provided in the Init method.
		/// </summary>
		private UpdaterTask task;

		/// <summary>
		/// The arguments for the processor in the manifest file.
		/// </summary>
		private string arguments = String.Empty;

		/// <summary>
		/// The path to the .net tool.
		/// </summary>
		private string utilPath = String.Empty;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public InstallUtilProcessor()
		{
		}

		#endregion

		#region IActivationProcessor Members

		/// <summary>
		/// Executes the processor.
		/// </summary>
		public void Execute()
		{
			using(Process installUtilProcess = new Process())
			{
				installUtilProcess.StartInfo.FileName = utilPath;
				installUtilProcess.StartInfo.Arguments = arguments;
				installUtilProcess.Start();
				installUtilProcess.WaitForExit();
				if ( installUtilProcess.ExitCode != 0 )
				{
					throw new Exception(  Resource.ResourceManager[ Resource.MessageKey.ToolGeneratedError, "installutil.exe", installUtilProcess.ExitCode ] ); 
				}
			}
		}

		/// <summary>
		/// Initializes the processor using the manifest configuration and the UpdaterTask instance.
		/// </summary>
		/// <param name="config">The configuration for the processor in the manifest file.</param>
		/// <param name="task">The UpdaterTask instance.</param>
		public void Init(ActivationProcessorProviderData config, UpdaterTask task)
		{
			this.task = task;

			if ( config.AnyAttributes == null || config.AnyElement == null )
			{
				throw new ConfigurationException( Resource.ResourceManager[ Resource.MessageKey.ProcessorNotConfigured ] );
			}

			InstallUtilAction action = InstallUtilAction.Install;

			StringBuilder sb = new StringBuilder();

			foreach( XmlAttribute attr in config.AnyAttributes )
			{
				if ( String.Compare( attr.Name, "action", false, CultureInfo.InvariantCulture) == 0 )
				{
					action = (InstallUtilAction)Enum.Parse( typeof( InstallUtilAction ), attr.Value, false );
					
				}
			}

			if ( action == InstallUtilAction.Uninstall )
			{
				sb.Append( "/uninstall");
			}
			sb.Append( " " );

			string assemblyPath = null;
			foreach( XmlNode elm in config.AnyElement.ChildNodes )
			{
				if ( String.Compare( elm.Name, "assembly", false, CultureInfo.InvariantCulture ) == 0 )
				{
					assemblyPath = elm.Attributes["path"].Value;

					if ( !Path.IsPathRooted( assemblyPath ) )
					{
						assemblyPath = Path.Combine( this.task.Manifest.Application.Location, assemblyPath );
					}

					sb.Append( elm.Attributes["options"].Value );
					sb.AppendFormat( " \"{0}\"", assemblyPath );
					sb.Append( " " );
				}
			}

			arguments = sb.ToString();
		}

		/// <summary>
		/// If the activation fails this method is called to revert the operations performed by the processor.
		/// </summary>
		public void OnError()
		{
		}

		/// <summary>
		/// Prepares the execution and throws an exception if the execution is not possible.
		/// </summary>
		public void PrepareExecution()
		{
			// Get the .Net Framework folder
			utilPath = Utilities.FileUtility.GetLatestDotNetFrameworkPath();
			if ( utilPath != null && utilPath.Length == 0 )
			{
				throw new InvalidOperationException( Resource.ResourceManager[ Resource.MessageKey.CannotFindDotNetFolder ] );
			}

			utilPath = Path.Combine( utilPath, "InstallUtil.exe" );
			// Validate the existence of the file.
			if ( !File.Exists( utilPath ) )
			{
				throw new InvalidOperationException( Resource.ResourceManager[ Resource.MessageKey.CannotFindTool, "installutil.exe" ] );
			}
		}

		#endregion
	}
}
