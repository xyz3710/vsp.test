//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// MsiProcessor.cs
//
// Contains the implementation of the MSI processor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;
using WindowsInstaller;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
{
	/// <summary>
	/// This processor uses Windows Installer to execute installation databases in the local system.
	/// </summary>
	/// <remarks>
	/// These attributes are defined to configure this processor. They are mandatory except where noted.
	/// <list type="table">
	/// <listheader><term>Attribute</term><description>Description</description></listheader>
	/// <item><term>type</term><description>The type name for this processor</description></item>
	/// </list>	
	/// The config element is defined to configure this processor, and has these child elements
	/// <list type="table">
	/// <listheader><term>Element</term><description>Description</description></listheader>
	/// <item><term>installType</term><description>The installation action to perform. Can be "Install", "Remove" or "Patch"</description></item>
	/// <item><term>packagePath</term><description>A reference to the msi to process as specified on the files list</description></item>
	/// <item><term>propertyValues</term><description>A list of pairs of property=value terms to initialize the installer</description></item>
	/// <item><term>uiLevel</term><description>Indicates the type of user interface to be used when opening and processing the package</description></item>
	/// <item><term>productCode</term><description>The GUID for the product. Can be used to uninstall the product without specifying the packagePath</description></item>
	/// </list>	
	/// </remarks>
	/// <example>
	/// <code>
	/// &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
	///		...
	///		&lt;files base="http://some-server/manifests/" &gt;
	///			&lt;file source="setup.msi"&gt;
	///		&lt;/files&gt;
	///		&lt;activation&gt;
	///			&lt;tasks&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.MSIProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" &gt;
	///					&lt;config&gt;
	///						&lt;installType&gt;Install&lt;/installType&gt;
	///						&lt;packagePath&gt;setup.msi&lt;/packagePath&gt;
	///						&lt;propertyValues&gt;ACTION=INSTALL&gt;/propertyValues&lt;
	///						&lt;uiLevel&gt;msiUILevelFull&lt;/uiLevel&gt;
	///					&lt;/config&gt;
	///				&lt;/task&gt;
	///			&lt;/tasks&gt;
	///		&lt;/activation&gt;
	///	&lt;/manifest&gt;	
	/// </code>
	/// This code sample tells the updater to download the Setup.msi file and run 
	/// Windows Installer on it to perform the product installation and showing the 
	/// full user interface support. 
	/// </example>
	public class MsiProcessor : IActivationProcessor
	{
		/// <summary>
		/// The type of installation included in the processor configuration in the manifest file.
		/// </summary>
		internal enum InstallType
		{
			/// <summary>
			/// The file specified is an MSI file so the processor will install the database in the local system.
			/// </summary>
			Install,

			/// <summary>
			/// The file specified is an MSP file so the processor will apply the patch to an existing database.
			/// </summary>
			Patch,

			/// <summary>
			/// Remove a product id from the system.
			/// </summary>
			Remove,
		}

		#region Private fields

		/// <summary>
		/// The UpdaterTask provided in the Init method.
		/// </summary>
		private UpdaterTask taskToProcess;

		/// <summary>
		/// The type of the installation specified in the processor configuration.
		/// </summary>
		private InstallType msiPackageInstallType;

		/// <summary>
		/// The path to the Windows Installer database.
		/// </summary>
		private string msiPackagePath = String.Empty;

		/// <summary>
		/// The properties specified for the selected operation.
		/// </summary>
		private string msiPropertyValues = String.Empty;

		/// <summary>
		/// The code of the product that must be uninstalled.
		/// </summary>
		private string msiProductCode = String.Empty;

		/// <summary>
		/// The level of UI provided to Windows Installer so no UI is displayed.
		/// </summary>
		private MsiUILevel uiLevelForTheMSI = MsiUILevel.msiUILevelProgressOnly;

		#endregion

		#region IActivationProcessor Members

		/// <summary>
		/// Executes the processor.
		/// </summary>
		public void Execute()
		{
			Installer installer = (Installer)new CWindowsInstaller();
			try
			{
				installer.UILevel = uiLevelForTheMSI;
				if ( msiPackageInstallType == InstallType.Patch )
				{
					if ( msiPackagePath == null || msiPackagePath.Length == 0 || !File.Exists( msiPackagePath ) )
					{
						throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.ArgumentMissing1, "packagePath" ] );
					}

					installer.ApplyPatch( msiPackagePath, "", MsiInstallType.msiInstallTypeDefault, msiPropertyValues );
				}
				if ( msiPackageInstallType == InstallType.Install )
				{
					if ( msiPackagePath == null || msiPackagePath.Length == 0 || !File.Exists( msiPackagePath ) )
					{
						throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.ArgumentMissing1 , "packagePath" ] );
					}
					installer.InstallProduct( msiPackagePath, msiPropertyValues );
				}
				if ( msiPackageInstallType == InstallType.Remove )
				{
					if ( msiProductCode == null || msiProductCode.Length == 0 )
					{
						throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.ArgumentMissing], "productCode" );
					}
					RemoveProduct( installer, msiProductCode );
				}
			}
			finally
			{
				if ( installer != null )
				{
					Marshal.ReleaseComObject( installer );
				}
			}
		}

		/// <summary>
		/// Initializes the processor using the manifest configuration and the UpdaterTask instance.
		/// </summary>
		/// <param name="config">The configuration for the processor in the manifest file.</param>
		/// <param name="task">The UpdaterTask instance.</param>
		public void Init( ActivationProcessorProviderData config, UpdaterTask task)
		{
			taskToProcess = task;

			foreach( XmlNode node in config.AnyElement.ChildNodes )
			{
				switch ( node.LocalName )
				{
					case "installType":
						msiPackageInstallType = (InstallType)Enum.Parse( typeof( InstallType ), node.InnerText );
						break;
					case "packagePath":
						msiPackagePath = node.InnerText;
						if( !Path.IsPathRooted( msiPackagePath ) )
						{
							msiPackagePath = Path.Combine( taskToProcess.DownloadFilesBase, msiPackagePath );
						}
						break;
					case "propertyValues":
						msiPropertyValues = node.InnerText;
						break;
					case "uiLevel":
						uiLevelForTheMSI = (MsiUILevel)Enum.Parse( typeof(MsiUILevel), node.InnerText );
						break;
					case "productCode":
						msiProductCode = node.InnerText;
						break;
				}
			}
		}

		/// <summary>
		/// If the activation fails this method is called to revert the operations performed by the processor.
		/// </summary>
		public void OnError()
		{
		}

		/// <summary>
		/// Prepares the execution and throws an exception if the execution is not possible.
		/// </summary>
		/// <remarks>This method is not implemented since there is no validation process required for this processor.</remarks>
		public void PrepareExecution()
		{
		}

		#endregion

		#region Private members
		
		/// <summary>
		/// Helper method to remove a product from the local system.
		/// </summary>
		/// <param name="installer">The Windows Installer instance.</param>
		/// <param name="productCode">The producr code to uninstall.</param>
		private void RemoveProduct( Installer installer, string productCode )
		{
			MsiInstallState state = installer.get_ProductState( productCode );
			if ( state == MsiInstallState.msiInstallStateDefault )
			{
				Session session = null;
				try
				{
					session = installer.OpenProduct( productCode );
					session.set_Property( "REMOVE", "ALL");
					session.DoAction( "INSTALL" );
				}
				finally
				{
					if ( session != null )
					{
						Marshal.ReleaseComObject( session );
					}
				}
			}
		}

		#endregion

	}

	[ComImport(),Guid("000C1090-0000-0000-C000-000000000046")]
	internal class CWindowsInstaller {}
}
