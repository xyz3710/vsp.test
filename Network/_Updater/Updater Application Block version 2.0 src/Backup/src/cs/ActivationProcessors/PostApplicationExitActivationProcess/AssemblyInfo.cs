//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// AssemblyInfo.cs
//
// 
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly:	FileIOPermission( SecurityAction.RequestMinimum ) ]
[assembly:	SecurityPermission(SecurityAction.RequestMinimum, Flags = SecurityPermissionFlag.UnmanagedCode)]
[assembly:	SecurityPermission(SecurityAction.RequestMinimum, ControlThread = true)]

[assembly: AssemblyVersion("2.0.0.0" ) ]

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: AssemblyTitle("ApplicationUpdater")]
[assembly: AssemblyDescription("Microsoft Updater Application Block")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("Bluebricks: UAB")]
[assembly: AssemblyCopyright("(c) 2003 Microsoft Corporation")]	

[assembly: AssemblyDelaySign(false)]

