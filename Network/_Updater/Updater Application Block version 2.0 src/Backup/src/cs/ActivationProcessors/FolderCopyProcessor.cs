//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// FlderCopyProcessor.cs
//
// Contains the implementation of the FolderCopy processor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Xml;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
{
	/// <summary>
	/// This processor recursively copies the contents of the specified folder to another folder.
	/// </summary>
	/// <remarks>
	/// These attributes are defined to configure this processor. They are mandatory except where noted.
	/// <list type="table">
	/// <listheader><term>Attribute</term><description>Description</description></listheader>
	/// <item><term>type</term><description>The type name for this processor</description></item>
	/// <item><term>source</term><description>A reference to a source folder as defined by the source attribute in the files list</description></item>
	/// <item><term>destination</term><description>The destination directory. This path is relative to the application location, if a full path is not specified</description></item>
	/// <item><term>overwrite</term><description>Optional. Indicates whether to overwrite existing files under the destination directory. A False value indicates to skip overwriting the existing files. Default is True.</description></item>
	/// </list>
	/// </remarks>
	/// <example>
	/// The following XML excerpt demonstrates how to configure this processor in the manifest file.
	/// <code>
	/// &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
	///		...
	///		&lt;files base="http://some-server/manifests/" &gt;
	///			&lt;file source="file1"&gt;
	///			&lt;file source="file2"&gt;
	///			&lt;file source="temp\file3"&gt;
	///			&lt;file source="doc\help"&gt;
	///		&lt;/files&gt;
	///		&lt;activation&gt;
	///			&lt;tasks&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FolderCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					source="."
	///					destination="."
	///					overwrite="False"
	///				/&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FolderCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					source="doc"
	///					destination="doc"
	///				/&gt;
	///			&lt;/tasks&gt;
	///		&lt;/activation&gt;
	///	&lt;/manifest&gt;
	/// </code>
	/// </example>
	public class FolderCopyProcessor : IActivationProcessor
	{
		#region Private members

		/// <summary>
		/// The UpdaterTask provided in the Init method.
		/// </summary>
		private UpdaterTask task;

		/// <summary>
		/// The source folder specification placed in the manifest file.
		/// </summary>
		private string sourceFolderSpec = String.Empty;

		/// <summary>
		/// The target folder specification placed in the manifest file.
		/// </summary>
		private string destFolderSpec = String.Empty;

		/// <summary>
		/// Whether the overwite of the target files is enabled or not.
		/// </summary>
		private bool enableOverwrite = true;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public FolderCopyProcessor()
		{
		}

		#endregion

		#region IActivationProcessor Members

		/// <summary>
		/// Executes the processor.
		/// </summary>
		/// <remarks>Uses the helper function to recursively copy the contents of a folder into a new one.</remarks>
		public void Execute()
		{
			if ( !Path.IsPathRooted( sourceFolderSpec ) )
			{
				sourceFolderSpec = Path.Combine( task.DownloadFilesBase, sourceFolderSpec );
			}

			if ( !Path.IsPathRooted( destFolderSpec ) )
			{
				destFolderSpec = Path.Combine( task.Manifest.Application.Location, destFolderSpec );
			}

			Utilities.FileUtility.CopyDirectory( sourceFolderSpec, destFolderSpec, enableOverwrite );
		}

		/// <summary>
		/// Initializes the processor using the manifest configuration and the UpdaterTask instance.
		/// </summary>
		/// <param name="config">The configuration for the processor in the manifest file.</param>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <remarks>This method is validating the correctness of the validation and will abort the activation if the 
		/// configuration is not correct.</remarks>
		public void Init(ActivationProcessorProviderData config, UpdaterTask task)
		{
			this.task = task;

			if ( config.AnyAttributes == null )
			{
				throw new ConfigurationException( Resource.ResourceManager[ Resource.MessageKey.ProcessorNotConfigured] );
			}

			foreach(XmlAttribute attr in config.AnyAttributes)
			{
				if ( String.Compare( attr.Name, "source", false, CultureInfo.InvariantCulture ) == 0 )
				{
					sourceFolderSpec = attr.Value;
				}
				if ( String.Compare( attr.Name, "destination", false, CultureInfo.InvariantCulture ) == 0 )
				{
					destFolderSpec = attr.Value;
				}
				if ( String.Compare( attr.Name, "overwrite", false, CultureInfo.InvariantCulture ) == 0 )
				{
					enableOverwrite = bool.Parse( attr.Value );
				}
			}

			if ( ( sourceFolderSpec != null && sourceFolderSpec.Length == 0 ) || ( destFolderSpec != null && destFolderSpec.Length == 0 ) )
			{
				throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.SourceAndDestinationExpected ] );
			}		
		}

		/// <summary>
		/// If the activation fails this method is called to revert the operations performed by the processor.
		/// </summary>
		/// <remarks>No error handling implemented in this processor.</remarks>
		public void OnError()
		{
		}

		/// <summary>
		/// Prepares the execution and throws an exception if the execution is not possible.
		/// </summary>
		/// <remarks>This method is not implemented because there is no validation process required for this processor.</remarks>
		public void PrepareExecution()
		{
		}

		#endregion
	}
}
