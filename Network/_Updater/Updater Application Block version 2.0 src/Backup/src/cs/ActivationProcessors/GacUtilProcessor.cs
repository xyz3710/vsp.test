//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// GacUtilProcessor.cs
//
// Contains the implementation of the GacUtil processor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Xml;
using Microsoft.Win32;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.ActivationProcessors
{
	/// <summary>
	/// This processor executes the GacUtil tool included with the .NET Framework 
	/// to install files in the global assembly cache.
	/// </summary>
	/// <remarks>
	/// These attributes are defined to configure this processor. They are mandatory except where noted.
	/// <list type="table">
	/// <listheader><term>Attribute</term><description>Description</description></listheader>
	/// <item><term>type</term><description>The type name for this processor</description></item>
	/// <item><term>options</term><description>The list of options to be passed to the gacutil utility</description></item>
	/// <item><term>assemblyFile</term><description>Optional. The path (relative to the application location) to an assembly dll to process</description></item>
	/// <item><term>assemblyName</term><description>Optional. The name of an assembly to process</description></item>
	/// </list>	
	/// One of the assemblyFile or assemblyName attributes must be specified.
	/// </remarks>
	/// <example>
	/// The following XML excerpt demonstrates how to configure this processor in the manifest file.
	/// <code>
	/// &lt;manifest manifestId="{311085F7-9320-4318-9A67-9BE32F04E933}" mandatory="False" xmlns=... &gt;
	///		...
	///		&lt;files base="http://some-server/manifests/" &gt;
	///			&lt;file source="myAssembly.dll"&gt;
	///		&lt;/files&gt;
	///		&lt;activation&gt;
	///			&lt;tasks&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					source="myAssembly.dll"
	///					destination="bin\myAssembly.dll"
	///				/&gt;
	///				&lt;task type="Microsoft.ApplicationBlocks.Updater.ActivationProcessors.GacUtilProcessor, Microsoft.ApplicationBlocks.Updater.ActivationProcessors" 
	///					options="/i"
	///					assemblyFile="bin\myAssembly.dll"
	///				/&gt;
	///			&lt;/tasks&gt;
	///		&lt;/activation&gt;
	///	&lt;/manifest&gt;
	/// </code>
	/// This manifest indicates the updater to download the file myAssembly.dll, to copy it to the application folder bin subfolder
	/// and to register the assembly in the global assembly cache.
	/// </example>
	public class GacUtilProcessor : IActivationProcessor
	{
		#region Private members

		/// <summary>
		/// The UpdaterTask provided in the Init method.
		/// </summary>
		private UpdaterTask task;

		/// <summary>
		/// Options passed to the GacUtil tool.
		/// </summary>
		private string options = String.Empty;

		/// <summary>
		/// The assembly specification placed in the manifest file.
		/// </summary>
		private string assemblySpec = String.Empty;

		/// <summary>
		/// The assembly specification placed in the manifest file.
		/// </summary>
		private string assemblyName = String.Empty;

		/// <summary>
		/// Stores the path to the .net tool.
		/// </summary>
		private string utilPath = String.Empty;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public GacUtilProcessor()
		{
		}

		#endregion

		#region IActivationProcessor Members

		/// <summary>
		/// Executes the processor.
		/// </summary>
		public void Execute()
		{
			if ( ( assemblySpec != null && assemblySpec.Length != 0 ) && !Path.IsPathRooted( assemblySpec ) )
			{
				assemblySpec = Path.Combine( task.Manifest.Application.Location, assemblySpec );
			}

			// Create the process and run the tool.
			using(Process gacProcess = new Process())
			{
				gacProcess.StartInfo.FileName = utilPath;
				gacProcess.StartInfo.Arguments = String.Format( CultureInfo.InvariantCulture, "{0} {1}", options, 
					assemblySpec.Length > 0 ? assemblySpec : assemblyName );
				gacProcess.Start();
				gacProcess.WaitForExit();
				if ( gacProcess.ExitCode != 0 )
				{
					throw new Exception(  Resource.ResourceManager[ Resource.MessageKey.ToolGeneratedError, "gacutil.exe", gacProcess.ExitCode ] ); 
				}
			}
		}

		/// <summary>
		/// Initializes the processor using the manifest configuration and the UpdaterTask instance.
		/// </summary>
		/// <param name="config">The configuration for the processor in the manifest file.</param>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <remarks>Validate the manifest configuration for correctness.</remarks>
		public void Init(ActivationProcessorProviderData config, UpdaterTask task)
		{
			this.task = task;

			if ( config.AnyAttributes == null )
			{
				throw new ConfigurationException( Resource.ResourceManager[ Resource.MessageKey.ProcessorNotConfigured ] );
			}

			foreach(XmlAttribute attr in config.AnyAttributes)
			{
				if ( String.Compare( attr.Name, "options", false, CultureInfo.InvariantCulture) == 0 )
				{
					options = attr.Value;
				}
				if ( String.Compare( attr.Name, "assemblyFile", false, CultureInfo.InvariantCulture) == 0 )
				{
					assemblySpec = attr.Value;
				}
				if ( String.Compare( attr.Name, "assemblyName", false, CultureInfo.InvariantCulture ) == 0 )
				{
					assemblyName = attr.Value;
				}
			}

			if ( ( options != null && options.Length == 0) || ( (assemblySpec != null && assemblySpec.Length == 0) && (assemblyName != null && assemblyName.Length == 0) ))
			{
				throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.AssemblyReferenceExpected ] );
			}		
		}

		/// <summary>
		/// If the activation fails this method is called to revert the operations performed by the processor.
		/// </summary>
		public void OnError()
		{
		}

		/// <summary>
		/// Prepares the execution and throws an exception if the execution is not possible.
		/// </summary>
		public void PrepareExecution()
		{
			// Get the .Net Framework folder
			utilPath = Utilities.FileUtility.GetLatestDotNetFrameworkPath();
			if ( utilPath != null && utilPath.Length == 0 )
			{
				throw new InvalidOperationException( Resource.ResourceManager[ Resource.MessageKey.CannotFindDotNetFolder ] );
			}

			utilPath = Path.Combine( utilPath, "GacUtil.exe" );
			// Validate the existence of the file.
			if ( !File.Exists( utilPath ) )
			{
				throw new InvalidOperationException( Resource.ResourceManager[ Resource.MessageKey.CannotFindTool, "gactuil.exe" ] );
			}
		}

		#endregion
	}
}
