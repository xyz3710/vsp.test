//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// IDownloader.cs
//
// Contains the definition if the IDownloader interface.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using Microsoft.Practices.EnterpriseLibrary.Configuration;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// Defines the contract that all downloaders must implement
	/// to be used as an updater downloader.
	/// </summary>
	public interface IDownloader : IConfigurationProvider
	{
		/// <summary>
		/// Performs the synchronous download of the files specified in the manifest.
		/// </summary>
		/// <param name="task">The associated <see cref="UpdaterTask"/> that holds a reference to the manifest to process</param>
		/// <param name="maxWaitTime">A time span indicating the maximum period of time to wait for a download. 
		/// An exception must be thrown if this period is exeeded.</param>
		void Download(UpdaterTask task, TimeSpan maxWaitTime);
		
		/// <summary>
		/// Initiates the asynchronous download of the files specified in the manifest.
		/// </summary>
		/// <param name="task">The associated <see cref="UpdaterTask"/> that holds a reference to the manifest to process.</param>
		void BeginDownload(UpdaterTask task);

		/// <summary>
		/// Terminates or cancels an unfinished asynchronous download.
		/// </summary>
		/// <param name="task">The associated <see cref="UpdaterTask"/> that holds a reference to the manifest to process</param>
		/// <returns>Returns true if the task was canceled.</returns>
		bool CancelDownload(UpdaterTask task);

		/// <summary>
		/// Notifies about the download progress for the update.
		/// </summary>
		event DownloadTaskProgressEventHandler DownloadProgress;
		
		/// <summary>
		/// Notifies that the downloading for an UpdaterTask has started.
		/// </summary>
		event DownloadTaskStartedEventHandler DownloadStarted;

		/// <summary>
		/// Notifies that the downloading for an UpdaterTask has finished.
		/// </summary>
		event DownloadTaskCompletedEventHandler DownloadCompleted;

		/// <summary>
		/// Notifies that an error ocurred while downloading the files for an UpdaterTask.
		/// </summary>
		event DownloadTaskErrorEventHandler DownloadError;

	}

}
