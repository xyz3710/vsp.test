//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ApplicationUpdaterException.cs
//
// Contains the implementation of the generic application updater exception.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Security.Permissions;
using System.Runtime.Serialization;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// This is a generic exception thrown by the updater.
	/// </summary>
	[Serializable]
	public class ApplicationUpdaterException : Exception 
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ApplicationUpdaterException() : base()
		{
		}

		/// <summary>
		/// Creates an ApplicationUpdaterException with a specified message.
		/// </summary>
		/// <param name="message">The exception message string.</param>
		public ApplicationUpdaterException( string message ) : base( message )
		{
		}

		/// <summary>
		/// Creates an ApplicationUpdaterException with a specified message and an inner exception.
		/// </summary>
		/// <param name="message">The exception message string.</param>
		/// <param name="innerException">The inner exception detected.</param>
		public ApplicationUpdaterException( string message, Exception innerException ) : base( message, innerException )
		{
		}

		/// <summary>
		/// Serialization constructor used for the serialization of the exception.
		/// </summary>
		/// <param name="info">The serialization information.</param>
		/// <param name="context">The serialization context.</param>
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		protected ApplicationUpdaterException(SerializationInfo info, StreamingContext context) : base( info, context ) 
		{
		}

		#endregion
	}

}
