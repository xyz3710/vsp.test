//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// Manifest.cs
//
// 
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System.Xml.Serialization;

namespace Microsoft.ApplicationBlocks.Updater.Xsd 
{
	/// <summary>
	/// The manifest schema defines the required structure of the manifest files.
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	[XmlRoot("manifest", Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest", IsNullable=true)]
	public class Manifest 
	{
		/// <summary/>
		private string description;
		/// <summary/>
		private ManifestApplication application;
		/// <summary/>
		private ManifestFiles files;
		/// <summary/>
		private ManifestDownloaderProviderData downloader;
		/// <summary/>
		private ActivationProcess activationProcess;
		/// <summary/>
		private string manifestId;
		/// <summary/>
		private YesNoBoolType mandatory;
		/// <summary/>
		private IncludedManifests includedManifests;

		/// <summary>
		/// A string that contains a simple description of the manifest. This can be used by a client application to inform the user of information about the update. 
		///</summary>
		[XmlElement("description")]
		public string Description
		{
			get { return description; }
			set { description = value; }
		}

		/// <summary>
		/// A complex type that describes the client application.
		/// </summary>
		[XmlElement("application")]
		public ManifestApplication Application
		{
			get { return application; }
			set { application = value; }
		}

		/// <summary>
		/// A complex type that contains the list of files to download.
		///</summary>
		[XmlElement("files")]
		public ManifestFiles Files
		{
			get { return files; }
			set { files = value; }
		}

		/// <summary>
		/// A complex type that specifies which downloader implementation to use.
		///</summary>
		[XmlElement("downloader")]
		public ManifestDownloaderProviderData Downloader
		{
			get { return downloader; }
			set { downloader = value; }
		}

		/// <summary>
		/// A complex type that specifies which activation processors to use. 
		///</summary>
		[XmlElement("activation")]
		public ActivationProcess ActivationProcess
		{
			get { return activationProcess; }
			set { activationProcess = value; }
		}

		/// <summary>
		/// A complex type that defines any other manifests that should be included.
		/// </summary>
		[XmlElement("includedManifests")]
		public IncludedManifests IncludedManifests
		{
			get { return includedManifests; }
			set { includedManifests = value; }
		}

		/// <summary>
		/// A string that specifies the unique identifier of the manifest. 
		///</summary>
		[XmlAttribute("manifestId")]
		public string ManifestId
		{
			get { return manifestId; }
			set { manifestId = value; }
		}
		
		/// <summary>
		/// Specifies whether this is a mandatory update.
		/// </summary>
		[XmlAttribute("mandatory")]
		public YesNoBoolType Mandatory
		{
			get { return mandatory; }
			set { mandatory = value; }
		}
	}
    
	/// <summary>
	/// Type that describes the client application.
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	public class ManifestApplication 
	{
		/// <summary/>
		private EntryPoint entryPoint;
		/// <summary/>
		private string location;
		/// <summary/>
		private string applicationId;
		/// <summary/>
		private System.Xml.XmlElement any;
		/// <summary/>
		private System.Xml.XmlAttribute[] anyAttr;
		
		/// <summary>
		/// A complex type that identifies the entry point for the application. For example, when using AppStart to start an application.
		/// </summary>
		[XmlElement("entryPoint")]
		public EntryPoint EntryPoint
		{
			get { return entryPoint; }
			set { entryPoint = value; }
		}
		
		/// <summary>
		/// A string that provides information about the path on the client computer where the updates should be copied.
		/// </summary>
		[XmlElement("location")]
		public string Location
		{
			get { return location; }
			set { location = value; }
		}

		/// <summary>
		/// A string that specifies the unique identifier of this application
		/// </summary>
		[XmlAttribute("applicationId")]
		public string ApplicationId
		{
			get { return applicationId; }
			set { applicationId = value; }
		}

		/// <summary>
		/// A reference to an extra element that might be added to the configuration
		/// </summary>
		[XmlAnyElement()]
		public System.Xml.XmlElement Any
		{
			get { return any; }
			set { any = value; }
		}
		
		/// <summary>
		/// An array containing the extra attributes that might be added to the configuration
		/// </summary>
		[XmlAnyAttribute()]
		public System.Xml.XmlAttribute[] AnyAttr
		{
			get { return anyAttr; }
			set { anyAttr = value; }
		}
	}

	/// <summary>
	/// A type that describes information about activation processors.
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	public class ActivationProcess
	{
		/// <summary/>
		private ActivationProcessorProviderData[] tasks;
		
		/// <summary>
		/// A complex type that contains a list of tasks to process.
		/// </summary>
		[XmlArray("tasks"), XmlArrayItem("task")]
		public ActivationProcessorProviderData[] Tasks
		{
			get { return tasks; }
			set { tasks = value; }
		}
	}

	/// <summary>
	/// A complex type that specifies which activation processors to use.
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	public class ActivationProcessorProviderData 
	{
		/// <summary/>
		private System.Xml.XmlElement any;
		/// <summary/>
		private string type;
		/// <summary/>
		private string name;
		/// <summary/>
		private System.Xml.XmlAttribute[] anyAttr;

		/// <summary>
		/// A reference to an extra element that might be added to the configuration
		/// </summary>
		[XmlAnyElement()]
		public System.Xml.XmlElement Any
		{
			get { return any; }
			set { any = value; }
		}
		
		/// <summary>
		/// A string that defines the fully qualified type of the processor.
		/// </summary>
		[XmlAttribute("type")]
		public string Type
		{
			get { return type; }
			set { type = value; }
		}

		/// <summary>
		/// A string that defines the name of the processor.
		/// </summary>
		[XmlAttribute("name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}
        
		/// <summary>
		/// An array containing the extra attributes that might be added to the configuration
		/// </summary>
		[XmlAnyAttribute()]
		public System.Xml.XmlAttribute[] AnyAttr
		{
			get { return anyAttr; }
			set { anyAttr = value; }
		}
	}
	
	/// <summary>
	/// A complex type that specifies which downloader implementation to use.
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	public class ManifestDownloaderProviderData 
	{
		/// <summary/>
		private System.Xml.XmlElement any;
		/// <summary/>
		private string name;
		/// <summary/>
		private System.Xml.XmlAttribute[] anyAttr;

		/// <summary>
		/// A reference to an extra element that might be added to the configuration
		/// </summary>
		[XmlAnyElement()]
		public System.Xml.XmlElement Any
		{
			get { return any; }
			set { any = value; }
		}
		
		/// <summary>
		/// A string that defines the name of the downloader.
		/// </summary>
		[XmlAttribute("name")]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}
        
		/// <summary>
		/// An array containing the extra attributes that might be added to the configuration
		/// </summary>
		[XmlAnyAttribute()]
		public System.Xml.XmlAttribute[] AnyAttr
		{
			get { return anyAttr; }
			set { anyAttr = value; }
		}
	}
	
	/// <summary>
	/// A complex type that identifies the entry point for the application. For example, when using AppStart to start an application.
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	public class EntryPoint 
	{
		/// <summary/>
		private string file;
		/// <summary/>
		private string parameters;

		/// <summary>
		/// A string that contains the file to use as the entry point for the application.
		/// </summary>
		[XmlAttribute("file")]
		public string File
		{
			get { return file; }
			set { file = value; }
		}
		
		/// <summary>
		/// A string that contains any parameters the entry point requires.
		/// </summary>
		[XmlAttribute("parameters")]
		public string Parameters
		{
			get { return parameters; }
			set { parameters = value; }
		}
	}
   
	/// <summary>
	/// A complex type that contains the list of files to download.
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	public class ManifestFiles
	{
		/// <summary/>
		private string basePath;
		/// <summary/>
		private YesNoBoolType hashComparison = YesNoBoolType.No;
		/// <summary/>
		private ManifestFile[] files;
		/// <summary/>
		private string hashProviderName;

		/// <summary>
		/// A string that specifies the location on the server from where the files should be downloaded.
		/// </summary>
		[XmlAttribute("base")]
		public string Base
		{
			get { return basePath; }
			set { basePath = value; }
		}

		/// <summary>
		/// A Boolean that defines whether a hash comparison should be made.
		/// </summary>
		[XmlAttribute("hashComparison")]
		public YesNoBoolType HashComparison
		{
			get { return hashComparison; }
			set { hashComparison = value; }
		}

		/// <summary>
		/// A string that defines the type of hashing mechanism to be used.
		/// </summary>
		[XmlAttribute("hashProvider")]
		public string HashProviderName
		{
			get { return hashProviderName; }
			set { hashProviderName = value; }
		}

		/// <summary>
		/// A complex type that describes the files to download.
		/// </summary>
		[XmlElement("file")]
		public ManifestFile[] Files
		{
			get { return files; }
			set { files = value; }
		}
	}

	/// <summary>
	/// Class that represents a list of included manifests
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	public class IncludedManifests
	{
		/// <summary/>
		private IncludedManifest[] manifest;

		/// <summary>
		/// An array of types that defines any other manifests that should be included.
		/// </summary>
		[XmlElement("manifest")]
		public IncludedManifest[] Manifest
		{
			get { return manifest; }
			set { manifest = value; }
		}
	}

	/// <summary>
	/// A type that defines any other manifests that should be included.
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	public class IncludedManifest 
	{
		/// <summary/>
		private string location;
		/// <summary/>
		private string manifestId;

		/// <summary>
		/// A string that specifies the location of the manifest.
		/// </summary>
		[XmlAttribute("location")]
		public string Location
		{
			get { return location; }
			set { location = value; }
		}

		/// <summary>
		/// A string that specifies a unique identifier for the manifest.
		/// </summary>
		[XmlAttribute("manifestId")]
		public string ManifestId
		{
			get { return manifestId; }
			set { manifestId = value; }
		}
	}

	/// <summary>
	/// A type that contains the list of files to download.
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	public class ManifestFile 
	{
		/// <summary/>
		private System.Xml.XmlElement any;
		/// <summary/>
		private string source;
		/// <summary/>
		private string hash;
		/// <summary/>
		private YesNoBoolType transient = YesNoBoolType.False;
		/// <summary/>
		private System.Xml.XmlAttribute[] anyAttr;

		/// <summary>
		/// A reference to an extra element that might be added to the configuration
		/// </summary>
		[XmlAnyElement()]
		public System.Xml.XmlElement Any
		{
			get { return any; }
			set { any = value; }
		}
        
		/// <summary>
		/// A string that specifies the file name relative to the location specified in the base attribute.
		/// </summary>
		[XmlAttribute("source")]
		public string Source
		{
			get { return source; }
			set { source = value; }
		}

		/// <summary>
		/// A string that defines the computed hash value of the file.
		/// </summary>
		[XmlAttribute("hash")]
		public string Hash
		{
			get { return hash; }
			set { hash = value; }
		}

		/// <summary>
		/// A Boolean that identifies if a file is required only for the installation process and can be deleted when the installation is complete.
		/// </summary>
		[XmlAttribute("transient")]
		public YesNoBoolType Transient
		{
			get { return transient; }
			set { transient = value; }
		}
		
		/// <summary>
		/// An array containing the extra attributes that might be added to the configuration
		/// </summary>
		[XmlAnyAttribute()]
		public System.Xml.XmlAttribute[] AnyAttr
		{
			get { return anyAttr; }
			set { anyAttr = value; }
		}
	}

	/// <summary>
	/// Enumerator for Yes-No choices in manifest. 
	/// </summary>
	[XmlType(Namespace="urn:schemas-microsoft-com:PAG:updater-application-block:v2:manifest")]
	public enum YesNoBoolType 
	{
		/// <summary>A <c>yes</c> choice.</summary>
		Yes,
		/// <summary>A <c>no</c> choice.</summary>
		No,
		/// <summary>A <c>true</c> choice.</summary>
		True,
		/// <summary>A <c>false</c> choice.</summary>
		False,
	}

}
