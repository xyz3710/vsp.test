//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// RegistryManager.cs
//
// Contains the implementation of the registry manager.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Permissions;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.ApplicationBlocks.Updater.Utilities;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// Manages the list of tasks under progress, from its initial registration when they
	/// are submitted to download, to their final state when they are successfully activated
	/// or cancelled.
	/// </summary>
	[Serializable]
	internal sealed class RegistryManager
	{
		#region Private fields

		/// <summary>
		/// The singleton instance stored.
		/// </summary>
		private static readonly RegistryManager instance = new RegistryManager();

		/// <summary>
		/// Root folder name for the registry.
		/// </summary>
		private const string root = "registry";

		/// <summary>
		/// Helper class for directory information.
		/// </summary>
		private DirectoryInfo rootDir;

		/// <summary>
		/// The in memory registry storage.
		/// </summary>
		private Hashtable registry = new Hashtable( 10 );

		/// <summary>
		/// Indicates if the list of tasks is loaded
		/// </summary>
		private bool loaded = false;

		#endregion

		#region Singleton implementation

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static RegistryManager Current
		{
			get { return instance; }
		}

		/// <summary>
		/// Default constructor disable because there is a singleton implementation.
		/// </summary>
		private RegistryManager()
		{
			string path = Path.Combine( new UpdaterConfigurationView().BasePath, root );
			if ( !Directory.Exists( path ) )
			{
				rootDir = Directory.CreateDirectory( path );
			}
			else
			{
				rootDir = new DirectoryInfo( path );
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Loads all the pending tasks.
		/// </summary>
		public void Load()
		{
			foreach( FileInfo fi in rootDir.GetFiles() )
			{
				this.LoadTask( fi.FullName );
			}
		}

		/// <summary>
		/// Updates the information of an existing stored task.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		public void UpdateTask(UpdaterTask task)
		{
			SaveTask( task );
		}

		/// <summary>
		/// Registers the task in the storage.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		public void RegisterTask(UpdaterTask task)
		{
			lock( Tasks.SyncRoot )
			{
				if ( !Tasks.ContainsKey( task.TaskId ) )
				{
					Tasks.Add( task.TaskId, task );
				}
			}
			SaveTask( task );
		}

		/// <summary>
		/// Removes the task from the storage.
		/// </summary>
		/// <param name="taskId">The task id.</param>
		public void UnRegisterTask(Guid taskId)
		{
			lock( Tasks.SyncRoot )
			{
				Tasks.Remove( taskId );
			}
			string fileName = Path.Combine( rootDir.FullName, taskId.ToString() + ".task" );
			FileUtility.DestroyFile( fileName );
		}

		/// <summary>
		/// Return all the tasks stored in memory.
		/// </summary>
		/// <returns>An array of UpdaterTask instances.</returns>
		public UpdaterTask[] GetTasks()
		{
			UpdaterTask[] result = new UpdaterTask[ Tasks.Count ];
			Tasks.Values.CopyTo( result, 0 );
			return result;
		}

		/// <summary>
		/// Returns all the stored tasks by a given application id.
		/// </summary>
		/// <param name="applicationId">The application id.</param>
		/// <returns>An array of UpdaterTask instances.</returns>
		public UpdaterTask[] GetByApplicationId(string applicationId)
		{
			ArrayList tasks = new ArrayList();
			lock( Tasks.SyncRoot )
			{
				foreach( UpdaterTask task in Tasks.Values )
				{
					if ( task.Manifest.Application.ApplicationId == applicationId )
					{
						tasks.Add( task );
					}
				}
			}
			return (UpdaterTask[])tasks.ToArray( typeof(UpdaterTask) );
		}

		/// <summary>
		/// Return the UpdaterTask for a given manifest id.
		/// </summary>
		/// <param name="manifestId">The manifest id.</param>
		/// <returns>An UpdaterTask instance.</returns>
		public UpdaterTask GetByManifestID(Guid manifestId)
		{
			lock( Tasks.SyncRoot )
			{
				foreach( UpdaterTask task in Tasks.Values )
				{
					if ( task.Manifest.ManifestId == manifestId )
					{
						return task;
					}
				}
			}
			return null;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Load the tasks stored in a specified path.
		/// </summary>
		/// <param name="taskFilePath">The base path for the registry storage.</param>
		/// <returns>An UpdaterTask instance.</returns>
		[SecurityPermission( SecurityAction.Demand, SerializationFormatter=true )]
		private UpdaterTask LoadTask( string taskFilePath )
		{
			UpdaterTask task = null;
			BinaryFormatter formatter = new BinaryFormatter();
			using( FileStream stream = new FileStream( taskFilePath, FileMode.Open, FileAccess.Read, FileShare.Read) )
			{
				task = (UpdaterTask)formatter.Deserialize( stream );
				lock( registry.SyncRoot )
				{
					if( task.State != UpdaterTaskState.Activated && !registry.ContainsKey( task.TaskId ) )
					{
						registry.Add( task.TaskId , task );
					}
				}
			}
			return task;
		}

		/// <summary>
		/// Stores a task in the registry storage.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter=true)]
		private void SaveTask( UpdaterTask task )
		{
			string filename = Path.Combine( rootDir.FullName, String.Format( CultureInfo.InvariantCulture, "{0}.task", task.TaskId.ToString() ) );
			try
			{
				using(Stream stream = new FileStream( filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read ) )
				{
					BinaryFormatter formatter = new BinaryFormatter();
					formatter.Serialize( stream, task );
				}
			}
			catch( Exception ex)
			{
				File.Delete( filename );
				Logger.LogAndThrowException( ex );
			}
		}


		/// <summary>
		/// Gets the list of registered tasks, ensuring the list is loaded
		/// </summary>
		private Hashtable Tasks
		{
			get
			{
				if ( !loaded )
				{
					Load();
					loaded = true;
				}
				return registry;
			}
		}

		#endregion

	}
}
