//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// Resources.cs
//
// Helper class to provide access to .NET resource files simply in other Updater classes.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Globalization;
using System.IO;
using System.Reflection; 
using System.Resources;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// Helper class used to manage application resources.
	/// </summary>
	/// <exclude/>
	public sealed class Resource
	{
		#region MessageKey enum

		/// <summary>
		/// The message keys defined in an enumeration to avoid message synchronization problems.
		/// </summary>
		public enum MessageKey
		{
			#region General Messages

			/// <summary/>
			ArgumentMissing,
			/// <summary/>
			ArgumentMissing1,
			/// <summary/>
			FileNotExists,

			#endregion

			#region Updater Messages
			/// <summary/>
			CannotFindActivationTaskForManifest,
			/// <summary/>
			ActivationTaskForManifestInInvalidState,
			/// <summary/>
			CallerIsNotUpdaterService,
			/// <summary/>
			MissingManifestUri,
			/// <summary/>
			UpdaterManagerOnlyToBeUsedByWindows,
			/// <summary/>
			UpdaterManagerUpdateCannotBeCanceled,
			/// <summary/>
			ApplicationIdCannotBeNullOrEmpty,
			/// <summary/>
			LocationCannotBeNullOrEmpty,
			/// <summary/>
			NoConfigurationToGetApplicationId,
			/// <summary/>
			ManifestManagerMustBeValidId,
			/// <summary/>
			CultureIdToGetComErrorStringsFormatted,

			#endregion

			#region Manifest Manager
			/// <summary/>
			FileHashIsNull,
			/// <summary/>
			ManifestSchemaError,
			/// <summary/>
			DuplicatedManifestId,

			// Processor Messages
			/// <summary/>
			SourceAndDestinationExpected,
			/// <summary/>
			PathExpected,
			/// <summary/>
			ProcessorNotConfigured,
			/// <summary/>
			AssemblyReferenceExpected,
			/// <summary/>
			CannotFindDotNetFolder,
			/// <summary/>
			CannotFindTool,
			/// <summary/>
			ToolGeneratedError,
			/// <summary/>
			SourceExpected,
			/// <summary/>
			ValidateHashExceptionMessage,
			/// <summary/>
			ActivationPausedMessage,
			/// <summary/>
			FileCopyProcessorSourceFileNotExists,
			/// <summary/>
			CantCreateAlgorithmProvider,
			#endregion

			#region Downloader Messages
			/// <summary/>
			UncPathUnsupported,
			/// <summary/>
			BitsDownloaderFailedDownload,
			/// <summary/>
			BitsDownloadedNotConfigured,
			/// <summary/>
			DownloadTimeoutExceeded,
			/// <summary/>
			BitsJobName,
			/// <summary/>
			BitsDescription,
			/// <summary/>
			BitsFilesDownloadJobName,
			/// <summary/>
			BitsFilesDownloadDescription,
			/// <summary/>
			CannotCreateDefaultDownloader,
			/// <summary/>
			CannotCreateDownloader,
			/// <summary/>
			BITSCannotConnectToJob,
			#endregion

			#region Activation Messager
			/// <summary/>
			ProcessorOnErrorException,
			#endregion

			#region Manifest Provider
			/// <summary/>
			DefaultManifestUriMissing,
			#endregion

			#region Manifest tool
			/// <summary/>
			ManifestAlreadyIncluded,
			/// <summary/>
			ProcessorAlreadyIncluded,
			/// <summary/>
			IncompleteCommandWasIssued,
			/// <summary/>
			MustSpecifyValidValueForArgument,
			/// <summary/>
			WelcomeToTheManifestTool,
			/// <summary/>
			TypeHelpToGetUsageInformation,
			/// <summary/>
			ReadyString,
			/// <summary/>
			CommandUnsupported,
			/// <summary/>
			UnsavedChangesProceedQuestion,
			/// <summary/>
			TheManifestIsValid,
			/// <summary/>
			TheManifestIsInvalid,
			/// <summary/>
			AddFileLabel,
			/// <summary/>
			OpenCryptographyConfigurationLabel,
			/// <summary/>
			OpenCryptographyConfigurationFilter,
			/// <summary/>
			ManifestFilesFilter,
			/// <summary/>
			YesString,
			/// <summary/>
			NoString,
			/// <summary/>
			ConfirmOperationTitle,
			/// <summary/>
			OpenManifestTitle,
			/// <summary/>
			ManifestValidationTitle,
			/// <summary/>
			PathDoesNotExists,
			/// <summary/>
			ApplicationDeployProcessor, 
			/// <summary/>
			FileCopyProcessor, 
			/// <summary/>
			FileDeleteProcessor, 
			/// <summary/>
			FolderCopyProcessor, 
			/// <summary/>
			FolderDeleteProcessor, 
			/// <summary/>
			GacUtilProcessor, 
			/// <summary/>
			InstallUtilProcessor, 
			/// <summary/>
			MSIProcessor, 
			/// <summary/>
			UncompressProcessor, 
			/// <summary/>
			HashValidationProcessor, 
			/// <summary/>
			WaitForExitProcessor,
			/// <summary/>
			UpdaterApplicationBlockConfiguration,
			/// <summary/>
			UpdaterApplicationBlock,
			/// <summary/>
			GetSetApplicationBasePath,
			/// <summary/>
			GetSetApplicationIdentifier,
			/// <summary/>
			GetSetManifestLocation,
			/// <summary/>
			ManifestProviderMenuItem,
			/// <summary/>
			ServiceConfigurationMenuItem,
			/// <summary/>
			DownloadersMenuItem,
			/// <summary/>
			DownloaderMenuItem,
			/// <summary/>
			CreateNewText,
			/// <summary/>
			ServiceConfigurationNode,
			/// <summary/>
			GetSetUpdaterServiceInterval,
			/// <summary/>
			BitsDownloaderNode,
			/// <summary/>
			GetSetUserName,
			/// <summary/>
			GetSetPassword,
			/// <summary/>
			GetSetAuthenticationScheme,
			/// <summary/>
			GetSetServerType,
			/// <summary/>
			ManifestManagerNode,
			/// <summary/>
			GetSetDownloaderTypeName,
			/// <summary/>
			GetSetAuthenticationType,
			/// <summary/>
			GetSetManifestSettings,
			/// <summary />
			EditorControlNotConfigured,
			/// <summary />
			InvalidProcessorItemForEdition,
			/// <summary />
			MissingManifestEditorConfiguration,
			/// <summary />
			MissingManifestEditorConfigurationTitle,
			/// <summary />
			ManifestEditorFilesNotAdded,
			/// <summary />
			ManifestEditorCannotChangeFolder,
			/// <summary />
			EditorValidationErrorCaption,
			/// <summary />
			EditorMissingApplicationId,
			/// <summary />
			EditorMissingManifestDescription,
			/// <summary />
			EditorMissingManifestId,
			/// <summary />
			EditorMissingApplicationLocation,
			/// <summary />
			BrowseForWorkingFolderDialogText,
			/// <summary />
			BrowseForSourceFolderDescription,
			/// <summary />
			InvalidSourceFolderMessage,
			/// <summary />
			InvalidSourceFolderCaption,
			/// <summary />
			ToolCannotLoadManifestSchema,
			#endregion
		}
		
		#endregion

		#region Static part

		/// <summary>
		/// Implement Singleton --.NET static initialization is guaranteed to be thread-safe
		/// </summary>
		private static readonly Resource resource = new Resource();

		/// <summary>
		/// Returns the singleton instance of the resource manager
		/// </summary>
		public static Resource ResourceManager
		{
			get
			{
				return resource;
			}
		}
        
		#endregion
		
		#region Instance part
        
		/// <summary>
		/// This is the ACTUAL resource manager, for which this class is just a convenience wrapper
		/// </summary>
		private ResourceManager resourceManager = null;

		/// <summary>
		/// Make constructor private so noone can directly create an instance of Resource, only use the Static Property ResourceManager
		/// </summary>
		private Resource()
		{
			resourceManager = new ResourceManager( this.GetType().Namespace + ".Resources.ApplicationUpdaterText", Assembly.GetExecutingAssembly() );
		}


		/// <summary>
		/// A convenience indexer that accesses the internal resource manager
		/// </summary>
		public string this [ string key ]
		{
			get
			{
				// try to retreive the message, if not, at least return the key
				string msg = resourceManager.GetString( key, CultureInfo.CurrentCulture );
				return ( msg != null ) ? msg : key;
			}
		}

		/// <summary>
		/// Returns the localized string for a string key and a set of parameters.
		/// </summary>
		public string this [ string key, params object[] par ]
		{
			get
			{
				return string.Format( CultureInfo.CurrentUICulture, resourceManager.GetString( key, CultureInfo.CurrentCulture ), par );
			}
		}

		/// <summary>
		/// Returns the localized string for a message key.
		/// </summary>
		public string this[ MessageKey key ]
		{
			get
			{
				return this[ key.ToString( CultureInfo.InvariantCulture ) ];
			}
		}

		/// <summary>
		/// Returns the localized message for a string key and a set of parameters.
		/// </summary>
		public string this[ MessageKey key, params object[] par ]
		{
			get
			{
				return String.Format( CultureInfo.CurrentUICulture, this[key], par );
			}
		}

		/// <summary>
		/// Returns the resource stream using the name of the embedded resource.
		/// </summary>
		public Stream GetStream( string name )
		{
			return Assembly.GetExecutingAssembly().GetManifestResourceStream( name );
		}

		#endregion
	}
}
