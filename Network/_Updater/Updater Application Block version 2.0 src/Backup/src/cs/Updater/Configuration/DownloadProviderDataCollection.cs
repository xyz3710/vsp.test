//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// DownloadProviderDataCollection.cs
//
// Contains the implementation of the collection of downloader data.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System.Collections;
using Microsoft.Practices.EnterpriseLibrary.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Configuration
{
	/// <summary>
	/// Defines a collection of downloader configuration in the loca configuration file.
	/// </summary>
	public class DownloadProviderDataCollection:  ProviderDataCollection 
	{
		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public DownloadProviderDataCollection()
		{
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Returns the downloader data by its position in the collection.
		/// </summary>
		public DownloadProviderData this[int index]
		{
			get { return (DownloadProviderData)GetProvider(index); }
			set { SetProvider( index, value ); }
		}

		/// <summary>
		/// Returns the downloader data by its name in the collection.
		/// </summary>
		public DownloadProviderData this[string name]
		{
			get { return (DownloadProviderData)GetProvider(name); }
			set { SetProvider( name, value ); }
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Adds downloader data to the collection.
		/// </summary>
		/// <param name="providerData">The downloader configuration data.</param>
		public void Add(DownloadProviderData providerData)
		{
			AddProvider( providerData.Name, providerData);
		}

		#endregion
	}
}
