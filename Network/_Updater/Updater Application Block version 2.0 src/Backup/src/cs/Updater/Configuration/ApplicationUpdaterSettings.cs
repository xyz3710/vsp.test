//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ApplicationUpdaterSettings.cs
//
// Contains the implementation of the application updater settings in the local configuration file.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System.Xml.Serialization;

namespace Microsoft.ApplicationBlocks.Updater.Configuration
{
	/// <summary>
	/// Defines the block configuration in the local configuration file.
	/// </summary>
	[XmlRoot("applicationUpdater", Namespace=ApplicationUpdaterSettings.ConfigurationNamespace)]
	public class ApplicationUpdaterSettings
	{
		#region Private members

		/// <summary>
		/// The base path for the temp folders.
		/// </summary>
		private string basePath;

		/// <summary>
		/// The application id.
		/// </summary>
		private string applicationId;

		/// <summary>
		/// The manifest Uri.
		/// </summary>
		private string manifestUri;

		/// <summary>
		/// The service configuration data.
		/// </summary>
		private ServiceSettings service;

		/// <summary>
		/// The collection of downloader providers.
		/// </summary>
		private DownloadProviderDataCollection downloaders;

		/// <summary>
		/// The settings for the manifest manager.
		/// </summary>
		private ManifestManagerSettings manifestManager;

		#endregion

		#region Public constants 

		/// <summary>
		/// The namespace for the XML configuration file.
		/// </summary>
		public const string ConfigurationNamespace = "urn:schemas-microsoft-com:PAG:updater-application-block:v2";

		/// <summary>
		/// Configuration key for application updater manager settings.
		/// </summary>
		public const string SectionName = "UpdaterConfiguration";

		#endregion

		#region Public properties 

		/// <summary>
		/// The base path for the temporary folders.
		/// </summary>
		[XmlAttribute("basePath")]
		public string BasePath
		{
			get { return basePath; }
			set { basePath = value; }
		}

		/// <summary>
		/// The application ID.
		/// </summary>
		[XmlAttribute("applicationId")]
		public string ApplicationId
		{
			get { return applicationId; }
			set { applicationId = value; }
		}

		/// <summary>
		/// The manifest URI.
		/// </summary>
		[XmlAttribute("manifestUri")]
		public string ManifestUri
		{
			get { return manifestUri; }
			set { manifestUri = value; }
		}
  
		/// <summary>
		/// The service configuration data.
		/// </summary>
		[XmlElement("service")]
		public ServiceSettings Service
		{
			get { return service; }
			set { service = value; }
		}

		/// <summary>
		/// The collection of downloader providers.
		/// </summary>
		[XmlArray("downloaders")]
		[XmlArrayItem("downloader", typeof(DownloadProviderData))] 
		public DownloadProviderDataCollection Downloaders
		{
			get { return downloaders; }
			set { downloaders = value; }
		} 

		/// <summary>
		/// The settings for the manifest manager.
		/// </summary>
		[XmlElement("manifestManager")]
		public ManifestManagerSettings ManifestManager
		{
			get { return manifestManager; }
			set { manifestManager = value; }
		} 

		#endregion
	}
}
