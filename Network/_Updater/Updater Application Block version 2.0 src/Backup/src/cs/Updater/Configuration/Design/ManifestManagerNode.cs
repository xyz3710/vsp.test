//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ManifestManagerNode.cs
//
// Contains the implementation of the manifest manager node.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.ComponentModel;
using System.Drawing.Design;
using Microsoft.ApplicationBlocks.Updater;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design.Validation;

namespace Microsoft.ApplicationBlocks.Updater.Configuration.Design
{
	/// <summary>
	/// Defines the confiuration node for the manifest manager.
	/// </summary>
	[Image(typeof(ManifestManagerNode))]
	public class ManifestManagerNode : ConfigurationNode
	{
		#region Private members

		/// <summary>
		/// The manifest manager configuration.
		/// </summary>
		private ManifestManagerSettings managerSettings;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ManifestManagerNode() : this( new ManifestManagerSettings() )
		{
		}

		/// <summary>
		/// Creates a ManifestManagerNode with the specified manifest manager settings.
		/// </summary>
		/// <param name="managerSettings">The manifest manager setting.</param>
		public ManifestManagerNode( ManifestManagerSettings managerSettings ): base()
		{
			this.managerSettings = managerSettings;
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The name of the node.
		/// </summary>
		[ReadOnly(true)]
		public override string Name
		{
			get
			{
				return base.Name;
			}
			set
			{
				base.Name = value;
			}
		}

		/// <summary>
		/// The manifest manager settings.
		/// </summary>
		[Browsable(false)]
		[ResourceDescription( Resource.MessageKey.GetSetManifestSettings )]
		public ManifestManagerSettings ManifestManagerSettings
		{
			get { return managerSettings; }
		}

		/// <summary>
		/// The authentication type.
		/// </summary>
		[Required]
		[ResourceDescription( Resource.MessageKey.GetSetAuthenticationType )]
		public AuthenticationType AuthenticationType
		{
			get { return managerSettings.AuthenticationType; }
			set { managerSettings.AuthenticationType = value; }
		}

		/// <summary>
		/// The user name credential.
		/// </summary>
		[ResourceDescription( Resource.MessageKey.GetSetUserName )]
		public string UserName
		{
			get { return managerSettings.UserName; }
			set { managerSettings.UserName = value; }
		}

		/// <summary>
		/// The password credential.
		/// </summary>
		[ResourceDescription( Resource.MessageKey.GetSetPassword )]
		[Editor( typeof( PasswordEditor ), typeof( UITypeEditor ) )]
		public Password Password
		{
			get { return new Password( managerSettings.Password ); }
			set { managerSettings.Password = value.PasswordText; }
		}

		#endregion

		#region Protected methods

		/// <summary>
		/// Prepare the context of execution.
		/// </summary>
		protected override void OnSited()
		{
			base.OnSited();
			Site.Name = Resource.ResourceManager[ Resource.MessageKey.ManifestManagerNode ]; 
		}

		protected override void AddDefaultChildNodes()
		{
			base.AddDefaultChildNodes ();

			ApplicationUpdaterNode appNode = Parent as ApplicationUpdaterNode;
			appNode.ManagerNode = this;
		}


		#endregion
	}
}
