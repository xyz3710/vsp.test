//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ServiceSettings.cs
//
// Contains the implementation of the service configuration.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System.Xml.Serialization;

namespace Microsoft.ApplicationBlocks.Updater.Configuration
{
	/// <summary>
	/// Defines the configuration required by the service to process updates.
	/// </summary>
	[XmlRoot("service", Namespace=ApplicationUpdaterSettings.ConfigurationNamespace)]
	public class ServiceSettings
	{
		#region Private members

		/// <summary>
		/// The polling interval of the service.
		/// </summary>
		private string interval;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ServiceSettings()
		{
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The polling interval.
		/// </summary>
		[XmlAttribute("updateInterval")]
		public string Interval
		{
			get { return interval; }
			set { interval = value; }
		}

		#endregion
	}
}
