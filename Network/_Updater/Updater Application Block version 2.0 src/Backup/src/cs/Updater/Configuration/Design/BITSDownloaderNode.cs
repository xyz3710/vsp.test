//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// BitsDownloaderNode.cs
//
// Contains the implementation of the Bits downloader configuration node.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.ComponentModel;
using System.Drawing.Design;
using Microsoft.ApplicationBlocks.Updater;
using Microsoft.ApplicationBlocks.Updater.Downloaders;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design;

namespace Microsoft.ApplicationBlocks.Updater.Configuration.Design
{
	/// <summary>
	/// Defines the configuration options for Bits downloader.
	/// </summary>
	[ServiceDependency(typeof(IXmlIncludeTypeService))]
	[ServiceDependency(typeof(INodeCreationService))]
	public class BitsDownloaderNode : DownloaderNode
	{
		#region Private members

		/// <summary>
		/// The BITS downloader data instance.
		/// </summary>
		private BitsDownloaderProviderData data;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public BitsDownloaderNode() : this( new BitsDownloaderProviderData() )
		{
		}

		/// <summary>
		/// Creates a BitsDownloaderNode using the provider data.
		/// </summary>
		/// <param name="data">The Bits downloader data in th local configuration file.</param>
		public BitsDownloaderNode( BitsDownloaderProviderData data ): base( data )
		{
			this.data = data;
		}
		
		#endregion

		#region Public properties

		/// <summary>
		/// The provider name.
		/// </summary>
		[ReadOnly(true)]
		public override string Name
		{
			get	{ return base.Name;	}
			set	{ base.Name = value; }
		}

		/// <summary>
		/// The provider data.
		/// </summary>
		[Browsable(false)]
		public override DownloadProviderData DownloadProviderData
		{
			get 
			{ 
				data.Name = base.Name;
				return data; 
			}
		}

		/// <summary>
		/// The user name credential.
		/// </summary>
		[ResourceDescription( Resource.MessageKey.GetSetUserName )]
		public string UserName
		{
			get{ return data.UserName; }
			set{ data.UserName = value; }
		}

		/// <summary>
		/// The password credential.
		/// </summary>
		[ResourceDescription( Resource.MessageKey.GetSetPassword )]
		[Editor( typeof( PasswordEditor ), typeof( UITypeEditor ) )]
		public Password Password
		{
			get { return new Password( data.Password ); }
			set { data.Password = value.PasswordText; }
		}

		/// <summary>
		/// The authentication scheme.
		/// </summary>
		[ResourceDescription( Resource.MessageKey.GetSetAuthenticationScheme )]
		public BG_AUTH_SCHEME AuthenticationScheme
		{
			get { return data.AuthenticationScheme; }
			set { data.AuthenticationScheme = value; }
		}

		/// <summary>
		/// The type of the target server.
		/// </summary>
		[ResourceDescription( Resource.MessageKey.GetSetServerType )]
		public BG_AUTH_TARGET TargetServerType
		{
			get { return data.TargetServerType; }
			set { data.TargetServerType = value; }
		}

		#endregion
	
		#region Public methods

		/// <summary>
		/// Overrides the base method.
		/// </summary>
		protected override void OnSited()
		{
			base.OnSited();
			Site.Name = Resource.ResourceManager[ Resource.MessageKey.BitsDownloaderNode ]; 
		}

		#endregion
	}
}
