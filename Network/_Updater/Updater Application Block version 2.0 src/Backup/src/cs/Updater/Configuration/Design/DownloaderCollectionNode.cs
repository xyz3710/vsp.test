//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// DownloaderCollectionNode.cs
//
// Contains the implementation of the downloader collection node.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.ComponentModel;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design;
using Microsoft.ApplicationBlocks.Updater;
using Microsoft.ApplicationBlocks.Updater.Downloaders;

namespace Microsoft.ApplicationBlocks.Updater.Configuration.Design
{
	/// <summary>
	/// Defines the collection of downloader nodes.
	/// </summary>
	[Image(typeof(DownloaderNodeCollection))]
	public class DownloaderNodeCollection : ConfigurationNode
	{
		#region Private members

		/// <summary>
		/// Mantains the download provider collection.
		/// </summary>
		private DownloadProviderDataCollection collection;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public DownloaderNodeCollection(): this(new DownloadProviderDataCollection())
		{
		}

		/// <summary>
		/// Creates a DownloaderNodeCollection with the deserialized collection.
		/// </summary>
		/// <param name="collection"></param>
		public DownloaderNodeCollection(DownloadProviderDataCollection collection) : base()
		{
			this.collection = collection;
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The name of the collection.
		/// </summary>
		[ReadOnly(true)]
		public override string Name
		{
			get { return base.Name; }
			set { base.Name = value; }
		}

		/// <summary>
		/// Gets the provider data collection configuration.
		/// </summary>
		[Browsable(false)]
		public virtual DownloadProviderDataCollection DownloaderDataCollection
		{
			get	
			{ 
				collection  = new DownloadProviderDataCollection();
				foreach (DownloaderNode node in Nodes)
				{
					collection.Add( node.DownloadProviderData );
				}

				return collection;
			}
		}

		#endregion

		#region Protected members

		/// <summary>
		/// Set the name of the current node selected.
		/// </summary>
		protected override void OnSited()
		{
			base.OnSited();
			Site.Name = Resource.ResourceManager[ Resource.MessageKey.DownloadersMenuItem ];

			foreach( DownloadProviderData downloader in collection )
			{
				if( downloader is BitsDownloaderProviderData )
					this.Nodes.Add( new BitsDownloaderNode( (BitsDownloaderProviderData)downloader ) ); 
				else
					this.Nodes.Add( new DownloaderNode( downloader ) ); 
			}
		}

		/// <summary>
		/// Add a menu item when the menu is required.
		/// </summary>
		protected override void OnAddMenuItems()
		{
			base.OnAddMenuItems();
			AddMenuItem( typeof(BitsDownloaderNode), Resource.ResourceManager[ Resource.MessageKey.BitsDownloaderNode ] );
		}

		#endregion

		#region Private members

		/// <summary>
		/// Adds a menu item using the base class methods.
		/// </summary>
		/// <param name="type">The type for the configuration information.</param>
		/// <param name="text">The text for the menu.</param>
		private void AddMenuItem(Type type, string text)
		{
			ConfigurationMenuItem item = new ConfigurationMenuItem(text,
				new AddChildNodeCommand(Site, type),
				this,
				Shortcut.None,
				Resource.ResourceManager[ Resource.MessageKey.CreateNewText ] + text,
				InsertionPoint.New);
			bool contains = Hierarchy.ContainsNodeType(this, type);
			item.Enabled = !contains;
			AddMenuItem(item);
		}

		#endregion
	}
}
