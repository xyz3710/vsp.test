//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ApplicationUpdaterNode.cs
//
// Contains the implementation of the application updater node.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.Windows.Forms;
using Microsoft.ApplicationBlocks.Updater;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design.Validation;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration.Design;

namespace Microsoft.ApplicationBlocks.Updater.Configuration.Design
{
	/// <summary>
	/// </summary>
	[Image(typeof(ApplicationUpdaterNode))]
	public class ApplicationUpdaterNode : ConfigurationNode
	{
		#region Private members

		/// <summary>
		/// The application updater settings.
		/// </summary>
		private ApplicationUpdaterSettings settings;

		private ConfigurationNodeChangedEventHandler nodeRemoveHandler;
		private ConfigurationNodeChangedEventHandler noreRenameHandler;

		private ManifestManagerNode managerNode = null;
		private ServiceConfigNode serviceNode = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ApplicationUpdaterNode(): this ( new ApplicationUpdaterSettings() )
		{
		}

		/// <summary>
		/// Creates a ApplicationUpdaterNode with the provided application updater settings.
		/// </summary>
		/// <param name="settings">The application updater settings.</param>
		public ApplicationUpdaterNode( ApplicationUpdaterSettings settings ): base()
		{
			this.nodeRemoveHandler = new ConfigurationNodeChangedEventHandler( OnNodeRemoved );
			this.noreRenameHandler = new ConfigurationNodeChangedEventHandler( OnNodeRenamed );
			this.settings = settings;
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The name of the node.
		/// </summary>
		[ReadOnly(true)]
		public override string Name
		{
			get	{ return base.Name;	}
			set	{ base.Name = value; }
		}

		/// <summary>
		/// The base path for the application.
		/// </summary>
		[ResourceDescription( Resource.MessageKey.GetSetApplicationBasePath )]
		public string BasePath
		{
			get { return settings.BasePath; }
			set { settings.BasePath = value; }
		}

		/// <summary>
		/// The application Id.
		/// </summary>
		[ResourceDescription( Resource.MessageKey.GetSetApplicationIdentifier )]
		[Required]
		public string ApplicationId
		{
			get { return settings.ApplicationId; }
			set { settings.ApplicationId = value; }
		}

		/// <summary>
		/// The manifest Url.
		/// </summary>
		[ResourceDescription( Resource.MessageKey.GetSetManifestLocation )]
		[Required]
		public string ManifestUri
		{
			get { return settings.ManifestUri; }
			set { settings.ManifestUri = value; }
		}

		/// <summary>
		/// The applicatoin updater settings.
		/// </summary>
		[Browsable(false)]
		public ApplicationUpdaterSettings Settings
		{
			get
			{
				GetManifestManager();
				GetDownloaders();
				GetServiceConfiguration();

				return settings;
			}
		}

		/// <summary>
		/// The manifest manager node
		/// </summary>
		[Browsable(false)]
		public ManifestManagerNode ManagerNode
		{
			get
			{
				return managerNode;
			}
			set
			{
				ILinkNodeService svc = GetService( typeof(ILinkNodeService) ) as ILinkNodeService;
				Debug.Assert(svc != null, "Could not get the ILinkNodeService");
				managerNode = (ManifestManagerNode)svc.CreateReference( managerNode, value, nodeRemoveHandler, nodeRemoveHandler );
			}
		}

		/// <summary>
		/// The service node
		/// </summary>
		[Browsable(false)]
		public ServiceConfigNode ServiceNode
		{
			get
			{
				return serviceNode;
			}
			set
			{
				ILinkNodeService svc = GetService( typeof( ILinkNodeService ) ) as ILinkNodeService;
				Debug.Assert(svc != null, "Could not get the ILinkNodeService");
				serviceNode = (ServiceConfigNode)svc.CreateReference( serviceNode, value, nodeRemoveHandler, noreRenameHandler );
			}
		}


		#endregion

		#region Private methods

		/// <summary>
		/// Get the manifest manager configuration.
		/// </summary>
		private void GetManifestManager()
		{
			ManifestManagerNode node = Hierarchy.FindNodeByType( this, typeof( ManifestManagerNode ) ) as ManifestManagerNode;
			if ( node != null )
			{
				settings.ManifestManager = node.ManifestManagerSettings;
			}
		}

		/// <summary>
		/// Get the configuration for the service.
		/// </summary>
		private void GetServiceConfiguration()
		{
			ServiceConfigNode node = Hierarchy.FindNodeByType( this, typeof( ServiceConfigNode ) ) as ServiceConfigNode;
			if ( node != null )
			{
				settings.Service = node.ServiceSettings;
			}
		}

		/// <summary>
		/// Get the downloaders configuration.
		/// </summary>
		private void GetDownloaders()
		{
			DownloaderNodeCollection nodes = Hierarchy.FindNodeByType( this, typeof( DownloaderNodeCollection ) ) as DownloaderNodeCollection;
			if ( nodes != null )
			{
				settings.Downloaders = nodes.DownloaderDataCollection;
			}
		}

		/// <summary>
		/// When the user right clicks the node 3 nodes will be added.
		/// </summary>
		protected override void OnAddMenuItems()
		{
			base.OnAddMenuItems();
			AddMenuItem( typeof(ManifestManagerNode), Resource.ResourceManager[ Resource.MessageKey.ManifestProviderMenuItem ] );
			AddMenuItem( typeof(ServiceConfigNode), Resource.ResourceManager[ Resource.MessageKey.ServiceConfigurationMenuItem ] );
			AddMenuItem( typeof(DownloaderNodeCollection), Resource.ResourceManager[ Resource.MessageKey.DownloadersMenuItem ] );
		}

		/// <summary>
		/// Set the name of the node 
		/// </summary>
		protected override void OnSited()
		{
			base.OnSited ();
			Site.Name = Resource.ResourceManager[ Resource.MessageKey.UpdaterApplicationBlock ];

			if( settings.ManifestManager != null )
			{
				this.Nodes.Add( new ManifestManagerNode( settings.ManifestManager ) ); 
			}
			
			if( settings.Service != null )
			{
				this.Nodes.Add( new ServiceConfigNode( settings.Service ) ); 
			}
			
			if( settings.Downloaders != null && settings.Downloaders.Count > 0 )
			{
				this.Nodes.Add( new DownloaderNodeCollection( settings.Downloaders ) ); 
			}
		}

		/// <summary>
		/// Add the default child nodes.
		/// </summary>
		protected override void AddDefaultChildNodes()
		{
			base.AddDefaultChildNodes ();
			CreateCryptographySettingsNode();
		}

		/// <summary>
		/// Allows child nodes to resolve references to sibling nodes in configuration.
		/// </summary>
		public override void ResolveNodeReferences()
		{
			base.ResolveNodeReferences ();
			this.ManagerNode = Hierarchy.FindNodeByType( this, typeof( ManifestManagerNode ) ) as ManifestManagerNode;
			this.ServiceNode = Hierarchy.FindNodeByType( this, typeof( ServiceConfigNode ) ) as ServiceConfigNode;
		}

		#endregion

		#region Private members
	
		/// <summary>
		/// Add a menu entry.
		/// </summary>
		/// <param name="type">The type of the child node.</param>
		/// <param name="text">The menu item text.</param>
		private void AddMenuItem(Type type, string text)
		{
			ConfigurationMenuItem item = new ConfigurationMenuItem(text,
				new AddChildNodeCommand(Site, type),
				this,
				Shortcut.None,
				Resource.ResourceManager[ Resource.MessageKey.CreateNewText ] + text, 
				InsertionPoint.New);
			bool contains = Hierarchy.ContainsNodeType(this, type);
			item.Enabled = !contains;
			AddMenuItem(item);
		}

		/// <summary>
		/// Checks whether there is a child node already created.
		/// </summary>
		/// <returns><c>true</c> if the child node is found, otherwise, <c>false</c>.</returns>
		private bool CryptographySettingsNodeExists()
		{
			CryptographySettingsNode node = Hierarchy.FindNodeByType(typeof(CryptographySettingsNode)) as CryptographySettingsNode;
			return (node != null);
		}

		/// <summary>
		/// Creates the cryptograohy child node.
		/// </summary>
		private void CreateCryptographySettingsNode()
		{
			if (!CryptographySettingsNodeExists())
			{
				AddConfigurationSectionCommand cmd = new AddConfigurationSectionCommand(Site, typeof(CryptographySettingsNode), CryptographySettings.SectionName);
				cmd.Execute(Hierarchy.RootNode);
			}
		}


		private void OnNodeRemoved(object sender, ConfigurationNodeChangedEventArgs e)
		{
			if ( e.Node is ManifestManagerNode )
			{
				settings.ManifestManager = null;
				managerNode = null;
			}

			if ( e.Node is ServiceConfigNode )
			{
				settings.Service = null;
				serviceNode = null;
			}
		}

		private void OnNodeRenamed(object sender, ConfigurationNodeChangedEventArgs e)
		{
			
		}

		#endregion
	}
}
