//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ActivationProcessorProviderData.cs
//
// Contains the implementation of the activation processor configuration data.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System.Xml;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Configuration
{
	/// <summary>
	/// Defines the configuration for the activation processor within the manifest.
	/// </summary>
	public class ActivationProcessorProviderData : ProviderData 
	{
		#region Private members

		/// <summary>
		/// The deserialized activation data.
		/// </summary>
		private Xsd.ActivationProcessorProviderData data;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates an instance of a ActivationProcessorProviderData using the deserialized 
		/// class.
		/// </summary>
		/// <param name="data">The deserialized data.</param>
		public ActivationProcessorProviderData( Xsd.ActivationProcessorProviderData data ) : base()
		{
			this.data = data;
		}

		/// <summary>
		/// Creates an instance of a ActivationProcessorProviderData using the deserialized 
		/// class and the processor name.
		/// </summary>
		/// <param name="data">The deserialized data.</param>
		/// <param name="name">The processor name.</param>
		public ActivationProcessorProviderData( string name, Xsd.ActivationProcessorProviderData data ) : base( name )
		{
			this.data = data;
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// The name of the type.
		/// </summary>
		[XmlAttribute("type")]
		public override string TypeName
		{
			get	{ return data.Type; }
			set { data.Type = value; }
		}

		/// <summary>
		/// The elements included in the configuration.
		/// </summary>
		[XmlAnyElement()]
		public XmlElement AnyElement
		{
			get	{ return data.Any; }
		}

		/// <summary>
		/// The attributes included in the configuration.
		/// </summary>
		[XmlAnyAttribute()] 
		public XmlAttribute[] AnyAttributes
		{
			get	{ return data.AnyAttr;	}
		}

		#endregion
	}
}
