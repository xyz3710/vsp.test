//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ActivationProcessorProviderDataCollection.cs
//
// Contains the implementation of the processor collection.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using Microsoft.Practices.EnterpriseLibrary.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Configuration
{
	/// <summary>
	/// Defines a collection of activation provider data items.
	/// </summary>
	public class ActivationProcessorProviderDataCollection : ProviderDataCollection 
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ActivationProcessorProviderDataCollection()
		{
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Returns the activation processor configuration at the specified index.
		/// </summary>
		public ActivationProcessorProviderData this[int index]
		{
			get { return (ActivationProcessorProviderData)GetProvider(index); }
			set { SetProvider( index, value ); }
		}

		/// <summary>
		/// Returns the activation processor configuration for the processor that matches the given name.
		/// </summary>
		public ActivationProcessorProviderData this[string name]
		{
			get { return (ActivationProcessorProviderData)GetProvider(name); }
			set { SetProvider( name, value ); }
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Adds a provider to the collection.
		/// </summary>
		/// <param name="providerData">The provider configuration data.</param>
		public void Add(ActivationProcessorProviderData providerData)
		{
			AddProvider( providerData );
		}

		#endregion
	}
}
