//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// DownloaderNode.cs
//
// Contains the implementation of the downloader node.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.ComponentModel;
using Microsoft.ApplicationBlocks.Updater;
using Microsoft.ApplicationBlocks.Updater.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Configuration.Design;

namespace Microsoft.ApplicationBlocks.Updater.Configuration.Design
{
	/// <summary>
	/// Defines the configuration node for the downloaders.
	/// </summary>
	[Image(typeof(DownloaderNode))]
	public class DownloaderNode : ConfigurationNode
	{
		#region Private members

		/// <summary>
		/// The generic downloader configuration.
		/// </summary>
		private DownloadProviderData data;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a DownloaderNode with the downloader data.
		/// </summary>
		/// <param name="data">The downloader configuration instance.</param>
		public DownloaderNode( DownloadProviderData data ): base()
		{
			this.data = data;
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Gets the downloader provider information.
		/// </summary>
		[Browsable(false)]
		public virtual DownloadProviderData DownloadProviderData
		{
			get { return data; }
		}

		#endregion

		#region Protected members 

		/// <summary>
		/// Set the name of the node.
		/// </summary>
		protected override void OnSited()
		{
			base.OnSited();
			Site.Name = Resource.ResourceManager[ Resource.MessageKey.DownloaderMenuItem ];
		}

		#endregion
	}
}
