//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// UpdaterConfigurationView.cs
//
// Contains the implementation of the updater configuration view.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Configuration;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Configuration
{
	/// <summary>
	/// 
	/// </summary>
	public class UpdaterConfigurationView : ConfigurationView
	{
		/// <summary>
		/// Represents a view into configuration data.
		/// </summary>
		public UpdaterConfigurationView(): base(ConfigurationManager.GetCurrentContext())
		{
		}

		/// <summary>
		/// <para>Initialize a new instance of the <see cref="UpdaterConfigurationView"/> with a <see cref="ConfigurationContext"/>.</para>
		/// </summary>
		/// <param name="context">
		/// <para>A <see cref="ConfigurationContext"/> object.</para>
		/// </param>
		public UpdaterConfigurationView(ConfigurationContext context): base(context)
		{
		}

		/// <summary>
		/// Returns the <see cref="DownloadProviderData"/> for the specified downloader name.
		/// </summary>
		/// <param name="downloadProviderName">The name of the downloader</param>
		/// <returns>A <see cref="DownloadProviderData"/> instance</returns>
		public virtual DownloadProviderData GetDownloadProviderData(string downloadProviderName)
		{
			return GetApplicationUpdaterSettings().Downloaders[downloadProviderName];
		}

		/// <summary>
		/// Returns the <see cref="ApplicationUpdaterSettings"/> configured for the updater.
		/// </summary>
		/// <returns>An <see cref="ApplicationUpdaterSettings"/> instance</returns>
		public virtual ApplicationUpdaterSettings GetApplicationUpdaterSettings()
		{
			return ConfigurationContext.GetConfiguration(ApplicationUpdaterSettings.SectionName) as ApplicationUpdaterSettings;
		}

		/// <summary>
		/// Gets the default URI to get the manifest from.
		/// </summary>
		public virtual Uri DefaultManifestUriLocation
		{
			get
			{
				Uri manifestUri = null;
				string location = ( GetApplicationUpdaterSettings() != null) ?  GetApplicationUpdaterSettings().ManifestUri : null;
				if ( location != null )
				{
					manifestUri = new Uri( location );
				}
				else
				{
					ConfigurationException manifestUriConfigurationException = new ConfigurationException( Resource.ResourceManager[ Resource.MessageKey.DefaultManifestUriMissing ] );
					Logger.LogException( manifestUriConfigurationException );
					throw manifestUriConfigurationException; 
				}
				return manifestUri;
			}
		}

		/// <summary>
		/// Gets the application ID for the configured application.
		/// </summary>
		public virtual string ApplicationId
		{
			get
			{
				string applicationId = null;

				if ( GetApplicationUpdaterSettings() != null && (GetApplicationUpdaterSettings().ApplicationId != null && GetApplicationUpdaterSettings().ApplicationId.Length != 0) )
				{
					applicationId = GetApplicationUpdaterSettings().ApplicationId;
				}

				return applicationId;
			}
		}
		
		/// <summary>
		/// The base path for the temp folders.
		/// </summary>
		public string BasePath
		{
			get
			{
				string applicationBasePath = null;

				if ( GetApplicationUpdaterSettings() != null )
				{
					applicationBasePath =  GetApplicationUpdaterSettings().BasePath;
				}
				if ( applicationBasePath == null || ( applicationBasePath != null && applicationBasePath.Length == 0) )
				{
					applicationBasePath = Path.Combine( Environment.CurrentDirectory, "UAB" );
				}

				if ( !Directory.Exists( applicationBasePath ) )
				{
					DirectoryInfo info = Directory.CreateDirectory( applicationBasePath );
					applicationBasePath = info.FullName;
				}
				else
				{
					applicationBasePath = Path.GetFullPath( applicationBasePath );
				}

				return applicationBasePath;
			}
		}

		/// <summary>
		/// Indicates whether the application is a service.
		/// </summary>
		public bool IsService
		{
			get
			{
				return (GetApplicationUpdaterSettings().Service != null);
			}
		}

		/// <summary>
		/// The polling interval between updates.
		/// </summary>
		public TimeSpan ServiceQueryInterval
		{
			get
			{
				if( IsService )
				{
					return TimeSpan.Parse( GetApplicationUpdaterSettings().Service.Interval );	
				}
				return TimeSpan.MaxValue;
			}
		}

		/// <summary>
		/// The manifest manager configuration.
		/// </summary>
		internal ManifestManagerSettings ManifestManager
		{
			get
			{
				return GetApplicationUpdaterSettings().ManifestManager;
			}
		}

		/// <summary>
		/// The configuration for the downloaders.
		/// </summary>
		internal DownloadProviderDataCollection Downloaders
		{
			get { return GetApplicationUpdaterSettings().Downloaders; }
		}

	}
}