//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// Logger.cs
//
// Contains the implementation of the logger helper.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Diagnostics;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Instrumentation;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// Helper class for logging and exception reporting.
	/// </summary>
	public sealed class Logger
	{
		#region Declarations

		/// <summary>
		/// The source name for the events.
		/// </summary>
		private const string eventLoggerSourceName	= "ApplicationUpdater"; 

		/// <summary>
		/// The type of the event entry.
		/// </summary>
		private const EventLogEntryType	defaultLogEntryType	= EventLogEntryType.Information;

		/// <summary>
		/// The default entry Id.
		/// </summary>
		private const int defaultLogEntryId	= 1000;

		/// <summary>
		/// The singleton instance of the helper class.
		/// </summary>
		private static readonly Logger instance = new Logger();

		/// <summary>
		/// The EntLib event logger.
		/// </summary>
		private EventLogger	logger;
		
		#endregion

		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		private Logger()
		{
			logger = new EventLogger( eventLoggerSourceName, defaultLogEntryType, defaultLogEntryId );
		}

		#endregion

		#region Singleton implementation

		/// <summary>
		/// Returns the singleton instance.
		/// </summary>
		private static Logger Current
		{
			get
			{
				return instance;
			}
		}

		#endregion

		#region Public static inteface

		/// <summary>
		/// Log an error message.
		/// </summary>
		/// <param name="message">The message string.</param>
		public static void LogError( string message )
		{
			Current.WriteLog( message, EventLogEntryType.Error );
		}

		/// <summary>
		/// Log a warning message.
		/// </summary>
		/// <param name="message">The message string.</param>
		public static void LogWarning( string message )
		{
			Current.WriteLog( message, EventLogEntryType.Warning );
		}

		/// <summary>
		/// Log an information message.
		/// </summary>
		/// <param name="message">The message string.</param>
		public static void LogInformation( string message )
		{
			Current.WriteLog( message, EventLogEntryType.Information );
		}

		/// <summary>
		/// Log an exception.
		/// </summary>
		/// <param name="ex">The exception instance.</param>
		public static void LogException( Exception ex )
		{
			LogException( ex, null );
		}

		/// <summary>
		/// Log an exception and a message.
		/// </summary>
		/// <param name="ex">The exception instance.</param>
		/// <param name="message">The message string.</param>
		public static void LogException( Exception ex, string message )
		{
			ExceptionFormatter formatter = new ExceptionFormatter();
			if ( message != null && message.Length > 0 )
			{
				LogError( message + "\n" + formatter.GetMessage( ex ) );
			}
			else
			{
				LogError( formatter.GetMessage( ex ) );
			}
		}

		/// <summary>
		/// Log and throw the exception specified.
		/// </summary>
		/// <param name="ex">The exception instance.</param>
		public static void LogAndThrowException( Exception ex )
		{
			LogAndThrowException( null, ex );
		}

		/// <summary>
		/// Log and throw the exception specified with a message string.
		/// </summary>
		/// <param name="ex">The exception instance.</param>
		/// <param name="message">The message string.</param>
		public static void LogAndThrowException( string message, Exception ex )
		{
			ExceptionFormatter formatter = new ExceptionFormatter();
			if ( message != null && message.Length > 0 )
			{
				LogError( message + "\n" + formatter.GetMessage( ex ) );
			}
			else
			{
				LogError( formatter.GetMessage( ex ) );
			}
			throw ex;
		}

		#endregion
		
		#region Instance members

		/// <summary>
		/// Write the log to EntLib logger.
		/// </summary>
		/// <param name="message">The message string.</param>
		/// <param name="entryType">The event type.</param>
		private void WriteLog( string message, EventLogEntryType entryType )
		{
			logger.Log( message, entryType );
		}

		#endregion
	}
}
