//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// UpdaterTaskState.cs
//
// Contains the implementation of the UpdaterTaskState enumeration.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// The enumeration of UpdaterTask states.
	/// </summary>
	public enum UpdaterTaskState
	{
		/// <summary>
		/// No state specified.
		/// </summary>
		None,

		/// <summary>
		/// Updater is starting the download process.
		/// </summary>
		Downloading,

		/// <summary>
		/// Updater has completed the download.
		/// </summary>
		Downloaded,

		/// <summary>
		/// A download error occurred.
		/// </summary>
		DownloadError,

		/// <summary>
		/// The activation is being initialized.
		/// </summary>
		ActivationInitializing,

		/// <summary>
		/// The activation initialization has been aborted.
		/// </summary>
		ActivationInitializationAborted,

		/// <summary>
		/// A new update is being activated.
		/// </summary>
		Activating,

		/// <summary>
		/// An error occurred during activation.
		/// </summary>
		ActivationError,

		/// <summary>
		/// A new update has been activated.
		/// </summary>
		Activated,

		/// <summary>
		/// A new update was cancelled.
		/// </summary>
		Cancelled
	}
}