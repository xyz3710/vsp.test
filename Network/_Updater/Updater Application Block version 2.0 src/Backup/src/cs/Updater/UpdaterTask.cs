//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// UpdaterTask.cs
//
// Contains the implementation of the updater task.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Microsoft.ApplicationBlocks.Updater.Configuration;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// Holds the state information about an update in progress.
	/// </summary>
	[Serializable]
	public class UpdaterTask : ISerializable
	{
		#region Private fields

		/// <summary>
		/// The task Id.
		/// </summary>
		private Guid id;

		/// <summary>
		/// The base folder where files will be downloaded.
		/// </summary>
		private string donwnloadFilesBase;

		/// <summary>
		/// The manifest that is being processed.
		/// </summary>
		private Manifest manifest;

		/// <summary>
		/// The task context where all the components can set information for later retrieval.
		/// </summary>
		private StateBag context;

		/// <summary>
		/// The status of the updater task.
		/// </summary>
		private UpdaterTaskState state = UpdaterTaskState.None;

		/// <summary>
		/// An object that can be used to synchronize access to the UpdaterTask.
		/// </summary>
		private object syncRoot = new object();

		#endregion

		#region Constructors

		/// <summary>
		/// Creates an instance of the updater task using the specified manifest.
		/// </summary>
		/// <param name="manifest">The manifest corresponding to an updater task.</param>
		public UpdaterTask( Manifest manifest )
		{
			id = Guid.NewGuid();
			context = new StateBag();
			Init( manifest );
		}

		#endregion

		#region UpdaterTask Members

		/// <summary>
		/// A property bag where components can set key-value pairs of information for later retrieval.
		/// </summary>
		/// <param name="key">The index key of the item to locate.</param>
		public object this[ string key ]
		{
			get
			{
				return context[ key ];
			}
			set
			{
				context[ key ] = value;
			}
		}

		/// <summary>
		/// Gets an object that can be used to synchronize access to the UpdaterTask.
		/// </summary>
		public object SyncRoot
		{
			get { return syncRoot; }
		}

		/// <summary>
		/// The base folder where files will be downloaded.
		/// </summary>
		public string DownloadFilesBase
		{
			get
			{
				return donwnloadFilesBase;
			}
		}

		/// <summary>
		/// Initializes an existing UpdaterTask with the specified manifest.
		/// </summary>
		/// <param name="manifest">The manifest corresponding to an updater task.</param>
		public void Init(Manifest manifest)
		{
			this.manifest = manifest;
			donwnloadFilesBase = Path.Combine( new UpdaterConfigurationView().BasePath, "downloader" );
			donwnloadFilesBase = Path.Combine( donwnloadFilesBase, this.manifest.Application.ApplicationId );
			donwnloadFilesBase = Path.Combine( donwnloadFilesBase, id.ToString() );
		}

		/// <summary>
		/// An unique identifier for the UpdaterTask.
		/// </summary>
		public Guid TaskId
		{
			get
			{
				return id;
			}
		}

		/// <summary>
		/// The current state of the UpdaterTask.
		/// <see cref="UpdaterTaskState"/>
		/// </summary>
		public UpdaterTaskState State
		{
			get
			{
				return state;
			}
			set
			{
				state = value;
			}
		} 

		/// <summary>
		/// The manifest corresponding to the current UpdaterTask.
		/// </summary>
		public Manifest Manifest
		{
			get
			{
				return manifest;
			}
		}
		
		#endregion

		#region ISerializable Members

		/// <summary>
		/// Constructor to support serialization required for storing the task.
		/// </summary>
		/// <param name="info">The serialization information.</param>
		/// <param name="context">The serialization context.</param>
		[SecurityPermission(SecurityAction.LinkDemand)]
		protected UpdaterTask(SerializationInfo info, StreamingContext context)
		{
			manifest = (Manifest)info.GetValue( "_manifest", typeof( Manifest) );
			state = (UpdaterTaskState)info.GetValue( "_state",typeof( UpdaterTaskState) );
			id = (Guid)info.GetValue( "_id", typeof( Guid ) );
			this.context = (StateBag)info.GetValue( "_context", typeof( StateBag ) );
			donwnloadFilesBase = (string)info.GetValue( "_donwnloadFilesBase", typeof( string ) );
		}

		/// <summary>
		/// Method used by the seralization mechanism to retrieve the serialized information.
		/// </summary>
		/// <param name="info">The serialization information.</param>
		/// <param name="context">The serialization context.</param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue( "_manifest", manifest );
			info.AddValue( "_state", state );
			info.AddValue( "_id", id );
			info.AddValue( "_context", this.context );
			info.AddValue( "_donwnloadFilesBase", donwnloadFilesBase );
		}

		#endregion

	}
	
}
