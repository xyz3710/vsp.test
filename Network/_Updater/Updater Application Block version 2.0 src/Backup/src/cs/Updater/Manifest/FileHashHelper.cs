//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// FileHashHelper.cs
//
// Contains the implementation of the file hash helper.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.IO;
using System.Security.Cryptography;

using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// Allows calculating and comparing hashes of existing files.
	/// </summary>
	internal sealed class FileHashHelper
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		private FileHashHelper()
		{
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Calculares the hash of a specific file and compares the resultng hash with the provided one.
		/// </summary>
		/// <param name="hashProviderName">The name of the EntLib hash provider.</param>
		/// <param name="filePath">The path of the file to calculate the hash.</param>
		/// <param name="hash">The calculated hash provided in the manifest.</param>
		/// <returns><c>true</c> if the hash results the same, otherwise <c>false</c>.</returns>
		public static bool CompareHash(string hashProviderName, string filePath, string hash )
		{
			using( FileStream fileStream = new FileStream( filePath, FileMode.Open, FileAccess.Read, FileShare.Read ) )
			{
				byte[] fileBytes = new byte[ fileStream.Length ];
				fileStream.Read( fileBytes, 0, fileBytes.Length );
				return Cryptographer.CompareHash( hashProviderName, fileBytes, Convert.FromBase64String( hash ) );
			}
		}

		#endregion
	}
}
