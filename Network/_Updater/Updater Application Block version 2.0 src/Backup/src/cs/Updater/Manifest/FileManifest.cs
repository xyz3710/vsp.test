//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// FileManifest.cs
//
// Contains the implementation of the file information in the manifest.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Xml;

namespace Microsoft.ApplicationBlocks.Updater
{
	/// <summary>
	/// Defines the information of a file in the manifest.
	/// </summary>
	public class FileManifest
	{
		#region Private members

		/// <summary>
		/// The source location for the file.
		/// </summary>
		private string sourceLocation;

		/// <summary>
		/// Indicates whether the file is transient.
		/// </summary>
		private bool transient;

		/// <summary>
		/// The calculated hash value for the file.
		/// </summary>
		private string fileHashValue;

		/// <summary>
		/// The list of attributes defined in the file element.
		/// </summary>
		private XmlAttribute[] otherAttributes;

		#endregion

		#region Constructor

		/// <summary>
		/// Creates a new FileManifest with the desrialized manifest file information.
		/// </summary>
		/// <param name="file">The FileManifest instance.</param>
		public FileManifest( Xsd.ManifestFile file )
		{
			otherAttributes = file.AnyAttr;
			sourceLocation = file.Source;
			fileHashValue = file.Hash;
			transient = file.Transient == Xsd.YesNoBoolType.True || file.Transient == Xsd.YesNoBoolType.Yes;
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The source location of the file.
		/// </summary>
		public string Source
		{
			get
			{
				return sourceLocation;
			}
		} 

		/// <summary>
		/// Whether the file is transient or not.
		/// </summary>
		public bool Transient
		{
			get
			{
				return transient;
			}
		}

		/// <summary>
		/// The calculated hash for the file.
		/// </summary>
		public string Hash
		{
			get
			{
				return fileHashValue;
			}
		}
		
		/// <summary>
		/// The attributes specified for the file.
		/// </summary>
		public XmlAttribute[] Attributes
		{
			get { return otherAttributes; }
		}

		#endregion
	}
}
