//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ActivationPausedException.cs
//
// This class is used for notification of a pause during the execution of an activator.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Microsoft.ApplicationBlocks.Updater.Activator
{
	/// <summary>
	/// Exception thrown by activators when the activation process is paused.
	/// </summary>
	[Serializable]
	public class ActivationPausedException : Exception
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ActivationPausedException() : base( Resource.ResourceManager[ Resource.MessageKey.ActivationPausedMessage ] )
		{
		}

		/// <summary>
		/// Creates an ActivationPausedException with a specified message.
		/// </summary>
		/// <param name="message">The exception message string.</param>
		public ActivationPausedException(string message) : base( message )
		{
		}

		/// <summary>
		/// Creates an ActivationPausedException with a specified message.
		/// </summary>
		/// <param name="message">The exception message string.</param>
		/// <param name="innerException">The inner exception detected.</param>
		public ActivationPausedException( string message, Exception innerException ) : base( message, innerException ) 
		{
		}

		/// <summary>
		/// Constructor used by the serialization infrastructure.
		/// </summary>
		/// <param name="info">The serialization information for the object.</param>
		/// <param name="context">The context for the serialization.</param>
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		protected ActivationPausedException(SerializationInfo info, StreamingContext context) : base( info, context ) 
		{
		}

		#endregion

		#region Public members

		/// <summary>
		/// Used by the serialization infrastructure.
		/// </summary>
		/// <param name="info">The serialization information.</param>
		/// <param name="context">The serialization context.</param>
		[SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter=true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context) 
		{
			base.GetObjectData( info, context );
		}

		#endregion
	}
}
