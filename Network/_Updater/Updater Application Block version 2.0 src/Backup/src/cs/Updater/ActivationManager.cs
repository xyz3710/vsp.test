//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ActivationManger.cs
// 
// Contains the implementatin of the activation manager.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using Microsoft.ApplicationBlocks.Updater.Utilities;

namespace Microsoft.ApplicationBlocks.Updater.Activator
{
	/// <summary>
	/// The activation manager orchestrates the activation process.
	/// </summary>
	internal sealed class ActivationManager
	{
		#region Constructor

		/// <summary>
		/// Default constructor
		/// </summary>
		public ActivationManager()
		{
		}

		#endregion

		#region Public methods

		/// <summary>
		/// Start the activation of an UpdaterTask.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		public void SubmitTask( UpdaterTask task )
		{
			if ( task.Manifest.ActivationProcessors != null  && task.Manifest.ActivationProcessors.Count > 0 )
			{
				AppDomain.CurrentDomain.AppendPrivatePath( task.DownloadFilesBase ); 
				Activate( task );
			}
			else
			{
				OnActivationCompleted(task, true);
			}
		}

		#endregion

		#region Private members

		/// <summary>
		/// Starts the activation of an UpdaterTask.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		private void Activate( UpdaterTask task )
		{
			int current = 0;

			// Initialization phase
			OnActivationInitializing( task );
			IActivationProcessor[] activationProcessors = new IActivationProcessor[ task.Manifest.ActivationProcessors.Count ];
			for(current=0; current<activationProcessors.Length; current++)
			{
				activationProcessors[current] = ProcessorFactory.Create( task.Manifest.ActivationProcessors[current].TypeName );
				try
				{
					activationProcessors[current].Init( task.Manifest.ActivationProcessors[current], task );
					activationProcessors[current].PrepareExecution();
				}
				catch( ActivationPausedException ex )
				{
					Logger.LogException( ex );
					OnActivationInitializationAborted( task, activationProcessors[current], ex );
					return;
				}
				catch( Exception ex )
				{
					Logger.LogException( ex );
					OnActivationError( task, activationProcessors[current], ex );
					return;
				}
			}

			// Execution phase
			OnActivationStarted( task );
			bool error = false;
			try
			{
				for(current=0; current<activationProcessors.Length && !error; current++)
				{
					activationProcessors[current].Execute( );
				}
			}
			catch( Exception ex )
			{
				error = true;
				Logger.LogException( ex );
				OnActivationError( task, activationProcessors[current], ex );
			}
			finally
			{
				// Error handling phase
				if ( error )
				{
					for(int i=current-1; i>=0; i--)
					{
						try
						{
							activationProcessors[i].OnError( );
						}
						catch( Exception ex )
						{
							// If an OnError method fails, just log the exception and
							// continue with the processing, giving the remaining processors
							// an opportunity to handle this situation
							Logger.LogException( ex, Resource.ResourceManager[ Resource.MessageKey.ProcessorOnErrorException, task.Manifest.ActivationProcessors[i].Name ] );
						}
					}
				}
				OnActivationCompleted( task, !error );
			}
		}

		#endregion

		#region Events

		/// <summary>
		/// Notifies about the initialization of the activation process.
		/// </summary>
		public event ActivationTaskInitializingEventHandler ActivationInitializing;

		/// <summary>
		/// Notifies about the initialization of the activation process was aborted.
		/// </summary>
		public event ActivationTaskInitializationAbortedEventHandler ActivationInitializationAborted;

		/// <summary>
		/// Notifies about the start of the activation process.
		/// </summary>
		public event ActivationTaskStartedEventHandler ActivationStarted;

		/// <summary>
		/// Notifies about an error during the activation process.
		/// </summary>
		public event ActivationTaskErrorEventHandler ActivationError;

		/// <summary>
		/// Notifies about the completion of the activation process.
		/// </summary>
		public event ActivationTaskCompletedEventHandler ActivationCompleted;

		/// <summary>
		/// Method used to fre the event.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		private void OnActivationStarted( UpdaterTask task )
		{
			task.State = UpdaterTaskState.Activating;
			if ( ActivationStarted != null )
			{
				TaskEventArgs args = new TaskEventArgs(task);
				ActivationStarted(this, args );
			}
		}

		/// <summary>
		/// Method used to fre the event.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		private void OnActivationInitializing(UpdaterTask task)
		{
			task.State = UpdaterTaskState.ActivationInitializing;
			if ( ActivationInitializing != null )
			{
				TaskEventArgs args = new TaskEventArgs(task);
				ActivationInitializing( this, args );
			}
		}

		/// <summary>
		/// Method used to fre the event.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <param name="processor">The processor instance.</param>
		/// <param name="ex">The exception instance.</param>
		private void OnActivationInitializationAborted( UpdaterTask task, IActivationProcessor processor, Exception ex )
		{
			task.State = UpdaterTaskState.ActivationInitializationAborted;
			if ( ActivationInitializationAborted != null )
			{
				ActivationTaskErrorEventArgs args = new ActivationTaskErrorEventArgs(task, processor, ex);
				ActivationInitializationAborted(this, args );
			}
		}

		/// <summary>
		/// Method used to fre the event.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <param name="processor">The processor instance.</param>
		/// <param name="ex">The exception instance.</param>
		private void OnActivationError( UpdaterTask task, IActivationProcessor processor, Exception ex)
		{
			task.State = UpdaterTaskState.ActivationError;
			if ( ActivationError != null )
			{
				ActivationTaskErrorEventArgs args = new ActivationTaskErrorEventArgs(task, processor, ex);
				ActivationError( this, args);
			}
		}

		/// <summary>
		/// Method used to fre the event.
		/// </summary>
		/// <param name="task">The UpdaterTask instance.</param>
		/// <param name="success">The success status.</param>
		private void OnActivationCompleted( UpdaterTask task, bool success)
		{
			if ( success )
			{
			        task.State = UpdaterTaskState.Activated;
			}
			if ( ActivationCompleted != null )
			{
				ActivationTaskCompleteEventArgs args = new ActivationTaskCompleteEventArgs(task, success);
				ActivationCompleted( this, args);
			}
		}

		#endregion
	}
}
