//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ValidateHashProcessorEditor.cs
//
// Contains the implementation of the editor control for the ValidateHashProcessor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Summary description for ValidateHashProcessorEditor.
	/// </summary>
	public class ValidateHashProcessorEditor : ProcessorEditorControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtSource;

		private ValidateHashProcessorItem item;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor
		/// </summary>
		public ValidateHashProcessorEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.txtSource = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Source";
			// 
			// txtSource
			// 
			this.txtSource.Location = new System.Drawing.Point(120, 9);
			this.txtSource.Name = "txtSource";
			this.txtSource.Size = new System.Drawing.Size(248, 20);
			this.txtSource.TabIndex = 3;
			this.txtSource.Text = "";
			// 
			// ValidateHashProcessorEditor
			// 
			this.Controls.Add(this.txtSource);
			this.Controls.Add(this.label1);
			this.Name = "ValidateHashProcessorEditor";
			this.Size = new System.Drawing.Size(376, 40);
			this.ResumeLayout(false);

		}
		#endregion

		/// Gets or sets the wrapped item to edit
		public override ProcessorItem Item
		{
			get
			{
				if ( item == null )
				{
					item = new ValidateHashProcessorItem();
				}
				return item;
			}
			set
			{
				item = value as ValidateHashProcessorItem;
				if ( item == null )
				{
					throw new ManifestEditorException( Resource.ResourceManager[ Resource.MessageKey.InvalidProcessorItemForEdition, value.GetType().FullName, this.GetType().FullName ] );
				}
				txtSource.Text = item.Source;
			}
		}

		/// Called when the user accepts the edition changes
		public override void OnOK()
		{
			base.OnOK ();
			item.Source = txtSource.Text;
		}
	}
}
