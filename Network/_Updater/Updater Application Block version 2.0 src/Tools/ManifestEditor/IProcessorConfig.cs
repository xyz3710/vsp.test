//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// IProcessorConfig.cs
//
// Contains the implementation of the IProcessorConfig interface.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System;
using System.Xml;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Describes the contract Processor Editors must implement
	/// </summary>
	public interface IProcessorConfig
	{
		/// <summary>
		/// Gets or sets the Processdor Item being edited
		/// </summary>
		ProcessorItem Item { get; set; }
	}
}
