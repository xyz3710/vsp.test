//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ProcessorEditorFactory.cs
//
// Contains the implementation of the processor editor factory.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Security.Cryptography;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Creates EntLib hash algorithm instances.
	/// </summary>
	public sealed class ProcessorEditorFactory
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		private ProcessorEditorFactory()
		{
		}

		#endregion

		#region Static members

		/// <summary>
		/// Factory method for a HashAlgorithm provoder.
		/// </summary>
		/// <param name="typeFullName">The provider type FullName of the ActivationProcessor to configure.</param>
		/// <returns>An instance of a ProcessorControlEditor associated to the item to edit.</returns>
		public static ProcessorEditorControl Create( string typeFullName )
		{
			Type t = Type.GetType( typeFullName );
			object instance = System.Activator.CreateInstance( t );
			if ( instance is ProcessorEditorControl )
			{
				return instance as ProcessorEditorControl;
			}
			throw new ManifestEditorException( Resource.ResourceManager[ Resource.MessageKey.EditorControlNotConfigured, typeFullName ] );
		}		

		#endregion
	}
}
