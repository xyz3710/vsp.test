//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// GacUtilProcessorEditor.cs
//
// Contains the implementation of the editor control for the GacUtilProcessor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Summary description for GacUtilProcessorEditor.
	/// </summary>
	public class GacUtilProcessorEditor : ProcessorEditorControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;

		private GacUtilProcessorItem item;
		private System.Windows.Forms.TextBox txtOptions;
		private System.Windows.Forms.TextBox txtAssemblyFile;
		private System.Windows.Forms.TextBox txtAssemblyName;
		private System.Windows.Forms.Label label3;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor
		/// </summary>
		public GacUtilProcessorEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtOptions = new System.Windows.Forms.TextBox();
			this.txtAssemblyFile = new System.Windows.Forms.TextBox();
			this.txtAssemblyName = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Options";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 36);
			this.label2.Name = "label2";
			this.label2.TabIndex = 1;
			this.label2.Text = "Assembly file";
			// 
			// txtOptions
			// 
			this.txtOptions.Location = new System.Drawing.Point(120, 9);
			this.txtOptions.Name = "txtOptions";
			this.txtOptions.Size = new System.Drawing.Size(248, 20);
			this.txtOptions.TabIndex = 3;
			this.txtOptions.Text = "";
			// 
			// txtAssemblyFile
			// 
			this.txtAssemblyFile.Location = new System.Drawing.Point(120, 37);
			this.txtAssemblyFile.Name = "txtAssemblyFile";
			this.txtAssemblyFile.Size = new System.Drawing.Size(248, 20);
			this.txtAssemblyFile.TabIndex = 4;
			this.txtAssemblyFile.Text = "";
			// 
			// txtAssemblyName
			// 
			this.txtAssemblyName.Location = new System.Drawing.Point(120, 65);
			this.txtAssemblyName.Name = "txtAssemblyName";
			this.txtAssemblyName.Size = new System.Drawing.Size(248, 20);
			this.txtAssemblyName.TabIndex = 6;
			this.txtAssemblyName.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 64);
			this.label3.Name = "label3";
			this.label3.TabIndex = 5;
			this.label3.Text = "Assembly name";
			// 
			// GacUtilProcessorEditor
			// 
			this.Controls.Add(this.txtAssemblyName);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtAssemblyFile);
			this.Controls.Add(this.txtOptions);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "GacUtilProcessorEditor";
			this.Size = new System.Drawing.Size(376, 96);
			this.ResumeLayout(false);

		}
		#endregion

		/// Gets or sets the wrapped item to edit
		public override ProcessorItem Item
		{
			get
			{
				if ( item == null )
				{
					item = new GacUtilProcessorItem();
				}
				return item;
			}
			set
			{
				item = value as GacUtilProcessorItem;
				if ( item == null )
				{
					throw new ManifestEditorException( Resource.ResourceManager[ Resource.MessageKey.InvalidProcessorItemForEdition, value.GetType().FullName, this.GetType().FullName ] );
				}
				txtOptions.Text = item.Options;
				txtAssemblyFile.Text = item.AssemblyFile;
				txtAssemblyName.Text = item.AssemblyName;
			}
		}

		/// Called when the user accepts the edition changes
		public override void OnOK()
		{
			base.OnOK ();
			item.Options = txtOptions.Text;
			item.AssemblyFile = txtAssemblyFile.Text;
			item.AssemblyName = txtAssemblyName.Text;
		}

		/// Called when the user rejects the edition changes
		public override void OnCancel()
		{
			base.OnCancel ();
		}
	}
}
