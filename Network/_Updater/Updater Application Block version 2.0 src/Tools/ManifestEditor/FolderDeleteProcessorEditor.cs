//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// FolderDeleteProcessorEditor.cs
//
// Contains the implementation of the editor control for the FolderDeleteProcessor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.Windows.Forms;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Summary description for FileCopyProcessorEditor.
	/// </summary>
	public class FolderDeleteProcessorEditor : ProcessorEditorControl
	{
		private System.Windows.Forms.Label label1;

		private FolderDeleteProcessorItem item;
		private System.Windows.Forms.TextBox txtPath;
		private System.Windows.Forms.CheckBox chkRecursive;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor
		/// </summary>
		public FolderDeleteProcessorEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.txtPath = new System.Windows.Forms.TextBox();
			this.chkRecursive = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Path";
			// 
			// txtPath
			// 
			this.txtPath.Location = new System.Drawing.Point(120, 9);
			this.txtPath.Name = "txtPath";
			this.txtPath.Size = new System.Drawing.Size(248, 20);
			this.txtPath.TabIndex = 3;
			this.txtPath.Text = "";
			// 
			// chkRecursive
			// 
			this.chkRecursive.Location = new System.Drawing.Point(120, 40);
			this.chkRecursive.Name = "chkRecursive";
			this.chkRecursive.TabIndex = 4;
			this.chkRecursive.Text = "Recursive";
			// 
			// FolderDeleteProcessorEditor
			// 
			this.Controls.Add(this.chkRecursive);
			this.Controls.Add(this.txtPath);
			this.Controls.Add(this.label1);
			this.Name = "FolderDeleteProcessorEditor";
			this.Size = new System.Drawing.Size(376, 72);
			this.ResumeLayout(false);

		}
		#endregion

		/// Gets or sets the wrapped item to edit
		public override ProcessorItem Item
		{
			get
			{
				if ( item == null )
				{
					item = new FolderDeleteProcessorItem();
				}
				return item;
			}
			set
			{
				item = value as FolderDeleteProcessorItem;
				if ( item == null )
				{
					throw new ManifestEditorException( Resource.ResourceManager[ Resource.MessageKey.InvalidProcessorItemForEdition, value.GetType().FullName, this.GetType().FullName ] );
				}
				txtPath.Text = item.Path;
				chkRecursive.Checked = bool.Parse( item.Recursive );
			}
		}

		/// Called when the user accepts the edition changes
		public override void OnOK()
		{
			base.OnOK ();
			item.Path = txtPath.Text;
			item.Recursive = chkRecursive.Checked.ToString( CultureInfo.InvariantCulture );
		}

		/// Called when the user rejects the edition changes
		public override void OnCancel()
		{
			base.OnCancel ();
		}
	}
}
