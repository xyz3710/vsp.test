//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// MSIProcessorEditor.cs
//
// Contains the implementation of the editor control for the MSIProcessor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Summary description for MSIProcessorConfig.
	/// </summary>
	public class MSIProcessorEditor : ProcessorEditorControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtProductCode;
		private System.Windows.Forms.TextBox txtPropertiesValues;
		private System.Windows.Forms.TextBox txtPackagePath;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox cboInstallType;
		private System.Windows.Forms.ComboBox cboUILevel;

		private MSIProcessorItem item;


		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor
		/// </summary>
		public MSIProcessorEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			cboInstallType.SelectedIndex = 0;
			cboUILevel.SelectedItem = "msiUILevelDefault";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtProductCode = new System.Windows.Forms.TextBox();
			this.txtPropertiesValues = new System.Windows.Forms.TextBox();
			this.txtPackagePath = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.cboInstallType = new System.Windows.Forms.ComboBox();
			this.cboUILevel = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 167);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Package path";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 87);
			this.label2.Name = "label2";
			this.label2.TabIndex = 1;
			this.label2.Text = "Product code";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 127);
			this.label3.Name = "label3";
			this.label3.TabIndex = 3;
			this.label3.Text = "Properties values";
			// 
			// txtProductCode
			// 
			this.txtProductCode.Location = new System.Drawing.Point(104, 88);
			this.txtProductCode.Name = "txtProductCode";
			this.txtProductCode.Size = new System.Drawing.Size(344, 20);
			this.txtProductCode.TabIndex = 3;
			this.txtProductCode.Text = "";
			// 
			// txtPropertiesValues
			// 
			this.txtPropertiesValues.Location = new System.Drawing.Point(104, 128);
			this.txtPropertiesValues.Name = "txtPropertiesValues";
			this.txtPropertiesValues.Size = new System.Drawing.Size(344, 20);
			this.txtPropertiesValues.TabIndex = 4;
			this.txtPropertiesValues.Text = "";
			// 
			// txtPackagePath
			// 
			this.txtPackagePath.Location = new System.Drawing.Point(104, 168);
			this.txtPackagePath.Name = "txtPackagePath";
			this.txtPackagePath.Size = new System.Drawing.Size(344, 20);
			this.txtPackagePath.TabIndex = 5;
			this.txtPackagePath.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 47);
			this.label4.Name = "label4";
			this.label4.TabIndex = 8;
			this.label4.Text = "UI Level";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 7);
			this.label5.Name = "label5";
			this.label5.TabIndex = 9;
			this.label5.Text = "Install type";
			// 
			// cboInstallType
			// 
			this.cboInstallType.DisplayMember = "installType";
			this.cboInstallType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboInstallType.Items.AddRange(new object[] {
																"Install",
																"Remove",
																"Patch"});
			this.cboInstallType.Location = new System.Drawing.Point(104, 8);
			this.cboInstallType.Name = "cboInstallType";
			this.cboInstallType.Size = new System.Drawing.Size(184, 21);
			this.cboInstallType.TabIndex = 1;
			// 
			// cboUILevel
			// 
			this.cboUILevel.DisplayMember = "uiLevel";
			this.cboUILevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboUILevel.Items.AddRange(new object[] {
															"msiUILevelNoChange",
															"msiUILevelDefault",
															"msiUILevelNone",
															"msiUILevelBasic",
															"msiUILevelReduced",
															"msiUILevelFull",
															"msiUILevelHideCancel",
															"msiUILevelProgressOnly",
															"msiUILevelEndDialog"});
			this.cboUILevel.Location = new System.Drawing.Point(104, 48);
			this.cboUILevel.Name = "cboUILevel";
			this.cboUILevel.Size = new System.Drawing.Size(184, 21);
			this.cboUILevel.TabIndex = 2;
			this.cboUILevel.ValueMember = "uiLevel";
			// 
			// MSIProcessorConfig
			// 
			this.Controls.Add(this.cboUILevel);
			this.Controls.Add(this.cboInstallType);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtPackagePath);
			this.Controls.Add(this.txtPropertiesValues);
			this.Controls.Add(this.txtProductCode);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "MSIProcessorConfig";
			this.Size = new System.Drawing.Size(464, 208);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary />
		protected override void OnPaint(PaintEventArgs pe)
		{
			// TODO: Add custom paint code here

			// Calling the base class OnPaint
			base.OnPaint(pe);
		}

		/// Gets or sets the wrapped item to edit
		public override ProcessorItem Item
		{
			get
			{
				if ( item == null )
				{
					item = new MSIProcessorItem();
				}
				return item;
			}
			set
			{
				item = value as MSIProcessorItem;
				if ( item == null )
				{
					throw new ManifestEditorException( Resource.ResourceManager[ Resource.MessageKey.InvalidProcessorItemForEdition, value.GetType().FullName, this.GetType().FullName ] );
				}

				cboInstallType.SelectedItem = item.InstallType;
				cboUILevel.SelectedItem = item.UILevel;
				txtPackagePath.Text = item.PackagePath;
				txtProductCode.Text = item.ProductCode;
				txtPropertiesValues.Text = item.Properties;
			}
		}

		/// Called when the user accepts the edition changes
		public override void OnOK()
		{
			base.OnOK ();

			if ( item == null )
			{
				item = new MSIProcessorItem();
			}

			string aux = cboInstallType.SelectedItem as string;
			if ( aux != null && aux.Length > 0 )
			{
				item.InstallType = aux;
			}
			aux = cboUILevel.SelectedItem as string;
			if ( aux != null && aux.Length > 0 )
			{
				item.UILevel = aux;
			}
			if ( txtPackagePath.Text.Length > 0 )
			{
				item.PackagePath = txtPackagePath.Text;
			}
			if ( txtPropertiesValues.Text.Length > 0 )
			{
				item.Properties = txtPropertiesValues.Text;
			}

			if ( txtProductCode.Text.Length > 0 )
			{
				item.ProductCode = txtProductCode.Text;
			}
		}

	}
}
