//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ConfigureProcessor.cs
//
// Contains the implementation of the ConfigureProcessor base class.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System;
using System.Configuration;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// The base control to use when creating processor editors
	/// </summary>
	public class ConfigureProcessor : Form, IProcessorConfig
	{
		private System.Windows.Forms.Panel pnlContainer;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;

		private ProcessorEditorControl processorEditor;
		private System.Windows.Forms.Label lblProcessorName;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ConfigureProcessor()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="item"></param>
		public ConfigureProcessor( ProcessorItem item ) : this()
		{
			Type processorType = Type.GetType( item.TypeName );
			NameValueCollection config = (NameValueCollection)ConfigurationSettings.GetConfig( EditorForm.ProcessorEditorsConfigSection );
			string controlTypeName = config[ processorType.FullName ];

			if ( controlTypeName == null )
			{
				throw new ManifestEditorException(
					Resource.ResourceManager[ Resource.MessageKey.EditorControlNotConfigured, processorType.FullName ] );
			}

			lblProcessorName.Text = item.Name;

			processorEditor = ProcessorEditorFactory.Create( controlTypeName );
			processorEditor.Item = item;

			// Set initial sizes

			this.Width += processorEditor.Width - pnlContainer.Width;
			this.Height += processorEditor.Height - pnlContainer.Height;

			processorEditor.Width = pnlContainer.Width;
			processorEditor.Height = pnlContainer.Height;



			processorEditor.Anchor = AnchorStyles.Bottom |
				AnchorStyles.Left |
				AnchorStyles.Right |
				AnchorStyles.Top;
			pnlContainer.Controls.Add( processorEditor );
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlContainer = new System.Windows.Forms.Panel();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.lblProcessorName = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// pnlContainer
			// 
			this.pnlContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.pnlContainer.Location = new System.Drawing.Point(0, 32);
			this.pnlContainer.Name = "pnlContainer";
			this.pnlContainer.Size = new System.Drawing.Size(408, 272);
			this.pnlContainer.TabIndex = 0;
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(328, 312);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 1;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOK.Location = new System.Drawing.Point(240, 312);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 2;
			this.btnOK.Text = "&OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// lblProcessorName
			// 
			this.lblProcessorName.Location = new System.Drawing.Point(8, 8);
			this.lblProcessorName.Name = "lblProcessorName";
			this.lblProcessorName.Size = new System.Drawing.Size(368, 16);
			this.lblProcessorName.TabIndex = 3;
			this.lblProcessorName.Text = "Processor Name";
			// 
			// ConfigureProcessor
			// 
			this.AcceptButton = this.btnOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(408, 366);
			this.Controls.Add(this.lblProcessorName);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.pnlContainer);
			this.Name = "ConfigureProcessor";
			this.Text = "Edit Processor Configuration";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			processorEditor.OnOK();
			Close();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			processorEditor.OnCancel();
		}

		#region IProcessorConfig Members

		/// <summary>
		/// Gets or sets the edited ProcessorItem
		/// </summary>
		public ProcessorItem Item
		{
			get
			{
				return processorEditor.Item;
			}
			set
			{
				processorEditor.Item = value;
			}
		}

		/// <summary>
		/// Called when the user clicks the OK button
		/// </summary>
		public void OnOK()
		{
		}

		/// <summary>
		/// Called when the user clicks the Cancel button
		/// </summary>
		public void OnCancel()
		{
		}

		#endregion
	}
}
