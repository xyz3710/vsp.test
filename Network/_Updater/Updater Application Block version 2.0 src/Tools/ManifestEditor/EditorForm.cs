//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// EditorForm.cs
//
// Contains the implementation of the editor form.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.Xml;
using System.Xml.Schema;

using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Defines the Editor form and process user input to call the manifest editor library.
	/// </summary>
	public class EditorForm : System.Windows.Forms.Form
	{

		internal static readonly string ProcessorEditorsConfigSection = "processorEditors";

		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtManifestId;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Button btnGenerateManifestID;
		private System.Windows.Forms.Button btnRemoveIncludedManifest;
		private System.Windows.Forms.Button btnAddIncludedManifest;
		private System.Windows.Forms.TextBox txtIncludedManifestId;
		private System.Windows.Forms.TextBox txtIncludedManifestLocation;
		private System.Windows.Forms.ListBox lstIncludedManifests;
		private System.Windows.Forms.TextBox txtManifestDescription;
		private System.Windows.Forms.CheckBox chkManifestMandatory;
		private System.Windows.Forms.Button btnSaveManifest;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnOpenManifest;
		private System.Windows.Forms.Button btnNewManifest;
		private System.Windows.Forms.Button btnViewManifest;
		private System.Windows.Forms.StatusBar sbMessageBar;
		private System.Windows.Forms.StatusBarPanel sbpDefaultPanel;
		private System.Windows.Forms.TextBox txtApplicationLocation;
		private System.Windows.Forms.TextBox txtApplicationId;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnRemoveFile;
		private System.Windows.Forms.Button btnAddFile;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtEntryPointParameters;
		private System.Windows.Forms.TextBox txtEntryPointFile;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDlg;
		private System.Windows.Forms.OpenFileDialog openFileDlg;
		private System.Windows.Forms.SaveFileDialog saveFileDlg;
		private System.Windows.Forms.Button btnBrowseForSourceFolder;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Button btnConfigureProcessor;
		private System.Windows.Forms.Button btnRemoveProcessor;
		private System.Windows.Forms.Button btnAddProcessor;
		private System.Windows.Forms.ListBox lstProcessors;
		private System.Windows.Forms.ComboBox cbProcessorType;
		private System.Windows.Forms.TextBox txtProcessorName;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.ToolTip tooltips;
		private System.Windows.Forms.Button btnBrowseForCryptoConfig;
		private ManifestEditor editor = new ManifestEditor();
		private System.Windows.Forms.ComboBox cbHashingAlgorithms;
		private System.Windows.Forms.CheckBox chkUseHashing;
		private System.Windows.Forms.CheckedListBox lstFiles;

		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.TextBox txtDownloaderName;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.CheckBox chkIncludeDownloader;
		private System.Windows.Forms.Button cmdValidate;
		private System.Windows.Forms.TextBox txtFilesURI;
		private System.Windows.Forms.TextBox txtLocalFilesBase;
		private bool updatingUI = false;

		/// <summary>
		/// Default constructor.
		/// </summary>
		public EditorForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
	
			txtLocalFilesBase.Text = editor.LocalFilesBase;
			cbProcessorType.DataSource = editor.Processors;

			UpdateUI();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(EditorForm));
			this.btnSaveManifest = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.btnRemoveIncludedManifest = new System.Windows.Forms.Button();
			this.btnAddIncludedManifest = new System.Windows.Forms.Button();
			this.label14 = new System.Windows.Forms.Label();
			this.txtIncludedManifestId = new System.Windows.Forms.TextBox();
			this.txtIncludedManifestLocation = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.lstIncludedManifests = new System.Windows.Forms.ListBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnGenerateManifestID = new System.Windows.Forms.Button();
			this.txtManifestDescription = new System.Windows.Forms.TextBox();
			this.txtManifestId = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.chkManifestMandatory = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.txtDownloaderName = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.chkIncludeDownloader = new System.Windows.Forms.CheckBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txtEntryPointParameters = new System.Windows.Forms.TextBox();
			this.txtEntryPointFile = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.lstFiles = new System.Windows.Forms.CheckedListBox();
			this.cbHashingAlgorithms = new System.Windows.Forms.ComboBox();
			this.btnBrowseForCryptoConfig = new System.Windows.Forms.Button();
			this.chkUseHashing = new System.Windows.Forms.CheckBox();
			this.btnBrowseForSourceFolder = new System.Windows.Forms.Button();
			this.txtLocalFilesBase = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btnRemoveFile = new System.Windows.Forms.Button();
			this.btnAddFile = new System.Windows.Forms.Button();
			this.label9 = new System.Windows.Forms.Label();
			this.txtFilesURI = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.txtApplicationLocation = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txtApplicationId = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.label16 = new System.Windows.Forms.Label();
			this.txtProcessorName = new System.Windows.Forms.TextBox();
			this.btnConfigureProcessor = new System.Windows.Forms.Button();
			this.btnRemoveProcessor = new System.Windows.Forms.Button();
			this.btnAddProcessor = new System.Windows.Forms.Button();
			this.label11 = new System.Windows.Forms.Label();
			this.lstProcessors = new System.Windows.Forms.ListBox();
			this.label10 = new System.Windows.Forms.Label();
			this.cbProcessorType = new System.Windows.Forms.ComboBox();
			this.btnOpenManifest = new System.Windows.Forms.Button();
			this.btnNewManifest = new System.Windows.Forms.Button();
			this.btnViewManifest = new System.Windows.Forms.Button();
			this.sbMessageBar = new System.Windows.Forms.StatusBar();
			this.sbpDefaultPanel = new System.Windows.Forms.StatusBarPanel();
			this.folderBrowserDlg = new System.Windows.Forms.FolderBrowserDialog();
			this.openFileDlg = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDlg = new System.Windows.Forms.SaveFileDialog();
			this.tooltips = new System.Windows.Forms.ToolTip(this.components);
			this.cmdValidate = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.sbpDefaultPanel)).BeginInit();
			this.SuspendLayout();
			// 
			// btnSaveManifest
			// 
			this.btnSaveManifest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSaveManifest.Location = new System.Drawing.Point(224, 344);
			this.btnSaveManifest.Name = "btnSaveManifest";
			this.btnSaveManifest.Size = new System.Drawing.Size(104, 23);
			this.btnSaveManifest.TabIndex = 12;
			this.btnSaveManifest.Text = "Save";
			this.tooltips.SetToolTip(this.btnSaveManifest, "Save the manifest to a file.");
			this.btnSaveManifest.Click += new System.EventHandler(this.btnSaveManifest_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Location = new System.Drawing.Point(576, 344);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(96, 24);
			this.btnClose.TabIndex = 13;
			this.btnClose.Text = "Close";
			this.tooltips.SetToolTip(this.btnClose, "Quit this application");
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Location = new System.Drawing.Point(8, 8);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(664, 328);
			this.tabControl1.TabIndex = 14;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.groupBox6);
			this.tabPage1.Controls.Add(this.groupBox1);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(656, 302);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Manifest Properties";
			// 
			// groupBox6
			// 
			this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox6.Controls.Add(this.btnRemoveIncludedManifest);
			this.groupBox6.Controls.Add(this.btnAddIncludedManifest);
			this.groupBox6.Controls.Add(this.label14);
			this.groupBox6.Controls.Add(this.txtIncludedManifestId);
			this.groupBox6.Controls.Add(this.txtIncludedManifestLocation);
			this.groupBox6.Controls.Add(this.label13);
			this.groupBox6.Controls.Add(this.label12);
			this.groupBox6.Controls.Add(this.lstIncludedManifests);
			this.groupBox6.Location = new System.Drawing.Point(0, 144);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(648, 144);
			this.groupBox6.TabIndex = 13;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Included Manifests";
			// 
			// btnRemoveIncludedManifest
			// 
			this.btnRemoveIncludedManifest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnRemoveIncludedManifest.Enabled = false;
			this.btnRemoveIncludedManifest.Location = new System.Drawing.Point(552, 112);
			this.btnRemoveIncludedManifest.Name = "btnRemoveIncludedManifest";
			this.btnRemoveIncludedManifest.TabIndex = 7;
			this.btnRemoveIncludedManifest.Text = "Remove";
			this.btnRemoveIncludedManifest.Click += new System.EventHandler(this.btnRemoveIncludedManifest_Click);
			// 
			// btnAddIncludedManifest
			// 
			this.btnAddIncludedManifest.Enabled = false;
			this.btnAddIncludedManifest.Location = new System.Drawing.Point(208, 96);
			this.btnAddIncludedManifest.Name = "btnAddIncludedManifest";
			this.btnAddIncludedManifest.TabIndex = 6;
			this.btnAddIncludedManifest.Text = "Add";
			this.btnAddIncludedManifest.Click += new System.EventHandler(this.btnAddIncludedManifest_Click);
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(16, 64);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(64, 23);
			this.label14.TabIndex = 5;
			this.label14.Text = "Manifest Id";
			// 
			// txtIncludedManifestId
			// 
			this.txtIncludedManifestId.Location = new System.Drawing.Point(88, 64);
			this.txtIncludedManifestId.Name = "txtIncludedManifestId";
			this.txtIncludedManifestId.Size = new System.Drawing.Size(192, 20);
			this.txtIncludedManifestId.TabIndex = 4;
			this.txtIncludedManifestId.Text = "";
			this.tooltips.SetToolTip(this.txtIncludedManifestId, "The id of the included manifest.");
			this.txtIncludedManifestId.TextChanged += new System.EventHandler(this.txtIncludedManifestId_TextChanged);
			// 
			// txtIncludedManifestLocation
			// 
			this.txtIncludedManifestLocation.Location = new System.Drawing.Point(88, 32);
			this.txtIncludedManifestLocation.Name = "txtIncludedManifestLocation";
			this.txtIncludedManifestLocation.Size = new System.Drawing.Size(192, 20);
			this.txtIncludedManifestLocation.TabIndex = 3;
			this.txtIncludedManifestLocation.Text = "";
			this.tooltips.SetToolTip(this.txtIncludedManifestLocation, "An URI referencing an included manifest");
			this.txtIncludedManifestLocation.TextChanged += new System.EventHandler(this.txtIncludedManifestLocation_TextChanged);
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(16, 32);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(64, 23);
			this.label13.TabIndex = 2;
			this.label13.Text = "Location";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(296, 16);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(120, 16);
			this.label12.TabIndex = 1;
			this.label12.Text = "Included Manifests";
			// 
			// lstIncludedManifests
			// 
			this.lstIncludedManifests.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lstIncludedManifests.ColumnWidth = 100;
			this.lstIncludedManifests.DisplayMember = "Description";
			this.lstIncludedManifests.Location = new System.Drawing.Point(296, 40);
			this.lstIncludedManifests.Name = "lstIncludedManifests";
			this.lstIncludedManifests.Size = new System.Drawing.Size(248, 95);
			this.lstIncludedManifests.TabIndex = 0;
			this.tooltips.SetToolTip(this.lstIncludedManifests, "The list of included manifests.");
			this.lstIncludedManifests.SelectedIndexChanged += new System.EventHandler(this.lstIncludedManifests_SelectedIndexChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.btnGenerateManifestID);
			this.groupBox1.Controls.Add(this.txtManifestDescription);
			this.groupBox1.Controls.Add(this.txtManifestId);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.chkManifestMandatory);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(4, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(644, 128);
			this.groupBox1.TabIndex = 12;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Manifest Properties";
			// 
			// btnGenerateManifestID
			// 
			this.btnGenerateManifestID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnGenerateManifestID.Location = new System.Drawing.Point(384, 24);
			this.btnGenerateManifestID.Name = "btnGenerateManifestID";
			this.btnGenerateManifestID.TabIndex = 8;
			this.btnGenerateManifestID.Text = "Generate";
			this.tooltips.SetToolTip(this.btnGenerateManifestID, "Generate a new GUID for the manifest Id.");
			this.btnGenerateManifestID.Click += new System.EventHandler(this.btnGenerateManifestID_Click);
			// 
			// txtManifestDescription
			// 
			this.txtManifestDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtManifestDescription.BackColor = System.Drawing.Color.LightGoldenrodYellow;
			this.txtManifestDescription.Location = new System.Drawing.Point(104, 64);
			this.txtManifestDescription.Multiline = true;
			this.txtManifestDescription.Name = "txtManifestDescription";
			this.txtManifestDescription.Size = new System.Drawing.Size(528, 56);
			this.txtManifestDescription.TabIndex = 6;
			this.txtManifestDescription.Text = "";
			this.tooltips.SetToolTip(this.txtManifestDescription, "Description for the manifest.");
			this.txtManifestDescription.TextChanged += new System.EventHandler(this.txtManifestDescription_TextChanged);
			// 
			// txtManifestId
			// 
			this.txtManifestId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtManifestId.BackColor = System.Drawing.Color.LightGoldenrodYellow;
			this.txtManifestId.Location = new System.Drawing.Point(104, 24);
			this.txtManifestId.Name = "txtManifestId";
			this.txtManifestId.Size = new System.Drawing.Size(272, 20);
			this.txtManifestId.TabIndex = 0;
			this.txtManifestId.Text = "";
			this.tooltips.SetToolTip(this.txtManifestId, "The unique id (GUID) identifying the manifest.");
			this.txtManifestId.TextChanged += new System.EventHandler(this.txtManifestId_TextChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(24, 64);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 23);
			this.label3.TabIndex = 7;
			this.label3.Text = "Description";
			// 
			// chkManifestMandatory
			// 
			this.chkManifestMandatory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.chkManifestMandatory.Location = new System.Drawing.Point(504, 24);
			this.chkManifestMandatory.Name = "chkManifestMandatory";
			this.chkManifestMandatory.TabIndex = 5;
			this.chkManifestMandatory.Text = "Mandatory";
			this.tooltips.SetToolTip(this.chkManifestMandatory, "Indicates whether the update is mandatory or not.");
			this.chkManifestMandatory.CheckedChanged += new System.EventHandler(this.chkManifestMandatory_CheckedChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "ManifestId";
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.txtDownloaderName);
			this.tabPage4.Controls.Add(this.label20);
			this.tabPage4.Controls.Add(this.chkIncludeDownloader);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Size = new System.Drawing.Size(656, 302);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "Downloader";
			// 
			// txtDownloaderName
			// 
			this.txtDownloaderName.Location = new System.Drawing.Point(168, 48);
			this.txtDownloaderName.Name = "txtDownloaderName";
			this.txtDownloaderName.Size = new System.Drawing.Size(232, 20);
			this.txtDownloaderName.TabIndex = 10;
			this.txtDownloaderName.Text = "BitsDownloader";
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(64, 48);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(100, 16);
			this.label20.TabIndex = 9;
			this.label20.Text = "Downloader Name";
			// 
			// chkIncludeDownloader
			// 
			this.chkIncludeDownloader.Location = new System.Drawing.Point(16, 8);
			this.chkIncludeDownloader.Name = "chkIncludeDownloader";
			this.chkIncludeDownloader.Size = new System.Drawing.Size(208, 24);
			this.chkIncludeDownloader.TabIndex = 2;
			this.chkIncludeDownloader.Text = "Use this downloader name";
			this.chkIncludeDownloader.CheckedChanged += new System.EventHandler(this.chkIncludeDownloader_CheckedChanged);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.groupBox3);
			this.tabPage2.Controls.Add(this.groupBox4);
			this.tabPage2.Controls.Add(this.groupBox2);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(656, 302);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Application Properties";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.txtEntryPointParameters);
			this.groupBox3.Controls.Add(this.txtEntryPointFile);
			this.groupBox3.Controls.Add(this.label8);
			this.groupBox3.Location = new System.Drawing.Point(312, 8);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(336, 88);
			this.groupBox3.TabIndex = 12;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Entry Point";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(16, 56);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(64, 16);
			this.label6.TabIndex = 4;
			this.label6.Text = "Parameters";
			// 
			// txtEntryPointParameters
			// 
			this.txtEntryPointParameters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtEntryPointParameters.Location = new System.Drawing.Point(80, 56);
			this.txtEntryPointParameters.Name = "txtEntryPointParameters";
			this.txtEntryPointParameters.Size = new System.Drawing.Size(240, 20);
			this.txtEntryPointParameters.TabIndex = 5;
			this.txtEntryPointParameters.Text = "";
			this.tooltips.SetToolTip(this.txtEntryPointParameters, "A list of parameters to be passed to the application upon launch.");
			// 
			// txtEntryPointFile
			// 
			this.txtEntryPointFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtEntryPointFile.Location = new System.Drawing.Point(80, 24);
			this.txtEntryPointFile.Name = "txtEntryPointFile";
			this.txtEntryPointFile.Size = new System.Drawing.Size(240, 20);
			this.txtEntryPointFile.TabIndex = 3;
			this.txtEntryPointFile.Text = "";
			this.tooltips.SetToolTip(this.txtEntryPointFile, "The name of the application main executable");
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(16, 24);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(32, 16);
			this.label8.TabIndex = 8;
			this.label8.Text = "File";
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox4.Controls.Add(this.lstFiles);
			this.groupBox4.Controls.Add(this.cbHashingAlgorithms);
			this.groupBox4.Controls.Add(this.btnBrowseForCryptoConfig);
			this.groupBox4.Controls.Add(this.chkUseHashing);
			this.groupBox4.Controls.Add(this.btnBrowseForSourceFolder);
			this.groupBox4.Controls.Add(this.txtLocalFilesBase);
			this.groupBox4.Controls.Add(this.label2);
			this.groupBox4.Controls.Add(this.btnRemoveFile);
			this.groupBox4.Controls.Add(this.btnAddFile);
			this.groupBox4.Controls.Add(this.label9);
			this.groupBox4.Controls.Add(this.txtFilesURI);
			this.groupBox4.Controls.Add(this.label5);
			this.groupBox4.Location = new System.Drawing.Point(8, 104);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(640, 192);
			this.groupBox4.TabIndex = 11;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Files";
			// 
			// lstFiles
			// 
			this.lstFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lstFiles.HorizontalScrollbar = true;
			this.lstFiles.Location = new System.Drawing.Point(96, 88);
			this.lstFiles.Name = "lstFiles";
			this.lstFiles.Size = new System.Drawing.Size(440, 94);
			this.lstFiles.TabIndex = 16;
			this.tooltips.SetToolTip(this.lstFiles, "The list of files included in the manifest. Check the files you want to be transi" +
				"ent i.e. non-permanent.");
			this.lstFiles.SelectedIndexChanged += new System.EventHandler(this.lstFiles_SelectedIndexChanged);
			this.lstFiles.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstFiles_ItemCheck);
			// 
			// cbHashingAlgorithms
			// 
			this.cbHashingAlgorithms.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cbHashingAlgorithms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbHashingAlgorithms.Location = new System.Drawing.Point(392, 24);
			this.cbHashingAlgorithms.Name = "cbHashingAlgorithms";
			this.cbHashingAlgorithms.Size = new System.Drawing.Size(144, 20);
			this.cbHashingAlgorithms.TabIndex = 15;
			this.tooltips.SetToolTip(this.cbHashingAlgorithms, "The hashing algorihtm used to compute the file hashes.");
			this.cbHashingAlgorithms.SelectedIndexChanged += new System.EventHandler(this.cbHashingAlgorithms_SelectedIndexChanged);
			// 
			// btnBrowseForCryptoConfig
			// 
			this.btnBrowseForCryptoConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnBrowseForCryptoConfig.Location = new System.Drawing.Point(552, 24);
			this.btnBrowseForCryptoConfig.Name = "btnBrowseForCryptoConfig";
			this.btnBrowseForCryptoConfig.TabIndex = 14;
			this.btnBrowseForCryptoConfig.Text = "Browse";
			this.btnBrowseForCryptoConfig.Click += new System.EventHandler(this.btnBrowseForCryptoConfig_Click);
			// 
			// chkUseHashing
			// 
			this.chkUseHashing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.chkUseHashing.Location = new System.Drawing.Point(296, 24);
			this.chkUseHashing.Name = "chkUseHashing";
			this.chkUseHashing.Size = new System.Drawing.Size(88, 16);
			this.chkUseHashing.TabIndex = 12;
			this.chkUseHashing.Text = "Use hashing";
			this.tooltips.SetToolTip(this.chkUseHashing, "Indicates whether the files will be validated using a hashing algorithm.");
			this.chkUseHashing.CheckedChanged += new System.EventHandler(this.chkUseHashing_CheckedChanged);
			// 
			// btnBrowseForSourceFolder
			// 
			this.btnBrowseForSourceFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnBrowseForSourceFolder.Location = new System.Drawing.Point(552, 56);
			this.btnBrowseForSourceFolder.Name = "btnBrowseForSourceFolder";
			this.btnBrowseForSourceFolder.TabIndex = 9;
			this.btnBrowseForSourceFolder.Text = "Browse";
			this.btnBrowseForSourceFolder.Click += new System.EventHandler(this.btnBrowseForSourceFolder_Click);
			// 
			// txtLocalFilesBase
			// 
			this.txtLocalFilesBase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtLocalFilesBase.Location = new System.Drawing.Point(96, 56);
			this.txtLocalFilesBase.Name = "txtLocalFilesBase";
			this.txtLocalFilesBase.Size = new System.Drawing.Size(440, 20);
			this.txtLocalFilesBase.TabIndex = 8;
			this.txtLocalFilesBase.Text = "";
			this.tooltips.SetToolTip(this.txtLocalFilesBase, "Root folder from where the files are analyzed. Consider this folder as the path f" +
				"or the virtual directory defined in Files location");
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(80, 23);
			this.label2.TabIndex = 7;
			this.label2.Text = "Source Folder";
			// 
			// btnRemoveFile
			// 
			this.btnRemoveFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnRemoveFile.Enabled = false;
			this.btnRemoveFile.Location = new System.Drawing.Point(552, 160);
			this.btnRemoveFile.Name = "btnRemoveFile";
			this.btnRemoveFile.TabIndex = 6;
			this.btnRemoveFile.Text = "Remove";
			this.btnRemoveFile.Click += new System.EventHandler(this.btnRemoveFile_Click);
			// 
			// btnAddFile
			// 
			this.btnAddFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAddFile.Location = new System.Drawing.Point(552, 128);
			this.btnAddFile.Name = "btnAddFile";
			this.btnAddFile.TabIndex = 5;
			this.btnAddFile.Text = "Add";
			this.btnAddFile.Click += new System.EventHandler(this.btnAddFile_Click);
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(16, 88);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(64, 16);
			this.label9.TabIndex = 4;
			this.label9.Text = "Files";
			// 
			// txtFilesURI
			// 
			this.txtFilesURI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtFilesURI.Location = new System.Drawing.Point(96, 24);
			this.txtFilesURI.Name = "txtFilesURI";
			this.txtFilesURI.Size = new System.Drawing.Size(192, 20);
			this.txtFilesURI.TabIndex = 1;
			this.txtFilesURI.Text = "";
			this.tooltips.SetToolTip(this.txtFilesURI, "Base path from where the files are downloaded");
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 24);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 23);
			this.label5.TabIndex = 0;
			this.label5.Text = "Files URI";
			this.tooltips.SetToolTip(this.label5, "This is the URI where the files will be located for download.");
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.txtApplicationLocation);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.txtApplicationId);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Location = new System.Drawing.Point(4, 8);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(300, 88);
			this.groupBox2.TabIndex = 10;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Application Properties";
			// 
			// txtApplicationLocation
			// 
			this.txtApplicationLocation.BackColor = System.Drawing.Color.LightGoldenrodYellow;
			this.txtApplicationLocation.Location = new System.Drawing.Point(112, 56);
			this.txtApplicationLocation.Name = "txtApplicationLocation";
			this.txtApplicationLocation.Size = new System.Drawing.Size(176, 20);
			this.txtApplicationLocation.TabIndex = 7;
			this.txtApplicationLocation.Text = "";
			this.tooltips.SetToolTip(this.txtApplicationLocation, "A hint for the application location on the client computer.");
			this.txtApplicationLocation.TextChanged += new System.EventHandler(this.txtApplicationLocation_TextChanged);
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(40, 56);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(64, 16);
			this.label7.TabIndex = 6;
			this.label7.Text = "Location";
			// 
			// txtApplicationId
			// 
			this.txtApplicationId.BackColor = System.Drawing.Color.LightGoldenrodYellow;
			this.txtApplicationId.Location = new System.Drawing.Point(112, 24);
			this.txtApplicationId.Name = "txtApplicationId";
			this.txtApplicationId.Size = new System.Drawing.Size(176, 20);
			this.txtApplicationId.TabIndex = 1;
			this.txtApplicationId.Text = "";
			this.tooltips.SetToolTip(this.txtApplicationId, "An identification for the application.");
			this.txtApplicationId.TextChanged += new System.EventHandler(this.txtApplicationId_TextChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(24, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(80, 16);
			this.label4.TabIndex = 0;
			this.label4.Text = "Application Id";
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.label16);
			this.tabPage3.Controls.Add(this.txtProcessorName);
			this.tabPage3.Controls.Add(this.btnConfigureProcessor);
			this.tabPage3.Controls.Add(this.btnRemoveProcessor);
			this.tabPage3.Controls.Add(this.btnAddProcessor);
			this.tabPage3.Controls.Add(this.label11);
			this.tabPage3.Controls.Add(this.lstProcessors);
			this.tabPage3.Controls.Add(this.label10);
			this.tabPage3.Controls.Add(this.cbProcessorType);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(656, 302);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Activation Process";
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(16, 48);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(88, 16);
			this.label16.TabIndex = 20;
			this.label16.Text = "Processor Type";
			// 
			// txtProcessorName
			// 
			this.txtProcessorName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtProcessorName.Location = new System.Drawing.Point(112, 16);
			this.txtProcessorName.Name = "txtProcessorName";
			this.txtProcessorName.Size = new System.Drawing.Size(360, 20);
			this.txtProcessorName.TabIndex = 19;
			this.txtProcessorName.Text = "";
			this.tooltips.SetToolTip(this.txtProcessorName, "Sets the processor name");
			this.txtProcessorName.TextChanged += new System.EventHandler(this.txtProcessorName_TextChanged);
			// 
			// btnConfigureProcessor
			// 
			this.btnConfigureProcessor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnConfigureProcessor.Enabled = false;
			this.btnConfigureProcessor.Location = new System.Drawing.Point(568, 96);
			this.btnConfigureProcessor.Name = "btnConfigureProcessor";
			this.btnConfigureProcessor.TabIndex = 18;
			this.btnConfigureProcessor.Text = "Configure";
			this.btnConfigureProcessor.Click += new System.EventHandler(this.btnConfigureProcessor_Click);
			// 
			// btnRemoveProcessor
			// 
			this.btnRemoveProcessor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnRemoveProcessor.Enabled = false;
			this.btnRemoveProcessor.Location = new System.Drawing.Point(568, 128);
			this.btnRemoveProcessor.Name = "btnRemoveProcessor";
			this.btnRemoveProcessor.TabIndex = 17;
			this.btnRemoveProcessor.Text = "Remove";
			this.btnRemoveProcessor.Click += new System.EventHandler(this.btnRemoveProcessor_Click);
			// 
			// btnAddProcessor
			// 
			this.btnAddProcessor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAddProcessor.Enabled = false;
			this.btnAddProcessor.Location = new System.Drawing.Point(488, 48);
			this.btnAddProcessor.Name = "btnAddProcessor";
			this.btnAddProcessor.TabIndex = 16;
			this.btnAddProcessor.Text = "Add";
			this.btnAddProcessor.Click += new System.EventHandler(this.btnAddProcessor_Click);
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(16, 80);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(136, 16);
			this.label11.TabIndex = 15;
			this.label11.Text = "Processors List";
			// 
			// lstProcessors
			// 
			this.lstProcessors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lstProcessors.DisplayMember = "Description";
			this.lstProcessors.HorizontalScrollbar = true;
			this.lstProcessors.Location = new System.Drawing.Point(16, 96);
			this.lstProcessors.Name = "lstProcessors";
			this.lstProcessors.Size = new System.Drawing.Size(544, 199);
			this.lstProcessors.TabIndex = 14;
			this.tooltips.SetToolTip(this.lstProcessors, "The list of processors to execute as part of the activation process for the appli" +
				"cation");
			this.lstProcessors.SelectedIndexChanged += new System.EventHandler(this.lstProcessors_SelectedIndexChanged);
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(16, 16);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(88, 16);
			this.label10.TabIndex = 13;
			this.label10.Text = "Processor Name";
			// 
			// cbProcessorType
			// 
			this.cbProcessorType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.cbProcessorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbProcessorType.Items.AddRange(new object[] {
																 "ApplicationDeploy",
																 "FileCopy",
																 "FileDelete",
																 "FolderCopy",
																 "FolderDelete",
																 "GacUtil",
																 "InstallUtil",
																 "MSI",
																 "Uncompress",
																 "ValidateHash",
																 "WaitForApplication"});
			this.cbProcessorType.Location = new System.Drawing.Point(112, 48);
			this.cbProcessorType.Name = "cbProcessorType";
			this.cbProcessorType.Size = new System.Drawing.Size(360, 20);
			this.cbProcessorType.TabIndex = 12;
			this.tooltips.SetToolTip(this.cbProcessorType, "The processor TypeName, it can be any of the predefined processors, or a type nam" +
				"e available on the client computer.");
			this.cbProcessorType.SelectedIndexChanged += new System.EventHandler(this.cbProcessorType_SelectedIndexChanged);
			// 
			// btnOpenManifest
			// 
			this.btnOpenManifest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnOpenManifest.Location = new System.Drawing.Point(120, 344);
			this.btnOpenManifest.Name = "btnOpenManifest";
			this.btnOpenManifest.Size = new System.Drawing.Size(96, 23);
			this.btnOpenManifest.TabIndex = 15;
			this.btnOpenManifest.Text = "Open";
			this.tooltips.SetToolTip(this.btnOpenManifest, "Open an existing manifest for edition");
			this.btnOpenManifest.Click += new System.EventHandler(this.btnOpenManifest_Click);
			// 
			// btnNewManifest
			// 
			this.btnNewManifest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnNewManifest.Location = new System.Drawing.Point(8, 344);
			this.btnNewManifest.Name = "btnNewManifest";
			this.btnNewManifest.Size = new System.Drawing.Size(104, 23);
			this.btnNewManifest.TabIndex = 16;
			this.btnNewManifest.Text = "New";
			this.tooltips.SetToolTip(this.btnNewManifest, "Creates a new empty manifest");
			this.btnNewManifest.Click += new System.EventHandler(this.btnNewManifest_Click);
			// 
			// btnViewManifest
			// 
			this.btnViewManifest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnViewManifest.Location = new System.Drawing.Point(464, 344);
			this.btnViewManifest.Name = "btnViewManifest";
			this.btnViewManifest.Size = new System.Drawing.Size(104, 23);
			this.btnViewManifest.TabIndex = 17;
			this.btnViewManifest.Text = "View";
			this.tooltips.SetToolTip(this.btnViewManifest, "Preview the manifest XML");
			this.btnViewManifest.Click += new System.EventHandler(this.btnViewManifest_Click);
			// 
			// sbMessageBar
			// 
			this.sbMessageBar.Location = new System.Drawing.Point(0, 376);
			this.sbMessageBar.Name = "sbMessageBar";
			this.sbMessageBar.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
																							this.sbpDefaultPanel});
			this.sbMessageBar.ShowPanels = true;
			this.sbMessageBar.Size = new System.Drawing.Size(680, 22);
			this.sbMessageBar.TabIndex = 18;
			// 
			// sbpDefaultPanel
			// 
			this.sbpDefaultPanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
			this.sbpDefaultPanel.Width = 664;
			// 
			// folderBrowserDlg
			// 
			this.folderBrowserDlg.ShowNewFolderButton = false;
			// 
			// cmdValidate
			// 
			this.cmdValidate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cmdValidate.Location = new System.Drawing.Point(352, 344);
			this.cmdValidate.Name = "cmdValidate";
			this.cmdValidate.Size = new System.Drawing.Size(104, 23);
			this.cmdValidate.TabIndex = 19;
			this.cmdValidate.Text = "Validate";
			this.tooltips.SetToolTip(this.cmdValidate, "Save the manifest to a file.");
			this.cmdValidate.Click += new System.EventHandler(this.cmdValidate_Click);
			// 
			// EditorForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(680, 398);
			this.Controls.Add(this.cmdValidate);
			this.Controls.Add(this.sbMessageBar);
			this.Controls.Add(this.btnViewManifest);
			this.Controls.Add(this.btnNewManifest);
			this.Controls.Add(this.btnOpenManifest);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnSaveManifest);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "EditorForm";
			this.Text = "Updater Manifest Tool";
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.sbpDefaultPanel)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new EditorForm());
		}

		/// <summary>
		/// Flush the current state to the manifest editor.
		/// </summary>
		private void UpdateManifest()
		{
			if ( txtManifestId.Text.Length > 0 )
			{
				editor.ManifestId = txtManifestId.Text;
			}

			if ( txtManifestDescription.Text.Length > 0 )
			{
				editor.Description = txtManifestDescription.Text;
			}

			if ( txtApplicationId.Text.Length > 0 )
			{
				editor.ApplicationId = txtApplicationId.Text;
			}

			if ( txtApplicationLocation.Text.Length > 0 )
			{
				editor.ApplicationLocation = txtApplicationLocation.Text;
			}

			if ( txtEntryPointFile.Text.Length > 0 )
			{
				editor.ApplicationEntryPointFile = txtEntryPointFile.Text;
			}

			if ( txtEntryPointParameters.Text.Length > 0 )
			{
				editor.ApplicationEntryPointParams = txtEntryPointParameters.Text;
			}

			if ( txtLocalFilesBase.Text.Length > 0 && lstFiles.Items.Count > 0 )
			{
				try
				{
					editor.LocalFilesBase = txtLocalFilesBase.Text;
				}
				catch {}
			}

			editor.FilesBase = txtFilesURI.Text;

			try
			{
				if ( cbHashingAlgorithms.SelectedIndex >= 0 )
				{
					editor.HashProviderName = cbHashingAlgorithms.SelectedItem as String;
				}
				editor.ComputeHashes();
			}
			catch(ManifestEditorException ex)
			{
				ProcessException( ex );
			}

//			if ( chkIncludeDownloader.Checked && txtDownloaderName.Text.Length > 0 )
//			{
			editor.IncludeDownloader = chkIncludeDownloader.Checked;
			editor.DownloaderName = txtDownloaderName.Text;
//			}
		}

		/// <summary>
		/// Generates a new manifest id.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnGenerateManifestID_Click(object sender, System.EventArgs e)
		{
			txtManifestId.Text = Guid.NewGuid().ToString("B");
		}

		/// <summary>
		/// Add an included manifest to the current manifest.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnAddIncludedManifest_Click(object sender, System.EventArgs e)
		{
			IncludedManifestItem item = new IncludedManifestItem(
				txtIncludedManifestId.Text,
				txtIncludedManifestLocation.Text );

			try
			{
				editor.AddIncludedManifest( item.ManifestId, item.Location );
				lstIncludedManifests.DataSource = editor.IncludedManifests;
				txtIncludedManifestId.Clear();
				txtIncludedManifestLocation.Clear();
			}
			catch(ManifestEditorException ex )
			{
				ProcessException( ex );
			}
			
		}

		/// <summary>
		/// Close the tool with a user confirmation.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnClose_Click(object sender, System.EventArgs e)
		{
			if ( ConfirmManifestDestruction() )
			{
				Close();
			}
		}

		/// <summary>
		/// Remove a manifest that was included.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnRemoveIncludedManifest_Click(object sender, System.EventArgs e)
		{
			IncludedManifestItem item = lstIncludedManifests.SelectedItem as IncludedManifestItem;
			editor.RemoveIncludedManifest( item.ManifestId );
			lstIncludedManifests.DataSource = editor.IncludedManifests;
			btnRemoveIncludedManifest.Enabled = lstIncludedManifests.Items.Count > 0;
		}

		/// <summary>
		/// Preview the manifest Xml.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnViewManifest_Click(object sender, System.EventArgs e)
		{
			UpdateManifest();
			ManifestViewer vw = new ManifestViewer( editor );
			vw.ShowDialog();
		}

		/// <summary>
		/// Select the source file path.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnBrowseForSourceFolder_Click(object sender, System.EventArgs e)
		{
			folderBrowserDlg.SelectedPath = editor.LocalFilesBase;
			folderBrowserDlg.Description = Resource.ResourceManager[ Resource.MessageKey.BrowseForWorkingFolderDialogText ];
			if( folderBrowserDlg.ShowDialog() == DialogResult.OK )
			{
				try
				{
					editor.LocalFilesBase = folderBrowserDlg.SelectedPath;
					txtLocalFilesBase.Text = editor.LocalFilesBase;

					if ( editor.ManifestFiles != null )
					{
						updatingUI = true;
						lstFiles.Items.Clear();
						foreach( ManifestFileItem file in  editor.ManifestFiles )
						{
							lstFiles.Items.Add( file.Source, file.Transient == Xsd.YesNoBoolType.Yes || file.Transient == Xsd.YesNoBoolType.True );
						}
					}
					
				}
				catch( ManifestEditorException ex )
				{
					sbpDefaultPanel.Text = ex.Message;
				}
				finally
				{
					updatingUI = false;
				}
			}
		}

		/// <summary>
		/// Adds a file to the manifest.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnAddFile_Click(object sender, System.EventArgs e)
		{
			openFileDlg.Title = Resource.ResourceManager[ Resource.MessageKey.AddFileLabel ];
			openFileDlg.FileName = null;
			openFileDlg.Filter = null;

			if ( editor.LocalFilesBase != null )
			{
				openFileDlg.InitialDirectory = editor.LocalFilesBase;
			}
			openFileDlg.Multiselect = true;
			if ( openFileDlg.ShowDialog() != DialogResult.Abort )
			{
				foreach(string filename in openFileDlg.FileNames)
				{
					if ( filename.StartsWith( editor.LocalFilesBase ) )
					{
						editor.AddFile( filename );
						lstFiles.Items.Add( filename.Replace( editor.LocalFilesBase, "" ) );
					}
					else
					{
						sbpDefaultPanel.Text = Resource.ResourceManager[ Resource.MessageKey.ManifestEditorFilesNotAdded ];
					}
				}
			}
		}

		/// <summary>
		/// Removes a file from the manifest.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnRemoveFile_Click(object sender, System.EventArgs e)
		{
			try
			{
				editor.RemoveFile( (string)lstFiles.SelectedItem );
				lstFiles.Items.Remove( lstFiles.SelectedItem );
			}
			catch(ManifestEditorException ex)
			{
				ProcessException( ex );
			}
		}

		/// <summary>
		/// Add a processor to the manifest.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnAddProcessor_Click(object sender, System.EventArgs e)
		{
			try
			{
				ProcessorItem item = editor.AddProcessor( txtProcessorName.Text, cbProcessorType.Text );
				
				lstProcessors.DataSource = editor.ActivationProcessors;
				lstProcessors.DisplayMember = "Description";

				txtProcessorName.Clear();

				ConfigureProcessor( item );
			}
			catch(ManifestEditorException ex)
			{
				ProcessException( ex );
			}
		}

		/// <summary>
		/// Remove a processor from the manifest.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnRemoveProcessor_Click(object sender, System.EventArgs e)
		{
			try
			{
				ProcessorItem item = (ProcessorItem)lstProcessors.SelectedItem;
				editor.RemoveProcessor( item.Name );
				lstProcessors.DataSource = editor.ActivationProcessors;
				btnRemoveProcessor.Enabled = lstProcessors.Items.Count > 0;

				if ( lstProcessors.Items.Count > 0 )
				{
					if ( lstProcessors.SelectedItem != null )
					{
						btnConfigureProcessor.Enabled = ProcessorIsEditable(  lstProcessors.SelectedItem  as ProcessorItem );
					}
					else
					{
						btnConfigureProcessor.Enabled = false;
					}
				}
				else
				{
					btnConfigureProcessor.Enabled = false;
				}
			}
			catch(ManifestEditorException ex)
			{
				ProcessException( ex );
			}
		}

		/// <summary>
		/// Browse cryptograpy configuration.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnBrowseForCryptoConfig_Click(object sender, System.EventArgs e)
		{
			LoadCryptoSettings();
		}

		/// <summary>
		/// Enable or disable the use hashing for the files.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void chkUseHashing_CheckedChanged(object sender, System.EventArgs e)
		{
			if ( !updatingUI )
			{
				editor.UseHashingAlgorithm = chkUseHashing.Checked;

				if ( chkUseHashing.Checked && cbHashingAlgorithms.Items.Count == 0 )
				{
					LoadCryptoSettings();
					chkUseHashing.Checked = cbHashingAlgorithms.Items.Count > 0;
				}
			}
		}

		/// <summary>
		/// Serach for cryptography settings.
		/// </summary>
		private void LoadCryptoSettings()
		{
			cbHashingAlgorithms.Items.Clear();

			openFileDlg.Title = Resource.ResourceManager[ Resource.MessageKey.OpenCryptographyConfigurationLabel ];
			openFileDlg.FileName = null;
			openFileDlg.Multiselect = false;
			openFileDlg.Filter = Resource.ResourceManager[ Resource.MessageKey.OpenCryptographyConfigurationFilter ];
			if ( DialogResult.OK == openFileDlg.ShowDialog() )
			{
				try
				{
					editor.LoadCryptographyConfig ( openFileDlg.FileName );
					foreach(HashProviderData data in editor.HashProviders )
					{
						cbHashingAlgorithms.Items.Add( data.Name );
					}
					if ( cbHashingAlgorithms.Items.Count > 0 )
					{
						cbHashingAlgorithms.SelectedIndex = 0;
					}
					
				}
				catch(Exception ex)
				{
					sbMessageBar.Text = ex.Message;
				}
			}
		}

		/// <summary>
		/// The user has selected a hashing algorithm.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void cbHashingAlgorithms_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if ( !updatingUI )
			{
				editor.HashProviderName = cbHashingAlgorithms.SelectedItem as string;
			}
		}

		/// <summary>
		/// Process a manifest editor exception.
		/// </summary>
		/// <param name="ex">The exception detected.</param>
		private void ProcessException( ManifestEditorException ex )
		{
			if ( ex.Message == null && ex.InnerException != null )
			{
				ProcessException( ex.InnerException );
			}

			sbpDefaultPanel.Text = ex.Message;
		}

		/// <summary>
		/// The user creates a new manifest.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnNewManifest_Click(object sender, System.EventArgs e)
		{
			if ( ConfirmManifestDestruction() )
			{
				editor.New();
				cbHashingAlgorithms.Items.Clear();
				UpdateUI();
			}
		}

		/// <summary>
		/// The user decides the save the manifest.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnSaveManifest_Click(object sender, System.EventArgs e)
		{
			saveFileDlg.Filter = Resource.ResourceManager[ Resource.MessageKey.ManifestFilesFilter ];

			if ( DialogResult.OK == saveFileDlg.ShowDialog() )
			{
				UpdateManifest();
				editor.SaveToFile( saveFileDlg.FileName );
			}
		}

		/// <summary>
		/// The user selected a file.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void lstFiles_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			if ( !updatingUI )
			{
				editor.SetFileTransient( lstFiles.SelectedItem as string, 
					e.NewValue == CheckState.Checked  ? Resource.ResourceManager[ Resource.MessageKey.YesString ] : Resource.ResourceManager[ Resource.MessageKey.NoString ] );
			}
		}

		/// <summary>
		/// Process a generic exception.
		/// </summary>
		/// <param name="ex">The exception received.</param>
		private void ProcessException( Exception ex )
		{
			sbpDefaultPanel.Text = ex.Message;
		}

		/// <summary>
		/// Ask the user for confirmation before cleaning the current manifest.
		/// </summary>
		/// <returns><c>true</c> if the user confirms the dialog, otherwise <c>false</c>.</returns>
		private bool ConfirmManifestDestruction()
		{
			if ( editor.IsDirty )
			{
				if ( DialogResult.Yes == MessageBox.Show( 
					Resource.ResourceManager[ Resource.MessageKey.UnsavedChangesProceedQuestion ], 
					Resource.ResourceManager[ Resource.MessageKey.ConfirmOperationTitle ], MessageBoxButtons.YesNo, MessageBoxIcon.Question ) )
				{
					return true;
				}
				return false;
			}
			return true;
		}

		/// <summary>
		/// Open a manifest from the file system.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void btnOpenManifest_Click(object sender, System.EventArgs e)
		{
			if ( ConfirmManifestDestruction() )
			{
				openFileDlg.FileName = null;
				openFileDlg.Filter = Resource.ResourceManager[ Resource.MessageKey.ManifestFilesFilter ];
				openFileDlg.Multiselect = false;
				openFileDlg.Title = Resource.ResourceManager[ Resource.MessageKey.OpenManifestTitle ];
				if ( DialogResult.OK == openFileDlg.ShowDialog() )
				{
					editor.LoadFromFile( openFileDlg.FileName );
				}



				UpdateUI();
			}
		}

		/// <summary>
		/// The manifes is set to mandatory.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void chkManifestMandatory_CheckedChanged(object sender, System.EventArgs e)
		{
			if ( !updatingUI )
			{
				editor.Mandatory = chkManifestMandatory.Checked ? Resource.ResourceManager[ Resource.MessageKey.YesString ] : Resource.ResourceManager[ Resource.MessageKey.NoString ];
			}
		}

		/// <summary>
		/// Set the controls values with the information in the manifest.
		/// </summary>
		private void UpdateUI()
		{
			updatingUI = true;

			// Manifest tabPane
			txtManifestId.Text = editor.ManifestId;
			chkManifestMandatory.Checked = editor.Mandatory == Resource.ResourceManager[ Resource.MessageKey.YesString ] || editor.Mandatory == Resource.ResourceManager[ Resource.MessageKey.NoString ];
			txtManifestDescription.Text = editor.Description;
			lstIncludedManifests.DataSource = editor.IncludedManifests;
			lstIncludedManifests.DisplayMember = "Description";
			txtLocalFilesBase.Text = editor.LocalFilesBase;

			// Application tabPane
			txtApplicationId.Text = editor.ApplicationId;
			txtApplicationLocation.Text = editor.ApplicationLocation;
			txtEntryPointFile.Text = editor.ApplicationEntryPointFile;
			txtEntryPointParameters.Text = editor.ApplicationEntryPointParams;
			chkUseHashing.Checked = editor.UseHashingAlgorithm;
			txtFilesURI.Text = editor.FilesBase;
			if ( editor.UseHashingAlgorithm)
			{
				LoadCryptoSettings();
			}
			if ( editor.HashProviderName != null )
			{
				foreach(string item in  cbHashingAlgorithms.Items )
				{
					if ( String.Compare( item, editor.HashProviderName, true, CultureInfo.InvariantCulture) == 0 )
					{
						cbHashingAlgorithms.SelectedItem = item;
						break;
					}
				}
			}
			
			ManifestFileItem[] files = editor.ManifestFiles;
			lstFiles.Items.Clear();
			if ( files != null )
			{
				txtLocalFilesBase.Text = editor.LocalFilesBase;


				if ( files.Length > 0 )
				{
					for(int i=0;i<files.Length;i++)
					{
						bool transient = files[i].Transient == Xsd.YesNoBoolType.Yes || files[i].Transient == Xsd.YesNoBoolType.True;
						lstFiles.Items.Add( files[i].Source, transient );
					}

					string newLocalFilesBase = txtLocalFilesBase.Text;
					if ( !ValidateManifestFiles( files, newLocalFilesBase ) )
					{
						foreach( ManifestFileItem file in files )
						{
							editor.RemoveFile( file.Source );
						}
						do
						{
							folderBrowserDlg.Description = Resource.ResourceManager[ Resource.MessageKey.BrowseForSourceFolderDescription ];
							folderBrowserDlg.SelectedPath = newLocalFilesBase;
							if ( DialogResult.OK != folderBrowserDlg.ShowDialog() )
							{
								MessageBox.Show( 
									Resource.ResourceManager[ Resource.MessageKey.InvalidSourceFolderMessage ],
									Resource.ResourceManager[ Resource.MessageKey.InvalidSourceFolderCaption ],
									MessageBoxButtons.OK  );
								editor.New();
								UpdateUI();
								return;
							}
							newLocalFilesBase = folderBrowserDlg.SelectedPath;
						} while ( !ValidateManifestFiles( files, newLocalFilesBase  ) );

						editor.LocalFilesBase = newLocalFilesBase;
						txtLocalFilesBase.Text = newLocalFilesBase;

						foreach( ManifestFileItem file in files )
						{
							editor.AddFile( file.Source, file.Transient.ToString( CultureInfo.InstalledUICulture )  );
						}
					}
				}
			}

			// Processors tabPane
			lstProcessors.DataSource = null;
			lstProcessors.Items.Clear();
			lstProcessors.DisplayMember = "Description";
			if ( editor.ActivationProcessors != null )
			{
				if ( editor.ActivationProcessors != null )
				{
					lstProcessors.DataSource = editor.ActivationProcessors;
				}
			}

			// Downloader tabPane
			if ( editor.IncludeDownloader )
			{
				chkIncludeDownloader.Checked = true;
				txtDownloaderName.Text = editor.DownloaderName;
			}
			else
			{
				chkIncludeDownloader.Checked = false;
				txtDownloaderName.Text = String.Empty;
			}

			editor.ClearDirtyFlag();

			updatingUI = false;
		}

		/// <summary>
		/// Verifies that the files included in the manifest are reachable from the specified folder.
		/// </summary>
		/// <param name="files">An array of <see cref="ManifestFileItem"/>.</param>
		/// <param name="filesBase">The path to the folder where the files should be.</param>
		/// <returns><c>true</c> if all the files are under the specified folder</returns>
		private bool ValidateManifestFiles( ManifestFileItem[] files, string filesBase )
		{
			bool allFilesValid = true;
			
			if ( files.Length > 0 )
			{
				
				for(int i=0;i<files.Length;i++)
				{
					string filePath = Path.Combine( filesBase, files[i].Source );
					allFilesValid = allFilesValid && File.Exists( filePath );
					if ( !allFilesValid )
					{
						return false;
					}
				}
			}
			return allFilesValid;
		}

		/// <summary>
		/// The check to enable the downloader configuration has be checked/unchecked.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void chkIncludeDownloader_CheckedChanged(object sender, System.EventArgs e)
		{
			if ( !updatingUI )
			{
				editor.IncludeDownloader = chkManifestMandatory.Checked;
			}
		
		}

		/// <summary>
		/// The user has validated the manifest.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void cmdValidate_Click(object sender, System.EventArgs e)
		{

			StringBuilder errorMessage = new StringBuilder();

			if ( txtManifestId.Text.Length == 0 )
			{
				errorMessage.Append( Resource.ResourceManager[ Resource.MessageKey.EditorMissingManifestId ] );	
			}
			if ( txtManifestDescription.Text.Length == 0 )
			{
				if ( errorMessage.Length > 0 )
				{
					errorMessage.Append( Environment.NewLine );
				}
				errorMessage.Append( Resource.ResourceManager[ Resource.MessageKey.EditorMissingManifestDescription ] );
			}
			if ( txtApplicationId.Text.Length == 0 )
			{
				if ( errorMessage.Length > 0 )
				{
					errorMessage.Append( Environment.NewLine );
				}
				errorMessage.Append( Resource.ResourceManager[ Resource.MessageKey.EditorMissingApplicationId ] );
			}
			if ( txtApplicationLocation.Text.Length == 0 )
			{
				if ( errorMessage.Length > 0 )
				{
					errorMessage.Append( Environment.NewLine );
				}
				errorMessage.Append( Resource.ResourceManager[ Resource.MessageKey.EditorMissingApplicationLocation ] );
			}

			if ( errorMessage.Length > 0 )
			{
				MessageBox.Show( this, 
					errorMessage.ToString(), 
					Resource.ResourceManager[ Resource.MessageKey.EditorValidationErrorCaption ],
					MessageBoxButtons.OK,
					MessageBoxIcon.Error );
				return;
			}

			UpdateManifest();

			string[] errors = editor.Validate();

			if ( errors == null )
			{
				MessageBox.Show(
					Resource.ResourceManager[ Resource.MessageKey.TheManifestIsValid ],
					Resource.ResourceManager[ Resource.MessageKey.ManifestValidationTitle ],
					MessageBoxButtons.OK,
					MessageBoxIcon.Information );
			}
			else
			{
				StringBuilder sb = new StringBuilder();
				sb.Append( Resource.ResourceManager[ Resource.MessageKey.TheManifestIsInvalid ] );
				sb.Append( Environment.NewLine );
				sb.Append( String.Join( Environment.NewLine, errors ) );

				MessageBox.Show(
					sb.ToString(),
					Resource.ResourceManager[ Resource.MessageKey.ManifestValidationTitle ],
					MessageBoxButtons.OK,
					MessageBoxIcon.Error);			
			}
		}

		/// <summary>
		/// Enables the button when data is entered.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void lstIncludedManifests_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			btnRemoveIncludedManifest.Enabled = lstIncludedManifests.SelectedIndex >= 0;
		}

		/// <summary>
		/// Enable the button when data is entered.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void txtIncludedManifestLocation_TextChanged(object sender, System.EventArgs e)
		{
			btnAddIncludedManifest.Enabled = txtIncludedManifestId.Text.Length > 0 &&
				txtIncludedManifestLocation.Text.Length > 0;
		}

		/// <summary>
		/// Enable the button when data is entered.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void txtIncludedManifestId_TextChanged(object sender, System.EventArgs e)
		{
			btnAddIncludedManifest.Enabled = txtIncludedManifestId.Text.Length > 0 &&
				txtIncludedManifestLocation.Text.Length > 0;		
		}

		/// <summary>
		/// Enable the button when data is entered.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void lstFiles_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			btnRemoveFile.Enabled = lstFiles.SelectedIndex >= 0;
		}

		/// <summary>
		/// Enable the button when data is entered.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void txtProcessorName_TextChanged(object sender, System.EventArgs e)
		{
			btnAddProcessor.Enabled = txtProcessorName.Text.Length > 0 &&
				cbProcessorType.Text.Length > 0;
		}

		/// <summary>
		/// Enable the button when data is entered.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void cbProcessorType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			btnAddProcessor.Enabled = txtProcessorName.Text.Length > 0 &&
				cbProcessorType.Text.Length > 0;
		}

		/// <summary>
		/// Enable the button when data is entered.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The event arguments.</param>
		private void lstProcessors_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			btnRemoveProcessor.Enabled = lstProcessors.SelectedIndex >= 0;
			if ( lstProcessors.SelectedIndex >=0)
			{
				btnConfigureProcessor.Enabled = ProcessorIsEditable( lstProcessors.SelectedItem  as ProcessorItem );
			}
			else
			{
				btnConfigureProcessor.Enabled = false;
			}
		}

		private void btnConfigureProcessor_Click(object sender, System.EventArgs e)
		{
			ConfigureProcessor( lstProcessors.SelectedItem as ProcessorItem );
		}

		private void ConfigureProcessor( ProcessorItem item )
		{
			if ( item != null )
			{
				if ( ProcessorIsEditable( item ) )
				{
					ConfigureProcessor cpf = new ConfigureProcessor( item );
					cpf.ShowDialog();
				}
			}
		}

		private bool ProcessorIsEditable( ProcessorItem item )
		{
			NameValueCollection config = (NameValueCollection)ConfigurationSettings.GetConfig( ProcessorEditorsConfigSection );
			if ( config == null )
			{
				MessageBox.Show( this,
					Resource.ResourceManager[ Resource.MessageKey.MissingManifestEditorConfiguration, ProcessorEditorsConfigSection ],
					Resource.ResourceManager[ Resource.MessageKey.MissingManifestEditorConfigurationTitle ],
					MessageBoxButtons.OK );
				return false;
			}
			Type processorType = Type.GetType( item.TypeName );
			return config[ processorType.FullName ] != null;
		}

		private void txtApplicationId_TextChanged(object sender, System.EventArgs e)
		{
			if ( txtApplicationId.Text.Length == 0 )
			{
				txtApplicationId.BackColor = Color.LightGoldenrodYellow;
			}
			else
			{
				txtApplicationId.BackColor = Color.White;
			}		
		}

		private void txtManifestId_TextChanged(object sender, System.EventArgs e)
		{
			if ( txtManifestId.Text.Length == 0 )
			{
				txtManifestId.BackColor = Color.LightGoldenrodYellow;
			}
			else
			{
				txtManifestId.BackColor = Color.White;
			}
		}

		private void txtManifestDescription_TextChanged(object sender, System.EventArgs e)
		{
			if ( txtManifestDescription.Text.Length == 0 )
			{
				txtManifestDescription.BackColor = Color.LightGoldenrodYellow;
			}
			else
			{
				txtManifestDescription.BackColor = Color.White;
			}		
		}

		private void txtApplicationLocation_TextChanged(object sender, System.EventArgs e)
		{
			if ( txtApplicationLocation.Text.Length == 0 )
			{
				txtApplicationLocation.BackColor = Color.LightGoldenrodYellow;
			}
			else
			{
				txtApplicationLocation.BackColor = Color.White;
			}		
		}

	}
}
