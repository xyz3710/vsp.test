//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ManifestViewer.cs
//
// Contains the implementation of the manifest viewer.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// A simple form to view the manifest xml.
	/// </summary>
	public class ManifestViewer : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox txtManifestXML;
		private System.Windows.Forms.Button btnClose;

		/// <summary>
		/// The manifest editor instance.
		/// </summary>
		private ManifestEditor editor;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ManifestViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Creates a ManifestViewer with the manifest editor.
		/// </summary>
		/// <param name="editor"></param>
		public ManifestViewer( ManifestEditor editor )
		{
			InitializeComponent();

			this.editor = editor;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtManifestXML = new System.Windows.Forms.TextBox();
			this.btnClose = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txtManifestXML
			// 
			this.txtManifestXML.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtManifestXML.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.txtManifestXML.Location = new System.Drawing.Point(8, 8);
			this.txtManifestXML.Multiline = true;
			this.txtManifestXML.Name = "txtManifestXML";
			this.txtManifestXML.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtManifestXML.Size = new System.Drawing.Size(676, 284);
			this.txtManifestXML.TabIndex = 0;
			this.txtManifestXML.Text = "txtManifestXML";
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Location = new System.Drawing.Point(588, 308);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(88, 23);
			this.btnClose.TabIndex = 1;
			this.btnClose.Text = "Close";
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// ManifestViewer
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(696, 342);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.txtManifestXML);
			this.Name = "ManifestViewer";
			this.ShowInTaskbar = false;
			this.Text = "ManifestViewer";
			this.Load += new System.EventHandler(this.ManifestViewer_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// Closes the dialog.
		/// </summary>
		/// <param name="sender">The sender object.</param>
		/// <param name="e">The event arguments.</param>
		private void btnClose_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		/// <summary>
		/// Set the contents of the dialog.
		/// </summary>
		/// <param name="sender">The sender object.</param>
		/// <param name="e">The event arguments.</param>
		private void ManifestViewer_Load(object sender, System.EventArgs e)
		{
			txtManifestXML.Text = editor.GetXml();
			txtManifestXML.SelectionLength = 0;
		}
	}
}
