//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// FolderCopyProcessorEditor.cs
//
// Contains the implementation of the editor control for the FolderCopyProcessor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.Windows.Forms;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Summary description for FileCopyProcessorEditor.
	/// </summary>
	public class FolderCopyProcessorEditor : ProcessorEditorControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtSource;
		private System.Windows.Forms.TextBox txtDestination;

		private FolderCopyProcessorItem item;
		private System.Windows.Forms.CheckBox chkOverwrite;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor
		/// </summary>
		public FolderCopyProcessorEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.chkOverwrite = new System.Windows.Forms.CheckBox();
			this.txtSource = new System.Windows.Forms.TextBox();
			this.txtDestination = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Source";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 40);
			this.label2.Name = "label2";
			this.label2.TabIndex = 1;
			this.label2.Text = "Destination";
			// 
			// chkOverwrite
			// 
			this.chkOverwrite.Location = new System.Drawing.Point(120, 72);
			this.chkOverwrite.Name = "chkOverwrite";
			this.chkOverwrite.TabIndex = 2;
			this.chkOverwrite.Text = "Overwrite";
			// 
			// txtSource
			// 
			this.txtSource.Location = new System.Drawing.Point(120, 9);
			this.txtSource.Name = "txtSource";
			this.txtSource.Size = new System.Drawing.Size(248, 20);
			this.txtSource.TabIndex = 3;
			this.txtSource.Text = "";
			// 
			// txtDestination
			// 
			this.txtDestination.Location = new System.Drawing.Point(120, 41);
			this.txtDestination.Name = "txtDestination";
			this.txtDestination.Size = new System.Drawing.Size(248, 20);
			this.txtDestination.TabIndex = 4;
			this.txtDestination.Text = "";
			// 
			// FolderCopyProcessorEditor
			// 
			this.Controls.Add(this.txtDestination);
			this.Controls.Add(this.txtSource);
			this.Controls.Add(this.chkOverwrite);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "FolderCopyProcessorEditor";
			this.Size = new System.Drawing.Size(376, 104);
			this.ResumeLayout(false);

		}
		#endregion

		/// Gets or sets the wrapped item to edit
		public override ProcessorItem Item
		{
			get
			{
				if ( item == null )
				{
					item = new FolderCopyProcessorItem();
				}
				return item;
			}
			set
			{
				item = value as FolderCopyProcessorItem;
				if ( item == null )
				{
					throw new ManifestEditorException( Resource.ResourceManager[ Resource.MessageKey.InvalidProcessorItemForEdition, value.GetType().FullName, this.GetType().FullName ] );
				}
				txtDestination.Text = item.Destination;
				txtSource.Text = item.Source;
				chkOverwrite.Checked = bool.Parse( item.Overwrite );
			}
		}

		/// Called when the user accepts the edition changes
		public override void OnOK()
		{
			base.OnOK ();
			item.Source = txtSource.Text;
			item.Destination = txtDestination.Text;
			item.Overwrite = chkOverwrite.Checked.ToString( CultureInfo.InvariantCulture );
		}

		/// Called when the user rejects the edition changes
		public override void OnCancel()
		{
			base.OnCancel ();
		}
	}
}
