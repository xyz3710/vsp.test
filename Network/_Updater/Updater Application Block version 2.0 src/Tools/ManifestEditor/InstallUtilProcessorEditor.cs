//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// InstallUtilProcessorEditor.cs
//
// Contains the implementation of the editor control for the InstallUtilProcessor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Summary description for InstallUtilProcessorEditor.
	/// </summary>
	public class InstallUtilProcessorEditor : ProcessorEditorControl
	{

		private ArrayList assemblies = new ArrayList();

		private System.Windows.Forms.Label label1;

		private InstallUtilProcessorItem item;
		private System.Windows.Forms.ComboBox cboAction;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ListBox lstAssemblies;
		private System.Windows.Forms.Button btnRemove;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.TextBox txtOptions;
		private System.Windows.Forms.TextBox txtPath;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor
		/// </summary>
		public InstallUtilProcessorEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			cboAction.SelectedIndex = 0;

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.cboAction = new System.Windows.Forms.ComboBox();
			this.lstAssemblies = new System.Windows.Forms.ListBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtPath = new System.Windows.Forms.TextBox();
			this.txtOptions = new System.Windows.Forms.TextBox();
			this.btnAdd = new System.Windows.Forms.Button();
			this.btnRemove = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Action";
			// 
			// cboAction
			// 
			this.cboAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboAction.Items.AddRange(new object[] {
														   "Install",
														   "Uninstall"});
			this.cboAction.Location = new System.Drawing.Point(72, 9);
			this.cboAction.Name = "cboAction";
			this.cboAction.Size = new System.Drawing.Size(121, 21);
			this.cboAction.TabIndex = 1;
			// 
			// lstAssemblies
			// 
			this.lstAssemblies.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lstAssemblies.DisplayMember = "Description";
			this.lstAssemblies.HorizontalScrollbar = true;
			this.lstAssemblies.Location = new System.Drawing.Point(8, 96);
			this.lstAssemblies.Name = "lstAssemblies";
			this.lstAssemblies.Size = new System.Drawing.Size(344, 108);
			this.lstAssemblies.TabIndex = 2;
			this.lstAssemblies.SelectedIndexChanged += new System.EventHandler(this.lstAssemblies_SelectedIndexChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.txtPath);
			this.groupBox1.Controls.Add(this.txtOptions);
			this.groupBox1.Controls.Add(this.btnAdd);
			this.groupBox1.Controls.Add(this.btnRemove);
			this.groupBox1.Controls.Add(this.lstAssemblies);
			this.groupBox1.Location = new System.Drawing.Point(8, 40);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(360, 248);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Assemblies";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 40);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 23);
			this.label3.TabIndex = 8;
			this.label3.Text = "Path";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 23);
			this.label2.TabIndex = 7;
			this.label2.Text = "Options";
			// 
			// txtPath
			// 
			this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtPath.Location = new System.Drawing.Point(72, 41);
			this.txtPath.Name = "txtPath";
			this.txtPath.Size = new System.Drawing.Size(280, 20);
			this.txtPath.TabIndex = 6;
			this.txtPath.Text = "";
			this.txtPath.TextChanged += new System.EventHandler(this.txtPath_TextChanged);
			// 
			// txtOptions
			// 
			this.txtOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtOptions.Location = new System.Drawing.Point(72, 17);
			this.txtOptions.Name = "txtOptions";
			this.txtOptions.Size = new System.Drawing.Size(280, 20);
			this.txtOptions.TabIndex = 5;
			this.txtOptions.Text = "";
			this.txtOptions.TextChanged += new System.EventHandler(this.txtOptions_TextChanged);
			// 
			// btnAdd
			// 
			this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAdd.Enabled = false;
			this.btnAdd.Location = new System.Drawing.Point(280, 68);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.TabIndex = 4;
			this.btnAdd.Text = "Add";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// btnRemove
			// 
			this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnRemove.Enabled = false;
			this.btnRemove.Location = new System.Drawing.Point(280, 216);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.TabIndex = 3;
			this.btnRemove.Text = "Remove";
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// InstallUtilProcessorEditor
			// 
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.cboAction);
			this.Controls.Add(this.label1);
			this.Name = "InstallUtilProcessorEditor";
			this.Size = new System.Drawing.Size(376, 304);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// Gets or sets the wrapped item to edit
		public override ProcessorItem Item
		{
			get
			{
				if ( item == null )
				{
					item = new InstallUtilProcessorItem();
				}
				return item;
			}
			set
			{
				item = value as InstallUtilProcessorItem;
				if ( item == null )
				{
					throw new ManifestEditorException( Resource.ResourceManager[ Resource.MessageKey.InvalidProcessorItemForEdition, value.GetType().FullName, this.GetType().FullName ] );
				}
				cboAction.SelectedItem =  item.Action;

				if ( item.Assemblies != null )
				{
					lstAssemblies.Items.AddRange( item.Assemblies );
				}

			}
		}

		/// Called when the user accepts the edition changes
		public override void OnOK()
		{
			base.OnOK ();
			item.Action = cboAction.SelectedItem as String;

			InstallUtilProcessorAssembly[] items = new InstallUtilProcessorAssembly[lstAssemblies.Items.Count];
			lstAssemblies.Items.CopyTo( items, 0 ); 
			item.Assemblies =  items;
		}

		/// Called when the user rejects the edition changes
		public override void OnCancel()
		{
			base.OnCancel ();
		}

		private void txtOptions_TextChanged(object sender, System.EventArgs e)
		{
			btnAdd.Enabled = txtOptions.Text.Length > 0 && txtPath.Text.Length > 0;
		}

		private void txtPath_TextChanged(object sender, System.EventArgs e)
		{
			btnAdd.Enabled = txtOptions.Text.Length > 0 && txtPath.Text.Length > 0;
		}

		private void lstAssemblies_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			btnRemove.Enabled = lstAssemblies.SelectedIndex >= 0;
		}

		private void btnRemove_Click(object sender, System.EventArgs e)
		{
			lstAssemblies.Items.Remove( lstAssemblies.SelectedItem );
		}

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			InstallUtilProcessorAssembly item = new InstallUtilProcessorAssembly( txtPath.Text, txtOptions.Text );
			lstAssemblies.Items.Add( item );
			
		}

	}
}
