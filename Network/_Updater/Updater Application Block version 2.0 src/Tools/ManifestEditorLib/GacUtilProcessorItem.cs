//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// GacUtilProcessorItem.cs
//
// Contains the implementation of the processor item for the GacUtil Processor
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Globalization;
using System.Xml;
using Xsd = Microsoft.ApplicationBlocks.Updater.Xsd;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Editor wrapper for the Xsd.ActivationProcessorProviderData for the GacUtilProcessorItem.
	/// </summary>
	public class GacUtilProcessorItem : ProcessorItem
	{
		#region Constructor

		/// <summary>
		/// Initialize the new instance based on the provided data
		/// </summary>
		/// <param name="data">An  ActivationProcessorProviderData to wrap</param>
		public GacUtilProcessorItem( Xsd.ActivationProcessorProviderData data ) : base( data )
		{
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		public GacUtilProcessorItem( ) : base(  )
		{
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Gets or sets the options list to use with the Gacutil.exe tool
		/// </summary>
		public string Options
		{
			get
			{
				return  GetAnyAttributeValue( "options", String.Empty );
			}
			set
			{
				SetAnyAttributeValue( "options", value );
			}
		}

		/// <summary>
		/// Gets or sets the assembly file argument for the Gacutil.exe tool.
		/// </summary>
		public string AssemblyFile
		{
			get
			{
				return  GetAnyAttributeValue( "assemblyFile", String.Empty );
			}
			set
			{
				SetAnyAttributeValue( "assemblyFile", value );
			}
		}

		/// <summary>
		/// Gets or sets the assembly name argument for the Gacutil.exe tool.
		/// </summary>
		public string AssemblyName
		{
			get
			{
				return GetAnyAttributeValue( "assemblyName", String.Empty );
			}
			set
			{
				SetAnyAttributeValue( "assemblyName", value );
			}
		}

		#endregion
	}
}
