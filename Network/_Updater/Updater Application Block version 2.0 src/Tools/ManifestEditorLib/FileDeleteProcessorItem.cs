//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// FileDeleteProcessorItem.cs
//
// Contains the implementation of the processor item for the FileDelete Processor
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Xml;
using Xsd = Microsoft.ApplicationBlocks.Updater.Xsd;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Editor wrapper for the Xsd.ActivationProcessorProviderData for the
	/// FileDeleteProcessor.
	/// </summary>
	public class FileDeleteProcessorItem : ProcessorItem
	{
		#region Constructor

		/// <summary>
		/// Initialize the new instance based on the provided data
		/// </summary>
		/// <param name="data">An  ActivationProcessorProviderData to wrap</param>
		public FileDeleteProcessorItem( Xsd.ActivationProcessorProviderData data ) : base( data )
		{
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		public FileDeleteProcessorItem( ) : base(  )
		{
		}

		#endregion

		#region Private members

	
		#endregion

		#region Public properties

		/// <summary>
		/// Gets or sets the source attribute value
		/// </summary>
		public string Path
		{
			get
			{
				return GetAnyAttributeValue( "path", String.Empty );
			}
			set
			{
				SetAnyAttributeValue( "path", value );
			}

		}

		#endregion
	}
}
