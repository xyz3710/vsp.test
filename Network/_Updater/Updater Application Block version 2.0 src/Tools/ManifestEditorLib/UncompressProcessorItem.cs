//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// UncompressProcessorItem.cs
//
// Contains the implementation of the processor item for the InstallUtil Processor
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Globalization;
using System.Xml;
using Xsd = Microsoft.ApplicationBlocks.Updater.Xsd;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Editor wrapper for the Xsd.ActivationProcessorProviderData for the UncompressProcessorItem.
	/// </summary>
	public class UncompressProcessorItem : ProcessorItem
	{
		#region Constructor

		/// <summary>
		/// Initialize the new instance based on the provided data
		/// </summary>
		/// <param name="data">An  ActivationProcessorProviderData to wrap</param>
		public UncompressProcessorItem( Xsd.ActivationProcessorProviderData data ) : base( data )
		{
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		public UncompressProcessorItem( ) : base(  )
		{
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Gets or sets the source attribute value
		/// </summary>
		public string Source
		{
			get
			{
				return  GetAnyAttributeValue( "source", String.Empty );
			}
			set
			{
				SetAnyAttributeValue( "source", value );
			}
		}

		/// <summary>
		/// Gets or sets the destination attribute value
		/// </summary>
		public string Destination
		{
			get
			{
				return  GetAnyAttributeValue( "destination", String.Empty );
			}
			set
			{
				SetAnyAttributeValue( "destination", value );
			}
		}

		#endregion
	}
}
