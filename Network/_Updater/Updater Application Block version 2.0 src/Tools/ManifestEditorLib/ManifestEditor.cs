//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ManifestEditor.cs
//
// Contains the implementation of the manifest editor.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration;

using Microsoft.ApplicationBlocks.Updater;
using Microsoft.ApplicationBlocks.Updater.ActivationProcessors;
using Microsoft.ApplicationBlocks.Updater.Downloaders;
using Microsoft.ApplicationBlocks.Updater.Utilities;
using Xsd = Microsoft.ApplicationBlocks.Updater.Xsd;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// This type is used to build and manage the creation or editing
	/// of manifests.
	/// </summary>
	public class ManifestEditor 
	{
		#region Private members

		/// <summary>
		/// The manifest being edited.
		/// </summary>
		private Xsd.Manifest manifest;

		/// <summary>
		/// A flag indicating if a change has been made to the manifest.
		/// </summary>
		private bool isDirty;

		/// <summary>
		/// A rooted local path which contains the files included in the manifest this value is used to 
		/// validate the file existence and computing the file hashes.
		/// </summary>
		private string localFilesBase;

		/// <summary>
		/// A list of the in-box processors info.
		/// </summary>
		private SortedList processors;

		/// <summary>
		/// The current selected cryptography setting used to calculate the file hashes.
		/// </summary>
		private CryptographySettings cryptoSettings;

		/// <summary>
		/// A flag indicating if the Downloader node must be included.
		/// </summary>
		private bool includeDownloader = false;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ManifestEditor()
		{
			
			processors = new SortedList();
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.ApplicationDeployProcessor ], typeof( ApplicationDeployProcessor ) );
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.FileCopyProcessor ], typeof( FileCopyProcessor ) );
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.FileDeleteProcessor ], typeof( FileDeleteProcessor ) );
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.FolderCopyProcessor ], typeof( FolderCopyProcessor ) );
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.FolderDeleteProcessor ], typeof( FolderDeleteProcessor ) );
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.GacUtilProcessor ], typeof( GacUtilProcessor ) );
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.InstallUtilProcessor ], typeof( InstallUtilProcessor ) );
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.MSIProcessor ], typeof( MsiProcessor ) );
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.UncompressProcessor ], typeof( UncompressProcessor ) );
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.HashValidationProcessor ], typeof( ValidateHashProcessor ) );
			processors.Add( Resource.ResourceManager[ Resource.MessageKey.WaitForExitProcessor ], typeof( WaitForApplicationExitProcessor ) );
		}

		#endregion

		#region Helpers

		/// <summary>
		/// Removes the preceding and trailing quotes.
		/// </summary>
		/// <param name="text">The text string to process.</param>
		/// <returns>A new string without the quotes.</returns>
		private static string CleanQuotes( string text )
		{
			if ( text.StartsWith( "\"" ) && text.EndsWith( "\"" ) )
			{
				return text.TrimStart('"').TrimEnd('"');
			}
			return text;
		}

		/// <summary>
		/// Return the processor type.
		/// </summary>
		/// <param name="alias">The processor alias.</param>
		/// <returns>The processor type name.</returns>
		private string GetProcessorType( string alias )
		{
			if ( processors.ContainsKey( alias ) )
			{
				Type t = (Type)processors[ alias ];
				return t.AssemblyQualifiedName;
			}

			return alias;
		}

		/// <summary>
		/// Return the relative file name.
		/// </summary>
		/// <param name="filename">The file name.</param>
		/// <returns>A new file name.</returns>
		private string GetRelativeFileName( string filename )
		{
			filename = CleanQuotes( filename );
			if ( filename.StartsWith( LocalFilesBase ) )
			{
				filename = filename.Replace( LocalFilesBase, "" );
			}
			return filename;
		}

		/// <summary>
		/// Set the dirty flag.
		/// </summary>
		public void SetDirtyFlag()
		{
			isDirty = true;
		}

		/// <summary>
		/// Clear the dirty flag.
		/// </summary>
		public void ClearDirtyFlag()
		{
			isDirty = false;
		}

		#endregion

		#region Properties


		/// <summary>
		/// The processor name list.
		/// </summary>
		public string[] Processors
		{
			get
			{
				ArrayList list = new ArrayList( processors.Keys );
				return (string[])list.ToArray(typeof(string));
			}
		}

		/// <summary>
		/// The manifest instance.
		/// </summary>
		private Xsd.Manifest Manifest
		{
			get
			{
				if ( manifest == null )
				{
					manifest = new Xsd.Manifest();
					manifest.Mandatory = Xsd.YesNoBoolType.No;
				}
				return manifest;
			}
		}

		#endregion

		#region Manifest edition

		/// <summary>
		/// The manifest ID.
		/// </summary>
		public string ManifestId
		{
			get
			{
				return Manifest.ManifestId;
			}
			set
			{
				Manifest.ManifestId = CleanQuotes( value );
				SetDirtyFlag();
			}
		}

		/// <summary>
		/// The manifest description.
		/// </summary>
		public string Description
		{
			get
			{
				return Manifest.Description;
			}
			set
			{
				Manifest.Description = CleanQuotes( value );
				SetDirtyFlag();
			}
		}

		/// <summary>
		/// Indicates whether the manifest is mandatory.
		/// </summary>
		public string Mandatory
		{
			get
			{
				return Manifest.Mandatory.ToString( CultureInfo.InvariantCulture );
			}
			set
			{
				Manifest.Mandatory = (Xsd.YesNoBoolType)Enum.Parse(typeof(Xsd.YesNoBoolType), value, true );
				SetDirtyFlag();
			}
		}
	
		/// <summary>
		/// Add an included manifest.
		/// </summary>
		/// <param name="id">The ID of the included manifest.</param>
		public void AddIncludedManifest( string id )
		{
			AddIncludedManifest( id, null );
		}

		/// <summary>
		/// Add an included manifest .
		/// </summary>
		/// <param name="id">The ID of the included manifest.</param>
		/// <param name="location">The location of the included manifest.</param>
		public void AddIncludedManifest( string id, string location )
		{
			Xsd.IncludedManifest man = FindIncludedManifest( id );

			if ( man == null )
			{
				man = new Xsd.IncludedManifest();
				man.ManifestId = CleanQuotes( id );
			}
			else
			{
				throw new ManifestEditorException( 
					Resource.ResourceManager[ Resource.MessageKey.ManifestAlreadyIncluded, id ] );
			}

			if ( Manifest.IncludedManifests == null )
			{
				Manifest.IncludedManifests = new Xsd.IncludedManifests();
			}
			if ( Manifest.IncludedManifests.Manifest == null )
			{
				Manifest.IncludedManifests.Manifest = new Xsd.IncludedManifest[] { man };
			}
			else
			{
				ArrayList manifests = new ArrayList( Manifest.IncludedManifests.Manifest );
				manifests.Add( man );
				Manifest.IncludedManifests.Manifest = (Xsd.IncludedManifest[])manifests.ToArray(typeof(Xsd.IncludedManifest));
			}
			
			if ( location != null )
			{
				man.Location = CleanQuotes( location );
			}

			SetDirtyFlag();

		}

		/// <summary>
		/// Removes the included manifest by its ID.
		/// </summary>
		/// <param name="id">The manifest ID.</param>
		public void RemoveIncludedManifest( string id )
		{
			if ( Manifest.IncludedManifests == null || Manifest.IncludedManifests.Manifest == null )
			{
				return;
			}

			ArrayList list = new ArrayList();
			foreach( Xsd.IncludedManifest man in Manifest.IncludedManifests.Manifest )
			{
				if ( String.Compare( id, man.ManifestId, true, CultureInfo.InvariantCulture ) != 0 )
				{
					list.Add( man );
				}
			}

			Manifest.IncludedManifests.Manifest = (Xsd.IncludedManifest[])list.ToArray(typeof(Xsd.IncludedManifest));
			SetDirtyFlag();
		}

		/// <summary>
		/// Set the manifes location for an included manifest by providing its ID.
		/// </summary>
		/// <param name="id">The ID of the manifest.</param>
		/// <param name="value">The value for the location field.</param>
		public void SetIncludedManifestLocation( string id, string value )
		{
			Xsd.IncludedManifest man = FindIncludedManifest( id );
			if ( man == null )
			{
				AddIncludedManifest( id, value );
			}
			else
			{
				man.Location = CleanQuotes( value );
			}
			SetDirtyFlag();
		}

		/// <summary>
		/// Find the included manifest in the manifest by its id.
		/// </summary>
		/// <param name="id">The id of the included manifest to search to.</param>
		/// <returns>The included manifest isntance.</returns>
		private Xsd.IncludedManifest FindIncludedManifest( string id )
		{
			if ( Manifest.IncludedManifests == null || Manifest.IncludedManifests.Manifest == null )
			{
				return null;
			}
			foreach( Xsd.IncludedManifest im in Manifest.IncludedManifests.Manifest )
			{
				if ( String.Compare( id, im.ManifestId, true, CultureInfo.InvariantCulture ) == 0 )
				{
					return im;
				}
			}
			return null;
		}

		/// <summary>
		/// Gets all the included manifests.
		/// </summary>
		public IncludedManifestItem[] IncludedManifests
		{
			get
			{
				if ( Manifest.IncludedManifests == null || Manifest.IncludedManifests.Manifest == null )
				{
					return null;
				}

				IncludedManifestItem[] items = new IncludedManifestItem[ Manifest.IncludedManifests.Manifest.Length ];
				for(int i=0;i<Manifest.IncludedManifests.Manifest.Length;i++)
				{
					items[i] = new IncludedManifestItem( Manifest.IncludedManifests.Manifest[i] );
				}

				return items;
			}
		}

		#endregion

		#region Application edition

		/// <summary>
		/// Gets or sets an string with the unique ID for the application
		/// </summary>
		public string ApplicationId
		{
			get
			{
				return Manifest.Application == null ? null : Manifest.Application.ApplicationId;
			}
			set
			{
				if ( Manifest.Application == null )
				{
					Manifest.Application = new Xsd.ManifestApplication();
				}
				Manifest.Application.ApplicationId = CleanQuotes( value );
				SetDirtyFlag();
			}
		}

		/// <summary>
		/// Gets or sets an string giving an indication (or hint) about the location of 
		/// the application on the client computer
		/// </summary>
		public string  ApplicationLocation
		{
			get
			{
				return Manifest.Application == null ? null : Manifest.Application.Location;
			}
			set
			{
				if ( Manifest.Application == null )
				{
					Manifest.Application = new Xsd.ManifestApplication();
				}
				Manifest.Application.Location = CleanQuotes( value );
				SetDirtyFlag();
			}
		}

		/// <summary>
		/// Gets or sets an string indicating the file that is the application entry-point
		/// </summary>
		public string ApplicationEntryPointFile
		{
			get
			{
				return ( Manifest.Application == null  || Manifest.Application.EntryPoint == null ) ? 
					null : Manifest.Application.EntryPoint.File;
			}
			set
			{
				if ( Manifest.Application == null )
				{
					Manifest.Application = new Xsd.ManifestApplication();
				}
				if ( Manifest.Application.EntryPoint == null )
				{
					Manifest.Application.EntryPoint = new Xsd.EntryPoint();
				}
				Manifest.Application.EntryPoint.File = CleanQuotes( value );
				SetDirtyFlag();
			}
		}

		/// <summary>
		/// Gets or sets an string indicating the parameters to be passed to the application
		/// </summary>
		public string ApplicationEntryPointParams
		{
			get
			{
				return ( Manifest.Application == null  || Manifest.Application.EntryPoint == null ) 
					? null : Manifest.Application.EntryPoint.Parameters;
			}
			set
			{
				if ( Manifest.Application == null )
				{
					Manifest.Application = new Xsd.ManifestApplication();
				}
				if ( Manifest.Application.EntryPoint == null )
				{
					Manifest.Application.EntryPoint = new Xsd.EntryPoint();
				}
				Manifest.Application.EntryPoint.Parameters = CleanQuotes( value );
				SetDirtyFlag();
			}
		}

		#endregion

		#region Files edition

		/// <summary>
		/// Gets or sets the Base attribute for the manifest included files.
		/// It is the URI where the downloader should get the files from.
		/// </summary>
		public string FilesBase
		{
			get
			{
				if ( Manifest.Files == null || Manifest.Files.Base == null || Manifest.Files.Base.Length == 0 )
				{
					return String.Empty;
				}
				return Manifest.Files.Base;
			}
			set
			{
				if ( Manifest.Files != null )
				{
					Manifest.Files.Base = CleanQuotes( value );
				}
			}
		}

				
		/// <summary>
		/// Gets or sets the correspondig file base to the FilesBase defined on the
		/// manifest.
		/// </summary>
		public string LocalFilesBase
		{
			get
			{
				if ( localFilesBase == null )
				{
					localFilesBase = Path.GetFullPath( @".\" );
				}
				return localFilesBase;
			}
			set
			{

				if ( !Directory.Exists( value ) )
				{
					throw new ArgumentException( Resource.ResourceManager[ Resource.MessageKey.PathDoesNotExists, localFilesBase ], "LocalFilesBase" );
				}
				if ( !value.EndsWith( @"\" ) )
				{
					value = value + @"\";
				}

				value = CleanQuotes( value );

				if ( Manifest.Files != null && Manifest.Files.Files != null && Manifest.Files.Files.Length > 0 )
				{
					// When changing this value, we should validate/change that the files source 
					// so they are reachable from the new base path
					// A way to ensure this is only to allow changes from one path to any of its
					// parents paths. 
					if ( localFilesBase.StartsWith( value ) )
					{

						// Recompute files source attribute
						if ( Manifest.Files != null && manifest.Files.Files != null )
						{
							foreach( Xsd.ManifestFile xsdFile in Manifest.Files.Files )
							{
								xsdFile.Source = Path.Combine( localFilesBase, xsdFile.Source ).Replace( value, "" );
							}
						}
						localFilesBase = value;
						SetDirtyFlag();
					}
					else
					{
						throw new ManifestEditorException( Resource.ResourceManager[ Resource.MessageKey.ManifestEditorCannotChangeFolder ] );
					}
				}
				else
				{
					localFilesBase = value;
				}
			}
		}

		/// <summary>
		/// Adds a new non-transient file to list of manifest files
		/// </summary>
		/// <param name="source">The full path file to add.</param>
		public void AddFile( string source )
		{
			AddFile( source, Resource.ResourceManager[ Resource.MessageKey.NoString ] );
		}

		/// <summary>
		/// Add a file to the collection and set the transient flag.
		/// </summary>
		/// <param name="source">The full path file to add.</param>
		/// <param name="transient">The transient flag value.</param>
		public void AddFile( string source, string transient )
		{
			string filename = GetRelativeFileName( source );

			Xsd.ManifestFile file = FindFile( filename );
			if ( file != null )
			{
				return;
			}

			if ( Manifest.Files == null )
			{
				Manifest.Files = new Xsd.ManifestFiles();
			}
			file = new Xsd.ManifestFile();
			file.Source = filename;

			if ( Manifest.Files.Files == null )
			{
				Manifest.Files.Files = new Xsd.ManifestFile[1] { file };
			}
			else
			{
				ArrayList files = new ArrayList( Manifest.Files.Files );
				files.Add( file );
				Manifest.Files.Files = (Xsd.ManifestFile[])files.ToArray(typeof(Xsd.ManifestFile));
			}
			file.Transient = (Xsd.YesNoBoolType)Enum.Parse(typeof(Xsd.YesNoBoolType), transient, true);
			SetDirtyFlag();
		}

		/// <summary>
		/// Remove a file from the files collection.
		/// </summary>
		/// <param name="source">The full path of the file that will be deleted.</param>
		public void RemoveFile( string source )
		{
			if ( Manifest.Files == null || Manifest.Files.Files == null )
			{
				return;
			}

			string filename = GetRelativeFileName( source );
			ArrayList list = new ArrayList();
			foreach( Xsd.ManifestFile file in Manifest.Files.Files )
			{
				if ( String.Compare( filename, file.Source, true, CultureInfo.CurrentUICulture ) != 0 )
				{
					list.Add( file );
				}
			}
			Manifest.Files.Files = (Xsd.ManifestFile[])list.ToArray(typeof(Xsd.ManifestFile));
			SetDirtyFlag();
		}

		/// <summary>
		/// Set the transient flag for an existing file in the collection.
		/// </summary>
		/// <param name="source">The full path of the file to set.</param>
		/// <param name="value">The value of the transient flag.</param>
		public void SetFileTransient( string source, string value )
		{
			Xsd.ManifestFile file = FindFile( source );
			if ( file == null )
			{
				AddFile( source, value );
			}
			else
			{
				file.Transient = (Xsd.YesNoBoolType)Enum.Parse(typeof(Xsd.YesNoBoolType), value, true );
			}
			SetDirtyFlag();
		}

		/// <summary>
		/// Finds a file in the file collection.
		/// </summary>
		/// <param name="name">The name of the file that will be added.</param>
		/// <returns>The manifest file information.</returns>
		private Xsd.ManifestFile FindFile( string name )
		{
			if ( Manifest.Files == null || Manifest.Files.Files == null )
			{
				return null;
			}

			name = GetRelativeFileName( name );
			foreach( Xsd.ManifestFile file in Manifest.Files.Files )
			{
				if ( String.Compare( file.Source, name, true, CultureInfo.CurrentUICulture ) == 0 )
				{
					return file;
				}
			}
			return null;
		}

		/// <summary>
		/// Gets the files added to the collection.
		/// </summary>
		public ManifestFileItem[] ManifestFiles
		{
			get
			{
				if ( Manifest.Files == null || Manifest.Files.Files == null )
				{
					return null;
				}
				ManifestFileItem[] list = new ManifestFileItem[Manifest.Files.Files.Length];
				for(int i=0;i<Manifest.Files.Files.Length;i++)
				{
					list[i] = new ManifestFileItem( Manifest.Files.Files[i] );
				}
				return list;
			}
		}

		/// <summary>
		/// Loads the CryptographySettings from a SecurityCryptographyConfiguration file.
		/// </summary>
		/// <param name="filename">The path to file containing the security and cryptography configuration.</param>
		public void LoadCryptographyConfig( string filename )
		{
			XmlDocument doc = new XmlDocument();
			doc.Load( filename );
			XmlNode node = doc.SelectSingleNode( "securityCryptographyConfiguration/xmlSerializerSection" );
			if ( node != null )
			{
				node = node.FirstChild;
				XmlNodeReader nr = new XmlNodeReader( node );
				XmlSerializer ser = new XmlSerializer( typeof(CryptographySettings), "http://www.microsoft.com/practices/enterpriselibrary/08-31-2004/security/cryptography" );
				cryptoSettings = ser.Deserialize( nr ) as CryptographySettings;
			}
		}

		/// <summary>
		/// Gets the list of available hash providers to use with the files.
		/// </summary>
		public HashProviderDataCollection HashProviders
		{
			get 
			{ 
				return cryptoSettings == null ? null : cryptoSettings.HashProviders; 
			}
		}

		/// <summary>
		/// Gets or sets a flag indicating whether to apply a hashing algorithm
		/// to the files included in the manifest
		/// </summary>
		public bool UseHashingAlgorithm
		{
			set
			{
				if ( Manifest.Files == null )
				{
					Manifest.Files = new Xsd.ManifestFiles();
				}
				Manifest.Files.HashComparison = value ? Xsd.YesNoBoolType.Yes : Xsd.YesNoBoolType.No;
				SetDirtyFlag();
			}
			get
			{
				if ( Manifest.Files == null )
				{
					return false;
				}
				else
				{
					return Manifest.Files.HashComparison == Xsd.YesNoBoolType.Yes ||
						Manifest.Files.HashComparison == Xsd.YesNoBoolType.True;
				}
			}
		}

		/// <summary>
		/// Gets or sets the hash provider name to use to calculate the manifest files hashes
		/// </summary>
		public string HashProviderName
		{
			set
			{
				if ( Manifest.Files == null )
				{
					Manifest.Files = new Xsd.ManifestFiles();
				}
				Manifest.Files.HashProviderName = value;
			}
			get
			{
				if ( Manifest.Files == null )
				{
					return null;
				}
				return Manifest.Files.HashProviderName;
			}
		}

		/// <summary>
		/// Calculate the hashes for the files included in the manifests
		/// </summary>
		public void ComputeHashes()
		{
			if ( Manifest.Files == null )
			{
				return;
			}
			try
			{

				if ( UseHashingAlgorithm )
				{
					Manifest.Files.HashComparison = Xsd.YesNoBoolType.Yes;

					if ( HashProviderName != null && Manifest.Files.Files != null)
					{
						HashProviderData provider = HashProviders[ HashProviderName ];
						HashAlgorithm alg = HashAlgorithmFactory.Create( provider );
						foreach(Xsd.ManifestFile file in Manifest.Files.Files )
						{
							using(FileStream fs = File.Open( Path.Combine( LocalFilesBase, file.Source ), FileMode.Open, FileAccess.Read, FileShare.Read ) )
							{
								byte[] hash = alg.ComputeHash( fs );
								file.Hash = Convert.ToBase64String( hash );
							}
						}
					}

				}
				else
				{
					Manifest.Files.HashComparison = Xsd.YesNoBoolType.No;
					Manifest.Files.HashProviderName = null;
					if ( Manifest.Files.Files != null )
					{
						foreach(Xsd.ManifestFile file in Manifest.Files.Files )
						{
							file.Hash = null;
						}
					}
				}
			}
			catch( Exception ex )
			{
				throw new ManifestEditorException( null, ex );
			}
		}

		#endregion

		#region Processor edition

		/// <summary>
		/// Adds a new processor to the list of ActivationProcessors, without its type information
		/// </summary>
		/// <param name="name">The name assigned to the processor</param>
		/// <returns>The new ActivationProcessorProviderData instance</returns>
		public ProcessorItem AddProcessor( string name )
		{
			return AddProcessor( name, null );
		}

		/// <summary>
		/// Adds a new processor to the list of ActivationProcessors
		/// </summary>
		/// <param name="name">The name assigned to the processor</param>
		/// <param name="type">The type name implementing the processor</param>
		/// <returns>The new ActivationProcessorProviderData instance</returns>
		public ProcessorItem AddProcessor( string name, string type )
		{
			if ( Manifest.ActivationProcess == null )
			{
				Manifest.ActivationProcess = new Xsd.ActivationProcess();
			}

			string processorName = CleanQuotes( name );
			Xsd.ActivationProcessorProviderData processor = FindProcessor( processorName );
			if ( processor != null )
			{
				throw new ManifestEditorException( Resource.ResourceManager[ Resource.MessageKey.ProcessorAlreadyIncluded, processorName ] );
			}

			processor = new Xsd.ActivationProcessorProviderData();
			processor.Name = processorName;

			if ( Manifest.ActivationProcess.Tasks == null )
			{
				Manifest.ActivationProcess.Tasks = new Xsd.ActivationProcessorProviderData[1] { processor };
			}
			else
			{
				ArrayList processors = new ArrayList( Manifest.ActivationProcess.Tasks );
				processors.Add( processor );
				Manifest.ActivationProcess.Tasks = (Xsd.ActivationProcessorProviderData[])processors.ToArray(typeof(Xsd.ActivationProcessorProviderData));
			}
			
			if ( type != null )
			{
				processor.Type = GetProcessorType( type );
			}

			SetDirtyFlag();

			return ProcessorItemFactory.Create( processor );
		}

		/// <summary>
		/// Sets the processor type
		/// </summary>
		/// <param name="name">The name of the processor to set its type to</param>
		/// <param name="type">The type for the processor</param>
		public void SetProcessorType( string name, string type )
		{
			Xsd.ActivationProcessorProviderData processor = FindProcessor( name );
			if ( processor == null )
			{
				AddProcessor( name, type );
			}
			else
			{
				processor.Type = type;
			}
			SetDirtyFlag();
		}

		/// <summary>
		/// Removes a processor from the list of ActivationProcessors
		/// </summary>
		/// <param name="name">The name of the processor to remove</param>
		public void RemoveProcessor( string name )
		{
			if ( Manifest.ActivationProcess == null || Manifest.ActivationProcess.Tasks == null )
			{
				return;
			}
			ArrayList list = new ArrayList();
			foreach( Xsd.ActivationProcessorProviderData proc in Manifest.ActivationProcess.Tasks )
			{
				if ( String.Compare( name, proc.Name, true, CultureInfo.CurrentUICulture ) != 0 )
				{
					list.Add( proc );
				}
			}
			Manifest.ActivationProcess.Tasks = (Xsd.ActivationProcessorProviderData[])list.ToArray(typeof(Xsd.ActivationProcessorProviderData));
			SetDirtyFlag();			
		}


		/// <summary>
		/// Locates a processor by its name.
		/// </summary>
		/// <param name="name">The name for the processor to search for.</param>
		/// <returns>An ActivationProcessorProviderData instance or null.</returns>
		public Xsd.ActivationProcessorProviderData FindProcessor( string name )
		{
			if ( Manifest.ActivationProcess == null || Manifest.ActivationProcess.Tasks == null )
			{
				return null;
			}
			foreach( Xsd.ActivationProcessorProviderData processor in Manifest.ActivationProcess.Tasks )
			{
				if ( String.Compare( name, processor.Name, true, CultureInfo.CurrentUICulture ) == 0 )
				{
					return processor;
				}
			}
			return null;
		}


		/// <summary>
		/// Gets the list of ActivationProcessors included in the manifest
		/// </summary>
		public ProcessorItem[] ActivationProcessors
		{
			get
			{
				if ( Manifest.ActivationProcess == null || Manifest.ActivationProcess.Tasks == null )
				{
					return null;
				}
				ProcessorItem[] items = new ProcessorItem[Manifest.ActivationProcess.Tasks.Length];
				for(int i=0;i<Manifest.ActivationProcess.Tasks.Length;i++)
				{
					items[i] = ProcessorItemFactory.Create( Manifest.ActivationProcess.Tasks[i] );
				}
				return items;
			}
		}

		#endregion

		#region Downloader edition

		/// <summary>
		/// Gets or sets the downloader name
		/// </summary>
		public string DownloaderName
		{
			get
			{
				return Manifest.Downloader == null ? null : Manifest.Downloader.Name;
			}
			set
			{
				if ( Manifest.Downloader == null )
				{
					manifest.Downloader = new Xsd.ManifestDownloaderProviderData();
				}
				manifest.Downloader.Name = CleanQuotes( value );
			}
		}

		/// <summary>
		/// Gets or sets whether the manifest includes the downloader information
		/// </summary>
		public bool IncludeDownloader
		{
			get
			{
				return Manifest.Downloader != null;
			}
			set
			{
				if ( value && Manifest.Downloader == null )
				{
					Manifest.Downloader = new Xsd.ManifestDownloaderProviderData();
				}
				includeDownloader = value;
			}
		}

		#endregion

		#region Common commands

		/// <summary>
		/// Generates an XML representation for the manifest
		/// </summary>
		/// <returns>An string containing the XML that represents the manifest</returns>
		[SecurityPermission( SecurityAction.Demand, SerializationFormatter=true )]
		public string GetXml()
		{

			if (!includeDownloader)
			{
				manifest.Downloader = null;
			}

			XmlSerializer ser = new XmlSerializer( typeof( Xsd.Manifest ) );
			StringBuilder sb = new StringBuilder();
			StringWriter sw = new StringWriter( sb, CultureInfo.InvariantCulture );
			XmlTextWriter xtw = new XmlTextWriter( sw );
			xtw.Formatting = Formatting.Indented;
			ser.Serialize( xtw, Manifest );
			xtw.Close();
			return sb.ToString();
		}

		/// <summary>
		/// Saves the edited manifest to a file
		/// </summary>
		/// <param name="filename">The path to the file to save the manifest to</param>
		[SecurityPermission( SecurityAction.Demand, SerializationFormatter=true )]
		public void SaveToFile( string filename )
		{
			if (!includeDownloader)
			{
				manifest.Downloader = null;
			}

			XmlSerializer ser = new XmlSerializer( typeof( Xsd.Manifest ) );
			using( StreamWriter sw = new StreamWriter( filename ) )
			{
				ser.Serialize( sw, manifest );
			}
			ClearDirtyFlag();
		}

		/// <summary>
		/// Loads a manifest from a persisted file
		/// </summary>
		/// <param name="filename">The path to the file containing the manifest</param>
		[SecurityPermission( SecurityAction.Demand, SerializationFormatter=true )]
		public void LoadFromFile( string filename )
		{
			XmlSerializer ser = new XmlSerializer( typeof( Xsd.Manifest ) );
			using( StreamReader sr = new StreamReader( filename ) )
			{
				manifest = (Xsd.Manifest)ser.Deserialize( sr );
			}
			ClearDirtyFlag();
		}

		/// <summary>
		/// Initializes a new manifest
		/// </summary>
		public void New()
		{
			manifest = null;
			cryptoSettings = null;
			ClearDirtyFlag();
		}

		/// <summary>
		/// Gets an indication about the edited state of the manifest
		/// </summary>
		public bool IsDirty
		{
			get { return isDirty; }
		}

		/// <summary>
		/// Validates the editing manifest.
		/// </summary>
		/// <returns>A list of validation errors</returns>
		public string[] Validate()
		{
			string manifestXml = GetXml();

			Stream schema = Resource.ResourceManager.GetStream( "Microsoft.ApplicationBlocks.Updater.Xsd.Manifest.xsd" );
			if ( schema == null )
			{
				schema = Resource.ResourceManager.GetStream( "Manifest.xsd" );
			}
			if ( schema == null )
			{
				return new string[] { Resource.ResourceManager[ Resource.MessageKey.ToolCannotLoadManifestSchema ]  };
			}


			using( schema )
			{

				SchemaValidator validator = new SchemaValidator( manifestXml, schema );
				if ( !validator.Validate() )
				{
					string[] result = new string[ validator.Errors.Length ];
					int index = 0;
					foreach( ValidationEventArgs error in validator.Errors )
					{
						result[ index++ ] = error.Message;
					}
					return result;
				}
			}
			return null;
		}

		#endregion

	}
}
