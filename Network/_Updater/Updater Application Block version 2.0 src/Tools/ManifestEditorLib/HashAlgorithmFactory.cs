//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// HashAlgorithmFactory.cs
//
// Contains the implementation of the hash algorithm factory.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Security.Cryptography;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Creates EntLib hash algorithm instances.
	/// </summary>
	public sealed class HashAlgorithmFactory
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		private HashAlgorithmFactory()
		{
		}

		#endregion

		#region Static members

		/// <summary>
		/// Factory method for a HashAlgorithm provider.
		/// </summary>
		/// <param name="provider">The provider instance.</param>
		/// <returns>An instance of a HashAlgorithm that is ready to use.</returns>
		public static HashAlgorithm Create(HashProviderData provider )
		{
			if ( provider is KeyedHashAlgorithmProviderData )
			{
				KeyedHashAlgorithmProviderData data = provider as KeyedHashAlgorithmProviderData;
				Type t = Type.GetType( data.AlgorithmType, true );
				return System.Activator.CreateInstance( t, new object[] { data.Key } ) as HashAlgorithm;
			}
			else if ( provider is HashAlgorithmProviderData )
			{
				HashAlgorithmProviderData data = provider as HashAlgorithmProviderData;
				Type t = Type.GetType( data.AlgorithmType, true );
				return System.Activator.CreateInstance( t ) as HashAlgorithm;
			}

			throw new ManifestEditorException( Resource.ResourceManager[ Resource.MessageKey.CantCreateAlgorithmProvider, provider.Name ] );
		}

		#endregion
	}
}
