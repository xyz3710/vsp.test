//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// WaitForApplicationExitProcessorItem.cs
//
// Contains the implementation of the processor item for the WaitForApplicationExit Processor
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Globalization;
using System.Xml;
using Xsd = Microsoft.ApplicationBlocks.Updater.Xsd;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Editor wrapper for the Xsd.ActivationProcessorProviderData for the WaitForApplicationExitProcessorItem.
	/// </summary>
	public class WaitForApplicationExitProcessorItem : ProcessorItem
	{
		#region Constructor

		/// <summary>
		/// Initialize the new instance based on the provided data
		/// </summary>
		/// <param name="data">An  ActivationProcessorProviderData to wrap</param>
		public WaitForApplicationExitProcessorItem( Xsd.ActivationProcessorProviderData data ) : base( data )
		{
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		public WaitForApplicationExitProcessorItem( ) : base(  )
		{
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Gets or sets the processName attribute value
		/// </summary>
		public string ProcessName
		{
			get
			{
				return  GetAnyAttributeValue( "processName", String.Empty );
			}
			set
			{
				SetAnyAttributeValue( "processName", value );
			}
		}

		#endregion
	}
}
