//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// MSIProcessorItem.cs
//
// Contains the implementation of the processor item for the MSI Processor
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Xml;
using Xsd = Microsoft.ApplicationBlocks.Updater.Xsd;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Editor wrapper for the Xsd.ActivationProcessorProviderData for the MSIProcessorItem.
	/// </summary>
	public class MSIProcessorItem : ProcessorItem
	{
		#region Constructor

		/// <summary>
		/// Initialize the new instance based on the provided data
		/// </summary>
		/// <param name="data">An  ActivationProcessorProviderData to wrap</param>
		public MSIProcessorItem( Xsd.ActivationProcessorProviderData data ) : base( data )
		{
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		public MSIProcessorItem( ) : base(  )
		{
		}

		#endregion

		#region Overrides

		/// <summary>
		/// Ensures that the Any element for the configuration exists
		/// </summary>
		protected override void EnsureAny()
		{
			if ( data.Any == null )
			{
				data.Any = Factory.CreateElement( "config" );
			}
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Gets or sets the installation type to perform.
		/// </summary>
		public string InstallType
		{
			get
			{
				return GetAnyNodeText( "installType", "Install" );
			}
			set
			{
				if ( GetAnyNode( "installType" ) == null )
				{
					EnsureAny();
					data.Any.AppendChild( data.Any.OwnerDocument.CreateElement( "installType" ) );
				}
				SetAnyNodeText( "installType", value );			
			}
		}

		/// <summary>
		/// Gets or sets the msiUILevel value for the Windows Installer
		/// user interface interaction level.
		/// </summary>
		public string UILevel
		{
			get
			{
				return GetAnyNodeText( "uiLevel", String.Empty );
			}
			set
			{
				if ( GetAnyNode( "uiLevel" ) == null )
				{
					EnsureAny();
					data.Any.AppendChild( data.Any.OwnerDocument.CreateElement( "uiLevel" ) );
				}
				SetAnyNodeText( "uiLevel", value );
			}
		}

		/// <summary>
		/// Gets or sets the list of properties to pass to Windows Installer 
		/// when processing the .msi or .msp file.
		/// </summary>
		public string Properties
		{
			get
			{
				return GetAnyNodeText( "propertyValues", String.Empty );
			}
			set
			{
				if ( GetAnyNode( "propertyValues" ) == null )
				{
					EnsureAny();
					data.Any.AppendChild( data.Any.OwnerDocument.CreateElement( "propertyValues" ) );
				}
				SetAnyNodeText( "propertyValues", value );
			}
		}

		/// <summary>
		/// Gets or sets the path to the .msi or .msp package file.
		/// </summary>
		public string PackagePath
		{
			get
			{
				return GetAnyNodeText( "packagePath", String.Empty );
			}
			set 
			{
				if ( GetAnyNode( "packagePath" ) == null )
				{
					EnsureAny();
					data.Any.AppendChild( data.Any.OwnerDocument.CreateElement( "packagePath" ) );
				}
				SetAnyNodeText( "packagePath", value );
			}
		}

		/// <summary>
		/// Gets or sets the product code Windows Installer will remove.
		/// </summary>
		public string ProductCode
		{
			get
			{
				return GetAnyNodeText( "productCode", String.Empty );
			}
			set
			{
				if ( GetAnyNode( "productCode" ) == null )
				{
					EnsureAny();
					data.Any.AppendChild( data.Any.OwnerDocument.CreateElement( "productCode" ) );
				}
				SetAnyNodeText( "productCode", value );
			}
		}

		#endregion
	}
}
