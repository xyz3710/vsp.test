//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ManifestEditorException.cs
//
// Contains the implementation of the manifest editor exception.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Defines an exception for the manifest editor.
	/// </summary>
	[Serializable]
	public class ManifestEditorException : Exception 
	{
		#region Constructors

		/// <summary>
		/// Default constructor.
		/// </summary>
		public ManifestEditorException() : base()
		{
		}

		/// <summary>
		/// Creates a ManifestEditorException with a message.
		/// </summary>
		/// <param name="message">The message string.</param>
		public ManifestEditorException( string message ) : base( message )
		{
		}

		/// <summary>
		/// Creates a ManifestEditorException with a message and an inner exception.
		/// </summary>
		/// <param name="message">The message string.</param>
		/// <param name="inner">The inner exception instance.</param>
		public ManifestEditorException( string message, Exception inner ) : base( message, inner )
		{
		}

		#endregion
	}
}
