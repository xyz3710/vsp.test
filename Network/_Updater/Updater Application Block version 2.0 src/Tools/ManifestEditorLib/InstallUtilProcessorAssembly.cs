//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// InstallUtilProcessorAssembly.cs
//
// Contains the implementation of the assembly data for the InstallUtil Processor
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================
using System;
using System.Globalization;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Editor wrapper for the Xsd.ActivationProcessorProviderData for the InstallUtilProcessorAssembly.
	/// </summary>
	public class InstallUtilProcessorAssembly
	{
		#region Declarations

		private string path;
		private string options;

		#endregion

		#region Constructor

		/// <summary>
		/// Creates an initializes a new instance
		/// </summary>
		/// <param name="path">The path to the target assembly</param>
		/// <param name="options">The options to use when executing the InstallUtil tool</param>
		public InstallUtilProcessorAssembly( string path, string options)
		{
			this.path = path;
			this.options = options;
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Gets or sets the path to the target assembly
		/// </summary>
		public string Path
		{
			get	{ return path; }
			set { path = value; }
		}

		/// <summary>
		/// Gets or sets the options to use with InstallUtil
		/// </summary>
		public string Options
		{
			get { return options; }
			set { options = value; }
		}

		/// <summary>
		/// Gets a description for this instance
		/// </summary>
		public string Description
		{
			get { return String.Format( CultureInfo.InvariantCulture, "{0} {1}", options, path ); }
		}

		#endregion

	}
}
