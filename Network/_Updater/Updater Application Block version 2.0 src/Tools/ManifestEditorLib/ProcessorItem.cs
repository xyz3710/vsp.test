//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ProcessorItem.cs
//
// Contains the implementation of the generic processor item.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Collections;
using System.Globalization;
using System.Xml;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Defines a processor item in the manifest as a read only class.
	/// </summary>
	public class ProcessorItem
	{
		#region Private members

		/// <summary>
		/// The wrapped provider data
		/// </summary>
		protected Xsd.ActivationProcessorProviderData data;

		// An XmlDocument to use to create new nodes and attributes
		private XmlDocument factory;

		#endregion

		#region Constructor

		/// <summary>
		/// Default constructor
		/// </summary>
		public ProcessorItem()
		{
			data = new Xsd.ActivationProcessorProviderData();
		}

		/// <summary>
		/// Initialize the new instance based on the provided data
		/// </summary>
		/// <param name="data">The deserialized instance.</param>
		public ProcessorItem( Xsd.ActivationProcessorProviderData data )
		{
			this.data = data;
		}

		/// <summary>
		/// Creates a ProcessorItem with the name of the processor and the name of the type.
		/// </summary>
		/// <param name="name">The name of the processor.</param>
		/// <param name="typeName">Then type name of the processor.</param>
		public ProcessorItem( string name, string typeName) : this()
		{
			data.Name = name;
			data.Type = typeName;
		}

		/// <summary>
		/// Gets the XmlDocument to use to create nodes and/or attributes
		/// </summary>
		protected XmlDocument Factory
		{
			get
			{
				if ( factory == null )
				{
					factory = new XmlDocument();
				}
				return factory;
			}
		}

		#endregion

		#region Protected members

		/// <summary>
		/// Returns the value from an attribute from the processor configuration node
		/// </summary>
		/// <param name="name">The attribute name</param>
		/// <param name="defaultValue">A default value to return when the attribute is not found</param>
		/// <returns>The attribute value or defaultValue</returns>
		protected string GetAnyAttributeValue( string name, string defaultValue )
		{
			XmlAttribute attr = GetAnyAttribute( name );
			if ( attr != null )
			{
				return attr.Value;
			}
			return defaultValue;
		}

		/// <summary>
		/// Returns an attribute from the processor configuration node
		/// </summary>
		/// <param name="name">The attribute name</param>
		/// <returns>The requested attribute, or null if it does not exist</returns>
		protected XmlAttribute GetAnyAttribute( string name )
		{
			if ( data.AnyAttr != null )
			{
				foreach( XmlAttribute attr in data.AnyAttr )
				{
					if ( String.Compare( attr.LocalName, name, false, CultureInfo.InvariantCulture ) == 0 )
					{
						return attr;
					}
				}
			}
			return null;
		}

		/// <summary>
		/// Assigns the value for an attribute in the processor configuration node. If
		/// the attribute does not exist it will be created.
		/// </summary>
		/// <param name="name">The attribute name.</param>
		/// <param name="value">The new value for the attribute.</param>
		protected void SetAnyAttributeValue( string name, string value )
		{

			XmlAttribute attr = GetAnyAttribute( name );

			if ( attr == null )
			{
				ArrayList list = data.AnyAttr == null ? new ArrayList() : new ArrayList( data.AnyAttr );
				attr = Factory.CreateAttribute( name );
				list.Add( attr );
				data.AnyAttr = (XmlAttribute[])list.ToArray(typeof(XmlAttribute));
			}
			attr.Value = value;

		}


		/// <summary>
		/// Returns a child node of the processor configuration node
		/// </summary>
		/// <param name="xpath">The relative XPath to the child node</param>
		/// <returns>The located node or null</returns>
		protected XmlNode GetAnyNode( string xpath )
		{
			if ( data.Any != null )
			{
				return data.Any.SelectSingleNode( xpath );
			}
			return null;
		}

		/// <summary>
		/// Returns the text value from the designed node
		/// </summary>
		/// <param name="xpath">The relative XPath to the child node</param>
		/// <param name="defaultValue">The value to return if no node is found</param>
		/// <returns>The node InnerText property or the defaultValue</returns>
		protected string GetAnyNodeText( string xpath, string defaultValue )
		{
			XmlNode node = GetAnyNode( xpath );

			if ( node != null )
			{
				return node.InnerText;
			}
			return defaultValue;
		}

		/// <summary>
		/// Changes the node InnerText value
		/// </summary>
		/// <param name="xpath">The relative XPath to the child node</param>
		/// <param name="text">The new value for the InnerText property</param>
		protected void SetAnyNodeText( string xpath, string text )
		{
			XmlNode node = GetAnyNode( xpath );
			
			if ( node != null )
			{
				node.InnerText = text;
			}
		}

		/// <summary>
		/// Ensures that the AnyElement for the processor configuration node exists.
		/// Override on derived classes to change this node name.
		/// </summary>
		protected virtual void EnsureAny()
		{
			if ( data.Any == null )
			{
				data.Any = Factory.CreateElement( "anyElement" );
			}
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Gets the name for the processor
		/// </summary>
		public string Name
		{
			get { return data.Name; }
		}

		/// <summary>
		/// Gets the processor type name
		/// </summary>
		public string TypeName
		{
			get { return data.Type; }
		}

		/// <summary>
		/// Gets the description string for this item
		/// </summary>
		public string Description
		{
			get
			{
				return String.Format( CultureInfo.CurrentUICulture, "{0} - {1}", Name, TypeName );
			}
		}

		/// <summary>
		/// Gets the Any element
		/// </summary>
		public XmlElement Any
		{
			get
			{
				return data.Any;
			}
		}

		/// <summary>
		/// Gets the list of extra attributes
		/// </summary>
		public XmlAttribute[] AnyAttr
		{
			get
			{
				return data.AnyAttr;
			}
		}

		#endregion
	}
}
