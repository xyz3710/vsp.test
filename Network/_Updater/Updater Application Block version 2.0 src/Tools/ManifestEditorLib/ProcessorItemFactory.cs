//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ProcessorItemFactory.cs
//
// Contains the implementation of the processor item factory.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Security.Cryptography;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography;
using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography.Configuration;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Creates ProcessorItem instances.
	/// </summary>
	public sealed class ProcessorItemFactory
	{
		#region Constructor

		/// <summary>
		/// Default constructor. This class is not instantiable.
		/// </summary>
		private ProcessorItemFactory()
		{
		}

		#endregion

		#region Static members

		/// <summary>
		/// Factory method for a ProcessorItem
		/// </summary>
		/// <param name="data">The actiavtion processor provider instance.</param>
		/// <returns>An instance of the wrapper edition class to use.</returns>
		public static ProcessorItem Create( Xsd.ActivationProcessorProviderData data )
		{
			Type processorType = Type.GetType( data.Type );
			switch ( processorType.FullName )
			{
				case "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.MsiProcessor":
					return new MSIProcessorItem( data );
				case "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileCopyProcessor":
					return new FileCopyProcessorItem( data );
				case "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FileDeleteProcessor":
					return new FileDeleteProcessorItem( data );
				case "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FolderCopyProcessor":
					return new FolderCopyProcessorItem( data );
				case "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.FolderDeleteProcessor":
					return new FolderDeleteProcessorItem( data );
				case "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.GacUtilProcessor":
					return new GacUtilProcessorItem( data );
				case "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.InstallUtilProcessor":
					return new InstallUtilProcessorItem( data );
				case "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.UncompressProcessor":
					return new UncompressProcessorItem( data );
				case "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.ValidateHashProcessor":
					return new ValidateHashProcessorItem( data );
				case "Microsoft.ApplicationBlocks.Updater.ActivationProcessors.WaitForApplicationExitProcessor":
					return new WaitForApplicationExitProcessorItem( data );
				default:
					return new ProcessorItem( data );
			}
		}

		#endregion
	}
}
