//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// ManifestFileItem.cs
//
// Contains the implementation of the manifest file item information.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Xml;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Defines the manifest file item information as a read only class.
	/// </summary>
	public class ManifestFileItem 
	{
		#region Private members

		private Xsd.ManifestFile manifestFile;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a ManifestFileItem with the manifest file XSD.
		/// </summary>
		/// <param name="file">The manifest file instance.</param>
		public ManifestFileItem( Xsd.ManifestFile file)
		{
			manifestFile = file;
		}

		#endregion

		#region Public properties

		/// <summary>
		/// The source of the file.
		/// </summary>
		public string Source
		{
			get { return manifestFile.Source; }
		}

		/// <summary>
		/// Indicates whether the file is transient.
		/// </summary>
		public Xsd.YesNoBoolType Transient
		{
			get { return manifestFile.Transient; }
		}

		/// <summary>
		/// The calculated hash for the file.
		/// </summary>
		public string Hash
		{
			get { return manifestFile.Hash; } 
		}

		/// <summary>
		/// Any element for the file.
		/// </summary>
		public XmlElement Any
		{
			get { return manifestFile.Any; }
		}

		/// <summary>
		/// Any attribute for the file.
		/// </summary>
		public XmlAttribute[] AnyAttr
		{
			get { return manifestFile.AnyAttr; } 
		}

		#endregion
	}
}
