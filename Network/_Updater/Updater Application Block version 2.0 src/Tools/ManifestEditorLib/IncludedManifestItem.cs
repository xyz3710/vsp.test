//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// IncludedManifestItem.cs
//
// Contains the implementation of the included manifest item.
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Globalization;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Defines the included-manifest item as a read-only class.
	/// </summary>
	public class IncludedManifestItem
	{
		#region Private members

		private Xsd.IncludedManifest manifest;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates an IncludedManifestItem with an ID and the manifest location.
		/// </summary>
		/// <param name="id">The manifest ID.</param>
		/// <param name="location">The manifest location.</param>
		public IncludedManifestItem( string id, string location )
		{
			manifest = new Xsd.IncludedManifest();
			manifest.Location = location;
			manifest.ManifestId = id;
		}

		/// <summary>
		/// Creates an IncludedManifestItem with a deserialized included manifest class.
		/// </summary>
		/// <param name="manifest">The manifest deserialized instance.</param>
		public IncludedManifestItem( Xsd.IncludedManifest manifest )
		{
			this.manifest = manifest;
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Gets the location of the included manifest.
		/// </summary>
		public string Location
		{
			get
			{
				return manifest.Location;
			}
		}

		/// <summary>
		/// Gets the ID of the included manifest.
		/// </summary>
		public string ManifestId
		{
			get
			{
				return manifest.ManifestId;
			}
		}

		/// <summary>
		/// Gets a description for the included manifest.
		/// </summary>
		public string Description
		{
			get
			{
				return String.Format( CultureInfo.CurrentUICulture, "{0} - {1}", ManifestId, Location );
			}
		}

		#endregion
	}
}
