//============================================================================================================
// Microsoft Updater Application Block for .NET
//  http://msdn.microsoft.com/library/en-us/dnbda/html/updater.asp
//	
// InstallUtilProcessorItem.cs
//
// Contains the implementation of the processor item for the InstallUtil Processor
// 
// For more information see the Updater Application Block Implementation Overview. 
// 
//============================================================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//============================================================================================================

using System;
using System.Globalization;
using System.Xml;
using Xsd = Microsoft.ApplicationBlocks.Updater.Xsd;

namespace Microsoft.ApplicationBlocks.Updater.Tools
{
	/// <summary>
	/// Editor wrapper for the Xsd.ActivationProcessorProviderData for the InstallUtilProcessorItem.
	/// </summary>
	public class InstallUtilProcessorItem : ProcessorItem
	{

		#region Declarations

		// The list of assemblies to process with InstallUtil
		private InstallUtilProcessorAssembly[] assemblies;

		#endregion

		#region Constructor

		/// <summary>
		/// Initialize the new instance based on the provided data
		/// </summary>
		/// <param name="data">An  ActivationProcessorProviderData to wrap</param>
		public InstallUtilProcessorItem( Xsd.ActivationProcessorProviderData data ) : base( data )
		{
			if ( data.Any != null )
			{
				assemblies = new InstallUtilProcessorAssembly[ data.Any.ChildNodes.Count ];
				for(int i=0;i<data.Any.ChildNodes.Count;i++)
				{
					XmlNode node = data.Any.ChildNodes[ i ];
					assemblies[i] = new InstallUtilProcessorAssembly( node.Attributes["path"].Value, node.Attributes["options"].Value );
				}
			}
		}

		/// <summary>
		/// Default constructor
		/// </summary>
		public InstallUtilProcessorItem( ) : base(  )
		{
		}

		#endregion

		#region Public properties

		/// <summary>
		/// Gets or sets the action attribute value
		/// </summary>
		public string Action
		{
			get
			{
				return  GetAnyAttributeValue( "action", String.Empty );
			}
			set
			{
				SetAnyAttributeValue( "action", value );
			}
		}

		/// <summary>
		/// Gets or sets the list of assemblies to process
		/// </summary>
		public InstallUtilProcessorAssembly[] Assemblies
		{
			get
			{
				return assemblies;
			}
			set
			{
				assemblies = value;

				if ( data.Any == null )
				{
					data.Any = Factory.CreateElement( "assemblies" );
				}
				else
				{
					data.Any.InnerXml = String.Empty;
				}
				foreach( InstallUtilProcessorAssembly item in assemblies )
				{
					XmlNode node = data.Any.OwnerDocument.CreateElement( "assembly" );
					XmlAttribute attr = data.Any.OwnerDocument.CreateAttribute( "path" );
					attr.Value = item.Path;
					node.Attributes.Append( attr );

					attr = data.Any.OwnerDocument.CreateAttribute( "options" );
					attr.Value = item.Options;
					node.Attributes.Append( attr );

					data.Any.AppendChild( node );
				}
			}
		}

		#endregion
	}
}
