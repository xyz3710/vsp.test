using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using HttpAsyncDownload.AutoUpdater;
using System.Diagnostics; //프로그램실행

namespace HttpAsyncDownload
{
	/*
	 * 클래스명 : HttpAsyncDownloadControl
	 * 작성자   : 전성대 (danny@jinwooinfo.co.kr)
	 * 작성일   : 2004.05.07
	 * 기능     : Http로부터 파일을 다운로드 받는 UserControl
	 */

	/// <summary>
	/// HttpAsyncDownloadControl에 대한 요약 설명입니다.
	/// </summary>
	public class HttpAsyncDownloadControl : System.Windows.Forms.UserControl
	{
		delegate void BoolDelegate(bool value);
		delegate void VoidDelegate();
		
		private string[] downloadList; // 다운로드 리스트
		private long[] fileSizeList; // 파일 크기 리스트
		private string path; // 파일 저장 경로
		private System.Windows.Forms.TextBox txtUrl;
		private System.Windows.Forms.Button btnDownload;
		private StatusForm statusForm;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Label lebel1;
		private System.Windows.Forms.Label label1; // 상태표시 폼
		/// <summary> 
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public HttpAsyncDownloadControl()
		{
			// 이 호출은 Windows.Forms Form 디자이너에 필요합니다.
			InitializeComponent();

			// TODO: InitForm을 호출한 다음 초기화 작업을 추가합니다.
			path = Environment.CurrentDirectory;
		}

		/// <summary> 
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HttpAsyncDownloadControl));
			this.btnDownload = new System.Windows.Forms.Button();
			this.txtUrl = new System.Windows.Forms.TextBox();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.lebel1 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnDownload
			// 
			this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
			this.btnDownload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnDownload.Location = new System.Drawing.Point(67, 200);
			this.btnDownload.Name = "btnDownload";
			this.btnDownload.Size = new System.Drawing.Size(152, 38);
			this.btnDownload.TabIndex = 0;
			this.btnDownload.Text = "업그레이드시작";
			this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
			// 
			// txtUrl
			// 
			this.txtUrl.Location = new System.Drawing.Point(11, 279);
			this.txtUrl.Name = "txtUrl";
			this.txtUrl.Size = new System.Drawing.Size(272, 21);
			this.txtUrl.TabIndex = 1;
			this.txtUrl.Text = "http://localhost/HttpAsyncDownloadWS/data";
			this.txtUrl.Visible = false;
			this.txtUrl.TextChanged += new System.EventHandler(this.txtUrl_TextChanged);
			// 
			// listBox1
			// 
			this.listBox1.ItemHeight = 12;
			this.listBox1.Location = new System.Drawing.Point(8, 32);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(272, 160);
			this.listBox1.TabIndex = 2;
			// 
			// lebel1
			// 
			this.lebel1.Location = new System.Drawing.Point(8, 8);
			this.lebel1.Name = "lebel1";
			this.lebel1.Size = new System.Drawing.Size(40, 16);
			this.lebel1.TabIndex = 3;
			this.lebel1.Text = "버전 :";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(49, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 16);
			this.label1.TabIndex = 4;
			// 
			// HttpAsyncDownloadControl
			// 
			this.BackColor = System.Drawing.SystemColors.Window;
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lebel1);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.txtUrl);
			this.Controls.Add(this.btnDownload);
			this.Name = "HttpAsyncDownloadControl";
			this.Size = new System.Drawing.Size(296, 304);
			this.Load += new System.EventHandler(this.HttpAsyncDownloadControl_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void HttpAsyncDownloadControl_Load(object sender, System.EventArgs e)
		{
			CheckState();
		}

		private void txtUrl_TextChanged(object sender, System.EventArgs e)
		{
			CheckState();
		}

		public void GetFileList()
		{
			try
			{
				AutoUpdate autoupdate = new AutoUpdate();
				string txtUrlValue = autoupdate.UpdateCheckWS();
				string[] list = txtUrlValue.Split(',');
				label1.Text = list[0];
				for (int i=1; i<list.Length-1; i++)
				{
					listBox1.Items.Add(list[i]);
				}
			}
			catch (WebException wex)
			{

			}
			catch (Exception ex)
			{

			}
		}

		private void btnDownload_Click(object sender, System.EventArgs e)
		{
			//
			// 테스트 : 파일 다운로드 리스트. 텍스트 박스의 내용을 count번 다운받음
			//
			downloadList = new string[listBox1.Items.Count];
			
			for(int i=0;i<listBox1.Items.Count;i++)
			{
				downloadList[i] = txtUrl.Text + "/" + listBox1.Items[i].ToString();
			}

			if (downloadList == null || downloadList.Length == 0)
				throw new Exception("다운로드할 파일이 설정되지 않았습니다.");

			UpdateUI(false);
			ShowStatusForm();
			statusForm.SetButtonState(false);
			statusForm.SetLabelTitle("다운로드할 파일목록과 크기를 가져옵니다.");

			VoidDelegate vd = new VoidDelegate(BeginCalculateFileSize);
			vd.BeginInvoke(null, null);
		}

		private void BeginCalculateFileSize()
		{
			fileSizeList = new long[downloadList.Length];

			for (int i=0; i<downloadList.Length; i++)
			{
				WebRequest req = WebRequest.Create(downloadList[i]);
				WebResponse resp = req.GetResponse();
				fileSizeList[i] = resp.ContentLength;
				resp.Close();

				statusForm.SetLabelTitle(String.Format("{0}개의 파일의 크기 및 상태를 분석했습니다.", i+1));
			}

			statusForm.SetLabelTitle(String.Format("{0}개의 파일을 다운로드합니다.", downloadList.Length));
			statusForm.SetButtonState(true);

			long currSize = 0;

			for (int i=0; i<downloadList.Length; i++)
			{
				WebRequest req = WebRequest.Create(downloadList[i]);
				WebResponse resp = req.GetResponse();
				Stream stream = resp.GetResponseStream();
				FileStream fs = new FileStream(GetSavedPath() + GetFileName(i), FileMode.Create);

				long currSizeForThisFile = 0;
				const int LENGTH = 1024;
				const int OFFSET = 0;
				byte[] buffer = new byte[LENGTH];

				while (true)
				{
					int rcvd = stream.Read(buffer, OFFSET, LENGTH);

					if (rcvd > 0 && !enableExit)
					{
						fs.Write(buffer, 0, rcvd);

						currSize += rcvd;
						currSizeForThisFile += rcvd;
						statusForm.SetStatus(i+1, downloadList.Length, currSize, currSizeForThisFile,
							GetAllFileSize(), fileSizeList[i], GetFileName(i));
					}
					else
					{
						fs.Close();
						stream.Close();
						resp.Close();
						break;
					}
				}

				if (enableExit) break;
			}

			// 다운로드 완료
			CloseStatusForm();
			UpdateUI(true);
			ProgramStart();
			Application.Exit();
		}
		bool enableExit;

		public void ShowStatusForm()
		{
			statusForm = new StatusForm();
			statusForm.Owner = this.ParentForm;
			statusForm.DownlaodControl = this;
			statusForm.Show();
		}

		private void CheckState()
		{
			string identifier = "http://";

			if (txtUrl.Text.IndexOf(identifier) > -1)
				btnDownload.Enabled = true;
			else
				btnDownload.Enabled = false;
		}

		public void CloseStatusForm()
		{
			if (statusForm.InvokeRequired)
			{
				MethodInvoker mi = new MethodInvoker(CloseStatusForm);
				this.BeginInvoke(mi);
			}
			else
			{
				statusForm.Close();
				statusForm.Dispose();
				enableExit = false;
			}
		}

		public void UpdateUI(bool value)
		{
			if (InvokeRequired)
			{
				BoolDelegate del = new BoolDelegate(UpdateUI);
				this.BeginInvoke(del, new object[]{ value });
			}
			else
			{
				txtUrl.Enabled = btnDownload.Enabled = value;
			}
		}

		private long GetAllFileSize()
		{
			long totalSize = 0;

			if (fileSizeList == null)
				throw new Exception("파일들의 크기가 설정되지 않았습니다.");

			foreach (long size in fileSizeList)
				totalSize += size;

			return totalSize;
		}

		private string GetFileName(int index)
		{
			string identifier = "/";
			int startIndex = downloadList[index].LastIndexOf(identifier);
			return downloadList[index].Substring(startIndex + identifier.Length,
				downloadList[index].Length - startIndex - identifier.Length);
		}

		private string GetSavedPath()
		{
			return (path[path.Length-1] == '\\') ? path : path + "\\";
		}

		public bool EnableExit
		{
			get { return enableExit; }
			set { enableExit = value;}
		}

		public void ProgramStart()
		{
			Process myProcess = new Process();
			ProcessStartInfo  myProcessStartInfo = new ProcessStartInfo("DonorStart.exe");
			myProcess.StartInfo = myProcessStartInfo;
			myProcess.Start();
		}
	}
}
