using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace HttpAsyncDownload
{
	/*
	 * 클래스명 : StatusForm
	 * 작성자   : 전성대 (danny@jinwooinfo.co.kr)
	 * 작성일   : 2004.05.07
	 * 기능     : Http로부터 파일을 다운로드 받는 UserControl의 상태를 보여주는 Form 클래스
	 */

	/// <summary>
	/// StatusForm에 대한 요약 설명입니다.
	/// </summary>
	public class StatusForm : System.Windows.Forms.Form
	{
		delegate void SetStatusDelegate(int currCount, int totalcount, long currSize, long currSizeForThisFile, long totalSize, long currFileSize,  string currFileName);
		delegate void IntDelegate(int value);
		delegate void BoolDelegate(bool value);
		delegate void StringDelegate(string value);

		private bool enableExit;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.ProgressBar progressBarTotal;
		private System.Windows.Forms.Label labelTotal;
		private System.Windows.Forms.Label labelTitle;
		private System.Windows.Forms.ProgressBar progressBarFile;
		private System.Windows.Forms.Label labelFile;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public StatusForm()
		{
			this.enableExit = enableExit;
			//
			// Windows Form 디자이너 지원에 필요합니다.
			//
			InitializeComponent();

			//
			// TODO: InitializeComponent를 호출한 다음 생성자 코드를 추가합니다.
			//
		}

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.progressBarTotal = new System.Windows.Forms.ProgressBar();
			this.labelTitle = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.progressBarFile = new System.Windows.Forms.ProgressBar();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.labelFile = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.labelTotal = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// progressBarTotal
			// 
			this.progressBarTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.progressBarTotal.Location = new System.Drawing.Point(48, 72);
			this.progressBarTotal.Name = "progressBarTotal";
			this.progressBarTotal.Size = new System.Drawing.Size(400, 10);
			this.progressBarTotal.TabIndex = 0;
			// 
			// labelTitle
			// 
			this.labelTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.labelTitle.Location = new System.Drawing.Point(8, 8);
			this.labelTitle.Name = "labelTitle";
			this.labelTitle.Size = new System.Drawing.Size(442, 12);
			this.labelTitle.TabIndex = 1;
			this.labelTitle.Text = "다운로드창";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.Window;
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.progressBarTotal);
			this.panel1.Controls.Add(this.progressBarFile);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.labelFile);
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Controls.Add(this.labelTotal);
			this.panel1.Controls.Add(this.labelTitle);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(458, 135);
			this.panel1.TabIndex = 3;
			// 
			// progressBarFile
			// 
			this.progressBarFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.progressBarFile.Location = new System.Drawing.Point(48, 48);
			this.progressBarFile.Name = "progressBarFile";
			this.progressBarFile.Size = new System.Drawing.Size(400, 10);
			this.progressBarFile.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Name = "label2";
			this.label2.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 72);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(33, 14);
			this.label1.TabIndex = 6;
			this.label1.Text = "전체:";
			// 
			// labelFile
			// 
			this.labelFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.labelFile.Location = new System.Drawing.Point(8, 26);
			this.labelFile.Name = "labelFile";
			this.labelFile.Size = new System.Drawing.Size(444, 15);
			this.labelFile.TabIndex = 5;
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(188, 104);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 25);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "취소(&C)";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// labelTotal
			// 
			this.labelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.labelTotal.Location = new System.Drawing.Point(8, 88);
			this.labelTotal.Name = "labelTotal";
			this.labelTotal.Size = new System.Drawing.Size(444, 15);
			this.labelTotal.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 48);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(33, 14);
			this.label3.TabIndex = 7;
			this.label3.Text = "파일:";
			// 
			// StatusForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(458, 135);
			this.ControlBox = false;
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "StatusForm";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "다운로드 진행창";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.StatusForm_Closing);
			this.Load += new System.EventHandler(this.StatusForm_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// 현재의 상태를 설정합니다.
		/// </summary>
		/// <param name="currCount">현재의 진행 카운트</param>
		/// <param name="totalcount">전체 카운트</param>
		/// <param name="currSize">현재의 진행중인 사이즈</param>
		/// <param name="currFileSize">현재의 진행중인 파일 사이즈</param>
		/// <param name="totalSize">전체 사이즈</param>
		/// <param name="currFileName">현재의 진행중인 파일명</param>
		public void SetStatus(int currCount, int totalCount, long currSize, long currSizeForThisFile,
			long totalSize, long currFileSize,  string currFileName)
		{
			int totalPercent = (int)(100 * currSize / totalSize);
			int currPercent = (int)(100 * currSizeForThisFile / currFileSize);

			SetLabelFile(String.Format("{0} 파일 다운로드중. {1}% 완료", currFileName, currPercent));

			SetLabelTotal(String.Format("전체 {0}% 완료. ({1}/{2}개 진행중) {3}/{4} KByte",
				totalPercent, currCount, totalCount, currSize, totalSize));

			SetProgressBarFile(currPercent);

			SetProgressBarTotal(totalPercent);
		}

		private void SetLabelTotal(string value)
		{
			if (labelTotal.InvokeRequired)
			{
				StringDelegate del = new StringDelegate(SetLabelTotal);
				this.BeginInvoke(del, new object[]{ value });
			}
			else
			{
				labelTotal.Text = value;
			}
		}

		private void SetLabelFile(string value)
		{
			if (labelFile.InvokeRequired)
			{
				StringDelegate del =new StringDelegate(SetLabelFile);
				this.BeginInvoke(del, new object[]{ value });
			}
			else
			{
				labelFile.Text = value;
			}
		}

		private void SetProgressBarTotal(int value)
		{
			if (progressBarTotal.InvokeRequired)
			{
				IntDelegate del = new IntDelegate(SetProgressBarTotal);
				this.BeginInvoke(del, new object[]{ value });
			}
			else
			{
				progressBarTotal.Value = value;
			}
		}
		
		private void SetProgressBarFile(int value)
		{
			if (progressBarFile.InvokeRequired)
			{
				IntDelegate del = new IntDelegate(SetProgressBarFile);
				this.BeginInvoke(del, new object[]{ value });
			}
			else
			{
				progressBarFile.Value = value;
			}
		}

		/// <summary>
		/// 상단 레이블의 문자열을 설정합니다.
		/// </summary>		
		public void SetLabelTitle(string value)
		{
			if (labelTitle.InvokeRequired)
			{
				StringDelegate del = new StringDelegate(SetLabelTitle);
				this.BeginInvoke(del, new object[]{ value });
			}
			else
			{
				labelTitle.Text = value;
			}
		}

		/// <summary>
		/// 버튼의 상태를 설정합니다.
		/// </summary>
		/// <param name="enabled"></param>
		public void SetButtonState(bool value)
		{
			if (btnCancel.InvokeRequired)
			{
				BoolDelegate del = new BoolDelegate(SetButtonState);
				this.BeginInvoke(del, new object[]{ value });
			}
			else
			{
				btnCancel.Enabled = value;
			}
		}

		private void StatusForm_Load(object sender, System.EventArgs e)
		{
			if (Owner != null)
			{
				// Owner폼위에 모달리스(Show())로 한가운데에 폼을 띄우기
				// 단, Owner 폼이 스크린의 바깥쪽에 있더라도 이 폼은 화면에 완전히 보여야 한다.
				// 참고. 모달(ShowDialog())로 띄울 경우는 OS가 알아서 처리해 준다.
				int x = Owner.Location.X + (Owner.Width - this.Width) / 2;
				int y = Owner.Location.Y + (Owner.Height - this.Height) / 2;
				x = Math.Min(Screen.PrimaryScreen.WorkingArea.Width - this.Width, x);
				y = Math.Min(Screen.PrimaryScreen.WorkingArea.Height - this.Height, y);
				this.Location = new Point(Math.Max(0, x), Math.Max(0, y));
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			btnCancel.Enabled = false;
			downloadContrl.EnableExit = true;
		}

		private void StatusForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
		}

		HttpAsyncDownloadControl downloadContrl;
		public HttpAsyncDownloadControl DownlaodControl
		{
			get { return downloadContrl; }
			set { downloadContrl = value;}
		}
	}
}
