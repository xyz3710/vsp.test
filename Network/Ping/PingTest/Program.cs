using System;
using System.Collections.Generic;
using System.Text;
using System.Management;
using System.Net.NetworkInformation;
using System.Threading;

namespace PingTester
{
	class PingTestDriver
	{
		private static DateTime _now;

		static void Main(string[] args)
		{
			List<string> hosts = new List<string>();

			//hosts.Add("192.168.25.10");
			hosts.Add("192.168.25.30");
			//hosts.Add("192.168.27.30");
			hosts.Add("192.168.25.28");

			DateTime testStartTime = DateTime.Now;

			PrintTime(testStartTime, "전체 테스트 시작");

			foreach (string host in hosts)
				PingTest(host);

			PrintTime(DateTime.Now- testStartTime, "전체 테스트 완료");
		}

		private static void PingTest(string hostName)
		{
			int pingTimeOut = 800;
			int cancelTimeOut = 1000;
			int mainThreadTimeOutAddTime = 500;

			AutoResetEvent waiter = new AutoResetEvent(false);
			Ping pingSender = new Ping();
			string data = "12345678901234567890123456789012";
			byte[] buffer = Encoding.ASCII.GetBytes(data);
			PingOptions options = new PingOptions(64, true);

			pingSender.PingCompleted += new PingCompletedEventHandler(pingSender_PingCompleted);

			Console.WriteLine("Ping TimeOut\t\t: {0}ms", pingTimeOut);
			Console.WriteLine("Cancel TimeOut\t\t: {0}ms", cancelTimeOut);
			Console.WriteLine("Main Thread TimeOut\t: {0}ms", pingTimeOut + mainThreadTimeOutAddTime);
			_now = DateTime.Now;
			PrintTime("테스트 시작");

			pingSender.SendAsync(hostName, pingTimeOut, buffer, options, waiter);

			// cancelTimeOut초 후에 Ping 취소한다.
			//Thread.Sleep(cancelTimeOut);
			//pingSender.SendAsyncCancel();

			//waiter.WaitOne(pingTimeOut + mainThreadTimeOutAddTime, false);
			waiter.WaitOne();
			PrintTime("메인 스레드 종료");
		}

		private static void PrintTime(string message)
		{
			Console.WriteLine(string.Empty.PadRight(79, '-'));
			Console.WriteLine("\t{0} <= {1}", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff"), message);
			Console.WriteLine("\t{0} <= 소요 시간", DateTime.Now - _now);
			_now = DateTime.Now;
			Console.WriteLine(string.Empty.PadRight(79, '-'));
		}

		private static void PrintTime(object spanTime, string message)
		{
			Console.WriteLine(string.Empty.PadRight(79, '*'));
			Console.WriteLine("{0} <= {1}", spanTime, message);
			Console.WriteLine(string.Empty.PadRight(79, '*'));
		}

        static void pingSender_PingCompleted(object sender, PingCompletedEventArgs e)
		{
			if (e.Cancelled == true)
			{
				PrintTime("테스트 취소");
				Console.WriteLine("테스트가 취소 되었습니다..");
			}
			else if (e.Error != null)
            {
				PrintTime("테스트 오류");
				Console.WriteLine("테스트가 아래와 같은 이유로 실패 하였습니다.");
				Console.WriteLine(e.Error.Message);
            }
			else
			{
				DisplayReply(e.Reply);

				// main thread가 작동 하도록 한다.
				((AutoResetEvent)e.UserState).Set();

				PrintTime("테스트 완료");
				Console.WriteLine("테스트가 완료 되었습니다.");
			}
		}

		private static void DisplayReply(PingReply reply)
		{
			if (reply == null)
				return;

			Console.WriteLine("Ping Reply Status : {0}", reply.Status.ToString());

			Console.WriteLine("Target Address\t : {0}", reply.Address.ToString());
			Console.WriteLine("RouteTrip time\t : {0}", reply.RoundtripTime);

			if (reply.Options != null)
			{
				Console.WriteLine("Time to Live\t : {0}", reply.Options.Ttl);
				Console.WriteLine("Don't Fragment\t : {0}", reply.Options.DontFragment);
			}

			Console.WriteLine("Buffer Size\t : {0}", reply.Buffer.Length > 0 ? reply.Buffer.Length : byte.MinValue);
		}
	}
}

