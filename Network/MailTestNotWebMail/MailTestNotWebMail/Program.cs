﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Mail;
using System.IO;

namespace MailTestNotWebMail
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] files = Directory.GetFiles(@"B:\Test\_Network\MailTestNotWebMail\MailTestNotWebMail", "*.cs");
			//MailHelper.Send(
			//	from: "notification.remes@ocic.co.kr",
			//	to: "ckb@ocic.co.kr;xyz37@ocic.co.kr",
			//	subject: "Mail test",
			//	body: "TestBody",
			//	//cc: "",
			//	filesToAttach: files
			//	);
			MailHelper.SendAsync(
				from: "notification.remes@ocic.co.kr",
				to: "xyz37@ocic.co.kr",
				subject: "Mail test",
				body: "TestBody",
				cc: "",
				filesToAttach: files
			);

			Console.ReadLine();
		}
	}
}
