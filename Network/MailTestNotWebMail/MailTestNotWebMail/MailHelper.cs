﻿/**********************************************************************************************************************/
/*	Domain		:	Common.Mail.MailSender
/*	Creator		:	X10\xyz37(김기원)
/*	Create		:	2011년 10월 3일 월요일 오전 12:36
/*	Purpose		:	메일 전송과 관련된 기능을 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	From을 지정할 경우 보낸사람이 변경되지 않는 문제는 있다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 10월 3일 월요일 오후 4:26
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Common.Mail
{
	/// <summary>
	/// 메일 전송과 관련된 기능을 제공합니다.
	/// </summary>
	public static class MailHelper
	{
		/// <summary>
		/// 에러 발생시 사용할 제목 문자열 입니다.
		/// </summary>
		public const string SubjectFormatStringInError = "리스닝 매직 - 오류 알림({0}) : {1}";

		static MailHelper()
		{
			SmtpServer = ConfigurationManager.AppSettings["SmtpServer"];
			SmtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
			EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
			UserName = ConfigurationManager.AppSettings["UserName"];
			Password = ConfigurationManager.AppSettings["Password"];
			SmtpUseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpUseDefaultCredentials"]);

			InitWebMailHelper();
		}

		private static void InitWebMailHelper()
		{
			// Init WebMail helper
			//WebMail.SmtpServer = SmtpServer;
			//WebMail.SmtpPort = SmtpPort;
			//WebMail.EnableSsl = EnableSsl;
			//WebMail.UserName = UserName;
			//WebMail.SmtpUseDefaultCredentials = SmtpUseDefaultCredentials;
			//WebMail.Password = Password;
		}

		#region Properties
		/// <summary>
		/// SmtpServer를 구하거나 설정합니다.
		/// </summary>
		public static string SmtpServer
		{
			get;
			set;
		}

		/// <summary>
		/// SmtpPort를 구하거나 설정합니다.
		/// </summary>
		public static int SmtpPort
		{
			get;
			set;
		}

		/// <summary>
		/// SSL 사용여부를 구하거나 설정합니다.
		/// </summary>
		public static bool EnableSsl
		{
			get;
			set;
		}

		/// <summary>
		/// SmtpServer의 사용자 이름을 구하거나 설정합니다.
		/// </summary>
		public static string UserName
		{
			get;
			set;
		}

		/// <summary>
		/// SmtpServer의 사용자 암호를 구하거나 설정합니다.
		/// </summary>
		public static string Password
		{
			get;
			set;
		}

		/// <summary>
		/// SmtpServer의 기본 자격증명을 사용할지 여부를 구하거나 설정합니다.
		/// </summary>
		public static bool SmtpUseDefaultCredentials
		{
			get;
			set;
		}

		/// <summary>
		/// 보낸 사람 Email을 구하거나 설정합니다.
		/// </summary>
		public static string From
		{
			get;
			set;
		}
		#endregion

		/// <summary>
		/// 비동기 방식을 사용하여 배달용 SMTP 서버로 지정된 메시지를 보냅니다.
		/// </summary>
		/// <param name="to">이 전자 메일 메시지를 받는 사람입니다.<br/>받는 사람을 세미콜론으로 구분하여 여러 명을 지정할 수도 있습니다.</param>
		/// <param name="subject">전자 메일 메시지의 제목 줄입니다.</param>
		/// <param name="body">전자 메일 메시지의 본문입니다. isBodyHtml 값이 true인 경우 본문의 HTML이 태그로 해석됩니다.</param>
		/// <param name="from">메시지를 보낸 사람의 전자 메일 주소입니다.</param>
		/// <param name="cc">전자 메일 메시지의 참조(CC) 수신자입니다(있을 경우).<br/>받는 사람을 세미콜론으로 구분하여 여러 명을 지정할 수도 있습니다.</param>
		/// <param name="bcc">전자 메일 메시지의 숨은 참조(BCC) 수신자입니다(있을 경우).<br/>받는 사람을 세미콜론으로 구분하여 여러 명을 지정할 수도 있습니다.</param>
		/// <param name="filesToAttach">전자 메일 메시지에 첨부할 파일을 지정하는 파일 이름의 컬렉션입니다.</param>
		/// <param name="isBodyHtml">전자 메일 메시지 본문을 HTML 형식으로 지정하려면 true이고, 일반 텍스트 형식으로 지정하려면 false입니다.</param>
		/// <param name="additionalHeaders">이 전자 메일 메시지와 함께 전송되는 헤더의 컬렉션입니다.</param>
		public static void SendAsync(
			string to,
			string subject,
			string body,
			string from = null,
			string cc = null,
			string bcc = null,
			IEnumerable<string> filesToAttach = null,
			bool isBodyHtml = true,
			IEnumerable<string> additionalHeaders = null)
		{
			InitWebMailHelper();
			Validate(filesToAttach, additionalHeaders);

			MailMessage message = new MailMessage();
			SetPropertiesOnMessage(message, to, subject, body, from, cc, bcc, filesToAttach, isBodyHtml, additionalHeaders);
			Object state = message;

			SmtpClient client = new SmtpClient();
			SetPropertiesOnClient(client);
			client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);

			try
			{
				client.SendAsync(message, state);
			}
			catch
			{
			}
		}

		private static void client_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
		{
			MailMessage mail = e.UserState as MailMessage;

			if (e.Cancelled == true && e.Error == null)
				// 성공적으로 발송시에는 mail 객체를 삭제한다.
				mail.Dispose();
		}

		/// <summary>
		/// 배달용 SMTP 서버로 지정된 메시지를 보냅니다.
		/// </summary>
		/// <param name="to">이 전자 메일 메시지를 받는 사람입니다.<br/>받는 사람을 세미콜론으로 구분하여 여러 명을 지정할 수도 있습니다.</param>
		/// <param name="subject">전자 메일 메시지의 제목 줄입니다.</param>
		/// <param name="body">전자 메일 메시지의 본문입니다. isBodyHtml 값이 true인 경우 본문의 HTML이 태그로 해석됩니다.</param>
		/// <param name="from">메시지를 보낸 사람의 전자 메일 주소입니다.</param>
		/// <param name="cc">전자 메일 메시지의 참조(CC) 수신자입니다(있을 경우).<br/>받는 사람을 세미콜론으로 구분하여 여러 명을 지정할 수도 있습니다.</param>
		/// <param name="bcc">전자 메일 메시지의 숨은 참조(BCC) 수신자입니다(있을 경우).<br/>받는 사람을 세미콜론으로 구분하여 여러 명을 지정할 수도 있습니다.</param>
		/// <param name="filesToAttach">전자 메일 메시지에 첨부할 파일을 지정하는 파일 이름의 컬렉션입니다.</param>
		/// <param name="isBodyHtml">전자 메일 메시지 본문을 HTML 형식으로 지정하려면 true이고, 일반 텍스트 형식으로 지정하려면 false입니다.</param>
		/// <param name="additionalHeaders">이 전자 메일 메시지와 함께 전송되는 헤더의 컬렉션입니다.</param>
		/// <remarks>보낼 때 오류는 전달하지 않습니다.</remarks>
		public static void Send(
			string to,
			string subject,
			string body,
			string from = null,
			string cc = null,
			string bcc = null,
			IEnumerable<string> filesToAttach = null,
			bool isBodyHtml = true,
			IEnumerable<string> additionalHeaders = null)
		{
			InitWebMailHelper();
			Validate(filesToAttach, additionalHeaders);

			using (MailMessage message = new MailMessage())
			{
				SetPropertiesOnMessage(message, to, subject, body, from, cc, bcc, filesToAttach, isBodyHtml, additionalHeaders);

				SmtpClient client = new SmtpClient();

				SetPropertiesOnClient(client);

				try
				{
					client.Send(message);
				}
				catch
				{
				}
			}
		}

		private static void Validate(IEnumerable<string> filesToAttach, IEnumerable<string> additionalHeaders)
		{
			if (filesToAttach != null)
			{
				foreach (string attachedFile in filesToAttach)
					if (string.IsNullOrEmpty(attachedFile) == true)
						throw new ArgumentException("filesToAttach의 목록이 null 입니다.");
			}

			if (additionalHeaders != null)
			{
				foreach (string additionalHeader in additionalHeaders)
					if (string.IsNullOrEmpty(additionalHeader) == true)
						throw new ArgumentException("additionalHeaders의 목록이 null 입니다.");
			}

			if (string.IsNullOrEmpty(SmtpServer) == true)
				throw new InvalidOperationException("SmtpServer가 지정되지 않았습니다.");
		}

		private static void SetEmailBySemicolon(this MailAddressCollection collection, string addressCollectionString)
		{
			string[] split = addressCollectionString.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

			foreach (string item in split)
				collection.Add(item);
		}

		private static void SetPropertiesOnMessage(
			MailMessage message,
			string to,
			string subject,
			string body,
			string from,
			string cc,
			string bcc,
			IEnumerable<string> filesToAttach,
			bool isBodyHtml,
			IEnumerable<string> additionalHeaders)
		{
			message.Subject = subject;
			message.Body = body;
			message.IsBodyHtml = isBodyHtml;

			if (to != null)
				message.To.SetEmailBySemicolon(to);

			if (string.IsNullOrEmpty(cc) == false)
				message.CC.SetEmailBySemicolon(cc);

			if (string.IsNullOrEmpty(bcc) == false)
				message.Bcc.SetEmailBySemicolon(bcc);

			if (additionalHeaders != null)
			{
				foreach (string additionalHeader in additionalHeaders)
				{
					int index = additionalHeader.IndexOf(':');

					if (index > 0)
					{
						string name = additionalHeader.Substring(0, index).TrimEnd(new char[0]);

						message.Headers.Add(name, additionalHeader.Substring(index + 1).TrimStart(new char[0]));
					}
				}
			}

			if (from != null)
				message.From = new MailAddress(from);
			else if (string.IsNullOrEmpty(From) == false)
				message.From = new MailAddress(From);
			else if ((message.From == null) || string.IsNullOrEmpty(message.From.Address))
			{
				//HttpContext current = HttpContext.Current;

				//if (current == null)
				//	throw new InvalidOperationException("From 사용자를 지정할 수 없습니다.");

				//message.From = new MailAddress("DoNotReply@" + current.Request.Url.Host);
			}

			if (filesToAttach != null)
			{
				foreach (string attachedFile in filesToAttach)
				{
					//if (Path.IsPathRooted(attachedFile) == false && (HttpRuntime.AppDomainAppPath != null))
					//	message.Attachments.Add(new Attachment(Path.Combine(HttpRuntime.AppDomainAppPath, attachedFile)));
					//else
					message.Attachments.Add(new Attachment(attachedFile));
				}
			}
		}

		private static void SetPropertiesOnClient(SmtpClient client)
		{
			if (SmtpServer != null)
				client.Host = SmtpServer;

			client.Port = SmtpPort;
			client.UseDefaultCredentials = SmtpUseDefaultCredentials;
			client.EnableSsl = EnableSsl;

			if (string.IsNullOrEmpty(UserName) == false)
				client.Credentials = new NetworkCredential(UserName, Password);
		}
	}
}