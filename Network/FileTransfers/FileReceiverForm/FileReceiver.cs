﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace GS.Common.Net
{
	/// <summary>
	/// File 전송용 수신자 class 입니다.
	/// </summary>
	[Serializable]
	public partial class FileReceiver
	{
		private const int DEFAULT_PORT = 19000;
		private const int SEGMENT_SIZE = 1024;
		private const int HEADER_SIZE = 12;		// 첫번째 8byte 데이터 길이, 두번째 4byte File 이름 길이
		private const int INDEX_HEADER_SIZE = 12;		// 전체 파일 갯수(int), 전체 파일 크기(long)

		[Serializable]
		private class StartServerParameter
		{
			public StartServerParameter(bool appendable, int listenerCount)
			{
				ListenerCount = listenerCount;
				Appendable = appendable;
			}

			public int ListenerCount
			{
				get;
				set;
			}

			public bool Appendable
			{
				get;
				set;
			}
		}

		#region Fields
		private Socket _socket;
		private string _receivedPath;
		private bool _completed;
		private bool _closeAfterReceive;
		private int _completedCount;
		private int _closeAfterReceiveReadyInterval;
		private System.Timers.Timer _timer;
		private byte[] _ack;
		#endregion

		#region Constructors
		/// <summary>
		/// FileReceiver class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="port">The port.</param>
		/// <param name="bufferSize">Size of the buffer.</param>
		public FileReceiver(int port = DEFAULT_PORT, int bufferSize = SEGMENT_SIZE * 512)
		{
			Port = port;
			BufferSize = bufferSize;

			Bind(port);
			_receivedPath = string.Empty;
			_ack = new byte[] { 0x06 };
		}
		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets the received path.
		/// </summary>
		/// <value>The received path.</value>
		public string ReceivedPath
		{
			get
			{
				return _receivedPath;
			}
			set
			{
				_receivedPath = value;

				if (Directory.Exists(_receivedPath) == false)
				{
					Directory.CreateDirectory(_receivedPath);
				}
			}
		}

		/// <summary>
		/// 다중으로 전송 받을 경우 중복된 파일들을 분류하기 위해 ReceivedPath에 Remote Client의 IpAddress를 폴더명을 추가한다.
		/// </summary>
		/// <value>true if [received path with remote ip address]; otherwise, false.</value>
		public bool ReceivedPathWithRemoteIpAddress
		{
			get;
			set;
		}

		/// <summary>
		/// 다중으로 전송 받을 경우 중복된 파일들을 분류하기 위해 ReceivedPath에 받는 날짜 폴더명을 추가한다.
		/// </summary>
		/// <value>true if [received path with date]; otherwise, false.</value>
		public bool ReceivedPathWithDate
		{
			get;
			set;
		}

		/// <summary>
		/// Port를 구하거나 설정합니다.
		/// </summary>
		/// <value>The port.</value>
		public int Port
		{
			get;
			set;
		}

		/// <summary>
		/// BufferSize를 구하거나 설정합니다.
		/// <remarks>기본 크기는 1024 * 512 Byte 입니다.</remarks>
		/// </summary>
		/// <value>The size of the buffer.</value>
		public int BufferSize
		{
			get;
			set;
		}

		/// <summary>
		/// Socket에 특정 Port를 성공적으로 Binding되었는지 여부를 구합니다.
		/// </summary>
		/// <value>true if this instance is bound; otherwise, false.</value>
		public bool IsBound
		{
			get;
			private set;
		}

		/// <summary>
		/// 전송한 뒤 Service를 중지 시킬지 여부를 결정합니다.
		/// </summary>
		/// <value>true if [close after receive]; otherwise, false.</value>
		public bool CloseAfterReceive
		{
			private get
			{
				return _closeAfterReceive;
			}
			set
			{
				_closeAfterReceive = value;

				// 특정 interval동안 Receiving을 처리한다.
				if (value == true && _timer == null)
				{
					_timer = new System.Timers.Timer();
					_timer.AutoReset = true;
					_timer.Interval = CloseAfterReceiveReadyInterval;
					_timer.Elapsed += new System.Timers.ElapsedEventHandler(OnIdleReceive);
				}
			}
		}

		/// <summary>
		/// Service가 현재 시작중인지 여부를 구합니다.
		/// </summary>
		/// <value>true if running; otherwise, false.</value>
		public bool Running
		{
			get;
			private set;
		}

		/// <summary>
		/// 전송한 뒤 Service를 중지 시키기 위해 대기하는 시간을 ms 단위로 구하거나 설정합니다.
		/// <remarks>기본 5초 입니다.(5000)</remarks>
		/// </summary>
		public int CloseAfterReceiveReadyInterval
		{
			get
			{
				if (_closeAfterReceiveReadyInterval == 0)
				{
					_closeAfterReceiveReadyInterval = 5000;
				}

				return _closeAfterReceiveReadyInterval;
			}
			set
			{
				_closeAfterReceiveReadyInterval = value;
			}
		}

		private int CompletedCount
		{
			get
			{
				return _completedCount;
			}
			set
			{
				// Timer를 중지 후 CloseAfterReceiveReadyInterval초 후에 이벤트를 발생시킨다.
				if (_timer != null)
				{
					_timer.Stop();
				}

				_completedCount = value;

				// 값이 입력 되면 CloseAfterReceiveReadyInterval초간 대기 한다.
				if (_timer != null)
				{
					_timer.Start();
				}
			}
		}

		private void OnIdleReceive(object sender, System.Timers.ElapsedEventArgs e)
		{
			if (CloseAfterReceive == true)
			{
				_timer.Stop();
				Stop();
			}
		}
		#endregion

		#region Start
		/// <summary>
		/// Socket Listener를 시작합니다.
		/// </summary>
		/// <param name="appendable">파일을 이어 받을지 여부를 결정합니다.</param>
		/// <param name="listenerCount">Socket을 listen할 thread의 개수를 결정합니다.</param>
		/// <exception cref="System.InvalidOperationException">Socket과 Binding 되지 않았습니다. Bind method를 사용하여 해당 Port와 Binding을 다시 시도 하십시오.</exception>
		public void Start(bool appendable, int listenerCount = 1)
		{
			if (string.IsNullOrEmpty(ReceivedPath) == true)
			{
				throw new InvalidOperationException("ReceivedPath를 지정해야 합니다.");
			}

			if (IsBound == false)
			{
				Bind(Port);
				//throw new InvalidOperationException("Socket과 Binding 되지 않았습니다. Bind method를 사용하여 해당 Port와 Binding을 다시 시도 하십시오.");
			}

			ParameterizedThreadStart parameterizedThreadStart = new ParameterizedThreadStart(InternalStartServer);
			StartServerParameter startServerParameter = new StartServerParameter(appendable, listenerCount);
			Thread[] thread = new Thread[listenerCount];

			OnStarted(new ServerEventArgs(GetLocalIpAddress(), Port, listenerCount));

			for (int i = 0; i < listenerCount; i++)
			{
				thread[i] = new Thread(parameterizedThreadStart);
				thread[i].Start(startServerParameter);
			}
		}

		private void Bind(int port)
		{
			IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);

			try
			{
				_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
				_socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
				_socket.Bind(endPoint);
				IsBound = true;
			}
			catch (SocketException ex)
			{
				if (ex.ErrorCode == 10048)
				{
					// ErrorCode : 10048
					// 각 소켓 주소(프로토콜/네트워크 주소/포트)는 하나만 사용할 수 있습니다
					return;
				}

				IsBound = false;
				throw new InvalidProgramException(string.Format("Socket을 아래와 같은 이유로 실패하였습니다.\r\n({0}){1}", ex.ErrorCode, ex.Message), ex);
			}
		}

		private string GetLocalIpAddress()
		{
			IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());
			string localIpAddress = string.Empty;

			foreach (IPAddress ipAddr in ipEntry.AddressList)
			{
				byte ipHeader = ipAddr.GetAddressBytes()[0];

				if (ipHeader == 0 || ipHeader == 127 || ipHeader == 169
					|| ipAddr.AddressFamily != AddressFamily.InterNetwork)
				{
					continue;
				}

				localIpAddress = Convert.ToString(ipAddr);

				if (localIpAddress != string.Empty)
				{
					break;
				}
			}

			return localIpAddress;
		}

		private void InternalStartServer(object startServerParameters)
		{
			StartServerParameter startServerParameter = startServerParameters as StartServerParameter;

			if (startServerParameter == null)
			{
				return;
			}

			_socket.Listen(startServerParameter.ListenerCount);
			SocketAsyncEventArgs e = null;
			_completed = true;
			Running = true;

			while (IsBound == true)
			{
				if (_completed == true)		// 한번 Accept 된 Socket은 완료가 되어야만 다시 받을 수 있다
				{
					e = new SocketAsyncEventArgs();
					e.UserToken = startServerParameter.Appendable;
					e.Completed += AcceptAsync_Completed;

					_completed = false;
					_socket.AcceptAsync(e);
				}

				Thread.Sleep(500);
			}
		}

		private void AcceptAsync_Completed(object sender, SocketAsyncEventArgs e)
		{
			if (e.SocketError != SocketError.Success)
			{
				return;
			}

			Socket acceptedSocket = e.AcceptSocket;
			Stream stream = null;
			BinaryWriter bw = null;
			bool appendable = (bool)e.UserToken;

			OnClientAccepted(e);

			try
			{
				string remoteIpAddress = ((IPEndPoint)acceptedSocket.RemoteEndPoint).Address.ToString();
				byte[] indexHeader = new byte[INDEX_HEADER_SIZE];
				int receivedLength = acceptedSocket.Receive(indexHeader, indexHeader.Length, SocketFlags.Partial);

				int totalIndex = BitConverter.ToInt32(indexHeader, 0);
				long totalLength = BitConverter.ToInt64(indexHeader, sizeof(int));
				long grandTotalReceivedLength = 0L;
				Func<long, double> grandPercent = x => ((double)x / (double)totalLength * 100d);

				if (receivedLength > 0)
				{
					OnIndexHeaderReceived(new FileTransferEventArgs(string.Empty, 0L, totalLength, 0, totalIndex, 0d, remoteIpAddress, Port));
				}

				for (int index = 1; index <= totalIndex; index++)
				{
					byte[] header = new byte[HEADER_SIZE];
					receivedLength = acceptedSocket.Receive(header, HEADER_SIZE, SocketFlags.Partial);		// Header 중 File 크기와 File 이름 길이를 구한다.

					// Header 정보를 읽어 들인다.
					long fileSize = BitConverter.ToInt64(header, 0);				// file 크기는 long 형이다
					int fileNameLength = BitConverter.ToInt32(header, sizeof(long));	// file 이름 길이는 int 형이다.
					byte[] fileNameByte = new byte[fileNameLength];

					receivedLength = acceptedSocket.Receive(fileNameByte, fileNameLength, SocketFlags.Partial);		// File 이름을 읽어 들인다.

					string fileName = Encoding.Unicode.GetString(fileNameByte, 0, fileNameLength);
					string filePath = string.Format(@"{0}{1}{2}\{3}",
											ReceivedPath,
											ReceivedPathWithRemoteIpAddress == true ?
												@"\" + ((IPEndPoint)acceptedSocket.RemoteEndPoint).Address.ToString()
												: string.Empty,
											ReceivedPathWithDate == true ?
												@"\" + DateTime.Today.ToShortDateString()
												: string.Empty,
											fileName);
					string directory = Path.GetDirectoryName(filePath);

					if (Directory.Exists(directory) == false)
					{
						Directory.CreateDirectory(directory);
					}

					if (receivedLength > 0)
					{
						OnHeaderReceived(new FileTransferEventArgs(filePath, HEADER_SIZE, header.Length, index, totalIndex, grandPercent(grandTotalReceivedLength), remoteIpAddress, Port));
					}

					if (appendable == false && File.Exists(filePath) == true)
					{
						File.Delete(filePath);
					}

					stream = File.Open(filePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
					bw = new BinaryWriter(stream);

					long totalReceived = 0L;
					byte[] receivedData = new byte[BufferSize];

					while (Running == true
						&& (receivedLength = acceptedSocket.Receive(receivedData, BufferSize, SocketFlags.Partial)) > 0)		// 데이터를 읽어들인다.
					{
						bw.Write(receivedData, 0, receivedLength);		// 읽은 크기 만큼 File에 Write 한다.

						totalReceived += receivedLength;
						grandTotalReceivedLength += receivedLength;

						OnReceiving(new FileTransferEventArgs(filePath, totalReceived, fileSize, index, totalIndex, grandPercent(grandTotalReceivedLength), remoteIpAddress, Port));

						if (totalReceived == fileSize)
						{

							break;
						}
					}

					bw.Flush();
					bw.Close();

					OnCompleted(new FileTransferEventArgs(filePath, totalReceived, fileSize, index, totalIndex, grandPercent(grandTotalReceivedLength), remoteIpAddress, Port));
					acceptedSocket.Send(_ack);		// 전송을 다 받은 뒤 다음 파일을 전송 받도록 신호를 보낸다.
				}

				CompletedCount = totalIndex;
			}
			catch (SocketException ex)
			{
				OnError(new ErrorEventArgs(new Exception(string.Format("({0}){1}", ex.ErrorCode, ex.Message))));
			}
			catch (IOException ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
			finally
			{
				if (bw != null)
				{
					bw.Close();
				}

				if (stream != null)
				{
					stream.Close();
				}

				if (acceptedSocket != null)
				{
					acceptedSocket.Close();
				}
			}

			_completed = true;
		}
		#endregion

		#region Stop
		/// <summary>
		/// Socket Listener를 중지합니다.
		/// </summary>
		public void Stop()
		{
			IsBound = false;

			if (_socket != null)
			{
				_socket.Close();
				_socket = null;
				OnStopped(new ServerEventArgs(GetLocalIpAddress(), Port, -1));
			}

			Running = false;
		}
		#endregion
	}
}
