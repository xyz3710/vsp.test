﻿namespace FileReceiverForm
{
	partial class ReceiverForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.pbTotal = new System.Windows.Forms.ProgressBar();
			this.tbTargetFolder = new System.Windows.Forms.TextBox();
			this.btnFolder = new System.Windows.Forms.Button();
			this.pbCurrent = new System.Windows.Forms.ProgressBar();
			this.tbMessage = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.btnStop = new System.Windows.Forms.Button();
			this.btnStart = new System.Windows.Forms.Button();
			this.fbdFolder = new System.Windows.Forms.FolderBrowserDialog();
			this.tlpMain.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 3;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tlpMain.Controls.Add(this.label1, 0, 0);
			this.tlpMain.Controls.Add(this.pbTotal, 0, 3);
			this.tlpMain.Controls.Add(this.tbTargetFolder, 1, 0);
			this.tlpMain.Controls.Add(this.btnFolder, 2, 0);
			this.tlpMain.Controls.Add(this.pbCurrent, 0, 2);
			this.tlpMain.Controls.Add(this.tbMessage, 0, 1);
			this.tlpMain.Controls.Add(this.tableLayoutPanel1, 0, 4);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 5;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
			this.tlpMain.Size = new System.Drawing.Size(784, 561);
			this.tlpMain.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(114, 38);
			this.label1.TabIndex = 0;
			this.label1.Text = "저장 폴더";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pbTotal
			// 
			this.tlpMain.SetColumnSpan(this.pbTotal, 3);
			this.pbTotal.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbTotal.Location = new System.Drawing.Point(3, 488);
			this.pbTotal.Name = "pbTotal";
			this.pbTotal.Size = new System.Drawing.Size(778, 32);
			this.pbTotal.TabIndex = 5;
			// 
			// tbTargetFolder
			// 
			this.tbTargetFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbTargetFolder.Location = new System.Drawing.Point(123, 7);
			this.tbTargetFolder.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
			this.tbTargetFolder.Name = "tbTargetFolder";
			this.tbTargetFolder.Size = new System.Drawing.Size(538, 25);
			this.tbTargetFolder.TabIndex = 1;
			// 
			// btnFolder
			// 
			this.btnFolder.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnFolder.Location = new System.Drawing.Point(667, 3);
			this.btnFolder.Name = "btnFolder";
			this.btnFolder.Size = new System.Drawing.Size(114, 32);
			this.btnFolder.TabIndex = 2;
			this.btnFolder.Text = "폴더 선택";
			this.btnFolder.UseVisualStyleBackColor = true;
			// 
			// pbCurrent
			// 
			this.tlpMain.SetColumnSpan(this.pbCurrent, 3);
			this.pbCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbCurrent.Location = new System.Drawing.Point(3, 450);
			this.pbCurrent.Name = "pbCurrent";
			this.pbCurrent.Size = new System.Drawing.Size(778, 32);
			this.pbCurrent.TabIndex = 4;
			// 
			// tbMessage
			// 
			this.tlpMain.SetColumnSpan(this.tbMessage, 3);
			this.tbMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbMessage.Location = new System.Drawing.Point(3, 41);
			this.tbMessage.Multiline = true;
			this.tbMessage.Name = "tbMessage";
			this.tbMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbMessage.Size = new System.Drawing.Size(778, 403);
			this.tbMessage.TabIndex = 3;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tlpMain.SetColumnSpan(this.tableLayoutPanel1, 3);
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Controls.Add(this.btnStop, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnStart, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 523);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 38);
			this.tableLayoutPanel1.TabIndex = 5;
			// 
			// btnStop
			// 
			this.btnStop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnStop.Location = new System.Drawing.Point(395, 3);
			this.btnStop.Name = "btnStop";
			this.btnStop.Size = new System.Drawing.Size(386, 32);
			this.btnStop.TabIndex = 1;
			this.btnStop.Text = "Sto&p";
			this.btnStop.UseVisualStyleBackColor = true;
			// 
			// btnStart
			// 
			this.btnStart.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnStart.Location = new System.Drawing.Point(3, 3);
			this.btnStart.Name = "btnStart";
			this.btnStart.Size = new System.Drawing.Size(386, 32);
			this.btnStart.TabIndex = 0;
			this.btnStart.Text = "&Start";
			this.btnStart.UseVisualStyleBackColor = true;
			// 
			// fbdFolder
			// 
			this.fbdFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
			// 
			// ReceiverForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 561);
			this.Controls.Add(this.tlpMain);
			this.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "ReceiverForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "File Receiver";
			this.tlpMain.ResumeLayout(false);
			this.tlpMain.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.TextBox tbTargetFolder;
		private System.Windows.Forms.Button btnFolder;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.FolderBrowserDialog fbdFolder;
		private System.Windows.Forms.ProgressBar pbCurrent;
		private System.Windows.Forms.ProgressBar pbTotal;
		private System.Windows.Forms.TextBox tbMessage;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.Button btnStart;
	}
}

