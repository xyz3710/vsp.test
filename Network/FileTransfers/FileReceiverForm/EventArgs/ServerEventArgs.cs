﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GS.Common.Net
{
	/// <summary>
	/// 서버 접속 정보에 대한 EventArgs입니다.
	/// </summary>
	[Serializable]
	[System.Diagnostics.DebuggerStepThrough]
	public class ServerEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ServerEventArgs"/> class.
		/// </summary>
		/// <param name="ipAddress">The ip address.</param>
		/// <param name="port">The port.</param>
		/// <param name="listenerCount">The listener count.</param>
		public ServerEventArgs(string ipAddress, int port, int listenerCount = 1)
		{
			IpAddress = ipAddress;
			Port = port;
			ListenerCount = listenerCount;
		}

		/// <summary>
		/// Gets or sets the ip address.
		/// </summary>
		/// <value>The ip address.</value>
		public string IpAddress
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the port.
		/// </summary>
		/// <value>The port.</value>
		public int Port
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the listener count.
		/// </summary>
		/// <value>The listener count.</value>
		public int ListenerCount
		{
			get;
			set;
		}
	}
}
