﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GS.Common.Net
{
	/// <summary>
	/// 파일 전송 정보에 대한 EventArgs입니다.
	/// </summary>
	[Serializable]
	[System.Diagnostics.DebuggerStepThrough]
	public class FileTransferEventArgs : ServerEventArgs
	{
		/// <summary>
		/// TransferInfoEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="filePath">전송중인 파일 이름 입니다.</param>
		/// <param name="currentSize">현재 전송된 byte 입니다.</param>
		/// <param name="totalSize">전체 전송할 byte 입니다.</param>
		/// <param name="currentIndex">전송되는 파일의 현재 인덱스.</param>
		/// <param name="totalIndex">전송되는 파일의 전체 인덱스.</param>
		/// <param name="grandPercentage">전체 파일 전송율.</param>
		/// <param name="ipAddress">서비스 하는 Server의 IpAddress 입니다.</param>
		/// <param name="port">Socket과 Binding되는 Port 번호 입니다.</param>
		public FileTransferEventArgs(
			string filePath,
			long currentSize,
			long totalSize,
			int currentIndex = 1,
			int totalIndex = 1,
			double grandPercentage = 1d,
			string ipAddress = "",
			int port = 0)
			: base(ipAddress, port)
		{
			FileName = filePath;
			CurrentSize = currentSize;
			TotalSize = totalSize;
			CurrentIndex = currentIndex;
			TotalIndex = totalIndex;
			GrandPercentage = grandPercentage;
		}

		/// <summary>
		/// Gets or sets the name of the file.
		/// </summary>
		/// <value>The name of the file.</value>
		public string FileName
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the size of the current.
		/// </summary>
		/// <value>The size of the current.</value>
		public long CurrentSize
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the total size.
		/// </summary>
		/// <value>The total size.</value>
		public long TotalSize
		{
			get;
			set;
		}

		/// <summary>
		/// Gets the percentage.
		/// </summary>
		/// <value>The percentage.</value>
		public double Percentage
		{
			get
			{
				return ((double)CurrentSize / (double)TotalSize) * 100d;
			}
		}

		/// <summary>
		/// Gets or sets the index of the current.
		/// </summary>
		/// <value>The index of the current.</value>
		public int CurrentIndex
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the total index.
		/// </summary>
		/// <value>The total index.</value>
		public int TotalIndex
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the grand percentage.
		/// </summary>
		/// <value>The grand percentage.</value>
		public double GrandPercentage
		{
			get;
			private set;
		}
	}
}
