﻿// ****************************************************************************************************************** //
//	Domain		:	GS.Common.Utility.RunningTimer
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2015년 6월 25일 목요일 오후 5:48
//	Purpose		:	특정 객체의 시작과 종료, 가동 시간을 관리 합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="RunningTimer.cs" company="(주)가치소프트">
//		Copyright (c) 2015. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GS.Common.Utility
{
	/// <summary>
	/// 특정 객체의 시작과 종료, 가동 시간을 관리 합니다.
	/// </summary>
	public class RunningTimer<T>
	{
		#region Elapsed Event
		/// <summary>
		/// Occurs when [on Elapsed].
		/// </summary>
		public event EventHandler<ElapsedEventArgs> Elapsed;

		/// <summary>
		/// Handles the <see cref="E:OnElapsed" /> event.
		/// </summary>
		/// <param name="stateObject">The state object.</param>
		/// <param name="e">The <see cref="ElapsedEventArgs" /> instance containing the event data.</param>
		protected void OnElapsed(T stateObject, TimeSpan e)
		{
			if (Elapsed != null)
			{
				Elapsed(this, new ElapsedEventArgs(stateObject, e));
			}
		}

		/// <summary>
		/// Elapsed's event data.
		/// </summary>
		[System.Diagnostics.DebuggerStepThrough]
		public class ElapsedEventArgs : EventArgs
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="ElapsedEventArgs"/> class.
			/// </summary>
			/// <param name="stateObject">The state object.</param>
			/// <param name="timeSpan">The time span.</param>
			public ElapsedEventArgs(T stateObject, TimeSpan timeSpan)
			{
				StateObject = stateObject;
				TimeSpan = timeSpan;
			}

			/// <summary>
			/// Gets or sets the state object.
			/// </summary>
			/// <value>The state object.</value>
			public T StateObject
			{
				get;
				set;
			}

			/// <summary>
			/// Gets or sets the time span.
			/// </summary>
			/// <value>The time span.</value>
			public TimeSpan TimeSpan
			{
				get;
				set;
			}
		}
		#endregion

		private T _stateObject;
		private DateTime _startTime;
		private System.Timers.Timer _runningIndicator;
		private System.Diagnostics.Stopwatch _runningTimer;

		/// <summary>
		/// Initializes a new instance of the <see cref="RunningTimer{T}"/> class.
		/// </summary>
		/// <param name="stateObject">The state object.</param>
		/// <param name="indicatorInterval">The indicator interval.</param>
		public RunningTimer(T stateObject, double indicatorInterval = 1000d)
		{
			_stateObject = stateObject;
			_startTime = DateTime.MinValue;
			_runningIndicator = new System.Timers.Timer(indicatorInterval);
			_runningTimer = new System.Diagnostics.Stopwatch();
			_runningIndicator.Elapsed += (sender, e) =>
			{
				OnElapsed(_stateObject, _runningTimer.Elapsed);
			};
		}

		/// <summary>
		/// Sets the state object.
		/// </summary>
		/// <param name="stateObject">The state object.</param>
		public void SetStateObject(T stateObject)
		{
			_stateObject = stateObject;
		}

		/// <summary>
		/// Set the Indicators the interval.
		/// </summary>
		/// <param name="interval">The interval.</param>
		public void SetIndicatorInterval(double interval)
		{
			_runningIndicator.Interval = interval;
		}

		/// <summary>
		/// Sets the start time.
		/// </summary>
		/// <returns>StartTime.</returns>
		public DateTime SetStartTime()
		{
			if (_startTime == DateTime.MinValue)
			{
				_startTime = DateTime.Now;
			}

			return _startTime;
		}

		/// <summary>
		/// Resets the start time.
		/// </summary>
		/// <returns>Duration from SetStartTime.</returns>
		public TimeSpan ResetStartTime()
		{
			var span = DateTime.Now - _startTime;

			_startTime = DateTime.MinValue;

			return span;
		}

		/// <summary>
		/// Sets the running time.
		/// </summary>
		public void SetRunningTime()
		{
			if (_runningIndicator.Enabled == false)
			{
				_runningIndicator.Start();
				_runningTimer.Start();
			}
		}

		/// <summary>
		/// Pauses the running time.
		/// </summary>
		/// <returns>Elapsed from set.</returns>
		public TimeSpan PauseRunningTime()
		{
			if (_runningIndicator.Enabled == true)
			{
				_runningIndicator.Stop();
				_runningTimer.Stop();
			}

			return _runningTimer.Elapsed;
		}

		/// <summary>
		/// Resets the running time.
		/// </summary>
		/// <returns>Elapsed from set.</returns>
		public TimeSpan ResetRunningTime()
		{
			var span = _runningTimer.Elapsed;

			if (_runningIndicator.Enabled == true)
			{
				_runningIndicator.Stop();
				_runningTimer.Reset();
			}

			return span;
		}

		/// <summary>
		/// Restarts(start after reset runninig timer) the running time.
		/// </summary>
		/// <returns>Elapsed from set.</returns>
		public TimeSpan RestartRunningTime()
		{
			var span = _runningTimer.Elapsed;

			if (_runningIndicator.Enabled == true)
			{
				_runningIndicator.Stop();
			}

			_runningTimer.Stop();
			_runningTimer.Start();
			_runningIndicator.Start();

			return span;
		}
	}
}
