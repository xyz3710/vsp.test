﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace GS.Common.Net
{
	partial class FileReceiver
	{
		/// <summary>
		/// Occurs when [on Started].
		/// </summary>
		public event EventHandler<ServerEventArgs> Started;

		/// <summary>
		/// Handles the <see cref="E:OnStarted"/> event.
		/// </summary>
		/// <param name="e">The <see cref="ServerEventArgs"/> instance containing the event data.</param>
		protected virtual void OnStarted(ServerEventArgs e)
		{
			if (Started != null)
			{
				Started(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Stopped].
		/// </summary>
		public event EventHandler<ServerEventArgs> Stopped;

		/// <summary>
		/// Handles the <see cref="E:OnStopped"/> event.
		/// </summary>
		/// <param name="e">The <see cref="ServerEventArgs"/> instance containing the event data.</param>
		protected virtual void OnStopped(ServerEventArgs e)
		{
			if (Stopped != null)
			{
				Stopped(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on ClientAccepted].
		/// </summary>
		public event EventHandler<SocketAsyncEventArgs> ClientAccepted;

		/// <summary>
		/// Handles the <see cref="E:OnClientAccepted"/> event.
		/// </summary>
		/// <param name="e">The <see cref="SocketAsyncEventArgs"/> instance containing the event data.</param>
		protected virtual void OnClientAccepted(SocketAsyncEventArgs e)
		{
			if (ClientAccepted != null)
			{
				ClientAccepted(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on IndexHeaderReceived].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> IndexHeaderReceived;

		/// <summary>
		/// Handles the <see cref="E:OnIndexHeaderReceived"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnIndexHeaderReceived(FileTransferEventArgs e)
		{
			if (IndexHeaderReceived != null)
			{
				IndexHeaderReceived(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on HeaderReceived].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> HeaderReceived;

		/// <summary>
		/// Handles the <see cref="E:OnHeaderReceived"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnHeaderReceived(FileTransferEventArgs e)
		{
			if (HeaderReceived != null)
			{
				HeaderReceived(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Receiving].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> Receiving;

		/// <summary>
		/// Handles the <see cref="E:OnReceiving"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnReceiving(FileTransferEventArgs e)
		{
			if (Receiving != null)
			{
				Receiving(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Completed].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> Completed;

		/// <summary>
		/// Handles the <see cref="E:OnCompleted"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnCompleted(FileTransferEventArgs e)
		{
			if (Completed != null)
			{
				Completed(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Error].
		/// </summary>
		public event EventHandler<ErrorEventArgs> Error;

		/// <summary>
		/// Handles the <see cref="E:OnError"/> event.
		/// </summary>
		/// <param name="e">The <see cref="ErrorEventArgs"/> instance containing the event data.</param>
		protected virtual void OnError(ErrorEventArgs e)
		{
			if (Error != null)
			{
				Error(this, e);
			}
		}
	}
}
