﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GS.Common.Net;
using System.IO;
using System.Diagnostics;
using GS.Common.Utility;
using GS.Common.Net.Properties;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using System.Data.Linq;
namespace FileReceiverForm
{
	public partial class ReceiverForm : Form
	{
		private RunningTimer<object> _runningTimer;

		public ReceiverForm()
		{
			InitializeComponent();

			var text = Text;
			_runningTimer = new RunningTimer<object>(null);
			_runningTimer.Elapsed += (sender, e) =>
			{
				Invoke(new MethodInvoker(() =>
				{
					var span = e.TimeSpan;

					Text = string.Format("{0}: {1:00}:{2:00}:{3:00}", text, span.Hours, span.Minutes, span.Seconds);
				}));
			};

			RegisterEventHandlers();
		}

		private void RegisterEventHandlers()
		{
			FileReceiver = new FileTransfer(Settings.Default.Port, Settings.Default.BufferSize)
			{
				CloseAfterReceive = false,
				ReceivedPathWithRemoteIpAddress = false,
				//ReceiveAfterAcceptClient = false,		// 전송할 수 있는 모드로 연다.
			};
			FileReceiver.Started += (sender, e) =>
			{
				WriteLog("{0} 개의 스레드로 {1} 포트에서 시작되었습니다.", e.ListenerCount, e.Port);
			};
			FileReceiver.Stopped += (sender, e) =>
			{
				WriteLog("{0} 포트의 서비스가 중지되었습니다.", e.Port);
			};
			FileReceiver.ClientAccepted += (sender, e) =>
			{
				if (FileReceiver.Targets.Count == 1)
				{
					Invoke(new MethodInvoker(() =>
					{
						btnSendFirst.Enabled = true;
					}));
				}
				else if (FileReceiver.Targets.Count > 1)
				{
					Invoke(new MethodInvoker(() =>
					{
						btnReceive.Enabled = true;
					}));
				}

				WriteLog("{0} 에서 접속되었습니다.", e.AcceptSocket.RemoteEndPoint.ToString());
			};
			FileReceiver.Error += (sender, e) =>
			{
				WriteLog("Error: {0}", e.GetException().Message);
			};
			FileReceiver.IndexHeaderReceived += (sender, e) =>
			{
				_runningTimer.SetRunningTime();
				WriteLog("{0}: {1:#,##0}bytes", e.TotalIndex, e.TotalSize);
			};
			FileReceiver.HeaderReceived += (sender, e) =>
			{
				_runningTimer.SetRunningTime();
				WriteLog("{0}: {1:#,##0}bytes", e.FileName, e.TotalSize);
			};
			FileReceiver.FileReceiving += (sender, e) =>
			{
				WriteLog("{0}: {1:#,##0}/{2:#,##0}", e.FileName, e.CurrentSize, e.TotalSize);
				SetCurrent(e.Percentage);
				SetTotalPercent((int)e.GrandPercentage);	// percent로 처리할 경우
			};
			FileReceiver.TransferCompleted += (sender, e) =>
			{
				_runningTimer.PauseRunningTime();
				WriteLog("{0}: completed", e.FileName);
				//SetTotal(e.CurrentIndex, e.TotalIndex);	// index 로 처리할 경우
			};

			FileReceiver.IndexHeaderSended += (sender, e) =>
			{
				_runningTimer.SetRunningTime();
				WriteLog("{0}: {1:#,##0}bytes", e.TotalIndex, e.TotalSize);
			};
			FileReceiver.HeaderSended += (sender, e) =>
			{
				WriteLog("{0}: {1:#,##0}bytes", e.FileName, e.TotalSize);
			};
			FileReceiver.FileSending += (sender, e) =>
			{
				WriteLog("{0}: {1:#,##0}/{2:#,##0}", e.FileName, e.CurrentSize, e.TotalSize);
				SetCurrent(e.Percentage);
				SetTotalPercent((int)e.GrandPercentage);		// percent로 처리할 경우
			};

			btnFolder.Click += (sender, e) =>
			{
				if (Directory.Exists(TargetPath) == true)
				{
					fbdFolder.SelectedPath = TargetPath;
				}

				if (fbdFolder.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
				{
					TargetPath = fbdFolder.SelectedPath;
				}
			};
			btnStart.Click += (sender, e) =>
			{
				FileReceiver.ReceivedPath = TargetPath;
				FileReceiver.Start(true, 50);
				EnableStartButton(false);
			};
			btnStop.Click += (sender, e) =>
			{
				FileReceiver.Stop();
				EnableStartButton(true);
			};

			EnableStartButton(true);

			btnSendFirst.Click += (sender, e) =>
			{
				var files = Directory.GetFiles(@"F:\Received");
				//var files = Directory.GetFiles(@"D:\VSP\Test\Network\FileTransfers\FileTransferSenderForm", "*.cs");

				if (FileReceiver.IsConnected == true)
				{
					FileReceiver.ReceiveAfterAcceptClient = false;
					FileReceiver.SendFile(FileReceiver.Targets.Values.First(), files);
					FileReceiver.ReceiveAfterAcceptClient = true;
				}
			};
			btnReceive.Click += (sender, e) =>
			{
				FileReceiver.ReceiveFiles(FileReceiver.Targets.Values.First(), false, @"F:\Received", true, false);
			};
		}

		private string TargetPath
		{
			get
			{
				return tbTargetFolder.Text;
			}
			set
			{
				tbTargetFolder.Text = value;
				FileReceiver.ReceivedPath = TargetPath;
			}
		}

		private FileTransfer FileReceiver
		{
			get;
			set;
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);
			TargetPath = @"F:\Received";
			btnStart.PerformClick();
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			FileReceiver.Stop();
			base.OnClosing(e);
		}

		private void WriteLog(string format, params object[] args)
		{
			Invoke(new MethodInvoker(() =>
			{
				tbMessage.AppendText(string.Format(format + Environment.NewLine, args));
			}));
		}

		private void EnableStartButton(bool enabled)
		{
			btnStart.Enabled = enabled;
			btnStop.Enabled = !enabled;
			tbTargetFolder.Enabled = enabled;
			btnFolder.Enabled = enabled;
		}

		private void SetCurrent(double percent)
		{
			Invoke(new MethodInvoker(() =>
			{
				pbCurrent.Value = (int)percent;
			}));
		}

		private void SetTotal(int value, int maximum)
		{
			Invoke(new MethodInvoker(() =>
			{
				pbTotal.Value = value;
				pbTotal.Maximum = maximum;
			}));
		}

		private void SetTotalPercent(int percent)
		{
			Invoke(new MethodInvoker(() =>
			{
				pbTotal.Value = percent;
				pbTotal.Maximum = 100;
			}));
		}
	}
}
