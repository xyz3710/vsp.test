﻿// ****************************************************************************************************************** //
//	Domain		:	GS.Common.Net.FileTransfer
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2015년 7월 13일 월요일 오후 4:49
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	TODO: Receiver쪽에서 Disconnect되는지 여부를 보강 해야 한다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="FileTranasfer.Receiver.cs" company="(주)가치소프트">
//		Copyright (c) 2015. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace GS.Common.Net
{
	/// <summary>
	/// File 전송용 수신자 class 입니다.
	/// </summary>
	partial class FileTransfer
	{
		private string _receivedPath;
		private bool _closeAfterReceive;
		private int _closeAfterReceiveReadyInterval;

		/// <summary>
		/// Gets or sets the received path.
		/// </summary>
		/// <value>The received path.</value>
		public string ReceivedPath
		{
			get
			{
				_receivedPath = _receivedPath ?? string.Empty;

				return _receivedPath;
			}
			set
			{
				_receivedPath = value;

				if (Directory.Exists(_receivedPath) == false)
				{
					Directory.CreateDirectory(_receivedPath);
				}
			}
		}

		/// <summary>
		/// 다중으로 전송 받을 경우 중복된 파일들을 분류하기 위해 ReceivedPath에 Remote Client의 IpAddress를 폴더명을 추가한다.
		/// </summary>
		/// <value>true if [received path with remote ip address]; otherwise, false.</value>
		public bool ReceivedPathWithRemoteIpAddress
		{
			get;
			set;
		}

		/// <summary>
		/// 다중으로 전송 받을 경우 중복된 파일들을 분류하기 위해 ReceivedPath에 받는 날짜 폴더명을 추가한다.
		/// </summary>
		/// <value>true if [received path with date]; otherwise, false.</value>
		public bool ReceivedPathWithDate
		{
			get;
			set;
		}

		/// <summary>
		/// 전송한 뒤 Service를 중지 시킬지 여부를 결정합니다.
		/// </summary>
		/// <value>true if [close after receive]; otherwise, false.</value>
		public bool CloseAfterReceive
		{
			private get
			{
				return _closeAfterReceive;
			}
			set
			{
				_closeAfterReceive = value;

				// 특정 interval동안 Receiving을 처리한다.
				if (value == true && _timer == null)
				{
					_timer = new System.Timers.Timer();
					_timer.AutoReset = true;
					_timer.Interval = CloseAfterReceiveReadyInterval;
					_timer.Elapsed += new System.Timers.ElapsedEventHandler(OnIdleReceive);
				}
			}
		}

		/// <summary>
		/// 전송한 뒤 Service를 중지 시키기 위해 대기하는 시간을 ms 단위로 구하거나 설정합니다.
		/// <remarks>기본 5초 입니다.(5000)</remarks>
		/// </summary>
		public int CloseAfterReceiveReadyInterval
		{
			get
			{
				if (_closeAfterReceiveReadyInterval == 0)
				{
					_closeAfterReceiveReadyInterval = 5000;
				}

				return _closeAfterReceiveReadyInterval;
			}
			set
			{
				_closeAfterReceiveReadyInterval = value;
			}
		}

		/// <summary>
		/// 클라이언트가 연결될 때 파일을 받을 수 있도록 등록할지 여부를 구하거나 설정합니다.
		/// </summary>
		/// <value>ReceiveAfterAcceptClient를 반환합니다.</value>
		public bool ReceiveAfterAcceptClient
		{
			get;
			set;
		}

		/// <summary>
		/// Socket에 특정 Port를 성공적으로 Binding되었는지 여부를 구합니다.
		/// </summary>
		/// <value>true if this instance is bound; otherwise, false.</value>
		private bool IsBound
		{
			get;
			set;
		}

		private int CompletedCount
		{
			get
			{
				return _completedCount;
			}
			set
			{
				// Timer를 중지 후 CloseAfterReceiveReadyInterval초 후에 이벤트를 발생시킨다.
				if (_timer != null)
				{
					_timer.Stop();
				}

				_completedCount = value;

				// 값이 입력 되면 CloseAfterReceiveReadyInterval초간 대기 한다.
				if (_timer != null)
				{
					_timer.Start();
				}
			}
		}

		private void OnIdleReceive(object sender, System.Timers.ElapsedEventArgs e)
		{
			if (CloseAfterReceive == true)
			{
				_timer.Stop();
				Stop();
			}
		}

		#region Start
		/// <summary>
		/// Socket Listener를 시작합니다.
		/// </summary>
		/// <param name="appendable">파일을 이어 받을지 여부를 결정합니다.</param>
		/// <param name="listenerCount">Socket을 listen할 thread의 개수를 결정합니다.</param>
		/// <exception cref="System.InvalidOperationException">Socket과 Binding 되지 않았습니다. Bind method를 사용하여 해당 Port와 Binding을 다시 시도 하십시오.</exception>
		public void Start(bool appendable, int listenerCount = 1)
		{
			if (string.IsNullOrEmpty(ReceivedPath) == true)
			{
				throw new InvalidOperationException("ReceivedPath를 지정해야 합니다.");
			}

			if (IsBound == false)
			{
				TargetSocket = Bind(Port);
				//throw new InvalidOperationException("Socket과 Binding 되지 않았습니다. Bind method를 사용하여 해당 Port와 Binding을 다시 시도 하십시오.");
			}

			ParameterizedThreadStart parameterizedThreadStart = new ParameterizedThreadStart(InternalStartServer);
			StartServerParameter startServerParameter = new StartServerParameter(appendable, listenerCount);
			Thread[] thread = new Thread[listenerCount];

			IsServerRole = true;
			OnStarted(new ServerEventArgs(GetLocalIpAddress(), Port, listenerCount));

			for (int i = 0; i < listenerCount; i++)
			{
				thread[i] = new Thread(parameterizedThreadStart);
				thread[i].Start(startServerParameter);
			}
		}

		/// <summary>
		/// Binds the specified port.
		/// </summary>
		/// <param name="port">The port.</param>
		/// <returns>Socket.</returns>
		/// <exception cref="System.InvalidProgramException"></exception>
		protected Socket Bind(int port)
		{
			Socket socket = null;
			IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);

			try
			{
				socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
				socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
				socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, true);
				socket.Bind(endPoint);
				IsBound = true;
			}
			catch (SocketException ex)
			{
				if (ex.ErrorCode == 10048)
				{
					// ErrorCode : 10048
					// 각 소켓 주소(프로토콜/네트워크 주소/포트)는 하나만 사용할 수 있습니다
					return socket;
				}

				IsBound = false;
				throw new InvalidProgramException(string.Format("Socket을 아래와 같은 이유로 실패하였습니다.\r\n({0}){1}", ex.ErrorCode, ex.Message), ex);
			}

			return socket;
		}

		private void InternalStartServer(object startServerParameters)
		{
			StartServerParameter startServerParameter = startServerParameters as StartServerParameter;

			if (startServerParameter == null)
			{
				return;
			}

			TargetSocket.Listen(startServerParameter.ListenerCount);

			SocketAsyncEventArgs e = null;
			_completed = true;
			Running = true;

			while (IsBound == true)
			{
				if (_completed == true)		// 한번 Accept 된 Socket은 완료가 되어야만 다시 받을 수 있다
				{
					e = new SocketAsyncEventArgs();
					e.UserToken = startServerParameter.Appendable;
					e.Completed += AcceptAsync_Completed;

					_completed = false;
					TargetSocket.AcceptAsync(e);
				}

				Thread.Sleep(500);
			}
		}

		private void AcceptAsync_Completed(object sender, SocketAsyncEventArgs e)
		{
			if (e.SocketError != SocketError.Success)
			{
				return;
			}

			Socket acceptedSocket = e.AcceptSocket;
			var remoteId = GetRemoteId(acceptedSocket);

			IsConnected = true;
			Targets.Add(remoteId, acceptedSocket);

			OnClientAccepted(e);

			if (ReceiveAfterAcceptClient == true)
			{
				bool appendable = (bool)e.UserToken;
				ReceiveFiles(acceptedSocket, appendable, ReceivedPath, ReceivedPathWithRemoteIpAddress, ReceivedPathWithDate);
			}
		}

		/// <summary>
		/// Receiveds the files.
		/// </summary>
		/// <param name="targetSocket">The target socket.</param>
		/// <param name="appendable">if set to true [appendable].</param>
		/// <param name="receivedPath">The received path.</param>
		/// <param name="receivedPathWithRemoteIpAddress">if set to true [received path with remote ip address].</param>
		/// <param name="receivedPathWithDate">if set to true [received path with date].</param>
		public void ReceiveFiles(
			Socket targetSocket,
			bool appendable,
			string receivedPath,
			bool receivedPathWithRemoteIpAddress,
			bool receivedPathWithDate)
		{
			Stream stream = null;
			BinaryWriter bw = null;
			bool forceDisconnect = false;

			try
			{
				string remoteIpAddress = ((IPEndPoint)targetSocket.RemoteEndPoint).Address.ToString();
				byte[] indexHeader = new byte[INDEX_HEADER_SIZE];
				int receivedLength = targetSocket.Receive(indexHeader, indexHeader.Length, SocketFlags.Partial);

				int totalIndex = BitConverter.ToInt32(indexHeader, 0);
				long totalLength = BitConverter.ToInt64(indexHeader, sizeof(int));
				long grandTotalReceivedLength = 0L;
				Func<long, double> grandPercent = x => ((double)x / (double)totalLength * 100d);

				if (receivedLength > 0)
				{
					OnIndexHeaderReceived(new FileTransferEventArgs(string.Empty, 0L, totalLength, 0, totalIndex, 0d, remoteIpAddress, Port));
				}

				for (int index = 1; index <= totalIndex; index++)
				{
					byte[] header = new byte[HEADER_SIZE];
					receivedLength = targetSocket.Receive(header, HEADER_SIZE, SocketFlags.Partial);		// Header 중 File 크기와 File 이름 길이를 구한다.

					// Header 정보를 읽어 들인다.
					long fileSize = BitConverter.ToInt64(header, 0);				// file 크기는 long 형이다
					int fileNameLength = BitConverter.ToInt32(header, sizeof(long));	// file 이름 길이는 int 형이다.
					byte[] fileNameByte = new byte[fileNameLength];

					receivedLength = targetSocket.Receive(fileNameByte, fileNameLength, SocketFlags.Partial);		// File 이름을 읽어 들인다.

					string fileName = Encoding.Unicode.GetString(fileNameByte, 0, fileNameLength);
					string filePath = string.Format(@"{0}{1}{2}\{3}",
											receivedPath,
											receivedPathWithRemoteIpAddress == true ?
												@"\" + remoteIpAddress
												: string.Empty,
											receivedPathWithDate == true ?
												@"\" + DateTime.Today.ToShortDateString()
												: string.Empty,
											fileName);
					string directory = Path.GetDirectoryName(filePath);

					if (Directory.Exists(directory) == false)
					{
						Directory.CreateDirectory(directory);
					}

					if (receivedLength > 0)
					{
						OnHeaderReceived(new FileTransferEventArgs(filePath, HEADER_SIZE, header.Length, index, totalIndex, grandPercent(grandTotalReceivedLength), remoteIpAddress, Port));
					}

					if (appendable == false && File.Exists(filePath) == true)
					{
						File.Delete(filePath);
					}

					stream = File.Open(filePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
					bw = new BinaryWriter(stream);

					long totalReceived = 0L;
					byte[] receivedData = new byte[BufferSize];

					while ((receivedLength = targetSocket.Receive(receivedData, BufferSize, SocketFlags.Partial)) > 0)		// 데이터를 읽어들인다.
					{
						bw.Write(receivedData, 0, receivedLength);		// 읽은 크기 만큼 File에 Write 한다.

						totalReceived += receivedLength;
						grandTotalReceivedLength += receivedLength;

						OnFileReceiving(new FileTransferEventArgs(filePath, totalReceived, fileSize, index, totalIndex, grandPercent(grandTotalReceivedLength), remoteIpAddress, Port));

						if (totalReceived == fileSize)
						{
							break;
						}
					}

					bw.Flush();
					bw.Close();
					OnTransferCompleted(new FileTransferEventArgs(filePath, totalReceived, fileSize, index, totalIndex, grandPercent(grandTotalReceivedLength), remoteIpAddress, Port));
					targetSocket.Send(_ack, SocketFlags.Partial);		// 전송을 다 받은 뒤 다음 파일을 전송 하도록 신호를 보낸다.
				}

				CompletedCount = totalIndex;
			}
			catch (SocketException ex)
			{
				forceDisconnect = true;
				OnError(new ErrorEventArgs(new Exception(string.Format("({0}){1}", ex.ErrorCode, ex.Message))));
			}
			catch (IOException ex)
			{
				forceDisconnect = true;
				OnError(new ErrorEventArgs(ex));
			}
			catch (Exception ex)
			{
				forceDisconnect = true;
				OnError(new ErrorEventArgs(ex));
			}
			finally
			{
				if (bw != null)
				{
					bw.Close();
				}

				if (stream != null)
				{
					stream.Close();
				}
			}

			_completed = true;

			if (forceDisconnect == false)
			{
				ReceiveFiles(targetSocket, appendable, receivedPath, receivedPathWithRemoteIpAddress, receivedPathWithDate);
			}
		}
		#endregion

		#region Stop
		/// <summary>
		/// Socket Listener를 중지합니다.
		/// </summary>
		public void Stop()
		{
			IsBound = false;

			if (TargetSocket != null)
			{
				TargetSocket.Close();
				TargetSocket = null;
				OnStopped(new ServerEventArgs(GetLocalIpAddress(), Port, -1));
			}

			Running = false;
		}
		#endregion
	}
}
