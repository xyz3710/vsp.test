﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace GS.Common.Net
{
	/// <summary>
	/// File 전송용 송신자 class 입니다.
	/// </summary>
	partial class FileTransfer
	{
		/// <summary>
		/// Connects the specified target ip address.
		/// </summary>
		/// <exception cref="System.InvalidOperationException"></exception>
		public void Connect()
		{
			var parsedTargetIpAddress = GetTargetIpAddress(TargetIpAddress);

			if (parsedTargetIpAddress == null)
			{
				throw new InvalidOperationException(string.Format("연결하려는 서버 정보를 확인하십시오.: {0}", parsedTargetIpAddress));
			}

			try
			{

				if (TargetSocket == null || TargetSocket.Connected == false)
				{
					TargetSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
					TargetSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
					TargetSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, true);
				}

				if (TargetSocket.Connected == false)
				{
					IPEndPoint endPoint = new IPEndPoint(parsedTargetIpAddress, Port);
					TargetSocket.Connect(endPoint);
					OnConnected(new ServerEventArgs(TargetIpAddress, Port));
					IsConnected = true;
				}
			}
			catch (SocketException ex)
			{
				OnError(new ErrorEventArgs(new Exception(string.Format("({0}){1}", ex.ErrorCode, ex.Message))));
			}
			catch (IOException ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
		}

		/// <summary>
		/// Disconnects the specified target socket.
		/// </summary>
		/// <param name="targetSocket">The target socket.</param>
		/// <exception cref="System.ArgumentNullException">targetSocket을 할당해야 합니다.</exception>
		public void Disconnect(Socket targetSocket)
		{
			if (targetSocket == null)
			{
				throw new ArgumentNullException("targetSocket을 할당해야 합니다.");
			}

			IsConnected = false;

			if (targetSocket != null)
			{
				if (targetSocket.Connected == true)
				{
					targetSocket.Close();
					OnDisconnected(new ServerEventArgs(TargetIpAddress, Port));
				}
			}
		}

		/// <summary>
		/// 지정된 Receiver에게 파일을 전송합니다.
		/// </summary>
		/// <param name="socket">The socket.</param>
		/// <param name="filePath">전송할 파일의 full path를 지정합니다.</param>
		public void SendFile(Socket socket, string filePath)
		{
			SendFile(socket, new string[] { filePath });
		}

		/// <summary>
		/// 지정된 Receiver에게 다수의 파일을 전송합니다.
		/// </summary>
		/// <param name="socket">The socket.</param>
		/// <param name="files">전송할 파일의 full path를 지정합니다.</param>
		/// <exception cref="System.InvalidOperationException"></exception>
		public void SendFile(Socket socket, string[] files)
		{
			for (int i = 0; i < files.Length; i++)
			{
				if (File.Exists(files[i]) == false)
				{
					throw new InvalidOperationException(string.Format("{0} 파일이 없습니다.", files));
				}

				files[i] = files[i];//.Replace(@"\", "/");
			}

			if (socket.Connected == false)
			{
				Connect();
			}

			Thread thread = new Thread(new ParameterizedThreadStart(InternalSendFile));
			var sfo = new SendFileObject
			{
				TargetSocket = socket,
				Files = files,
			};

			thread.Start(sfo);
		}

		private class SendFileObject
		{
			public Socket TargetSocket
			{
				get;
				set;
			}

			public string[] Files
			{
				get;
				set;
			}
		}

		private void InternalSendFile(object sendFileObject)
		{
			var sfo = sendFileObject as SendFileObject;
			string[] fileNames = sfo.Files;

			if (fileNames == null || fileNames.Length == 0)
			{
				return;
			}

			FileStream fs = null;
			Socket targetSocket = sfo.TargetSocket;

			try
			{
				// 1차 총 파일 갯수/총 파일 크기 전송
				var totalIndex = fileNames.Length;
				var totalLength = GetTotalFileLength(fileNames);
				var indexHeader = new byte[INDEX_HEADER_SIZE];		// 파일 갯수, 총 파일 크기
				var fileCountBytes = BitConverter.GetBytes(totalIndex);
				var totalLengthBytes = BitConverter.GetBytes(totalLength);
				long grandTotalSentLength = 0L;
				Func<long, double> grandPercent = x => ((double)x / (double)totalLength * 100d);

				fileCountBytes.CopyTo(indexHeader, 0);
				totalLengthBytes.CopyTo(indexHeader, fileCountBytes.Length);

				int sent = targetSocket.Send(indexHeader);

				if (sent > 0)
				{
					OnIndexHeaderSended(new FileTransferEventArgs(string.Empty, 0L, totalLength, 0, totalIndex, 0d, TargetIpAddress, Port));
				}

				for (int i = 0; i < fileNames.Length; i++)
				{
					var index = i + 1;
					var fileName = fileNames[i];
					int bufferSize = BufferSize;
					byte[] fileNameByte = Encoding.Unicode.GetBytes(Path.GetFileName(fileName));
					FileInfo fileInfo = new FileInfo(fileName);
					long fileSize = fileInfo.Length;
					byte[] fileSizeLengthByte = BitConverter.GetBytes(fileSize);
					byte[] fileNameLengthByte = BitConverter.GetBytes(fileNameByte.Length);
					byte[] header = new byte[fileNameLengthByte.Length + fileSizeLengthByte.Length + fileNameByte.Length];

					// Header 구성
					fileSizeLengthByte.CopyTo(header, 0);												// 첫번째 8byte 데이터 길이
					fileNameLengthByte.CopyTo(header, fileSizeLengthByte.Length);						// 두번째 4byte File 이름 길이
					fileNameByte.CopyTo(header, fileSizeLengthByte.Length + fileNameLengthByte.Length);	// 세번째 nbyte File 이름

					// 2차 File 길이, 파일 이름 길이, 파일 이름을 전송한다.
					sent = targetSocket.Send(header);

					if (sent > 0)
					{
						OnHeaderSended(new FileTransferEventArgs(fileName, header.Length, header.Length, index, totalIndex, grandPercent(grandTotalSentLength), TargetIpAddress, Port));
					}

					// 2차 파일 데이터를 bufferSize 만큼 전송 한다.
					long totalSendingLength = 0L;
					long totalReadingLength = 0L;
					int readingLength = 0;
					int sendingLength = 0;
					byte[] sendingData = new byte[BufferSize];

					// 전체 크기 보다 bufferSize가 크면 기본 bufferSize를 줄인다.
					if (fileSize < bufferSize)
					{
						bufferSize = (int)fileSize;
					}

					fs = File.OpenRead(fileName);

					// 데이터를 읽어들인다.
					while (targetSocket.Connected == true
						&& (readingLength = fs.Read(sendingData, 0, bufferSize)) > 0)
					{
						sendingLength = targetSocket.Send(sendingData, bufferSize, SocketFlags.Partial);
						totalSendingLength += sendingLength;
						totalReadingLength += readingLength;
						grandTotalSentLength += sendingLength;

						OnFileSending(new FileTransferEventArgs(fileName, totalSendingLength, fileSize, index, totalIndex, grandPercent(grandTotalSentLength), TargetIpAddress, Port));

						// 기본 bufferSize 보다 남은 크기가 크면 기본 bufferSize는 (전체 크기 - 읽은 총합) 이다.
						if (BufferSize > (fileSize - totalReadingLength))
						{
							bufferSize = (int)(fileSize - totalReadingLength);
						}
					}

					targetSocket.Receive(_ack, SocketFlags.Partial);		// ACK 신호를 받은 뒤 다음 파일을 전송한다.
					OnTransferCompleted(new FileTransferEventArgs(fileName, totalSendingLength, fileSize, index, totalIndex, grandPercent(grandTotalSentLength), TargetIpAddress, Port));
				}
			}
			catch (SocketException ex)
			{
				OnError(new ErrorEventArgs(new Exception(string.Format("({0}){1}", ex.ErrorCode, ex.Message))));
			}
			catch (IOException ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
			finally
			{
				if (fs != null)
				{
					fs.Close();
				}
			}
		}

		private long GetTotalFileLength(string[] fileNames)
		{
			return fileNames.Sum(x => new FileInfo(x).Length);
		}
	}
}
