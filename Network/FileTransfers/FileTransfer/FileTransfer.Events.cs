﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace GS.Common.Net
{
	partial class FileTransfer
	{
		/// <summary>
		/// Occurs when [on Started].
		/// </summary>
		public event EventHandler<ServerEventArgs> Started;

		/// <summary>
		/// Handles the <see cref="E:OnStarted"/> event.
		/// </summary>
		/// <param name="e">The <see cref="ServerEventArgs"/> instance containing the event data.</param>
		protected virtual void OnStarted(ServerEventArgs e)
		{
			if (Started != null)
			{
				Started(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Stopped].
		/// </summary>
		public event EventHandler<ServerEventArgs> Stopped;

		/// <summary>
		/// Handles the <see cref="E:OnStopped"/> event.
		/// </summary>
		/// <param name="e">The <see cref="ServerEventArgs"/> instance containing the event data.</param>
		protected virtual void OnStopped(ServerEventArgs e)
		{
			if (Stopped != null)
			{
				Stopped(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on ClientAccepted].
		/// </summary>
		public event EventHandler<SocketAsyncEventArgs> ClientAccepted;

		/// <summary>
		/// Handles the <see cref="E:OnClientAccepted"/> event.
		/// </summary>
		/// <param name="e">The <see cref="SocketAsyncEventArgs"/> instance containing the event data.</param>
		protected virtual void OnClientAccepted(SocketAsyncEventArgs e)
		{
			if (ClientAccepted != null)
			{
				ClientAccepted(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on IndexHeaderReceived].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> IndexHeaderReceived;

		/// <summary>
		/// Handles the <see cref="E:OnIndexHeaderReceived"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnIndexHeaderReceived(FileTransferEventArgs e)
		{
			if (IndexHeaderReceived != null)
			{
				IndexHeaderReceived(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on HeaderReceived].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> HeaderReceived;

		/// <summary>
		/// Handles the <see cref="E:OnHeaderReceived"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnHeaderReceived(FileTransferEventArgs e)
		{
			if (HeaderReceived != null)
			{
				HeaderReceived(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on FileReceiving].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> FileReceiving;

		/// <summary>
		/// Handles the <see cref="E:OnFileReceiving"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnFileReceiving(FileTransferEventArgs e)
		{
			if (FileReceiving != null)
			{
				FileReceiving(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on TransferCompleted].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> TransferCompleted;

		/// <summary>
		/// Handles the <see cref="E:OnCompleted"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnTransferCompleted(FileTransferEventArgs e)
		{
			if (TransferCompleted != null)
			{
				TransferCompleted(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Error].
		/// </summary>
		public event EventHandler<ErrorEventArgs> Error;

		/// <summary>
		/// Handles the <see cref="E:OnError"/> event.
		/// </summary>
		/// <param name="e">The <see cref="ErrorEventArgs"/> instance containing the event data.</param>
		protected virtual void OnError(ErrorEventArgs e)
		{
			if (Error != null)
			{
				Error(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Connected].
		/// </summary>
		public event EventHandler<ServerEventArgs> Connected;

		/// <summary>
		/// Handles the <see cref="E:OnConnected" /> event.
		/// </summary>
		/// <param name="e">The <see cref="ServerEventArgs" /> instance containing the event data.</param>
		protected virtual void OnConnected(ServerEventArgs e)
		{
			if (Connected != null)
			{
				Connected(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Disconnected].
		/// </summary>
		public event EventHandler<ServerEventArgs> Disconnected;

		/// <summary>
		/// Handles the <see cref="E:OnDisconnected"/> event.
		/// </summary>
		/// <param name="e">The <see cref="ServerEventArgs"/> instance containing the event data.</param>
		protected virtual void OnDisconnected(ServerEventArgs e)
		{
			if (Disconnected != null)
			{
				Disconnected(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on IndexHeaderSended].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> IndexHeaderSended;

		/// <summary>
		/// Handles the <see cref="E:OnIndexHeaderSended"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnIndexHeaderSended(FileTransferEventArgs e)
		{
			if (IndexHeaderSended != null)
			{
				IndexHeaderSended(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on HeaderSended].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> HeaderSended;

		/// <summary>
		/// Handles the <see cref="E:OnHeaderSended"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnHeaderSended(FileTransferEventArgs e)
		{
			if (HeaderSended != null)
			{
				HeaderSended(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on FileSending].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> FileSending;

		/// <summary>
		/// Handles the <see cref="E:OnFileSending"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnFileSending(FileTransferEventArgs e)
		{
			if (FileSending != null)
			{
				FileSending(this, e);
			}
		}
	}
}
