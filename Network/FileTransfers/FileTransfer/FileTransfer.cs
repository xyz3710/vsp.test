﻿// ****************************************************************************************************************** //
//	Domain		:	GS.Common.Net.FileTransfer
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2015년 7월 13일 월요일 오후 9:03
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	단일 소켓에 대해서 파일을 전송하거나 받을 수 있다. 
//					초기에 ReceiveAfterAcceptClient 값에 따라서 파일을 전송받을 수 있는지 여부를 결정한다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="FileTransfer.cs" company="(주)가치소프트">
//		Copyright (c) 2015. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace GS.Common.Net
{
	/// <summary>
	/// Class FileTransfer.
	/// </summary>
	[Serializable]
	public partial class FileTransfer
	{
		/// <summary>
		/// The default port
		/// </summary>
		protected const int DEFAULT_PORT = 19000;
		/// <summary>
		/// The segment size
		/// </summary>
		protected const int SEGMENT_SIZE = 1024;
		/// <summary>
		/// The header size
		/// </summary>
		protected const int HEADER_SIZE = 12;		// 첫번째 8byte 데이터 길이, 두번째 4byte File 이름 길이
		/// <summary>
		/// The index header size
		/// </summary>
		protected const int INDEX_HEADER_SIZE = 12;		// 전체 파일 갯수(int), 전체 파일 크기(long)

		#region Fields
		private bool _completed;
		private int _completedCount;
		private System.Timers.Timer _timer;
		private byte[] _ack;
		#endregion

		/// <summary>
		/// Initializes a new instance of the <see cref="FileTransfer" /> class.
		/// </summary>
		/// <param name="port">The port.</param>
		/// <param name="bufferSize">Size of the buffer.</param>
		/// <param name="targetIpAddress">The target ip address.</param>
		public FileTransfer(int port = DEFAULT_PORT, int bufferSize = SEGMENT_SIZE * 512, string targetIpAddress = null)
		{
			_ack = new byte[] { 0x06 };
			Targets = new Dictionary<string, Socket>();
			TargetIpAddress = targetIpAddress;
			Port = port;
			BufferSize = bufferSize;
			ReceiveAfterAcceptClient = true;
		}

		/// <summary>
		/// Gets or sets the targets.
		/// </summary>
		/// <value>The targets.</value>
		public Dictionary<string, Socket> Targets
		{
			get;
			set;
		}

		/// <summary>
		/// IsServerRole를 구하거나 설정합니다.
		/// </summary>
		/// <value>IsServerRole를 반환합니다.</value>
		public bool IsServerRole
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets or sets the target socket.
		/// </summary>
		/// <value>The target socket.</value>
		public Socket TargetSocket
		{
			get;
			set;
		}

		/// <summary>
		/// Port를 구하거나 설정합니다.
		/// </summary>
		/// <value>The port.</value>
		public int Port
		{
			get;
			set;
		}

		/// <summary>
		/// BufferSize를 구하거나 설정합니다.
		/// <remarks>기본 크기는 1024 * 512 Byte 입니다.</remarks>
		/// </summary>
		/// <value>The size of the buffer.</value>
		public int BufferSize
		{
			get;
			set;
		}

		/// <summary>
		/// Listener가 현재 시작중인지 여부를 구합니다.
		/// </summary>
		/// <value>true if running; otherwise, false.</value>
		public bool Running
		{
			get;
			private set;
		}

		/// <summary>
		/// 현재 소켓이 연결되어 있는지 여부를 구합니다.
		/// </summary>
		public bool IsConnected
		{
			get;
			private set;
		}

		/// <summary>
		/// Server IpAddress를 구하거나 설정합니다.
		/// </summary>
		/// <value>The server ip address.</value>
		public string TargetIpAddress
		{
			get;
			set;
		}

		/// <summary>
		/// Gets the local ip address.
		/// </summary>
		/// <returns>System.String.</returns>
		protected string GetLocalIpAddress()
		{
			IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());
			string localIpAddress = string.Empty;

			foreach (IPAddress ipAddr in ipEntry.AddressList)
			{
				byte ipHeader = ipAddr.GetAddressBytes()[0];

				if (ipHeader == 0 || ipHeader == 127 || ipHeader == 169
					|| ipAddr.AddressFamily != AddressFamily.InterNetwork)
				{
					continue;
				}

				localIpAddress = Convert.ToString(ipAddr);

				if (localIpAddress != string.Empty)
				{
					break;
				}
			}

			return localIpAddress;
		}

		/// <summary>
		/// Gets the target ip address ip address or dns host name.
		/// </summary>
		/// <param name="targetIpAddressOrHostName">Name of the target ip address or host.</param>
		/// <returns>IPAddress.</returns>
		protected IPAddress GetTargetIpAddress(string targetIpAddressOrHostName)
		{
			IPAddress targetServerIP = null;
			var hostAddress = Dns.GetHostAddresses(targetIpAddressOrHostName);

			if (hostAddress != null && hostAddress.Length > 0)
			{
				targetServerIP = IPAddress.Parse(hostAddress[0].ToString());
			}

			return targetServerIP;
		}

		/// <summary>
		/// Gets the remote identifier.
		/// </summary>
		/// <param name="socket">The socket.</param>
		/// <returns>System.String.</returns>
		protected string GetRemoteId(Socket socket)
		{
			var remoteId = string.Empty;

			if (socket == null)
			{
				return remoteId;
			}

			try
			{
				if (IsServerRole == true)
				{
					remoteId = Convert.ToString(socket.RemoteEndPoint as IPEndPoint);
				}
				else
				{
					remoteId = Convert.ToString(socket.LocalEndPoint as IPEndPoint);
				}
			}
			catch (SocketException)
			{
			}
			catch (ObjectDisposedException)
			{
			}

			return remoteId;
		}

		private class StartServerParameter
		{
			public StartServerParameter(bool appendable, int listenerCount)
			{
				ListenerCount = listenerCount;
				Appendable = appendable;
			}

			public int ListenerCount
			{
				get;
				set;
			}

			public bool Appendable
			{
				get;
				set;
			}
		}
	}
}
