﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GS.Common.Net;
using GS.Common.Utility;
using System.IO;
using GS.Common.Net.Properties;
namespace FileSenderForm
{
	public partial class SenderForm : Form
	{
		private RunningTimer<object> _runningTimer;

		public SenderForm()
		{
			InitializeComponent();

			var text = Text;
			_runningTimer = new RunningTimer<object>(null);
			_runningTimer.Elapsed += (sender, e) =>
			{
				Invoke(new MethodInvoker(() =>
				{
					var span = e.TimeSpan;

					Text = string.Format("{0}: {1:00}:{2:00}:{3:00}", text, span.Hours, span.Minutes, span.Seconds);
				}));
			};

			RegisterEventHandlers();
		}

		private void RegisterEventHandlers()
		{
			var totalFileLength = 0L;

			FileSender = new FileSender(Settings.Default.TargetIpAddress, Settings.Default.Port, Settings.Default.BufferSize);
			FileSender.Connected += (sender, e) =>
			{
				WriteLog("{0}에 {1} 포트로 연결 하였습니다.", e.IpAddress, e.Port);
			};
			FileSender.Disconnected += (sender, e) =>
			{
				WriteLog("{0}의 {1} 포트로 연결이 종료 되었습니다.", e.IpAddress, e.Port);
			};
			FileSender.Error += (sender, e) =>
			{
				WriteLog("Error: {0}", e.GetException().Message);
			};
			FileSender.IndexHeaderSended += (sender, e) =>
			{
				_runningTimer.SetRunningTime();
				WriteLog("{0}: {1:#,##0}bytes", e.TotalIndex, e.TotalSize);
				totalFileLength = e.TotalSize;
			};
			FileSender.HeaderSended += (sender, e) =>
			{
				WriteLog("{0}: {1:#,##0}bytes", e.FileName, e.TotalSize);
			};
			FileSender.Sending += (sender, e) =>
			{
				WriteLog("{0}: {1:#,##0}/{2:#,##0}", e.FileName, e.CurrentSize, e.TotalSize);
				SetCurrent(e.Percentage);
				SetTotalPercent((int)e.GrandPercentage);		// percent로 처리할 경우
			};
			FileSender.Completed += (sender, e) =>
			{
				_runningTimer.PauseRunningTime();
				WriteLog("{0}: completed", e.FileName);
				//SetTotal(e.CurrentIndex, e.TotalIndex);		// index 로 처리할 경우
			};
			btnFile.Click += (sender, e) =>
			{
				if (ofdFiles.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
				{
					TargetFiles = ofdFiles.FileNames;
				}
			};
			btnSend.Click += (sender, e) =>
			{
				FileSender.SendFile(TargetFiles);
				//EnableSendButton(false);
			};

			//EnableSendButton(true);
		}

		private string[] TargetFiles
		{
			get
			{
				return tbFiles.Lines;
			}
			set
			{
				tbFiles.Lines = value;
			}
		}

		private FileSender FileSender
		{
			get;
			set;
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);

			TargetFiles = Directory.GetFiles(@"J:\", "*.cs");
		}

		private void WriteLog(string format, params object[] args)
		{
			Invoke(new MethodInvoker(() =>
			{
				tbMessage.AppendText(string.Format(format + Environment.NewLine, args));
			}));
		}

		private void EnableSendButton(bool enabled)
		{
			btnSend.Enabled = enabled;
			btnStop.Enabled = !enabled;
		}

		private void SetCurrent(double percent)
		{
			Invoke(new MethodInvoker(() =>
			{
				pbCurrent.Value = (int)percent;
			}));
		}

		private void SetTotal(int value, int maximum)
		{
			Invoke(new MethodInvoker(() =>
			{
				pbTotal.Value = value;
				pbTotal.Maximum = maximum;
			}));
		}

		private void SetTotalPercent(int percent)
		{
			Invoke(new MethodInvoker(() =>
			{
				pbTotal.Value = percent;
				pbTotal.Maximum = 100;
			}));
		}
	}
}
