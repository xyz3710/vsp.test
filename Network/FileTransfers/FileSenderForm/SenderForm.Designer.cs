﻿namespace FileSenderForm
{
	partial class SenderForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.tbFiles = new System.Windows.Forms.TextBox();
			this.pbTotal = new System.Windows.Forms.ProgressBar();
			this.pbCurrent = new System.Windows.Forms.ProgressBar();
			this.tbMessage = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.btnStop = new System.Windows.Forms.Button();
			this.btnFile = new System.Windows.Forms.Button();
			this.btnSend = new System.Windows.Forms.Button();
			this.ofdFiles = new System.Windows.Forms.OpenFileDialog();
			this.tlpMain.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 3;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tlpMain.Controls.Add(this.tbFiles, 0, 1);
			this.tlpMain.Controls.Add(this.pbTotal, 0, 4);
			this.tlpMain.Controls.Add(this.pbCurrent, 0, 3);
			this.tlpMain.Controls.Add(this.tbMessage, 0, 2);
			this.tlpMain.Controls.Add(this.tableLayoutPanel1, 0, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 5;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34.97758F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65.02242F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
			this.tlpMain.Size = new System.Drawing.Size(784, 561);
			this.tlpMain.TabIndex = 0;
			// 
			// tbFiles
			// 
			this.tlpMain.SetColumnSpan(this.tbFiles, 3);
			this.tbFiles.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbFiles.Location = new System.Drawing.Point(3, 41);
			this.tbFiles.Multiline = true;
			this.tbFiles.Name = "tbFiles";
			this.tbFiles.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbFiles.Size = new System.Drawing.Size(778, 150);
			this.tbFiles.TabIndex = 1;
			// 
			// pbTotal
			// 
			this.tlpMain.SetColumnSpan(this.pbTotal, 3);
			this.pbTotal.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbTotal.Location = new System.Drawing.Point(3, 525);
			this.pbTotal.Name = "pbTotal";
			this.pbTotal.Size = new System.Drawing.Size(778, 33);
			this.pbTotal.TabIndex = 3;
			// 
			// pbCurrent
			// 
			this.tlpMain.SetColumnSpan(this.pbCurrent, 3);
			this.pbCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbCurrent.Location = new System.Drawing.Point(3, 487);
			this.pbCurrent.Name = "pbCurrent";
			this.pbCurrent.Size = new System.Drawing.Size(778, 32);
			this.pbCurrent.TabIndex = 2;
			// 
			// tbMessage
			// 
			this.tlpMain.SetColumnSpan(this.tbMessage, 3);
			this.tbMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbMessage.Location = new System.Drawing.Point(3, 197);
			this.tbMessage.Multiline = true;
			this.tbMessage.Name = "tbMessage";
			this.tbMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbMessage.Size = new System.Drawing.Size(778, 284);
			this.tbMessage.TabIndex = 0;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tlpMain.SetColumnSpan(this.tableLayoutPanel1, 3);
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.Controls.Add(this.btnStop, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnFile, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnSend, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 38);
			this.tableLayoutPanel1.TabIndex = 5;
			// 
			// btnStop
			// 
			this.btnStop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnStop.Location = new System.Drawing.Point(525, 3);
			this.btnStop.Name = "btnStop";
			this.btnStop.Size = new System.Drawing.Size(256, 32);
			this.btnStop.TabIndex = 2;
			this.btnStop.Text = "Sto&p";
			this.btnStop.UseVisualStyleBackColor = true;
			// 
			// btnFile
			// 
			this.btnFile.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnFile.Location = new System.Drawing.Point(3, 3);
			this.btnFile.Name = "btnFile";
			this.btnFile.Size = new System.Drawing.Size(255, 32);
			this.btnFile.TabIndex = 0;
			this.btnFile.Text = "파일 선택";
			this.btnFile.UseVisualStyleBackColor = true;
			// 
			// btnSend
			// 
			this.btnSend.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnSend.Location = new System.Drawing.Point(264, 3);
			this.btnSend.Name = "btnSend";
			this.btnSend.Size = new System.Drawing.Size(255, 32);
			this.btnSend.TabIndex = 1;
			this.btnSend.Text = "&Send";
			this.btnSend.UseVisualStyleBackColor = true;
			// 
			// ofdFiles
			// 
			this.ofdFiles.Multiselect = true;
			this.ofdFiles.RestoreDirectory = true;
			this.ofdFiles.ShowReadOnly = true;
			// 
			// SenderForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 561);
			this.Controls.Add(this.tlpMain);
			this.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "SenderForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "File Sender";
			this.tlpMain.ResumeLayout(false);
			this.tlpMain.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.Button btnFile;
		private System.Windows.Forms.ProgressBar pbCurrent;
		private System.Windows.Forms.ProgressBar pbTotal;
		private System.Windows.Forms.TextBox tbMessage;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Button btnSend;
		private System.Windows.Forms.OpenFileDialog ofdFiles;
		private System.Windows.Forms.TextBox tbFiles;
		private System.Windows.Forms.Button btnStop;
	}
}

