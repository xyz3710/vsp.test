﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GS.Common.Net
{
	partial class FileSender
	{
		/// <summary>
		/// Occurs when [on Connected].
		/// </summary>
		public event EventHandler<ServerEventArgs> Connected;

		/// <summary>
		/// Handles the <see cref="E:OnConnected" /> event.
		/// </summary>
		/// <param name="e">The <see cref="ServerEventArgs" /> instance containing the event data.</param>
		protected virtual void OnConnected(ServerEventArgs e)
		{
			if (Connected != null)
			{
				Connected(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Disconnected].
		/// </summary>
		public event EventHandler<ServerEventArgs> Disconnected;

		/// <summary>
		/// Handles the <see cref="E:OnDisconnected"/> event.
		/// </summary>
		/// <param name="e">The <see cref="ServerEventArgs"/> instance containing the event data.</param>
		protected virtual void OnDisconnected(ServerEventArgs e)
		{
			if (Disconnected != null)
			{
				Disconnected(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on IndexHeaderSended].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> IndexHeaderSended;

		/// <summary>
		/// Handles the <see cref="E:OnIndexHeaderSended"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnIndexHeaderSended(FileTransferEventArgs e)
		{
			if (IndexHeaderSended != null)
			{
				IndexHeaderSended(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on HeaderSended].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> HeaderSended;

		/// <summary>
		/// Handles the <see cref="E:OnHeaderSended"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnHeaderSended(FileTransferEventArgs e)
		{
			if (HeaderSended != null)
			{
				HeaderSended(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Sending].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> Sending;

		/// <summary>
		/// Handles the <see cref="E:OnSending"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs"/> instance containing the event data.</param>
		protected virtual void OnSending(FileTransferEventArgs e)
		{
			if (Sending != null)
			{
				Sending(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Completed].
		/// </summary>
		public event EventHandler<FileTransferEventArgs> Completed;

		/// <summary>
		/// Handles the <see cref="E:OnCompleted"/> event.
		/// </summary>
		/// <param name="e">The <see cref="FileTransferEventArgs "/> instance containing the event data.</param>
		protected virtual void OnCompleted(FileTransferEventArgs e)
		{
			if (Completed != null)
			{
				Completed(this, e);
			}
		}

		/// <summary>
		/// Occurs when [on Error].
		/// </summary>
		public event EventHandler<ErrorEventArgs> Error;

		/// <summary>
		/// Handles the <see cref="E:OnError"/> event.
		/// </summary>
		/// <param name="e">The <see cref="ErrorEventArgs"/> instance containing the event data.</param>
		protected virtual void OnError(ErrorEventArgs e)
		{
			if (Error != null)
			{
				Error(this, e);
			}
		}
	}
}
