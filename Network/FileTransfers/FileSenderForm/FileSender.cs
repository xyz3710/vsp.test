﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using System.Data.Linq;

namespace GS.Common.Net
{
	/// <summary>
	/// File 전송용 송신자 class 입니다.
	/// </summary>
	[Serializable]
	public partial class FileSender
	{
		private const int DEFAULT_PORT = 19000;
		private const int SEGMENT_SIZE = 1024;
		private const int INDEX_HEADER_SIZE = 12;		// 전체 파일 갯수(int), 전체 파일 크기(long)

		private byte[] _ack;

		#region Constructors
		/// <summary>
		/// FileSender class의 새 인스턴스를 초기화 합니다.
		/// <remarks>기본 Port를 19090번을 사용합니다.</remarks>
		/// </summary>
		/// <param name="targetIpAddress">File을 전송할 Receiver의 IpAddress입니다.</param>
		/// <param name="port">The port.</param>
		/// <param name="bufferSize">Size of the buffer.</param>
		public FileSender(string targetIpAddress = null, int port = DEFAULT_PORT, int bufferSize = SEGMENT_SIZE * 512)
		{
			if (string.IsNullOrEmpty(targetIpAddress) == true)
			{
				TargetIpAddress = GetLocalIpAddress();
			}
			else
			{
				TargetIpAddress = targetIpAddress;
			}

			Port = port;
			BufferSize = bufferSize;
			_ack = new byte[] { 0x06 };
		}

		private string GetLocalIpAddress()
		{
			IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());
			string localIpAddress = string.Empty;

			foreach (IPAddress ipAddr in ipEntry.AddressList)
			{
				byte ipHeader = ipAddr.GetAddressBytes()[0];

				if (ipHeader == 0 || ipHeader == 127 || ipHeader == 169
					|| ipAddr.AddressFamily != AddressFamily.InterNetwork)
				{
					continue;
				}

				localIpAddress = Convert.ToString(ipAddr);

				if (localIpAddress != string.Empty)
				{
					break;
				}
			}

			return localIpAddress;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Server IpAddress를 구하거나 설정합니다.
		/// </summary>
		/// <value>The server ip address.</value>
		public string TargetIpAddress
		{
			get;
			set;
		}

		/// <summary>
		/// Port를 구하거나 설정합니다.
		/// </summary>
		/// <value>The port.</value>
		public int Port
		{
			get;
			set;
		}

		/// <summary>
		/// BufferSize를 구하거나 설정합니다.
		/// </summary>
		/// <value>The size of the buffer.</value>
		public int BufferSize
		{
			get;
			set;
		}
		#endregion

		#region SendFile
		/// <summary>
		/// 지정된 Receiver에게 파일을 전송합니다.
		/// </summary>
		/// <param name="filePath">전송할 파일의 full path를 지정합니다.</param>
		public void SendFile(string filePath)
		{
			SendFile(new string[] { filePath });
		}

		/// <summary>
		/// 지정된 Receiver에게 다수의 파일을 전송합니다.
		/// </summary>
		/// <param name="filePaths">전송할 파일의 full path를 지정합니다.</param>
		public void SendFile(string[] filePaths)
		{
			for (int i = 0; i < filePaths.Length; i++)
			{
				if (File.Exists(filePaths[i]) == false)
				{
					throw new InvalidOperationException(string.Format("{0} 파일이 없습니다.", filePaths));
				}

				filePaths[i] = filePaths[i];//.Replace(@"\", "/");
			}

			Thread thread = new Thread(new ParameterizedThreadStart(InternalSendFile));

			thread.Start(filePaths);
		}

		private void InternalSendFile(object sendedFileName)
		{
			string[] fileNames = sendedFileName as string[];

			if (fileNames == null || fileNames.Length == 0)
			{
				return;
			}

			Socket clientSocket = null;
			FileStream fs = null;
			var targetIpAddress = GetTargetIpAddress();

			if (targetIpAddress == null)
			{
				throw new InvalidOperationException(string.Format("연결하려는 서버 정보를 확인하십시오.: {0}", TargetIpAddress));
			}

			try
			{
				IPEndPoint endPoint = new IPEndPoint(targetIpAddress, Port);

				if (clientSocket == null)
				{
					clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
				}

				if (clientSocket.Connected == false)
				{
					clientSocket.Connect(endPoint);
					OnConnected(new ServerEventArgs(TargetIpAddress, Port));
				}

				// 1차 총 파일 갯수/총 파일 크기 전송
				var totalIndex = fileNames.Length;
				var totalLength = fileNames.Sum(x => new FileInfo(x).Length);
				var indexHeader = new byte[INDEX_HEADER_SIZE];		// 파일 갯수, 총 파일 크기
				var fileCountBytes = BitConverter.GetBytes(totalIndex);
				var totalLengthBytes = BitConverter.GetBytes(totalLength);
				long grandTotalSentLength = 0L;
				Func<long, double> grandPercent = x => ((double)x / (double)totalLength * 100d);

				fileCountBytes.CopyTo(indexHeader, 0);
				totalLengthBytes.CopyTo(indexHeader, fileCountBytes.Length);

				int sent = clientSocket.Send(indexHeader);

				if (sent > 0)
				{
					OnIndexHeaderSended(new FileTransferEventArgs(string.Empty, 0L, totalLength, 0, totalIndex, 0d, TargetIpAddress, Port));
				}

				for (int i = 0; i < fileNames.Length; i++)
				{
					var index = i + 1;
					var fileName = fileNames[i];
					int bufferSize = BufferSize;
					byte[] fileNameByte = Encoding.Unicode.GetBytes(Path.GetFileName(fileName));
					FileInfo fileInfo = new FileInfo(fileName);
					long fileSize = fileInfo.Length;
					byte[] fileSizeLengthByte = BitConverter.GetBytes(fileSize);
					byte[] fileNameLengthByte = BitConverter.GetBytes(fileNameByte.Length);
					byte[] header = new byte[fileNameLengthByte.Length + fileSizeLengthByte.Length + fileNameByte.Length];

					// Header 구성
					fileSizeLengthByte.CopyTo(header, 0);												// 첫번째 8byte 데이터 길이
					fileNameLengthByte.CopyTo(header, fileSizeLengthByte.Length);						// 두번째 4byte File 이름 길이
					fileNameByte.CopyTo(header, fileSizeLengthByte.Length + fileNameLengthByte.Length);	// 세번째 nbyte File 이름

					// 2차 File 길이, 파일 이름 길이, 파일 이름을 전송한다.
					sent = clientSocket.Send(header);

					if (sent > 0)
					{
						OnHeaderSended(new FileTransferEventArgs(fileName, header.Length, header.Length, index, totalIndex, grandPercent(grandTotalSentLength), TargetIpAddress, Port));
					}

					// 2차 파일 데이터를 bufferSize 만큼 전송 한다.
					long totalSendingLength = 0L;
					long totalReadingLength = 0L;
					int readingLength = 0;
					int sendingLength = 0;
					byte[] sendingData = new byte[BufferSize];

					// 전체 크기 보다 bufferSize가 크면 기본 bufferSize를 줄인다.
					if (fileSize < bufferSize)
					{
						bufferSize = (int)fileSize;
					}

					fs = File.OpenRead(fileName);

					// 데이터를 읽어들인다.
					while (clientSocket.Connected == true
						&& (readingLength = fs.Read(sendingData, 0, bufferSize)) > 0)
					{
						sendingLength = clientSocket.Send(sendingData, bufferSize, SocketFlags.Partial);
						totalSendingLength += sendingLength;
						totalReadingLength += readingLength;
						grandTotalSentLength += sendingLength;

						OnSending(new FileTransferEventArgs(fileName, totalSendingLength, fileSize, index, totalIndex, grandPercent(grandTotalSentLength), TargetIpAddress, Port));

						// 기본 bufferSize 보다 남은 크기가 크면 기본 bufferSize는 (전체 크기 - 읽은 총합) 이다.
						if (BufferSize > (fileSize - totalReadingLength))
						{
							bufferSize = (int)(fileSize - totalReadingLength);
						}
					}

					OnCompleted(new FileTransferEventArgs(fileName, totalSendingLength, fileSize, index, totalIndex, grandPercent(grandTotalSentLength), TargetIpAddress, Port));
					clientSocket.Receive(_ack);		// 전송을 ACK 신호를 받은 뒤 다음 파일을 전송한다.
				}
			}
			catch (SocketException ex)
			{
				OnError(new ErrorEventArgs(new Exception(string.Format("({0}){1}", ex.ErrorCode, ex.Message))));
			}
			catch (IOException ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
			catch (Exception ex)
			{
				OnError(new ErrorEventArgs(ex));
			}
			finally
			{
				if (fs != null)
				{
					fs.Close();
				}

				//if (clientSocket != null)
				//{
				//	if (clientSocket.Connected == true)
				//	{
				//		clientSocket.Close();
				//		OnDisconnected(new ServerEventArgs(TargetIpAddress, Port));
				//	}
				//}
			}
		}

		private IPAddress GetTargetIpAddress()
		{
			IPAddress targetServerIP = null;
			var hostAddress = Dns.GetHostAddresses(TargetIpAddress);

			if (hostAddress != null && hostAddress.Length > 0)
			{
				targetServerIP = IPAddress.Parse(hostAddress[0].ToString());
			}

			return targetServerIP;
		}
		#endregion
	}
}
