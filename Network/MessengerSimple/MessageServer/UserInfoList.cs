using System;
using System.Net.Sockets;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace MessageServer
{
	public class UserInfoList
	{
		private static Hashtable clientTable = new Hashtable();

		public static bool AddClient(string userName, string id)
		{
			lock (clientTable)
			{
				//사용자 아이디가 이미 있는 경우
				if (clientTable.ContainsValue(userName))
				{
					return false;
				}
				else
				{
					//GUID와 사용자 아이디를 추가
					clientTable.Add(id, userName);
					return true;
				}
			}
		}

		public static bool RemoveClient(string userName, string id)
		{
			lock (clientTable)
			{
				//사용자 아이디가 이미 있는 경우
				if (clientTable.ContainsValue(userName))
				{
					clientTable.Remove(id);
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		//사용자 정보 리스트를 얻기 위한 속성(Property) 정의
		public static Hashtable GetList
		{
			get
			{
				return clientTable;
			}
		}
	}
}

