using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.Net.Sockets;

namespace MessageClient
{
	public delegate void DisplayMessage(string message);

	public class ClientForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox tbMessageHistory;
		private System.Windows.Forms.TextBox tbMessage;
		private System.Windows.Forms.ListBox lbUsers;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox tbUserName;
		private System.Windows.Forms.Button btnConnect;

		private System.ComponentModel.Container components = null;

		private string _userId;
		private string _userName;
		//서버에게 처음으로 보내는 메세지(사용자 아이디) 인지, 아닌지
		private bool _firstTime;
		private TcpClient _client;
		private byte[] _recByte;

		private Encoding _encoding;

		//Constructor
		public ClientForm()
		{
			_encoding = Encoding.Default;
			_recByte = new byte[1024];
			_firstTime = true;
			InitializeComponent();
		}

		public void GetStreamMsg(IAsyncResult ar)
		{
			int byteCount;

			try
			{
				byteCount = (_client.GetStream()).EndRead(ar);

				//서버와 접속이 끊김
				if (byteCount < 1)
				{
					Disconnect();
					MessageBox.Show("접속끊김!!");
					return;
				}

				//메세지 해석
				BuildText(_recByte, 0, byteCount);

				//비동기 수신 시작
				if (_firstTime == false)
				{
					AsyncCallback GetMsgCallback = new AsyncCallback(GetStreamMsg);
					(_client.GetStream()).BeginRead(_recByte, 0, 1024, GetMsgCallback, this);
				}
			}
			catch (Exception ed)
			{
				Disconnect();
				MessageBox.Show("Exception Occured :" + ed.ToString());
			}
		}

		public void BuildText(byte[] dataByte, int offset, int count)
		{
			string tmp = _encoding.GetString(dataByte, offset, count);

			char[] spliters = { '@' };
			//서버로부터 온 첫번째 메세지이면

			if (_firstTime == true)
			{
				//분리자 '@'로 토큰으로 나눔
				string[] tempString = tmp.Split(spliters);

				//사용자 아이디 에러
				if (tempString[0] == "미안합니다")
				{
					object[] temp = { tempString[1] };
					this.Invoke(new DisplayMessage(DisplayText), temp);
					Disconnect();
				}
				else
				{
					//클라이언트의 GUID
					this._userId = tempString[0];

					for (int i = 1; i < tempString.Length; i++)
					{
						//서버에 접속한 사용자 아이디
						object[] temp = { tempString[i] };

						//UI(user interface) 쓰레드가 아닌 Worker 쓰레드이므로
						//Invoke()를 호출해서 listbox를 업데이트 해야 한다
						this.Invoke(new DisplayMessage(AddUser), temp);
					}

					_firstTime = false;

					AsyncCallback GetMsgCallback = new AsyncCallback(GetStreamMsg);
					(_client.GetStream()).BeginRead(_recByte, 0, 1024, GetMsgCallback, this);
				}
			}

			//서버와 처음으로 주고 받는 메세지가 아닌경우
			//즉 이벤트내용(접속됨, 접속끊김)이거나 채팅 내용을 담고 있는 메세지 인경우
			else
			{
				//자신의 GUID가 있다면 이벤트 내용
				if (tmp.IndexOf(this._userId) >= 0)
				{
					string[] tempString = tmp.Split(spliters);
					//새로운 사용자가 접속
					if (tempString[1] == "접속됨")
					{
						object[] temp = { tempString[2] };
						this.Invoke(new DisplayMessage(AddUser), temp);
					}
					//기존의 사용자 접속 끊음
					else if (tempString[1] == "접속끊김")
					{
						object[] temp = { tempString[2] };
						this.Invoke(new DisplayMessage(RemoveUser), temp);
					}
				}
				else
				{
					//채 내용
					tmp += "\r\n";
					object[] temp = { tmp };
					//DisplayText 
					this.Invoke(new DisplayMessage(DisplayText), temp);
				}
			}
		}

		private void RemoveUser(string user)
		{
			if (lbUsers.Items.Contains(user))
				lbUsers.Items.Remove(user);

			DisplayText(user + " 채팅방을 떠나셨습니다\r\n");
		}

		private void AddUser(string user)
		{
			if (!lbUsers.Items.Contains(user))
				lbUsers.Items.Add(user);

			if (!_firstTime)
				DisplayText(user + " 채팅방에 참여하셨습니다.\r\n");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					Disconnect();
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//Method to send a message to the server
		public void SendMsg(string msg)
		{
			NetworkStream myNts = _client.GetStream();
			msg += "\r\n";
			Byte[] byteSend = _encoding.GetBytes(msg.ToCharArray());
			myNts.Write(byteSend, 0, byteSend.Length);
			myNts.Flush();
		}

		//Method to Display Text in the TextBox
		public void DisplayText(string msg)
		{
			tbMessageHistory.AppendText(msg);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbMessageHistory = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tbUserName = new System.Windows.Forms.TextBox();
			this.btnConnect = new System.Windows.Forms.Button();
			this.tbMessage = new System.Windows.Forms.TextBox();
			this.lbUsers = new System.Windows.Forms.ListBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tbMessageHistory
			// 
			this.tbMessageHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			| System.Windows.Forms.AnchorStyles.Left)));
			this.tbMessageHistory.Enabled = false;
			this.tbMessageHistory.Location = new System.Drawing.Point(0, 0);
			this.tbMessageHistory.Multiline = true;
			this.tbMessageHistory.Name = "tbMessageHistory";
			this.tbMessageHistory.ReadOnly = true;
			this.tbMessageHistory.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbMessageHistory.Size = new System.Drawing.Size(456, 278);
			this.tbMessageHistory.TabIndex = 2;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tbUserName);
			this.groupBox1.Controls.Add(this.btnConnect);
			this.groupBox1.Location = new System.Drawing.Point(456, 240);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(160, 72);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "사용자 아이디";
			// 
			// tbUserName
			// 
			this.tbUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.tbUserName.Location = new System.Drawing.Point(8, 16);
			this.tbUserName.Name = "tbUserName";
			this.tbUserName.Size = new System.Drawing.Size(143, 21);
			this.tbUserName.TabIndex = 0;
			// 
			// btnConnect
			// 
			this.btnConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnConnect.Location = new System.Drawing.Point(8, 40);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(144, 27);
			this.btnConnect.TabIndex = 1;
			this.btnConnect.Text = "접속";
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// tbMessage
			// 
			this.tbMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.tbMessage.Enabled = false;
			this.tbMessage.Location = new System.Drawing.Point(8, 288);
			this.tbMessage.Name = "tbMessage";
			this.tbMessage.Size = new System.Drawing.Size(448, 21);
			this.tbMessage.TabIndex = 1;
			this.tbMessage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
			// 
			// lbUsers
			// 
			this.lbUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			| System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.lbUsers.ItemHeight = 12;
			this.lbUsers.Location = new System.Drawing.Point(456, 0);
			this.lbUsers.Name = "lbUsers";
			this.lbUsers.Size = new System.Drawing.Size(160, 232);
			this.lbUsers.TabIndex = 3;
			// 
			// ClientForm
			// 
			this.AcceptButton = this.btnConnect;
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(616, 315);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.lbUsers);
			this.Controls.Add(this.tbMessage);
			this.Controls.Add(this.tbMessageHistory);
			this.Name = "ClientForm";
			this.Text = "메신저 클라이언트";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.Run(new ClientForm());
		}

		private void Disconnect()
		{
			if (_client != null)
			{
				_client.Close();
				_client = null;
			}
			//Reset the Buttons and Variables

			if (InvokeRequired == true)
			{
				Invoke(new MethodInvoker(() =>
				{
					lbUsers.Items.Clear();
					btnConnect.Text = "접속";
					tbUserName.Enabled = true;
					tbMessage.Enabled = false;
					this.AcceptButton = btnConnect;
					tbUserName.Focus();
				}));
			}

			_firstTime = true;
			_userId = "";
		}

		private void btnConnect_Click(object sender, System.EventArgs e)
		{
			//If user Cliked Connect
			if (btnConnect.Text == "접속" && tbUserName.Text != "")
			{
				try
				{
					//서버에 접속
					_client = new TcpClient("localhost", 1980);
					DisplayText("서버에 접속됨 ...\r\n");

					//비동기 수신 시작
					AsyncCallback GetMsgCallback = new AsyncCallback(GetStreamMsg);
					(_client.GetStream()).BeginRead(_recByte, 0, 1024, GetMsgCallback, null);

					//사용자 아이디 전송
					SendMsg(tbUserName.Text);
					this._userName = tbUserName.Text;

					string tmp = " [" + _userName + "]";
					this.Text += tmp;
					btnConnect.Text = "종료";
					tbUserName.Enabled = false;
					tbMessage.Enabled = true;
					this.AcceptButton = null;
					tbMessage.Focus();
				}
				catch
				{
					Disconnect();
					MessageBox.Show("서버에 접속할 수 없습니다...");
				}
			}
			else if (btnConnect.Text == "종료")
			{
				Close();
			}
		}

		private void OnKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if ((e.KeyChar == (int)Keys.Enter) && tbMessage.Text != "")
			{
				//클라이언트의 채팅 내용 전송
				SendMsg(tbMessage.Text);
				tbMessage.Text = "";
			}
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);
			tbUserName.Text = DateTime.Now.ToString("ss_fff");
			btnConnect.PerformClick();
		}
	}
}
