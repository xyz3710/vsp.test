﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.ICommand
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 2일 수요일 오후 9:11
/*	Purpose		:	Client에 Command를 실행할 수 있는 Interface 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// Client에 Command를 실행할 수 있는 Interface 입니다.
	/// </summary>
	public interface ICommand
	{
		/// <summary>
		/// 명령을 실행합니다.
		/// </summary>
		/// <returns>성공 여부를 return 합니다.</returns>
		bool Execute();
	}
}
