﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.ClientContext
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 4일 금요일 오후 2:38
/*	Purpose		:	DASiT.FX.Win.Messenger.CallbackObject를 구현한 class와 추가적인 데이터 교환을 위한 Context class입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// <seealso cref="iDASiT.FX.Win.Messenger.CallbackObject"/>를 구현한 class와 추가적인 데이터 교환을 위한 Context class입니다.
 	/// </summary>
	[Serializable]
	public class ClientContext : ILogicalThreadAffinative
	{
		#region Fields
		private string _clientIpAddress;
		private string _userId;
		#endregion

		#region Constructors
		/// <summary>
		/// ClientContext class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public ClientContext()
		{
		}

		/// <summary>
		/// ClientContext class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="clientIpAddress">Context를 호출한 Client의 IpAddress 입니다.</param>
		public ClientContext(string clientIpAddress)
			: this()
		{
			_clientIpAddress = clientIpAddress;
		}

		/// <summary>
		/// ClientContext class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="clientIpAddress">Context를 호출한 Client의 IpAddress 입니다.</param>
		/// <param name="userId">Context를 호출한 Client의 UserId 입니다.</param>
		public ClientContext(string clientIpAddress, string userId)
			: this()
		{
			_clientIpAddress = clientIpAddress;
			_userId = userId;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Context의 이름을 구합니다.
		/// </summary>
		public static string ContextName
		{
			get
			{
				return "ClientInfo";
			}
		}

		///
		/// ClientIpAddress를 구하거나 설정합니다.
		/// </summary>
		public string ClientIpAddress
		{
			get
			{
				return _clientIpAddress;
			}
			set
			{
				_clientIpAddress = value;
			}
		}

		/// <summary>
		/// Context를 호출한 Client의 UserId를 구하거나 설정합니다.
		/// </summary>
		public string UserId
		{
			get
			{
				return _userId;
			}
			set
			{
				_userId = value;
			}
		}
		#endregion
	}
}
