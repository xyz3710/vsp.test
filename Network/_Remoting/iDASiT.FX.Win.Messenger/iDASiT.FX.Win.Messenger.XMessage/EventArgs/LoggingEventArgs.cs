﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.LoggingEventArgs
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 4일 금요일 오후 12:37
/*	Purpose		:	Login/Logout Event를 위한 EventArgs입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// Login/Logout Event를 위한 EventArgs입니다.
	/// </summary>
	[Serializable]
	public class LoggingEventArgs : MessageEventArgs
	{
		#region Fields
		private string _userId;
		#endregion

		#region Constructors

		/// <summary>
		/// Login Event에 대한 LogEventArgs class의 새 인스턴스를 초기화 합니다.
		/// <remarks>(모든 client에 로그인 message를 BroadCasting 합니다.)</remarks>
		/// </summary>
		/// <param name="userId">Login/Logout한 UserId입니다.</param>
		/// <param name="fromIpAddress">Login/Logout한 client의 IpAddress입니다.</param>
		public LoggingEventArgs(string userId, string fromIpAddress)
			: this(SendType.BroadCast, userId, fromIpAddress, null)
		{
		}

		/// <summary>
		/// Login Event에 대한 LogEventArgs class의 새 인스턴스를 초기화 합니다.
		/// <remarks>(특정 client에만 알립니다.)</remarks>
		/// </summary>
		/// <param name="userId">Login/Logout한 UserId입니다.</param>
		/// <param name="fromIpAddress">Login/Logout한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">알릴 client의 IpAddress입니다.</param>
		public LoggingEventArgs(string userId, string fromIpAddress, string toIpAddress)
			: this(SendType.SendTo, userId, fromIpAddress, toIpAddress)
		{
		}
        
		/// <summary>
		/// Login Event에 대한 LogEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="sendType">Login/Logout을 전달할 message type입니다.</param>
		/// <param name="userId">Login/Logout한 UserId입니다.</param>
		/// <param name="fromIpAddress">Login/Logout한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">알릴 client의 IpAddress입니다.</param>
		public LoggingEventArgs(SendType sendType, string userId, string fromIpAddress, string toIpAddress)
			: this(sendType, userId, fromIpAddress, toIpAddress, null)
		{
		}

        /// <summary>
		/// Login Event에 대한 LogEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="sendType">Login/Logout을 전달할 message type입니다.</param>
		/// <param name="userId">Login/Logout한 UserId입니다.</param>
		/// <param name="fromIpAddress">Login/Logout한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="tag">Message 전달시 사용될 Tag입니다.</param>
		public LoggingEventArgs(SendType sendType, string userId, string fromIpAddress, string toIpAddress, object tag)
			: base(EventSource.Login, sendType, userId, fromIpAddress, toIpAddress, tag)
		{
			_userId = userId;
		}

		/// <summary>
		/// LogEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="eventSource">Event가 발생한 Source 입니다.</param>
		/// <param name="sendType">Login/Logout을 전달할 message type입니다.</param>
		/// <param name="userId">Login/Logout한 UserId입니다.</param>
		/// <param name="fromIpAddress">Login/Logout한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="tag">Message 전달시 사용될 Tag입니다.</param>
		public LoggingEventArgs(EventSource eventSource, SendType sendType, string userId, string fromIpAddress, string toIpAddress, object tag)
			: base(eventSource, sendType, userId, fromIpAddress, toIpAddress, tag)
		{
			_userId = userId;
		}
		#endregion

		#region Properties
		/// <summary>
		/// UserId를 구하거나 설정합니다.
		/// </summary>
		public string UserId
		{
			get
			{
				return _userId;
			}
			set
			{
				_userId = value;
			}
		}

		public new string Message
		{
			get
			{
				return UserId;
			}
			set
			{
				UserId = value;
			}
		}
        #endregion        
	}
}
