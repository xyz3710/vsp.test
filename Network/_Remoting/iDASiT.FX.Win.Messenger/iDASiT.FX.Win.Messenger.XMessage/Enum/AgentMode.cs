﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.AgentMode
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 2일 수요일 오후 8:15
/*	Purpose		:	Agent의 Mode을 선택합니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// Agent의 Mode을 선택합니다.
	/// </summary>
	public enum AgentMode
	{
		/// <summary>
		/// Agent를 Client Mode로 작동하도록 합니다.
		/// </summary>
		Client = 0,
		/// <summary>
		/// Agent를 Server에 접근해서 <seealso cref="iDASiT.FX.Win.Messenger.AgentMode.Client"/> Mode로 생성된 Client를 제어 합니다.
		/// </summary>
		Controller,
	}
}
