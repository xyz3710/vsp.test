﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.SendType
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 3일 목요일 오후 9:58
/*	Purpose		:	전송할 Message Type을 선택합니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// 전송할 Message Type을 선택합니다.
	/// </summary>
	[Serializable]
	public enum SendType
	{
		/// <summary>
		/// Message를 자신을 제외한 Client에 전파 합니다.
		/// </summary>
		BroadCast = 0,
		/// <summary>
		/// Message를 특정 Client에만 보냅니다.
		/// </summary>
		SendTo,
	}
}
