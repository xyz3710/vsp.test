﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.EventSource
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 4일 금요일 오후 1:22
/*	Purpose		:	전송 되는 Event Message의 원본을 선택합니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	변경시 XMessage의 InternalSendMessage 부분을 수정해 줘야 한다.
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// 전송 되는 Event Message의 원본을 선택합니다.
	/// </summary>
	[Serializable]
	public enum EventSource
	{
		/// <summary>
		/// Message를 전송합니다.
		/// </summary>
		Message = 0,
		/// <summary>
		/// Login Message를 전송합니다.
		/// </summary>
		Login,
		/// <summary>
		/// Logout Message를 전송합니다.
		/// </summary>
		Logout,
		/// <summary>
		/// 비정상 Logout Message를 전송합니다.
		/// </summary>
		AbnormalLogout,
		/// <summary>
		/// PutFile Message를 전송합니다.
		/// </summary>
		PutFile,
		/// <summary>
		/// GetFile Receiver 요청 Message를 전송합니다.
		/// </summary>
		GetFileReceiveRequest,
		/// <summary>
		/// GetFile Sender 요청 Message를 전송합니다.
		/// </summary>
		GetFileSendRequest,
		/// <summary>
		/// GetFileList Message를 전송합니다.
		/// </summary>
		GetFileList,
		RegisterUserAction,
		UnregisterUserAction,
		UpdateUserAction,
	}
}
