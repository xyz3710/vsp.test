﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.FileSender
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 22일 화요일 오후 4:00
/*	Purpose		:	File 전송용 송신자 class 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using iDASiT.FX.Win.Messenger;

namespace iDASiT.FX.Win.Messenger
{
	/// <summary>
	/// File 전송용 송신자 class 입니다.
	/// </summary>
	[Serializable]
	public class FileSender
	{
		#region Fields
		private const int DEFAULT_PORT = 19090;
		private const int SEGMENT_SIZE = 1024;

		private string _targetIpAddress;
		private int _port;
		private int _bufferSize;
		#endregion

		#region Unused
		/*
		#region Event Delegator
		/// <summary>
		/// 서버와 접속/종료 이벤트를 처리할 메서드를 나타냅니다. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void ServiceEventHandler(object sender, ServerInfoEventArgs e);
		/// <summary>
		/// packet이 전송되면 이벤트를 처리할 메서드를 나타냅니다. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void TransferEventHandler(object sender, TransferInfoEventArgs e);
		/// <summary>
		/// packet이 전송되면 이벤트를 처리할 메서드를 나타냅니다. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void ErrorEventHandler(object sender, iDASiT.FX.Win.Messenger.ErrorEventArgs e);
		#endregion
		*/
		#endregion

		#region Event
		/// <summary>
		/// 서버와 접속이 되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler Connected;
		/// <summary>
		/// 서버와 접속이 종료되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler Disconnected;
		/// <summary>
		/// Header를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler HeaderSended;
		/// <summary>
		/// 데이터를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler Sending;
		/// <summary>
		/// 데이터 전송이 완료 되면 발생합니다.
		/// </summary>
		public event TransferEventHandler Completed;
		/// <summary>
		/// Error가 발생할 경우 발생합니다.
		/// </summary>
		public event ErrorEventHandler Error;
		#endregion

		#region EventHandler
		/// <summary>
		/// Connected 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnConnected(ServerInfoEventArgs e)
		{
			if (Connected != null)
				Connected(this, e);
		}

		/// <summary>
		/// Disconnected 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnDisconnected(ServerInfoEventArgs e)
		{
			if (Disconnected != null)
				Disconnected(this, e);
		}

		/// <summary>
		/// HeaderSended 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnHeaderSended(TransferInfoEventArgs e)
		{
			if (HeaderSended != null)
				HeaderSended(this, e);
		}

		/// <summary>
		/// Sending 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnSending(TransferInfoEventArgs e)
		{
			if (Sending != null)
				Sending(this, e);
		}

		/// <summary>
		/// Completed 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnCompleted(TransferInfoEventArgs e)
		{
			if (Completed != null)
				Completed(this, e);
		}

		/// <summary>
		/// Error 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnError(iDASiT.FX.Win.Messenger.ErrorEventArgs e)
		{
			if (Error != null)
				Error(this, e);
		}
		#endregion

		#region Constructors
		/// <summary>
		/// FileSender class의 새 인스턴스를 초기화 합니다.
		/// <remarks>LocalHost로 Service를 연결합니다.</remarks>
		/// </summary>
		public FileSender()
		{
			if (_targetIpAddress == null)
			{
				IPAddress[] ipAddress = Dns.GetHostAddresses(Dns.GetHostName());

				if (ipAddress.Length > 0)
					_targetIpAddress = ipAddress[0].ToString();
			}

			Port = DEFAULT_PORT;
		}

		/// <summary>
		/// FileSender class의 새 인스턴스를 초기화 합니다.
		/// <remarks>기본 Port를 19090번을 사용합니다.</remarks>
		/// </summary>
		/// <param name="targetIpAddress">File을 전송할 Receiver의 IpAddress입니다.</param>
		public FileSender(string targetIpAddress)
			: this(targetIpAddress, DEFAULT_PORT)
		{
		}

		/// <summary>
		/// FileSender class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="targetIpAddress">File을 전송할 Receiver의 IpAddress입니다.</param>
		/// <param name="port">File을 전송할 Receiver의 의 Service Port입니다.</param>
		public FileSender(string targetIpAddress, int port)
		{
			TargetIpAddress = targetIpAddress;
			Port = port;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Target IpAddress를 구하거나 설정합니다.
		/// </summary>
		public string TargetIpAddress
		{
			get
			{
				return _targetIpAddress;
			}
			set
			{
				_targetIpAddress = value;
			}
		}

		/// <summary>
		/// Port를 구하거나 설정합니다.
		/// </summary>
		public int Port
		{
			get
			{
				return _port;
			}
			set
			{
				_port = value;
			}
		}

		/// <summary>
		/// BufferSize를 구하거나 설정합니다.
		/// <remarks>기본 크기는 1024 * 512 Byte 입니다.</remarks>
		/// </summary>
		public int BufferSize
		{
			get
			{
				if (_bufferSize == 0)
					_bufferSize = SEGMENT_SIZE * 512;

				return _bufferSize;
			}
			set
			{
				_bufferSize = value;
			}
		}
		#endregion

		#region SendFile
		/// <summary>
		/// 지정된 Receiver에게 파일을 전송합니다.
		/// </summary>
		/// <param name="filePath">전송할 파일의 full path를 지정합니다.</param>
		public void SendFile(string filePath)
		{
			SendFile(new string[] { filePath });
		}

		/// <summary>
		/// 지정된 Receiver에게 다수의 파일을 전송합니다.
		/// </summary>
		/// <param name="filePaths">전송할 파일의 full path를 지정합니다.</param>
		public void SendFile(string[] filePaths)
		{
			foreach (string examFile in filePaths)
			{
				if (File.Exists(examFile) == false)
					throw new InvalidOperationException(string.Format("{0} 파일이 없습니다.", filePaths));
			}

			Thread[] threads = new Thread[filePaths.Length];

			for (int i = 0; i < threads.Length; i++)
				threads[i] = new Thread(new ParameterizedThreadStart(InternalSendFile));

			for (int i = 0; i < threads.Length; i++)
				threads[i].Start(filePaths[i]);
		}

		private void InternalSendFile(object sendedFileName)
		{
			string fileName = sendedFileName as string;

			if (fileName == null)
				return;

			fileName = fileName.Replace(@"\", "/");

			Socket clientSocket = null;
			FileStream fs = null;

			try
			{
				IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(TargetIpAddress), Port);
				byte[] fileNameByte = Encoding.Unicode.GetBytes(Path.GetFileName(fileName));
				FileInfo fileInfo = new FileInfo(fileName);
				long fileSize = fileInfo.Length;
				byte[] fileSizeLengthByte = BitConverter.GetBytes(fileSize);
				byte[] fileNameLengthByte = BitConverter.GetBytes(fileNameByte.Length);
				byte[] header = new byte[fileNameLengthByte.Length + fileSizeLengthByte.Length + fileNameByte.Length];

				// Header 구성
				fileSizeLengthByte.CopyTo(header, 0);											// 첫번째 8byte 데이터 길이
				fileNameLengthByte.CopyTo(header, fileSizeLengthByte.Length);					// 두번째 4byte File 이름 길이
				fileNameByte.CopyTo(header, fileSizeLengthByte.Length + fileNameLengthByte.Length);	// 세번째 nbyte File 이름

				if (clientSocket == null || clientSocket.Connected == false)
					clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

				if (clientSocket.Connected == false)
				{
					clientSocket.Connect(endPoint);

					OnConnected(new ServerInfoEventArgs(TargetIpAddress, Port));
				}

				// 1차 File 길이, 파일 이름 길이, 파일 이름을 전송한다.
				int result = clientSocket.Send(header);

				if (result > 0)
					OnHeaderSended(new TransferInfoEventArgs(fileName, header.Length, header.Length, TargetIpAddress, Port));

				// 2차 파일 데이터를 bufferSize 만큼 전송 한다.
				long totalSendingLength = 0L;
				long totalReadingLength = 0L;
				int readingLength = 0;
				int sendingLength = 0;
				byte[] sendingData = new byte[BufferSize];

				// 전체 크기 보다 bufferSize가 크면 기본 bufferSize를 줄인다.
				if (fileSize < BufferSize)
					BufferSize = (int)fileSize;

				fs = File.OpenRead(fileName);

				// BufferSize는 전역으로 사용되기 때문에 값이 변경되거나 변경되지 않아서 Exception이 발생한다.
				int bufferSize = BufferSize;

				// 데이터를 읽어들인다.
				while ((readingLength = fs.Read(sendingData, 0, bufferSize)) > 0)
				{
					sendingLength = clientSocket.Send(sendingData, bufferSize, SocketFlags.Partial);
					totalSendingLength += sendingLength;
					totalReadingLength += readingLength;

					OnSending(new TransferInfoEventArgs(fileName, totalSendingLength, fileSize, TargetIpAddress, Port));

					// 기본 bufferSize 보다 남은 크기가 크면 기본 bufferSize는 (전체 크기 - 읽은 총합) 이다.
					if (BufferSize > (int)(fileSize - totalReadingLength))
						bufferSize = (int)(fileSize - totalReadingLength);
				}

				OnCompleted(new TransferInfoEventArgs(fileName, totalSendingLength, fileSize, TargetIpAddress, Port));
			}
			catch (SocketException ex)
			{
				OnError(new iDASiT.FX.Win.Messenger.ErrorEventArgs(string.Format("({0}){1}", ex.ErrorCode, ex.Message), ex));
			}
			catch (IOException ex)
			{
				OnError(new iDASiT.FX.Win.Messenger.ErrorEventArgs(ex.Message, ex));
			}
			catch (Exception ex)
			{
				OnError(new iDASiT.FX.Win.Messenger.ErrorEventArgs(ex.Message, ex));
			}
			finally
			{
				if (fs != null)
					fs.Close();

				if (clientSocket != null)
				{
					if (clientSocket.Connected == true)
					{
						clientSocket.Close();
						OnDisconnected(new ServerInfoEventArgs(TargetIpAddress, Port));
					}
				}
			}
		}
		#endregion
	}
}
