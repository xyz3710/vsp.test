﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 2일 수요일 오후 9:30
/*	Purpose		:	Messenger의 Remote Event Delegator 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;

namespace iDASiT.FX.Win.Messenger
{
	#region Login/Logout
	/// <summary>
	/// Message를 전송하기 위한 이벤트 처리기 대리자 입니다.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void LogEventHandler(object sender, LoggingEventArgs e);
	/// <summary>
	/// Message를 전송하기 위한 이벤트 처리기 대리자 입니다.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void SendMessageEventHandler(object sender, MessageEventArgs e);
	#endregion
	
	#region PutFiles(FileSender)/GetFiles(FileReceiver)
	/// <summary>
	/// 서비스가 시작/종료되는 이벤트를 처리할 메서드를 나타냅니다. 
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void ServiceEventHandler(object sender, ServerInfoEventArgs e);
	/// <summary>
	/// Client가 접속했을 경우 이벤트를 처리할 메서드를 나타냅니다. 
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void AcceptClientEventHandler(object sender, SocketAsyncEventArgs e);
	/// <summary>
	/// packet이 전송되면 이벤트를 처리할 메서드를 나타냅니다. 
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void TransferEventHandler(object sender, TransferInfoEventArgs e);
	/// <summary>
	/// packet이 전송되면 이벤트를 처리할 메서드를 나타냅니다. 
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void ErrorEventHandler(object sender, ErrorEventArgs e);
	#endregion
}
