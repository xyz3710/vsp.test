﻿//#define CONSOLE
/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.XMessage
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 2일 수요일 오후 6:05
/*	Purpose		:	iDASiT Windows Framework Messenger의 Server와 Agent의 공통 Object입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Net.Sockets;
using System.IO;
using System.Net;
using System.Threading;
using System.ComponentModel;

namespace iDASiT.FX.Win.Messenger
{
    /// <summary>
	/// iDASiT Windows Framework Messenger의 Server와 Agent의 공통 Object입니다.
	/// </summary>
	public class XMessage : MarshalByRefObject
	{
		#region Constants
		/// <summary>
		/// PutFile에 사용되는 Port입니다.
		/// </summary>
		public const int PutFilePort = 19091;
		/// <summary>
		/// GetFile에 사용되는 Port입니다.
		/// </summary>
		public const int GetFilePort = 19090;
		#endregion

		#region Fields
        private AgentMode _agentMode;
		private string _clientIpAddress;
		private Dictionary<string, string> _agents;		// ClientIpAddress, UserId로 구성된다.(Login을 해야 생성된다.)
		#endregion
		
		#region Constructors
		/// <summary>
		/// XMessage class의 새 인스턴스를 <seealso cref="iDASiT.FX.Win.Messenger.AgentMode.Client"/>로 초기화 합니다.
		/// <remarks>Client에 Message BroadCasting등 제어를 하려면 AgentMode를 <seealso cref="iDASiT.FX.Win.Messenger.AgentMode.Controller"/>로 설정해야 합니다.<br/>
		/// 각 Method에서 FromIpAddress를 할당하지 않으려면 ClientIpAddress를 설정해야 합니다.</remarks> 
		/// </summary>
		public XMessage()
		{
			_agentMode = AgentMode.Client;
			_agents = new Dictionary<string, string>();
			DebugLog("생성자 호출 : {0}", this.GetHashCode());
		}
		#endregion

		#region Events
		/// <summary>
		/// Login 하면 발생하는 이벤트 입니다.
		/// </summary>
		public event LogEventHandler LoggedIn;
		/// <summary>
		/// Logout 하면 발생하는 이벤트 입니다.
		/// </summary>
		public event LogEventHandler LoggedOut;		
		/// <summary>
		/// Message를 전송하면 발생하는 이벤트 입니다.
		/// <remarks><seealso cref="iDASiT.FX.Win.Messenger.CallbackObject"/>에서 상속받은</remarks>
		/// </summary>
		public event SendMessageEventHandler SendedMessage;

		/// <summary>
		/// 서버와 접속이 되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler PutFileConnected;
		/// <summary>
		/// 서버와 접속이 종료되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler PutFileDisconnected;
		/// <summary>
		/// Header를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileHeaderSended;
		/// <summary>
		/// 데이터를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileSending;
		/// <summary>
		/// 데이터 전송이 완료 되면 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileCompleted;
		/// <summary>
		/// Error가 발생할 경우 발생합니다.
		/// </summary>
		public event ErrorEventHandler PutFileError;
		#endregion

		#region Properties
		///
		/// AgentMode를 구하거나 설정합니다.
		/// </summary>
		public AgentMode AgentMode
		{
			get
			{
				return _agentMode;
			}
			set
			{
				_agentMode = value;
			}
		}

		/// <summary>
		/// ClientIpAddress를 구하거나 설정합니다.
		/// <remarks>FromIpAddress 속성으로 사용됩니다.</remarks>
		/// </summary>
		public string ClientIpAddress
		{
			get
			{
				if (string.IsNullOrEmpty(_clientIpAddress) == true)
					throw new InvalidOperationException("ClientIpAddress를 설정해야 합니다.");

				return _clientIpAddress;
			}
			set
			{
				_clientIpAddress = value;
			}
		}

		/// <summary>
		/// 현재 접속한 Agent 목록을 구하거나 설정합니다.
		/// </summary>
		public Dictionary<string, string> Agents
		{
			get
			{
				return _agents;
			}
			set
			{
				_agents = value;
			}
		}
		#endregion

		#region InitializeLifetimeService
		/// <summary>
		/// Service life time은 null을 return 합니다.
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}
		#endregion
		        
		#region Messagees
		/// <summary>
		/// 선택된 client로 Message를 전파 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="message">전파할 message를 입력합니다.</param>
		/// <returns>전파한 Client의 개수를 return 합니다.</returns>
		public int BroadCastMessage(string message)
		{
			if (AgentMode == AgentMode.Client)
				throw new InvalidOperationException("AgentMode를 Controller로 설정해야 사용할 수 있습니다.");

			return SendMessage(message, null);
		}

		/// <summary>
		/// 선택된 client로 Message를 전송 합니다.
		/// </summary>
		/// <param name="message">전파할 message를 입력합니다.</param>
		/// <param name="toIpAddresses">Message를 전송할 client IpAddress 입니다.</param>
		/// <returns>전송한 Client의 개수를 return 합니다.</returns>
		public int SendMessage(string message, params string[] toIpAddresses)
		{
			#region For Test
			/*
			int result = 0;

			if (SendedMessage != null)
			{
				DebugLog("SendMessage from {0} : {1}", fromIpAddress, message);

				if (AgentMode == AgentMode.Client && toIpAddresses == null)
					throw new InvalidOperationException("AgentMode가 Client로 설정되어 있으면 toIpAddresses에 null을 할당할 수 없습니다.");

				ClientContext clientContext = CallContext.GetData(ClientContext.ContextName) as ClientContext;
				Delegate[] sendingMessageGetInvocationList = SendedMessage.GetInvocationList();

DebugLog("GetList");

				foreach (Delegate sendMessageDelegate in sendingMessageGetInvocationList)
				{
					CallbackObject obj = sendMessageDelegate.Target as CallbackObject;
DebugLog("CallbackObject 생성");
					try
					{
						DebugLog("clientContext.ClientIpAddress : {0}", clientContext.ClientIpAddress);
						DebugLog("obj.ClientIpAddress : {0}", obj.ClientIpAddress);
						// 자기 자신의 EventHandler를 호출할 필요는 없다.
						#region For Test
						/*
						if (obj != null && obj.ClientIpAddress == clientContext.ClientIpAddress)
						{
DebugLog("CallbackObject 호출 후");
							continue;
						}
DebugLog("GotClieIpAddress");
					}
					catch (SocketException sEx)
					{
						switch (sEx.ErrorCode)
						{
							case 10054:		// 대상 컴퓨터에서 연결을 거부했으므로 연결하지 못했습니다 .
							case 10061:		// 현재 연결은 원격 호스트에 의해 강제로 끊겼습니다.
								// 해당 Client가 정상 종료를 하지 않거나 MulticastDelegate에서 제거 하지 않고 종료한 경우이다.
								SendedMessage = MulticastDelegate.Remove(SendedMessage, sendMessageDelegate) as SendMessageEventHandler;

								break;
						}

						continue;
					}

					if (toIpAddresses == null)
					{
DebugLog("BroadCastMessage");
						// BroadCating일 경우
						SendMessageEventHandler handler = sendMessageDelegate as SendMessageEventHandler;

						if (handler != null)
							// Client가 강제로 종료되었을 경우 SendingMessage에서 제거 하지 않아서 
							// 나머지 client들이 message를 받지 못하기 때문에 각각 Invoke 시킨다.
							handler.BeginInvoke(this, new MessageEventArgs(SendType.BroadCast, message, fromIpAddress, string.Empty), null, null);
					}
					else
					{
DebugLog("SendTo");
						// 특정 Client에 전송할 경우
						foreach (string toIpAddress in toIpAddresses)
						{
							// 대상 client에 SendMessage 이벤트를 실행한다.
							//if (obj != null && obj.ClientIpAddress == toIpAddress)
								sendMessageDelegate.DynamicInvoke(this, new MessageEventArgs(SendType.SendTo, message, fromIpAddress, toIpAddress));
						}
					}

					result++;
				}
			}

			return result;
			*/
			#endregion
			return InternalSendMessage(EventSource.Message, message, toIpAddresses);
		}

        /// <summary>
		/// 선택된 client로 Message를 전송 합니다.
		/// </summary>
		/// <param name="eventSource"></param>
		/// <param name="message">전파할 message를 입력합니다.</param>
		/// <param name="toIpAddresses">Message를 전송할 client IpAddress 입니다.</param>
		/// <returns>전송한 Client의 개수를 return 합니다.</returns>
		private int InternalSendMessage(EventSource eventSource, string message, params string[] toIpAddresses)
		{
			int result = 0;
			SendType sendType = toIpAddresses == null || toIpAddresses.Length == 0 ? SendType.BroadCast : SendType.SendTo;
			Delegate eventHandler = null;

			switch (eventSource)
			{
				case EventSource.Message:
				case EventSource.PutFile:
				case EventSource.GetFileReceiveRequest:
				case EventSource.GetFileSendRequest:
				case EventSource.GetFileList:
					eventHandler = SendedMessage;

					break;
				case EventSource.Login:
					eventHandler = LoggedIn;

					break;
				case EventSource.Logout:
					eventHandler = LoggedOut;

					break;
			}

			if (eventHandler != null)
			{
				if (eventSource == EventSource.Message 
						&& AgentMode == AgentMode.Client && toIpAddresses == null)
					throw new InvalidOperationException("AgentMode가 Client로 설정되어 있으면 toIpAddresses에 null을 할당할 수 없습니다.");

				ClientContext clientContext = CallContext.GetData(ClientContext.ContextName) as ClientContext;

				if (clientContext == null)
					throw new InvalidOperationException("CallbackObject를 구현한 class에서 ClientContext를 구할 수 없습니다.");

				string fromIpAddress = clientContext.ClientIpAddress;
				Delegate[] sendingMessageGetInvocationList = eventHandler.GetInvocationList();
				Dictionary<string, string> successAgnets = new Dictionary<string, string>();

				string toIpAddressString = string.Empty;

				if (toIpAddresses != null)
				{
					StringBuilder toIps = new StringBuilder();

					foreach (string ipAddress in toIpAddresses)
						toIps.AppendFormat("{0};", ipAddress);

					toIpAddressString = toIps.ToString(0, toIps.Length - 1);
				}

				if (eventSource != EventSource.GetFileReceiveRequest || eventSource != EventSource.PutFile)
                	WriteLog(eventSource.ToString(), 
						sendType, 
						fromIpAddress, 
						toIpAddressString,
						message);

				foreach (Delegate xMessageDelegate in sendingMessageGetInvocationList)
				{
					CallbackObject obj = xMessageDelegate.Target as CallbackObject;
					string eventHandlerClientIpAddress = string.Empty;
					string userId = string.Empty;

					try
					{
						// 자기 자신의 EventHandler를 호출할 필요는 없다.
						// FileReceiver는 자기 자신이 Receiver를 열어야 하므로 호출할 수 있다.
						if (obj != null)
						{
							eventHandlerClientIpAddress = obj.ClientIpAddress;
							userId = obj.UserId;

							if (eventSource == EventSource.Login 
								&& fromIpAddress == eventHandlerClientIpAddress 
								&& Agents.ContainsKey(eventHandlerClientIpAddress) == false)
								Agents.Add(fromIpAddress, userId);

							if (eventSource == EventSource.Logout
								&& fromIpAddress == eventHandlerClientIpAddress 
								&& Agents.ContainsKey(eventHandlerClientIpAddress) == true)
								Agents.Remove(fromIpAddress);

							// Message를 성공적으로 보낸 Agent만 저장하여 비정상 Logout한 Agent를 제거한다.
							if (successAgnets.ContainsKey(eventHandlerClientIpAddress) == false)
								successAgnets.Add(eventHandlerClientIpAddress, userId);

							if (eventHandlerClientIpAddress == clientContext.ClientIpAddress
								&& eventSource != EventSource.GetFileReceiveRequest)
								continue;
						}
					}
					catch (SocketException sEx)
					{
						switch (sEx.ErrorCode)
						{
							case 10054:		// 대상 컴퓨터에서 연결을 거부했으므로 연결하지 못했습니다 .
							case 10061:		// 현재 연결은 원격 호스트에 의해 강제로 끊겼습니다.
								switch (eventSource)
								{
									case EventSource.Message:
									case EventSource.PutFile:
									case EventSource.GetFileReceiveRequest:
									case EventSource.GetFileSendRequest:
									case EventSource.GetFileList:
										SendedMessage = MulticastDelegate.Remove(SendedMessage, xMessageDelegate) as SendMessageEventHandler;

										break;
									case EventSource.Login:
										LoggedIn = MulticastDelegate.Remove(LoggedIn, xMessageDelegate) as LogEventHandler;

										break;
									case EventSource.Logout:
										LoggedOut = MulticastDelegate.Remove(LoggedOut, xMessageDelegate) as LogEventHandler;

										break;
								}

								break;
						}

						continue;
					}

					// 각 EventHandler에 따라서 Invoke를 시킨다.
					switch (eventSource)
					{
						case EventSource.Message:
						case EventSource.PutFile:
						case EventSource.GetFileReceiveRequest:
						case EventSource.GetFileSendRequest:
						case EventSource.GetFileList:
							SendMessageEventHandler mHandler = xMessageDelegate as SendMessageEventHandler;

							if (mHandler != null)
							{
								if (sendType == SendType.BroadCast)
									mHandler.BeginInvoke(this, new MessageEventArgs(eventSource, sendType, message, fromIpAddress, string.Empty), null, null);
								else if (sendType == SendType.SendTo)
									// 특정 Client에 전송할 경우
									foreach (string toIpAddress in toIpAddresses)
									{
										if (eventHandlerClientIpAddress == toIpAddress)
											// 대상 client에 SendMessage 이벤트를 실행한다.
											mHandler.BeginInvoke(this, new MessageEventArgs(eventSource, sendType, message, fromIpAddress, toIpAddress), null, null);
									}

								result++;
							}

							break;
						case EventSource.Login:
						case EventSource.Logout:
							LogEventHandler lHandler = xMessageDelegate as LogEventHandler;

							if (lHandler != null)
							{
								if (sendType == SendType.BroadCast)
									lHandler.BeginInvoke(this, new LoggingEventArgs(eventSource, sendType, message, fromIpAddress, string.Empty, null), null, null);
								else if (sendType == SendType.SendTo)
									// 특정 Client에 전송할 경우
									foreach (string toIpAddress in toIpAddresses)
									{
										if (eventHandlerClientIpAddress == toIpAddress)
											// 대상 client에 SendMessage 이벤트를 실행한다.
											lHandler.BeginInvoke(this, new LoggingEventArgs(eventSource, sendType, message, fromIpAddress, toIpAddress, null), null, null);
									}

								result++;
							}

							break;
					}
				}

				Dictionary<string, string> faileAgent = new Dictionary<string, string>();

				// 실패한 Agent는 제거한다.
				foreach (string key in Agents.Keys)
					if (successAgnets.ContainsKey(key) == false)
						faileAgent.Add(key, Agents[key]);

				foreach (string key in faileAgent.Keys)
				{
					// 해당 Client가 정상 종료를 하지 않거나 MulticastDelegate에서 제거 하지 않고 종료한 경우이다.
					// 강제로 Logout한 User를 알린다.
					LoggedOut(this, new LoggingEventArgs(EventSource.AbnormalLogout, SendType.BroadCast, faileAgent[key], key, null, null));
					WriteLog(EventSource.AbnormalLogout.ToString(), SendType.BroadCast, key, string.Empty, string.Format("{0} Logout {1}", faileAgent[key]));

					Agents.Remove(key);
				}

#if CONSOLE
				foreach (string key in Agents.Keys)
					DebugLog("agent : {0}", Agents[key]);
#endif
			}

			return result;
		}
		#endregion

		#region Login/Logout
		/// <summary>
		/// 선택된 서버로 Login 합니다.
		/// <remarks>(모든 client에 로그인 message를 BroadCasting 합니다.)</remarks>
		/// </summary>
		/// <param name="userId">Login한 UserId입니다.</param>
		public bool Login(string userId)
		{
			SendType sendType = SendType.BroadCast;
			string toIpAddress = null;
			object tag = null;

			return Login(sendType, userId, toIpAddress, tag);
		}

		/// <summary>
		/// 선택된 서버로 Login 합니다.
		/// <remarks>(특정 client에만 알립니다.)</remarks>
		/// </summary>
		/// <param name="userId">Login한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public bool Login(string userId, string toIpAddress)
		{
			SendType sendType = SendType.SendTo;
			object tag = null;
		
			return Login(sendType, userId, toIpAddress, tag);
		}

        /// <summary>
		/// 선택된 서버로 Login 합니다.
		/// </summary>
		/// <param name="sendType">Login을 전달할 message type입니다.</param>
		/// <param name="userId">Login한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="tag">Login시 전달할 Tag입니다.</param>
		public bool Login(SendType sendType, string userId, string toIpAddress, object tag)
		{
			int result = 0;

			if (sendType == SendType.BroadCast)
			{
				// Login의 BroadCastMessage는 AgentMode와 상관없이 이루어져야 한다.
				AgentMode agentMode = AgentMode;
				AgentMode = AgentMode.Controller;

				result = InternalSendMessage(EventSource.Login, userId, null);

				AgentMode = agentMode;
			}
			else if (sendType == SendType.SendTo)
				result = InternalSendMessage(EventSource.Login, userId, toIpAddress);

			return result > 0;
		}

		/// <summary>
		/// 선택된 서버에서 Logout 합니다.
		/// <remarks>(모든 client에 로그인 message를 BroadCasting 합니다.)</remarks>
		/// </summary>
		/// <param name="userId">Logout한 UserId입니다.</param>
		public bool Logout(string userId)
		{
			SendType sendType = SendType.BroadCast;
			string toIpAddress = null;
			object tag = null;

			return Logout(sendType, userId, toIpAddress, tag);
		}

        /// <summary>
		/// 선택된 서버에서 Logout 합니다.
		/// <remarks>(특정 client에만 알립니다.)</remarks>
		/// </summary>
		/// <param name="userId">Logout한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public bool Logout(string userId, string toIpAddress)
		{
			SendType sendType = SendType.SendTo;
			object tag = null;

			return Logout(sendType, userId, toIpAddress, tag);
		}

        /// <summary>
		/// 선택된 서버에서 Logout 합니다.
		/// </summary>
		/// <param name="sendType">Logout을 전달할 message type입니다.</param>
		/// <param name="userId">Logout한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="tag">Lotout시 전달할 Tag입니다.</param>
		public bool Logout(SendType sendType, string userId, string toIpAddress, object tag)
		{
			int result = 0;

			if (sendType == SendType.BroadCast)
			{
				// Login의 BroadCastMessage는 AgentMode와 상관없이 이루어져야 한다.
				AgentMode agentMode = AgentMode;
				AgentMode = AgentMode.Controller;

				result = InternalSendMessage(EventSource.Logout, userId, null);

				AgentMode = agentMode;
			}
			else if (sendType == SendType.SendTo)
				result = InternalSendMessage(EventSource.Logout, userId, toIpAddress);

			if (tag != null)
			{
				// TODO: Tag data를 전송한다.
				// by KIMKIWON\xyz37 in 2008년 7월 14일 월요일 오후 2:08
			}

			return result > 0;
		}
		#endregion

		#region Object
		/// <summary>
		/// Client에 요청 message 보냅니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="requestObject">요청할 객체를 선택합니다.</param>
		/// <returns>성공 여부를 return 합니다.</returns>
		public bool Request(object requestObject)
		{
			if (AgentMode == AgentMode.Client)
				throw new InvalidOperationException("생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.");

			bool result = false;
			

			return result;
		}
		#endregion

		#region Files
		private delegate void SendFileMessage(string filePath, string[] targetIpAddresses);

		#region SendFileMessageData class
		private class SendFileMessageData
		{
			private string _message;
			private string[] _fileList;
			private string[] _targetIpAddresses;

			/// <summary>
			/// SendFileMessageData class의 새 인스턴스를 초기화 합니다.
			/// </summary>
			/// <param name="message"></param>
			/// <param name="targetIpAddresses"></param>
			public SendFileMessageData(string message, string[] targetIpAddresses)
			{
				_message = message;
				_targetIpAddresses = targetIpAddresses;
			}

			/// <summary>
			/// SendPutMessageData class의 새 인스턴스를 초기화 합니다.
			/// </summary>
			/// <param name="localFileList"></param>
			/// <param name="targetIpAddresses"></param>
			public SendFileMessageData(string[] fileList, string[] targetIpAddresses)
			{
				_fileList = fileList;
				_targetIpAddresses = targetIpAddresses;
			}

			#region Properties
			/// <summary>
			/// Message를 구하거나 설정합니다.
			/// </summary>
			public string Message
			{
				get
				{
					return _message;
				}
				set
				{
					_message = value;
				}
			}

			/// <summary>
			/// LocalFileList를 구하거나 설정합니다.
			/// </summary>
			public string[] FileList
			{
				get
				{
					return _fileList;
				}
				set
				{
					_fileList = value;
				}
			}

			/// <summary>
			/// ToIpAddresses를 구하거나 설정합니다.
			/// </summary>
			public string[] TargetIpAddresses
			{
				get
				{
					return _targetIpAddresses;
				}
				set
				{
					_targetIpAddresses = value;
				}
			}
			#endregion
		}
		#endregion

		#region GetFiles
		/// <summary>
		/// Controller에서 File을 가져 옵니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 remote path 입니다.</param>
		/// <param name="filePattern">가져오려는 파일 pattern 입니다.</param>
		/// <param name="localPath">가져온 파일을 저장할 로컬 경로입니다.<br/>경로 아래 IpAddress 형태로 폴더가 생성됩니다.</param>
		/// <param name="fromIpAddresses">데이터를 가져올 Client 입니다.</param>
		public void GetFiles(string remotePath, string filePattern, string localPath, params string[] fromIpAddresses)
		{
			GetFiles(remotePath, new string[] { filePattern }, localPath, fromIpAddresses);			
		}

		/// <summary>
		/// Controller에서 File을 가져 옵니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 local file path 입니다.</param>
		/// <param name="filePatterns">가져오려는 파일 pattern 입니다.</param>
		/// <param name="localPath">가져온 파일을 저장할 로컬 경로입니다.<br/>경로 아래 IpAddress 형태로 폴더가 생성됩니다.</param>
		/// <param name="fromIpAddresses">데이터를 가져올 Client 입니다.</param>
		public void GetFiles(string remotePath, string[] filePatterns, string localPath, params string[] fromIpAddresses)
		{
			if (AgentMode == AgentMode.Client)
				throw new InvalidOperationException("생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.");

			// Client에 파일과 pattern을 보낸다.
			List<string> fileList = new List<string>();
			StringBuilder filePatternList = new StringBuilder();
			
			foreach (string pattern in filePatterns)
				fileList.AddRange(Directory.GetFiles(remotePath, pattern, SearchOption.TopDirectoryOnly));

			// 받는 쪽에서 ; 으로 paring 할 수 있도록 묶는다.
			filePatternList.AppendFormat("{0};", remotePath);

			foreach (string fileName in fileList)
				filePatternList.AppendFormat("{0};", Path.GetFileName(fileName));

			ClientContext clientContext = CallContext.GetData(ClientContext.ContextName) as ClientContext;

			// 호출한 client에서 FileReceiver의 실행 message를 전송한다.
			InternalSendMessage(EventSource.GetFileReceiveRequest, localPath, clientContext.ClientIpAddress);

			// fromIpAddresses에서 Sender의 실행 message를 전송한다.
			InternalSendMessage(EventSource.GetFileSendRequest, filePatternList.ToString(), fromIpAddresses);
			#region For Test(Server에서 Receiver 생성)
			/*
			SendFileMessage sendGetFileMessage = new SendFileMessage(SendGetFileMessageToTarget);
			AsyncCallback asyncCallback = new AsyncCallback(CallbackSendGetFileMessage);
			SendFileMessageData sendGetMessageData = new SendFileMessageData(filePatternList.ToString(), fromIpAddresses);

			sendGetFileMessage.BeginInvoke(localPath, fromIpAddresses, asyncCallback, sendGetMessageData);
			*/
			#endregion
		}

		#region For Test(Server에서 Receiver 생성)
		/*
		private void SendGetFileMessageToTarget(string filePath, string[] targetIpAddresses)
		{
			Thread[] threads = new Thread[targetIpAddresses.Length];
			ParameterizedThreadStart threadParameter = new ParameterizedThreadStart(InternalGetFiles);

			for (int i = 0; i < targetIpAddresses.Length; i++)
				threads[i] = new Thread(threadParameter);

			for (int i = 0; i < targetIpAddresses.Length; i++)
				threads[i].Start(string.Format(@"{0}\{1}", filePath, targetIpAddresses[i]));
		}

		private void InternalGetFiles(object receivedPath)
		{
			FileReceiver fileReceiver = new FileReceiver(GetFilePort);
			
			fileReceiver.ReceivedPath = receivedPath as string;
			fileReceiver.CloseAfterReceive = true;

			//fileReceiver.Started += new ServiceEventHandler(OnGetFilesStarted);
			//fileReceiver.Stopped += new ServiceEventHandler(OnGetFilesStopped);
			//fileReceiver.AcceptClient += new AcceptClientEventHandler(OnGetFilesAcceptClient);
			//fileReceiver.HeaderReceived += new TransferEventHandler(OnGetFilesHeaderReceived);
			//fileReceiver.Receiving += new TransferEventHandler(OnGetFilesReceiving);
			fileReceiver.Completed += new TransferEventHandler(OnGetFilesCompleted);
			fileReceiver.Error += new ErrorEventHandler(OnGetFilesError);

			fileReceiver.Start(false, 1);

			while (fileReceiver.Running == false)
				Thread.Sleep(500);
			
			DebugLog("Port : {0}", fileReceiver.Port);
			//DebugLog("{0} : {1}, {2}", e.Message, e.FromIpAddress, e.ToIpAddress);
		}

		private void CallbackSendGetFileMessage(IAsyncResult ar)
		{
			SendFileMessageData data = ar.AsyncState as SendFileMessageData;

			if (data != null)
				InternalSendMessage(EventSource.GetFile, data.Message, data.TargetIpAddresses);
		}
		*/
		#endregion

        /// <summary>
		/// 원격 Client의 File 목록을 구합니다.
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 remote path 입니다.</param>
		/// <param name="fromIpAddresses">구하고자 하는 Client IpAddress입니다.</param>
		/// <returns></returns>
		public List<string> GetRemoteFileList(string remotePath, params string[] fromIpAddresses)
		{
			// TODO: GetRemoteList
			// by KIMKIWON\xyz37 in 2008년 7월 17일 목요일 오후 6:52
			return null;
		}
		#endregion

		#region PutFiles
		/// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.<br></br>
		/// 지정된 Folder만 검색합니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePattern">전송하려는 파일 pattern 입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string filePattern, string remotePath, params string[] toIpAddresses)
		{
			PutFiles(localPath, new string[] { filePattern }, remotePath, toIpAddresses);
		}

        /// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.<br></br>
		/// 지정된 Folder만 검색합니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePatterns">전송하려는 파일 pattern 입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string[] filePatterns, string remotePath, params string[] toIpAddresses)
		{
			SearchOption searchOption = SearchOption.TopDirectoryOnly;

			PutFiles(localPath, filePatterns, searchOption, remotePath, toIpAddresses);
		}

        /// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePatterns">전송하려는 파일 pattern 입니다.</param>
		/// <param name="searchOption">로컬 파일을 검색하는 Option입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string[] filePatterns, SearchOption searchOption, string remotePath, params string[] toIpAddresses)
		{
			if (AgentMode == AgentMode.Client)
				throw new InvalidOperationException("생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.");

			//if (toIpAddresses.Length < 1)
			//	throw new InvalidOperationException("ToIpAddress를 설정하십시오.");

			#region Target client를 SendedMessage에서 찾는다.
			// Message를 수신할 수 있어야 파일도 수신 할 수 있다.
			// BroadCast File일 경우 message를 보낼 수 있는 Client list를 구한다.
			List<string> clientList = new List<string>();
			ClientContext clientContext = CallContext.GetData(ClientContext.ContextName) as ClientContext;

			if (clientContext == null)
				throw new InvalidOperationException("CallbackObject를 구현한 class에서 ClientContext를 구할 수 없습니다.");

			foreach (Delegate xMessageDelegate in SendedMessage.GetInvocationList())
			{
				CallbackObject obj = xMessageDelegate.Target as CallbackObject;
				string eventHandlerClientIpAddress = string.Empty;

				try
				{
					// 자기 자신의 EventHandler를 호출할 필요는 없다.
					if (obj != null)
					{
						eventHandlerClientIpAddress = obj.ClientIpAddress;

						if (eventHandlerClientIpAddress == clientContext.ClientIpAddress)
							continue;
						else
							clientList.Add(eventHandlerClientIpAddress);
					}
				}
				catch
				{
				}
			}

			List<string> targetClientList = new List<string>();

			// 실제 EventHandler에 등록된 client에만 File을 전송하도록 목록을 다시 구한다.
			foreach (string targetIpAddress in toIpAddresses)
			{
				bool exists = clientList.Exists(delegate(string checkIpAddress)
				{
					if (targetIpAddress == checkIpAddress)
						return true;

					return false;
				});

				if (exists == true)
					targetClientList.Add(targetIpAddress);
			}

			if (targetClientList.Count > 0)
				toIpAddresses = targetClientList.ToArray();
			else
				return;
			#endregion

			List<string> fileList = new List<string>();

			foreach (string searchPattern in filePatterns)
				fileList.AddRange(Directory.GetFiles(localPath, searchPattern, searchOption));

			if (toIpAddresses.Length == 0 || fileList.Count == 0)
				return;

			SendFileMessage sendPutFileMessage = new SendFileMessage(SendPutFileMessageToTarget);
			AsyncCallback asyncCallback = new AsyncCallback(CallbackSendPutFileMessage);
			SendFileMessageData sendPutMessageData = new SendFileMessageData(fileList.ToArray(), toIpAddresses);

			sendPutFileMessage.BeginInvoke(remotePath, toIpAddresses, asyncCallback, sendPutMessageData);
		}

		private void SendPutFileMessageToTarget(string remotePath, string[] toIpAddresses)
		{
			InternalSendMessage(EventSource.PutFile, remotePath, toIpAddresses);
		}

		private void CallbackSendPutFileMessage(IAsyncResult ar)
		{
			SendFileMessageData data = ar.AsyncState as SendFileMessageData;

			if (data != null)
			{
				Thread[] threads = new Thread[data.TargetIpAddresses.Length];
				ParameterizedThreadStart threadParameter = new ParameterizedThreadStart(InternalPutFiles);

				for (int i = 0; i < data.TargetIpAddresses.Length; i++)
					threads[i] = new Thread(threadParameter);

				for (int i = 0; i < data.TargetIpAddresses.Length; i++)
				{
					PutFilesParameter param = new PutFilesParameter(data.TargetIpAddresses[i], data.FileList);

					threads[i].Start(param);
				}
			}
		}

		private void InternalPutFiles(object putFilesParameter)
		{
			PutFilesParameter param = putFilesParameter as PutFilesParameter;

			if (param != null)
			{
				FileSender fileSender = new FileSender(param.ToIpAddress, PutFilePort);

				fileSender.Connected += new ServiceEventHandler(OnPutFilesConnected);
				fileSender.Disconnected += new ServiceEventHandler(OnPutFilesDisconnected);
				fileSender.HeaderSended += new TransferEventHandler(OnPutFilesHeaderSended);
				fileSender.Sending += new TransferEventHandler(OnPutFilesSending);
				fileSender.Completed += new TransferEventHandler(OnPutFilesCompleted);
				fileSender.Error += new ErrorEventHandler(OnPutFilesError);

				fileSender.SendFile(param.FilePaths);
			}
		}

		/// <summary>
		/// PutFiles의 PutFileConnected 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesConnected(object sender, ServerInfoEventArgs e)
		{
			if (PutFileConnected != null)
			{
				try
				{
					PutFileConnected(sender, e);
				}
				catch
				{
				}
				finally
				{
					// 해당 파일이 완료되면 이벤트를 더이상 수신하지 않는다.
					PutFileConnected = MulticastDelegate.RemoveAll(PutFileConnected, PutFileConnected) as ServiceEventHandler;
				}
			}

			DebugLog("{0}:{1} Connected.", e.ServerIpAddress, e.Port, e.ListenerCount);
		}

		/// <summary>
		/// PutFiles의 PutFileDisconnected 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesDisconnected(object sender, ServerInfoEventArgs e)
		{
			if (PutFileDisconnected != null)
			{
				try
				{
					PutFileDisconnected(sender, e);
				}
				catch
				{
				}
				finally
				{
					// 해당 파일이 완료되면 이벤트를 더이상 수신하지 않는다.
					PutFileDisconnected = MulticastDelegate.RemoveAll(PutFileDisconnected, PutFileDisconnected) as ServiceEventHandler;
				}
			}

			DebugLog("{0}:{1} Disconnected.", e.ServerIpAddress, e.Port, e.ListenerCount);
		}

		/// <summary>
		/// PutFiles의 PutFileHeaderSended 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesHeaderSended(object sender, TransferInfoEventArgs e)
		{
			if (PutFileHeaderSended != null)
			{
				try
				{
					PutFileHeaderSended(sender, e);
				}
				catch
				{
				}
				finally
				{
					PutFileHeaderSended = MulticastDelegate.RemoveAll(PutFileHeaderSended, PutFileHeaderSended) as TransferEventHandler;
				}
			}

			DebugLog("Header Sended\t{0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		/// <summary>
		/// PutFiles의 PutFileSending 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesSending(object sender, TransferInfoEventArgs e)
		{
			if (PutFileSending != null)
			{
				try
				{
					PutFileSending(sender, e);
				}
				catch 
				{
				}
				finally
				{
					PutFileSending = MulticastDelegate.RemoveAll(PutFileSending, PutFileSending) as TransferEventHandler;
				}
			}

			//Console.Write("\rSending\t\t{0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		/// <summary>
		/// PutFiles의 PutFileCompleted 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesCompleted(object sender, TransferInfoEventArgs e)
		{
			if (PutFileCompleted != null)
			{
				// BroadCast PutFiles 일 경우 Socket exception이 발생할 수 있어서 구현
				Delegate[] putFileCompletedGetInvocationList = PutFileCompleted.GetInvocationList();

				foreach (Delegate eventDelegate in putFileCompletedGetInvocationList)
				{
					try
					{
						CallbackObject obj = eventDelegate.Target as CallbackObject;
						string eventHandlerClientIpAddress = string.Empty;

						if (obj != null)
							eventHandlerClientIpAddress = obj.ClientIpAddress as string;

						ClientContext clientContext = CallContext.GetData(ClientContext.ContextName) as ClientContext;

						if (eventHandlerClientIpAddress == clientContext.ClientIpAddress)
						{
							eventDelegate.DynamicInvoke(sender, e);

							WriteLog("PutFileCompleted", SendType.SendTo, eventHandlerClientIpAddress, e.ServerIpAddress, e.FileName, e.CurrentSize);
						}
					}
					catch
					{
						// 해당 이벤트가 불완전 제거 되면 강제로 삭제 한다.
						PutFileCompleted = MulticastDelegate.Remove(PutFileCompleted, eventDelegate) as TransferEventHandler;

						continue;
					}
				}
			}

			DebugLog("Completed\t{0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		/// <summary>
		/// PutFiles의 PutFileError 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFilesError(object sender, ErrorEventArgs e)
		{
			if (PutFileError != null)
			{
				try
				{
					PutFileError(sender, e);
				}
				catch
				{
				}
				finally
				{
					// 해당 파일이 완료되면 이벤트를 더이상 수신하지 않는다.
					PutFileError = MulticastDelegate.RemoveAll(PutFileError, PutFileError) as ErrorEventHandler;
				}				
			}

			DebugLog(e.Message);
		}

        #region PutFilesParameter class
		private class PutFilesParameter
		{
			#region Fields
			private string _toIpAddress;
			private string[] _filePath;
			#endregion

			#region Constructors
			/// <summary>
			/// PutFilesParameter class의 새 인스턴스를 초기화 합니다.
			/// </summary>
			/// <param name="toIpAddress"></param>
			/// <param name="filePath"></param>
			public PutFilesParameter(string toIpAddress, params string[] filePath)
			{
				_toIpAddress = toIpAddress;
				_filePath = filePath;
			}
			#endregion

			#region Properties
			/// <summary>
			/// ToIpAddress를 구하거나 설정합니다.
			/// </summary>
			public string ToIpAddress
			{
				get
				{
					return _toIpAddress;
				}
				set
				{
					_toIpAddress = value;
				}
			}

			/// <summary>
			/// FilePath를 구하거나 설정합니다.
			/// </summary>
			public string[] FilePaths
			{
				get
				{
					return _filePath;
				}
				set
				{
					_filePath = value;
				}
			}
			#endregion
		}
		#endregion
		#endregion
		#endregion

		#region Execute Command
		/// <summary>
		/// Client 명령을 실행 합니다.
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		public bool ExecuteCommand(ICommand command)
		{
			return command.Execute();
		}
		#endregion

		private void DebugLog(string format, params object[] args)
		{
#if CONSOLE && DEBUG
			Console.WriteLine(format, args);
#endif
		}

		public void WriteLog(string eventSource, SendType sendType, string fromIpAddress, string toIpAddress, string message, params object[] otherInfo)
		{
			string format = "{0}\t{1}\t{2}\t{3}\t{4}";

			for (int i = 0; i < otherInfo.Length; i++)
				format = string.Format("{0}\t{1}", format, otherInfo[i].ToString());
			
			Console.WriteLine(format, 
					eventSource,
					sendType,
					fromIpAddress,
					toIpAddress,
					message);
		}
	}
}
