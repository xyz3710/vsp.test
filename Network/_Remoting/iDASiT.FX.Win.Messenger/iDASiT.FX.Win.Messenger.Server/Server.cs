﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Windows.Forms;
using System.IO;

namespace iDASiT.FX.Win.Messenger.Server
{
	class Server
	{
		static void Main(string[] args)
		{
			string configFilename = string.Format("{0}.config", Path.GetFileName(Application.ExecutablePath));
			RemotingConfiguration.Configure(configFilename, false);

			Console.WriteLine("Hit enter to exit");
			Console.ReadLine();
		}
	}
}
