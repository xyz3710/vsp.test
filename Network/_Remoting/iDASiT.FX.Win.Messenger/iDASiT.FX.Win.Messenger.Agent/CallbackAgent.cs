﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Messenger.Agent.CallbackAgent
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 7월 4일 금요일 오후 2:35
/*	Purpose		:	Agent가 Server에 Message 전송 후 응답할 Method를 정의하는 객체 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.IO;
using System.Windows.Forms;

namespace iDASiT.FX.Win.Messenger.Agent
{
	/// <summary>
	/// Agent가 Server에 Message 전송 후 응답할 Method를 정의하는 객체 입니다.
	/// </summary>
	public class CallbackAgent : CallbackObject, IDisposable
	{
		#region Fields
        private string _clientIpAddress;
		private string _userId;
		private XMessage _xMessage;
		private AgentMode _agentMode;
		private ClientContext _clientContext;
		#endregion

		#region Constructors
		/// <summary>
		/// CallbackClient class의 새 인스턴스를 초기화 합니다.(첫번째 bind된 Client IpAddress를 입력합니다.)
		/// </summary>
		public CallbackAgent()
			: this(Dns.GetHostAddresses(Dns.GetHostName())[0].ToString(), AgentMode.Client)
		{
		}

		/// <summary>
		/// CallbackClient class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="agentMode"></param>
		public CallbackAgent(AgentMode agentMode)
			: this(Dns.GetHostAddresses(Dns.GetHostName())[0].ToString(), agentMode)
		{
		}

        /// <summary>
		/// CallbackClient class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="clientIpAddress"></param>
		/// <param name="agentMode"></param>
		public CallbackAgent(string clientIpAddress, AgentMode agentMode)
		{
			_clientIpAddress = clientIpAddress;
			_clientContext = new ClientContext(clientIpAddress);

			CallContext.SetData(ClientContext.ContextName, _clientContext);

			string configFilename = string.Format("{0}.config", Path.GetFileName(Application.ExecutablePath));
			RemotingConfiguration.Configure(configFilename, false);

			AgentMode = agentMode;
			_xMessage = new XMessage();
			
			if (_xMessage == null)
				throw new InvalidOperationException("Server에 연결할 수 없습니다.");

			_xMessage.AgentMode = agentMode;

			_xMessage.LoggedIn += new LogEventHandler(OnLogin);
			_xMessage.SendedMessage += new SendMessageEventHandler(OnSendMessage);
			_xMessage.LoggedOut += new LogEventHandler(OnLogout);

			_xMessage.PutFileConnected += new ServiceEventHandler(OnPutFileConnected);
			_xMessage.PutFileDisconnected += new ServiceEventHandler(OnPutFileDisconnected);
			_xMessage.PutFileHeaderSended += new TransferEventHandler(OnPutFileHeaderSended);
			_xMessage.PutFileSending += new TransferEventHandler(OnPutFileSending);			
			_xMessage.PutFileCompleted += new TransferEventHandler(OnPutFileCompleted);
			_xMessage.PutFileError += new ErrorEventHandler(OnPutFileError);
		}
		#endregion

		#region Distructor
		/// <summary>
		/// 등록한 Event를 모두 제거한다.
		/// </summary>
		~CallbackAgent()
		{
			Dispose();
		}
		#endregion

		#region IDisposable 멤버

		/// <summary>
		/// CallbackClient에서 사용하는 모든 리소스를 해제합니다.
		/// </summary>
		public void Dispose()
		{
			_xMessage.LoggedIn -= new LogEventHandler(OnLogin);
			_xMessage.SendedMessage -= new SendMessageEventHandler(OnSendMessage);
			_xMessage.LoggedOut -= new LogEventHandler(OnLogout);

			_xMessage.PutFileConnected -= new ServiceEventHandler(OnPutFileConnected);
			_xMessage.PutFileDisconnected -= new ServiceEventHandler(OnPutFileDisconnected);
			_xMessage.PutFileHeaderSended -= new TransferEventHandler(OnPutFileHeaderSended);
			_xMessage.PutFileSending -= new TransferEventHandler(OnPutFileSending);
			_xMessage.PutFileCompleted -= new TransferEventHandler(OnPutFileCompleted);
			_xMessage.PutFileError -= new ErrorEventHandler(OnPutFileError);
		}

		#endregion

		#region Message Events
		/// <summary>
		/// SendTo Message를 수신하면 발생하는 이벤트 입니다.
		/// </summary>
		public event SendMessageEventHandler SendedMessage;
		/// <summary>
		/// BroadCast Message를 수신하면 발생하는 이벤트 입니다.
		/// </summary>
		public event SendMessageEventHandler BroadCastedMessage;
		#endregion

		#region Login/Logout Events
		/// <summary>
		/// Login 하면 발생하는 이벤트 입니다.
		/// </summary>
		public event LogEventHandler LoggedIn;
		/// <summary>
		/// Logout 하면 발생하는 이벤트 입니다.
		/// </summary>
		public event LogEventHandler LoggedOut;
		#endregion

		#region GetFile Receiver Events
		/// <summary>
		/// 서비스가 시작되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler GetFileStarted;
		/// <summary>
		/// 서비스가 중지되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler GetFileStopped;
		/// <summary>
		/// Client가 접속했을 경우 발생합니다.
		/// </summary>
		public event AcceptClientEventHandler GetFileAcceptClient;
		/// <summary>
		/// Header를 전송 받았을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileHeaderReceived;
		/// <summary>
		/// 데이터를 전송 받을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileReceiving;
		/// <summary>
		/// 데이터 수신이 완료되면 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileCompleted;
		/// <summary>
		/// Error가 발생할 경우 발생합니다.
		/// </summary>
		public event ErrorEventHandler GetFileError;
		#endregion

		#region GetFile Sender Events
		/// <summary>
		/// 서버와 접속이 되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler GetFileSendConnected;
		/// <summary>
		/// 서버와 접속이 종료되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler GetFileSendDisconnected;
		/// <summary>
		/// Header를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileSendHeaderSended;
		/// <summary>
		/// 데이터를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileSendSending;
		/// <summary>
		/// 데이터 전송이 완료되면 발생합니다.
		/// </summary>
		public event TransferEventHandler GetFileSendCompleted;
		/// <summary>
		/// Error가 발생할 경우 발생합니다.
		/// </summary>
		public event ErrorEventHandler GetFileSendError;
		#endregion

		#region PutFile Events
		/// <summary>
		/// 서버와 접속이 되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler PutFileConnected;
		/// <summary>
		/// 서버와 접속이 종료되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler PutFileDisconnected;
		/// <summary>
		/// Header를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileHeaderSended;
		/// <summary>
		/// 데이터를 전송했을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileSending;
		/// <summary>
		/// 데이터 전송이 완료되면 발생합니다.
		/// </summary>
		public event TransferEventHandler PutFileCompleted;
		/// <summary>
		/// Error가 발생할 경우 발생합니다.
		/// </summary>
		public event ErrorEventHandler PutFileError;
		/// <summary>
		/// PutFile에 대한 데이터 수신이 완료되면 발생합니다.
		/// </summary>
		public event SendMessageEventHandler PutFileReceived;
		#endregion

		#region Properties
		/// <summary>
		/// Client의 IpAddress를 구하거나 설정합니다.
		/// <remarks>Server 객체에서 어떤 EventHandler에서 호출하였는지 구별하기 위해 override된 형태로 제공을 해야합니다.</remarks>
		/// </summary>
		public override string ClientIpAddress
		{
			get
			{
				return _clientIpAddress;
			}
		}

		/// <summary>
		/// Login한 UserId를 구하거나 설정합니다.
		/// </summary>
		public override string UserId
		{
			get
			{
				return _userId;
			}			
		}

		/// <summary>
		/// Client의 AgentMode를 구하거나 설정합니다.
		/// </summary>
		private AgentMode AgentMode
		{
			get
			{
				return _agentMode;
			}
			set
			{
				_agentMode = value;
			}
		}
		#endregion

		#region Protected methods
		#region Message Callback
		/// <summary>
		/// <seealso cref="iDASiT.FX.Win.Messenger.SendMessage"/>를 호출할 경우 내부적으로 Message를 처리할 Method입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalSendMessageCallback(object sender, MessageEventArgs e)
		{
			#region Peer to Peer Message
			// 특정 client에게 보낼 경우(자기 자신은 응답하지 않는다.)
			if (e.SendType == SendType.SendTo && e.EventSource == EventSource.Message
						&& string.Compare(ClientIpAddress, e.FromIpAddress, true) != 0
						&& string.Compare(ClientIpAddress, e.ToIpAddress, true) == 0)
			{
				OnSendedMessage(sender, e);

				return;
			}
			#endregion

			#region BroadCast Message
			// BroadCasting 일 경우
			if (e.SendType == SendType.BroadCast && e.EventSource == EventSource.Message
						&& string.Compare(ClientIpAddress, e.FromIpAddress, true) != 0)
			{
				OnBroadCastMessage(sender, e);

				return;
			}
			#endregion

			#region PutFile
			if (e.EventSource == EventSource.PutFile 
				&& string.Compare(ClientIpAddress, e.FromIpAddress, true) != 0
				&& string.Compare(ClientIpAddress, e.ToIpAddress, true) == 0)
			{
				try
				{
					FileReceiver fileReceiver = new FileReceiver(XMessage.PutFilePort);

					fileReceiver.ReceivedPath = e.Message;
					fileReceiver.CloseAfterReceive = true;
					fileReceiver.Start(false, 1);

					OnPutFileReceived(sender, e);
				}
				catch
				{
				}

				return;
			}
			#endregion

			#region GetFile Receiver
			// 서버객체를 사용하므로 Receiver를 서버에서 실행하면 서버만 Listen 할 수 있어서 각 client에서 listen 해야 한다.
			if (e.EventSource == EventSource.GetFileReceiveRequest 
				&& string.Compare(ClientIpAddress, e.ToIpAddress, true) == 0)
			{
				InternalGetFile(e.Message);

				return;
			}
			#endregion

			#region GetFile Sender
			if (e.EventSource == EventSource.GetFileSendRequest 
				&& string.Compare(ClientIpAddress, e.FromIpAddress, true) != 0
				&& string.Compare(ClientIpAddress, e.ToIpAddress, true) == 0)
			{
				try
				{
					#region Message를 parsing해서 FIleList를 구한다.
					string[] filePath = e.Message.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

					if (filePath.Length < 2)
						return;

					string[] fileList = new string[filePath.Length - 1];

					for (int i = 1; i < filePath.Length; i++)
						fileList[i - 1] = string.Format(@"{0}\{1}", filePath[0], filePath[i]);
					#endregion

					FileSender fileSender = new FileSender(e.FromIpAddress, XMessage.GetFilePort);

					fileSender.Connected += new ServiceEventHandler(OnGetFileSendConnected);
					fileSender.Disconnected += new ServiceEventHandler(OnGetFileSendDisconnected);
					fileSender.HeaderSended += new TransferEventHandler(OnGetFileSendHeaderSended);
					fileSender.Sending += new TransferEventHandler(OnGetFileSendSending);
					fileSender.Completed += new TransferEventHandler(OnGetFileSendCompleted);
					fileSender.Error += new ErrorEventHandler(OnGetFileSendError);

					fileSender.SendFile(fileList);
				}
				catch
				{
				}

				return;
			}
			#endregion
		}

		/// <summary>
		/// Peer to Peer Message를 수신 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnSendedMessage(object sender, MessageEventArgs e)
		{
			if (SendedMessage != null)
				SendedMessage(sender, e);
		}

		/// <summary>
		/// BroadCast Message를 수신 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnBroadCastMessage(object sender, MessageEventArgs e)
		{
			if (BroadCastedMessage != null)
				BroadCastedMessage(sender, e);
		}

		/// <summary>
		/// PutFile 후 파일 수신 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected virtual void OnPutFileReceived(object sender, MessageEventArgs e)
		{
			if (PutFileReceived != null)
				PutFileReceived(sender, e);
		}
		#endregion

		#region Login/Logout Callback
		/// <summary>
		/// 원격 서버에 Login 되면 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalLoginCallback(object sender, LoggingEventArgs e)
		{
			if (LoggedIn != null)
				LoggedIn(sender, e);
		}

		/// <summary>
		/// 원격 서버에서 Logout 되면 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalLogoutCallback(object sender, LoggingEventArgs e)
		{
			if (LoggedOut != null)
				LoggedOut(sender, e);
		}
		#endregion

		#region PutFile Callback
		/// <summary>
		/// 서버와 접속이 되면 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileConnectedCallback(object sender, ServerInfoEventArgs e)
		{
			if (PutFileConnected != null)
				PutFileConnected(sender, e);
		}

		/// <summary>
		/// 서버와 접속이 종료되면 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileDisconnectedCallback(object sender, ServerInfoEventArgs e)
		{
			if (PutFileDisconnected != null)
				PutFileDisconnected(sender, e);
		}

		/// <summary>
		/// Header를 전송했을 경우 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileHeaderSendedCallback(object sender, TransferInfoEventArgs e)
		{
			if (PutFileHeaderSended != null)
				PutFileHeaderSended(sender, e);
		}

		/// <summary>
		/// 데이터를 전송했을 경우 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileSendingCallback(object sender, TransferInfoEventArgs e)
		{
			if (PutFileSending != null)
				PutFileSending(sender, e);
		}

		/// <summary>
		/// 데이터 전송이 완료되면 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileCompletedCallback(object sender, TransferInfoEventArgs e)
		{
			if (PutFileCompleted != null)
				PutFileCompleted(sender, e);
		}

		/// <summary>
		/// 전송 도중 Error가 발생할 경우 Client에 Callback으로 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected override void InternalPutFileErrorCallback(object sender, ErrorEventArgs e)
		{
			if (PutFileError != null)
				PutFileError(sender, e);
		}
		#endregion

		#region GetFile
		private void InternalGetFile(string receivedPath)
		{
			FileReceiver fileReceiver = new FileReceiver(XMessage.GetFilePort);

			fileReceiver.ReceivedPath = receivedPath as string;
			fileReceiver.ReceivedPathWithRemoteIpAddress = true;
			fileReceiver.CloseAfterReceive = true;

			fileReceiver.Started += new ServiceEventHandler(OnGetFileStarted);
			fileReceiver.Stopped += new ServiceEventHandler(OnGetFileStopped);
			fileReceiver.AcceptClient += new AcceptClientEventHandler(OnGetFileAcceptClient);
			fileReceiver.HeaderReceived += new TransferEventHandler(OnGetFileHeaderReceived);
			fileReceiver.Receiving += new TransferEventHandler(OnGetFileReceiving);
			fileReceiver.Completed += new TransferEventHandler(OnGetFileCompleted);
			fileReceiver.Error += new ErrorEventHandler(OnGetFileError);

			fileReceiver.Start(false, 100);

			while (fileReceiver.Running == false)
				Thread.Sleep(500);
		}

		#region GetFile Receiver EventHandler
		/// <summary>
		/// Started 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileStarted(object sender, ServerInfoEventArgs e)
		{
			if (GetFileStarted != null)
				GetFileStarted(this, e);
		}

		/// <summary>
		/// Stopped 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileStopped(object sender, ServerInfoEventArgs e)
		{
			if (GetFileStopped != null)
				GetFileStopped(this, e);
		}

		/// <summary>
		/// AcceptClient 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileAcceptClient(object sender, SocketAsyncEventArgs e)
		{
			if (GetFileAcceptClient != null)
				GetFileAcceptClient(this, e);
		}

		/// <summary>
		/// HeaderReceived 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileHeaderReceived(object sender, TransferInfoEventArgs e)
		{
			if (GetFileHeaderReceived != null)
				GetFileHeaderReceived(this, e);
		}

		/// <summary>
		/// Receiving 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileReceiving(object sender, TransferInfoEventArgs e)
		{
			if (GetFileReceiving != null)
				GetFileReceiving(this, e);
		}

		/// <summary>
		/// Completed 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileCompleted(object sender, TransferInfoEventArgs e)
		{
			if (GetFileCompleted != null)
				GetFileCompleted(this, e);
		}

		/// <summary>
		/// Error 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileError(object sender, iDASiT.FX.Win.Messenger.ErrorEventArgs e)
		{
			if (GetFileError != null)
				GetFileError(this, e);
		}
		#endregion

		#region GetFile Sender EventHandler
		/// <summary>
		/// GetFile의 Connected 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileSendConnected(object sender, ServerInfoEventArgs e)
		{
			if (GetFileSendConnected != null)
				GetFileSendConnected(this, e);
		}

		/// <summary>
		/// GetFile의 Disconnected 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileSendDisconnected(object sender, ServerInfoEventArgs e)
		{
			if (GetFileSendDisconnected != null)
				GetFileSendDisconnected(this, e);
		}

		/// <summary>
		/// GetFile의 HeaderSended 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileSendHeaderSended(object sender, TransferInfoEventArgs e)
		{
			if (GetFileSendHeaderSended != null)
				GetFileSendHeaderSended(this, e);
		}

		/// <summary>
		/// GetFile의 Sending 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileSendSending(object sender, TransferInfoEventArgs e)
		{
			if (GetFileSendSending != null)
				GetFileSendSending(this, e);
		}

		/// <summary>
		/// GetFile의 Completed 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileSendCompleted(object sender, TransferInfoEventArgs e)
		{
			if (GetFileSendCompleted != null)
				GetFileSendCompleted(this, e);
		}

		/// <summary>
		/// GetFile의 Error 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected void OnGetFileSendError(object sender, iDASiT.FX.Win.Messenger.ErrorEventArgs e)
		{
			if (GetFileSendError != null)
				GetFileSendError(this, e);
		}
		#endregion
		#endregion
		#endregion

		#region Public methods
		/// <summary>
		/// 이 인스턴스의 수명 정책을 제어하기 위한 수명 서비스 개체를 가져옵니다.
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}

		/// <summary>
		/// 선택된 서버로 Login 합니다.
		/// <remarks>(모든 client에 로그인 message를 BroadCasting 합니다.)</remarks>
		/// </summary>
		/// <param name="userId">Login한 UserId입니다.</param>
		public bool Login(string userId)
		{
			SendType sendType = SendType.BroadCast;
			string toIpAddress = null;
			object tag = null;
			_userId = userId;
			_xMessage.AgentMode = AgentMode;

			if (_clientContext == null)
				_clientContext = new ClientContext(ClientIpAddress, userId);
			else
			{
				_clientContext.ClientIpAddress = ClientIpAddress;
				_clientContext.UserId = userId;
			}

			CallContext.SetData(ClientContext.ContextName, _clientContext);

			return _xMessage.Login(sendType, userId, toIpAddress, tag);
		}

		/// <summary>
		/// 선택된 서버로 Login 합니다.
		/// <remarks>(특정 client에만 알립니다.)</remarks>
		/// </summary>
		/// <param name="userId">Login한 UserId입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public bool Login(string userId, string toIpAddress)
		{
			SendType sendType = SendType.SendTo;
			object tag = null;
			_userId = userId;
			_xMessage.AgentMode = AgentMode;

			if (_clientContext == null)
				_clientContext = new ClientContext(ClientIpAddress, userId);
			else
			{
				_clientContext.ClientIpAddress = ClientIpAddress;
				_clientContext.UserId = userId;
			}

			CallContext.SetData(ClientContext.ContextName, _clientContext);

			return _xMessage.Login(sendType, userId, toIpAddress, tag);
		}

		/// <summary>
		/// 선택된 서버에서 Logout 합니다.
		/// <remarks>(모든 client에 로그인 message를 BroadCasting 합니다.)</remarks>
		/// </summary>
		public bool Logout()
		{
			SendType sendType = SendType.BroadCast;
			string toIpAddress = null;
			object tag = null;
			_xMessage.AgentMode = AgentMode;

			return _xMessage.Logout(sendType, UserId, toIpAddress, tag);
		}

		/// <summary>
		/// 선택된 서버에서 Logout 합니다.
		/// <remarks>(특정 client에만 알립니다.)</remarks>
		/// </summary>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public bool Logout(string toIpAddress)
		{
			SendType sendType = SendType.SendTo;
			object tag = null;
			_xMessage.AgentMode = AgentMode;

			return _xMessage.Logout(sendType, UserId, toIpAddress, tag);
		}

		/// <summary>
		/// 선택된 client로 Message를 전파 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="message">전파할 message를 입력합니다.</param>
		/// <returns>전파한 Client의 개수를 return 합니다.</returns>
		public int BroadCastMessage(string message)
		{
			_xMessage.AgentMode = AgentMode;

			return _xMessage.SendMessage(message, null);
		}

		/// <summary>
		/// 선택된 client로 Message를 전송 합니다.
		/// </summary>
		/// <param name="message">전파할 message를 입력합니다.</param>
		/// <param name="toIpAddresses">Message를 전송할 client IpAddress 입니다.</param>
		/// <returns>전송한 Client의 개수를 return 합니다.</returns>
		public int SendMessage(string message, params string[] toIpAddresses)
		{
			_xMessage.AgentMode = AgentMode;
			
			return _xMessage.SendMessage(message, toIpAddresses);
		}

		/// <summary>
		/// Controller에서 File을 가져 옵니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 remote path 입니다.</param>
		/// <param name="filePattern">가져오려는 파일 pattern 입니다.</param>
		/// <param name="localPath">가져온 파일을 저장할 로컬 경로입니다.<br/>경로 아래 IpAddress 형태로 폴더가 생성됩니다.</param>
		/// <param name="fromIpAddresses">데이터를 가져올 Client 입니다.</param>
		public void GetFiles(string remotePath, string filePattern, string localPath, params string[] fromIpAddresses)
		{
			_xMessage.AgentMode = AgentMode;
			
			_xMessage.GetFiles(remotePath, filePattern, localPath, fromIpAddresses);
		}

		/// <summary>
		/// Controller에서 File을 가져 옵니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="remotePath">가져오려고 하는 client의 local file path 입니다.</param>
		/// <param name="filePatterns">가져오려는 파일 pattern 입니다.</param>
		/// <param name="localPath">가져온 파일을 저장할 로컬 경로입니다.<br/>경로 아래 IpAddress 형태로 폴더가 생성됩니다.</param>
		/// <param name="fromIpAddresses">데이터를 가져올 Client 입니다.</param>
		public void GetFiles(string remotePath, string[] filePatterns, string localPath, params string[] fromIpAddresses)
		{
			_xMessage.AgentMode = AgentMode;
			
			_xMessage.GetFiles(remotePath, filePatterns, localPath, fromIpAddresses);
		}

		/// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.<br></br>
		/// 지정된 Folder만 검색합니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePattern">전송하려는 파일 pattern 입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string filePattern, string remotePath, params string[] toIpAddresses)
		{
			_xMessage.AgentMode = AgentMode;
			
			_xMessage.PutFiles(localPath, new string[] { filePattern }, remotePath, toIpAddresses);
		}

		/// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.<br></br>
		/// 지정된 Folder만 검색합니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePatterns">전송하려는 파일 pattern 입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string[] filePatterns, string remotePath, params string[] toIpAddresses)
		{
			SearchOption searchOption = SearchOption.TopDirectoryOnly;
			_xMessage.AgentMode = AgentMode;

			_xMessage.PutFiles(localPath, filePatterns, searchOption, remotePath, toIpAddresses);
		}

		/// <summary>
		/// Controller에서 Client로 File을 전송 합니다.
		/// <remarks>생성자의 AgentMode를 Controller로 설정해야 사용할 수 있습니다.</remarks>
		/// </summary>
		/// <param name="localPath">전송하려는 파일이 있는 경로 입니다.</param>
		/// <param name="filePatterns">전송하려는 파일 pattern 입니다.</param>
		/// <param name="searchOption">로컬 파일을 검색하는 Option입니다.</param>
		/// <param name="remotePath">client에 전송되어 저장되는 remote path 입니다.</param>
		/// <param name="toIpAddresses">전송할 Client 입니다.</param>
		public void PutFiles(string localPath, string[] filePatterns, SearchOption searchOption, string remotePath, params string[] toIpAddresses)
		{
			_xMessage.AgentMode = AgentMode;
			
			_xMessage.PutFiles(localPath, filePatterns, searchOption, remotePath, toIpAddresses);
		}
		#endregion
	}
}
