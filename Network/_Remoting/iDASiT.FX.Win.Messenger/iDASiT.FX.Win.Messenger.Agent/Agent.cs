﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.IO;
using System.Windows.Forms;
using System.Net;
using System.Runtime.Remoting.Messaging;

namespace iDASiT.FX.Win.Messenger.Agent
{
	public class Agent
	{
		private static AgentMode _agentMode;

		static void Main(string[] args)
		{
			string message = string.Empty;

			_agentMode = AgentMode.Controller;
			ShowUsage();

			CallbackAgent client = new CallbackAgent(_agentMode);

			client.LoggedIn += new LogEventHandler(client_LoggedIn);
			client.LoggedOut += new LogEventHandler(client_LoggedOut);
			client.SendedMessage += new SendMessageEventHandler(client_SendedMessage);
			client.BroadCastedMessage += new SendMessageEventHandler(client_BroadCastedMessage);
			client.GetFileCompleted += new TransferEventHandler(client_GetFileCompleted);
			client.GetFileSendCompleted += new TransferEventHandler(client_GetFileSendCompleted);
			client.PutFileCompleted += new TransferEventHandler(client_PutFileCompleted);
			client.PutFileReceived += new SendMessageEventHandler(client_PutFileReceived);

			client.Login(Dns.GetHostName());

			while ((message = Console.ReadLine().ToLower()) != "q")
			{
				if (message == string.Empty)
					continue;

				string key = message.ToLower().Substring(0, 1);
				string[] data = message.Split(' ');

				for (int i = 1; i < data.Length; i++)
					data[i] = string.Format("192.168.25.{0}", data[i]);

				string[] targets = new string[data.Length - 1];

				switch (key)
				{
					case "s":
						for (int i = 1; i < data.Length; i++)
							targets[i - 1] = data[i];

						client.PutFiles(@"C:\WINDOWS\system32", "at.exe", @"D:\Temp", targets);
						ShowUsage();

						continue;
					case "p":
						StringBuilder messages = new StringBuilder();

						for (int i = 2; i < data.Length; i++)
							messages.AppendFormat(string.Format("{0} ", data[i].Replace("192.168.25.", string.Empty)));

						client.SendMessage(messages.ToString(0, messages.Length - 1), data[1]);
						ShowUsage();


						continue;
					case "g":
						for (int i = 1; i < data.Length; i++)
							targets[i - 1] = data[i];

						client.GetFiles(@"C:\WINDOWS\system32", "at.exe", @"D:\Temp", targets);
						//client.GetFiles(@"C:\WINDOWS\system32", "c*.nls", @"D:\Temp", targets);
						ShowUsage();

						continue;
				}

				if (_agentMode == AgentMode.Controller)
                	client.BroadCastMessage(message);
				else
					client.SendMessage(message, "192.168.25.246");
			}

			client.Logout();
		}

		static void client_LoggedIn(object sender, LoggingEventArgs e)
		{
			Console.WriteLine("Login : {0}\t{1}\t{2}\t{3}", e.SendType, e.UserId, e.FromIpAddress, e.ToIpAddress);
		}

		static void client_LoggedOut(object sender, LoggingEventArgs e)
		{
			Console.WriteLine("Logout : {0}\t{1}\t{2}\t{3}", e.SendType, e.UserId, e.FromIpAddress, e.ToIpAddress);
		}

		static void client_SendedMessage(object sender, MessageEventArgs e)
		{
			Console.WriteLine("".PadRight(79, '-'));
			Console.WriteLine(string.Format("{0}님의 말 : {1}", e.FromIpAddress, e.Message));
			Console.WriteLine("".PadRight(79, '-'));
		}
		static void client_BroadCastedMessage(object sender, MessageEventArgs e)
		{
			Console.WriteLine("".PadRight(79, '*'));
			Console.WriteLine(string.Format("{0}님이 전파한 말 : {1}", e.FromIpAddress, e.Message));
			Console.WriteLine("".PadRight(79, '*'));
		}

		static void client_PutFileReceived(object sender, MessageEventArgs e)
		{
			Console.WriteLine("Put from {0} in {1}", e.FromIpAddress, e.Message);
		}

		static void client_PutFileCompleted(object sender, TransferInfoEventArgs e)
		{
			Console.WriteLine("Put Completed {0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);			
		}

		static void client_GetFileSendCompleted(object sender, TransferInfoEventArgs e)
		{
			Console.WriteLine("Get Send Completed {0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		static void client_GetFileCompleted(object sender, TransferInfoEventArgs e)
		{
			Console.WriteLine("Get Receive Completed {0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		private static void ShowUsage()
		{
			Console.WriteLine("'q' to quit Agent({0} mode)-default message = {1}", _agentMode, _agentMode == AgentMode.Controller ? "BroadCasting" : "Peer to Peer to 192.168.25.31");

			if (_agentMode == AgentMode.Controller)
			{
				Console.WriteLine("\tp 31 msg : send msg message to 192.168.25.31");
				Console.WriteLine("\ts 31 246 : send \"at.exe\" file to 192.168.25.31, 192.168.25.246");
				Console.WriteLine("\tg 31 246 : get file from 192.168.25.31, 192.168.25.246 to D:\\Temp\\<Ip>");
			}
		}
	}
}
