﻿namespace Server
{
	partial class ServerForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnHello = new System.Windows.Forms.Button();
			this.tbLog = new System.Windows.Forms.TextBox();
			this.btnHi = new System.Windows.Forms.Button();
			this.txtMessage = new System.Windows.Forms.TextBox();
			this.btnSend = new System.Windows.Forms.Button();
			this.btnStart = new System.Windows.Forms.Button();
			this.btnStop = new System.Windows.Forms.Button();
			this.lbTargets = new System.Windows.Forms.ListBox();
			this.btnDeselectTarget = new System.Windows.Forms.Button();
			this.tbPath = new System.Windows.Forms.TextBox();
			this.btnFileSend = new System.Windows.Forms.Button();
			this.odfSendFile = new System.Windows.Forms.OpenFileDialog();
			this.SuspendLayout();
			// 
			// btnHello
			// 
			this.btnHello.Location = new System.Drawing.Point(15, 14);
			this.btnHello.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnHello.Name = "btnHello";
			this.btnHello.Size = new System.Drawing.Size(96, 38);
			this.btnHello.TabIndex = 0;
			this.btnHello.Text = "Hello";
			this.btnHello.UseVisualStyleBackColor = true;
			// 
			// tbLog
			// 
			this.tbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tbLog.Location = new System.Drawing.Point(235, 86);
			this.tbLog.Multiline = true;
			this.tbLog.Name = "tbLog";
			this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbLog.Size = new System.Drawing.Size(568, 351);
			this.tbLog.TabIndex = 7;
			// 
			// btnHi
			// 
			this.btnHi.Location = new System.Drawing.Point(119, 14);
			this.btnHi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnHi.Name = "btnHi";
			this.btnHi.Size = new System.Drawing.Size(96, 38);
			this.btnHi.TabIndex = 1;
			this.btnHi.Text = "Hi";
			this.btnHi.UseVisualStyleBackColor = true;
			// 
			// txtMessage
			// 
			this.txtMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtMessage.Location = new System.Drawing.Point(235, 20);
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(465, 27);
			this.txtMessage.TabIndex = 2;
			// 
			// btnSend
			// 
			this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSend.Location = new System.Drawing.Point(707, 14);
			this.btnSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnSend.Name = "btnSend";
			this.btnSend.Size = new System.Drawing.Size(96, 38);
			this.btnSend.TabIndex = 3;
			this.btnSend.Text = "Sen&d";
			this.btnSend.UseVisualStyleBackColor = true;
			// 
			// btnStart
			// 
			this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnStart.Location = new System.Drawing.Point(603, 443);
			this.btnStart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnStart.Name = "btnStart";
			this.btnStart.Size = new System.Drawing.Size(96, 38);
			this.btnStart.TabIndex = 9;
			this.btnStart.Text = "&Start";
			this.btnStart.UseVisualStyleBackColor = true;
			// 
			// btnStop
			// 
			this.btnStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnStop.Location = new System.Drawing.Point(707, 443);
			this.btnStop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnStop.Name = "btnStop";
			this.btnStop.Size = new System.Drawing.Size(96, 38);
			this.btnStop.TabIndex = 10;
			this.btnStop.Text = "S&top";
			this.btnStop.UseVisualStyleBackColor = true;
			// 
			// lbTargets
			// 
			this.lbTargets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.lbTargets.FormattingEnabled = true;
			this.lbTargets.ItemHeight = 20;
			this.lbTargets.Location = new System.Drawing.Point(15, 53);
			this.lbTargets.Name = "lbTargets";
			this.lbTargets.Size = new System.Drawing.Size(214, 384);
			this.lbTargets.TabIndex = 6;
			// 
			// btnDeselectTarget
			// 
			this.btnDeselectTarget.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnDeselectTarget.Location = new System.Drawing.Point(15, 443);
			this.btnDeselectTarget.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnDeselectTarget.Name = "btnDeselectTarget";
			this.btnDeselectTarget.Size = new System.Drawing.Size(214, 38);
			this.btnDeselectTarget.TabIndex = 8;
			this.btnDeselectTarget.Text = "&Deselect Target";
			this.btnDeselectTarget.UseVisualStyleBackColor = true;
			// 
			// tbPath
			// 
			this.tbPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tbPath.Location = new System.Drawing.Point(235, 53);
			this.tbPath.Name = "tbPath";
			this.tbPath.Size = new System.Drawing.Size(465, 27);
			this.tbPath.TabIndex = 4;
			// 
			// btnFileSend
			// 
			this.btnFileSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnFileSend.Location = new System.Drawing.Point(707, 53);
			this.btnFileSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnFileSend.Name = "btnFileSend";
			this.btnFileSend.Size = new System.Drawing.Size(96, 27);
			this.btnFileSend.TabIndex = 5;
			this.btnFileSend.Text = "Open &File";
			this.btnFileSend.UseVisualStyleBackColor = true;
			// 
			// odfSendFile
			// 
			this.odfSendFile.DefaultExt = "*.*";
			this.odfSendFile.FileName = "openFileDialog1";
			this.odfSendFile.Filter = "\"All files|*.*\"";
			this.odfSendFile.Multiselect = true;
			this.odfSendFile.ReadOnlyChecked = true;
			this.odfSendFile.ShowReadOnly = true;
			this.odfSendFile.Title = "Select files to send.";
			// 
			// ServerForm
			// 
			this.AcceptButton = this.btnSend;
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(815, 487);
			this.Controls.Add(this.lbTargets);
			this.Controls.Add(this.tbPath);
			this.Controls.Add(this.txtMessage);
			this.Controls.Add(this.tbLog);
			this.Controls.Add(this.btnFileSend);
			this.Controls.Add(this.btnSend);
			this.Controls.Add(this.btnHi);
			this.Controls.Add(this.btnStop);
			this.Controls.Add(this.btnDeselectTarget);
			this.Controls.Add(this.btnStart);
			this.Controls.Add(this.btnHello);
			this.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "ServerForm";
			this.Text = "Server";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnHello;
		private System.Windows.Forms.TextBox tbLog;
		private System.Windows.Forms.Button btnHi;
		private System.Windows.Forms.TextBox txtMessage;
		private System.Windows.Forms.Button btnSend;
		private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.ListBox lbTargets;
		private System.Windows.Forms.Button btnDeselectTarget;
		private System.Windows.Forms.TextBox tbPath;
		private System.Windows.Forms.Button btnFileSend;
		private System.Windows.Forms.OpenFileDialog odfSendFile;
	}
}

