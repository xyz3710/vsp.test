﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;

namespace ServerAsync
{
	public partial class ServerForm : Form
	{
		private const string HOST = "127.0.0.1";
		private const int PORT = 19000;

		public ServerForm()
		{
			InitializeComponent();

			Server = new TransferServer();
			Server.Logging += (senderObject, ea) =>
			{
				WriteLog(ea.Message);
			};
			Server.SocketLogging += (senderObject, ea) =>
			{
				WriteLog(string.Format("{0}\r\n\t{1}", ea.Message, ea.SocketException.SocketErrorCode));
			};
			Server.ExceptionLogging += (senderObject, ea) =>
			{
				WriteLog(string.Format("{0}\r\n\t{1}", ea.Message, ea.Exception.Message));
			};
			Server.Started += OnStarted;
			Server.Stopped += OnStopped;
			Server.Connected += OnConnected;
			Server.Disconnected += OnDisconnected;
			Server.Received += OnReceived;
			Server.Sent += OnSent;

			RegisterButtonHandler();
		}

		private void RegisterButtonHandler()
		{
			btnHello.Click += (senderObject, ea) =>
			{
				Send(" HELLO , I'm server.");
			};
			btnHi.Click += (senderObject, ea) =>
			{
				Send(" HI !! , I'm server.");
			};
			btnSend.Click += (senderObject, ea) =>
			{
				Send(txtMessage.Text);
				txtMessage.ResetText();
			};
			btnDeselectTarget.Click += (senderObject, ea) =>
			{
				lbTargets.ClearSelected();
			};
			btnStart.Click += (senderObject, ea) =>
			{
				Server.Start(port: PORT);
			};
			btnStop.Click += (senderObject, ea) =>
			{
				Server.Stop();
			};
			btnFileSend.Click += (senderObject, ea) =>
			{

			};
		}

		private TransferServer Server
		{
			get;
			set;
		}

		private Encoding Encoding
		{
			get
			{
				return Encoding.UTF8;
			}
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);
			AdjustLocaltion();
			btnStart.PerformClick();
		}

		private void AdjustLocaltion()
		{
			Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
			Location = new Point(workingArea.Left, workingArea.Top);
		}

		private void WriteLog(string message)
		{
			Invoke(new MethodInvoker(() =>
			{
				tbLog.Text = tbLog.Text.Insert(0, message + Environment.NewLine);
			}));
		}

		private void SetText()
		{
			Invoke(new MethodInvoker(() =>
			{
				Text = string.Format("Server => IsRunning: {0}, IsConnected: {1}", Server.IsRunning, Server.IsConnected);
			}));
		}

		private void EnableSendButtons(bool enabled)
		{
			Invoke(new MethodInvoker(() =>
			{
				btnHello.Enabled = enabled;
				btnHi.Enabled = enabled;
				btnSend.Enabled = enabled;
			}));
		}

		private void EnableServiceControl(bool enabled)
		{
			Invoke(new MethodInvoker(() =>
			{
				btnStart.Enabled = !enabled;
				btnStop.Enabled = enabled;
			}));
		}

		private void ClearTargetList()
		{
			Invoke(new MethodInvoker(() =>
			{
				lbTargets.Items.Clear();
			}));
		}

		private void AddTargetList(string item)
		{
			Invoke(new MethodInvoker(() =>
			{
				lbTargets.Items.Add(item);
			}));
		}

		private void RemoteTargetList(string item)
		{
			Invoke(new MethodInvoker(() =>
			{
				lbTargets.Items.Remove(item);
			}));
		}

		private void OnStarted(object senderObject, EventArgs ea)
		{
			SetText();
			EnableServiceControl(true);
			EnableSendButtons(false);

			ClearTargetList();
		}
		private void OnStopped(object senderObject, EventArgs ea)
		{
			SetText();
			EnableServiceControl(false);
			EnableSendButtons(false);
		}

		private void OnConnected(object sender, TransferServer.ConnectedEventArgs e)
		{
			SetText();
			WriteLog(string.Format("Connected: {0}", e.RemoteId));
			AddTargetList(e.RemoteId);
			EnableSendButtons(Server.Targets.Count > 0);
		}

		private void OnDisconnected(object sender, TransferServer.DisconnectedEventArgs e)
		{
			SetText();
			WriteLog(string.Format("Disconnected: {0}\r\n{1} {2}", e.RemoteId, e.SocketErrorCode, e.SocketException.Message));
			RemoteTargetList(e.RemoteId);
			EnableSendButtons(Server.Targets.Count > 0);
		}

		private void OnSent(object sender, TransferServer.TransferEventArgs e)
		{
			var message = Encoding.GetString(e.Data, 0, e.TransferedLength);

			WriteLog(string.Format("Sent: [{0}] {1}", e.RemoteId, message));
		}

		private void OnReceived(object sender, TransferServer.TransferEventArgs e)
		{
			var message = Encoding.GetString(e.Data, 0, e.TransferedLength);

			WriteLog(string.Format("Received: [{0}] {1}", e.RemoteId, message));
		}

		private void Send(string message)
		{
			if (lbTargets.SelectedIndex != -1)
			{
				var remoteId = Convert.ToString(lbTargets.SelectedItem);
				var messageBytes = Encoding.GetBytes(string.Format("[{0}]{1}", remoteId, message));

				Server.Send(messageBytes, remoteId);
			}
			else
			{
				foreach (string remoteId in Server.Targets.Keys)
				{
					var messageBytes = Encoding.GetBytes(string.Format("[{0}]{1}", remoteId, message));

					Server.Send(messageBytes, remoteId);
				}
			}
		}
	}
}
