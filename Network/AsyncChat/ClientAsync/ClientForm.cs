﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common;

namespace ClientAsync
{
	public partial class ClientForm : Form
	{
		private const string HOST = "127.0.0.1";
		private const int PORT = 19000;

		public ClientForm()
		{
			InitializeComponent();

			Client = new TransferClient();
			Client.Logging += (senderObject, ea) =>
			{
				WriteLog(ea.Message);
			};
			Client.SocketLogging += (senderObject, ea) =>
			{
				WriteLog(string.Format("{0}\r\n\t{1}", ea.Message, ea.SocketException.SocketErrorCode));
			};
			Client.ExceptionLogging += (senderObject, ea) =>
			{
				WriteLog(string.Format("{0}\r\n\t{1}", ea.Message, ea.Exception.Message));
			};
			Client.Started += OnStarted;
			Client.Stopped += OnStopped;
			Client.Connected += OnConnected;
			Client.Disconnected += OnDisconnected;
			Client.Received += OnReceived;
			Client.Sent += OnSent;

			RegisterButtonHandler();
		}

		private void RegisterButtonHandler()
		{
			btnHello.Click += (senderObject, ea) =>
			{
				Send(" HELLO , I'm client.");
			};
			btnHi.Click += (senderObject, ea) =>
			{
				Send(" HI !! , I'm client.");
			};
			btnSend.Click += (senderObject, ea) =>
			{
				Send(txtMessage.Text);
				txtMessage.ResetText();
			};
			btnFileSend.Click += (senderObject, ea) =>
			{

			};
			btnStart.Click += (senderObject, ea) =>
			{
				Client.Start(HOST, PORT);
			};
			btnStop.Click += (senderObject, ea) =>
			{
				Client.Stop();
			};
		}

		private TransferClient Client
		{
			get;
			set;
		}

		private Encoding Encoding
		{
			get
			{
				return Encoding.UTF8;
			}
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);
			AdjustLocaltion();
			btnStart.PerformClick();

			tbPath.Text = @"E:\GachiSoft\Etc\GS.ImageCollector\GS.ImageCollector\bin\Debug\GS.Common.pdb";
		}

		private void AdjustLocaltion()
		{
			Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
			Location = new Point(workingArea.Width - Size.Width, workingArea.Height - Size.Height);
		}

		private void WriteLog(string message)
		{
			Invoke(new MethodInvoker(() =>
			{
				tbLog.Text = tbLog.Text.Insert(0, message + Environment.NewLine);
			}));
		}

		private void SetText(string remoteId = "")
		{
			Invoke(new MethodInvoker(() =>
			{
				Text = string.Format("Client:{0} => IsRunning: {1}, IsConnected: {2}", remoteId, Client.IsRunning, Client.IsConnected);
			}));
		}
		private void EnableSendButtons(bool enabled)
		{
			Invoke(new MethodInvoker(() =>
			{
				btnHello.Enabled = enabled;
				btnHi.Enabled = enabled;
				btnSend.Enabled = enabled;
			}));
		}

		private void EnableServiceControl(bool enabled)
		{
			Invoke(new MethodInvoker(() =>
			{
				btnStart.Enabled = !enabled;
				btnStop.Enabled = enabled;
			}));
		}

		private void OnStarted(object senderObject, EventArgs ea)
		{
			SetText();
			EnableServiceControl(true);
			EnableSendButtons(false);
		}

		private void OnStopped(object senderObject, EventArgs ea)
		{
			SetText();
			EnableServiceControl(false);
			EnableSendButtons(false);
		}

		private void OnConnected(object sender, TransferServer.ConnectedEventArgs e)
		{
			SetText(e.RemoteId);
			EnableSendButtons(true);
			WriteLog(string.Format("Connected: {0}", e.RemoteId));
		}

		private void OnDisconnected(object sender, TransferServer.DisconnectedEventArgs e)
		{
			SetText(e.RemoteId);
			EnableSendButtons(false);
			WriteLog(string.Format("Disconnected: {0}\r\n{1} {2}", e.RemoteId, e.SocketErrorCode, e.SocketException.Message));
		}

		private void OnSent(object sender, TransferServer.TransferEventArgs e)
		{
			var message = Encoding.GetString(e.Data, 0, e.TransferedLength);

			WriteLog(string.Format("Sent: [{0}] {1}", e.RemoteId, message));
		}

		private void OnReceived(object sender, TransferServer.TransferEventArgs e)
		{
			var message = Encoding.GetString(e.Data, 0, e.TransferedLength);

			WriteLog(string.Format("Received: [{0}] {1}", e.RemoteId, message));
		}

		private void Send(string message)
		{
			var messageBytes = Encoding.GetBytes(message);

			Client.Send(messageBytes);
		}
	}
}
