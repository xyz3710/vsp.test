﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	/// <summary>
	/// Class AsyncObject.
	/// </summary>
	public class AsyncObject
	{
		/// <summary>
		/// Initializes a new instance of the AsyncObject class.
		/// </summary>
		/// <param name="bufferSize">Size of the buffer.</param>
		public AsyncObject(int bufferSize = 1)
		{
			Buffer = new byte[bufferSize];
		}

		/// <summary>
		/// Buffer를 구하거나 설정합니다.
		/// </summary>
		/// <value>Buffer를 반환합니다.</value>
		public byte[] Buffer
		{
			get;
			set;
		}

		/// <summary>
		/// Buffer 길이를 구합니다.
		/// </summary>
		/// <value>Buffer 길이를 반환합니다.</value>
		public int BufferLength
		{
			get
			{
				return Buffer.Length;
			}
		}

		/// <summary>
		/// 작업 소켓을 구하거나 설정합니다.
		/// </summary>
		/// <value>Socket을 반환합니다.</value>
		public System.Net.Sockets.Socket Socket
		{
			get;
			set;
		}

	}
}
