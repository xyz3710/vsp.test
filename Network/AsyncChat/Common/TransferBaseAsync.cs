﻿2015년 5월 14일 목요일using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Common
{
	/// <summary>
	/// Class TransferBaseAsync.
	/// </summary>
	public abstract partial class TransferBaseAsync : IDisposable
	{
		/// <summary>
		/// The default max buffer size
		/// </summary>
		protected const int MAX_BUFFER_SIZE = 1024;
		/// <summary>
		/// The default host
		/// </summary>
		protected const string DEFAULT_HOST = "127.0.0.1";
		/// <summary>
		/// The default port
		/// </summary>
		protected const int DEFAULT_PORT = 19000;

		/// <summary>
		/// Initializes a new instance of the <see cref="TransferBase"/> class.
		/// </summary>
		/// <param name="maxBufferSize">Maximum size of the buffer(default 1024).</param>
		public TransferBaseAsync(int maxBufferSize)
		{
			MaxBufferSize = maxBufferSize;
			ReceiveHandler = new AsyncCallback(ReceiveCallback);
			SendHandler = new AsyncCallback(SendCallback);
			TimeForFailedConnectionRefresh = 50;
		}

		/// <summary>
		/// 현재 작업 Socket를 구하거나 설정합니다.
		/// </summary>
		/// <value>현재 작업 소켓을 반환합니다.</value>
		protected Socket CurrentSocket
		{
			get;
			set;
		}

		/// <summary>
		/// 연결된 작업 Socket를 구하거나 설정합니다.
		/// </summary>
		/// <value>연결된 작업 소켓을 반환합니다.</value>
		protected Socket TargetSocket
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the receive handler.
		/// </summary>
		/// <value>The receive handler.</value>
		protected AsyncCallback ReceiveHandler
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the send handler.
		/// </summary>
		/// <value>The send handler.</value>
		protected AsyncCallback SendHandler
		{
			get;
			set;
		}

		/// <summary>
		/// Gets a value indicating whether this instance is server role.
		/// </summary>
		/// <value><c>true</c> if this instance is server role; otherwise, <c>false</c>.</value>
		protected abstract bool IsServerRole
		{
			get;
		}

		/// <summary>
		/// Gets or sets the maximum size of the buffer.
		/// </summary>
		/// <value>The maximum size of the buffer.</value>
		public int MaxBufferSize
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the host address.
		/// </summary>
		/// <value>The host address.</value>
		public string HostAddress
		{
			get;
			protected set;
		}

		/// <summary>
		/// Gets or sets the port.
		/// </summary>
		/// <value>The port.</value>
		public int Port
		{
			get;
			protected set;
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is running.
		/// </summary>
		/// <value><c>true</c> if this instance is running; otherwise, <c>false</c>.</value>
		public bool IsRunning
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is connected.
		/// </summary>
		/// <value><c>true</c> if this instance is connected; otherwise, <c>false</c>.</value>
		public bool IsConnected
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the remote identifier.
		/// </summary>
		/// <value>The remote identifier.</value>
		public string RemoteId
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the time for failed connection refesh.
		/// </summary>
		/// <value>The time to failed connection.</value>
		public int TimeForFailedConnectionRefresh
		{
			get;
			set;
		}

		/// <summary>
		/// Gets the remote identifier by <seealso cref="IsServerRole"/>. 
		/// <remarks>This used in server remoteId.</remarks>
		/// </summary>
		/// <param name="socket">The socket.</param>
		/// <returns>System.String.</returns>
		protected string GetRemoteId(Socket socket)
		{
			var remoteId = string.Empty;

			try
			{
				if (IsServerRole == true)
				{
				remoteId = Convert.ToString(socket.RemoteEndPoint as IPEndPoint);
			}
				else
			{
					remoteId = Convert.ToString(socket.LocalEndPoint as IPEndPoint);
			}
			}
			catch (SocketException)
			{
			}
			catch (ObjectDisposedException)
			{
			}

			return remoteId;
		}

		/// <summary>
		/// Sockets the exception handler.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="socketException">The socket exception.</param>
		/// <param name="remoteId">The remote identifier.</param>
		protected virtual void SocketExceptionHandler(string message, SocketException socketException, Socket workingSoket)
		{
			OnSocketLogging(message, socketException);

			switch (socketException.SocketErrorCode)
			{
				case SocketError.Disconnecting:
				case SocketError.ConnectionReset:
				case SocketError.ConnectionAborted:
				case SocketError.ConnectionRefused:
					var remoteId = RemoteId;

					if (workingSoket != null)
					{
						remoteId = GetRemoteId(workingSoket);
					}

					OnDisconnected(remoteId, socketException);
					Thread.Sleep(TimeForFailedConnectionRefresh);
					Initialize();

					break;
			}
		}

		/// <summary>
		/// Exceptions the handler.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="exception">The exception.</param>
		protected virtual void ExceptionHandler(string message, Exception exception)
		{
			OnExceptionLogging(message, exception);
		}

		/// <summary>
		/// Initializes this TargetSocket.
		/// </summary>
		protected abstract void Initialize();

		/// <summary>
		/// Receive data with asynchronous.
		/// </summary>
		/// <param name="ao">The async object.</param>
		protected virtual void BeginReceive(AsyncObject ao)
		{
			//if (TargetSocket == null)
			//{
			//	Initialize();
			//}

			if (ao.Socket == null)
			{
				Initialize();
			}

			if (ao.Socket.Connected == false)
			{
				OnLogging("Target was not connected.");

				return;
			}

			try
			{
				ao.Socket.BeginReceive(ao.Buffer, 0, ao.BufferLength, SocketFlags.None, ReceiveHandler, ao);
			}
			catch (SocketException se)
			{
				SocketExceptionHandler("BeginReceive was fail: ", se, ao.Socket);
				BeginReceive(ao);
			}
			catch (Exception ex)
			{
				ExceptionHandler("BeginReceive unknown exception", ex);
			}
		}

		/// <summary>
		/// Receives the asyncCallback.
		/// </summary>
		/// <param name="ar">The async result data.</param>
		protected void ReceiveCallback(IAsyncResult ar)
		{
			//if (TargetSocket == null || TargetSocket.Connected == false)
			//{
			//	return;
			//}

			AsyncObject ao = ar.AsyncState as AsyncObject;

			if (ao.Socket == null || ao.Socket.Connected == false)
			{
				return;
			}

			OnLogging("Received.");

			try
			{
				int receivedLength = ao.Socket.EndReceive(ar);

				OnReceived(ao.Buffer, receivedLength, GetRemoteId(ao.Socket));
			}
			catch (SocketException se)
			{
				SocketExceptionHandler("ReceiveCallback with target was fail: ", se, ao.Socket);
			}
			catch (Exception ex)
			{
				ExceptionHandler("ReceiveCallback unknown exception: ", ex);
			}
			finally
			{
				BeginReceive(ao);
			}
		}

		/// <summary>
		/// Send data with asynchronous.
		/// </summary>
		/// <param name="ao">The async object.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		protected virtual bool BeginSend(AsyncObject ao)
		{
			bool result = false;

			if (ao.Socket == null)
			{
				Initialize();
			}

			if (ao.Socket.Connected == false)
			{
				OnLogging("Target was not connected.");

				return result;
			}

			try
			{
				ao.Socket.BeginSend(ao.Buffer, 0, ao.BufferLength, SocketFlags.None, SendHandler, ao);
				result = true;
			}
			catch (SocketException se)
			{
				SocketExceptionHandler("BeginSend was fail: ", se, ao.Socket);
			}
			catch (Exception ex)
			{
				ExceptionHandler("BeginSend unknown exception", ex);
			}

			return result;
		}

		/// <summary>
		/// Sends the asyncCallback.
		/// </summary>
		/// <param name="ar">The async result data.</param>
		protected void SendCallback(IAsyncResult ar)
		{
			OnLogging("Sent.");
			AsyncObject ao = ar.AsyncState as AsyncObject;

			try
			{
				int sentLength = ao.Socket.EndSend(ar);

				OnSent(ao.Buffer, sentLength, GetRemoteId(ao.Socket));
			}
			catch (SocketException se)
			{
				SocketExceptionHandler("SendCallback with target was fail: ", se, ao.Socket);
			}
			catch (Exception ex)
			{
				ExceptionHandler("SendCallback unknown exception: ", ex);
			}
		}

		/// <summary>
		/// Sends the data to connected target.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <returns>If target was not connected, then return false.</returns>
		public virtual bool Send(byte[] data)
		{
			return Send(data, TargetSocket);
		}

		/// <summary>
		/// Sends the data specified target.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="targetSocket">The target socket.</param>
		/// <returns>If target was not connected, then return false.</returns>
		public virtual bool Send(byte[] data, Socket targetSocket)
		{
			if (targetSocket == null)
			{
				OnLogging(string.Format("targetSocket({0}) was null, use default TagetSocket(last connected target).", GetRemoteId(targetSocket)));
				Initialize();

				return Send(data);
			}

			if (targetSocket.Connected == false)
			{
				OnLogging(string.Format("targetSocket({0}) was not connected.", GetRemoteId(targetSocket)));

				return false;
			}

			AsyncObject ao = new AsyncObject(data.Length)
			{
				Buffer = data,
				Socket = targetSocket,
			};

			return BeginSend(ao);
		}

		/// <summary>
		/// Starts the specified host.
		/// </summary>
		/// <param name="hostAddress">The host address.</param>
		/// <param name="port">The port.</param>
		public virtual void Start(string hostAddress = DEFAULT_HOST, int port = DEFAULT_PORT)
		{
			if (IsRunning == true)
			{
				return;
			}

			HostAddress = hostAddress;
			Port = port;
			Initialize();
			OnStarted();
		}

		/// <summary>
		/// Stops this instance.
		/// </summary>
		public virtual void Stop()
		{
			StopAllSocket();
		}

		private void StopAllSocket()
		{
			if (IsRunning == false)
			{
				return;
			}

			if (CurrentSocket != null)
			{
				if (CurrentSocket.Connected == true)
				{
					//CurrentSocket.Shutdown(SocketShutdown.Both);
				}

				CurrentSocket.Close();
				CurrentSocket = null;
			}

			if (TargetSocket != null)
			{
				if (TargetSocket.Connected == true)
				{
					//TargetSocket.Shutdown(SocketShutdown.Both);
				}

				TargetSocket.Close();
				TargetSocket = null;
			}

			OnStopped();
		}

		#region IDisposable

		/// <summary>
		/// Releases the resources that have been allocated defines a method.
		/// </summary>
		public virtual void Dispose()
		{
			if (IsRunning == true)
			{
				StopAllSocket();
			}
		}

		#endregion
	}
}
