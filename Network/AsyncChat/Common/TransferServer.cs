﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	/// <summary>
	/// Class TransferServer.
	/// </summary>
	public partial class TransferServer : TransferBase
	{
		/// <summary>
		/// Initializes a new instance of the TransferServer class.
		/// </summary>
		/// <param name="maxBufferSize">Maximum size of the buffer(default 1024).</param>
		public TransferServer(int maxBufferSize = MAX_BUFFER_SIZE)
			: base(maxBufferSize)
		{
			AcceptHandler = new AsyncCallback(AcceptCallback);
			Targets = new Dictionary<string, Socket>();
		}

		/// <summary>
		/// Gets a value indicating whether this instance is server role.
		/// </summary>
		/// <value><c>true</c> if this instance is server role; otherwise, <c>false</c>.</value>
		protected override bool IsServerRole
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Gets or sets the targets.
		/// </summary>
		/// <value>The targets.</value>
		public Dictionary<string, Socket> Targets
		{
			get;
			set;
		}

		private AsyncCallback AcceptHandler
		{
			get;
			set;
		}

		/// <summary>
		/// Initializes current socket.
		/// </summary>
		protected override void Initialize()
		{
			if (CurrentSocket != null)
			{
				CurrentSocket.Dispose();
				CurrentSocket = null;
			}

			try
			{
				CurrentSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				CurrentSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
				var endPoint = new IPEndPoint(IPAddress.Any, Port);

				CurrentSocket.Bind(endPoint);
				CurrentSocket.Listen(10);
				BeginAccept();
			}
			catch (SocketException se)
			{
				SocketExceptionHandler("Initialize socket was fail: ", se, CurrentSocket);
			}
			catch (Exception ex)
			{
				ExceptionHandler("Initialize unknown exception: ", ex);
			}
		}

		/// <summary>
		/// Accept client with Begins the .
		/// </summary>
		private void BeginAccept()
		{
			if (CurrentSocket == null)
			{
				Initialize();

				return;
			}

			try
			{
				CurrentSocket.BeginAccept(AcceptHandler, null);
			}
			catch (SocketException se)
			{
				SocketExceptionHandler("BeginAccept was fail: ", se, CurrentSocket);
			}
			catch (Exception ex)
			{
				ExceptionHandler("BeginAccept unknown exception: ", ex);
			}
		}

		/// <summary>
		/// Accepts the asyncCallback.
		/// </summary>
		/// <param name="ar">The async result data.</param>
		private void AcceptCallback(IAsyncResult ar)
		{
			if (CurrentSocket == null)
			{
				return;
			}

			AsyncObject ao = new AsyncObject(MaxBufferSize);

			try
			{
				TargetSocket = CurrentSocket.EndAccept(ar);
				RemoteId = GetRemoteId(TargetSocket);
				ao.Socket = TargetSocket;

				if (Targets.ContainsKey(RemoteId) == false)
				{
					Targets.Add(RemoteId, TargetSocket);
				}

				OnLogging(string.Format("Target {0} connected.", RemoteId));
				OnConnected(RemoteId, TargetSocket);

				BeginReceive(ao);
			}
			catch (SocketException se)
			{
				SocketExceptionHandler("AcceptCallback with target was fail: ", se, ao.Socket);
			}
			catch (ObjectDisposedException)
			{
			}
			catch (Exception ex)
			{
				ExceptionHandler("AcceptCallback unknown exception: ", ex);
			}
			finally
			{
				BeginAccept();
			}
		}

		/// <summary>
		/// Called when [disconnected].
		/// </summary>
		/// <param name="remoteId">The remote identifier.</param>
		/// <param name="socketException">The socket exception.</param>
		protected override void OnDisconnected(string remoteId, SocketException socketException)
		{
			if (Targets.ContainsKey(remoteId) == true)
			{
				Targets.Remove(remoteId);
			}

			base.OnDisconnected(remoteId, socketException);
		}

		/// <summary>
		/// Sends the data to connected target.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <returns>If target was not connected, then return false.</returns>
		public override bool Send(byte[] data)
		{
			if (Targets.Count == 0)
			{
				OnLogging("Connected target not found.");

				return false;
			}

			var result = true;

			foreach (var remoteId in Targets.Keys)
			{
				result &= base.Send(data, Targets[remoteId]);
			}

			return result;
		}

		/// <summary>
		/// Sends the data specified target.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="remoteId">The remote identifier.</param>
		/// <returns>If target was not connected, then return false.</returns>
		public bool Send(byte[] data, string remoteId)
		{
			if (Targets.ContainsKey(remoteId) == true)
			{
				return base.Send(data, Targets[remoteId]);
			}
			else
			{
				OnLogging(string.Format("RemoteId:{0} was not connected.", remoteId));

				return false;
			}
		}

		/// <summary>
		/// Stops this instance.
		/// </summary>
		public override void Stop()
		{
			base.Stop();

			var se = new SocketException((int)SocketError.ConnectionAborted);

			Targets.Keys.ToList().ForEach(remoteId =>
			{
				var socket = Targets[remoteId];

				socket.Close();
				socket = null;

				OnDisconnected(remoteId, se);
				Targets.Remove(remoteId);
			});

			Targets.Clear();
		}
	}
}
