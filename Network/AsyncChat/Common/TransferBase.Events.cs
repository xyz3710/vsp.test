﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	/// <summary>
	/// TransferBase partial class for Events.
	/// </summary>
	partial class TransferBase
	{
		#region Logging Event
		/// <summary>
		/// Occurs when [logging].
		/// </summary>
		public event EventHandler<LoggingEventArgs> Logging;

		/// <summary>
		/// Called when [logging].
		/// </summary>
		/// <param name="message">The message.</param>
		protected virtual void OnLogging(string message)
		{
			if (Logging != null)
			{
				Logging(this, new LoggingEventArgs
				{
					Message = message,
				});
			}
		}

		/// <summary>
		/// Logging event's data
		/// </summary>
		[System.Diagnostics.DebuggerStepThrough]
		public class LoggingEventArgs : EventArgs
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="LoggingEventArgs"/> class.
			/// </summary>
			public LoggingEventArgs()
			{
			}

			/// <summary>
			/// Message를 구하거나 설정합니다.
			/// </summary>
			/// <value>Message를 반환합니다.</value>
			public string Message
			{
				get;
				set;
			}
		}
		#endregion

		#region SocketLogging Event
		/// <summary>
		/// Occurs when [socket logging].
		/// </summary>
		public event EventHandler<SocketLoggingEventArgs> SocketLogging;

		/// <summary>
		/// Called when [socket logging].
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="socketException">The socket exception.</param>
		protected virtual void OnSocketLogging(string message, SocketException socketException)
		{
			if (SocketLogging != null)
			{
				SocketLogging(this, new SocketLoggingEventArgs
				{
					Message = message,
					SocketException = socketException,
				});
			}
		}

		/// <summary>
		/// SocketLogging event's data
		/// </summary>
		[System.Diagnostics.DebuggerStepThrough]
		public class SocketLoggingEventArgs : LoggingEventArgs
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="SocketLoggingEventArgs"/> class.
			/// </summary>
			public SocketLoggingEventArgs()
			{
			}

			/// <summary>
			/// Gets or sets the socket exception.
			/// </summary>
			/// <value>The socket exception.</value>
			public SocketException SocketException
			{
				get;
				set;
			}
		}
		#endregion

		#region ExceptionLogging Event
		/// <summary>
		/// Occurs when [exception logging].
		/// </summary>
		public event EventHandler<ExceptionLoggingEventArgs> ExceptionLogging;

		/// <summary>
		/// Called when [exception logging].
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="exception">The exception.</param>
		protected virtual void OnExceptionLogging(string message, Exception exception)
		{
			if (ExceptionLogging != null)
			{
				ExceptionLogging(this, new ExceptionLoggingEventArgs
				{
					Message = message,
					Exception = exception,
				});
			}
		}

		/// <summary>
		/// ExceptionLogging event's data
		/// </summary>
		[System.Diagnostics.DebuggerStepThrough]
		public class ExceptionLoggingEventArgs : LoggingEventArgs
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="ExceptionLoggingEventArgs"/> class.
			/// </summary>
			public ExceptionLoggingEventArgs()
			{
			}

			/// <summary>
			/// Gets or sets the exception.
			/// </summary>
			/// <value>The exception.</value>
			public Exception Exception
			{
				get;
				set;
			}
		}
		#endregion

		#region Started Event
		/// <summary>
		/// Occurs when [started].
		/// </summary>
		public event EventHandler<EventArgs> Started;

		/// <summary>
		/// Called when [started].
		/// </summary>
		protected virtual void OnStarted()
		{
			IsRunning = true;

			if (Started != null)
			{
				Started(this, EventArgs.Empty);
			}
		}
		#endregion

		#region Stopped Event
		/// <summary>
		/// Occurs when [stopped].
		/// </summary>
		public event EventHandler<EventArgs> Stopped;

		/// <summary>
		/// Called when [stopped].
		/// </summary>
		protected virtual void OnStopped()
		{
			IsRunning = false;
			IsConnected = false;

			if (Stopped != null)
			{
				Stopped(this, EventArgs.Empty);
			}
		}
		#endregion

		#region Connected Event
		/// <summary>
		/// Occurs when [connected].
		/// </summary>
		public event EventHandler<ConnectedEventArgs> Connected;

		/// <summary>
		/// Called when [connected].
		/// </summary>
		/// <param name="remoteId">The remote identifier.</param>
		/// <param name="targetSocket">The target socket.</param>
		protected virtual void OnConnected(string remoteId, Socket targetSocket)
		{
			IsConnected = true;

			if (Connected != null)
			{
				Connected(this, new ConnectedEventArgs
				{
					RemoteId = remoteId,
					TargetSocket = targetSocket,
				});
			}
		}

		/// <summary>
		/// Connected event's data
		/// </summary>
		[System.Diagnostics.DebuggerStepThrough]
		public class ConnectedEventArgs : EventArgs
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="ConnectedEventArgs"/> class.
			/// </summary>
			public ConnectedEventArgs()
			{
			}

			/// <summary>
			/// Gets or sets the remote identifier.
			/// </summary>
			/// <value>The remote identifier.</value>
			public string RemoteId
			{
				get;
				set;
			}

			/// <summary>
			/// Gets or sets the target socket.
			/// </summary>
			/// <value>The target socket.</value>
			public Socket TargetSocket
			{
				get;
				set;
			}
		}
		#endregion

		#region Disconnected Event
		/// <summary>
		/// Occurs when [disconnected].
		/// </summary>
		public event EventHandler<DisconnectedEventArgs> Disconnected;

		/// <summary>
		/// Called when [disconnected].
		/// </summary>
		/// <param name="remoteId">The remote identifier.</param>
		/// <param name="socketException">The socket exception.</param>
		protected virtual void OnDisconnected(string remoteId, SocketException socketException)
		{
			IsConnected = false;

			if (Disconnected != null)
			{
				Disconnected(this, new DisconnectedEventArgs
				{
					RemoteId = remoteId,
					SocketException = socketException,
				});
			}
		}

		/// <summary>
		/// Disconnected event's data
		/// </summary>
		[System.Diagnostics.DebuggerStepThrough]
		public class DisconnectedEventArgs : EventArgs
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="DisconnectedEventArgs"/> class.
			/// </summary>
			public DisconnectedEventArgs()
			{
			}

			/// <summary>
			/// Gets or sets the remote identifier.
			/// </summary>
			/// <value>The remote identifier.</value>
			public string RemoteId
			{
				get;
				set;
			}

			/// <summary>
			/// Gets or sets the socket exception.
			/// </summary>
			/// <value>The socket exception.</value>
			public SocketException SocketException
			{
				get;
				set;
			}

			/// <summary>
			/// Gets the socket error code.
			/// </summary>
			/// <value>The socket error.</value>
			public SocketError SocketErrorCode
			{
				get
				{
					return SocketException.SocketErrorCode;
				}
			}
		}
		#endregion

		#region Received Event
		/// <summary>
		/// Occurs when [received].
		/// </summary>
		public event EventHandler<TransferEventArgs> Received;

		/// <summary>
		/// Called when [received].
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="transferedLength">Length of the transfered.</param>
		/// <param name="remoteId">The remote identifier.</param>
		protected virtual void OnReceived(byte[] data, int transferedLength, string remoteId)
		{
			if (Received != null)
			{
				Received(this, new TransferEventArgs
				{
					Data = data,
					TransferedLength = transferedLength,
					RemoteId = remoteId,
				});
			}
		}
		#endregion

		#region Sent Event
		/// <summary>
		/// Occurs when [sent].
		/// </summary>
		public event EventHandler<TransferEventArgs> Sent;

		/// <summary>
		/// Called when [sent].
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="transferedLength">Length of the transfered.</param>
		/// <param name="remoteId">The remote identifier.</param>
		protected virtual void OnSent(byte[] data, int transferedLength, string remoteId)
		{
			if (Sent != null)
			{
				Sent(this, new TransferEventArgs
				{
					Data = data,
					TransferedLength = transferedLength,
					RemoteId = remoteId,
				});
			}
		}
		#endregion

		/// <summary>
		/// Transfer event's data
		/// </summary>
		[System.Diagnostics.DebuggerStepThrough]
		public class TransferEventArgs : EventArgs
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="TransferEventArgs"/> class.
			/// </summary>
			public TransferEventArgs()
			{
			}

			/// <summary>
			/// Initializes a new instance of the TransferEventArgs class.
			/// </summary>
			/// <param name="data">The data.</param>
			/// <param name="transferedLength">Length of the transfered.</param>
			public TransferEventArgs(byte[] data, int transferedLength)
			{
				Data = data;
				TransferedLength = transferedLength;
			}

			/// <summary>
			/// Gets or sets the data.
			/// </summary>
			/// <value>The data.</value>
			public byte[] Data
			{
				get;
				set;
			}

			/// <summary>
			/// Gets or sets the length of the transfered.
			/// </summary>
			/// <value>The length of the transfered.</value>
			public int TransferedLength
			{
				get;
				set;
			}

			/// <summary>
			/// Gets or sets the remote identifier.
			/// </summary>
			/// <value>The remote identifier.</value>
			public string RemoteId
			{
				get;
				set;
			}
		}

	}
}
