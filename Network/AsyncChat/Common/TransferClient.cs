﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	/// <summary>
	/// Class TransferClient.
	/// </summary>
	public partial class TransferClient : TransferBase
	{
		#region ServerStopped Event
		/// <summary>
		/// Occurs when [server stopped].
		/// </summary>
		public event EventHandler<DisconnectedEventArgs> ServerStopped;

		/// <summary>
		/// Called when [server stopped].
		/// </summary>
		/// <param name="socketException">The socket exception.</param>
		protected void OnServerStopped(SocketException socketException)
		{
			if (ServerStopped != null)
			{
				ServerStopped(this, new DisconnectedEventArgs
				{
					SocketException = socketException,
				});
			}
		}
		#endregion

		/// <summary>
		/// Initializes a new instance of the TransferClient class.
		/// </summary>
		/// <param name="maxBufferSize">Maximum size of the buffer(default 1024).</param>
		public TransferClient(int maxBufferSize = MAX_BUFFER_SIZE)
			: base(maxBufferSize)
		{
			ConnectHandler = new AsyncCallback(ConnectCallback);
		}

		/// <summary>
		/// Gets a value indicating whether this instance is server role.
		/// </summary>
		/// <value><c>true</c> if this instance is server role; otherwise, <c>false</c>.</value>
		protected override bool IsServerRole
		{
			get
			{
				return false;
			}
		}

		private AsyncCallback ConnectHandler
		{
			get;
			set;
		}

		/// <summary>
		/// Initializes current socket.
		/// </summary>
		protected override void Initialize()
		{
			if (CurrentSocket != null)
			{
				CurrentSocket.Close();
				CurrentSocket = null;
			}

			try
			{
				CurrentSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

				BeginConnect();
			}
			catch (SocketException se)
			{
				SocketExceptionHandler("Initialize socket was fail: ", se, CurrentSocket);
			}
			catch (Exception ex)
			{
				ExceptionHandler("Initialize unknown exception: ", ex);
			}
		}

		/// <summary>
		/// Connect client with Begins the .
		/// </summary>
		private void BeginConnect()
		{
			if (CurrentSocket == null)
			{
				Initialize();

				return;
			}

			try
			{
				AsyncObject ao = new AsyncObject(MaxBufferSize)
				{
					Socket = CurrentSocket,
				};

				CurrentSocket.BeginConnect(HostAddress, Port, ConnectHandler, ao);
			}
			catch (SocketException se)
			{
				SocketExceptionHandler("BeginConnect was fail: ", se, CurrentSocket);
			}
			catch (Exception ex)
			{
				ExceptionHandler("BeginConnect unknown exception: ", ex);
			}
		}

		/// <summary>
		/// Connects the asyncCallback.
		/// </summary>
		/// <param name="ar">The async result data.</param>
		private void ConnectCallback(IAsyncResult ar)
		{
			AsyncObject ao = ar.AsyncState as AsyncObject;

			try
			{
				CurrentSocket.EndConnect(ar);
				TargetSocket = ao.Socket;

				RemoteId = GetRemoteId(TargetSocket);
				OnLogging(string.Format("Target {0} connected.", RemoteId));
				OnConnected(RemoteId, TargetSocket);

				BeginReceive(ao);
			}
			catch (SocketException se)
			{
				SocketExceptionHandler("ConnectCallback with target was fail: ", se, ao.Socket);
			}
			catch (Exception ex)
			{
				ExceptionHandler("ConnectCallback unknown exception: ", ex);
			}
		}
	}
}
