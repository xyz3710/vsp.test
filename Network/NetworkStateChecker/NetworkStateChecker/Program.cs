﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace NetworkStateChecker
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("현재 컴퓨터 네트워크 상태:");
			if (NetworkInterface.GetIsNetworkAvailable())
			{
				Console.WriteLine(" Online");
			}
			else
			{
				Console.WriteLine(" Offline");
			}

		}
	}
}


