using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace Test.TcpServer
{
	public partial class FrmTcpServer : Form
	{
		private delegate void AddListBoxItemDelegate(string message);

		private int _port;
		private Encoding _encoding;
		private TcpListener _tcpListener;

		public FrmTcpServer()
		{
			InitializeComponent();
		}

		private void FrmTcpServer_Shown(object sender, EventArgs e)
		{
			InitMessages();

			_port = 2002;
			_encoding = Encoding.Default;
		}

		private void InitMessages()
		{
			lstMessage.Items.Clear();
		}

		private void AddListBoxItem(string message)
		{
			if (InvokeRequired == true)
			{
				AddListBoxItemDelegate addListBoxItemDelegate = new AddListBoxItemDelegate(AddListBoxItem);

				Invoke(addListBoxItemDelegate, message);
			}
			else
			{
				lstMessage.Items.Add(message);
				lstMessage.Update();
			}
		}

		private void btnStartServer_Click(object sender, EventArgs e)
		{
			try
			{
				IPAddress iPAddress = IPAddress.Parse("127.0.0.1");

				_tcpListener = new TcpListener(iPAddress, _port);
				_tcpListener.Start();

				Thread startServer = new Thread(StartServer);

				startServer.Start();
			}
			catch (Exception ex)
			{
				MessageBox.Show("[에러 발생]" + ex.ToString(), Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void StartServer()
		{
			while (true)
			{
				AddListBoxItem("서버 시작 - 클라이언트의 접속을 기다립니다...");

				// 클라이언트 접속을 기다림
				using (Socket socket = _tcpListener.AcceptSocket())
				{
					if (socket.Connected == true)
					{
						lstMessage.Items.Add("클라이언트 접속됨...");

						byte[] receive = new byte[128];

						// 클라이언트가 보내준 데이터를 받음
						int i = socket.Receive(receive, receive.Length, 0);

						// Default 형식으로 되어있는 byte 배열을 string 형으로 변환
						string receiveString = _encoding.GetString(receive);

						AddListBoxItem(string.Format("[수신] : {0}", receiveString));

						// 현재 시간을 얻어서 string으로 변환
						DateTime now = DateTime.Now;
						string sendString = string.Format("서버의 현재 시간은 {0} {1}", now.ToShortDateString(), now.ToShortTimeString());

						// string 형을 default 형식의 byte 배열로 변환
						byte[] send = _encoding.GetBytes(sendString.ToCharArray());

						// 클라이언트에게 데이터를 보냄
						socket.Send(send, send.Length, 0);
						AddListBoxItem(string.Format("[송신] : {0}", sendString));

						// 클라이언트 접속 종료
						socket.Shutdown(SocketShutdown.Both);
						socket.Close();
					}
				}
			}
		}

		private void FrmTcpServer_FormClosed(object sender, FormClosedEventArgs e)
		{
			if (_tcpListener != null)
				_tcpListener.Stop();
		}

		private void btnServerStop_Click(object sender, EventArgs e)
		{
			if (_tcpListener != null)
				_tcpListener.Stop();
		}
	}
}