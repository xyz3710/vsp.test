﻿namespace Test.TcpServer
{
	partial class FrmTcpServer
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.lstMessage = new System.Windows.Forms.ListBox();
			this.btnStartServer = new System.Windows.Forms.Button();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.btnServerStop = new System.Windows.Forms.Button();
			this.tlpMain.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// lstMessage
			// 
			this.lstMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstMessage.FormattingEnabled = true;
			this.lstMessage.ItemHeight = 12;
			this.lstMessage.Location = new System.Drawing.Point(3, 3);
			this.lstMessage.Name = "lstMessage";
			this.lstMessage.Size = new System.Drawing.Size(286, 232);
			this.lstMessage.TabIndex = 0;
			this.lstMessage.TabStop = false;
			// 
			// btnStartServer
			// 
			this.btnStartServer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnStartServer.Location = new System.Drawing.Point(3, 3);
			this.btnStartServer.Name = "btnStartServer";
			this.btnStartServer.Size = new System.Drawing.Size(140, 22);
			this.btnStartServer.TabIndex = 1;
			this.btnStartServer.Text = "Server 시작";
			this.btnStartServer.UseVisualStyleBackColor = true;
			this.btnStartServer.Click += new System.EventHandler(this.btnStartServer_Click);
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 1;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Controls.Add(this.tableLayoutPanel1, 0, 1);
			this.tlpMain.Controls.Add(this.lstMessage, 0, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 2;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpMain.Size = new System.Drawing.Size(292, 273);
			this.tlpMain.TabIndex = 2;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.btnStartServer, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnServerStop, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 245);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(292, 28);
			this.tableLayoutPanel1.TabIndex = 3;
			// 
			// btnServerStop
			// 
			this.btnServerStop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnServerStop.Location = new System.Drawing.Point(149, 3);
			this.btnServerStop.Name = "btnServerStop";
			this.btnServerStop.Size = new System.Drawing.Size(140, 22);
			this.btnServerStop.TabIndex = 1;
			this.btnServerStop.Text = "Server 종료";
			this.btnServerStop.UseVisualStyleBackColor = true;
			this.btnServerStop.Click += new System.EventHandler(this.btnServerStop_Click);
			// 
			// FrmTcpServer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.tlpMain);
			this.Name = "FrmTcpServer";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Tcp Server";
			this.Shown += new System.EventHandler(this.FrmTcpServer_Shown);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmTcpServer_FormClosed);
			this.tlpMain.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox lstMessage;
		private System.Windows.Forms.Button btnStartServer;
		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Button btnServerStop;
	}
}

