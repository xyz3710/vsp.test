using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;

namespace Test.TcpClient
{
	public partial class FrmTcpClient : Form
	{
		private delegate void AddListBoxItemDelegate(string message);

		private int _port;
		private Encoding _encoding;
		private System.Net.Sockets.TcpClient _tcpClient;
		private NetworkStream _networkStream;

		public FrmTcpClient()
		{
			InitializeComponent();
		}

		private void FrmTcpClient_Shown(object sender, EventArgs e)
		{
			InitMessages();

			_port = 2002;
			_encoding = Encoding.Default;
		}

		private void InitMessages()
		{
			lstMessage.Items.Clear();
		}

		private void AddListBoxItem(string message)
		{
			if (InvokeRequired == true)
			{
				AddListBoxItemDelegate addListBoxItemDelegate = new AddListBoxItemDelegate(AddListBoxItem);

				Invoke(addListBoxItemDelegate, message);
			}
			else
			{
				lstMessage.Items.Add(message);
				lstMessage.Update();
			}
		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			try
			{
				_tcpClient = new System.Net.Sockets.TcpClient("localhost", _port);

				AddListBoxItem("서버에 접속했습니다...");

				// 전송 / 수신을 하기 위해 Network Stream을 얻는다.
				_networkStream = _tcpClient.GetStream();

				Thread connectServer = new Thread(StartClient);

				connectServer.Start();
			}
			catch (SocketException ex)
			{
				MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			catch (Exception ex)
			{
				MessageBox.Show("[에러 발생]" + ex.ToString(), Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void StartClient()
		{
			bool continueLoop = true;

			try
			{
				while (continueLoop == true)
				{
					string sendString = "서버의 시간을 보내주십시오.";
					byte[] bytes = _encoding.GetBytes(sendString.ToCharArray());

					// 서버로 송신
					_networkStream.Write(bytes, 0, bytes.Length);

					// 버퍼에 있는 stream을 flush
					_networkStream.Flush();

					AddListBoxItem(string.Format("[송신] : {0}", sendString));

					byte[] receive = new byte[128];
					// 서버에서 수신
					int i = _networkStream.Read(receive, 0, receive.Length);

					string receiveString = _encoding.GetString(receive);

					AddListBoxItem(string.Format("[수신] : {0}", receiveString));

					_networkStream.Close();
					_tcpClient.Close();
					AddListBoxItem("서버와 접속이 끊어졌습니다.");

					continueLoop = false;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}