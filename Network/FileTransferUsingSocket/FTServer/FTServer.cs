using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace FTServer
{
	public class FTServer
	{
		private const int PORT = 1980;
		private const int BackLog = 100;
		private const int SEGMENT_SIZE = 1024;
		private IPEndPoint _iPEndPoint;
		private Socket _socket;
		private string _receivedPath;
		
		/// <summary>
		/// FTServer class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public FTServer()
		{
			CurrentMessage = "Stopped...";
			_iPEndPoint = new IPEndPoint(IPAddress.Any, PORT);
			_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
			_socket.Bind(_iPEndPoint);
		}

		#region Properties
        /// <summary>
        /// ReceivedPath를 구하거나 설정합니다.
        /// </summary>
        public string ReceivedPath
        {
        	get
        	{
        		return _receivedPath;
        	}
        	set
        	{
        		_receivedPath = value;
        	}
        }
        
        /// <summary>
        /// CurrentMessage를 구하거나 설정합니다.
        /// </summary>
		public string CurrentMessage
        {
        	set
        	{
				Console.WriteLine(value);
        	}
        }
        
        #endregion

		public void StartServer(bool appendable)
		{
			BinaryWriter bw = null;
			Socket clientSocket = null;

			try
			{
				CurrentMessage = "Starting...";
				_socket.Listen(BackLog);

				CurrentMessage = "Running and waiting to receive file.";

				clientSocket = _socket.Accept();

				int headerSize = 8;
				int bufferSize = SEGMENT_SIZE * SEGMENT_SIZE;
				byte[] headerData = new byte[headerSize];

				int receivedLength = clientSocket.Receive(headerData);

				CurrentMessage = "Receiving data...";

				// Header 정보를 읽어 들인다.
				long dataLen = (long)BitConverter.ToInt32(headerData, 0);		// 4byte로 넘어와서 Int32로 변환했지만 file 크기는 long 형이다
				int fileNameLen = BitConverter.ToInt32(headerData, 4);
				byte[] fileNameData = new byte[fileNameLen];

				// File 이름을 읽어 들인다.				
				receivedLength = clientSocket.Receive(fileNameData);

				string fileName = Encoding.Unicode.GetString(fileNameData, 0, fileNameLen);
				string filePath = string.Format(@"{0}/{1}", ReceivedPath, fileName);

				if (appendable == false && File.Exists(filePath) == true)
					File.Delete(filePath);
                	
				Stream stream = File.Open(filePath, FileMode.Append);
				
				bw = new BinaryWriter(stream);

				byte[] clientData = new byte[bufferSize];
				long totalReceived = 0L;

				DateTime startTime = DateTime.Now;
				Console.WriteLine(startTime.ToLongTimeString());

				// 데이터를 읽어들인다.
				while ((receivedLength = clientSocket.Receive(clientData, bufferSize, SocketFlags.Partial)) > 0)
				{
					bw.Write(clientData, 0, receivedLength);

					totalReceived += receivedLength;
					Console.Write("\rTotal / Current : {0:#,##0} / {1:#,##0}", dataLen, totalReceived);
				}

				Console.WriteLine("\r\n{0}", DateTime.Now - startTime);

				CurrentMessage = "Saving file...";
			}
			catch
			{
				CurrentMessage = "File Receiving error.";
			}
			finally
			{
				if (bw != null)
				{
					bw.Flush();
					bw.Close();
				}

				if (clientSocket != null)
					clientSocket.Close();

				CurrentMessage = "Received & Saved file\r\nServer Stopped.";
			}
		}
	}
}
