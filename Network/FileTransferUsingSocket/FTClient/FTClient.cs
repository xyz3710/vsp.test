using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace FTClient
{
	public class FTClient
	{
		private const int PORT = 1980;
		private const int SEGMENT_SIZE = 1024;
		
		private string _fileName;
		private IPAddress _serverIPAddress;

		/// <summary>
		/// FTClient class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public FTClient()
		{
			CurrentMessage = "Idle...";

			if (_serverIPAddress == null)
			{
				IPAddress[] ipAddress = Dns.GetHostAddresses(Dns.GetHostName());

				if (ipAddress.Length > 0)
					_serverIPAddress = ipAddress[0];
			}
		}

		public FTClient(string ipAddress)
			: this ()
		{
			_serverIPAddress = IPAddress.Parse(ipAddress);
		}

		#region Properties
		/// <summary>
		/// CurrentMessage를 구하거나 설정합니다.
		/// </summary>
		public string CurrentMessage
		{
			set
			{
				Console.WriteLine(value);				
			}
		}
        
        /// <summary>
        /// FileName를 구하거나 설정합니다.
        /// </summary>
		public string FileName
        {
        	get
        	{
				return _fileName.Replace("\\", "/");
        	}
			private set
        	{
        		_fileName = value;
        	}
        }
        
        #endregion
        
		public void SendFile(string fileName)
		{
			Socket clientSocket = null;
			FileName = fileName;
			FileStream fs = null;
			try
			{
				IPEndPoint ep = new IPEndPoint(_serverIPAddress, PORT);
				
				clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

				string filePath = string.Empty;

				#region For Test
				/*
				while (FileName.IndexOf("/") > -1)
				{
					filePath += Path.GetDirectoryName(FileName);
					FileName = Path.GetFileName(FileName);
				}
				*/
				#endregion

				byte[] fileNameByte = Encoding.Unicode.GetBytes(Path.GetFileName(FileName));

				if (fileNameByte.Length > 850 * SEGMENT_SIZE)
				{
					CurrentMessage = "File size is more than 850kb, please try with small file.";

					return;
				}

				CurrentMessage = "Sending Header...";

				FileInfo fileInfo = new FileInfo(fileName);
				long fileSize = fileInfo.Length;
				//byte[] fileData = File.ReadAllBytes(FileName);
				byte[] headerData = new byte[8 + fileNameByte.Length];
				byte[] fileNameLen = BitConverter.GetBytes(fileNameByte.Length);
				byte[] fileDataLen = BitConverter.GetBytes(fileSize);
				int bufferSize = SEGMENT_SIZE * SEGMENT_SIZE;

				fileDataLen.CopyTo(headerData, 0);		// 첫번째 4byte 데이터 길이
				fileNameLen.CopyTo(headerData, 4);		// 두번째 4byte File 이름 길이				
				fileNameByte.CopyTo(headerData, 8);		// 파일 이름
				//fileData.CopyTo(headerData, 8 + fileNameByte.Length);

				CurrentMessage = "Connection to server...";

				clientSocket.Connect(ep);

				CurrentMessage = "File sending...";

				// 1차 File 길이, 파일 이름 길이, 파일 이름을 전송한다.
				int result = clientSocket.Send(headerData);

				// 2차 파일 데이터를 bufferSize 만큼 전송 한다.
				long totalSendingLen = 0L;
				long totalReadingLen = 0L;
				int readingLen = 0;
				int sendingLen = 0;
				byte[] sendingData = new byte[bufferSize];
				
				fs = File.OpenRead(fileName);

				// 전체 크기 보다 bufferSize가 크면 기본 bufferSize를 줄인다.
				if (fileSize < bufferSize)
					bufferSize = (int)fileSize;

				DateTime startTime = DateTime.Now;
				Console.WriteLine(startTime.ToLongTimeString());

				// 데이터를 읽어들인다.
				while ((readingLen = fs.Read(sendingData, 0, bufferSize)) > 0)
				{
					sendingLen = clientSocket.Send(sendingData, bufferSize, SocketFlags.Partial);
					totalSendingLen += sendingLen;
					totalReadingLen += readingLen;

					Console.Write("\rTotal / Current : {0:#,##0} / {1:#,##0}", fileSize, totalSendingLen);

					// 기본 bufferSize 보다 남은 크기가 크면 기본 bufferSize는 (전체 크기 - 읽은 총합) 이다.
					if (bufferSize > (int)(fileSize - totalReadingLen))
						bufferSize = (int)(fileSize - totalReadingLen);
				}

				Console.WriteLine("\r\n{0}", DateTime.Now - startTime);

				CurrentMessage = "Disconnecting...";
				CurrentMessage = "File transferred";
			}
			catch (SocketException ex)
			{
				CurrentMessage = string.Format("File sending fail. {0}", ex.Message);                	
			}
			finally
			{
				if (fs != null)
					fs.Close();

				if (clientSocket != null)
					clientSocket.Close();
			}
		}
	}
}
