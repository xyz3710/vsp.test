using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace EchoServer
{
	class EchoServer
	{
		static void Main(string[] args)
		{
			int port = 3000;
			TcpListener tcpListener = new TcpListener(IPAddress.Loopback, port);

			tcpListener.Start();

			while (true)
			{
				Console.WriteLine("Echo Server waiting...");

				TcpClient tcpClient = tcpListener.AcceptTcpClient();

				Console.WriteLine("Echo Client connected");

				NetworkStream ns = tcpClient.GetStream();
				StreamReader sr = new StreamReader(ns);
				string msg = sr.ReadLine();

				Console.WriteLine("Client message [{0}]", msg);

				StreamWriter sw= new StreamWriter(ns);
				
				sw.WriteLine(msg);
				sw.Flush();
				Console.WriteLine("[ECHO] : [{0}]", msg);

				Console.WriteLine("Close");

				sw.Close();
				sr.Close();
				ns.Close();
				tcpClient.Close();
			}
		}
	}
}
