using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace UdpEchoClient
{
	class UdpEchoClient
	{
		static void Main(string[] args)
		{
			string msg = "Hello UDP client";
			int port = 7000;
			string host = IPAddress.Loopback.ToString();
            UdpClient udpClient = new UdpClient(port);
			byte[] bytes = Encoding.Default.GetBytes(msg);
			
			Console.WriteLine("Send data");
			udpClient.Send(bytes, bytes.Length, host, 3500);
			Console.WriteLine(msg);

			Console.WriteLine("Receive data");
			IPEndPoint recvEP = new IPEndPoint(IPAddress.Any, 0);
			byte[] recvBytes = udpClient.Receive(ref recvEP);
			string recvMsg = Encoding.Default.GetString(recvBytes);

			Console.WriteLine("[ECHO] : {0}", recvMsg);

			udpClient.Close();
		}
	}
}
