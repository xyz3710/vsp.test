using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace UdpEchoServer
{
	class UdpEchoServer
	{
		static void Main(string[] args)
		{
			IPEndPoint clientEP = new IPEndPoint(IPAddress.Any, 0);
			UdpClient server = new UdpClient(3500);

			while (true)
			{
				try
				{
					Console.WriteLine("Message waiting...");
					byte[] bytes = server.Receive(ref clientEP);

					Console.WriteLine("Client connection Address : {0}, Port : {1}", clientEP.Address, clientEP.Port);

					string recvMsg = Encoding.Default.GetString(bytes);

					Console.WriteLine("[ECHO] : Client message : {0}", recvMsg);

					server.Send(bytes, bytes.Length, clientEP);
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);

					break;
				}
			}
		}
	}
}
