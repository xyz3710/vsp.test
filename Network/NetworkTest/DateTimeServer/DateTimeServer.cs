using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace NetworkTest
{
	class DateTimeServer
	{
		static void Main(string[] args)
		{
			int port = 13;
			TcpListener tcpListener = new TcpListener(IPAddress.Loopback, port);

			tcpListener.Start();

			Console.WriteLine("DateTime Server waiting...");

			while (true)
			{
				TcpClient tcpClient = tcpListener.AcceptTcpClient();

				Console.WriteLine("\t{0}에 client가 접속했습니다.", port);

				NetworkStream networkStream = tcpClient.GetStream();
				DateTime now = DateTime.Now;
				byte[] byteTime = Encoding.Default.GetBytes(now.ToString());

				try
				{
					Console.WriteLine("\tclient에 현재 시간 전송 : {0}", now);

					networkStream.Write(byteTime, 0, byteTime.Length);
					networkStream.Flush();
					networkStream.Close();
					tcpClient.Close();
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());

					break;
				}
			}

			tcpListener.Stop();
		}
	}
}
