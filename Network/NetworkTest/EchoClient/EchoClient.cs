using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;

namespace EchoClient
{
	class EchoClient
	{
		static void Main(string[] args)
		{
			string host = IPAddress.Loopback.ToString();
			int port = 3000;
			TcpClient tcpClient = new TcpClient(host, port);
			NetworkStream ns = tcpClient.GetStream();
			StreamWriter sw= new StreamWriter(ns);
			
			sw.WriteLine("Hey echo server");
			sw.Flush();
			
			StreamReader sr = new StreamReader(ns);
			string msg = sr.ReadLine();

			Console.WriteLine("[ECHO] : {0}", msg);

			sr.Close();
			sw.Close();
			ns.Close();
		}
	}
}
