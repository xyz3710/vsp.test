using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace NetworkTest
{
	class DateTimeClient
	{
		static void Main(string[] args)
		{
			try
			{
				string host = IPAddress.Loopback.ToString();
				int port = 13;

				TcpClient tcpClient = new TcpClient(host, port);
				NetworkStream networkStream = tcpClient.GetStream();
				int packSize = 1024;
				byte[] bytes = new byte[packSize];
				int bytesRead = networkStream.Read(bytes, 0, packSize);

				Console.WriteLine(Encoding.Default.GetString(bytes, 0, packSize));
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
