using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.Net.Sockets;

namespace MessageClient
{
	public delegate void displayMessage( string msg );

	public class ClientForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox msgViewBox;
		private System.Windows.Forms.TextBox sendBox;
		private System.Windows.Forms.ListBox userlistBox;
		private System.ComponentModel.Container components = null;
		
		private string userID, userName;
		//서버에게 처음으로 보내는 메세지(사용자 아이디) 인지, 아닌지
		bool firstTime=true;
		private TcpClient chatClient;
		private byte[] recByte = new byte[1024];
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox usernameBox;
		private System.Windows.Forms.Button connectButton;

		private Encoding Default = Encoding.Default;
		//Constructor
		public ClientForm()
		{
			InitializeComponent();
		}

		public void GetStreamMsg(IAsyncResult ar)
		{
			int byteCount;
			try
			{
				byteCount = (chatClient.GetStream()).EndRead(ar);

				//서버와 접속이 끊김
				if(byteCount <1)
				{
					Disconnect();
					MessageBox.Show("접속끊김!!");
					return;
				}
				//메세지 해석
				BuildText(recByte,0,byteCount);

				//비동기 수신 시작
				if(!firstTime)
				{
					AsyncCallback GetMsgCallback = new AsyncCallback(GetStreamMsg);
					(chatClient.GetStream()).BeginRead(recByte,0,1024,GetMsgCallback,this);
				}
			}
			catch(Exception ed)
			{
				Disconnect();
				MessageBox.Show("Exception Occured :"+ed.ToString());
			}
		}

		public void BuildText(byte[] dataByte, int offset, int count)
		{
			string tmp = Default.GetString( dataByte, offset, count );

			char[] spliters ={'@'};
			//서버로부터 온 첫번째 메세지이면
			if(firstTime)
			{
				//분리자 '@'로 토큰으로 나눔
				string[] tempString = tmp.Split(spliters);

				//사용자 아이디 에러
				if(tempString[0]=="미안합니다")
				{
					object[] temp = {tempString[1]};
					this.Invoke(new displayMessage(DisplayText),temp);
					Disconnect();
				}
				else
				{
					//클라이언트의 GUID
					this.userID = tempString[0];

					for(int i=1;i<tempString.Length;i++)
					{
						//서버에 접속한 사용자 아이디
						object[] temp = {tempString[i]};

						//UI(user interface) 쓰레드가 아닌 Worker 쓰레드이므로
						//Invoke()를 호출해서 listbox를 업데이트 해야 한다
						this.Invoke(new displayMessage(AddUser),temp);
					}
					
					firstTime=false;
					
					AsyncCallback GetMsgCallback = new AsyncCallback(GetStreamMsg);
					(chatClient.GetStream()).BeginRead(recByte,0,1024,GetMsgCallback,this);
				}
				
			}

				//서버와 처음으로 주고 받는 메세지가 아닌경우
				//즉 이벤트내용(접속됨, 접속끊김)이거나 체팅 내용을 담고 있는 메세지 인경우
			else
			{
				
				//자신의 GUID가 있다면 이벤트 내용
				if(tmp.IndexOf(this.userID)>=0)
				{
					string[] tempString = tmp.Split(spliters);
					//새로운 사용자가 접속
					if(tempString[1]=="접속됨")
					{
						object[] temp = {tempString[2]};
						this.Invoke(new displayMessage(AddUser),temp);
					}
						//기존의 사용자 접속 끊음
					else if(tempString[1]=="접속끊김")
					{
						object[] temp = {tempString[2]};
						this.Invoke(new displayMessage(RemoveUser),temp);
					}
				}
				else
				{
					//체팅 내용
					tmp += "\r\n";
					object[] temp = {tmp};
					//DisplayText 
					this.Invoke(new displayMessage(DisplayText),temp);
				}
			}
		}

		private void RemoveUser(string user)
		{
			if(userlistBox.Items.Contains(user))
				userlistBox.Items.Remove(user);

			DisplayText(user+" 채팅방을 떠나셨습니다\r\n");
		}

		private void AddUser(string user)
		{
			if(!userlistBox.Items.Contains(user))
				userlistBox.Items.Add(user);

			if(!firstTime)
				DisplayText(user+" 채팅방에 참여하셨습니다.\r\n");
		}
		
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					Disconnect();
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		
		//Method to send a message to the server
		public void SendMsg(string msg)
		{
			NetworkStream myNts = chatClient.GetStream();
			msg += "\r\n";
			Byte[] byteSend = Default.GetBytes(msg.ToCharArray());
			myNts.Write(byteSend,0,byteSend.Length) ;		
			myNts.Flush() ;
		}

		//Method to Display Text in the TextBox
		public void DisplayText(string msg)
		{
			msgViewBox.AppendText(msg);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.msgViewBox = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.usernameBox = new System.Windows.Forms.TextBox();
			this.connectButton = new System.Windows.Forms.Button();
			this.sendBox = new System.Windows.Forms.TextBox();
			this.userlistBox = new System.Windows.Forms.ListBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// msgViewBox
			// 
			this.msgViewBox.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left);
			this.msgViewBox.Enabled = false;
			this.msgViewBox.Multiline = true;
			this.msgViewBox.Name = "msgViewBox";
			this.msgViewBox.ReadOnly = true;
			this.msgViewBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.msgViewBox.Size = new System.Drawing.Size(456, 278);
			this.msgViewBox.TabIndex = 2;
			this.msgViewBox.Text = "";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.usernameBox,
																					this.connectButton});
			this.groupBox1.Location = new System.Drawing.Point(456, 240);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(160, 72);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "사용자 아이디";
			// 
			// usernameBox
			// 
			this.usernameBox.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.usernameBox.Location = new System.Drawing.Point(8, 16);
			this.usernameBox.Name = "usernameBox";
			this.usernameBox.Size = new System.Drawing.Size(143, 21);
			this.usernameBox.TabIndex = 0;
			this.usernameBox.Text = "";
			// 
			// connectButton
			// 
			this.connectButton.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.connectButton.Location = new System.Drawing.Point(8, 40);
			this.connectButton.Name = "connectButton";
			this.connectButton.Size = new System.Drawing.Size(144, 27);
			this.connectButton.TabIndex = 1;
			this.connectButton.Text = "접속";
			this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
			// 
			// sendBox
			// 
			this.sendBox.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.sendBox.Enabled = false;
			this.sendBox.Location = new System.Drawing.Point(8, 288);
			this.sendBox.Name = "sendBox";
			this.sendBox.Size = new System.Drawing.Size(448, 21);
			this.sendBox.TabIndex = 1;
			this.sendBox.Text = "";
			this.sendBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
			// 
			// userlistBox
			// 
			this.userlistBox.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.userlistBox.ItemHeight = 12;
			this.userlistBox.Location = new System.Drawing.Point(456, 0);
			this.userlistBox.Name = "userlistBox";
			this.userlistBox.Size = new System.Drawing.Size(160, 232);
			this.userlistBox.TabIndex = 3;
			// 
			// ClientForm
			// 
			this.AcceptButton = this.connectButton;
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(616, 315);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.groupBox1,
																		  this.userlistBox,
																		  this.sendBox,
																		  this.msgViewBox});
			this.Name = "ClientForm";
			this.Text = "메신저 클라이언트";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new ClientForm());
		}

		
		private void Disconnect()
		{
			if(chatClient!=null)
			{
				chatClient.Close();
				chatClient=null;
			}
			//Reset the Buttons and Variables
			userlistBox.Items.Clear();
			connectButton.Text="접속";
			usernameBox.Enabled=true;
			sendBox.Enabled=false;
			this.AcceptButton=connectButton;
			firstTime=true;
			userID="";
			usernameBox.Focus();
		}

		private void connectButton_Click(object sender, System.EventArgs e)
		{
			//If user Cliked Connect
			if(connectButton.Text=="접속"&&usernameBox.Text!="")
			{
				try
				{
					//서버에 접속
					chatClient = new TcpClient("localhost",1980);
					DisplayText("서버에 접속됨 ...\r\n");

					//비동기 수신 시작
					AsyncCallback GetMsgCallback = new AsyncCallback(GetStreamMsg);
					(chatClient.GetStream()).BeginRead(recByte,0,1024,GetMsgCallback,null);

					//사용자 아이디 전송
					SendMsg(usernameBox.Text);
					this.userName=usernameBox.Text;

					string tmp = " [" + userName + "]";
					this.Text += tmp;
					usernameBox.Text="";
					connectButton.Text="종료";
					usernameBox.Enabled=false;
					sendBox.Enabled=true;
					this.AcceptButton=null;
					sendBox.Focus();
				}
				catch
				{
					Disconnect();
					MessageBox.Show("서버에 접속할 수 없읍니다...");
				}
			}
			else if(connectButton.Text=="종료")
			{
				Close();
			}	
		}

		private void OnKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if( (e.KeyChar == (int)Keys.Enter ) && sendBox.Text!="" )
			{
				//클라이언트의 체팅 내용 전송
				SendMsg(sendBox.Text);
				sendBox.Text="";

			}
		}
			
	}
}
