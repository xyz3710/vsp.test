using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace MessageServer
{
	public class ServerForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListBox logBox;
		private Thread serverThread;
		private TcpListener serverListener;

		//서버에 접속한 클라이언트의 GUID와 객체를 가지고 있는 해쉬 테이블
		private Hashtable clientTable;
		
		private System.ComponentModel.Container components = null;

		public ServerForm()
		{
			InitializeComponent();
			clientTable = new Hashtable();

			serverThread = new Thread(new ThreadStart(startListen));
			serverThread.Start();
			AddLog("메신저 서버 시작");
			
		}

		public void startListen()
		{
			try
			{
				serverListener = new TcpListener(1980);
				serverListener.Start();
				do
				{
					//새로운 클라이언트가 접속할 때마다 새로운 User 클래스를 만든다
					User newClient = new User(serverListener.AcceptTcpClient());

					//새로 생성된 User 클래스에 델리게이트를 붙임
					newClient.Disconnected+= new DisconnectDelegate(OnDisconnected);
					newClient.Connected+=new ConnectDelegate(this.OnConnected);
					newClient.MessageReceived+=new MessageDelegate(OnMessageReceived);

					newClient.Connect();
				}
				while(true);
			}
			catch (Exception ex)
			{
				serverListener.Stop();
			}
		}

		//User 클래스에서 Connected 이벤트가 발생했을 때 콜백되는 이벤트 핸들러
		public void OnConnected(object sender, EventArgs e)
		{
			//새로 접속한 User 클래스
			User temp = (User)sender;

			//새로 접속한 User 클래스의 GUID와 
			//User 클래스 객체를 해쉬테이블에 추가 
			clientTable.Add(temp.ID,temp);
			User tempClient;
			AddLog("클라이언트 접속됨:"+temp.UserName);
			
			//해쉬 테이블에 있는 모든 User 클래스들(서버에 접속한 클라이언트들)
			//에게 새로운 클라이언트가 접속했음을 알림
			foreach(DictionaryEntry d in clientTable)
			{
				tempClient =(User)d.Value;
				tempClient.SendMsg(tempClient.ID+"@접속됨@"+temp.UserName);
			}
		}

		//User 클래스에서 DisConnected 이벤트가 발생했을 때 콜백되는 이벤트 핸들러
		public void OnDisconnected(object sender, EventArgs e)
		{
			//접속인 끊어진 User 클래스
			User temp =(User)sender;

			//접속이 끊어진 User 클래스가 해쉬 테이블에 있으면
			if(clientTable.ContainsKey(temp.ID))
			{
				AddLog("클라이언트 접속 끊김:"+temp.UserName);
				//ServerForm 클래스가 관리하는 해쉬테이블에서 제거
				clientTable.Remove(temp.ID);
				//UserInfoList 클래스가 관리하는 해쉬테이블에서 제거
				UserInfoList.RemoveClient(temp.UserName,temp.ID);
				User tempClient;
				//접속해 있는 모든 클라이언트들에게 
				//클라이언트 하나가 연결이 끊겼음을 알림
				foreach(DictionaryEntry d in clientTable)
				{
					tempClient =(User)d.Value;
					tempClient.SendMsg(tempClient.ID+"@접속끊김@"+temp.UserName);
				}
			}
		}

		//User 클래스에서 MessageReceived 이벤트가 발생했을 때 콜백되는 이벤트 핸들러
		public void OnMessageReceived(object sender, MessageEventArgs e)
		{
			//메세지를 보낸 User 클래스
			User temp = (User)sender;
			string str = "[ " + temp.UserName + " ]" + " : "+e.Message;
			AddLog(str);
			User tempClient;
			//한 클라이언트가 보낸 메세지를
			//서버에 연결된 모든 클라이언트들에게 전송
			foreach(DictionaryEntry d in clientTable)
			{
				tempClient =(User)d.Value;
				tempClient.SendMsg(str);
			}
		}
		
		public void AddLog(string msg)
		{
			logBox.Items.Add(msg);
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					if(serverListener!=null)
						serverListener.Stop();
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.logBox = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// logBox
			// 
			this.logBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.logBox.ItemHeight = 12;
			this.logBox.Location = new System.Drawing.Point(0, 0);
			this.logBox.Name = "logBox";
			this.logBox.Size = new System.Drawing.Size(471, 424);
			this.logBox.TabIndex = 0;
			// 
			// ServerForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(471, 431);
			this.Controls.Add(this.logBox);
			this.Name = "ServerForm";
			this.Text = "메신저 서버";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.ServerForm_Closing);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new ServerForm());
		}

		private void ServerForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			serverListener.Stop();
		}
	}
}
