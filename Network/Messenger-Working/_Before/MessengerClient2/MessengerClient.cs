﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace MessengerClient
{
	public partial class MessengerClient : Form
	{
		#region Delegates
		private delegate void DisplayMessageOtherThread(string message);
		#endregion

		#region Fields
		private int _port;
		private TcpClient _tcpClient;
		private IPAddress _ipAddress;
		private Encoding _encoding;
		private int _packSize;
		#endregion

		#region Constructors
		public MessengerClient()
		{
			InitializeComponent();

			string serverIPAddress = GetLocalIp();

			txtServerIp.Text = serverIPAddress;

			_port = 1980;
			_ipAddress = IPAddress.Parse(serverIPAddress);

			_encoding = Encoding.Default;
			_packSize = 1024;

			// test 화면 위치 조정
			SuspendLayout();

			Rectangle workingArea = Screen.GetWorkingArea(this);
			Location = new Point(workingArea.Width - Size.Width, workingArea.Height - Size.Height);

			ResumeLayout();
		}

		private string GetLocalIp()
		{
			// Get local ip address
			IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());

			return ipEntry.AddressList[0].ToString();
		}
		#endregion

		private void MessengerClient_Shown(object sender, EventArgs e)
		{
			// Get local ip address
			IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());

			ipEntry.AddressList[0].ToString();
		}
		
		private void btnSend_Click(object sender, EventArgs e)
		{
			string message = txtMessage.Text;

			if (message == string.Empty)
				return;

			if (SendMessage(message) == true)
				DisplayMessage(message);

			txtMessage.Clear();
		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			txtMessage.Select();

			try
			{
				_tcpClient = new TcpClient(_ipAddress.ToString(), _port);
				DisplayMessage("서버에 접속됨");

				// 비동기 방식으로 메세지 수신 시작
				AsyncCallback getMessageCallback = new AsyncCallback(GetAsyncStreamMessage);
				byte[] receiveBytes = new byte[_packSize];

				_tcpClient.GetStream().BeginRead(receiveBytes, 0, _packSize, getMessageCallback, receiveBytes);

				string userId = "xyz37";
				// TODO: Client에 선언되어 있는 ClientConnectDelimeter를 참조하도록 한다.
				// by MOBILE\xyz37 in 2008년 1월 26일 토요일 오후 8:53
				string clientInfo = string.Format("{0}{1}{2}", userId, "@", GetHashCode());
				// 접속 후 접속한 Client 정보를 전송한다.
				SendMessage(clientInfo);
			}
			catch (Exception ex)
			{
				DisplayMessage(ex.Message);
			}
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			try
			{
				CloseConnection();
			}
			catch (Exception ex)
			{
				DisplayMessage(ex.Message);
			}
		}

		private bool SendMessage(string message)
		{
			bool result = false;

			try
			{
				if (_tcpClient != null && _tcpClient.Connected == true)
				{
					NetworkStream ns = _tcpClient.GetStream();
					byte[] sendBytes = _encoding.GetBytes(message.ToCharArray());

					ns.Write(sendBytes, 0, sendBytes.Length);
					ns.Flush();

					result = true;
				}
				else
					DisplayMessage("연결되지 않았습니다.");
			}
			catch (Exception ex)
			{
				DisplayMessage(ex.Message);
			}

			return result;
		}

		private void GetAsyncStreamMessage(IAsyncResult ar)
		{
			long inCount;

			try
			{
				if (_tcpClient == null || _tcpClient.Connected == false)
					return;

				// Thread간에 충돌 나지 않게 Stream Lock
				lock (_tcpClient.GetStream())
				{
					// Stream 수신이 완료되면 수신된 크기를 intCount에 할당
					inCount = _tcpClient.GetStream().EndRead(ar);
				}

				if (inCount < 1)
				{
					// Server 와 접속이 끊긴 상태
					CloseConnection();
				}
				else
				{
					byte[] beforeReceiveBytes = (byte[])ar.AsyncState;
					string message = _encoding.GetString(beforeReceiveBytes, 0, beforeReceiveBytes.Length);

					DisplayMessage(message);

					AsyncCallback getMessageCallback = new AsyncCallback(GetAsyncStreamMessage);
					byte[] receiveBytes = new byte[_packSize];

					_tcpClient.GetStream().BeginRead(receiveBytes, 0, _packSize, getMessageCallback, receiveBytes);
				}
			}
			catch (SocketException ex)
			{
				if (ex.SocketErrorCode == SocketError.ConnectionReset)
					DisplayMessage("서버와 연결이 끊어졌습니다.");
			}
			catch (Exception ex)
			{
				DisplayMessage(ex.Message);
			}
		}

		private void DisplayMessage(string message)
		{
			if (InvokeRequired == true)
			{
				DisplayMessageOtherThread displayMessageOtherThread = new DisplayMessageOtherThread(DisplayMessage);

				Invoke(displayMessageOtherThread, message);
			}
			else
				lstMessage.Items.Add(message);
		}

		private void CloseConnection()
		{
			if (_tcpClient != null && _tcpClient.Connected == true)
               	_tcpClient.Close();
		}
	}
}