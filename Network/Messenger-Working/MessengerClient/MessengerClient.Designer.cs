﻿namespace MessengerClient
{
	partial class MessengerClient
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnSendMessage = new System.Windows.Forms.Button();
			this.lstMessage = new System.Windows.Forms.ListBox();
			this.txtMessage = new System.Windows.Forms.TextBox();
			this.btnConnect = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.txtServerIp = new System.Windows.Forms.TextBox();
			this.tlpClientMain = new System.Windows.Forms.TableLayoutPanel();
			this.tlpServerIp = new System.Windows.Forms.TableLayoutPanel();
			this.tlpMessage = new System.Windows.Forms.TableLayoutPanel();
			this.tlpClientMain.SuspendLayout();
			this.tlpServerIp.SuspendLayout();
			this.tlpMessage.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnSendMessage
			// 
			this.btnSendMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnSendMessage.Location = new System.Drawing.Point(430, 3);
			this.btnSendMessage.Name = "btnSendMessage";
			this.btnSendMessage.Size = new System.Drawing.Size(194, 22);
			this.btnSendMessage.TabIndex = 0;
			this.btnSendMessage.Text = "보내기";
			this.btnSendMessage.UseVisualStyleBackColor = true;
			this.btnSendMessage.Click += new System.EventHandler(this.btnSend_Click);
			// 
			// lstMessage
			// 
			this.lstMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstMessage.FormattingEnabled = true;
			this.lstMessage.ItemHeight = 12;
			this.lstMessage.Location = new System.Drawing.Point(3, 3);
			this.lstMessage.Name = "lstMessage";
			this.lstMessage.Size = new System.Drawing.Size(621, 388);
			this.lstMessage.TabIndex = 1;
			// 
			// txtMessage
			// 
			this.txtMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtMessage.Location = new System.Drawing.Point(3, 3);
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(421, 21);
			this.txtMessage.TabIndex = 2;
			// 
			// btnConnect
			// 
			this.btnConnect.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnConnect.Location = new System.Drawing.Point(430, 3);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(94, 22);
			this.btnConnect.TabIndex = 0;
			this.btnConnect.Text = "접속";
			this.btnConnect.UseVisualStyleBackColor = true;
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// btnClose
			// 
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnClose.Location = new System.Drawing.Point(530, 3);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(94, 22);
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "종료";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// txtServerIp
			// 
			this.txtServerIp.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtServerIp.Location = new System.Drawing.Point(3, 3);
			this.txtServerIp.Name = "txtServerIp";
			this.txtServerIp.Size = new System.Drawing.Size(421, 21);
			this.txtServerIp.TabIndex = 2;
			// 
			// tlpClientMain
			// 
			this.tlpClientMain.ColumnCount = 1;
			this.tlpClientMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpClientMain.Controls.Add(this.lstMessage, 0, 0);
			this.tlpClientMain.Controls.Add(this.tlpServerIp, 0, 1);
			this.tlpClientMain.Controls.Add(this.tlpMessage, 0, 2);
			this.tlpClientMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpClientMain.Location = new System.Drawing.Point(0, 0);
			this.tlpClientMain.Name = "tlpClientMain";
			this.tlpClientMain.RowCount = 3;
			this.tlpClientMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpClientMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpClientMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpClientMain.Size = new System.Drawing.Size(627, 453);
			this.tlpClientMain.TabIndex = 3;
			// 
			// tlpServerIp
			// 
			this.tlpServerIp.ColumnCount = 3;
			this.tlpServerIp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpServerIp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tlpServerIp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tlpServerIp.Controls.Add(this.btnConnect, 1, 0);
			this.tlpServerIp.Controls.Add(this.txtServerIp, 0, 0);
			this.tlpServerIp.Controls.Add(this.btnClose, 2, 0);
			this.tlpServerIp.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpServerIp.Location = new System.Drawing.Point(0, 397);
			this.tlpServerIp.Margin = new System.Windows.Forms.Padding(0);
			this.tlpServerIp.Name = "tlpServerIp";
			this.tlpServerIp.RowCount = 1;
			this.tlpServerIp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpServerIp.Size = new System.Drawing.Size(627, 28);
			this.tlpServerIp.TabIndex = 2;
			// 
			// tlpMessage
			// 
			this.tlpMessage.ColumnCount = 2;
			this.tlpMessage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMessage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
			this.tlpMessage.Controls.Add(this.btnSendMessage, 1, 0);
			this.tlpMessage.Controls.Add(this.txtMessage, 0, 0);
			this.tlpMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMessage.Location = new System.Drawing.Point(0, 425);
			this.tlpMessage.Margin = new System.Windows.Forms.Padding(0);
			this.tlpMessage.Name = "tlpMessage";
			this.tlpMessage.RowCount = 1;
			this.tlpMessage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMessage.Size = new System.Drawing.Size(627, 28);
			this.tlpMessage.TabIndex = 3;
			// 
			// MessengerClient
			// 
			this.AcceptButton = this.btnSendMessage;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(627, 453);
			this.Controls.Add(this.tlpClientMain);
			this.Name = "MessengerClient";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Client";
			this.Shown += new System.EventHandler(this.MessengerClient_Shown);
			this.tlpClientMain.ResumeLayout(false);
			this.tlpServerIp.ResumeLayout(false);
			this.tlpServerIp.PerformLayout();
			this.tlpMessage.ResumeLayout(false);
			this.tlpMessage.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnSendMessage;
		private System.Windows.Forms.ListBox lstMessage;
		private System.Windows.Forms.TextBox txtMessage;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.TextBox txtServerIp;
		private System.Windows.Forms.TableLayoutPanel tlpClientMain;
		private System.Windows.Forms.TableLayoutPanel tlpServerIp;
		private System.Windows.Forms.TableLayoutPanel tlpMessage;
	}
}

