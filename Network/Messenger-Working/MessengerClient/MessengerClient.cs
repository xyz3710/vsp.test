﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using MessengerServer;

namespace MessengerClient
{
	public partial class MessengerClient : Form
	{
		#region Delegates
		private delegate void DisplayMessageOtherThread(string message);
		#endregion

		#region Fields
		private Client _client;
		private int _port;
		private IPAddress _iPAddress;
		private Encoding _encoding;
		#endregion

		public IPAddress IPAddress
		{
			get
			{
				_iPAddress = IPAddress.Parse(txtServerIp.Text);
				return _iPAddress;
			}
			set
			{
				_iPAddress = value;
				txtServerIp.Text = _iPAddress.ToString();
			}
		}

		#region Constructors
		public MessengerClient()
		{
			InitializeComponent();

			string serverIPAddress = GetLocalIp();

			txtServerIp.Text = serverIPAddress;

			_port = 1980;
			IPAddress = IPAddress.Parse(serverIPAddress);

			_encoding = Encoding.Default;

			// test 화면 위치 조정
			SuspendLayout();

			Rectangle workingArea = Screen.GetWorkingArea(this);
			Location = new Point(workingArea.Width - Size.Width, workingArea.Height - Size.Height);

			ResumeLayout();
		}

		private string GetLocalIp()
		{
			// Get local ip address
			IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());

			return ipEntry.AddressList[0].ToString();
		}
		#endregion

		private void MessengerClient_Shown(object sender, EventArgs e)
		{
			// Get local ip address
			IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());

			ipEntry.AddressList[0].ToString();
		}
		
		private void btnSend_Click(object sender, EventArgs e)
		{
			string message = txtMessage.Text;

			if (message == string.Empty)
				return;

			if (_client != null && _client.SendMessage(message) == true)
				DisplayMessage(message);

			txtMessage.Clear();
		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			txtMessage.Select();

			try
			{
				_client = new Client();

				_client.Connected += new Client.ConnectedHander(_client_Connected);
				_client.Disconnected += new Client.DisconnectedHandler(_client_Disconnected);
				_client.MessageReceived += new Client.MessageReceivedHandler(_client_MessageReceived);
				_client.Error += new Client.ErrorHandler(_client_Error);

				_client.Connect(IPAddress, _port);
				string userId = "xyz37";
				// TODO: Client에 선언되어 있는 ClientConnectDelimeter를 참조하도록 한다.
				// by MOBILE\xyz37 in 2008년 1월 26일 토요일 오후 8:53
				string clientInfo = string.Format("{0}{1}{2}", userId, "@", GetHashCode());
				// 접속 후 접속한 Client 정보를 전송한다.
				_client.LoginByUser(userId);
			}
			catch (Exception ex)
			{
				DisplayMessage(ex.Message);
			}
		}

		private void _client_Connected(object sender, MessengerServer.UserEventArgs.ConnectEventArgs e)
		{
			DisplayMessage("서버에 접속됨");
		}

		private void _client_Disconnected(object sender, MessengerServer.UserEventArgs.ConnectEventArgs e)
		{
			DisplayMessage("서버와 접속이 종료되었습니다.");
		}

		private void _client_MessageReceived(object sender, MessengerServer.UserEventArgs.MessageEventArgs e)
		{
			DisplayMessage(e.Message);
		}

		private void _client_Error(MessengerServer.UserEventArgs.ClientErrorEventArgs e)
		{
			DisplayMessage(e.Message);
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			_client.Disconnect();
		}

		private void DisplayMessage(string message)
		{
			if (InvokeRequired == true)
			{
				DisplayMessageOtherThread displayMessageOtherThread = new DisplayMessageOtherThread(DisplayMessage);

				Invoke(displayMessageOtherThread, message);
			}
			else
				lstMessage.Items.Add(message);
		}

		private void CloseConnection()
		{
			_client.Disconnect();
		}
	}
}