/**********************************************************************************************************************/
/*	Domain		:	MessengerServer.Client.Clients
/*	Creator		:	MOBILE\xyz37
/*	Create		:	2008년 1월 23일 수요일 오전 1:20
/*	Purpose		:	접속된 Client 정보를 관리합니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerServer
{
	/// <summary>
	/// 접속된 Client 정보를 관리합니다.
	/// </summary>
	public class Clients : Dictionary<Guid, Client>
	{
		#region Delegates
		/// <summary>
		/// Client가 추가될 때 발생하는 이벤트 대리자 입니다.
		/// </summary>
		/// <param name="guid"></param>
		/// <param name="client"></param>
		public delegate void AddClientHandler(Guid guid, Client client);
		/// <summary>
		/// Client가 제거될 때 발생하는 이벤트 대리자 입니다.
		/// </summary>
		/// <param name="guid"></param>
		/// <param name="client"></param>
		public delegate void RemoveClientHandler(Guid guid, Client client);
		#endregion

		#region Events
		/// <summary>
		/// Client가 추가될 때 발생하는 이벤트입니다.
		/// </summary>
		public event AddClientHandler AddClient;
		/// <summary>
		/// Client가 제거될 때 발생하는 이벤트입니다.
		/// </summary>
		public event RemoveClientHandler RemoveClient;
		#endregion

		/// <summary>
		/// UserId로 Client 정보를 구합니다.
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public Client this[string userId]
		{
			get
			{
				foreach (Client client in Values)
				{
					if (client.UserId == userId)
						return client;
				}
				
				return null;
			}
		}

		/// <summary>
		/// 지정한 Guid와 Client를 사전에 추가합니다.
		/// </summary>
		/// <param name="guid"></param>
		/// <param name="client"></param>
		public new void Add(Guid guid, Client client)
		{
			if (ContainsKey(guid) == false)
			{
				base.Add(guid, client);
				OnAddClient(guid, client);
			}
		}

		/// <summary>
		/// 지정한 Guid가 있으면 제거 합니다.
		/// </summary>
		/// <param name="guid"></param>
		/// <returns></returns>
		public new bool Remove(Guid guid)
		{
			bool result = false;

			if (ContainsKey(guid) == true)
			{
				result = base.Remove(guid);
				OnRemoveClient(guid, this[guid]);
			}

			return result;
		}

		/// <summary>
		/// Client가 추가될 때 처리하는 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="guid"></param>
		/// <param name="client"></param>
		protected void OnAddClient(Guid guid, Client client)
		{
        	if (AddClient != null)
            	AddClient(guid, client);            
        }

		/// <summary>
		/// Client가 제거될 때 처리하는 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="guid"></param>
		/// <param name="client"></param>
		protected void OnRemoveClient(Guid guid, Client client)
		{
        	if (RemoveClient != null)
            	RemoveClient(guid, client);            
        }
	}
}
