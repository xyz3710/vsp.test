/**********************************************************************************************************************/
/*	Domain		:	MessengerServer.UserEventArgs.MessageEventArgs
/*	Creator		:	MOBILE\xyz37
/*	Create		:	2008년 1월 12일 토요일 오전 1:04
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace MessengerServer.UserEventArgs
{
	/// <summary>
	/// 
	/// </summary>
	public class MessageEventArgs : EventArgs
	{
		#region Fields
		private string _message;
		private Client _client;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the ReceiveMessageEventArgs class.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="client"></param>
		public MessageEventArgs(string message, Client client)
		{
			_message = message;
			_client = client;
		}
		#endregion

		#region Properties
		/// <summary>
		/// 메세지 이벤트 발생시의 메세지를 구하거나 설정합니다.
		/// </summary>
		public string Message
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}

		/// <summary>
		/// 메세지를 발생시킨 Client를 구하거나 설정합니다.
		/// </summary>
		public Client Client
		{
			get
			{
				return _client;
			}
			set
			{
				_client = value;
			}
		}
		#endregion
	}
}
