/**********************************************************************************************************************/
/*	Domain		:	MessengerServer.UserInfo
/*	Creator		:	MOBILE\xyz37
/*	Create		:	2008년 1월 12일 토요일 오전 12:24
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	Client당 접속을 한번만 허용하게 하기 위해 Client의 HashCode를 입력 받는다.
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using MessengerServer.UserEventArgs;
using System.Net.Sockets;
using System.Net;

namespace MessengerServer
{
	/// <summary>
	/// 
	/// </summary>
    public class Client
	{
		#region Delegates
		/// <summary>
		/// Message를 수신할 때 발생하는 이벤트 대리자 입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void MessageReceivedHandler(object sender, MessageEventArgs e);
		/// <summary>
		/// 접속 되었을 때 발생하는 이벤트 대리자 입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void ConnectedHander(object sender, ConnectEventArgs e);
		/// <summary>
		/// 접속이 종료 되었을 때 발생하는 이벤트 대리자 입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void DisconnectedHandler(object sender, ConnectEventArgs e);
		/// <summary>
		/// 에러가 발생할 때 발생하는 이벤트 대리자 입니다.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public delegate void ErrorHandler(ClientErrorEventArgs e);
		/// <summary>
		/// 사용자가 로그인 했을 때 발생하는 이벤트 대리자 입니다.
		/// </summary>
		/// <param name="userId"></param>
		public delegate void LoginHandler(string userId);
		#endregion

		#region Events
		/// <summary>
		/// Message를 수신할 때 발생하는 이벤트 입니다.
		/// </summary>
		public event MessageReceivedHandler MessageReceived;
		/// <summary>
		/// 접속 되었을 때 발생하는 이벤트 입니다.
		/// </summary>
		public event ConnectedHander Connected;
		/// <summary>
		/// 접속이 종료 되었을 때 발생하는 이벤트 입니다.
		/// </summary>
		public event DisconnectedHandler Disconnected;
		/// <summary>
		/// 에러가 발생할 때 발생하는 이벤트 입니다.
		/// </summary>
		public event ErrorHandler Error;
		/// <summary>
		/// 사용자가 로그인 했을 때 발생할 때 발생하는 이벤트 입니다.
		/// </summary>
		public event LoginHandler Login;
		#endregion

		#region Constants
		/// <summary>
		/// Client가 접속할 때 UserId와 HashCode를 구별하는 Delimeter입니다.
		/// </summary>
		public static string ClientConnectDelimeter = "@";
		#endregion

		#region Fields
		private Guid _key;
		private bool _isConnected;
		private TcpClient _tcpClient;
		private Encoding _encoding;
		private int _packSize;
		private string _userId;
		private bool _isLogin;
		private bool _isFirst;
		#endregion

		#region Constructors
		/// <summary>
		/// Client 클래스의 새로운 인스턴스를 초기화 합니다.
		/// </summary>
		public Client()
		{
			_encoding = Encoding.Default;
			_packSize = 1024;
			_isFirst = true;
		}
		#endregion

		#region Properties
		/// <summary>
		/// 접속된 Client의 IP Address를 구합니다.
		/// </summary>
		public string IpAddress
		{
			get
			{
				string ipAddress = IPAddress.None.ToString();

				if (_tcpClient.Connected == true)
					ipAddress = ((IPEndPoint)_tcpClient.Client.RemoteEndPoint).Address.ToString();

				return ipAddress;
			}
		}
		
		/// <summary>
		/// 접속된 Client의 TCP Port를 구합니다.
		/// </summary>
		public int Port
		{
			get
			{
				int port = 0;

				if (_tcpClient.Connected == true)
					port = ((IPEndPoint)_tcpClient.Client.RemoteEndPoint).Port;
				
				return port;
			}
		}

		/// <summary>
		/// 접속된 Key를 구하거나 설정합니다.
		/// </summary>
		public Guid Key
		{
			get
			{
				return _key;
			}
			protected set
			{
				_key = value;
			}
		}

		/// <summary>
		/// 원격 호스트에 연결되어 있는지 여부를 나타내는 값을 가져옵니다.
		/// </summary>
		public bool IsConnected
		{
			get
			{
				return _isConnected;
			}
		}

		/// <summary>
		/// 접속된 UserId를 구합니다.
		/// </summary>
		public string UserId
		{
			get
			{
				return _userId;
			}
		}

		/// <summary>
		/// 원격 호스트에 Id를 가지고 로그인 했는지 여부를 나타내는 값을 구합니다.
		/// </summary>
		public bool IsLogin
		{
			get
			{
				return _isLogin;
			}
			private set
			{
				_isLogin = value;
				_isFirst = !value;
			}
		}
		#endregion

		#region ToString
		public override string ToString()
		{
			return string.Format("{0}, {1}, {2}", Key, IpAddress, Port);
		}
		#endregion

		#region Connect
		/// <summary>
		/// TcpClient가 연결된 뒤 연결을 설정합니다.
		/// </summary>
		/// <param name="iPAddress">Server IPAddress</param>
		/// <param name="port">Server TCP port</param>
		public void Connect(IPAddress iPAddress, int port)
		{
			Connect(new TcpClient(iPAddress.ToString(), port));
		}

		/// <summary>
		/// TcpClient가 연결된 뒤 연결을 설정합니다.
		/// </summary>
		/// <param name="tcpClient">서버와 연결한 TcpClient</param>
		public void Connect(TcpClient tcpClient)
		{
			_tcpClient = tcpClient;

			// 접속한 UserKey 생성
			Key = Guid.NewGuid();

			// 비동기 방식으로 메세지 수신 시작
			AsyncCallback getMessageCallback = new AsyncCallback(GetAsyncStreamMessage);
			byte[] receiveBytes = new byte[_packSize];

			try
			{
				_tcpClient.GetStream().BeginRead(receiveBytes, 0, _packSize, getMessageCallback, receiveBytes);
				_isConnected = true;
				OnConnected(this);
			}
			catch (Exception ex)
			{
				OnError("연결은 되었지만 메세지를 수신할 수 없습니다.\r\n종료 후 다시 시도해 주십시오.", ex);
			}
		}
		#endregion
		
		#region Disconnect
		/// <summary>
		/// Client를 Disconnect 시킵니다.
		/// </summary>
		public void Disconnect()
		{
			_isConnected = false;
			IsLogin = false;

			try
			{
				if (_tcpClient != null && _tcpClient.Connected == true)
					_tcpClient.Close();
			}
			catch (Exception ex)
			{
				OnError("Disconnect 도중 에러가 발생하였습니다.", ex);
			}
		}
		#endregion

		#region Login
		/// <summary>
		/// 접속한 뒤 사용자 계정으로 로그인 합니다.
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public bool LoginByUser(string userId)
		{
			bool result = false;

			if (_isFirst == true && SendMessage(userId) == true)	// Login한 사용자 계정을 보낸다.
			{
				IsLogin = true;
				result = true;
				_userId = userId;
				OnLogin(userId);
			}

			return result;
		}
		#endregion

		#region SendMessage
		/// <summary>
		/// 현재 TcpClient로 메세지를 전송합니다.
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		public bool SendMessage(string message)
		{
			if (_isConnected == false)
				return false;
            	
			bool result = false;

			try
			{
				if (_tcpClient != null && _tcpClient.Connected == true)
				{
					NetworkStream ns = _tcpClient.GetStream();
					byte[] sendBytes = _encoding.GetBytes(message.ToCharArray());

					ns.Write(sendBytes, 0, sendBytes.Length);
					ns.Flush();

					result = true;
				}
			}
			catch (Exception ex)
			{
				OnError("메세지 전송 도중 에러가 발생하여 메세지를 전송할 수 없습니다.", ex);
			}

			return result;
		}
		#endregion

		#region Private methods
		private void GetAsyncStreamMessage(IAsyncResult ar)
		{
			if (_isConnected == false)
				return;

			long inCount;

			try
			{
				// Thread간에 충돌 나지 않게 Stream Lock
				lock (_tcpClient.GetStream())
				{
					// Stream 수신이 완료되면 수신된 크기를 intCount에 할당
					inCount = _tcpClient.GetStream().EndRead(ar);
				}

				if (inCount < 1)
				{
					// server와 접속이 끊긴 상태
					_tcpClient.Close();

					OnDisconnected(this);
				}
				else
				{
					byte[] beforeReceiveBytes = (byte[])ar.AsyncState;
					string message = _encoding.GetString(beforeReceiveBytes, 0, beforeReceiveBytes.Length);

					OnMessageReceived(message, this);

					AsyncCallback getMessageCallback = new AsyncCallback(GetAsyncStreamMessage);
					byte[] receiveBytes = new byte[_packSize];

					_tcpClient.GetStream().BeginRead(receiveBytes, 0, _packSize, getMessageCallback, receiveBytes);
				}
			}
			catch (Exception ex)
			{
				bool errorHandled = false;

				if (ex.InnerException != null)
				{
					SocketException sEx = ex.InnerException as SocketException;

					if (sEx != null)
					{
						errorHandled = true;
						OnError(sEx.Message, ex);
						#region For Test
						/*
						if (sEx.SocketErrorCode == SocketError.ConnectionReset)
						{
							OnError("", ex);
							ConnectionClosed();		// 현재 연결은 원격 호스트에 의해 강제로 끊겼습니다.
						}
						*/
						#endregion
					}
				}

				if (errorHandled == false)
					OnError("메세지 수신시 오류가 발생하였습니다.", ex);
					
				ConnectionClosed();
			}
		}

		private void ConnectionClosed()
		{
			try
			{
				if (_tcpClient != null && _tcpClient.Connected == true)
					_tcpClient.Close();

				OnDisconnected(this);
			}
			catch (Exception ex)
			{
				OnError("Disconnect 도중 에러가 발생하였습니다.", ex);
			}
		}
		#endregion

		#region Protected methods
		/// <summary>
		/// Message를 수신할 때 발생하는 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="client"></param>
		protected void OnMessageReceived(string message, Client client)
		{
			if (MessageReceived != null)
				MessageReceived(this, new MessageEventArgs(message, client));
		}

		/// <summary>
		/// 접속 되었을 때 발생하는 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="client"></param>
		protected void OnConnected(Client client)
		{
			if (Connected != null)
				Connected(this, new ConnectEventArgs(client));
		}

		/// <summary>
		/// 접속이 종료 되었을 때 발생하는 핸들러 이벤트 입니다.
		/// </summary>
		/// <param name="client"></param>
		protected void OnDisconnected(Client client)
		{
			if (Disconnected != null)
				Disconnected(this, new ConnectEventArgs(client));
		}

		/// <summary>
		/// 에러가 발생할 때 발생하는 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="exception"></param>
		protected void OnError(string message, Exception exception)
		{
			if (Error != null)
				Error(new ClientErrorEventArgs(message, exception));
		}

		/// <summary>
		/// 사용자가 로그인할 때 발생하는 이벤트 핸들러 입니다.
		/// </summary>
		/// <param name="userId"></param>
		protected void OnLogin(string userId)
		{
			if (Login != null)
				Login(userId);
		}
		#endregion	
	}
}
