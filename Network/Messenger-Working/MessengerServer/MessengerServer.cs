using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MessengerServer;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;
using MessengerServer.UserEventArgs;

namespace Messenger
{
	/// <summary>
	/// Messenger Server 클래스 입니다.
	/// </summary>
	public partial class MessengerServer : Form
	{
		#region Delegates
		private delegate void DisplayMessageOtherThread(string message);
		private delegate void AddOrRemoveClientOtherThread(Client client);
		#endregion

		#region Fields
		private TcpListener _tcpListener;
		private int _port;
		private IPAddress _ipAddress;
		private Clients _clients;
		#endregion

		/// <summary>
		/// MessengerServer 클래스 인스턴스를 생성합니다.
		/// </summary>
        public MessengerServer()
		{
			InitializeComponent();

			_port = 1980;
			_ipAddress = IPAddress.Any;
			_clients = new Clients();

			_clients.AddClient += new Clients.AddClientHandler(Clients_AddClient);
			_clients.RemoveClient += new Clients.RemoveClientHandler(Clients_RemoveClient);

			btnStart.Select();
		}

		private void Clients_AddClient(Guid guid, Client client)
		{
			AddOrRemoveClient(client);
		}

		private void Clients_RemoveClient(Guid guid, Client client)
		{
			AddOrRemoveClient(client);
		}

		private void btnStart_Click(object sender, EventArgs e)
		{
			txtMessage.Select();

			Thread startThread = new Thread(new ThreadStart(StartServer));

			startThread.Start();
		}

		private void btnStop_Click(object sender, EventArgs e)
		{
			ConnectionClosd();
			lstMessage.Items.Clear();

			foreach (Client client in _clients.Values)
			{
				client.SendMessage("서버가 종료되었습니다.");
				client.Disconnect();
			}

			lstClients.Items.Clear();
		}

		private void btnSendMessage_Click(object sender, EventArgs e)
		{
			string message = txtMessage.Text;

			MessageBroadCasting(message);
			DisplayMessage(message);

			txtMessage.Clear();
		}

		private void MessengerServer_FormClosing(object sender, FormClosingEventArgs e)
		{
			ConnectionClosd();
		}

		private void StartServer()
		{
			try
			{
				IPEndPoint iPEndPoint = new IPEndPoint(_ipAddress, _port);

				_tcpListener = new TcpListener(iPEndPoint);
				_tcpListener.Start();

				DisplayMessage("서버 시작됨");

				bool startServer = true;

				do
				{
					// 새로운 User가 접속할 때 마다 새로운 User를 생성한다.
					TcpClient tcpClient = _tcpListener.AcceptTcpClient();
					Client client = new Client();

					client.Connected += new Client.ConnectedHander(client_Connected);
					client.Disconnected += new Client.DisconnectedHandler(client_Disconnected);
					client.Login += new Client.LoginHandler(client_Login);
					client.MessageReceived += new Client.MessageReceivedHandler(client_MessageReceived);

					client.Connect(tcpClient);

					// 일단 접속한 User에 대해서 
					_clients.Add(client.Key, client);
				}
				while (startServer == true);
			}
			catch (Exception ex)
			{
				Debug.Print(ex.Message);
				if (_tcpListener != null)
				{
					_tcpListener.Stop();
					DisplayMessage("서버 종료됨");
				}
			}
		}

		private void client_Connected(object sender, ConnectEventArgs e)
		{
			DisplayMessage(string.Format("{0} 접속됨", e.Client.IpAddress));
		}

		private void client_Login(string userId)
		{
			DisplayMessage(string.Format("{0} 사용자가 로그인 했습니다.", userId));
		}

		private void client_Disconnected(object sender, ConnectEventArgs e)
		{
			DisplayMessage(string.Format("{0} 종료됨", e.Client.IpAddress));
		}

		private void client_MessageReceived(object sender, MessageEventArgs e)
		{
			DisplayMessage(string.Format("{0}-> {1}", e.Client, e.Message));
		}

		private void ConnectionClosd()
		{
			if (_tcpListener != null)
				_tcpListener.Stop();
		}

		private void MessageBroadCasting(string message)
		{
			foreach (Client client in _clients.Values)
			{
				client.SendMessage(message);
			}
		}

		#region Cross Thread Handler
		private void DisplayMessage(string message)
		{
			if (InvokeRequired == true)
			{
				DisplayMessageOtherThread displayMessageOtherThread = new DisplayMessageOtherThread(DisplayMessage);

				Invoke(displayMessageOtherThread, message);
			}
			else
				lstMessage.Items.Add(message);
		}

		private void AddOrRemoveClient(Client client)
		{
			if (InvokeRequired == true)
			{
				AddOrRemoveClientOtherThread addOrRemoveClientOtherThread = new AddOrRemoveClientOtherThread(AddOrRemoveClient);

				Invoke(addOrRemoveClientOtherThread, client);
			}
			else
				lstClients.Items.Add(string.Format("{0}{1}{2}", client.UserId, Client.ClientConnectDelimeter, client.Key));
		}
		#endregion
	}
}