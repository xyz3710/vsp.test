﻿namespace Messenger
{
	partial class MessengerServer
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnStart = new System.Windows.Forms.Button();
			this.btnStop = new System.Windows.Forms.Button();
			this.lstMessage = new System.Windows.Forms.ListBox();
			this.txtMessage = new System.Windows.Forms.TextBox();
			this.btnSendMessage = new System.Windows.Forms.Button();
			this.tlpServerMain = new System.Windows.Forms.TableLayoutPanel();
			this.tlpButton = new System.Windows.Forms.TableLayoutPanel();
			this.tlpMessage = new System.Windows.Forms.TableLayoutPanel();
			this.lstClients = new System.Windows.Forms.ListBox();
			this.tlpServerMain.SuspendLayout();
			this.tlpButton.SuspendLayout();
			this.tlpMessage.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnStart
			// 
			this.btnStart.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnStart.Location = new System.Drawing.Point(3, 3);
			this.btnStart.Name = "btnStart";
			this.btnStart.Size = new System.Drawing.Size(310, 22);
			this.btnStart.TabIndex = 0;
			this.btnStart.Text = "시작";
			this.btnStart.UseVisualStyleBackColor = true;
			this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
			// 
			// btnStop
			// 
			this.btnStop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnStop.Location = new System.Drawing.Point(319, 3);
			this.btnStop.Name = "btnStop";
			this.btnStop.Size = new System.Drawing.Size(310, 22);
			this.btnStop.TabIndex = 2;
			this.btnStop.Text = "종료";
			this.btnStop.UseVisualStyleBackColor = true;
			this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
			// 
			// lstMessage
			// 
			this.lstMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstMessage.FormattingEnabled = true;
			this.lstMessage.ItemHeight = 12;
			this.lstMessage.Location = new System.Drawing.Point(3, 3);
			this.lstMessage.Name = "lstMessage";
			this.lstMessage.Size = new System.Drawing.Size(436, 388);
			this.lstMessage.TabIndex = 1;
			// 
			// txtMessage
			// 
			this.txtMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtMessage.Location = new System.Drawing.Point(3, 3);
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(426, 21);
			this.txtMessage.TabIndex = 1;
			// 
			// btnSendMessage
			// 
			this.btnSendMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnSendMessage.Location = new System.Drawing.Point(435, 3);
			this.btnSendMessage.Name = "btnSendMessage";
			this.btnSendMessage.Size = new System.Drawing.Size(194, 22);
			this.btnSendMessage.TabIndex = 3;
			this.btnSendMessage.Text = "보내기";
			this.btnSendMessage.UseVisualStyleBackColor = true;
			this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
			// 
			// tlpServerMain
			// 
			this.tlpServerMain.ColumnCount = 2;
			this.tlpServerMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tlpServerMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
			this.tlpServerMain.Controls.Add(this.lstMessage, 0, 0);
			this.tlpServerMain.Controls.Add(this.tlpButton, 0, 1);
			this.tlpServerMain.Controls.Add(this.tlpMessage, 0, 2);
			this.tlpServerMain.Controls.Add(this.lstClients, 1, 0);
			this.tlpServerMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpServerMain.Location = new System.Drawing.Point(0, 0);
			this.tlpServerMain.Name = "tlpServerMain";
			this.tlpServerMain.RowCount = 3;
			this.tlpServerMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpServerMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpServerMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpServerMain.Size = new System.Drawing.Size(632, 453);
			this.tlpServerMain.TabIndex = 4;
			// 
			// tlpButton
			// 
			this.tlpButton.ColumnCount = 2;
			this.tlpServerMain.SetColumnSpan(this.tlpButton, 2);
			this.tlpButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpButton.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpButton.Controls.Add(this.btnStart, 0, 0);
			this.tlpButton.Controls.Add(this.btnStop, 1, 0);
			this.tlpButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpButton.Location = new System.Drawing.Point(0, 397);
			this.tlpButton.Margin = new System.Windows.Forms.Padding(0);
			this.tlpButton.Name = "tlpButton";
			this.tlpButton.RowCount = 1;
			this.tlpButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpButton.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpButton.Size = new System.Drawing.Size(632, 28);
			this.tlpButton.TabIndex = 2;
			// 
			// tlpMessage
			// 
			this.tlpMessage.ColumnCount = 2;
			this.tlpServerMain.SetColumnSpan(this.tlpMessage, 2);
			this.tlpMessage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMessage.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
			this.tlpMessage.Controls.Add(this.txtMessage, 0, 0);
			this.tlpMessage.Controls.Add(this.btnSendMessage, 1, 0);
			this.tlpMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMessage.Location = new System.Drawing.Point(0, 425);
			this.tlpMessage.Margin = new System.Windows.Forms.Padding(0);
			this.tlpMessage.Name = "tlpMessage";
			this.tlpMessage.RowCount = 1;
			this.tlpMessage.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMessage.Size = new System.Drawing.Size(632, 28);
			this.tlpMessage.TabIndex = 2;
			// 
			// lstClients
			// 
			this.lstClients.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstClients.FormattingEnabled = true;
			this.lstClients.ItemHeight = 12;
			this.lstClients.Location = new System.Drawing.Point(445, 3);
			this.lstClients.Name = "lstClients";
			this.lstClients.Size = new System.Drawing.Size(184, 388);
			this.lstClients.TabIndex = 1;
			// 
			// MessengerServer
			// 
			this.AcceptButton = this.btnSendMessage;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(632, 453);
			this.Controls.Add(this.tlpServerMain);
			this.Name = "MessengerServer";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Server";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MessengerServer_FormClosing);
			this.tlpServerMain.ResumeLayout(false);
			this.tlpButton.ResumeLayout(false);
			this.tlpMessage.ResumeLayout(false);
			this.tlpMessage.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.ListBox lstMessage;
		private System.Windows.Forms.TextBox txtMessage;
		private System.Windows.Forms.Button btnSendMessage;
		private System.Windows.Forms.TableLayoutPanel tlpServerMain;
		private System.Windows.Forms.TableLayoutPanel tlpButton;
		private System.Windows.Forms.TableLayoutPanel tlpMessage;
		private System.Windows.Forms.ListBox lstClients;
	}
}

