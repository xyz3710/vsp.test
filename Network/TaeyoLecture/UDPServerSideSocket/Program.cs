﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ServerSideSocket
{
	class ServerClass
	{
		public static Socket Server, ServerYou;

		public static byte[] getByte = new byte[1024];
		public static byte[] setByte = new byte[1024];

		public const int sPort = 5000;
		public const int sPortyou = 5001;

		[STAThread]
		static void Main(string[] args)
		{
			string stringbyte = null;
			IPAddress serverIP = IPAddress.Parse("127.0.0.1");
			IPAddress serverIPYou = IPAddress.Parse("127.0.0.1");

			IPEndPoint serverEndPoint = new IPEndPoint(serverIP, sPort);
			IPEndPoint serverEndPointYou = new IPEndPoint(serverIPYou, sPortyou);

			try
			{
				Server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
								ProtocolType.Udp);
				ServerYou = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
								ProtocolType.Udp);

				Server.Bind(serverEndPoint);
				ServerYou.Connect(serverEndPointYou);

				Console.WriteLine("-----------------------------------------------------");
				Console.WriteLine(" 클라이언트의 연결을 기다립니다........ ");
				Console.WriteLine("-----------------------------------------------------");

				while (true)
				{
					Server.Receive(getByte, 0, getByte.Length, SocketFlags.None);
					stringbyte = Encoding.UTF7.GetString(getByte);
					Console.WriteLine(Server.Connected);
					if (stringbyte != String.Empty)
					{
						int getValueLength = 0;

						getValueLength = byteArrayDefrag(getByte);
						stringbyte = Encoding.UTF7.GetString(getByte, 0, getValueLength + 1);

						Console.WriteLine("수신데이터:{0} | 길이:{1}",
										stringbyte, getValueLength + 1);
						setByte = Encoding.UTF7.GetBytes(stringbyte);
						ServerYou.Send(setByte, 0, setByte.Length, SocketFlags.None);
						Console.WriteLine("(((((발송했음-" +
										Encoding.UTF7.GetString(setByte) + ")))");
					}
				}
			}

			catch (System.Net.Sockets.SocketException socketEx)
			{
				Console.WriteLine("[Error]:{0}", socketEx.Message);
			}
			catch (System.Exception commonEx)
			{
				Console.WriteLine("[Error]:{0}", commonEx.Message);
			}
			finally
			{
				Server.Close();
				ServerYou.Close();
			}
		}

		public static int byteArrayDefrag(byte[] sData)
		{
			int endLength = 0;
			for (int i = 0; i < sData.Length; i++)
			{
				if ((byte)sData[i] != (byte)0)
				{
					endLength = i;
				}
			}

			return endLength;
		}
	}
}