﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ClientSideSocket
{
	class ClientClass
	{
		public static Socket socket, socketme;
		public static byte[] getbyte = new byte[1024];
		public static byte[] setbyte = new byte[1024];

		public const int sPort = 5000;            // 발신용
		public const int sPortMe = 5001;        // 수신용

		[STAThread]
		static void Main(string[] args)
		{
			string sendstring = null;
			IPAddress serverIP = IPAddress.Parse("127.0.0.1");
			IPAddress ServerIPMe = IPAddress.Parse("127.0.0.1");

			IPEndPoint serverEndPoint = new IPEndPoint(serverIP, sPort);
			IPEndPoint serverEndPointMe = new IPEndPoint(ServerIPMe, sPortMe);

			socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
							ProtocolType.Udp);
			socketme = new Socket(AddressFamily.InterNetwork, SocketType.Dgram,
							ProtocolType.Udp);

			Console.WriteLine("-----------------------------------------------------");
			Console.WriteLine(" 서버로 접속을 시작합니다. [엔터를 입력하세요] ");
			Console.WriteLine("-----------------------------------------------------");
			Console.ReadLine();

			socket.Connect(serverEndPoint);
			socketme.Bind(serverEndPointMe);

			if (socket.Connected)
			{
				Console.WriteLine(">> 정상적으로 연결되었습니다(전송한 데이터를 입력해주세요)");
			}

			while (true)
			{
				Console.Write(">>");
				sendstring = Console.ReadLine();
				sendMessage(sendstring);
			}
		}

		public static int byteArrayDefrag(byte[] sData)
		{
			int endLength = 0;
			for (int i = 0; i < sData.Length; i++)
			{
				if ((byte)sData[i] != (byte)0)
				{
					endLength = i;
				}
			}

			return endLength;
		}

		public static void sendMessage(string sendmsg)
		{
			string getstring = null;

			if (sendmsg != string.Empty)
			{
				int getValueLength = 0;

				setbyte = Encoding.UTF7.GetBytes(sendmsg);
				socket.Send(setbyte, 0, setbyte.Length, SocketFlags.None);

				socketme.Receive(getbyte, 0, getbyte.Length, SocketFlags.None);

				getValueLength = byteArrayDefrag(getbyte);
				getstring = Encoding.UTF7.GetString(getbyte, 0, getValueLength + 1);

				Console.WriteLine("(수신된 데이터 :{0} | 길이{1})", getstring,
							  getValueLength + 1);
			}
			else
			{
				Console.WriteLine("수신된 데이터 없음");
			}
		}
	}
}