﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ServerHighSocket
{
	class Class1
	{
		static byte[] rData = new byte[1024];
		static byte[] sData = new byte[1024];

		[STAThread]
		static void Main(string[] args)
		{
			IPAddress ipAddress = null;

			foreach (IPAddress ipAddr in Dns.GetHostEntry("127.0.0.1").AddressList)
			{
				if (ipAddr.AddressFamily == AddressFamily.InterNetwork)
				{
					if (ipAddr.ToString().StartsWith("169") == false)
					{
						ipAddress = ipAddr;

						break;
					}
				}
			}

			IPEndPoint ipEnd = new IPEndPoint(ipAddress, 6000);

			TcpListener listener = new TcpListener(ipAddress, 6000);
			listener.Start();

			Console.WriteLine("--------------------------------");
			Console.WriteLine(" 클라이언트의 연결을 기다립니다.");
			Console.WriteLine("--------------------------------");

			TcpClient client = listener.AcceptTcpClient();
			NetworkStream ns = client.GetStream();

			while (true)
			{
				int rDataLength = ns.Read(rData, 0, rData.Length);

				if (rDataLength > 0)
				{
					Console.WriteLine("수신데이터>>{0}", Encoding.UTF7.GetString(rData, 0, Class1.byteArrayDefrag(rData) + 1));
					ns.Write(rData, 0, rData.Length);
				}
			}
		}

		public static int byteArrayDefrag(byte[] sData)
		{
			int endLength = 0;
			for (int i = 0; i < sData.Length; i++)
			{
				if ((byte)sData[i] != (byte)0)
				{
					endLength = i;
				}
			}
			return endLength;
		}
	}
}