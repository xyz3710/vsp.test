﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ClientHighSocket
{
	class Class1
	{
		static byte[] rData = new byte[1024];
		static byte[] sData = new byte[1024];

		[STAThread]
		static void Main(string[] args)
		{
			IPAddress ipAddress = null;

			foreach (IPAddress ipAddr in Dns.GetHostEntry("127.0.0.1").AddressList)
			{
				if (ipAddr.AddressFamily == AddressFamily.InterNetwork)
				{
					if (ipAddr.ToString().StartsWith("169") == false)
					{
						ipAddress = ipAddr;

						break;
					}
				}
			}

			TcpClient client = new TcpClient();
			client.Connect(ipAddress, 6000);
			NetworkStream ns = client.GetStream();

			Console.WriteLine("--------------------------");
			Console.WriteLine(" 서버에 접속합니다. ");
			Console.WriteLine("--------------------------");

			while (true)
			{
				if (ns.CanWrite)
				{
					Console.Write("발송할데이터>>>");
					sData = Encoding.UTF7.GetBytes(Console.ReadLine());
					ns.Write(sData, 0, sData.Length);

					int rDataLength = ns.Read(rData, 0, rData.Length);

					if (rDataLength > 0)
					{
						Console.WriteLine("수신데이터>>{0}", Encoding.UTF7.GetString(rData, 0, Class1.byteArrayDefrag(rData) + 1));
					}
				}
			}
		}

		public static int byteArrayDefrag(byte[] sData)
		{
			int endLength = 0;
			for (int i = 0; i < sData.Length; i++)
			{
				if ((byte)sData[i] != (byte)0)
				{
					endLength = i;
				}
			}

			return endLength;
		}
	}
}