﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestClient
{
	public partial class ClientForm : Form
	{
		public ClientForm()
		{
			InitializeComponent();
			AdjustLocation();
		}

		private void AdjustLocation()
		{
			SuspendLayout();

			Rectangle workingArea = Screen.AllScreens[0].WorkingArea;
			Location = new Point(workingArea.X + workingArea.Width - Width, workingArea.Bottom - Height);

			ResumeLayout();
		}
	}
}
