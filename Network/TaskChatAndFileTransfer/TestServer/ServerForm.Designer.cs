﻿namespace TestServer
{
	partial class ServerForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblIsConnected = new System.Windows.Forms.Label();
			this.btnIsConnected = new System.Windows.Forms.Button();
			this.tbResult = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// lblIsConnected
			// 
			this.lblIsConnected.Location = new System.Drawing.Point(787, 57);
			this.lblIsConnected.Name = "lblIsConnected";
			this.lblIsConnected.Size = new System.Drawing.Size(92, 30);
			this.lblIsConnected.TabIndex = 6;
			this.lblIsConnected.Text = "연결 안됨";
			this.lblIsConnected.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btnIsConnected
			// 
			this.btnIsConnected.Location = new System.Drawing.Point(788, 13);
			this.btnIsConnected.Name = "btnIsConnected";
			this.btnIsConnected.Size = new System.Drawing.Size(88, 32);
			this.btnIsConnected.TabIndex = 5;
			this.btnIsConnected.Text = "연결 체크";
			this.btnIsConnected.UseVisualStyleBackColor = true;
			// 
			// tbResult
			// 
			this.tbResult.Location = new System.Drawing.Point(12, 12);
			this.tbResult.Multiline = true;
			this.tbResult.Name = "tbResult";
			this.tbResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbResult.Size = new System.Drawing.Size(769, 479);
			this.tbResult.TabIndex = 4;
			// 
			// ServerForm
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(889, 503);
			this.Controls.Add(this.lblIsConnected);
			this.Controls.Add(this.btnIsConnected);
			this.Controls.Add(this.tbResult);
			this.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "ServerForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Server";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblIsConnected;
		private System.Windows.Forms.Button btnIsConnected;
		private System.Windows.Forms.TextBox tbResult;
	}
}

