﻿// ****************************************************************************************************************** //
//	Domain		:	GS.MessageTransfer.MessageEventArgs
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2014년 12월 6일 토요일 오후 11:59
//	Purpose		:	SendingMessage Event를 위한 Argument입니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="MessageEventArgs.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //


using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace GS.MessageTransfer
{
	/// <summary>
	/// SendingMessage Event를 위한 Argument입니다.
	/// </summary>
	[Serializable]
	[DebuggerDisplay("EventSource:{EventSource}, SendType:{SendType}, Message:{Message}, FromUserId:{FromUserId}, FromIpAddress:{FromIpAddress}, ToIpAddress:{ToIpAddress}, Tag:{Tag}", Name = "MessageEventArgs")]
	public class MessageEventArgs : EventArgs
	{
		#region Constructors
		/// <summary>
		/// MessageEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="sendType">전송한 message type입니다.</param>
		/// <param name="message">전송한 message입니다.</param>
		/// <param name="fromIpAddress">전송한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public MessageEventArgs(SendTypes sendType, string message, string fromIpAddress, string toIpAddress)
			: this(EventSource.Message, sendType, message, fromIpAddress, toIpAddress, String.Empty, null)
		{
		}

		/// <summary>
		/// MessageEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="eventSource">Event가 발생한 Source 입니다.</param>
		/// <param name="sendType">전송한 message type입니다.</param>
		/// <param name="message">전송한 message입니다.</param>
		/// <param name="fromIpAddress">전송한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		public MessageEventArgs(EventSource eventSource, SendTypes sendType, string message, string fromIpAddress, string toIpAddress)
			: this(eventSource, sendType, message, fromIpAddress, toIpAddress, String.Empty, null)
		{
		}

		/// <summary>
		/// MessageEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="sendType">전송한 message type입니다.</param>
		/// <param name="message">전송한 message입니다.</param>
		/// <param name="fromIpAddress">전송한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="fromUserId">메세지를 전달한 사용자 Id 입니다.</param>
		/// <param name="tag">Message 전달시 사용될 Tag입니다.</param>
		protected MessageEventArgs(SendTypes sendType, string message, string fromIpAddress, string toIpAddress, string fromUserId, object tag)
		{
			EventSource = EventSource.Message;
			SendType = sendType;
			Message = message;
			FromIpAddress = fromIpAddress;
			ToIpAddress = toIpAddress;
			FromUserId = fromUserId;
			Tag = tag;
		}

		/// <summary>
		/// MessageEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="eventSource">Event가 발생한 Source 입니다.</param>
		/// <param name="sendType">전송한 message type입니다.</param>
		/// <param name="message">전송한 message입니다.</param>
		/// <param name="fromIpAddress">전송한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="tag">Message 전달시 사용될 Tag입니다.</param>
		public MessageEventArgs(EventSource eventSource, SendTypes sendType, string message, string fromIpAddress, string toIpAddress, object tag)
			: this(eventSource, sendType, message, fromIpAddress, toIpAddress, String.Empty, tag)
		{
		}

		/// <summary>
		/// MessageEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="eventSource">Event가 발생한 Source 입니다.</param>
		/// <param name="sendType">전송한 message type입니다.</param>
		/// <param name="message">전송한 message입니다.</param>
		/// <param name="fromIpAddress">전송한 client의 IpAddress입니다.</param>
		/// <param name="toIpAddress">전송될 client의 IpAddress입니다.</param>
		/// <param name="fromUserId">메세지를 전달한 사용자 Id 입니다.</param>
		/// <param name="tag">Message 전달시 사용될 Tag입니다.</param>
		public MessageEventArgs(EventSource eventSource, SendTypes sendType, string message, string fromIpAddress, string toIpAddress, string fromUserId, object tag)
		{
			EventSource = eventSource;
			SendType = sendType;
			Message = message;
			FromIpAddress = fromIpAddress;
			ToIpAddress = toIpAddress;
			FromUserId = fromUserId;
			Tag = tag;
		}
		#endregion

		#region Properties
		/// <summary>
		/// EventSource를 구하거나 설정합니다.
		/// </summary>
		public EventSource EventSource
		{
			get;
			set;
		}

		/// <summary>
		/// SendType를 구하거나 설정합니다.
		/// </summary>
		public SendTypes SendType
		{
			get;
			set;
		}

		/// <summary>
		/// Message를 구하거나 설정합니다.
		/// </summary>
		public string Message
		{
			get;
			set;
		}

		/// <summary>
		/// UserId를 구하거나 설정합니다.
		/// </summary>
		public string FromUserId
		{
			get;
			set;
		}

		/// <summary>
		/// FromIpAddress를 구하거나 설정합니다.
		/// </summary>
		public string FromIpAddress
		{
			get;
			set;
		}

		/// <summary>
		/// ToIpAddress를 구하거나 설정합니다.
		/// </summary>
		public string ToIpAddress
		{
			get;
			set;
		}

		/// <summary>
		/// Tag를 구하거나 설정합니다.
		/// </summary>
		public object Tag
		{
			get;
			set;
		}
		#endregion

		#region ToString
		/// <summary>
		/// 현재 Object를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return string.Format("EventSource:{0}, SendType:{1}, Message:{2}, FromUserId:{3}, FromIpAddress:{4}, ToIpAddress:{5}, Tag:{6}",
					EventSource, SendType, Message, FromUserId, FromIpAddress, ToIpAddress, Tag);
		}
		#endregion
	}
}
