﻿// ****************************************************************************************************************** //
//	Domain		:	GS.MessageTransfer.ErrorEventArgs
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2014년 12월 6일 토요일 오후 11:59
//	Purpose		:	파일 전송 정보에 대한 EventArgs입니다
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="ErrorEventArgs.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;

namespace GS.MessageTransfer
{
	/// <summary>
	/// 파일 전송 정보에 대한 EventArgs입니다.
	/// </summary>
	[Serializable]
	public class ErrorEventArgs : EventArgs
	{
		#region Constructors
		/// <summary>
		/// ErrorEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="message">에러 발생 이유 입니다.</param>
		/// <param name="exception">Inner Exception 입니다.</param>
		public ErrorEventArgs(string message, Exception exception = null)
		{
			Message = message;
			Exception = exception;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Message를 구하거나 설정합니다.
		/// </summary>
		public string Message
		{
			get;
			set;
		}

		/// <summary>
		/// Exception를 구하거나 설정합니다.
		/// </summary>
		public Exception Exception
		{
			get;
			set;
		}
		#endregion
	}
}
