﻿// ****************************************************************************************************************** //
//	Domain		:	GS.MessageTransfer.ServerInfoEventArgs
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2014년 12월 7일 일요일 오전 12:00
//	Purpose		:	서버 접속 정보에 대한 EventArgs입니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="ServerInfoEventArgs.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;

namespace GS.MessageTransfer
{
	/// <summary>
	/// 서버 접속 정보에 대한 EventArgs입니다.
	/// </summary>
	[Serializable]
	public class ServerInfoEventArgs : EventArgs
	{
		#region Constructors
		/// <summary>
		/// ServerInfoEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="serverIpAddress">서비스 하는 Server의 IpAddress 입니다.</param>
		/// <param name="port">Socket과 Binding되는 Port 번호 입니다.</param>
		/// <param name="listenerCount">서비스를 Listening하는 Thread의 개수 입니다.</param>
		public ServerInfoEventArgs(string serverIpAddress, int port, int listenerCount = 1)
		{
			ServerIpAddress = serverIpAddress;
			Port = port;
			ListenerCount = listenerCount;
		}
		#endregion

		#region Properties
		/// <summary>
		/// ServerIpAddress를 구하거나 설정합니다.
		/// </summary>
		public string ServerIpAddress
		{
			get;
			set;
		}

		/// <summary>
		/// Port를 구하거나 설정합니다.
		/// </summary>
		public int Port
		{
			get;
			set;
		}

		/// <summary>
		/// ListenerCount를 구하거나 설정합니다.
		/// </summary>
		public int ListenerCount
		{
			get;
			set;
		}
		#endregion
	}
}
