﻿// ****************************************************************************************************************** //
//	Domain		:	GS.MessageTransfer.TransferInfoEventArgs
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2014년 12월 7일 일요일 오전 12:00
//	Purpose		:	파일 전송 정보에 대한 EventArgs입니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="TransferInfoEventArgs.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;

namespace GS.MessageTransfer
{
	/// <summary>
	/// 파일 전송 정보에 대한 EventArgs입니다.
	/// </summary>
	[Serializable]
	public class TransferInfoEventArgs : ServerInfoEventArgs
	{
		#region Constructors
		/// <summary>
		/// TransferInfoEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="fileName">전송중인 파일 이름 입니다.</param>
		/// <param name="currentSize">현재 전송된 byte 입니다.</param>
		/// <param name="totalSize">전체 전송할 byte 입니다.</param>
		/// <param name="serverIpAddress">서비스 하는 Server의 IpAddress 입니다.</param>
		/// <param name="port">Socket과 Binding되는 Port 번호 입니다.</param>
		public TransferInfoEventArgs(
			string fileName,
			long currentSize,
			long totalSize,
			string serverIpAddress = "",
			int port = 0)
			: base(serverIpAddress, port)
		{
			FileName = fileName;
			CurrentSize = currentSize;
			TotalSize = totalSize;
		}
		#endregion

		#region Properties
		/// <summary>
		/// FileName를 구하거나 설정합니다.
		/// </summary>
		public string FileName
		{
			get;
			set;
		}

		/// <summary>
		/// CurrentSize를 구하거나 설정합니다.
		/// </summary>
		public long CurrentSize
		{
			get;
			set;
		}

		/// <summary>
		/// TotalSize를 구하거나 설정합니다.
		/// </summary>
		public long TotalSize
		{
			get;
			set;
		}

		/// <summary>
		/// 현재 전송한 전송율을 구합니다.
		/// </summary>
		public double Percentage
		{
			get
			{
				return ((double)CurrentSize / (double)TotalSize) * 100d;
			}
		}
		#endregion
	}
}
