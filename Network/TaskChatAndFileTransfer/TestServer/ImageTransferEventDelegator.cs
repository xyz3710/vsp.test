﻿// ****************************************************************************************************************** //
//	Domain		:	
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2014년 12월 6일 토요일 오후 11:56
//	Purpose		:	Messenger의 Remote Event Delegator 입니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="XMessageEventDelegator.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;

namespace GS.MessageTransfer
{
	/// <summary>
	/// Message를 전송하기 위한 이벤트 처리기 대리자 입니다.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	[Serializable]
	public delegate void SendMessageEventHandler(object sender, MessageEventArgs e);

	/// <summary>
	/// 서비스가 시작/종료되는 이벤트를 처리할 메서드를 나타냅니다. 
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	[Serializable]
	public delegate void ServiceEventHandler(object sender, ServerInfoEventArgs e);
	/// <summary>
	/// Client가 접속했을 경우 이벤트를 처리할 메서드를 나타냅니다. 
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	[Serializable]
	public delegate void AcceptClientEventHandler(object sender, SocketAsyncEventArgs e);
	/// <summary>
	/// packet이 전송되면 이벤트를 처리할 메서드를 나타냅니다. 
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	[Serializable]
	public delegate void TransferEventHandler(object sender, TransferInfoEventArgs e);
	/// <summary>
	/// packet이 전송되면 이벤트를 처리할 메서드를 나타냅니다. 
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	[Serializable]
	public delegate void ErrorEventHandler(object sender, ErrorEventArgs e);
}
