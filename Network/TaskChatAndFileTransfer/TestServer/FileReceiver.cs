﻿// ****************************************************************************************************************** //
//	Domain		:	GS.MessageTransfer.FileReceiver
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2014년 12월 7일 일요일 오전 12:01
//	Purpose		:	File 전송용 수신자 class 입니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="FileReceiver.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using GS.MessageTransfer.Common;

namespace GS.MessageTransfer
{
	/// <summary>
	/// File 전송용 수신자 class 입니다.
	/// </summary>
	[Serializable]
	public class FileReceiver
	{
		#region StartServerParameter class
		[Serializable]
		private class StartServerParameter
		{
			#region Constructors
			/// <summary>
			/// StartServerParameter class의 새 인스턴스를 초기화 합니다.
			/// </summary>
			/// <param name="appendable">파일을 이어 받을지 여부를 결정합니다.</param>
			/// <param name="listenerCount">Socket을 listen할 thread의 개수를 결정합니다.</param>
			public StartServerParameter(bool appendable, int listenerCount)
			{
				ListenerCount = listenerCount;
				Appendable = appendable;
			}
			#endregion

			#region Properties
			/// <summary>
			/// ListenerCount를 구하거나 설정합니다.
			/// </summary>
			public int ListenerCount
			{
				get;
				set;
			}

			/// <summary>
			/// Appendable를 구하거나 설정합니다.
			/// </summary>
			public bool Appendable
			{
				get;
				set;
			}
			#endregion
		}
		#endregion

		#region Constants
		private const int DEFAULT_PORT = 19090;
		private const int SEGMENT_SIZE = 1024;
		#endregion

		#region Fields
		private Socket _socket;
		private string _receivedPath;
		private int _bufferSize;
		private bool _completed;
		private bool _closeAfterReceive;
		private int _completedCount;
		private int _closeAfterReceiveReadyInterval;
		private System.Timers.Timer _timer;
		#endregion

		#region Event
		/// <summary>
		/// 서비스가 시작되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler Started;
		/// <summary>
		/// 서비스가 중지되면 발생합니다.
		/// </summary>
		public event ServiceEventHandler Stopped;
		/// <summary>
		/// Client가 접속했을 경우 발생합니다.
		/// </summary>
		public event AcceptClientEventHandler AcceptClient;
		/// <summary>
		/// Header를 전송 받았을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler HeaderReceived;
		/// <summary>
		/// 데이터를 전송 받을 경우 발생합니다.
		/// </summary>
		public event TransferEventHandler Receiving;
		/// <summary>
		/// 데이터 수신이 완료 되면 발생합니다.
		/// </summary>
		public event TransferEventHandler Completed;
		/// <summary>
		/// Error가 발생할 경우 발생합니다.
		/// </summary>
		public event ErrorEventHandler Error;
		#endregion

		#region EventHandler
		/// <summary>
		/// Started 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnStarted(ServerInfoEventArgs e)
		{
			if (Started != null)
			{
				Started(this, e);
			}
		}

		/// <summary>
		/// Stopped 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnStopped(ServerInfoEventArgs e)
		{
			if (Stopped != null)
			{
				Stopped(this, e);
			}
		}

		/// <summary>
		/// AcceptClient 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnAcceptClient(SocketAsyncEventArgs e)
		{
			if (AcceptClient != null)
			{
				AcceptClient(this, e);
			}
		}

		/// <summary>
		/// HeaderReceived 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnHeaderReceived(TransferInfoEventArgs e)
		{
			if (HeaderReceived != null)
			{
				HeaderReceived(this, e);
			}
		}

		/// <summary>
		/// Receiving 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnReceiving(TransferInfoEventArgs e)
		{
			if (Receiving != null)
			{
				Receiving(this, e);
			}
		}

		/// <summary>
		/// Completed 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnCompleted(TransferInfoEventArgs e)
		{
			if (Completed != null)
			{
				Completed(this, e);
			}
		}

		/// <summary>
		/// Error 이벤트가 발생하면 처리됩니다.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnError(GS.MessageTransfer.ErrorEventArgs e)
		{
			if (Error != null)
			{
				Error(this, e);
			}
		}
		#endregion

		#region Constructors
		/// <summary>
		/// FileReceiver class의 새 인스턴스를 초기화 합니다.
		/// <remarks>기본 Port를 19090번을 사용합니다.</remarks>
		/// </summary>
		/// <param name="port">The port.</param>
		public FileReceiver(int port = DEFAULT_PORT)
		{
			Port = port;
			Bind(port);
		}
		#endregion

		#region Properties
		/// <summary>
		/// ReceivedPath를 구하거나 설정합니다.
		/// </summary>
		public string ReceivedPath
		{
			get
			{
				return _receivedPath.Replace(@"\", "/");
			}
			set
			{
				_receivedPath = value;

				if (Directory.Exists(_receivedPath) == false)
				{
					Directory.CreateDirectory(_receivedPath);
				}
			}
		}

		/// <summary>
		/// 다중으로 전송 받을 경우 중복된 파일들을 분류하기 위해 ReceivedPath에 Remote Client의 IpAddress를 폴더명을 추가한다.
		/// </summary>
		public bool ReceivedPathWithRemoteIpAddress
		{
			get;
			set;
		}

		/// <summary>
		/// 다중으로 전송 받을 경우 중복된 파일들을 분류하기 위해 ReceivedPath에 받는 날짜 폴더명을 추가한다.
		/// </summary>
		public bool ReceivedPathWithDate
		{
			get;
			set;
		}

		/// <summary>
		/// Port를 구하거나 설정합니다.
		/// </summary>
		public int Port
		{
			get;
			set;
		}

		/// <summary>
		/// BufferSize를 구하거나 설정합니다.
		/// <remarks>기본 크기는 1024 * 512 Byte 입니다.</remarks>
		/// </summary>
		public int BufferSize
		{
			get
			{
				if (_bufferSize == 0)
				{
					_bufferSize = SEGMENT_SIZE * 512;
				}

				return _bufferSize;
			}
			set
			{
				_bufferSize = value;
			}
		}

		/// <summary>
		/// Socket에 특정 Port를 성공적으로 Binding되었는지 여부를 구합니다.
		/// </summary>
		public bool IsBound
		{
			get;
			private set;
		}

		/// <summary>
		/// 전송한 뒤 Service를 중지 시킬지 여부를 결정합니다.
		/// </summary>
		public bool CloseAfterReceive
		{
			private get
			{
				return _closeAfterReceive;
			}
			set
			{
				_closeAfterReceive = value;

				// 특정 interval동안 Receiving을 처리한다.
				if (value == true && _timer == null)
				{
					_timer = new System.Timers.Timer();
					_timer.AutoReset = true;
					_timer.Interval = CloseAfterReceiveReadyInterval;
					_timer.Elapsed += new System.Timers.ElapsedEventHandler(OnIdleReceive);
				}
			}
		}

		/// <summary>
		/// Service가 현재 시작중인지 여부를 구합니다.
		/// </summary>
		public bool Running
		{
			get;
			private set;
		}

		/// <summary>
		/// 전송한 뒤 Service를 중지 시키기 위해 대기하는 시간을 ms 단위로 구하거나 설정합니다.
		/// <remarks>기본 5초 입니다.(5000)</remarks>
		/// </summary>
		public int CloseAfterReceiveReadyInterval
		{
			get
			{
				if (_closeAfterReceiveReadyInterval == 0)
				{
					_closeAfterReceiveReadyInterval = 5000;
				}

				return _closeAfterReceiveReadyInterval;
			}
			set
			{
				_closeAfterReceiveReadyInterval = value;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is connected.
		/// </summary>
		/// <value><c>true</c> if this instance is connected; otherwise, <c>false</c>.</value>
		public bool IsConnected
		{
			get
			{
				return _socket.IsConnected();
			}
		}

		private int CompletedCount
		{
			get
			{
				return _completedCount;
			}
			set
			{
				// Timer를 중지 후 CloseAfterReceiveReadyInterval초 후에 이벤트를 발생시킨다.
				if (_timer != null)
				{
					_timer.Stop();
				}

				_completedCount = value;

				// 값이 입력 되면 CloseAfterReceiveReadyInterval초간 대기 한다.
				if (_timer != null)
				{
					_timer.Start();
				}
			}
		}

		private void OnIdleReceive(object sender, System.Timers.ElapsedEventArgs e)
		{
			if (CloseAfterReceive == true)
			{
				_timer.Stop();
				Stop();
			}
		}
		#endregion

		#region Bind
		private void Bind(int port)
		{
			IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);

			try
			{
				_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
				_socket.Bind(endPoint);
				IsBound = true;
			}
			catch (SocketException ex)
			{
				if (ex.ErrorCode == 10048)
				{
					return;
				}

				// ErrorCode : 10048
				// 각 소켓 주소(프로토콜/네트워크 주소/포트)는 하나만 사용할 수 있습니다
				IsBound = false;
				throw new InvalidProgramException(string.Format("Socket을 아래와 같은 이유로 실패하였습니다.\r\n({0}){1}", ex.ErrorCode, ex.Message), ex);
			}
		}
		#endregion

		#region Start
		/// <summary>
		/// Socket Listener를 시작합니다.
		/// </summary>
		/// <param name="appendable">파일을 이어 받을지 여부를 결정합니다.</param>
		/// <param name="listenerCount">Socket을 listen할 thread의 개수를 결정합니다.</param>
		public void Start(bool appendable = true, int listenerCount = 1)
		{
			if (IsBound == false)
			{
				throw new InvalidOperationException("Socket과 Binding 되지 않았습니다. Bind method를 사용하여 해당 Port와 Binding을 다시 시도 하십시오.");
			}

			_socket.Listen(listenerCount);

			ParameterizedThreadStart parameterizedThreadStart = new ParameterizedThreadStart(InternalStartServer);
			StartServerParameter startServerParameter = new StartServerParameter(appendable, listenerCount);
			Thread[] thread = new Thread[listenerCount];

			OnStarted(new ServerInfoEventArgs(IpManager.LocalIp, Port, listenerCount));

			for (int i = 0; i < listenerCount; i++)
			{
				thread[i] = new Thread(parameterizedThreadStart);
				thread[i].Start(startServerParameter);
			}
		}

		private void InternalStartServer(object startServerParameters)
		{
			StartServerParameter startServerParameter = startServerParameters as StartServerParameter;

			if (startServerParameter == null)
			{
				return;
			}

			_socket.Listen(startServerParameter.ListenerCount);
			SocketAsyncEventArgs e = null;
			_completed = true;
			Running = true;

			while (IsBound == true)
			{
				// 한번 Accept 된 Socket은 완료가 되어야만 다시 받을 수 있다
				if (_completed == true)
				{
					e = new SocketAsyncEventArgs();
					e.UserToken = startServerParameter.Appendable;
					e.Completed += AcceptAsync_Completed;

					_completed = false;
					_socket.AcceptAsync(e);
				}

				Thread.Sleep(500);
			}
		}

		private void AcceptAsync_Completed(object sender, SocketAsyncEventArgs e)
		{
			if (e.SocketError != SocketError.Success)
			{
				return;
			}

			Socket acceptedSocket = e.AcceptSocket;
			Stream stream = null;
			BinaryWriter bw = null;
			bool appendable = (bool)e.UserToken;

			OnAcceptClient(e);

			try
			{
				int headerSize = 12;		// 첫번째 8byte 데이터 길이, 두번째 4byte File 이름 길이
				byte[] header = new byte[headerSize];
				// Header 중 File 크기와 File 이름 길이를 구한다.
				int receivedLength = acceptedSocket.Receive(header, headerSize, SocketFlags.Partial);

				// Header 정보를 읽어 들인다.
				long fileSize = (long)BitConverter.ToInt64(header, 0);				// file 크기는 long 형이다
				int fileNameLength = BitConverter.ToInt32(header, sizeof(long));	// file 이름 길이는 int 형이다.
				byte[] fileNameByte = new byte[fileNameLength];

				// File 이름을 읽어 들인다.				
				receivedLength = acceptedSocket.Receive(fileNameByte, fileNameLength, SocketFlags.Partial);

				string fileName = Encoding.Unicode.GetString(fileNameByte, 0, fileNameLength);
				string filePath = string.Format("{0}{1}{2}/{3}",
										ReceivedPath,
										ReceivedPathWithRemoteIpAddress == true ?
											"/" + ((IPEndPoint)acceptedSocket.RemoteEndPoint).Address.ToString()
											: string.Empty,
										ReceivedPathWithDate == true ?
											"/" + DateTime.Today.ToShortDateString()
											: string.Empty,
										fileName);
				string directory = Path.GetDirectoryName(filePath);
				string remoteIpAddress = ((IPEndPoint)acceptedSocket.RemoteEndPoint).Address.ToString();

				if (Directory.Exists(directory) == false)
				{
					Directory.CreateDirectory(directory);
				}

				if (receivedLength > 0)
				{
					OnHeaderReceived(new TransferInfoEventArgs(filePath, headerSize, header.Length, remoteIpAddress, Port));
				}

				if (appendable == false && File.Exists(filePath) == true)
				{
					File.Delete(filePath);
				}

				stream = File.Open(filePath, FileMode.Append);
				bw = new BinaryWriter(stream);

				long totalReceived = 0L;
				byte[] receivedData = new byte[BufferSize];

				// 데이터를 읽어들인다.
				while ((receivedLength = acceptedSocket.Receive(receivedData, BufferSize, SocketFlags.Partial)) > 0)
				{
					// 읽은 크기 만큼 File에 Write 한다.
					bw.Write(receivedData, 0, receivedLength);

					totalReceived += receivedLength;
					OnReceiving(new TransferInfoEventArgs(filePath, totalReceived, fileSize, remoteIpAddress, Port));
				}

				OnCompleted(new TransferInfoEventArgs(filePath, totalReceived, fileSize, remoteIpAddress, Port));
				CompletedCount++;
			}
			catch (SocketException ex)
			{
				OnError(new GS.MessageTransfer.ErrorEventArgs(string.Format("({0}){1}", ex.ErrorCode, ex.Message), ex));
			}
			catch (IOException ex)
			{
				OnError(new GS.MessageTransfer.ErrorEventArgs(ex.Message, ex));
			}
			catch (Exception ex)
			{
				OnError(new GS.MessageTransfer.ErrorEventArgs(ex.Message, ex));
			}
			finally
			{
				if (bw != null)
				{
					bw.Flush();
					bw.Close();
				}

				if (stream != null)
				{
					stream.Close();
				}

				if (acceptedSocket != null)
				{
					acceptedSocket.Close();
				}
			}

			_completed = true;
		}
		#endregion

		#region Stop
		/// <summary>
		/// Socket Listener를 중지합니다.
		/// </summary>
		public void Stop()
		{
			IsBound = false;

			if (_socket != null)
			{
				if (_socket.Connected == true)
				{
					_socket.Shutdown(SocketShutdown.Receive);
				}

				_socket.Close();
				_socket = null;
				OnStopped(new ServerInfoEventArgs(IpManager.LocalIp, Port, -1));
			}

			Running = false;
		}
		#endregion
	}
}
