﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GS.MessageTransfer;

namespace TestServer
{
	public partial class ServerForm : Form
	{
		public ServerForm()
		{
			InitializeComponent();
			AdjustLocation();

		}

		private void AdjustLocation()
		{
			SuspendLayout();

			Rectangle workingArea = Screen.AllScreens[0].WorkingArea;
			Location = workingArea.Location;

			ResumeLayout();
		}

		private FileReceiver FileReceiver
		{
			get;
			set;
		}

		protected override void OnLoad(EventArgs e)
		{
			FileReceiver = new FileReceiver();
			FileReceiver.AcceptClient += FileReceiver_AcceptClient;
			FileReceiver.HeaderReceived += FileReceiver_HeaderReceived;
			FileReceiver.Completed += FileReceiver_Completed;
			FileReceiver.ReceivedPath = @"J:\";
			FileReceiver.ReceivedPathWithDate = true;
			FileReceiver.Start(true, 50);
		}

		void FileReceiver_HeaderReceived(object sender, TransferInfoEventArgs e)
		{
			tbResult.InvokeText(string.Format("{0} header received.\r\n{1}", e.FileName, tbResult.Text));
		}

		private void FileReceiver_Completed(object sender, TransferInfoEventArgs e)
		{
			tbResult.InvokeText(string.Format("{0} received.\r\n{1}", e.FileName, tbResult.Text));
			//System.Diagnostics.Debug.WriteLine(string.Format("{0} received.", e.FileName), "MainForm.FileReceiver_Completed");
		}

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			base.OnFormClosing(e);

			FileReceiver.Stop();
		}
		void FileReceiver_AcceptClient(object sender, System.Net.Sockets.SocketAsyncEventArgs e)
		{
			var remote = e.AcceptSocket.RemoteEndPoint as IPEndPoint;

			tbResult.InvokeText(string.Format("{0} connected.\r\n{1}", remote.Address, tbResult.Text));
			System.Diagnostics.Debug.WriteLine(string.Format("{0} connected", e.Count), "MainForm.FileReceiver_AcceptClient");
		}
	}
}
