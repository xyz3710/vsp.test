﻿// ****************************************************************************************************************** //
//	Domain		:	GS.MessageTransfer.FormAction
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2014년 12월 6일 토요일 오후 11:57
//	Purpose		:	Agent의 Mode을 선택합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="FormAction.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;

namespace GS.MessageTransfer
{
	/// <summary>
	/// Agent의 Mode을 선택합니다.
	/// </summary>
	public enum FormAction
	{
		/// <summary>
		/// Form을 Open 했을 경우를 선택합니다.
		/// </summary>
		Open = 0,
		/// <summary>
		/// Form을 Close 했을 경우를 선택합니다.
		/// </summary>
		Close,
		/// <summary>
		/// Form을 활성화 했을 경우를 선택합니다.
		/// </summary>
		Active,
	}
}
