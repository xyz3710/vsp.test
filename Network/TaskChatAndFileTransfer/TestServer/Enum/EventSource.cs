﻿// ****************************************************************************************************************** //
//	Domain		:	GS.MessageTransfer.EventSource
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2014년 12월 6일 토요일 오후 11:57
//	Purpose		:	전송 되는 Event Message의 원본을 나열합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	변경시 XMessage의 InternalSendMessage 부분을 수정해 줘야 한다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="EventSource.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;

namespace GS.MessageTransfer
{
	/// <summary>
	/// 전송 되는 Event Message의 원본을 나열합니다.
	/// </summary>
	[Serializable]
	public enum EventSource
	{
		/// <summary>
		/// Message를 전송합니다.
		/// </summary>
		Message = 0,
		/// <summary>
		/// 정상 종료 처리 되지 않은 client를 위해서 특정 interval을 두어 dummy message를 전파 합니다.
		/// </summary>
		DummySignalMessageBroadCasted,
		/// <summary>
		/// 비정상 Logout Message를 전송합니다.
		/// </summary>
		AbnormalLogout,
		/// <summary>
		/// ICommand를 구현하는 class를 사용하여 명령을 실행합니다.
		/// </summary>
		ExecuteCommand,
		/// <summary>
		/// PutFile Message를 전송합니다.
		/// </summary>
		PutFile,
		/// <summary>
		/// PutFile이 완료되면 Client로 부터 Callback Message를 전송받습니다.
		/// </summary>
		PutFileCompleted,
		/// <summary>
		/// GetFile Receiver 요청 Message를 전송합니다.
		/// </summary>
		GetFileSendResponse,
		/// <summary>
		/// GetFile Sender 요청 Message를 전송합니다.
		/// </summary>
		GetFileSendRequest,
		/// <summary>
		/// GetFileList Message를 전송합니다.
		/// </summary>
		GetFileList,
		/// <summary>
		/// Exception Message를 전송합니다.
		/// </summary>
		Exception,
		/// <summary>
		/// Log Message를 전송합니다.
		/// </summary>
		Log,
		/// <summary>
		/// XMessage를 배포 Agent로 사용할 경우 Deploy Message를 전송합니다.
		/// </summary>
		Deploy,
	}
}
