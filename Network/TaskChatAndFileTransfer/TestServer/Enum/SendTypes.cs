﻿// ****************************************************************************************************************** //
//	Domain		:	GS.MessageTransfer.SendTypes
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2014년 12월 6일 토요일 오후 11:58
//	Purpose		:	전송할 Message Type을 선택합니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="SendTypes.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Text;

namespace GS.MessageTransfer
{
	/// <summary>
	/// 전송할 Message Type을 선택합니다.
	/// </summary>
	[Serializable]
	public enum SendTypes
	{
		/// <summary>
		/// Message를 자신을 제외한 Client에 전파 합니다.
		/// </summary>
		BroadCast = 0,
		/// <summary>
		/// Message를 특정 Client에만 보냅니다.
		/// </summary>
		SendTo,
	}
}
