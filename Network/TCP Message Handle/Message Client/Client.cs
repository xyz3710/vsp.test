using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.IO;

namespace Message_Client
{
	public class Client
	{
		public static void Main()
		{
			Client.Connect("127.0.0.1", "Hello Papa");
		}

		public static void Connect(String server, String message)
		{
			try
			{
				// Create a TcpClient.
				// Note, for this client to work you need to have a TcpServer 
				// connected to the same address as specified by the server, port
				// combination.
				Int32 port = 13000;
				TcpClient client = new TcpClient(server, port);

				// Translate the passed message into ASCII and store it as a Byte array.
				Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

				// Get a client stream for reading and writing.
				Stream stream = client.GetStream();

				// Send the message to the connected TcpServer. 
				stream.Write(data, 0, data.Length);

				Console.WriteLine("Sent: {0}", message);

				//Inform the user that the message was written
				//to the stream.
				Console.WriteLine("The message was sent.");
				
				// Receive the TcpServer.response.

				// Buffer to store the response bytes.
				data = new Byte[256];

				// String to store the response ASCII representation.
				String responseData = String.Empty;

				// Read the first batch of the TcpServer response bytes.
				Int32 bytes = stream.Read(data, 0, data.Length);
				responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
				Console.WriteLine("Received: {0}", responseData);

				// Close everything.
				stream.Close();
				client.Close();
			}
			catch (ArgumentNullException e)
			{
				Console.WriteLine("ArgumentNullException: {0}", e);
			}
			catch (SocketException e)
			{
				Console.WriteLine("SocketException: {0}", e);
			}

			Console.WriteLine("\n Press Enter to continue...");
			Console.Read();
		}
	}
}
/*
		static void Main(string[] args)
		{
			try
			{
				//Create a TCP connection to a listening TCP process.
				//Use "localhost" to specify the current computer or
				//replace "localhost" with the IP address of the 
				//listening process.  
				TcpClient TCP = new TcpClient("localhost", 11000);

				//Create a network stream from the TCP connection. 
				NetworkStream NetStream = TCP.GetStream();

				//Create a new instance of the RijndaelManaged class
				// and encrypt the stream.
				RijndaelManaged RMCrypto = new RijndaelManaged();

				byte[] Key = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
				byte[] IV = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };

				//Create a CryptoStream, pass it the NetworkStream, and encrypt 
				//it with the Rijndael class.
				CryptoStream CryptStream = new CryptoStream(NetStream,
				RMCrypto.CreateEncryptor(Key, IV),
				CryptoStreamMode.Write);

				//Create a StreamWriter for easy writing to the 
				//network stream.
				StreamWriter SWriter = new StreamWriter(CryptStream);

				//Write to the stream.
				SWriter.WriteLine("Hello World!");

				//Inform the user that the message was written
				//to the stream.
				Console.WriteLine("The message was sent.");

				//Close all the connections.
				SWriter.Close();
				CryptStream.Close();
				NetStream.Close();
				TCP.Close();
			}
			catch
			{
				//Inform the user that an exception was raised.
				Console.WriteLine("The connection failed.");
			}
		}
*/