﻿// ****************************************************************************************************************** //
//	Domain		:	
//	Creator		:	GS_DEV\xyz37(Kim Ki Won)
//	Create		:	2017년 11월 8일 수요일 20:54
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	https://stackoverflow.com/questions/209779/how-can-you-change-network-settings-ip-address-dns-wins-host-name-with-code
/*					dns 셋업할 때는 1초 정도 쉬어야 값이 들어간다.
 *					netsh interface ip set address name="Wi-Fi" static 192.168.254.107 255.255.255.0 192.168.254.254
					choice/t 1 /d y
					netsh interface ipv4 set dns name="Wi-Fi" source=static address=8.8.8.8 primary
					netsh interface ipv4 add dnsserver name="Wi-Fi" address=8.8.4.4 index=2
*/

// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="Program.cs" company="(주)가치소프트">
//		Copyright (c) 2017. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace SetIpAddress
{
	class Program
	{
		static void Main(string[] args)
		{
			SetDHCP("Wi-Fi");
			SetIpAddress("Wi-Fi", "192.168.254.107", "255.255.255.0", "192.168.254.254", "8.8.8.8", "8.8.4.4");
		}

		/// <summary>
		/// Sets the ip address.
		/// </summary>
		/// <param name="nicName">Name of the nic.</param>
		/// <param name="ipAddress">The ip address.</param>
		/// <param name="subnetMask">The subnet mask.</param>
		/// <param name="gateway">The gateway.</param>
		/// <param name="dns1">The DNS1.</param>
		/// <param name="dns2">The DNS2.</param>
		/// <returns></returns>
		public static bool SetIpAddress(
			string nicName,
			string ipAddress,
			string subnetMask,
			string gateway = null,
			string dns1 = null,
			string dns2 = null)
		{
			ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
			ManagementObjectCollection moc = mc.GetInstances();

			NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
			NetworkInterface networkInterface = interfaces.FirstOrDefault(x => x.Name == nicName);
			string nicDesc = nicName;

			if (networkInterface != null)
			{
				nicDesc = networkInterface.Description;
			}

			foreach (ManagementObject mo in moc)
			{
				if ((bool)mo["IPEnabled"] == true
					&& mo["Description"].Equals(nicDesc) == true)
				{
					try
					{
						ManagementBaseObject newIP = mo.GetMethodParameters("EnableStatic");

						newIP["IPAddress"] = new string[] { ipAddress };
						newIP["SubnetMask"] = new string[] { subnetMask };

						ManagementBaseObject setIP = mo.InvokeMethod("EnableStatic", newIP, null);

						if (gateway != null)
						{
							ManagementBaseObject newGateway = mo.GetMethodParameters("SetGateways");

							newGateway["DefaultIPGateway"] = new string[] { gateway };
							newGateway["GatewayCostMetric"] = new int[] { 1 };

							ManagementBaseObject setGateway = mo.InvokeMethod("SetGateways", newGateway, null);
						}


						if (dns1 != null || dns2 != null)
						{
							ManagementBaseObject newDns = mo.GetMethodParameters("SetDNSServerSearchOrder");
							var dns = new List<string>();

							if (dns1 != null)
							{
								dns.Add(dns1);
							}

							if (dns2 != null)
							{
								dns.Add(dns2);
							}

							newDns["DNSServerSearchOrder"] = dns.ToArray();

							ManagementBaseObject setDNS = mo.InvokeMethod("SetDNSServerSearchOrder", newDns, null);
						}
					}
					catch
					{
						return false;
					}
				}
			}

			return true;
		}

		/// <summary>
		/// Sets the DHCP.
		/// </summary>
		/// <param name="nicName">Name of the nic.</param>
		/// <returns></returns>
		public static bool SetDHCP(string nicName)
		{
			ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
			ManagementObjectCollection moc = mc.GetInstances();

			NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
			NetworkInterface networkInterface = interfaces.FirstOrDefault(x => x.Name == nicName);
			string nicDesc = nicName;

			if (networkInterface != null)
			{
				nicDesc = networkInterface.Description;
			}

			foreach (ManagementObject mo in moc)
			{
				if ((bool)mo["IPEnabled"] == true
					&& mo["Description"].Equals(nicDesc) == true)
				{
					try
					{
						ManagementBaseObject newDNS = mo.GetMethodParameters("SetDNSServerSearchOrder");

						newDNS["DNSServerSearchOrder"] = null;
						ManagementBaseObject enableDHCP = mo.InvokeMethod("EnableDHCP", null, null);
						ManagementBaseObject setDNS = mo.InvokeMethod("SetDNSServerSearchOrder", newDNS, null);
					}
					catch
					{
						return false;
					}
				}
			}

			return true;
		}
	}
}
