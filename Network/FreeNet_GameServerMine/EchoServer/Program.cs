﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GS.Common.Net.NetworkServices;

namespace EchoServer
{
	class Program
	{
		static void Main(string[] args)
		{
			NSPacketBufferManager.Initialize(2000);
			NetworkService service = new NetworkService(1000);

			service.SessionCreatedCallback += OnSessionCreated;
			service.Initialize();
			service.Listen(string.Empty, 10900, 100);
		}

		private static void OnSessionCreated(NSUserToken token)
		{

		}
	}
}
