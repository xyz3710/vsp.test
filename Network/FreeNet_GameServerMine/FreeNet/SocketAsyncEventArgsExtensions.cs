﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	public static class SocketAsyncEventArgsExtensions
	{
		/// <summary>
		/// Gets the user token.
		/// </summary>
		/// <param name="args">The <see cref="SocketAsyncEventArgs"/> instance containing the event data.</param>
		/// <returns></returns>
		public static NSUserToken GetUserToken(this SocketAsyncEventArgs args)
		{
			return args.UserToken as NSUserToken;
		}
	}
}
