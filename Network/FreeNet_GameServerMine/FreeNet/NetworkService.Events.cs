﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	partial class NetworkService
	{
		#region Logging Event
		/// <summary>
		/// Occurs when [logging].
		/// </summary>
		public Action<LoggingEventArgs> Logging;

		/// <summary>
		/// Called when [logging].
		/// </summary>
		/// <param name="message">The message.</param>
		protected virtual void OnLogging(string message)
		{
			if (Logging != null)
			{
				Logging(new LoggingEventArgs { Message = message });
			}
		}
		#endregion

		#region ExceptionLogging Event
		/// <summary>
		/// Occurs when [exception logging].
		/// </summary>
		public Action<ExceptionLoggingEventArgs> ExceptionLogging;

		/// <summary>
		/// Called when [exception logging].
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="exception">The exception.</param>
		protected virtual void OnExceptionLogging(string message, Exception exception)
		{
			if (ExceptionLogging != null)
			{
				ExceptionLogging(new ExceptionLoggingEventArgs
				{
					Message = message,
					Exception = exception,
				});
			}
		}
		#endregion

		#region SocketLogging Event
		/// <summary>
		/// Occurs when [socket logging].
		/// </summary>
		public Action<SocketLoggingEventArgs> SocketLogging;

		/// <summary>
		/// Called when [socket logging].
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="socketException">The socket exception.</param>
		protected virtual void OnSocketLogging(string message, SocketException socketException)
		{
			if (SocketLogging != null)
			{
				SocketLogging(new SocketLoggingEventArgs
				{
					Message = message,
					SocketException = socketException,
				});
			}
		}
		#endregion

		#region BufferReceived Event
		/// <summary>
		/// Occurs when [on BufferReceived].
		/// </summary>
		public Action<BufferReceivedEventArgs> BufferReceived;

		/// <summary>
		/// Handles the <see cref="E:OnBufferReceived"/> event.
		/// </summary>
		/// <param name="e">The <see cref="BufferReceivedEventArgs"/> instance containing the event data.</param>
		protected virtual void OnBufferReceived(BufferReceivedEventArgs e)
		{
			if (BufferReceived != null)
			{
				BufferReceived(e);
			}
		}
		#endregion

		#region Received Event
		/// <summary>
		/// 수신된 byte가 모두 수신하여 NSPacket으로 완성 되면 호출됩니다.
		/// </summary>
		public Action<Const<byte[]>> Received;

		/// <summary>
		/// Handles the <see cref="E:OnReceived" /> event.
		/// </summary>
		/// <param name="e">The instance containing the event data.</param>
		protected virtual void OnReceived(Const<byte[]> e)
		{
			if (Received != null)
			{
				Received(e);
			}
		}
		#endregion

		#region BeginSend Event
		/// <summary>
		/// Occurs when [on BeginSend].
		/// </summary>
		public Action<BeginSendEventArgs> BeginSend;

		/// <summary>
		/// Handles the <see cref="E:OnBeginSend"/> event.
		/// </summary>
		/// <param name="e">The <see cref="BeginSendEventArgs"/> instance containing the event data.</param>
		protected virtual void OnBeginSend(BeginSendEventArgs e)
		{
			if (BeginSend != null)
			{
				BeginSend(e);
			}
		}
		#endregion

		#region SendCompleted Event
		/// <summary>
		/// Occurs when [on SendCompleted].
		/// </summary>
		public Action<SendCompletedEventArgs> SendCompleted;

		/// <summary>
		/// Handles the <see cref="E:OnSendCompleted"/> event.
		/// </summary>
		/// <param name="e">The <see cref="SendCompletedEventArgs"/> instance containing the event data.</param>
		protected virtual void OnSendCompleted(SendCompletedEventArgs e)
		{
			if (SendCompleted != null)
			{
				SendCompleted(e);
			}
		}
		#endregion

		#region Disconnected Event
		/// <summary>
		/// Occurs when [on Disconnected].
		/// </summary>
		public Action<NSUserToken> Disconnected;

		/// <summary>
		/// Handles the <see cref="E:OnDisconnected" /> event.
		/// </summary>
		/// <param name="e">The instance containing the event data.</param>
		protected virtual void OnDisconnected(NSUserToken e)
		{
			if (Disconnected != null)
			{
				Disconnected(e);
			}
		}
		#endregion
	}
}
