﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// ExceptionLogging event's data
	/// </summary>
	[System.Diagnostics.DebuggerStepThrough]
	[System.Diagnostics.DebuggerDisplay("Message:{Message}, Exception:{Exception}", Name = "ExceptionLoggingEventArgs")]
	public class ExceptionLoggingEventArgs : LoggingEventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ExceptionLoggingEventArgs" /> class.
		/// </summary>
		public ExceptionLoggingEventArgs()
		{
		}
		
		/// <summary>
		/// Gets or sets the exception.
		/// </summary>
		/// <value>The exception.</value>
		public Exception Exception
		{
			get;
			set;
		}
	}
}
