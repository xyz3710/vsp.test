﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// Logging event's data
	/// </summary>
	[System.Diagnostics.DebuggerStepThrough]
	[System.Diagnostics.DebuggerDisplay("Message:{Message}", Name = "LoggingEventArgs")]
	public class LoggingEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LoggingEventArgs" /> class.
		/// </summary>
		public LoggingEventArgs()
		{
		}
		
		/// <summary>
		/// Message를 구하거나 설정합니다.
		/// </summary>
		/// <value>Message를 반환합니다.</value>
		public string Message
		{
			get;
			set;
		}
	}
}
