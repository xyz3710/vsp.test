﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// SocketLogging event's data
	/// </summary>
	[System.Diagnostics.DebuggerStepThrough]
	[System.Diagnostics.DebuggerDisplay("Message:{Message}, SocketException:{SocketException}", Name = "SocketLoggingEventArgs")]
	public class SocketLoggingEventArgs : LoggingEventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SocketLoggingEventArgs"/> class.
		/// </summary>
		public SocketLoggingEventArgs()
		{
		}
		
		/// <summary>
		/// Gets or sets the socket exception.
		/// </summary>
		/// <value>The socket exception.</value>
		public System.Net.Sockets.SocketException SocketException
		{
			get;
			set;
		}
	}
}
