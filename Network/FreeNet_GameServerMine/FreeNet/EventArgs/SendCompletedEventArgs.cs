﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// SendCompleted's event data.
	/// </summary>
	[System.Diagnostics.DebuggerStepThrough]
	[System.Diagnostics.DebuggerDisplay("SentCount:{SentCount}", Name = "SendCompletedEventArgs")]
	public class SendCompletedEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SendCompletedEventArgs" /> class.
		/// </summary>
		/// <param name="sentCount">The sent count.</param>
		public SendCompletedEventArgs(long sentCount)
		{
			SentCount = sentCount;
		}
		
		/// <summary>
		/// Gets or sets the sent count.
		/// </summary>
		/// <value>The sent count.</value>
		public long SentCount
		{
			get;
			set;
		}
	}
}
