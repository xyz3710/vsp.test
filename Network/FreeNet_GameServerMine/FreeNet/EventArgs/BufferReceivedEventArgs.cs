﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// BufferReceived's event data.
	/// </summary>
	[System.Diagnostics.DebuggerStepThrough]
	[System.Diagnostics.DebuggerDisplay("Buffer:{Buffer}, Offset:{Offset}, BytesTransferred:{BytesTransferred}", Name = "BufferReceivedEventArgs")]
	public class BufferReceivedEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BufferReceivedEventArgs" /> class.
		/// </summary>
		/// <param name="buffer">The buffer.</param>
		/// <param name="offset">The offset.</param>
		/// <param name="bytesTransferred">The bytes transferred.</param>
		public BufferReceivedEventArgs(byte[] buffer, int offset, int bytesTransferred)
		{
			Buffer = buffer;
			Offset = offset;
			BytesTransferred = bytesTransferred;
		}

		/// <summary>
		/// Buffer를 구하거나 설정합니다.
		/// </summary>
		/// <value>Buffer를 반환합니다.</value>
		public byte[] Buffer
		{
			get;
			set;
		}

		/// <summary>
		/// Offset를 구하거나 설정합니다.
		/// </summary>
		/// <value>Offset를 반환합니다.</value>
		public int Offset
		{
			get;
			set;
		}
		
		/// <summary>
		/// BytesTransferred를 구하거나 설정합니다.
		/// </summary>
		/// <value>BytesTransferred를 반환합니다.</value>
		public int BytesTransferred
		{
			get;
			set;
		}
	}
}
