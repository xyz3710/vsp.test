﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// BeginSend's event data.
	/// </summary>
	[System.Diagnostics.DebuggerStepThrough]
	[System.Diagnostics.DebuggerDisplay("SendCount:{SendCount}, BodySize:{BodySize}", Name = "BeginSendEventArgs")]
	public class BeginSendEventArgs : EventArgs
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BeginSendEventArgs" /> class.
		/// </summary>
		/// <param name="sendCount">The send count.</param>
		/// <param name="bodySize">Size of the body.</param>
		public BeginSendEventArgs(long sendCount, short bodySize)
		{
			SendCount = sendCount;
			BodySize = bodySize;
		}

		/// <summary>
		/// SendCount를 구하거나 설정합니다.
		/// </summary>
		/// <value>SendCount를 반환합니다.</value>
		public long SendCount
		{
			get;
			set;
		}
		
		/// <summary>
		/// BodySize를 구하거나 설정합니다.
		/// </summary>
		/// <value>BodySize를 반환합니다.</value>
		public short BodySize
		{
			get;
			set;
		}
	}
}
