﻿using System;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// Message Resolver
	/// </summary>
	public class MessageResolver
	{
		private int _remainBytes;
		private int _currentPosition;
		private int _positionToRead;
		private int _messageSize;
		private byte[] _messageBuffer;

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageResolver"/> class.
		/// </summary>
		/// <param name="bufferSize">Size of the buffer.</param>
		public MessageResolver(int bufferSize = 1024)
		{
			_remainBytes = 0;
			_currentPosition = 0;
			_positionToRead = 0;
			_messageSize = 0;
			_messageBuffer = new byte[bufferSize];
		}

		/// <summary>
		/// 소켓 버퍼로부터 데이터를 수신할 때 마다 호출된다.
		/// </summary>
		/// <remarks>
		/// 데이터가 남아 있을때 까지 계속 패킷을 만들어 callback을 호출해 둔다.
		/// 하나의 패킷을 완성하지 못했다면 버퍼에 보관해 놓은 뒤 다음 수신을 기다린다.
		/// </remarks>
		/// <param name="buffer">The buffer.</param>
		/// <param name="offset">The offset.</param>
		/// <param name="bytesTransferred">The bytes transferred.</param>
		/// <param name="callback">The callback.</param>
		public void OnReceived(byte[] buffer, int offset, int bytesTransferred, Action<Const<byte[]>> callback)
		{
			_remainBytes = bytesTransferred;    // 이번 receive로 읽어오게 될 바이트 수

			int sourcePosition = offset;        // 원본 버퍼의 위치, 패킷이 여러개 뭉쳐 올 경우 원본 버퍼의 위치는 계속 앞으로 가야 하는데 그 처리를 위한 변수.

			while (_remainBytes > 0)
			{
				bool completed = false;

				if (_currentPosition < CC.HeaderSize)       // 헤더 만큼 못 읽은 경우 헤더를 먼저 읽는다.
				{
					_positionToRead = CC.HeaderSize;        // 목표 위치 설정(헤더 위치까지 도달 하도록 설정)

					completed = ReadUntil(buffer, ref sourcePosition, offset, bytesTransferred);

					if (completed == false)
					{
						return;      // 아직 다 못읽었으므로 다음 receive를 기다린다.
					}

					_messageSize = GetBodySize();       // 헤더 하나를 온전히 읽어왔으므로 메세지 사이즈를 구한다.
					_positionToRead = _messageSize + CC.HeaderSize;     // 다음 위치 설정
				}

				completed = ReadUntil(buffer, ref sourcePosition, offset, bytesTransferred);

				if (completed == true)      // 하나의 패킷을 완성
				{
					callback(new Const<byte[]>(_messageBuffer));
					ClearBuffer();
				}
			}
		}

		/// <summary>
		/// 설정 위치까지 바이트의 원본 버퍼로부터 복사한다.
		/// </summary>
		/// <remarks>데이터가 모자랄 경우 현재 남은 바이트 까지만 복사</remarks>
		/// <param name="buffer">The buffer.</param>
		/// <param name="sourcePosition">The source position.</param>
		/// <param name="offset">The offset.</param>
		/// <param name="bytesTransferred">The bytes transferred.</param>
		/// <returns>다 읽었으면 true, 데이터가 모자라서 못 읽었으면 false를 반환</returns>
		private bool ReadUntil(byte[] buffer, ref int sourcePosition, int offset, int bytesTransferred)
		{
			if (_currentPosition >= (offset + bytesTransferred))
			{
				return false;       // 더이상 읽을 데이터가 없다.
			}

			int copySize = _positionToRead - _currentPosition;      // 읽을 바이트 수(데이터가 분리되어 올 경우 이전에 읽어놓은 값을 빼줘서 부족한 만큼 읽어올 수 있도록 계산)

			if (_remainBytes < copySize)        // 남은데이터가 적다만 가능한 만큼 복사
			{
				copySize = _remainBytes;
			}

			Array.Copy(buffer, sourcePosition, _messageBuffer, _currentPosition, copySize);

			sourcePosition += copySize;     // 원본 위치 이동
			_currentPosition += copySize;       // 타겟 버퍼 위치 이동
			_remainBytes -= copySize;

			if (_currentPosition < _positionToRead)     // 목표 지점에 도달 못했으면
			{
				return false;
			}

			return false;
		}

		private int GetBodySize()
		{
			return BitConverter.ToInt16(_messageBuffer, 0);     // 성능상 short이라고 고정한다.
																/*
																if (CC.HeaderSize.GetType() == typeof(short))
																{
																	return BitConverter.ToInt16(_messageBuffer, 0);
																}
																else
																{
																	return BitConverter.ToInt32(_messageBuffer, 0);
																}
																*/
		}

		private void ClearBuffer()
		{
			Array.Clear(_messageBuffer, 0, _messageBuffer.Length);
			_currentPosition = 0;
			_messageSize = 0;
		}
	}
}