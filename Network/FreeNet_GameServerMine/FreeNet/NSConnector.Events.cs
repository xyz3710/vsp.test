﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	partial class NSConnector
	{
		#region Logging Event
		/// <summary>
		/// Occurs when [logging].
		/// </summary>
		public Action<LoggingEventArgs> Logging;

		/// <summary>
		/// Called when [logging].
		/// </summary>
		/// <param name="message">The message.</param>
		protected virtual void OnLogging(string message)
		{
			if (Logging != null)
			{
				Logging(new LoggingEventArgs { Message = message });
			}
		}
		#endregion

		#region ExceptionLogging Event
		/// <summary>
		/// Occurs when [exception logging].
		/// </summary>
		public Action<ExceptionLoggingEventArgs> ExceptionLogging;

		/// <summary>
		/// Called when [exception logging].
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="exception">The exception.</param>
		protected virtual void OnExceptionLogging(string message, Exception exception)
		{
			if (ExceptionLogging != null)
			{
				ExceptionLogging(new ExceptionLoggingEventArgs
				{
					Message = message,
					Exception = exception,
				});
			}
		}
		#endregion

		#region Connected Event
		/// <summary>
		/// Occurs when [on Connected].
		/// </summary>
		public Action<NSUserToken> Connected;

		/// <summary>
		/// Handles the <see cref="E:OnConnected" /> event.
		/// </summary>
		/// <param name="e">The instance containing the event data.</param>
		protected virtual void OnConnected(NSUserToken e)
		{
			if (Connected != null)
			{
				Connected(e);
			}
		}
		#endregion

		#region Disconnected Event
		/// <summary>
		/// Occurs when [on Disconnected].
		/// </summary>
		public Action<NSUserToken> Disconnected;

		/// <summary>
		/// Handles the <see cref="E:OnDisconnected" /> event.
		/// </summary>
		/// <param name="e">The instance containing the event data.</param>
		protected virtual void OnDisconnected(NSUserToken e)
		{
			if (Disconnected != null)
			{
				Disconnected(e);
			}
		}
		#endregion
	}
}
