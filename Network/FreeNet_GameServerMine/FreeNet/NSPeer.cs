﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// 하나의 세션 개체를 나타낸다.
	/// </summary>
	/// <remarks>NSUserToken 보다 로직쪽에 더 근접한 성격의 클래스</remarks>
	public class NSPeer
	{
		/// <summary>
		/// 소켓 버퍼로부터 데이터를 수신하여 패킷 하나를 완성했을 때 호출 된다.
		/// 호출 흐름 : .Net Socket ReceiveAsync -&gt; NSUserToken.OnReceive -> NSPeer.OnMessage
		/// 
		/// 패킷 순서에 대해서(TCP)
		///		이 메서드는 .Net Socket의 스레드풀에 의해 작동되어 호출되므로 어느 스레드에서 호출될지 알 수 없다.
		///		하지만 하나의 CPeer객체에 대해서는 이 메서드가 완료된 이후 다음 패킷이 들어오도록 구현되어 있으므로
		///		클라이언트가 보낸 패킷 순서는 보장이 된다.
		///		
		/// 주의할점
		///		이 메서드에서 다른 CPeer객체를 참조하거나 공유자원에 접근할 때는 멀티스레드 관련 문제가 발생할 수 있으므로
		///		lock등의 처리를 해줘야 한다.
		///		게임 패킷을 처리할 때는 lock을 걸고 queue에 복사한 뒤 싱글 스레드로 처리하는것이 편하다.
		/// </summary>
		/// <param name="buffer">
		/// Socket버퍼로부터 복사된 CUserToken의 버퍼를 참조한다.
		/// 이 메서드가 리턴되면 buffer는 비워지며 다음 패킷을 담을 준비를 한다.
		/// 따라서 메서드를 리턴하기 전에 사용할 데이터를 모두 빼내야 한다.
		/// </param>
		public void OnMessage(Const<byte[]> buffer)
		{
			NSPacket packet = new NSPacket(buffer.Value);
			short protocolId = packet.PopInt16();

			switch (protocolId)
			{
				case 1:
					int number = packet.Pop();
					string text = packet.PopString();

					Console.WriteLine("[{0}] [Received] {1} : {2}, {3}", System.Threading.Thread.CurrentThread.ManagedThreadId, protocolId, number, text);
					break;
			}
		}
	}
}
