using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace GS.Common.Net.NetworkServices
{
	internal class BufferManager
	{
		private int _numberOfBytes;
		private byte[] _buffer;
		private int _currentIndex;
		private int _bufferSize;
		private Stack<int> _freeIndexPool;

		/// <summary>
		/// Initializes a new instance of the <see cref="BufferManager"/> class.
		/// </summary>
		/// <param name="totalBytes">The total bytes.</param>
		/// <param name="bufferSize">Size of the buffer.</param>
		public BufferManager(int totalBytes, int bufferSize)
		{
			_numberOfBytes = totalBytes;
			_currentIndex = 0;
			_bufferSize = bufferSize;
			_freeIndexPool = new Stack<int>();
		}

		/// <summary>
		/// Initializes the buffer.
		/// </summary>
		public void InitBuffer()
		{
			_buffer = new byte[_numberOfBytes];
		}

		/// <summary>
		/// Sets the buffer.
		/// </summary>
		/// <param name="args">The <see cref="SocketAsyncEventArgs"/> instance containing the event data.</param>
		/// <returns></returns>
		public bool SetBuffer(SocketAsyncEventArgs args)
		{
			if (_freeIndexPool.Count > 0)
			{
				args.SetBuffer(_buffer, _freeIndexPool.Pop(), _bufferSize);
			}
			else
			{
				if ((_numberOfBytes - _bufferSize) < _currentIndex)
				{
					return false;
				}

				args.SetBuffer(_buffer, _currentIndex, _bufferSize);
				_currentIndex += _bufferSize;
			}

			return true;
		}

		/// <summary>
		/// Frees the buffer.
		/// </summary>
		/// <param name="args">The <see cref="SocketAsyncEventArgs"/> instance containing the event data.</param>
		public void FreeBuffer(SocketAsyncEventArgs args)
		{
			_freeIndexPool.Push(args.Offset);

			args.SetBuffer(null, 0, 0);
		}
	}
}
