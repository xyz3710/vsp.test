﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// 서버와 클라이언트 공통으로 사용하는 세션 개체
	/// * 서버일 경우 : 
	///     하나의 클라이언트 개체를 나타낸다.
	///     이 인터페이스를 구현한 개체를 NetworkService 클래스의 SessionsCreateCallback 호출시 생성하여 반환한다.
	///     개체를 풀링할 지 여부는 사용자 의도대로 구현
	/// * 클라이언트일 경우 : 
	///     접속한 서버 개체를 나타낸다.
	/// </summary>
	/// <remarks>Thread unsafe 하므로 공유 자원에 접근할 대 동기화 처리가 필요한다.</remarks>
	public interface INSSession
	{
		/// <summary>
		/// Gets the identifier.
		/// </summary>
		/// <remarks>NSUserToken.Id를 주로 사용합니다.</remarks>
		/// <value>The identifier.</value>
		long Id
		{
			get;
		}

		/// <summary>
		/// 소켓 버퍼로부터 데이터를 수신하여 패킷 하나를 완성했을 때 호출 된다.
		/// 호출 흐름: .Net Socket ReceiveAsync -&gt; NSUserToken.OnReceive -&gt;NSPeer.OnMessage
		/// 
		/// 패킷 순서에 대해서(TCP)
		///     이 메서드는 .Net Socket의 스레드 풀에 의해 작동되어 호출 되므로 어느 스레드에서 호출될 지 알 수 없다.
		///     하지만 하나의 NSPeer에 대해서는 이 메서드가 완료된 이후 다음 패킷이 들어오도록 구현되어 있으므로
		///     클라이언트가 보낸 패킷 순서는 보장 된다.
		/// </summary>
		/// <param name="buffer">The buffer.</param>
		void Received(Const<byte[]> buffer);

		/// <summary>
		/// 서버에서 제거가 되면 처리할 메서드 입니다.
		/// </summary>
		void Disconnect();

		/// <summary>
		/// Sends the specified packet.
		/// </summary>
		/// <param name="packet">The packet.</param>
		void Send(NSPacket packet);

		/// <summary>
		/// Close this instance's socket.
		/// </summary>
		void Close();

		/// <summary>
		/// Processes the user operation.
		/// </summary>
		/// <param name="packet">The packet.</param>
		void ProcessUserOperation(NSPacket packet);
	}
}
