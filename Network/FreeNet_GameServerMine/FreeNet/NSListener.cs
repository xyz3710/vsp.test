using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// Network Service Listener
	/// </summary>
	public class NSListener
	{
		/// <summary>
		/// Gets or sets the accept new client callback.
		/// </summary>
		/// <remarks>accepted client socket, userToken</remarks>
		/// <value>The accept new client callback.</value>
		public Action<Socket, object> AcceptedNewClient
		{
			get;
			set;
		}

		private SocketAsyncEventArgs _acceptArgs;       // 비동기 Accept를 위한 EventArgs;
		private Socket _listenSocket;       // 클라이언트 접속을 처리할 소켓
		private AutoResetEvent _acceptFlowControlEvent;       // Accept 처리의 순서를 제어하기 위한 이벤트 변수

		/// <summary>
		/// Initializes a new instance of the <see cref="NSListener"/> class.
		/// </summary>
		public NSListener()
		{
			AcceptedNewClient = null;
		}

		/// <summary>
		/// Starts the specified host.
		/// </summary>
		/// <param name="host">The host.("" 일 경우 전체 IP에서 받아들인다)</param>
		/// <param name="port">The port.</param>
		/// <param name="backLog">The back log.</param>
		public void Start(string host, int port, int backLog)
		{
			_listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

			IPAddress address;

			if (host == "0.0.0.0" || host == string.Empty)
			{
				address = IPAddress.Any;
			}
			else
			{
				address = IPAddress.Parse(host);
			}

			IPEndPoint endPoint = new IPEndPoint(address, port);

			try
			{
				_listenSocket.Bind(endPoint);
				_listenSocket.Listen(backLog);

				_acceptArgs = new SocketAsyncEventArgs();
				_acceptArgs.Completed += OnAcceptCompleted;

				Thread listenThread = new Thread(DoListen);

				listenThread.Start();

				_listenSocket.AcceptAsync(_acceptArgs);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		private void DoListen()
		{
			_acceptFlowControlEvent = new AutoResetEvent(false);

			while (true)
			{
				_acceptArgs.AcceptSocket.Dispose();
				_acceptArgs.AcceptSocket = null;     // SocketAsyncEventArgs를 재사용하기 위해

				bool pending = true;

				try
				{
					// 비동기 accept를 호출하여 클라이언트를 받아들인다.
					// 비동기 메서드지만 동기적으로 수행이 완료될 경우도 있으니 리턴값을 확인하여 분기
					pending = _listenSocket.AcceptAsync(_acceptArgs);
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);

					continue;
				}

				// 즉시 완료 되면 이벤트가 발생하지 않으므로 false 일 경우 callback 메서드를 직접 호출
				// pending 상태라면 비동기 요청이 들어간 상태이므로 callback 메서드를 기다린다.
				// http://msdn.microsoft.com/ko-kr/library/system.net.sockets.socket.acceptasync%28v=vs.110%29.aspx  
				if (pending == false)
				{
					OnAcceptCompleted(null, _acceptArgs);
				}

				// 클라이언트 접속 처리가 완료되면 이벤트 개체의 신호를 받아 다리 루프를 수행 한다.
				_acceptFlowControlEvent.WaitOne();
			}
		}

		private void OnAcceptCompleted(object sender, SocketAsyncEventArgs e)
		{
			if (e.SocketError == SocketError.Success)
			{
				Socket clientSocket = e.AcceptSocket;       // 새로 생긴 소켓을 보관 후

				_acceptFlowControlEvent.Set();      // 다음 연결을 받아들인다.

				// 이 클래스는 accept 까지의 역할만 수행하고 클라이언트의 접속 이후의 처리는 
				// 외부로 넘기기 위해 callback 메서드를 호출해준다.
				// 소켓 처리부와 컨텐츠 구현부를 분리하기 위해서
				// 컨텐츠 처리부는 자주 바뀌지만 socket accept는 상대적 변경이 적어서 분리
				if (AcceptedNewClient != null)
				{
					AcceptedNewClient(clientSocket, e.UserToken);
				}

				return;
			}
			else
			{
				Console.WriteLine("Fail to accept client.");
			}

			_acceptFlowControlEvent.Set();
		}
	}
}
