﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// Endpoint정보를 받아서 서버에 접속한다.
	/// 접속하려는 서버 하나당 인스턴스 한개씩 생성하여 사용하면 된다.
	/// </summary>
	public partial class NSConnector
	{
		private Socket _client;
		private NetworkService _networkService;
		private IPEndPoint _endPoint;

		/// <summary>
		/// Initializes a new instance of the <see cref="NSConnector" /> class.
		/// </summary>
		/// <param name="networkService">The network service.</param>
		/// <param name="timeForFailedConnectionRefresh">The time for failed connection refresh.</param>
		public NSConnector(NetworkService networkService, int timeForFailedConnectionRefresh = 50)
		{
			_networkService = networkService;
			TimeForFailedConnectionRefresh = timeForFailedConnectionRefresh;
		}

		/// <summary>
		/// Gets or sets the time for failed connection refesh.
		/// </summary>
		/// <value>The time to failed connection.</value>
		public int TimeForFailedConnectionRefresh
		{
			get;
			set;
		}

		/// <summary>
		/// Connects the specified ip address.
		/// </summary>
		/// <param name="ipAddress">The ip address.</param>
		/// <param name="port">The port.</param>
		public void Connect(string ipAddress, int port)
		{
			_endPoint = new IPEndPoint(IPAddress.Parse(ipAddress), port);
			Connect(_endPoint);
		}

		/// <summary>
		/// Connects the specified remote endpoint.
		/// </summary>
		/// <param name="remoteEndpoint">The remote endpoint.</param>
		public void Connect(IPEndPoint remoteEndpoint)
		{
			_endPoint = remoteEndpoint;
			_client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

			// 비동기 접속을 위한 event args.
			SocketAsyncEventArgs saea = new SocketAsyncEventArgs();

			saea.Completed += ConnectCompleted;
			saea.RemoteEndPoint = remoteEndpoint;

			bool pending = _client.ConnectAsync(saea);

			if (pending == false)
			{
				ConnectCompleted(null, saea);
			}
		}

		private void ConnectCompleted(object sender, SocketAsyncEventArgs e)
		{
			NSUserToken token = new NSUserToken();

			if (e.SocketError == SocketError.Success)
			{
				_networkService.ConnectCompleted(_client, token);       // 데이터 수신 준비.

				if (token.Disconnected == null)
				{
					token.Disconnected = OnTokenDisconnected;
				}

				try
				{
					OnConnected(token);
					OnLogging(string.Format("[NSConnector.ConnectCompleted] Token Id: {0}", token.Id));
				}
				catch (Exception ex)
				{
					OnExceptionLogging("[Connected] event handler error.", ex);
				}
			}
			else
			{
				OnLogging(string.Format("[NSConnector.ConnectCompleted] Failed to connect. {0}", e.SocketError));

				Thread.Sleep(TimeForFailedConnectionRefresh);
				try
				{
					OnDisconnected(token);
				}
				catch (Exception ex)
				{
					OnExceptionLogging("[Disconnected] event handler error.", ex);
				}

				Connect(_endPoint);
			}
		}

		private void OnTokenDisconnected(NSUserToken e)
		{
			if (_endPoint != null)
			{
				Thread.Sleep(TimeForFailedConnectionRefresh);
				Connect(_endPoint);
			}
		}
	}
}
