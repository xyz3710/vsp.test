using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// Const
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public struct Const<T>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Const{T}"/> struct.
		/// </summary>
		/// <param name="value">The value.</param>
		public Const(T value)
		{
			Value = value;
		}

		/// <summary>
		/// Gets the value.
		/// </summary>
		/// <value>The value.</value>
		public T Value
		{
			get;
			private set;
		}
	}
}
