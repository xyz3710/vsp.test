﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// Network Service
	/// </summary>
	public partial class NetworkService
	{
		/// <summary>
		/// Gets or sets the session created callback.
		/// </summary>
		/// <value>The session created callback.</value>
		public Action<NSUserToken> SessionCreatedCallback
		{
			get;
			set;
		}

		private const int PRE_ALLOC_COUNT = 2;

		private int _connectedCount;
		private NSListener _clientListener;      // 클라이언트 접속을 받기 위한 개체
		private SocketAsyncEventArgsPool _receivePool;
		private SocketAsyncEventArgsPool _sendPool;
		private BufferManager _bufferManager;

		/// <summary>
		/// Initializes a new instance of the <see cref="NetworkService" /> class.
		/// </summary>
		/// <param name="maxConnections">The maximum connections.</param>
		/// <param name="bufferSize">Size of the buffer.</param>
		public NetworkService(int maxConnections, int bufferSize = 1024)
		{
			MaxConnections = maxConnections;
			BufferSize = bufferSize;

			_connectedCount = 0;
			SessionCreatedCallback = null;
		}

		/// <summary>
		/// Gets the maximum connections.
		/// </summary>
		/// <value>The maximum connections.</value>
		public int MaxConnections
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the size of the buffer.
		/// </summary>
		/// <value>The size of the buffer.</value>
		public int BufferSize
		{
			get;
			private set;
		}

		/// <summary>
		/// Initializes this instance.
		/// </summary>
		public void Initialize()
		{
			InitBufferManager();
			InitPool();
		}

		private void InitBufferManager()
		{
			_bufferManager = new BufferManager(MaxConnections * BufferSize * PRE_ALLOC_COUNT, BufferSize);
			_bufferManager.InitBuffer();
		}

		private void InitPool()
		{
			_receivePool = new SocketAsyncEventArgsPool(MaxConnections);
			_sendPool = new SocketAsyncEventArgsPool(MaxConnections);

			SocketAsyncEventArgs arg = null;

			for (int i = 0; i < MaxConnections; i++)
			{
				// 동일한 소켓에 대고 send, receive를 하므로, user token은 세션별로 하나씩만 만들어 놓고 
				// receive, send EventArgs에서 동일한 token을 참조하도록 구성한다.
				NSUserToken token = new NSUserToken();

				{
					arg = new SocketAsyncEventArgs();
					arg.Completed += OnReceiveCompleted;
					arg.UserToken = token;

					_bufferManager.SetBuffer(arg);
					_receivePool.Push(arg);
				}

				{
					arg = new SocketAsyncEventArgs();
					arg.Completed += OnSendCompleted;
					arg.UserToken = token;

					_bufferManager.SetBuffer(arg);
					_sendPool.Push(arg);
				}
			}
		}

		private void OnReceiveCompleted(object sender, SocketAsyncEventArgs e)
		{
			if (e.LastOperation == SocketAsyncOperation.Receive)
			{
				ProcessReceive(e);

				return;
			}

			OnExceptionLogging("The last operation completed on the socket was not a receive.", new ArgumentException("The last operation completed on the socket was not a receive."));
		}

		private void OnSendCompleted(object sender, SocketAsyncEventArgs e)
		{
			NSUserToken token = e.GetUserToken();

			token.ProcessSend(e);

			if (token.BeginSend == null)
			{
				token.BeginSend = BeginSend;
			}
		}

		/// <summary>
		/// Listens the specified host.
		/// </summary>
		/// <param name="host">The host.</param>
		/// <param name="port">The port.</param>
		/// <param name="backLog">The back log.</param>
		public void Listen(string host, int port, int backLog)
		{
			_clientListener = new NSListener();
			_clientListener.AcceptedNewClient += OnNewClient;
			_clientListener.Start(host, port, backLog);
		}

		/// <summary>
		/// 원격 서버에 접속 성공 했을 때 호출됩니다.
		/// </summary>
		/// <param name="socket">The socket.</param>
		/// <param name="token">The token.</param>
		public void ConnectCompleted(Socket socket, NSUserToken token)
		{
			// SocketAsyncEventArgsPool에서 빼오지 않고 그때 그때 할당해서 사용한다.
			// 풀은 서버에서 클라이언트와의 통신용으로만 쓰려고 만든것이기 때문이다.
			// 클라이언트 입장에서 서버와 통신을 할 때는 접속한 서버당 두개의 EventArgs만 있으면 되기 때문에 그냥 new해서 쓴다.
			// 서버간 연결에서도 마찬가지이다.
			// 풀링처리를 하려면 c->s로 가는 별도의 풀을 만들어서 써야 한다.
			SocketAsyncEventArgs receivedArgs = new SocketAsyncEventArgs();

			receivedArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnReceiveCompleted);
			receivedArgs.UserToken = token;
			receivedArgs.SetBuffer(new byte[BufferSize], 0, BufferSize);

			SocketAsyncEventArgs sendArgs = new SocketAsyncEventArgs();

			sendArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnSendCompleted);
			sendArgs.UserToken = token;
			sendArgs.SetBuffer(new byte[BufferSize], 0, BufferSize);

			BeginReceive(socket, receivedArgs, sendArgs);
		}

		private void OnNewClient(Socket clientSock, object token)
		{
			Interlocked.Increment(ref _connectedCount);

			SocketAsyncEventArgs receivedArgs = _receivePool.Pop();
			SocketAsyncEventArgs sendArgs = _sendPool.Pop();
			NSUserToken userToken = null;

			if (SessionCreatedCallback != null)     // SocketAsyncEvnetArgs를 생성할 때 만들어 두었던 UserToken을 꺼내와서 callback 메서드로 넘겨준다.
			{
				userToken = receivedArgs.GetUserToken();
				SessionCreatedCallback(userToken);
			}

			BeginReceive(clientSock, receivedArgs, sendArgs);

			//if (userToken != null)
			//{
			//	userToken.StartKeepAlive(3000);
			//}
		}

		private void BeginReceive(Socket clientSock, SocketAsyncEventArgs receivedArgs, SocketAsyncEventArgs sendArgs)
		{
			NSUserToken token = receivedArgs.GetUserToken();        // receivedArgs, sendArgs 아무곳에서 꺼내와도 둘도 동일한 UserToken을 들고 있다.

			token.SetEventArgs(receivedArgs, sendArgs);
			token.Socket = clientSock;

			// 데이터를 받을 수 있도록 소켓 메서드를 호출해준다.
			// 비동기로 수신할 경우 워커 스레드에서 대기중으로 있다가 Completed에 설정해 놓은 메서드 호출
			// 동기로 완료될 경우에는 직접 완료 메서드를 호출해 줘야 한다.
			bool pending = clientSock.ReceiveAsync(receivedArgs);

			if (pending == false)
			{
				ProcessReceive(receivedArgs);
			}
		}

		private void ProcessReceive(SocketAsyncEventArgs e)
		{
			NSUserToken token = e.GetUserToken();

			if (e.BytesTransferred > 0 && e.SocketError == SocketError.Success)
			{
				// 이후 작업은 UserToken에 맡긴다.
				token.OnBufferReceive(e.Buffer, e.Offset, e.BytesTransferred);

				// 다음 메세지 수신을 위해 다시 ReceiveAsync 메서드를 호출한다.
				bool pending = token.Socket.ReceiveAsync(e);

				if (pending == false)
				{
					ProcessReceive(e);
				}
			}
			else
			{
				OnLogging(string.Format("[NetworkSerice.OnProcessReceive] Error: {0}, Transferred: {1}", e.SocketError, e.BytesTransferred));
				CloseClient(token);
			}
		}

		/// <summary>
		/// Closes the client socket.
		/// </summary>
		/// <param name="token">The token.</param>
		public void CloseClient(NSUserToken token)
		{
			try
			{
				token.Disconnect();
			}
			catch (Exception ex)
			{
				OnExceptionLogging("Close client error.", ex);
			}

			try
			{
				OnDisconnected(token);
			}
			catch (Exception ex)
			{
				OnExceptionLogging("[Disconnected] event handler error.", ex);
			}

			if (_receivePool != null)
			{
				_receivePool.Push(token.ReceiveEventArgs);
			}

			if (_sendPool != null)
			{
				_sendPool.Push(token.SendEventArgs);
			}
		}
	}
}
