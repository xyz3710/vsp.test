﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// NSPacketBufferManager
	/// </summary>
	public class NSPacketBufferManager
	{
		private static object _bufferLockObject = new object();
		private static Stack<NSPacket> _pool;
		private static int _capaticy;

		/// <summary>
		/// Initializes the specified capacity.
		/// </summary>
		/// <param name="capacity">The capacity.</param>
		public static void Initialize(int capacity)
		{
			_pool = new Stack<NSPacket>(capacity);
			_capaticy = capacity;
			Allocate();
		}

		private static void Allocate()
		{
			for (int i = 0; i < _capaticy; i++)
			{
				_pool.Push(new NSPacket());
			}
		}

		/// <summary>
		/// Pops this instance.
		/// </summary>
		/// <returns></returns>
		public static NSPacket Pop()
		{
			lock (_bufferLockObject)
			{
				if (_pool.Count <= 0)
				{
					//System.Diagnostics.Debug.WriteLine("Reallocate", "NSPacketBufferManager.Pop");
					Allocate();
				}

				return _pool.Pop();
			}
		}

		/// <summary>
		/// Pushes the specified packet.
		/// </summary>
		/// <param name="packet">The packet.</param>
		public static void Push(NSPacket packet)
		{
			lock (_bufferLockObject)
			{
				_pool.Push(packet);
			}
		}
	}
}
