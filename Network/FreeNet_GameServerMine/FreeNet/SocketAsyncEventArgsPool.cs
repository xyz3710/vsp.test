using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// SocketAsyncEventArgs Pool
	/// </summary>
	public class SocketAsyncEventArgsPool
	{
		private Stack<SocketAsyncEventArgs> _pool;

		/// <summary>
		/// Initializes a new instance of the <see cref="SocketAsyncEventArgsPool"/> class.
		/// </summary>
		/// <param name="capacity">The capacity.</param>
		public SocketAsyncEventArgsPool(int capacity)
		{
			_pool = new Stack<SocketAsyncEventArgs>(capacity);
		}

		/// <summary>
		/// Pushes the specified item.
		/// </summary>
		/// <param name="item">The <see cref="SocketAsyncEventArgs"/> instance containing the event data.</param>
		/// <exception cref="System.ArgumentException">Push: item of type SocketAsyncEventArgs must not be null.</exception>
		public void Push(SocketAsyncEventArgs item)
		{
			if (item == null)
			{
				throw new ArgumentException("Push: item of type SocketAsyncEventArgs must not be null.");
			}

			lock (_pool)
			{
				_pool.Push(item);
			}
		}

		/// <summary>
		/// Pops this instance.
		/// </summary>
		/// <returns></returns>
		public SocketAsyncEventArgs Pop()
		{
			lock (_pool)
			{
				return _pool.Pop();
			}
		}

		/// <summary>
		/// Gets the count.
		/// </summary>
		/// <value>The count.</value>
		public int Count
		{
			get
			{
				return _pool.Count;
			}
		}
	}
}
