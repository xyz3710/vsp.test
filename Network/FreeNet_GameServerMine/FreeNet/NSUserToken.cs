using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// Network Service UserToken
	/// </summary>
	public partial class NSUserToken
	{
		private static int _id;
		private Queue<NSPacket> _sendingQueue;
		private object _sendingLockObject;

		/// <summary>
		/// Initializes a new instance of the <see cref="NSUserToken" /> class.
		/// </summary>
		/// <param name="bufferSize">Size of the buffer.</param>
		public NSUserToken(int bufferSize = 1024)
		{
			Id = ++_id;
			MessageResolver = new MessageResolver(bufferSize);
			_sendingQueue = new Queue<NSPacket>();
			_sendingLockObject = new object();
		}

		/// <summary>
		/// Gets the identifier.
		/// </summary>
		/// <value>The identifier.</value>
		public long Id
		{
			get;
			private set;
		}

		/// <summary>
		/// Socket를 구합니다.
		/// </summary>
		/// <value>Socket를 반환합니다.</value>
		public Socket Socket
		{
			get;
			internal set;
		}

		/// <summary>
		/// Gets a value indicating whether this instance is connected.
		/// </summary>
		/// <value><c>true</c> if this instance is connected; otherwise, <c>false</c>.</value>
		public bool IsConnected
		{
			get
			{
				if (Socket == null)
				{
					return false;
				}

				return Socket.Connected;
			}
		}

		/// <summary>
		/// Gets the network service session.
		/// </summary>
		/// <remarks>session object. 어플리케이션에서 구현하여 사용</remarks>
		/// <value>The network service session.</value>
		public INSSession NSSession
		{
			get;
			internal set;
		}

		/// <summary>
		/// Gets the receive event arguments.
		/// </summary>
		/// <value>The receive event arguments.</value>
		public SocketAsyncEventArgs ReceiveEventArgs
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the send event arguments.
		/// </summary>
		/// <value>The send event arguments.</value>
		public SocketAsyncEventArgs SendEventArgs
		{
			get;
			private set;
		}

		private MessageResolver MessageResolver
		{
			get;
			set;
		}

		/// <summary>
		/// Sets the session object.
		/// </summary>
		/// <param name="session">The session.</param>
		public void SetSession(INSSession session)
		{
			NSSession = session;
		}

		/// <summary>
		/// 보내는 패킷 중 버퍼가 수신 될 때 처리 합니다.
		/// </summary>
		/// <remarks>전체 패킷을 다 받은 후에는 <see cref="OnReceived"/>를 호출하여 처리 합니다.</remarks>
		/// <param name="buffer">The buffer.</param>
		/// <param name="offset">The offset.</param>
		/// <param name="bytesTransferred">The bytes transferred.</param>
		public void OnBufferReceive(byte[] buffer, int offset, int bytesTransferred)
		{
			try
			{
				OnBufferReceived(new BufferReceivedEventArgs(buffer, offset, bytesTransferred));
			}
			catch (Exception ex)
			{
				OnExceptionLogging("[BufferReceived] event handler error.", ex);
			}

			MessageResolver.OnReceived(buffer, offset, bytesTransferred, OnPacketReceived);
		}

		/// <summary>
		/// 수신된 byte가 모두 수신하여 NSPacket으로 완성 되면 호출됩니다.
		/// </summary>
		/// <param name="buffer">The buffer.</param>
		private void OnPacketReceived(Const<byte[]> buffer)
		{
			try
			{
				OnReceived(buffer);
			}
			catch (Exception ex)
			{
				OnExceptionLogging("[PacketReceived] event handler error.", ex);
			}

			if (NSSession != null)
			{
				NSSession.Received(buffer);
			}
		}

		/// <summary>
		/// Disconnected from server.
		/// </summary>
		public void Disconnect()
		{
			_sendingQueue.Clear();

			try
			{
				OnDisconnected(this);
			}
			catch (Exception ex)
			{
				OnExceptionLogging("[Disconnected] event handler error.", ex);
			}

			if (NSSession != null)
			{
				NSSession.Disconnect();
			}
		}

		/// <summary>
		/// Sets the event arguments.
		/// </summary>
		/// <param name="receiveEventArgs">The <see cref="SocketAsyncEventArgs"/> instance containing the event data.</param>
		/// <param name="sendEventArgs">The <see cref="SocketAsyncEventArgs"/> instance containing the event data.</param>
		public void SetEventArgs(SocketAsyncEventArgs receiveEventArgs, SocketAsyncEventArgs sendEventArgs)
		{
			ReceiveEventArgs = receiveEventArgs;
			SendEventArgs = sendEventArgs;
		}

		/// <summary>
		/// Sends the specified packet.
		/// </summary>
		/// <param name="packet">The packet.</param>
		public void Send(NSPacket packet)
		{
			lock (_sendingLockObject)
			{
				if (_sendingQueue.Count <= 0)       // 큐가 비어 있다면 큐에 추가하고 바로 비동기 전송 메서드를 호출 한다.
				{
					_sendingQueue.Enqueue(packet);
					StartSend();

					return;
				}

				_sendingQueue.Enqueue(packet);      // 큐에 있다면 이전 전송이 완료되지 않은 상태으므로 큐에 추가만 한다.
													// 현재 수행중이 SendAsync가 완료된 이후에 큐를 검사하여 데이터가 있으면 SendAsync를 호출하여 전송해준다.
			}
		}

		private void StartSend()
		{
			lock (_sendingLockObject)
			{
				NSPacket packet = _sendingQueue.Peek();       // 전송이 아직 완료된 상태가 아니므로 데이터만 가져오고 큐에서 제거하진 않는다.
				short bodySize = packet.RecordSize();        // 헤더에 패킷 크기를 기록한다.

				try
				{
					OnBeginSend(new BeginSendEventArgs(_sentCount, bodySize));
				}
				catch (Exception ex)
				{
					OnExceptionLogging("[BeginSend] event handler error.", ex);
				}

				SendEventArgs.SetBuffer(SendEventArgs.Offset, packet.Position);     // 이번에 보낼 패킷 크기만큼 버퍼 크기를 설정
				Array.Copy(packet.Buffer, 0, SendEventArgs.Buffer, SendEventArgs.Offset, packet.Position);      // 패킷 내용을 SocketAsyncEventArgs 버퍼에 복사

				bool pending = Socket.SendAsync(SendEventArgs);

				if (pending == false)
				{
					ProcessSend(SendEventArgs);
				}
			}
		}

		private static long _sentCount = 0;
		private static object _sendCountLockObject = new object();

		internal void ProcessSend(SocketAsyncEventArgs e)
		{
			if (e.BytesTransferred <= 0 || e.SocketError != SocketError.Success)
			{
				OnLogging(string.Format("[NSUserToken.ProcessSend] Failt to send. Error: {0}, Transferred: {1}", e.SocketError, e.BytesTransferred));

				return;
			}

			lock (_sendingLockObject)
			{
				if (_sendingQueue.Count <= 0)
				{
					OnExceptionLogging("Sending queue count is less than zero!", new InvalidOperationException());

					return;     // 재전송 로직 검토 필요
				}

				int size = _sendingQueue.Peek().Position;

				if (e.BytesTransferred != size)
				{
					OnLogging(string.Format("[NSUserToken.ProcessSend] Need to send more! Transferred: {0}, Packet size: {1}", e.BytesTransferred, size));

					e.SetBuffer(e.Offset, size - e.BytesTransferred);       // 남은 데이터 만큼 버퍼를 재설정 해주고 다시 전송한다.

					bool pending = Socket.SendAsync(e);

					if (pending == false)
					{
						ProcessSend(e);
					}

					return;
				}

				lock (_sendCountLockObject)
				{
					_sentCount++;
					//System.Diagnostics.Debug.WriteLine(string.Format("Process send: {0}, Transferred: {1}, Sent count: {2}", e.SocketError, e.BytesTransferred, _sentCount), "NSUserToken.ProcessSend");
				}

				NSPacket packet = _sendingQueue.Dequeue();        // 전송 완료된 패킷을 큐에서 제거 한다.
				NSPacket.Destroy(packet);

				try
				{
					OnSendCompleted(new SendCompletedEventArgs(_sentCount));
				}
				catch (Exception ex)
				{
					OnExceptionLogging("[SendCompleted] event handler error.", ex);
				}

				if (_sendingQueue.Count > 0)        // 전송하지 않은 대기중인 패킷이 있다면 다시 전송 요청
				{
					StartSend();
				}
			}
		}

		/// <summary>
		/// Sends the directly.
		/// </summary>
		/// <param name="packet">The packet.</param>
		internal void SendDirectly(NSPacket packet)
		{
			packet.RecordSize();
			SendEventArgs.SetBuffer(SendEventArgs.Offset, packet.Position);
			Array.Copy(packet.Buffer, 0, SendEventArgs.Buffer, SendEventArgs.Offset, packet.Position);

			bool pending = Socket.SendAsync(SendEventArgs);

			if (pending == false)
			{
				ProcessSend(SendEventArgs);
			}
		}

		/// <summary>
		/// Disconnects this instance.
		/// </summary>
		/// <exception cref="System.InvalidOperationException">Client socket has already closed</exception>
		public void Close()
		{
			try
			{
				Socket.Shutdown(SocketShutdown.Send);
			}
			catch (Exception ex)
			{
				throw new InvalidOperationException("Client socket has already closed", ex);
			}
			finally
			{
				Socket.Close();
				OnDisconnected(this);
			}
		}

		/// <summary>
		/// Starts the keep alive.
		/// </summary>
		/// <param name="interval">The interval.</param>
		public void StartKeepAlive(int interval = 3000)
		{
			System.Threading.Timer keepAlive = new System.Threading.Timer(e =>
			{
				NSPacket packet = NSPacket.Create(0);

				packet.Push(0);

				Send(packet);
			}, null, 0, interval);
		}
	}
}
