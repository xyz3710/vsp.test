﻿using System;
using System.Text;

namespace GS.Common.Net.NetworkServices
{
	/// <summary>
	/// Network Service Packet
	/// </summary>
	/// <remarks>byte[] 버퍼를 참조로 보관하여 PopXXX 메서드 호출 순서대로 데이터 변환을 수행한다.</remarks>
	public class NSPacket
	{
		private short _protocolId;

		/// <summary>
		/// Creates the specified protocol identifier.
		/// </summary>
		/// <param name="protocolId">The protocol identifier.</param>
		/// <returns></returns>
		public static NSPacket Create(short protocolId)
		{
			NSPacket packet = NSPacketBufferManager.Pop();

			packet.SetProtocol(protocolId);

			return packet;
		}

		/// <summary>
		/// Destroys the specified packet.
		/// </summary>
		/// <param name="packet">The packet.</param>
		public static void Destroy(NSPacket packet)
		{
			NSPacketBufferManager.Push(packet);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="NSPacket"/> class.
		/// </summary>
		/// <param name="buffer">The buffer.</param>
		/// <param name="owner">The owner.</param>
		public NSPacket(byte[] buffer, INSSession owner = null)
		{
			Buffer = buffer;
			Position = CC.HeaderSize;       // 헤더는 읽을 필요 없으니 그 이후부터 시작한다.
			Owner = owner;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="NSPacket"/> class.
		/// </summary>
		/// <param name="bufferSize">Size of the buffer.</param>
		public NSPacket(int bufferSize = 1024)
		{
			Buffer = new byte[bufferSize];
		}

		/// <summary>
		/// Gets the owner.
		/// </summary>
		/// <value>The owner.</value>
		public INSSession Owner
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the buffer.
		/// </summary>
		/// <value>The buffer.</value>
		public byte[] Buffer
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the position.
		/// </summary>
		/// <value>The position.</value>
		public int Position
		{
			get;
			private set;
		}

		/// <summary>
		/// Records the size.
		/// </summary>
		/// <returns>body size</returns>
		public short RecordSize()
		{
			short bodySize = Convert.ToInt16((Position - CC.HeaderSize));
			byte[] header = BitConverter.GetBytes(bodySize);

			header.CopyTo(Buffer, 0);

			return bodySize;
		}

		/// <summary>
		/// Pops the protocol identifier.
		/// </summary>
		/// <returns></returns>
		public short PopProtocolId()
		{
			return PopInt16();
		}

		/// <summary>
		/// Pops the int16.
		/// </summary>
		/// <returns></returns>
		public short PopInt16()
		{
			short data = BitConverter.ToInt16(Buffer, Position);

			Position += sizeof(short);

			return data;
		}

		/// <summary>
		/// Pops the int32.
		/// </summary>
		/// <returns></returns>
		public int Pop()
		{
			int data = BitConverter.ToInt32(Buffer, Position);

			Position += sizeof(int);

			return data;
		}

		/// <summary>
		/// Pops the string.
		/// </summary>
		/// <returns></returns>
		public string PopString()
		{
			short length = PopInt16();      // 문자열의 최대 길이는 0 ~ 32767
			string data = Encoding.UTF8.GetString(Buffer, Position, length);

			Position += length;

			return data;
		}

		/// <summary>
		/// Pops the blob string.
		/// </summary>
		/// <returns></returns>
		public string PopBlobString()
		{
			int length = Pop();
			string data = Encoding.UTF8.GetString(Buffer, Position, length);

			Position += length;

			return data;
		}

		/// <summary>
		/// Pushes the specified data.
		/// </summary>
		/// <param name="data">The data.</param>
		public void Push(short data)
		{
			byte[] temp = BitConverter.GetBytes(data);

			temp.CopyTo(Buffer, Position);
			Position += temp.Length;
		}

		/// <summary>
		/// Pushes the specified data.
		/// </summary>
		/// <param name="data">The data.</param>
		public void Push(int data)
		{
			byte[] temp = BitConverter.GetBytes(data);

			temp.CopyTo(Buffer, Position);
			Position += temp.Length;
		}

		/// <summary>
		/// Pushes the specified data.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <exception cref="System.ArgumentOutOfRangeException">data length over 32767(short)</exception>
		public void Push(string data)
		{
			if (data.Length > short.MaxValue)
			{
				throw new ArgumentOutOfRangeException(string.Format("data length must be short {0}, use PushBlob method.", short.MaxValue));
			}

			byte[] temp = Encoding.UTF8.GetBytes(data);
			short length = (short)temp.Length;
			byte[] lengthBytes = BitConverter.GetBytes(length);

			lengthBytes.CopyTo(Buffer, Position);
			Position += sizeof(short);

			temp.CopyTo(Buffer, Position);
			Position += temp.Length;
		}

		/// <summary>
		/// Pushes the BLOB.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <exception cref="System.ArgumentOutOfRangeException"></exception>
		public void PushBlob(string data)
		{
			if (data.Length > int.MaxValue)
			{
				throw new ArgumentOutOfRangeException(string.Format("data length must be short {0}.", int.MaxValue));
			}

			byte[] temp = Encoding.UTF8.GetBytes(data);
			byte[] lengthBytes = BitConverter.GetBytes(temp.Length);

			lengthBytes.CopyTo(Buffer, Position);
			Position += sizeof(int);

			temp.CopyTo(Buffer, Position);
			Position += temp.Length;
		}

		/// <summary>
		/// Sets the protocol Id.
		/// </summary>
		/// <param name="protocolId">The protocol identifier.</param>
		public void SetProtocol(short protocolId)
		{
			_protocolId = protocolId;
			Position = CC.HeaderSize;

			Push(protocolId);
		}
	}
}