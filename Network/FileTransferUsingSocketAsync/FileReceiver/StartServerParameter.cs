﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocketFileServer
{
	internal class StartServerParameter
	{
		#region Fields
		private int _listenerCount;
		private bool _appendable;
		#endregion

		#region Constructors
		/// <summary>
		/// StartServerParameter class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="appendable">파일을 이어 받을지 여부를 결정합니다.</param>
		/// <param name="listenerCount">Socket을 listen할 thread의 개수를 결정합니다.</param>
		public StartServerParameter(bool appendable, int listenerCount)
		{
			_listenerCount = listenerCount;
			_appendable = appendable;
		}
		#endregion

		#region Properties
		/// <summary>
		/// ListenerCount를 구하거나 설정합니다.
		/// </summary>
		public int ListenerCount
		{
			get
			{
				return _listenerCount;
			}
			set
			{
				_listenerCount = value;
			}
		}
        
        /// <summary>
        /// Appendable를 구하거나 설정합니다.
        /// </summary>
		public bool Appendable
        {
        	get
        	{
        		return _appendable;
        	}
        	set
        	{
        		_appendable = value;
        	}
        }        
        #endregion
        
	}
}
