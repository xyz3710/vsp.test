﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SocketFileServer
{
	class FileReceiverTestDriver
	{
		static void Main(string[] args)
		{
			FileReceiver fileReceiver = new FileReceiver();

			fileReceiver.ReceivedPath = @"D:\Temp";
			fileReceiver.Started += new FileReceiver.ServiceEventHandler(fileReceiver_Started);
			fileReceiver.Stopped += new FileReceiver.ServiceEventHandler(fileReceiver_Stopped);
			fileReceiver.AcceptClient += new FileReceiver.AcceptClientEventHandler(fileReceiver_AcceptClient);
			fileReceiver.HeaderReceived += new FileReceiver.TransferEventHandler(fileReceiver_HeaderReceived);
			fileReceiver.Receiving += new FileReceiver.TransferEventHandler(fileReceiver_Receiving);
			fileReceiver.Completed += new FileReceiver.TransferEventHandler(fileReceiver_Completed);
			fileReceiver.Error += new FileReceiver.ErrorEventHandler(fileReceiver_Error);
			//fileReceiver.CloseAfterReceive = true;
			fileReceiver.Start(false, 100);

			Console.ReadLine();

			fileReceiver.Stop();
		}

		static void fileReceiver_Error(object sender, ErrorEventArgs e)
		{
			Console.WriteLine(e.Message);
		}

		static void fileReceiver_Completed(object sender, TransferInfoEventArgs e)
		{
			Console.WriteLine("\nCompleted\t{0} : {3,000}% {1:#,##0}, {2:#,##0} in {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), DateTime.Now);
		}

		static void fileReceiver_Receiving(object sender, TransferInfoEventArgs e)
		{
			Console.Write("\rReceiving\t{0} : {3,000}% {1:#,##0}, {2:#,##0}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage));
		}

		static void fileReceiver_HeaderReceived(object sender, TransferInfoEventArgs e)
		{
			Console.WriteLine("\nHeader Received\t{0} : {3,000}% {1:#,##0}, {2:#,##0}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage));
		}

		static void fileReceiver_AcceptClient(object sender, System.Net.Sockets.SocketAsyncEventArgs e)
		{
			Console.WriteLine("{0} connected.", e.RemoteEndPoint.AddressFamily.ToString());
		}

		static void fileReceiver_Stopped(object sender, ServerInfoEventArgs e)
		{
			Console.WriteLine("Server Stopped.");
		}

		static void fileReceiver_Started(object sender, ServerInfoEventArgs e)
		{
			Console.WriteLine("{2} Thread(s) running and waiting to receive file in {0}:{1}", e.ServerIpAddress, e.Port, e.ListenerCount);
		}
	}
}
