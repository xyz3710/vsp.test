﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocketFileServer
{
	/// <summary>
	/// 파일 전송 정보에 대한 EventArgs입니다.
	/// </summary>
	public class ErrorEventArgs : EventArgs
	{
		#region Fields
		private string _message;
		private Exception _exception;
		#endregion

		#region Constructors
		/// <summary>
		/// ErrorEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="message">에러 발생 이유 입니다.</param>
		public ErrorEventArgs(string message)
			: this(message, null)
		{
		}

		/// <summary>
		/// ErrorEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="message">에러 발생 이유 입니다.</param>
		/// <param name="exception">Inner Exception 입니다.</param>
		public ErrorEventArgs(string message, Exception exception)
		{
			_message = message;
			_exception = exception;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Message를 구하거나 설정합니다.
		/// </summary>
		public string Message
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}
        
        /// <summary>
        /// Exception를 구하거나 설정합니다.
        /// </summary>
		public Exception Exception
        {
        	get
        	{
        		return _exception;
        	}
        	set
        	{
        		_exception = value;
        	}
        }        
        #endregion        
	}
}
