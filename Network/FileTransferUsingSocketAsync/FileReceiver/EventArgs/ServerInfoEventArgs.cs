﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocketFileServer
{
	/// <summary>
	/// 서버 접속 정보에 대한 EventArgs입니다.
	/// </summary>
	public class ServerInfoEventArgs : EventArgs
	{
		#region Fields
		private string _serverIpAddress;
		private int _port;
		private int _listenerCount;
		#endregion

		#region Constructors
		/// <summary>
		/// ServerInfoEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="serverIpAddress">서비스 하는 Server의 IpAddress 입니다.</param>
		/// <param name="port">Socket과 Binding되는 Port 번호 입니다.</param>
		public ServerInfoEventArgs(string serverIpAddress, int port)
			: this(serverIpAddress, port, 1)
		{
		}

		/// <summary>
		/// ServerInfoEventArgs class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="serverIpAddress">서비스 하는 Server의 IpAddress 입니다.</param>
		/// <param name="port">Socket과 Binding되는 Port 번호 입니다.</param>
		/// <param name="listenerCount">서비스를 Listening하는 Thread의 개수 입니다.</param>
		public ServerInfoEventArgs(string serverIpAddress, int port, int listenerCount)
		{
			_serverIpAddress = serverIpAddress;
			_port = port;
			_listenerCount = listenerCount;
		}
		#endregion

		#region Properties
		/// <summary>
		/// ServerIpAddress를 구하거나 설정합니다.
		/// </summary>
		public string ServerIpAddress
		{
			get
			{
				return _serverIpAddress;
			}
			set
			{
				_serverIpAddress = value;
			}
		}
        
        /// <summary>
        /// Port를 구하거나 설정합니다.
        /// </summary>
        public int Port
        {
        	get
        	{
        		return _port;
        	}
        	set
        	{
        		_port = value;
        	}
        }
        
        /// <summary>
        /// ListenerCount를 구하거나 설정합니다.
        /// </summary>
        public int ListenerCount
        {
        	get
        	{
        		return _listenerCount;
        	}
        	set
        	{
        		_listenerCount = value;
        	}
        }        
        #endregion        
	}
}
