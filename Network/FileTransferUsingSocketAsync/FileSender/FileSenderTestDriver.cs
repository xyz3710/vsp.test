﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;

namespace SocketFileClient
{
	class FileSenderTestDriver
	{
		private static string fileName = @"c:\Windows\System32\at.exe";

		static void Main(string[] args)
		{
			FileSender fileSender = null;
			//fileName = @"X:\김기원\_0\2.avi";
			//string fileName = @"D:\Music\[MV] MC 몽 - 서커스.avi";

			if (args.Length == 2)
			{
				fileSender = new FileSender(args[0]);
				fileName = args[1];
			}	
			else
			{
				fileSender = new FileSender();
				Console.WriteLine("FileSender.exe [Server IpAddress] [Filename]");
				return;
			}

			fileSender.Connected += new FileSender.ServiceEventHandler(fileSender_Connected);
			fileSender.Disconnected += new FileSender.ServiceEventHandler(fileSender_Disconnected);
			fileSender.HeaderSended += new FileSender.TransferEventHandler(fileSender_HeaderSended);
			fileSender.Sending += new FileSender.TransferEventHandler(fileSender_Sending);
			fileSender.Completed += new FileSender.TransferEventHandler(fileSender_Completed);
			fileSender.Error += new FileSender.ErrorEventHandler(fileSender_Error);
			
			List<string> list = new List<string>();
			list.Add(@"c:\windows\system32\atl.dll");
			list.Add(@"c:\windows\system32\atl71.dll");

			fileSender.SendFile(fileName);
		}

		#region EventHandler
		static void fileSender_Error(object sender, ErrorEventArgs e)
		{
			Console.WriteLine("{0}", e.Message);
		}

		static void fileSender_Completed(object sender, TransferInfoEventArgs e)
		{
			Console.WriteLine("\nCompleted\t{0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		static void fileSender_Sending(object sender, TransferInfoEventArgs e)
		{
			Console.Write("\rSending\t{0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		static void fileSender_HeaderSended(object sender, TransferInfoEventArgs e)
		{
			Console.WriteLine("Header Sended\t{0} : {3,000}% {1:#,##0}, {2:#,##0} to {4}", e.FileName, e.CurrentSize, e.TotalSize, Math.Round(e.Percentage), e.ServerIpAddress);
		}

		static void fileSender_Disconnected(object sender, ServerInfoEventArgs e)
		{
			Console.WriteLine("{0}:{1} Disconnected.", e.ServerIpAddress, e.Port, e.ListenerCount);
		}

		static void fileSender_Connected(object sender, ServerInfoEventArgs e)
		{
			Console.WriteLine("{0}:{1} Connected.", e.ServerIpAddress, e.Port, e.ListenerCount);
		}
		#endregion

		private static void SendFileThread(object serverIp)
		{
			FileSender fileSender = new FileSender(serverIp as string);

			lock (fileSender)
			{
				fileSender.Connected += new FileSender.ServiceEventHandler(fileSender_Connected);
				fileSender.Disconnected += new FileSender.ServiceEventHandler(fileSender_Disconnected);
				fileSender.HeaderSended += new FileSender.TransferEventHandler(fileSender_HeaderSended);
				fileSender.Sending += new FileSender.TransferEventHandler(fileSender_Sending);
				fileSender.Completed += new FileSender.TransferEventHandler(fileSender_Completed);
				fileSender.Error += new FileSender.ErrorEventHandler(fileSender_Error);

				List<string> list = new List<string>();

				list.AddRange(Directory.GetFiles(@"c:\WINDOWS\system32", "c*.nls", SearchOption.TopDirectoryOnly));
				//list.Add(@"c:\Windows\System32\at.exe");
				//list.Add(@"c:\Windows\System32\bootcfg.exe");

				//fileSender.SendFile(@"x:\김기원\_0\1.avi");
				//fileSender.SendFile(@"E:\_2\한발\한발28차세미나(김태영).zip");
				//list.AddRange(Directory.GetFiles(@"E:\_2", "*.zip", SearchOption.AllDirectories));
				fileSender.SendFile(list.ToArray());
			}
		}
	}
}
