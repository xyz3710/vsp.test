﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Management;

namespace GetNicInfo
{
	class Program
	{
		static void Main(string[] args)
		{
			ManagementObjectSearcher query = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled='TRUE'");
			ManagementObjectCollection queryCol = query.Get();

			foreach (ManagementObject mo in queryCol)
			{
				string[] address = (string[])mo["IPAddress"];
				string[] subnets = (string[])mo["IPSubnet"];
				string[] defaultgateways = (string[])mo["DefaultIPGateway"];

				Console.WriteLine("Network Card: {0}", mo["Description"]);
				Console.WriteLine("    MAC Address: {0}", mo["MACAddress"]);

				if (address != null)
					foreach (string ipaddress in address)
						Console.WriteLine("    IP Address: {0}", ipaddress);

				if (subnets != null)
					foreach (string subnet in subnets)
						Console.WriteLine("    Subnet Mask: {0}", subnets);

				if (defaultgateways != null)
					foreach (string defaultgateway in defaultgateways)
						Console.WriteLine("    Gateway: {0}", defaultgateway);
			}
		}
	}
}
