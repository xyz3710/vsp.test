﻿/**********************************************************************************************************************/
/*	Domain		:	IISRecycleAppPool.Program
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2009년 2월 4일 수요일 오후 5:10
/*	Purpose		:	IIS6, 7 버전의 Application Pool을 Recycle 시킵니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	Test paramenters : /a:"iDASiT webServices" /r/a:"테스트 1-1_1" /r
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 2월 4일 수요일 오후 5:11
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Management;
using System.Windows.Forms;

namespace IISRecycleAppPool
{
	/// <summary>
	/// IIS의 Application Pool을 관리합니다.
	/// </summary>
	public class IISAppPoolManager
	{
		private const string IIS_NAMESPACE = @"\\.\root\MicrosoftIISv2";
		private const string APP_POOL_NAMESPACE = "W3SVC/AppPools/";

		/// <summary>
		/// AppPool List를 구합니다.
		/// </summary>
		/// <returns>AppPool List</returns>
		public static List<string> GetAppPoolList()
		{
			List<string> pools = new List<string>();
			ManagementScope scope = new ManagementScope(IIS_NAMESPACE);

			try
			{
				scope.Connect();
			}
			catch (ManagementException ex)
			{
				if (ex.ErrorCode == ManagementStatus.InvalidNamespace)
					return null;
			}

			if (scope.IsConnected == true)
			{
				SelectQuery query = new SelectQuery("select * from IIsApplicationPool");
				ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);

				foreach (ManagementObject appPool in searcher.Get())
				{
					string poolName = appPool.Properties["Name"].Value.ToString();
					string[] poolNames = poolName.Split('/');

					// APP_POOL_NAMESPACE이 IIS 버전에 따라서 Pascal Case 또는 Upper Case로 나오므로 Replace를 시켜준다.
					if (poolNames.Length == 3)
						pools.Add(poolNames[2]);
					else
					{
						pools.Add(poolName.Replace(APP_POOL_NAMESPACE, string.Empty));
						pools.Add(poolName.Replace(APP_POOL_NAMESPACE.ToUpper(), string.Empty));
					}
				}
			}

			return pools;
		}

		/// <summary>
		/// 존재하는 Application Pool을 Recycling 합니다.
		/// </summary>
		/// <param name="appPoolNames">Recycling할 AppPoolName</param>
		/// <returns></returns>
		public static bool RecycleAppPool(string appPoolName)
		{
			List<string> appPoolNames = new List<string>(1);

			appPoolNames.Add(appPoolName);

			return RecycleAppPool(appPoolNames)[appPoolName];
		}

		/// <summary>
		/// 존재하는 Application Pool을 Recycling 합니다.
		/// </summary>
		/// <param name="appPoolNames">Recycling할 AppPoolNames</param>
		/// <returns></returns>
		public static Dictionary<string, bool> RecycleAppPool(ICollection<string> appPoolNames)
		{
			Dictionary<string, bool> result = new Dictionary<string, bool>(appPoolNames.Count);

			if (appPoolNames.Count == 0)
				return result;

			List<string> appPoolList = GetAppPoolList();
			ManagementScope scope = new ManagementScope(IIS_NAMESPACE);
			ManagementPath path = new ManagementPath();
			ObjectGetOptions options = new ObjectGetOptions(null, TimeSpan.MaxValue, true);
			ManagementObject appPool = null;

			try
			{
				scope.Connect();
			}
			catch (ManagementException ex)
			{
				if (ex.ErrorCode == ManagementStatus.InvalidNamespace)
					throw new Exception("지원하지 않는 WMI Name 이거나 IIS 6.0 이상이 아닙니다.");
			}

			foreach (string appPoolName in appPoolNames)
			{
				if (appPoolList.Exists(delegate(string name)
				{
					if (name.ToLower() == appPoolName.ToLower())
						return true;

					return false;
				}) == false)
				{
					result.Add(appPoolName, false);

					continue;
				}

				if (scope.IsConnected == true)
				{
					// NOTICE: Win2003과 Win2008이 틀린다 Win2008은 Management code로 관리
					// by KIMKIWON\xyz37 in 2009년 2월 4일 수요일 오후 4:47
					path.Path = string.Format("IIsApplicationPool.Name='{0}{1}'", 
									APP_POOL_NAMESPACE, 
                                    appPoolName);
					try
					{
						appPool = new ManagementObject(scope, path, null);
						appPool.InvokeMethod("Recycle", null, null);
						result.Add(appPoolName, true);
					}
					catch 
					{
						result.Add(appPoolName, false);
					}
				}
			}

			return result;
		}
	}
}
