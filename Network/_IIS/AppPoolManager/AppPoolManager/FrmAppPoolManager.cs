﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Management;

namespace IISRecycleAppPool
{
	public partial class FrmAppPoolManager : Form
	{
		public FrmAppPoolManager()
		{
			InitializeComponent();
		}

		private void frmRecycleAppPool_Shown(object sender, EventArgs e)
		{
			RefreshAppPools();
		}

		private void btnRefreshAppPools_Click(object sender, EventArgs e)
		{
			RefreshAppPools();
		}

		private void btnRecycle_Click(object sender, EventArgs e)
		{
			int selectedCount = lstAppPools.SelectedIndices.Count;

			if (selectedCount > 0)
			{
				List<string> appPoolNames = new List<string>(selectedCount);

				foreach (string appPoolName in lstAppPools.SelectedItems)
					appPoolNames.Add(appPoolName);

				Dictionary<string, bool> result = RecycleAppPool(appPoolNames);

				foreach (string appPoolName in result.Keys)
					txtResult.Text = txtResult.Text.Insert(0, string.Format("{0} : {1} recycling {2}.{3}",
																	  DateTime.Now,
																	  appPoolName,
																	  result[appPoolName] == true ? "successful" : "failure",
																	  Environment.NewLine));

				lstAppPools.SelectedIndex = -1;
			}
		}

		private void RefreshAppPools()
		{
			lstAppPools.Items.Clear();

			List<string> appPools = IISAppPoolManager.GetAppPoolList();

			if (appPools == null)
				MessageBox.Show("해당 IIS를 지원할 수 없습니다.\r\nIIS 6.0 이후를 지원합니다.", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
			else
			{
				foreach (string appPoolName in appPools)
					lstAppPools.Items.Add(appPoolName);
			}
		}

		private Dictionary<string, bool> RecycleAppPool(List<string> appPoolNames)
		{
			return IISAppPoolManager.RecycleAppPool(appPoolNames);
		}
	}
}