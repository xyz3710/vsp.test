﻿/**********************************************************************************************************************/
/*	Domain		:	IISRecycleAppPool.Program
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2009년 2월 4일 수요일 오후 5:10
/*	Purpose		:	IIS6, 7 버전의 Application Pool을 Recycle 시킵니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	Test paramenters : /a:"iDASiT webServices" /r/a:"테스트 1-1_1" /r
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 2월 4일 수요일 오후 5:11
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using System.Diagnostics;

namespace IISRecycleAppPool
{
	static class Program
	{
		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		static int Main(string[] args)
		{
			int exitCode = 0;
			
			if (args.Length > 0)
			{
				string appPoolName = string.Empty;
				bool showUsage = false;
				List<string> targetAppPoolNames = new List<string>();

				RegexOptions options = RegexOptions.Singleline;
				Regex regex = new Regex(@"/[a,A:]+[""]?(?<Name>[\s\w\-]*)[""]?[\s]*(?<Recycle>/[r,R]+)|(?<Help>/[h,H,?]+)?",
					options);
				StringBuilder input = new StringBuilder();

				foreach (string arg in args)
					input.Append(arg);

				foreach (Match match in regex.Matches(input.ToString()))
				{
					if (match.Groups["Help"].Success == true)
					{
						ShowUsages();
						showUsage = true;

						break;
					}

					if (match.Groups["Name"].Success == true 
						&& match.Groups["Recycle"].Success == true)
						targetAppPoolNames.Add(match.Groups["Name"].Value);
				}

				if (showUsage == false && targetAppPoolNames.Count > 0)
				{
					Dictionary<string, bool> iISAppPoolManagerRecycleAppPool = IISAppPoolManager.RecycleAppPool(targetAppPoolNames);

					foreach (bool result in iISAppPoolManagerRecycleAppPool.Values)
					{
						if (result == true)
							exitCode++;
					}
				}
			}
			else
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				Application.Run(new FrmAppPoolManager());

				exitCode = -1;
			}

			return exitCode;
		}

		private static void ShowUsages()
		{
			string appName = Path.GetFileName(Application.ExecutablePath);

			Console.WriteLine();
			Console.WriteLine("    IIS Application Pool Manager.{0}", Environment.NewLine);
			Console.WriteLine("    {0} [/a:<Application Pool Name> /r] [repeat...]", appName);
			Console.WriteLine();
			Console.WriteLine("    Options : ");
			Console.WriteLine("    \t/a : <Application Pool Name> : Specific Application Pool Name");
			Console.WriteLine("    \t/r : Recycling specific Application Pool");
			Console.WriteLine("    \t/? : Show help message.");
			Console.WriteLine("    \t/h : Show help message.");
			Console.WriteLine();
			Console.WriteLine("    Exit Code : ");
			Console.WriteLine("    \t-1 : GUI use");
			Console.WriteLine("    \t> 0(Greater than 0) : Recycling success count.");
			Console.WriteLine();
			Console.WriteLine("    ex) : {0} /a:\"Web Services1\" /r /a:\"Web Services2\" /r", appName);
			Console.WriteLine("          Recycling \"Web Services1\" and \"Web Services2\" Application Pool");
			Console.WriteLine();
		}
	}
}