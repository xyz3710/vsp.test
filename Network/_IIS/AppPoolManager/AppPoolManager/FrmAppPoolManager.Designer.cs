﻿namespace IISRecycleAppPool
{
	partial class FrmAppPoolManager
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAppPoolManager));
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.btnRefreshAppPools = new System.Windows.Forms.Button();
			this.lstAppPools = new System.Windows.Forms.ListBox();
			this.txtResult = new System.Windows.Forms.TextBox();
			this.btnRecyle = new System.Windows.Forms.Button();
			this.tlpMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 2;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.Controls.Add(this.btnRefreshAppPools, 0, 0);
			this.tlpMain.Controls.Add(this.lstAppPools, 0, 1);
			this.tlpMain.Controls.Add(this.txtResult, 0, 2);
			this.tlpMain.Controls.Add(this.btnRecyle, 1, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 3;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
			this.tlpMain.Size = new System.Drawing.Size(470, 338);
			this.tlpMain.TabIndex = 0;
			// 
			// btnRefreshAppPools
			// 
			this.btnRefreshAppPools.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnRefreshAppPools.Location = new System.Drawing.Point(3, 3);
			this.btnRefreshAppPools.Name = "btnRefreshAppPools";
			this.btnRefreshAppPools.Size = new System.Drawing.Size(229, 22);
			this.btnRefreshAppPools.TabIndex = 0;
			this.btnRefreshAppPools.Text = "&Refresh App Pools";
			this.btnRefreshAppPools.UseVisualStyleBackColor = true;
			this.btnRefreshAppPools.Click += new System.EventHandler(this.btnRefreshAppPools_Click);
			// 
			// lstAppPools
			// 
			this.tlpMain.SetColumnSpan(this.lstAppPools, 2);
			this.lstAppPools.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstAppPools.FormattingEnabled = true;
			this.lstAppPools.ItemHeight = 12;
			this.lstAppPools.Location = new System.Drawing.Point(3, 31);
			this.lstAppPools.Name = "lstAppPools";
			this.lstAppPools.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.lstAppPools.Size = new System.Drawing.Size(464, 208);
			this.lstAppPools.TabIndex = 2;
			// 
			// txtResult
			// 
			this.txtResult.BackColor = System.Drawing.SystemColors.Window;
			this.tlpMain.SetColumnSpan(this.txtResult, 2);
			this.txtResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtResult.Location = new System.Drawing.Point(3, 248);
			this.txtResult.Multiline = true;
			this.txtResult.Name = "txtResult";
			this.txtResult.ReadOnly = true;
			this.txtResult.Size = new System.Drawing.Size(464, 87);
			this.txtResult.TabIndex = 2;
			// 
			// btnRecyle
			// 
			this.btnRecyle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnRecyle.Location = new System.Drawing.Point(238, 3);
			this.btnRecyle.Name = "btnRecyle";
			this.btnRecyle.Size = new System.Drawing.Size(229, 22);
			this.btnRecyle.TabIndex = 1;
			this.btnRecyle.Text = "Re&cycle";
			this.btnRecyle.UseVisualStyleBackColor = true;
			this.btnRecyle.Click += new System.EventHandler(this.btnRecycle_Click);
			// 
			// FrmAppPoolManager
			// 
			this.AcceptButton = this.btnRefreshAppPools;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(470, 338);
			this.Controls.Add(this.tlpMain);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FrmAppPoolManager";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "IIS App Pool Manager";
			this.Shown += new System.EventHandler(this.frmRecycleAppPool_Shown);
			this.tlpMain.ResumeLayout(false);
			this.tlpMain.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.Button btnRefreshAppPools;
		private System.Windows.Forms.ListBox lstAppPools;
		private System.Windows.Forms.TextBox txtResult;
		private System.Windows.Forms.Button btnRecyle;
	}
}

