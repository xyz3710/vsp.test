﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.NetworkInformation;
using System.Reflection;

namespace GetRealNic
{
	class Program
	{
		static void Main(string[] args)
		{
			ShowNICInfo();

			Console.ReadLine();
		}

		private static void ShowNICInfo()
		{
			NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
			int realAdapterIndex = -1;
			var isIP6 = false;

			for (int i = 0; i < nics.Length; i++)
			{
				if (nics[i].OperationalStatus != OperationalStatus.Up)
				{
					continue;
				}

				Console.WriteLine("Name : {0}", nics[i].Name);
				Console.WriteLine("NetworkInterfaceType : {0}", nics[i].NetworkInterfaceType);
				Console.WriteLine("OperationalStatus : {0}", nics[i].OperationalStatus);

				IPv4InterfaceStatistics adapterStat = (nics[i]).GetIPv4Statistics();
				DisplayStatics(adapterStat);

				UnicastIPAddressInformationCollection uniCast = (nics[i]).GetIPProperties().UnicastAddresses;

				if (uniCast != null)
				{
					foreach (UnicastIPAddressInformation uni in uniCast)
					{
						isIP6 = uni.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6;

						if (adapterStat.UnicastPacketsReceived > 0
							&& adapterStat.UnicastPacketsSent > 0
							&& nics[i].NetworkInterfaceType != NetworkInterfaceType.Loopback)
						{
							Console.WriteLine("This is Real NIC : {0}", uni.Address);
							realAdapterIndex = i;
						}

						Console.WriteLine("IP{0} Address ..... : {1}", isIP6 == true ? "6" : " ", uni.Address);
					}
				}

				Console.WriteLine("");
			}

			Console.WriteLine("Real Interface Name : {0}, {1}",
				nics[realAdapterIndex].Name,
				nics[realAdapterIndex].GetIPProperties().UnicastAddresses[0].Address);
		}

		private static void DisplayStatics(IPv4InterfaceStatistics iPv4InterfaceStatistics)
		{
			Type type = iPv4InterfaceStatistics.GetType();

			foreach (PropertyInfo pi in type.GetProperties())
			{
				Console.WriteLine("{0} : {1}", pi.Name, pi.GetValue(iPv4InterfaceStatistics, null));
			}
		}
	}
}
