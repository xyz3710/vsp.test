﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using iDASiT.FX.Win.Messenger;
using System.Diagnostics;

namespace FileTransferUsingSocket
{
	[Serializable]
	public class SerializableClass : CommandBase
	{
		private string _name;
		private int _age;
		private bool _sex;
		private List<string> _jobs;

		/// <summary>
		/// SerializableClass class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public SerializableClass()
		{
			_jobs = new List<string>();
		}

		/// <summary>
		/// SerializableClass class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="age"></param>
		/// <param name="sex"></param>
		public SerializableClass(string name, int age, bool sex)
			: this()
		{
			_name = name;
			_age = age;
			_sex = sex;			
		}

		#region Properties
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// Age를 구하거나 설정합니다.
		/// </summary>
		public int Age
		{
			get
			{
				return _age;
			}
			set
			{
				_age = value;
			}
		}

		/// <summary>
		/// Sex를 구하거나 설정합니다.
		/// </summary>
		public bool Sex
		{
			get
			{
				return _sex;
			}
			set
			{
				_sex = value;
			}
		}

		public List<string> Jobs
		{
			get
			{
				return _jobs;
			}
			set
			{
				_jobs = value;
			}
		}
		#endregion

		public override string Description
		{
			get
			{
				return "SerializableClass";
			}
		}

		public override void Execute()
		{
			ProcessStartInfo psi = new ProcessStartInfo(@"C:\WINDOWS\system32\cmd.exe");

			psi.ErrorDialog = true;

			Process.Start(psi);
		}
	}
}
