/**********************************************************************************************************************/
/*	Domain		:	FTClient.FTClient
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 8월 28일 목요일 오후 4:04
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	Header는 원본 길이 8byte, 압출 후 길이 8byte로 구성되어 있다
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using FileTransferUsingSocket;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Soap;
using System.Xml.Serialization;

namespace FTClient
{
	public class FTClient
	{
		private const int PORT = 1980;
		private const int SEGMENT_SIZE = 1024;

		private IPAddress _serverIPAddress;

		/// <summary>
		/// FTClient class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public FTClient()
		{
			CurrentMessage = "Idle...";

			if (_serverIPAddress == null)
			{
				IPAddress[] ipAddress = Dns.GetHostAddresses(Dns.GetHostName());

				if (ipAddress.Length > 0)
					_serverIPAddress = ipAddress[0];
			}
		}

		public FTClient(string ipAddress)
			: this ()
		{
			_serverIPAddress = IPAddress.Parse(ipAddress);
		}

		#region Properties
		/// <summary>
		/// CurrentMessage를 구하거나 설정합니다.
		/// </summary>
		public string CurrentMessage
		{
			set
			{
				Console.WriteLine(value);				
			}
		}        
        #endregion

		public void SendFile<T>(T serializableClass)
		{
			Socket clientSocket = null;

			try
			{
				XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
				byte[] serializedData = null;
				long beforeCompressLength = 0L;
				long afterCompressLength = 0L;

				// Memory Stream으로 Serialize
				using (MemoryStream writeMS = new MemoryStream())
				{
					xmlSerializer.Serialize(writeMS, serializableClass);
					beforeCompressLength = writeMS.Length;

					if (writeMS.Length > SEGMENT_SIZE)
					{
						#region 압축
						using (MemoryStream compressedMS = new MemoryStream())
						{
							GZipStream gZip = new GZipStream(compressedMS, CompressionMode.Compress);
							byte[] compressedData = writeMS.ToArray();

							gZip.Write(compressedData, 0, compressedData.Length);
							gZip.Flush();
							gZip.Close();

							serializedData = compressedMS.ToArray();
							compressedMS.Close();

							CurrentMessage = "Compressed.";
						}
						#endregion						

						afterCompressLength = serializedData.Length;
					}
					else
						serializedData = writeMS.ToArray();

					writeMS.Close();
				}

				IPEndPoint ep = new IPEndPoint(_serverIPAddress, PORT);

				clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

				CurrentMessage = "Connection to server...";

				clientSocket.Connect(ep);

				CurrentMessage = "Header sending...";

				byte[] serializedDataLength = BitConverter.GetBytes(beforeCompressLength);			// 원본 stream 크기를 저장한다.
				byte[] compressedLength = BitConverter.GetBytes(afterCompressLength);			// 원본 stream 크기를 저장한다.
				byte[] headerData = new byte[serializedDataLength.Length + compressedLength.Length];

				// NOTICE: Header는 원본 길이 8byte, 압출 후 길이 8byte로 구성되어 있다
				// by KIMKIWON\xyz37 in 2008년 8월 28일 목요일 오후 4:04
				// 압축 여부를 나타내는 flag
				serializedDataLength.CopyTo(headerData, 0);
				compressedLength.CopyTo(headerData, serializedDataLength.Length);

				// Header 전송
				int sendLength = clientSocket.Send(headerData);
				int bufferSize = SEGMENT_SIZE * SEGMENT_SIZE;
				long totalSendingLen = 0L;
				int sendingLen = 0;
				int totalLength = serializedData.Length;

				// 전체 크기 보다 bufferSize가 크면 기본 bufferSize를 줄인다.
				if (totalLength < bufferSize)
					bufferSize = totalLength;

				// 데이터를 읽어들인다.
				while ((sendingLen = clientSocket.Send(serializedData, (int)totalSendingLen, bufferSize, SocketFlags.Partial)) > 0)
				{
					totalSendingLen += sendingLen;

					Console.Write("\rTotal / Current : {0:#,##0} / {1:#,##0}", totalLength, totalSendingLen);

					// 기본 bufferSize 보다 남은 크기가 크면 기본 bufferSize는 (전체 크기 - 읽은 총합) 이다.
					bufferSize = (int)Math.Min(bufferSize, totalLength - totalSendingLen);
				}

				CurrentMessage = "\r\nDisconnecting...";
				CurrentMessage = "Stream transferred";
			}
			catch (SocketException ex)
			{
				CurrentMessage = string.Format("Stream sending fail. {0}", ex.Message);
			}
			finally
			{
				if (clientSocket != null)
					clientSocket.Close();
			}
		}
	}
}
