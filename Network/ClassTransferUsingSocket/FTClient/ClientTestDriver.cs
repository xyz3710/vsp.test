﻿using System;
using System.Collections.Generic;
using System.Text;
using FileTransferUsingSocket;

namespace FTClient
{
	class ClientTestDriver
	{
		static void Main(string[] args)
		{
			FTClient fTClient = null;

			if (args.Length == 1)
				fTClient = new FTClient(args[0]);	
			else
			{
				fTClient = new FTClient();
				Console.WriteLine("FTClient.exe [Server IpAddress] [Filename]");
			}

			// Seiralize
			SerializableClass serializableClass = new SerializableClass("김기원", 35, true);

			for (int i = 0; i < 1000000; i++)
			{
				serializableClass.Jobs.Add("프로그래머");
				serializableClass.Jobs.Add("시스템 엔지니어");
				serializableClass.Jobs.Add("SM");
			}

			fTClient.SendFile<SerializableClass>(serializableClass);
		}
	}
}
