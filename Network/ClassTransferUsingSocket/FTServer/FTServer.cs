using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using FileTransferUsingSocket;
using System.IO.Compression;
using System.Xml.Serialization;
using System.Reflection;
using System.Collections;

namespace FTServer
{
	public class FTServer
	{
		private const int PORT = 1980;
		private const int BackLog = 100;
		private const int SEGMENT_SIZE = 1024;
		private IPEndPoint _iPEndPoint;
		private Socket _socket;
		private string _receivedPath;
		
		/// <summary>
		/// FTServer class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public FTServer()
		{
			CurrentMessage = "Stopped...";
			_iPEndPoint = new IPEndPoint(IPAddress.Any, PORT);
			_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
			_socket.Bind(_iPEndPoint);
		}

		#region Properties
        /// <summary>
        /// ReceivedPath를 구하거나 설정합니다.
        /// </summary>
        public string ReceivedPath
        {
        	get
        	{
        		return _receivedPath;
        	}
        	set
        	{
        		_receivedPath = value;
        	}
        }
        
        /// <summary>
        /// CurrentMessage를 구하거나 설정합니다.
        /// </summary>
		public string CurrentMessage
        {
        	set
        	{
				Console.WriteLine(value);
        	}
        }
        
        #endregion

		public T StartServer<T>(bool appendable)
			where T : new ()
		{
			Socket clientSocket = null;
			T deSerializedClass = new T();

			try
			{
				CurrentMessage = "Starting...";
				_socket.Listen(BackLog);

				CurrentMessage = "Running and waiting to receive stream.";

				clientSocket = _socket.Accept();

				// Header 정보를 읽어들인다.
				byte[] headerData = new byte[sizeof(long) * 2];		// 압축전 데이터 길이, 압축 후 데이터 길이

				CurrentMessage = "Header receiving...";

				int receivedLength = clientSocket.Receive(headerData);

				long originalLength = BitConverter.ToInt64(headerData, 0);
				long compressedLength = BitConverter.ToInt64(headerData, sizeof(long));
				bool isCompressed = compressedLength > 0;
				long targetSize = isCompressed == true ? compressedLength : originalLength;
				int bufferSize = SEGMENT_SIZE * SEGMENT_SIZE;
				List<byte> serializedData = new List<byte>();
				long totalReceivedLength = 0L;

				bufferSize = (int)Math.Min(targetSize, bufferSize);

				byte[] receivedData = new byte[bufferSize];

				CurrentMessage = "Receiving data...";

				// 데이터를 읽어들인다.
				while ((receivedLength = clientSocket.Receive(receivedData, (int)bufferSize, SocketFlags.Partial)) > 0)
				{
					serializedData.AddRange(receivedData);
					totalReceivedLength += receivedLength;

					Console.Write("\rTotal / Current : {0:#,##0} / {1:#,##0}", 
                        isCompressed == true ? compressedLength : originalLength, 
                        totalReceivedLength);

					// 기본 bufferSize 보다 남은 크기가 크면 기본 bufferSize는 (전체 크기 - 읽은 총합) 이다.
					bufferSize = (int)Math.Min(bufferSize, targetSize - totalReceivedLength);
				}

				// 버퍼 이상의 데이터는 지운다.
				serializedData.RemoveRange((int)totalReceivedLength, (int)(serializedData.Count - totalReceivedLength));

				XmlSerializer xmlDSerializer = new XmlSerializer(deSerializedClass.GetType());
				byte[] deSerializedData = new byte[originalLength];

				using (MemoryStream readMS = new MemoryStream(serializedData.ToArray()))
				{
					if (isCompressed == true)
					{
						CurrentMessage = string.Format("Compressed : original : {0}, compressed : {1}", originalLength, compressedLength);

						using (GZipStream gZipD = new GZipStream(readMS, CompressionMode.Decompress))
						{ 
							int readLength = gZipD.Read(deSerializedData, 0, (int)originalLength);

							if (readLength != deSerializedData.Length)
								throw new Exception("전송 받은 데이터 길이가 원본과 틀립니다.");
						}

						using (MemoryStream compressedMS = new MemoryStream(deSerializedData))
						{
							deSerializedClass = (T)xmlDSerializer.Deserialize(compressedMS);
						}
					}
					else
						deSerializedClass = (T)xmlDSerializer.Deserialize(readMS);
				}

				CurrentMessage = "\r\nEnd stream...";
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error : {0}", ex.Message);
				CurrentMessage = "Stream Receiving error.";
			}
			finally
			{
				if (clientSocket != null)
					clientSocket.Close();

				CurrentMessage = "Received & Saved file\r\nServer Stopped.";
			}

			return deSerializedClass;
		}
	}
}
