﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using FileTransferUsingSocket;
using System.Reflection;
using iDASiT.FX.Win.Messenger;

namespace FTServer
{
	class ServerTestDriver
	{
		static void Main(string[] args)
		{
			FTServer fTServer = new FTServer();

			fTServer.ReceivedPath = @"F:\1";
			SerializableClass serializableClass = fTServer.StartServer<SerializableClass>(false);

			if (serializableClass != null)
			{
				Console.WriteLine("\r\nPrinted DeSerialize class properties.");

				Type type = serializableClass.GetType();

				foreach (PropertyInfo pi in type.GetProperties())
					Console.WriteLine("{0}\t: {1}", pi.Name, pi.GetValue(serializableClass, null));

				if (serializableClass is CommandBase)
					serializableClass.Execute();
			}
			else
				Console.WriteLine("result is null");

			Console.ReadLine();
		}
	}
}
