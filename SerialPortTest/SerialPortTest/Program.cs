﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace SerialPortTest
{
	class Program
	{
		static void Main(string[] args)
		{
			var seInfo = Program.GetPnPSerialPorts();

			Console.WriteLine("By WMI");

			foreach (var item in seInfo)
			{
				Console.WriteLine("{0}\t{1}", item.Name, item.Description);
			}

			Console.WriteLine();

			Console.WriteLine("By SerialPort.GetPortNames");
			foreach (string item in SerialPort.GetPortNames())
			{
				Console.WriteLine(item);
			}

			Console.WriteLine("\r\nend");
			Console.ReadLine();
		}

		public static List<DeviceCompactInfo> GetPnPSerialPorts()
		{
			return GetPnPDeviceInfo("%(COM%")
				.Select(x => new DeviceCompactInfo
				{
					Name = x.Name.Substring(x.Name.LastIndexOf("(COM")).Replace("(", string.Empty).Replace(")", string.Empty),
					Description = x.Name,
					Manufacturer = x.Manufacturer,
					DeviceID = x.DeviceID,
					SystemName = x.SystemName,
				})
				.ToList();
		}

		public static List<DeviceCompactInfo> GetPnPDeviceInfo(string captionLikeCondition)
		{
			var selectQuery = "SELECT Caption, Description, Manufacturer, SystemName, DeviceID From Win32_PnPEntity ";
			var query = string.Format("{0} WHERE ConfigManagerErrorCode = 0 and Caption like '{1}' ", selectQuery, captionLikeCondition);
			var searcher = new System.Management.ManagementObjectSearcher(query);
			var pnpList = searcher.Get().Cast<System.Management.ManagementBaseObject>()
				.Select(x => new DeviceCompactInfo
				{
					Name = Convert.ToString(x["Caption"]),
					Description = Convert.ToString(x["Description"]),
					Manufacturer = Convert.ToString(x["Manufacturer"]),
					SystemName = Convert.ToString(x["SystemName"]),
					DeviceID = Convert.ToString(x["DeviceID"]),
				})
				.ToList();

			return pnpList;
		}
	}

	/// <summary>
	/// Device의 간단 정보입니다.
	/// </summary>
	[System.Diagnostics.DebuggerDisplay("Name:{Name}, Description:{Description}, Manufacturer:{Manufacturer}, SystemName:{SystemName}, DeviceID:{DeviceID}", Name = "DeviceCompactInfo")]
	public class DeviceCompactInfo
	{
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		/// <value>Name를 반환합니다.</value>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Description를 구하거나 설정합니다.
		/// </summary>
		/// <value>Description를 반환합니다.</value>
		public string Description
		{
			get;
			set;
		}

		/// <summary>
		/// Manufacturer를 구하거나 설정합니다.
		/// </summary>
		/// <value>Manufacturer를 반환합니다.</value>
		public string Manufacturer
		{
			get;
			set;
		}

		/// <summary>
		/// SystemName를 구하거나 설정합니다.
		/// </summary>
		/// <value>SystemName를 반환합니다.</value>
		public string SystemName
		{
			get;
			set;
		}

		/// <summary>
		/// DeviceID를 구하거나 설정합니다.
		/// </summary>
		/// <value>DeviceID를 반환합니다.</value>
		public string DeviceID
		{
			get;
			set;
		}
	}
}
