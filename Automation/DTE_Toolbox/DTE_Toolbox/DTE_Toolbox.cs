﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EnvDTE;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.SqlServer.MessageBox;
using System.Collections;
using System.IO;

namespace DTE_Toolbox
{
	public partial class DTE_Toolbox : Form
	{
		public DTE_Toolbox()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			try
			{
				DTE dte = new DTE();
				Window window = dte.Windows.Item(Constants.vsWindowKindToolbox);
				ToolBox toolBox = window.Object as ToolBox;
				ToolBoxTabs toolBoxTabs = toolBox.ToolBoxTabs;

				MessageBox.Show(string.Format("툴 박스 수 : {0}", toolBoxTabs.Count));
			}
			catch (Exception ex)
			{
				DoException("알 수 없는 오류 첫번째 입니다.", ex);
			}
		}

		/// <summary>
		/// Child Form의 Exception 처리를 UserFriendly한 화면으로 처리합니다.
		/// </summary>
		/// <param name="exception"></param>
		public DialogResult DoException(Exception exception)
		{
			return DoException(string.Empty, exception);
		}

		public DialogResult DoException(string message, Exception exception)
		{
			DialogResult dialogResult = DialogResult.None;
			ApplicationException applicationException = new ApplicationException(message, exception);

			applicationException.Source = exception.Source;

			dialogResult = LoaderExceptionMessageBox(applicationException);

			switch (dialogResult)
			{
				case System.Windows.Forms.DialogResult.None:
					
					break;
				case System.Windows.Forms.DialogResult.OK:
					
					break;
				case System.Windows.Forms.DialogResult.Cancel:
					
					break;
				case System.Windows.Forms.DialogResult.Abort:
					Application.Exit();

					break;
				case System.Windows.Forms.DialogResult.Retry:
					
					break;
				case System.Windows.Forms.DialogResult.Ignore:
					
					break;
				case System.Windows.Forms.DialogResult.Yes:
					
					break;
				case System.Windows.Forms.DialogResult.No:
					
					break;
			}

			return dialogResult;
		}

		private DialogResult LoaderExceptionMessageBox(Exception exception)
		{
			ExceptionMessageBox exMessageBox = 
				new ExceptionMessageBox(exception, 
                    ExceptionMessageBoxButtons.AbortRetryIgnore,
					ExceptionMessageBoxSymbol.Exclamation, 
                    ExceptionMessageBoxDefaultButton.Button2
					);

			exMessageBox.OnCopyToClipboard += new CopyToClipboardEventHandler(exMessageBox_OnCopyToClipboard);

			exMessageBox.Caption = exception.Source;

			return exMessageBox.Show(Owner);
		}

		void exMessageBox_OnCopyToClipboard(object sender, CopyToClipboardEventArgs e)
		{
			ExceptionMessageBox exceptionMessageBox = sender as ExceptionMessageBox;

			if (exceptionMessageBox != null)
			{
				Exception exception = exceptionMessageBox.Message;
				Queue<Exception> queue = new Queue<Exception>();
				StringBuilder messages = new StringBuilder();
				string blockBar = Environment.NewLine.PadLeft(80, '=');
				string bar = Environment.NewLine.PadLeft(80, '-');

				queue.Enqueue(exception);

				while (queue.Count > 0)
                {
					Exception ex = queue.Dequeue();

					if (ex == null)
						continue;

					messages.AppendFormat("{0}{1}", Environment.NewLine, blockBar);
					messages.AppendFormat("{0}({1}){2}", ex.Message, ex.Source, Environment.NewLine);

					if (ex.Data.Count > 0)
					{
						messages.Append(bar);
						messages.AppendFormat("{0}{1}", "추가데이터 :", Environment.NewLine);

						foreach (object key in ex.Data.Keys)
							messages.AppendFormat("\t{0}\t: {1}{2}", key, ex.Data[key], Environment.NewLine);
					}

					if (string.IsNullOrEmpty(ex.StackTrace) == false)
					{
						messages.Append(bar);
						messages.AppendFormat("{0}{1}", "프로그램위치 :", Environment.NewLine);
						messages.AppendFormat("{0}{1}", ex.StackTrace, Environment.NewLine);
					}

					if (ex.InnerException != null)
						queue.Enqueue(ex.InnerException);
				}

				// TODO: 프로그램 기본 경로를 세팅 파일에서 구한다.
				// by KIMKIWON\xyz37 in 2007년 10월 4일 목요일 오후 1:40
				string menuPath = string.Format(@"{0}\iDASiT", Environment.GetFolderPath(System.Environment.SpecialFolder.ProgramFiles));
				string exceptionPath = string.Format(@"{0}\Log", menuPath);
				DateTime now = DateTime.Now;
				string fileName = string.Format("exception_{0}_{1}_{2,00}{3,00}{4,00}.log", 
                                      exception.Source, 
                                      now.ToShortDateString().Replace("-", string.Empty), 
                                      now.Hour, 
                                      now.Minute, 
                                      now.Second);
				string filePath = string.Format(@"{0}\{1}", exceptionPath, fileName);

				if (File.Exists(exceptionPath) == false)
					Directory.CreateDirectory(exceptionPath);
				
				using (StreamWriter streamWriter = new StreamWriter(filePath))
                {
					streamWriter.Write(messages.ToString());

					streamWriter.Flush();
					streamWriter.Close();
                }

				MessageBox.Show("Message를 처리하였습니다.", "오류 메세지 처리 성공", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}			
		}
	}
}