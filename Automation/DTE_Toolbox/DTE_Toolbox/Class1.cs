﻿using EnvDTE;
using EnvDTE80;
using System.Windows.Forms;
using System;

namespace DTE_Toolbox
{
	public class Test
	{
		public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
		{
			_applicationObject = (DTE2)application;
			_addInInstance = (AddIn)addInInst;
			OutputToolWindow(_applicationObject);
		}

		public void OutputToolWindow(DTE2 dte)
		{
			OutputWindow myOut;
			myOut = _applicationObject.ToolWindows.OutputWindow;
	
			OutputWindowPane myPane;
			String txt = null;
			MessageBox.Show("Creating an output window.");

			myPane = myOut.OutputWindowPanes.Add("My output");
			myPane.Activate();
			MessageBox.Show("Adding some text to the output window...");

			myPane.OutputString("This is the collection of tool windows,reached through the Output Window object:" + "\n");

			foreach (EnvDTE80.Window2 tempWindow in myOut.Parent.Collection)
				txt = txt + (tempWindow.Caption + "\n");

			MessageBox.Show("Displaying all the tool window captions in the output window...");

			myPane.OutputString(txt);
		}

	}
}

