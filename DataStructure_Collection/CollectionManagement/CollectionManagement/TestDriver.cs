using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionManagement
{
	class TestDriver
	{
		static void Main(string[] args)
		{
			TestDriver testDriver = new TestDriver();

			testDriver.Run();
		}

		private void Run()
		{
			List<Employee> col = new List<Employee>();

			col.Add(new Employee("Xyz37", 40000));
			col.Add(new Employee("John", 25500));
			col.Add(new Employee("Smith", 32000));

			#region Sorting
			PrintCollection("Before Sorting", col);
			
			Comparison<Employee> compareBySalary = new Comparison<Employee>(CompareBySalary);

			col.Sort(compareBySalary);

			PrintCollection("After Sorting by Salary", col);

			Comparison<Employee> compareByName = delegate(Employee emp1, Employee emp2)
			{
				return 0 - emp1.Name.CompareTo(emp2.Name);
			};

			col.Sort(compareByName);

			PrintCollection("After Sorting by Name desc", col);
			#endregion

			#region Searching List

			#endregion

		}

		private void PrintCollection(string msg, List<Employee> col)
		{
			Console.WriteLine(msg);

			foreach (Employee employee in col)
				Console.WriteLine(employee);

			Console.WriteLine();
		}                        

		private int CompareBySalary(Employee emp1, Employee emp2)
		{
			return emp1.Salary.CompareTo(emp2.Salary);
		}
	}
}
