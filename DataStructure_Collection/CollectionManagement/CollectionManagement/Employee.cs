using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionManagement
{
	public class Employee
	{
		private string _name;
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		private double _salary;
		public double Salary
		{
			get
			{
				return _salary;
			}
			set
			{
				_salary = value;
			}
		}

		/// <summary>
		/// Initializes a new instance of the Employee class.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="salary"></param>
		public Employee(string name, double salary)
		{
			_name = name;
			_salary = salary;
		}

		public override string ToString()
		{
			return string.Format("Name : {0}, Salary : {1}", Name, Salary);
		}
	}
}
