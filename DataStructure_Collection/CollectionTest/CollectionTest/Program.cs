﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionTest
{
	class Program
	{
		private delegate bool Existing(List<string> data, string item);

		static void Main(string[] args)
		{
			List<Employee> col = new List<Employee>();

			col.Add(new Employee("기원", 1000d));
			col.Add(new Employee("태영", 2000d));
			col.Add(new Employee("영재", 500d));

			#region For Test
			/*
			Predicate<Employee> exists = delegate(Employee employee)
			{

			};

							bool exist = _harvestingList.Exists(delegate(string keyPath)
				{
					return keyPath == harvesting.KeyPath;
				});

			if (col.Exists(exists))
			*/
			#endregion

			double total = 0d;

			col.ForEach(delegate(Employee employee)
			{
				total += employee.Salary;
			});

			Console.WriteLine(total);
		}
	}

	public class Employee
	{
		private string _name;
		private double _salary;

		#region Properties
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// Salary를 구하거나 설정합니다.
		/// </summary>
		public double Salary
		{
			get
			{
				return _salary;
			}
			set
			{
				_salary = value;
			}
		}

		#endregion
		/// <summary>
		/// Initializes a new instance of the Employee class.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="salary"></param>
		public Employee(string name, double salary)
		{
			_name = name;
			_salary = salary;
		}
	}
}
