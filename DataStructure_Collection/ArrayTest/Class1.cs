using	 System;


public	class	ArrayTest
{
	public class BluetoothServiceName
	{
		public static string[]	ServiceName	= new string[]{
															  "Serial Port",
															  "PANU",
															  "NAP",
															  "GN",
															  "ObexObjectPush",
															  "ObexFileTransfer",
															  "DialupNetworking",
															  "HeadsetAudioGateway",
															  "Headset",
															  "HandsfreeAudioGateway",
															  "Handsfree",
															  "Fax",
															  "Imaging",
															  "HumanInterfaceDevice"
														  };
		public static Guid[]		ServiceGuid	= new Guid[]{
																new Guid(0x00001101, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// Serial Port
																new Guid(0x00001115, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// PANU
																new Guid(0x00001116, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// NAP
																new Guid(0x00001117, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// GN
																new Guid(0x00001105, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// ObexObjectPush
																new Guid(0x00001106, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// ObexFileTransfer
																new Guid(0x00001103, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// DialupNetworking
																new Guid(0x00001112, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// HeadsetAudioGateway
																new Guid(0x00001108, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// Headset
																new Guid(0x0000111F, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// HandsfreeAudioGateway
																new Guid(0x0000111E, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// Handsfree
																new Guid(0x00001111, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// Fax
																new Guid(0x0000111A, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),	// Imaging
																new Guid(0x00001124, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB)	// HumanInterfaceDevice
															};
		public static string GetServiceName(Guid guid)
		{
			return (string)ServiceName[Array.IndexOf(ServiceGuid, guid, 0, ServiceGuid.Length)];
		}

		public static Guid GetServiceGuid(string serviceName)
		{
			return (Guid)ServiceGuid[Array.IndexOf(ServiceName, serviceName, 0, ServiceName.Length)];
		}
	}

	public	class	ArrayTestMain
	{
		public	static	void	Main()
		{
			Guid guid = new Guid(0x00001103, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB);
			string		service = BluetoothServiceName.GetServiceName(guid);

			Console.WriteLine("Service : {0}\n", service);

			for (int i = 0 ; i < BluetoothServiceName.ServiceGuid.Length; i++)
				Console.WriteLine("{0}", BluetoothServiceName.GetServiceName(BluetoothServiceName.ServiceGuid[i]));

			Console.WriteLine();

			for (int i = 0 ; i < BluetoothServiceName.ServiceGuid.Length; i++)
				Console.WriteLine("{0}", BluetoothServiceName.GetServiceGuid(BluetoothServiceName.ServiceName[i]));
		}	// main
	}	// class : 
}	// class : 

