﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace SortedList01
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] data = new string[]{
				"0003",
				"0002",
				"0004"};
			SortedList sort = new SortedList();

			for (int i = 0; i < data.Length; i++)
			{
				sort.Add(data[i], i);
			}

			Console.WriteLine(data[(int)sort.GetByIndex(sort.Count-1)]);

			Console.ReadLine();
		}
	}
}
