﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RingBufferTest
{
	class Program
	{
		static void Main(string[] args)
		{
			RingBuffer<int> ringBuffer = new RingBuffer<int>(3);

			ringBuffer.Add(5);
			DisplayBuffer(ringBuffer);

			ringBuffer.Add(1);
			DisplayBuffer(ringBuffer);

			ringBuffer.Add(2);
			DisplayBuffer(ringBuffer);

			ringBuffer.Add(3);
			DisplayBuffer(ringBuffer);

			ringBuffer.Add(4);
			DisplayBuffer(ringBuffer);

			ringBuffer.Add(6);
			DisplayBuffer(ringBuffer);
		}

		private static void DisplayBuffer(RingBuffer<int> ringBuffer)
		{
			foreach (var item in ringBuffer.Buffer)
			{
				Console.WriteLine(item);
			}

			Console.WriteLine();
		}
	}
}
