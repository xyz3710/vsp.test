﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace RingBufferTest
{
	internal class RingBuffer<T>
	//where T : class, IDisposable
	{
		/// <summary>
		/// Initializes a new instance of the RingBuffer class.
		/// </summary>
		/// <param name="count">The ring count.</param>
		public RingBuffer(int count)
		{
			Count = count;
			Index = 0;
			Buffer = new T[count];
		}

		/// <summary>
		/// RingBuffer Indexer
		/// </summary>
		/// <param name="index">The index.</param>
		/// <returns>Buffer</returns>
		/// <exception cref="System.IndexOutOfRangeException"></exception>
		public T this[int index]
		{
			get
			{
				if (index < 0 || index >= Count)
				{
					throw new IndexOutOfRangeException();
				}

				int index2 = (Index - Count + index) % Count;

				return Buffer[index2];
			}
		}

		/// <summary>
		/// 버퍼 개수를 반환합니다.
		/// </summary>
		/// <value>Count를 반환합니다.</value>
		public int Count
		{
			get;
			private set;
		}

		/// <summary>
		/// 현재 버퍼 인덱스를 구합니다.
		/// </summary>
		/// <value>Index를 반환합니다.</value>
		public int Index
		{
			get;
			private set;
		}

		/// <summary>
		/// Buffer를 구합니다.
		/// </summary>
		/// <value>Buffer를 반환합니다.</value>
		public T[] Buffer
		{
			get;
			private set;
		}

		/// <summary>
		/// 링 버퍼에 이미지를 추가합니다.
		/// </summary>
		/// <param name="bitmap">The bitmap.</param>
		/// <returns>추가된 버퍼의 인덱스를 반환 합니다.</returns>
		public int Add(T bitmap)
		{
			Index %= Count;
			//if (Buffer[Index] != null)
			//{
			//	Buffer[Index].Dispose();
			//}

			Buffer[Index] = bitmap;

			return Index++;
		}

		/// <summary>
		/// 링 버퍼를 초기화 합니다.
		/// </summary>
		public void Reset()
		{
			for (int i = 0; i < Count; i++)
			{
				//if (Buffer[i] != null)
				//{
				//	Buffer[i].Dispose();
				//}
			}

			Index = 0;
		}
	}
}
