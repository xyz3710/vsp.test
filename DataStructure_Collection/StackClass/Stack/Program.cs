﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stack
{
	public interface IStackItem
	{
		/// <summary>
		/// Gets the next item.
		/// </summary>
		/// <returns></returns>
		IStackItem GetNextItem();

		/// <summary>
		/// Sets the next item.
		/// </summary>
		/// <param name="item">The item.</param>
		void SetNextItem(IStackItem item);
	}

	public class Stack : IStackItem
	{
		private IStackItem top;
		private IStackItem next;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public Stack()
		{
			top = null;
		}

		#endregion	

		/// <summary>
		/// Pushes the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		public void Push(IStackItem item)
		{
			if (top == null)
			{
				top = item;
			}
			else
			{
				item.SetNextItem(top);

				top = item;
			}
		}

		/// <summary>
		/// Pops this instance.
		/// </summary>
		/// <returns></returns>
		public IStackItem Pop()
		{
			if (top == null)
			{
				const string MESSAGE = "Stack is empty.";

				Console.WriteLine(MESSAGE);

				throw new OverflowException(MESSAGE);
			}

			IStackItem topItem = top;

			top = top.GetNextItem();

			return topItem;
		}

		/// <summary>
		/// Prints all.
		/// </summary>
		/// <returns></returns>
		public void PrintAll()
		{
			const int WIDTH = 80;

			printHalfString(WIDTH, "This stack has under value");

			IStackItem item = top;

			while (item != null)
			{
				Console.WriteLine(item);

				item = item.GetNextItem();
			}

			printHalfString(WIDTH, "End of stack printed");			
		}

		private static void printHalfString(int WIDTH, string endString)
		{
			int endStringLength = endString.Length;
			int half = (WIDTH - endStringLength) / 2;

			for (int i = 0; i < WIDTH; i++)
			{
				if (i >= half && i < half + endStringLength)
				{
					Console.Write(endString.ToCharArray()[i - half].ToString());

					continue;
				}

				Console.Write("=");
			}
		}

		#region IStackItem Members

		public IStackItem GetNextItem()
		{
			return next;
		}

		public void SetNextItem(IStackItem item)
		{
			next = item;
		}

		#endregion
	}

	public class StackInit : IStackItem
	{
		private IStackItem next;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public StackInit()
		{
			next = null;
		}

		#endregion
		
		#region IStackItem Members

		public IStackItem GetNextItem()
		{
			return next;
		}

		public void SetNextItem(IStackItem item)
		{
			next = item;
		}

		#endregion
	}

	public class IntItem : StackInit
	{
		private int value;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public IntItem(int value)
		{
			this.value = value;
		}

		#endregion		

		public override string ToString()
		{
			return value.ToString();
		}
	}

	public class StringItem : StackInit
	{
		private string value;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public StringItem(string value)
		{
			this.value = value;
		}

		#endregion
		
		public override string ToString()
		{
			return value;
		}
	}

	public class ComplextNumberItem : StackInit
	{
		private double real;
		private double imaginary;

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		public ComplextNumberItem(double real, double imaginary)
		{
			this.real = real;
			this.imaginary = imaginary;
		}

		#endregion
		
		public override string ToString()
		{
			return real.ToString() + " + " + imaginary.ToString();
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Stack stack = new Stack();

			stack.Push(new IntItem(10));
			stack.Push(new IntItem(20));
			stack.Push(new StringItem("string1"));
			stack.Push(new StringItem("string2"));
			stack.Push(new ComplextNumberItem(10.0, 15.5));
			stack.Push(new ComplextNumberItem(20.0, 25.5));

			stack.PrintAll();

			Console.WriteLine(stack.Pop());
			Console.WriteLine(stack.Pop());
			Console.WriteLine(stack.Pop());
			Console.WriteLine(stack.Pop());
			Console.WriteLine(stack.Pop());
			Console.WriteLine(stack.Pop());

			stack.PrintAll();

			Console.ReadLine();
		}
	}
}
