﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ListTest01
{
	class CustomClass
	{
		/// <summary>
		/// FirstName를 구하거나 설정합니다.
		/// </summary>
		public string FirstName
		{
			get;
			set;
		}

		/// <summary>
		/// LastName를 구하거나 설정합니다.
		/// </summary>
		public string LastName
		{
			get;
			set;
		}

		/// <summary>
		/// Age를 구하거나 설정합니다.
		/// </summary>
		public int Age
		{
			get;
			set;
		}
	}
}
