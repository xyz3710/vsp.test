using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Stack01
{
	public class StackEnumerator : IEnumerator
	{
		private StackImitation _stack;

		public StackEnumerator(StackImitation stack)
		{
			_stack = stack;
		}

		#region IEnumerator Members

		public object Current
		{
			get
			{
				object ret = _stack.Pop();

				if (_stack.Count < 0)
					ret = null;

				return ret;
			}
		}

		public bool MoveNext()
		{
			bool ret = false;

			if (_stack.Count != 0 && _stack.Peek() != null)
				ret = true;

			return ret;
		}

		public void Reset()
		{
			_stack.Reset();
		}

		#endregion
	}

}
