using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Stack01
{

	public class StackImitation : IEnumerable
	{
		private ArrayList _stack;
		private object _top;
		private int _position;

		public StackImitation()
		{
			_stack = new ArrayList();
			_top = new object();
			_position = 0;
		}

		public void Push(object obj)
		{
			_stack.Add(obj);
			_top = obj;
			_position++;
		}

		public object Pop()
		{
			if (_position < 0)
				return null; 

			object ret = _stack[(_position-- - 1)];

			return ret;
		}

		public object Peek()
		{
			return _top;
		}

		public int Count
		{
			get
			{
				return _stack.Count;
			}
		}

		public void Reset()
		{
			_position = _stack.Count - 1;
		}

		public void Clear()
		{
			_stack.Clear();
		}

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return new StackEnumerator(this);
		}

		#endregion
	}

}
