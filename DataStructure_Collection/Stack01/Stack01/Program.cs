﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Stack01
{
	public class Program
	{
		public static void Main(string[] args)
		{
			StackImitation stack = new StackImitation();

			for (int i = 0; i < 10; i++)
				stack.Push(i);

			Console.WriteLine("Peek : {0}", stack.Peek());

			int cnt = 0;

			foreach (int item in stack)
			{
				Console.WriteLine(item);
				if (cnt++  == 5)
					break;
			}

			Console.WriteLine();

			foreach (int item in stack)
				Console.WriteLine(item);
		}
	}
}
