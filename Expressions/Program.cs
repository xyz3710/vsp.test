﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NCalc;

namespace ExpressionsTest
{
	class Program
	{
		static void Main(string[] args)
		{
			string math1 = "1678 > 1677";
			string math2 = "'Card' == 'Card'";
			string value1 = new System.Data.DataTable().Compute(math1, null).ToString();
			string value2 = new System.Data.DataTable().Compute(math2, null).ToString();

			Console.WriteLine(value1);
			Console.WriteLine(value2);
		}
	}
}
