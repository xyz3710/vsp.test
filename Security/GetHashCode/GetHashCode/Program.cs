﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace GetHashCode
{
	class Program
	{
		/// <summary>
		/// http://sharpertutorials.com/calculate-md5-checksum-file/
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		static string GetMD5HashFromFile(string fileName)
		{
			byte[] byteResults = null;

			using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				MD5 md5 = new MD5CryptoServiceProvider();

				byteResults = md5.ComputeHash(fileStream);

				fileStream.Close();
			}

			StringBuilder sb = new StringBuilder();
			
			for (int i = 0; i < byteResults.Length; i++)
				sb.Append(byteResults[i].ToString("X2"));

			return sb.ToString();
		}

		static void Main(string[] args)
		{
			string filename = "03F05761ADA2FE062E2BC2F955B6753D.mp3";
			string checkSum = GetMD5HashFromFile(filename);

			Console.WriteLine("file checkSum : {0}\r\nMatch : {1}", checkSum, Path.GetFileNameWithoutExtension(filename) == checkSum);
			//TestCheckSum();
		}

		/// <summary>
		/// http://support.microsoft.com/kb/307020/ko
		/// </summary>
		private static void TestCheckSum()
		{
			string sSourceData;
			byte[] tmpSource;
			byte[] tmpHash;
			sSourceData = "MySourceData";
			//Create a byte array from source data
			tmpSource = ASCIIEncoding.ASCII.GetBytes(sSourceData);

			//Compute hash based on source data
			tmpHash = new MD5CryptoServiceProvider().ComputeHash(tmpSource);
			Console.WriteLine(ByteArrayToString(tmpHash));

			sSourceData = "NotMySourceData";
			tmpSource = ASCIIEncoding.ASCII.GetBytes(sSourceData);

			byte[] tmpNewHash = new MD5CryptoServiceProvider().ComputeHash(tmpSource);
			bool bEqual = false;

			if (tmpNewHash.Length == tmpHash.Length)
			{
				int i = 0;
				while ((i < tmpNewHash.Length) && (tmpNewHash[i] == tmpHash[i]))
				{
					i += 1;
				}
				if (i == tmpNewHash.Length)
				{
					bEqual = true;
				}
			}

			if (bEqual)
				Console.WriteLine("The two hash values are the same");
			else
				Console.WriteLine("The two hash values are not the same");
			Console.ReadLine();
		}

		static string ByteArrayToString(byte[] arrInput)
		{
			int i;
			StringBuilder sOutput = new StringBuilder(arrInput.Length);
			for (i = 0; i < arrInput.Length - 1; i++)
			{
				sOutput.Append(arrInput[i].ToString("X2"));
			}
			return sOutput.ToString();
		}
	}
}

