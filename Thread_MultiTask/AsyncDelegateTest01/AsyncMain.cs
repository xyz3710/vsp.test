using System;

namespace AsyncDelegateTest01
{
	public class AsyncMain
	{
		private static int threadId;

		/// <summary>
		/// 비동기 호출이 완료될 때 CallBack mehtod 실행
		/// 비동기 호출을 시작하는 스레드가 결과를 처리할 필요가 없는 경우에는 
		/// 호출이 완료될 때 콜백 메서드를 실행할 수 있습니다. 
		/// 콜백 메서드는 ThreadPool 스레드에서 실행됩니다.
		/// 콜백 메서드를 사용하려면 BeginInvoke에 해당 메서드를 나타내는 AsyncCallback 대리자를 전달해야 합니다. 
		/// 또한 콜백 메서드에서 사용하는 정보가 들어 있는 개체를 전달할 수도 있습니다. 
		/// 예를 들어, 호출을 시작하는데 사용된 대리자를 전달하면 콜백 메서드에서 EndInvoke를 호출할 수 있습니다
		/// </summary>
		public static void Main()
		{
			AsyncDemo ad = new AsyncDemo();
			
			// delegate 생성
			AsyncDemo.AsyncDelegate asyncDel04 = new AsyncDelegateTest01.AsyncDemo.AsyncDelegate(ad.TestMethod);

			// 비동기 호출을 초기화 합니다.
			// call back method를 나타내는 대리자와 EndInvoke를 호출하는데 필요한 데이터
			IAsyncResult ar = asyncDel04.BeginInvoke(3000, out threadId, 
				new AsyncCallback(callBackMethod), asyncDel04);

			Console.WriteLine("Press Enter to close application.");
			Console.ReadLine();
		}

		/// <summary>
		/// CallBack method는 AsyncCallBack 대리자와 동일한 signature를 가져야 합니다.
		/// </summary>
		public static void callBackMethod(IAsyncResult ar)
		{
			AsyncDemo.AsyncDelegate asyncDel04 = (AsyncDemo.AsyncDelegate)ar.AsyncState;

			// 결과를 구하기 위하서 EndInvoke를 호출합니다.
			string ret = asyncDel04.EndInvoke(out threadId, ar);

			Console.WriteLine("The call executed on thread {0}, with return value \"{0}\".", threadId, ret);
		}
	}
}
