using System;
using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace AsyncDelegateTest01
{
	public class AsyncDemo
	{
		/// <summary>
		/// 비동기 호출하려는 method와 같은 signature여야 합니다.
		/// </summary>
		public delegate string AsyncDelegate(int callDuration, out int threadId);

		/// <summary>
		/// 비동기 TestMethod
		/// </summary>
		/// <param name="callDuration"></param>
		/// <param name="threadId"></param>
		/// <returns></returns>
		public string TestMethod(int callDuration, out int threadId)
		{
			Console.WriteLine("Test method begins");
			Thread.Sleep(callDuration);
			threadId = AppDomain.GetCurrentThreadId();

			return String.Format("MyCallTime was {0}", callDuration); 
		}
	}

}
