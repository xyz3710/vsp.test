/**********************************************************************************************************************/
/*	Name		:	AsyncDelegateTest01.TestDriver
/*	Purpose		:	비동기 호출 대기 4가지 방법들`
/*	Creator		:	Kim Ki Won
/*	Create		:	2006년 10월 3일 화요일 오후 2:00:30
/*	Modifier	:	
/*	Update		:	2006년 10월 3일 화요일
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace AsyncDelegateTest01
{
	public class TestDriver01
	{
		/// <summary>
		/// EndInvoke로 비동기 호출 대기
		/// 메서드를 비동기 방식으로 실행하는 가장 간단한 방법은 
		/// BeginInvoke로 시작하고 기본 스레드에서 일정한 작업을 수행한 다음 EndInvoke를 호출하는 것입니다. 
		/// 비동기 호출이 완료될 때까지 EndInvoke는 반환되지 않습니다. 
		/// 이 방법은 파일이나 네트워크 작업에 사용하는 것이 좋습니다. 
		/// 그러나 EndInvoke를 차단하기 때문에 사용자 인터페이스 스레드에서는 사용하지 않아야 합니다.
		/// </summary>
		public void Main1()
		{
			// 비동기 mehtod에서 ThreadId를 저장합니다.
			int threadId;
			AsyncDemo ad = new AsyncDemo();

			// delegate 생성
			AsyncDemo.AsyncDelegate asyncDel01 = new AsyncDemo.AsyncDelegate(ad.TestMethod);

			// 비동기 호출 초기화
			IAsyncResult ar = asyncDel01.BeginInvoke(3000, out threadId, null, null);

			Thread.Sleep(0);

			Console.WriteLine("Main thread {0} does some work.", AppDomain.GetCurrentThreadId());

			// 비동기 호출이 완료될 때 호출 대기를 위한 EndInvoke 호출
			string ret = asyncDel01.EndInvoke(out threadId, ar);

			Console.WriteLine("The call executed on thread {0}, with return value \"{0}\".", threadId, ret);
		}

		/// <summary>
		/// WaitHandle로 비동기 호출 대기
		/// WaitHandle을 사용하여 대기하는 방법은 일반적으로 스레드를 동기화하는 방법입니다. 
		/// BeginInvoke에 의해 반환되는 IAsyncResult의 AsyncWaitHandle 속성을 사용하여 WaitHandle을 적용할 수 있습니다. 
		/// 비동기 호출이 완료되면 WaitHandle 신호를 받으며 WaitOne을 호출하여 대기할 수 있습니다.
		/// WaitHandle을 사용하는 경우 비동기 호출이 완료되고 EndInvoke를 호출하여 결과를 검색하기 전에 
		/// 추가 처리 작업을 수행할 수 있습니다
		/// </summary>
		public void Main2()
		{
			// 비동기 mehtod에서 ThreadId를 저장합니다.
			int threadId;
			AsyncDemo ad = new AsyncDemo();
			
			// delegate 생성
			AsyncDemo.AsyncDelegate asyncDel02 = new AsyncDelegateTest01.AsyncDemo.AsyncDelegate(ad.TestMethod);

			// 비동기 호출 초기화
			IAsyncResult ar = asyncDel02.BeginInvoke(3000, out threadId, null, null);

			Thread.Sleep(0);

			Console.WriteLine("Main thread {0} does some work.", AppDomain.GetCurrentThreadId());

			// 신호가 발생될 때 까지 WaitHandle을 기다립니다.
			ar.AsyncWaitHandle.WaitOne();

			// 추가 작업을 여기서 처리합니다.
			// 결과를 구하기 위하서 EndInvoke를 호출합니다.
			string ret = asyncDel02.EndInvoke(out threadId, ar);

			Console.WriteLine("The call executed on thread {0}, with return value \"{0}\".", threadId, ret);
		}

		/// <summary>
		/// 비동기 호출 완료에 대한 폴링
		/// BeginInvoke에 의해 반환되는 IAsyncResult의 IsCompleted 속성을 사용하여 비동기 호출이 완료되는 시기를 알 수 있습니다. 
		/// 사용자 인터페이스 스레드에서 비동기 호출을 수행하는 경우 이 동작을 수행할 수 있습니다. 
		/// 완료에 대해 폴링하면 사용자 인터페이스 스레드에서 사용자 입력 처리를 계속할 수 있습니다
		/// </summary>
		public static void Main3()
		{
			// 비동기 mehtod에서 ThreadId를 저장합니다.
			int threadId;
			AsyncDemo ad = new AsyncDemo();
			
			// delegate 생성
			AsyncDemo.AsyncDelegate asyncDel03 = new AsyncDelegateTest01.AsyncDemo.AsyncDelegate(ad.TestMethod);

			// 비동기 호출 초기화
			IAsyncResult ar = asyncDel03.BeginInvoke(3000, out threadId, null, null);

			// Poll while simulating work
			while (ar.IsCompleted == false)
			{
				Thread.Sleep(10);
			}

			// 결과를 구하기 위하서 EndInvoke를 호출합니다.
			string ret = asyncDel03.EndInvoke(out threadId, ar);

			Console.WriteLine("The call executed on thread {0}, with return value \"{0}\".", threadId, ret);
		}
	}
}
