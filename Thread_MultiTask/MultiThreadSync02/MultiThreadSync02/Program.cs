﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MultiThreadSync02
{
	class Program
	{
		static void Main(string[] args)
		{
			List<Thread> threads = new List<Thread>();
			MyClass myClass = new MyClass();

			for (int i = 0; i < 10; i++)
				threads.Add(new Thread(myClass.Say));

			for (int j = 0; j < threads.Count; j++)
			{
				threads[j].Name = j.ToString();
				threads[j].Start();
				threads[j].Join();
			}

			Console.WriteLine("Main terminated");
		}
	}

	public class MyClass
	{
		private static int _count;

		public void Say()
		{
			Random random = new Random();

			Thread.Sleep(Convert.ToInt32(random.NextDouble() * 1000));
			Console.WriteLine("Thread : {0:00}, {1:00}", Thread.CurrentThread.Name, _count++);
		}
	}
}
