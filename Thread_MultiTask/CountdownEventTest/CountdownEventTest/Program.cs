﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// CountdownEvent class: http://msdn.microsoft.com/ko-kr/library/system.threading.countdownevent(v=vs.110).aspx
namespace CountdownEventTest
{
	class Program
	{
		// Demonstrates:
		//	CountdownEvent construction
		//	CountdownEvent.AddCount();
		//	CountdownEvent.Signal();
		//	CountdownEvent.Wait();
		//	CountdownEvent.Wait() w/ cancellation;
		//	CountdownEvent.Reset();
		//	CountdownEvent.IsSet;
		//	CountdownEvent.InitialCount;
		//	CountdownEvent.CurrentCount;
		static void Main(string[] args)
		{
			// Initialize a queue and a CountdownEvent
			int initialCount = 10000;
			ConcurrentQueue<int> queue = new ConcurrentQueue<int>(Enumerable.Range(0, initialCount));
			CountdownEvent cde = new CountdownEvent(initialCount);

			//cde.AddCount(10);
			// This is the logic for all queue consumers
			Action consumer = () =>
			{
				int local;
				// decrement CDE count once for each element consumed from queue
				while (queue.TryDequeue(out local) == true)
				{
					Console.WriteLine(cde.CurrentCount);
					cde.Signal();
				}
			};

			// Now empty the queue with a couple of asynchronousu tasks
			Task t1 = Task.Factory.StartNew(consumer);
			Task t2 = Task.Factory.StartNew(consumer);

			// And wait for queue to empty by waiting on cde
			cde.Wait();		// will return when cde count reaches 0

			Console.WriteLine("Done empty queue. InitialCount={0}, CurrentCount={1}, IsSet={2}", cde.InitialCount, cde.CurrentCount, cde.IsSet);

			// Proper form is to wait for the tasks to complete, even if you that their work is done already.
			Task.WaitAll(t1, t2);

			// Now try waiting with cancellation
			CancellationTokenSource cts = new CancellationTokenSource();
			cts.Cancel();		// cancels the CancellationTokenSource

			try
			{
				cde.Wait(cts.Token);
			}
			catch (OperationCanceledException)
			{
				Console.WriteLine("cde.Wait(preCanceledToken) threw OCE, as expected.");
			}

			// It's good for th release a CountdownEvent when you're done with it.
			cde.Dispose();
		}
	}
}
