#define VALUE_TYPE
#undef VALUE_TYPE
#define REFERENCE_TYPE
//#undef

using System;
using System.Text;
using System.Threading;

namespace MultiThreadSyncStatic01
{
	public class Top
	{
#if VALUE_TYPE
		private static int _limit = 0;					// static member가 값 타입일 경우

		public void SayHello()
		{
			int hash = Thread.CurrentThread.GetHashCode();
			int count = 0;

			lock (typeof(Top))							// static member가 값 타입일 경우
			{
				while (count < 10)
				{
					Console.WriteLine("Thread {0} : {1}", hash, _limit++);
					count++;
					Thread.Sleep(10);
				}
			}
		}
#endif

#if REFERENCE_TYPE
		private static object obj = new object();		// static member가 참조 타입일 경우

		public void SayHello()
		{
			int hash = Thread.CurrentThread.GetHashCode();
			int count = 0;

			lock (Top.obj)								// static member가 참조 타입일 경우
			{
				while (count < 10)
				{
					Console.WriteLine("Thread {0} : {1}", hash, count++);

					Thread.Sleep(10);
				}
			}
		}
#endif
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			Top t1 = new Top();
			Top t2 = new Top();

			Thread thread1 = new Thread(new ThreadStart(t1.SayHello));
			Thread thread2 = new Thread(new ThreadStart(t1.SayHello));
			
			thread1.Start();
			thread2.Start();

			Console.WriteLine("Thread {0} has terminated.", Thread.CurrentThread.GetHashCode());
		}
	}
}
