// statements_lock2.cs
using System;
using System.Threading;

class Test
{
	static void main()
	{
		Thread[] threads = new Thread[10];
		Account acc = new Account(1000);
		for (int i = 0; i < 10; i++)
		{
			Thread t = new Thread(new ThreadStart(acc.DoTransactions));
			threads[i] = t;
		}
		for (int i = 0; i < 10; i++)
		{
			threads[i].Start();
		}
	}
}