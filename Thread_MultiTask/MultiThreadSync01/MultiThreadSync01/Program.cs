using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MultiThreadSync01
{
	public class Top
	{
		private int _limit = 0;		// ���� �ڿ�

		public void SayHello()
		{
			int hash = Thread.CurrentThread.GetHashCode();
			int count = 0;

			lock (this)
			{
				while (count < 10)
				{
					Console.WriteLine("Thread {0} : {1}", hash, _limit++);
					count++;
					Thread.Sleep(10);
				}
			}
		}
	}
	
	class MultiThreadSync01
	{
		static void Main(string[] args)
		{
			Top t = new Top();
			
			Thread thread1 = new Thread(new ThreadStart(t.SayHello));
			Thread thread2 = new Thread(new ThreadStart(t.SayHello));

			thread1.Start();
			thread2.Start();

			Console.WriteLine("\nThread {0} Main terminated", Thread.CurrentThread.GetHashCode());
		}
	}
}
