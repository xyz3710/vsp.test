using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace AsyncOperationManagerExample
{
	public class CalculatePrimeProgressChangedEventArgs :
				ProgressChangedEventArgs
	{
		private int latestPrimeNumberValue = 1;

		public CalculatePrimeProgressChangedEventArgs(
			int latestPrime,
			int progressPercentage,
			object userToken)
			: base(progressPercentage, userToken)
		{
			this.latestPrimeNumberValue = latestPrime;
		}

		public int LatestPrimeNumber
		{
			get
			{
				return latestPrimeNumberValue;
			}
		}
	}
}
