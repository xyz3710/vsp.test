namespace csharp
{
  using System;
  using System.Threading;

  class MultiThreadApp
  {
    public static void Main()
    {
      MultiThreadApp app = new MultiThreadApp();

      app.DoTest();
    }

    private void DoTest()
    {
      Thread[] aThread =
        {
          new Thread( new ThreadStart(DoPrinting) ),
          new Thread( new ThreadStart(DoSpelling) ),
          new Thread( new ThreadStart(DoSaving) )
        };

      foreach( Thread t in aThread)
      {
        t.Start();
      }

      foreach( Thread t in aThread)
      {
        t.Join();
      }

      Console.WriteLine("모든 스레드가 종료되었습니다");
    } 




    private void DoPrinting()
    {
      Console.WriteLine("인쇄 시작");
      for ( int LoopCtr = 0; LoopCtr < 100; LoopCtr++)
      {
        Thread.Sleep(120);
        Console.Write("p|");

      }

      Console.WriteLine("인쇄 완료");
    }

    private void DoSpelling()
    {
      Console.WriteLine("철자 검사 시작");
      for ( int LoopCtr = 0; LoopCtr < 100; LoopCtr++)
      {
        Thread.Sleep(100);
        Console.Write("c|");
      }
      
      Console.WriteLine("철자 검사 완료");
    }

    private void DoSaving()
    {
      Console.WriteLine("저장 시작");
      for ( int LoopCtr = 0; LoopCtr < 100; LoopCtr++)
      {
        Thread.Sleep(50);
        Console.Write("s|");
      }
      Console.WriteLine("저장 완료");
    }
  }

}