using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Monitor02_CriticalSection
{
	public class AppMain
	{
		private int counter = 0;

		public static void Main()
		{
			AppMain app = new AppMain();

			app.DoTest();

			Console.WriteLine("Thread {0} has terminated.", Thread.CurrentThread.GetHashCode());
		}

		public void DoTest()
		{
			Thread t1 = new Thread(new ThreadStart(Incrementer));
			Thread t2 = new Thread(new ThreadStart(Decrementer));

			t1.Start();
			t2.Start();
			//t1.Join();
			//t2.Join();
		}

		private void Incrementer()
		{
			//Monitor.Enter(this);

			try
			{
				lock (this)	// lock은 내부적으로 Monitor.Enter, Monitor.Exit를 처리하므로 결과는 같다
				{
					while (counter < 10)
					{
						Console.WriteLine("Incrementer : " + counter.ToString());
						counter++;
					}
				}
			} // end of try

			finally
			{
				//Monitor.Exit(this);
			} // end of finally
		} // end of Incrementer

		private void Decrementer()
		{
			//Monitor.Enter(this);
			// 임계 영역에 들어가지 못했다
			if (Monitor.TryEnter(this) == false)
				Console.WriteLine("임계 영역에 들어가는데 실패");
			else
			{
				// 임계 영역에 들어간 경우
				try
				{
					while (counter > 0)
					{
						Console.WriteLine("Decrementer : " + counter.ToString());
						counter--;
					}
				} // end of try

				finally
				{
					Monitor.Exit(this);
				} // end of finally
			}
		} // end of Decrementer
	}
}
