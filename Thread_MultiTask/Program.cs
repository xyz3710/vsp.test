﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using GS.RPass.UI.Cameras;
using System.Windows.Forms;
using System.Threading;
using GS.Win.Common.Utility.Log;
using System.Drawing.Imaging;
using System.IO;
using FlyCapture2Managed;
using System.Diagnostics;

namespace CameraTest
{
	class Program
	{
		static AutoResetEvent event_1 = new AutoResetEvent(true);
		static bool bTerminated = false;
		static int count = 0;

		static void Main(string[] args)
		{
			//var task = Task.Factory.StartNew(() =>
			//{
			//TestSingleThread();
			//});

			Thread t = new Thread(TestSingleThread);
			t.Start();

			//const int TEST_LOOP = 100000;

			//for (int i = 0; i < TEST_LOOP; i++)
			int i = 0;

			while (true)
			{
				Thread.Sleep(1000);
				Console.WriteLine(i);
				count = i++;
				event_1.Set();
			}
			
			bTerminated = true;

			Console.ReadLine();
			//task.Wait();
			//TestMultiThread();
		}

		private static void TestSingleThread()
		{
			ManagedBusManager busMgr = new ManagedBusManager();
			ManagedPGRGuid _guidToUse = busMgr.GetCameraFromIndex(0);

			ManagedCamera PGCamera = new ManagedCamera();

			const Mode k_fmt7Mode = Mode.Mode10;
			const FlyCapture2Managed.PixelFormat k_fmt7PixelFormat = FlyCapture2Managed.PixelFormat.PixelFormatRaw8;

			// Begin Connect
			PGCamera.Connect(_guidToUse);

			CameraInfo camInfo = PGCamera.GetCameraInfo();

			// Query for available Format 7 modes
			bool supported = false;
			Format7Info fmt7Info = PGCamera.GetFormat7Info(k_fmt7Mode, ref supported);

			if ((k_fmt7PixelFormat & (FlyCapture2Managed.PixelFormat)fmt7Info.pixelFormatBitField) == 0)
			{
				// Pixel format not supported!
				Console.WriteLine("Pixel format is not supported");
				return;
			}

			// 카메라 Gain 값 설정. 
			// Manual 모드, 값은 0
			PropertyType pt = PropertyType.Gain;
			CameraProperty cp = PGCamera.GetProperty(pt);
			cp.autoManualMode = false;
			cp.absValue = 0.0f;
			PGCamera.SetProperty(cp);

			Format7ImageSettings fmt7ImageSettings = new Format7ImageSettings();
			fmt7ImageSettings.mode = k_fmt7Mode;
			fmt7ImageSettings.offsetX = 0;
			fmt7ImageSettings.offsetY = 0;
			fmt7ImageSettings.width = fmt7Info.maxWidth;
			fmt7ImageSettings.height = fmt7Info.maxHeight;
			fmt7ImageSettings.pixelFormat = k_fmt7PixelFormat;

			// Validate the settings to make sure that they are valid
			bool settingsValid = false;
			Format7PacketInfo fmt7PacketInfo = PGCamera.ValidateFormat7Settings(fmt7ImageSettings, ref settingsValid);

			if (settingsValid != true)
			{
				Console.WriteLine("Error!!!");
				// Settings are not valid
				return;
			}

			// Set the settings to the camera
			PGCamera.SetFormat7Configuration(fmt7ImageSettings, fmt7PacketInfo.recommendedBytesPerPacket);

			// Set embedded timestamp to on
			EmbeddedImageInfo embeddedInfo = PGCamera.GetEmbeddedImageInfo();

			embeddedInfo.timestamp.onOff = true;
			PGCamera.SetEmbeddedImageInfo(embeddedInfo);
			// End Connect


			const string FOLDER = "Images";
			if (Directory.Exists(FOLDER) == false)
			{
				Directory.CreateDirectory(FOLDER);
			}

			FileLogger fileLogger = new FileLogger(".");

			ManagedImage _rawImage = new ManagedImage();
			ManagedImage _processedImage = new ManagedImage();

			PGCamera.StartCapture();

			while (bTerminated == false)
			{
				event_1.WaitOne();

				// Begin GetFrame
				try
				{
					PGCamera.RetrieveBuffer(_rawImage);
				}
				catch (FC2Exception ex)
				{
					Console.WriteLine("Error: " + ex.Message);
				}

				if (_processedImage.bitmap != null)
				{
					_processedImage.bitmap.Dispose();
				}

				_rawImage.Convert(FlyCapture2Managed.PixelFormat.PixelFormatBgr, _processedImage);

				//Task.Factory.StartNew(() =>
				//{
				//using (var bitmap = new Bitmap(_processedImage.bitmap.Clone() as Bitmap))	// End GetFrame
				//{
				//	bitmap.Save(string.Format(@"{0}\{1:00000}.jpg", FOLDER, count), ImageFormat.Jpeg);
				//}
				//});

				//Thread.Sleep(10);
				//if (ReadKeyWithTimeOut(1).KeyChar == 'q')
				//{
				//	break;
				//}

				#region PERFORMANCE TEST STOP 95940
#if PERFORMANCE_TEST
				string msg4 = string.Format("Stop: Stop \t{0}", DateTime.Now - performanceTestStart95940);
				Console.WriteLine(msg4, "Program.Main");
				fileLogger.WriteLine(msg4);
#endif
				#endregion
			}

			PGCamera.StopCapture();

			_rawImage.Dispose();
			_processedImage.Dispose();

			PGCamera.Disconnect();

			Console.ReadLine();
		}

		private static ConsoleKeyInfo ReadKeyWithTimeOut(int timeOutMilliseconds)
		{
			DateTime timeoutvalue = DateTime.Now.AddMilliseconds(timeOutMilliseconds);

			while (DateTime.Now < timeoutvalue)
			{
				if (Console.KeyAvailable)
				{
					ConsoleKeyInfo cki = Console.ReadKey();

					return cki;
				}
				else
				{
					System.Threading.Thread.Sleep(100);
				}
			}

			return new ConsoleKeyInfo(' ', ConsoleKey.Spacebar, false, false, false);
		}

		private static void TestMultiThread()
		{
			Program program = new Program();

			var connectResult = program.InitCameras();
			Console.WriteLine("connectResult: {0}", connectResult);

			if (connectResult == false)
			{
				return;
			}

			const string FOLDER = "Images";
			if (Directory.Exists(FOLDER) == false)
			{
				Directory.CreateDirectory(FOLDER);
			}

			const int TEST_LOOP = 100000;
			FileLogger fileLogger = new FileLogger(".");

			for (int i = 0; i < TEST_LOOP; i++)
			{
				Console.WriteLine(i);

				if (i % 10 == 0)
				{
					Thread.Sleep(1000);
				}
				#region PERFORMANCE TEST START 95932
#if PERFORMANCE_TEST
				DateTime performanceTestStart95932 = DateTime.Now;
				string msg1 = string.Format("{0:00000} Run: Start\t{1}", i, performanceTestStart95932);
				Console.WriteLine(msg1, "Program.Main");
				fileLogger.WriteLine(msg1);
#endif
				#endregion
				program.Camera.Run();
				#region PERFORMANCE TEST STOP 95932
#if PERFORMANCE_TEST
				string msg2 = string.Format("Run: Stop \t{0}", DateTime.Now - performanceTestStart95932);
				Console.WriteLine(msg2, "Program.Main");
				fileLogger.WriteLine(msg2);
#endif
				#endregion
				#region PERFORMANCE TEST START 95940
#if PERFORMANCE_TEST
				DateTime performanceTestStart95940 = DateTime.Now;
				string msg3 = string.Format("Stop: Start\t{0}", performanceTestStart95940);
				Console.WriteLine(msg3, "Program.Main");
				fileLogger.WriteLine(msg3);
#endif
				#endregion


				using (var bitmap = program.Camera.GetFrame2())
				{
					bitmap.Save(string.Format(@"{0}\{1:00000}.jpg", FOLDER, i), ImageFormat.Png);
				}

				Thread.Sleep(10);
				program.Camera.Stop();

				#region PERFORMANCE TEST STOP 95940
#if PERFORMANCE_TEST
				string msg4 = string.Format("Stop: Stop \t{0}", DateTime.Now - performanceTestStart95940);
				Console.WriteLine(msg4, "Program.Main");
				fileLogger.WriteLine(msg4);
#endif
				#endregion

				Thread.Sleep(10);
			}

			Console.ReadLine();
		}

		private static void PointGreyCameraTest()
		{
			PointGreyCamera camera = new PointGreyCamera();

			camera.Initialize();
			camera.Connect();

		}

		private CameraBase Camera
		{
			get;
			set;
		}

		private bool InitCameras()
		{
			Camera = new PointGreyCamera();

			int connectedCameras = Camera.GetConnectedCameras();

			if (connectedCameras == 0)
			{
				const string RECONNECT_MESSAGE = "카메라를 발견할 수 없습니다.\r\n카메라를 연결 후 \"설정\" 메뉴의 \"카메라 설정\", \"카메라 연결\" 메뉴나 Ctrl+Shift+C 단축키로 인식 시켜주십시오.";

				MessageBox.Show(
					RECONNECT_MESSAGE,
					"카메라 연결",
					MessageBoxButtons.OK,
					MessageBoxIcon.Information);

				return false;
			}

			//RegisterCameraEventHandler();
			Camera.Initialize();
			Camera.Connect();

			return connectedCameras > 0;
		}
	}
}
