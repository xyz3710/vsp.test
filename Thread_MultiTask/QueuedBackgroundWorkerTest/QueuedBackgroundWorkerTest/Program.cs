﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
namespace QueuedBackgroundWorkerTest
{
	class Program
	{
		private readonly Queue<QueueItem<int>> _workerQueue = new Queue<QueueItem<int>>();
		private int _workerId = 1;

		internal void BackgroundTest()
		{
			QueuedBackgroundWorker.QueueWorkItem(
				_workerQueue,
				_workerId++,
				doWorkArgs =>
				{
					Thread.Sleep(1000);
					var currentTaskId = doWorkArgs.Argument;
					var now = DateTime.Now.ToLongTimeString();
					var message = string.Format("DoWork thread started at '{0}': Task Number={1}", now, currentTaskId);

					Console.WriteLine(message);

					return new
					{
						WorkerId = currentTaskId,
						Message = message
					};
				},
				completedArgs =>
				{
					var currentWorkerId = completedArgs.Result.WorkerId;
					var msg = completedArgs.Result.Message;

					var now = DateTime.Now.ToShortTimeString();
					var completeMessage = string.Format(
						"RunWorkerCompleted completed at '{0}'; for Task Number={1}, DoWork Message={2}",
						now,
						currentWorkerId,
						msg);

					Console.WriteLine(completedArgs);
				}
			);
			QueuedBackgroundWorker.QueueWorkItem(
				_workerQueue,
				_workerId++,
				doWorkArgs =>
				{
					Thread.Sleep(3000);
					var currentTaskId = doWorkArgs.Argument;
					var now = DateTime.Now.ToLongTimeString();
					var message = string.Format("DoWork thread started at '{0}': Task Number={1}", now, currentTaskId);

					Console.WriteLine(message);

					return new
					{
						WorkerId = currentTaskId,
						Message = message
					};
				}, null
			);
		}
		static void Main(string[] args)
		{
			new Program().BackgroundTest();

			Console.WriteLine("Press any key to next test...");
			Console.ReadLine();
			var workerQueue = new Queue<QueueItem<string>>();

			QueuedBackgroundWorker.QueueWorkItem(workerQueue, "", Working);
		}

		private void Working(string arg)
		{
			
		}
	}
}
