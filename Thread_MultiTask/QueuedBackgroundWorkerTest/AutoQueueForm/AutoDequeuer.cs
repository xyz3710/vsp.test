﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoQueueForm
{
	public class AutoDequeuer<T> : Queue<T>
	{
		public AutoDequeuer(bool supportsCancellation = true)
		{
			BackgroundWorker = new BackgroundWorker
			{
				WorkerReportsProgress = false,
				WorkerSupportsCancellation = supportsCancellation,
			};
			BackgroundWorker.DoWork += BackgroundWorker_DoWork;
		}

		private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			var worker = sender as BackgroundWorker;

			while (true)
			{
				if (worker.CancellationPending == true)
				{
					e.Cancel = true;

					break;
				}
				else
				{
					var item = Dequeue();
				}
			}
		}

		/// <summary>
		/// Gets the background worker.
		/// </summary>
		/// <value>The background worker.</value>
		public BackgroundWorker BackgroundWorker
		{
			get;
			private set;
		}

		public new void Enqueue(T item)
		{
			base.Enqueue(item);
			BackgroundWorker.RunWorkerAsync();
		}

		/// <summary>
		/// Cancels the asynchronous.
		/// </summary>
		public void CancelAsync()
		{
			if (BackgroundWorker.WorkerSupportsCancellation == true)
			{
				BackgroundWorker.CancelAsync();
			}
		}

		public void RunAsync()
		{
			if (BackgroundWorker.IsBusy == false)
			{
				BackgroundWorker.RunWorkerAsync();
			}
		}
	}
}
