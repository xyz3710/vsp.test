﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QueuedBackgroundWorkerTestForm
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			var worker = new QueuedBackgroundWorker();

			worker.RunAsync<int, int>(Calculate, 2);

			worker.RunAsync<MultiplyArgument, int>(Multiply, new MultiplyArgument()
			{
				A = 6,
				B = 2
			}, MultiplyCompleted);
		}
		private int Calculate(int a)
		{
			return a * 2;
		}

		private int Multiply(MultiplyArgument calculateArgument)
		{
			return calculateArgument.A * calculateArgument.B;
		}

		private void MultiplyCompleted(int result, Exception error)
		{
			Console.WriteLine("worker completed, result: " + result.ToString());
		}

	}

	public class MultiplyArgument
	{
		public int A
		{
			get;
			set;
		}

		public int B
		{
			get;
			set;
		}

	}
}
