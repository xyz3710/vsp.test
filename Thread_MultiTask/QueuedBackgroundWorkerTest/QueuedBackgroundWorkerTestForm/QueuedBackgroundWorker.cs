﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace QueuedBackgroundWorkerTestForm
{
	/// <summary>
	/// This is thread-safe
	/// </summary>
	public class QueuedBackgroundWorker
	{
		public delegate void WorkerCompletedDelegate<K>(K result, Exception error);

		private Queue<object> Queue;
		private object lockingObject;

		public QueuedBackgroundWorker()
		{
			lockingObject = new object();
			Queue = new Queue<object>();
		}

		#region Methods

		/// <summary>
		/// doWork is a method with one argument
		/// </summary>
		/// <typeparam name="T">is the type of the input parameter</typeparam>
		/// <typeparam name="K">is the type of the output result</typeparam>
		/// <param name="inputArgument"></param>
		/// <param name="doWork"></param>
		/// <param name="workerCompleted"></param>
		public void RunAsync<T, K>(Func<T, K> doWork, T inputArgument, WorkerCompletedDelegate<K> workerCompleted)
		{
			BackgroundWorker bw = GetBackgroundWorker<T, K>(doWork, workerCompleted);

			Queue.Enqueue(new QueueItem(bw, inputArgument));

			lock (lockingObject)
			{
				if (Queue.Count == 1)
				{
					((QueueItem)Queue.Peek()).RunWorkerAsync();
				}
			}
		}

		/// <summary>
		/// Use this method if you don't need to handle when the worker is completed
		/// </summary>
		/// <param name="doWork"></param>
		/// <param name="inputArgument"></param>
		public void RunAsync<T, K>(Func<T, K> doWork, T inputArgument)
		{
			RunAsync(doWork, inputArgument, null);
		}

		private BackgroundWorker GetBackgroundWorker<T, K>(Func<T, K> doWork, WorkerCompletedDelegate<K> workerCompleted)
		{
			BackgroundWorker bw = new BackgroundWorker()
			{
				WorkerReportsProgress = false,
				WorkerSupportsCancellation = false
			};

			bw.DoWork += (sender, args) =>
			{
				if (doWork != null)
				{
					args.Result = (K)doWork((T)args.Argument);
				}
			};

			bw.RunWorkerCompleted += (sender, args) =>
			{
				if (workerCompleted != null)
				{
					workerCompleted((K)args.Result, args.Error);
				}
				Queue.Dequeue();
				lock (lockingObject)
				{
					if (Queue.Count > 0)
					{
						((QueueItem)Queue.Peek()).RunWorkerAsync();
					}
				}
			};
			return bw;
		}

		#endregion
	}

	public class QueueItem
	{
		public QueueItem(BackgroundWorker backgroundWorker, object argument)
		{
			BackgroundWorker = backgroundWorker;
			Argument = argument;
		}

		public object Argument
		{
			get;
			private set;
		}

		public BackgroundWorker BackgroundWorker
		{
			get;
			private set;
		}

		public void RunWorkerAsync()
		{
			BackgroundWorker.RunWorkerAsync(Argument);
		}

	}
}
