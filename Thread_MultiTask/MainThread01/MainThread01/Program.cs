using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MainThread01
{
	public class Top
	{
		private int _limit = 0;

		public void SayHello()
		{
			try
			{
				while (_limit < 10)
				{
					Console.WriteLine("Thread {0} : {1}", Thread.CurrentThread.GetHashCode(), _limit++);
					Thread.Sleep(1000);

					if (_limit == 5)
					{
						//Thread.CurrentThread.Suspend();
					}
				}
			}
			catch (ThreadAbortException ex)
			{
				Console.WriteLine("Occurred ThreadAbortException : {0}", ex.Message);
			}
		}
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			Thread thread = Thread.CurrentThread;

			Console.WriteLine("1. HashCode\t{0}", thread.GetHashCode());
			Console.WriteLine("2. Name\t{0}", thread.Name);
			Console.WriteLine("3. CurrentCulture\t{0}", thread.CurrentCulture);
			Console.WriteLine("4. Priority\t{0}", thread.Priority);
			Console.WriteLine("5. State\t{0}", thread.ThreadState);
			Console.WriteLine("6. IsBackground\t{0}\n", thread.IsBackground);


			Top t = new Top();
			ThreadStart ts = new ThreadStart(t.SayHello);
			Thread thread1 = new Thread(ts);

			thread1.Start();
			Console.WriteLine("Thread {0} 메인 3초 Sleep 설정", Thread.CurrentThread.GetHashCode());
			Thread.Sleep(3000);

			thread1.Abort();
			
			Console.WriteLine("Thread {0} 메인 3초 Sleep 해제", Thread.CurrentThread.GetHashCode());
			//thread.Resume();

			Console.WriteLine("\nEnd of thread1\n");
		}
	}
}
