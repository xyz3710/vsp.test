using System;
using System.Runtime.Remoting.Messaging;

namespace AsyncDelegateTest02
{
	/// <summary>
	/// Class1에 대한 요약 설명입니다.
	/// </summary>
	public class Class1
	{
		// delegate 선언
		public delegate void ProgressValue();

		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			// delegate instance 생성
			ProgressValue progressValue = new ProgressValue(asyncMethod);
			
			// callback method 선언
			AsyncCallback callBack = new AsyncCallback(asyncCallbackMethod);

			// 비동기 호출 초기화
			IAsyncResult ar = progressValue.BeginInvoke(callBack, progressValue);
	
			Console.ReadLine();
		}

		/// <summary>
		/// 비동기 mehtod
		/// </summary>
		/// <param name="value"></param>
		private static void asyncMethod()
		{
			for (int i = 0; i < 1000000; i++)
			{
				Console.WriteLine("Value : {0}", i);
			}
		}

		/// <summary>
		/// 비동기 callback method
		/// </summary>
		/// <param name="ar"></param>
		private static void asyncCallbackMethod(IAsyncResult ar)
		{
			ProgressValue progressValueDelegate = (ProgressValue)ar.AsyncState;

			progressValueDelegate.EndInvoke(ar);
		}
	}
}
