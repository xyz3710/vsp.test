﻿using System;
using System.Threading;

public class TestDriver
{
	static void Main()
	{
		BackgroundTest shortTest = new BackgroundTest(10);
		Thread foregroundThread = 
            new Thread(new ThreadStart(shortTest.RunLoop));
		foregroundThread.Name = "ForegroundThread";

		BackgroundTest longTest = new BackgroundTest(50);
		Thread backgroundThread = 
            new Thread(new ThreadStart(longTest.RunLoop));
		backgroundThread.Name = "BackgroundThread";
		backgroundThread.IsBackground = true;

		foregroundThread.Start();
		backgroundThread.Start();
	}
}
