using System;
using System.Threading;

public class BackgroundTest
{
	int maxIterations;

	public BackgroundTest(int maxIterations)
	{
		this.maxIterations = maxIterations;
	}

	public void RunLoop()
	{
		String threadName = Thread.CurrentThread.Name;

		for (int i = 0; i < maxIterations; i++)
		{
			Console.WriteLine("{0} count: {1}",
				threadName, i.ToString());
			Thread.Sleep(250);
		}
		Console.WriteLine("{0} finished counting.", threadName);
	}
}