﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ResetEventTests
{
	class NormalClass
	{
		private System.Timers.Timer _timer;
		private Random _rnd;

		public NormalClass()
		{
			_rnd = new Random(1000);
			// 1초에 한번씩 이벤트 발생
			_timer = new System.Timers.Timer();

			_timer.Interval = 1000;
			_timer.Elapsed += _timer_Elapsed;
		}

		public void Run()
		{
			_timer.Start();
		}

		void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			DateTime current = DateTime.Now;

			// 임의의 프로세스 처리시간 대기
			int val = _rnd.Next(0, 10000);
			Thread.Sleep(val);

			Console.WriteLine(current.ToString("HH:mm:ss"));
		}
	}
}
