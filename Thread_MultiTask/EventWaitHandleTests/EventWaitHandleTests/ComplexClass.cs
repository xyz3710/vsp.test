﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ResetEventTests
{
	class ComplexClass
	{
		private int _count;
		private EventWaitHandle _waitHandle;

		public ComplexClass()
		{
			_count = 0;
			Console.Write("1: AutoResetEvent\t2: ManualResetEvent - ");

			switch (Console.ReadKey().KeyChar)
			{
				case '1':
					_waitHandle = new AutoResetEvent(true);

					break;
				case '2':
					_waitHandle = new ManualResetEvent(true);

					break;
			}

			Console.WriteLine();
			var t1 = new Thread(new ThreadStart(Work));
			var t2 = new Thread(new ThreadStart(Work));
			var t3 = new Thread(new ThreadStart(Work));
			var t4 = new Thread(new ThreadStart(Work));

			t1.Start();
			t2.Start();
			t3.Start();
			t4.Start();
		}
		private void Work()
		{
			_waitHandle.WaitOne();

			for (int i = 0; i < 5; i++)
			{
				Console.WriteLine(_count++);
				Thread.Sleep(1000);
			}

			_waitHandle.Set();
		}
	}
}
