using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ResetEventTests
{
	public class UserHandleClass
	{
		private int _count;

		public UserHandleClass()
		{
			_count = 0;
			EventWait = new AutoResetEvent(false);
		}

		private EventWaitHandle EventWait
		{
			get;
			set;
		}

		private bool IsBegin
		{
			get;
			set;
		}

		private bool IsStart
		{
			get;
			set;
		}
		
		
		public void Start()
		{
			IsStart = true;
			EventWait.Set();
		}

		public void Stop()
		{
			IsStart = false;
			EventWait.Reset();
		}

		public void End()
		{
			IsBegin = false;
		}

		public void Begin()
		{
			IsBegin = true;

			while (IsBegin == true)
			{
				EventWait.WaitOne();

				Console.WriteLine("Count: {0}", _count++);
				Thread.Sleep(1000);

				if (IsStart == true)
				{
					EventWait.Set();
				}
			}
		}
	}
}
