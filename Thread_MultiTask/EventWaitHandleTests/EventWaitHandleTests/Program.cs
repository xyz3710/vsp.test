﻿// ****************************************************************************************************************** //
//	Domain		:	ResetEventTests.Program
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	Wednesday, June 11, 2014 10:04 AM
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	http://kuimoani.tistory.com/entry/C-%EC%8A%A4%EB%A0%88%EB%93%9C-%EB%8F%99%EA%B8%B0%ED%99%94%EB%A5%BC-%EC%9C%84%ED%95%9C-AutoResetEvent%EC%99%80-ManualResetEvent
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="Program.cs" company="(주)가치소프트">
//		Copyright (c) 2014. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ResetEventTests
{
	class Program
	{
		static void Main(string[] args)
		{
			//(new NormalClass()).Run();

			//(new ManualResetClass()).Run();

			//(new AutoResetClass()).Run();

			//ComplexClass complexClass = new ComplexClass();
			//Thread.Sleep(1000);

			// http://csharpstudy.com/Threads/autoresetevent.aspx
			//TrafficTest();

			UserHandlingTest();
		}

		private static void TrafficTest()
		{
			Traffic traffic = new Traffic();

			// 2개의 쓰레드 구동
			Thread v = new Thread(traffic.ProcessVertical);
			Thread h = new Thread(traffic.ProcessHorizontal);
			v.Start();
			h.Start();

			// 메인쓰레드에서 데이타 전송
			for (int i = 0; i < 30; i += 3)
			{
				traffic.AddVertical(new int[] { i, i + 1, i + 2 });
				traffic.AddHorizontal(new int[] { i, i + 1, i + 2 });
				Thread.Sleep(10);
			}

			Thread.Sleep(1000);
			traffic.Running = false;
		}
		private static void UserHandlingTest()
		{
			var userHandle = new UserHandleClass();
			bool terminate = false;

			Task.Factory.StartNew(() =>
			{
				userHandle.Begin();
			});

			while (terminate == false)
			{
				Console.WriteLine(string.Empty.PadLeft(20, '='));
				Console.WriteLine("s: To Start");
				Console.WriteLine("t: To Stop");
				Console.WriteLine("q: To End test");
				Console.WriteLine(string.Empty.PadLeft(20, '='));

				switch (Console.ReadKey().KeyChar)
				{
					case 's':
						userHandle.Start();

						break;
					case 't':
						userHandle.Stop();

						break;
					case 'q':
						userHandle.End();
						terminate = true;

						break;
				}
			}
		}

	}
}
