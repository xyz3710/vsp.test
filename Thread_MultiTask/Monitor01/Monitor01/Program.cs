using System;
using System.Text;
using System.Threading;

namespace Monitor01
{
	public class Top
	{
		private static object obj = new object();

		public void SayHello()
		{
			int hash = Thread.CurrentThread.GetHashCode();
			int count = 0;
			
			Monitor.Enter(Top.obj);		// 동기화 진입점

			try
			{
				while (count < 10)
				{
					Console.WriteLine("Thread {0} : {1}", hash, count++);
					Thread.Sleep(10);

					if (count == 5)
					{
						throw new Exception();
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				Monitor.Exit(Top.obj);		// 동기화 종료점
			}
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Top t1 = new Top();
			Top t2 = new Top();

			Thread thread1 = new Thread(new ThreadStart(t1.SayHello));
			Thread thread2 = new Thread(new ThreadStart(t1.SayHello));
			
			thread1.Start();
			thread2.Start();

			Console.WriteLine("Thread {0} has terminated.", Thread.CurrentThread.GetHashCode());
		}
	}
}
