﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
namespace RPassStat
{
	class Program
	{
		static void Main(string[] args)
		{
			var path = @"D:\R-PassImages";

			if (Directory.Exists(path) == false)
			{
				MessageBox.Show(
					string.Format("{0} 폴더가 없습니다.", path),
					string.Empty,
					MessageBoxButtons.OK,
					MessageBoxIcon.Warning);
				
			}
			var dirs = Directory.GetDirectories(path).OrderBy(x => x);
			var dateDirs = dirs.ToDictionary(x => Path.GetFileNameWithoutExtension(x), x =>
			{
				return Directory.GetFiles(x).Length;
			});
			var sum = dateDirs.GroupBy(x => x.Key.Substring(0, 6), x => x)
				.Select(x => new
				{
					Month = string.Format("{0}-{1}", x.Key.Substring(0, 4), x.Key.Substring(4, 2)),
					Sum = x.Sum(y => y.Value),
				});
			List<string> sumData = sum.Select(x => string.Format("{0} : {1:#,##0}", x.Month, x.Sum)).ToList();

			sumData.Insert(0, "========== 월별 ==========");
			sumData.Add("\r\n========== 일별 ==========");
			sumData.AddRange(dateDirs.Keys.Select(x => string.Format("{0}-{1}-{2} : {3:#,##0}", x.Substring(0, 4), x.Substring(4, 2), x.Substring(6), dateDirs[x])));

			File.WriteAllLines(@"R-PassStat.txt", sumData);
		}
	}
}
