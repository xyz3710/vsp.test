﻿namespace ImageTest
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.pbImage = new System.Windows.Forms.PictureBox();
			this.btnLoad = new System.Windows.Forms.Button();
			this.btnZoom1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
			this.SuspendLayout();
			// 
			// pbImage
			// 
			this.pbImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbImage.Location = new System.Drawing.Point(12, 12);
			this.pbImage.Name = "pbImage";
			this.pbImage.Size = new System.Drawing.Size(1023, 606);
			this.pbImage.TabIndex = 0;
			this.pbImage.TabStop = false;
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(13, 625);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(75, 23);
			this.btnLoad.TabIndex = 1;
			this.btnLoad.Text = "&Load";
			this.btnLoad.UseVisualStyleBackColor = true;
			this.btnLoad.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnZoom1
			// 
			this.btnZoom1.Location = new System.Drawing.Point(94, 624);
			this.btnZoom1.Name = "btnZoom1";
			this.btnZoom1.Size = new System.Drawing.Size(75, 23);
			this.btnZoom1.TabIndex = 1;
			this.btnZoom1.Text = "&Zoom addressee";
			this.btnZoom1.UseVisualStyleBackColor = true;
			this.btnZoom1.Click += new System.EventHandler(this.btnZoom1_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1047, 665);
			this.Controls.Add(this.btnZoom1);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.pbImage);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.PictureBox pbImage;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.Button btnZoom1;
	}
}

