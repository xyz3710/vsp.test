using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GraphicElements
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void pointToolStripMenuItem_Click(object sender, EventArgs e)
		{
				
		}

		private void lineToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}

		private void rectangleToolStripMenuItem_Click(object sender, EventArgs e)
		{

		}

		private void circleToolStripMenuItem_Click(object sender, EventArgs e)
		{
		
		}

		private void Form1_Paint(object sender, PaintEventArgs e)
		{
			Bitmap bmp = new Bitmap(this.ClientRectangle.Width, this.ClientRectangle.Height);

			//Graphics gImage = Graphics.FromImage(bmp);

			//gImage.FillRectangle(Brushes.Red, 0, 0, 20, 20);

			//gImage.DrawRectangle(Pens.Black, 10, 10, bmp.Width-20, bmp.Height-20);



			Graphics gScreen = e.Graphics;

			gScreen.FillRectangle(Brushes.White, this.ClientRectangle);

			gScreen.DrawImage(bmp, new Rectangle(10, 10, bmp.Width, bmp.Height));
		}

		private void Form1_Click(object sender, EventArgs e)
		{
			// Point
			int pointWidth = 1;
			Size pointSize = new Size(pointWidth, pointWidth);

			Pen pointPen = new Pen(Color.Red, pointWidth);
			Graphics pointGraph = this.CreateGraphics();

			pointGraph.DrawRectangle(pointPen, new Rectangle(getCurrentPosition(), pointSize));

			pointPen.Dispose();
			pointGraph.Dispose();

			
			// Ellipse
			Size ellipseSize = new Size(400, 200);
			Pen ellipsePen = new Pen(Color.Red, 1);
			Graphics ellipseGraph = this.CreateGraphics();

			ellipseGraph.DrawEllipse(ellipsePen, new Rectangle(getCurrentPosition(), ellipseSize));
			ellipseGraph.FillEllipse(Brushes.Gold, new Rectangle(getCurrentPosition(), ellipseSize));

			ellipsePen.Dispose();
			ellipseGraph.Dispose();
		}

		private Point getCurrentPosition()
		{
			Point currentPosition = new Point(
								MousePosition.X - this.Location.X - 5,
								MousePosition.Y - this.Location.Y - 24
							);

			return currentPosition;
		}
	}
}