﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace TransparentForm
{
	public partial class Form1 : Form
	{
		private Form _overlay = new Form();
		private Form _nonTrans = new Form();

		public Form1()
		{
			InitializeComponent();
			this.TopMost = true;
			//this.FormBorderStyle = FormBorderStyle.None;
			this.TransparencyKey = this.BackColor = Color.Magenta;
			exceptPanel.BackColor = BackColor;
			this.Opacity = 0.7;
			this.Paint += PaintBackground;

			_overlay.TopMost = true;
			_overlay.FormBorderStyle = FormBorderStyle.None;
			//_overlay.TransparencyKey = _overlay.BackColor = Color.Magenta;
			_overlay.StartPosition = FormStartPosition.Manual;
			_overlay.Location = this.Location;
			_overlay.MouseDown += HandleIconClick;
			_overlay.Paint += PaintIcons;
			_overlay.Click += (senderObject, ea) =>
			{
				lblMessage.Text = "폼이 감춰졌습니다.";
				_overlay.Hide();
			};
			this.Resize += delegate
			{
				_overlay.Size = this.Size;
			};
			this.LocationChanged += delegate
			{
				_overlay.Location = this.Location;
				_nonTrans.Location = pnlNonTrans.PointToScreen(Point.Empty);
				_nonTrans.BringToFront();
			};

			var loc = pnlNonTrans.PointToScreen(Point.Empty);
			//var loc = pnlNonTrans.FindForm().PointToClient(pnlNonTrans.Parent.PointToScreen(pnlNonTrans.Location));

			_nonTrans.ShowInTaskbar = false;
			_nonTrans.TransparencyKey = _nonTrans.BackColor;
			_nonTrans.TopMost = true;
			_nonTrans.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			_nonTrans.StartPosition = FormStartPosition.Manual;
			_nonTrans.Size = pnlNonTrans.Size;
			_nonTrans.Location = pnlNonTrans.Location;
			_nonTrans.Controls.Add(pnlNonTrans);
			_nonTrans.Show();
			_nonTrans.BringToFront();


		}

		void pnlNotTrans_Paint(object sender, PaintEventArgs e)
		{
			var rc = new Rectangle(0, 0, this.ClientSize.Width, this.ClientSize.Height);

			using (var brush = new LinearGradientBrush(rc, Color.FromArgb(0x99, 0xFF, 0x00), Color.White, 90f))
			{
				e.Graphics.FillRectangle(brush, rc);
			}
		}
		private void PaintBackground(object sender, PaintEventArgs e)
		{
			var rc = new Rectangle(0, 0, this.ClientSize.Width, this.ClientSize.Height);

			using (var brush = new LinearGradientBrush(rc, Color.Gray, Color.White, 90f))
			{
				e.Graphics.FillRectangle(brush, rc);
			}
		}
		private void PaintIcons(object sender, PaintEventArgs e)
		{
			e.Graphics.DrawIcon(Properties.Resources.R_Pass, 50, 30);
			// etc...
		}
		void HandleIconClick(object sender, MouseEventArgs e)
		{
			lblMessage.Text = "아이콘이 눌렸습니다.";
		}

		private void btnOverlay_Click(object sender, EventArgs e)
		{
			_overlay.Show();
		}
	}
}
