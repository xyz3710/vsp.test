﻿namespace TransparentForm
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnOverlay = new System.Windows.Forms.Button();
			this.lblMessage = new System.Windows.Forms.Label();
			this.exceptPanel = new System.Windows.Forms.Panel();
			this.pnlNonTrans = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.pnlNonTrans.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnOverlay
			// 
			this.btnOverlay.Location = new System.Drawing.Point(274, 538);
			this.btnOverlay.Name = "btnOverlay";
			this.btnOverlay.Size = new System.Drawing.Size(164, 23);
			this.btnOverlay.TabIndex = 0;
			this.btnOverlay.Text = "Overlay 폼 보이기";
			this.btnOverlay.UseVisualStyleBackColor = true;
			this.btnOverlay.Click += new System.EventHandler(this.btnOverlay_Click);
			// 
			// lblMessage
			// 
			this.lblMessage.Location = new System.Drawing.Point(497, 538);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(390, 23);
			this.lblMessage.TabIndex = 1;
			// 
			// exceptPanel
			// 
			this.exceptPanel.BackColor = System.Drawing.Color.Magenta;
			this.exceptPanel.Location = new System.Drawing.Point(465, 67);
			this.exceptPanel.Name = "exceptPanel";
			this.exceptPanel.Size = new System.Drawing.Size(339, 201);
			this.exceptPanel.TabIndex = 2;
			// 
			// pnlNonTrans
			// 
			this.pnlNonTrans.BackColor = System.Drawing.Color.White;
			this.pnlNonTrans.Controls.Add(this.button1);
			this.pnlNonTrans.Location = new System.Drawing.Point(465, 274);
			this.pnlNonTrans.Name = "pnlNonTrans";
			this.pnlNonTrans.Size = new System.Drawing.Size(339, 49);
			this.pnlNonTrans.TabIndex = 2;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(65, 14);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(200, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "not Transparent";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(939, 573);
			this.Controls.Add(this.pnlNonTrans);
			this.Controls.Add(this.exceptPanel);
			this.Controls.Add(this.lblMessage);
			this.Controls.Add(this.btnOverlay);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.pnlNonTrans.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnOverlay;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.Panel exceptPanel;
		private System.Windows.Forms.Panel pnlNonTrans;
		private System.Windows.Forms.Button button1;
	}
}

