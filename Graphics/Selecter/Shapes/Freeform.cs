using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selecter
{
	/// <summary>
	/// Class Freeform.
	/// </summary>
	public class Freeform : Shape
	{
		/// <summary>
		/// Initializes a new instance of the Freeform class.
		/// </summary>
		/// <param name="points">The points.</param>
		public Freeform(Point[] points)
		{
			base.Points = points;
		}

		/// <summary>
		/// 현재 선택된 타입 종류를 구하거나 설정합니다.
		/// </summary>
		/// <value>선택된 타입</value>
		/// <exception cref="System.NotImplementedException"></exception>
		public override ShapeTypes ShapeType
		{
			get
			{
				return ShapeTypes.FreeForm;
			}
		}

		/// <summary>
		/// Determines whether the specified point is visible.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <returns><c>true</c> if the specified point is visible; otherwise, <c>false</c>.</returns>
		/// <exception cref="System.NotImplementedException"></exception>
		public override bool IsVisible(Point point)
		{
			bool result = false;

			using (GraphicsPath gp = new GraphicsPath())
			{
				gp.AddPolygon(base.Points);
				result = gp.IsVisible(point);
			}

			return result;
		}
	}
}

