using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selecter
{
	/// <summary>
	/// Class Ellipse.
	/// </summary>
	public class Ellipse : Shape
	{
		/// <summary>
		/// Initializes a new instance of the Ellipse class.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		public Ellipse(Rectangle rectangle)
		{
			base.Rectangle = rectangle;
		}

		/// <summary>
		/// 현재 선택된 타입 종류를 구하거나 설정합니다.
		/// </summary>
		/// <value>선택된 타입</value>
		/// <exception cref="System.NotImplementedException"></exception>
		public override ShapeTypes ShapeType
		{
			get
			{
				return ShapeTypes.Ellipse;
			}
		}

		/// <summary>
		/// Determines whether the specified point is visible.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <returns><c>true</c> if the specified point is visible; otherwise, <c>false</c>.</returns>
		/// <exception cref="System.NotImplementedException"></exception>
		public override bool IsVisible(Point point)
		{
			bool result = false;

			using (GraphicsPath gp = new GraphicsPath())
			{
				gp.AddEllipse(base.Rectangle);
				result = gp.IsVisible(point);
			}

			return result;
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
		public override int GetHashCode()
		{
			return base.Rectangle.GetHashCode();
		}
	}
}
