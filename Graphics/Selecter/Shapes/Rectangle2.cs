using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selecter
{
	/// <summary>
	/// Class Rectangle2.
	/// </summary>
	public class Rectangle2 : Shape
	{
		/// <summary>
		/// Initializes a new instance of the Rectangle2 class.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		public Rectangle2(Rectangle rectangle)
		{
			base.Rectangle = rectangle;
		}

		/// <summary>
		/// 현재 선택된 타입 종류를 구하거나 설정합니다.
		/// </summary>
		/// <value>선택된 타입</value>
		/// <exception cref="System.NotImplementedException"></exception>
		public override ShapeTypes ShapeType
		{
			get
			{
				return ShapeTypes.Rectangle;
			}
		}

		/// <summary>
		/// Determines whether the specified point is visible.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <returns><c>true</c> if the specified point is visible; otherwise, <c>false</c>.</returns>
		public override bool IsVisible(Point point)
		{
			return base.Rectangle.Contains(point);
		}
	}
}

