﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selecter
{
	/// <summary>
	/// Class Shape.
	/// </summary>
	public abstract class Shape : IEqualityComparer<Shape>
	{
		/// <summary>
		/// Initializes a new instance of the Shape class.
		/// </summary>
		public Shape()
		{
			Region = new Region();
			GraphicsPath = new GraphicsPath();
		}

		/// <summary>
		/// 현재 선택된 타입 종류를 구하거나 설정합니다.
		/// </summary>
		/// <value>선택된 타입</value>
		public abstract ShapeTypes ShapeType
		{
			get;
		}

		public abstract bool IsVisible(Point point);

		/// <summary>
		/// Region를 구하거나 설정합니다.
		/// </summary>
		/// <value>Region를 반환합니다.</value>
		public Region Region
		{
			get;
			set;
		}

		/// <summary>
		/// GraphicsPath를 구하거나 설정합니다.
		/// </summary>
		/// <value>GraphicsPath를 반환합니다.</value>
		public GraphicsPath GraphicsPath
		{
			get;
			set;
		}

		/// <summary>
		/// Color를 구하거나 설정합니다.
		/// </summary>
		/// <value>Color를 반환합니다.</value>
		public Color Color
		{
			get;
			set;
		}
		
		/// <summary>
		/// Points를 구하거나 설정합니다.
		/// </summary>
		/// <value>Points를 반환합니다.</value>
		public Point[] Points
		{
			get;
			set;
		}

		/// <summary>
		/// Rectangle를 구하거나 설정합니다.
		/// </summary>
		/// <value>Rectangle를 반환합니다.</value>
		public Rectangle Rectangle
		{
			get;
			set;
		}

		/// <summary>
		/// Rectangle에 값이 할당되어있는지 여부를 구하거나 설정합니다.
		/// </summary>
		/// <value>Rectangle에 값이 할당되어 있을 경우 true를 반환합니다.</value>
		protected bool IsRectangle
		{
			get
			{
				return ShapeType == ShapeTypes.Rectangle || ShapeType == ShapeTypes.Ellipse;
			}
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
		/// </summary>
		/// <param name="obj">현재 개체와 비교할 개체입니다.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			var area = obj as Shape;

			return Equals(this, area);
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
		public override int GetHashCode()
		{
			return GetHashCode(this);
		}

		#region IEqualityComparer<Shape> 멤버

		/// <summary>
		/// 지정한 개체가 같은지 여부를 확인합니다.
		/// </summary>
		/// <param name="x">비교할 <paramref name="T" /> 형식의 첫 번째 개체입니다.</param>
		/// <param name="y">비교할 <paramref name="T" /> 형식의 두 번째 개체입니다.</param>
		/// <returns>지정한 개체가 같으면 true이고, 그렇지 않으면 false입니다.</returns>
		public bool Equals(Shape x, Shape y)
		{
			if (x.IsRectangle == true && y.IsRectangle == true)
			{
				return x.Rectangle.Location == y.Rectangle.Location &&
					x.Rectangle.Size == y.Rectangle.Size;
			}
			else if (x.IsRectangle == false && y.IsRectangle == false)
			{
				return x.Points == y.Points;
			}

			return false;
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <param name="obj">해시 코드가 반환될 <see cref="T:System.Object" />입니다.</param>
		/// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
		public int GetHashCode(Shape obj)
		{
			if (obj.IsRectangle == true)
			{
				return obj.Rectangle.GetHashCode();
			}
			else if (obj.IsRectangle == false)
			{
				return obj.Points.GetHashCode();
			}

			return 0;
		}

		#endregion
	}
}
