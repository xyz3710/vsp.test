﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.Threading;

namespace Selecter
{
	public partial class MainForm : Form
	{
		private int _marqueeOffset;

		public MainForm()
		{
			InitializeComponent();
			Selection = new AdvSelection();
			Selecting = new AdvSelection();
			MouseTracer = new MouseTracer();
			LeftColor = Color.FromArgb(0xFF, 0xCC, 0x00);
			RightColor = Color.FromArgb(0xFF, 0xFF, 0x99);
			//pictureBox.Image = new Bitmap(pictureBox.Width, pictureBox.Height);
		}

		private ShapeTypes SelectionTool
		{
			get;
			set;
		}

		private MouseTracer MouseTracer
		{
			get;
			set;
		}

		/// <summary>
		/// 현재 선택중인 선택 영역을 구하거나 설정 합니다.
		/// </summary>
		private AdvSelection Selecting
		{
			get;
			set;
		}

		/// <summary>
		/// 선택된 영역을 구하거나 설정합니다.
		/// </summary>
		private AdvSelection Selection
		{
			get;
			set;
		}

		private Color LeftColor
		{
			get;
			set;
		}

		private Color RightColor
		{
			get;
			set;
		}

		private void MarqueeTimer_Tick(object sender, EventArgs e)
		{
			_marqueeOffset++;
			_marqueeOffset %= 6;
			pictureBox.Refresh();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			SuspendLayout();
			var screen = Screen.AllScreens[0];
			Location = new Point(screen.WorkingArea.Right - Width, screen.WorkingArea.Top);
			WindowState = FormWindowState.Normal;
			ResumeLayout();

			#region 투명한 메인폼 만들기
			/*
			GraphicsPath path = new GraphicsPath(FillMode.Alternate);
			path.AddString("Close? Right Click!", new FontFamily("Verdana"), (int)FontStyle.Bold, 50, new Point(0, 0), StringFormat.GenericDefault);
			path.AddRectangle(new Rectangle(20, 70, 100, 100));
			path.AddEllipse(new Rectangle(140, 70, 100, 100));
			path.AddEllipse(new Rectangle(260, 70, 100, 100));
			path.AddRectangle(new Rectangle(380, 70, 100, 100));
			Region rgn = new Region(path);
			this.Region = rgn;
			*/
			#endregion
		}

		private void btnRectangle_Click(object sender, EventArgs e)
		{
			SelectionTool = ShapeTypes.Rectangle;
		}

		private void btnEllipse_Click(object sender, EventArgs e)
		{
			SelectionTool = ShapeTypes.Ellipse;
		}

		private void btnFreeform_Click(object sender, EventArgs e)
		{
			SelectionTool = ShapeTypes.FreeForm;
		}

		private void btnLoadImage_Click(object sender, EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			if (ofd.ShowDialog() == DialogResult.OK)
			{
				pictureBox.Image = Bitmap.FromFile(ofd.FileName);
			}

			pictureBox.Refresh();
		}

		private void Picture_Paint(object sender, PaintEventArgs e)
		{
			//e.Graphics.DrawImageUnscaled(pictureBox.Image, 0, 0);
			var selectorColor = GetSelectorColor();
			// Box 형태로 보일경우
			var selectorBrush = new SolidBrush(selectorColor);
			var selectorWidth = 4;
			var selectorPen = new Pen(selectorBrush, selectorWidth);

			if (cbFillSelector.Checked == true)
			{
				// 선택할 때 색으로 채울 경우
				e.Graphics.FillRegion(new SolidBrush(selectorColor), Selecting.Selected);
			}
			else
			{
				switch (SelectionTool)
				{
					case ShapeTypes.Rectangle:
						e.Graphics.DrawRectangle(selectorPen, Rectangle.Round(Selecting.Selected.GetBounds(e.Graphics)));

						break;
					case ShapeTypes.Ellipse:
						e.Graphics.DrawEllipse(selectorPen, Rectangle.Round(Selecting.Selected.GetBounds(e.Graphics)));

						break;
					case ShapeTypes.FreeForm:
						// TODO: 
						// by KIMKIWON\xyz37(김기원) in 2014년 4월 8일 화요일 오후 3:28
						// e.Graphics.DrawPolygon(selectorPen, Rectangle.Round(Selecting.Selected.GetBounds(e.Graphics)));

						break;
				}
			}

			if (cbFill.Checked == true)
				e.Graphics.FillRegion(new SolidBrush(selectorColor), Selection.Selected);

			Bitmap ant = new Bitmap(pictureBox.Width, pictureBox.Height);

			using (Graphics g = Graphics.FromImage(ant))
			//Graphics g = e.Graphics;
			{
				g.Clear(Color.Magenta);
				//g.DrawPath(new Pen(Color.Black), Selection.Line());		// border

				if (cbMarquee.Checked == true)
				{
					using (Pen marqueePen = new Pen(Color.White, 3)
						{
							DashStyle = System.Drawing.Drawing2D.DashStyle.Dash,
							DashPattern = new float[2] { 3, 3 },
							DashOffset = _marqueeOffset
						})
					{
						g.DrawPath(marqueePen, Selection.Line());		// marquee
					}
				}

				//g.FillRegion(new SolidBrush(Color.Magenta), Selection.Selected);
				//g.FillRegion(new SolidBrush(Color.FromArgb(128, 128, 255, 255)), Selection.Selected);

				if (cbFillSelector.Checked == true)
				{
					g.FillPath(new SolidBrush(selectorColor), Selection.Outline);
				}
				else
				{
					foreach (Shape shape in Selection.Shapes)
					{
						using (var brush = new SolidBrush(shape.Color))
						{
							using (var shapePen = new Pen(brush, selectorWidth))
							{
								switch (shape.ShapeType)
								{
									case ShapeTypes.Rectangle:
										g.DrawRectangle(shapePen, shape.Rectangle);

										break;
									case ShapeTypes.Ellipse:
										g.DrawEllipse(shapePen, shape.Rectangle);

										break;
									case ShapeTypes.FreeForm:
										g.DrawPolygon(shapePen, shape.Points);

										break;
								}
							}
						}

						// 색으로 채울 경우
						//g.FillPath(brush, shape.GraphicsPath);
					}
				}
			}

			ant.MakeTransparent(Color.Magenta);
			e.Graphics.DrawImageUnscaled(ant, 0, 0);
			ant.Dispose();

			UpdateText();
		}

		private void Picture_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Left)
			{
				SelectionTool = ShapeTypes.Rectangle;
			}
			else if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{
				SelectionTool = ShapeTypes.Ellipse;
			}

			MouseTracer.MouseDown(e);
		}

		private void Picture_MouseMove(object sender, MouseEventArgs e)
		{
			if (MouseTracer.IsMouseDown == true)
			{
				MouseTracer.MouseMove(e);
				Selecting.Clear();

				switch (SelectionTool)
				{
					case ShapeTypes.Rectangle:
						Selecting.AddRectangle(MouseTracer.GetRectangle(), LeftColor);

						break;
					case ShapeTypes.Ellipse:
						Selecting.AddEllipse(MouseTracer.GetRectangle(), RightColor);

						break;
					case ShapeTypes.FreeForm:

						break;
				}

				(sender as Control).Refresh();
			}
		}

		private void Picture_MouseUp(object sender, MouseEventArgs e)
		{
			MouseTracer.MouseUp(e);

			switch (SelectionTool)
			{
				case ShapeTypes.Rectangle:
					Selection.AddRectangle(MouseTracer.GetRectangle(), LeftColor);
					MarqueeTimer.Start();

					break;
				case ShapeTypes.Ellipse:
					Selection.AddEllipse(MouseTracer.GetRectangle(), RightColor);
					MarqueeTimer.Start();

					break;
				case ShapeTypes.FreeForm:

					break;
			}

			Selecting.Clear();

			if (MouseTracer.IsSamePosition == true)
			{
				// 한점을 클릭하면 선택 영역을 지운다. 다만, 현재 포함된 영역이 아니어야 한다.
				if (Selection.IsVisible(MouseTracer.Start) == false)
				{
					Selection.Clear();
				}
				else
				{
					var shape = Selection.GetShape(e.Location);

					if (shape != null)
					{
						Selection.RemoveByShape(shape);
					}
				}
			}

			(sender as Control).Refresh();
			MouseTracer.MouseUpEnd();
		}

		private void UpdateText()
		{
			Invoke(new MethodInvoker(() =>
			{
				Text = string.Format("Selecting: {0}, Selection: {1}", Selecting.Shapes.Count, Selection.Shapes.Count);
			}));
		}

		private Color GetSelectorColor()
		{
			Color selectorColor = Color.Empty;

			switch (SelectionTool)
			{
				case ShapeTypes.Rectangle:
					selectorColor = LeftColor;

					break;
				case ShapeTypes.Ellipse:
					selectorColor = RightColor;

					break;
				case ShapeTypes.FreeForm:

					break;
			}

			return selectorColor;
		}
	}
}
