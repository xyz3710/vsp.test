﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Selecter
{
	/// <summary>
	/// Class for Advanced Selection.
	/// </summary>
	public class AdvSelection : IDisposable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AdvSelection"/> class.
		/// </summary>
		public AdvSelection()
		{
			InternalRegion = new Region();
			InternalGP = new GraphicsPath();
			Shapes = new List<Shape>();

			Clear();
		}

		/// <summary>
		/// Gets the selected region.
		/// </summary>
		/// <value>The selected region.</value>
		public Region Selected
		{
			get
			{
				return InternalRegion;
			}
		}

		/// <summary>
		/// Gets the outline.
		/// </summary>
		/// <value>The outline.</value>
		public GraphicsPath Outline
		{
			get
			{
				return InternalGP;
			}
		}

		/// <summary>
		/// 도형을 구하거나 설정합니다.
		/// </summary>
		/// <value>도형을 반환합니다.</value>
		public List<Shape> Shapes
		{
			get;
			set;
		}

		/// <summary>
		/// Region를 구하거나 설정합니다.
		/// </summary>
		/// <value>Region를 반환합니다.</value>
		private Region InternalRegion
		{
			get;
			set;
		}

		/// <summary>
		/// GraphicsPath를 구하거나 설정합니다.
		/// </summary>
		/// <value>GraphicsPath를 반환합니다.</value>
		private GraphicsPath InternalGP
		{
			get;
			set;
		}

		/// <summary>
		/// 특정 지점이 현재 영역에 포함되는지 여부를 반환 합니다.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <returns><c>true</c> if [is in visible] [the specified point]; otherwise, <c>false</c>.</returns>
		public bool IsVisible(Point point)
		{
			return Outline.IsVisible(point) || Shapes.Count(x => x.GraphicsPath.IsVisible(point)) > 0;
		}

		/// <summary>
		/// 지정된 포인트가 있는 <see cref="Shapes" /> 객체를 구합니다.
		/// </summary>
		/// <param name="location">The location.</param>
		/// <returns>Shape.</returns>
		public Shape GetShape(Point location)
		{
			var shapes = Shapes.Where(x => x.IsVisible(location)).ToList();

			if (shapes.Count == 0)
			{
				return null;
			}
			else if (shapes.Count == 1)
			{
				return shapes[0];
			}
			else
			{
				return shapes.Last();
			}
		}

		/// <summary>
		/// Adds the rectangle.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		/// <param name="color">The color.</param>
		public void AddRectangle(Rectangle rectangle, Color color)
		{
			Shape shape = new Rectangle2(rectangle);

			if (Shapes.Contains(shape) == false && IsZeroSize(rectangle) == false)
			{
				shape.GraphicsPath.AddRectangle(rectangle);
				shape.Region.Union(shape.GraphicsPath);
				shape.Color = color;
				Shapes.Add(shape);
			}

			InternalGP.AddRectangle(rectangle);
			InternalRegion.Union(InternalGP);
		}

		/// <summary>
		/// Adds the ellipse.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		/// <param name="color">The color.</param>
		public void AddEllipse(Rectangle rectangle, Color color)
		{
			Shape shape = new Ellipse(rectangle);

			if (Shapes.Contains(shape) == false && IsZeroSize(rectangle) == false)
			{
				shape.GraphicsPath.AddEllipse(rectangle);
				shape.Region.Union(shape.GraphicsPath);
				shape.Color = color;
				Shapes.Add(shape);
			}

			InternalGP.AddEllipse(rectangle);
			InternalRegion.Union(InternalGP);
		}

		/// <summary>
		/// Adds the freeform.
		/// </summary>
		/// <param name="points">The points.</param>
		public void AddFreeForm(Point[] points, Color color)
		{
			Shape shape = new Freeform(points);

			if (Shapes.Contains(shape) == false)
			{
				shape.GraphicsPath.AddPolygon(points);
				shape.Region.Union(shape.GraphicsPath);
				shape.Color = color;
				Shapes.Add(shape);
			}

			InternalGP.AddPolygon(points);
			InternalRegion.Union(InternalGP);
		}

		/// <summary>
		/// Removes the by shape.
		/// </summary>
		/// <param name="shape">The shape.</param>
		public void RemoveByShape(Shape shape)
		{
			switch (shape.ShapeType)
			{
				case ShapeTypes.Rectangle:
					RemoveRectangle(shape.Rectangle);

					break;
				case ShapeTypes.Ellipse:
					RemoveEllipse(shape.Rectangle);

					break;
				case ShapeTypes.FreeForm:
					RemoveFreeform(shape.Points);

					break;
			}
		}

		/// <summary>
		/// Removes the rectangle.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		public void RemoveRectangle(Rectangle rectangle)
		{
			Shape shape = new Rectangle2(rectangle);

			if (Shapes.Contains(shape) == true)
			{
				Shapes.Remove(shape);
			}

			InternalGP.AddRectangle(rectangle);
			InternalRegion.Exclude(InternalGP);
		}

		/// <summary>
		/// Removes the ellipse.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		public void RemoveEllipse(Rectangle rectangle)
		{
			Shape shape = new Ellipse(rectangle);

			if (Shapes.Contains(shape) == true)
			{
				Shapes.Remove(shape);
			}

			InternalGP.AddEllipse(rectangle);
			InternalRegion.Exclude(InternalGP);
		}

		/// <summary>
		/// Removes the freeform.
		/// </summary>
		/// <param name="points">The points.</param>
		public void RemoveFreeform(Point[] points)
		{
			Shape shape = new Freeform(points);

			if (Shapes.Contains(shape) == true)
			{
				Shapes.Remove(shape);
			}

			InternalGP.AddPolygon(points);
			InternalRegion.Exclude(InternalGP);
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			Shapes.Clear();

			InternalRegion.MakeEmpty();
			InternalGP.Reset();
		}

		/// <summary>
		/// Lines this instance.
		/// </summary>
		/// <returns>GraphicsPath.</returns>
		public GraphicsPath Line()
		{
			GraphicsPath gp = new GraphicsPath();

			if (InternalGP.PointCount > 0)
			{
				gp.AddPath(InternalGP, false);
				gp.Widen(new Pen(Color.White, 2));
			}

			return gp;
		}

		private bool IsZeroSize(Rectangle rectangle)
		{
			return rectangle.Width == 0 && rectangle.Height == 0;
		}

		#region IDisposable 멤버

		public void Dispose()
		{
			if (InternalRegion != null)
			{
				InternalRegion.Dispose();
			}

			if (InternalGP != null)
			{
				InternalGP.Dispose();
			}

			Shapes.Clear();
		}

		#endregion
	}
}
