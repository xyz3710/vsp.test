﻿namespace Selecter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.pictureBox = new System.Windows.Forms.PictureBox();
			this.btnRectangle = new System.Windows.Forms.Button();
			this.btnEllipse = new System.Windows.Forms.Button();
			this.btnLoadImage = new System.Windows.Forms.Button();
			this.MarqueeTimer = new System.Windows.Forms.Timer(this.components);
			this.cbFill = new System.Windows.Forms.CheckBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.btnFreeform = new System.Windows.Forms.Button();
			this.cbFillSelector = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.cbMarquee = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// pictureBox
			// 
			this.tableLayoutPanel1.SetColumnSpan(this.pictureBox, 7);
			this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox.Location = new System.Drawing.Point(3, 3);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(1121, 640);
			this.pictureBox.TabIndex = 1;
			this.pictureBox.TabStop = false;
			this.pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.Picture_Paint);
			this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Picture_MouseDown);
			this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Picture_MouseMove);
			this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Picture_MouseUp);
			// 
			// btnRectangle
			// 
			this.btnRectangle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnRectangle.Location = new System.Drawing.Point(3, 681);
			this.btnRectangle.Name = "btnRectangle";
			this.btnRectangle.Size = new System.Drawing.Size(154, 26);
			this.btnRectangle.TabIndex = 2;
			this.btnRectangle.Text = "Select Rectangle";
			this.btnRectangle.UseVisualStyleBackColor = true;
			this.btnRectangle.Click += new System.EventHandler(this.btnRectangle_Click);
			// 
			// btnEllipse
			// 
			this.btnEllipse.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnEllipse.Location = new System.Drawing.Point(163, 681);
			this.btnEllipse.Name = "btnEllipse";
			this.btnEllipse.Size = new System.Drawing.Size(154, 26);
			this.btnEllipse.TabIndex = 3;
			this.btnEllipse.Text = "Select Ellipse";
			this.btnEllipse.UseVisualStyleBackColor = true;
			this.btnEllipse.Click += new System.EventHandler(this.btnEllipse_Click);
			// 
			// btnLoadImage
			// 
			this.btnLoadImage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnLoadImage.Location = new System.Drawing.Point(963, 681);
			this.btnLoadImage.Name = "btnLoadImage";
			this.btnLoadImage.Size = new System.Drawing.Size(161, 26);
			this.btnLoadImage.TabIndex = 4;
			this.btnLoadImage.Text = "Load Image";
			this.btnLoadImage.UseVisualStyleBackColor = true;
			this.btnLoadImage.Click += new System.EventHandler(this.btnLoadImage_Click);
			// 
			// MarqueeTimer
			// 
			this.MarqueeTimer.Interval = 25;
			this.MarqueeTimer.Tick += new System.EventHandler(this.MarqueeTimer_Tick);
			// 
			// cbFill
			// 
			this.cbFill.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbFill.Location = new System.Drawing.Point(483, 681);
			this.cbFill.Name = "cbFill";
			this.cbFill.Size = new System.Drawing.Size(154, 26);
			this.cbFill.TabIndex = 5;
			this.cbFill.Text = "선택된 사각형 채우기";
			this.cbFill.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 7;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
			this.tableLayoutPanel1.Controls.Add(this.pictureBox, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnLoadImage, 6, 2);
			this.tableLayoutPanel1.Controls.Add(this.cbFill, 3, 2);
			this.tableLayoutPanel1.Controls.Add(this.btnRectangle, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.btnEllipse, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.btnFreeform, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.cbFillSelector, 4, 2);
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.cbMarquee, 5, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1127, 710);
			this.tableLayoutPanel1.TabIndex = 6;
			// 
			// btnFreeform
			// 
			this.btnFreeform.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnFreeform.Location = new System.Drawing.Point(323, 681);
			this.btnFreeform.Name = "btnFreeform";
			this.btnFreeform.Size = new System.Drawing.Size(154, 26);
			this.btnFreeform.TabIndex = 3;
			this.btnFreeform.Text = "Select Freeform";
			this.btnFreeform.UseVisualStyleBackColor = true;
			this.btnFreeform.Click += new System.EventHandler(this.btnFreeform_Click);
			// 
			// cbFillSelector
			// 
			this.cbFillSelector.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbFillSelector.Location = new System.Drawing.Point(643, 681);
			this.cbFillSelector.Name = "cbFillSelector";
			this.cbFillSelector.Size = new System.Drawing.Size(154, 26);
			this.cbFillSelector.TabIndex = 6;
			this.cbFillSelector.Text = "선택하는 사각형 채우기";
			this.cbFillSelector.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.tableLayoutPanel1.SetColumnSpan(this.label1, 7);
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 646);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(1121, 32);
			this.label1.TabIndex = 7;
			this.label1.Text = "마우스 왼쪽 버튼 : 사각형, 오른쪽 버튼, 타원";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// cbMarquee
			// 
			this.cbMarquee.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbMarquee.Location = new System.Drawing.Point(803, 681);
			this.cbMarquee.Name = "cbMarquee";
			this.cbMarquee.Size = new System.Drawing.Size(154, 26);
			this.cbMarquee.TabIndex = 6;
			this.cbMarquee.Text = "Marquee 보이기";
			this.cbMarquee.UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1127, 710);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button btnRectangle;
        private System.Windows.Forms.Button btnEllipse;
        private System.Windows.Forms.Button btnLoadImage;
        private System.Windows.Forms.Timer MarqueeTimer;
        private System.Windows.Forms.CheckBox cbFill;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Button btnFreeform;
		private System.Windows.Forms.CheckBox cbFillSelector;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox cbMarquee;
    }
}

