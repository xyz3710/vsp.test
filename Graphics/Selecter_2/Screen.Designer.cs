﻿namespace Selecter
{
    partial class Screen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.Picture = new System.Windows.Forms.PictureBox();
			this.SelectRectangle = new System.Windows.Forms.Button();
			this.SelectEllipse = new System.Windows.Forms.Button();
			this.LoadImage = new System.Windows.Forms.Button();
			this.MarqueeTimer = new System.Windows.Forms.Timer(this.components);
			this.Fill = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
			this.SuspendLayout();
			// 
			// Picture
			// 
			this.Picture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Picture.Location = new System.Drawing.Point(0, 0);
			this.Picture.Name = "Picture";
			this.Picture.Size = new System.Drawing.Size(1126, 639);
			this.Picture.TabIndex = 1;
			this.Picture.TabStop = false;
			this.Picture.Paint += new System.Windows.Forms.PaintEventHandler(this.Picture_Paint);
			this.Picture.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Picture_MouseDown);
			this.Picture.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Picture_MouseMove);
			this.Picture.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Picture_MouseUp);
			// 
			// SelectRectangle
			// 
			this.SelectRectangle.Location = new System.Drawing.Point(12, 662);
			this.SelectRectangle.Name = "SelectRectangle";
			this.SelectRectangle.Size = new System.Drawing.Size(145, 36);
			this.SelectRectangle.TabIndex = 2;
			this.SelectRectangle.Text = "Select Rectangle";
			this.SelectRectangle.UseVisualStyleBackColor = true;
			this.SelectRectangle.Click += new System.EventHandler(this.SelectRectangle_Click);
			// 
			// SelectEllipse
			// 
			this.SelectEllipse.Location = new System.Drawing.Point(164, 662);
			this.SelectEllipse.Name = "SelectEllipse";
			this.SelectEllipse.Size = new System.Drawing.Size(145, 36);
			this.SelectEllipse.TabIndex = 3;
			this.SelectEllipse.Text = "Select Ellipse";
			this.SelectEllipse.UseVisualStyleBackColor = true;
			this.SelectEllipse.Click += new System.EventHandler(this.SelectEllipse_Click);
			// 
			// LoadImage
			// 
			this.LoadImage.Location = new System.Drawing.Point(520, 662);
			this.LoadImage.Name = "LoadImage";
			this.LoadImage.Size = new System.Drawing.Size(145, 36);
			this.LoadImage.TabIndex = 4;
			this.LoadImage.Text = "Load Image";
			this.LoadImage.UseVisualStyleBackColor = true;
			this.LoadImage.Click += new System.EventHandler(this.LoadImage_Click);
			// 
			// AntTimer
			// 
			this.MarqueeTimer.Interval = 25;
			this.MarqueeTimer.Tick += new System.EventHandler(this.MarqueeTimer_Tick);
			// 
			// Fill
			// 
			this.Fill.AutoSize = true;
			this.Fill.Location = new System.Drawing.Point(315, 673);
			this.Fill.Name = "Fill";
			this.Fill.Size = new System.Drawing.Size(129, 16);
			this.Fill.TabIndex = 5;
			this.Fill.Text = "Fill Selected Area?";
			this.Fill.UseVisualStyleBackColor = true;
			// 
			// Screen
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1127, 710);
			this.Controls.Add(this.Fill);
			this.Controls.Add(this.LoadImage);
			this.Controls.Add(this.SelectEllipse);
			this.Controls.Add(this.SelectRectangle);
			this.Controls.Add(this.Picture);
			this.Name = "Screen";
			((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Picture;
        private System.Windows.Forms.Button SelectRectangle;
        private System.Windows.Forms.Button SelectEllipse;
        private System.Windows.Forms.Button LoadImage;
        private System.Windows.Forms.Timer MarqueeTimer;
        private System.Windows.Forms.CheckBox Fill;
    }
}

