﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selecter
{
	public abstract class Area
	{
		/// <summary>
		/// Points를 구하거나 설정합니다.
		/// </summary>
		/// <value>Points를 반환합니다.</value>
		public Point[] Points
		{
			get;
			set;
		}
	}

	public class Rectangle2 : Area
	{
	}

	public class Ellipse : Area
	{
	}

	public class Freeform : Area
	{
	}
}
