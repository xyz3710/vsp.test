﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Selecter
{
	internal class MouseTracer
	{
		/// <summary>
		/// Initializes a new instance of the MouseTracer class.
		/// </summary>
		public MouseTracer()
		{
			IsMouseDown = false;
			Start = new Point();
			End = new Point();
		}

		/// <summary>
		/// MouseDown 여부를 구하거나 설정합니다.
		/// </summary>
		public bool IsMouseDown
		{
			get;
			set;
		}

		/// <summary>
		/// Start를 구하거나 설정합니다.
		/// </summary>
		public Point Start;

		/// <summary>
		/// End를 구하거나 설정합니다.
		/// </summary>
		public Point End;

		/// <summary>
		/// MouseEventArgs를 구합니다.
		/// </summary>
		/// <value>MouseEventArgs를 반환합니다.</value>
		public MouseEventArgs MouseEventArgs
		{
			get;
			private set;
		}

		/// <summary>
		/// 왼쪽 버튼 눌림 상태를 구하거나 설정합니다.
		/// </summary>
		/// <value>왼쪽 버튼이 눌리면 True를 반환합니다.</value>
		public bool LeftButtonPressed
		{
			get;
			set;
		}

		/// <summary>
		/// 오른쪽 버튼 눌림 상태를 구하거나 설정합니다.
		/// </summary>
		/// <value>오른쪽 버튼이 눌리면 True를 반환합니다.</value>
		public bool RightButtonPressed
		{
			get;
			set;
		}

		/// <summary>
		/// Gets a value indicating whether [same position].
		/// </summary>
		/// <value><c>true</c> if [same position]; otherwise, <c>false</c>.</value>
		public bool IsSamePosition
		{
			get
			{
				return Start.X == End.X && Start.Y == End.Y;
			}
		}

		/// <summary>
		/// Mouses down.
		/// </summary>
		/// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
		public void MouseDown(MouseEventArgs e)
		{
			SetStart(e);
			IsMouseDown = true;

			if (MouseEventArgs != null)
			{
				if (e.Button == System.Windows.Forms.MouseButtons.Left)
				{
					LeftButtonPressed = true;
				}

				if (e.Button == System.Windows.Forms.MouseButtons.Right)
				{
					RightButtonPressed = true;
				}
			}
		}

		/// <summary>
		/// Mouses the move.
		/// </summary>
		/// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
		public void MouseMove(MouseEventArgs e)
		{
			SetEnd(e);
		}

		/// <summary>
		/// Mouses up.
		/// </summary>
		public void MouseUp(MouseEventArgs e)
		{
			SetEnd(e);
			IsMouseDown = false;
		}

		/// <summary>
		/// Mouses up 이벤트가 끝날때 호출해서 버튼 상태를 초기화 합니다.
		/// </summary>
		public void MouseUpEnd()
		{
			LeftButtonPressed = false;
			RightButtonPressed = false;
		}

		/// <summary>
		/// Gets the rectangle.
		/// </summary>
		/// <returns>Rectangle.</returns>
		public Rectangle GetRectangle()
		{
			return new Rectangle(
					   Math.Min(Start.X, End.X),
					   Math.Min(Start.Y, End.Y),
					   Math.Abs(Start.X - End.X),
					   Math.Abs(Start.Y - End.Y));
		}
		/// <summary>
		/// Sets the start.
		/// </summary>
		/// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
		private void SetStart(MouseEventArgs e)
		{
			MouseEventArgs = e;
			Start.X = e.X;
			Start.Y = e.Y;
		}

		/// <summary>
		/// Sets the end.
		/// </summary>
		/// <param name="e">The <see cref="MouseEventArgs"/> instance containing the event data.</param>
		private void SetEnd(MouseEventArgs e)
		{
			MouseEventArgs = e;
			End.X = e.X;
			End.Y = e.Y;
		}

	}
}
