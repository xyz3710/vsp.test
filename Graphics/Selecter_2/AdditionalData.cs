﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selecter
{
	/// <summary>
	/// Enum AdditionalData
	/// </summary>
	public enum AdditionalDatas
	{
		/// <summary>
		/// The sender
		/// </summary>
		Sender,
		/// <summary>
		/// The addressee
		/// </summary>
		Addressee,
		/// <summary>
		/// The bar code
		/// </summary>
		BarCode,
	}
}
