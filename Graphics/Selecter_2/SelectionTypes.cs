﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Selecter
{
	/// <summary>
	/// 영역 선택 종류를 나열합니다.
	/// </summary>
	public enum SelectionTypes
	{
		/// <summary>
		/// The rectangle
		/// </summary>
		Rectangle,
		/// <summary>
		/// The ellipse
		/// </summary>
		Ellipse,
		/// <summary>
		/// The free points
		/// </summary>
		FreePoints,
	}
}
