﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Selecter
{
	public partial class Screen : Form
	{
		private int _marqueeOffset;

		public Screen()
		{
			InitializeComponent();
			Selection = new AdvSelection();
			Selecting = new AdvSelection();
			MouseTracer = new MouseTracer();
			Picture.Image = new Bitmap(Picture.Width, Picture.Height);
		}

		private SelectionTypes SelectionTool
		{
			get;
			set;
		}

		private MouseTracer MouseTracer
		{
			get;
			set;
		}

		private AdvSelection Selecting
		{
			get;
			set;
		}

		private AdvSelection Selection
		{
			get;
			set;
		}

		private void MarqueeTimer_Tick(object sender, EventArgs e)
		{
			_marqueeOffset++;
			_marqueeOffset %= 6;
			Picture.Refresh();
		}

		private void SelectRectangle_Click(object sender, EventArgs e)
		{
			SelectionTool = SelectionTypes.Rectangle;
		}

		private void SelectEllipse_Click(object sender, EventArgs e)
		{
			SelectionTool = SelectionTypes.Ellipse;
		}

		private void LoadImage_Click(object sender, EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			if (ofd.ShowDialog() == DialogResult.OK)
			{
				Picture.Image = Bitmap.FromFile(ofd.FileName);
			}

			Picture.Refresh();
		}

		private void Picture_Paint(object sender, PaintEventArgs e)
		{
			e.Graphics.DrawImageUnscaled(Picture.Image, 0, 0);
			e.Graphics.FillRegion(new SolidBrush(Color.FromArgb(128, 0, 0, 255)), Selecting.Selected);

			if (Fill.Checked == true)
				e.Graphics.FillRegion(new SolidBrush(Color.FromArgb(128, 0, 0, 255)), Selection.Selected);

			//Pen pen = new Pen(Color.White, 1)
			//{
			//	DashStyle = System.Drawing.Drawing2D.DashStyle.Dash,
			//	DashPattern = new float[2] { 3, 3 },
			//	DashOffset = _marqueeOffset
			//};

			Bitmap ant = new Bitmap(Picture.Width, Picture.Height);

			using (Graphics g = Graphics.FromImage(ant))
			{
				g.Clear(Color.Magenta);
				//g.DrawPath(new Pen(Color.Black), Selection.Line());		// border
				//g.DrawPath(pen, Selection.Line());		// marquee
				//g.FillRegion(new SolidBrush(Color.Magenta), Selection.Selected);
				g.FillRegion(new SolidBrush(Color.FromArgb(128, 128, 255, 255)), Selection.Selected);
			}

			ant.MakeTransparent(Color.Magenta);

			e.Graphics.DrawImageUnscaled(ant, 0, 0);
		}

		private void Picture_MouseDown(object sender, MouseEventArgs e)
		{
			MouseTracer.MouseDown(e);
		}

		private void Picture_MouseMove(object sender, MouseEventArgs e)
		{
			if (MouseTracer.IsMouseDown == true)
			{
				MouseTracer.MouseMove(e);
				Selecting.Clear();

				switch (SelectionTool)
				{
					case SelectionTypes.Rectangle:
						Selecting.AddRectangle(MouseTracer.GetRectangle());

						break;
					case SelectionTypes.Ellipse:
						Selecting.AddCircle(MouseTracer.GetRectangle());

						break;
					case SelectionTypes.FreePoints:

						break;
				}

				Picture.Refresh();
			}
		}

		private void Picture_MouseUp(object sender, MouseEventArgs e)
		{
			MouseTracer.MouseUp(e);

			switch (SelectionTool)
			{
				case SelectionTypes.Rectangle:
					Selection.AddRectangle(MouseTracer.GetRectangle());
					MarqueeTimer.Start();

					break;
				case SelectionTypes.Ellipse:
					Selection.AddCircle(MouseTracer.GetRectangle());
					MarqueeTimer.Start();

					break;
				case SelectionTypes.FreePoints:

					break;
			}

			Selecting.Clear();

			if (MouseTracer.IsSamePosition == true)
			{
				// 한점을 클릭하면 선택 영역을 지운다. 다만, 현재 포함된 영역이 아니어야 한다.
				if (Selection.IsInVisible(MouseTracer.Start) == false)
				{
					Selection.Clear();
				}
			}

			Picture.Refresh();

			if (MouseTracer.LeftButtonPressed == true && MouseTracer.RightButtonPressed == true)
			{
				Selection.AdditionalData = AdditionalDatas.BarCode;
			}

			if (MouseTracer.LeftButtonPressed == true)
			{
				Selection.AdditionalData = AdditionalDatas.Sender;
			}

			if (MouseTracer.RightButtonPressed == true)
			{
				Selection.AdditionalData = AdditionalDatas.Addressee;
			}

			MouseTracer.MouseUpEnd();
		}
	}
}
