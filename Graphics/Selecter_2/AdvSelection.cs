﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Selecter
{
	/// <summary>
	/// Class for Advanced Selection.
	/// </summary>
	public class AdvSelection : IDisposable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AdvSelection"/> class.
		/// </summary>
		public AdvSelection()
		{
			InternalRegion = new Region();
			InternalGP = new GraphicsPath();
			Rectangles = new List<Rectangle>();
			Ellipses = new List<Rectangle>();
			FreeForms = new List<Point[]>();

			Clear();
		}

		/// <summary>
		/// Gets the selected region.
		/// </summary>
		/// <value>The selected region.</value>
		public Region Selected
		{
			get
			{
				return InternalRegion;
			}
		}

		/// <summary>
		/// Gets the outline.
		/// </summary>
		/// <value>The outline.</value>
		public GraphicsPath Outline
		{
			get
			{
				return InternalGP;
			}
		}

		/// <summary>
		/// Rectangle 영역을 구하거나 설정합니다.
		/// </summary>
		/// <value>EllipsesRectangle 영역을 반환합니다.</value>
		public List<Rectangle> Rectangles
		{
			get;
			set;
		}

		/// <summary>
		/// Ellipses 영역을 구하거나 설정합니다.
		/// </summary>
		/// <value>Ellipses 영역을 반환합니다.</value>
		public List<Rectangle> Ellipses
		{
			get;
			set;
		}

		/// <summary>
		/// FreeForms 영역을 구하거나 설정합니다.
		/// </summary>
		/// <value>FreeForms 영역을 반환합니다.</value>
		public List<Point[]> FreeForms
		{
			get;
			set;
		}

		/// <summary>
		/// 추가 Data를 구하거나 설정합니다.
		/// </summary>
		/// <value>추가 Data를 반환합니다.</value>
		public AdditionalDatas AdditionalData
		{
			get;
			set;
		}

		/// <summary>
		/// Region를 구하거나 설정합니다.
		/// </summary>
		/// <value>Region를 반환합니다.</value>
		private Region InternalRegion
		{
			get;
			set;
		}

		/// <summary>
		/// GraphicsPath를 구하거나 설정합니다.
		/// </summary>
		/// <value>GraphicsPath를 반환합니다.</value>
		private GraphicsPath InternalGP
		{
			get;
			set;
		}

		/// <summary>
		/// 특정 지점이 현재 영역에 포함되는지 여부를 반환 합니다.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <returns><c>true</c> if [is in visible] [the specified point]; otherwise, <c>false</c>.</returns>
		public bool IsInVisible(Point point)
		{
			return Outline.IsVisible(point);
		}

		public SelectionTypes GetSelectionType(Point point)
		{
			return SelectionTypes.Rectangle;
		}

		/// <summary>
		/// Adds the rectangle.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		public void AddRectangle(Rectangle rectangle)
		{
			if (Rectangles.Contains(rectangle) == false && IsZeroSize(rectangle) == false)
			{
				Rectangles.Add(rectangle);
			}

			InternalGP.AddRectangle(rectangle);
			InternalRegion.Union(InternalGP);
		}

		/// <summary>
		/// Adds the circle.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		public void AddCircle(Rectangle rectangle)
		{
			if (Ellipses.Contains(rectangle) == false && IsZeroSize(rectangle) == false)
			{
				Ellipses.Add(rectangle);
			}

			InternalGP.AddEllipse(rectangle);
			InternalRegion.Union(InternalGP);
		}

		/// <summary>
		/// Adds the freeform.
		/// </summary>
		/// <param name="points">The points.</param>
		public void AddFreeForm(Point[] points)
		{
			if (FreeForms.Contains(points) == false)
			{
				FreeForms.Add(points);
			}

			InternalGP.AddPolygon(points);
			InternalRegion.Union(InternalGP);
		}

		/// <summary>
		/// Removes the rectangle.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		public void RemoveRectangle(Rectangle rectangle)
		{
			if (Rectangles.Contains(rectangle) == false)
			{
				Rectangles.Remove(rectangle);
			}

			InternalGP.AddRectangle(rectangle);
			InternalRegion.Exclude(InternalGP);
		}

		/// <summary>
		/// Removes the circle.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		public void RemoveCircle(Rectangle rectangle)
		{
			if (Ellipses.Contains(rectangle) == false)
			{
				Ellipses.Remove(rectangle);
			}

			InternalGP.AddEllipse(rectangle);
			InternalRegion.Exclude(InternalGP);
		}

		/// <summary>
		/// Removes the freeform.
		/// </summary>
		/// <param name="points">The points.</param>
		public void RemoveFreeform(Point[] points)
		{
			if (FreeForms.Contains(points) == false)
			{
				FreeForms.Remove(points);
			}

			InternalGP.AddPolygon(points);
			InternalRegion.Exclude(InternalGP);
		}

		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear()
		{
			Rectangles.Clear();
			Ellipses.Clear();
			FreeForms.Clear();

			InternalRegion.MakeEmpty();
			InternalGP.Reset();
		}

		/// <summary>
		/// Lines this instance.
		/// </summary>
		/// <returns>GraphicsPath.</returns>
		public GraphicsPath Line()
		{
			GraphicsPath gp = new GraphicsPath();

			if (InternalGP.PointCount > 0)
			{
				gp.AddPath(InternalGP, false);
				gp.Widen(new Pen(Color.White, 2));
			}

			return gp;
		}

		private bool IsZeroSize(Rectangle rectangle)
		{
			return rectangle.Width == 0 && rectangle.Height == 0;
		}

		#region IDisposable 멤버

		public void Dispose()
		{
			if (InternalRegion != null)
			{
				InternalRegion.Dispose();
			}

			if (InternalGP != null)
			{
				InternalGP.Dispose();
			}

			Rectangles.Clear();
			Ellipses.Clear();
			FreeForms.Clear();
		}

		#endregion
	}
}
