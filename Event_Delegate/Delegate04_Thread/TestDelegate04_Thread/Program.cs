﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TestDelegate04_Thread
{
	class ThreadTest
	{
		public void TestThreadMethod()
		{
			Console.WriteLine("Start Thread");

			for (int i = 0; i < 5; i++)
			{
				Console.WriteLine("TestThread:" + i);
			}

			Console.WriteLine("End Thread");
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			ThreadTest tt = new ThreadTest();

			ThreadStart ts = new ThreadStart(tt.TestThreadMethod);

			Thread t = new Thread(ts);

			t.Start();

			Console.ReadLine();
		}
	}
}
