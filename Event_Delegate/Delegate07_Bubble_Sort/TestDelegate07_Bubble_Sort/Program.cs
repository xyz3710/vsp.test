﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDelegate07_Bubble_Sort
{
	public delegate bool CompareOp(object lhs, object rhs);

	class BubbleSortor
	{
		public static void Sort(object[] sortArray, CompareOp rhsIsGreater)
		{
			for (int i = 0; i < sortArray.Length; i++)
			{
				for (int j = 0; j < i; j++)
				{
					if (rhsIsGreater(sortArray[i], sortArray[j]) == true)
					{
						object temp = sortArray[i];
						sortArray[i] = sortArray[j];
						sortArray[j] = temp;
					}
				}
			}
		}
	}

	class Employee
	{
		private string name;
		private decimal salary;

		public Employee(string name, decimal salary)
		{
			this.name = name;
			this.salary = salary;
		}

		public override string ToString()
		{
			return string.Format(name + ", {0:C}", salary);

		}

		public static bool RhsIsGreater(object lhs, object rhs)
		{
			Employee Lhs = (Employee)lhs;
			Employee Rhs = (Employee)rhs;

			return (Rhs.salary > Lhs.salary) ? true : false;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Employee[] employees =
			{
				new Employee("Karli Watson", 20000),
				new Employee("Bill Gates", 10000),
				new Employee("Simon Robinson", 25000),
				new Employee("Mortimer", (decimal)1000000.38),
				new Employee("Arabel Jones", 23000),
				new Employee("Avon from 'Blake's 7'", 50000)
			};

			for (int i = 0; i < employees.Length; i++)
			{
				Console.WriteLine(employees[i].ToString());
			}
			Console.WriteLine();

			CompareOp EmployeeCompareOp = new CompareOp(Employee.RhsIsGreater);

			BubbleSortor.Sort(employees, EmployeeCompareOp);

			for (int i = 0; i < employees.Length; i++)
			{
				Console.WriteLine(employees[i].ToString());
			}

			Console.ReadLine();
		}
	}
}
