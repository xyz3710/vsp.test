﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestEvent01_Add
{
	public enum NumState : int
	{
		Increase,
		Decrease
	}

	public class CheckNumEventArgs : EventArgs
	{
		private NumState numstate;

		public CheckNumEventArgs(NumState numstate) : base()
		{
			this.numstate = numstate;
		}

		public NumState NumberState
		{
			get
			{
				return numstate;
			}
		}
	}

	public class CheckNumber
	{
		private int m_data;		// 기준값

		public delegate void CheckNum(CheckNumEventArgs e);

		public event CheckNum OnCheckState;

		public CheckNumber()
		{
			m_data = 0;
		}

		public void Run()
		{
			while (true)
			{
				Console.WriteLine("Input radominzed number then return.");
				Console.WriteLine("Exit - hit 999 then return.");

				int input = int.Parse(Console.ReadLine());

				if (input == 999)
					break;

				if (m_data > input)		// descrease
				{
					OnCheckState(new CheckNumEventArgs(NumState.Decrease));
				}

				if (m_data < input)		// increase
				{
					OnCheckState(new CheckNumEventArgs(NumState.Increase));
				}

				if (m_data == input)	// equal
					continue;

				m_data = input;
			}
		}
	}

	public class CheckNumberHandle
	{
		public CheckNumberHandle(CheckNumber checkState)
		{
			checkState.OnCheckState +=new CheckNumber.CheckNum(OnCheckStateHandler);
		}

		void OnCheckStateHandler(CheckNumEventArgs e)
		{
			switch (e.NumberState)
			{
				case NumState.Increase:
					Console.WriteLine("======== Increase =======");

					break;
				case NumState.Decrease:
					Console.WriteLine("======== Decrease =======");

					break;
			}
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			CheckNumber checkNumber = new CheckNumber();

			CheckNumberHandle checkNumberHandle = new CheckNumberHandle(checkNumber);

			checkNumber.Run();
		}
	}
}
