﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestEvent01
{
	public class MyObject
	{
		public delegate void ClickHandler(object sender, EventArgs e);

		public event ClickHandler Click;

		protected void OnClick()
		{
			if (Click != null)
				Click(this, null);
		}
	}

	class Program
	{
		private static void ClickFunction(object sender, EventArgs args)
		{
			Console.WriteLine("Process Click event");
		}

		static void Main(string[] args)
		{
			MyObject myObject = new MyObject();

			myObject.Click += new MyObject.ClickHandler(ClickFunction);
		}
	}
}
