﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDelegate06_simple
{
	class MathOperations
	{
		public static void MultiplyByTwo(double value)
		{
			double result = value * 2;

			Console.WriteLine("Multiplying by 2 : {0} gives {1}", value, result);
		}

		public static void Square(double value)
		{
			double result = value * value;

			Console.WriteLine("Squareing : {0} gives {1}", value, result);
		}
	}

	delegate void DoubleOp(double x);

	class Program
	{
		static void Main(string[] args)
		{
			DoubleOp operations = new DoubleOp(MathOperations.MultiplyByTwo);
			operations += new DoubleOp(MathOperations.Square);

			ProcessAndDisplayNumber(operations, 2.0);
			ProcessAndDisplayNumber(operations, 7.94);
			ProcessAndDisplayNumber(operations, 1.414);

			Console.ReadLine();
		}

		private static void ProcessAndDisplayNumber(DoubleOp action, double value)
		{
			Console.WriteLine("\nProcessAndDisplayNumber called with value = " + value);

			action(value);
		}
	}
}
