using System;
using System.Collections.Generic;
using System.Text;

namespace EventHandler
{
	public enum RequestType
	{
		AdRequest,
		PersonalMessageRequest,
	}

	public class UserRequestEventArgs : EventArgs
	{
		private RequestType _request;

		public UserRequestEventArgs(RequestType request) : base()
		{
			_request = request;
		}

		public RequestType Request
		{
			get
			{
				return _request;
			}
		}
	}

	public class UserInputMonitor
	{
		public delegate void UserRequest(object sender, UserRequestEventArgs e);

		public event UserRequest OnUserRequest;

		public void Run()
		{
			while (true)
			{
				Console.WriteLine("Select preferred option");
				Console.WriteLine("   Request advertisement - hit A to return");
				Console.WriteLine("   Request personal message from Monitor "
					+ "hit P to return");
				Console.WriteLine("   Exit - hit X to return");

				string response = Console.ReadLine();
				char responseChar = (response == "") ? ' ' : char.ToUpper(response[0]);

				switch (responseChar)
				{
					case 'A':
						OnUserRequest(this, 
							new UserRequestEventArgs(RequestType.AdRequest));

						break;
					case 'P':
						OnUserRequest(this,
							new UserRequestEventArgs(RequestType.PersonalMessageRequest));

						break;
					case 'X':

						return;
				}
			}
		}
	}

	public class MessageDisplayer
	{
		public MessageDisplayer(UserInputMonitor monitor)
		{
			monitor.OnUserRequest += new UserInputMonitor.UserRequest(UserRequestHandler);
		}

		protected void UserRequestHandler(object sender, UserRequestEventArgs e)
		{
			switch (e.Request)
			{
				case RequestType.AdRequest:
					Console.WriteLine("Mortimer Phones is better then anyone else because \n"
						+ "all our software is written in C#\n");

					break;
				case RequestType.PersonalMessageRequest:
					Console.WriteLine("Today Mortimer issued the following statement : \n"
						+ "Nevermore!\n");

					break;
			}
		}	
	}

	public class ManagerStaffMonitor
	{
		public ManagerStaffMonitor(UserInputMonitor monitor)
		{
			monitor.OnUserRequest += new UserInputMonitor.UserRequest(UserRequestHandler);
		}

		protected void UserRequestHandler(object sender, UserRequestEventArgs e)
		{
			if (e.Request == RequestType.PersonalMessageRequest)
			{
				global::System.Windows.Forms.MessageBox.Show("Kaark!", "Mortimer says...");
			}
		}
	
	}
	
	
	class Program
	{
		static void Main(string[] args)
		{
			UserInputMonitor inputMonitor = new UserInputMonitor();
			MessageDisplayer inputProcessor = new MessageDisplayer(inputMonitor);
			ManagerStaffMonitor mortimer = new ManagerStaffMonitor(inputMonitor);

			inputMonitor.Run();
		}
	}
}
