﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDelegate06_simple
{
	class MathOperations
	{
		public static double MultiplyByTwo(double value)
		{
			return value * 2;
		}

		public static double Square(double value)
		{
			return value * value;
		}
	}

	delegate double DoubleOp(double x);

	class Program
	{
		static void Main(string[] args)
		{
			DoubleOp[] operations =
			{
				new DoubleOp(MathOperations.MultiplyByTwo),
				new DoubleOp(MathOperations.Square)
			};

			for (int i = 0; i < operations.Length; i++)
			{
				Console.WriteLine("Using operations[{0}] : ", i);

				ProcessAndDisplayNumber(operations[i], 2.0);
				ProcessAndDisplayNumber(operations[i], 7.94);
				ProcessAndDisplayNumber(operations[i], 1.414);

				Console.WriteLine();
			}

			Console.ReadLine();
		}

		private static void ProcessAndDisplayNumber(DoubleOp action, double value)
		{
			double result = action(value);

			Console.WriteLine("Value is {0},\t result of operation is {1}",
				value, result);
		}
	}
}
