﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace TestEvent04
{
	// A delegate type for hooking up chang notifications.
	public delegate void ChangedEventHandler(object sender, EventArgs e);

	// A class that work just like ArrayList, but sends event
	// notification whenever the list changes.
	public class ListWithChangeEvent : ArrayList
	{
		// An event that clients can use to be notified whenever the
		// elements of the list change.
		public event ChangedEventHandler Changed;

		// Invoke the Change event; called whenever list changes
		protected virtual void OnChange(EventArgs e)
		{
			if (Changed != null)
			{
				Changed(this, e);
			}
		}

		// Override some of the methods that can change the list;
		// invoke event after each
		public override int Add(object value)
		{
			int i = base.Add(value);
			OnChange(EventArgs.Empty);

			return i;
		}

		public override void Clear()
		{
			base.Clear();

			OnChange(EventArgs.Empty);
		}

		public override object this[int index]
		{
			set
			{
				base[index] = value;

				OnChange(EventArgs.Empty);
			}
		}
	}
}

namespace TestEvents
{
	using TestEvent04;

	public class EventListener
	{
		private ListWithChangeEvent m_list;

		public EventListener(ListWithChangeEvent list)
		{
			m_list = list;

			// Add "ListChanged" to the changed event on m_list:
			m_list.Changed += new ChangedEventHandler(ListChanged);
		}

		// This will be called whenever the list changes.
		private void ListChanged(object sender, EventArgs e)
		{
			Console.WriteLine("This is called when the event fires.");
		}

		public void Detach()
		{
			// Detach the event and delete the list
			m_list.Changed -= new ChangedEventHandler(ListChanged);
			m_list = null;
		}
	}

	class Test
	{
		// Test the ListWithChangedEvent class.
		static void Main(string[] args)
		{
			// Create a new list.
			ListWithChangeEvent list = new ListWithChangeEvent();
			
			// Create a class that listens to the list's change event.
			EventListener listener = new EventListener(list);
			
			// Add and remove items from the list.
			list.Add("item 1");
			list.Clear();
			listener.Detach();

			Console.ReadLine();
		}
	}  
}
	
