﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace TestEvent02
{
	public class MyClass
	{
		public delegate void LogHandler(string message);

		public event LogHandler Log;

		public void Process()
		{
			OnLog("Process() Begin");

			OnLog("Process() End");
		}

		protected void OnLog(string message)
		{
			if (Log != null)
				Log(message);
		}
	}

	public class FileLogger
	{
		private FileStream fs;
		private StreamWriter sw;

		public FileLogger(string fileName)
		{
			fs = new FileStream(fileName, FileMode.Create);
			sw = new StreamWriter(fs);
		}

		public void Logger(string s)
		{
			sw.WriteLine(s);
		}

		public void Close()
		{
			sw.Close();
			fs.Close();
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			FileLogger fl = new FileLogger(@"D:\Temp\process.log");

			MyClass myClass = new MyClass();

			myClass.Log += new MyClass.LogHandler(Logger);
			myClass.Log += new MyClass.LogHandler(fl.Logger);

			myClass.Process();

			fl.Close();
		}

		static void Logger(string message)
		{
			Console.WriteLine(message);
		}
	}
}
