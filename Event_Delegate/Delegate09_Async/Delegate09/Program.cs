﻿/**********************************************************************************************************************/
/*	Domain		:	Delegate09.AsyncResultSample
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2010년 8월 13일 금요일 오후 4:15
/*	Purpose		:	대리자 비동기 호출
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	http://msdn.microsoft.com/ko-kr/library/2e08f6yc.aspx
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using System.Data.Linq;

namespace Delegate09
{
	[Synchronization()]
	public class SampleSyncronized : ContextBoundObject
	{
		public int Square(int i)
		{
			return i * i;
		}
	}

	public delegate int SampSyncSqrDelegate(int i);

	//Main sample class
	public class AsyncResultSample
	{
		public static void Main()
		{
			int callParameter = 0;
			int callResult = 0;

			//call the method asynchronously
			Console.WriteLine("Making an asynchronous call on the object.  ");

			SampleSyncronized sampSyncObj = new SampleSyncronized();

			DisplayCurrentThreadId();
			CallAnonymousMethodCallback();

			SampSyncSqrDelegate sampleDelegate = new SampSyncSqrDelegate(sampSyncObj.Square);
			AsyncCallback callBack = new AsyncCallback(CallbackMethod);

			// 1. BeginInvoke, EndInvoke를 이용하여 비동기 호출이 끝났는지 체크
			callParameter = 10;
			IAsyncResult aResult = sampleDelegate.BeginInvoke(callParameter, null, null);
			// IAsyncResult.AsyncWaitHandle을 조사하여 함수의 콜이 끝났는지 체크
			aResult.AsyncWaitHandle.WaitOne();
			// IAsyncResult.IsComplete를 이용하여 비동기 호출 끝났는지 체크			
			// while (aResult.IsCompleted == false);
			callResult = sampleDelegate.EndInvoke(aResult);
			DisplayCurrentThreadId();
			DisplayResult("Step1", callParameter, callResult);

			// 2. AsyncCallback 함수를 이용하여 비동기 호출이 끝났는지 체크
			callParameter = 11;
			sampleDelegate.BeginInvoke(callParameter, callBack, callParameter);

			// 대기 상태가 없이 프로그램이 종료 되면 Callback 함수가 호출되지 않는다.
			// Step1과 Step2의 순서를 바꾸면 대기 상태를 만들지 않아도 된다.
			Thread.Sleep(10);
			return;

			// 2. AsyncCallback 함수를 이용하여 비동기 호출이 끝났는지 체크
			callParameter = 12;
			sampleDelegate.BeginInvoke(callParameter, callBack, callParameter);

			// 1. BeginInvoke, EndInvoke를 이용하여 비동기 호출이 끝났는지 체크
			callParameter = 13;
			aResult = sampleDelegate.BeginInvoke(callParameter, null, null);
			// IAsyncResult.AsyncWaitHandle을 조사하여 함수의 콜이 끝났는지 체크
			aResult.AsyncWaitHandle.WaitOne();
			// IAsyncResult.IsComplete를 이용하여 비동기 호출 끝났는지 체크			
			// while (aResult.IsCompleted == false);
			callResult = sampleDelegate.EndInvoke(aResult);
			DisplayCurrentThreadId();
			DisplayResult("Step1", callParameter, callResult);
		}

		private static void CallAnonymousMethodCallback()
		{
			// 무명 메서드를 이용해서 하나의 메서드에서 실행한다.
			SampSyncSqrDelegate sampSyncSqrDelegate = new SampSyncSqrDelegate(new SampleSyncronized().Square);
			int callParameter = 12;

			sampSyncSqrDelegate.BeginInvoke(
				callParameter,
				asyncResult =>
				{
					int value = Convert.ToInt32(asyncResult.AsyncState);
					AsyncResult result = asyncResult as AsyncResult;
					SampSyncSqrDelegate temp = result.AsyncDelegate as SampSyncSqrDelegate;
					int invokeResult = temp.EndInvoke(result);

					DisplayCurrentThreadId();
					DisplayResult("CallAnonymousMethodCallback", value, invokeResult);
				},
				callParameter);
		}

		private static void CallbackMethod(IAsyncResult ar)
		{
			// Obtains the last parameter of the delegate call.
			int value = Convert.ToInt32(ar.AsyncState);

			// Obtains return value from the delegate call using EndInvoke.
			AsyncResult aResult = (AsyncResult)ar;
			SampSyncSqrDelegate temp = (SampSyncSqrDelegate)aResult.AsyncDelegate;
			int result = temp.EndInvoke(ar);

			DisplayCurrentThreadId();
			DisplayResult("Step2 in CallbackMethod", value, result);
		}

		private static void DisplayResult(string message, int value, int result)
		{
			Console.WriteLine(
				"Thread in {0,-4}:Result of {1,4} is {2,6}. <= {3}",
				Thread.CurrentThread.ManagedThreadId,
				value,
				result,
				message);
		}

		private static void DisplayCurrentThreadId()
		{
			Console.WriteLine("Current Thread in {0,-4}", Thread.CurrentThread.ManagedThreadId);
		}
	}
}
