﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Runtime.CompilerServices;

namespace TestEvent03
{
	public class DelegateCache
	{
		private Hashtable delegateStorage = new Hashtable();

		public Delegate Find(object key)
		{
			return ((Delegate)delegateStorage[key]);
		}

		public void Add(object key, Delegate myDelegate)
		{
			delegateStorage[key] = 
				Delegate.Combine((Delegate)delegateStorage[key], myDelegate);
		}

		public void Remove(object key, Delegate myDelegate)
		{
			delegateStorage[key] = 
				Delegate.Remove((Delegate)delegateStorage[key], myDelegate);
		}
	}

	public class MyClass
	{
		public delegate void LogHandler(string message);

		private DelegateCache delegateCache = new DelegateCache();
		private static object logEventKey = new object();		// unique key

		public event LogHandler Log
		{
			[MethodImpl(MethodImplOptions.Synchronized)]
			add
			{
				delegateCache.Add(logEventKey, value);
			}

			[MethodImpl(MethodImplOptions.Synchronized)]
			remove
			{
				delegateCache.Remove(logEventKey, value);
			}
		}

		public void Process()
		{
			OnLog("Process() Begin");

			OnLog("Process() End");
		}

		protected void OnLog(string message)
		{
			LogHandler lh = (LogHandler)delegateCache.Find(logEventKey);

			if (lh != null)
				lh(message);
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			MyClass myClass = new MyClass();

			myClass.Log += new MyClass.LogHandler(Logger);
			myClass.Log += new MyClass.LogHandler(Logger);

			myClass.Process();

			myClass.Log -= new MyClass.LogHandler(Logger);
		}

		static void Logger(string message)
		{
			Console.WriteLine(message);
		}
	}
}
