﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDelegate03
{
	public delegate void SimpleDelegate1();
	public delegate void SimpleDelegate2(int i);

	class AType
	{
		public void F1()
		{
			Console.WriteLine("AType.F1");
		}

		public void F2(int x)
		{
			Console.WriteLine("AType.F2 x = " + x);
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			AType atype = new AType();

			SimpleDelegate1 s1 = new SimpleDelegate1(atype.F1);
			SimpleDelegate2 s2 = new SimpleDelegate2(atype.F2);

			s1();
			s2(1000);

			Console.ReadLine();
		}
	}
}
