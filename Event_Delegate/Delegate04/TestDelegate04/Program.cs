﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDelegate04
{
	public delegate void TopDelegator(string str);

	public class Top
	{
		public static void StaticMethod(string str)
		{
			Console.WriteLine("Static method입니다.\t");
			Console.WriteLine(str);
		}

		public void NormalMethod(string str)
		{
			Console.WriteLine("Normal method입니다.\t");
			Console.WriteLine(str);
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Top t = new Top();

			TopDelegator td1 = new TopDelegator(t.NormalMethod);
			TopDelegator td2 = new TopDelegator(Top.StaticMethod);

			td1("Jabook");
			td2("Novel C#");

			Console.ReadLine();
		}
	}
}
