﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDelegate02
{
	public class Run
	{
		public delegate void GetRunModule(string message);
	}

	public class Operator1
	{
		private static string className = typeof(Operator1).Name;

		public static void Run(string message)
		{
			Console.WriteLine(className + " Running({0})", message);
		}
	}

	public class Operator2
	{
		private static string className = typeof(Operator2).Name;

		public static void Run(string message)
		{
			Console.WriteLine(className + " Running({0})", message);
		}
	}

	public class Operator3
	{
		private static string className = typeof(Operator3).Name;

		public static void Run(string message)
		{
			Console.WriteLine(className + " Running({0})", message);
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Run.GetRunModule run1 = null;

			run1 += new Run.GetRunModule(Operator1.Run);
			run1 += new Run.GetRunModule(Operator2.Run);
			run1 += new Run.GetRunModule(Operator3.Run);
			run1 -= new Run.GetRunModule(Operator2.Run);

			run1("Run1 RUN operator");

			Console.WriteLine();

			Run.GetRunModule run2 = (Run.GetRunModule)Delegate.Combine(
				new Delegate[]{
					new Run.GetRunModule(Operator1.Run),
					new Run.GetRunModule(Operator2.Run),
					new Run.GetRunModule(Operator3.Run)
				});

			run2("Run2 RUN operator");

			Console.WriteLine();

			foreach (Run.GetRunModule runModule in run1.GetInvocationList())
			{
				try
				{
					runModule("RunModule");
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
				}
			}

			Console.ReadLine();
		}
	}
}