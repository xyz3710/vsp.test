﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace TestDelegate
{
	class DBConnection
	{
		protected static int NextConnectionNbr = 1;
		protected string connectionName;

		public DBConnection()
		{
			connectionName = "Database connection "
			+ DBConnection.NextConnectionNbr++;
		}

		public string ConnectionName
		{
			get
			{
				return connectionName;
			}
		}
	}

	class DBManager
	{
		protected ArrayList activeConnections;

		public DBManager()
		{
			activeConnections = new ArrayList();

			for (int i = 1; i < 6; i++)
			{
				activeConnections.Add(new DBConnection());
			}
		}

		public delegate void EnumConnectionsCallback(
			DBConnection connection);

		public void EnumConnections(
			EnumConnectionsCallback callback)
		{
			foreach (DBConnection connecton in activeConnections)
			{
				callback(connecton);
			}
		}
	}

	class InstanceDelegate
	{
		public static void PrintConnections(DBConnection connecton)
		{
			Console.WriteLine("[InstanceDelegate.PrintConnection] {0}",
				connecton.ConnectionName);
		}

		static void Main(string[] args)
		{
			DBManager dbManager = new DBManager();

			Console.WriteLine("[Main] Instantiating the " +
				"delegate metod.");
			DBManager.EnumConnectionsCallback printConnections = 
				new DBManager.EnumConnectionsCallback(PrintConnections);

			Console.WriteLine("[Main] Calling EnumConnections " +
				"- passing the delegate.");

			dbManager.EnumConnections(printConnections);

			Console.ReadLine();
		}
	}
}
