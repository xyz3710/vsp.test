using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Delegate01
{
	public class CoreTempMonitor2
	{
		private event StartPumpCallback CoreOverheating;
		private ArrayList callBacks = new ArrayList();

		public void Add(StartPumpCallback callback)
		{
			callBacks.Add(callback);
		}

		public void SwitchOnAllPumps()
		{
			foreach (StartPumpCallback callback in callBacks)
				callback();
		}
	}
}
