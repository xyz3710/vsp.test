﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Delegate01
{
	public delegate void StartPumpCallback();

	class Program
	{
		static void Main(string[] args)
		{
			CoreTempMonitor2 ctm = new CoreTempMonitor2();
			ElectricPumpDriver ed1 = new ElectricPumpDriver();
			PneumaticPumpDirver pd1 = new PneumaticPumpDirver();

			//CoreOverheaing += new StartPumpCallback(pd1.SwitchOn);

			ctm.Add(new StartPumpCallback(ed1.StartElectricPumpRunning));

			ctm.Add(new StartPumpCallback(pd1.SwitchOn));

			ctm.SwitchOnAllPumps();
		}
	}
}
