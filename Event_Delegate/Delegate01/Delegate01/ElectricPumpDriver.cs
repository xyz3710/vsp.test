using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Delegate01
{
	public class ElectricPumpDriver
	{
		public void StartElectricPumpRunning(object sender, CoreOverheatingEventArgs args)
		{
			int currentTemperature = args.GetTemperature();

			Console.WriteLine("Start Electric Pump running");
		}
	}
}
