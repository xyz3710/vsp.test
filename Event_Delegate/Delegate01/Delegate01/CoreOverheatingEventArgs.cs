using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Delegate01
{
	public class CoreOverheatingEventArgs : EventArgs
	{
		private readonly int _temperature;

		public CoreOverheatingEventArgs(int temperature)
		{
			_temperature = temperature;
		}

		public int GetTemperature()
		{
			return _temperature;
		}
	}
}
