﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace TestDelegate01
{
	public class FileLogger
	{
		FileStream fileStream;
		StreamWriter streamWriter;

		public FileLogger(string fileName)
		{
			fileStream = new FileStream(fileName, FileMode.Create);
			streamWriter = new StreamWriter(fileStream);
		}

		public void Logger(string s)
		{
			streamWriter.WriteLine(s);
		}

		public void Close()
		{
			streamWriter.Close();
			fileStream.Close();
		}
	}

	public class MyClass
	{
		public delegate void LogHandler(string message);

		public void Process(LogHandler logHandler)
		{
			if (logHandler != null)
				logHandler("Process begin...");

			if (logHandler != null)
				logHandler("Process end...");
		}
	}

	class Program
	{
		private static void Logger(string message)
		{
			Console.WriteLine(message);
		}

		static void Main(string[] args)
		{
			FileLogger fl = new FileLogger(@"D:\Temp\process.log");

			MyClass myClass = new MyClass();

			MyClass.LogHandler lh = new MyClass.LogHandler(fl.Logger);
			MyClass.LogHandler lh2 = new MyClass.LogHandler(Logger);

			myClass.Process(lh);
			myClass.Process(lh2);

			fl.Close();

			Console.ReadLine();
		}
	}
}
