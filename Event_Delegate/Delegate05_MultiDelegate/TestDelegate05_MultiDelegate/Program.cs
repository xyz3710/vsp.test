﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDelegate05_MultiDelegate
{
	public delegate void TopDelegator(string str);

	class Top
	{
		public static void StaticMethod(string str)
		{
			Console.Write("Static method\t");
			Console.WriteLine(str);
		}

		public void NormalMethod(string str)
		{
			Console.Write("Normal method\t");
			Console.WriteLine(str);
		}
	}

	class Program
	{
		private delegate string GetAString();

		static void Main(string[] args)
		{
			Top t = new Top();

			TopDelegator bank;
			TopDelegator td1 = new TopDelegator(t.NormalMethod);
			TopDelegator td2 = new TopDelegator(Top.StaticMethod);

			bank = td1;
			bank("Add TopDelegator 1");

			bank += td2;
			bank("Add TopDelegator 2");

			bank -= td1;
			bank("Remove TopDelegator 1");

			
			int x = 40;
			GetAString FirstStringMethod = new GetAString(x.ToString);

			Console.WriteLine("String is " + FirstStringMethod());


			Console.ReadLine();
		}
	}
}
