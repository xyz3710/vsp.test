﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DZone.CallCenter.UcWorkers
{
	public class UcWorkerStatusItem1 : UserControl
	{
		private Panel pnlMain;
		private ListView lvLocals;
		private ColumnHeader chLocalName;
		private ColumnHeader chOrderTime;
		private Label lbOnline;
		private Label lbStatus;
		private TableLayoutPanel tlpMain;

		private void InitializeComponent()
		{
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
			"디디치킨(용화점)",
			"19:06"}, -1);
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("갈채닭발");
			System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("봉추찜닭");
			System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("가장맛있는족발");
			System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("장충동왕족발보쌈");
			System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("구어조은닭");
			System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("용화동굽네치킨");
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.pnlMain = new System.Windows.Forms.Panel();
			this.lvLocals = new System.Windows.Forms.ListView();
			this.chLocalName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chOrderTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lbOnline = new System.Windows.Forms.Label();
			this.lbStatus = new System.Windows.Forms.Label();
			this.tlpMain.SuspendLayout();
			this.pnlMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.AutoSize = true;
			this.tlpMain.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tlpMain.ColumnCount = 1;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpMain.Controls.Add(this.pnlMain, 0, 1);
			this.tlpMain.Controls.Add(this.lbOnline, 0, 0);
			this.tlpMain.Controls.Add(this.lbStatus, 0, 2);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 3;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tlpMain.Size = new System.Drawing.Size(292, 194);
			this.tlpMain.TabIndex = 0;
			// 
			// pnlMain
			// 
			this.pnlMain.Controls.Add(this.lvLocals);
			this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlMain.Location = new System.Drawing.Point(4, 35);
			this.pnlMain.Name = "pnlMain";
			this.pnlMain.Size = new System.Drawing.Size(284, 124);
			this.pnlMain.TabIndex = 1;
			// 
			// lvLocals
			// 
			this.lvLocals.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
			this.chLocalName,
			this.chOrderTime});
			this.lvLocals.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvLocals.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.lvLocals.FullRowSelect = true;
			this.lvLocals.GridLines = true;
			this.lvLocals.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.lvLocals.HideSelection = false;
			this.lvLocals.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
			listViewItem1,
			listViewItem2,
			listViewItem3,
			listViewItem4,
			listViewItem5,
			listViewItem6,
			listViewItem7});
			this.lvLocals.Location = new System.Drawing.Point(0, 0);
			this.lvLocals.MultiSelect = false;
			this.lvLocals.Name = "lvLocals";
			this.lvLocals.ShowGroups = false;
			this.lvLocals.Size = new System.Drawing.Size(284, 124);
			this.lvLocals.TabIndex = 1;
			this.lvLocals.UseCompatibleStateImageBehavior = false;
			this.lvLocals.View = System.Windows.Forms.View.Details;
			// 
			// chLocalName
			// 
			this.chLocalName.Text = "가맹점 이름";
			this.chLocalName.Width = 212;
			// 
			// chOrderTime
			// 
			this.chOrderTime.Text = "주문시간";
			this.chOrderTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.chOrderTime.Width = 50;
			// 
			// lbOnline
			// 
			this.lbOnline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(206)))), ((int)(((byte)(33)))));
			this.lbOnline.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbOnline.Font = new System.Drawing.Font("맑은 고딕", 11F, System.Drawing.FontStyle.Bold);
			this.lbOnline.ForeColor = System.Drawing.Color.Navy;
			this.lbOnline.Location = new System.Drawing.Point(2, 2);
			this.lbOnline.Margin = new System.Windows.Forms.Padding(1);
			this.lbOnline.Name = "lbOnline";
			this.lbOnline.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.lbOnline.Size = new System.Drawing.Size(288, 28);
			this.lbOnline.TabIndex = 2;
			this.lbOnline.Text = "김기사";
			this.lbOnline.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbStatus
			// 
			this.lbStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(185)))), ((int)(((byte)(185)))));
			this.lbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbStatus.ForeColor = System.Drawing.Color.Navy;
			this.lbStatus.Location = new System.Drawing.Point(2, 164);
			this.lbStatus.Margin = new System.Windows.Forms.Padding(1);
			this.lbStatus.Name = "lbStatus";
			this.lbStatus.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.lbStatus.Size = new System.Drawing.Size(288, 28);
			this.lbStatus.TabIndex = 2;
			this.lbStatus.Text = "10건 / 150,000원";
			this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// UcWorkerStatusItem
			// 
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.tlpMain);
			this.DoubleBuffered = true;
			this.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.Margin = new System.Windows.Forms.Padding(1);
			this.Name = "UcWorkerStatusItem";
			this.Size = new System.Drawing.Size(292, 194);
			this.tlpMain.ResumeLayout(false);
			this.pnlMain.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
