﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DZone.CallCenter.UcWorkers
{
	public partial class UcWorkerStatusItem : UserControl
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UcWorkerStatusItem"/> class.
		/// </summary>
		public UcWorkerStatusItem()
		{
			InitializeComponent();

			Font = new Font("맑은 고딕", 11f);
			//ItemHeight = lvLocals.GetItemRect(0).Height;
			ItemHeight = 24;
		}

		/// <summary>
		/// 리스트 항목의 기본 높이를 구합니다.
		/// </summary>
		/// <value>ItemHeight를 반환합니다.</value>
		public int ItemHeight
		{
			get;
			private set;
		}


	}
}
