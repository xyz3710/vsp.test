﻿namespace DZone.CallCenter.UcWorkers
{
	partial class UcWorkerStatusItem
	{
		/// <summary> 
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region 구성 요소 디자이너에서 생성한 코드

		/// <summary> 
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem(new string[] {
            "디디치킨(용화점)",
            "19:06"}, -1);
			System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("갈채닭발");
			System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem("봉추찜닭");
			System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem("가장맛있는족발");
			System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem("장충동왕족발보쌈");
			System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem("구어조은닭");
			System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem("용화동굽네치킨");
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.pnlMain = new System.Windows.Forms.Panel();
			this.lvLocals = new System.Windows.Forms.ListView();
			this.chLocalName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.chOrderTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lbOnline = new System.Windows.Forms.Label();
			this.lbStatus = new System.Windows.Forms.Label();
			this.tlpMain.SuspendLayout();
			this.pnlMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.AutoSize = true;
			this.tlpMain.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tlpMain.ColumnCount = 1;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tlpMain.Controls.Add(this.pnlMain, 0, 1);
			this.tlpMain.Controls.Add(this.lbOnline, 0, 0);
			this.tlpMain.Controls.Add(this.lbStatus, 0, 2);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 3;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tlpMain.Size = new System.Drawing.Size(290, 192);
			this.tlpMain.TabIndex = 1;
			// 
			// pnlMain
			// 
			this.pnlMain.BackColor = System.Drawing.Color.White;
			this.pnlMain.Controls.Add(this.lvLocals);
			this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlMain.Location = new System.Drawing.Point(2, 33);
			this.pnlMain.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.pnlMain.Name = "pnlMain";
			this.pnlMain.Size = new System.Drawing.Size(286, 126);
			this.pnlMain.TabIndex = 1;
			// 
			// lvLocals
			// 
			this.lvLocals.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chLocalName,
            this.chOrderTime});
			this.lvLocals.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvLocals.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.lvLocals.FullRowSelect = true;
			this.lvLocals.GridLines = true;
			this.lvLocals.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.lvLocals.HideSelection = false;
			this.lvLocals.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem8,
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14});
			this.lvLocals.Location = new System.Drawing.Point(0, 0);
			this.lvLocals.MultiSelect = false;
			this.lvLocals.Name = "lvLocals";
			this.lvLocals.ShowGroups = false;
			this.lvLocals.Size = new System.Drawing.Size(286, 126);
			this.lvLocals.TabIndex = 1;
			this.lvLocals.UseCompatibleStateImageBehavior = false;
			this.lvLocals.View = System.Windows.Forms.View.Details;
			// 
			// chLocalName
			// 
			this.chLocalName.Text = "가맹점 이름";
			this.chLocalName.Width = 212;
			// 
			// chOrderTime
			// 
			this.chOrderTime.Text = "주문시간";
			this.chOrderTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.chOrderTime.Width = 50;
			// 
			// lbOnline
			// 
			this.lbOnline.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(206)))), ((int)(((byte)(33)))));
			this.lbOnline.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbOnline.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
			this.lbOnline.ForeColor = System.Drawing.Color.Navy;
			this.lbOnline.Location = new System.Drawing.Point(2, 2);
			this.lbOnline.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.lbOnline.Name = "lbOnline";
			this.lbOnline.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.lbOnline.Size = new System.Drawing.Size(286, 28);
			this.lbOnline.TabIndex = 2;
			this.lbOnline.Text = "김기사";
			this.lbOnline.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lbStatus
			// 
			this.lbStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(185)))), ((int)(((byte)(185)))));
			this.lbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbStatus.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.lbStatus.ForeColor = System.Drawing.Color.Navy;
			this.lbStatus.Location = new System.Drawing.Point(2, 162);
			this.lbStatus.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.lbStatus.Name = "lbStatus";
			this.lbStatus.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.lbStatus.Size = new System.Drawing.Size(286, 28);
			this.lbStatus.TabIndex = 2;
			this.lbStatus.Text = "10건 / 150,000원";
			this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// UcWorkerStatusItem
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.White;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.tlpMain);
			this.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.Name = "UcWorkerStatusItem";
			this.Size = new System.Drawing.Size(290, 192);
			this.tlpMain.ResumeLayout(false);
			this.pnlMain.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.Panel pnlMain;
		private System.Windows.Forms.ListView lvLocals;
		private System.Windows.Forms.ColumnHeader chLocalName;
		private System.Windows.Forms.ColumnHeader chOrderTime;
		private System.Windows.Forms.Label lbOnline;
		private System.Windows.Forms.Label lbStatus;
	}
}
