﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DZone.CallCenter.UcWorkers;

namespace ScrollablePanelTest
{
	public partial class Form1 : Form
	{
		private int _listItemCount;

		public Form1()
		{
			InitializeComponent();

			_listItemCount = 4;
		}

		private int ItemHeight
		{
			get;
			set;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			SetHeight(5);
		}

		private void button4_Click(object sender, EventArgs e)
		{
			SetHeight(-1);
		}

		private void SetHeight(int increase)
		{
			Action<UcWorkerStatusItem> op = null;

			op = x => x.Height += (x.ItemHeight * increase);

			foreach (var item in flpMain.Controls.Cast<UcWorkerStatusItem>())
			{
				if ((_listItemCount <= 4 && increase < 0)
					|| (_listItemCount >= 50 && increase > 0))
				{
					return;
				}

				op(item);
			}

			_listItemCount += increase;
		}
	}
}
