﻿namespace ScrollablePanelTest
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.flpMain = new System.Windows.Forms.FlowLayoutPanel();
			this.button2 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.ucWorkerStatusItem1 = new DZone.CallCenter.UcWorkers.UcWorkerStatusItem();
			this.ucWorkerStatusItem2 = new DZone.CallCenter.UcWorkers.UcWorkerStatusItem();
			this.ucWorkerStatusItem3 = new DZone.CallCenter.UcWorkers.UcWorkerStatusItem();
			this.flpMain.SuspendLayout();
			this.tlpMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// flpMain
			// 
			this.flpMain.AutoScroll = true;
			this.tlpMain.SetColumnSpan(this.flpMain, 2);
			this.flpMain.Controls.Add(this.ucWorkerStatusItem1);
			this.flpMain.Controls.Add(this.ucWorkerStatusItem2);
			this.flpMain.Controls.Add(this.ucWorkerStatusItem3);
			this.flpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flpMain.Location = new System.Drawing.Point(3, 49);
			this.flpMain.Name = "flpMain";
			this.flpMain.Size = new System.Drawing.Size(1016, 478);
			this.flpMain.TabIndex = 1;
			// 
			// button2
			// 
			this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button2.Location = new System.Drawing.Point(3, 3);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(505, 40);
			this.button2.TabIndex = 0;
			this.button2.Text = "+";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button4
			// 
			this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button4.Location = new System.Drawing.Point(514, 3);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(505, 40);
			this.button4.TabIndex = 0;
			this.button4.Text = "-";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 2;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.Controls.Add(this.button4, 1, 0);
			this.tlpMain.Controls.Add(this.flpMain, 0, 1);
			this.tlpMain.Controls.Add(this.button2, 0, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 2;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Size = new System.Drawing.Size(1022, 530);
			this.tlpMain.TabIndex = 2;
			// 
			// ucWorkerStatusItem1
			// 
			this.ucWorkerStatusItem1.BackColor = System.Drawing.Color.White;
			this.ucWorkerStatusItem1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ucWorkerStatusItem1.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.ucWorkerStatusItem1.Location = new System.Drawing.Point(1, 1);
			this.ucWorkerStatusItem1.Margin = new System.Windows.Forms.Padding(1);
			this.ucWorkerStatusItem1.Name = "ucWorkerStatusItem1";
			this.ucWorkerStatusItem1.Size = new System.Drawing.Size(292, 170);
			this.ucWorkerStatusItem1.TabIndex = 0;
			// 
			// ucWorkerStatusItem2
			// 
			this.ucWorkerStatusItem2.BackColor = System.Drawing.Color.White;
			this.ucWorkerStatusItem2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ucWorkerStatusItem2.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.ucWorkerStatusItem2.Location = new System.Drawing.Point(295, 1);
			this.ucWorkerStatusItem2.Margin = new System.Windows.Forms.Padding(1);
			this.ucWorkerStatusItem2.Name = "ucWorkerStatusItem2";
			this.ucWorkerStatusItem2.Size = new System.Drawing.Size(292, 170);
			this.ucWorkerStatusItem2.TabIndex = 0;
			// 
			// ucWorkerStatusItem3
			// 
			this.ucWorkerStatusItem3.BackColor = System.Drawing.Color.White;
			this.ucWorkerStatusItem3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ucWorkerStatusItem3.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.ucWorkerStatusItem3.Location = new System.Drawing.Point(589, 1);
			this.ucWorkerStatusItem3.Margin = new System.Windows.Forms.Padding(1);
			this.ucWorkerStatusItem3.Name = "ucWorkerStatusItem3";
			this.ucWorkerStatusItem3.Size = new System.Drawing.Size(292, 170);
			this.ucWorkerStatusItem3.TabIndex = 0;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1022, 530);
			this.Controls.Add(this.tlpMain);
			this.Name = "Form1";
			this.Text = "Form1";
			this.flpMain.ResumeLayout(false);
			this.tlpMain.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.FlowLayoutPanel flpMain;
		private System.Windows.Forms.Button button2;
		private DZone.CallCenter.UcWorkers.UcWorkerStatusItem ucWorkerStatusItem1;
		private System.Windows.Forms.Button button4;
		private DZone.CallCenter.UcWorkers.UcWorkerStatusItem ucWorkerStatusItem3;
		private DZone.CallCenter.UcWorkers.UcWorkerStatusItem ucWorkerStatusItem2;
		private System.Windows.Forms.TableLayoutPanel tlpMain;
	}
}

