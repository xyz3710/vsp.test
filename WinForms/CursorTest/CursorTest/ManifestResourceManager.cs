/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Resources.ManifestResourceManager
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 12월 2일 화요일 오후 6:41
/*	Purpose		:	내장된 Manifest Resource를 관리해 줍니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Reflection;
using Microsoft.Win32;
using System.Windows.Forms;

namespace iDASiT.FX.Win.Resources
{
    /// <summary>
	/// 내장된 Manifest Resource를 관리해 줍니다.
	/// </summary>
	public class ManifestResourceManager
	{
		/// <summary>
		/// Xml type의 Resource Stream을 구합니다.
		/// </summary>
		/// <param name="resourceType"></param>
		/// <returns></returns>
		public static Stream GetXmlStream(XmlResourceType resourceType)
		{
			return InternalManifestResourceManager.GetStreamByName(string.Format("Localize.{0}.xml", resourceType));
		}

		/// <summary>
		/// Animated Cursor의 Resource Stream을 구합니다.
		/// </summary>
		/// <param name="cursorType"></param>
		/// <returns></returns>
		public static Stream GetCursorStream(CursorType cursorType)
		{
			return InternalManifestResourceManager.GetStreamByName(string.Format("Cursors.{0}.ani", cursorType));
		}

		/// <summary>
		/// Resource에 포함된 Animated Cursor를 구합니다.
		/// </summary>
		/// <param name="cursorType">구하고자 하는 Cursor Type</param>
		public static Cursor GetAnimatedCursor(CursorType cursorType)
		{
			Cursor aniCursor = Cursors.Default;
			string cursorFileName = string.Format(@"{0}\{1}.ani", 
                                        Environment.GetEnvironmentVariable("Temp", EnvironmentVariableTarget.User), 
                                        cursorType);

			if (File.Exists(cursorFileName) == false)
			{
				using (Stream resStream = ManifestResourceManager.GetCursorStream(cursorType))
				{
					if (resStream != null)
					{
						byte[] buffer = new byte[resStream.Length];
						int readByte = resStream.Read(buffer, 0, (int)resStream.Length);

						using (FileStream fs = new FileStream(cursorFileName, FileMode.Create))
						{
							fs.Write(buffer, 0, readByte);
							fs.Flush();
						}
					}
				}
			}

			aniCursor = CursorFacotry.Create(cursorFileName);

			return aniCursor;
		}
	}
}
