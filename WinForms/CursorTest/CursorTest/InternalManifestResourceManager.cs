/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Resources.InternalManifestResourceManager
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 12월 2일 화요일 오후 7:17
/*	Purpose		:	내장된 Manifest Resource를 관리를 위한 class입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Reflection;
using Microsoft.Win32;

namespace iDASiT.FX.Win.Resources
{
	/// <summary>
	/// 내장된 Manifest Resource를 관리를 위한 class입니다.
	/// </summary>
	internal class InternalManifestResourceManager
	{
		/// <summary>
		/// Resource 이름에 의해 현재 Assembly에서 Resource stream을 구합니다.
		/// </summary>
		/// <param name="resourceName">NameSpace를 제외한 Resource의 이름</param>
		/// <returns></returns>
		public static Stream GetStreamByName(string resourceName)
		{
			Assembly assembly = Assembly.GetCallingAssembly();
			Stream stream = null;

			if (assembly != null)
			{
				Type[] types = assembly.GetTypes();
                string nameSpace = "iDASiT.FX.Win.Resources";

				if (types.Length > 0)
                	nameSpace = types[0].Namespace;

				stream = assembly.GetManifestResourceStream(string.Format("{0}.{1}", nameSpace, resourceName));
			}

			return stream;
		}
	}
}
