﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Resources.ResourceType
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 12월 2일 화요일 오후 6:37
/*	Purpose		:	ManifestResourceManager가 관리하는 Xml Resource Type입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.FX.Win.Resources
{
	/// <summary>
	/// ManifestResourceManager가 관리하는 Xml Resource Type입니다.
	/// </summary>
	public enum XmlResourceType
	{
		/// <summary>
		/// Captions.xml
		/// </summary>
		Captions,
		/// <summary>
		/// ContextCaptions.xml
		/// </summary>
		ContextCaptions,
		/// <summary>
		/// Messages.xml
		/// </summary>
		Messages,
		/// <summary>
		/// Themes.xml
		/// </summary>
		Themes,
	}
}
