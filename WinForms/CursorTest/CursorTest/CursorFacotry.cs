using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Reflection;
using System.IO;
using iDASiT.FX.Win.Resources;

namespace iDASiT.FX.Win.Resources
{
	/// <summary>
	/// Resource로 포함된 Animated Cursor를 관리합니다.
	/// </summary>
	internal class CursorFacotry
	{
		[DllImport("User32.dll")]
		private static extern IntPtr LoadCursorFromFile(string fileName);

		/// <summary>
		/// System에서 지원하는 Cursor를 Load할 수 있습니다.
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static Cursor Create(string filename)
		{
			if (File.Exists(filename) == false)
				return Cursors.Default;

			IntPtr hCursor = LoadCursorFromFile(filename);

			if (!IntPtr.Zero.Equals(hCursor))
				return new Cursor(hCursor);
			else
				return Cursors.Default;
		}


	}
}
