﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Reflection;
using System.IO;
using iDASiT.FX.Win.Resources;

namespace CursorTest
{
	public partial class CursorTestForm : Form
	{
        public CursorTestForm()
		{
			InitializeComponent();
		}

		private void CursorTestForm_Shown(object sender, EventArgs e)
		{
			TableLayoutPanel tlpMain = new TableLayoutPanel();
			
			tlpMain.RowCount = 2;
			tlpMain.ColumnCount = 3;
			tlpMain.Dock = DockStyle.Fill;
			Controls.Add(tlpMain);

			for (int row = 0; row < tlpMain.RowCount; row++)
			{
				tlpMain.RowStyles.Add(new ColumnStyle(SizeType.Percent, 50f));

				for (int col = 0; col < tlpMain.ColumnCount; col++)
				{
					Button button = new Button();
					CursorType cursorType = (CursorType)Enum.Parse(typeof(CursorType), (row * tlpMain.ColumnCount + col).ToString());

					button.Text = cursorType.ToString();
					button.Dock = DockStyle.Fill;
					button.Click += delegate(object senderObject, EventArgs eventArgs)
					{
						Cursor.Current = ManifestResourceManager.GetAnimatedCursor(cursorType);
						Thread.Sleep(1000);
					};

					tlpMain.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
					tlpMain.Controls.Add(button, col, row);
				}
			}
		}
	}
}