﻿using System.Windows.Forms;

namespace PropertyGridAttributes
{
	public class TestLowRankForm : Form
	{
		private PropertyGrid pgTest;
		private Button btnCancel;
		private Button btnOK;
		private TestLowRankItem testLowRankItem;

		public TestLowRankForm()
		{
			InitializeComponent();
		}

		public TestLowRankForm(TestLowRankItem testLowRankItem)
			: this()
		{
			this.testLowRankItem = testLowRankItem;
			pgTest.SelectedObject = testLowRankItem;
		}

		private void InitializeComponent()
		{
			this.pgTest = new System.Windows.Forms.PropertyGrid();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// pgTest
			// 
			this.pgTest.Dock = System.Windows.Forms.DockStyle.Left;
			this.pgTest.Location = new System.Drawing.Point(0, 0);
			this.pgTest.Name = "pgTest";
			this.pgTest.Size = new System.Drawing.Size(362, 450);
			this.pgTest.TabIndex = 2;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(372, 71);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// btnOK
			// 
			this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOK.Location = new System.Drawing.Point(372, 28);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 4;
			this.btnOK.Text = "&OK";
			this.btnOK.UseVisualStyleBackColor = true;
			// 
			// TestLowRankForm
			// 
			this.AcceptButton = this.btnOK;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(459, 450);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.pgTest);
			this.Name = "TestLowRankForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.ResumeLayout(false);

		}
	}
}