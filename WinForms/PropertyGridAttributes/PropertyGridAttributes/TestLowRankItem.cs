﻿namespace PropertyGridAttributes
{
	public class TestLowRankItem
	{
		public TestLowRankItem()
		{

		}

		/// <summary>
		/// A를 구하거나 설정합니다.
		/// </summary>
		public int A
		{
			get;
			set;
		}

		/// <summary>
		/// B를 구하거나 설정합니다.
		/// </summary>
		public int B
		{
			get;
			set;
		}

		/// <summary>
		/// C를 구하거나 설정합니다.
		/// </summary>
		public int C
		{
			get;
			set;
		}

		/// <summary>
		/// D를 구하거나 설정합니다.
		/// </summary>
		public int D
		{
			get;
			set;
		}


	}
}