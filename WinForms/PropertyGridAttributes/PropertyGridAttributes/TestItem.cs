﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyGridAttributes
{
	public class TestItem
	{
		public string NonUseA
		{
			get;
			set;
		}

		public string NonUseB
		{
			get;
			set;
		}

		public string NonUseC
		{
			get;
			set;
		}

		[Editor(typeof(TestLowRankItemEditor), typeof(UITypeEditor))]
		[TypeConverterAttribute(typeof(TestFormatConverter))]
		public TestLowRankItem LowRankItem
		{
			get;
			set;
		}

		public TestItem()
		{
			NonUseA = "Test A";
			NonUseB = "Test B";
			NonUseC = "Test C";
		}
	}
}
