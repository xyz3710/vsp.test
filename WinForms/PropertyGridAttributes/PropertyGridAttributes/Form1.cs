﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PropertyGridAttributes
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			btnOK.Click += (sender, ea) =>
			{

			};
			btnCancel.Click += (sender, ea) =>
			{

			};
			pgTest.SelectedObject = new TestItem();
		}
	}
}
