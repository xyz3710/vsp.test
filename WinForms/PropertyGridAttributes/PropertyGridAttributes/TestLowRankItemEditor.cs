﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms.Design;

namespace PropertyGridAttributes
{
	public class TestLowRankItemEditor : UITypeEditor
	{
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.Modal;
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			IWindowsFormsEditorService svc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

			if (svc != null)
			{
				if (value == null)
				{
					value = new TestLowRankItem();
				}

				// value를 as로 받을 경우 Dialog에서 cancel 하더라도 값이 변경되기 때문에 대입하였음.
				TestLowRankItem rollBackItem = value as TestLowRankItem;

				// Dialog Editor를 생성하여 Editing 합니다.
				TestLowRankForm testDialog = new TestLowRankForm(value as TestLowRankItem);

				svc.ShowDialog(testDialog);

				if (testDialog.DialogResult == System.Windows.Forms.DialogResult.Cancel)
				{
					return rollBackItem;
				}
			}

			return value;
		}
	}
}