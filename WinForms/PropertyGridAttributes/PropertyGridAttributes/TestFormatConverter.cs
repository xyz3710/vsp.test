﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace PropertyGridAttributes
{
	public class TestFormatConverter : ExpandableObjectConverter
	{
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(TestItem))
			{
				return true;
			}

			return base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string) && value is TestLowRankItem)
			{
				// 이 부분이 Property Grid의 Title value를 지정하는 곳 입니다.
				TestLowRankItem item = value as TestLowRankItem;

				return item.A.ToString();
			}

			return base.ConvertTo(context, culture, value, destinationType);
		}

		/// <summary>
		/// (옵션) 형식 변환기가 문자열에서 변환할 수 있게 지정하면 표의 개체 문자열 표현을 편집할 수 있습니다.
		/// 이렇게 하려면 먼저 CanConvertFrom 메서드를 재정의하고 소스 Type 매개 변수가 String 형식일 경우 true를 반환하고,
		/// 그렇지 않으면 기본 클래스 CanConvertFrom 메서드의 값을 반환합니다.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="sourceType"></param>
		/// <returns></returns>
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
			{
				return true;
			}

			return base.CanConvertFrom(context, sourceType);
		}

		/// <summary>
		/// 개체의 기본 클래스를 편집할 수 있게 하려면 ConvertFrom 메서드를 재정의하고 값 매개 변수가 String인지 확인합니다.
		/// 값 매개 변수가 String이 아니면 기본 클래스 ConvertFrom 메서드의 값을 반환하고,
		/// 그렇지 않으면 값 매개 변수를 기반으로 클래스(사용자 예제의 SpellingOptions 클래스)의 새 인스턴스를 반환합니다.
		/// 값 매개 변수에서 클래스의 각 속성에 대한 값을 구문 분석해야 합니다.
		/// ConvertTo 메서드에서 만든 구분 문자열의 형식을 알고 있으면 구문 분석을 실행하는 데 도움이 됩니다.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			return base.ConvertFrom(context, culture, value);
		}
	}
}