using System;

namespace System.Windows.Forms
{
	/// <summary>
	/// 
	/// </summary>
	public class KeyShortcut
	{
		private Keys _key;

		/// <summary>
		/// Initializes a new instance of the <see cref="KeyShortcut"/> class.
		/// </summary>
		/// <param name="shortcut">The shortcut.</param>
		public KeyShortcut(Shortcut shortcut)
			: this((Keys)shortcut)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="KeyShortcut"/> class.
		/// </summary>
		public KeyShortcut()
			: this(Keys.None)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="KeyShortcut"/> class.
		/// </summary>
		/// <param name="key">The key.</param>
		public KeyShortcut(Keys key)
		{
			_key = CheckKey(key, false);
		}

		/// <summary>
		/// Gets the key.
		/// </summary>
		/// <value>
		/// The key.
		/// </value>
		public virtual Keys Key { get { return _key; } }

		/// <summary>
		/// Returns a <see cref="System.String" /> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String" /> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			if (this == Empty)
			{
				return "(none)";
			}

			if (IsExist == false)
			{
				return string.Empty;
			}

			string res = GetKeyDisplayText(Key);

			return res;
		}

		/// <summary>
		/// Gets a value indicating whether this instance is exist.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is exist; otherwise, <c>false</c>.
		/// </value>
		public virtual bool IsExist
		{
			get
			{
				if (Key == Keys.None || IsValidShortcut(Key) == false)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Checks the key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="isSecond">if set to <c>true</c> [is second].</param>
		/// <returns></returns>
		protected virtual Keys CheckKey(Keys key, bool isSecond)
		{
			Keys v = IsValidShortcut(key) ? key : Keys.None;

			if (isSecond == true)
			{
				if (Key == Keys.None)
				{
					v = Keys.None;
				}
			}

			return v;
		}

		/// <summary>
		/// Determines whether [is valid shortcut] [the specified key].
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		///   <c>true</c> if [is valid shortcut] [the specified key]; otherwise, <c>false</c>.
		/// </returns>
		protected virtual bool IsValidShortcut(Keys key)
		{
			if (key == Keys.None)
			{
				return false;
			}

			key = key & (~Keys.Modifiers);

			if (key == Keys.None || key == Keys.ControlKey || key == Keys.ShiftKey || key == Keys.Alt)
			{
				return false;
			}

			return true;
		}

		/// <summary>
		/// Gets the key display text.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public static string GetKeyDisplayText(Keys key)
		{
			string res = string.Empty;

			if (key == Keys.None)
			{
				return res;
			}

			if ((key & Keys.Control) != 0 || key == Keys.ControlKey)
			{
				res = ControlKeyName;
			}

			if ((key & Keys.Shift) != 0 || key == Keys.ShiftKey)
			{
				res += (res.Length > 0 ? "+" : string.Empty) + ShiftKeyName;
			}

			if ((key & Keys.Alt) != 0 || key == Keys.Alt)
			{
				res += (res.Length > 0 ? "+" : string.Empty) + AltKeyName;
			}

			key = key & (~Keys.Modifiers);

			if (key != Keys.None)
			{
				res += (res.Length > 0 ? "+" : string.Empty) + key.ToString();
			}

			return res;
		}

		static string GetModifierKeyName(Keys key)
		{
			string keyName = new KeysConverter().ConvertToString(key);
			int index = keyName.IndexOf("+");

			if (index == -1)
			{
				return keyName;
			}

			return keyName.Substring(0, index);
		}

		public static string AltKeyName { get { return GetModifierKeyName(Keys.Alt); } }

		/// <summary>
		/// Gets the name of the shift key.
		/// </summary>
		/// <value>
		/// The name of the shift key.
		/// </value>
		public static string ShiftKeyName { get { return GetModifierKeyName(Keys.Shift); } }

		/// <summary>
		/// Gets the name of the control key.
		/// </summary>
		/// <value>
		/// The name of the control key.
		/// </value>
		public static string ControlKeyName { get { return GetModifierKeyName(Keys.Control); } }

		/// <summary>
		/// The empty
		/// </summary>
		public static KeyShortcut Empty = new KeyShortcut();

		public static bool operator ==(KeyShortcut left, KeyShortcut right)
		{
			if (Object.ReferenceEquals(left, right))
			{
				return true;
			}

			if (Object.ReferenceEquals(left, null))
			{
				return false;
			}

			if (Object.ReferenceEquals(right, null))
			{
				return false;
			}

			return (left.Key == right.Key);
		}

		/// <summary>
		/// Implements the operator !=.
		/// </summary>
		/// <param name="left">The left.</param>
		/// <param name="right">The right.</param>
		/// <returns>
		/// The result of the operator.
		/// </returns>
		public static bool operator !=(KeyShortcut left, KeyShortcut right)
		{
			return !(left == right);
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
		/// </summary>
		/// <param name="value">The <see cref="System.Object" /> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		public override bool Equals(object value)
		{
			KeyShortcut shcut = value as KeyShortcut;

			if (shcut == null)
			{
				return false;
			}

			return _key == shcut.Key;
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
		/// </returns>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
