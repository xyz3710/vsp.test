﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotKeyInputBox
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();

			//txtEditor.KeyDown += TextBox1_KeyDown;
			//txtEditor.PreviewKeyDown += TextBox1_PreviewKeyDown;

			txtEditor.KeyDown += (sender, e) =>
			{
				e.Handled = true;

				if (IsFitKey(e) == false)
				{
					return;
				}

				if (EditingKey() == true)
				{
					_scValue = new KeyShortcut(e.KeyData);
				}

				ShowShortcuts();
			};
			txtEditor.KeyPress += (sender, e) =>
			{
				e.Handled = true;
			};
			txtEditor.TextChanged += (sender, e) =>
			{
				ShowShortcuts();
			};


			tbShortcut.SetShortcutTextBox();
			btnSetShortcut.Click += (sender, e) =>
			{
				var key = Keys.Control | Keys.C;

				tbShortcut.Tag = key;
				tbShortcut.Text = key.ToString();
				//tbShortcut.Tag = null;
			};
		}

		private KeysConverter _keyConverter = new KeysConverter();

		private void TextBox1_KeyDown(object sender, KeyEventArgs e)
		{
			Keys modifierKeys = e.Modifiers;
			Keys pressedKey = e.KeyData ^ modifierKeys; //remove modifier keys

			e.Handled = false;
			System.Diagnostics.Debug.WriteLine($"{modifierKeys}, {pressedKey}");
			var a = RemoveHelperText(_keyConverter.ConvertToString(e.KeyData));
			txtEditor.ResetText();
			txtEditor.Text = a;
		}

		private string RemoveHelperText(string convertToString)
		{
			var result = convertToString.Replace("+ControlKey", string.Empty);

			result = result.Replace("+ShiftKey", string.Empty);
			result = result.Replace("+Menu", string.Empty);

			return result;
		}

		private KeyShortcut _scValue;

		private KeyShortcut ShortcutValue
		{
			get { return _scValue; }
		}

		private void ShowShortcuts()
		{
			if (_scValue.IsExist == true)
			{
				txtEditor.Text = _keyConverter.ConvertToString(_scValue.Key);
			}
			else
			{
				txtEditor.Text = string.Empty;
			}

			txtEditor.SelectionStart = txtEditor.Text.Length;
		}

		private bool IsFitKey(KeyEventArgs e)
		{
			if (e.KeyCode == Keys.ControlKey ||
				e.KeyCode == Keys.ShiftKey ||
				e.KeyCode == Keys.Menu ||
				e.KeyCode == Keys.Capital)
			{
				return false;
			}

			if (e.KeyData == Keys.Back)
			{
				_scValue = new KeyShortcut(_scValue.Key);
				ShowShortcuts();

				return false;
			}

			if (e.KeyData == Keys.Enter)
			{
				DialogResult = DialogResult.OK;

				return false;
			}

			if (e.KeyData == Keys.Escape)
			{
				DialogResult = DialogResult.Cancel;

				return false;
			}

			if (EditingKey() == true)
			{
				if (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.Z
					&& e.Alt == false
					&& e.Control == false)
				{
					return false;
				}
			}

			return true;
		}

		private bool EditingKey()
		{
			return true;
		}

	}
}
