﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace HotKeyEditor
{
	public class HotKeyHelper
	{
		private static HashSet<Key> _ignoredKey = new HashSet<Key>() { Key.LeftAlt, Key.RightAlt, Key.LeftCtrl,
			Key.RightCtrl, Key.LeftShift, Key.RightShift, Key.RWin, Key.LWin};

		public static readonly DependencyProperty IsHotKeyEditorProperty =
			DependencyProperty.RegisterAttached(
				"IsHotKeyEditor",
				typeof(Boolean),
				typeof(TextBox),
				new UIPropertyMetadata(false, OnIsHotKeyEditorChanged));

		public static void SetIsHotKeyEditor(UIElement element, Boolean value)
		{
			element.SetValue(IsHotKeyEditorProperty, value);
		}

		public static Boolean GetIsHotKeyEditor(UIElement element)
		{
			return (Boolean)element.GetValue(IsHotKeyEditorProperty);
		}

		static void OnIsHotKeyEditorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var textBox = d as System.Windows.Forms.TextBox;

			if ((bool)e.NewValue)
			{
				textBox.PreviewKeyDown += textBox_PreviewKeyDown;
			}
			else
			{
				textBox.PreviewKeyDown -= textBox_PreviewKeyDown;
			}
		}

		private static void textBox_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
			var textBox = sender as TextBox;
			var lastHotKey = textBox.Tag as HotKey;

			if (!_ignoredKey.Contains(e.Key) && (e.Key != Key.System || (e.Key == Key.System && !_ignoredKey.Contains(e.SystemKey))))
			{
				var key = (e.Key == Key.System && !_ignoredKey.Contains(e.SystemKey)) ? e.SystemKey : e.Key;
				if (!string.IsNullOrWhiteSpace(textBox.Text) && (lastHotKey == null))
				{
					textBox.Text = string.Empty;
				}
				else
				{
					var hotKey = new HotKey()
					{
						Ctrl = ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control),
						Alt = ((Keyboard.Modifiers & ModifierKeys.Alt) == ModifierKeys.Alt),
						Shift = ((Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift),
						Key = key
					};
					textBox.Text = string.Format("{0}{1}{2}",
						lastHotKey != null ? lastHotKey.ToString() : string.Empty,
						string.IsNullOrWhiteSpace(textBox.Text) ? string.Empty : ", ",
						hotKey);
					textBox.SelectionStart = textBox.Text.Length;
					textBox.SelectionLength = 0;
					if (lastHotKey == null)
					{
						textBox.Tag = hotKey;
					}
					else
					{
						textBox.Tag = null;
					}
				}
			}
			e.Handled = true;
		}
	}
}



