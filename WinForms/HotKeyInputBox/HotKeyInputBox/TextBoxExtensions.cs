﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Forms
{
	/// <summary>
	/// Class TextBoxExtensions.
	/// </summary>
	public static class TextBoxExtensions
	{
		/// <summary>
		/// Masks the numeric only.
		/// </summary>
		/// <param name="textBox">The text box.</param>
		/// <param name="includeDot">if set to <c>true</c> [include dot].</param>
		public static void MaskNumericOnly(this TextBox textBox, bool includeDot = false)
		{
			textBox.KeyPress += (sender, e) =>
			{
				var numberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
				string decimalSeparator = numberFormatInfo.NumberDecimalSeparator;
				string groupSeparator = numberFormatInfo.NumberGroupSeparator;
				string negativeSign = numberFormatInfo.NegativeSign;

				string keyInput = e.KeyChar.ToString();

				if (char.IsDigit(e.KeyChar) == true)
				{
					// Digits are OK
				}
				else if (keyInput.Equals(decimalSeparator) == true
					|| keyInput.Equals(groupSeparator) == true
					|| keyInput.Equals(negativeSign) == true)
				{
					// Decimal separator is OK
				}
				else if (e.KeyChar == '\b')
				{
					// Backspace key is OK
				}
				else
				{
					e.Handled = true;
				}
			};
		}

		/// <summary>
		/// TextBox에 Enter키가 눌렸을 때 처리할 이벤트 핸들러를 등록 합니다.
		/// </summary>
		/// <param name="textBox">The text box.</param>
		/// <param name="action">The action.</param>
		public static void SetEnterKeyDown(this TextBox textBox, Action<TextBox> action)
		{
			textBox.KeyDown += (sender, e) =>
			{
				if (e.KeyData == Keys.Enter)
				{
					action(sender as TextBox);
				}
			};
		}

		/// <summary>
		/// Sets the hint text.
		/// </summary>
		/// <param name="textBox">The text box.</param>
		/// <param name="hintText">The hint text.</param>
		public static void SetHintText(this TextBox textBox, string hintText = "내용을 입력해주십시오.")
		{
			var hintTextColor = System.Drawing.SystemColors.GrayText;

			textBox.Text = hintText;
			textBox.ForeColor = hintTextColor;
			textBox.Leave += (sender, e) =>
			{
				var tb = sender as TextBox;

				if (tb != null && tb.Text.Length == 0)
				{
					tb.Text = hintText;
					tb.ForeColor = hintTextColor;
				}
			};
			textBox.Enter += (sender, e) =>
			{
				var tb = sender as TextBox;

				if (tb != null && tb.Text == hintText)
				{
					tb.Text = string.Empty;
					tb.ForeColor = System.Drawing.SystemColors.WindowText;
				}
			};
		}

		/// <summary>
		/// Sets the text box to shortcut text box.
		/// </summary>
		/// <remarks>If you set shortcut key then assign TextBox.Tag to <seealso cref="System.Windows.Forms.Keys"/> and change TextBox.Text. see example.</remarks>
		/// <param name="textBox">The text box.</param>
		/// <param name="pressedKeyValidateAction">Checks that the currently pressed key is valid. 
		/// If it returns false, it does not perform internal validation and does not display the key typed on the screen.</param>
		/// <example>
		/// // Set shortcut to textbox.
		/// Keys key = Keys.Control | Keys.C;
		/// 
		/// tbShortcut.Tag = key;
		/// tbShortcut.Text = key.ToString();  // After text changed TextBox.Tag assigned null.
		/// </example>
		public static void SetShortcutTextBox(this TextBox textBox, Func<KeyEventArgs, bool> pressedKeyValidateAction = null)
		{
			KeysConverter keyConverter = new KeysConverter();
			KeyShortcut scValue = new KeyShortcut();
			bool editingKey = true;
			Action ShowShortcuts = () =>
			{
				if (scValue.IsExist == true)
				{
					textBox.Text = keyConverter.ConvertToString(scValue.Key);
				}
				else
				{
					textBox.Text = string.Empty;
				}

				textBox.SelectionStart = textBox.Text.Length;
			};
			Func<KeyEventArgs, bool> IsFitKey = e =>
			{
				if (pressedKeyValidateAction != null)
				{
					var result = pressedKeyValidateAction(e);

					if (result == false)
					{
						return false;
					}
				}

				if (e.KeyCode == Keys.ControlKey ||
					e.KeyCode == Keys.ShiftKey ||
					e.KeyCode == Keys.Menu ||
					e.KeyCode == Keys.Capital)
				{
					return false;
				}

				if (e.KeyData == Keys.Back)
				{
					//scValue = new KeyShortcut(scValue.Key);		// If you keep previous key
					scValue = new KeyShortcut(Keys.None);       // If you remove current key 

					ShowShortcuts();

					return false;
				}

				if (editingKey == true)
				{
					if (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.Z
						&& e.Alt == false
						&& e.Control == false)
					{
						return false;
					}
				}

				return true;
			};

			textBox.KeyDown += (sender, e) =>
			{
				e.Handled = true;

				if (IsFitKey(e) == false)
				{
					return;
				}

				if (editingKey == true)
				{
					scValue = new KeyShortcut(e.KeyData);
				}

				ShowShortcuts();
			};
			textBox.KeyPress += (sender, e) =>
			{
				e.Handled = true;
			};
			textBox.TextChanged += (sender, e) =>
			{
				if (textBox.Tag != null)
				{
					if (textBox.Tag is Keys assignedKey)
					{
						scValue = new KeyShortcut(assignedKey);
						textBox.Tag = null;
					}
				}

				ShowShortcuts();
			};
		}
	}
}
