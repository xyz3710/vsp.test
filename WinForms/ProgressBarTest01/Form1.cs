using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using System.Runtime.Remoting.Messaging;

namespace ProgressBarTest01
{
	/// <summary>
	/// Form1에 대한 요약 설명입니다.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private bool _play;
		private int _currentValue;
		private int _maxLoop = 100000;

		private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label lblCount;
		private System.Windows.Forms.Label lblCurrentValue;
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.Container components = null;

		// delegate 선언
		public delegate int UpdateValueDelegate();

		public Form1()
		{
			//
			// Windows Form 디자이너 지원에 필요합니다.
			//
			InitializeComponent();
		}

		#region Dispose
		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion

		#region Windows Form 디자이너에서 생성한 코드
		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnStart = new System.Windows.Forms.Button();
			this.btnStop = new System.Windows.Forms.Button();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.lblCount = new System.Windows.Forms.Label();
			this.lblCurrentValue = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnStart
			// 
			this.btnStart.Location = new System.Drawing.Point(16, 16);
			this.btnStart.Name = "btnStart";
			this.btnStart.TabIndex = 0;
			this.btnStart.Text = "시작";
			this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
			// 
			// btnStop
			// 
			this.btnStop.Location = new System.Drawing.Point(96, 16);
			this.btnStop.Name = "btnStop";
			this.btnStop.TabIndex = 1;
			this.btnStop.Text = "중지";
			this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(16, 48);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(832, 23);
			this.progressBar1.TabIndex = 2;
			// 
			// lblCount
			// 
			this.lblCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblCount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.lblCount.Location = new System.Drawing.Point(176, 16);
			this.lblCount.Name = "lblCount";
			this.lblCount.TabIndex = 3;
			this.lblCount.Text = "Count";
			// 
			// lblCurrentValue
			// 
			this.lblCurrentValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblCurrentValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.lblCurrentValue.Location = new System.Drawing.Point(288, 16);
			this.lblCurrentValue.Name = "lblCurrentValue";
			this.lblCurrentValue.TabIndex = 3;
			this.lblCurrentValue.Text = "현재값 : ";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(864, 413);
			this.Controls.Add(this.lblCount);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.btnStop);
			this.Controls.Add(this.btnStart);
			this.Controls.Add(this.lblCurrentValue);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		#region Main
		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}
		#endregion

		private void btnStart_Click(object sender, System.EventArgs e)
		{
			_play = true;
			progressBar1.Maximum = _maxLoop;

			// delegate instance 생성
			UpdateValueDelegate updateValue = new UpdateValueDelegate(asyncMethod);

			// callback method 선언
			AsyncCallback asyncCallback = new AsyncCallback(asyncCallbackMethod);

			// 비동기 호출 초기화
			IAsyncResult ar = updateValue.BeginInvoke(asyncCallback, updateValue);
		}

		/// <summary>
		/// async mehtod
		/// </summary>
		private int asyncMethod()
		{
			while(_play == true)
			{
				progressBar1.Value = _currentValue++;
				lblCount.Text = _currentValue.ToString();
			}

			// 실제 _currentValue 값은 의미없으나 return 형 signature를 맞추기 위해 test 하였다.
			return _currentValue; 
		}

		/// <summary>
		/// call back 함수로서 비동기 작업이 끝나면 호출됩니다.
		/// </summary>
		/// <param name="ar"></param>
		private void asyncCallbackMethod(IAsyncResult ar)
		{
			UpdateValueDelegate updateValueDelegate = (UpdateValueDelegate)ar.AsyncState;

			// EndInvoke를 호출합니다.
			// parameter가 있으면 return값 등을 구할 수 있습니다.
			int currentValue = updateValueDelegate.EndInvoke(ar);

			lblCurrentValue.Text = String.Format("현재값 : {0}", currentValue);
		}

		private void btnStop_Click(object sender, System.EventArgs e)
		{
			_play = false;
		}
	}
}
