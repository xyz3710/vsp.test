using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Design;
using System.ComponentModel.Design;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace System.Windows.Forms
{
	[ToolboxBitmap(typeof(System.Windows.Forms.TabControl))]
	//[Designer(typeof(Designers.FlatTabControlDesigner))]
	public class FlatTabControl : System.Windows.Forms.TabControl
	{
		private const int MARGIN = 5;

		private System.ComponentModel.Container components = null;
		private SubClass upDownHandler;
		private ImageList arrowImages;
		private bool _upDown;
		Rectangle closeX = Rectangle.Empty;
		StringFormat _stringFormat;

		/// <summary>
		/// Initializes a new instance of the <see cref="FlatTabControl"/> class.
		/// </summary>
		public FlatTabControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.DoubleBuffer, true);
			SetStyle(ControlStyles.ResizeRedraw, true);
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);

			BackColor = SystemColors.Control;
			//DrawMode = TabDrawMode.OwnerDrawFixed;

			arrowImages = new ImageList();
			//leftRightImages.ImageSize = new Size(16, 16);		// default
			_stringFormat = new StringFormat()
			{
				Alignment = StringAlignment.Center,
				LineAlignment = StringAlignment.Center
			};

			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FlatTabControl));
			Bitmap updownImage = ((System.Drawing.Bitmap)(resources.GetObject("TabIcons.bmp")));

			if (updownImage != null)
			{
				updownImage.MakeTransparent(Color.White);
				arrowImages.Images.AddStrip(updownImage);
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}

				arrowImages.Dispose();
			}
			base.Dispose(disposing);
		}

		/// <summary>
		/// <see cref="M:System.Windows.Forms.Control.CreateControl" /> 메서드를 발생시킵니다.
		/// </summary>
		protected override void OnCreateControl()
		{
			base.OnCreateControl();

			FindUpDown();
		}

		/// <summary>
		/// <see cref="E:System.Windows.Forms.Control.ControlAdded" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.Windows.Forms.ControlEventArgs" />입니다.</param>
		protected override void OnControlAdded(ControlEventArgs e)
		{
			base.OnControlAdded(e);

			FindUpDown();
			UpdateUpDown();

			var tabPage = e.Control as TabPage;

			if (tabPage == null)
			{
				return;
			}

			SelectedTab = tabPage;
		}

		/// <summary>
		/// <see cref="E:System.Windows.Forms.Control.ControlRemoved" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.Windows.Forms.ControlEventArgs" />입니다.</param>
		protected override void OnControlRemoved(ControlEventArgs e)
		{
			base.OnControlRemoved(e);

			FindUpDown();
			UpdateUpDown();

			if (TabCount > 1)
			{
				var tabPage = e.Control as TabPage;

				if (tabPage == null)
				{
					return;
				}

				var index = TabPages.IndexOf(tabPage);
				var isLast = index == (TabPages.Count - 1);
				var selectedIndex = -1;

				if (isLast == true)
				{
					selectedIndex = index - 1;
				}
				else
				{
					selectedIndex = index + 1;
				}

				SelectedTab = TabPages[selectedIndex];
			}
		}

		/// <summary>
		/// <see cref="E:System.Windows.Forms.TabControl.SelectedIndexChanged" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.EventArgs" />입니다.</param>
		protected override void OnSelectedIndexChanged(EventArgs e)
		{
			base.OnSelectedIndexChanged(e);

			UpdateUpDown();
		}

		/// <summary>
		/// <see cref="E:System.Windows.Forms.Control.MouseClick" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.Windows.Forms.MouseEventArgs" />입니다.</param>
		protected override void OnMouseClick(MouseEventArgs e)
		{
			base.OnMouseClick(e);
		}

		/// <summary>
		/// <see cref="E:System.Windows.Forms.Control.Paint" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.Windows.Forms.PaintEventArgs" />입니다.</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			DrawControl(e.Graphics);
		}

		Rectangle TabBoundary;
		RectangleF TabTextBoundary;

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			//Graphics g = pevent.Graphics;
			//g.FillRectangle(new SolidBrush(Color.Red), 0, 0, this.Size.Width, this.Size.Height);

			//foreach (TabPage tp in this.TabPages)
			//{
			//	//drawItem
			//	int index = this.TabPages.IndexOf(tp);

			//	TabBoundary = this.GetTabRect(index);
			//	TabTextBoundary = (RectangleF)this.GetTabRect(index);

			//	g.FillRectangle(new SolidBrush(Color.LightBlue), this.TabBoundary);
			//	g.DrawString("tabPage " + index.ToString(), this.Font, new SolidBrush(Color.Black), this.TabTextBoundary, _stringFormat);
			//}
		}

		private void DrawControl(Graphics g)
		{
			if (Visible == false)
			{
				return;
			}

			Rectangle tabControlArea = ClientRectangle;
			Rectangle tabArea = DisplayRectangle;

			using (var clientBrush = new SolidBrush(BackColor))
			{
				g.FillRectangle(clientBrush, tabControlArea);
			}

			int delta = SystemInformation.Border3DSize.Width;

			using (var border = new Pen(SystemColors.ControlDark))
			{
				tabArea.Inflate(delta, delta);
				g.DrawRectangle(border, tabArea);
			}

			Region savedRegion = g.Clip;		// clip region for drawing tabs
			Rectangle region;
			int width = tabArea.Width + MARGIN;

			if (_upDown == false)
			{
				if (NativeMethods.IsWindowVisible(upDownHandler.Handle))		// exclude updown control for painting
				{
					Rectangle updownRec = new Rectangle();
					NativeMethods.GetWindowRect(upDownHandler.Handle, ref updownRec);
					Rectangle updown2Rec = RectangleToClient(updownRec);

					width = updown2Rec.X;
				}
			}

			region = new Rectangle(tabArea.Left, tabControlArea.Top, width - MARGIN, tabControlArea.Height);

			g.SetClip(region);

			for (int i = 0; i < TabCount; i++)
			{
				DrawTab(g, TabPages[i], i);
			}

			g.Clip = savedRegion;

			if (SelectedTab != null)		// draw background to cover flat border areas
			{
				TabPage tabPage = SelectedTab;
				Color color = tabPage.BackColor;

				using (var border = new Pen(color))
				{
					tabArea.Offset(1, 1);
					tabArea.Width -= 2;
					tabArea.Height -= 2;

					g.DrawRectangle(border, tabArea);
					tabArea.Width -= 1;
					tabArea.Height -= 1;
					g.DrawRectangle(border, tabArea);
				}
			}
		}

		private void DrawTab(Graphics g, TabPage tabPage, int index)
		{
			Rectangle bound = GetTabRect(index);
			RectangleF tabTextArea = (RectangleF)GetTabRect(index);
			bool selected = (SelectedIndex == index);
			Point[] point = new Point[7];

			if (Alignment == TabAlignment.Top)
			{
				point[0] = new Point(bound.Left, bound.Bottom);
				point[1] = new Point(bound.Left, bound.Top + 3);
				point[2] = new Point(bound.Left + 3, bound.Top);
				point[3] = new Point(bound.Right - 3, bound.Top);
				point[4] = new Point(bound.Right, bound.Top + 3);
				point[5] = new Point(bound.Right, bound.Bottom);
				point[6] = new Point(bound.Left, bound.Bottom);
			}
			else
			{
				point[0] = new Point(bound.Left, bound.Top);
				point[1] = new Point(bound.Right, bound.Top);
				point[2] = new Point(bound.Right, bound.Bottom - 3);
				point[3] = new Point(bound.Right - 3, bound.Bottom);
				point[4] = new Point(bound.Left + 3, bound.Bottom);
				point[5] = new Point(bound.Left, bound.Bottom - 3);
				point[6] = new Point(bound.Left, bound.Top);
			}

			using (var tabBrush = new SolidBrush(tabPage.BackColor))		// fill this tab with background color
			{
				g.FillPolygon(tabBrush, point);
			}

			// draw border
			g.DrawPolygon(SystemPens.ControlDark, point);

			if (selected == true)		// clear bottom lines
			{
				using (Pen pen = new Pen(tabPage.BackColor))
				{
					switch (Alignment)
					{
						case TabAlignment.Top:
							g.DrawLine(pen, bound.Left + 1, bound.Bottom, bound.Right - 1, bound.Bottom);
							g.DrawLine(pen, bound.Left + 1, bound.Bottom + 1, bound.Right - 1, bound.Bottom + 1);

							break;

						case TabAlignment.Bottom:
							g.DrawLine(pen, bound.Left + 1, bound.Top, bound.Right - 1, bound.Top);
							g.DrawLine(pen, bound.Left + 1, bound.Top - 1, bound.Right - 1, bound.Top - 1);
							g.DrawLine(pen, bound.Left + 1, bound.Top - 2, bound.Right - 1, bound.Top - 2);

							break;
					}
				}
			}

			if ((tabPage.ImageIndex >= 0) && (ImageList != null) && (ImageList.Images[tabPage.ImageIndex] != null))
			{	// draw tab's icon
				int leftMargin = 8;
				int rightMargin = 2;
				Image image = ImageList.Images[tabPage.ImageIndex];
				Rectangle imgaeRectangle = new Rectangle(bound.X + leftMargin, bound.Y + 1, image.Width, image.Height);
				float adjustRectangle = (float)(leftMargin + image.Width + rightMargin);

				imgaeRectangle.Y += (bound.Height - image.Height) / 2;
				tabTextArea.X += adjustRectangle;
				tabTextArea.Width -= adjustRectangle;

				g.DrawImage(image, imgaeRectangle);
			}

			//Size xSize = new Size(13, 13);
			//TabPage tp = TabPages[e.Index];
			//e.DrawBackground();

			//using (SolidBrush brush = new SolidBrush(e.ForeColor))
			//{
			//	e.Graphics.DrawString(tp.Text + "   ", e.Font, brush, e.Bounds.X + 3, e.Bounds.Y + 4);
			//}

			//if (e.State == DrawItemState.Selected)
			//{
			//	closeX = new Rectangle(e.Bounds.Right - xSize.Width, e.Bounds.Top, xSize.Width, xSize.Height);

			//	using (SolidBrush brush = new SolidBrush(Color.LightGray))
			//	{
			//		e.Graphics.DrawString("x", e.Font, brush, e.Bounds.Right - xSize.Width, e.Bounds.Y + 4);
			//	}
			//}

			{
				float adjustRectangle = (float)(12);

				//tabTextArea.X -= adjustRectangle;
				//tabTextArea.Width += adjustRectangle;
			}

			using (var tabBrush = new SolidBrush(tabPage.ForeColor))
			{
				g.DrawString(tabPage.Text, Font, tabBrush, tabTextArea, _stringFormat);
			}
		}

		private void DrawIcons(Graphics g)
		{
			if ((arrowImages == null) || (arrowImages.Images.Count != 4))
			{
				return;
			}

			Rectangle TabControlArea = ClientRectangle;
			Rectangle r0 = new Rectangle();
			NativeMethods.GetClientRect(upDownHandler.Handle, ref r0);

			using (Brush backBrush = new SolidBrush(SystemColors.Control))
			{
				g.FillRectangle(backBrush, r0);
			}

			using (Pen border = new Pen(SystemColors.ControlDark))
			{
				Rectangle borderRectangle = r0;

				borderRectangle.Inflate(-1, -1);
				g.DrawRectangle(border, borderRectangle);
			}

			int middle = (r0.Width / 2);
			int top = (r0.Height - 16) / 2;
			int left = (middle - 16) / 2;

			Rectangle r1 = new Rectangle(left, top, 16, 16);
			Rectangle r2 = new Rectangle(middle + left, top, 16, 16);
			Image image = arrowImages.Images[1];		// draw buttons

			if (image != null)
			{
				if (TabCount > 0)
				{
					Rectangle r3 = GetTabRect(0);

					if (r3.Left < TabControlArea.Left)
					{
						g.DrawImage(image, r1);
					}
					else
					{
						image = arrowImages.Images[3];

						if (image != null)
						{
							g.DrawImage(image, r1);
						}
					}
				}
			}

			image = arrowImages.Images[0];

			if (image != null)
			{
				if (TabCount > 0)
				{
					Rectangle r3 = GetTabRect(TabCount - 1);

					if (r3.Right > (TabControlArea.Width - r0.Width))
					{
						g.DrawImage(image, r2);
					}
					else
					{
						image = arrowImages.Images[2];

						if (image != null)
						{
							g.DrawImage(image, r2);
						}
					}
				}
			}
		}

		private void FindUpDown()
		{
			bool found = false;
			IntPtr pWnd = NativeMethods.GetWindow(Handle, NativeMethods.GW_CHILD);

			while (pWnd != IntPtr.Zero)
			{
				char[] className = new char[33];		// Get the window class name
				int length = NativeMethods.GetClassName(pWnd, className, 32);
				string foundClass = new string(className, 0, length);

				if (foundClass == "msctls_updown32")
				{
					found = true;

					if (_upDown == false)
					{
						upDownHandler = new SubClass(pWnd, true);
						upDownHandler.SubClassedWndProc += OnSubClassedWndProc;
						_upDown = true;
					}

					break;
				}

				pWnd = NativeMethods.GetWindow(pWnd, NativeMethods.GW_HWNDNEXT);
			}

			if (found == false && _upDown == true)
			{
				_upDown = false;
			}
		}

		private void UpdateUpDown()
		{
			if (_upDown == true)
			{
				if (NativeMethods.IsWindowVisible(upDownHandler.Handle))
				{
					Rectangle rect = new Rectangle();

					NativeMethods.GetClientRect(upDownHandler.Handle, ref rect);
					NativeMethods.InvalidateRect(upDownHandler.Handle, ref rect, true);
				}
			}
		}

		private int OnSubClassedWndProc(ref Message m)
		{
			switch (m.Msg)
			{
				case NativeMethods.WM_PAINT:		// redraw
					{
						IntPtr hDC = NativeMethods.GetWindowDC(upDownHandler.Handle);

						using (Graphics g = Graphics.FromHdc(hDC))
						{
							DrawIcons(g);
						}

						NativeMethods.ReleaseDC(upDownHandler.Handle, hDC);

						// return 0 (processed)
						m.Result = IntPtr.Zero;

						Rectangle rect = new Rectangle();		// validate current rect
						NativeMethods.GetClientRect(upDownHandler.Handle, ref rect);
						NativeMethods.ValidateRect(upDownHandler.Handle, ref rect);
					}

					return 1;
			}

			return 0;
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}

		#endregion

		#region Properties

		/// <summary>
		/// 이 탭 컨트롤의 탭 페이지 컬렉션을 가져옵니다.
		/// </summary>
		/// <value>The tab pages.</value>
		/// <returns>이 <see cref="T:System.Windows.Forms.TabControl" />의 <see cref="T:System.Windows.Forms.TabPage" /> 개체가 들어 있는 <see cref="T:System.Windows.Forms.TabControl.TabPageCollection" />입니다.</returns>
		[Editor(typeof(TabPageExCollectionEditor), typeof(UITypeEditor))]
		public new TabPageCollection TabPages
		{
			get
			{
				return base.TabPages;
			}
		}

		/// <summary>
		/// 탭을 맞출 컨트롤의 영역(예: 위쪽)을 가져오거나 설정합니다.
		/// </summary>
		/// <value>The alignment.</value>
		/// <returns>
		///   <see cref="T:System.Windows.Forms.TabAlignment" /> 값 중 하나입니다. 기본값은 Top입니다.</returns>
		///   <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence" />
		///   <IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   </PermissionSet>
		public new TabAlignment Alignment
		{
			get
			{
				return base.Alignment;
			}
			set
			{
				TabAlignment tabAlignment = value;

				if ((tabAlignment != TabAlignment.Top) && (tabAlignment != TabAlignment.Bottom))
				{
					tabAlignment = TabAlignment.Top;
				}

				base.Alignment = tabAlignment;
			}
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool Multiline
		{
			get
			{
				return base.Multiline;
			}
			set
			{
				base.Multiline = false;
			}
		}

		/// <summary>
		/// 이 멤버는 이 컨트롤에 의미가 없습니다.
		/// </summary>
		/// <value>The color of the back.</value>
		/// <returns>항상 <see cref="P:System.Drawing.SystemColors.Control" />입니다.</returns>
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
				Invalidate();
			}
		}

		#endregion

		internal class TabPageExCollectionEditor : CollectionEditor
		{
			public TabPageExCollectionEditor(System.Type type)
				: base(type)
			{
			}

			protected override Type CreateCollectionItemType()
			{
				return typeof(TabPage);
			}
		}
	}
}
