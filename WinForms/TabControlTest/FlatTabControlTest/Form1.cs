﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlatTabControlTest
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private static int nt = 0;

		private void button2_Click(object sender, System.EventArgs e)
		{
			TabPage tabPage = new System.Windows.Forms.TabPage("newTab");

			switch (nt)
			{
				case 0:
					{
						tabPage.BackColor = Color.LightCoral;
						tabPage.ImageIndex = 0;
						nt = 1;
					}
					break;

				case 1:
					{
						tabPage.BackColor = Color.LightGoldenrodYellow;
						tabPage.ImageIndex = 1;
						nt = 2;
					}
					break;

				case 2:
					{
						tabPage.BackColor = Color.LightSeaGreen;
						tabPage.ImageIndex = 2;
						nt = 0;
					}
					break;
			}

			tabControl1.TabPages.Add(tabPage);
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			tabControl1.TabPages.RemoveAt(tabControl1.SelectedIndex);
		}

		private void tabControl1_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
		{
			MessageBox.Show("1");
		}
	}
}
