using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace InheritanceExam
{
	public class TipForms : DotNetGuruLibrary.TipEnableForm
	{
		private System.Windows.Forms.Button btnToolTip;
		private System.ComponentModel.IContainer components = null;

		protected void SetToolTip()
		{
			this.AddToolTip(btnToolTip, "툴 팁을 추가한 버튼입니다.");
		}

		public TipForms()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			SetToolTip();
			btnToolTip.MouseDown += new MouseEventHandler(btnToolTip_MouseDown);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnToolTip = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnToolTip
			// 
			this.btnToolTip.Location = new System.Drawing.Point(52, 68);
			this.btnToolTip.Name = "btnToolTip";
			this.btnToolTip.TabIndex = 0;
			this.btnToolTip.Text = "button1";
			this.btnToolTip.Click += new System.EventHandler(this.btnToolTip_Click);
			// 
			// TipForms
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.btnToolTip);
			this.Name = "TipForms";
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main()
		{
			Application.Run(new TipForms());
		}

		private void btnToolTip_Click(object sender, System.EventArgs e)
		{
			
		}

		private void btnToolTip_MouseDown(object sender, MouseEventArgs e)
		{
			this.TipEnableForm_MouseDown(sender, e);
		}
	}
}

