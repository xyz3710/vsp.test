using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace DotNetGuruLibrary
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class TipEnableForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ToolTip toolTip;
		private System.ComponentModel.IContainer components;

		protected ToolTip GetToolTipCtrl()
		{
			return toolTip;
		}

		protected void AddToolTip(Control control, string caption)
		{
			toolTip.SetToolTip(control, caption);
		}

		public TipEnableForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			// 
			// TipEnableForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Name = "TipEnableForm";
			this.Text = "Form1";
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TipEnableForm_MouseDown);

		}
		#endregion

		protected void TipEnableForm_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			MessageBox.Show("이벤트 상속");
		}
	}
}
