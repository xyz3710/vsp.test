using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace BaseForm
{
	/// <summary>
	/// Summary description for BaseForm.
	/// </summary>
	public class BaseForm : System.Windows.Forms.Form
	{
		protected System.Windows.Forms.ListView listViewEx;
	
		public BaseForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.Resize += new EventHandler(BaseForm_Resize);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem();
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem();
			System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem();
			this.listView1 = new System.Windows.Forms.ListView();
			// 
			// listView1
			// 
			this.listView1.FullRowSelect = true;
			listViewItem1.Text = "1";
			listViewItem2.Text = "2";
			listViewItem3.Text = "3";
			this.listView1.Items.Add(listViewItem1);
			this.listView1.Items.Add(listViewItem2);
			this.listView1.Items.Add(listViewItem3);
			this.listView1.Size = new System.Drawing.Size(168, 180);
			this.listView1.View = System.Windows.Forms.View.List;
			// 
			// BaseForm
			// 
			this.ClientSize = new System.Drawing.Size(174, 195);
			this.Controls.Add(this.listView1);
			this.Text = "BaseForm";

		}
		#endregion

		private void BaseForm_Resize(object sender, EventArgs e)
		{
			this.listViewEx.Capture = true;
		}
	}
}
