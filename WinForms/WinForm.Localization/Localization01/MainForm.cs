﻿/**********************************************************************************************************************/
/*	Domain		:	WinformGlobalization01.MainForm
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 12월 14일 월요일 오전 9:45
/*	Purpose		:	Globalization 테스트 폼
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	1. MainForm 또는 LocalizedForm의 Localiable = true로 한다.
 *					2. Winform control을 배치 한다.
 *					3. Localiable = true인 폼의 Language를 지역화 하기 위한 언어로 변경한 뒤
 *					4. 해당 리소스를 변경한다.
 *					*. Debug 폴더를 보면 해당 위성 리소스 언어셋 아래 해당 어셈블리들이 있다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using System.Reflection;

namespace WinForm.Localization
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			// NOTICE: 초기 UICulture 세팅이 Main 함수에서 "en"으로 설정되어 있다.
			// by KIMKIWON\xyz37(김기원) in 2009년 12월 14일 월요일 오전 9:41
			txtLanguageSet.Text = Thread.CurrentThread.CurrentUICulture.ToString();
			//Thread.CurrentThread.CurrentUICulture = new CultureInfo(txtLanguageSet.Text);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Thread.CurrentThread.CurrentUICulture = new CultureInfo(txtLanguageSet.Text);
			LocalizedForm globalizationForm = new LocalizedForm();
			
			globalizationForm.ShowDialog();

			foreach (InputLanguage item in InputLanguage.InstalledInputLanguages)
				Console.WriteLine(item.LayoutName);
		}

		private void OnLanguageSetCheckedChanged(object sender, EventArgs e)
		{
			txtLanguageSet.Text = (sender as RadioButton).Tag as string;
			Thread.CurrentThread.CurrentUICulture = new CultureInfo(txtLanguageSet.Text);
			this.Update();
		}
	}
}
