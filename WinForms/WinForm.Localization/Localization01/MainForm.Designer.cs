﻿namespace WinForm.Localization
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.txtLanguageSet = new System.Windows.Forms.TextBox();
			this.rdkoKr = new System.Windows.Forms.RadioButton();
			this.rdKo = new System.Windows.Forms.RadioButton();
			this.rden = new System.Windows.Forms.RadioButton();
			this.SuspendLayout();
			// 
			// button1
			// 
			resources.ApplyResources(this.button1, "button1");
			this.button1.Name = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label1
			// 
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			// 
			// txtLanguageSet
			// 
			resources.ApplyResources(this.txtLanguageSet, "txtLanguageSet");
			this.txtLanguageSet.Name = "txtLanguageSet";
			// 
			// rdkoKr
			// 
			resources.ApplyResources(this.rdkoKr, "rdkoKr");
			this.rdkoKr.Name = "rdkoKr";
			this.rdkoKr.TabStop = true;
			this.rdkoKr.Tag = "ko-KR";
			this.rdkoKr.UseVisualStyleBackColor = true;
			this.rdkoKr.CheckedChanged += new System.EventHandler(this.OnLanguageSetCheckedChanged);
			// 
			// rdKo
			// 
			resources.ApplyResources(this.rdKo, "rdKo");
			this.rdKo.Name = "rdKo";
			this.rdKo.TabStop = true;
			this.rdKo.Tag = "ko";
			this.rdKo.UseVisualStyleBackColor = true;
			this.rdKo.CheckedChanged += new System.EventHandler(this.OnLanguageSetCheckedChanged);
			// 
			// rden
			// 
			resources.ApplyResources(this.rden, "rden");
			this.rden.Name = "rden";
			this.rden.TabStop = true;
			this.rden.Tag = "en";
			this.rden.UseVisualStyleBackColor = true;
			this.rden.CheckedChanged += new System.EventHandler(this.OnLanguageSetCheckedChanged);
			// 
			// MainForm
			// 
			this.AcceptButton = this.button1;
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.rden);
			this.Controls.Add(this.rdKo);
			this.Controls.Add(this.rdkoKr);
			this.Controls.Add(this.txtLanguageSet);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button1);
			this.Name = "MainForm";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtLanguageSet;
		private System.Windows.Forms.RadioButton rdkoKr;
		private System.Windows.Forms.RadioButton rdKo;
		private System.Windows.Forms.RadioButton rden;
	}
}