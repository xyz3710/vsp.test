﻿namespace NotifyIconTest
{
	partial class NotifyIconTestDriverForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnShowNotifyIconLoop = new System.Windows.Forms.Button();
			this.btnHideNotifyIcon = new System.Windows.Forms.Button();
			this.btnShowNotifyIcon = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnShowNotifyIconLoop
			// 
			this.btnShowNotifyIconLoop.Location = new System.Drawing.Point(63, 60);
			this.btnShowNotifyIconLoop.Name = "btnShowNotifyIconLoop";
			this.btnShowNotifyIconLoop.Size = new System.Drawing.Size(167, 23);
			this.btnShowNotifyIconLoop.TabIndex = 0;
			this.btnShowNotifyIconLoop.Text = "&Show Notify Icon Loop";
			this.btnShowNotifyIconLoop.UseVisualStyleBackColor = true;
			this.btnShowNotifyIconLoop.Click += new System.EventHandler(this.btnShowNotifyIconLoop_Click);
			// 
			// btnHideNotifyIcon
			// 
			this.btnHideNotifyIcon.Location = new System.Drawing.Point(63, 190);
			this.btnHideNotifyIcon.Name = "btnHideNotifyIcon";
			this.btnHideNotifyIcon.Size = new System.Drawing.Size(167, 23);
			this.btnHideNotifyIcon.TabIndex = 1;
			this.btnHideNotifyIcon.Text = "&Hide Notify Icon";
			this.btnHideNotifyIcon.UseVisualStyleBackColor = true;
			this.btnHideNotifyIcon.Click += new System.EventHandler(this.btnHideNotifyIcon_Click);
			// 
			// btnShowNotifyIcon
			// 
			this.btnShowNotifyIcon.Location = new System.Drawing.Point(63, 125);
			this.btnShowNotifyIcon.Name = "btnShowNotifyIcon";
			this.btnShowNotifyIcon.Size = new System.Drawing.Size(167, 23);
			this.btnShowNotifyIcon.TabIndex = 0;
			this.btnShowNotifyIcon.Text = "&Show Notify Icon";
			this.btnShowNotifyIcon.UseVisualStyleBackColor = true;
			this.btnShowNotifyIcon.Click += new System.EventHandler(this.btnShowNotifyIcon_Click_1);
			// 
			// NotifyIconTestDriverForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.btnHideNotifyIcon);
			this.Controls.Add(this.btnShowNotifyIcon);
			this.Controls.Add(this.btnShowNotifyIconLoop);
			this.Name = "NotifyIconTestDriverForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "NotifyIconTest Driver Form";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnShowNotifyIconLoop;
		private System.Windows.Forms.Button btnHideNotifyIcon;
		private System.Windows.Forms.Button btnShowNotifyIcon;

	}
}

