﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NotifyIconTest
{
	public partial class NotifyIconTestForm : Form
	{
		private bool _isHide;

        public NotifyIconTestForm()
		{
			InitializeComponent();
		}

		private bool IsHide
		{
			get
			{
				return _isHide;
			}
			set
			{
				_isHide = value;
			}
		}

		private void NotifyIconTestForm_Shown(object sender, EventArgs e)
		{
			// 최초 hide 작업 없이 System Control 버튼을 눌렀을 경우 종료 된다.
			IsHide = true;
		}

		private void NotifyIconTestForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			e.Cancel = IsHide;
			HideForm();
		}

		private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			if (IsHide == true)
				ShowForm();
			else
				HideForm();

			IsHide = !IsHide;
		}

		private void NotifyIconTestForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
				HideForm();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			QuitProgram();
		}

		private void closeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			QuitProgram();
		}

		private void ShowForm()
		{
			FormVisible(true);
		}

		private void HideForm()
		{
			FormVisible(false);
		}

		private void FormVisible(bool isShow)
		{
			notifyIcon.Visible = !isShow;
			ShowInTaskbar = isShow;
			IsHide = !isShow;

			if (isShow == true)
			{
				Show();
				Activate();
			}
			else
				Hide();
		}

		private void QuitProgram()
		{
			// FormClosing event handler set FormClosingEventArgs.Cancel = false
			IsHide = false;
			Close();
		}
	}
}
