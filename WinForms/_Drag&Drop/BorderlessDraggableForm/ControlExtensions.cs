﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace System.Windows.Forms
{
	public static class ControlExtensions
	{
		/// <summary>
		/// 지정된 컨트롤을 드래그 하여 이동할 수 있도록 합니다.
		/// <remarks>
		/// IButtonControl을 구현한 버튼 클래스의 경우에는 클릭할 경우 해당 컨트롤을 Disabled 시켜서
		/// 이동 후 Enabled 속성을 원래의 상태로 변경하는 트릭을 사용하였습니다.
		/// </remarks>
		/// </summary>
		/// <param name="form">이동하려는 폼(하위 컨트롤 모두를 이동하도록 지정합니다.)</param>
		/// <param name="exceptControlsOnMove">드래그해서 이동하려고 할 때 제외하려는 컨트롤(예를 들어 종료 버튼 등)</param>
		public static void SetMovableControl(this Form form, params Control[] exceptControlsOnMove)
		{
			Rectangle screenRectangle = form.RectangleToScreen(form.ClientRectangle);
			int titleHeight = screenRectangle.Top - form.Top;
			int borderWidth = screenRectangle.Left - form.Left;
			bool draggable = false;
			var dragOffset = System.Drawing.Point.Empty;
			var mouseDownLocation = System.Drawing.Point.Empty;
			var noneClickControls = new Dictionary<Control, bool>();
			KeyValuePair<Control, bool>? noneClickControl = null;
			Control noneClickControlsParent = null;
			Control disabledControl = null;
			//var mouseDownArea = Rectangle.Empty;		// 클릭되는 영역을 검사할 경우 주석을 제거 합니다.

			form.RecursiveAction(x =>
			{
				x.MouseDown += (sender, e) =>
				{
					draggable = true;
					var senderControl = sender as Control;

					if (senderControl is IButtonControl)
					{
						// 이동하는 동안 해당 컨트롤이 클릭되지 않도록 한다.
						noneClickControl = new KeyValuePair<Control, bool>(senderControl, senderControl.Enabled);
						senderControl.Enabled = false;
						disabledControl = senderControl;
						noneClickControlsParent = senderControl.Parent;		// 컨트롤을 Disabled 시키면 클릭 이벤트가 발생하지 않으나 부모 컨트롤로 제어권이 넘이 간다.
						dragOffset = new System.Drawing.Point(e.X + senderControl.Location.X, e.Y + senderControl.Location.Y);
						mouseDownLocation = form.Location;
						//mouseDownArea = control.RectangleToScreen(control.ClientRectangle);
					}
					else
					{
						dragOffset = new System.Drawing.Point(e.X, e.Y);
					}
				};
				x.MouseMove += (sender, e) =>
				{
					if (draggable == true)
					{
						System.Drawing.Point p2s = form.PointToScreen(e.Location);
						form.Location = new System.Drawing.Point(p2s.X - dragOffset.X - borderWidth, p2s.Y - dragOffset.Y - titleHeight);
					}
				};
				x.MouseUp += (sender, e) =>
				{
					draggable = false;

					if (noneClickControlsParent != null && disabledControl != null && noneClickControl != null)
					{
						disabledControl.Enabled = noneClickControl.Value.Value;
						disabledControl.Refresh();

						//if (mouseDownArea.Contains(control.Location))		// 클릭 위치를 영역으로 검사할 경우
						//{
						//	(disabledControl as IButtonControl).PerformClick();
						//}

						if (mouseDownLocation == form.Location)		// 마우스 위치 이동이 없을 경우 IButtonControl의 상태에 따라서 Click 이벤트를 발생 시켜야 한다.
						{
							(disabledControl as IButtonControl).PerformClick();
						}

						disabledControl = null;
						noneClickControl = null;
						noneClickControlsParent = null;
					}
				};
			}, true, exceptControlsOnMove);
		}

		/// <summary>
		/// control의 하위 컨트롤을 탐색하면서 특정한 작업을 합니다.
		/// <remarks>control에는 action을 처리하지 않습니다.</remarks>
		/// </summary>
		/// <param name="control">The control.</param>
		/// <param name="action">The action.</param>
		/// <param name="actionToControl">control에도 action을 적용할지 여부.</param>
		/// <param name="excludeControlList">The exclude control list.</param>
		/// <exception cref="System.ArgumentNullException">control</exception>
		public static void RecursiveAction(
			this Control control,
			Action<Control> action,
			bool actionToControl = true,
			params Control[] excludeControlList)
		{
			if (control == null)
			{
				throw new ArgumentNullException("control");
			}

			var queue = new Queue<System.Windows.Forms.Control.ControlCollection>();

			if (actionToControl == true)
			{
				action(control);
			}

			queue.Enqueue(control.Controls);

			while (queue.Count > 0)
			{
				var controls = queue.Dequeue();

				if (controls == null || controls.Count == 0)
				{
					continue;
				}

				foreach (Control item in controls)
				{
					if (excludeControlList != null)
					{
						if (excludeControlList.SingleOrDefault(expControl => (item == expControl)) != null)
						{
							continue;
						}
					}

					action(item);
					queue.Enqueue(item.Controls);
				}
			}
		}

	}
}
