﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BorderlessDraggable
{
	public partial class BorderlessDraggableForm : Form
	{
		public BorderlessDraggableForm()
		{
			InitializeComponent();

			btnMessage.Click += btnMessage_Click;
			btnMessageDisabled.Click += btnMessage_Click;
			btnInPanel.Click += btnMessage_Click;

			this.SetMovableControl(btnClose, btnInPanel);

			//DisableMouseClicks(btnMessage);
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btnMessage_Click(object sender, EventArgs e)
		{
			var control = sender as Control;

			MessageBox.Show(string.Format("{0} was clicked!!!", control.Text));
			//System.Diagnostics.Debug.WriteLine(string.Format("{0} : {1}", this.Size, this.Location), "BorderlessDraggableForm.btnMessage_Click");
			//System.Diagnostics.Debug.WriteLine(this.ClientRectangle, "BorderlessDraggableForm.btnMessage_Click");
		}

		/// <summary>
		/// Disables the mouse clicks.
		/// </summary>
		/// <param name="excludeControls">마우스 클릭을 제한하지 않을 컨트롤.</param>
		private void DisableMouseClicks(params Control[] excludeControls)
		{
			if (this.Filter == null)
			{
				this.Filter = new MouseClickMessageFilter
				{
					ExcludeControls = excludeControls,
				};

				Application.AddMessageFilter(this.Filter);
			}
		}

		private void EnableMouseClicks()
		{
			if ((this.Filter != null))
			{
				Application.RemoveMessageFilter(this.Filter);
				this.Filter = null;
			}
		}

		private MouseClickMessageFilter Filter;

		public class MouseClickMessageFilter : IMessageFilter
		{
			private const int LButtonDown = 0x201;
			private const int LButtonUp = 0x202;
			private const int LButtonDoubleClick = 0x203;

			/// <summary>
			/// 메시지가 디스패치되기 전에 필터링합니다.
			/// </summary>
			/// <param name="m">디스패치될 메시지입니다. 이 메시지는 수정할 수 없습니다.</param>
			/// <returns>메시지를 필터링하고 디스패치되지 않게 하려면 true이고 다음 필터 또는 컨트롤에 계속 사용하려면 false입니다.</returns>
			public bool PreFilterMessage(ref System.Windows.Forms.Message m)
			{
				switch (m.Msg)
				{
					case LButtonDown:
					case LButtonUp:
					case LButtonDoubleClick:
						if (ExcludeControls != null)
						{
							if (ExcludeControls.Select(x => x.Handle).Contains(m.HWnd) == true)
							{
								return false;
							}
						}

						return true;
				}

				return false;
			}

			/// <summary>
			/// 마우스 클릭을 제한하지 않을 컨트롤을 구하거나 설정합니다.
			/// </summary>
			/// <value>마우스 클릭을 제한하지 않을 컨트롤을 반환합니다.</value>
			public Control[] ExcludeControls
			{
				get;
				set;
			}
		}
	}
}
