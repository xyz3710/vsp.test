﻿namespace BorderlessDraggable
{
	partial class BorderlessDraggableForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnClose = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnInPanel = new System.Windows.Forms.Button();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.btnMessageDisabled = new System.Windows.Forms.Button();
			this.btnMessage = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.btnOnForm = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Location = new System.Drawing.Point(423, 12);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(81, 22);
			this.btnClose.TabIndex = 0;
			this.btnClose.Text = "X";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.btnInPanel);
			this.panel1.Location = new System.Drawing.Point(22, 76);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(155, 51);
			this.panel1.TabIndex = 1;
			// 
			// btnInPanel
			// 
			this.btnInPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnInPanel.Location = new System.Drawing.Point(0, 0);
			this.btnInPanel.Name = "btnInPanel";
			this.btnInPanel.Size = new System.Drawing.Size(153, 49);
			this.btnInPanel.TabIndex = 0;
			this.btnInPanel.Text = "Button in Pannel";
			this.btnInPanel.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.btnMessageDisabled, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnMessage, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.button3, 1, 1);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(204, 68);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(300, 126);
			this.tableLayoutPanel1.TabIndex = 2;
			// 
			// btnMessageDisabled
			// 
			this.btnMessageDisabled.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnMessageDisabled.Enabled = false;
			this.btnMessageDisabled.Location = new System.Drawing.Point(153, 4);
			this.btnMessageDisabled.Name = "btnMessageDisabled";
			this.btnMessageDisabled.Size = new System.Drawing.Size(143, 55);
			this.btnMessageDisabled.TabIndex = 1;
			this.btnMessageDisabled.Text = "&Message Box Diabled\r\nClick event assigned !";
			this.btnMessageDisabled.UseVisualStyleBackColor = true;
			// 
			// btnMessage
			// 
			this.btnMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnMessage.Location = new System.Drawing.Point(4, 4);
			this.btnMessage.Name = "btnMessage";
			this.btnMessage.Size = new System.Drawing.Size(142, 55);
			this.btnMessage.TabIndex = 0;
			this.btnMessage.Text = "&Message Box\r\nClick event assigned !";
			this.btnMessage.UseVisualStyleBackColor = true;
			// 
			// button3
			// 
			this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button3.Location = new System.Drawing.Point(153, 66);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(143, 56);
			this.button3.TabIndex = 0;
			this.button3.Text = "button1";
			this.button3.UseVisualStyleBackColor = true;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.Transparent;
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel2.Location = new System.Drawing.Point(1, 155);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(514, 174);
			this.panel2.TabIndex = 3;
			// 
			// btnOnForm
			// 
			this.btnOnForm.Location = new System.Drawing.Point(204, 12);
			this.btnOnForm.Name = "btnOnForm";
			this.btnOnForm.Size = new System.Drawing.Size(124, 38);
			this.btnOnForm.TabIndex = 4;
			this.btnOnForm.Text = "Button on Form";
			this.btnOnForm.UseVisualStyleBackColor = true;
			// 
			// BorderlessDraggableForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(516, 331);
			this.Controls.Add(this.btnOnForm);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.btnClose);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "BorderlessDraggableForm";
			this.Opacity = 0.7D;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.panel1.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Button btnInPanel;
		private System.Windows.Forms.Button btnMessage;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btnOnForm;
		private System.Windows.Forms.Button btnMessageDisabled;
	}
}

