﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BorderlessDraggable
{
	public class MouseClickSupress : IDisposable
	{
		#region DLL Imports
		[DllImport(KERNEL32_DLL)]
		static extern IntPtr LoadLibrary(string lpFileName);

		[DllImport(USER32_DLL, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
		static extern bool UnhookWindowsHookEx(IntPtr hookHandle);

		[DllImport(USER32_DLL, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
		static extern IntPtr SetWindowsHookEx(int hookID, MouseHookProc callback, IntPtr hInstance, uint threadID);

		[DllImport(USER32_DLL, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
		static extern int CallNextHookEx(IntPtr hookHandle, int nCode, IntPtr wParam, IntPtr lParam);

		[DllImport(KERNEL32_DLL, CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr GetModuleHandle(string lpModuleName);
		#endregion

		#region Constants
		private const string KERNEL32_DLL = "kernel32.dll";
		private const string USER32_DLL = "user32.dll";

		private const int WH_MOUSE_LL = 14;

		private enum MouseMessages
		{
			WM_LBUTTONDOWN = 0x0201,
			WM_LBUTTONUP = 0x0202,
			WM_MOUSEMOVE = 0x0200,
			WM_MOUSEWHEEL = 0x020A,
			WM_RBUTTONDOWN = 0x0204,
			WM_RBUTTONUP = 0x0205
		}
		#endregion

		[StructLayout(LayoutKind.Sequential)]
		private struct MSLLHOOKSTRUCT
		{
			public POINT pt;
			public uint mouseData;
			public uint flags;
			public uint time;
			public IntPtr dwExtraInfo;
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct POINT
		{
			public int x;
			public int y;
		}

		#region Fields
		private delegate int MouseHookProc(int nCode, IntPtr wParam, IntPtr lParam);

		private IntPtr _hookMouseHandle;
		private MouseHookProc _hookMouseProc;
		private bool _hooked;
		#endregion

		private int HookMouseProc(int code, IntPtr wParam, IntPtr lParam)
		{
			if (code >= 0 && MouseMessages.WM_LBUTTONDOWN == (MouseMessages)wParam)
			{
				MSLLHOOKSTRUCT hookStruct = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
				Console.WriteLine("{0}: {1}, {2}", (MouseMessages)wParam, hookStruct.pt.x, hookStruct.pt.y);

				return 1;
			}

			return CallNextHookEx(_hookMouseHandle, code, wParam, lParam);
		}

		/// <summary>
		/// 지정된 키가 눌렸을 때 키보드를 후킹 합니다.
		/// <remarks><see cref="HookedKeys"/>가 지정되면 지정된 키만 후킹합니다.</remarks>
		/// </summary>
		public void SetHook()
		{
			if (_hooked == true)
			{
				return;
			}

			_hookMouseProc = new MouseHookProc(HookMouseProc);

			using (var curProcess = System.Diagnostics.Process.GetCurrentProcess())
			{
				using (var curModule = curProcess.MainModule)
				{
					_hookMouseHandle = SetWindowsHookEx(WH_MOUSE_LL, HookMouseProc, GetModuleHandle(curModule.ModuleName), 0);
				}
			}

			//IntPtr hInstance = LoadLibrary("user32");
			//_hookMouseHandle = SetWindowsHookEx(WH_MOUSE_LL, HookMouseProc, hInstance, 0);
			_hooked = true;
		}

		/// <summary>
		/// 키보드 후킹을 중지합니다.
		/// </summary>
		public void Unhook()
		{
			if (_hooked == false)
			{
				return;
			}

			UnhookWindowsHookEx(_hookMouseHandle);
			_hooked = false;
		}

		#region IDisposable 멤버

		/// <summary>
		/// 관리되지 않는 리소스의 확보, 해제 또는 다시 설정과 관련된 응용 프로그램 정의 작업을 수행합니다.
		/// </summary>
		public void Dispose()
		{
			Unhook();
		}

		#endregion
	}
}
