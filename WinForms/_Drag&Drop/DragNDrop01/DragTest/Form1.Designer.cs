﻿namespace DragTest
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbSource1 = new System.Windows.Forms.Label();
			this.lbTarget = new System.Windows.Forms.Label();
			this.tbSource = new System.Windows.Forms.TextBox();
			this.tbTarget = new System.Windows.Forms.TextBox();
			this.lbSource2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lbSource1
			// 
			this.lbSource1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbSource1.Location = new System.Drawing.Point(31, 40);
			this.lbSource1.Name = "lbSource1";
			this.lbSource1.Size = new System.Drawing.Size(220, 73);
			this.lbSource1.TabIndex = 0;
			this.lbSource1.Text = "Source1";
			this.lbSource1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lbTarget
			// 
			this.lbTarget.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbTarget.Location = new System.Drawing.Point(376, 153);
			this.lbTarget.Name = "lbTarget";
			this.lbTarget.Size = new System.Drawing.Size(220, 73);
			this.lbTarget.TabIndex = 0;
			this.lbTarget.Text = "Target";
			this.lbTarget.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tbSource
			// 
			this.tbSource.Location = new System.Drawing.Point(31, 127);
			this.tbSource.Name = "tbSource";
			this.tbSource.Size = new System.Drawing.Size(220, 24);
			this.tbSource.TabIndex = 1;
			this.tbSource.Text = "Test";
			// 
			// tbTarget
			// 
			this.tbTarget.Location = new System.Drawing.Point(376, 229);
			this.tbTarget.Name = "tbTarget";
			this.tbTarget.Size = new System.Drawing.Size(220, 24);
			this.tbTarget.TabIndex = 1;
			// 
			// lbSource2
			// 
			this.lbSource2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbSource2.Location = new System.Drawing.Point(31, 203);
			this.lbSource2.Name = "lbSource2";
			this.lbSource2.Size = new System.Drawing.Size(220, 73);
			this.lbSource2.TabIndex = 0;
			this.lbSource2.Text = "Source2";
			this.lbSource2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(639, 302);
			this.Controls.Add(this.tbTarget);
			this.Controls.Add(this.tbSource);
			this.Controls.Add(this.lbTarget);
			this.Controls.Add(this.lbSource2);
			this.Controls.Add(this.lbSource1);
			this.Font = new System.Drawing.Font("굴림", 11F);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lbSource1;
		private System.Windows.Forms.Label lbTarget;
		private System.Windows.Forms.TextBox tbSource;
		private System.Windows.Forms.TextBox tbTarget;
		private System.Windows.Forms.Label lbSource2;
	}
}

