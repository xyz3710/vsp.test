﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DragTest
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();

			lbSource1.MouseDown += LbSource_MouseDown;
			lbSource2.MouseDown += LbSource_MouseDown;
			tbSource.MouseDown += LbSource_MouseDown;

			lbTarget.DragDrop += LbTarget_DragDrop;
			lbTarget.DragEnter += LbTarget_DragEnter;
			lbTarget.DragOver += LbTarget_DragOver;

			lbTarget.AllowDrop = true;

			var timer = new System.Timers.Timer(1000);

			timer.Elapsed += (sender, e) =>
			{
				Invoke(new MethodInvoker(() =>
				{
					lbSource1.Text = DateTime.Now.ToString();
				}));
			};
			timer.Start();
		}

		private void LbSource_MouseDown(object sender, MouseEventArgs e)
		{
			if (sender is Label sourceLabel)
			{
				DoDragDrop(sourceLabel, DragDropEffects.All);
			}

			if (sender is TextBox sourceTextBox)
			{
				DoDragDrop(sourceTextBox.Text, DragDropEffects.All);
			}
		}

		private void LbTarget_DragOver(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(typeof(Label)) == true)
			{
				e.Effect = DragDropEffects.Copy;
			}
			else
			{
				e.Effect = DragDropEffects.None;
			}
		}

		private void LbTarget_DragEnter(object sender, DragEventArgs e)
		{
			if ((e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
			{
				e.Effect = DragDropEffects.Copy;
			}
		}

		private void LbTarget_DragDrop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(typeof(Label)) == true)
			{
				if (e.Data.GetData(typeof(Label)) is Label sourceLabel)
				{
					tbTarget.Text = sourceLabel.Text;
				}
			}
		}
	}
}
