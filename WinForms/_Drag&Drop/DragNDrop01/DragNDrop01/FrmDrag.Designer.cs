﻿namespace DragNDrop01
{
	partial class FrmDrag
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblLabel = new System.Windows.Forms.Label();
			this.txtDrag = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// lblLabel
			// 
			this.lblLabel.AllowDrop = true;
			this.lblLabel.AutoSize = true;
			this.lblLabel.Location = new System.Drawing.Point(18, 12);
			this.lblLabel.Name = "lblLabel";
			this.lblLabel.Size = new System.Drawing.Size(97, 12);
			this.lblLabel.TabIndex = 1;
			this.lblLabel.Text = "Draggable Label";
			// 
			// txtDrag
			// 
			this.txtDrag.AllowDrop = true;
			this.txtDrag.Location = new System.Drawing.Point(13, 36);
			this.txtDrag.Multiline = true;
			this.txtDrag.Name = "txtDrag";
			this.txtDrag.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtDrag.Size = new System.Drawing.Size(604, 158);
			this.txtDrag.TabIndex = 2;
			// 
			// FrmDrag
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(628, 213);
			this.Controls.Add(this.txtDrag);
			this.Controls.Add(this.lblLabel);
			this.Name = "FrmDrag";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "frmDrag";
			this.TopMost = true;
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragDropHandler);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragEnterHandler);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblLabel;
		private System.Windows.Forms.TextBox txtDrag;
	}
}

