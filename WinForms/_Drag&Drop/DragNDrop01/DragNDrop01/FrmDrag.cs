﻿/**********************************************************************************************************************/
/*	Domain		:	
/*	Creator		:	SUPERMOBILE\xyz37
/*	Create		:	2007년 7월 31일 화요일 오후 3:27
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	DragEventArgs.AllowedEffect는 clipboard에 정보를 보내주는 source단에서 결정할 수 있다.
/**********************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DragNDrop01
{
	public partial class FrmDrag : Form
	{
		public FrmDrag()
		{
			InitializeComponent();

			txtDrag.AllowDrop = true;
			txtDrag.DragOver += new DragEventHandler(DragOverHandler);
			txtDrag.DragEnter += new DragEventHandler(DragEnterHandler);
			txtDrag.DragDrop += new DragEventHandler(DragDropHandler);

			lblLabel.AllowDrop = true;
			lblLabel.DragOver += new DragEventHandler(DragOverHandler);
			lblLabel.DragEnter += new DragEventHandler(DragEnterHandler);
			lblLabel.DragDrop += new DragEventHandler(DragDropHandler);

			this.AllowDrop = true;
			this.DragOver += new DragEventHandler(DragOverHandler);
			this.DragEnter += new DragEventHandler(DragEnterHandler);
			this.DragDrop += new DragEventHandler(DragDropHandler);

		}

		private void DragDropHandler(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.Text) == true)
			{
				string text = (string)e.Data.GetData(DataFormats.Text);

				if (sender is TextBox)
                	(sender as TextBox).Text += text;

				if (sender is Form)
					(sender as Form).Text = text;

				if (sender is Label)
					(sender as Label).Text = text;
				
			}
		}

		private void DragEnterHandler(object sender, DragEventArgs e)
		{
            
		}
       
		private void DragOverHandler(object sender, DragEventArgs e)
		{
			// e.Effect로 데이터를 실제 clipboard에서 copy & move 시킬 수 있다.
			// 아래에서 e.Effect에 DragDropEffects.Move를 주면
			// 실제 clipboard에서 move action이 일어난다.
			if (e.Data.GetDataPresent(DataFormats.Text) == true)
			{
				if (((e.KeyState & 8) == 8) 
						&& (e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
					e.Effect = DragDropEffects.Copy;
				else if ((e.KeyState & 4) == 4 
						&& (e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
					e.Effect = DragDropEffects.Move;
				else
					e.Effect = DragDropEffects.Copy;
			}
			else
				e.Effect = DragDropEffects.None;

			
		}
	}
}