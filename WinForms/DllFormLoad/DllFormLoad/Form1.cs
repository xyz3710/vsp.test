﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace DllFormLoad
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			Assembly formAssembly = Assembly.LoadFrom(@"D:\VSP\_Test\_Forms\DllFormLoad\DllFormLoad\Forms\F0010.dll");
			Type formType = formAssembly.GetType("iDASiT.Win.Mes.F0010");

			if (formType != null)
			{
				object obj = Activator.CreateInstance(formType);

				obj.GetType().InvokeMember("Text", BindingFlags.SetProperty, null, obj, new object[] { "Test" });
				// BindingFlags.InvokeMethod를 해야 참조 어셈블리도 참조된다.
				object ac = obj.GetType().InvokeMember("Show", BindingFlags.InvokeMethod, null, obj, new object[] { });
			}            
		}
	}
}