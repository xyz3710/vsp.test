﻿// RadioButton, Button의 PerformClick을 이용하여 실행함
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TestWinbar
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void radioButton1_Click(object sender, EventArgs e)
		{
			MessageBox.Show("Radio button1 click");
		}

		private void button1_Click(object sender, EventArgs e)
		{
			MessageBox.Show("Button1 click");
		}

		private void button2_Click(object sender, EventArgs e)
		{
			// Perform Radio Button1
			radioButton1.PerformClick();

			// Perform Button 1
			button1.PerformClick();
		}
	}

	public class MyClass
	{
		public static int Add(int a, int b)
		{
			return a + b;
		}
	}
}