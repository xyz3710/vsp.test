using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Blueman.Notifier;

public class TrayIcon
{
	private NotifyIconEx notifyIcon = new NotifyIconEx();
	private	ContextMenu contextMenu = new ContextMenu();
	private	MenuItem menuClose = new MenuItem();
	private System.IntPtr hIcon = IntPtr.Zero;

	#region External Method

	#region ExtractIconEx
	[DllImport("coredll", SetLastError=true)]
	extern public static System.IntPtr ExtractIconEx(
		string lpszFile,
		int nIconIndex,
		int hiconLarge,
		ref IntPtr hIcon,
		uint nIcons
		);
	#endregion

	#region DestroyIcon
	[DllImport("coredll", SetLastError=true)]
	extern public static System.IntPtr DestroyIcon(
		IntPtr hIcon
		);
	#endregion

	#endregion

	public TrayIcon()
	{
		const string szResuorceName = "BluemanResource.dll";
		string szFullPath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
		string szPath = System.IO.Path.GetDirectoryName(szFullPath) + "\\" + szResuorceName;

		ExtractIconEx(szPath, 0, 0, ref hIcon, 1);

		notifyIcon.IconHandle = hIcon;
		notifyIcon.Visible = true;

		menuClose.Click += new EventHandler(menuClose_Click);
		contextMenu.MenuItems.Add(menuClose);
		//notifyIcon.ContextMenu = contextMenu;
	}

	public static void Main()
	{
		Application.Run(new TrayIcon());
	}

	private void menuClose_Click(object sender, EventArgs e)
	{
		notifyIcon.Dispose();
		contextMenu.Dispose();
		menuClose.Dispose();

		Application.Exit();
	}
}

