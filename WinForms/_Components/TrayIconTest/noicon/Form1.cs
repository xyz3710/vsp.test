using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Resources;

namespace noicon
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;

		private MyClass.NotifyIcon notifyIcon1;

		////////////////////////////////////////////////
		//	API
		////////////////////////////////////////////////
		[DllImport("coredll")]
		extern public static IntPtr ExtractIconEx(
			string lpszFile, 
			int nIconIndex, 
			int hiconLarge, 
			ref IntPtr hIcon, 
			uint nIcons);

		[DllImport("coredll", SetLastError=true)]
		extern public static IntPtr DestroyIcon(
			IntPtr hIcon
			);

		public enum IconState
		{
			bluetoothConn = -3,
			bluetoothOff,
			bluetoothOn
		}
		
		public Form1()
		{
			InitializeComponent();
			notifyIcon1 = new MyClass.NotifyIcon();
			notifyIcon1.DoubleClick += new EventHandler(notifyIcon1_DoubleClick);
			IcIpSet();
		}

		protected override void Dispose( bool disposing )
		{
			base.Dispose( disposing );
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(68, 8);
			this.button1.Text = "Remove";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(68, 36);
			this.button2.Text = "Icon1";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.Add(this.menuItem1);
			this.contextMenu1.MenuItems.Add(this.menuItem2);
			// 
			// menuItem1
			// 
			this.menuItem1.Text = "Click";
			this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Text = "Close";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(20, 112);
			this.textBox1.Multiline = true;
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox1.Size = new System.Drawing.Size(168, 108);
			this.textBox1.Text = "";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(68, 60);
			this.button3.Text = "Icon2";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(68, 84);
			this.button4.Text = "Icon3";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// Form1
			// 
			this.ClientSize = new System.Drawing.Size(206, 258);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
			this.Load += new System.EventHandler(this.Form1_Load);

		}
		#endregion

		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			notifyIcon1.Remove();
		}

		////////////////////////////////////////////////
		//	Method
		////////////////////////////////////////////////
		private void FormIconSet(string ico)
		{
			Assembly assembly = Assembly.GetExecutingAssembly();
			ResourceManager rm = new ResourceManager("noicon.bluetooth", assembly);
			this.Icon = (Icon)rm.GetObject(ico);
		}

		private System.IntPtr icON = IntPtr.Zero;
		private System.IntPtr icOFF = IntPtr.Zero;
		private System.IntPtr icCONN = IntPtr.Zero;
		private ArrayList IntPtrList;

		/// <summary>
		/// 실행할때 ExtractIconEx를 하면 리소스를 계속 잡아 먹는다.
		/// 그래서 핸들만 한번 구한후 핸들로 엑세스 하면 괜찮다.
		/// </summary>
		private void IcIpSet()
		{
			string szPath = @"\Program Files\noicon\IconLibrary.dll";
			ExtractIconEx(szPath, (int)IconState.bluetoothOn, 0, ref icON, 1);
			ExtractIconEx(szPath, (int)IconState.bluetoothOff, 0, ref icOFF, 1);
			ExtractIconEx(szPath, (int)IconState.bluetoothConn, 0, ref icCONN, 1);

			/////////////////////////
			ArrayList na = new ArrayList();
			na.Add(-3);
			na.Add(-2);
			na.Add(-1);

			ArrayList ip = new ArrayList();

			IntPtr ico = IntPtr.Zero;
			for(int i=0; i<na.Count; i++)
			{
				IntPtr ic = IntPtr.Zero;
				ExtractIconEx(szPath, (int)na[i], 0, ref ic, 1);
				ip.Add(ic);
				textBox1.Text += ic.ToString() + "\r\n";
				// 여기서 핸들을 지우게 되면 사용 할 수가 없다.
				// 따라서 폼이 종료될때 지우면 된다.
				//DestroyIcon(ic);
			}

			IntPtrList = ip;
		}

		////////////////////////////////////////////////
		//	Button
		////////////////////////////////////////////////
		private void button2_Click(object sender, System.EventArgs e)
		{
			FormIconSet("ON");
			//if(icON != IntPtr.Zero)
			//	notifyIcon1.Add(icON);			
			notifyIcon1.Add((IntPtr)IntPtrList[2]);
			textBox1.Text = IntPtrList[2].ToString();
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			FormIconSet("OFF");
			//if(icOFF != IntPtr.Zero)
			//	notifyIcon1.Add(icOFF);
			notifyIcon1.Add((IntPtr)IntPtrList[1]);
			textBox1.Text = IntPtrList[1].ToString();
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			FormIconSet("CONN");
			//if(icCONN != IntPtr.Zero)
			//	notifyIcon1.Add(icCONN);
			notifyIcon1.Add((IntPtr)IntPtrList[0]);
			textBox1.Text = IntPtrList[0].ToString();
		}

		////////////////////////////////////////////////
		//	ContextMenu
		////////////////////////////////////////////////
		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Click");
		}
		
		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			DestroyIcon(icON);
			DestroyIcon(icOFF);
			DestroyIcon(icCONN);
			this.Dispose();
		}
		
		////////////////////////////////////////////////
		//	EVENT
		////////////////////////////////////////////////
		private void notifyIcon1_DoubleClick(object sender, EventArgs e)
		{
			// title bar가 없어야 동작..
			this.Focus();
			this.Show();

			// 실제 마우스 포인터 위치
			Point mp = new Point(Control.MousePosition.X, Control.MousePosition.Y);
			contextMenu1.Show(this, new Point(0,0));
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			this.Hide();
			notifyIcon1.Add((IntPtr)IntPtrList[2]);
		}

		private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
			this.Hide();    			
		}
	}
}