using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace MyClass
{
	public class NotifyIcon
	{
		// 마우스 좌표 반환 못함.
		public event EventHandler DoubleClick;
		public event EventHandler MouseDown;
		public event EventHandler MouseUp;

		private bool m_add = false;
		private bool m_doubleclick = false;
		private NotifyIconMW notifyIconMW;
		private int uID = 5000;

		public NotifyIcon()
		{
			notifyIconMW = new NotifyIconMW(this);
			notifyIconMW.uID = uID;
		}

		~NotifyIcon()
		{
			Remove();			
		}

		public void Add(IntPtr hIcon)
		{
			if(!m_add)
			{
				TrayMessage(notifyIconMW.Hwnd, (int)NIFMESSAGE.ADD, (uint)uID, hIcon);
				m_add = true;
			}
			Modify(hIcon);
		}

		public void Remove()
		{
			m_add = false;
			TrayMessage(notifyIconMW.Hwnd, (int)NIFMESSAGE.DELETE, (uint)uID, IntPtr.Zero);
		}

		public void Modify(IntPtr hIcon)
		{			
			TrayMessage(notifyIconMW.Hwnd, (int)NIFMESSAGE.MODIFY, (uint)uID, hIcon);
		}

		private void TrayMessage(IntPtr hwnd, int dwMessage, uint uID, IntPtr hIcon)
		{
			NOTIFYICONDATA notdata = new NOTIFYICONDATA();

			notdata.cbSize = 152;
			notdata.hIcon =  hIcon;
			notdata.hWnd = hwnd;
			notdata.uCallbackMessage = (int)WM.NOTIFY_TRAY;
			notdata.uFlags = (int)NIFDATAFLAGS.MESSAGE | (int)NIFDATAFLAGS.ICON;
			notdata.uID = uID;

			int ret = Shell_NotifyIcon(dwMessage, ref notdata);
		}

		#region NotifyIconMW

		internal class NotifyIconMW : Microsoft.WindowsCE.Forms.MessageWindow
		{
			private int m_uID = 0;
			private NotifyIcon notifyIcon;

			public NotifyIconMW(NotifyIcon notIcon)
			{
				notifyIcon = notIcon;	
			}
			
			public int uID
			{
				set	{ m_uID = value; }
				get { return m_uID; }
			}

			protected override void WndProc(ref Microsoft.WindowsCE.Forms.Message msg)
			{				 
				if(msg.Msg == (int)WM.NOTIFY_TRAY)
				{
					if((int)msg.WParam == m_uID)
					{
						switch((int)msg.LParam)
						{
							case (int)WM.LBUTTONDOWN:
								if (notifyIcon.MouseDown != null)									
									notifyIcon.MouseDown(notifyIcon, null);
								break;							
							case (int)WM.LBUTTONUP:
								if (notifyIcon.MouseUp != null)
									notifyIcon.MouseUp(notifyIcon, null);
								break;							
							case (int)WM.LBUTTONDBLCLK:
								if (notifyIcon.MouseDown != null)
									notifyIcon.MouseDown(notifyIcon, null);
								notifyIcon.m_doubleclick = true;
								if (notifyIcon.DoubleClick != null)
									notifyIcon.DoubleClick(notifyIcon, null);
								break;
							case (int)WM.RBUTTONDOWN:							
								if (notifyIcon.MouseDown != null)
									notifyIcon.MouseDown(notifyIcon, null);
								break;
							case (int)WM.RBUTTONUP:							
								if (notifyIcon.MouseUp != null)
									notifyIcon.MouseUp(notifyIcon, null);
								break;
						}
					}
				}								
			}
		}
		#endregion

		#region API Declarations

		public enum WM
		{
			LBUTTONDOWN		= 0x0201,
			LBUTTONUP		= 0x0202,
			LBUTTONDBLCLK	= 0x0203,
			RBUTTONDOWN		= 0x0204,
			RBUTTONUP		= 0x0205,
			RBUTTONDBLCLK	= 0x0206,
			NOTIFY_TRAY		= 0x004E,
		}

		internal enum NIFMESSAGE
		{
			ADD       =  0x00000000,
			MODIFY    =  0x00000001,
			DELETE    =  0x00000002,
		}

		internal enum NIFDATAFLAGS
		{
			MESSAGE		= 0x00000001,
			ICON		= 0x00000002,
			TOOLTIP		= 0x00000004,
		}

		internal struct NOTIFYICONDATA 
		{ 
			internal int cbSize;
			internal IntPtr hWnd; 
			internal uint uID; 
			internal uint uFlags; 
			internal uint uCallbackMessage; 
			internal IntPtr hIcon; 
			//internal char[] szTip = new char[64]; 
			//internal IntPtr szTip;
		}

		[DllImport("coredll", EntryPoint="Shell_NotifyIcon", SetLastError=true)]
		internal static extern int Shell_NotifyIcon(
			int dwMessage,ref NOTIFYICONDATA pnid);

		#endregion
	}
}