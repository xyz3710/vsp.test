using System;
using System.Runtime.InteropServices;
using NotifyIconEx;

namespace TrayIconTest.Test
{
	#region Blueman state Icon color
	/// <summary>
	/// Order to extract Icons from Resource dll
	/// </summary>
	public enum BluemanState : int
	{
		/// <summary>
		/// State that Blueman connected other devices(Green Icon)
		/// </summary>
		BluetoothConn	= 0,
		/// <summary>
		/// State that Blueman radio Off(Gray Icon)
		/// </summary>
		BluetoothOff,
		/// <summary>
		/// State that Blueman radio On(Blue Icon)
		/// </summary>
		BluetoothOn,
		/// <summary>
		/// Blueman State Count
		/// </summary>
		BluemanStateCount
	}
	#endregion

	/// <summary>
	/// Specifies a component that creates an icon in the status area
	/// </summary>
	public class NotifyIcon : NotifyIconEx.NotifyIconEx
	{
		public void Dispose()
		{
			base.Dispose (true);
		}

		public void ChangeIcon(BluemanState state)
		{
			const	string	szResuorceName = "BluemanResource.dll";
			System.IntPtr	hIcon = System.IntPtr.Zero;
			string	szFullPath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
			string	szPath = System.IO.Path.GetDirectoryName(szFullPath) + "\\" + szResuorceName;
			
			ExtractIconEx(szPath, (int)state, 0, ref hIcon, 1);

			base.ChangeIcon(hIcon);

			DestroyIcon(hIcon);
		}

		#region External Method

		#region ExtractIconEx
		[DllImport("coredll", SetLastError=true)]
		extern public static System.IntPtr ExtractIconEx(
			string lpszFile, 
			int nIconIndex, 
			int hiconLarge, 
			ref IntPtr hIcon, 
			uint nIcons
			); 
		#endregion
	
		#region DestroyIcon
		[DllImport("coredll", SetLastError=true)]
		extern public static System.IntPtr DestroyIcon(
			IntPtr hIcon
			);
		#endregion

		#endregion
	}
}
