using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;

namespace TrayIconTest.Test
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.PictureBox pbState;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Resources.ResourceManager res;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem6;
		private	System.IntPtr	hIcon = System.IntPtr.Zero;
		private System.Windows.Forms.MenuItem mnuBluetoothOn;
		private NotifyIcon notifyIcon1 = new NotifyIcon();

		[DllImport("coredll", SetLastError=true)]
		extern public static IntPtr ExtractIconEx(
			string lpszFile, 
			int nIconIndex, 
			int hiconLarge, 
			ref IntPtr hIcon, 
			uint nIcons); 
		
		[DllImport("coredll", SetLastError=true)]
		extern public static IntPtr DestroyIcon(IntPtr hIcon);

		[DllImport("coredll.dll", EntryPoint="GetLastError", SetLastError=true)]
		private static extern uint GetLastError();

		private const string	BTOn = "Turn Bluetooth On";
		private const string	BTOff = "Turn Bluetooth Off";

		private System.IntPtr[]	iconHandle = new IntPtr[3];
		
		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			
			notifyIcon1.DoubleClick += new EventHandler(notifyIcon1_DoubleClick);

			InitializeComponent();
			mnuBluetoothOn.Text = BTOff;

			// namespace의 가장 앞 부분만 적어준다
			res = new System.Resources.ResourceManager("TrayIconTest.StateIcon", 
				System.Reflection.Assembly.GetExecutingAssembly());

		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.pbState = new System.Windows.Forms.PictureBox();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.mnuBluetoothOn = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(11, 120);
			this.button1.Size = new System.Drawing.Size(64, 24);
			this.button1.Text = "TrayOn";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(119, 120);
			this.button2.Size = new System.Drawing.Size(64, 24);
			this.button2.Text = "TrayOff";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// imageList1
			// 
			this.imageList1.Images.Add(((System.Drawing.Image)(resources.GetObject("resource"))));
			this.imageList1.Images.Add(((System.Drawing.Image)(resources.GetObject("resource1"))));
			this.imageList1.Images.Add(((System.Drawing.Image)(resources.GetObject("resource2"))));
			this.imageList1.ImageSize = new System.Drawing.Size(32, 32);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(11, 152);
			this.button3.Size = new System.Drawing.Size(41, 24);
			this.button3.Text = "On";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(119, 152);
			this.button4.Size = new System.Drawing.Size(64, 24);
			this.button4.Text = "Connect";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(65, 152);
			this.button5.Size = new System.Drawing.Size(41, 24);
			this.button5.Text = "Off";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// pbState
			// 
			this.pbState.Location = new System.Drawing.Point(81, 20);
			this.pbState.Size = new System.Drawing.Size(32, 32);
			this.pbState.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.Add(this.mnuBluetoothOn);
			this.contextMenu1.MenuItems.Add(this.menuItem2);
			this.contextMenu1.MenuItems.Add(this.menuItem3);
			this.contextMenu1.MenuItems.Add(this.menuItem4);
			this.contextMenu1.MenuItems.Add(this.menuItem5);
			this.contextMenu1.MenuItems.Add(this.menuItem6);
			// 
			// mnuBluetoothOn
			// 
			this.mnuBluetoothOn.Text = "Bluetooth On";
			this.mnuBluetoothOn.Click += new System.EventHandler(this.mnuBluetoothOn_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Text = "-";
			// 
			// menuItem3
			// 
			this.menuItem3.Text = "Blueman Manager";
			this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
			// 
			// menuItem4
			// 
			this.menuItem4.Text = "Blueman Setting";
			this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Text = "-";
			// 
			// menuItem6
			// 
			this.menuItem6.Text = "Exit";
			this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			// 
			// Form1
			// 
			this.ClientSize = new System.Drawing.Size(198, 222);
			this.Controls.Add(this.pbState);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button5);
			this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Text = "Form1";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
			this.Load += new System.EventHandler(this.Form1_Load);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>


		public void changeNotifyIcon(BluemanState state)
		{
//			const	string	szResuorceName = "BluemanResource.dll";
//			string	szFullPath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
//			string	szPath = System.IO.Path.GetDirectoryName(szFullPath) + "\\" + szResuorceName;
//			
//			ExtractIconEx(szPath, (int)state, 0, ref hIcon, 1);
//			notifyIcon1.ChangeIcon(hIcon);
//			DestroyIcon(hIcon);
//			this.pbState.Image = (Image)res.GetObject(state.ToString());

			notifyIcon1.ChangeIcon(iconHandle[(int)state]);
			this.Icon = (Icon)res.GetObject(state.ToString());
		}

		private Size getContextSize(System.Windows.Forms.ContextMenu cm)
		{
//			System.Drawing.Bitmap	bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
//			System.Drawing.Graphics	gx = System.Drawing.Graphics.FromImage(bmp);
//			const	int	MARGIN_LEFT = 26*2;
//			const	int	MARGIN_TOP = 9*2;
//			int		maxStringLength = 0;
//			int		index = -1;
//			Size	ret = new Size(0, 0);
//
//			for (int i = 0; i < cm.MenuItems.Count; i++)
//			{
//				if (maxStringLength <= cm.MenuItems[i].Text.Length)
//				{
//					maxStringLength = cm.MenuItems[i].Text.Length;
//					index = i;
//				}
//			}
//
//			SizeF	size = gx.MeasureString(cm.MenuItems[index].Text, this.Font);
//
//			ret.Width = (int)size.Width + MARGIN_LEFT;
//			ret.Height = (int)size.Height * cm.MenuItems.Count + MARGIN_TOP;
//			
//			return ret;
			System.Drawing.Bitmap	bmp = new Bitmap(1, 1);
			System.Drawing.Graphics	gx = System.Drawing.Graphics.FromImage(bmp);
			const	int	MARGIN_LEFT = 17*2;
			const	int	MARGIN_TOP = 8*2;
			int		maxStringLength = 0;
			int		index = -1;
			Size	ret = new Size(0, 0);

			for (int i = 0; i < cm.MenuItems.Count; i++)
			{
				if (maxStringLength <= cm.MenuItems[i].Text.Length)
				{
					maxStringLength = cm.MenuItems[i].Text.Length;
					index = i;
				}
			}

			SizeF	size = gx.MeasureString(cm.MenuItems[index].Text, this.Font);

			ret.Width = (int)size.Width + MARGIN_LEFT;
			ret.Height = (int)size.Height * cm.MenuItems.Count + MARGIN_TOP;
			
			return ret;

		}

		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			changeNotifyIcon(BluemanState.BluetoothOn);
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			changeNotifyIcon(BluemanState.BluetoothOff);
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			changeNotifyIcon(BluemanState.BluetoothConn);
		}

		#region context menu

		private void mnuBluetoothOn_Click(object sender, System.EventArgs e)
		{
			if (mnuBluetoothOn.Text.Equals(BTOff))
			{
				mnuBluetoothOn.Text = BTOn;
				this.notifyIcon1.ChangeIcon(BluemanState.BluetoothOff);
//				changeNotifyIcon(BluemanState.BluetoothOff);
			}
			else
			{
				mnuBluetoothOn.Text = BTOff;
				this.notifyIcon1.ChangeIcon(BluemanState.BluetoothOn);
//				changeNotifyIcon(BluemanState.BluetoothOn);
			}
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			this.Text = "Blueman Manager";
		}

		private void menuItem4_Click(object sender, System.EventArgs e)
		{
			this.Text = "Blueman Setting";
		}

		private void menuItem6_Click(object sender, System.EventArgs e)
		{
			notifyIcon1.Dispose();
			
			for (int i = 0; i < iconHandle.Length; i++)
				DestroyIcon(iconHandle[i]);

			this.Dispose();
		}

		private void notifyIcon1_DoubleClick(object sender, System.EventArgs e)
		{
			int		posX = Control.MousePosition.X;
			System.Drawing.Point pos = new Point(0, 0);

			Size	cmSize = getContextSize(contextMenu1);
			
			pos.X = posX - (cmSize.Width/2);
			pos.Y = Screen.PrimaryScreen.WorkingArea.Height - cmSize.Height;

//			this.Location = pos;
//			this.ClientSize = cmSize;
			contextMenu1.Show(this, new Point(0, 0));
		}
		#endregion

		#region Imported Lib
/*
		[DllImport("coredll", SetLastError=true)]
		extern public static System.IntPtr LoadIcon(
			System.IntPtr hInstance, 
			string lpIconName
			); 

		[DllImport("coredll", SetLastError=true)]
		extern public static System.IntPtr ExtractIcon(
			System.IntPtr	hInst,
			string			lpszExeFileName,
			uint			nIconIndex
		);

		[DllImport("coredll.dll", SetLastError=true)]
		extern public static System.IntPtr GetModuleHandle(
			string lpModuleName
		);

		[DllImport("coredll", SetLastError=true)]
		extern public static IntPtr LoadImage(
			IntPtr hinst,
			string lpszName, 
			LoadImgType uType, 
			int cxDesired, 
			int cyDesired, 
			uint fuLoad 
			);

		public enum LoadImgType 
		{
			IMAGE_BITMAP = 0,
			IMAGE_ICON,
			IMAGE_CURSOR
		}
*/
		#endregion
		
		private void button1_Click(object sender, System.EventArgs e)
		{
			notifyIcon1.Visible = true;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			notifyIcon1.Visible = false;
		}

		private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
			this.Visible = false;
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			const	string	szResuorceName = "BluemanResource.dll";
			string	szFullPath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
			string	szPath = System.IO.Path.GetDirectoryName(szFullPath) + "\\" + szResuorceName;
			
			for (int i = 0; i < (int)BluemanState.BluemanStateCount; i++)
			{
				// form을 종료할 때 리소스를 삭제한다.
				ExtractIconEx(szPath, (i-(int)BluemanState.BluemanStateCount), 0, ref hIcon, 1);

				iconHandle[i] = hIcon;
			}

			notifyIcon1.Visible = true;
			this.Visible = false;

			notifyIcon1.Click += new EventHandler(notifyIcon1_Click);
		}

		private void notifyIcon1_Click(object sender, EventArgs e)
		{
			this.Focus();
			this.Visible = true;
			this.WindowState = FormWindowState.Maximized;
		}
	}
}
