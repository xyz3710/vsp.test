﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

namespace GetSystemIcon
{
	public partial class Form1 : Form
	{
		private string curDir;

		public Form1()
		{
			InitializeComponent();

			curDir = @"\";
			print_Directory_Info(new DirectoryInfo(curDir));
		}

		private void move_Directory()
		{
			DirectoryInfo dir;
			string selectedItem = ltvDir.FocusedItem.Text;

			if (selectedItem == "..")
			{
				char[] trim = { '\\' };
				curDir = curDir.TrimEnd(trim);
				dir = new DirectoryInfo(curDir).Parent;
				curDir = dir.FullName + "\\";
			}
			else
			{
				dir = new DirectoryInfo(curDir + selectedItem);
				if (dir.Exists)
					curDir += selectedItem + "\\";
				else
					return;
			}
			print_Directory_Info(new DirectoryInfo(curDir));
		}

		private void print_Directory_Info(DirectoryInfo dir)
		{
			txtCurDir.Text = curDir;

			int imgCount = 0;
			imgList.Images.Clear();
			ltvDir.Items.Clear();

			if (dir.Parent != null)
			{
				imgList.Images.Add(ExtractIcon.GetIcon(".", true, false));
				ltvDir.Items.Add(new ListViewItem(new string[] { "..", "", "" }));
			}

			DirectoryInfo[] sub = dir.GetDirectories();
			foreach (DirectoryInfo d in sub)
			{
				imgList.Images.Add(ExtractIcon.GetIcon(d.FullName, true, false));
				ltvDir.Items.Add(new ListViewItem(new string[] { d.Name, "", d.LastWriteTime.ToString() }));
			}

			FileInfo[] files = dir.GetFiles();
			foreach (FileInfo f in files)
			{
				imgList.Images.Add(ExtractIcon.GetIcon(f.FullName, true, true));
				ltvDir.Items.Add(
					new ListViewItem(new string[] { 
						f.Name, 
						get_File_Size(f.Length), 
						f.LastWriteTime.ToString()}));
			}
		}

		private string get_File_Size(string f_Length)
		{
			if (f_Length == "")
				return "";
			else
				return get_File_Size(Convert.ToInt64(f_Length));
		}

		private string get_File_Size(long f_Length)
		{
			if (f_Length < 1024)
				return f_Length.ToString() + "B";
			else if (f_Length < 1024 * 1024)
				return (f_Length / 1024) + "KB";
			else if (f_Length < 1024 * 1024 * 1024)
				return (int)(f_Length / 1024 / 1024) + "MB";
			else
				return "";
		}

		private void ltvDir_ItemActivate(object sender, EventArgs e)
		{
			move_Directory();
		}
	}

	public class ExtractIcon
	{
		[DllImport("ceshell.dll", EntryPoint="SHGetFileInfo", SetLastError=true)]
		private static extern int SHGetFileInfo(
			string pszPath, 
			uint dwFileAttributes,
			out SHFILEINFO psfi, 
			uint cbfileInfo,
			SHGFI uFlags);

		[StructLayout(LayoutKind.Sequential)]
		private struct SHFILEINFO
		{
			public SHFILEINFO(bool b)
			{
				hIcon=IntPtr.Zero;
				iIcon=0;
				dwAttributes=0;
				szDisplayName="";
				szTypeName="";
			}
			public IntPtr hIcon;
			public int iIcon;
			public uint dwAttributes;
			[MarshalAs(UnmanagedType.LPStr, SizeConst=260)]
			public string szDisplayName;
			[MarshalAs(UnmanagedType.LPStr, SizeConst=80)]
			public string szTypeName;
		};

		private ExtractIcon()
		{
		}

		private enum SHGFI
		{
			SmallIcon=0x00000001,
			OpenIcon=0x00000002,
			LargeIcon=0x00000000,
			Icon=0x00000100,
			DisplayName=0x00000200,
			Typename=0x00000400,
			SysIconIndex=0x00004000,
			LinkOverlay=0x00008000,
			UseFileAttributes=0x00000010
		}

		/// <summary>
		/// Get the associated Icon for a file or application, this method always returns
		/// an icon. If the strPath is invalid or there is no idonc the default icon is returned
		/// </summary>

		/// <param name="strPath">full path to the file or directory</param>
		/// <param name="bSmall">if true, the 16x16 icon is returned otherwise the 32x32</param>
		/// <param name="bOpen">if true, and strPath is a folder, returns the 'open' icon rather than the 'closed'</param>
		/// <returns></returns>
		public static Icon GetIcon(string strPath, bool bSmall, bool bOpen)
		{
			SHFILEINFO info = new SHFILEINFO(true);
			int cbFileInfo = 352;		// Marshal.SizeOf(info);
			SHGFI flags;
			
			if (bSmall == true)
				flags = SHGFI.Icon|SHGFI.SmallIcon;
			else
				flags = SHGFI.Icon|SHGFI.LargeIcon;
			
			if (bOpen == true)
				flags = flags | SHGFI.OpenIcon;

			SHGetFileInfo(strPath, 256, out info, (uint)cbFileInfo, flags);

			return Icon.FromHandle(info.hIcon);
		}
	}
}