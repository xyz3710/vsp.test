﻿namespace GetSystemIcon
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtCurDir = new System.Windows.Forms.TextBox();
			this.ltvDir = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.imgList = new System.Windows.Forms.ImageList();
			this.SuspendLayout();
			// 
			// txtCurDir
			// 
			this.txtCurDir.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtCurDir.Location = new System.Drawing.Point(14, 12);
			this.txtCurDir.Name = "txtCurDir";
			this.txtCurDir.Size = new System.Drawing.Size(611, 23);
			this.txtCurDir.TabIndex = 0;
			// 
			// ltvDir
			// 
			this.ltvDir.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ltvDir.Columns.Add(this.columnHeader1);
			this.ltvDir.Columns.Add(this.columnHeader2);
			this.ltvDir.Columns.Add(this.columnHeader3);
			this.ltvDir.Location = new System.Drawing.Point(14, 41);
			this.ltvDir.Name = "ltvDir";
			this.ltvDir.Size = new System.Drawing.Size(611, 402);
			this.ltvDir.TabIndex = 1;
			this.ltvDir.View = System.Windows.Forms.View.Details;
			this.ltvDir.ItemActivate += new System.EventHandler(this.ltvDir_ItemActivate);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Name";
			this.columnHeader1.Width = 311;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Size";
			this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.columnHeader2.Width = 125;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Date";
			this.columnHeader3.Width = 169;
			// 
			// imgList
			// 
			this.imgList.ImageSize = new System.Drawing.Size(16, 16);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(638, 455);
			this.Controls.Add(this.ltvDir);
			this.Controls.Add(this.txtCurDir);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox txtCurDir;
		private System.Windows.Forms.ListView ltvDir;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ImageList imgList;
	}
}

