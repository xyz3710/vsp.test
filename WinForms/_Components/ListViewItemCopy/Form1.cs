using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;

namespace SmartDeviceApplication3
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ColumnHeader columnHeaderName;
		private System.Windows.Forms.ColumnHeader columnHeaderAddress;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.Button btnPaste;
		private System.Windows.Forms.ListView lvDevice;
		public System.Array arItems;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			base.Dispose( disposing );
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem();
			System.Windows.Forms.ListViewItem.ListViewSubItem listViewSubItem1 = new System.Windows.Forms.ListViewItem.ListViewSubItem();
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem();
			System.Windows.Forms.ListViewItem.ListViewSubItem listViewSubItem2 = new System.Windows.Forms.ListViewItem.ListViewSubItem();
			this.lvDevice = new System.Windows.Forms.ListView();
			this.columnHeaderName = new System.Windows.Forms.ColumnHeader();
			this.columnHeaderAddress = new System.Windows.Forms.ColumnHeader();
			this.btnClear = new System.Windows.Forms.Button();
			this.btnPaste = new System.Windows.Forms.Button();
			// 
			// lvDevice
			// 
			this.lvDevice.Columns.Add(this.columnHeaderName);
			this.lvDevice.Columns.Add(this.columnHeaderAddress);
			this.lvDevice.FullRowSelect = true;
			listViewItem1.ImageIndex = 0;
			listViewSubItem1.Text = "super addr";
			listViewItem1.SubItems.Add(listViewSubItem1);
			listViewItem1.Text = "Serial port Super";
			listViewItem2.ImageIndex = 0;
			listViewSubItem2.Text = "dell addr";
			listViewItem2.SubItems.Add(listViewSubItem2);
			listViewItem2.Text = "Dell Axim";
			this.lvDevice.Items.Add(listViewItem1);
			this.lvDevice.Items.Add(listViewItem2);
			this.lvDevice.Location = new System.Drawing.Point(1, 2);
			this.lvDevice.Size = new System.Drawing.Size(232, 166);
			this.lvDevice.View = System.Windows.Forms.View.SmallIcon;
			// 
			// columnHeaderName
			// 
			this.columnHeaderName.Text = "Name";
			this.columnHeaderName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.columnHeaderName.Width = 112;
			// 
			// columnHeaderAddress
			// 
			this.columnHeaderAddress.Text = "Address";
			this.columnHeaderAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.columnHeaderAddress.Width = 120;
			// 
			// btnClear
			// 
			this.btnClear.Location = new System.Drawing.Point(33, 176);
			this.btnClear.Text = "clear";
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// btnPaste
			// 
			this.btnPaste.Location = new System.Drawing.Point(129, 176);
			this.btnPaste.Text = "paste";
			this.btnPaste.Click += new System.EventHandler(this.btnPaste_Click);
			// 
			// Form1
			// 
			this.ClientSize = new System.Drawing.Size(234, 199);
			this.Controls.Add(this.btnPaste);
			this.Controls.Add(this.btnClear);
			this.Controls.Add(this.lvDevice);
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>

		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void btnClear_Click(object sender, System.EventArgs e)
		{
			arItems = Array.CreateInstance(typeof(ListViewItem), lvDevice.Items.Count);

			for (int i = 0; i < arItems.Length; i++)
				arItems.SetValue(lvDevice.Items[i], i);
			
			lvDevice.Items.Clear();
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
		}

		private void btnPaste_Click(object sender, System.EventArgs e)
		{
			ListViewItem[] lvi = (ListViewItem[])arItems;

			for (int i = 0; i < arItems.Length; i++)
			{
				lvDevice.Items.Add(lvi[i]);
			}
		}
	}
}
