using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;

namespace t2
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ListView lv;
		private System.Windows.Forms.ImageList imageList2;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.Button button1;
	
		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			base.Dispose( disposing );
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem();
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem();
			System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem();
			System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem();
			System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem();
			this.imageList1 = new System.Windows.Forms.ImageList();
			this.lv = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.imageList2 = new System.Windows.Forms.ImageList();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			// 
			// imageList1
			// 
			this.imageList1.Images.Add(((System.Drawing.Image)(resources.GetObject("resource"))));
			this.imageList1.Images.Add(((System.Drawing.Image)(resources.GetObject("resource1"))));
			this.imageList1.Images.Add(((System.Drawing.Image)(resources.GetObject("resource2"))));
			this.imageList1.Images.Add(((System.Drawing.Image)(resources.GetObject("resource3"))));
			this.imageList1.Images.Add(((System.Drawing.Image)(resources.GetObject("resource4"))));
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			// 
			// lv
			// 
			this.lv.Columns.Add(this.columnHeader1);
			this.lv.Columns.Add(this.columnHeader2);
			this.lv.Columns.Add(this.columnHeader3);
			this.lv.FullRowSelect = true;
			listViewItem1.ImageIndex = 0;
			listViewItem1.Text = "1";
			listViewItem2.ImageIndex = 1;
			listViewItem2.Text = "2";
			listViewItem3.ImageIndex = 2;
			listViewItem3.Text = "3";
			listViewItem4.ImageIndex = 3;
			listViewItem4.Text = "4";
			listViewItem5.ImageIndex = 4;
			listViewItem5.Text = "5";
			this.lv.Items.Add(listViewItem1);
			this.lv.Items.Add(listViewItem2);
			this.lv.Items.Add(listViewItem3);
			this.lv.Items.Add(listViewItem4);
			this.lv.Items.Add(listViewItem5);
			this.lv.LargeImageList = this.imageList2;
			this.lv.Location = new System.Drawing.Point(4, 0);
			this.lv.Size = new System.Drawing.Size(228, 204);
			this.lv.SmallImageList = this.imageList1;
			this.lv.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lv_MouseDown);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "ColumnHeader";
			this.columnHeader1.Width = 60;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "ColumnHeader";
			this.columnHeader2.Width = 60;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "ColumnHeader";
			this.columnHeader3.Width = 60;
			// 
			// imageList2
			// 
			this.imageList2.Images.Add(((System.Drawing.Image)(resources.GetObject("resource5"))));
			this.imageList2.Images.Add(((System.Drawing.Image)(resources.GetObject("resource6"))));
			this.imageList2.Images.Add(((System.Drawing.Image)(resources.GetObject("resource7"))));
			this.imageList2.Images.Add(((System.Drawing.Image)(resources.GetObject("resource8"))));
			this.imageList2.Images.Add(((System.Drawing.Image)(resources.GetObject("resource9"))));
			this.imageList2.ImageSize = new System.Drawing.Size(32, 32);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(40, 224);
			this.button1.Size = new System.Drawing.Size(28, 20);
			this.button1.Text = "l";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(80, 224);
			this.button2.Size = new System.Drawing.Size(28, 20);
			this.button2.Text = "s";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(120, 224);
			this.button3.Size = new System.Drawing.Size(28, 20);
			this.button3.Text = "d";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(160, 224);
			this.button4.Size = new System.Drawing.Size(28, 20);
			this.button4.Text = "L";
			// 
			// Form1
			// 
			this.ClientSize = new System.Drawing.Size(238, 263);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.lv);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button4);
			this.Text = "Form1";
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>

		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			lv.View = View.List;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			lv.View = View.SmallIcon;
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			lv.View = View.Details;
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			lv.View = View.LargeIcon;
		}

		private void lv_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			MessageBox.Show("test");
		}

		private void Form1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			MessageBox.Show("test");
		}

		public class ClickableLabel : System.Windows.Forms.Label
		{
			
		}
	}
}
