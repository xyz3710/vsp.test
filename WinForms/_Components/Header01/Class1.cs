using System;
using System.Collections;

namespace TestHeader01
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		public static void PrintCode(IEnumerable iList)
		{
			foreach (int s in iList)
				Console.WriteLine("{0}", s);
/*
			IEnumerator	iEnum = iList.GetEnumerator();

			while (iEnum.MoveNext())
				Console.WriteLine("{0}", iEnum.Current);
*/				
		}
		
		[STAThread]
		static void Main(string[] args)
		{
			byte[]		Header = new byte[]{1, 82, 30};
			byte[]		Data = new byte[]{57, 1, 67, 97, 110, 48, 73, 47, 47, 110, 1, 82, 30, 69, 65, 96, 86, 90, 91, 82, 49, 1, 82, 30, 110, 54, 42};
			ArrayList	buffer = new ArrayList();
			int			cntHeader = 0;
			int			received;
			int			cnt = 0;

			while (cnt < Data.Length)
			{
				received = Data[cnt++];

				if (received == -1)
					continue;

				// 입력된 값이 SOH면 계속 값을 읽어와서 header의 각 byte와 일치 하면
				// 계속 값을 읽어와서 header와 같으면(Header.Length == cntHeader+1)
				// 데이터를 출력하고 clear 시킨다.
				if (received == Header[cntHeader])
				{
					if (Header.Length <= cntHeader+1)
					{
						PrintCode(buffer);
						buffer.Clear();
						cntHeader = 0;

						continue;
					}
					else
					{
						cntHeader++;

						continue;
					}
				}
				else	// 중간에 header의 일부만 있었을 경우 cntHeader를 리셋한다
					cntHeader = 0;

				// Header를 count 하기 시작하면 다시 0이 될때까지는 값을 저장해서는 안된다.(header 부분이므로)
				if (cntHeader > 0)
					continue;

//				buffer.Add(received);

				// 입력된값이 readable한 값이면 buffer에 저장한다
				if ((received >= 'A' && received <= 'Z') 
					|| (received >= '0' && received <= '9'))
				{
					buffer.Add(received);
				}
				
			}	// while

			PrintCode(buffer);
			buffer.Clear();
			cntHeader = 0;

		}
	}
}
