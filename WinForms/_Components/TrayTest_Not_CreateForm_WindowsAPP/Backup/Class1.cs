using System;
using System.Drawing;
using System.Windows.Forms;

public class TrayIcon
{
	private	NotifyIcon notifyIcon = new NotifyIcon();
	private	ContextMenu contextMenu = new ContextMenu();
	private	MenuItem menuClose = new MenuItem("�� ��");

	public TrayIcon()
	{
//		notifyIcon = new NotifyIcon();
//		contextMenu = new ContextMenu();
//		menuClose = new MenuItem("�� ��");

		notifyIcon.Icon = new Icon(@"D:\VSP\Blueman\BluemanCommon\Resources\Bluetooth.ico");
		notifyIcon.Visible = true;

		menuClose.Click += new EventHandler(menuClose_Click);
		contextMenu.MenuItems.Add(menuClose);
		notifyIcon.ContextMenu = contextMenu;
	}

	public static void Main()
	{
		new TrayIcon();
		Application.Run();
	}

	private void menuClose_Click(object sender, EventArgs e)
	{
		notifyIcon.Dispose();
		contextMenu.Dispose();
		menuClose.Dispose();

		Application.Exit();
	}
}

