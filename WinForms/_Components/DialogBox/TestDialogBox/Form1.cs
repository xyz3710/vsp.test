﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;

namespace TestDialogBox
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Soft Input Pannel On/Off control
		/// </summary>
		/// <param name="control">true is on, false is off</param>
		[DllImport("coredll.dll", EntryPoint="CreateFileW", SetLastError=true)]
		public extern static void SipShowIM(SIPShow control);

		private void btnGetPath_Click(object sender, EventArgs e)
		{
			const string InitPath = @"\My Documents\FTP";

			DirectoryInfo di = Directory.CreateDirectory(InitPath);

			if (di.Exists == true)
			{
				saveFileDialog.FileName = "Select Folder";
				saveFileDialog.Filter = "";
				saveFileDialog.InitialDirectory = InitPath;

				SipShowIM(SIPShow.Off);
				DialogResult dr = saveFileDialog.ShowDialog();

				if (dr == DialogResult.OK)
				{
					string path = saveFileDialog.FileName;

					path = Path.GetDirectoryName(path);

					txtBoxPath.Text = path;
				}
			}
		}
	}

	public enum SIPShow : uint
	{
		/// <summary>
		/// SIP On.
		/// </summary>
		On=0x0001,
		/// <summary>
		/// SIP Off
		/// </summary>
		Off=0x000,
	}

}