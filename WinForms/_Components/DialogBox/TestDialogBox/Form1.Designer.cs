﻿namespace TestDialogBox
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.btnGetPath = new System.Windows.Forms.Button();
			this.txtBoxPath = new System.Windows.Forms.TextBox();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.SuspendLayout();
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "\"\"";
			this.openFileDialog1.InitialDirectory = "\\Windows";
			// 
			// btnGetPath
			// 
			this.btnGetPath.Location = new System.Drawing.Point(23, 24);
			this.btnGetPath.Name = "btnGetPath";
			this.btnGetPath.Size = new System.Drawing.Size(72, 20);
			this.btnGetPath.TabIndex = 0;
			this.btnGetPath.Text = "Get path";
			this.btnGetPath.Click += new System.EventHandler(this.btnGetPath_Click);
			// 
			// txtBoxPath
			// 
			this.txtBoxPath.Location = new System.Drawing.Point(3, 243);
			this.txtBoxPath.Name = "txtBoxPath";
			this.txtBoxPath.Size = new System.Drawing.Size(236, 23);
			this.txtBoxPath.TabIndex = 1;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(242, 269);
			this.Controls.Add(this.txtBoxPath);
			this.Controls.Add(this.btnGetPath);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button btnGetPath;
		private System.Windows.Forms.TextBox txtBoxPath;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
	}
}

