using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;
using CustomControlDemo;

namespace ListViewTest
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private	lv lv1;
		private OpenNETCF.Windows.Forms.ButtonEx buttonEx1;
		private ClickLabel clbl;
		private System.Windows.Forms.Button lbl;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem();
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem();
			System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem();
			this.lv1 = new lv();
			// 
			// lv1
			// 
			listViewItem1.Text = "1";
			listViewItem2.Text = "2";
			listViewItem3.Text = "3";
			this.lv1.Items.Add(listViewItem1);
			this.lv1.Items.Add(listViewItem2);
			this.lv1.Items.Add(listViewItem3);
			this.lv1.Location = new System.Drawing.Point(4, 4);
			this.lv1.Size = new System.Drawing.Size(112, 96);
		
			this.lv1.Click += new EventHandler(lv1_Click);
			this.lv1.MouseDown += new MouseEventHandler(lv1_MouseDown);
			this.Controls.Add(this.lv1);
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			base.Dispose( disposing );
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonEx1 = new OpenNETCF.Windows.Forms.ButtonEx();
			// 
			// buttonEx1
			// 
			this.buttonEx1.Location = new System.Drawing.Point(124, 184);
			this.buttonEx1.Text = "buttonEx1";
			this.buttonEx1.TextAlign = OpenNETCF.Drawing.ContentAlignment.MiddleCenter;
			this.buttonEx1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonEx1_MouseDown);
			// 
			// Form1
			// 
			this.ClientSize = new System.Drawing.Size(214, 215);
			this.Controls.Add(this.buttonEx1);
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>

		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			CustomControlDemo.ClickableLabel cl = new CustomControlDemo.ClickableLabel();
			Bitmap bmp = new Bitmap(1,1);
			Graphics gr = Graphics.FromImage(bmp);

			cl.Text = " Clickable Label ";
			
			SizeF sizeText = gr.MeasureString(cl.Text, cl.Font);
			
			cl.BorderStyle = BorderStyle.FixedSingle;
			cl.Width = (int)sizeText.Width;
			cl.Height = (int)sizeText.Height;
			cl.Left = this.Width - cl.Width - 10;
			cl.Top = 150;

			cl.Click += new EventHandler(cl_Click);
			cl.MouseDown += new MouseEventHandler(cl_MouseDown);

			this.Controls.Add(cl);


			this.clbl = new ClickLabel();

			clbl.Left = 100;
			clbl.Top = 100;
			clbl.Text = "Clickable Label";

			clbl.Click += new EventHandler(clbl_Click);
			clbl.MouseDown += new MouseEventHandler(clbl_MouseDown);
			this.Controls.Add(clbl);

			lbl = new Button();

			lbl.Left = 150;
			lbl.Top = 50;
			lbl.Text = "label";
			lbl.Click += new EventHandler(lbl_Click);
			lbl.MouseDown += new MouseEventHandler(lbl_MouseDown);

			this.Controls.Add(lbl);
		}

		private void lv1_Click(object sender, EventArgs e)
		{
			//this.Text = lv1.Msg;
			this.Text = "Click";
		}

		private void lv1_MouseDown(object sender, MouseEventArgs e)
		{
			this.Text = "Mouse Down";
		}

		private void cl_Click(object sender, EventArgs e)
		{
			this.Text = "Label Click";
		}

		private void cl_MouseDown(object sender, MouseEventArgs e)
		{
			this.Text = "Label Mouse Down";
		}

		private void clbl_Click(object sender, EventArgs e)
		{
			this.Text = "ClickLabel Click";
		}

		private void clbl_MouseDown(object sender, MouseEventArgs e)
		{
			this.Text = "ClickLabel Mouse Down";
		}

		private void buttonEx1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			this.Text = "button Ex Mouse Down";
		}

		private void lbl_Click(object sender, EventArgs e)
		{
			this.Text = "lbl click";
		}

		private void lbl_MouseDown(object sender, MouseEventArgs e)
		{
			this.Text = "lbl mouse down";
		}
	}
}
