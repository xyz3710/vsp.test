using System;
using System.Drawing;
using System.Windows.Forms;

namespace ListViewTest
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class lv : System.Windows.Forms.ListView 
	{
		public string Msg;

		public new event EventHandler Click;

		protected virtual new void OnClick(EventArgs e)
		{
			if(Control.MouseButtons == MouseButtons.Left)
			{
				this.Msg = "Click";
			}

			if(this.Click != null)
			{
				//fire Click event
				this.Click(this, e);
			}
		}

		public new event System.Windows.Forms.MouseEventHandler MouseDown;

		protected new void OnMouseDown(MouseEventArgs e)
		{
			if(this.MouseDown != null)
			{
				//fire Mouse Down event
				this.MouseDown(this, e);
			}
		}
	}

	public class ClickLabel : System.Windows.Forms.Form
	{
		public ClickLabel()
		{
			this.Click += new EventHandler(OnClick);
			this.MouseDown += new MouseEventHandler(OnMouseDown);
		}

		//public new event EventHandler Click;
		/// <summary>
		/// made Event
		/// </summary>
		//public new event MouseEventHandler MouseDown;

		protected virtual void OnClick(object sender, EventArgs e)
		{
			this.Text = "Click";

		//	base.OnClick(e);
		}

		protected virtual void OnMouseDown(object sender, MouseEventArgs e)
		{
			this.Text = "MouseDown";
		
		//	base.OnMouseDown (e);
		}

	}
}

namespace CustomControlDemo
{
	// Vertical alignment
	public enum VAlign
	{
		Top, Middle, Bottom
	}

	// Horizontal alignment
	public enum HAlign
	{
		Left, Middle, Right
	}
	
	public class ClickableLabel : System.Windows.Forms.Control
	{
		// Internal storage for properties

		private VAlign      vrtAlign = VAlign.Middle;
		private HAlign      hrzAlign = HAlign.Middle;
		
		private BorderStyle bdrStyle = BorderStyle.None;
		
		// Constructor

		public ClickableLabel()
		{
			InitializeComponent();
		}

		// Dispose

		protected override void Dispose( bool disposing )
		{
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	
		// VAlign property

		public VAlign VAlign
		{
			get { return VAlign; }
			set
			{
				vrtAlign = value;
				Invalidate();
			}
		}

		// HAlign property

		public HAlign HAlign
		{
			get { return HAlign; }
			set
			{
				hrzAlign = value;
				Invalidate();
			}
		}

		// BorderStyle Property

		public BorderStyle BorderStyle
		{
			get { return bdrStyle; }
			set 
			{
				bdrStyle = value;
				Invalidate();
			}
		}

		// Text property

		public override string Text
		{
			// get {} function is provided by base class
			// and is not overridden
			// Override set functionality to ensure that
			// redraws take place
			set
			{
				base.Text = value;
				Invalidate();
			}
		}

		protected override void OnPaint(PaintEventArgs pe)
		{
			int borderSpace=0;
			Graphics gr = pe.Graphics;

			using (Pen penDraw = new Pen(Color.Black))
			{
				switch(BorderStyle)
				{
					case BorderStyle.FixedSingle:
						gr.DrawRectangle(penDraw, 0, 0, Width - 1, Height - 1);
						borderSpace = 2;
						break;
					case BorderStyle.Fixed3D:
						gr.DrawRectangle(penDraw, ClientRectangle);
						borderSpace = 2;
						break;
					case BorderStyle.None:
						// No border to draw
						borderSpace = 0;
						break;
				}
      
				SizeF sizeText = gr.MeasureString(Text, Font);

				float posX=0.0F;
				float posY=0.0F;

				switch (hrzAlign)
				{
					case HAlign.Left:
						posX = borderSpace;
						break;
					case HAlign.Middle:
						posX = (Width-sizeText.Width)/2;
						break;
					case HAlign.Right:
						posX = Width - sizeText.Width - borderSpace;
						break;
				}

				switch (vrtAlign)
				{
					case VAlign.Top:
						posY = borderSpace;
						break;
					case VAlign.Middle:
						posY = (Height - sizeText.Height)/2;
						break;
					case VAlign.Bottom:
						posY = Height - sizeText.Height - borderSpace;
						break;
				}

				SolidBrush b  = new SolidBrush(ForeColor);
				gr.DrawString(Text, Font, b, posX, posY);
				b.Dispose();
			}
			
			base.OnPaint(pe);
		}
	}
}

