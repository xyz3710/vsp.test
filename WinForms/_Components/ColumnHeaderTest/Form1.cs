using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;

namespace test
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.Button button2;
	
		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			base.Dispose( disposing );
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.listView1 = new System.Windows.Forms.ListView();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			// 
			// listView1
			// 
			this.listView1.Columns.Add(this.columnHeader1);
			this.listView1.Columns.Add(this.columnHeader2);
			this.listView1.Columns.Add(this.columnHeader3);
			this.listView1.FullRowSelect = true;
			this.listView1.Location = new System.Drawing.Point(16, 40);
			this.listView1.Size = new System.Drawing.Size(184, 224);
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.ItemActivate += new System.EventHandler(this.listView1_ItemActivate);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(21, 8);
			this.button1.Size = new System.Drawing.Size(80, 24);
			this.button1.Text = "hide/wait";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(117, 8);
			this.button2.Size = new System.Drawing.Size(80, 24);
			this.button2.Text = "show/default";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.Add(this.menuItem1);
			this.contextMenu1.MenuItems.Add(this.menuItem4);
			this.contextMenu1.MenuItems.Add(this.menuItem5);
			this.contextMenu1.MenuItems.Add(this.menuItem6);
			// 
			// menuItem1
			// 
			this.menuItem1.MenuItems.Add(this.menuItem2);
			this.menuItem1.MenuItems.Add(this.menuItem3);
			this.menuItem1.Text = "test";
			// 
			// menuItem2
			// 
			this.menuItem2.Text = "test";
			// 
			// menuItem3
			// 
			this.menuItem3.Text = "test";
			// 
			// menuItem4
			// 
			this.menuItem4.Text = "test";
			// 
			// menuItem5
			// 
			this.menuItem5.Text = "-";
			// 
			// menuItem6
			// 
			this.menuItem6.Text = "test";
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "ColumnHeader";
			this.columnHeader1.Width = 100;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "ColumnHeader";
			this.columnHeader2.Width = 100;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "ColumnHeader";
			this.columnHeader3.Width = 100;
			// 
			// Form1
			// 
			this.ClientSize = new System.Drawing.Size(218, 279);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.listView1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Text = "Bluetooth Manager";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Resize += new System.EventHandler(this.Form1_Resize);
			this.Load += new System.EventHandler(this.Form1_Load);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>

		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void listView1_ItemActivate(object sender, System.EventArgs e)
		{
			contextMenu1.Show(this, System.Windows.Forms.Form.MousePosition);
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			string[]	items = new string[]{"s", "name", "addr"};

			System.Windows.Forms.ListViewItem lvi;

			for (int i = 0; i < 20; i++)
			{
				items[0] = i.ToString();
				lvi = new ListViewItem(items);
				listView1.Items.Add(lvi);
			}
		}

		private void Form1_Resize(object sender, System.EventArgs e)
		{
			listView1.Top = 38;
			listView1.Left = 1;
			listView1.Width = this.Width-2;
			listView1.Height = this.Height - 76;
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			Cursor.Hide();
			Cursor.Current = Cursors.WaitCursor;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			Cursor.Show();
			Cursor.Current = Cursors.Default;
		}
	}
}
