using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Data;

namespace t4
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private	System.Windows.Forms.Timer contextTm = new Timer();

		private System.Drawing.Point mPt = new Point(0, 0);
	
		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			TabNHold tnh = new TabNHold(this, contextMenu1);
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			base.Dispose( disposing );
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.Add(this.menuItem1);
			this.contextMenu1.MenuItems.Add(this.menuItem2);
			this.contextMenu1.MenuItems.Add(this.menuItem3);
			// 
			// menuItem1
			// 
			this.menuItem1.Text = "Connect";
			// 
			// menuItem2
			// 
			this.menuItem2.Text = "-";
			// 
			// menuItem3
			// 
			this.menuItem3.Text = "Refresh";
			// 
			// Form1
			// 
			this.ClientSize = new System.Drawing.Size(202, 251);
			this.Text = "Form1";
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>

		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			contextTm.Interval = 500;
			contextTm.Tick += new EventHandler(contextTm_Tick);
		}

		private void contextTm_Tick(object sender, EventArgs e)
		{
			contextMenu1.Show(this, new Point(10, 10));
		}

		private void Form1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			mPt.X = e.X;
			mPt.Y = e.Y;
			contextTm.Enabled = true;
		}

		private void Form1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
		
		}

		private void Form1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			contextTm.Enabled = false;
		}
	}
}
