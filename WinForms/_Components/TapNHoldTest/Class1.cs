using System;

namespace t4
{
	public class TabNHold : System.Windows.Forms.Control
	{
		private	System.Windows.Forms.Control		ctrl;
		private	System.Windows.Forms.ContextMenu	cm;
		private	System.Drawing.Point				mPt;
		private System.Drawing.Brush				brush;
		private System.Drawing.Graphics				mdc;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;

		private	bool	isMouseUp = false;

		public TabNHold(System.Windows.Forms.Control control, System.Windows.Forms.ContextMenu contextMenu)
		{
			ctrl	= control;
			cm		= contextMenu;

			this.MouseDown	+= new System.Windows.Forms.MouseEventHandler(TabNHold_MouseDown);
			this.MouseMove	+= new System.Windows.Forms.MouseEventHandler(TabNHold_MouseMove);
			this.MouseUp	+= new System.Windows.Forms.MouseEventHandler(TabNHold_MouseUp);
		}

		private void TabNHold_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			isMouseUp = false;

			mPt.X = e.X;
			mPt.Y = e.Y;

			this.DrawStart();
		}

		private void TabNHold_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			mPt.X = e.X;
			mPt.Y = e.Y;
		}

		private void TabNHold_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			cm.Show(ctrl, mPt);
			isMouseUp = true;
		}

		private void DrawStart()
		{
			// 작은 원 위치 X, Y 
			// (top, top right, right, right bottom, bottom, bottom left, left, left top)
			int[]	axisX	= new int[]{15, 25, 15, 5, 1, 5, 29, 25};
			int[]	axisY	= new int[]{2, 6, 30, 26, 17, 7, 17, 27};
			// 한개의 점이 그려지는 시간
			int		time = 30;
			System.Drawing.Point	pt = new System.Drawing.Point(mPt.X-18, mPt.Y-18);

			mdc		= ctrl.CreateGraphics();
			brush	= new System.Drawing.SolidBrush(System.Drawing.Color.Black);

			for(int i = 0; i < axisX.Length; i++)
			{
				mdc.FillEllipse(brush, 
					new System.Drawing.Rectangle(axisX[i] + pt.X, axisY[i] + pt.Y, 6, 6));

				System.Threading.Thread.Sleep(time);

				ctrl.Invalidate();
			}
		}

		private void DrawStop()
		{
			
		}
	}
}
