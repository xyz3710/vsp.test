using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace TestHeader02
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox txtBox;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// txtBox
			// 
			this.txtBox.Location = new System.Drawing.Point(16, 14);
			this.txtBox.Multiline = true;
			this.txtBox.Name = "txtBox";
			this.txtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtBox.Size = new System.Drawing.Size(272, 480);
			this.txtBox.TabIndex = 0;
			this.txtBox.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(304, 509);
			this.Controls.Add(this.txtBox);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			ArrayList iList = new ArrayList();
			System.Text.Encoding	TE = System.Text.Encoding.ASCII;
			byte[]	buf = new byte[26];
			
			for (int i = 65; i < 65+26; i++)
				iList.Add(i);

			for (int i = 0; i < iList.Count; i++)
				buf[i] = System.Convert.ToByte((int)iList[i]);
						
			txtBox.Text = TE.GetString(buf, 0, buf.Length);
		}
	}
}
