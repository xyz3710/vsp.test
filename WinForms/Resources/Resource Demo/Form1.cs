using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Resources;
using System.Reflection;

namespace Resource_Demo
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox tbTitle;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbChapter;
		private System.Windows.Forms.TextBox tbAuthor;
		private System.Windows.Forms.TextBox tbPublisher;
		private System.Windows.Forms.PictureBox logo;
		private System.Windows.Forms.Label Title;

		private System.Resources.ResourceManager rm;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			Assembly assembly = Assembly.GetExecutingAssembly();

			rm = new ResourceManager("Resource_Demo.Demo", assembly);
			logo.Image = (Image)rm.GetObject("WroxLogo");
			tbTitle.Text	= rm.GetString("Title");
			tbChapter.Text	= rm.GetString("Chapter");
			tbAuthor.Text	= rm.GetString("Author");
			tbPublisher.Text= rm.GetString("Publisher");
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.logo = new System.Windows.Forms.PictureBox();
			this.Title = new System.Windows.Forms.Label();
			this.tbTitle = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.tbChapter = new System.Windows.Forms.TextBox();
			this.tbAuthor = new System.Windows.Forms.TextBox();
			this.tbPublisher = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// logo
			// 
			this.logo.Location = new System.Drawing.Point(12, 12);
			this.logo.Name = "logo";
			this.logo.Size = new System.Drawing.Size(260, 100);
			this.logo.TabIndex = 0;
			this.logo.TabStop = false;
			// 
			// Title
			// 
			this.Title.Location = new System.Drawing.Point(16, 148);
			this.Title.Name = "Title";
			this.Title.TabIndex = 1;
			this.Title.Text = "Title";
			// 
			// tbTitle
			// 
			this.tbTitle.Location = new System.Drawing.Point(164, 149);
			this.tbTitle.Name = "tbTitle";
			this.tbTitle.TabIndex = 2;
			this.tbTitle.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 177);
			this.label2.Name = "label2";
			this.label2.TabIndex = 1;
			this.label2.Text = "Chapter";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 206);
			this.label3.Name = "label3";
			this.label3.TabIndex = 1;
			this.label3.Text = "Author";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(16, 235);
			this.label4.Name = "label4";
			this.label4.TabIndex = 1;
			this.label4.Text = "Publisher";
			// 
			// tbChapter
			// 
			this.tbChapter.Location = new System.Drawing.Point(164, 178);
			this.tbChapter.Name = "tbChapter";
			this.tbChapter.TabIndex = 2;
			this.tbChapter.Text = "";
			// 
			// tbAuthor
			// 
			this.tbAuthor.Location = new System.Drawing.Point(164, 207);
			this.tbAuthor.Name = "tbAuthor";
			this.tbAuthor.TabIndex = 2;
			this.tbAuthor.Text = "";
			// 
			// tbPublisher
			// 
			this.tbPublisher.Location = new System.Drawing.Point(164, 236);
			this.tbPublisher.Name = "tbPublisher";
			this.tbPublisher.TabIndex = 2;
			this.tbPublisher.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.tbTitle);
			this.Controls.Add(this.Title);
			this.Controls.Add(this.logo);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.tbChapter);
			this.Controls.Add(this.tbAuthor);
			this.Controls.Add(this.tbPublisher);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}
	}
}
