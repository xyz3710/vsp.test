using System;
using System.Resources;
using System.Drawing;

namespace Demo
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			ResourceWriter	rw = new ResourceWriter("Demo.resources");
			//using (Image image = Image.FromFile("logo.gif"))
			using (Image image = Image.FromFile("bluetooth_active.ico"))
			{
				rw.AddResource("WroxLogo", image);
				rw.AddResource("Title", "Professional C#");
				rw.AddResource("Chapter", "Assemblies");
				rw.AddResource("Author", "Christian Nagel");
				rw.AddResource("Publisher", "Wrox press");
				rw.Close();
			}
		}
	}
}
