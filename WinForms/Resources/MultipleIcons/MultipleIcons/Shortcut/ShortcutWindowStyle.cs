﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Framework.Win.Updater.ShortcutWindowStyle
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 11월 5일 월요일 오후 9:25
/*	Purpose		:	Shortcut의 Windows Style을 나타냅니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace iDASiT.Framework.Win.Updater
{
	/// <summary>
	/// Shortcut의 Windows Style을 나타냅니다.
	/// </summary>
	[Flags]
	public enum ShortcutWindowStyle
	{
		/// <summary>
		/// Hide
		/// </summary>
		WshHide=0,
		/// <summary>
		/// NormalFocus
		/// </summary>
		WshNormalFocus=1,
		/// <summary>
		/// MinimizedFocus
		/// </summary>
		WshMinimizedFocus=2,
		/// <summary>
		/// MaximizedFocus
		/// </summary>
		WshMaximizedFocus=3,
		/// <summary>
		/// NormalNoFocus
		/// </summary>
		WshNormalNoFocus=4,
		/// <summary>
		/// MinimizedNoFocus
		/// </summary>
		WshMinimizedNoFocus=6,
	}
}
