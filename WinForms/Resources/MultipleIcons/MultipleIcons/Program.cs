﻿/**********************************************************************************************************************/
/*	Domain		:	MultipleIcons.Program
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 18일 수요일 오전 10:22
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
 *		1. MultipleIcon.res 만들기
 *			파일-새로 만들기-파일(^+N)
 *				"네이티브 리소스 템플릿" 선택
 *			Icon resource 추가(ID는 나중에 숫자로 변환되고 숫자가 작을 수록 첫번째 아이콘으로 나온다.)
 *		2. 프로젝트 폴더에 해당 *.res 파일을 추가
 *			프로젝트 속성에서 리소스-리소스 파일에서 해당 파일을 선택한다.
 *		3. 빌드 하여 바로 가기를 생성하면 여러 아이콘이 실행파일에 나온다.
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using iDASiT.Framework.Win.Updater;
using iDASiT.Framework.Win.Configuration;
using IWshRuntimeLibrary;
using System.Reflection;
using System.Windows.Forms;

namespace MultipleIcons
{
	class Program
	{
		static void Main(string[] args)
		{
			WriteShortcut("Multiple Icon 1", 0);
			WriteShortcut("Multiple Icon 2", 1);
		}

		private static void WriteShortcut(string shortcutName, int iconNum)
		{
			string workingFolder = Application.StartupPath;
			string appPath = Application.ExecutablePath;
			string shortcutDesc = "다중 아이콘을 지원 합니다.";
			string targetPath = Application.StartupPath;
			string shortcutPath = string.Format("{0}\\{1}.lnk", targetPath, shortcutName);
			WshShell shell = new WshShell();
			IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutPath);

			shortcut.TargetPath = appPath;
			shortcut.Description = shortcutDesc;
			shortcut.WindowStyle = (int)ShortcutWindowStyle.WshNormalFocus;
			shortcut.WorkingDirectory = workingFolder;
			shortcut.IconLocation = string.Format("{0},{1}", appPath, iconNum);
			shortcut.Save();
		}
	}
}
