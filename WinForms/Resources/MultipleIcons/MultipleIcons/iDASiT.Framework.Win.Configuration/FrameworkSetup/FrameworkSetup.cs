﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Framework.Win.Configuration.Setup
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 12월 4일 화요일 오후 7:27
/*	Purpose		:	Framework을 설치한 뒤 참고 되는 Registry를 관리합니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using Microsoft.Win32;

namespace iDASiT.Framework.Win.Configuration
{
	/// <summary>
	/// Framework을 설치한 뒤 참고 되는 Registry를 관리합니다.
	/// </summary>
	public class FrameworkSetup
	{
        #region Use Singleton Pattern
        private static FrameworkSetup _newInstance;
		private RegistryKey _localMachineSoftwareKey;
      
        #region Constructor for Single Ton Pattern
        /// <summary>
        /// Constructor for Single Ton Pattern
        /// </summary>
        /// <returns></returns>
        private FrameworkSetup()
        {
        	_newInstance = null;
			_localMachineSoftwareKey = Registry.LocalMachine.CreateSubKey("Software");
        }
        #endregion 
        
        #region GetInstance
        /// <summary>
        /// Create unique instance
        /// </summary>
        /// <returns></returns>
        public static FrameworkSetup GetInstance()
        {
        	if (_newInstance == null)
        		_newInstance = new FrameworkSetup();
        
        	return _newInstance;
        }
        #endregion
        #endregion
		
		#region Properties
		private RegistryKey CompanyKey
		{
			get
			{
				return _localMachineSoftwareKey.CreateSubKey("iDASiT", RegistryKeyPermissionCheck.ReadWriteSubTree);
			}
		}

		private RegistryKey FrameworkWinKey
		{
			get
			{
				return CompanyKey.CreateSubKey("Framework.Win", RegistryKeyPermissionCheck.ReadWriteSubTree);
			}
		}
		
		/// <summary>
		/// AutoServerSwitcherPath을 구하거나 설정합니다.
		/// </summary>
		public string AutoServerSwitcherPath
		{
			get
			{
				return FrameworkWinKey.GetValue("AutoServerSwitcher", @"C:\Program Files\iDASiT\Framework.Win\AutoServerSwitcher.exe") as string;
			}
			set
			{
				FrameworkWinKey.SetValue("AutoServerSwitcher", value);
			}
		}
		
		/// <summary>
		/// LoaderFilePath을 구하거나 설정합니다.
		/// </summary>
		public string LoaderFilePath
		{
			get
			{
				return FrameworkWinKey.GetValue("FrameworkWinLoader", @"C:\Program Files\iDASiT\Framework.Win\iDASiT.Framework.Win.Loader.exe") as string;
			}
			set
			{
				FrameworkWinKey.SetValue("FrameworkWinLoader", value);
			}
		}

		/// <summary>
		/// LoginFilePath를 구하거나 설정합니다.
		/// </summary>
		public string LoginFilePath
		{
			get
			{
				return FrameworkWinKey.GetValue("FrameworkWinLogin", @"C:\Program Files\iDASiT\Framework.Win\iDASiT.Framework.Win.Login.exe") as string;
			}
			set
			{
				FrameworkWinKey.SetValue("FrameworkWinLogin", value);
			}
		}

		#region MenuConsoleFilePath 사용안함
		/*
		/// <summary>
		/// MenuConsoleFilePath를 구하거나 설정합니다.
		/// </summary>
		public string MenuConsoleFilePath
		{
			get
			{
				return FrameworkWinKey.GetValue("FrameworkWinMenuConsole", @"C:\Program Files\iDASiT\Framework.Win\iDASiT.Framework.Win.MenuConsole.exe") as string;
			}
			set
			{
				FrameworkWinKey.SetValue("FrameworkWinMenuConsole", value);
			}
		}
		*/
		#endregion

		/// <summary>
		/// ResourcesFilePath를 구하거나 설정합니다.
		/// </summary>
		public string ResourcesFilePath
		{
			get
			{
				return FrameworkWinKey.GetValue("FrameworkWinResources", @"C:\Program Files\iDASiT\Framework.Win\iDASiT.Framework.Win.Resources.dll") as string;
			}
			set
			{
				FrameworkWinKey.SetValue("FrameworkWinResources", value);
			}
		}

		/// <summary>
		/// UpdaterFilePath를 구하거나 설정합니다.
		/// </summary>
		public string UpdaterFilePath
		{
			get
			{
				return FrameworkWinKey.GetValue("FrameworkWinUpdater", @"C:\Program Files\iDASiT\Framework.Win\iDASiT.Framework.Win.Updater.exe") as string;
			}
			set
			{
				FrameworkWinKey.SetValue("FrameworkWinUpdater", value);
			}
		}

		/// <summary>
		/// WorkingFolder를 구하거나 설정합니다.
		/// </summary>
		public string WorkingFolder
		{
			get
			{
				return FrameworkWinKey.GetValue("WorkingFolder", @"C:\Program Files\iDASiT\Framework.Win") as string;
			}
			set
			{
				FrameworkWinKey.SetValue("WorkingFolder", value);
			}
		}

		/// <summary>
		/// InstallFolder를 구하거나 설정합니다.
		/// </summary>
		public string InstallFolder
		{
			get
			{
				return CompanyKey.GetValue("InstallFolder", @"C:\Program Files\iDASiT") as string;
			}
			set
			{
				CompanyKey.SetValue("InstallFolder", value);
			}
		}

		/// <summary>
		/// Log Folder를 구하거나 설정합니다.
		/// </summary>
		public string LogFolder
		{
			get
			{
				return CompanyKey.GetValue("LogFolder", @"C:\Program Files\iDASiT\Log") as string;
			}
			set
			{
				CompanyKey.SetValue("LogFolder", value);
			}
		}

		/// <summary>
		/// GacUtilFilename을 구하거나 설정합니다.
		/// </summary>
		public string GacUtilFilename
		{
			get
			{
				return CompanyKey.GetValue("GacUtilFilename", @"C:\Program Files\iDASiT\Framework.Win\gacutil.exe") as string;
			}
			set
			{
				CompanyKey.SetValue("GacUtilFilename", value);
			}
		}

		/// <summary>
		/// PlantId를 구하거나 설정합니다.
		/// </summary>
		public string PlantId
		{
			get
			{
				return CompanyKey.GetValue("PlantId", "DCCPCS1") as string;
			}
			set
			{
				CompanyKey.SetValue("PlantId", value);
			}
		}
		#endregion
	}
}
