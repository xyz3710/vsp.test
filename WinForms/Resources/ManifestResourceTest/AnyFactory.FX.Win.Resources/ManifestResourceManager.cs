/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Resources.ManifestResourceManager
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 12월 2일 화요일 오후 6:41
/*	Purpose		:	내장된 Manifest Resource를 관리해 줍니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Reflection;
using Microsoft.Win32;
using System.Windows.Forms;

namespace AnyFactory.FX.Win.Resources
{
    /// <summary>
	/// 내장된 Manifest Resource를 관리해 줍니다.
	/// </summary>
	public class ManifestResourceManager
	{
		/// <summary>
		/// 포함된 Xml type의 Resource Stream을 구합니다.
		/// </summary>
		/// <param name="resourceType"></param>
		/// <returns></returns>
		public static Stream GetXmlStream(XmlResourceType resourceType)
		{
			return InternalManifestResourceManager.GetStreamByName(string.Format("Localize.{0}.xml", resourceType));
		}

		/// <summary>
		/// 포함된 Animated Cursor의 Resource Stream을 구합니다.
		/// </summary>
		/// <param name="cursorType"></param>
		/// <returns></returns>
		public static Stream GetCursorStream(CursorType cursorType)
		{
			return InternalManifestResourceManager.GetStreamByName(string.Format("Cursors.{0}.ani", cursorType));
		}

		/// <summary>
		/// 포함된 Image Resource의 Resource Stream을 구합니다.
		/// </summary>
		/// <returns></returns>
		public static Stream GetImageStream(CursorType cursorType)
		{
			return InternalManifestResourceManager.GetStreamByName(string.Format("Cursors.{0}.ani", cursorType));
		}

		/// <summary>
		/// Resource에 포함된 Animated Cursor를 구합니다.
		/// </summary>
		/// <param name="cursorType">구하고자 하는 Cursor Type</param>
		public static Cursor GetAnimatedCursor(CursorType cursorType)
		{
			Cursor aniCursor = Cursors.Default;
			string cursorFileName = string.Format(@"{0}\{1}.ani", 
                                        Environment.GetEnvironmentVariable("Temp", EnvironmentVariableTarget.Machine), 
                                        cursorType);
			
			if (File.Exists(cursorFileName) == false)
			{
				using (Stream resStream = ManifestResourceManager.GetCursorStream(cursorType))
				{					
					if (resStream != null)
					{
						System.Diagnostics.Debug.WriteLine("null 아니네", "ManifestResourceManager.GetAnimatedCursor");
						byte[] buffer = new byte[resStream.Length];
						int readByte = resStream.Read(buffer, 0, (int)resStream.Length);

						using (FileStream fs = new FileStream(cursorFileName, FileMode.Create))
						{
							fs.Write(buffer, 0, readByte);
							fs.Flush();
						}
					}
					else
						System.Diagnostics.Debug.WriteLine("null 이네", "ManifestResourceManager.GetAnimatedCursor");
				}
			}

			aniCursor = CursorFacotry.Create(cursorFileName);

			return aniCursor;
		}

		/// <summary>
		/// 해당 type에 포함된 Resource를 파일 형태로 구합니다.
		/// </summary>
		/// <param name="type">포함된 resource를 구할 type(type 정보에는 namespace와 리소스 포함된 assembly 정보를 포함)</param>
		/// <param name="resourceFileName"></param>
		/// <param name="targetPath"></param>
		public static bool GetFileFromResource(Type type, string resourceFileName, string targetPath)
		{
			if (string.IsNullOrEmpty(targetPath) == true)
				throw new InvalidOperationException("targetPath 인자는 null이거나 string.Empty일 수 없습니다.");

			string streamPath = Path.GetDirectoryName(targetPath);
			bool result = false;

			if (Directory.Exists(streamPath) == false)
				Directory.CreateDirectory(streamPath);

			using (Stream resStream = InternalManifestResourceManager.GetStreamByName(type.Assembly, type.Namespace, resourceFileName))
			{
				if (resStream != null)
				{
					byte[] buffer = new byte[resStream.Length];
					int readByte = resStream.Read(buffer, 0, (int)resStream.Length);

					using (FileStream fs = new FileStream(targetPath, FileMode.Create))
					{
						fs.Write(buffer, 0, readByte);
						fs.Flush();
						result = true;
					}
				}
			}

			return result;
		}
	}
}
