﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Resources.CursorType
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 12월 2일 화요일 오후 6:37
/*	Purpose		:	ManifestResourceManager가 관리하는 Cursor Type입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace AnyFactory.FX.Win.Resources
{
	/// <summary>
	/// ManifestResourceManager가 관리하는 Cursor Type입니다.
	/// </summary>
	public enum CursorType
	{
		/// <summary>
		/// AeroBusy
		/// </summary>
		AeroBusy = 0,
		/// <summary>
		/// AeroBusyLarge
		/// </summary>
		AeroBusyLarge,
		/// <summary>
		/// AeroBusyExtraLarge
		/// </summary>
		AeroBusyExtraLarge,
		/// <summary>
		/// AeroWorking
		/// </summary>
		AeroWorking,
		/// <summary>
		/// AeroWorkingLarge
		/// </summary>
		AeroWorkingLarge,
		/// <summary>
		/// AeroWorkingExtraLarge
		/// </summary>
		AeroWorkingExtraLarge,
	}
}
