/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Resources.InternalManifestResourceManager
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 12월 2일 화요일 오후 7:17
/*	Purpose		:	내장된 Manifest Resource를 관리를 위한 class입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Reflection;
using Microsoft.Win32;

namespace AnyFactory.FX.Win.Resources
{
	/// <summary>
	/// 내장된 Manifest Resource를 관리를 위한 class입니다.
	/// </summary>
	internal class InternalManifestResourceManager
	{
		private const string NAME_SPACE = "AnyFactory.FX.Win.Resources";

		/// <summary>
		/// AnyFactory.FX.Win.Resources의 기본 namespace를 사용한 Resource 이름에 의해 현재 Assembly에서 Resource stream을 구합니다.
		/// </summary>
		/// <param name="resourceName">NameSpace를 제외한 Resource의 이름</param>
		/// <returns></returns>
		public static Stream GetStreamByName(string resourceName)
		{
			string nameSpace = NAME_SPACE;

			return GetStreamByName(nameSpace, resourceName);
		}

        /// <summary>
		/// 지정된 namespace를 사용한 Resource 이름에 의해 현재 Assembly에서 Resource stream을 구합니다.
		/// </summary>
		/// <param name="nameSpace">nameSpace</param>
		/// <param name="resourceName">NameSpace를 제외한 Resource의 이름</param>
		/// <returns></returns>
		public static Stream GetStreamByName(string nameSpace, string resourceName)
		{
			return GetStreamByName(Assembly.GetCallingAssembly(), nameSpace, resourceName);
		}
		
		/// <summary>
		/// 지정된 namespace를 사용한 Resource 이름에 의해 resourceIncludedAssembly에서 Resource stream을 구합니다.
		/// </summary>
		/// <param name="resourceIncludedAssembly">resource가 포함된 assembly</param>
		/// <param name="nameSpace">nameSpace</param>
		/// <param name="resourceName">NameSpace를 제외한 Resource의 이름</param>
		/// <returns></returns>
		public static Stream GetStreamByName(Assembly resourceIncludedAssembly, string nameSpace, string resourceName)
		{
			if (string.IsNullOrEmpty(nameSpace) == true || string.IsNullOrEmpty(resourceName) == true)
				throw new InvalidOperationException("nameSpace나 resourceName이 string.Empty 일 수 없습니다.");

			Stream stream = null;

			if (resourceIncludedAssembly != null)
				stream = resourceIncludedAssembly.GetManifestResourceStream(string.Format("{0}.{1}", nameSpace, resourceName));

			return stream;
		}		
	}
}
