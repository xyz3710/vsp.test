﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ManifestResourceTest
{
	class Program
	{
		static void Main(string[] args)
		{
			Program program = new Program();

			program.TestMethod();
		}

		private void TestMethod()
		{
			const string RESOURCE_FILE_NAME = "CursorFacotry.cs";
			const string OUT_FILE_NAME = @"D:\EmbededResource.cs";
			bool result = false;
			
            result = AnyFactory.FX.Win.Resources.ManifestResourceManager.GetFileFromResource(this.GetType(), RESOURCE_FILE_NAME, OUT_FILE_NAME);
			result &= File.Exists(OUT_FILE_NAME);

			Console.WriteLine(string.Format("{0} 파일이 {1}에 생성 {2}", 
                                  RESOURCE_FILE_NAME, 
                                  OUT_FILE_NAME, 
                                  result == true ? "되었습니다." : "되지 않았습니다."));	
		}
	}
}
