using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace FileDialogExtender.Win32Types
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct POINT
	{
		public int x;
		public int y;

		#region Constructors
		public POINT(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public POINT(Point point)
		{
			x = point.X;
			y = point.Y;
		}
		#endregion
	}
}
