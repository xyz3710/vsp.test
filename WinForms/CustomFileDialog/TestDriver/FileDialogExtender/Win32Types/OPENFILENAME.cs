using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using FileDialogExtender;

namespace FileDialogExtender.Win32Types
{
	/// <summary>
	/// See the documentation for OPENFILENAME
	/// </summary>
	//typedef struct tagOFN { 
	//  DWORD         lStructSize; 
	//  HWND          hwndOwner; 
	//  HINSTANCE     hInstance; 
	//  LPCTSTR       lpstrFilter; 
	//  LPTSTR        lpstrCustomFilter; 
	//  DWORD         nMaxCustFilter; 
	//  DWORD         nFilterIndex; 
	//  LPTSTR        lpstrFile; 
	//  DWORD         nMaxFile; 
	//  LPTSTR        lpstrFileTitle; 
	//  DWORD         nMaxFileTitle; 
	//  LPCTSTR       lpstrInitialDir; 
	//  LPCTSTR       lpstrTitle; 
	//  DWORD         Flags; 
	//  WORD          nFileOffset; 
	//  WORD          nFileExtension; 
	//  LPCTSTR       lpstrDefExt; 
	//  LPARAM        lCustData; 
	//  LPOFNHOOKPROC lpfnHook; 
	//  LPCTSTR       lpTemplateName; 
	//#if (_WIN32_WINNT >= 0x0500)
	//  void *        pvReserved;
	//  DWORD         dwReserved;
	//  DWORD         FlagsEx;
	//#endif // (_WIN32_WINNT >= 0x0500)
	//} OPENFILENAME, *LPOPENFILENAME;
	[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Auto)]
	internal struct OPENFILENAME
	{
		public UInt32 lStructSize;
		public IntPtr hwndOwner;
		public IntPtr hInstance;
		public String lpstrFilter;
		public String lpstrCustomFilter;
		public UInt32 nMaxCustFilter;
		public Int32 nFilterIndex;
		public String lpstrFile;
		public UInt32 nMaxFile;
		public String lpstrFileTitle;
		public UInt32 nMaxFileTitle;
		public String lpstrInitialDir;
		public String lpstrTitle;
		public UInt32 Flags;
		public UInt16 nFileOffset;
		public UInt16 nFileExtension;
		public String lpstrDefExt;
		public IntPtr lCustData;
		public OfnHookProc lpfnHook;
		public String lpTemplateName;
		public IntPtr pvReserved;
		public UInt32 dwReserved;
		public UInt32 FlagsEx;
	};
}
