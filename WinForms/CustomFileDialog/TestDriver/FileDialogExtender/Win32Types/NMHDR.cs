using System;
using System.Runtime.InteropServices;

namespace FileDialogExtender.Win32Types
{
	//#region NCCALCSIZE_PARAMS
	//internal struct NCCALCSIZE_PARAMS
	//{
	//    public RECT     rgrc1;
	//    public RECT     rgrc2;
	//    public RECT     rgrc3;
	//    public IntPtr   lppos;
	//}
	//#endregion


	[StructLayout(LayoutKind.Sequential)]
	internal struct NMHDR
	{
		public IntPtr hwndFrom;
		public uint idFrom;
		public uint code;
	}
}
