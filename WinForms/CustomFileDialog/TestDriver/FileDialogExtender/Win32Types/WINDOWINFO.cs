using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using FileDialogExtender;

namespace FileDialogExtender.Win32Types
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct WINDOWINFO
	{
		public UInt32 cbSize;
		public RECT rcWindow;
		public RECT rcClient;
		public UInt32 dwStyle;
		public UInt32 dwExStyle;
		public UInt32 dwWindowStatus;
		public UInt32 cxWindowBorders;
		public UInt32 cyWindowBorders;
		public UInt16 atomWindowType;
		public UInt16 wCreatorVersion;
	}
}
