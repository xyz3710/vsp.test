using System;

namespace FileDialogExtender.Win32Types
{
	/// <summary>
	/// Defines the shape of hook procedures that can be called by the OpenFileDialog
	/// </summary>
	internal delegate IntPtr OfnHookProc(IntPtr hWnd, UInt16 msg, Int32 wParam, Int32 lParam);
}
