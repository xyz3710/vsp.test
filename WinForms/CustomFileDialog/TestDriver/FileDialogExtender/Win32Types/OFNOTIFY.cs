using System;
using System.Runtime.InteropServices;

namespace FileDialogExtender.Win32Types
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct OFNOTIFY
	{
		public NMHDR hdr;
		public IntPtr OpenFileName;
		public IntPtr fileNameShareViolation;
	}
}
