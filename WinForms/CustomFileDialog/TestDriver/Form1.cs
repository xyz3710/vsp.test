﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FileDialogExtender.Win32Types;
using System.Runtime.InteropServices;

namespace TestDriver
{
	public partial class Form1 : Form
	{
		private int _filterIndex;

		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			saveFileDialog.Filter = "Text files (*.txt)|*.txt|Excel (*.xls)|*.xls|All files (*.*)|*.*";
			saveFileDialog.ShowDialog(this);
						
		}

		protected override void WndProc(ref Message m)
		{
			//System.Diagnostics.Debug.WriteLine(((WindowsMessage)m.Msg).ToString());
			//switch ((WindowsMessage)m.Msg)
			//{
			//	case WindowsMessage.WM_NOTIFY:

			//		break;
			//}
		}
		/*
		protected override void WndProc(ref Message m)
		{
			//WM_NOTIFY, 49276
			System.Diagnostics.Debug.WriteLine(((WindowsMessage)m.Msg).ToString());
			switch ((WindowsMessage)m.Msg)
			{
				case WindowsMessage.WM_NOTIFY:
					OFNOTIFY ofNotify = (OFNOTIFY)Marshal.PtrToStructure(m.LParam, typeof(OFNOTIFY));

					switch (ofNotify.hdr.code)
					{
						case (uint)DialogChangeStatus.CDN_SELCHANGE:
							StringBuilder filePath = new StringBuilder(256);
							NativeMethods.SendMessage(new HandleRef(this, NativeMethods.GetParent(Handle)), (uint)DialogChangeProperties.CDM_GETFILEPATH, (IntPtr)256, filePath);

							// OnFileNameChanged(this as IWin32Window, filePath.ToString());

							break;
						case (uint)DialogChangeStatus.CDN_FOLDERCHANGE:
							StringBuilder folderPath = new StringBuilder(256);
							NativeMethods.SendMessage(new HandleRef(this, NativeMethods.GetParent(Handle)), (int)DialogChangeProperties.CDM_GETFOLDERPATH, (IntPtr)256, folderPath);

							// OnFolderNameChanged(this as IWin32Window, folderPath.ToString());

							break;
						case (uint)DialogChangeStatus.CDN_TYPECHANGE:
							OPENFILENAME ofn = (OPENFILENAME)Marshal.PtrToStructure(ofNotify.OpenFileName, typeof(OPENFILENAME));
							int i = ofn.nFilterIndex;
							
							if (_filterIndex != i)
							{
								_filterIndex = i;
								MessageBox.Show(
                                    string.Format("{0}을 눌렀습니다.", i), 
                                    Text, 
                                    MessageBoxButtons.OK, 
                                    MessageBoxIcon.Warning);

								// OnFilterChanged(this as IWin32Window, i);
							}

							break;
						case (uint)DialogChangeStatus.CDN_INITDONE:
							break;
						case (uint)DialogChangeStatus.CDN_SHAREVIOLATION:
							break;
						case (uint)DialogChangeStatus.CDN_HELP:
							break;
						case (uint)DialogChangeStatus.CDN_INCLUDEITEM:
							break;

						case (uint)DialogChangeStatus.CDN_FILEOK://0xfffffda2:
#pragma warning disable 1690, 0414
							// FileDialogControlBase _CustomCtrl 
							//NativeMethods.SetWindowPos(_CustomCtrl._hFileDialogHandle, IntPtr.Zero,
							//(int)_CustomCtrl._OpenDialogWindowRect.left,
							//(int)_CustomCtrl._OpenDialogWindowRect.top,
							//(int)_CustomCtrl._OpenDialogWindowRect.Width,
							//(int)_CustomCtrl._OpenDialogWindowRect.Height,
							//FileDialogControlBase.NativeFileDialogWrapper.UFLAGSSIZE);

							break;
#pragma warning restore 1690, 0414
						default:
							break;

					}
					break;
				case WindowsMessage.WM_COMMAND:
					switch (NativeMethods.GetDlgCtrlID(m.LParam))//switch (m.WParam & 0x0000ffff)
					{
						case (int)ControlsId.ButtonOk://OK
							break;
						case (int)ControlsId.ButtonCancel://Cancel
							break;
						case (int)ControlsId.ButtonHelp:  //0x0000040e://help
							break;
					}
					break;
			}

			base.WndProc(ref m);
		}
		*/
	}
}
