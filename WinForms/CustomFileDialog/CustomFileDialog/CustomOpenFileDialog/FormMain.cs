//  Copyright (c) 2006, Gustavo Franco
//  Copyright ?Decebal Mihailescu 2007
//  Email:  dmihailescu@hotmail.com
//  Email:  gustavo_franco@hotmail.com
//  All rights reserved.

//  Redistribution and use in source and binary forms, with or without modification, 
//  are permitted provided that the following conditions are met:

//  Redistributions of source code must retain the above copyright notice, 
//  this list of conditions and the following disclaimer. 
//  Redistributions in binary form must reproduce the above copyright notice, 
//  this list of conditions and the following disclaimer in the documentation 
//  and/or other materials provided with the distribution. 

//  THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
//  KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
//  PURPOSE. IT CAN BE DISTRIBUTED FREE OF CHARGE AS LONG AS THIS HEADER 
//  REMAINS UNCHANGED.

using System;
using System.IO;
using System.Data;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using AboutUtil;

namespace CustomControls
{
    
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
            
        }

        protected override void OnLoad(EventArgs e)
        {
            lblFilePath.Text = @"C:\WINNT\profiles\All Users\Documents\My Pictures\Sample Pictures\winter.jpg";
            base.OnLoad(e);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            
            if (sender.Equals(_btnSelect))
            {
                MyOpenFileDialogControl openDialog= new MyOpenFileDialogControl();;
                if (openDialog.ShowDialog(this) == DialogResult.OK)
                {
                    lblFilePath.Text = openDialog.MSDialog.FileName;
                }
                    openDialog.Dispose();
                    openDialog = null;
            }
            else
                if (sender.Equals(_btnSave))
                {
                    MySaveDialogControl saveDialog = new MySaveDialogControl(lblFilePath.Text, this);

					saveDialog.EventFilterChanged += new FileDialogExtenders.FileDialogControlBase.FilterChangedEventHandler(saveDialog_EventFilterChanged);
                    if (saveDialog.ShowDialog(this) == DialogResult.OK)
                    {
                        lblFilePath.Text = saveDialog.MSDialog.FileName;
                    }
                    saveDialog.Dispose();
                    saveDialog = null;
                }

            System.GC.Collect();
            GC.WaitForPendingFinalizers();
        }

		void saveDialog_EventFilterChanged(IWin32Window sender, int index)
		{
			MessageBox.Show(string.Format("{0}을 선택했어요", index),
				Text,
				MessageBoxButtons.OK,
				MessageBoxIcon.Warning);
            
		}

        private void _btnAbout_Click(object sender, EventArgs e)
        {
            About dlg = new About(this);
            dlg.ShowDialog(this);
        }
    }
}