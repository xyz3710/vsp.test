
namespace CustomControls
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._btnSelect = new System.Windows.Forms.Button();
            this._FileSize = new System.Windows.Forms.Label();
            this._btnSave = new System.Windows.Forms.Button();
            this.lblFilePath = new System.Windows.Forms.Label();
            this._btnAbout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _btnSelect
            // 
            this._btnSelect.Location = new System.Drawing.Point(68, 17);
            this._btnSelect.Name = "_btnSelect";
            this._btnSelect.Size = new System.Drawing.Size(75, 23);
            this._btnSelect.TabIndex = 24;
            this._btnSelect.Text = "Se&lect";
            this._btnSelect.UseVisualStyleBackColor = true;
            this._btnSelect.Click += new System.EventHandler(this.button1_Click);
            // 
            // _FileSize
            // 
            this._FileSize.AutoSize = true;
            this._FileSize.Location = new System.Drawing.Point(138, 446);
            this._FileSize.Name = "_FileSize";
            this._FileSize.Size = new System.Drawing.Size(16, 13);
            this._FileSize.TabIndex = 31;
            this._FileSize.Text = "   ";
            // 
            // _btnSave
            // 
            this._btnSave.Location = new System.Drawing.Point(216, 17);
            this._btnSave.Name = "_btnSave";
            this._btnSave.Size = new System.Drawing.Size(75, 23);
            this._btnSave.TabIndex = 24;
            this._btnSave.Text = "&Save";
            this._btnSave.UseVisualStyleBackColor = true;
            this._btnSave.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblFilePath
            // 
            this.lblFilePath.AutoSize = true;
            this.lblFilePath.Location = new System.Drawing.Point(17, 73);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(50, 13);
            this.lblFilePath.TabIndex = 34;
            this.lblFilePath.Text = "File path:";
            // 
            // _btnAbout
            // 
            this._btnAbout.Location = new System.Drawing.Point(364, 16);
            this._btnAbout.Name = "_btnAbout";
            this._btnAbout.Size = new System.Drawing.Size(75, 23);
            this._btnAbout.TabIndex = 35;
            this._btnAbout.Text = "&About";
            this._btnAbout.UseVisualStyleBackColor = true;
            this._btnAbout.Click += new System.EventHandler(this._btnAbout_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 133);
            this.Controls.Add(this._btnAbout);
            this.Controls.Add(this.lblFilePath);
            this.Controls.Add(this._FileSize);
            this.Controls.Add(this._btnSave);
            this.Controls.Add(this._btnSelect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Click To Open a Dialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _btnSelect;
        private System.Windows.Forms.Label _FileSize;
        private System.Windows.Forms.Button _btnSave;
        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.Button _btnAbout;


    }
}

