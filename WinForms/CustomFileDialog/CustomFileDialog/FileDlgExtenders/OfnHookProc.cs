using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using FileDialogExtenders;

namespace Win32Types
{
	#region OPENFILENAME

	/// <summary>
	/// Defines the shape of hook procedures that can be called by the OpenFileDialog
	/// </summary>
	internal delegate IntPtr OfnHookProc(IntPtr hWnd, UInt16 msg, Int32 wParam, Int32 lParam);
	#endregion
}
