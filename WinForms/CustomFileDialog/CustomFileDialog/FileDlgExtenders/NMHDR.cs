using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using FileDialogExtenders;

namespace Win32Types
{
	//#region NCCALCSIZE_PARAMS
	//internal struct NCCALCSIZE_PARAMS
	//{
	//    public RECT     rgrc1;
	//    public RECT     rgrc2;
	//    public RECT     rgrc3;
	//    public IntPtr   lppos;
	//}
	//#endregion

	#region NMHDR

	[StructLayout(LayoutKind.Sequential)]
	internal struct NMHDR
	{
		public IntPtr hwndFrom;
		public uint idFrom;
		public uint code;
	}
	#endregion
}
