using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using FileDialogExtenders;

namespace Win32Types
{
	#region OFNOTIFY

	[StructLayout(LayoutKind.Sequential)]
	internal struct OFNOTIFY
	{
		public NMHDR hdr;
		public IntPtr OpenFileName;
		public IntPtr fileNameShareViolation;
	}
	#endregion
}
