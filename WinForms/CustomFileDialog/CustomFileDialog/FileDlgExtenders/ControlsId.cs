using System;
using System.IO;
using System.Text;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using Win32Types;

namespace FileDialogExtenders
{
	internal enum ControlsId : int
	{
		ButtonOk=0x1,
		ButtonCancel=0x2,
		ButtonHelp=0x40E,//0x0000040e
		GroupFolder=0x440,
		LabelFileType=0x441,
		LabelFileName=0x442,
		LabelLookIn=0x443,
		DefaultView=0x461,
		LeftToolBar=0x4A0,
		ComboFileName=0x47c,
		ComboFileType=0x470,
		ComboFolder=0x471,
		CheckBoxReadOnly=0x410
	}
}
