using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using FileDialogExtenders;

namespace Win32Types
{
	#region RECT

	[StructLayout(LayoutKind.Sequential)]
	internal struct RECT
	{
		public int left;
		public int top;
		public int right;
		public int bottom;

		#region Properties

		public POINT Location
		{
			get
			{
				return new POINT((int)left, (int)top);
			}
			set
			{
				right   -= (left -  value.x);
				bottom  -= (bottom -  value.y);
				left    =  value.x;
				top     =  value.y;
			}
		}

		internal uint Width
		{
			get
			{
				return (uint)Math.Abs(right - left);
			}
			set
			{
				right = left + (int)value;
			}
		}

		internal uint Height
		{
			get
			{
				return (uint)Math.Abs(bottom - top);
			}
			set
			{
				bottom = top + (int)value;
			}
		}
		#endregion

		#region Overrides
		public override string ToString()
		{
			return left + ":" + top + ":" + right + ":" + bottom;
		}
		#endregion
	}
	#endregion
}
