using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using FileDialogExtenders;

namespace Win32Types
{
	#region POINT

	[StructLayout(LayoutKind.Sequential)]
	internal struct POINT
	{
		public int x;
		public int y;

		#region Constructors
		public POINT(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public POINT(Point point)
		{
			x = point.X;
			y = point.Y;
		}
		#endregion
	}
	#endregion
}
