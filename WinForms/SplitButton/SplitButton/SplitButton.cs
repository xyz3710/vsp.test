﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms.VisualStyles;
using System.ComponentModel;

namespace System.Windows.Forms
{
	using System.Drawing;

	/// <summary>
	/// Button with dropdown context menus.
	/// </summary>
	/// <seealso cref="System.Windows.Forms.Button" />
	/// <example>
	/// <code>
	/// SplitButton sbSelect = new SplitButton();
	/// 
	/// Controls.Add(sbSelect);
	/// sbSelect.AddMenu("All Files", x =>
	///	{
	///		MessageBox.Show(x.Text);
	///	});
	///	sbSelect.AddMenu("Selected File", x =>
	///	{
	///		MessageBox.Show(x.Text);
	///	}, true);
	///	sbSelect.AddMenu("Remove this Menu", x =>
	///	{
	///		MessageBox.Show(x.Text);
	///		sbSelect.RemoveMenu(x.Text);
	///	});
	/// </code>
	/// </example>
	public class SplitButton : Button
	{
		private const int PushButtonWidth = 14;

		private static int BorderSize = SystemInformation.Border3DSize.Width * 2;

		private Rectangle dropDownRectangle;
		private ContextMenuStrip cmMenu;
		private PushButtonState _state;
		private bool _skipNextOpen;
		private bool _showSplit;

		/// <summary>
		/// Initializes a new instance of the <see cref="SplitButton"/> class.
		/// </summary>
		public SplitButton()
		{
			_showSplit = true;
			dropDownRectangle = new Rectangle();

			AutoSize = true;
		}

		/// <summary>
		/// Gets or sets a value indicating whether split arrow.
		/// </summary>
		[Category("Appearance")]
		[Description("Get or sets a value indicating whether split arrow.")]
		[DefaultValue(true)]
		public bool ShowSplit
		{
			get
			{
				return _showSplit;
			}
			set
			{
				if (value != _showSplit)
				{
					_showSplit = value;
					Invalidate();

					if (Parent != null)
					{
						Parent.PerformLayout();
					}
				}
			}
		}

		private PushButtonState State
		{
			get
			{
				return _state;

			}
			set
			{
				if (_state.Equals(value) == false)
				{
					_state = value;
					Invalidate();
				}
			}
		}

		/// <summary>
		/// 컨트롤에 들어갈 수 있는 사각형 영역의 크기를 검색 합니다.
		/// </summary>
		/// <param name="proposedSize">컨트롤에 대 한 사용자 지정 크기 영역입니다.</param>
		/// <returns>
		/// 형식의 정렬 된 쌍 <see cref="T:System.Drawing.Size" /> 너비와 높이의 직사각형을 표시 합니다.
		/// </returns>
		public override Size GetPreferredSize(Size proposedSize)
		{
			Size preferredSize = base.GetPreferredSize(proposedSize);

			if (_showSplit && !string.IsNullOrEmpty(Text) && TextRenderer.MeasureText(Text, Font).Width + PushButtonWidth > preferredSize.Width)
			{
				return preferredSize + new Size(PushButtonWidth + BorderSize * 2, 0);
			}

			return preferredSize;
		}

		/// <summary>
		/// 지정된 키가 일반 입력 키인지 또는 전처리를 필요로 하는 특수 키인지 여부를 확인합니다.
		/// </summary>
		/// <param name="keyData"><see cref="T:System.Windows.Forms.Keys" /> 값 중 하나입니다.</param>
		/// <returns>
		/// 지정된 키가 일반 입력 키이면 true이고, 그렇지 않으면 false입니다.
		/// </returns>
		protected override bool IsInputKey(Keys keyData)
		{
			if (keyData.Equals(Keys.Down) == true && _showSplit == true)
			{
				return true;
			}
			else
			{
				return base.IsInputKey(keyData);
			}
		}

		/// <summary>
		/// 발생은 <see cref="E:System.Windows.Forms.Control.GotFocus" /> 이벤트입니다.
		/// </summary>
		/// <param name="e"><see cref="T:System.EventArgs" /> 이벤트 데이터를 포함 합니다.</param>
		protected override void OnGotFocus(EventArgs e)
		{
			if (_showSplit == false)
			{
				base.OnGotFocus(e);

				return;
			}

			if (State.Equals(PushButtonState.Pressed) == false
					&& State.Equals(PushButtonState.Disabled) == false)
			{
				State = PushButtonState.Default;
			}
		}

		/// <summary>
		/// 발생은 <see cref="M:System.Windows.Forms.ButtonBase.OnKeyUp(System.Windows.Forms.KeyEventArgs)" /> 이벤트입니다.
		/// </summary>
		/// <param name="kevent">A <see cref="T:System.Windows.Forms.KeyEventArgs" /> 이벤트 데이터를 포함 합니다.</param>
		protected override void OnKeyDown(KeyEventArgs kevent)
		{
			if (_showSplit == true)
			{
				if (kevent.KeyCode.Equals(Keys.Down) == true)
				{
					ShowContextMenuStrip();
				}
				else if (kevent.KeyCode.Equals(Keys.Space) == true
					&& kevent.Modifiers == Keys.None)
				{
					State = PushButtonState.Pressed;
				}
			}

			base.OnKeyDown(kevent);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Windows.Forms.ButtonBase.OnKeyUp(System.Windows.Forms.KeyEventArgs)" /> event.
		/// </summary>
		/// <param name="kevent">A <see cref="T:System.Windows.Forms.KeyEventArgs" /> that contains the event data.</param>
		protected override void OnKeyUp(KeyEventArgs kevent)
		{
			if (kevent.KeyCode.Equals(Keys.Space) == true)
			{
				if (Control.MouseButtons == MouseButtons.None)
				{
					State = PushButtonState.Normal;
				}
			}

			base.OnKeyUp(kevent);
		}

		/// <summary>
		/// 발생은 <see cref="M:System.Windows.Forms.ButtonBase.OnLostFocus(System.EventArgs)" /> 이벤트입니다.
		/// </summary>
		/// <param name="e"><see cref="T:System.EventArgs" /> 이벤트 데이터를 포함 합니다.</param>
		protected override void OnLostFocus(EventArgs e)
		{
			if (_showSplit == false)
			{
				base.OnLostFocus(e);

				return;
			}

			if (State.Equals(PushButtonState.Pressed) == false
					&& State.Equals(PushButtonState.Disabled) == false)
			{
				State = PushButtonState.Normal;
			}
		}

		/// <summary>
		///   <see cref="E:System.Windows.Forms.Control.MouseDown" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.Windows.Forms.MouseEventArgs" />입니다.</param>
		protected override void OnMouseDown(MouseEventArgs e)
		{
			if (_showSplit == false)
			{
				base.OnMouseDown(e);

				return;
			}

			if (dropDownRectangle.Contains(e.Location) == true)
			{
				ShowContextMenuStrip();
			}
			else
			{
				State = PushButtonState.Pressed;
			}
		}

		/// <summary>
		/// </summary>
		/// <param name="e">이벤트에 대한 정보를 제공합니다.</param>
		protected override void OnMouseEnter(EventArgs e)
		{
			if (_showSplit == false)
			{
				base.OnMouseEnter(e);

				return;
			}

			if (State.Equals(PushButtonState.Pressed) == false
				&& State.Equals(PushButtonState.Disabled) == false)
			{
				State = PushButtonState.Hot;
			}
		}

		/// <summary>
		/// </summary>
		/// <param name="e">이벤트에 대한 누락된 정보를 제공합니다.</param>
		protected override void OnMouseLeave(EventArgs e)
		{
			if (_showSplit == false)
			{
				base.OnMouseLeave(e);

				return;
			}

			if (State.Equals(PushButtonState.Pressed) == false
				&& State.Equals(PushButtonState.Disabled) == false)
			{
				if (Focused == true)
				{
					State = PushButtonState.Default;
				}
				else
				{
					State = PushButtonState.Normal;
				}
			}
		}

		/// <summary>
		///   <see cref="M:System.Windows.Forms.ButtonBase.OnMouseUp(System.Windows.Forms.MouseEventArgs)" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="mevent">이벤트 데이터가 들어 있는 <see cref="T:System.Windows.Forms.MouseEventArgs" />입니다.</param>
		protected override void OnMouseUp(MouseEventArgs mevent)
		{
			if (_showSplit == false)
			{
				base.OnMouseUp(mevent);

				return;
			}

			if (ContextMenuStrip == null || ContextMenuStrip.Visible == false)
			{
				SetButtonDrawState();

				if (Bounds.Contains(Parent.PointToClient(Cursor.Position)) == true
						&& dropDownRectangle.Contains(mevent.Location) == false)
				{
					OnClick(new EventArgs());
				}
			}
		}

		/// <summary>
		/// 발생은 <see cref="M:System.Windows.Forms.ButtonBase.OnPaint(System.Windows.Forms.PaintEventArgs)" /> 이벤트입니다.
		/// </summary>
		/// <param name="pevent">A <see cref="T:System.Windows.Forms.PaintEventArgs" /> 이벤트 데이터를 포함 합니다.</param>
		protected override void OnPaint(PaintEventArgs pevent)
		{
			base.OnPaint(pevent);

			if (_showSplit == false)
			{
				return;
			}

			Graphics g = pevent.Graphics;
			Rectangle bounds = this.ClientRectangle;

			if (State != PushButtonState.Pressed
				&& IsDefault == true
				&& Application.RenderWithVisualStyles == false)
			{
				Rectangle backgroundBounds = bounds;

				backgroundBounds.Inflate(-1, -1);
				ButtonRenderer.DrawButton(g, backgroundBounds, State);

				g.DrawRectangle(SystemPens.WindowFrame, 0, 0, bounds.Width - 1, bounds.Height - 1);
			}
			else
			{
				ButtonRenderer.DrawButton(g, bounds, State);
			}

			dropDownRectangle = new Rectangle(bounds.Right - PushButtonWidth - 1, BorderSize, PushButtonWidth, bounds.Height - BorderSize * 2);

			int internalBorder = BorderSize;
			Rectangle focusRect = new Rectangle(internalBorder, internalBorder, bounds.Width - dropDownRectangle.Width - internalBorder, bounds.Height - (internalBorder * 2));

			bool drawSplitLine = (State == PushButtonState.Hot || State == PushButtonState.Pressed || Application.RenderWithVisualStyles == false);

			if (RightToLeft == RightToLeft.Yes)
			{
				dropDownRectangle.X = bounds.Left + 1;
				focusRect.X = dropDownRectangle.Right;

				if (drawSplitLine == true)
				{
					g.DrawLine(SystemPens.ButtonShadow, bounds.Left + PushButtonWidth, BorderSize, bounds.Left + PushButtonWidth, bounds.Bottom - BorderSize);
					g.DrawLine(SystemPens.ButtonFace, bounds.Left + PushButtonWidth + 1, BorderSize, bounds.Left + PushButtonWidth + 1, bounds.Bottom - BorderSize);
				}
			}
			else
			{
				if (drawSplitLine == true)
				{
					g.DrawLine(SystemPens.ButtonShadow, bounds.Right - PushButtonWidth, BorderSize, bounds.Right - PushButtonWidth, bounds.Bottom - BorderSize);
					g.DrawLine(SystemPens.ButtonFace, bounds.Right - PushButtonWidth - 1, BorderSize, bounds.Right - PushButtonWidth - 1, bounds.Bottom - BorderSize);
				}
			}

			PaintArrow(g, dropDownRectangle);
			TextFormatFlags formatFlags = TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter;

			if (UseMnemonic == false)
			{
				formatFlags = formatFlags | TextFormatFlags.NoPrefix;
			}
			else if (ShowKeyboardCues == true)
			{
				formatFlags = formatFlags | TextFormatFlags.HidePrefix;
			}

			if (string.IsNullOrEmpty(this.Text) == false)
			{
				TextRenderer.DrawText(g, Text, Font, focusRect, SystemColors.ControlText, formatFlags);
			}

			if (State != PushButtonState.Pressed
				&& Focused == true)
			{
				ControlPaint.DrawFocusRectangle(g, focusRect);
			}
		}

		private void PaintArrow(Graphics g, Rectangle dropDownRect)
		{
			Point middle = new Point(Convert.ToInt32(dropDownRect.Left + dropDownRect.Width / 2), Convert.ToInt32(dropDownRect.Top + dropDownRect.Height / 2));

			middle.X += (dropDownRect.Width % 2);

			Point[] arrow = new Point[] { new Point(middle.X - 4, middle.Y - 3), new Point(middle.X + 4, middle.Y - 3), new Point(middle.X, middle.Y + 4) };

			g.FillPolygon(SystemBrushes.ControlText, arrow);
		}

		private void ShowContextMenuStrip()
		{
			if (_skipNextOpen == true)
			{
				_skipNextOpen = false;

				return;
			}

			State = PushButtonState.Pressed;

			if (ContextMenuStrip != null)
			{
				ContextMenuStrip.Closing += new ToolStripDropDownClosingEventHandler(ContextMenuStrip_Closing);
				ContextMenuStrip.Show(this, new Point(0, Height), ToolStripDropDownDirection.BelowRight);
			}
		}

		private void ContextMenuStrip_Closing(object sender, ToolStripDropDownClosingEventArgs e)
		{
			ContextMenuStrip cms = sender as ContextMenuStrip;

			if (cms != null)
			{
				cms.Closing -= new ToolStripDropDownClosingEventHandler(ContextMenuStrip_Closing);
			}

			SetButtonDrawState();

			if (e.CloseReason == ToolStripDropDownCloseReason.AppClicked)
			{
				_skipNextOpen = (dropDownRectangle.Contains(this.PointToClient(Cursor.Position)));
			}
		}

		private void SetButtonDrawState()
		{
			if (Bounds.Contains(Parent.PointToClient(Cursor.Position)) == true)
			{
				State = PushButtonState.Hot;
			}
			else if (Focused == true)
			{
				State = PushButtonState.Default;
			}
			else
			{
				State = PushButtonState.Normal;
			}
		}

		/// <summary>
		/// Adds the menu.
		/// </summary>
		/// <param name="text">The text(This text used remove key).</param>
		/// <param name="onClickAction">The on click action.</param>
		/// <param name="isDefaultMenu">if set to true then text to Text and onClickAction to Click event handler.</param>
		/// <param name="image">The image.</param>
		public ToolStripItem AddMenu(string text,
			EventHandler onClickAction,
			bool isDefaultMenu = false,
			Image image = null)
		{
			return AddMenu(text, x => { onClickAction(x, EventArgs.Empty); }, isDefaultMenu, image);
		}

		/// <summary>
		/// Adds the menu.
		/// </summary>
		/// <param name="text">The text(This text used remove key).</param>
		/// <param name="onClickAction">The on click action.</param>
		/// <param name="isDefaultMenu">if set to true then text to Text and onClickAction to Click event handler.</param>
		/// <param name="image">The image.</param>
		public ToolStripItem AddMenu(string text,
			Action<ToolStripItem> onClickAction,
			bool isDefaultMenu = false,
			Image image = null)
		{
			if (cmMenu == null)
			{
				cmMenu = new ContextMenuStrip();
				ContextMenuStrip = cmMenu;
			}

			var item = new ToolStripMenuItem(text, image)
			{
				Name = text,
			};

			if (onClickAction != null)
			{
				item.Click += (sender, e) =>
				{
					onClickAction(item);
				};
			}

			cmMenu.Items.Add(item);

			if (isDefaultMenu == true)
			{
				item.Font = new Font(item.Font, FontStyle.Bold);
				Text = text;
				Click += (sender, e) =>
				{
					onClickAction(item);
				};
			}

			return item;
		}

		/// <summary>
		/// Removes the menu.
		/// </summary>
		/// <param name="text">The text(That used AddMenu method).</param>
		public void RemoveMenu(string text)
		{
			if (cmMenu != null)
			{
				cmMenu.Items.RemoveByKey(text);
			}
		}
	}
}
