﻿namespace SplitButton
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.sbSelect = new System.Windows.Forms.SplitButton();
			this.cmMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.allFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.selectedFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.cmMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// sbSelect
			// 
			this.sbSelect.AutoSize = true;
			this.sbSelect.Location = new System.Drawing.Point(63, 41);
			this.sbSelect.Name = "sbSelect";
			this.sbSelect.Size = new System.Drawing.Size(158, 26);
			this.sbSelect.TabIndex = 0;
			this.sbSelect.Text = "Select Files";
			this.sbSelect.UseVisualStyleBackColor = true;
			// 
			// cmMenu
			// 
			this.cmMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.allFilesToolStripMenuItem,
			this.selectedFilesToolStripMenuItem});
			this.cmMenu.Name = "cmMenu";
			this.cmMenu.Size = new System.Drawing.Size(147, 48);
			// 
			// allFilesToolStripMenuItem
			// 
			this.allFilesToolStripMenuItem.Name = "allFilesToolStripMenuItem";
			this.allFilesToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.allFilesToolStripMenuItem.Text = "All Files";
			// 
			// selectedFilesToolStripMenuItem
			// 
			this.selectedFilesToolStripMenuItem.Name = "selectedFilesToolStripMenuItem";
			this.selectedFilesToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.selectedFilesToolStripMenuItem.Text = "Selected Files";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.sbSelect);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.cmMenu.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.SplitButton sbSelect;
		private System.Windows.Forms.ContextMenuStrip cmMenu;
		private System.Windows.Forms.ToolStripMenuItem allFilesToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem selectedFilesToolStripMenuItem;
	}
}

