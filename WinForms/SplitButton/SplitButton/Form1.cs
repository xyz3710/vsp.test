﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SplitButton
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			sbSelect.AddMenu("All Files", x =>
			{
				MessageBox.Show(x.Text);
			});
			sbSelect.AddMenu("Selected File", x =>
			{
				MessageBox.Show(x.Text);
			}, true);
			sbSelect.AddMenu("Remove this Menu", x =>
			{
				MessageBox.Show(x.Text);
				sbSelect.RemoveMenu(x.Text);
			});
		}
	}
}
