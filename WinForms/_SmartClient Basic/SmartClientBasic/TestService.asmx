<%@ WebService Language="C#" Class="TestService" %>

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;

[WebService(Namespace = "http://localhost.com/SmartClientBasic/TestService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class TestService : System.Web.Services.WebService 
{
    public TestService()
    {
        //
        // TODO: Add any constructor code required
        //
    }

    [WebMethod]
    public string TestWebMethod()
    {
        return "웹 서비스를 잘 호출 했습니다.";
    }

    [WebMethod]
    public DataSet GetDataSet()
    {
        SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Products",
			"SERVER=(local);UID=Tester;PWD=test;Database=Northwind");
        DataSet ds = new DataSet();

        adapter.Fill(ds);

        return ds;
    }
}
