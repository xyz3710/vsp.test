using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace ClientModule
{
	public partial class UIControl : UserControl
	{
		public UIControl()
		{
			InitializeComponent();
		}

		private void UIControl_Load(object sender, EventArgs e)
		{
			// 현재 어셈블리 버전을 표시한다
			Assembly assembly = Assembly.GetExecutingAssembly();

			lblAssemblyVersion.Text += " : " + assembly.GetName().Version.ToString();
		}

		private void btnTest1_Click(object sender, EventArgs e)
		{
			MessageBox.Show("스마트 클라이언트 작동 테스트", 
				"테스트", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void btnTest2_Click(object sender, EventArgs e)
		{
			StreamReader reader = File.OpenText(@"C:\Windows\odbc.ini");
			string result = reader.ReadToEnd();

			reader.Close();

			MessageBox.Show(result, "ODBC.INI 파일 내용",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void btnTest3_Click(object sender, EventArgs e)
		{
			QueryDataForm qdf = new QueryDataForm();

			qdf.ShowDialog();
		}
	}
}
