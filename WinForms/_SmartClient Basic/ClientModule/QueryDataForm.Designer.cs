namespace ClientModule
{
	partial class QueryDataForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.dgProducts = new System.Windows.Forms.DataGridView();
			this.oDBCdBaseBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.bthClose = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dgProducts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.oDBCdBaseBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// dgProducts
			// 
			this.dgProducts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dgProducts.AutoGenerateColumns = false;
			this.dgProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgProducts.DataSource = this.oDBCdBaseBindingSource;
			this.dgProducts.Location = new System.Drawing.Point(12, 12);
			this.dgProducts.Name = "dgProducts";
			this.dgProducts.RowTemplate.Height = 23;
			this.dgProducts.Size = new System.Drawing.Size(608, 380);
			this.dgProducts.TabIndex = 0;
			// 
			// bthClose
			// 
			this.bthClose.BackColor = System.Drawing.Color.Silver;
			this.bthClose.ForeColor = System.Drawing.Color.Black;
			this.bthClose.Location = new System.Drawing.Point(265, 404);
			this.bthClose.Name = "bthClose";
			this.bthClose.Size = new System.Drawing.Size(102, 37);
			this.bthClose.TabIndex = 1;
			this.bthClose.Text = "�ݱ�(&C)";
			this.bthClose.UseVisualStyleBackColor = false;
			this.bthClose.Click += new System.EventHandler(this.bthClose_Click);
			// 
			// QueryDataForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(632, 453);
			this.Controls.Add(this.bthClose);
			this.Controls.Add(this.dgProducts);
			this.Name = "QueryDataForm";
			this.Text = "QueryDataForm";
			this.Load += new System.EventHandler(this.QueryDataForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgProducts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.oDBCdBaseBindingSource)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dgProducts;
		private System.Windows.Forms.Button bthClose;
		private System.Windows.Forms.BindingSource oDBCdBaseBindingSource;
	}
}