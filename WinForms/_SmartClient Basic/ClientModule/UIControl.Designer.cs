namespace ClientModule
{
	partial class UIControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblTitle = new System.Windows.Forms.Label();
			this.lblAssemblyVersion = new System.Windows.Forms.Label();
			this.btnTest1 = new System.Windows.Forms.Button();
			this.btnTest2 = new System.Windows.Forms.Button();
			this.btnTest3 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblTitle
			// 
			this.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.lblTitle.AutoSize = true;
			this.lblTitle.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTitle.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.lblTitle.Location = new System.Drawing.Point(61, 54);
			this.lblTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(391, 39);
			this.lblTitle.TabIndex = 0;
			this.lblTitle.Text = "Smart Client UI Control";
			// 
			// lblAssemblyVersion
			// 
			this.lblAssemblyVersion.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.lblAssemblyVersion.Location = new System.Drawing.Point(64, 130);
			this.lblAssemblyVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblAssemblyVersion.Name = "lblAssemblyVersion";
			this.lblAssemblyVersion.Size = new System.Drawing.Size(384, 19);
			this.lblAssemblyVersion.TabIndex = 1;
			this.lblAssemblyVersion.Text = "Assembly Version";
			this.lblAssemblyVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btnTest1
			// 
			this.btnTest1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.btnTest1.BackColor = System.Drawing.Color.LightSeaGreen;
			this.btnTest1.Location = new System.Drawing.Point(176, 191);
			this.btnTest1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnTest1.Name = "btnTest1";
			this.btnTest1.Size = new System.Drawing.Size(161, 36);
			this.btnTest1.TabIndex = 2;
			this.btnTest1.Text = "테스트 #1";
			this.btnTest1.UseVisualStyleBackColor = false;
			this.btnTest1.Click += new System.EventHandler(this.btnTest1_Click);
			// 
			// btnTest2
			// 
			this.btnTest2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.btnTest2.BackColor = System.Drawing.Color.DarkCyan;
			this.btnTest2.Location = new System.Drawing.Point(176, 253);
			this.btnTest2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnTest2.Name = "btnTest2";
			this.btnTest2.Size = new System.Drawing.Size(161, 36);
			this.btnTest2.TabIndex = 3;
			this.btnTest2.Text = "테스트 #2";
			this.btnTest2.UseVisualStyleBackColor = false;
			this.btnTest2.Click += new System.EventHandler(this.btnTest2_Click);
			// 
			// btnTest3
			// 
			this.btnTest3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.btnTest3.BackColor = System.Drawing.Color.CadetBlue;
			this.btnTest3.Location = new System.Drawing.Point(176, 315);
			this.btnTest3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnTest3.Name = "btnTest3";
			this.btnTest3.Size = new System.Drawing.Size(161, 36);
			this.btnTest3.TabIndex = 4;
			this.btnTest3.Text = "테스트 #3";
			this.btnTest3.UseVisualStyleBackColor = false;
			this.btnTest3.Click += new System.EventHandler(this.btnTest3_Click);
			// 
			// UIControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnTest3);
			this.Controls.Add(this.btnTest2);
			this.Controls.Add(this.btnTest1);
			this.Controls.Add(this.lblAssemblyVersion);
			this.Controls.Add(this.lblTitle);
			this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "UIControl";
			this.Size = new System.Drawing.Size(513, 405);
			this.Load += new System.EventHandler(this.UIControl_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Label lblAssemblyVersion;
		private System.Windows.Forms.Button btnTest1;
		private System.Windows.Forms.Button btnTest2;
		private System.Windows.Forms.Button btnTest3;
	}
}
