namespace Launcher
{
	partial class LauncherForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ucClient = new ClientModule.UIControl();
			this.SuspendLayout();
			// 
			// ucClient
			// 
			this.ucClient.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ucClient.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ucClient.Location = new System.Drawing.Point(0, 0);
			this.ucClient.Margin = new System.Windows.Forms.Padding(0);
			this.ucClient.Name = "ucClient";
			this.ucClient.Size = new System.Drawing.Size(505, 378);
			this.ucClient.TabIndex = 0;
			// 
			// LauncherForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(505, 378);
			this.Controls.Add(this.ucClient);
			this.Name = "LauncherForm";
			this.Text = "Smart Client Launcher Application";
			this.ResumeLayout(false);

		}

		#endregion

		private ClientModule.UIControl ucClient;
	}
}

