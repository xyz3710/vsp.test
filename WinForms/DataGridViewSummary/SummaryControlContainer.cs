using System;
using System.Data;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using System.Data.Linq;

namespace System.Windows.Forms
{
	/// <summary>
	/// Class SummaryControlContainer.
	/// </summary>
	public class SummaryControlContainer : Control
	{
		#region Declare variables
		private Dictionary<string, ReadOnlyTextBox> _sumBoxes;
		private DataGridViewSummary dgv;
		private Label sumRowHeaderLabel;

		/// <summary>
		/// Gets or sets the initial height.
		/// </summary>
		/// <value>The initial height.</value>
		public int InitialHeight
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets a value indicating whether [last visible state].
		/// </summary>
		/// <value>true if [last visible state]; otherwise, false.</value>
		public bool LastVisibleState
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the color of the summary row back.
		/// </summary>
		/// <value>The color of the summary row back.</value>
		public Color SummaryRowBackColor
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the summary value.
		/// <remarks>Key is columnName</remarks>
		/// </summary>
		/// <value>The summary value.</value>
		public Dictionary<string, object> SummaryValues
		{
			get;
			set;
		}

		/// <summary>
		/// Event is raised when visibility changes and the
		/// lastVisibleState is not the new visible state
		/// </summary>
		public event EventHandler VisibilityChanged;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SummaryControlContainer"/> class.
		/// </summary>
		/// <param name="dgv">The DGV.</param>
		/// <exception cref="System.Exception">DataGridView is null!</exception>
		public SummaryControlContainer(DataGridViewSummary dgv)
		{
			if (dgv == null)
				throw new Exception("DataGridView is null!");

			this.dgv = dgv;

			_sumBoxes = new Dictionary<string, ReadOnlyTextBox>();
			SummaryValues = new Dictionary<string, object>();
			sumRowHeaderLabel = new Label();

			this.dgv.CreateSummary += new EventHandler(dgv_CreateSummary);
			this.dgv.RowsAdded += new DataGridViewRowsAddedEventHandler(dgv_RowsAdded);
			this.dgv.RowsRemoved += new DataGridViewRowsRemovedEventHandler(dgv_RowsRemoved);
			this.dgv.CellValueChanged += new DataGridViewCellEventHandler(dgv_CellValueChanged);

			this.dgv.Scroll += new ScrollEventHandler(dgv_Scroll);
			this.dgv.ColumnWidthChanged += new DataGridViewColumnEventHandler(dgv_ColumnWidthChanged);
			this.dgv.RowHeadersWidthChanged += new EventHandler(dgv_RowHeadersWidthChanged);
			this.VisibleChanged += new EventHandler(SummaryControlContainer_VisibleChanged);

			this.dgv.ColumnAdded += new DataGridViewColumnEventHandler(dgv_ColumnAdded);
			this.dgv.ColumnRemoved += new DataGridViewColumnEventHandler(dgv_ColumnRemoved);
			this.dgv.ColumnStateChanged += new DataGridViewColumnStateChangedEventHandler(dgv_ColumnStateChanged);
			this.dgv.ColumnDisplayIndexChanged += new DataGridViewColumnEventHandler(dgv_ColumnDisplayIndexChanged);

		}

		private void dgv_ColumnDisplayIndexChanged(object sender, DataGridViewColumnEventArgs e)
		{
			//resizeSumBoxes();
			ReCreateSumBoxes();
		}

		private void dgv_ColumnStateChanged(object sender, DataGridViewColumnStateChangedEventArgs e)
		{
			ResizeSumBoxes();
		}

		private void dgv_ColumnRemoved(object sender, DataGridViewColumnEventArgs e)
		{
			ReCreateSumBoxes();
		}

		private void dgv_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
		{
			ReCreateSumBoxes();
		}

		private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			var columnName = GetColumnName(e.ColumnIndex);

			if (_sumBoxes.ContainsKey(columnName) == true)
			{
				ReadOnlyTextBox roTextBox = _sumBoxes[columnName];

				if (roTextBox.IsSummary)
				{
					CalcSummaries();
				}
			}
		}

		private void dgv_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
		{
			CalcSummaries();
		}

		private void dgv_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
		{
			CalcSummaries();
		}

		private void SummaryControlContainer_VisibleChanged(object sender, EventArgs e)
		{
			if (LastVisibleState != this.Visible)
			{
				OnVisiblityChanged(sender, e);
			}
		}

		/// <summary>
		/// Handles the <see cref="E:VisiblityChanged" /> event.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void OnVisiblityChanged(object sender, EventArgs e)
		{
			if (VisibilityChanged != null)
				VisibilityChanged(sender, e);

			LastVisibleState = this.Visible;
		}

		#endregion

		#region Events and delegates

		private void dgv_CreateSummary(object sender, EventArgs e)
		{
			ReCreateSumBoxes();
			CalcSummaries();
		}

		private void dgv_Scroll(object sender, ScrollEventArgs e)
		{
			ResizeSumBoxes();
		}

		private void dgv_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
		{
			ResizeSumBoxes();
		}

		private void dgv_RowHeadersWidthChanged(object sender, EventArgs e)
		{
			ResizeSumBoxes();
		}

		/// <summary>
		/// <see cref="E:System.Windows.Forms.Control.Resize" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.EventArgs" />입니다.</param>
		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			ResizeSumBoxes();
		}

		private void dgv_Resize(object sender, EventArgs e)
		{
			ResizeSumBoxes();
		}

		#endregion

		#region Functions

		private string GetColumnName(int columnIndex)
		{
			return dgv.Columns[columnIndex].Name;
		}

		/// <summary>
		/// Checks if passed object is of type of integer
		/// </summary>
		/// <param name="o">object</param>
		/// <returns>true/ false</returns>
		protected bool IsInteger(object o)
		{
			if (o is Int64)
				return true;
			if (o is Int32)
				return true;
			if (o is Int16)
				return true;
			return false;
		}

		/// <summary>
		/// Checks if passed object is of type of decimal/ double
		/// </summary>
		/// <param name="o">object</param>
		/// <returns>true/ false</returns>
		protected bool IsDecimal(object o)
		{
			if (o is Decimal)
				return true;
			if (o is Single)
				return true;
			if (o is Double)
				return true;
			return false;
		}

		/// <summary>
		/// Enable manual refresh of the SummaryDataGridView
		/// </summary>
		internal void RefreshSummary()
		{
			CalcSummaries();
		}

		/// <summary>
		/// Calculate the Sums of the summary columns
		/// </summary>
		private void CalcSummaries()
		{
			foreach (ReadOnlyTextBox roTextBox in _sumBoxes.Values)
			{
				if (roTextBox.IsSummary)
				{
					roTextBox.Tag = 0;
					roTextBox.Text = "0";
					roTextBox.Invalidate();
				}
			}

			if (dgv.SummaryColumns != null && dgv.SummaryColumns.Length > 0 && _sumBoxes.Count > 0)
			{
				foreach (DataGridViewRow dgvRow in dgv.Rows)
				{
					foreach (DataGridViewCell dgvCell in dgvRow.Cells)
					{
						foreach (var columnName in _sumBoxes.Keys)
						{
							if (dgvCell.OwningColumn.Name != columnName || _sumBoxes.ContainsKey(columnName) == false)
							{
								continue;
							}

							ReadOnlyTextBox sumBox = _sumBoxes[columnName];

							if (sumBox != null && sumBox.IsSummary)
							{
								if (dgvCell.Value != null && !(dgvCell.Value is DBNull))
								{
									if (IsInteger(dgvCell.Value))
									{
										sumBox.Tag = Convert.ToInt64(sumBox.Tag) + Convert.ToInt64(dgvCell.Value);
									}
									else if (IsDecimal(dgvCell.Value))
									{
										sumBox.Tag = Convert.ToDecimal(sumBox.Tag) + Convert.ToDecimal(dgvCell.Value);
									}
									else
									{
										sumBox.Tag = Convert.ToInt32(sumBox.Tag) + 1;
									}

									if (SummaryValues.ContainsKey(columnName) == false)
									{
										SummaryValues.Add(columnName, sumBox.Tag);
									}
									else
									{
										SummaryValues[columnName] = sumBox.Tag;
									}

									sumBox.Text = string.Format("{0}", sumBox.Tag);
									sumBox.Invalidate();
								}
							}
						}
					}
				}

				if (dgv.Rows.Count == 0)
				{
					foreach (string key in SummaryValues.Keys.ToList())
					{
						SummaryValues[key] = 0;
					}
				}
			}
		}

		/// <summary>
		/// Create summary boxes for each Column of the DataGridView        
		/// </summary>
		private void ReCreateSumBoxes()
		{
			ReadOnlyTextBox sumBox;

			foreach (Control control in _sumBoxes.Values)
			{
				this.Controls.Remove(control);
			}

			_sumBoxes.Clear();

			int iCnt = 0;

			List<DataGridViewColumn> sortedColumns = SortedColumns;

			foreach (DataGridViewColumn dgvColumn in sortedColumns)
			{
				if (string.IsNullOrEmpty(dgvColumn.Name) == true)
				{
					continue;
				}

				sumBox = new ReadOnlyTextBox();
				_sumBoxes.Add(dgvColumn.Name, sumBox);

				sumBox.Top = 0;
				sumBox.Height = dgv.RowTemplate.Height;
				sumBox.BorderColor = dgv.GridColor;

				if (SummaryRowBackColor == null)
				{
					sumBox.BackColor = dgv.DefaultCellStyle.BackColor;
				}
				else
				{
					sumBox.BackColor = SummaryRowBackColor;

				}
				sumBox.BringToFront();

				if (dgv.ColumnCount - iCnt == 1)
					sumBox.IsLastColumn = true;

				var individualColumnFormat = false;

				if (dgv.SummaryColumns != null && dgv.SummaryColumnsFormat != null)
				{
					individualColumnFormat = dgv.SummaryColumns.Length == dgv.SummaryColumnsFormat.Length && dgv.SummaryColumns.Length > 0;
				}

				if (dgv.SummaryColumns != null && dgv.SummaryColumns.Length > 0)
				{
					for (int iCntX = 0; iCntX < dgv.SummaryColumns.Length; iCntX++)
					{
						if (dgv.SummaryColumns[iCntX] == dgvColumn.DataPropertyName ||
							dgv.SummaryColumns[iCntX] == dgvColumn.Name)
						{
							dgvColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

							if (individualColumnFormat == true)
							{
								sumBox.FormatString = dgv.SummaryColumnsFormat[iCntX];
							}
							else
							{
								sumBox.FormatString = dgv.SummaryDefaultFormatString ?? DataGridViewSummary.SUMMARY_DEFAULT_FORMAT_STRING;
							}

							sumBox.TextAlign = TextHelper.TranslateGridColumnAligment(dgvColumn.DefaultCellStyle.Alignment);
							sumBox.IsSummary = true;

							if (dgvColumn.ValueType == typeof(System.Int32) || dgvColumn.ValueType == typeof(System.Int16) ||
								dgvColumn.ValueType == typeof(System.Int64) || dgvColumn.ValueType == typeof(System.Single) ||
								dgvColumn.ValueType == typeof(System.Double) || dgvColumn.ValueType == typeof(System.Single) ||
								dgvColumn.ValueType == typeof(System.Decimal))
							{
								sumBox.Tag = System.Activator.CreateInstance(dgvColumn.ValueType);
							}
						}
					}
				}

				sumBox.BringToFront();
				this.Controls.Add(sumBox);

				iCnt++;
			}

			if (dgv.DisplaySumRowHeader)
			{
				sumRowHeaderLabel.Font = new Font(dgv.DefaultCellStyle.Font, dgv.SumRowHeaderTextBold ? System.Drawing.FontStyle.Bold : System.Drawing.FontStyle.Regular);
				sumRowHeaderLabel.Anchor = AnchorStyles.Left;
				sumRowHeaderLabel.TextAlign = ContentAlignment.MiddleLeft;
				sumRowHeaderLabel.Height = sumRowHeaderLabel.Font.Height;
				sumRowHeaderLabel.Top = Convert.ToInt32(Convert.ToDouble(this.InitialHeight - sumRowHeaderLabel.Height) / 2F);
				sumRowHeaderLabel.Text = dgv.SumRowHeaderText;

				sumRowHeaderLabel.ForeColor = dgv.DefaultCellStyle.ForeColor;
				sumRowHeaderLabel.Margin = new Padding(0);
				sumRowHeaderLabel.Padding = new Padding(0);

				this.Controls.Add(sumRowHeaderLabel);
			}
			CalcSummaries();
			ResizeSumBoxes();
		}

		/// <summary>
		/// Order the columns in the way they are displayed
		/// </summary>
		private List<DataGridViewColumn> SortedColumns
		{
			get
			{
				List<DataGridViewColumn> result = new List<DataGridViewColumn>();
				DataGridViewColumn column = dgv.Columns.GetFirstColumn(DataGridViewElementStates.None);
				if (column == null)
					return result;
				result.Add(column);
				while ((column = dgv.Columns.GetNextColumn(column, DataGridViewElementStates.None, DataGridViewElementStates.None)) != null)
					result.Add(column);

				return result;
			}
		}

		/// <summary>
		/// Resize the summary Boxes depending on the 
		/// width of the Columns of the DataGridView
		/// </summary>
		private void ResizeSumBoxes()
		{
			this.SuspendLayout();

			if (_sumBoxes.Count > 0)
				try
				{
					int rowHeaderWidth = dgv.RowHeadersVisible ? dgv.RowHeadersWidth - 1 : 0;
					int sumLabelWidth = dgv.RowHeadersVisible ? dgv.RowHeadersWidth - 1 : 0;
					int curPos = rowHeaderWidth;

					if (dgv.DisplaySumRowHeader && sumLabelWidth > 0)
					{
						if (dgv.RightToLeft == RightToLeft.Yes)
						{
							if (sumRowHeaderLabel.Dock != DockStyle.Right)
								sumRowHeaderLabel.Dock = DockStyle.Right;
						}
						else
						{
							if (sumRowHeaderLabel.Dock != DockStyle.Left)
								sumRowHeaderLabel.Dock = DockStyle.Left;

						}
					}
					else
					{
						if (sumRowHeaderLabel.Visible)
							sumRowHeaderLabel.Visible = false;
					}

					int iCnt = 0;
					Rectangle oldBounds;

					foreach (DataGridViewColumn dgvColumn in SortedColumns) //dgv.Columns)
					{
						if (_sumBoxes.ContainsKey(dgvColumn.Name) == false)
						{
							continue;
						}

						ReadOnlyTextBox sumBox = _sumBoxes[dgvColumn.Name];

						if (sumBox != null)
						{
							oldBounds = sumBox.Bounds;
							if (!dgvColumn.Visible)
							{
								sumBox.Visible = false;
								continue;
							}

							int from = curPos - dgv.HorizontalScrollingOffset;

							int width = dgvColumn.Width + (iCnt == 0 ? 0 : 0);

							if (from < rowHeaderWidth)
							{
								width -= rowHeaderWidth - from;
								from = rowHeaderWidth;
							}

							if (from + width > this.Width)
								width = this.Width - from;

							if (width < 4)
							{
								if (sumBox.Visible)
									sumBox.Visible = false;
							}
							else
							{
								if (this.RightToLeft == RightToLeft.Yes)
									from = this.Width - from - width;


								if (sumBox.Left != from || sumBox.Width != width)
									sumBox.SetBounds(from, 0, width, 0, BoundsSpecified.X | BoundsSpecified.Width);

								if (!sumBox.Visible)
									sumBox.Visible = true;
							}

							curPos += dgvColumn.Width + (iCnt == 0 ? 0 : 0);
							if (oldBounds != sumBox.Bounds)
								sumBox.Invalidate();

						}
						iCnt++;
					}
				}
				finally
				{
					this.ResumeLayout();
				}
		}
		#endregion
	}
}
