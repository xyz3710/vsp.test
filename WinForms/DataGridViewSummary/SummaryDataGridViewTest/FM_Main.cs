﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SummaryDataGridViewTest
{
	public partial class FM_Main : Form
	{
		public FM_Main()
		{
			InitializeComponent();
			InitializeApp();
			this.btnShow.Click += new EventHandler(btnShow_Click);
		}


		public void InitializeApp()
		{
			BindingList<MenuItem> source = new BindingList<MenuItem>();

			for (int i = 0; i < 10; i++)
			{
				source.Add(new MenuItem
				{
					MenuId = i,
					MenuName = string.Format("menu{0}", i),
					Count = i * 105,
					UnitPrice = i * 15.3d,
				});
			}

			dataGridView.Columns.Clear();
			dataGridView.AutoGenerateColumns = true;
			//this.dataGridView.SummaryDefaultFormatString = "#,##0.##";
			dataGridView.SummaryColumns = new string[] { "Count", "Price", "MenuName" };
			dataGridView.SummaryColumnsFormat = new string[] { "#,##0 개", "#,##0 원" };

			dataGridView.SummaryRowSpace = 1;
			dataGridView.SummaryRowBackColor = Color.Wheat;

			//Show the SummaryBox
			dataGridView.SummaryRowVisible = true;
			//dataGridView.CreateSummaryRow();

			dataGridView.DataSource = source;

			var summary = dataGridView.SummaryValues;

			foreach (var item in summary)
			{
				Console.WriteLine("{0}: {1}", item.Key, item.Value);
			}
		}

		private void btnShow_Click(object sender, EventArgs e)
		{
			this.dataGridView.SummaryRowVisible = !dataGridView.SummaryRowVisible;
		}
	}

	internal class MenuItem
	{
		public string MenuName
		{
			get;
			set;
		}

		public int Count
		{
			get;
			set;
		}

		public double UnitPrice
		{
			get;
			set;
		}

		public double Price
		{
			get
			{
				return Count * UnitPrice;
			}
		}

		public int MenuId
		{
			get;
			set;
		}
	}
}
