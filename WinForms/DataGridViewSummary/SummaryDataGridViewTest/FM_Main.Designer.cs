﻿using System.Windows.Forms;
namespace SummaryDataGridViewTest
{
	partial class FM_Main
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlButton = new System.Windows.Forms.Panel();
			this.btnShow = new System.Windows.Forms.Button();
			this.dataGridView = new System.Windows.Forms.DataGridViewSummary();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.pnlButton.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// pnlButton
			// 
			this.pnlButton.Controls.Add(this.btnShow);
			this.pnlButton.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlButton.Location = new System.Drawing.Point(6, 239);
			this.pnlButton.Name = "pnlButton";
			this.pnlButton.Size = new System.Drawing.Size(676, 50);
			this.pnlButton.TabIndex = 4;
			// 
			// btnShow
			// 
			this.btnShow.Location = new System.Drawing.Point(14, 14);
			this.btnShow.Name = "btnShow";
			this.btnShow.Size = new System.Drawing.Size(155, 27);
			this.btnShow.TabIndex = 1;
			this.btnShow.Text = "Show/Hide Summary";
			this.btnShow.UseVisualStyleBackColor = true;
			// 
			// dataGridView
			// 
			this.dataGridView.AllowUserToOrderColumns = true;
			this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
			this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView.Location = new System.Drawing.Point(6, 5);
			this.dataGridView.Margin = new System.Windows.Forms.Padding(0);
			this.dataGridView.Name = "dataGridView";
			this.dataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.dataGridView.Size = new System.Drawing.Size(676, 234);
			this.dataGridView.SummaryColumns = new string[] {
        "UnitPrice"};
			this.dataGridView.SummaryColumnsFormat = null;
			this.dataGridView.SummaryRowBackColor = System.Drawing.Color.Empty;
			this.dataGridView.SumRowHeaderText = "SUM";
			this.dataGridView.TabIndex = 5;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Column1";
			this.Column1.Name = "Column1";
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Column2";
			this.Column2.Name = "Column2";
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Column3";
			this.Column3.Name = "Column3";
			// 
			// Column4
			// 
			this.Column4.HeaderText = "Column4";
			this.Column4.Name = "Column4";
			// 
			// Column5
			// 
			this.Column5.HeaderText = "Column5";
			this.Column5.Name = "Column5";
			// 
			// FM_Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(688, 294);
			this.Controls.Add(this.dataGridView);
			this.Controls.Add(this.pnlButton);
			this.Name = "FM_Main";
			this.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
			this.Text = "Summary DGV";
			this.pnlButton.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlButton;
		private System.Windows.Forms.Button btnShow;
		private DataGridViewSummary dataGridView;
		private DataGridViewTextBoxColumn Column1;
		private DataGridViewTextBoxColumn Column2;
		private DataGridViewTextBoxColumn Column3;
		private DataGridViewTextBoxColumn Column4;
		private DataGridViewTextBoxColumn Column5;
	}
}

