using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Drawing;

namespace System.Windows.Forms
{
	/// <summary>
	/// Class ReadOnlyTextBox.
	/// </summary>
	public partial class ReadOnlyTextBox : Control
	{

		StringFormat format;
		/// <summary>
		/// Initializes a new instance of the <see cref="ReadOnlyTextBox"/> class.
		/// </summary>
		public ReadOnlyTextBox()
		{
			InitializeComponent();

			format = new StringFormat(StringFormatFlags.NoWrap | StringFormatFlags.FitBlackBox | StringFormatFlags.MeasureTrailingSpaces);
			format.LineAlignment = StringAlignment.Center;

			this.Height = 10;
			this.Width = 10;

			this.Padding = new Padding(2);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ReadOnlyTextBox"/> class.
		/// </summary>
		/// <param name="container">The container.</param>
		public ReadOnlyTextBox(IContainer container)
		{
			container.Add(this);
			InitializeComponent();

			this.TextChanged += new EventHandler(ReadOnlyTextBox_TextChanged);
		}

		private void ReadOnlyTextBox_TextChanged(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(FormatString) && !string.IsNullOrEmpty(Text))
			{
				Text = string.Format(FormatString, Text);
			}
		}

		private Color borderColor = Color.Black;
		/// <summary>
		/// Gets or sets a value indicating whether this instance is summary.
		/// </summary>
		/// <value>true if this instance is summary; otherwise, false.</value>
		public bool IsSummary
		{
			get;
			set;
		}
		/// <summary>
		/// Gets or sets a value indicating whether this instance is last column.
		/// </summary>
		/// <value>true if this instance is last column; otherwise, false.</value>
		public bool IsLastColumn
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the format string.
		/// </summary>
		/// <value>The format string.</value>
		public string FormatString
		{
			get;
			set;
		}

		private HorizontalAlignment textAlign = HorizontalAlignment.Left;
		/// <summary>
		/// Gets or sets the text align.
		/// </summary>
		/// <value>The text align.</value>
		[DefaultValue(HorizontalAlignment.Left)]
		public HorizontalAlignment TextAlign
		{
			get
			{
				return textAlign;
			}
			set
			{
				textAlign = value;
				setFormatFlags();
			}
		}

		private StringTrimming trimming = StringTrimming.None;
		/// <summary>
		/// Gets or sets the trimming.
		/// </summary>
		/// <value>The trimming.</value>
		[DefaultValue(StringTrimming.None)]
		public StringTrimming Trimming
		{
			get
			{
				return trimming;
			}
			set
			{
				trimming = value;
				setFormatFlags();
			}
		}

		private void setFormatFlags()
		{
			format.Alignment = TextHelper.TranslateAligment(TextAlign);
			format.Trimming = trimming;
		}

		/// <summary>
		/// Gets or sets the color of the border.
		/// </summary>
		/// <value>The color of the border.</value>
		public Color BorderColor
		{
			get
			{
				return borderColor;
			}
			set
			{
				borderColor = value;
			}
		}

		/// <summary>
		/// <see cref="E:System.Windows.Forms.Control.Paint" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.Windows.Forms.PaintEventArgs" />입니다.</param>
		protected override void OnPaint(PaintEventArgs e)
		{
			int subWidth = 0;
			Rectangle textBounds;

			if (string.IsNullOrEmpty(FormatString) == false && string.IsNullOrEmpty(Text) == false)
			{
				Text = String.Format("{0:" + FormatString + "}", Convert.ToDecimal(Tag));
			}

			textBounds = new Rectangle(this.ClientRectangle.X + 2, this.ClientRectangle.Y + 2, this.ClientRectangle.Width - 2, this.ClientRectangle.Height - 2);
			using (Pen pen = new Pen(borderColor))
			{
				if (IsLastColumn)
					subWidth = 1;

				e.Graphics.FillRectangle(new SolidBrush(this.BackColor), this.ClientRectangle);
				e.Graphics.DrawRectangle(pen, this.ClientRectangle.X, this.ClientRectangle.Y, this.ClientRectangle.Width - subWidth, this.ClientRectangle.Height - 1);
				e.Graphics.DrawString(Text, Font, Brushes.Black, textBounds, format);
			}
		}
	}
}


