﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Windows.Forms
{
	/// <summary>
	/// Class DataGridViewSummaryColumn.
	/// </summary>
	public class DataGridViewSummaryColumn
	{
		/// <summary>
		/// Alignment를 구하거나 설정합니다.
		/// </summary>
		/// <value>Alignment를 반환합니다.</value>
		public DataGridViewContentAlignment Alignment
		{
			get;
			set;
		}

		/// <summary>
		/// SummaryTypes를 구하거나 설정합니다.
		/// </summary>
		/// <value>SummaryTypes를 반환합니다.</value>
		public DataGridViewSummaryTypes SummaryType
		{
			get;
			set;
		}

		/// <summary>
		/// ColumnName를 구하거나 설정합니다.
		/// </summary>
		/// <value>ColumnName를 반환합니다.</value>
		public string Column
		{
			get;
			set;
		}

		/// <summary>
		/// Format를 구하거나 설정합니다.
		/// </summary>
		/// <value>Format를 반환합니다.</value>
		public string Format
		{
			get;
			set;
		}

		/// <summary>
		/// Tag를 구하거나 설정합니다.
		/// </summary>
		/// <value>Tag를 반환합니다.</value>
		public object Tag
		{
			get;
			set;
		}
	}
}
