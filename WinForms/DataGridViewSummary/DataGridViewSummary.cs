// ****************************************************************************************************************** //
//	Domain		:	System.Windows.Forms.DataGridViewSummary
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2015년 7월 7일 화요일 오전 10:08
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	http://www.codeproject.com/Articles/51889/Summary-DataGridView
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="DataGridViewSummary.cs" company="DeliveryZoneI Inc.">
//		Copyright (c) 2015. DeliveryZoneI Inc. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Collections.Generic;

namespace System.Windows.Forms
{
	/// <summary>
	/// Class DataGridViewSummary.
	/// <remarks>Todo. Add RightToLeft Support for ReadOnlyTextbox</remarks>
	/// </summary>
	[DefaultProperty("SummaryColumns")]
	public partial class DataGridViewSummary : DataGridView, ISupportInitialize
	{
		private string[] _summaryColumns;
		private int _summaryRowSpace;

		#region Constants
		/// <summary>
		/// The summary default format string
		/// </summary>
		internal const string SUMMARY_DEFAULT_FORMAT_STRING = "#,##0.##";
		#endregion

		#region Constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="DataGridViewSummary"/> class.
		/// </summary>
		public DataGridViewSummary()
		{
			InitializeComponent();

			refBox = new TextBox();
			panel = new Panel();
			spacePanel = new Panel();
			hScrollBar = new HScrollBar();
			summaryControl = new SummaryControlContainer(this);

			SummaryDefaultFormatString = SUMMARY_DEFAULT_FORMAT_STRING;
			DisplaySumRowHeader = true;
			SumRowHeaderTextBold = true;
			SummaryRowVisible = true;
			SummaryRowSpace = 1;

			summaryControl.VisibilityChanged += new EventHandler(summaryControl_VisibilityChanged);

			Resize += new EventHandler(DataGridControlSum_Resize);
			RowHeadersWidthChanged += new EventHandler(DataGridControlSum_Resize);
			ColumnAdded += new DataGridViewColumnEventHandler(DataGridControlSum_ColumnAdded);
			ColumnRemoved += new DataGridViewColumnEventHandler(DataGridControlSum_ColumnRemoved);
			DataBindingComplete += (sender, e) =>
			{
				CreateSummaryRow();
				summaryControl.RefreshSummary();
			};

			hScrollBar.Scroll += new ScrollEventHandler(hScrollBar_Scroll);
			hScrollBar.VisibleChanged += new EventHandler(hScrollBar_VisibleChanged);

			hScrollBar.Top += summaryControl.Bottom;
			hScrollBar.Minimum = 0;
			hScrollBar.Maximum = 0;
			hScrollBar.Value = 0;
			SummaryRowBackColor = Color.White;
		}

		#endregion

		#region Browsable properties
		/// <summary>
		/// Gets or sets a value indicating whether [display sum row header].
		/// </summary>
		/// <value>true if [display sum row header]; otherwise, false.</value>
		[Browsable(true), Category("Summary")]
		[DefaultValue(true)]
		public bool DisplaySumRowHeader
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the sum row header text.
		/// </summary>
		/// <value>The sum row header text.</value>
		[Browsable(true), Category("Summary")]
		[DefaultValue("")]
		public string SumRowHeaderText
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets a value indicating whether [sum row header text bold].
		/// </summary>
		/// <value>true if [sum row header text bold]; otherwise, false.</value>
		[Browsable(true), Category("Summary")]
		[DefaultValue(true)]
		public bool SumRowHeaderTextBold
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the summary columns.
		/// </summary>
		/// <value>The summary columns.</value>
		[Browsable(true),
		Category("Summary")]
		public string[] SummaryColumns
		{
			get
			{
				return _summaryColumns;
			}
			set
			{
				_summaryColumns = value;

				if (_summaryColumns != null)
				{
					summaryControl.RefreshSummary();
				}
			}
		}

		/// <summary>
		/// Gets or sets the summary columns format.
		/// </summary>
		/// <example>#,##0 EA</example>
		/// <value>The summary columns formatString.</value>
		[Browsable(true), Category("Summary")]
		public string[] SummaryColumnsFormat
		{
			get;
			set;
		}

		/// <summary>
		/// Display the summary Row
		/// </summary>
		private bool summaryRowVisible;

		/// <summary>
		/// Gets or sets a value indicating whether [summary row visible].
		/// </summary>
		/// <value>true if [summary row visible]; otherwise, false.</value>
		[Browsable(true), Category("Summary")]
		[DefaultValue(true)]
		public bool SummaryRowVisible
		{
			get
			{
				return summaryRowVisible;
			}
			set
			{
				summaryRowVisible = value;
				if (summaryControl != null && spacePanel != null)
				{
					summaryControl.Visible = value;
					spacePanel.Visible = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the summary row space.
		/// </summary>
		/// <value>The summary row space.</value>
		[Browsable(true),
		Category("Summary")]
		[DefaultValue(1)]
		public int SummaryRowSpace
		{
			get
			{
				return _summaryRowSpace;
			}
			set
			{
				if (_summaryRowSpace == value)
				{
					return;
				}

				_summaryRowSpace = value;
				spacePanel.Height = _summaryRowSpace;
			}
		}

		/// <summary>
		/// Gets or sets the format string.
		/// </summary>
		/// <value>The format string.</value>
		[Browsable(true), Category("Summary"), DefaultValue(SUMMARY_DEFAULT_FORMAT_STRING)]
		public string SummaryDefaultFormatString
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the color of the summary row back.
		/// </summary>
		/// <value>The color of the summary row back.</value>
		[Browsable(true), Category("Summary")]
		[DefaultValue(typeof(Color), "0xFFFFFF")]
		public Color SummaryRowBackColor
		{
			get
			{
				return summaryControl.SummaryRowBackColor;
			}
			set
			{
				summaryControl.SummaryRowBackColor = value;
			}
		}

		/// <summary>
		/// advoid user from setting the scrollbars manually
		/// </summary>
		/// <value>The scroll bars.</value>
		/// <returns>
		///   <see cref="T:System.Windows.Forms.ScrollBars" /> 값 중 하나입니다. 기본값은 <see cref="F:System.Windows.Forms.ScrollBars.Both" />입니다.
		///   </returns>
		///   <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence" />
		///   <IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   </PermissionSet>
		[Browsable(false)]
		public new ScrollBars ScrollBars
		{
			get
			{
				return base.ScrollBars;
			}
			set
			{
				base.ScrollBars = value;
			}
		}

		/// <summary>
		/// Gets the summary value.
		/// </summary>
		/// <value>The summary value.</value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Dictionary<string, object> SummaryValues
		{
			get
			{
				return summaryControl.SummaryValues;
			}
		}

		#endregion

		#region Declare variables

		/// <summary>
		/// Occurs when [create summary].
		/// </summary>
		public event EventHandler CreateSummary;
		private HScrollBar hScrollBar;
		private SummaryControlContainer summaryControl;
		private Panel panel, spacePanel;
		private TextBox refBox;

		#endregion

		#region public functions

		/// <summary>
		/// Refresh the summary
		/// </summary>
		public void RefreshSummary()
		{
			if (this.summaryControl != null)
				this.summaryControl.RefreshSummary();
		}

		#endregion

		#region Calculate Columns and Scrollbars width
		private void DataGridControlSum_ColumnRemoved(object sender, DataGridViewColumnEventArgs e)
		{
			calculateColumnsWidth();
			summaryControl.Width = columnsWidth;
			hScrollBar.Maximum = Convert.ToInt32(columnsWidth);
			resizeHScrollBar();
		}
		private void DataGridControlSum_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
		{
			calculateColumnsWidth();
			summaryControl.Width = columnsWidth;
			hScrollBar.Maximum = Convert.ToInt32(columnsWidth);
			resizeHScrollBar();
		}

		int columnsWidth = 0;
		/// <summary>
		/// Calculate the width of all visible columns
		/// </summary>
		private void calculateColumnsWidth()
		{
			columnsWidth = 0;
			for (int iCnt = 0; iCnt < ColumnCount; iCnt++)
			{
				if (Columns[iCnt].Visible)
				{
					if (Columns[iCnt].AutoSizeMode == DataGridViewAutoSizeColumnMode.Fill)
					{
						columnsWidth += Columns[iCnt].MinimumWidth;
					}
					else
						columnsWidth += Columns[iCnt].Width;
				}
			}
		}

		#endregion

		#region Other Events and delegates

		/// <summary>
		/// Moves viewable area of DataGridView according to the position of the scrollbar
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void hScrollBar_Scroll(object sender, ScrollEventArgs e)
		{
			int position = Convert.ToInt32((Convert.ToDouble(e.NewValue) / (Convert.ToDouble(hScrollBar.Maximum) / Convert.ToDouble(Columns.Count))));
			if (position < Columns.Count)
				FirstDisplayedScrollingColumnIndex = position;
		}

		/// <summary>
		/// Creates the summary row.
		/// </summary>
		public void CreateSummaryRow()
		{
			OnCreateSummary(this, EventArgs.Empty);
		}

		/// <summary>
		/// Calls the CreateSummary event
		/// </summary>
		private void OnCreateSummary(object sender, EventArgs e)
		{
			if (CreateSummary != null)
				CreateSummary(sender, e);
		}

		#endregion

		#region Adjust summaryControl, scrollbar

		/// <summary>
		/// Position the summaryControl under the
		/// DataGridView
		/// </summary>
		private void AdjustSumControlToGrid()
		{
			if (summaryControl == null || Parent == null)
			{
				return;
			}

			summaryControl.Top = Height + SummaryRowSpace;
			summaryControl.Left = Left;
			summaryControl.Width = Width;
		}

		/// <summary>
		/// Position the hScrollbar under the summaryControl
		/// </summary>
		private void AdjustScrollbarToSummaryControl()
		{
			if (Parent != null)
			{
				hScrollBar.Top = refBox.Height + 2;
				hScrollBar.Width = Width;
				hScrollBar.Left = Left;

				resizeHScrollBar();
			}
		}

		/// <summary>
		/// Resizes the horizontal scrollbar acording
		/// to the with of the client size and maximum size of the scrollbar
		/// </summary>
		private void resizeHScrollBar()
		{
			//Is used to calculate the LageChange of the scrollbar
			int vscrollbarWidth = 0;
			if (VerticalScrollBar.Visible)
				vscrollbarWidth = VerticalScrollBar.Width;

			int rowHeaderWith = RowHeadersVisible ? RowHeadersWidth : 0;

			if (columnsWidth > 0)
			{
				//This is neccessary if AutoGenerateColumns = true because DataGridControlSum_ColumnAdded won't be fired
				if (hScrollBar.Maximum == 0)
					hScrollBar.Maximum = columnsWidth;

				//Calculate how much of the columns are visible in %
				int scrollBarWidth = Convert.ToInt32(Convert.ToDouble(ClientSize.Width - RowHeadersWidth - vscrollbarWidth) / (Convert.ToDouble(hScrollBar.Maximum) / 100F));

				if (scrollBarWidth > 100 || columnsWidth + rowHeaderWith < ClientSize.Width)
				{
					if (hScrollBar.Visible)
					{
						hScrollBar.Visible = false;
					}
				}
				else if (scrollBarWidth > 0)
				{
					if (!hScrollBar.Visible)
					{
						hScrollBar.Visible = true;
					}
					hScrollBar.LargeChange = hScrollBar.Maximum / 100 * scrollBarWidth;
					hScrollBar.SmallChange = hScrollBar.LargeChange / 5;
				}
			}
		}

		private void DataGridControlSum_Resize(object sender, EventArgs e)
		{
			if (Parent != null)
			{
				calculateColumnsWidth();
				resizeHScrollBar();
				AdjustSumControlToGrid();
				AdjustScrollbarToSummaryControl();
			}
		}

		/// <summary>
		/// Recalculate the width of the summary control according to 
		/// the state of the scrollbar
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void hScrollBar_VisibleChanged(object sender, EventArgs e)
		{
			if (Parent != null)
			{
				//only perform operation if parent is visible
				if (Parent.Visible)
				{
					int height;
					if (hScrollBar.Visible)
						height = summaryControl.InitialHeight + hScrollBar.Height;
					else
						height = summaryControl.InitialHeight;

					if (summaryControl.Height != height && summaryControl.Visible)
					{
						summaryControl.Height = height;
						this.Height = panel.Height - summaryControl.Height - SummaryRowSpace;
					}
				}
			}
		}

		/// <summary>
		/// Recalculate the height of the DataGridView
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void summaryControl_VisibilityChanged(object sender, EventArgs e)
		{
			if (!summaryControl.Visible)
			{
				ScrollBars = ScrollBars.Both;
				this.Height = panel.Height;
			}
			else
			{
				this.Height = panel.Height - summaryControl.Height - SummaryRowSpace;
				ScrollBars = ScrollBars.Vertical;
			}
		}

		/// <summary>
		/// When the DataGridView is visible for the first time a panel is created.
		/// The DataGridView is then removed from the parent control and added as
		/// child to the newly created panel
		/// </summary>
		private void ChangeParent()
		{
			if (!DesignMode && Parent != null)
			{
				summaryControl.InitialHeight = this.refBox.Height;
				summaryControl.Height = summaryControl.InitialHeight;
				summaryControl.BackColor = this.RowHeadersDefaultCellStyle.BackColor;
				summaryControl.ForeColor = Color.Transparent;
				summaryControl.RightToLeft = this.RightToLeft;
				panel.Bounds = this.Bounds;
				panel.BackColor = this.BackgroundColor;

				panel.Dock = this.Dock;
				panel.Anchor = this.Anchor;
				panel.Padding = this.Padding;
				panel.Margin = this.Margin;
				panel.Top = this.Top;
				panel.Left = this.Left;
				panel.BorderStyle = this.BorderStyle;

				Margin = new Padding(0);
				Padding = new Padding(0);
				Top = 0;
				Left = 0;

				summaryControl.Dock = DockStyle.Bottom;
				this.Dock = DockStyle.Fill;

				if (this.Parent is TableLayoutPanel)
				{
					int rowSpan, colSpan;

					TableLayoutPanel tlp = this.Parent as TableLayoutPanel;
					TableLayoutPanelCellPosition cellPos = tlp.GetCellPosition(this);

					rowSpan = tlp.GetRowSpan(this);
					colSpan = tlp.GetColumnSpan(this);

					tlp.Controls.Remove(this);
					tlp.Controls.Add(panel, cellPos.Column, cellPos.Row);
					tlp.SetRowSpan(panel, rowSpan);
					tlp.SetColumnSpan(panel, colSpan);
				}
				else
				{
					Control parent = this.Parent;
					//remove DataGridView from ParentControls
					parent.Controls.Remove(this);
					parent.Controls.Add(panel);
				}

				this.BorderStyle = BorderStyle.None;

				panel.BringToFront();


				hScrollBar.Top = refBox.Height + 2;
				hScrollBar.Width = this.Width;
				hScrollBar.Left = this.Left;

				summaryControl.Controls.Add(hScrollBar);
				hScrollBar.BringToFront();
				panel.Controls.Add(this);

				spacePanel = new Panel();
				spacePanel.BackColor = panel.BackColor;
				spacePanel.Height = SummaryRowSpace;
				spacePanel.Dock = DockStyle.Bottom;

				panel.Controls.Add(spacePanel);
				panel.Controls.Add(summaryControl);

				resizeHScrollBar();
				AdjustSumControlToGrid();
				AdjustScrollbarToSummaryControl();

				resizeHScrollBar();
			}
		}

		#endregion

		#region ISupportInitialzie

		/// <summary>
		/// 초기화가 시작됨을 개체에 알립니다.
		/// </summary>
		public void BeginInit()
		{
		}

		/// <summary>
		/// 초기화가 완료됨을 개체에 알립니다.
		/// </summary>
		public void EndInit()
		{
			ChangeParent();
		}

		#endregion
	}
}