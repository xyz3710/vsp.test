using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Forms
{
	/// <summary>
	/// Enum SummaryTypes
	/// </summary>
	public enum DataGridViewSummaryTypes
	{
		/// <summary>
		/// The count
		/// </summary>
		Count,
		/// <summary>
		/// The summary
		/// </summary>
		Summary,
		/// <summary>
		/// The average
		/// </summary>
		Average,
	}
}
