using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace System.Windows.Forms
{
	/// <summary>
	/// Class TextHelper.
	/// </summary>
	public static class TextHelper
	{
		/// <summary>
		/// Translates the aligment.
		/// </summary>
		/// <param name="aligment">The aligment.</param>
		/// <returns>StringAlignment.</returns>
		public static StringAlignment TranslateAligment(HorizontalAlignment aligment)
		{
			if (aligment == HorizontalAlignment.Left)
				return StringAlignment.Near;
			else if (aligment == HorizontalAlignment.Right)
				return StringAlignment.Far;
			else
				return StringAlignment.Center;
		}

		/// <summary>
		/// Translates the grid column aligment.
		/// </summary>
		/// <param name="aligment">The aligment.</param>
		/// <returns>HorizontalAlignment.</returns>
		public static HorizontalAlignment TranslateGridColumnAligment(DataGridViewContentAlignment aligment)
		{
			if (aligment == DataGridViewContentAlignment.BottomLeft || aligment == DataGridViewContentAlignment.MiddleLeft || aligment == DataGridViewContentAlignment.TopLeft)
				return HorizontalAlignment.Left;
			else if (aligment == DataGridViewContentAlignment.BottomRight || aligment == DataGridViewContentAlignment.MiddleRight || aligment == DataGridViewContentAlignment.TopRight)
				return HorizontalAlignment.Right;
			else
				return HorizontalAlignment.Left;
		}

		/// <summary>
		/// Translates the aligment to flag.
		/// </summary>
		/// <param name="aligment">The aligment.</param>
		/// <returns>TextFormatFlags.</returns>
		public static TextFormatFlags TranslateAligmentToFlag(HorizontalAlignment aligment)
		{
			if (aligment == HorizontalAlignment.Left)
				return TextFormatFlags.Left;
			else if (aligment == HorizontalAlignment.Right)
				return TextFormatFlags.Right;
			else
				return TextFormatFlags.HorizontalCenter;
		}

		/// <summary>
		/// Translates the trimming to flag.
		/// </summary>
		/// <param name="trimming">The trimming.</param>
		/// <returns>TextFormatFlags.</returns>
		public static TextFormatFlags TranslateTrimmingToFlag(StringTrimming trimming)
		{
			if (trimming == StringTrimming.EllipsisCharacter)
				return TextFormatFlags.EndEllipsis;
			else if (trimming == StringTrimming.EllipsisPath)
				return TextFormatFlags.PathEllipsis;
			if (trimming == StringTrimming.EllipsisWord)
				return TextFormatFlags.WordEllipsis;
			if (trimming == StringTrimming.Word)
				return TextFormatFlags.WordBreak;
			else
				return TextFormatFlags.Default;
		}
	}
}
