﻿using System;
using System.Windows.Forms;
using AnyFactory.FX.Win.Controls.Extenders;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;
using AnyFactory.FX.Win.Loader.Help;
using AnyFactory.FX.Win.Utility;
using AnyFactory.FX.Win.Configuration;
using System.IO;
using System.Diagnostics;

namespace TimerMessageBox
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Text = "debug me!!";
			btnTest.Select();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			return;
			MessageBox.Show("정보",
				Text,
				MessageBoxButtons.OK,
				MessageBoxIcon.Information);
			MessageBox.Show("경고",
				Text,
				MessageBoxButtons.OK,
				MessageBoxIcon.Warning);
			MessageBox.Show("에러",
				Text,
				MessageBoxButtons.OK,
				MessageBoxIcon.Error);
			if (MessageBox.Show("질문",
					Text,
					MessageBoxButtons.YesNo,
					MessageBoxIcon.Question) == DialogResult.Yes)
			{
				
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			#region ForTestResaon
			// TEST: ForTestResaon in button2_Click
			// by KIMKIWON\xyz37 in 2009년 11월 5일 목요일 오후 9:47
			/*
			string path = @"D:\iSet-DA.ESG\Win\trunk1";
			string searchPattern = "?????????";
			SearchOption searchOption = SearchOption.AllDirectories;
			string[] excluedPatterns = new string[] { ".svn",
						   "obj",
						   "bin",
						   "Properties" };
			_folderCacheManager = new FolderCacheManager(path,
									  searchPattern,
									  searchOption,
									  excluedPatterns,
									  true)
			{
				DefaultFileName = "AnyFactory.FX.Win.Loader.CacheIndex.xml",
			};

			List<string> searchIndex = _folderCacheManager.SearchIndex(null, "MP0F0010");
			*/
			#endregion
		}

		private void btnCount1_Click(object sender, EventArgs e)
		{
			btnConfirm1.Tag = btnConfirm1.Text;
			AddCounter(btnConfirm1, 5);
		}

		private void btnCount2_Click(object sender, EventArgs e)
		{
			//btnConfirm2.AddCounterText(3, btnConfirm2_Click);
			btnConfirm1.Click += btnConfirm1_Click;
			btnConfirm1.Click += btnConfirm2_Click;

			Type type = btnConfirm1.GetType();
			Control targetControl = btnConfirm1;
			MethodInfo mi = type.GetMethod("get_Events", BindingFlags.Instance | BindingFlags.NonPublic);

			if (mi != null)
			{
				EventHandlerList eventHandlerList = mi.Invoke(targetControl, null) as EventHandlerList;

				if (eventHandlerList != null)
				{
					FieldInfo fi = GetParentControlType(targetControl).GetField("EventMouseClick", BindingFlags.NonPublic | BindingFlags.Static);

					if (fi != null)
					{
						object mouseClickKey = fi.GetValue(targetControl);
						Delegate mouseClickDelegate = eventHandlerList[mouseClickKey];

						if (mouseClickDelegate != null)
							mouseClickDelegate.DynamicInvoke(targetControl, null);
					}
				}
			}
		}

		/// <summary>
		/// WinForm Control의 최상위 Control type을 구합니다.
		/// </summary>
		/// <param name="targetControl"></param>
		/// <returns></returns>
		private Type GetParentControlType(Control targetControl)
		{
			Type type = targetControl.GetType();
			Queue<Type> queue = new Queue<Type>();

			queue.Enqueue(type);

			while (queue.Count > 0)
			{
				Type targetBaseType = queue.Dequeue().BaseType;

				if (targetBaseType == typeof(System.Windows.Forms.Control))
				{
					// Form -> ContainerControl -> ScrollableControl -> Control 이와 같이 총 네 번의 상속을 받으므로 
					return targetBaseType;
				}
				else if (targetBaseType != typeof(object))
					queue.Enqueue(targetBaseType);
			}

			return type;
		}

		private void btnConfirm1_Click(object sender, EventArgs e)
		{
			MessageBox.Show(string.Format("{0}가 눌렸습니다.", (sender as Button).Text));
		}

		private void btnConfirm2_Click(object sender, EventArgs e)
		{
			MessageBox.Show(string.Format("{0}가 눌렸습니다.", (sender as Button).Text));
		}

		private void AddCounter(Button button, int count)
		{
			button.AddCounterAction(
				count,
				sender =>
				{
					btnConfirm1_Click(sender, new EventArgs());
				},
				(sender, tickedCount) =>
				{
					button.Text = string.Format("{0}({1})", button.Tag, tickedCount);
				},
				1000);
		}

		#region FolderCacheManagerTest
		private void btnTest_Click(object sender, EventArgs e)
		{
			InitFolderCacheManager();
			//_folderCacheManager.RefreshCache();
			//Text = _folderCacheManager.Folders.Count.ToString();
			
			List<string> searchIndex = _folderCacheManager.SearchIndex(false, "Test blank", "UMMCommon", "RP0F0170");
			OpenProject("RP0F0170");
		}

		private FolderCacheManager _folderCacheManager;

		private void InitFolderCacheManager()
		{
			string path = LoaderSetting.CacheFolderManagerRootPath;
			string searchPattern = LoaderSetting.CacheFolderManagerRootSearchPattern;
			SearchOption searchOption = SearchOption.AllDirectories;
			string[] excluedPatterns = new string[] { ".svn",
						   "obj",
						   "bin",
						   "Properties" };
			_folderCacheManager = new FolderCacheManager(path, searchPattern, searchOption, excluedPatterns, true)
			{
				DefaultFileName = "AnyFactory.FX.Win.Loader.CacheIndex.xml",
			};
		}

		private bool NotFoundAction()
		{
			return (MessageBox.Show(MessageSettings.GetValue("QuestNotFoundInCacheAndRescan"),
					Text,
					MessageBoxButtons.YesNo,
					MessageBoxIcon.Question) == DialogResult.Yes);
		}

		private void OpenProject(string formId)
		{
			// 개발자를 위해 해당 프로젝트를 open 한다.
			if (_folderCacheManager.CheckIndexFilePath() == false)
			{
				if (MessageBox.Show(string.Format(
										"{0}\r\n{1}",
										_folderCacheManager.RootPath,
										MessageSettings.GetValue("QuestIndexReCreation")),
						Text,
						MessageBoxButtons.YesNo,
						MessageBoxIcon.Question) == DialogResult.No)
					return;
			}

			List<string> searchedIndexes = _folderCacheManager.SearchIndex(NotFoundAction, formId);

			if (searchedIndexes.Count == 0)
			{
				MessageBox.Show(MessageSettings.GetValue("ProjectNotFound"),	// 지정한 프로젝트를 찾을 수 없어서 프로젝트를 로드할 수 없습니다.
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Warning);
				return;
			}

			foreach (string projectDir in searchedIndexes)
			{
				string projectFile = string.Format("{0}.csproj", projectDir);

				if (File.Exists(projectFile) == false)
					continue;

				if (MessageBox.Show(string.Format(
										MessageSettings.GetValue("QuestLoadProjectInVisualStudio"),		// {0} 폴더의\r\n{1} 프로젝트를 Visual Studio로 로드 하시겠습니까?
										Path.GetDirectoryName(projectFile),
										Path.GetFileName(projectFile)),
						Text,
						MessageBoxButtons.YesNo,
						MessageBoxIcon.Question) == DialogResult.Yes)
				{
					Process process = new Process()
					{
						StartInfo = new ProcessStartInfo(projectFile),
					};

					process.Start();
					Close();                    
				}
			}
		}
		#endregion
	}
}
