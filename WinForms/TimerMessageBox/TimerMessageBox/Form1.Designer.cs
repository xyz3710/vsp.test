﻿namespace TimerMessageBox
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCount1 = new System.Windows.Forms.Button();
			this.btnConfirm1 = new System.Windows.Forms.Button();
			this.btnConfirm2 = new System.Windows.Forms.Button();
			this.btnCount2 = new System.Windows.Forms.Button();
			this.btnTest = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnCount1
			// 
			this.btnCount1.Location = new System.Drawing.Point(13, 12);
			this.btnCount1.Name = "btnCount1";
			this.btnCount1.Size = new System.Drawing.Size(243, 23);
			this.btnCount1.TabIndex = 0;
			this.btnCount1.Text = "확인1 카운트 5";
			this.btnCount1.Click += new System.EventHandler(this.btnCount1_Click);
			// 
			// btnConfirm1
			// 
			this.btnConfirm1.Location = new System.Drawing.Point(13, 86);
			this.btnConfirm1.Name = "btnConfirm1";
			this.btnConfirm1.Size = new System.Drawing.Size(243, 23);
			this.btnConfirm1.TabIndex = 2;
			this.btnConfirm1.Text = "확 인1";
			// 
			// btnConfirm2
			// 
			this.btnConfirm2.Location = new System.Drawing.Point(13, 115);
			this.btnConfirm2.Name = "btnConfirm2";
			this.btnConfirm2.Size = new System.Drawing.Size(243, 23);
			this.btnConfirm2.TabIndex = 3;
			this.btnConfirm2.Text = "확 인2";
			// 
			// btnCount2
			// 
			this.btnCount2.Location = new System.Drawing.Point(15, 41);
			this.btnCount2.Name = "btnCount2";
			this.btnCount2.Size = new System.Drawing.Size(243, 23);
			this.btnCount2.TabIndex = 1;
			this.btnCount2.Text = "확인2 카운트 3";
			this.btnCount2.Click += new System.EventHandler(this.btnCount2_Click);
			// 
			// btnTest
			// 
			this.btnTest.Location = new System.Drawing.Point(13, 214);
			this.btnTest.Name = "btnTest";
			this.btnTest.Size = new System.Drawing.Size(243, 23);
			this.btnTest.TabIndex = 4;
			this.btnTest.Text = "&FolderCacheManagerTest";
			this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
			// 
			// Form1
			// 
			this.AcceptButton = this.btnTest;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(271, 249);
			this.Controls.Add(this.btnTest);
			this.Controls.Add(this.btnConfirm2);
			this.Controls.Add(this.btnConfirm1);
			this.Controls.Add(this.btnCount2);
			this.Controls.Add(this.btnCount1);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnCount1;
		private System.Windows.Forms.Button btnConfirm1;
		private System.Windows.Forms.Button btnConfirm2;
		private System.Windows.Forms.Button btnCount2;
		private System.Windows.Forms.Button btnTest;
	}
}