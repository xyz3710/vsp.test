﻿/**********************************************************************************************************************/
/*	Domain		:	UpdateModule.MessageForm
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 10월 30일 금요일 오후 12:51
/*	Purpose		:	Message를 시간안에 보여지도록 하는 form
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 10월 30일 금요일 오후 12:52
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UpdateModule
{
	public partial class MessageForm : Form
	{
		private int _countSec;
		private int _enableCloseButtonSec;

		public MessageForm()
			: this(20, 10)
		{
		}
		
		public MessageForm(int countSec, int enableCloseButtonSec)
		{
			InitializeComponent();
			_countSec = countSec;
			_enableCloseButtonSec = enableCloseButtonSec;

			if (_enableCloseButtonSec > 0)
				btnClose.Enabled = false;
		}

		private void MessageForm_Shown(object sender, EventArgs e)
		{
			Timer timer = new Timer();
			int count = _countSec;

			timer.Interval = 1000;

			timer.Tick += new EventHandler(delegate(object s, EventArgs ea)
			{
				if (count == 0)
					Close();

				btnClose.Text = string.Format("확  인({0}초 남았습니다.)", count--);

				if (count == _enableCloseButtonSec)
					btnClose.Enabled = true;
			});

			timer.Start();
		}

		private void MessageForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
				Close();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
