﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Loader.Help.MenuIndexForm
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 10월 29일 목요일 오후 8:21
/*	Purpose		:	MenuView 정보를 조회 합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	Kim Ki Won
/*	Update		:	2009년 11월 17일 화요일 오후 9:13
/*	Changes		:	참조된 Assembly 정보가 조회되도록 수정
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 10월 30일 금요일 오전 11:10
/**********************************************************************************************************************/

#region Using
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using AnyFactory.FX.Win.Configuration;
using AnyFactory.FX.Win.Controls.UltraGridHelper;
using AnyFactory.FX.Win.Utility;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
#endregion

namespace AnyFactory.FX.Win.Loader.Help
{
	/// <summary>
	/// MenuIndexForm instance class를 초기화 합니다.
	/// </summary>
	public partial class MenuIndexForm : BaseForm
	{
		#region Constants
		private const string FORMID_KEY = "FormId";
		#endregion

		#region Fields
		private FolderCacheManager _folderCacheManager;
		private string _formId;
		#endregion

		#region Constructor
		/// <summary>
		/// MenuIndexForm의 인스턴스를 생성합니다.
		/// </summary>
		/// <param name="menuList">Grid에 binding할 menu list</param>
		/// <param name="resetLocation">처리후 reset할 위치</param>
		/// <param name="ownersAssembly">현재 참조하는 form의 assembly 정보입니다.</param>
		public MenuIndexForm(IEnumerable<MenuView> menuList, Point resetLocation, Assembly ownersAssembly)
			: this(menuList, resetLocation, ownersAssembly, String.Empty)
		{
		}

		/// <summary>
		/// MenuIndexForm의 인스턴스를 생성합니다.
		/// </summary>
		/// <param name="menuList">Grid에 binding할 menu list</param>
		/// <param name="resetLocation">처리후 reset할 위치</param>
		/// <param name="ownersAssembly">현재 참조하는 form의 assembly 정보입니다.</param>
		/// <param name="formId"></param>
		public MenuIndexForm(
			IEnumerable<MenuView> menuList,
			Point resetLocation,
			Assembly ownersAssembly,
			string formId)
			: base()
		{
			InitializeComponent();

			InitializeForm();
			MenuList = new List<MenuView>(menuList);
			ResetLocation = resetLocation;
			OwnersAssembly = ownersAssembly;
			FormId = formId;
		}
		#endregion

		#region InitializeForm
		private void InitializeForm()
		{
			base.ThemeStyle = ThemeStyle;
		}
		#endregion

		#region Properties
		private List<MenuView> MenuList
		{
			get;
			set;
		}

		/// <summary>
		/// ResetLocation를 구하거나 설정합니다.
		/// </summary>
		private Point ResetLocation
		{
			get;
			set;
		}

		/// <summary>
		/// MenuName를 구하거나 설정합니다.
		/// </summary>
		private string MenuName
		{
			get
			{
				return xtxtMenuName.Text;
			}
		}

		private Assembly OwnersAssembly
		{
			get;
			set;
		}

		/// <summary>
		/// FormId를 구하거나 설정합니다.
		/// </summary>
		private new string FormId
		{
			get
			{
				if (string.IsNullOrEmpty(_formId) == true && xgridList.ActiveRow != null)
				{
					_formId = Convert.ToString(xgridList.ActiveRow.Cells[FORMID_KEY].Value);

					if (_formId.Contains(":") == true)
						_formId = string.Empty;
				}

				return _formId;
			}
			set
			{
				_formId = value;
			}
		}
		#endregion

		#region Toolbar Event Handler
		/// <summary>
		/// 표준 툴바의 [신규] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnNewClick()
		{
			base.OnNewClick();
		}

		/// <summary>
		/// 표준 툴바의 [저장] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnSaveClick()
		{
			base.OnSaveClick();


		}

		/// <summary>
		/// 표준 툴바의 [조회] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnSearchClick()
		{
			base.OnSearchClick();

			BindMenuList();
			BindReferences();
		}

		/// <summary>
		/// 표준 툴바의 [삭제] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnDeleteClick()
		{
			base.OnDeleteClick();


		}

		/// <summary>
		/// 표준 툴바의 [실행] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnRunClick()
		{
			base.OnRunClick();

		}

		/// <summary>
		/// 표준 툴바의 [취소] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnCancelClick()
		{
			base.OnCancelClick();


		}

		/// <summary>
		/// 표준 툴바의 [행 추가] 버튼이 눌렸을 때<br/>
		/// <seealso cref="AnyFactory.FX.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnAddRowClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnAddRowClick();
		}

		/// <summary>
		/// 표준 툴바의 [행 삭제] 버튼이 눌렸을 때
		/// <seealso cref="AnyFactory.FX.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnDelRowClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnDelRowClick();
		}

		/// <summary>
		/// 표준 툴바의 [엑셀] 버튼이 눌렸을 때
		/// <seealso cref="AnyFactory.FX.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnExcelClick()
		{
			base.ToolbarTargetXGrid = xgridList;
			base.OnExcelClick();
		}

		/// <summary>
		/// 표준 툴바의 [미리보기] 버튼이 눌렸을 때
		/// <seealso cref="AnyFactory.FX.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnPreviewClick()
		{
			base.ToolbarTargetXGrid = xgridList;
			base.OnPreviewClick();
		}

		/// <summary>
		/// 표준 툴바의 [인쇄] 버튼이 눌렸을 때
		/// <seealso cref="AnyFactory.FX.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnPrintClick()
		{
			base.ToolbarTargetXGrid = xgridList;
			base.OnPrintClick();
		}

		/// <summary>
		/// 표준 툴바의 [창닫기] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnExitClick()
		{
			base.OnExitClick();


		}
		#endregion

		#region Event Methods
		private void MenuIndexForm_Load(object sender, EventArgs e)
		{
			if (LoaderWrapper.Instance.DebugMode == true)
				SetDebugMode();
			else
				SetNormal();
		}

		private void MenuIndexForm_Shown(object sender, EventArgs e)
		{
			SuspendLayout();
			Location = ResetLocation;
			ResumeLayout();

			if (FormId != string.Empty)
			{
				xtxtMenuName.Enabled = false;
				abSearch.Enabled = xtxtMenuName.Enabled;
				tlpMain.RowStyles[0].SizeType = SizeType.Absolute;
				tlpMain.RowStyles[0].Height = 0;
			}

			InitFolderCacheManager();
			SetFocus(xtxtMenuName);
			OnSearchClick();
		}

		private void MenuIndexForm_KeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.F12:
					OpenProject();

					break;
			}
		}

		private void xgridList_AfterRowActivate(object sender, EventArgs e)
		{
			FormId = string.Empty;
		}

		private void xbtnOpenProject_Click(object sender, EventArgs e)
		{
			OpenProject();
		}
		#endregion

		#region User defined Methods
		private void InitListGrid()
		{
			bool isDebug = !LoaderWrapper.Instance.DebugMode;
			UltraGridColumnHelper xcolApplicationId = new UltraGridColumnHelper(xgridList, "ApplicationId", 100);
			UltraGridColumnHelper xcolMenuName = new UltraGridColumnHelper(xgridList, "MenuName", 200);
			UltraGridColumnHelper xcolFormId = new UltraGridColumnHelper(xgridList, FORMID_KEY, 100);
			UltraGridColumnHelper xcolMenuDesc = new UltraGridColumnHelper(xgridList, "MenuDesc", 220);
			UltraGridColumnHelper xcolArguments = new UltraGridColumnHelper(xgridList, "Arguments", 100);
			UltraGridColumnHelper xcolFormName = new UltraGridColumnHelper(xgridList, "FormName", 100, false, isDebug);
			UltraGridColumnHelper xcolFormDesc = new UltraGridColumnHelper(xgridList, "FormDesc", 220, false, isDebug);
			UltraGridColumnHelper xcolFormPath = new UltraGridColumnHelper(xgridList, "FormPath", 100, false, isDebug);
			UltraGridColumnHelper xcolCanCreate = new UltraGridColumnHelper(xgridList, "CanCreate", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolCanRead = new UltraGridColumnHelper(xgridList, "CanRead", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolCanUpdate = new UltraGridColumnHelper(xgridList, "CanUpdate", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolCanDelete = new UltraGridColumnHelper(xgridList, "CanDelete", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolCanExecute = new UltraGridColumnHelper(xgridList, "CanExecute", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolCanUndo = new UltraGridColumnHelper(xgridList, "CanUndo", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolCanAddRow = new UltraGridColumnHelper(xgridList, "CanAddRow", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolCanDeleteRow = new UltraGridColumnHelper(xgridList, "CanDeleteRow", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolIsEnabled = new UltraGridColumnHelper(xgridList, "IsEnabled", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolIsVisible = new UltraGridColumnHelper(xgridList, "IsVisible", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolEnableDuplicatedOpen = new UltraGridColumnHelper(xgridList, "EnableDuplicatedOpen", UltraGridColumnStyle.CheckBox, 100);
			UltraGridColumnHelper xcolDepth = new UltraGridColumnHelper(xgridList, "Depth", UltraGridColumnStyle.Numeric, false, isDebug);
			UltraGridColumnHelper xcolSeq = new UltraGridColumnHelper(xgridList, "Seq", UltraGridColumnStyle.Numeric, false, isDebug);
			UltraGridColumnHelper xcolIsRoot = new UltraGridColumnHelper(xgridList, "IsRoot", UltraGridColumnStyle.CheckBox, 100, false, isDebug);
			UltraGridColumnHelper xcolHasChildren = new UltraGridColumnHelper(xgridList, "HasChildren", UltraGridColumnStyle.CheckBox, 100, false, isDebug);
			UltraGridColumnHelper xcolFormType = new UltraGridColumnHelper(xgridList, "FormType", 100, false, isDebug);
			UltraGridColumnHelper xcolIsContainer = new UltraGridColumnHelper(xgridList, "IsContainer", UltraGridColumnStyle.CheckBox, 100, false, isDebug);
			UltraGridColumnHelper xcolUserId = new UltraGridColumnHelper(xgridList, "UserId", 100, false, true);
			UltraGridColumnHelper xcolFormGroupId = new UltraGridColumnHelper(xgridList, "FormGroupId", 100, false, true);
			UltraGridColumnHelper xcolFormGroupName = new UltraGridColumnHelper(xgridList, "FormGroupName", 100, false, true);
			UltraGridColumnHelper xcolFormGroupDesc = new UltraGridColumnHelper(xgridList, "FormGroupDesc", 100, false, true);
			UltraGridColumnHelper xcolMenuId = new UltraGridColumnHelper(xgridList, "MenuId", 100, false, isDebug);
			UltraGridColumnHelper xcolParentMenuId = new UltraGridColumnHelper(xgridList, "ParentMenuId", 100, false, isDebug);

			xcolApplicationId.Fixed = true;
			xcolMenuName.Fixed = true;
			xcolFormId.Fixed = true;
			xcolApplicationId.SortIndicator = SortIndicator.Ascending;
			xcolDepth.SortIndicator = SortIndicator.Ascending;
			xcolSeq.SortIndicator = SortIndicator.Ascending;
			xcolMenuName.AllowRowFiltering = DefaultableBoolean.True;
			xcolFormName.AllowRowFiltering = DefaultableBoolean.True;
			xcolFormId.AllowRowFiltering = DefaultableBoolean.True;
			xcolUserId.AllowRowFiltering = DefaultableBoolean.True;
			xcolIsEnabled.AllowRowFiltering = DefaultableBoolean.True;

			UltraGridHelper.SetGridOptions(xgridList, false, false, false, true);
		}

		private void InitReferenceGrid()
		{
			UltraGridColumnHelper xcolName = new UltraGridColumnHelper(xgridReferences, "Name", 250);
			UltraGridColumnHelper xcolVersion = new UltraGridColumnHelper(xgridReferences, "Version", 120);
			UltraGridColumnHelper xcolFullName = new UltraGridColumnHelper(xgridReferences, "FullName", 400);
			UltraGridColumnHelper xcolCultureInfo = new UltraGridColumnHelper(xgridReferences, "CultureInfo", 120);
			UltraGridColumnHelper xcolProcessorArchitecture = new UltraGridColumnHelper(xgridReferences, "ProcessorArchitecture", 120);

			xcolName.Fixed = true;
			xcolVersion.Fixed = true;
			xcolName.SortIndicator = SortIndicator.Ascending;
			xcolVersion.SortIndicator = SortIndicator.Ascending;
			xcolName.AllowRowFiltering = DefaultableBoolean.True;
			xcolVersion.AllowRowFiltering = DefaultableBoolean.True;
			xcolFullName.AllowRowFiltering = DefaultableBoolean.True;
			xgridReferences.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

			UltraGridHelper.SetGridOptions(xgridReferences, false, false, false, true);
		}

		private void InitFolderCacheManager()
		{
			string path = LoaderSetting.CacheFolderManagerRootPath;
			string searchPattern = LoaderSetting.CacheFolderManagerRootSearchPattern;
			SearchOption searchOption = SearchOption.AllDirectories;
			string[] excluedPatterns = new string[] { ".svn",
						   "obj",
						   "bin",
						   "Properties" };
			_folderCacheManager = new FolderCacheManager(path,
									  searchPattern,
									  searchOption,
									  excluedPatterns,
									  true)
									  {
										  DefaultFileName = "AnyFactory.FX.Win.Loader.CacheIndex.xml",
									  };
		}

		private void ClearListGrid()
		{
			UltraGridHelper.DataBind(xgridList, null);
			InitListGrid();
		}

		private void ClearReferenceGrid()
		{
			UltraGridHelper.DataBind(xgridReferences, null);
			InitReferenceGrid();
		}

		private void BindMenuList()
		{
			ClearListGrid();

			List<MenuView> results = new List<MenuView>();

			results.AddRange(MenuList.FindAll(delegate(MenuView menuViewItem)
			{
				if (string.IsNullOrEmpty(MenuName) == false)
					return (menuViewItem.MenuName.ToLower().Contains(MenuName.ToLower()) == true);
				else if (FormId != string.Empty)
					return (menuViewItem.FormId.ToLower().Contains(FormId.ToLower()) == true);
				else
					return true;
			}));

			UltraGridHelper.DataBind(xgridList, results, true);
			xlblStatus.Text = string.Format(
								   string.Format("{{0:{0}}}{{1}}", GlobalFormat.IntegerFormatString),
								   results.Count,
								   MessageSettings.GetValue("QueryCompletedCount"));
		}

		private void BindReferences()
		{
			ClearReferenceGrid();

			if (OwnersAssembly == null)
				return;

			xgrbList.Text = string.Format("{0}, Version : {1}", xgrbList.Tag, OwnersAssembly.GetName().Version);

			List<AssemblyNameWrapper> list = GetAssemblyNameStartWithNamespace(OwnersAssembly, string.Empty)
				.Select(asmName => new AssemblyNameWrapper
				{
					Name = asmName.Name,
					Version = asmName.Version.ToString(),
					FullName = asmName.FullName,
					CultureInfo = asmName.CultureInfo.ToString(),
					ProcessorArchitecture = asmName.ProcessorArchitecture.ToString(),
				})
				.ToList();
			UltraGridHelper.DataBind(xgridReferences, list, true);
		}

		private bool NotFoundAction()
		{
			return (MessageBox.Show(MessageSettings.GetValue("QuestNotFoundInCacheAndRescan"),
					Text,
					MessageBoxButtons.YesNo,
					MessageBoxIcon.Question) == DialogResult.Yes);
		}

		private void OpenProject()
		{
			// 개발자를 위해 해당 프로젝트를 open 한다.
			if (_folderCacheManager.CheckIndexFilePath() == false)
			{
				if (MessageBox.Show(string.Format(
										"{0}\r\n{1}",
										_folderCacheManager.RootPath,
										MessageSettings.GetValue("QuestIndexReCreation")),
						Text,
						MessageBoxButtons.YesNo,
						MessageBoxIcon.Question) == DialogResult.No)
					return;
			}

			List<string> searchedIndexes = _folderCacheManager.SearchIndex(NotFoundAction, FormId);

			if (searchedIndexes.Count == 0)
			{
				MessageBox.Show(MessageSettings.GetValue("ProjectNotFound"),	// 지정한 프로젝트를 찾을 수 없어서 프로젝트를 로드할 수 없습니다.
					Text,
					MessageBoxButtons.OK,
					MessageBoxIcon.Warning);
				return;
			}

			foreach (string projectDir in searchedIndexes)
			{
				string projectFile = string.Format("{0}.csproj", projectDir);

				if (File.Exists(projectFile) == false)
					continue;

				if (MessageBox.Show(string.Format(
										MessageSettings.GetValue("QuestLoadProjectInVisualStudio"),		// {0} 폴더의\r\n{1} 프로젝트를 Visual Studio로 로드 하시겠습니까?
										Path.GetDirectoryName(projectFile),
										Path.GetFileName(projectFile)),
						Text,
						MessageBoxButtons.YesNo,
						MessageBoxIcon.Question) == DialogResult.Yes)
				{
					Process process = new Process()
					{
						StartInfo = new ProcessStartInfo(projectFile),
					};

					process.Start();
					Close();
				}
			}
		}

		private void SetDebugMode()
		{
			tlpBottom.ColumnStyles[0].Width = 50f;
			tlpBottom.ColumnStyles[1].Width = 50f;
		}

		private void SetNormal()
		{
			tlpBottom.ColumnStyles[0].Width = 100f;
			tlpBottom.ColumnStyles[1].Width = 0f;
		}

		private IEnumerable<AssemblyName> GetAssemblyNameStartWithNamespace(Assembly assembly, string nameSpace)
		{
			return assembly.GetReferencedAssemblies()
				.Where(asm => asm.Name.StartsWith(nameSpace))
				.OrderByDescending(asm => asm.Name);
		}
		#endregion
	}
}
