﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Loader.Help.AssemblyInfoWrapper
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 11월 17일 화요일 오후 8:47
/*	Purpose		:	AssemblyName중 필요한 정보를 Wrapping 합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 11월 17일 화요일 오후 8:49
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AnyFactory.FX.Win.Loader.Help
{
	internal class AssemblyNameWrapper
	{
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Version를 구하거나 설정합니다.
		/// </summary>
		public string Version
		{
			get;
			set;
		}

		/// <summary>
		/// FullName를 구하거나 설정합니다.
		/// </summary>
		public string FullName
		{
			get;
			set;
		}

		/// <summary>
		/// CultureInfo를 구하거나 설정합니다.
		/// </summary>
		public string CultureInfo
		{
			get;
			set;
		}

		/// <summary>
		/// ProcessorArchitecture를 구하거나 설정합니다.
		/// </summary>
		public string ProcessorArchitecture
		{
			get;
			set;
		}
	}
}
