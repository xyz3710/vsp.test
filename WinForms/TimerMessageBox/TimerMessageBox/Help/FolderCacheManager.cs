﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Utility.FolderCacheManager
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 11월 3일 화요일 오후 9:19
/*	Purpose		:	스캔한 Folder에 대한 Cache를 관리하는 기능을 제공 하는 클래스 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	Kim Ki Won
/*	Update		:	2010년 1월 7일 목요일 오후 2:49
/*	Changes		:	Xml관련 부분을 외부 클래스로 refactoring
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	1. IndexFilePath가 있는지 검사
 *					2. IndexFilePath가 있으면 RootPath가 있는지 검사, 없으면 ScanFolders & CreateChild
 *					3. RootPath가 있으면 SearchIndex, 없으면 ScanFolders & CreateChild
 *					4. SearchIndex 검색 결과가 있으면 종료, 
 *					5. 검색 결과가 없으면 RefreshCache 후, 3.번 항목으로 재이동
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 11월 4일 수요일 오후 4:45
/**********************************************************************************************************************/

#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using AnyFactory.FX.Win.Configuration;
using System.Xml.Linq;
#endregion

namespace AnyFactory.FX.Win.Utility
{
	/// <summary>
	/// 스캔한 Folder에 대한 Cache를 관리하는 기능을 제공 하는 클래스 입니다.
	/// </summary>
	public class FolderCacheManager
	{
		#region Constants
		private const string DEFAULT_FILE_NAME = "AnyFactory.FX.Win.Utility.CacheIndex.xml";
		#endregion

		#region Fields
		private string _indexFilePath;
		private string _defaultFileName;
		private FolderCacheXmlManager _folderCacheXmlManager;
		#endregion

		#region Constructors
		/// <summary>
		/// FolderCacheManager 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="rootPath">폴더 cache를 처리할 root 경로</param>
		/// <param name="searchPattern">검색 패턴</param>
		/// <param name="searchOption">검색 option</param>
		/// <param name="excludedPatterns">제외 패턴</param>
		/// <param name="exactlyMatch">정확히 일치하는지 여부</param>
		public FolderCacheManager(string rootPath,
			string searchPattern,
			SearchOption searchOption,
			string[] excludedPatterns,
			bool exactlyMatch)
			: this(rootPath)
		{
			SearchPattern = searchPattern;
			SearchOption = searchOption;
			ExcludedPatterns = excludedPatterns;
			ExactlyMatch = exactlyMatch;
		}

		/// <summary>
		/// FolderCacheManager 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="rootPath">폴더 cache를 처리할 root 경로</param>
		public FolderCacheManager(string rootPath)
		{
			RootPath = rootPath;
			_folderCacheXmlManager = new FolderCacheXmlManager(IndexFilePath);
		}
		#endregion

		#region Delegates
		/// <summary>
		/// <seealso cref="SearchIndex(NotFoundActionHandler, string[])"/> 결과가 없을 경우 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <returns>true를 반환할 경우 <seealso cref="SearchIndex(NotFoundActionHandler, string[])"/>의 remark의 step 5를 실행 합니다.</returns>
		public delegate bool NotFoundActionHandler();
		#endregion

		#region Properties
		/// <summary>
		/// 검색한 Index 파일을 저장할 경로를 구하거나 설정합니다.
		/// </summary>
		/// <remarks>지정한 값이 없으면 기본으로 %AppData%\<seealso cref="DefaultFileName"/>으로 저장됩니다.</remarks>
		public string IndexFilePath
		{
			get
			{
				if (string.IsNullOrEmpty(_indexFilePath) == true)
					_indexFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), DefaultFileName);

				return _indexFilePath;
			}
			set
			{
				_indexFilePath = value;
				_folderCacheXmlManager.CacheFileName = _indexFilePath;
			}
		}

		/// <summary>
		/// RootPath를 구하거나 설정합니다.
		/// </summary>
		public string RootPath
		{
			get;
			set;
		}

		/// <summary>
		/// <seealso cref="IndexFilePath"/>에 사용될 파일이름을 구하거나 설정합니다.
		/// </summary>
		/// <remarks>기본 파일이름은 AnyFactory.FX.Win.Utility.CacheIndex.xml 입니다.</remarks>
		public string DefaultFileName
		{
			get
			{
				if (string.IsNullOrEmpty(_defaultFileName) == true)
					_defaultFileName = DEFAULT_FILE_NAME;

				return _defaultFileName;
			}
			set
			{
				_defaultFileName = value;

				if (IndexFilePath == Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), DEFAULT_FILE_NAME))
					IndexFilePath = string.Empty;		// 재설정 해서 IndexFilePath 값이 할당 될 수 있도록 한다.

				_folderCacheXmlManager.CacheFileName = IndexFilePath;
			}
		}

		/// <summary>
		/// 검색 패턴을 구하거나 설정합니다.
		/// </summary>
		/// <remarks><seealso cref="SearchIndex(string, bool, string[])"/>를 실행하면 저장합니다.</remarks>
		public string SearchPattern
		{
			get;
			set;
		}

		/// <summary>
		/// 검색 옵션을 구하거나 설정합니다.
		/// </summary>
		/// <remarks><seealso cref="SearchIndex(string, bool, string[])"/>를 실행하면 저장합니다.</remarks>
		public SearchOption SearchOption
		{
			get;
			set;
		}

		/// <summary>
		/// 제외 패턴을 구하거나 설정합니다.
		/// </summary>
		/// <remarks><seealso cref="SearchIndex(string, bool, string[])"/>를 실행하면 저장합니다.</remarks>
		public string[] ExcludedPatterns
		{
			get;
			set;
		}

		/// <summary>
		/// 정확히 일치하는지 여부를 구하거나 설정합니다.
		/// </summary>
		/// <remarks><seealso cref="SearchIndex(string, bool, string[])"/>를 실행하면 저장합니다.</remarks>
		public bool ExactlyMatch
		{
			get;
			set;
		}

		/// <summary>
		/// RootPath에 있는 모든 폴더를 구합니다.
		/// </summary>
		/// <remarks>rootPath에 폴더가 없으면 빈 배열을 반환 합니다.</remarks>
		public List<string> Folders
		{
			get
			{
				return _folderCacheXmlManager.GetFolders(RootPath);
			}
		}
		#endregion

		#region ScanFolders
		/// <summary>
		/// 지정한 path를 검색하여 폴더 목록을 구합니다.
		/// </summary>
		/// <returns></returns>
		/// <remarks>기존에 한번 실행 했거나 <seealso cref="FolderCacheManager(string, string, SearchOption, string[], bool)"/>에서 해당 값을 지정한 경우에만 사용할 수 있습니다.</remarks>
		public List<string> ScanFolders()
		{
			return ScanFolders(RootPath, SearchPattern, SearchOption, ExcludedPatterns);
		}

		/// <summary>
		/// 지정한 path를 검색하여 searchPattern에 맞는 파일의 폴더 목록을 구합니다.
		/// </summary>
		/// <param name="path">검색할 경로입니다.</param>
		/// <param name="searchPattern">path에 있는 파일 이름과 일치하는지 비교할 검색 문자열입니다. 매개 변수는 마침표 두 개("..")로 끝나거나 마침표 두 개("..")로<br/>
		/// 시작하는 System.IO.Path.DirectorySeparatorChar 또는 System.IO.Path.AltDirectorySeparatorChar를<br/>
		/// 포함할 수 없으며 System.IO.Path.InvalidPathChars에 있는 모든 문자를 포함할 수 없습니다.</param>
		/// <param name="searchOption">검색 작업에 모든 하위 디렉터리를 포함할지 아니면 현재 디렉터리만 포함할지 지정하는 System.IO.SearchOption 값 중 하나입니다.</param>
		/// <param name="excludePatterns">제외할 폴더 검색 pattern</param>
		/// <returns></returns>
		public List<string> ScanFolders(string path, string searchPattern, SearchOption searchOption, params string[] excludePatterns)
		{
			SearchPattern = searchPattern;
			SearchOption = searchOption;
			ExcludedPatterns = excludePatterns;

			if (Directory.Exists(path) == false)
				return new List<string>();

			// searchPattern이 있는 파일의 확장자를 제외한 이름을 구한다.
			List<string> folderList = new List<string>(
					Directory.GetFiles(path, searchPattern, searchOption).Select(file => file.Replace(Path.GetExtension(file), string.Empty))
				);

			return folderList.FindAll(delegate(string item)
			{
				bool contained = true;

				foreach (string excludePattern in excludePatterns)
					contained &= !item.Contains(excludePattern);

				return contained;
			});
		}
		#endregion

		#region SearchIndex
		/// <summary>
		/// 저장된 <seealso cref="IndexFilePath"></seealso>의 <seealso cref="RootPath"/>내에 searchFolders의 원래 경로를 검사합니다.<br/>
		/// 지정된 <seealso cref="RootPath"/>와 <seealso cref="ExactlyMatch"/> 값으로 찾습니다.
		/// </summary>
		/// <param name="notFoundActionHandler">remark에 있는 내용중 step4일 경우 처리할 대리자</param>
		/// <param name="searchFolders">검색할 index명</param>
		/// <returns>cache내 검색된 폴더 목록을 반환합니다.</returns>
		/// <remarks>
		/// <seealso cref="FolderCacheManager(string, string, SearchOption, string[], bool)"/>를 이용하여 값이 모두 설정된 경우에 사용할 수 있습니다.<br/>
		/// 1. IndexFilePath가 있는지 검사<br/>
		/// 2. IndexFilePath가 있으면 RootPath가 있는지 검사, 없으면 ScanFolders &amp; AddFolder<br/>
		/// 3. RootPath가 있으면 SearchIndex, 없으면 ScanFolders &amp; CreateChild<br/>
		/// 4. SearchIndex 검색 결과가 있으면 종료,<br/>
		/// 5. 검색 결과가 없으면 RefreshCache 후, 3.번 항목으로 재이동
		/// </remarks>
		public List<string> SearchIndex(NotFoundActionHandler notFoundActionHandler, params string[] searchFolders)
		{
			if (String.IsNullOrEmpty(RootPath) == true)
				throw new ArgumentException(MessageSettings.GetValue("ShouldAssignRootPath"), "RootPath");

			if (String.IsNullOrEmpty(SearchPattern) == true)
				throw new ArgumentException(MessageSettings.GetValue("ShouldAssignSearchPattern"), "SearchPattern");

			// step 1.
			if (CheckIndexFilePath() == false)
				RefreshCache();
			else
				_folderCacheXmlManager.Load();

			// step 2.
			if (_folderCacheXmlManager.GetRoot(RootPath) == null)
				RefreshCache();

			// step 3.
			List<string> results = SearchIndex(RootPath, ExactlyMatch, searchFolders);

			// step 4.
			if (results.Count > 0)
				return results;
			else
			{
				if (notFoundActionHandler != null)
				{
					if (notFoundActionHandler() == true)
					{
						// step 5.
						if (RefreshCache(RootPath) == true)
							return SearchIndex(RootPath, ExactlyMatch, searchFolders);
					}
				}
			}

			return results;
		}

		/// <summary>
		/// 저장된 <seealso cref="IndexFilePath"></seealso>의 <seealso cref="RootPath"/>의 index key가 있는지 검사합니다.<br/>
		/// 지정된 <seealso cref="RootPath"/>로 찾습니다.
		/// </summary>
		/// <param name="exactlyMatch">searchFolders가 정확히 일치 하는지 여부</param>
		/// <param name="searchFolders">검색할 index명</param>
		/// <returns>cache내 검색된 폴더 목록을 반환합니다.</returns>
		public List<string> SearchIndex(bool exactlyMatch, params string[] searchFolders)
		{
			return SearchIndex(RootPath, exactlyMatch, searchFolders);
		}

		/// <summary>
		/// 저장된 <seealso cref="IndexFilePath"/>에서 index의 key가 있는지 검사합니다.
		/// </summary>
		/// <param name="rootPath"><seealso cref="IndexFilePath"/> 파일에서 검색할 rootPath</param>
		/// <param name="exactlyMatch">searchFolders가 정확히 일치 하는지 여부</param>
		/// <param name="searchFolders">검색할 index명</param>
		/// <returns>cache내 검색된 폴더 목록을 반환합니다.</returns>
		public List<string> SearchIndex(string rootPath, bool exactlyMatch, params string[] searchFolders)
		{
			ExactlyMatch = exactlyMatch;

			try
			{
				if (_folderCacheXmlManager.IsLoad == false)
					_folderCacheXmlManager.Load();
			}
			catch (IOException)
			{
				RefreshCache();
			}

			return _folderCacheXmlManager.GetFolders(rootPath, exactlyMatch, searchFolders);
		}

		/// <summary>
		/// IndexFilePath가 있는지 검사합니다.
		/// </summary>
		/// <returns></returns>
		public bool CheckIndexFilePath()
		{
			return File.Exists(IndexFilePath);
		}
		#endregion

		#region RefreshCache
		/// <summary>
		/// <seealso cref="RootPath"/>를 기존 설정을 가지고 refresh합니다.
		/// </summary>
		/// <returns></returns>
		public bool RefreshCache()
		{
			return RefreshCache(RootPath);
		}

		/// <summary>
		/// rootPath를 기존 설정을 가지고 refresh합니다.
		/// </summary>
		/// <param name="rootPath">저장할 rootPath</param>
		/// <returns></returns>
		public bool RefreshCache(string rootPath)
		{
			List<string> saveFolders = ScanFolders(rootPath, SearchPattern, SearchOption, ExcludedPatterns);

			if (saveFolders == null || saveFolders.Count == 0)
				return false;

			_folderCacheXmlManager.ResetDocument();
			_folderCacheXmlManager.AddFolder(rootPath, SearchPattern, saveFolders.ToArray());
			_folderCacheXmlManager.Save();

			return true;
		}
		#endregion
	}
}