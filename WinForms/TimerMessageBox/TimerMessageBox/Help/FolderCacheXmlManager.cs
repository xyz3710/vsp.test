﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Utility.FolderCacheXmlManager
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2010년 1월 6일 수요일 오후 10:55
/*	Purpose		:	FolderCacheManager에서 사용할 Xml 관리용 클래스입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2010년 1월 7일 목요일 오후 2:02
/**********************************************************************************************************************/

#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.IO;
#endregion

namespace AnyFactory.FX.Win.Utility
{
	/// <summary>
	/// FolderCacheManager에서 사용할 Xml 관리용 클래스입니다.
	/// </summary>
	internal class FolderCacheXmlManager
	{
		#region Constants
		private const string GRAND_ROOT_KEY = "Indexes";
		private const string ROOT_KEY = "Folders";
		private const string ROOT_ATTRIBUTE_KEY = "root";
		private const string SEARCHPATTERN_ATTRIBUTE_KEY = "searchPattern";
		private const string BLANK_STRING_KEY = "blankString";
		private const string BLANK_STRING = " ";
		private const string NUMERIC_PREFIX = "N";
		private const string DEFAULT_FILE_NAME = "AnyFactory.FX.Win.Utility.CacheIndex.xml";
		#endregion

		#region Fields
		private XDocument _xDocument;
		private readonly string[] _blankReplaceString = new string[] { "_", "-", "__", "--", "___", "---" };
		#endregion

		#region Constructors
		/// <summary>
		/// CacheXmlWraper class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="cacheFileName">Cache로 저장할 xml 파일 이름을 지정합니다.</param>
		/// <exception cref="ArgumentException">cacheFileName이 string.Empty 이거나 null이면 발생 합니다.</exception>
		public FolderCacheXmlManager(string cacheFileName)
		{
			#region Check Parameters
			if (string.IsNullOrEmpty(cacheFileName) == true)
				throw new ArgumentException("FolderCacheXmlManager: cacheFileName이 지정되지 않았습니다..");
			#endregion

			CacheFileName = cacheFileName;
			SetXDocument(cacheFileName);
		}

		private void SetXDocument(string cacheFileName)
		{
			if (File.Exists(cacheFileName) == true)
				Load(cacheFileName);
			else
				ResetDocument();
		}
		#endregion

		#region Properties
		/// <summary>
		/// CacheFileName를 구하거나 설정합니다.
		/// </summary>
		public string CacheFileName
		{
			get;
			set;
		}

		/// <summary>
		/// 문서가 Load 되었는지 여부를 구합니다.
		/// </summary>
		public bool IsLoad
		{
			get;
			private set;
		}

		/// <summary>
		/// 현재 문서에 로드 되어 있는 모든 root element를 구합니다.
		/// </summary>
		/// <remarks>문서가 없거나 로드되지 않으면 빈 배열을 반환 합니다.</remarks>
		public List<XElement> Roots
		{
			get
			{
				if (IsLoad == false)
					Load();

				if (_xDocument == null)
					return new List<XElement>();

				return _xDocument.Root.Nodes().Cast<XElement>().ToList();
			}
		}
		#endregion

		#region Add methods
		/// <summary>
		/// Root element를 추가 합니다.
		/// </summary>
		/// <param name="rootPath">추가할 root 경로 속성을 지정합니다.</param>
		/// <param name="searchPattern">추가할 파일 패턴 속성을 지정합니다.</param>
		/// <returns></returns>
		public XElement AddRoot(string rootPath, string searchPattern)
		{
			XElement root = new XElement(ROOT_KEY,
								new XAttribute(ROOT_ATTRIBUTE_KEY, rootPath),
								new XAttribute(SEARCHPATTERN_ATTRIBUTE_KEY, searchPattern));

			_xDocument.Root.Add(root);

			return root;
		}

		/// <summary>
		/// <seealso cref="CacheFileName"/>에 rootPath의 폴더 내용을 추가 합니다.
		/// </summary>
		/// <param name="rootPath">추가할 root 경로 속성을 지정합니다.</param>
		/// <param name="searchPattern">root element가 없을 경우 추가할 파일 패턴 속성을 지정합니다.</param>
		/// <param name="folderPath">추가할 폴더 목록을 지정합니다.</param>
		/// <returns></returns>
		public bool AddFolder(string rootPath, string searchPattern, params string[] folderPath)
		{
			XElement root = GetRoot(rootPath);

			if (root == null)
				root = AddRoot(rootPath, searchPattern);

			return AddFolder(root, folderPath);
		}

		/// <summary>
		/// <seealso cref="CacheFileName"/>에 rootPath의 폴더 내용을 추가 합니다.
		/// </summary>
		/// <param name="root">추가할 root element를 지정합니다.</param>
		/// <param name="folderPath">추가할 폴더 목록을 지정합니다.</param>
		/// <returns></returns>
		public bool AddFolder(XElement root, params string[] folderPath)
		{
			if (folderPath == null || folderPath.Length == 0)
				return false;

			string blankString = string.Empty;
			bool replace = false;

			foreach (string path in folderPath)
			{
				string originPath = Path.GetFileName(path);

				replace = ParseXmlDocumentFormat(ref originPath, ref blankString);

				XElement folderElement = new XElement(originPath, path);

				if (replace == true)
					folderElement.SetAttributeValue(BLANK_STRING_KEY, blankString);

				root.Add(folderElement);
			}

			return true;
		}
		#endregion

		#region Get methods
		/// <summary>
		/// 지정된 rootPath를 사용한 Root element를 구합니다.
		/// </summary>
		/// <param name="rootPath">검색할 root 경로 속성을 지정합니다.</param>
		/// <returns>검색된 첫번째 root를 반환하거나 없을 경우 null을 반환 합니다.</returns>
		public XElement GetRoot(string rootPath)
		{
			List<XElement> roots 
				= Roots.Where(element => element.Attribute(ROOT_ATTRIBUTE_KEY).Value == rootPath)
						.ToList();

			if (roots.Count > 0)
				return roots[0];
			else
				return null;
		}

		/// <summary>
		/// 지정된 rootPath에 있는 모든 폴더를 구합니다.
		/// </summary>
		/// <remarks>rootPath에 폴더가 없으면 빈 배열을 반환 합니다.</remarks>
		public List<string> GetFolders(string rootPath)
		{
			XElement root = GetRoot(rootPath);

			if (root == null)
				return new List<string>();

			return root.Descendants().Select(x => x.Value).ToList();
		}

		/// <summary>
		/// 지정된 rootPath를 사용하여 폴더를 검색합니다.
		/// </summary>
		/// <param name="rootPath">검색하려는 root 경로 속성을 지정합니다.</param>
		/// <param name="exactlyMatch">folderName이 정확히 일치 하는지 여부를 지정합니다.</param>
		/// <param name="folderName">검색하려는 폴더 이름을 지정합니다.</param>
		/// <returns>검색한 폴더를 반환 하거나, 해당 rootPath의 root element가 없으면 빈 배열을 반환 합니다.</returns>
		public List<string> GetFolders(string rootPath, bool exactlyMatch, params string[] folderName)
		{
			XElement root = GetRoot(rootPath);

			if (root == null)
				return new List<string>();

			return GetFolders(root, exactlyMatch, folderName);
		}

		/// <summary>
		/// 지정된 rootPath를 사용하여 폴더를 검색합니다.
		/// </summary>
		/// <param name="root">검색하려는 root element 속성을 지정합니다.</param>
		/// <param name="exactlyMatch">folderName이 정확히 일치 하는지 여부를 지정합니다.</param>
		/// <param name="folderName">검색하려는 폴더 이름을 지정합니다.</param>
		/// <returns>검색한 폴더 중 실제로 존재하는 폴더를 반환 하거나, 해당 rootPath의 root element가 없으면 빈 배열을 반환 합니다.</returns>
		public List<string> GetFolders(XElement root, bool exactlyMatch, params string[] folderName)
		{
			List<string> result = new List<string>();

			if (root == null)
				return result;

			string originString = string.Empty;
			string blankString = string.Empty;
			IEnumerable<XElement> allElements = null;

			if (exactlyMatch == false)
				allElements = root.Descendants().Cast<XElement>();

			foreach (string folderNameOnly in folderName)
			{
				originString = folderNameOnly;
				ParseXmlDocumentFormat(ref originString, ref blankString);

				if (exactlyMatch == true)
					result.AddRange(root.Descendants(originString)
							.Select(x => x.Value));
				else
					result.AddRange(allElements
							.Where(x => x.Name.ToString().ToLower().Contains(originString.ToLower()))
							.Select(x => x.Value));
			}

			// 존재 하는 폴더만 다시 filtering 한다.
			return result.FindAll(item => Directory.Exists(Path.GetDirectoryName(item)));
		}
		#endregion

		#region Manipulate document
		/// <summary>
		/// <seealso cref="CacheFileName"/>으로 문서를 로드 합니다.
		/// </summary>
		public void Load()
		{
			Load(CacheFileName);
		}

		/// <summary>
		/// 지정된 cacheFileName으로 문서를 로드 합니다.
		/// </summary>
		/// <param name="cacheFileName"></param>
		public void Load(string cacheFileName)
		{
			try
			{
				_xDocument = XDocument.Load(cacheFileName);
			}
			catch (Exception)
			{
				ResetDocument();
			}

			IsLoad = true;
		}

		/// <summary>
		/// <seealso cref="CacheFileName"/>으로 문서를 저장 합니다.
		/// </summary>
		public void Save()
		{
			Save(CacheFileName);
		}

		/// <summary>
		/// 지정된 cacheFileName으로 문서를 저장 합니다.
		/// </summary>
		/// <param name="cacheFileName"></param>
		public void Save(string cacheFileName)
		{
			_xDocument.Save(cacheFileName);
		}

		/// <summary>
		/// 문서 형식을 초기화 합니다.
		/// </summary>
		public void ResetDocument()
		{
			_xDocument = new XDocument(
						new XDeclaration("1.0", "utf-8", "yes"),
						new XElement(GRAND_ROOT_KEY)
					);
			IsLoad = false;
		}
		#endregion

		#region Privates
		/// <summary>
		/// Xml 문서 포멧에 맞도록 parsing 합니다.
		/// </summary>
		/// <param name="originPath"></param>
		/// <param name="blankString"></param>
		/// <returns></returns>
		private bool ParseXmlDocumentFormat(ref string originPath, ref string blankString)
		{
			if (Char.IsNumber(originPath[0]) == true)
				originPath = NUMERIC_PREFIX + originPath;

			return CheckReplacedBlankString(ref originPath, ref blankString);
		}

		/// <summary>
		/// blank 문자열이 있는지 검사 후 있으면 적절한 대체 문자를 blankString에 return 합니다.
		/// </summary>
		/// <param name="originalString">원래 문자열</param>
		/// <param name="blankString">공백이 변경된 문자열(originalString에 공백이 없을 경우 <seealso cref="System.String.Empty"/>)</param>
		/// <returns>originalString의 문자열이 변경되면 true를 반환 합니다.</returns>
		private bool CheckReplacedBlankString(ref string originalString, ref string blankString)
		{
			if (originalString.Contains(BLANK_STRING) == true)
			{
				foreach (string replaceString in _blankReplaceString)
				{
					if (originalString.Contains(replaceString) == true)
						continue;
					else
					{
						originalString = originalString.Replace(BLANK_STRING, replaceString);
						blankString = replaceString;

						return true;
					}
				}
			}
			else
				blankString = string.Empty;

			return false;
		}

		/// <summary>
		/// blankString의 값으로 blank를 변환 하기 전 문자열울 구합니다.
		/// </summary>
		/// <param name="targetString">변환할 문자열</param>
		/// <param name="blankString">blank를 변환 시킬 문자열</param>
		/// <returns>원래 공백이 있는 문자열을 반환 합니다.</returns>
		private string GetOriginalString(string targetString, string blankString)
		{
			if (blankString == string.Empty)
				return targetString;

			return targetString.Replace(blankString, BLANK_STRING);
		}
		#endregion
	}
}