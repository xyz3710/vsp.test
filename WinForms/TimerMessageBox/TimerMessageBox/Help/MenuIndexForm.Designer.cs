﻿namespace AnyFactory.FX.Win.Loader.Help
{
	partial class MenuIndexForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuIndexForm));
			Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinScrollBar.ScrollBarLook scrollBarLook1 = new Infragistics.Win.UltraWinScrollBar.ScrollBarLook();
			Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
			Infragistics.Win.UltraWinScrollBar.ScrollBarLook scrollBarLook2 = new Infragistics.Win.UltraWinScrollBar.ScrollBarLook();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.xgrbBottom = new Infragistics.Win.Misc.UltraGroupBox();
			this.tlpBottom = new System.Windows.Forms.TableLayoutPanel();
			this.abFormExit = new AnyFactory.FX.Win.Controls.ActionButton.ActionButtonBase(this.components);
			this.xlblStatus = new Infragistics.Win.Misc.UltraLabel();
			this.xbtnOpenProject = new Infragistics.Win.Misc.UltraButton();
			this.xgrbList = new Infragistics.Win.Misc.UltraGroupBox();
			this.xgridList = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.xgrbCondition = new Infragistics.Win.Misc.UltraGroupBox();
			this.tlpCondition = new System.Windows.Forms.TableLayoutPanel();
			this.xlblManuName = new Infragistics.Win.Misc.UltraLabel();
			this.xtxtMenuName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.abSearch = new AnyFactory.FX.Win.Controls.ActionButton.ActionButtonBase(this.components);
			this.xgrbReferences = new Infragistics.Win.Misc.UltraGroupBox();
			this.xgridReferences = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.pnlContainerBody.SuspendLayout();
			this.tlpMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgrbBottom)).BeginInit();
			this.xgrbBottom.SuspendLayout();
			this.tlpBottom.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgrbList)).BeginInit();
			this.xgrbList.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgridList)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xgrbCondition)).BeginInit();
			this.xgrbCondition.SuspendLayout();
			this.tlpCondition.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xtxtMenuName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.xgrbReferences)).BeginInit();
			this.xgrbReferences.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xgridReferences)).BeginInit();
			this.SuspendLayout();
			// 
			// pnlContainerBody
			// 
			this.pnlContainerBody.BackColor = System.Drawing.Color.White;
			this.pnlContainerBody.Controls.Add(this.tlpMain);
			// 
			// tlpMain
			// 
			this.tlpMain.BackColor = System.Drawing.Color.White;
			this.tlpMain.ColumnCount = 1;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Controls.Add(this.xgrbBottom, 0, 3);
			this.tlpMain.Controls.Add(this.xgrbList, 0, 1);
			this.tlpMain.Controls.Add(this.xgrbCondition, 0, 0);
			this.tlpMain.Controls.Add(this.xgrbReferences, 0, 2);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(3, 3);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 4;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tlpMain.Size = new System.Drawing.Size(975, 580);
			this.tlpMain.TabIndex = 0;
			// 
			// xgrbBottom
			// 
			appearance16.BackColor = System.Drawing.Color.White;
			appearance16.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			appearance16.ForeColor = System.Drawing.Color.Black;
			appearance16.TextVAlignAsString = "Middle";
			this.xgrbBottom.Appearance = appearance16;
			this.xgrbBottom.BackColorInternal = System.Drawing.Color.White;
			this.xgrbBottom.ContentPadding.Bottom = 2;
			this.xgrbBottom.ContentPadding.Left = 2;
			this.xgrbBottom.ContentPadding.Right = 2;
			this.xgrbBottom.ContentPadding.Top = 2;
			this.xgrbBottom.Controls.Add(this.tlpBottom);
			this.xgrbBottom.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			appearance20.FontData.BoldAsString = "True";
			appearance20.ForeColor = System.Drawing.Color.Black;
			appearance20.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance20.TextVAlignAsString = "Top";
			this.xgrbBottom.HeaderAppearance = appearance20;
			this.xgrbBottom.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
			this.xgrbBottom.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
			this.xgrbBottom.Location = new System.Drawing.Point(3, 533);
			this.xgrbBottom.Name = "xgrbBottom";
			this.xgrbBottom.Size = new System.Drawing.Size(969, 44);
			this.xgrbBottom.TabIndex = 3;
			// 
			// tlpBottom
			// 
			this.tlpBottom.BackColor = System.Drawing.Color.White;
			this.tlpBottom.ColumnCount = 3;
			this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 121F));
			this.tlpBottom.Controls.Add(this.abFormExit, 2, 0);
			this.tlpBottom.Controls.Add(this.xlblStatus, 0, 0);
			this.tlpBottom.Controls.Add(this.xbtnOpenProject, 1, 0);
			this.tlpBottom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpBottom.Location = new System.Drawing.Point(5, 11);
			this.tlpBottom.Margin = new System.Windows.Forms.Padding(0);
			this.tlpBottom.Name = "tlpBottom";
			this.tlpBottom.RowCount = 1;
			this.tlpBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tlpBottom.Size = new System.Drawing.Size(959, 28);
			this.tlpBottom.TabIndex = 0;
			// 
			// abFormExit
			// 
			appearance13.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(139)))), ((int)(((byte)(141)))));
			appearance13.ForeColor = System.Drawing.Color.Black;
			appearance13.Image = ((object)(resources.GetObject("appearance13.Image")));
			appearance13.ImageHAlign = Infragistics.Win.HAlign.Left;
			appearance13.ImageVAlign = Infragistics.Win.VAlign.Middle;
			appearance13.TextHAlignAsString = "Center";
			appearance13.TextVAlignAsString = "Middle";
			this.abFormExit.Appearance = appearance13;
			this.abFormExit.BackColorInternal = System.Drawing.Color.White;
			this.abFormExit.ButtonSize = AnyFactory.FX.Win.Controls.ActionButton.ButtonSize.Size16;
			this.abFormExit.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.abFormExit.ButtonType = AnyFactory.FX.Win.Controls.ActionButton.ButtonType.FormExit;
			this.abFormExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.abFormExit.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance14.ForeColor = System.Drawing.Color.BlueViolet;
			appearance14.TextHAlignAsString = "Center";
			appearance14.TextVAlignAsString = "Middle";
			this.abFormExit.HotTrackAppearance = appearance14;
			this.abFormExit.ImageAndTextLayout = Infragistics.Win.HAlign.Left;
			this.abFormExit.Location = new System.Drawing.Point(841, 3);
			this.abFormExit.Name = "abFormExit";
			this.abFormExit.Padding = new System.Drawing.Size(12, 0);
			this.abFormExit.ShowFocusRect = false;
			this.abFormExit.ShowOutline = false;
			this.abFormExit.Size = new System.Drawing.Size(115, 22);
			this.abFormExit.TabIndex = 0;
			this.abFormExit.Text = "창닫기";
			this.abFormExit.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// xlblStatus
			// 
			appearance17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			appearance17.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(139)))), ((int)(((byte)(141)))));
			appearance17.ForeColor = System.Drawing.Color.Black;
			appearance17.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance17.TextHAlignAsString = "Right";
			appearance17.TextVAlignAsString = "Middle";
			this.xlblStatus.Appearance = appearance17;
			this.xlblStatus.BackColorInternal = System.Drawing.Color.White;
			this.xlblStatus.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xlblStatus.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xlblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xlblStatus.Location = new System.Drawing.Point(3, 3);
			this.xlblStatus.Name = "xlblStatus";
			this.xlblStatus.Padding = new System.Drawing.Size(2, 0);
			this.xlblStatus.Size = new System.Drawing.Size(413, 22);
			this.xlblStatus.TabIndex = 1;
			this.xlblStatus.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// xbtnOpenProject
			// 
			appearance38.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(139)))), ((int)(((byte)(141)))));
			appearance38.ForeColor = System.Drawing.Color.Black;
			this.xbtnOpenProject.Appearance = appearance38;
			this.xbtnOpenProject.BackColorInternal = System.Drawing.Color.White;
			this.xbtnOpenProject.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
			this.xbtnOpenProject.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xbtnOpenProject.Location = new System.Drawing.Point(422, 3);
			this.xbtnOpenProject.Name = "xbtnOpenProject";
			this.xbtnOpenProject.ShowFocusRect = false;
			this.xbtnOpenProject.ShowOutline = false;
			this.xbtnOpenProject.Size = new System.Drawing.Size(413, 22);
			this.xbtnOpenProject.TabIndex = 2;
			this.xbtnOpenProject.Text = "선택한 프로젝트 열기(F12)";
			this.xbtnOpenProject.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			this.xbtnOpenProject.Click += new System.EventHandler(this.xbtnOpenProject_Click);
			// 
			// xgrbList
			// 
			appearance18.BackColor = System.Drawing.Color.White;
			appearance18.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			appearance18.ForeColor = System.Drawing.Color.Black;
			appearance18.TextVAlignAsString = "Middle";
			this.xgrbList.Appearance = appearance18;
			this.xgrbList.BackColorInternal = System.Drawing.Color.White;
			this.xgrbList.ContentPadding.Bottom = 2;
			this.xgrbList.ContentPadding.Left = 2;
			this.xgrbList.ContentPadding.Right = 2;
			this.xgrbList.ContentPadding.Top = 2;
			this.xgrbList.Controls.Add(this.xgridList);
			this.xgrbList.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			appearance37.FontData.BoldAsString = "True";
			appearance37.ForeColor = System.Drawing.Color.Black;
			appearance37.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance37.TextVAlignAsString = "Top";
			this.xgrbList.HeaderAppearance = appearance37;
			this.xgrbList.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
			this.xgrbList.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
			this.xgrbList.Location = new System.Drawing.Point(3, 53);
			this.xgrbList.Name = "xgrbList";
			this.xgrbList.Size = new System.Drawing.Size(969, 234);
			this.xgrbList.TabIndex = 1;
			this.xgrbList.Tag = "Form 정보";
			this.xgrbList.Text = "Form 정보";
			// 
			// xgridList
			// 
			appearance39.BackColor = System.Drawing.Color.White;
			appearance39.BorderColor = System.Drawing.Color.Gray;
			appearance39.ForeColor = System.Drawing.Color.Black;
			this.xgridList.DisplayLayout.Appearance = appearance39;
			this.xgridList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance40.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance40.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance40.BorderColor = System.Drawing.SystemColors.Window;
			this.xgridList.DisplayLayout.GroupByBox.Appearance = appearance40;
			appearance41.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xgridList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance41;
			this.xgridList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridList.DisplayLayout.GroupByBox.Hidden = true;
			appearance42.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance42.BackColor2 = System.Drawing.SystemColors.Control;
			appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xgridList.DisplayLayout.GroupByBox.PromptAppearance = appearance42;
			this.xgridList.DisplayLayout.GroupByBox.Style = Infragistics.Win.UltraWinGrid.GroupByBoxStyle.Compact;
			this.xgridList.DisplayLayout.MaxColScrollRegions = 1;
			this.xgridList.DisplayLayout.MaxRowScrollRegions = 1;
			appearance43.BackColor = System.Drawing.SystemColors.Window;
			appearance43.ForeColor = System.Drawing.SystemColors.ControlText;
			this.xgridList.DisplayLayout.Override.ActiveCellAppearance = appearance43;
			appearance44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(197)))), ((int)(((byte)(183)))));
			appearance44.BorderColor = System.Drawing.Color.Orange;
			appearance44.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.xgridList.DisplayLayout.Override.ActiveRowAppearance = appearance44;
			this.xgridList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridList.DisplayLayout.Override.BorderStyleSpecialRowSeparator = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridList.DisplayLayout.Override.BorderStyleSummaryFooter = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridList.DisplayLayout.Override.BorderStyleSummaryValue = Infragistics.Win.UIElementBorderStyle.None;
			appearance45.BackColor = System.Drawing.SystemColors.Window;
			this.xgridList.DisplayLayout.Override.CardAreaAppearance = appearance45;
			appearance46.BorderColor = System.Drawing.Color.Silver;
			appearance46.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.xgridList.DisplayLayout.Override.CellAppearance = appearance46;
			this.xgridList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xgridList.DisplayLayout.Override.CellPadding = 0;
			this.xgridList.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
			this.xgridList.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
			this.xgridList.DisplayLayout.Override.FixedCellSeparatorColor = System.Drawing.Color.OrangeRed;
			this.xgridList.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
			appearance47.BackColor = System.Drawing.SystemColors.Control;
			appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance47.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance47.BorderColor = System.Drawing.SystemColors.Window;
			this.xgridList.DisplayLayout.Override.GroupByRowAppearance = appearance47;
			appearance48.TextHAlignAsString = "Center";
			appearance48.TextVAlignAsString = "Middle";
			this.xgridList.DisplayLayout.Override.HeaderAppearance = appearance48;
			this.xgridList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xgridList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance49.BackColor = System.Drawing.Color.LightBlue;
			this.xgridList.DisplayLayout.Override.HotTrackCellAppearance = appearance49;
			appearance50.BorderColor = System.Drawing.Color.Orange;
			this.xgridList.DisplayLayout.Override.HotTrackRowAppearance = appearance50;
			appearance51.BackColor = System.Drawing.Color.LightBlue;
			this.xgridList.DisplayLayout.Override.HotTrackRowCellAppearance = appearance51;
			appearance52.BackColor = System.Drawing.Color.Silver;
			this.xgridList.DisplayLayout.Override.HotTrackRowSelectorAppearance = appearance52;
			appearance53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(235)))), ((int)(((byte)(230)))));
			this.xgridList.DisplayLayout.Override.RowAlternateAppearance = appearance53;
			appearance54.BackColor = System.Drawing.Color.White;
			appearance54.BorderColor = System.Drawing.Color.Silver;
			this.xgridList.DisplayLayout.Override.RowAppearance = appearance54;
			appearance55.TextHAlignAsString = "Center";
			appearance55.TextVAlignAsString = "Middle";
			this.xgridList.DisplayLayout.Override.RowSelectorAppearance = appearance55;
			appearance56.TextHAlignAsString = "Center";
			appearance56.TextVAlignAsString = "Middle";
			this.xgridList.DisplayLayout.Override.RowSelectorHeaderAppearance = appearance56;
			this.xgridList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			this.xgridList.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow;
			appearance57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(225)))), ((int)(((byte)(190)))));
			this.xgridList.DisplayLayout.Override.SpecialRowSeparatorAppearance = appearance57;
			this.xgridList.DisplayLayout.Override.SpecialRowSeparatorHeight = 3;
			appearance58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(237)))), ((int)(((byte)(218)))));
			appearance58.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			this.xgridList.DisplayLayout.Override.SummaryFooterAppearance = appearance58;
			this.xgridList.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance59.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(237)))), ((int)(((byte)(218)))));
			appearance59.TextVAlignAsString = "Middle";
			this.xgridList.DisplayLayout.Override.SummaryValueAppearance = appearance59;
			appearance60.BackColor = System.Drawing.SystemColors.ControlLight;
			this.xgridList.DisplayLayout.Override.TemplateAddRowAppearance = appearance60;
			scrollBarLook1.ViewStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.WindowsVista;
			this.xgridList.DisplayLayout.ScrollBarLook = scrollBarLook1;
			this.xgridList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.xgridList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.xgridList.DisplayLayout.UseFixedHeaders = true;
			this.xgridList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.xgridList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xgridList.Location = new System.Drawing.Point(5, 29);
			this.xgridList.Name = "xgridList";
			this.xgridList.Size = new System.Drawing.Size(959, 200);
			this.xgridList.TabIndex = 0;
			this.xgridList.Text = "ultraGrid1";
			this.xgridList.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnUpdate;
			this.xgridList.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
			this.xgridList.AfterRowActivate += new System.EventHandler(this.xgridList_AfterRowActivate);
			// 
			// xgrbCondition
			// 
			appearance31.BackColor = System.Drawing.Color.White;
			appearance31.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			appearance31.ForeColor = System.Drawing.Color.Black;
			appearance31.TextVAlignAsString = "Middle";
			this.xgrbCondition.Appearance = appearance31;
			this.xgrbCondition.BackColorInternal = System.Drawing.Color.White;
			this.xgrbCondition.ContentPadding.Bottom = 2;
			this.xgrbCondition.ContentPadding.Left = 2;
			this.xgrbCondition.ContentPadding.Right = 2;
			this.xgrbCondition.ContentPadding.Top = 2;
			this.xgrbCondition.Controls.Add(this.tlpCondition);
			this.xgrbCondition.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			appearance36.FontData.BoldAsString = "True";
			appearance36.ForeColor = System.Drawing.Color.Black;
			appearance36.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance36.TextVAlignAsString = "Top";
			this.xgrbCondition.HeaderAppearance = appearance36;
			this.xgrbCondition.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
			this.xgrbCondition.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
			this.xgrbCondition.Location = new System.Drawing.Point(3, 3);
			this.xgrbCondition.Name = "xgrbCondition";
			this.xgrbCondition.Size = new System.Drawing.Size(969, 44);
			this.xgrbCondition.TabIndex = 2;
			// 
			// tlpCondition
			// 
			this.tlpCondition.BackColor = System.Drawing.Color.White;
			this.tlpCondition.ColumnCount = 4;
			this.tlpCondition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
			this.tlpCondition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.36364F));
			this.tlpCondition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tlpCondition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.63636F));
			this.tlpCondition.Controls.Add(this.xlblManuName, 0, 0);
			this.tlpCondition.Controls.Add(this.xtxtMenuName, 1, 0);
			this.tlpCondition.Controls.Add(this.abSearch, 2, 0);
			this.tlpCondition.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpCondition.Location = new System.Drawing.Point(5, 11);
			this.tlpCondition.Margin = new System.Windows.Forms.Padding(0);
			this.tlpCondition.Name = "tlpCondition";
			this.tlpCondition.RowCount = 1;
			this.tlpCondition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpCondition.Size = new System.Drawing.Size(959, 28);
			this.tlpCondition.TabIndex = 0;
			// 
			// xlblManuName
			// 
			appearance32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			appearance32.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(139)))), ((int)(((byte)(141)))));
			appearance32.ForeColor = System.Drawing.Color.Black;
			appearance32.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance32.TextHAlignAsString = "Right";
			appearance32.TextVAlignAsString = "Middle";
			this.xlblManuName.Appearance = appearance32;
			this.xlblManuName.BackColorInternal = System.Drawing.Color.White;
			this.xlblManuName.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xlblManuName.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xlblManuName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xlblManuName.Location = new System.Drawing.Point(3, 3);
			this.xlblManuName.Name = "xlblManuName";
			this.xlblManuName.Padding = new System.Drawing.Size(2, 0);
			this.xlblManuName.Size = new System.Drawing.Size(124, 22);
			this.xlblManuName.TabIndex = 0;
			this.xlblManuName.Text = "메뉴 이름(일부)";
			this.xlblManuName.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// xtxtMenuName
			// 
			appearance33.BackColor = System.Drawing.Color.White;
			appearance33.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(139)))), ((int)(((byte)(141)))));
			appearance33.ForeColor = System.Drawing.Color.Black;
			appearance33.TextHAlignAsString = "Left";
			appearance33.TextVAlignAsString = "Middle";
			this.xtxtMenuName.Appearance = appearance33;
			this.xtxtMenuName.AutoSize = false;
			this.xtxtMenuName.BackColor = System.Drawing.Color.White;
			this.xtxtMenuName.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xtxtMenuName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
			this.xtxtMenuName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xtxtMenuName.Location = new System.Drawing.Point(133, 3);
			this.xtxtMenuName.Name = "xtxtMenuName";
			this.xtxtMenuName.Size = new System.Drawing.Size(251, 22);
			this.xtxtMenuName.TabIndex = 0;
			this.xtxtMenuName.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// abSearch
			// 
			appearance34.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(139)))), ((int)(((byte)(141)))));
			appearance34.ForeColor = System.Drawing.Color.Black;
			appearance34.Image = ((object)(resources.GetObject("appearance34.Image")));
			appearance34.ImageHAlign = Infragistics.Win.HAlign.Left;
			appearance34.ImageVAlign = Infragistics.Win.VAlign.Middle;
			appearance34.TextHAlignAsString = "Center";
			appearance34.TextVAlignAsString = "Middle";
			this.abSearch.Appearance = appearance34;
			this.abSearch.BackColorInternal = System.Drawing.Color.White;
			this.abSearch.ButtonSize = AnyFactory.FX.Win.Controls.ActionButton.ButtonSize.Size16;
			this.abSearch.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsXPCommandButton;
			this.abSearch.ButtonType = AnyFactory.FX.Win.Controls.ActionButton.ButtonType.Search;
			this.abSearch.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance35.ForeColor = System.Drawing.Color.BlueViolet;
			appearance35.TextHAlignAsString = "Center";
			appearance35.TextVAlignAsString = "Middle";
			this.abSearch.HotTrackAppearance = appearance35;
			this.abSearch.ImageAndTextLayout = Infragistics.Win.HAlign.Left;
			this.abSearch.Location = new System.Drawing.Point(390, 3);
			this.abSearch.Name = "abSearch";
			this.abSearch.Padding = new System.Drawing.Size(12, 0);
			this.abSearch.ShowFocusRect = false;
			this.abSearch.ShowOutline = false;
			this.abSearch.Size = new System.Drawing.Size(114, 22);
			this.abSearch.TabIndex = 1;
			this.abSearch.Text = "조회";
			this.abSearch.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// xgrbReferences
			// 
			appearance15.BackColor = System.Drawing.Color.White;
			appearance15.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			appearance15.ForeColor = System.Drawing.Color.Black;
			appearance15.TextVAlignAsString = "Middle";
			this.xgrbReferences.Appearance = appearance15;
			this.xgrbReferences.BackColorInternal = System.Drawing.Color.White;
			this.xgrbReferences.ContentPadding.Bottom = 2;
			this.xgrbReferences.ContentPadding.Left = 2;
			this.xgrbReferences.ContentPadding.Right = 2;
			this.xgrbReferences.ContentPadding.Top = 2;
			this.xgrbReferences.Controls.Add(this.xgridReferences);
			this.xgrbReferences.Dock = System.Windows.Forms.DockStyle.Fill;
			appearance19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(222)))), ((int)(((byte)(214)))));
			appearance19.FontData.BoldAsString = "True";
			appearance19.ForeColor = System.Drawing.Color.Black;
			appearance19.ForegroundAlpha = Infragistics.Win.Alpha.Opaque;
			appearance19.TextVAlignAsString = "Top";
			this.xgrbReferences.HeaderAppearance = appearance19;
			this.xgrbReferences.HeaderBorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded4;
			this.xgrbReferences.HeaderPosition = Infragistics.Win.Misc.GroupBoxHeaderPosition.TopInsideBorder;
			this.xgrbReferences.Location = new System.Drawing.Point(3, 293);
			this.xgrbReferences.Name = "xgrbReferences";
			this.xgrbReferences.Size = new System.Drawing.Size(969, 234);
			this.xgrbReferences.TabIndex = 1;
			this.xgrbReferences.Text = "참조된 Reference Assembly 정보";
			// 
			// xgridReferences
			// 
			appearance1.BackColor = System.Drawing.Color.White;
			appearance1.BorderColor = System.Drawing.Color.Gray;
			appearance1.ForeColor = System.Drawing.Color.Black;
			this.xgridReferences.DisplayLayout.Appearance = appearance1;
			this.xgridReferences.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridReferences.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
			appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
			appearance2.BorderColor = System.Drawing.SystemColors.Window;
			this.xgridReferences.DisplayLayout.GroupByBox.Appearance = appearance2;
			appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xgridReferences.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
			this.xgridReferences.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridReferences.DisplayLayout.GroupByBox.Hidden = true;
			appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
			appearance4.BackColor2 = System.Drawing.SystemColors.Control;
			appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
			this.xgridReferences.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
			this.xgridReferences.DisplayLayout.GroupByBox.Style = Infragistics.Win.UltraWinGrid.GroupByBoxStyle.Compact;
			this.xgridReferences.DisplayLayout.MaxColScrollRegions = 1;
			this.xgridReferences.DisplayLayout.MaxRowScrollRegions = 1;
			appearance5.BackColor = System.Drawing.SystemColors.Window;
			appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
			this.xgridReferences.DisplayLayout.Override.ActiveCellAppearance = appearance5;
			appearance6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(197)))), ((int)(((byte)(183)))));
			appearance6.BorderColor = System.Drawing.Color.Orange;
			appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.xgridReferences.DisplayLayout.Override.ActiveRowAppearance = appearance6;
			this.xgridReferences.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridReferences.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridReferences.DisplayLayout.Override.BorderStyleSpecialRowSeparator = Infragistics.Win.UIElementBorderStyle.None;
			this.xgridReferences.DisplayLayout.Override.BorderStyleSummaryFooter = Infragistics.Win.UIElementBorderStyle.Solid;
			this.xgridReferences.DisplayLayout.Override.BorderStyleSummaryValue = Infragistics.Win.UIElementBorderStyle.None;
			appearance7.BackColor = System.Drawing.SystemColors.Window;
			this.xgridReferences.DisplayLayout.Override.CardAreaAppearance = appearance7;
			appearance8.BorderColor = System.Drawing.Color.Silver;
			appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
			this.xgridReferences.DisplayLayout.Override.CellAppearance = appearance8;
			this.xgridReferences.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
			this.xgridReferences.DisplayLayout.Override.CellPadding = 0;
			this.xgridReferences.DisplayLayout.Override.ColumnAutoSizeMode = Infragistics.Win.UltraWinGrid.ColumnAutoSizeMode.VisibleRows;
			this.xgridReferences.DisplayLayout.Override.ColumnSizingArea = Infragistics.Win.UltraWinGrid.ColumnSizingArea.EntireColumn;
			this.xgridReferences.DisplayLayout.Override.FixedCellSeparatorColor = System.Drawing.Color.OrangeRed;
			this.xgridReferences.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;
			appearance9.BackColor = System.Drawing.SystemColors.Control;
			appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
			appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
			appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
			appearance9.BorderColor = System.Drawing.SystemColors.Window;
			this.xgridReferences.DisplayLayout.Override.GroupByRowAppearance = appearance9;
			appearance10.TextHAlignAsString = "Center";
			appearance10.TextVAlignAsString = "Middle";
			this.xgridReferences.DisplayLayout.Override.HeaderAppearance = appearance10;
			this.xgridReferences.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
			this.xgridReferences.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
			appearance21.BackColor = System.Drawing.Color.LightBlue;
			this.xgridReferences.DisplayLayout.Override.HotTrackCellAppearance = appearance21;
			appearance22.BorderColor = System.Drawing.Color.Orange;
			this.xgridReferences.DisplayLayout.Override.HotTrackRowAppearance = appearance22;
			appearance23.BackColor = System.Drawing.Color.LightBlue;
			this.xgridReferences.DisplayLayout.Override.HotTrackRowCellAppearance = appearance23;
			appearance24.BackColor = System.Drawing.Color.Silver;
			this.xgridReferences.DisplayLayout.Override.HotTrackRowSelectorAppearance = appearance24;
			appearance25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(235)))), ((int)(((byte)(230)))));
			this.xgridReferences.DisplayLayout.Override.RowAlternateAppearance = appearance25;
			appearance11.BackColor = System.Drawing.Color.White;
			appearance11.BorderColor = System.Drawing.Color.Silver;
			this.xgridReferences.DisplayLayout.Override.RowAppearance = appearance11;
			appearance26.TextHAlignAsString = "Center";
			appearance26.TextVAlignAsString = "Middle";
			this.xgridReferences.DisplayLayout.Override.RowSelectorAppearance = appearance26;
			appearance27.TextHAlignAsString = "Center";
			appearance27.TextVAlignAsString = "Middle";
			this.xgridReferences.DisplayLayout.Override.RowSelectorHeaderAppearance = appearance27;
			this.xgridReferences.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			this.xgridReferences.DisplayLayout.Override.SpecialRowSeparator = Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow;
			appearance28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(225)))), ((int)(((byte)(190)))));
			this.xgridReferences.DisplayLayout.Override.SpecialRowSeparatorAppearance = appearance28;
			this.xgridReferences.DisplayLayout.Override.SpecialRowSeparatorHeight = 3;
			appearance29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(237)))), ((int)(((byte)(218)))));
			appearance29.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(196)))), ((int)(((byte)(211)))));
			this.xgridReferences.DisplayLayout.Override.SummaryFooterAppearance = appearance29;
			this.xgridReferences.DisplayLayout.Override.SummaryFooterCaptionVisible = Infragistics.Win.DefaultableBoolean.False;
			appearance30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(237)))), ((int)(((byte)(218)))));
			appearance30.TextVAlignAsString = "Middle";
			this.xgridReferences.DisplayLayout.Override.SummaryValueAppearance = appearance30;
			appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
			this.xgridReferences.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
			scrollBarLook2.ViewStyle = Infragistics.Win.UltraWinScrollBar.ScrollBarViewStyle.WindowsVista;
			this.xgridReferences.DisplayLayout.ScrollBarLook = scrollBarLook2;
			this.xgridReferences.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			this.xgridReferences.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			this.xgridReferences.DisplayLayout.UseFixedHeaders = true;
			this.xgridReferences.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
			this.xgridReferences.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xgridReferences.Location = new System.Drawing.Point(5, 29);
			this.xgridReferences.Name = "xgridReferences";
			this.xgridReferences.Size = new System.Drawing.Size(959, 200);
			this.xgridReferences.TabIndex = 0;
			this.xgridReferences.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnUpdate;
			this.xgridReferences.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
			this.xgridReferences.AfterRowActivate += new System.EventHandler(this.xgridList_AfterRowActivate);
			// 
			// MenuIndexForm
			// 
			this.AcceptButton = this.abSearch;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.abFormExit;
			this.ClientSize = new System.Drawing.Size(983, 588);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "MenuIndexForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "메뉴 및 폼 정보";
			this.ThemeStyle = AnyFactory.FX.Win.ThemeStyle.Theme3;
			this.Load += new System.EventHandler(this.MenuIndexForm_Load);
			this.Shown += new System.EventHandler(this.MenuIndexForm_Shown);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MenuIndexForm_KeyDown);
			this.pnlContainerBody.ResumeLayout(false);
			this.tlpMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgrbBottom)).EndInit();
			this.xgrbBottom.ResumeLayout(false);
			this.tlpBottom.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgrbList)).EndInit();
			this.xgrbList.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgridList)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xgrbCondition)).EndInit();
			this.xgrbCondition.ResumeLayout(false);
			this.tlpCondition.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xtxtMenuName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.xgrbReferences)).EndInit();
			this.xgrbReferences.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xgridReferences)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.TableLayoutPanel tlpCondition;
		private Infragistics.Win.Misc.UltraLabel xlblManuName;
		private Infragistics.Win.UltraWinGrid.UltraGrid xgridList;
		private Infragistics.Win.Misc.UltraGroupBox xgrbList;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor xtxtMenuName;
		private AnyFactory.FX.Win.Controls.ActionButton.ActionButtonBase abSearch;
		private Infragistics.Win.Misc.UltraGroupBox xgrbCondition;
		private Infragistics.Win.Misc.UltraGroupBox xgrbBottom;
		private System.Windows.Forms.TableLayoutPanel tlpBottom;
		private AnyFactory.FX.Win.Controls.ActionButton.ActionButtonBase abFormExit;
		private Infragistics.Win.Misc.UltraLabel xlblStatus;
		private Infragistics.Win.Misc.UltraButton xbtnOpenProject;
		private Infragistics.Win.Misc.UltraGroupBox xgrbReferences;
		private Infragistics.Win.UltraWinGrid.UltraGrid xgridReferences;
	}
}