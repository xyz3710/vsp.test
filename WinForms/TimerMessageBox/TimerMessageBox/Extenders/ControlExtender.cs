/**********************************************************************************************************************/
/*	Domain		:	TimerMessageBox.ControlExtender
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 11월 6일 금요일 오후 3:05
/*	Purpose		:	System.Windowns.Forms.Control에 대한 확장 Method를 제공합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2009년 11월 6일 금요일 오후 3:06
/**********************************************************************************************************************/

#region Using
using System;
using System.Windows.Forms;
#endregion

namespace AnyFactory.FX.Win.Controls.Extenders
{
	/// <summary>
	/// <seealso cref="System.Windowns.Forms.Control"/>에 대한 확장 Method를 제공합니다.
	/// </summary>
	public static class ControlExtender
	{
		#region AddCounterAction Handler
		/// <summary>
		/// <seealso cref="AddCounterAction"/>의 카운터가 완료된 뒤 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">CounterTickedHandler가 처리하는 control</param>
		public delegate void CounterCompletedHandler(Control sender);

		/// <summary>
		/// <seealso cref="AddCounterAction"/>의 카운터가 감소될 때 처리할 메서드를 나타냅니다.
		/// </summary>
		/// <param name="sender">CounterTickedHandler가 처리하는 control</param>
		/// <param name="tickedCount">감소되는 카운터</param>
		public delegate void CounterTickedHandler(Control sender, int tickedCount);
		#endregion

		#region AddCounterAction
		/// <summary>
		/// Control에 대해서 1초 간격의 카운터 기능을 할당합니다.
		/// </summary>
		/// <param name="control">할당할 control</param>
		/// <param name="count">1초 간격으로 카운팅 할 숫자입니다.</param>
		/// <param name="counterCompletedAction">couter가 완료되어서 1이 되면 처리할 메서드입니다.</param>
		/// <param name="counterTickedAction">tickInterval에 의해 timer tick이 발생하면 처리할 메서드입니다.</param>
		/// <example>아래 예제는 button1에 1초 간격으로 5번 카운팅(5, 4, 3, 2, 1) 한 뒤 OnButton1ClickAction를 호출 합니다.
		/// <code>
		/// Button button1 = new Button();
		/// button1.AddCounterAction(5, OnTimerTickAction);
		/// 
		/// private void OnTimerTickAction(object sender, int counter)
		/// {
		///		Button button = sender as Button;
		/// 
		///		if (button != null)
		///		{
		///			button.Text = string.Format("{0}({1})", button.Tag, counter);
		/// 
		///			if (counter == 1)
		///				OnButton1ClickAction();
		///		}
		/// }
		/// </code>
		/// </example>
		public static void AddCounterAction(
			this Control control,
			int count,
			CounterCompletedHandler counterCompletedAction,
			CounterTickedHandler counterTickedAction)
		{
			int tickInterval = 1000;

			AddCounterAction(control, count, counterCompletedAction, counterTickedAction, tickInterval);
		}

		/// <summary>
		/// Control에 대해서 tickInterval 간격의 카운터 기능을 할당합니다.
		/// </summary>
		/// <param name="control">할당할 control</param>
		/// <param name="count">tickInterval의 간격으로 카운팅 할 숫자입니다.</param>
		/// <param name="counterCompletedAction">couter가 완료되어서 1이 되면 처리할 메서드입니다.</param>
		/// <param name="counterTickedAction">tickInterval에 의해 timer tick이 발생하면 처리할 메서드입니다.</param>
		/// <param name="tickInterval">이벤트를 발생시킬 시간(밀리초)를 설정합니다.</param>
		/// <example>아래 예제는 button1에 500 밀리초 간격으로 5번 카운팅(5, 4, 3, 2, 1) 한 뒤 OnButton1ClickAction를 호출 합니다.
		/// <code>
		/// Button button1 = new Button();
		/// button1.AddCounterAction(5, OnTimerTickAction, 500);
		/// 
		/// private void OnTimerTickAction(object sender, int counter)
		/// {
		/// 	Button button = sender as Button;
		/// 
		/// 	if (button != null)
		/// 	{
		/// 		button.Text = string.Format("{0}({1})", button.Tag, counter);
		/// 
		/// 		if (counter == 1)
		/// 			OnButton1ClickAction();
		/// 	}
		/// }
		/// </code>
		/// </example>
		public static void AddCounterAction(
			this Control control,
			int count,
			CounterCompletedHandler counterCompletedAction,
			CounterTickedHandler counterTickedAction,
			int tickInterval)
		{
			Timer timer = new Timer
			{
				Interval = tickInterval,
			};
			int internalCounter = count;

			timer.Tick += new EventHandler(delegate(object s, EventArgs ea)
			{
				if (internalCounter < 1)
				{
					timer.Stop();

					if (counterCompletedAction != null)
						counterCompletedAction(control);

					return;
				}

				if (counterTickedAction != null)
					counterTickedAction(control, internalCounter--);
			});

			timer.Start();
		}
		#endregion

		#region AddCounterText
		/// <summary>
		/// Control의 Text 오른쪽에 1초 간격의 카운터 기능을 추가 합니다.
		/// </summary>
		/// <param name="control">Text에 할당할 control</param>
		/// <param name="count">1초 간격으로 카운팅 할 숫자입니다.</param>
		/// <param name="clickEventHandler">카운트가 완료되거나 중간에 Click 할 때 처리할 메서드 입니다.</param>
		/// <example>아래 예제는 button1에 700 밀리초 간격으로 button1의 Text를 (count#)Text 형태로 변경하여<br></br>
		/// 3번 카운팅(3, 2, 1) 한 뒤 button1_Click를 호출 합니다.
		/// <code>
		/// button1.AddCounterText(3, button1_Click, true, 700);
		/// </code>
		/// </example>
		/// <remarks>완료 후 원래의 Text로 변경합니다.</remarks>
		public static void AddCounterText(
			this Control control,
			int count,
			EventHandler clickEventHandler)
		{
			int tickInterval = 1000;

			AddCounterText(control, count, clickEventHandler, false, tickInterval);
		}

		/// <summary>
		/// Control의 Text에 tickInterval 간격의 카운터 기능을 추가 합니다.
		/// </summary>
		/// <param name="control">Text에 할당할 control</param>
		/// <param name="count">tickInterval의 간격으로 카운팅 할 숫자입니다.</param>
		/// <param name="clickEventHandler">카운트가 완료되거나 중간에 Click 할 때 처리할 메서드 입니다.</param>
		/// <param name="leftSideOfText">카운터를 텍스트 왼쪽에 놓을지 여부 입니다.</param>
		/// <param name="tickInterval">이벤트를 발생시킬 시간(밀리초)를 설정합니다.</param>
		/// <example>아래 예제는 button1에 700 밀리초 간격으로 button1의 Text를 (count#)Text 형태로 변경하여<br/>
		/// 3번 카운팅(3, 2, 1) 한 뒤 button1_Click를 호출 합니다.
		/// <code>
		/// button1.AddCounterText(3, button1_Click, true, 700);
		/// </code>
		/// </example>
		/// <remarks>완료 후 원래의 Text로 변경합니다.</remarks>
		public static void AddCounterText(
			this Control control,
			int count,
			EventHandler clickEventHandler,
			bool leftSideOfText,
			int tickInterval)
		{
			string originalText = control.Text;

			control.AddCounterAction(
				count,
				sender =>
				{
					control.Text = originalText;

					if (clickEventHandler != null)
						clickEventHandler(sender, new EventArgs());
				},
				(sender, tickedCount) =>
				{
					control.Text = leftSideOfText == true ? 
		                                       string.Format("({0}){1}", tickedCount, originalText) : 
		                                       string.Format("{0}({1})", originalText, tickedCount);
				},
				tickInterval);
		}

		/// <summary>
		/// Control의 Text에 1초 간격의 카운터 기능을 추가 합니다.
		/// </summary>
		/// <param name="control">Text에 할당할 control</param>
		/// <param name="count">tickInterval의 간격으로 카운팅 할 숫자입니다.</param>
		/// <param name="counterCompletedHandler">카운트가 완료되거나 중간에 Click 할 때 처리할 메서드 입니다.</param>
		/// <param name="displayFormat">originalText, tickedCount 순서로 보여질 <seealso cref="System.String.Format"></seealso> 문자열을 입력합니다.<br></br>
		/// ex) originalText : Text, tickedCount : 3 일 때, {0}({1}) =&gt; Text(3)
		/// </param>
		/// <example>아래 예제는 button1에 1초 간격으로 button1의 Text를 displayFormat 형태로 변경하여<br></br>
		/// 3번 카운팅(3, 2, 1) 한 뒤 TestMethod를 호출 합니다.
		/// <code>
		/// button1.AddCounterText(3, TestMethod, "{0}({1})");
		/// </code>
		/// </example>
		/// <remarks>완료 후 원래의 Text로 변경합니다.</remarks>
		public static void AddCounterText(
            this Control control, 
            int count, 
            CounterCompletedHandler counterCompletedHandler, 
            string displayFormat)
		{
			int tickInterval = 1000;
			
			AddCounterText(control, count, counterCompletedHandler, displayFormat, tickInterval);
		}

        /// <summary>
		/// Control의 Text에 tickInterval 간격의 카운터 기능을 추가 합니다.
		/// </summary>
		/// <param name="control">Text에 할당할 control</param>
		/// <param name="count">tickInterval의 간격으로 카운팅 할 숫자입니다.</param>
		/// <param name="counterCompletedHandler">카운트가 완료되거나 중간에 Click 할 때 처리할 메서드 입니다.</param>
		/// <param name="displayFormat">originalText, tickedCount 순서로 보여질 <seealso cref="System.String.Format"/> 문자열을 입력합니다.<br/>
		/// ex) originalText : Text, tickedCount : 3 일 때, {0}({1}) =&gt; Text(3)
		/// </param>
		/// <param name="tickInterval">이벤트를 발생시킬 시간(밀리초)를 설정합니다.</param>
		/// <example>아래 예제는 button1에 700 밀리초 간격으로 button1의 Text를 displayFormat 형태로 변경하여<br/>
		/// 3번 카운팅(3, 2, 1) 한 뒤 TestMethod를 호출 합니다.
		/// <code>
		/// button1.AddCounterText(3, TestMethod, true, 700);
		/// </code>
		/// </example>
		/// <remarks>완료 후 원래의 Text로 변경합니다.</remarks>
		public static void AddCounterText(
			this Control control,
			int count,
			CounterCompletedHandler counterCompletedHandler,
			string displayFormat,
			int tickInterval)
		{
			string originalText = control.Text;

			control.AddCounterAction(
				count,
				sender =>
				{
					control.Text = originalText;

					if (counterCompletedHandler != null)
						counterCompletedHandler(sender);
				},
				(sender, tickedCount) =>
				{
					control.Text = string.Format(displayFormat, originalText, tickedCount);
				},
				tickInterval);
		}
		#endregion
	}
}
