﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessageHook
{
	/// <summary>
	/// Class GlobalKeyboardHook.
	/// </summary>
	public class GlobalKeyboardHook : IDisposable
	{
		#region DLL Imports
		[DllImport(KERNEL32_DLL)]
		static extern IntPtr LoadLibrary(string lpFileName);

		[DllImport(USER32_DLL, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
		static extern IntPtr SetWindowsHookEx(int hookID, KeyboardHookProc callback, IntPtr hInstance, uint threadID);

		[DllImport(USER32_DLL, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
		static extern bool UnhookWindowsHookEx(IntPtr hookHandle);

		[DllImport(USER32_DLL, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
		static extern int CallNextHookEx(IntPtr hookHandle, int nCode, int wParam, ref GlobalKeyboardHookStruct lParam);

		[DllImport(USER32_DLL)]
		static extern short GetKeyState(VirtualKeyStates virtualKeyStates);

		[DllImport(USER32_DLL, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
		static extern IntPtr SetWindowsHookEx(int hookID, MouseHookProc callback, IntPtr hInstance, uint threadID);

		[DllImport(USER32_DLL, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
		static extern int CallNextHookEx(IntPtr hookHandle, int nCode, IntPtr wParam, IntPtr lParam);
		#endregion

		#region Constants
		private const string KERNEL32_DLL = "kernel32.dll";
		private const string USER32_DLL = "user32.dll";

		private const int WM_KEYDOWN = 0x100;
		private const int WM_KEYUP = 0x101;
		private const int WM_SYSKEYDOWN = 0x104;
		private const int WM_SYSKEYUP = 0x105;
		private const int WH_KEYBOARD_LL = 13;
		private const int WH_MOUSE_LL = 14;

		private enum MouseMessages
		{
			WM_LBUTTONDOWN = 0x0201,
			WM_LBUTTONUP = 0x0202,
			WM_MOUSEMOVE = 0x0200,
			WM_MOUSEWHEEL = 0x020A,
			WM_RBUTTONDOWN = 0x0204,
			WM_RBUTTONUP = 0x0205
		}
		#endregion

		#region Fields
		private delegate int KeyboardHookProc(int nCode, int wParam, ref GlobalKeyboardHookStruct lParam);
		private delegate int MouseHookProc(int nCode, IntPtr wParam, IntPtr lParam);

		private IntPtr _hookKeyboardHandle;
		private IntPtr _hookMouseHandle;
		private KeyboardHookProc _hookKeyboardProc;
		private MouseHookProc _hookMouseProc;
		private Keys _previousKey;
		private bool _hooked;
		#endregion

		#region Events
		/// <summary>
		/// <see cref="E:System.Windows.Forms.Control.KeyDown" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.Windows.Forms.KeyEventArgs" />입니다.</param>
		public event KeyEventHandler KeyDown;
		/// <summary>
		/// <see cref="E:System.Windows.Forms.Control.KeyUp" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.Windows.Forms.KeyEventArgs" />입니다.</param>
		public event KeyEventHandler KeyUp;
		#endregion

		private struct GlobalKeyboardHookStruct
		{
			public int vkCode;
			public int scanCode;
			public int flags;
			public int time;
			public int dwExtraInfo;
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct MSLLHOOKSTRUCT
		{
			public POINT pt;
			public uint mouseData;
			public uint flags;
			public uint time;
			public IntPtr dwExtraInfo;
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct POINT
		{
			public int x;
			public int y;
		}

		#region Privates
		private bool IsKeyPressed(VirtualKeyStates virtualKeyStates)
		{
			return (GetKeyState(virtualKeyStates) & (1 << 7)) == 128;
		}

		private int HookKeyboardProc(int code, int wParam, ref GlobalKeyboardHookStruct lParam)
		{
			if (code >= 0)
			{
				Keys key = (Keys)lParam.vkCode;
				//System.Diagnostics.Debug.WriteLine("key: {0}", key);

				if (HookedKeys.Count == 0 || HookedKeys.Contains(key) == true)
				{
					if (key == Keys.Return)
					{
						key |= Keys.Enter;
					}

					if (key == Keys.LControlKey || key == Keys.RControlKey)
					{
						key = Keys.Control;
						key |= Keys.ControlKey;
					}

					if (key == Keys.LShiftKey || key == Keys.RShiftKey)
					{
						key = Keys.Shift;
						key |= Keys.ShiftKey;
					}

					if (key == Keys.LMenu || key == Keys.RMenu)
					{
						key = Keys.Alt;
						key |= Keys.Menu;
					}

					//key |= _previousKey;

					KeyEventArgs ke = new KeyEventArgs(key);

					_previousKey = key;

					if (KeyUp != null && (wParam == WM_KEYUP || wParam == WM_SYSKEYUP))
					{
						KeyUp(this, ke);
						_previousKey = Keys.None;
					}
					else if (KeyDown != null && (wParam == WM_KEYDOWN || wParam == WM_SYSKEYDOWN))
					{
						KeyDown(this, ke);
					}

					if (ke.Handled == true)
					{
						return 1;
					}
				}
			}

			return CallNextHookEx(_hookKeyboardHandle, code, wParam, ref lParam);
		}

		private int HookMouseProc(int code, IntPtr wParam, IntPtr lParam)
		{
			if (code >= 0 && MouseMessages.WM_LBUTTONDOWN == (MouseMessages)wParam)
			{
				MSLLHOOKSTRUCT hookStruct = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
				Console.WriteLine("{0}: {1}, {2}", (MouseMessages)wParam, hookStruct.pt.x, hookStruct.pt.y);

				return 1;
			}

			return CallNextHookEx(_hookMouseHandle, code, wParam, lParam);
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="GlobalKeyboardHook"/> class.
		/// <remarks>생성 시 <seealso cref="Hook"/>메서드를 실행합니다.</remarks>
		/// </summary>
		public GlobalKeyboardHook()
		{
			HookedKeys = new List<Keys>();
			SetHook();
		}

		/// <summary>
		/// Finalizes an instance of the <see cref="GlobalKeyboardHook"/> class.
		/// </summary>
		~GlobalKeyboardHook()
		{
			Unhook();
		}
		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets the hooked keys.
		/// </summary>
		/// <value>The hooked keys.</value>
		public List<Keys> HookedKeys
		{
			get;
			set;
		}
		#endregion

		/// <summary>
		/// Sets the hooked keys.
		/// </summary>
		/// <param name="keys">The key.</param>
		public void SetHookedKeys(params Keys[] keys)
		{
			if (keys != null)
			{
				HookedKeys = keys.ToList();
			}
		}

		/// <summary>
		/// 지정된 키가 눌렸을 때 키보드를 후킹 합니다.
		/// <remarks><see cref="HookedKeys"/>가 지정되면 지정된 키만 후킹합니다.</remarks>
		/// </summary>
		public void SetHook()
		{
			if (_hooked == true)
			{
				return;
			}

			_hookKeyboardProc = new KeyboardHookProc(HookKeyboardProc);
			_hookMouseProc = new MouseHookProc(HookMouseProc);
			IntPtr hInstance = LoadLibrary("user32");
			_hookKeyboardHandle = SetWindowsHookEx(WH_KEYBOARD_LL, _hookKeyboardProc, hInstance, 0);
			//_hookMouseHandle = SetWindowsHookEx(WH_MOUSE_LL, _hookMouseProc, hInstance, 0);
			_hooked = true;
		}

		/// <summary>
		/// 키보드 후킹을 중지합니다.
		/// </summary>
		public void Unhook()
		{
			if (_hooked == false)
			{
				return;
			}

			UnhookWindowsHookEx(_hookKeyboardHandle);
			UnhookWindowsHookEx(_hookMouseHandle);
			_hooked = false;
		}

		#region IDisposable 멤버

		/// <summary>
		/// 관리되지 않는 리소스의 확보, 해제 또는 다시 설정과 관련된 응용 프로그램 정의 작업을 수행합니다.
		/// </summary>
		public void Dispose()
		{
			Unhook();
		}

		#endregion
	}
}
