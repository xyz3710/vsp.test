﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessageHook
{
	public partial class Form1 : Form
	{
		[DllImport("user32.dll")]
		static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc callback, IntPtr hInstance, uint threadId);

		[DllImport("user32.dll")]
		static extern bool UnhookWindowsHookEx(IntPtr hInstance);

		[DllImport("user32.dll")]
		static extern IntPtr CallNextHookEx(IntPtr idHook, int nCode, int wParam, IntPtr lParam);

		[DllImport("kernel32.dll")]
		static extern IntPtr LoadLibrary(string lpFileName);

		[DllImport("user32.dll")]
		static extern short GetKeyState(VirtualKeyStates virtualKeyStates);

		private bool IsKeyPressed(VirtualKeyStates virtualKeyStates)
		{
			return (GetKeyState(virtualKeyStates) & (1 << 7)) == 128;
		}

		private const int WH_KEYBOARD_LL = 13;
		private const int WM_KEYDOWN = 0x100;

		private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
		private LowLevelKeyboardProc _keyboardProc;
		private IntPtr _hookId;

		public event Action<Keys, bool, bool> KeyDown2;
		public event KeyEventHandler KeyDown3;

		[StructLayout(LayoutKind.Sequential)]
		private class KBDLLHOOKSTRUCT
		{
			public uint vkCode;
			public uint scanCode;
			public KBDLLHOOKSTRUCTFlags flags;
			public uint time;
			public UIntPtr dwExtraInfo;
		}

		[Flags]
		private enum KBDLLHOOKSTRUCTFlags : uint
		{
			LLKHF_EXTENDED = 0x01,
			LLKHF_INJECTED = 0x10,
			LLKHF_ALTDOWN = 0x20,
			LLKHF_UP = 0x80,
		}

		public void SetHook()
		{
			IntPtr hInstance = LoadLibrary("User32");

			_keyboardProc = HookProc;
			_hookId = SetWindowsHookEx(WH_KEYBOARD_LL, _keyboardProc, hInstance, 0);
		}

		public void UnHook()
		{
			UnhookWindowsHookEx(_hookId);
		}

		public IntPtr HookProc(int code, IntPtr wParam, IntPtr lParam)
		{
			if (code >= 0 && wParam == (IntPtr)WM_KEYDOWN)
			{
				var vkCode = (VirtualKeyStates)Marshal.ReadInt32(lParam);
				var key = (Keys)vkCode;

				Console.WriteLine("{0}, {1}", key, (int)key);
				listBox1.Items.Insert(0, "You pressed a key : " + (Keys)vkCode);

				if (vkCode == VirtualKeyStates.VK_F11)
				{
					KBDLLHOOKSTRUCT replacementKey = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(KBDLLHOOKSTRUCT));
					replacementKey.vkCode = (uint)VirtualKeyStates.VK_F11;
					Marshal.StructureToPtr(replacementKey, lParam, false);

					return CallNextHookEx(_hookId, code, (int)wParam, lParam);
				}
				//  return IntPtr.Zero;		// suppress key
			}

			return CallNextHookEx(_hookId, code, (int)wParam, lParam);
		}

		public Form1()
		{
			InitializeComponent();
		}

		void _globalKeyboardHook_KeyUp(object sender, KeyEventArgs e)
		{
			//Console.WriteLine("Up: {0}, {1}, {2}, {3}", e.KeyCode, e.KeyData, e.Modifiers, e.SuppressKeyPress);
		}

		void _globalKeyboardHook_KeyDown(object sender, KeyEventArgs e)
		{
			Console.WriteLine("Down: {0}, {1}", e.KeyCode, e.Modifiers);

			if (e.KeyCode == Keys.F && e.Modifiers == Keys.Control)
			{
				MessageBox.Show("Ctrl+F가 눌렸습니다.");
			}
		}

		private GlobalKeyboardHook _globalKeyboardHook;

		private void Form1_Load(object sender, EventArgs e)
		{
			//SetHook();	//현재 클래스의 후킹 이용

			// GlobalKeyboardHook 후킹 이용
			_globalKeyboardHook = new GlobalKeyboardHook();
			_globalKeyboardHook.SetHookedKeys(Keys.Enter);
			_globalKeyboardHook.KeyDown += _globalKeyboardHook_KeyDown;
			_globalKeyboardHook.KeyUp += _globalKeyboardHook_KeyUp;

			// 현재 폼의 KeyPreView의 OnKeyDown 테스트
			//KeyPreview = true;
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);
			Console.WriteLine("Down: {0}, {1}", e.KeyCode, e.Modifiers);
			//Console.WriteLine("{0}, {1}, {2} => {3}", (int)e.KeyCode, (int)e.KeyData, e.Modifiers, e.KeyCode | Keys.Control);
			//Console.WriteLine("{0}, {1}, {2}", (int)e.KeyData, (int)e.Modifiers, (int)(e.KeyData & Keys.Modifiers));

			if (e.KeyCode == Keys.F && e.Modifiers == Keys.Control)
			{
				MessageBox.Show("Ctrl+F가 눌렸습니다.");
			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			//_globalKeyboardHook.Unhook();
			UnHook();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Console.WriteLine("button clicked. !!");
			MessageBox.Show("button clicked. !!");
		}

	}
}
