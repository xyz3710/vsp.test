using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace ContainerCollection
{
	public partial class FrmCollectionTestDriver : Form
	{
		private int _color;
		private Random _randomColor;

		public FrmCollectionTestDriver()
		{
			InitializeComponent();

			_randomColor = new Random(1);

			//SetControlsDockFill(Controls);
			SetControlsDockFill2(GetAllControls());
		}

		private Control[] GetAllControls()
		{
			ArrayList allControls = new ArrayList();
			Queue queue = new Queue();

			queue.Enqueue(Controls);

			while (queue.Count > 0)
			{
				Control.ControlCollection controls = queue.Dequeue() as Control.ControlCollection;

				if (controls == null || controls.Count == 0)
					continue;

				foreach (Control control in controls)
				{
					allControls.Add(control);

					queue.Enqueue(control.Controls);
				}
			}

			return (Control[])allControls.ToArray(typeof(Control));
		}

		private void SetControlsDockFill2(Control[] controls)
		{
			foreach (Control control in controls)
			{
				control.Dock = DockStyle.Fill;

				if (control is TableLayoutPanel)
					(control as TableLayoutPanel).CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

				control.BackColor = Color.FromArgb(_randomColor.Next(1, 255), _randomColor.Next(1, 255), _randomColor.Next(1, 255));
			}
		}

		private void SetControlsDockFill(Control.ControlCollection controls)
		{
			foreach (Control control in controls)
			{
				if (control.GetType().BaseType == typeof(Panel))
					SetControlsDockFill(control.Controls);

				control.Dock = DockStyle.Fill;

				if (control is TableLayoutPanel)
					(control as TableLayoutPanel).CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

				control.BackColor = Color.FromArgb(_randomColor.Next(1, 255), _randomColor.Next(1, 255), _randomColor.Next(1, 255));
			}
		}
	}
}