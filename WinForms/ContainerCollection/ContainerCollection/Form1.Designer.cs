﻿namespace ContainerCollection
{
	partial class FrmCollectionTestDriver
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.tlpLeft = new System.Windows.Forms.TableLayoutPanel();
			this.tlpLeft1 = new System.Windows.Forms.TableLayoutPanel();
			this.tlpLeft1_1 = new System.Windows.Forms.TableLayoutPanel();
			this.tlpLeft1_1_1 = new System.Windows.Forms.TableLayoutPanel();
			this.tlpRight = new System.Windows.Forms.TableLayoutPanel();
			this.tlpRight1 = new System.Windows.Forms.TableLayoutPanel();
			this.lblTest = new System.Windows.Forms.Label();
			this.tlpMain.SuspendLayout();
			this.tlpLeft.SuspendLayout();
			this.tlpLeft1.SuspendLayout();
			this.tlpLeft1_1.SuspendLayout();
			this.tlpRight.SuspendLayout();
			this.tlpRight1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.BackColor = System.Drawing.SystemColors.Control;
			this.tlpMain.ColumnCount = 2;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.Controls.Add(this.tlpLeft, 0, 0);
			this.tlpMain.Controls.Add(this.tlpRight, 1, 0);
			this.tlpMain.Location = new System.Drawing.Point(38, 58);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 1;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpMain.Size = new System.Drawing.Size(996, 460);
			this.tlpMain.TabIndex = 0;
			// 
			// tlpLeft
			// 
			this.tlpLeft.ColumnCount = 2;
			this.tlpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.47059F));
			this.tlpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.52941F));
			this.tlpLeft.Controls.Add(this.tlpLeft1, 0, 0);
			this.tlpLeft.Location = new System.Drawing.Point(3, 3);
			this.tlpLeft.Name = "tlpLeft";
			this.tlpLeft.RowCount = 2;
			this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft.Size = new System.Drawing.Size(476, 444);
			this.tlpLeft.TabIndex = 1;
			// 
			// tlpLeft1
			// 
			this.tlpLeft1.ColumnCount = 2;
			this.tlpLeft1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.08696F));
			this.tlpLeft1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.91304F));
			this.tlpLeft1.Controls.Add(this.tlpLeft1_1, 0, 0);
			this.tlpLeft1.Location = new System.Drawing.Point(3, 3);
			this.tlpLeft1.Name = "tlpLeft1";
			this.tlpLeft1.RowCount = 2;
			this.tlpLeft1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft1.Size = new System.Drawing.Size(345, 210);
			this.tlpLeft1.TabIndex = 0;
			// 
			// tlpLeft1_1
			// 
			this.tlpLeft1_1.ColumnCount = 2;
			this.tlpLeft1_1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft1_1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft1_1.Controls.Add(this.tlpLeft1_1_1, 0, 0);
			this.tlpLeft1_1.Location = new System.Drawing.Point(3, 3);
			this.tlpLeft1_1.Name = "tlpLeft1_1";
			this.tlpLeft1_1.RowCount = 2;
			this.tlpLeft1_1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft1_1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft1_1.Size = new System.Drawing.Size(267, 91);
			this.tlpLeft1_1.TabIndex = 0;
			// 
			// tlpLeft1_1_1
			// 
			this.tlpLeft1_1_1.ColumnCount = 2;
			this.tlpLeft1_1_1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft1_1_1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft1_1_1.Location = new System.Drawing.Point(3, 3);
			this.tlpLeft1_1_1.Name = "tlpLeft1_1_1";
			this.tlpLeft1_1_1.RowCount = 2;
			this.tlpLeft1_1_1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft1_1_1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpLeft1_1_1.Size = new System.Drawing.Size(107, 35);
			this.tlpLeft1_1_1.TabIndex = 0;
			// 
			// tlpRight
			// 
			this.tlpRight.ColumnCount = 3;
			this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101F));
			this.tlpRight.Controls.Add(this.tlpRight1, 0, 0);
			this.tlpRight.Location = new System.Drawing.Point(501, 3);
			this.tlpRight.Name = "tlpRight";
			this.tlpRight.RowCount = 1;
			this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpRight.Size = new System.Drawing.Size(314, 100);
			this.tlpRight.TabIndex = 0;
			// 
			// tlpRight1
			// 
			this.tlpRight1.BackColor = System.Drawing.SystemColors.Control;
			this.tlpRight1.ColumnCount = 2;
			this.tlpRight1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpRight1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpRight1.Controls.Add(this.lblTest, 0, 0);
			this.tlpRight1.Location = new System.Drawing.Point(3, 3);
			this.tlpRight1.Name = "tlpRight1";
			this.tlpRight1.RowCount = 2;
			this.tlpRight1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpRight1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tlpRight1.Size = new System.Drawing.Size(100, 94);
			this.tlpRight1.TabIndex = 0;
			// 
			// lblTest
			// 
			this.lblTest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblTest.Location = new System.Drawing.Point(3, 0);
			this.lblTest.Name = "lblTest";
			this.lblTest.Size = new System.Drawing.Size(44, 23);
			this.lblTest.TabIndex = 0;
			this.lblTest.Text = "label1";
			// 
			// FrmCollectionTestDriver
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1046, 530);
			this.Controls.Add(this.tlpMain);
			this.Name = "FrmCollectionTestDriver";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.tlpMain.ResumeLayout(false);
			this.tlpLeft.ResumeLayout(false);
			this.tlpLeft1.ResumeLayout(false);
			this.tlpLeft1_1.ResumeLayout(false);
			this.tlpRight.ResumeLayout(false);
			this.tlpRight1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private System.Windows.Forms.TableLayoutPanel tlpLeft;
		private System.Windows.Forms.TableLayoutPanel tlpLeft1;
		private System.Windows.Forms.TableLayoutPanel tlpLeft1_1;
		private System.Windows.Forms.TableLayoutPanel tlpLeft1_1_1;
		private System.Windows.Forms.TableLayoutPanel tlpRight;
		private System.Windows.Forms.TableLayoutPanel tlpRight1;
		private System.Windows.Forms.Label lblTest;
	}
}

