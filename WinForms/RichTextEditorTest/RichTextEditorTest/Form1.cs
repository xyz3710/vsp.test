﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RichTextEditorTest
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			textBox1.KeyDown += (sender, e) =>
			{
				if (e.KeyCode == Keys.Enter)
				{
					InsertSend(textBox1.Text);
				}
			};
			textBox2.KeyDown += (sender, e) =>
			{
				if (e.KeyCode == Keys.Enter)
				{
					InsertReceiver(textBox2.Text);
				}
			};
		}

		private void InsertReceiver(string text)
		{
			wbMessages.AddReceiverText(text, DateTime.Now.ToString("MM-dd HH:mm"));
		}

		private void InsertSend(string text)
		{
			wbMessages.AddSenderText(text, DateTime.Now.ToString("MM-dd HH:mm"));
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);

			bool[] isSender = new bool[] { false, true, false, false, true };
			string[] messages = new string[] { "받고", "보내고", "받고", "받고", "보내고" };
			string[] times = new string[] { "1", "2", "3", "4", "5" };

			wbMessages.InitMessages(isSender, messages, times);
		}
	}

	public static class WebBrowser4MessageExtensions
	{
		#region HTML
		/*
<html>
<style>
	table {
		width: 100%;
		border: 0px solid #bcbcbc;
		table-layout: fixed;
	}
	td {
		border: 0px solid #bcbcbc;
	}
	.space {
		border: 0px solid #bcbcbc;
	}
	.time {
		padding-left: 3px;
		border: 0px solid #bcbcbc;
	}
	.msg {
		display: inline-block;
		border-radius: 7px;
		background: #e5f7fd;
		padding: 8px;
		border: 1px solid #bcbcbc;
	}
</style>
<body>
	<table>
		<tr style='line-height:0px'>
			<td style='width:40px'>&nbsp;</td>
			<td style='width:100%'>&nbsp;</td>
			<td style='width:40px'>&nbsp;</td>
			<td style='width:90px'>&nbsp;</td>
		</tr>
		<tr>
			<td class='msg' colspan='2'>보낸사람<br/>내용</td>
			<td class='space'>&nbsp;</td>
			<td class='time'>05-06 10:39</td>
		</tr>
		<tr>
			<td class='space'>&nbsp;</td>
			<td class='msg' colspan='2'>받은사람<br/>내용</td>
			<td class='time'>05-06 10:39</td>
		</tr>
	</table>
</body>
</html>
		 */
		#endregion

		private const string SENDER_STRING = "<tr><td class='msg' colspan='2'>{0}</td><td class='space'>&nbsp;</td><td class='time'>{1}</td></tr>";
		private const string RECEIVER_STRING = "<tr><td class='space'>&nbsp;</td><td class='msg' colspan='2'>{0}</td><td class='time'>{1}</td></tr>";

		public static void InitMessages(this WebBrowser webBrowser, bool[] isSender, string[] messages, string[] times)
		{
			StringBuilder sb = new StringBuilder();

			sb = GetHeader(sb);

			for (int i = 0; i < messages.Length; i++)
			{
				if (isSender[i] == true)
				{
					sb.AppendFormat(SENDER_STRING, messages[i], times[i]);
				}
				else
				{
					sb.AppendFormat(RECEIVER_STRING, messages[i], times[i]);
				}
			}

			sb = GetFooter(sb);
			webBrowser.DocumentText = sb.ToString();
		}

		private static StringBuilder GetHeader(StringBuilder sb)
		{
			sb.AppendLine("<html>");
			sb.AppendLine("<style>");
			sb.AppendLine("table {");
			sb.AppendLine("width: 100%;");
			sb.AppendLine("border: 0px solid #bcbcbc;");
			sb.AppendLine("table-layout: fixed;");
			sb.AppendLine("}");
			sb.AppendLine("td {");
			sb.AppendLine("border: 0px solid #bcbcbc;");
			sb.AppendLine("}");
			sb.AppendLine(".space {");
			sb.AppendLine("border: 0px solid #bcbcbc;");
			sb.AppendLine("}");
			sb.AppendLine(".time {");
			sb.AppendLine("padding-left: 3px;");
			sb.AppendLine("border: 0px solid #bcbcbc;");
			sb.AppendLine("}");
			sb.AppendLine(".msg {");
			sb.AppendLine("display: inline-block;");
			sb.AppendLine("border-radius: 7px;");
			sb.AppendLine("background: #e5f7fd;");
			sb.AppendLine("padding: 8px;");
			sb.AppendLine("border: 1px solid #bcbcbc;");
			sb.AppendLine("}");
			sb.AppendLine("</style>");
			sb.AppendLine("<body>");
			sb.AppendLine("<table>");
			sb.AppendLine("<tr style='line-height:0px'>");
			sb.AppendLine("<td style='width:40px'>&nbsp;</td>");
			sb.AppendLine("<td style='width:100%'>&nbsp;</td>");
			sb.AppendLine("<td style='width:40px'>&nbsp;</td>");
			sb.AppendLine("<td style='width:90px'>&nbsp;</td>");
			sb.AppendLine("</tr>");

			return sb;
		}

		private static StringBuilder GetFooter(StringBuilder sb)
		{
			sb.AppendLine("</table>");
			sb.AppendLine("</body>");
			sb.AppendLine("</html>");

			return sb;
		}

		private static void SetMessageText(this WebBrowser webBrowser, string formatString, string message, string time)
		{
			if (message == string.Empty)
			{
				message = "&nbsp";
			}
			else
			{
				message = message.Replace("\r\n", "<br/>");
			}

			webBrowser.DocumentText = webBrowser.DocumentText.Insert(GetLastIndex(webBrowser), string.Format(formatString, message, time));
			webBrowser.ScrollMessageIntoView();
		}

		private static void ScrollMessageIntoView(this WebBrowser webBrowser)
		{
			System.Windows.Forms.Application.DoEvents();

			if (webBrowser.Document != null)
			{
				webBrowser.Document.Window.ScrollTo(0, webBrowser.Document.Body.ScrollRectangle.Height);
			}
		}

		private static int GetLastIndex(WebBrowser webBrowser)
		{
			return webBrowser.DocumentText.IndexOf("</table>");
		}

		public static void AddSenderText(this WebBrowser webBrowser, string message, string time)
		{
			webBrowser.SetMessageText(SENDER_STRING, message, time);
		}

		public static void AddReceiverText(this WebBrowser webBrowser, string message, string time)
		{
			webBrowser.SetMessageText(RECEIVER_STRING, message, time);
		}
	}
}
