﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
	public class TestClass
	{
		/// <summary>
		/// SubClass를 구하거나 설정합니다.
		/// </summary>
		/// <value>SubClass를 반환합니다.</value>
		public SubClass SubClass
		{
			get;
			set;
		}

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		/// <value>Name를 반환합니다.</value>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Size를 구하거나 설정합니다.
		/// </summary>
		/// <value>Size를 반환합니다.</value>
		public Size Size
		{
			get;
			set;
		}
	}
}
