﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class SubClass
	{
		/// <summary>
		/// Size를 구하거나 설정합니다.
		/// </summary>
		/// <value>Size를 반환합니다.</value>
		public Size Size
		{
			get;
			set;
		}

		/// <summary>
		/// Location를 구하거나 설정합니다.
		/// </summary>
		/// <value>Location를 반환합니다.</value>
		public Point Location
		{
			get;
			set;
		}

		/// <summary>
		/// Value를 구하거나 설정합니다.
		/// </summary>
		/// <value>Value를 반환합니다.</value>
		public long Value
		{
			get;
			set;
		}
		
	}
}
