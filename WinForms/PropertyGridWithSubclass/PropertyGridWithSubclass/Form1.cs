﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		protected override void OnShown(EventArgs e)
		{
			TestClass testClass = new TestClass
			{
				Name = "Name",
				Size = new System.Drawing.Size(50, 50),
				SubClass = new SubClass
				{
					Location = new Point(100, 100),
					Size = new Size(200, 200),
					Value = 10L,
				},
			};

			pg.PropertySort = PropertySort.CategorizedAlphabetical;
			pg.SelectedObject = testClass;
		}
	}
}
