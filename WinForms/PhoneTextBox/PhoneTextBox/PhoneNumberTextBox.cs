﻿// ****************************************************************************************************************** //
//	Domain		:	System.Windows.Forms.PhoneNumberTextBox
//	Creator		:	GS_DEV\xyz37(Kim Ki Won)
//	Create		:	2015년 9월 2일 수요일 오전 10:17
//	Purpose		:	한국식 전화번호 입력에 하이픈(-)을 자동으로 삽입해줍니다.
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Forms
{
	/// <summary>
	/// 한국식 전화번호 입력에 하이픈(-)을 자동으로 삽입해줍니다.
	/// </summary>
	public class PhoneNumberTextBox : TextBox
	{
		private bool _suppressKeyChanged;

		/// <summary>
		/// <see cref="E:System.Windows.Forms.Control.KeyPress" /> 이벤트를 발생시킵니다.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.Windows.Forms.KeyPressEventArgs" />입니다.</param>
		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			if ((e.KeyChar >= 32 && e.KeyChar < 48)
				|| (e.KeyChar > 57 && e.KeyChar < 255)
				|| Text.Length > 13)		// 숫자는 최대 14 자리까지(하이픈 포함)
			{

				if (e.KeyChar == (int)Keys.Back)
				{
					Text = Text.Remove(SelectionStart - 1, 1);
				}

				e.Handled = true;
			}

			if (char.IsDigit(e.KeyChar) == false)
			{
				MessageBox.Show(
					"숫자만 가능합니다.",
					"입력 오류",
					MessageBoxButtons.OK,
					MessageBoxIcon.Error);
				e.Handled = true;
			}
		}

		/// <summary>
		/// Handles the <see cref="E:TextChanged" /> event.
		/// </summary>
		/// <param name="e">이벤트 데이터가 들어 있는 <see cref="T:System.EventArgs" />입니다.</param>
		protected override void OnTextChanged(EventArgs e)
		{
			if (_suppressKeyChanged == true)
			{
				return;
			}

			_suppressKeyChanged = true;

			var length = GetOnlyDigitLength();

			RemoveHyphen();

			if (length == 8)
			{
				InsertTailHyphen();
			}
			else if (length > 8)
			{
				if (length == 9)
				{
					InsertHeaderHyphen(2);
				}
				else if (length == 10 || length == 11)
				{
					InsertHeaderHyphen(3);
				}
				else if (length >= 12)
				{
					InsertHeaderHyphen(4);
				}

				InsertTailHyphen();
			}

			SelectionStart = Text.Length;
			_suppressKeyChanged = false;
		}

		private void InsertHeaderHyphen(int digit)
		{
			Text = Text.Insert(digit, "-");
		}

		private void InsertTailHyphen()
		{
			Text = Text.Insert(Text.Length - 4, "-");
		}

		private void RemoveHyphen()
		{
			Text = Text.Replace("-", string.Empty);
		}

		private int GetOnlyDigitLength()
		{
			return Text.Length - Text.Count(c => c == '-');
		}
	}
}
