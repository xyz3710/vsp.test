﻿namespace PhoneTextBox
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.phoneTextBox1 = new System.Windows.Forms.PhoneNumberTextBox();
			this.SuspendLayout();
			// 
			// phoneTextBox1
			// 
			this.phoneTextBox1.Location = new System.Drawing.Point(157, 85);
			this.phoneTextBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.phoneTextBox1.Name = "phoneTextBox1";
			this.phoneTextBox1.Size = new System.Drawing.Size(223, 27);
			this.phoneTextBox1.TabIndex = 0;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(565, 224);
			this.Controls.Add(this.phoneTextBox1);
			this.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "전화 번호 입력 테스트 폼";
			this.TopMost = true;
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PhoneNumberTextBox phoneTextBox1;
	}
}

