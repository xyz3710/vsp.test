﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PersonsApp.Controllers
{
	/// <summary>
	/// Class Person.
	/// </summary>
	public class Person
	{
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>The identifier.</value>
		public int Id
		{
			get;
			set;
		}
		/// <summary>
		/// Gets or sets the first.
		/// </summary>
		/// <value>The first.</value>
		public string First
		{
			get;
			set;
		}
		/// <summary>
		/// Gets or sets the last.
		/// </summary>
		/// <value>The last.</value>
		public string Last
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Class PersonController.
	/// </summary>
	public class PersonController : ApiController
	{
		private List<Person> persons = new List<Person> { 
				new Person { Id = 1, First = "KiWon", Last = "Kim" }, 
				new Person { Id = 2, First = "KiWon", Last = "Kim" },
				new Person { Id = 3, First = "KiWon", Last = "Kim" },
			};

		// api/values
		/// <summary>
		/// Gets this instance.
		/// </summary>
		/// <returns>IEnumerable{Person}.</returns>
		public IEnumerable<Person> Get()
		{
			return persons;
		}

		// api/values/5
		/// <summary>
		/// Gets the specified identifier.
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <returns>IHttpActionResult.</returns>
		public IHttpActionResult Get(int id)
		{
			return Ok(persons.First());
		}

		// POST api/values
		/// <summary>
		/// Posts the specified value.
		/// </summary>
		/// <param name="value">The value.</param>
		public void Post([FromBody]Person value)
		{

		}
	}
}