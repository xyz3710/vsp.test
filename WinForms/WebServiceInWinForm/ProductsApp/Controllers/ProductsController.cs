﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProductsApp.Models;

namespace ProductsApp.Controllers
{
	/// <summary>
	/// Class ProductsController.
	/// </summary>
	public class ProductsController : ApiController
	{
		/// <summary>
		/// The products
		/// </summary>
		Product[] products = new Product[] 
		{ 
			new Product { Id = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1 }, 
			new Product { Id = 2, Name = "Yo-yo", Category = "Toys", Price = 3.75M }, 
			new Product { Id = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M } 
		};

		// api/products
		/// <summary>
		/// Gets all products.
		/// </summary>
		/// <returns>IEnumerable{Product}.</returns>
		public IEnumerable<Product> GetAllProducts()
		{
			return products;
		}

		public Product Get(int id)
		{
			return products.SingleOrDefault(x => x.Id == id);
		}

		// api/products/5
		/// <summary>
		/// Gets the product.
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <returns>IHttpActionResult.</returns>
		//public IHttpActionResult GetProduct(int id)
		//{
		//	var product = products.FirstOrDefault(x => x.Id == id);

		//	if (product == null)
		//	{
		//		return NotFound();
		//	}

		//	return Ok(product);
		//}

		// POST api/values
		/// <summary>
		/// Posts the specified value.
		/// </summary>
		/// <param name="value">The value.</param>
		public void Post([FromBody]string value)
		{

		}
	}
}
