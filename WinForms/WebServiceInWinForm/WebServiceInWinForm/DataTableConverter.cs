﻿using System;
using System.Data;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Globalization;

namespace DZone.Utils.Json
{
	/// <summary>
	/// Converts a <see cref="DataTable"/> to and from JSON.
	/// </summary>
	public class SADataTableConverter : Newtonsoft.Json.JsonConverter
	{
		/// <summary>
		/// Writes the JSON representation of the object.
		/// </summary>
		/// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
		/// <param name="value">The value.</param>
		/// <param name="serializer">The calling serializer.</param>
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			DataTable table = (DataTable)value;

			writer.WriteStartObject();

			writer.WritePropertyName("type");
			writer.WriteValue("DataTable");

			writer.WritePropertyName("tableName");
			writer.WriteValue(table.TableName);

			writer.WritePropertyName("columns");
			writer.WriteStartArray();
			foreach (DataColumn col in table.Columns)
			{
				writer.WriteStartArray();
				writer.WriteValue(col.ColumnName);
				writer.WriteValue(GetXmlDataTypeNameFromType(col.DataType));
				writer.WriteEndArray();
			}
			writer.WriteEndArray();

			writer.WritePropertyName("data");
			writer.WriteStartArray();
			foreach (DataRow row in table.Rows)
			{
				writer.WriteStartArray();
				foreach (DataColumn col in table.Columns)
				{
					writer.WriteValue(row[col.ColumnName]);
				}
				writer.WriteEndArray();
			}

			writer.WriteEndArray();

			writer.WriteEndObject();

			//writer.WriteStartArray();

			//foreach (DataRow row in table.Rows)
			//{
			//  writer.WriteStartObject();
			//  foreach (DataColumn column in row.Table.Columns)
			//  {
			//    writer.WritePropertyName(column.ColumnName);
			//    serializer.Serialize(writer, row[column]);
			//  }
			//  writer.WriteEndObject();
			//}

			//writer.WriteEndArray();
		}

		/// <summary>
		/// Reads the JSON representation of the object.
		/// </summary>
		/// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
		/// <param name="objectType">Type of the object.</param>
		/// <param name="existingValue">The existing value of object being read.</param>
		/// <param name="serializer">The calling serializer.</param>
		/// <returns>The object value.</returns>
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			DataTable dt = new DataTable();

			string strFmt = "SADataTableConverter.ReadJson : TokenType={0}, ValueType={1}, Value={2}";
			while (reader.Read())
			{
				Debug.WriteLine(String.Format(strFmt, reader.TokenType, reader.ValueType, reader.Value));
				if (reader.TokenType == JsonToken.EndObject)
					break;

				if (reader.TokenType == JsonToken.PropertyName)
				{
					if (String.Compare(reader.Value.ToString(), "tableName", true) == 0)
					{
						reader.Read();
						dt.TableName = reader.Value.ToString();
					}
					else if (String.Compare(reader.Value.ToString(), "columns", true) == 0)
					{
						reader.Read();
						String[][] columns = serializer.Deserialize<String[][]>(reader);
						foreach (String[] column in columns)
						{
							// 컬럼 형식(column[1])에 따라 형식을 지정하는 로직 필요함
							dt.Columns.Add(column[0], GetTypeFromXmlDataTypeName(column[1]));
						}
					}
					else if (String.Compare(reader.Value.ToString(), "data", true) == 0)
					{
						reader.Read();
						String[][] datas = serializer.Deserialize<String[][]>(reader);

						foreach (String[] dataRow in datas)
						{
							DataRow dr = dt.NewRow();
							// 컬럼 형식에 맞게 타입을 변경해야함.
							for (int c = 0; c < dataRow.Length; c++)
							{
								dr[c] = FromString(dt.Columns[c].DataType, dataRow[c]);
							}
							dt.Rows.Add(dr);
						}
					}
				}
			}

			return dt;
		}

		static object FromString(Type type, string text)
		{
			try
			{
				return Convert.ChangeType(text, type, CultureInfo.InvariantCulture);
			}
			catch
			{
				return null;
			}
		}

		private static Type GetColumnDataType(JsonToken tokenType)
		{
			switch (tokenType)
			{
				case JsonToken.Integer:
					return typeof(long);
				case JsonToken.Float:
					return typeof(double);
				case JsonToken.String:
				case JsonToken.Null:
				case JsonToken.Undefined:
					return typeof(string);
				case JsonToken.Boolean:
					return typeof(bool);
				case JsonToken.Date:
					return typeof(DateTime);
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		static Type GetTypeFromXmlDataTypeName(string typeName)
		{
			Type type = null;

			switch (typeName)
			{
				case "string":
					type = typeof(string);

					break;
				case "decimal":
					type = typeof(Decimal);

					break;
				case "int":
					type = typeof(int);

					break;
				case "boolean":
					type = typeof(Boolean);

					break;
				case "float":
					type = typeof(Single);

					break;
				case "double":
					type = typeof(double);

					break;
				case "byte":
					type = typeof(SByte);

					break;
				case "unsignedByte":
					type = typeof(Byte);

					break;
				case "short":
					type = typeof(Int16);

					break;
				case "long":
					type = typeof(long);

					break;
				case "unsignedShort":
					type = typeof(UInt16);

					break;
				case "unsignedInt":
					type = typeof(UInt32);

					break;
				case "unsignedLong":
					type = typeof(UInt64);

					break;
				case "base64Binary/":
					type = typeof(Byte[]);

					break;
				case "dateTime":
					type = typeof(DateTime);

					break;
				case "duration":
					type = typeof(TimeSpan);

					break;
			}

			return type;
		}

		static string GetXmlDataTypeNameFromType(Type type)
		{
			if (type == typeof(Char))
				return "_";         // has to have SimpleType in this column. 
			if (type == typeof(Byte[]) || type == typeof(SqlBytes))
				return "base64Binary/";       // has to have SimpleType in this column.
			if (type == typeof(DateTime) || type == typeof(SqlDateTime))
				return "dateTime";
			if (type == typeof(TimeSpan))
				return "duration";
			if (type == typeof(Decimal) || type == typeof(SqlDecimal) || type == typeof(SqlMoney))
				return "decimal";
			if (type == typeof(int))
				return "int";
			if (type == typeof(Boolean) || type == typeof(SqlBoolean))
				return "boolean";
			if (type == typeof(Single) || type == typeof(SqlSingle))
				return "float";
			if (type == typeof(double) || type == typeof(SqlDouble))
				return "double";
			if (type == typeof(SByte) || type == typeof(SqlByte))
				return "byte";
			if (type == typeof(Byte))
				return "unsignedByte";
			if (type == typeof(Int16) || type == typeof(SqlInt16))
				return "short";
			if (type == typeof(Int32) || type == typeof(SqlInt32))
				return "int";
			if (type == typeof(Int64) || type == typeof(SqlInt64))
				return "long";
			if (type == typeof(UInt16))
				return "unsignedShort";
			if (type == typeof(UInt32))
				return "unsignedInt";
			if (type == typeof(UInt64))
				return "unsignedLong";
			//if (type == typeof(System.Numerics.BigInteger))
			//    return Keywords.XSD_ANYTYPE; //"integer"; 
			if (type == typeof(Uri))
				return "anyURI";
			if (type == typeof(SqlBinary))
				return "hexBinary";
			if (type == typeof(string) || type == typeof(SqlGuid) || type == typeof(SqlString) || type == typeof(SqlChars))
				return "string";
			//if (type == typeof(object) || type == typeof(SqlXml) || type == typeof(DateTimeOffset))
			//    return Keywords.XSD_ANYTYPE;

			if (type.IsEnum == true)
			{
				return "int";
			}

			return String.Empty;
			// by default, if we dont map anything, we will map to String 
			// but I can not make Sql Types that will map to string be unmapped, because in schema , I will miss the second part and wont 
			// be able to differenciate between string snd SqlString and others that map to String
		}

		/// <summary>
		/// Determines whether this instance can convert the specified value type.
		/// </summary>
		/// <param name="valueType">Type of the value.</param>
		/// <returns>
		/// 	<c>true</c> if this instance can convert the specified value type; otherwise, <c>false</c>.
		/// </returns>
		public override bool CanConvert(Type valueType)
		{
			return (valueType == typeof(DataTable));
		}
	}
}
