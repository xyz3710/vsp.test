/**********************************************************************************************************************/
/*	Domain		:	Luxury.SmartAudio.ResultCodesExtension
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011??4??12???�요???�후 7:57
/*	Purpose		:	ResultCodes???�???�장 기능???�공?�니??
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011??4??12???�요???�후 7:57
/**********************************************************************************************************************/

using System;

namespace DZone.WebModule
{
	/// <summary>
	/// ResultCodes???�???�장 기능???�공?�니??
	/// </summary>
	public static class ResultCodesExtension
	{
		/// <summary>
		/// ResultCode 값의 int 값을 구합?�다.
		/// </summary>
		/// <param name="resultCodes"></param>
		/// <returns></returns>
		public static int ToInt32(this ResultCodes resultCodes)
		{
			return (int)resultCodes;
		}

		/// <summary>
		/// ResultCode 값의 string 값을 구합?�다.
		/// </summary>
		/// <param name="resultCodes"></param>
		/// <returns></returns>
		public static string ValueToString(this ResultCodes resultCodes)
		{
			return Convert.ToString((int)resultCodes);
		}

		/// <summary>
		/// int 값으�?ResultCodes�?반환?�니??
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static ResultCodes GetResultCodes(this int value)
		{
			ResultCodes resultCodes = ResultCodes.NotDefined;

			if (Enum.IsDefined(typeof(ResultCodes), value) == true)
				resultCodes = (ResultCodes)Enum.Parse(typeof(ResultCodes), Convert.ToString(value));

			return resultCodes;
		}

		/// <summary>
		/// string ?�의 int 값으�?ResultCodes�?반환?�니??
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static ResultCodes GetResultCodes(this string value)
		{
			return GetResultCodes(Convert.ToInt32(value));
		}
	}
}
