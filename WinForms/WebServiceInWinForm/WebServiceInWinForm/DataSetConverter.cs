﻿using System;
using System.Data;
using Newtonsoft.Json;

namespace DZone.Utils.Json
{
	/// <summary>
	/// Converts a <see cref="DataSet"/> to and from JSON.
	/// </summary>
	public class SADataSetConverter : Newtonsoft.Json.JsonConverter
	{
		/// <summary>
		/// Writes the JSON representation of the object.
		/// </summary>
		/// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
		/// <param name="value">The value.</param>
		/// <param name="serializer">The calling serializer.</param>
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			DataSet dataSet = (DataSet)value;

			SADataTableConverter converter = new SADataTableConverter();

			writer.WriteStartObject();
			writer.WritePropertyName("dataSetName");
			writer.WriteValue(dataSet.DataSetName);
			if (dataSet.Tables.Count > 0)
			{
				writer.WritePropertyName("tables");
                writer.WriteStartArray();
				foreach (DataTable table in dataSet.Tables)
				{
                    //writer.WriteStartArray();
					converter.WriteJson(writer, table, serializer);
                    //writer.WriteEndArray();
				}
                writer.WriteEndArray();
			}

			writer.WriteEndObject();
		}

		/// <summary>
		/// Reads the JSON representation of the object.
		/// </summary>
		/// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
		/// <param name="objectType">Type of the object.</param>
		/// <param name="existingValue">The existing value of object being read.</param>
		/// <param name="serializer">The calling serializer.</param>
		/// <returns>The object value.</returns>
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			DataSet ds = new DataSet();

			SADataTableConverter converter = new SADataTableConverter();

			reader.Read();
            if (reader.TokenType == JsonToken.PropertyName)
            {
                reader.Read();
                if (!string.IsNullOrWhiteSpace((string)reader.Value))
                    ds.DataSetName = (string)reader.Value;
            }

            while (reader.Read())
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    if (reader.ValueType == typeof(string) && (string)reader.Value == "tables")
                        break;
                }
                else if (reader.TokenType == JsonToken.EndObject)
                    break;
            }

            if (!(reader.ValueType == typeof(string) && (string)reader.Value == "tables"))
                return ds;

            if (reader.Read() && reader.TokenType == JsonToken.StartArray)
            {
                reader.Read();
                while (reader.TokenType == JsonToken.StartObject)
                {
                    DataTable dt = (DataTable)converter.ReadJson(reader, typeof(DataTable), null, serializer);
                    ds.Tables.Add(dt);

                    reader.Read();
                }

            }

            reader.Read(); // JsonToken.EndObject

			return ds;
		}

		/// <summary>
		/// Determines whether this instance can convert the specified value type.
		/// </summary>
		/// <param name="valueType">Type of the value.</param>
		/// <returns>
		/// 	<c>true</c> if this instance can convert the specified value type; otherwise, <c>false</c>.
		/// </returns>
		public override bool CanConvert(Type valueType)
		{
			return (valueType == typeof(DataSet));
		}
	}
}