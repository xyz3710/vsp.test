﻿/**********************************************************************************************************************/
/*	Domain		:	Luxury.SmartAudio.Enums.ResultCodes
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2011년 4월 12일 화요일 오후 6:04
/*	Purpose		:	반환 코드를 나열합니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2011년 4월 20일 수요일 오후 1:44
/**********************************************************************************************************************/

using System;

namespace DZone.WebModule
{
	/// <summary>
	/// 반환 코드를 구성합니다.
	/// </summary>
	public enum ResultCodes
	{
		/// <summary>
		/// 성공 반환 코드 : 0
		/// </summary>
		Success = 0,
		/// <summary>
		/// 실패 반환 코드 : -1
		/// </summary>
		Fail = -1,
		/// <summary>
		/// 유효한 검사 반환 코드 : 0
		/// </summary>
		Valid = 0,
		/// <summary>
		/// 유효하지않은 검사 반환 코드 : -1
		/// </summary>
		Invalid = -1,

		//#region Notice : -691 ~ -699
		///// <summary>
		///// 공지사항이 없습니다. : -691
		///// </summary>
		//NT_NoticeNotFound = -691,
		///// <summary>
		///// 공지사항 관련 알 수없는 예외입니다. : -699
		///// </summary>
		//NT_UnknownException = -699,
		//#endregion

		// 기관 사용기간 만료됨
		Company_Expired = -1000,
		Company_NotExists = -1001,

		#region WebService : -700 ~ -750
		/// <summary>
		/// WebService 관련 Target Page의 이름이 없을  경우입니다. : -700
		/// </summary>
		WS_TargetPageNotExists = -700,
		/// <summary>
		/// WebService 관련 Data field could not empty. 오류입니다. : -701
		/// </summary>
		WS_DataFieldCouldNotEmpty = -701,
		/// <summary>
		/// WebService 관련 Data ID field could not empty. 오류입니다. : -702
		/// </summary>
		WS_DataIdFieldCouldNotEmpty = -702,
		/// <summary>
		/// WebService 관련 PacketSize or TotalLength field value is not valid. 오류입니다. : -703
		/// </summary>
		WS_PacketSizeOrTotalLengthFieldValueIsNotValid = -703,
		/// <summary>
		/// WebService 관련 PacketID field value is not valid. 오류입니다. : -704
		/// </summary>
		WS_PacketIdFieldValueIsNotValid = -704,
		/// <summary>
		/// WebService 관련 Synchronization 오류 입니다. : -705
		/// </summary>
		WS_SynchronizationFail = -705,
		/// <summary>
		/// WebService 관련 I/F method에 version range(MinVersion, MaxVersion)가 없는 오류입니다. : -706
		/// </summary>
		WS_TargetMethodVersionRangeNotDefined = -706,
		/// <summary>
		/// WebService 관련 I/F method에 기술된 version 정보가 유효하지 않습니다. : -707
		/// </summary>
		WS_TargetMethodVersionInvalid = -707,
		#endregion

		#region BizBase : -751 ~ -780
		/// <summary>
		/// BizBase 의 알 수 없는 Database 예외(Error code : 2146232060) 입니다. : -751
		/// </summary>
		BZ_UnknownDb2146232060 = -751,
		/// <summary>
		/// BizBase 의 알 수 없는 Database 예외입니다. : -760
		/// </summary>
		BZ_UnknownDbException = -760,

		/// <summary>
		/// BizBase.DoCommand의 알 수 없는 예외입니다. : -761
		/// </summary>
		BZ_UnkownDoCommandException = -761,
		/// <summary>
		/// BizBase.DoRead의 알 수 없는 예외입니다. : -768
		/// </summary>
		BZ_UnkownDoReadException = -768,
		/// <summary>
		/// BizBase.DoWrite의 알 수 없는 예외입니다. : -779
		/// </summary>
		BZ_UnkownDoWriteException = -779,
		#endregion

		#region Package 관련 -781 ~ -810

		/// <summary>
		/// 구매 처리를 실패하였습니다. : -781
		/// </summary>
		PK_PurchaseProcessFail = -781,
		/// <summary>
		/// 판매 처리를 실패하였습니다. : -782
		/// </summary>
		PK_SaleProcessFail = -782,
		/// <summary>
		/// 다운로드 받은 패키지는 구매 취소할 수 없습니다.
		/// </summary>
		PK_CannotPurchaseCancelForDownload = -783,
		/// <summary>
		/// 구매 취소 처리를 실패하였습니다. : -784
		/// </summary>
		PK_PurchaseCancelProcessFail = -784,
		/// <summary>
		/// 판매 취소 처리를 실패 하였습니다.
		/// </summary>
		PK_SaleCancelProcessFail = -785,
		/// <summary>
		/// 이미 구매한 이력이 있습니다. : -786
		/// </summary>
		PK_AlreadyPurchase = -786,
		/// <summary>
		/// 다운로드하지 않은 패키지는 정산할 수 없습니다. : -787
		/// </summary>
		PK_DidnotDownloadThePackageCannotSettle = -787,
		/// <summary>
		/// 정산 처리를 실패 하였습니다. : -788
		/// </summary>
		PK_SettlementProcessFail = -788,

		/// <summary>
		/// Package 관련 알 수없는 예외입니다. : -810
		/// </summary>
		PK_UnknownException = -810,
		#endregion


		#region 사용자 관련 -901 ~ -930
		/// <summary>
		/// 사용자를 찾을 수 없습니다. : -901
		/// </summary>
		US_UserNotFound = -901,
		/// <summary>
		/// 계정이 잠겼습니다. : -902
		/// </summary>
		US_AccountWasLock = -902,
		/// <summary>
		/// 계정이 잠금에 실패하였습니다. : -903
		/// </summary>
		US_LockdownFail = -903,
		/// <summary>
		/// 인증 실패입니다. : -904
		/// </summary>
		US_AuthenticationFail = -904,
		/// <summary>
		/// 잠긴 사용자가 아닙니다. : -905
		/// </summary>
		US_NotLockdown = -905,
		/// <summary>
		/// 계정이 이미 잠겼습니다. : -906
		/// </summary>
		US_AccountAlreadyLocked = -906,
		/// <summary>
		/// 암호가 일치하지 않습니다. : -907
		/// </summary>
		US_PasswordNotCorrect = -907,
		/// <summary>
		/// 해당 Id가 이미 사용중입니다.. : -908
		/// </summary>
		US_UserIdAlreadyInUse = -908,
		/// <summary>
		/// 기기 아이디가 일치하지 않습니다.. : -909
		/// </summary>
		US_UUIDMismatch = -909,
		/// <summary>
		/// 계정을 조합하는중 오류가 발생했습니다. : -910
		/// </summary>
		US_CombineAccountFail = -910,
		/// <summary>
		/// 해당 Email이 이미 사용중입니다.. : -911
		/// </summary>
		US_EmailAlreadyInUse = -911,
		/// <summary>
		/// 해당 UserId, Email이 이미 사용중입니다.. : -912
		/// </summary>
		US_UserIdEmailAlreadyInUse = -912,
		/// <summary>
		/// 해당 UUID가 이미 사용중입니다.. : -913
		/// </summary>
		US_UUIDAlreadyInUse = -913,
		/// <summary>
		/// 사용자의 이메일이 활성화 되지 않았습니다.
		/// </summary>
		US_NotActivated = -914,


		/// <summary>
		/// 사용자 관련 알 수없는 예외입니다. : -930
		/// </summary>
		US_UnknownException = -930,
		#endregion

		#region Data 처리 관련 -931 ~ -960
		/// <summary>
		/// 데이터를 찾을 수 없습니다. : -921
		/// </summary>
		DT_DataNotFound = -921,
		/// <summary>
		/// 입력 파라미터의 값이 비었습니다. : -922
		/// </summary>
		DT_InputParameterEmpty = -922,
		/// <summary>
		/// 다른 사람이 이미 업데이트함 : -923
		/// </summary>
		DT_AleadyUpdatedByOther = -923,
		#endregion

		#region Not defined
		/// <summary>
		/// 정의되지 않음 : -999
		/// </summary>
		NotDefined = -999,
		#endregion
	}
}
