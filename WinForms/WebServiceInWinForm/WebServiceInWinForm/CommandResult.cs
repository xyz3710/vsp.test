﻿using System;
using DZone.WebModule;
using System.Data;
using Newtonsoft.Json;
using DZone.Utils.Json;
using System.Diagnostics;
namespace DZone.ServerIF
{
	[DebuggerDisplay("StateCode:{StateCode}, ResultNum:{ResultNum}, ReturnData:{ReturnData}, ResultData:{ResultData}, ExceptionMessage:{ExceptionMessage}, ResultCodes:{ResultCodes}", Name = "CommandResult")]
	public class CommandResult
	{
		/// <summary>
		/// Initializes a new instance of the CommandResult class.
		/// </summary>
		public CommandResult()
		{
		}

		/// <summary>
		/// 예외 발생
		/// </summary>
		/// <param name="stateCode"></param>
		/// <param name="exceptionMessage"></param>
		/// <param name="returnData"></param>
		public CommandResult(string stateCode, string exceptionMessage, string returnData)
		{
			StateCode = stateCode;
			ExceptionMessage = exceptionMessage;
			ResultNum = 0;
			ResultData = null;
			ReturnData = returnData;
		}

		/// <summary>
		/// Create, Update, Delete 성공 후 영향받은 행 지정
		/// </summary>
		/// <param name="stateCode"></param>
		/// <param name="resultNum"></param>
		/// <param name="returnData"></param>
		public CommandResult(string stateCode, int resultNum, string returnData)
		{
			StateCode = stateCode;
			ExceptionMessage = null;
			ResultNum = resultNum;
			ResultData = null;
			ReturnData = returnData;
		}

		/// <summary>
		/// Read 성공 후 영향받은 행과 추가(tiny) 반환값 지정
		/// </summary>
		/// <param name="stateCode"></param>
		/// <param name="resultData"></param>
		/// <param name="returnData"></param>
		public CommandResult(string stateCode, DataSet resultData, string returnData)
		{
			StateCode = stateCode;
			ExceptionMessage = null;
			ResultData = resultData;
			ReturnData = returnData;
		}

		/// <summary>
		/// StateCode를 구하거나 설정합니다.
		/// </summary>
		public string StateCode
		{
			get;
			private set;
		}

		/// <summary>
		/// ResultNum를 구하거나 설정합니다.
		/// </summary>
		public int ResultNum
		{
			get;
			private set;
		}

		/// <summary>
		/// Create, Update, Delete 성공 후 받은 반환 데이터
		/// </summary>
		public string ReturnData
		{
			get;
			private set;
		}

		/// <summary>
		/// ResultData를 구하거나 설정합니다.
		/// </summary>
		[JsonConverter(typeof(SADataSetConverter))]
		public DataSet ResultData
		{
			get;
			private set;
		}

		/// <summary>
		/// ExceptionMessage를 구하거나 설정합니다.
		/// </summary>
		public string ExceptionMessage
		{
			get;
			private set;
		}

		/// <summary>
		/// StateCode에 의해 ResultCodes enum 형식을 구합니다.
		/// </summary>
		public ResultCodes ResultCodes
		{
			get
			{
				return StateCode.GetResultCodes();
			}
		}
	}

}
