using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DZone.ContractDefination;

namespace WebServiceInWinForm
{
	public class IFReadResultConverter : MediaTypeFormatter
	{
		#region Constructor
		/// <summary>
		/// IFReadResultConverter class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public IFReadResultConverter()
		{
			SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
		}
		#endregion

		public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
		{
			base.SetDefaultContentHeaders(type, headers, mediaType);
		}

		public override Task<object> ReadFromStreamAsync(Type type, System.IO.Stream readStream, HttpContent content, IFormatterLogger formatterLogger, System.Threading.CancellationToken cancellationToken)
		{
			return base.ReadFromStreamAsync(type, readStream, content, formatterLogger, cancellationToken);
		}

		public override Task WriteToStreamAsync(Type type, object value, System.IO.Stream writeStream, HttpContent content, System.Net.TransportContext transportContext, System.Threading.CancellationToken cancellationToken)
		{
			return base.WriteToStreamAsync(type, value, writeStream, content, transportContext, cancellationToken);
		}

		//public static void ConfigureApis(HttpConfiguration config)
		//{
		//	config.Formatters.Add(new IFReadResultConverter());
		//}

		public override bool CanReadType(Type type)
		{
			if (type == typeof(IFReadResult))
			{
				return true;
			}
			else
			{
				Type enumerableType = typeof(IEnumerable<IFReadResult>);

				return enumerableType.IsAssignableFrom(type);
			}
		}

		public override bool CanWriteType(Type type)
		{
			if (type == typeof(IFReadResult))
			{
				return true;
			}
			else
			{
				Type enumerableType = typeof(IEnumerable<IFReadResult>);

				return enumerableType.IsAssignableFrom(type);
			}
		}

		public override Task WriteToStreamAsync(Type type, object value, System.IO.Stream writeStream, HttpContent content, System.Net.TransportContext transportContext)
		{
			return base.WriteToStreamAsync(type, value, writeStream, content, transportContext);
		}

		public override Task<object> ReadFromStreamAsync(Type type, System.IO.Stream readStream, HttpContent content, System.Net.Http.Formatting.IFormatterLogger formatterLogger)
		{
			return base.ReadFromStreamAsync(type, readStream, content, formatterLogger);
		}
	}
}

