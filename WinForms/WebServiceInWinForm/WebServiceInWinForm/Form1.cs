﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DZone.ServerIF;
using Newtonsoft.Json;
using ProductsApp.Models;

namespace WebServiceInWinForm
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);

			btnTest.PerformClick();
		}

		private void Run()
		{
			var uri = "http://teratorrent.net:88/CommonIF/ReadCommand?commandData={\"type\":\"DataTable\",\"tableName\":\"param\",\"columns\":[[\"CallCenterId\",\"int\"]],\"data\":[[10001]]}&operationInfo={\"ClientId\":\"IMEI354280050422450\",\"NetworkOperator\":\"\",\"NetworkOperatorCode\":\"\",\"RequestMode\":\"Command\",\"UserID\":\"NotYetLogin\",\"SyncInit\":false,\"SyncRequired\":false,\"TargetPage\":\"GetAll\",\"SessionID\":0,\"VersionCode\":1}";
			//var uri = "http://teratorrent.net:88/CommonIF/ReadCommand2?commandData={\"type\":\"DataTable\",\"tableName\":\"param\",\"columns\":[[\"CallId\",\"string\"]],\"data\":[[\"10010000000000010\"]]}&operationInfo={\"ClientId\":\"IMEI354280050422450\",\"NetworkOperator\":\"\",\"NetworkOperatorCode\":\"\",\"RequestMode\":\"Command\",\"UserID\":\"NotYetLogin\",\"SyncInit\":false,\"SyncRequired\":false,\"TargetPage\":\"GetCallById\",\"SessionID\":0,\"VersionCode\":1}";

			try
			{
				WebClient webClient = new WebClient();
				string response = string.Empty;
				string address = "http://teratorrent.net:88/CommonIF/ReadCommand";
				address = "http://teratorrent.net:88/CommonIF/ReadCommand";
				string data = "commandData={\"type\":\"DataTable\",\"tableName\":\"param\",\"columns\":[[\"CallCenterId\",\"int\"]],\"data\":[[100]]}&operationInfo={\"ClientId\":\"IMEI354280050422450\",\"NetworkOperator\":\"\",\"NetworkOperatorCode\":\"\",\"RequestMode\":\"Command\",\"UserID\":\"NotYetLogin\",\"SyncInit\":false,\"SyncRequired\":false,\"TargetPage\":\"GetAll\",\"SessionID\":0,\"VersionCode\":1}";

				address = "http://teratorrent.net:88/CommonIF/ReadCommand";
				data = "operationInfo={\"ClientId\":\"FromUrlBuilder\",\"SyncRequired\":false,\"UserID\":\"\",\"TargetPage\":\"GetAreasByCallCenterId\",\"RequestMode\":2,\"SyncInit\":false,\"SyncName\":\"\"}&commandData={\"type\":\"DataTable\",\"tableName\":\"param\",\"columns\":[[\"CallCenterId\",\"int\"]],\"data\":[[10001]]}";
				//response = GetData(webClient, address + "2", data);
				webClient.Encoding = Encoding.UTF8;
				response = PostAction(webClient, address, data);
				//response = GetData(webClient, address + "2", data);

				// Write test
				/*
				address = "http://teratorrent.net:88/CommonIF/WriteCommand";
				data = "commandData={\"type\":\"DataTable\",\"tableName\":\"param\",\"columns\":[[\"callCenterId\",\"int\"],[\"localId\",\"int\"]],\"data\":[[10001,2]]}&operationInfo={\"TargetPage\":\"CreateCall\",\"ClientId\":\"IMEI354280050422450\",\"NetworkOperator\":\"\",\"NetworkOperatorCode\":\"\",\"RequestMode\":\"Command\",\"UserID\":\"NotYetLogin\",\"SyncInit\":false,\"SyncRequired\":false,\"SessionID\":0,\"VersionCode\":1}";
				response = GetPostData(webClient, address, data);
				*/

				var result = JsonConvert.DeserializeObject<DZone.ContractDefination.IFReadResult>(response);
			}
			catch (WebException ex)
			{
				if (ex.Response is HttpWebResponse)
				{
					switch (((HttpWebResponse)ex.Response).StatusCode)
					{
						case HttpStatusCode.NotFound:
							//response = null;

							break;

						default:
							throw ex;
					}
				}
			}
		}

		/// <summary>
		/// Gets the data.
		/// </summary>
		/// <param name="webClient">The web client.</param>
		/// <param name="address">The address.</param>
		/// <param name="data">The data.</param>
		/// <returns>System.String.</returns>
		private string GetAction(WebClient webClient, string address, string data)
		{
			using (Stream stream = webClient.OpenRead(string.Format("{0}?{1}", address, data)))
			{
				using (StreamReader reader = new StreamReader(stream))
				{
					return reader.ReadToEnd();
				}
			}
		}

		/// <summary>
		/// Gets the post data.
		/// </summary>
		/// <param name="webClient">The web client.</param>
		/// <param name="address">host + controller + action</param>
		/// <param name="data">request data.</param>
		/// <returns>System.String.</returns>
		private string PostAction(WebClient webClient, string address, string data)
		{
			webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

			return webClient.UploadString(address, data);
		}

		private async Task RunAsync()
		{
			using (var client = new HttpClient())
			{
				//client.BaseAddress = new Uri("http://teratorrent.net:88");
				client.BaseAddress = new Uri("http://teratorrent.net:88");
				client.DefaultRequestHeaders.Accept.Clear();
				client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
				//client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("text/html"));

				//HttpResponseMessage response = await client.GetAsync("api/products/1");
				string requestUri = "CommonIF/ReadCommand2?commandData={\"type\":\"DataTable\",\"tableName\":\"param\",\"columns\":[[\"CallCenterId\",\"int\"]],\"data\":[[100]]}&operationInfo={\"ClientId\":\"IMEI354280050422450\",\"NetworkOperator\":\"\",\"NetworkOperatorCode\":\"\",\"RequestMode\":\"Command\",\"UserID\":\"NotYetLogin\",\"SyncInit\":false,\"SyncRequired\":false,\"TargetPage\":\"GetAll\",\"SessionID\":0,\"VersionCode\":1}";

				requestUri = "CommonIF/ReadCommand2?commandData={\"type\":\"DataTable\",\"tableName\":\"param\",\"columns\":[[\"CallId\",\"string\"]],\"data\":[[\"1001000000000001\"]]}&operationInfo={\"ClientId\":\"IMEI354280050422450\",\"NetworkOperator\":\"\",\"NetworkOperatorCode\":\"\",\"RequestMode\":\"Command\",\"UserID\":\"NotYetLogin\",\"SyncInit\":false,\"SyncRequired\":false,\"TargetPage\":\"GetCallById\",\"SessionID\":0,\"VersionCode\":1}";

				HttpResponseMessage response = await client.GetAsync(requestUri);
				var formatters = new List<MediaTypeFormatter> 
				{ 
					new CommandResultConverter(), 
					new JsonMediaTypeFormatter(),
					new XmlMediaTypeFormatter(),
				};

				// response.EnsureSuccessStatusCode()
				if (response.IsSuccessStatusCode)
				{
					try
					{
						//var data = await response.Content.ReadAsAsync<Product>();
						var data = response.Content.ReadAsAsync<DZone.ContractDefination.IFReadResult>();

						Text = data.ToString();
					}
					catch (Exception ex)
					{
						System.Diagnostics.Debug.WriteLine(ex, "Form1.RunAsync");
					}
				}
			}
		}

		private async Task RunAsyncPost()
		{
			using (var client = new HttpClient())
			{
				//client.BaseAddress = new Uri("http://teratorrent.net:88");
				client.BaseAddress = new Uri("http://teratorrent.net:88");
				client.DefaultRequestHeaders.Accept.Clear();
				client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
				//client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("text/html"));

				//HttpResponseMessage response = await client.GetAsync("api/products/1");
				string requestUri = "CommonIF/ReadCommand2?commandData={\"type\":\"DataTable\",\"tableName\":\"param\",\"columns\":[[\"CallCenterId\",\"int\"]],\"data\":[[100]]}&operationInfo={\"ClientId\":\"IMEI354280050422450\",\"NetworkOperator\":\"\",\"NetworkOperatorCode\":\"\",\"RequestMode\":\"Command\",\"UserID\":\"NotYetLogin\",\"SyncInit\":false,\"SyncRequired\":false,\"TargetPage\":\"GetAll\",\"SessionID\":0,\"VersionCode\":1}";

				requestUri = "CommonIF/ReadCommand2?commandData={\"type\":\"DataTable\",\"tableName\":\"param\",\"columns\":[[\"CallId\",\"string\"]],\"data\":[[\"1001000000000001\"]]}&operationInfo={\"ClientId\":\"IMEI354280050422450\",\"NetworkOperator\":\"\",\"NetworkOperatorCode\":\"\",\"RequestMode\":\"Command\",\"UserID\":\"NotYetLogin\",\"SyncInit\":false,\"SyncRequired\":false,\"TargetPage\":\"GetCallById\",\"SessionID\":0,\"VersionCode\":1}";

				var msg = new HttpResponseMessage
				{
				};
				HttpResponseMessage response = await client.PostAsJsonAsync(requestUri, msg);

				// response.EnsureSuccessStatusCode()
				if (response.IsSuccessStatusCode)
				{
					try
					{
						//var data = await response.Content.ReadAsAsync<Product>();
						var data = response.Content.ReadAsAsync<DZone.ContractDefination.IFReadResult>();

						Text = data.ToString();
					}
					catch (Exception ex)
					{
						System.Diagnostics.Debug.WriteLine(ex, "Form1.RunAsync");
					}
				}
			}
		}

		private void btnTest_Click(object sender, EventArgs e)
		{
			Text = string.Empty;
			Update();
			Run();
		}
	}
}