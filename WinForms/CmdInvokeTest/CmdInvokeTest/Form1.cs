﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CmdInvokeTest
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnInvoke_Click(object sender, EventArgs e)
		{
			string output = Invoke(@".\Test.cmd", createNoWindow: false, useStandardOutput: true);

			WriteLog(output);
		}

		private void WriteLog(string message)
		{
			tbLog.AppendText(message);
		}

		/// <summary>
		/// Invokes the process.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="arguments">The arguments.</param>
		/// <param name="workingPath">The working path.</param>
		/// <param name="createNoWindow">창을 생성할지 여부</param>
		/// <param name="useShellExecute">OS shell을 사용할지 여부</param>
		/// <param name="errorDialog">프로세스를 시작하지 못할 때 에러 상자를 표시할지 여부</param>
		/// <param name="verb">The verb.</param>
		/// <param name="waitForExit">프로세스가 종료될 때 까지 대기할지 여부</param>
		/// <param name="useStandardOutput">화면 출력에 대한 결과를 구할지 여부(true면 useShellExecute=false, waitForExit=true, psi 값는 내부값으로 설정되고, 반환값으로 화면의 내용을 반환)</param>
		/// <param name="windowStyle">The window style.</param>
		/// <param name="psi">앞의 옵션 대신 직접 <see cref="System.Diagnostics.ProcessStartInfo" />를 사용할 경우 지정한다.</param>
		/// <returns>if success return string.Empty, but error message.</returns>
		/// <exception cref="System.IO.FileNotFoundException"></exception>
		/// <example>
		/// default psi is
		/// psi = new ProcessStartInfo()
		/// {
		/// FileName = filePath,
		/// CreateNoWindow = false,
		/// UseShellExecute = true,
		/// WorkingDirectory = Path.GetDirectoryName(filePath),
		/// ErrorDialog = true,
		/// Verb = "runas",
		/// };
		/// </example>
		public static string Invoke(
			string filePath,
			string arguments = null,
			string workingPath = null,
			bool createNoWindow = true,
			bool useShellExecute = true,
			bool errorDialog = true,
			string verb = "runas",
			bool waitForExit = false,
			bool useStandardOutput = false,
			ProcessWindowStyle windowStyle = ProcessWindowStyle.Normal,
			ProcessStartInfo psi = null)
		{
			if (File.Exists(filePath) == false)
			{
				throw new FileNotFoundException(filePath);
			}

			var result = string.Empty;

			if (psi == null || useStandardOutput == true)
			{
				psi = new ProcessStartInfo()
				{
					FileName = filePath,
					Arguments = arguments,
					CreateNoWindow = createNoWindow,
					UseShellExecute = useShellExecute,
					WorkingDirectory = workingPath ?? Path.GetDirectoryName(filePath),
					ErrorDialog = errorDialog,
					Verb = verb,
					WindowStyle = windowStyle,
				};

				if (useStandardOutput == true)
				{
					psi.RedirectStandardOutput = true;
					psi.RedirectStandardError = true;
					psi.UseShellExecute = false;
				}
			}

			try
			{
				var proc = Process.Start(psi);

				if (useStandardOutput == true)
				{
					proc.WaitForExit();

					if (proc.ExitCode == 0)
					{
						result = proc.StandardOutput.ReadToEnd();
					}
					else
					{
						result = proc.StandardError.ReadToEnd();
					}
				}
				else if (waitForExit == true)
				{
					proc.WaitForExit();
				}
			}
			catch (Exception ex)
			{
				result = ex.GetBaseException().Message;
			}

			return result;
		}
	}
}
