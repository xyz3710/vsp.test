﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextBoxHeightable
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();

			textBox1.TextChanged += OnTextChanged;
			textBox2.AutoSize = true;
			textBox3.TextChanged += OnTextChanged;
			textBox4.TextChanged += OnTextChanged;
		}

		private void OnTextChanged(object sender, EventArgs e)
		{
			var textBox = sender as TextBox;
			var size = new Size(textBox.ClientSize.Width, int.MaxValue);
			var flags = TextFormatFlags.WordBreak;
			var padding = 3;
			var borders = textBox.Height - textBox.ClientSize.Height;

			size = TextRenderer.MeasureText(textBox.Text, textBox.Font, size, flags);

			int height = size.Height + borders + padding;

			if (textBox.Top + height > this.ClientSize.Height - 10)
			{
				height = this.ClientSize.Height - 10 - textBox.Top;
			}

			textBox.Height = height;

			if (size.Width > textBox.Width)		// word wrap 되면 위치를 이동한다.
			{
				var parent = textBox.Parent;
				var ps = parent.Size;

				textBox.Top = (ps.Height - height) / 2;
			}

			// center에 drawing
			/*
			var top = (textBox.Height - height) / 2;
			var g = textBox.CreateGraphics();

			g.DrawString(textBox.Text, textBox.Font, new System.Drawing.SolidBrush(textBox.ForeColor), new Point(padding, top));
			*/
		}

		private void textBox4_TextChanged(object sender, EventArgs e)
		{
			var textBox = sender as TextBox;
			Size sz = new Size(textBox.ClientSize.Width, int.MaxValue);
			TextFormatFlags flags = TextFormatFlags.WordBreak;
			int padding = 3;
			int borders = textBox.Height - textBox.ClientSize.Height;
			sz = TextRenderer.MeasureText(textBox.Text, textBox.Font, sz, flags);
			int h = sz.Height + borders + padding;

			if (textBox.Top + h > this.ClientSize.Height - 10)
			{
				h = this.ClientSize.Height - 10 - textBox.Top;
			}

			textBox.Height = h;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			this.textBox1.AutoSize = true;
			this.textBox1.Text = "Hello world!";
			this.textBox1.Font = new System.Drawing.Font("Arial", 10, FontStyle.Regular);

			this.textBox2.AutoSize = false;
			this.textBox2.Text = "Hello world!";
			this.textBox2.Font = new System.Drawing.Font("Arial", 10, FontStyle.Regular);
		}
	}
}
