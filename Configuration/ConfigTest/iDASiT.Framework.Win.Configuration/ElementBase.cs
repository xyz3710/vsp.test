﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Framework.Win.Configuration.ElementBase
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 10월 9일 화요일 오전 8:53
/*	Purpose		:	Config 파일의 ElelmentName, Key, Type, InnerText를 구성하는 기본 클래스입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Diagnostics;

namespace iDASiT.Framework.Win.Configuration
{
	/// <summary>
	/// Config 파일의 ElelmentName, Key, Type, InnerText를 구성하는 기본 클래스입니다.
	/// </summary>
	[Serializable]
	[DebuggerDisplay("{Key}, {ElementName}, {Type}, {InnerText}")]
    public class ElementBase
	{
		#region Fields
		private string _elementName;
		private string _key;
		private string _type;
		private string _innerText;
		#endregion

		#region Constructors
		/// <summary>
		/// ConfigClassBase class의 새로운 인스턴스를 생성합니다.
		/// </summary>
		/// <param name="elementName">ElementName</param>
		/// <param name="innerText">InnerText</param>
		public ElementBase(string elementName, string innerText)
		{
			_elementName = elementName;
			_innerText = innerText;
		}
		#endregion

		#region Properties
		/// <summary>
		/// ElementName를 구하거나 설정합니다.
		/// </summary>
		public string ElementName
		{
			get
			{
				return _elementName;
			}
			set
			{
				_elementName = value;
			}
		}

		/// <summary>
		/// Key를 구하거나 설정합니다.
		/// </summary>
		public string Key
		{
			get
			{
				return _key;
			}
			set
			{
				_key = value;
			}
		}

		/// <summary>
		/// Type를 구하거나 설정합니다.
		/// </summary>
		public string Type
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}
                
		/// <summary>
		/// InnerText를 구하거나 설정합니다.
		/// </summary>
		public string InnerText
		{
			get
			{
				return _innerText;
			}
			set
			{
				_innerText = value;
			}
		}
		#endregion

		#region ToString
		public override string ToString()
		{
			return string.Format("Key:{0}, ElementName:{1}, Type:{2}, InnerText:{3}", Key, ElementName, Type, InnerText);
		}
		#endregion
	}
}
