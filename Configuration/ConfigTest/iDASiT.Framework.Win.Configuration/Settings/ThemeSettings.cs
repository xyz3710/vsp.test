﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Framework.Win.Configuration.ThemeSettings
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 10월 9일 화요일 오후 2:20
/*	Purpose		:	Theme setting 파일을 읽어서 리소스 화 시켜주는 class
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	iDASiT.Framework.Win.Reousrces에 Themes.xml이 있어야 합니다.
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Drawing;

namespace iDASiT.Framework.Win.Configuration
{
	/// <summary>
	/// Theme setting 파일을 읽어서 리소스 화 시켜주는 class
	/// </summary>
	public class ThemeSettings : ConfigSettingBase
	{
		#region Constants
		private readonly string GRAND_NODE_STRING = "Themes";
		private readonly string SECTION = "Control";
		#endregion

		#region Constructor
		/// <summary>
		/// CaptionSettings 클래스 인스턴스를 생성합니다.
		/// </summary>
		public ThemeSettings()
		{
			Assembly assembly = null;
			string resNamespace = "iDASiT.Framework.Win.Resources";

			try
			{
				assembly = Assembly.LoadFrom(string.Format(@"{0}\{1}\{2}\{3}.dll",
						Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), 
						"iDASiT", 
						"Framework.Win",
						resNamespace));

				using (Stream resStream = assembly.GetManifestResourceStream(string.Format("{0}.{1}", resNamespace, "Themes.xml")))
				{
					if (resStream == null)
						throw new Exception();

					Stream = resStream;

					resStream.Close();
				}

				GrandNodeSectionName = GRAND_NODE_STRING;
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format("리소스 파일({0}.dll)을 로드하지 못했습니다.\r\n같은 경로에 복사해 주십시오", resNamespace), ex);
			}			
		}		
		#endregion

		#region GetColorValue
		/// <summary>
		/// 해당 키로 설정값을 구해옵니다.
		/// </summary>
		/// <param name="key">설정값의 key</param>
		/// <returns>설정값</returns>
		public Color this[string key]
		{
			get
			{
				return GetValue<Color>(SECTION, key);
			}
		}

		public Font GetFont(string key)
		{
			return GetValue<Font>(SECTION, key);
		}
		#endregion

	}
}
