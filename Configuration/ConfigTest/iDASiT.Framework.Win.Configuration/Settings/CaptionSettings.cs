 /**********************************************************************************************************************/
/*	Domain		:	iDASiT.Framework.Win.Configuration.CaptionSettings
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 10월 9일 화요일 오후 2:20
/*	Purpose		:	DbAttributes caption setting 파일을 읽어서 리소스 화 시켜주는 class
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	iDASiT.Framework.Win.Reousrces에 Captions.xml이 있어야 합니다.
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using System.Reflection;

namespace iDASiT.Framework.Win.Configuration
{
	/// <summary>
	/// DbAttributes caption setting 파일을 읽어서 리소스 화 시켜주는 class
	/// </summary>
	public class CaptionSettings : ConfigSettingBase
	{
		#region Constants
		private readonly string GRAND_NODE_STRING = "Columns";
		private readonly string SECTION = "Column";
		#endregion

		#region Constructor
		/// <summary>
		/// CaptionSettings 클래스 인스턴스를 생성합니다.
		/// </summary>
		public CaptionSettings()
		{
			Assembly assembly = null;
			string resNamespace = "iDASiT.Framework.Win.Resources";

			try
			{
				assembly = Assembly.LoadFrom(string.Format(@"{0}\{1}\{2}\{3}.dll",
						Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), 
						"iDASiT", 
						"Framework.Win",
						resNamespace));

				using (Stream resStream = assembly.GetManifestResourceStream(string.Format("{0}.{1}", resNamespace, "Captions.xml")))
				{
					if (resStream == null)
						throw new Exception();

					Stream = resStream;

					resStream.Close();
				}

				GrandNodeSectionName = GRAND_NODE_STRING;
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format("리소스 파일({0}.dll)을 로드하지 못했습니다.\r\n같은 경로에 복사해 주십시오", resNamespace), ex);
			}			
		}		
		#endregion

		#region GetKeyValue
		/// <summary>
		/// 해당 키로 설정값을 구해옵니다.
		/// </summary>
		/// <param name="key">설정값의 key</param>
		/// <returns>설정값</returns>
		public string this[string key]
		{
			get
			{
				return GetKeyValue(SECTION, key, string.Empty);				
			}
		}
		#endregion
	}
}
