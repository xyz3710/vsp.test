﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Framework.Win.Configuration.ConfigSettingBase
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 10월 9일 화요일 오전 8:47
/*	Purpose		:	ElelmentName, Key, Type, InnerText 형태로 각 Node를 구성하기 위해 사용되는 클래스
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	Key, Value 형태로 config 파일을 구성 할 수 있다.
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Drawing;
using System.ComponentModel;

namespace iDASiT.Framework.Win.Configuration
{
	/// <summary>
	/// Elelment, Attributes, InnerText 형태로 각 Node를 구성하기 위해 사용되는 클래스
	/// </summary>
	public class ConfigSettingBase : IDisposable
	{
		#region Constants
		private const string CONFIG_SECTION_KEY = "configuration";
		#endregion

		#region Fields
		/// <summary>
		/// Key로 사용되는 속성 이름 입니다.
		/// </summary>
		protected string KeyAttributeName = "key";
		/// <summary>
		/// Type으로 사용하는 속성 이름 입니다.
		/// </summary>
		protected string TypeAttributeName = "type";

		private XmlDocument _xmlConfigDoc;
		private string _filename;
		private Stream _stream;
		private string _grandNodeSectionName;
		#endregion

		#region Constructor
		/// <summary>
		/// ConfigSettingBase class의 인스턴스를 생성합니다.
		/// <remarks>GrandNodeSectionName을 먼저 선택해서 작업하고자 하는 노드를 구합니다.<br/>
		/// 조작하고자하는 파일 또는 stream을 반드시 설정해야 합니다.</remarks>
		/// </summary>
		public ConfigSettingBase()
		{
			_xmlConfigDoc = new XmlDocument();
		}

		/// <summary>
		/// ConfigSettingBase class의 인스턴스를 생성합니다.
		/// <remarks>GrandNodeSectionName을 먼저 선택해서 작업하고자 하는 노드를 구합니다.</remarks>
		/// <param name="stream">설정 파일을 관리할 파일 stream</param>
		/// </summary>
		public ConfigSettingBase(Stream stream)
			: this()
		{
			_stream = stream;

			LoadSteam();
		}

		/// <summary>
		/// ConfigSettingBase class의 인스턴스를 생성합니다.
		/// <remarks>GrandNodeSectionName을 먼저 선택해서 작업하고자 하는 노드를 구합니다.</remarks>
		/// </summary>
		/// <param name="filename">설정 파일을 관리할 파일명</param>
		public ConfigSettingBase(string filename)
			: this()
		{
			_filename = filename;

			LoadFile();
		}
		#endregion

		#region Properties
		/// <summary>
		/// GrandNodeSectionName을 구하거나 설정합니다.
		/// </summary>
		protected string GrandNodeSectionName
		{
			get
			{
				return _grandNodeSectionName;
			}
			set
			{
				_grandNodeSectionName = value;
			}
		}

		/// <summary>
		/// 파일 이름을 설정합니다.
		/// </summary>
		protected string Filename
		{
			set
			{
				_filename = value;
				LoadFile();
			}
		}

		/// <summary>
		/// 파일 stream을 설정합니다.
		/// </summary>
		protected Stream Stream
		{
			set
			{
				_stream = value;
				LoadSteam();
			}
		}
		#endregion

		#region Private methods
		private void LoadSteam()
		{
			try
			{
				_xmlConfigDoc.Load(_stream);
			}
			catch (Exception ex)
			{
				throw new InvalidOperationException("stream이 없거나 잘못 지정되었습니다", ex);
			}
		}

		private void LoadFile()
		{
			try
			{
				_xmlConfigDoc.Load(_filename);
			}
			catch
			{
				ResetFile();
			}
		}

		private ElementBase GetNewElement(string sectionName, string key, Type typeofValue, string value)
		{
			ElementBase elementBase = new ElementBase(sectionName, value);

			elementBase.Key = key;
			elementBase.Type = typeofValue.FullName;

			return elementBase;
		}

		private void SetAttributes(ref XmlElement xmlElement, ElementBase elementBase)
		{
			xmlElement.SetAttribute(KeyAttributeName, elementBase.Key);
			xmlElement.SetAttribute(TypeAttributeName, elementBase.Type);
			xmlElement.InnerText = elementBase.InnerText;
		}
		#endregion

		#region Protected methods
		/// <summary>
		/// 잘못된 파일일 경우 제거 후 다시 작성한다.
		/// </summary>
		protected void ResetFile()
		{
			// 잘못된 파일일 경우 제거 후 다시 작성한다.
			string headerString = string.Format("<?xml version=\"{0}\" encoding=\"{1}\" ?>{2}", "1.0", "utf-8", Environment.NewLine);
			_xmlConfigDoc.LoadXml(string.Format("{0}<{1}></{1}>", headerString, CONFIG_SECTION_KEY));
			_xmlConfigDoc.Save(_filename);
		}

		#region GrandNode manipulation
		/// <summary>
		/// GrandNode를 설정합니다.
		/// <remarks><see cref="GrandNodeSectionName"/>을 먼저 설정해야 합니다.</remarks>
		/// </summary>
		protected void SetGrandNode()
		{
			// 기존 Menus section이 있으면 기존 값을 사용한다.
			if (_xmlConfigDoc.DocumentElement.ChildNodes != null && _xmlConfigDoc.DocumentElement.ChildNodes.Count == 0)
			{
				if (string.IsNullOrEmpty(GrandNodeSectionName) == true)
					throw new InvalidOperationException("GrandNodeSectionName을 먼저 설정해야 합니다.");

				XmlElement menusElement = _xmlConfigDoc.CreateElement(GrandNodeSectionName);

				_xmlConfigDoc.DocumentElement.AppendChild(menusElement);
			}
		}

		/// <summary>
		/// GrandNode를 구합니다.
		/// <remarks><see cref="GrandNodeSectionName"/>을 먼저 설정해야 합니다.</remarks>
		/// </summary>
		/// <returns></returns>
		protected XmlNode GetGrandNode()
		{
			if (string.IsNullOrEmpty(GrandNodeSectionName) == true)
				throw new InvalidOperationException("GrandNodeSectionName을 먼저 설정해야 합니다.");

			XmlNode grandNode = _xmlConfigDoc.DocumentElement.SelectSingleNode(string.Format("/{0}/{1}", CONFIG_SECTION_KEY, GrandNodeSectionName));

			return grandNode;
		}

		/// <summary>
		/// GrandNode list를 구해옵니다.
		/// </summary>
		/// <returns></returns>
		protected XmlNodeList GetGrandNodes()
		{
			return _xmlConfigDoc.DocumentElement.ChildNodes;
		}
		#endregion

		/// <summary>
		/// Section에서 key로 node를 선택합니다.
		/// </summary>
		/// <param name="sectionName"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		protected XmlNode SelectSingleNode(string sectionName, string key)
		{
			List<XmlNode> nodes = GetAllNodes(sectionName);
			XmlNode selectedNode = null;

			foreach (XmlNode xmlNode in nodes)
			{
				if (xmlNode.Attributes[KeyAttributeName].Value == key)
				{
					selectedNode = xmlNode;

					break;
				}
			}

			return selectedNode;
		}

		/// <summary>
		/// 선택한 section name이 있는 node만을 구합니다.
		/// <remarks>string.Empty로 할 경우 전체 node를 구한다.</remarks>
		/// </summary>
		/// <param name="sectionName">구하고자 하는 section name</param>
		/// <returns></returns>
		protected List<XmlNode> GetAllNodes(string sectionName)
		{
			List<XmlNode> nodeList = new List<XmlNode>();
			Queue<XmlNodeList> queue = new Queue<XmlNodeList>();
			XmlNode grandNode = GetGrandNode();

			if (grandNode != null)
			{
				queue.Enqueue(grandNode.ChildNodes);

				while (queue.Count > 0)
				{
					XmlNodeList xmlNodeList = queue.Dequeue();

					if (xmlNodeList == null || xmlNodeList.Count == 0)
						continue;

					foreach (XmlNode xmlNode in xmlNodeList)
					{
						if (sectionName == string.Empty)
							nodeList.Add(xmlNode);
						else if (xmlNode.Name == sectionName)
							nodeList.Add(xmlNode);

						queue.Enqueue(xmlNode.ChildNodes);
					}
				}
			}

			return nodeList;
		}

		/// <summary>
		/// 설정된 내용으로 저장합니다.
		/// </summary>
		protected void Save()
		{
			if (string.IsNullOrEmpty(_filename) == false)
				_xmlConfigDoc.Save(_filename);
			else if (_stream != null)
				_xmlConfigDoc.Save(_stream);
		}

		/// <summary>
		/// sectionName와 Key로 특정 Attribute 값을 구합니다.
		/// </summary>
		/// <param name="sectionName">section name</param>
		/// <param name="key">key attribute</param>
		/// <param name="attributeName">attribute name</param>
		/// <returns></returns>
		protected string GetAttributeValue(string sectionName, string key, string attributeName)
		{
			string result = String.Empty;
			XmlNode xmlNode = SelectSingleNode(sectionName, key);

			if (xmlNode != null)
				result = GetAttributeValue(xmlNode, attributeName);

			return result;
		}

		/// <summary>
		/// sectionName와 Key로 특정 Attribute 값을 구합니다.
		/// </summary>
		/// <param name="xmlNode">구하고자 하는 XmlNode</param>
		/// <param name="attributeName">attribute name</param>
		/// <returns></returns>
		protected string GetAttributeValue(XmlNode xmlNode, string attributeName)
		{
			string result = String.Empty;

			if (xmlNode != null)
				result = xmlNode.Attributes[TypeAttributeName].Value;

			return result;
		}

		/// <summary>
		/// 해당 section의 key로 InnerText의 값을 구합니다.
		/// </summary>
		/// <param name="sectionName">section name</param>
		/// <param name="key">key attribute</param>
		/// <param name="defaultValue">기본값</param>
		/// <returns></returns>
		protected string GetKeyValue(string sectionName, string key, string defaultValue)
		{
			string result = defaultValue;
			XmlNode xmlNode = SelectSingleNode(sectionName, key);

			if (xmlNode != null)
				result = xmlNode.InnerText;

			return result;
		}

		/// <summary>
		/// 해당 section의 key로 T type의 값을 구합니다.
		/// </summary>
		/// <typeparam name="T">구하고자 하는 Type</typeparam>
		/// <param name="sectionName">section name</param>
		/// <param name="key">key attribute</param>
		/// <returns></returns>
		protected T GetValue<T>(string sectionName, string key)
		{
			T t = default(T);

			ElementBase element = GetValueType(sectionName, key, typeof(string).FullName);
			//Type keyType = Type.GetType(element.Type);
			Type returnType = typeof(T);

			// NOTICE: 현재 .Net 2.0의 버그로 System.Drawing.Color, System.Drawing.Font등의 Type은 생성되지 않아서 비교를 먼저 할 수 없다.
			// 때문에 일단 구하고 그 FullName을 비교하여 성공여부를 결정한다.
			// by KIMKIWON\xyz37 in 2007년 10월 17일 수요일 오후 2:43
			//if (keyType == returnType)

			TypeConverter converter = TypeDescriptor.GetConverter(returnType);

			if (converter.CanConvertTo(typeof(string)) == true)
				t = (T)converter.ConvertFrom(element.InnerText);

			if (returnType.FullName == element.Type)
				return t;

			return default(T);
		}

		/// <summary>
		/// 해당 section의 key로 InnerText와 Type을 Return 합니다.
		/// </summary>
		/// <param name="sectionName">section name</param>
		/// <param name="key">key attribute</param>
		/// <param name="defaultValue">기본값</param>
		/// <returns>InnerText와 Type을 Return 합니다.</returns>
		protected ElementBase GetValueType(string sectionName, string key, string defaultValue)
		{
			ElementBase element = new ElementBase(key, defaultValue);
			string value = defaultValue;
			XmlNode xmlNode = SelectSingleNode(sectionName, key);

			element.Key = sectionName;
			element.Type = typeof(string).FullName;

			if (xmlNode != null)
			{
				element.InnerText = xmlNode.InnerText;
				element.Type = GetAttributeValue(xmlNode, KeyAttributeName);
			}

			return element;
		}

		/// <summary>
		/// 해당 section의 key로 InnerText의 값을 설정합니다.
		/// <remarks>해당 section name과 동일한 section 아래에 자동으로 추가합니다.(없을 경우는 실패)</remarks>
		/// </summary>
		/// <param name="sectionName">section name</param>
		/// <param name="key">key attribute</param>
		/// <param name="value">설정할 값</param>
		/// <returns></returns>
		protected bool SetKeyValue(string sectionName, string key, string value)
		{
			return SetKeyValue(sectionName, key, typeof(string), value);
		}

		/// <summary>
		/// 해당 section의 key로 InnerText의 값을 설정합니다.
		/// <remarks>해당 section name과 동일한 section 아래에 자동으로 추가합니다.(없을 경우는 실패)</remarks>
		/// </summary>
		/// <param name="sectionName">section name</param>
		/// <param name="key">key attribute</param>
		/// <param name="typeofValue">Value의 type</param>
		/// <param name="value">설정할 값</param>
		/// <returns></returns>
		protected bool SetKeyValue(string sectionName, string key, Type typeofValue, string value)
		{
			bool result = false;
			XmlNode selectNode = SelectSingleNode(sectionName, key);

			// 현재 section의 node가 없으면 추가
			if (selectNode == null)
			{
				List<XmlNode> allNodes = GetAllNodes(sectionName);
				if (allNodes.Count == 0)
					return false;

				XmlElement newElement = _xmlConfigDoc.CreateElement(sectionName);
				ElementBase elementBase = GetNewElement(sectionName, key, typeofValue, value);

				SetAttributes(ref newElement, elementBase);

				if (allNodes[0].ParentNode.AppendChild(newElement) != null)
					result = true;
			}
			else
			{
				ElementBase elementBase = GetNewElement(sectionName, key, typeofValue, value);
				XmlElement xmlElement = selectNode as XmlElement;

				SetAttributes(ref xmlElement, elementBase);

				result =  true;
			}

			if (result == true)
			{
				try
				{
					Save();
				}
				catch (Exception ex)
				{
					throw new Exception("저장할 수 없습니다.", ex);
				}
			}

			return result;
		}

		/// <summary>
		/// 해당 section의 모든 node 값을 구해옵니다.
		/// </summary>
		/// <param name="sectionName">section name</param>
		/// <returns></returns>
		protected List<ElementBase> GetElements(string sectionName)
		{
			List<ElementBase> elements = new List<ElementBase>();
			List<XmlNode> allNodes = GetAllNodes(sectionName);

			foreach (XmlNode xmlNode in allNodes)
			{
				ElementBase element = new ElementBase(xmlNode.Name, xmlNode.InnerText);

				foreach (XmlAttribute attr in xmlNode.Attributes)
				{
					if (attr.Name == KeyAttributeName)
						element.Key = attr.Value;

					if (attr.Name == TypeAttributeName)
						element.Type = attr.Value;
				}

				elements.Add(element);
			}

			return elements;
		}
		#endregion

		#region IDisposable 멤버

		/// <summary>
		/// ConfigSettingBase에서 사용하는 모든 리소스를 해제합니다.
		/// </summary>
		public void Dispose()
		{
			_stream.Dispose();
			_xmlConfigDoc = null;
		}

		#endregion
	}
}
