﻿using System;
using System.Collections.Generic;
using System.Text;
using iDASiT.Framework.Win.Configuration;
using System.Xml;
using System.Reflection;
using System.IO;
using System.Drawing;

namespace ConfigSettingBaseTestDriver
{
	class Program
	{
		static void Main(string[] args)
		{
			#region ConfigSettingBase test
			/*
			string filename = @"D:\VSP\ConfigTest\test.xml";
			ConfigSettingBase config = new ConfigSettingBase(filename);

			config.GrandNodeSectionName = "Columns";
			Console.WriteLine(config.GetKeyValue("Column", "Holding", "홀딩(기본값)"));
			config.SetKeyValue("Column", "LotQty", typeof(string), "Lot 수량");
			config.SetKeyValue("Column", "LotQtyAmount", typeof(int), "Lot 총수량");
			
			List<ElementBase> es = config.GetElements("Column");

			foreach (ElementBase elementBase in es)
				Console.WriteLine("{0}, {1}, {2}, {3}", elementBase.ElementName, elementBase.Key, elementBase.Type, elementBase.InnerText);
			*/
			#endregion

			new TestClass().Run();
		}
	}

	public class TestClass
	{
		public void Run()
		{
			//Assembly assembly = Assembly.LoadFrom("ConfigResources.dll");
			#region For Test
			/*
			Assembly assembly = Assembly.GetExecutingAssembly();
			Stream stream = assembly.GetManifestResourceStream(string.Format("{0}.Resources.{1}", GetType().Namespace, "DbAttributes.xml"));
			CaptionSettings captionSettings = new CaptionSettings(stream);
			*/
			#endregion

			ThemeSettings themeSettings = new ThemeSettings();

			Color color = themeSettings["Theme2LabelColor"];
			Font font = themeSettings.GetFont("Font");

			Console.WriteLine(color);
			Console.WriteLine(font);
		}
	}
}
