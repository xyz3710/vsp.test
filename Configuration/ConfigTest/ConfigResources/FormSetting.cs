﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace ConfigResources
{
	public class FormSetting
	{
		private string _menuConfigFilePath;
		private string _favoritesMenuConfigFilePath;
		private Color _backColor;

		#region Properties
		/// <summary>
		/// MenuConfigFilePath를 구하거나 설정합니다.
		/// </summary>
		public string MenuConfigFilePath
		{
			get
			{
				return _menuConfigFilePath;
			}
			set
			{
				_menuConfigFilePath = value;
			}
		}

		/// <summary>
		/// FavoritesMenuConfigFilePath를 구하거나 설정합니다.
		/// </summary>
		public string FavoritesMenuConfigFilePath
		{
			get
			{
				return _favoritesMenuConfigFilePath;
			}
			set
			{
				_favoritesMenuConfigFilePath = value;
			}
		}

		/// <summary>
		/// BackColor를 구하거나 설정합니다.
		/// </summary>
		public Color BackColor
		{
			get
			{
				return _backColor;
			}
			set
			{
				_backColor = value;
			}
		}

		#endregion
        
        
	}
}
