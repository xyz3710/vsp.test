﻿/**********************************************************************************************************************/
/*	Domain		:	ConfigTest.RefAssembly
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2007년 10월 7일 일요일 오후 1:32
/*	Purpose		:	참조용 어셈블리 테스트
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	일단 버전을 1.0.0.0 으로 처음 빌드 후 RefAssemblyTest에서 참조한다
 *					.Net Configuration Tool(mscorcfg.msc)에서 응용프로그램 구성에서 
 *					구성 어셈블리를 추가 후 바인팅 정책에서 1.1.0.0으로 빌드 한 RefAssembly를 참조하여
 *					변경된 config 파일을 체크한다.
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace RefAssemblyTest
{
	public class RefAssembly
	{
		public static string GetAssemblyFullName()
		{
			Assembly assembly = Assembly.GetExecutingAssembly();

			return assembly.FullName;
		}
	}
}
