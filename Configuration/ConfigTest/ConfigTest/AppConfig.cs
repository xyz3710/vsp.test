﻿using System;
using System.Configuration;
using System.Xml;

namespace ConfigTest
{
	public class AppConfig
	{
		private Configuration _configuration;

		/// <summary>
		/// Initializes a new instance of the AppConfig class.
		/// </summary>
		public AppConfig()
		{
			//string asmPath = string.Format("{0}.config", Application.ExecutablePath);
			
			_configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

			Console.WriteLine("Config filepath : {0}", _configuration.FilePath);

			foreach (ConfigurationSectionGroup csg in _configuration.SectionGroups)
				Console.WriteLine("{0}", csg.Name);
		}

		public string ReadSetting(string sectionGroupName, string sectionName, string settingName)
		{
			// Get sectionGroup
			ConfigurationSectionGroup configurationSectionGroup = _configuration.GetSectionGroup(sectionGroupName);

			// Get section
			ClientSettingsSection section = configurationSectionGroup.Sections.Get(sectionName) as ClientSettingsSection;

			// Get setting
			SettingElement setting = section.Settings.Get(settingName);

			// Read setting value
			if (setting == null)
				return string.Empty;

			return setting.Value.ValueXml.InnerText;
		}

		public void WriteSetting(string sectionGroupName, string sectionName, string settingName, string newSettingValue)
		{
			// Get sectionGroup
			ConfigurationSectionGroup configurationSectionGroup = _configuration.GetSectionGroup(sectionGroupName);

			if (configurationSectionGroup == null)
			{
				ConfigurationSectionGroup addConfigSectionGroup = new ConfigurationSectionGroup();

				_configuration.SectionGroups.Add(sectionGroupName, addConfigSectionGroup);
				configurationSectionGroup = _configuration.GetSectionGroup(sectionGroupName);
			}

			// Get section
			ClientSettingsSection section = configurationSectionGroup.Sections.Get(sectionName) as ClientSettingsSection;

			if (section == null)
			{
				ConfigurationSection addConfigSection = new ClientSettingsSection();

				configurationSectionGroup.Sections.Add(sectionName, addConfigSection);
				section = configurationSectionGroup.Sections.Get(sectionName) as ClientSettingsSection;
			}

			// Get setting
			SettingElement setting = section.Settings.Get(settingName);

//			// Write a setting value
//			if (setting == null)
//			{
//				SettingElement settingElement = new SettingElement(settingName, SettingsSerializeAs.String);
//				
//				section.Settings.Add(settingElement);
//				setting = section.Settings.Get(settingName);
//				
//				//value.ValueXml = 
//				settingElement.Value.ValueXml.InnerText = newSettingValue;
//			}
//			else
//				setting.Value.ValueXml.InnerText = newSettingValue;

			Flush();
		}

		public void Flush()
		{
			_configuration.Save(ConfigurationSaveMode.Full, true);
		}
	}
}
