﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace ConfigTest
{
	public class Test : ApplicationSettingsBase
	{
		[ApplicationScopedSetting()]
		[DefaultSettingValue("UnName")]
		public string Name
		{
			get
			{
				return (string)this["Name"];
			}
			set
			{
				this["Name"] = value;
			}
		}
	}
}
