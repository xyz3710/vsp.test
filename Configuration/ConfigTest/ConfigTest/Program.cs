﻿using System;
using System.Configuration;
using System.Collections.Specialized;
using System.Reflection;
using System.IO;
using System.Xml;

namespace ConfigTest
{
	class Program
	{
		static void Main(string[] args)
		{
			AppConfig appConfig = new AppConfig();

			string highScore = appConfig.ReadSetting("applicationSettings", "ConfigTest.Settings", "HighScore");
			string testString = appConfig.ReadSetting("applicationSettings", "ConfigTest.Settings", "TestString");

			Console.WriteLine("HighScore : {0}", highScore);
			Console.WriteLine("TestSTring: {0}", testString);

			
			appConfig.WriteSetting("applicationSettings", "ConfigTest.Settings", "HighScore", "200");
			appConfig.WriteSetting("applicationSettings", "ConfigTest.Settings", "TestString", "샘플 스트링입니다.");
			appConfig.Flush();

			highScore = appConfig.ReadSetting("applicationSettings", "ConfigTest.Settings", "HighScore");
			testString = appConfig.ReadSetting("applicationSettings", "ConfigTest.Settings", "TestString");

			Console.WriteLine("HighScore : {0}", highScore);
			Console.WriteLine("TestSTring: {0}", testString);

			NameValueCollection appSettings = ConfigurationManager.AppSettings;

			foreach (string key in appSettings)
				Console.WriteLine("{0} : {1}", key, appSettings[key]);

			Assembly assembly = Assembly.LoadFrom("ConfigResources.dll");
			Stream stream = assembly.GetManifestResourceStream("ConfigResources.Resources.FormSetting.config");
			XmlDocument settingDoc = new XmlDocument();

			settingDoc.Load(stream);

			
		}		
	}
}
