﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.InteropServices;

namespace MyConfigSectionHandler
{
    public class MyHandler : ConfigurationSection
	{
		/// <summary>
		/// MyHandler class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public MyHandler()
		{
		}

		/// <summary>
		/// MyHandler class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public MyHandler(string attribVal)
		{
			MyAttrib1 = attribVal;
		}

		[ConfigurationProperty("myAttrib1", DefaultValue="Clowns", IsRequired = true)]
		[StringValidator(InvalidCharacters="~!@#$%^&*()[]{}/;'\"|\\", MinLength = 1, MaxLength = 60)]		
		public string MyAttrib1
		{
			get
			{
				return this["myAttrib1"] as string;
			}
			set
			{
				this["myAttrib1"] = value;
			}
		}

		[ConfigurationProperty("myChildSection")]
		public MyChildConfigElement MyChildSection
		{
			get
			{
				return this["myChildSection"] as MyChildConfigElement;
			}
			set
			{
				this["myChildSection"] = value;
			}
		}
	}
}
