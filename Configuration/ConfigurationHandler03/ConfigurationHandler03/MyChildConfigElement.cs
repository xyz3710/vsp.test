using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.InteropServices;

namespace MyConfigSectionHandler
{
	public class MyChildConfigElement : ConfigurationElement
	{
		/// <summary>
		/// MyChildConfigElement class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public MyChildConfigElement()
		{
		}

		/// <summary>
		/// MyChildConfigElement class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public MyChildConfigElement(string a1, string a2)
		{
			MyChildAttribute1 = a1;
			MyChildAttribute2 = a2;
		}

		[ConfigurationProperty("myChildAttrib1", DefaultValue="Zippy", IsRequired=true)]
		[StringValidator(InvalidCharacters="!@#$%^&*()[]{}/;'\"|\\", MinLength=1, MaxLength=60)]
		public string MyChildAttribute1
		{
			get
			{
				return this["myChildAttrib1"] as string;
			}
			set
			{
				this["myChildAttrib1"] = value;
			}
		}

		[ConfigurationProperty("myChildAttrib2", DefaultValue="Michael Zawondy", IsRequired=true)]
		[StringValidator(InvalidCharacters="!@#$%^&*()[]{}/;'\"|\\", MinLength=1, MaxLength=60)]
		public string MyChildAttribute2
		{
			get
			{
				return this["myChildAttrib2"] as string;
			}
			set
			{
				this["myChildAttrib2"] = value;
			}
		}
	}
}
