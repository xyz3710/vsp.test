﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace MyConfigSectionHandler
{
	class Program
	{
		static void Main(string[] args)
		{
			MyHandler myHandler = (MyHandler)ConfigurationManager.GetSection("myCustomGroup/myCustomSection");

			Console.WriteLine(myHandler.MyAttrib1);
			Console.WriteLine(string.Format("attrib1 : {0}\r\nattrib2 : {1}", 
                                  myHandler.MyChildSection.MyChildAttribute1, 
                                  myHandler.MyChildSection.MyChildAttribute2));
		}
	}
}
