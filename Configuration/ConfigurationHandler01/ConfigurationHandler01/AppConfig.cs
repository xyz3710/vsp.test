﻿using System.Collections.Specialized;
using System;

namespace ConfiguationHandler01
{
	public class AppConfig
	{
		private static string mSiteName;
		private static string mConnectString;
		static AppConfig()
		{
			NameValueCollection nvc = new NameValueCollection();
			try
			{
				// GetConfig 메소드를 호출하면 web.config에 등록된 클래스 내의
				// Create method가 호출되어 NameValueCollection 객체를 되돌린다. 
				nvc = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("AppConfig");

				if ((nvc != null))
				{
					mConnectString = nvc["ConnectString"];
					mSiteName = nvc["SiteName"];
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

		}
		public static string SiteName
		{
			get
			{
				return mSiteName;
			}
		}

		public static string ConnectString
		{
			get
			{
				return mConnectString;
			}
		}
	}
}