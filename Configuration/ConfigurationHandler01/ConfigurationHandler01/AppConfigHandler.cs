﻿using System.Collections.Specialized;
using System.Configuration;
using System.Xml;
using System;

namespace ConfiguationHandler01
{
	public class AppConfigHandler : IConfigurationSectionHandler
	{
		#region IConfigurationSectionHandler 멤버

		public object Create(object parent, object configContext, XmlNode input)
		{
			// 1. xpath를 통한 처리
			// 2. xml elements enumeration
			// 3. NameValueSectionHandler의 이용
			NameValueCollection nvc = null;
			NameValueSectionHandler handler = null;

			try
			{
				handler = new NameValueSectionHandler();
				nvc = (NameValueCollection)handler.Create(parent, configContext, input);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			return nvc;
		}

		#endregion
	}
}

