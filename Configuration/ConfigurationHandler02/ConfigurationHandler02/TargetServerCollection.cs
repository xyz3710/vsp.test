using System;
using System.Configuration;

namespace ConfigurationHandler02
{
	public class TargetServerCollection : ConfigurationElementCollection
	{

		protected override ConfigurationElement CreateNewElement()
		{
			return new TargetServer();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((TargetServer)element).ServerName;
		}

		public TargetServer this[int index]
		{
			get
			{
				return (TargetServer)BaseGet(index);
			}
			set
			{
				if (BaseGet(index) != null)
					BaseRemoveAt(index);

				BaseAdd(index, value);
			}
		}

		public new TargetServer this[string name]
		{
			get
			{
				return (TargetServer)BaseGet(name);
			}
		}

		public int IndexOf(TargetServer targetServer)
		{
			return BaseIndexOf(targetServer);
		}

		public void Add(TargetServer targetServer)
		{
			BaseAdd(targetServer);
		}

		protected override void BaseAdd(ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}

		public void Remove(TargetServer targetServer)
		{
			if (BaseIndexOf(targetServer) >= 0)
				BaseRemove(targetServer.ServerName);
		}

		public void RemoveAt(int index)
		{
			BaseRemoveAt(index);
		}

		public void Remove(string name)
		{
			BaseRemove(name);
		}

		public void Clear()
		{
			BaseClear();
		}
	}
}
