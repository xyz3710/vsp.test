using System;
using System.Configuration;

namespace ConfigurationHandler02
{
	public class Source : ConfigurationElement
	{
		[ConfigurationProperty("sourceDirectory")]
		public string SourceDirectory
		{
			get
			{
				return this["sourceDirectory"] as string;
			}
			set
			{
				this["sourceDirectory"] = value;
			}
		}

		[ConfigurationProperty("archiveDirectory")]
		public string ArchiveDirectory
		{
			get
			{
				return this["archiveDirectory"] as string;
			}
			set
			{
				this["archiveDirectory"] = value;
			}
		}
	}
}
