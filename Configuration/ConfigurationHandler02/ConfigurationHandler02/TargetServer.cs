using System;
using System.Configuration;

namespace ConfigurationHandler02
{
	public class TargetServer : ConfigurationElement
	{
		[ConfigurationProperty("serverName")]
		public string ServerName
		{
			get
			{
				return this["serverName"] as string;
			}
			set
			{
				this["serverName"] = value;
			}
		}

		[ConfigurationProperty("serverIp")]
		public string ServerIp
		{
			get
			{
				return this["serverIp"] as string;
			}
			set
			{
				this["serverIp"] = value;
			}
		}

		[ConfigurationProperty("targetDirectory")]
		public string TargetDirectory
		{
			get
			{
				return this["targetDirectory"] as string;
			}
			set
			{
				this["targetDirectory"] = value;
			}
		}
	}
}
