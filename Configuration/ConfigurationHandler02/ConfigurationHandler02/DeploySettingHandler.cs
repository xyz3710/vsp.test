﻿using System;
using System.Configuration;
using System.Diagnostics;

namespace ConfigurationHandler02
{
    public class DeploySettingHandler : ConfigurationSection
	{
		private static Configuration _currentConfig;
	
		static DeploySettingHandler()
		{
			_currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
		}

		public static DeploySettingHandler Current
		{
			get
			{
				DeploySettingHandler result = null;
				
				try
				{
				//for (int i = 0; i < _currentConfig.Sections.Count; i++)
				//{
				//	Debug.Print(i.ToString());
				//	Debug.Print(_currentConfig.Sections[i].ToString());
				//}

					result = ConfigurationManager.GetSection("deploySettingGroup/deploySetting") as DeploySettingHandler;
				}
				catch (Exception ex)
				{
					Debug.Print(ex.Message);
				}
				
				return result;
			}
		}

		public void Save()
		{
			_currentConfig.Save();
		}

		public void RefreshSection()
		{
			ConfigurationManager.RefreshSection("");
		}

		[ConfigurationProperty("sourceDirectory")]
		public Source Source
		{
			get
			{
				return this["source"] as Source;
			}
		}

		[ConfigurationProperty("targetServers")]
		public TargetServerCollection TargetServers
		{
			get
			{
				return this["targetServers"] as TargetServerCollection;
			}
		}
	}
}
