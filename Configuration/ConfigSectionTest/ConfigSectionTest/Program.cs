﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Windows.Forms;

namespace ConfigSectionTest
{
	class Program
	{
		static void Main(string[] args)
		{

			//Application.Run(new Form1());

			//return;

			Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			UrlSectionGroup group = config.SectionGroups["myCustomGroup"] as UrlSectionGroup;

			if (group == null)
			{
				// 없을 경우 새로운 Group을 생성한다.
				group = new UrlSectionGroup();

				config.SectionGroups.Add("myCustomGroup", group);
			}

			UrlSection section1 = group.MyUrls;

			if (section1 == null)
			{
				// 없을 경우 새로운 Section을 생성한다.
				section1 = new UrlSection();
				group.Sections.Add("MyUrls", section1);
				section1.UrlAttribute = "empas";
				section1.Default = new UrlElement("daum", "http://daum.net", 90);
			}
            
			// SectionGroup을 사용할 경우 직접 XPath로 접근 할 수 있으며 아래와 같이 사용해야 한다.
			//UrlSection section = config.SectionGroups["myCustomGroup"].Sections["MyUrls"] as UrlSection;
			//UrlSection section = ConfigurationManager.GetSection("myCustomGroup/MyUrls") as UrlSection;
			#region SectionGroup 미 사용시 
			//UrlSection section = config.GetSection("MyUrls") as UrlSection;
			/*			
			<?xml version="1.0" encoding="utf-8" ?>
			<configuration>
				<configSections>
					<section name="MyUrls" type="ConfigSectionTest.UrlSection, ConfigSectionTest, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"
										allowDefinition="Everywhere" allowExeDefinition="MachineToApplication" restartOnExternalChanges="true"/>
				 </configSections>
					<MyUrls urlAttribute="empas">		<!-- Section & Attribute -->
						<default name="daum" url="http://daum.net" port="80" />		<!-- Element -->
						<urls>
							<add name="naver" url="http://naver.com" port="81" />
							<add name="empas" url="http://empas.com" port="83" />
						</urls>
					</MyUrls>
			</configuration>
			*/
			#endregion

			UrlElementCollection elements = section1.Servers;

			if (elements == null || elements.Count == 0)
			{
				elements = new UrlElementCollection();
				section1.Servers = elements;

				elements.Default = "naver";
				elements.Add(new UrlElement("daum", "http://daum.net", 90));
				elements.Add(new UrlElement("naver", "http://naver.com", 93));
				elements.Add(new UrlElement("empas", "http://empas.com", 94));
			}

			PrintBar();
			Console.WriteLine(string.Format("Section Attribute : {0}", section1.UrlAttribute));
			Console.Write("Section selected Element : ");
			PrintConfig(elements[section1.UrlAttribute]);
			PrintBar();
			Console.WriteLine();

			PrintBar();
			Console.Write("Default url : ");
			PrintConfig(section1.Default);
			PrintBar();
						
			Console.WriteLine();
			foreach (UrlElement urlElement in elements)
				PrintConfig(urlElement);

			PrintBar();
			Console.WriteLine(string.Format("ElementCollection의 Default : {0}", elements.Default));
			PrintConfig(elements[elements.Default]);
			PrintBar();

			elements.Add(new UrlElement("test1", "http://test1.com", 88));
			elements.Add(new UrlElement("test2", "http://test1.com", 89));
		
			elements["yahoo"] = new UrlElement("yahoo", "http://yahoo.co.kr", 89);

			// MyUrls2에 대한 처리
			Console.WriteLine();
			Url2Section section2 = group.MyUrls2;

			if (section2 == null)
			{
				section2 = new Url2Section();
				group.Sections.Add("MyUrls2", section2);
			}

			Url2DefaultElementCollection urls2 = section2.Urls2;

			if (urls2 == null || urls2.Count == 0)
				urls2.Add(new Url2DefaultElement("yahoo"));

			UrlElementCollection urls21 = urls2[0].Urls;
			if (urls21 == null || urls21.Count == 0)
			{
				urls21.Add(new UrlElement("yahoo", "http://yahoo.com", 103));
				urls21.Add(new UrlElement("empas", "http://empas.com", 104));	
			}
			
			PrintBar();
			Console.WriteLine(string.Format("Default url2 : {0}", section2.Urls2[0].Default));
			PrintConfig(section2.Urls2[0].Urls[section2.Urls2[0].Default]);
			PrintBar();

			Console.WriteLine();
			foreach (UrlElement urlElement in urls2[0].Urls)
				PrintConfig(urlElement);

			//config.SaveAs("testConfig.config", ConfigurationSaveMode.Full, false);
			config.Save();

			Console.WriteLine("config를 수정한 후 아무키나 누르세요");
			Console.ReadLine();

			#region Refresh Test
			// Refresh 테스트는 잘 안된다.
			ConfigurationManager.RefreshSection("MyUrls");

			section1 = config.SectionGroups["myCustomGroup"].Sections["MyUrls"] as UrlSection;
			elements = section1.Servers;

			Console.WriteLine();
			PrintBar();
			foreach (UrlElement urlElement in elements)
				PrintConfig(urlElement);
			#endregion
		}

		private static void PrintBar()
		{
			Console.Write(string.Empty.PadRight(80, '='));
		}

		private static void PrintConfig(UrlElement url)
		{
			if (url == null)
				Console.WriteLine("Null 정보가 전달되었습니다.");
			else
				Console.WriteLine(string.Format("{0}\t{1}\t{2}", url.Name, url.Port, url.Url));
		}
            
	}
}
