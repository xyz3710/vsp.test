﻿using System;
using System.Collections;
using System.Configuration;

namespace ConfigSectionTest
{
	class UsingConfigurationSectionGroup
	{
		static int indentLevel = 0;

		static void Main(string[] args)
		{

			// Get the application configuration file.
			System.Configuration.Configuration config =
                    ConfigurationManager.OpenExeConfiguration(
					ConfigurationUserLevel.None);

			// Get the collection of the section groups.
			ConfigurationSectionGroupCollection
				sectionGroups = config.SectionGroups;
			ShowSectionGroupCollectionInfo(sectionGroups);
		}

		static String getSpacer()
		{
			String spacer = "";
			for (int i = 0; i < indentLevel; i++)
			{
				spacer = spacer + "    ";
			}
			return spacer;
		}

		static void ForceDeclaration(
			ConfigurationSectionGroup sectionGroup)
		{

			// Get the application configuration file.
			System.Configuration.Configuration config =
                    ConfigurationManager.OpenExeConfiguration(
					ConfigurationUserLevel.None);

			sectionGroup.ForceDeclaration();

			config.Save(ConfigurationSaveMode.Full);

			Console.WriteLine(
				"Forced declaration for the group: {0}",
				sectionGroup.Name);
		}

		static void ForceDeclaration(
			ConfigurationSectionGroup sectionGroup,
			bool force)
		{
			sectionGroup.ForceDeclaration(force);

			Console.WriteLine(
				"Forced declaration for the group: {0} is {1}",
				sectionGroup.Name, force.ToString());
		}

		static void ShowSectionGroupCollectionInfo(
			ConfigurationSectionGroupCollection sectionGroups)
		{
			foreach (String groupName in sectionGroups.Keys)
			{
				ConfigurationSectionGroup sectionGroup = 
                    (ConfigurationSectionGroup)sectionGroups[groupName];
				ShowSectionGroupInfo(sectionGroup);
				if (sectionGroup.Name == "system.web")
				{
					ForceDeclaration(sectionGroup, true);
				}

			}
		}

		static void ShowSectionGroupInfo(
			ConfigurationSectionGroup sectionGroup)
		{
			// Get the group name including the 
			// parent group names.
			Console.WriteLine(getSpacer() + 
                "Section Group Name: " + 
                sectionGroup.Name);

			// Get the group name without including
			// the parent group names.
			Console.WriteLine(getSpacer() +
                "Section Group Name: " + 
                sectionGroup.SectionGroupName);

			indentLevel++;

			Console.WriteLine(getSpacer() + 
                "Type: " + sectionGroup.Type);

			Console.WriteLine(getSpacer() +
                "Is Group Required?: " +
                sectionGroup.IsDeclarationRequired);

			Console.WriteLine(getSpacer() + 
                "Is Group Declared?: " + 
                sectionGroup.IsDeclared);

			Console.WriteLine(getSpacer() + 
                "Contained Sections:");

			indentLevel++;

			ConfigurationSectionCollection sectionCollection = 
                sectionGroup.Sections;
			foreach (String sectionName in sectionCollection.Keys)
			{
				ConfigurationSection section = 
                    (ConfigurationSection)sectionCollection[sectionName];
				Console.WriteLine(getSpacer() + "Section Name:" 
                    + section.SectionInformation.Name);
			}

			indentLevel--;

			Console.WriteLine(getSpacer() + 
                "Contained Section Groups:");

			indentLevel++;

			ConfigurationSectionGroupCollection sectionGroups = 
                sectionGroup.SectionGroups;
			ShowSectionGroupCollectionInfo(sectionGroups);

			indentLevel--;
		}
	}
}