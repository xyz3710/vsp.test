﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace ConfigSectionTest
{
	public class UrlElement : ConfigurationElement
	{
		public UrlElement(string newName, string newUrl, int newPort)
		{
			Name = newName;
			Url = newUrl;
			Port = newPort;
		}

		public UrlElement(string elementName)
		{
			Name = elementName;
		}

		public UrlElement()
		{

		}

		/// <summary>
        /// Name를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("name", DefaultValue="daum", IsRequired=true, IsKey=true)]        
        [Description("Name를 구하거나 설정합니다.")]
        public string Name
        {
        	get
        	{
        		return (string)this["name"];
        	}
        	set
        	{
        		this["name"] = value;
        	}
        }
        
        /// <summary>
        /// Url를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("url", DefaultValue="http://daum.net", IsRequired=true)]
        [Description("Url를 구하거나 설정합니다.")]
        public string Url
        {
        	get
        	{
        		return (string)this["url"];
        	}
        	set
        	{
        		this["url"] = value;
        	}
        }
        
        /// <summary>
        /// Port를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("port", DefaultValue=80, IsRequired=false)]
        [Description("Port를 구하거나 설정합니다.")]
        public int Port
        {
        	get
        	{
        		return (int)this["port"];
        	}
        	set
        	{
        		this["port"] = value;
        	}
        }
	}
}
