﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace ConfigSectionTest
{
	public class Url2DefaultElement : ConfigurationElement
	{
		public Url2DefaultElement(string defaultUrl)
		{
			Default = defaultUrl;
		}

		public Url2DefaultElement()
		{

		}

		/// <summary>
        /// Default를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("default", DefaultValue="yahoo", IsRequired=true, IsKey=true)]
        [Description("Default를 구하거나 설정합니다.")]
        public string Default
        {
        	get
        	{
        		return (string)this["default"];
        	}
        	set
        	{
        		this["default"] = value;
        	}
        }

		/// <summary>
        /// Urls를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("", IsRequired=false, IsKey=false, IsDefaultCollection=true)]
        [Description("Urls를 구하거나 설정합니다.")]
        public UrlElementCollection Urls
        {
        	get
        	{
				// app.config에서 <!--<urls>-->를 해제하면 urls 키를 넣어야 한다.
				return (UrlElementCollection)this[""];
        	}
        	set
        	{
        		this[""] = value;
        	}
        }
	}
}
