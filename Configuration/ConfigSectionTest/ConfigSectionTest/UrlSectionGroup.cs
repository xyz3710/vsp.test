﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace ConfigSectionTest
{
	/// <summary>
	/// 구성 파일에 있는 관련된 섹션의 그룹을 나타냅니다.
	/// </summary>
	public class UrlSectionGroup : ConfigurationSectionGroup
	{
		/// <summary>
		/// UrlSectionGroup class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public UrlSectionGroup()
		{
		}

		/// <summary>
		/// MyCustomGroup를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("MyUrls", DefaultValue="MyUrls", IsRequired=false, IsKey=false)]
		[Description("MyUrls를 구하거나 설정합니다.")]
		public UrlSection MyUrls
		{
			get
			{
				return (UrlSection)base.Sections["MyUrls"];
			}
			set
			{
				base.Sections.Add("MyUrls", value);
			}
		}

		/// <summary>
		/// MyCustomGroup를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("MyUrls2", DefaultValue="MyUrls2", IsRequired=false, IsKey=false)]
		[Description("MyUrls2를 구하거나 설정합니다.")]
		public Url2Section MyUrls2
		{
			get
			{
				return (Url2Section)base.Sections["MyUrls2"];
			}
			set
			{
				base.Sections.Add("MyUrls2", value);
			}
		}
	}
}
