﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace ConfigSectionTest
{
	public class UrlSection : ConfigurationSection
	{
		/// <summary>
		/// UrlSection class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public UrlSection()
		{
		}

		/// <summary>
        /// UrlAttribute를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("urlAttribute", DefaultValue="daum", IsRequired=false)]
        [Description("UrlAttribute를 구하거나 설정합니다.")]
        public string UrlAttribute
        {
        	get
        	{
        		return (string)this["urlAttribute"];
        	}
        	set
        	{
        		this["urlAttribute"] = value;
        	}
        }
        
        /// <summary>
        /// Default를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("default")]
        [Description("Default를 구하거나 설정합니다.")]
        public UrlElement Default
        {
        	get
        	{
        		return (UrlElement)this["default"];
        	}
			set
			{
				this["default"] = value;
			}
        }
        
        /// <summary>
        /// Servers를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("servers", IsRequired=false)]
        [Description("Servers를 구하거나 설정합니다.")]
        public UrlElementCollection Servers
        {
        	get
        	{
        		return (UrlElementCollection)this["servers"];
        	}
        	set
        	{
        		this["servers"] = value;
        	}
        }
	}
}
