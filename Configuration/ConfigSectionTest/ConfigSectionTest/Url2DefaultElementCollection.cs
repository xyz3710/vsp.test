﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace ConfigSectionTest
{
	/// <summary>
	/// 자식 요소의 컬렉션을 포함하는 <seealso cref="ConfigSectionTest.Url2DefaultElementCollection"/> 구성 요소를 나타냅니다.
	/// </summary>
	[ConfigurationCollection(typeof(Url2DefaultElement), AddItemName="Server")]
	public class Url2DefaultElementCollection : ConfigurationElementCollection
	{
		#region Constructors
		/// <summary>
		/// 자식 요소의 컬렉션을 포함하는 <seealso cref="ConfigSectionTest.Url2DefaultElementCollection"/>의 새 인스턴스를 초기화합니다.
		/// </summary>
		public Url2DefaultElementCollection()
		{
		}
		#endregion

		#region CollectionType
		/// <summary>
		/// <seealso cref="ConfigSectionTest.Url2DefaultElementCollectionType"/>의 종류를 가져옵니다.
		/// </summary>
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}
		#endregion

		#region Protected methods
		/// <summary>
		/// 파생 클래스에서 재정의 될 때 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>를 만듭니다.
		/// </summary>
		/// <returns></returns>
		protected override ConfigurationElement CreateNewElement()
		{
			return new Url2DefaultElement();
		}

		/// <summary>
		/// 파생 클래스에서 재정의 될 때 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>를 만듭니다.
		/// </summary>
		/// <param name="elementName">지정된 element 이름으로 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>를 만듭니다.</param>
		/// <returns></returns>
		protected override ConfigurationElement CreateNewElement(string elementName)
		{
			return new Url2DefaultElement(elementName);
		}

		/// <summary>
		/// 파생 클래스에서 재정의 될 때 지정된 구성 요소의 요소 키를 가져옵니다.
		/// </summary>
		/// <param name="element"></param>
		/// <returns></returns>
		protected override object GetElementKey(ConfigurationElement element)
		{
			if (element == null)
				return null;

			return ((Url2DefaultElement)element).Default;
		}

		/// <summary>
		/// 구성 요소를 구성 요소 컬렉션에 추가합니다.
		/// </summary>
		/// <param name="index">지정된 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>를 추가할 인덱스 위치 입니다.</param>
		/// <param name="element">추가할 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>입니다.</param>
		protected override void BaseAdd(int index, ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}
		#endregion

		#region Properties
		/// <summary>
		/// <seealso cref="ConfigSectionTest.Url2DefaultElementCollection"/>의 추가 작업과 연결할 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>의 이름을 가져오거나 설정합니다.
		/// </summary>
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}

		/// <summary>
		/// <seealso cref="ConfigSectionTest.Url2DefaultElementCollection"/>의 지우기 작업과 연결할 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>의 이름을 가져오거나 설정합니다.
		/// </summary>
		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.ClearElementName = value;
			}
		}

		/// <summary>
		/// <seealso cref="ConfigSectionTest.Url2DefaultElementCollection"/>의 제거 작업과 연결할 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>의 이름을 가져오거나 설정합니다.
		/// </summary>
		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
			set
			{
				base.RemoveElementName = value;
			}
		}
		#endregion

		#region Public methods
		/// <summary>
		/// 인덱스를 통하여 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>를 가져오거나 설정합니다.
		/// </summary>
		/// <param name="index">액세스할 <seealso cref="ConfigSectionTest.Url2DefaultElementCollection"/>의 인덱스 입니다.</param>
		/// <returns></returns>
		public Url2DefaultElement this[int index]
		{
			get
			{
				return (Url2DefaultElement)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
					base.BaseRemoveAt(index);

				BaseAdd(index, value);
			}
		}

		/// <summary>
		/// Url2DefaultElement를 Property 이름으로 구하거나 설정합니다.
		/// </summary>
		/// <param name="propertyName">액세스할 <seealso cref="Url2DefaultElement"/>의 Property 이름입니다.</param>
		/// <returns></returns>
		public new Url2DefaultElement this[string propertyName]
		{
			get
			{
				return (Url2DefaultElement)base.BaseGet(propertyName);
			}
			set
			{
				ConfigurationElement element = base.BaseGet(propertyName);

				if (element == null)
					BaseAdd(value, false);
				else
				{
					int index = BaseIndexOf(element);

					if (base.BaseGet(index) != null)
						base.BaseRemoveAt(index);

					BaseAdd(index, value);
				}
			}
		}

		/// <summary>
		/// 지정된 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>의 인덱스입니다.
		/// </summary>
		/// <param name="url2DefaultElement">지정된 인덱스 위치에 대한 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>입니다.</param>
		/// <returns></returns>
		public int IndexOf(Url2DefaultElement url2DefaultElement)
		{
			return base.BaseIndexOf(url2DefaultElement);
		}

		/// <summary>
		/// 구성 요소를 구성 요소 컬렉션에 추가합니다.
		/// </summary>
		/// <param name="url2DefaultElement">추가할 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>입니다.</param>
		public void Add(Url2DefaultElement url2DefaultElement)
		{
			base.BaseAdd(url2DefaultElement);
		}

		/// <summary>
		/// 컬렉션에서 <seealso cref="ConfigSectionTest.Url2DefaultElementCollection"/>를 제거합니다.
		/// </summary>
		/// <param name="url2DefaultElement">제거할 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>입니다.</param>
		public void Remove(Url2DefaultElement url2DefaultElement)
		{
			if (BaseIndexOf(url2DefaultElement) >= 0)
				base.BaseRemove(url2DefaultElement.Default);
		}

		/// <summary>
		/// 컬렉션에서 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>를 제거합니다.
		/// </summary>
		/// <param name="key">제거할 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>의 키입니다.</param>
		public void Remove(string key)
		{
			base.BaseRemove(key);
		}

		/// <summary>
		/// 지정된 인덱스 위치에서 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>를 제거합니다.
		/// </summary>
		/// <param name="index">제거할 <seealso cref="ConfigSectionTest.Url2DefaultElement"/>의 인덱스 위치입니다.</param>
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}

		/// <summary>
		/// 컬렉션에서 구성 요소 개체를 모두 제거 합니다.
		/// </summary>
		public void Clear()
		{
			base.BaseClear();
		}
		#endregion
	}
}
