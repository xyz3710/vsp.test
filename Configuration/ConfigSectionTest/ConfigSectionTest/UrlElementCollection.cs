﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace ConfigSectionTest
{
	/// <summary>
	/// 자식 요소의 컬렉션을 포함하는 <seealso cref="ConfigSectionTest.UrlElementCollection"/> 구성 요소를 나타냅니다.
	/// </summary>
	[ConfigurationCollection(typeof(UrlElement), AddItemName="server")]
	public class UrlElementCollection : ConfigurationElementCollection
	{
		private const string DEFAULT_KEY = "default";

		#region Constructors
		/// <summary>
		/// 자식 요소의 컬렉션을 포함하는 <seealso cref="ConfigSectionTest.UrlElementCollection"/>의 새 인스턴스를 초기화합니다.
		/// </summary>
		public UrlElementCollection()
		{
		}
		#endregion

		#region CollectionType
		/// <summary>
		/// <seealso cref="ConfigSectionTest.UrlElementCollectionType"/>의 종류를 가져옵니다.
		/// </summary>
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}
		#endregion

		#region Protected methods
		/// <summary>
		/// 파생 클래스에서 재정의 될 때 <seealso cref="ConfigSectionTest.UrlElement"/>를 만듭니다.
		/// </summary>
		/// <returns></returns>
		protected override ConfigurationElement CreateNewElement()
		{
			return new UrlElement();
		}

		/// <summary>
		/// 파생 클래스에서 재정의 될 때 <seealso cref="ConfigSectionTest.UrlElement"/>를 만듭니다.
		/// </summary>
		/// <param name="elementName">지정된 element 이름으로 <seealso cref="ConfigSectionTest.UrlElement"/>를 만듭니다.</param>
		/// <returns></returns>
		protected override ConfigurationElement CreateNewElement(string elementName)
		{
			return new UrlElement(elementName);
		}

		/// <summary>
		/// 파생 클래스에서 재정의 될 때 지정된 구성 요소의 요소 키를 가져옵니다.
		/// </summary>
		/// <param name="element"></param>
		/// <returns></returns>
		protected override object GetElementKey(ConfigurationElement element)
		{
			if (element == null)
				return null;

			return ((UrlElement)element).Name;
		}
		
		/// <summary>
		/// 구성 요소를 구성 요소 컬렉션에 추가합니다.
		/// </summary>
		/// <param name="index">지정된 <seealso cref="ConfigSectionTest.UrlElement"/>를 추가할 인덱스 위치 입니다.</param>
		/// <param name="element">추가할 <seealso cref="ConfigSectionTest.UrlElement"/>입니다.</param>
		protected override void BaseAdd(int index, ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}
		#endregion

		#region Properties
		/// <summary>
		/// <seealso cref="ConfigSectionTest.UrlElementCollection"/>의 추가 작업과 연결할 <seealso cref="ConfigSectionTest.UrlElement"/>의 이름을 가져오거나 설정합니다.
		/// </summary>
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}

		/// <summary>
		/// <seealso cref="ConfigSectionTest.UrlElementCollection"/>의 지우기 작업과 연결할 <seealso cref="ConfigSectionTest.UrlElement"/>의 이름을 가져오거나 설정합니다.
		/// </summary>
		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.ClearElementName = value;
			}
		}

		/// <summary>
		/// <seealso cref="ConfigSectionTest.UrlElementCollection"/>의 제거 작업과 연결할 <seealso cref="ConfigSectionTest.UrlElement"/>의 이름을 가져오거나 설정합니다.
		/// </summary>
		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
			set
			{
				base.RemoveElementName = value;
			}
		}
		#endregion

		#region Public methods
		/// <summary>
		/// 인덱스를 통하여 <seealso cref="ConfigSectionTest.UrlElement"/>를 가져오거나 설정합니다.
		/// </summary>
		/// <param name="index">액세스할 <seealso cref="ConfigSectionTest.UrlElementCollection"/>의 인덱스 입니다.</param>
		/// <returns></returns>
		public UrlElement this[int index]
		{
			get
			{
				return (UrlElement)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
					base.BaseRemoveAt(index);

				BaseAdd(index, value);
			}
		}

		/// <summary>
		/// UrlElement를 Property 이름으로 구하거나 설정합니다.
		/// </summary>
		/// <param name="propertyName">액세스할 <seealso cref="UrlElement"/>의 Property 이름입니다.</param>
		/// <returns></returns>
		public new UrlElement this[string propertyName]
		{
			get
			{
				return (UrlElement)base.BaseGet(propertyName);
			}
			set
			{
				ConfigurationElement element = base.BaseGet(propertyName);

				if (element == null)
					BaseAdd(value, false);
				else
				{
					int index = BaseIndexOf(element);

					if (base.BaseGet(index) != null)
						base.BaseRemoveAt(index);

					BaseAdd(index, value);
				}
			}
		}

		/// <summary>
		/// 지정된 <seealso cref="ConfigSectionTest.UrlElement"/>의 인덱스입니다.
		/// </summary>
		/// <param name="urlElement">지정된 인덱스 위치에 대한 <seealso cref="ConfigSectionTest.UrlElement"/>입니다.</param>
		/// <returns></returns>
		public int IndexOf(UrlElement urlElement)
		{
			return base.BaseIndexOf(urlElement);
		}

		/// <summary>
		/// 구성 요소를 구성 요소 컬렉션에 추가합니다.
		/// </summary>
		/// <param name="urlElement">추가할 <seealso cref="ConfigSectionTest.UrlElement"/>입니다.</param>
		public void Add(UrlElement urlElement)
		{
			base.BaseAdd(urlElement);
		}

		/// <summary>
		/// 컬렉션에서 <seealso cref="ConfigSectionTest.UrlElementCollection"/>를 제거합니다.
		/// </summary>
		/// <param name="urlElement">제거할 <seealso cref="ConfigSectionTest.UrlElement"/>입니다.</param>
		public void Remove(UrlElement urlElement)
		{
			if (BaseIndexOf(urlElement) >= 0)
				base.BaseRemove(urlElement.Name);
		}

		/// <summary>
		/// 컬렉션에서 <seealso cref="ConfigSectionTest.UrlElement"/>를 제거합니다.
		/// </summary>
		/// <param name="key">제거할 <seealso cref="ConfigSectionTest.UrlElement"/>의 키입니다.</param>
		public void Remove(string key)
		{
			base.BaseRemove(key);
		}

		/// <summary>
		/// 지정된 인덱스 위치에서 <seealso cref="ConfigSectionTest.UrlElement"/>를 제거합니다.
		/// </summary>
		/// <param name="index">제거할 <seealso cref="ConfigSectionTest.UrlElement"/>의 인덱스 위치입니다.</param>
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}

		/// <summary>
		/// 컬렉션에서 구성 요소 개체를 모두 제거 합니다.
		/// </summary>
		public void Clear()
		{
			base.BaseClear();
		}

		/// <summary>
		/// Default url을 구하거나 설정합니다.
        /// </summary>
		[ConfigurationProperty(DEFAULT_KEY, DefaultValue="", IsRequired=false)]
        [Description("Default url을 구하거나 설정합니다.")]
		public string Default
        {
        	get
        	{
				return base.ElementInformation.Properties[DEFAULT_KEY].Value as string;
        	}
        	set
        	{
				base.ElementInformation.Properties[DEFAULT_KEY].Value = value;
        	}
        }
		#endregion
	}
}
