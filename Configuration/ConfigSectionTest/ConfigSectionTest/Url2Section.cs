﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace ConfigSectionTest
{
	public class Url2Section : ConfigurationSection
	{
		/// <summary>
		/// UrlSection2 class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public Url2Section()
		{
		}

        /// <summary>
        /// Url2를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("", IsRequired=false, IsKey=false, IsDefaultCollection=true)]
        [Description("Url2를 구하거나 설정합니다.")]
        public Url2DefaultElementCollection Urls2
        {
        	get
        	{
				// app.config에서 <!--<urls2>--> 를 해제하면 url2 키를 넣어야 한다.
				return (Url2DefaultElementCollection)this[""];
        	}
        	set
        	{
        		this[""] = value;
        	}
        }
	}
}
