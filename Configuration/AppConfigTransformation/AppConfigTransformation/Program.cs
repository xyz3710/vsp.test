﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace AppConfigTransformation
{
	class Program
	{
		static void Main(string[] args)
		{
			var server = ConfigurationManager.AppSettings["Server"];
			
			Console.WriteLine("Server: {0}", server);
		}
	}
}
