﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace iDASiT.Framework.Win.Configuration.Handler
{
	class UpdaterTestDriver
	{
		static void Main(string[] args)
		{
			System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			SectionGroup group = config.SectionGroups[SectionGroup.SectionGroupNameString] as SectionGroup;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new SectionGroup();

				config.SectionGroups.Add(SectionGroup.SectionGroupNameString, group);
			}

			UpdaterSection updaterSection = group.Updater;

			if (updaterSection == null)
			{
				updaterSection = new UpdaterSection();
				group.Sections.Add(UpdaterSection.SectionNameString, updaterSection);

				updaterSection.ForceDownloadFrameworkWin = false;
				updaterSection.ForceDownloadFrameworkWinBaseDate = DateTime.Parse("2008-06-01 00:00:01");
				//updaterSection.ForceDownloadFolders.Add(new FileInfo(""));
				updaterSection.UpdateSkipFolders.Add(new FileInfo("iDoIt"));
				updaterSection.UpdateSkipFolders.Add(new FileInfo("Infragistics"));
				updaterSection.UpdateSkipFolders.Add(new FileInfo("Log"));
			}

			// Server 정보
			UpdateServerCollection serverInfos = updaterSection.Servers;

			if (serverInfos == null || serverInfos.Count == 0)
			{
				serverInfos = new UpdateServerCollection();
				updaterSection.Servers = serverInfos;

				serverInfos.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				serverInfos.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				updaterSection.Servers.Default = "TestServer";
				updaterSection.Servers.DeployFolder = "iDASiT.Framework.Win.Deploy";
				updaterSection.Servers.UpdateFile = "UpdateFile.xml";
				updaterSection.Servers.BufferSize = 512000;
			}

			config.Save();


			Console.WriteLine("\nWith Wrapper\n");

			UpdaterWrapper uw = UpdaterWrapper.GetInstance();

			ConfigurationManager.RefreshSection(UpdaterSection.SectionNameString);

			uw.DeployFolder = "iDASiT.Framework.Win.Updater";
			uw.UpdateFile = "Updater.xml";
			uw.BufferSize = 512;
			uw.ServerAdd("BO", "http://192.168.25.247", 8088);
			uw.ServerRemove("RealServer");
			uw.Save();

			Console.WriteLine(string.Format("Default : {0}", uw.Default));
			Console.WriteLine(string.Format("\tUrl : {0}", uw.Servers[uw.Default]));
			Console.WriteLine(string.Format("DeployFolder : {0}", uw.DeployFolder));
			Console.WriteLine(string.Format("UpdateFile : {0}", uw.UpdateFile));
			Console.WriteLine(string.Format("BufferSize : {0}", uw.BufferSize));

			Console.WriteLine("Server List");
			foreach (string url in uw.Servers.Keys)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}", url,uw.Servers[url]));

			Console.WriteLine(string.Format("ForceDownloadFrameworkWin : {0}", updaterSection.ForceDownloadFrameworkWin));
			Console.WriteLine(string.Format("ForceDownloadFrameworkWinBaseDate : {0}", updaterSection.ForceDownloadFrameworkWinBaseDate));

			Console.WriteLine("ForceDownloadFolders List");
			foreach (string fi in uw.ForceDownloadFolders)
				Console.WriteLine(string.Format("\tName : {0}", fi));

			Console.WriteLine("UpdateSkipFolders List");
			foreach (string fi in uw.UpdateSkipFolders)
				Console.WriteLine(string.Format("\tName : {0}", fi));


			ConfigurationManager.RefreshSection(UpdaterSection.SectionNameString);

			Console.WriteLine("\nWith Configuration\n");

			Console.WriteLine(string.Format("Default : {0}", updaterSection.Servers.Default));
			Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", 
				updaterSection.Servers[updaterSection.Servers.Default].Alias, 
				updaterSection.Servers[updaterSection.Servers.Default].Url, 
				updaterSection.Servers[updaterSection.Servers.Default].Port));
			Console.WriteLine(string.Format("DeployFolder : {0}", updaterSection.Servers.DeployFolder));
			Console.WriteLine(string.Format("UpdateFile : {0}", updaterSection.Servers.UpdateFile));
			Console.WriteLine(string.Format("BufferSize : {0}", updaterSection.Servers.BufferSize));
			
			Console.WriteLine("Server List");
			foreach (ServerInfo si in updaterSection.Servers)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", si.Alias, si.Url, si.Port));

			Console.WriteLine(string.Format("ForceDownloadFrameworkWin : {0}", updaterSection.ForceDownloadFrameworkWin));
			Console.WriteLine(string.Format("ForceDownloadFrameworkWinBaseDate : {0}", updaterSection.ForceDownloadFrameworkWinBaseDate));

			Console.WriteLine("ForceDownloadFolders List");
			foreach (FileInfo fi in updaterSection.ForceDownloadFolders)
				Console.WriteLine(string.Format("\tName : {0}", fi.Name));

			Console.WriteLine("UpdateSkipFolders List");
			foreach (FileInfo fi in updaterSection.UpdateSkipFolders)
				Console.WriteLine(string.Format("\tName : {0}", fi.Name));
		}
	}
}
