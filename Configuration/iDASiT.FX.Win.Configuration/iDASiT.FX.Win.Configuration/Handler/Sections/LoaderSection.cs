﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.LoaderSection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:31
/*	Purpose		:	Login Section에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// Login Section에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class LoaderSection : ConfigurationSection
	{
		/// <summary>
		/// iDASiT.FX.Win.Configuration.Handler의 Loader SectionName string을 구합니다.
		/// </summary>
		public const string SectionNameString = "Loader";

		/// <summary>
		/// LoaderSection class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public LoaderSection()
		{
		}

		/// <summary>
		/// PlantId를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("plantId", DefaultValue="DCCPCS1", IsRequired=true)]
		[Description("PlantId를 구하거나 설정합니다.")]
		public string PlantId
		{
			get
			{
				return (string)this["plantId"];
			}
			set
			{
				this["plantId"] = value;
			}
		}

		/// <summary>
		/// UserId를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("userId", DefaultValue="DEV", IsRequired=true)]
		[Description("UserId를 구하거나 설정합니다.")]
		public string UserId
		{
			get
			{
				return (string)this["userId"];
			}
			set
			{
				this["userId"] = value;
			}
		}

		/// <summary>
		/// EnableMessageService를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("enableMessageService", DefaultValue="false", IsRequired=true)]
		[Description("EnableMessageService를 구하거나 설정합니다.")]
		public bool EnableMessageService
		{
			get
			{
				return (bool)this["enableMessageService"];
			}
			set
			{
				this["enableMessageService"] = value;
			}
		}

		/// <summary>
		/// DebugMode를 구하거나 설정합니다.
		/// <remarks>Loader에서는 Client form을 local이나 server 로드할지 여부를 결정합니다.</remarks>
		/// </summary>
		[ConfigurationProperty("debugMode", DefaultValue="true", IsRequired=true)]
		[Description("DebugMode를 구하거나 설정합니다.(Loader에서는 Client form을 local이나 server 로드할지 여부를 결정합니다)")]
		public bool DebugMode
		{
			get
			{
				return (bool)this["debugMode"];
			}
			set
			{
				this["debugMode"] = value;
			}
		}
        
		/// <summary>
		/// MessageAgent에서 서버와의 연결 상태를 check하는 Interval를 구하거나 설정합니다.(ms 단위)
		/// </summary>
		[ConfigurationProperty("checkServerInterval", DefaultValue="2000", IsRequired=true)]
		[Description("MessageAgent에서 서버와의 연결 상태를 check하는 Interval를 구하거나 설정합니다.(ms 단위)")]
		public int CheckServerInterval
		{
			get
			{
				return (int)this["checkServerInterval"];
			}
			set
			{
				this["checkServerInterval"] = value;
			}
		}
	}
}
