﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.UpdaterCoreSection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:31
/*	Purpose		:	Updater Core Section에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// Updater Core Section에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class UpdaterCoreSection : ConfigurationSection
	{
		/// <summary>
		/// iDASiT.FX.Win.Configuration.Handler의 UpdaterCore SectionName string을 구합니다.
		/// </summary>
		public const string SectionNameString = "UpdaterCore";

		/// <summary>
		/// UpdaterCoreSection class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public UpdaterCoreSection()
		{
		}
				
        /// <summary>
        /// Servers를 구하거나 설정합니다.
        /// </summary>
		[ConfigurationProperty("updateServers", IsRequired=true)]
		[Description("UpdateServer를 구하거나 설정합니다.")]
        public UpdateServerCollection Servers
        {
        	get
        	{
				return (UpdateServerCollection)this["updateServers"];
        	}
        	set
        	{
				this["updateServers"] = value;
        	}
        }
	}
}
