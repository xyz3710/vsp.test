﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.Updater.UpdateServerCollection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 12일 목요일 오후 5:35
/*	Purpose		:	UpdateServer 컬렉션을 포함하는 UpdateServerCollection 구성 요소를 나타냅니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// UpdateServer 컬렉션을 포함하는 UpdateServerCollection 구성 요소를 나타냅니다.
	/// </summary>
	internal class UpdateServerCollection : ServerInfoCollection
	{
		private const string BUFFERSIZE_KEY = "bufferSize";
		private const string UPDATEFILE_KEY = "updateFile";
		private const string DEPLOYFOLDER_KEY = "deployFolder";

		/// <summary>
		/// DeployFolder를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty(DEPLOYFOLDER_KEY, DefaultValue="iDASiT.FX.Win.Deploy", IsRequired=true)]
		[Description("DeployFolder를 구하거나 설정합니다.")]
		public string DeployFolder
		{
			get
			{
				return base.ElementInformation.Properties[DEPLOYFOLDER_KEY].Value as string;
			}
			set
			{
				base.ElementInformation.Properties[DEPLOYFOLDER_KEY].Value = value;
			}
		}

		/// <summary>
		/// UpdateFile를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty(UPDATEFILE_KEY, DefaultValue="UpdateFile.xml", IsRequired=true)]
		[Description("UpdateFile를 구하거나 설정합니다.")]
		public string UpdateFile
		{
			get
			{
				return base.ElementInformation.Properties[UPDATEFILE_KEY].Value as string;
			}
			set
			{
				base.ElementInformation.Properties[UPDATEFILE_KEY].Value = value;
			}
		}

		/// <summary>
		/// BufferSize를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty(BUFFERSIZE_KEY, DefaultValue="512000", IsRequired=true)]
		[Description("BufferSize를 구하거나 설정합니다.")]
		public int BufferSize
		{
			get
			{
				return (int)base.ElementInformation.Properties[BUFFERSIZE_KEY].Value;
			}
			set
			{
				base.ElementInformation.Properties[BUFFERSIZE_KEY].Value = value;
			}
		}
	}
}
