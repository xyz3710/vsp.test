﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.InternalServerInfo
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:36
/*	Purpose		:	현재 assembly 내에서 Server 정보에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// 현재 assembly 내에서 Server 정보에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class InternalServerInfo : ConfigurationElement
	{
		/// <summary>
		/// ServerInfo class의 새 인스턴스를 초기화합니다.
		/// <param name="alias">Server alias(ex : Real)</param>
		/// <param name="uri">Server service Uri(ex : http://127.0.0.1)</param>
		/// <param name="port">Server serivce port(Option)</param>
		/// </summary>
		public InternalServerInfo(string alias, string uri, int port)
		{
			Alias = alias;
			Uri = uri;
			Port = port;
		}

		/// <summary>
		/// ServerInfo class의 새 인스턴스를 초기화합니다.
		/// <param name="alias">Server alias(ex : Real)</param>
		/// <param name="uri">Server service Uri(ex : http://127.0.0.1)</param>
		/// </summary>
		public InternalServerInfo(string alias, string uri)
		{
			Alias = alias;
			Uri = uri;
		}

		/// <summary>
		/// ServerInfo class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public InternalServerInfo()
		{
		}

		/// <summary>
		/// Alias를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("alias", IsRequired=true, IsKey=true)]
		[Description("Alias를 구하거나 설정합니다.")]
		public string Alias
		{
			get
			{
				return (string)this["alias"];
			}
			set
			{
				this["alias"] = value;
			}
		}

		/// <summary>
		/// Uri를 구하거나 설정합니다.(ex : http://127.0.0.1)
		/// </summary>
		[ConfigurationProperty("uri", DefaultValue="http://127.0.0.1", IsRequired=true)]
		[Description("Uri를 구하거나 설정합니다.(ex : http://127.0.0.1)")]
		public string Uri
		{
			get
			{
				string uri = (string)this["uri"];

				if (string.IsNullOrEmpty(uri) == true)
					uri = "http://127.0.0.1";

				return uri;
			}
			set
			{
				this["uri"] = value;
			}
		}

		/// <summary>
		/// Port를 구하거나 설정합니다.(default : 80)
		/// </summary>
		[ConfigurationProperty("port", DefaultValue="80", IsRequired=false)]
		[Description("Port를 구하거나 설정합니다.(default : 80)")]
		public int Port
		{
			get
			{
				int port = (int)this["port"];

				if (port == 0)
					port = 80;

				return port;
			}
			set
			{
				this["port"] = value;
			}
		}

		/// <summary>
		/// 입력된 Uri로 Protocol을 구합니다.(default : http://)
		/// </summary>
		public string Protocol
		{
			get
			{
				string protocol = "http://";

				if (string.IsNullOrEmpty(Uri) == true)
					return protocol;

				Regex regex = new Regex(@"(?<Protocol>[A-Z,a-z]{3,}\;\/\/)");

				if (regex.IsMatch(Uri) == true)
					protocol = regex.Match(Uri).Groups["Protocol"].Value;

				return protocol;
			}
		}

		/// <summary>
		/// 입력된 Uri로 Host Ip or Domain을 구합니다.(default : 127.0.0.1)
		/// </summary>
		public string Host
		{
			get
			{
				string host = "127.0.0.1";

				if (string.IsNullOrEmpty(Uri) == true)
					return host;

				Regex regex = new Regex(@"(?<Host>[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})");

				if (regex.IsMatch(Uri) == true)
					host = regex.Match(Uri).Groups["Host"].Value;

				return host;
			}
		}
	}
}
