﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.LoaderWrapper
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 11일 수요일 오후 3:28
/*	Purpose		:	Loader의 설정 파일에 관련된 Wrapper 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler;

namespace iDASiT.FX.Win.Configuration
{
	/// <summary>
	/// Loader의 설정 파일에 관련된 Wrapper 클래스 입니다.
	/// </summary>
	public class LoaderWrapper : WrapperBase
	{
		#region Fields
        private LoaderSection _loaderSection;
		#endregion

		#region Use Singleton LoaderWrapper
		private static LoaderWrapper _newInstance;

		#region Constructor for Single Ton LoaderWrapper
		/// <summary>
        /// LoaderWrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
        /// </summary>
        /// <returns></returns>
        private LoaderWrapper(string exePath)
        {
        	_newInstance = null;
			Config = GetConfig(exePath);

			Sections group = Config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new Sections();

				Config.SectionGroups.Add(Sections.SectionGroupNameString, group);
			}

			_loaderSection = group.Loader;

			if (_loaderSection == null)
			{
				_loaderSection = new LoaderSection();
				group.Sections.Add(LoaderSection.SectionNameString, _loaderSection);

				_loaderSection.PlantId = "DCCPCS1";
				_loaderSection.UserId = "DEV";
				_loaderSection.EnableMessageService = false;
				_loaderSection.DebugMode = true;
				_loaderSection.CheckServerInterval = 2000;
			}

			SaveModifed();
        }
        #endregion 
        
        #region GetInstance
		/// <summary>
		/// LoaderWrapper class의 Single Ton을 위한 새 인스턴스를 초기화 합니다.
		/// 
		/// </summary>
		/// <returns></returns>
		public static LoaderWrapper GetInstance()
		{
			string exePath = String.Empty;

			return GetInstance(exePath);
		}
        
		/// <summary>
        /// LoaderWrapper class의 Single Ton을 위한 새 인스턴스를 초기화 합니다.
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.</param>
		/// </summary>
        /// <returns></returns>
		public static LoaderWrapper GetInstance(string exePath)
        {
        	if (_newInstance == null)
				_newInstance = new LoaderWrapper(exePath);
        
        	return _newInstance;
        }

		/// <summary>
		/// LoaderWrapper class의 초기화된 인스턴스를 구합니다.
		/// <remarks>기본 config는 현재 실행 파일이름.config 입니다.</remarks>
		/// </summary>
		public static LoaderWrapper Instance
		{
			get
			{
				if (_newInstance == null)
					GetInstance();

				return _newInstance;
			}
		}
		#endregion

        #endregion
                
		#region Properties
		/// <summary>
		/// PlantId를 구하거나 설정합니다.
		/// </summary>
		public string PlantId
		{
			get
			{
				return _loaderSection.PlantId;
			}
			set
			{
				_loaderSection.PlantId = value;
			}
		}

		/// <summary>
		/// UserId를 구하거나 설정합니다.
		/// </summary>
		public string UserId
		{
			get
			{
				return _loaderSection.UserId;
			}
			set
			{
				_loaderSection.UserId = value;
			}
		}

		/// <summary>
		/// EnableMessageService를 구하거나 설정합니다.
		/// </summary>
		public bool EnableMessageService
		{
			get
			{
				return _loaderSection.EnableMessageService;
			}
			set
			{
				_loaderSection.EnableMessageService = value;
			}
		}

		/// <summary>
		/// Client form을 local이나 server 로드할지 여부를 구하거나 설정합니다.
		/// </summary>
		public bool DebugMode
		{
			get
			{
				return _loaderSection.DebugMode;
			}
			set
			{
				_loaderSection.DebugMode = value;
			}
		}

		/// <summary>
		/// MessageAgent에서 서버와의 연결 상태를 check하는 Interval를 구하거나 설정합니다.(ms 단위)
		/// </summary>
		public int CheckServerInterval
		{
			get
			{
				return _loaderSection.CheckServerInterval;
			}
			set
			{
				_loaderSection.CheckServerInterval = value;
			}
		}		
        #endregion	
	}
}
