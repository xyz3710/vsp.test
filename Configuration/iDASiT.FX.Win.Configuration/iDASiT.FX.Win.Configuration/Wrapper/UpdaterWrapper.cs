﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.UpdaterWrapper
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 11일 수요일 오후 3:28
/*	Purpose		:	Updater의 설정 파일에 관련된 Wrapper 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler;
using System.Text.RegularExpressions;

namespace iDASiT.FX.Win.Configuration
{
	/// <summary>
	/// Updater의 설정 파일에 관련된 Wrapper 클래스 입니다.
	/// </summary>
	public class UpdaterWrapper : WrapperBase
	{
		#region Fields
		private UpdaterSection _updaterSection;
		private FileInfoCollection _forceDownloadFolderCollection;
		private FileInfoCollection _updateSkipFolderCollection;
		private List<string> _forceDownloadFolders;
		private List<string> _updateSkipFolders;
		#endregion

		#region Use Singleton UpdaterWrapper
		private static UpdaterWrapper _newInstance;

		#region Constructor for Single Ton UpdaterWrapper
		/// <summary>
		/// UpdaterWrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
		/// </summary>
		/// <returns></returns>
		private UpdaterWrapper(string exePath)
		{
			_newInstance = null;
			Config = GetConfig(exePath);

			Sections group = Config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new Sections();

				Config.SectionGroups.Add(Sections.SectionGroupNameString, group);
			}

			_updaterSection = group.Updater;

			if (_updaterSection == null)
			{
				_updaterSection = new UpdaterSection();
				group.Sections.Add(UpdaterSection.SectionNameString, _updaterSection);

				_updaterSection.ForceDownloadFXWin = false;
				_updaterSection.ForceDownloadFXWinBaseDate = DateTime.Parse("2008-09-25 00:00:01");
				//updaterSection.ForceDownloadFolders.Add(new FileInfo(""));
				_updaterSection.UpdateSkipFolders.Add(new FileInfo("iDoIt"));
				_updaterSection.UpdateSkipFolders.Add(new FileInfo("Infragistics"));
				_updaterSection.UpdateSkipFolders.Add(new FileInfo("Log"));
			}

			_forceDownloadFolderCollection = _updaterSection.ForceDownloadFolders;
			_updateSkipFolderCollection = _updaterSection.UpdateSkipFolders;

			SaveModifed();
		}
		#endregion

		#region GetInstance
		/// <summary>
		/// UpdaterWrapper class의 Single Ton을 위한 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <returns></returns>
		public static UpdaterWrapper GetInstance()
		{
			string exePath = String.Empty;

			return GetInstance(exePath);
		}

		/// <summary>
		/// UpdaterWrapper class의 Single Ton BufferSize을 위한 새 인스턴스를 초기화 합니다.
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.</param>
		/// </summary>
		/// <returns></returns>
		public static UpdaterWrapper GetInstance(string exePath)
		{
			if (_newInstance == null)
				_newInstance = new UpdaterWrapper(exePath);

			return _newInstance;
		}

		/// <summary>
		/// UpdaterWrapper class의 초기화된 인스턴스를 구합니다.
		/// <remarks>기본 config는 현재 실행 파일이름.config 입니다.</remarks>
		/// </summary>
		public static UpdaterWrapper Instance
		{
			get
			{
				if (_newInstance == null)
					GetInstance();

				return _newInstance;
			}
		}
		#endregion

		#endregion

		#region Properties
		#region For Test
		/*
		/// <summary>
		/// DeployFolder를 구하거나 설정합니다.
		/// </summary>
		public string DeployFolder
		{
			get
			{
				return UpdaterCoreWrapper.Instance.DeployFolder;
			}
			set
			{
				UpdaterCoreWrapper.Instance.DeployFolder = value;
			}
		}

		/// <summary>
		/// UpdateFile를 구하거나 설정합니다.
		/// </summary>
		public string UpdateFile
		{
			get
			{
				return UpdaterCoreWrapper.Instance.UpdateFile;
			}
			set
			{
				UpdaterCoreWrapper.Instance.UpdateFile = value;
			}
		}

		/// <summary>
		/// BufferSize를 구하거나 설정합니다.
		/// </summary>
		public int BufferSize
		{
			get
			{
				return UpdaterCoreWrapper.Instance.BufferSize;
			}
			set
			{
				UpdaterCoreWrapper.Instance.BufferSize = value;
			}
		}

		/// <summary>
		/// Default Server의 Alias를 구하거나 설정합니다.
		/// </summary>
		public string DefaultAlias
		{
			get
			{
				return UpdaterCoreWrapper.Instance.DefaultAlias;
			}
			set
			{
				UpdaterCoreWrapper.Instance.DefaultAlias = value;
			}
		}

		/// <summary>
		/// Server를 구하거나 설정합니다.<br/>
		/// </summary>
		public Dictionary<string, ServerInfo> Servers
		{
			get
			{
				return UpdaterCoreWrapper.Instance.Servers;
			}
			set
			{
				UpdaterCoreWrapper.Instance.Servers = value;
			}
		}

		/// <summary>
		/// 기본 서버에 대한 Server 정보를 구합니다.
		/// </summary>
		public ServerInfo Default
		{
			get
			{
				return UpdaterCoreWrapper.Instance.Servers[DefaultAlias];
			}
		}
		
		/// <summary>
		/// 입력된 정보로 기본 Url(Uri : Port / DeployFolder)을 구합니다.
		/// </summary>
		public string DefaultUrl
		{
			get
			{
				return UpdaterCoreWrapper.Instance.DefaultUrl;
			}
		}
		*/
		#endregion

		/// <summary>
		/// <seealso cref="ForceDownloadFXWinBaseDate"/> 설정일 이전의 FX.Win을 강제로 다운로드 하도록 하는 값을 구하거나 설정합니다.
		/// </summary>
		public bool ForceDownloadFXWin
		{
			get
			{
				return _updaterSection.ForceDownloadFXWin;
			}
			set
			{
				_updaterSection.ForceDownloadFXWin = value;
			}
		}

		/// <summary>
		/// <seealso cref="ForceDownloadFXWin"/> 값이 True로 되어 있을 경우 Local에 있는 FX.Win의 날짜가 설정일 이전은 강제 다운 로드 합니다.
		/// </summary>
		public DateTime ForceDownloadFXWinBaseDate
		{
			get
			{
				return _updaterSection.ForceDownloadFXWinBaseDate;
			}
			set
			{
				_updaterSection.ForceDownloadFXWinBaseDate = value;
			}
		}

		/// <summary>
		/// ForceDownloadFolder을 구하거나 설정합니다.
		/// <remarks>ForceDownloadFolder의 추가나 삭제 모두 제거는 <seealso cref="ForceDownloadFolderAdd"/>, <seealso cref="ForceDownloadFolderRemove"/>, <seealso cref="ForceDownloadFolderClear"/> 메서드를 사용하십시오.</remarks>
		/// </summary>
		public List<string> ForceDownloadFolders
		{
			get
			{
				if (_forceDownloadFolders == null)
				{
					_forceDownloadFolders = new List<string>();

					foreach (FileInfo fi in _forceDownloadFolderCollection)
						_forceDownloadFolders.Add(fi.Name);
				}

				return _forceDownloadFolders;
			}
			set
			{
				if (value.Count > 0)
				{
					_forceDownloadFolders.Clear();

					foreach (string filename in value)
						_forceDownloadFolderCollection.Add(new FileInfo(filename));

					_forceDownloadFolders = value;
				}
			}
		}

		/// <summary>
		/// UpdateSkipFolder을 구하거나 설정합니다.
		/// <remarks>UpdateSkipFolder의 추가나 삭제 모두 제거는 <seealso cref="UpdateSkipFolderAdd"/>, <seealso cref="UpdateSkipFolderRemove"/>, <seealso cref="UpdateSkipFolderClear"/> 메서드를 사용하십시오.</remarks>
		/// </summary>
		public List<string> UpdateSkipFolders
		{
			get
			{
				if (_updateSkipFolders == null)
				{
					_updateSkipFolders = new List<string>();

					foreach (FileInfo fi in _updateSkipFolderCollection)
						_updateSkipFolders.Add(fi.Name);
				}

				return _updateSkipFolders;
			}
			set
			{
				if (value.Count > 0)
				{
					_updateSkipFolders.Clear();

					foreach (string filename in value)
						_updateSkipFolderCollection.Add(new FileInfo(filename));

					_updateSkipFolders = value;
				}
			}
		}
		#endregion

		/// <summary>
		/// Upate Server를 추가합니다.
		/// </summary>
		/// <param name="alias">추가할 Server alias(ex : Real)</param>
		/// <param name="url">추가할 Server service Url(ex : http://127.0.0.1)</param>
		/// <param name="port">추가할 Server serivce port(Option)</param>
		public void ServerAdd(string alias, string url, int port)
		{
			UpdaterCoreWrapper.Instance.ServerAdd(alias, url, port);
		}

		/// <summary>
		/// Upate Server를 제거합니다.
		/// </summary>
		/// <param name="alias">Server alias(ex : Real)</param>
		public void ServerRemove(string alias)
		{
			UpdaterCoreWrapper.Instance.ServerRemove(alias);
		}

		/// <summary>
		/// Upate Server 목록을 모두 제거 합니다.
		/// </summary>
		public void ServerClear()
		{
			UpdaterCoreWrapper.Instance.ServerClear();
		}

		/// <summary>
		/// 대상 Filename을 추가합니다.
		/// </summary>
		/// <param name="filename">추가할 filename</param>
		public void ForceDownloadFolderAdd(string filename)
		{
			_forceDownloadFolderCollection.Add(new FileInfo(filename));
		}

		/// <summary>
		/// 대상 Filename을 제거합니다.
		/// </summary>
		/// <param name="filename">제거할 filename</param>
		public void ForceDownloadFolderRemove(string filename)
		{
			_forceDownloadFolderCollection.Remove(filename);
		}

		/// <summary>
		/// 대상 Filename 목록을 모두 제거 합니다.
		/// </summary>
		public void ForceDownloadFolderClear()
		{
			_forceDownloadFolderCollection.Clear();
		}

		/// <summary>
		/// 대상 Filename을 추가합니다.
		/// </summary>
		/// <param name="filename">추가할 filename</param>
		public void UpdateSkipFolderAdd(string filename)
		{
			_updateSkipFolderCollection.Add(new FileInfo(filename));
		}

		/// <summary>
		/// 대상 Filename을 제거합니다.
		/// </summary>
		/// <param name="filename">제거할 filename</param>
		public void UpdateSkipFolderRemove(string filename)
		{
			_updateSkipFolderCollection.Remove(filename);
		}

		/// <summary>
		/// 대상 Filename 목록을 모두 제거 합니다.
		/// </summary>
		public void UpdateSkipFolderClear()
		{
			_updateSkipFolderCollection.Clear();
		}
	}
}
