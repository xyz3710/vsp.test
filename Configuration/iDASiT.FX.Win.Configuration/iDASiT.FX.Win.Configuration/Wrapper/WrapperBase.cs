﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.WrapperBase
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 9월 24일 수요일 오후 12:29
/*	Purpose		:	설정 파일의 Wrapper에 관련된 기본 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler;
using System.Text.RegularExpressions;

namespace iDASiT.FX.Win.Configuration
{
	/// <summary>
	/// 설정 파일의 Wrapper에 관련된 기본 클래스 입니다.
	/// </summary>
	public class WrapperBase
	{		
		/// <summary>
		/// 설정 파일을 처리하는 Instance 입니다.
		/// </summary>
		protected static System.Configuration.Configuration Config;
        
		/// <summary>
        /// LoginWrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
        /// </summary>
        /// <returns></returns>
		protected System.Configuration.Configuration GetConfig(string exePath)
        {
			if (string.IsNullOrEmpty(exePath) == true)
				Config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			else
			{
				string extension = System.IO.Path.GetExtension(exePath).Replace(".", string.Empty).ToLower();

				if (extension == "config")
				{
					ExeConfigurationFileMap configFile = new ExeConfigurationFileMap();

					configFile.ExeConfigFilename = exePath;

					Config = ConfigurationManager.OpenMappedExeConfiguration(configFile, ConfigurationUserLevel.None);
				}
				else
					Config = ConfigurationManager.OpenExeConfiguration(exePath);
			}

			return Config;
        }
        
		/// <summary>
		/// Config 파일에 변경 사항만을 저장합니다.
		/// </summary>		
		public void SaveModifed()
		{
			Config.Save(ConfigurationSaveMode.Modified);
		}

		/// <summary>
		/// Config 파일에 저장 합니다.
		/// </summary>
		public void Save()
		{
			Config.Save();
		}
	}
}
