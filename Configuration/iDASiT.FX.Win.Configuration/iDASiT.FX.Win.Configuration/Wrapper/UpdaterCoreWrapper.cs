﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.UpdaterCoreWrapper
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 11일 수요일 오후 3:28
/*	Purpose		:	Updater Core의 설정 파일에 관련된 Wrapper 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler;
using System.Text.RegularExpressions;

namespace iDASiT.FX.Win.Configuration
{
	/// <summary>
	/// Updater Core의 설정 파일에 관련된 Wrapper 클래스 입니다.
	/// </summary>
	public class UpdaterCoreWrapper : WrapperBase
	{
		#region Fields
		private UpdaterCoreSection _updaterCoreSection;
		private UpdateServerCollection _serverInfos;
		private Dictionary<string, ServerInfo> _servers;
		#endregion

		#region Use Singleton UpdaterWrapper
		private static UpdaterCoreWrapper _newInstance;

		#region Constructor for Single Ton UpdaterWrapper
		/// <summary>
        /// UpdaterCoreWrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
        /// </summary>
		/// <param name="exePath">실행 파일의 경로 입니다.</param>
        /// <returns></returns>
        private UpdaterCoreWrapper(string exePath)
        {
        	_newInstance = null;
			Config = GetConfig(exePath);

			Sections group = Config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new Sections();

				Config.SectionGroups.Add(Sections.SectionGroupNameString, group);
			}

			_updaterCoreSection = group.UpdaterCore;

			if (_updaterCoreSection == null)
			{
				_updaterCoreSection = new UpdaterCoreSection();
				group.Sections.Add(UpdaterCoreSection.SectionNameString, _updaterCoreSection);
			}

			// Server 정보
			_serverInfos = _updaterCoreSection.Servers;

			if (_serverInfos == null || _serverInfos.Count == 0)
			{
				_serverInfos = new UpdateServerCollection();
				_updaterCoreSection.Servers = _serverInfos;

				_serverInfos.Add(new InternalServerInfo("Web Server", "http://192.168.25.246", 8088));
				_serverInfos.Add(new InternalServerInfo("BO Server", "http://192.168.25.247", 8088));
				_serverInfos.Add(new InternalServerInfo("Test Server", "http://192.168.25.28", 8088));

				// Default Server 정보
				_updaterCoreSection.Servers.Default = "Test Server";
				_updaterCoreSection.Servers.DeployFolder = "iDASiT.FX.Win.Deploy";
				_updaterCoreSection.Servers.UpdateFile = "UpdateFile.xml";
				_updaterCoreSection.Servers.BufferSize = 512000;
			}

			SaveModifed();
        }
        #endregion 
        
        #region GetInstance
		/// <summary>
		/// UpdaterCoreWrapper class의 Single Ton을 위한 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <returns></returns>
		public static UpdaterCoreWrapper GetInstance()
		{
			string exePath = String.Empty;

			return GetInstance(exePath);
		}
        
		/// <summary>
        /// UpdaterCoreWrapper class의 Single Ton을 위한 새 인스턴스를 초기화 합니다.
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.</param>
		/// </summary>
        /// <returns></returns>
		public static UpdaterCoreWrapper GetInstance(string exePath)
        {
        	if (_newInstance == null)
				_newInstance = new UpdaterCoreWrapper(exePath);
        
        	return _newInstance;
        }

		/// <summary>
		/// UpdaterCoreWrapper class의 초기화된 인스턴스를 구합니다.
		/// <remarks>기본 config는 현재 실행 파일이름.config 입니다.</remarks>
		/// </summary>
		public static UpdaterCoreWrapper Instance
		{
			get
			{
				if (_newInstance == null)
					GetInstance();

				return _newInstance;
			}
		}
		#endregion
        #endregion
                
		#region Properties
		/// <summary>
		/// DeployFolder를 구하거나 설정합니다.
		/// </summary>
		public string DeployFolder
		{
			get
			{
				return _updaterCoreSection.Servers.DeployFolder;
			}
			set
			{
				_updaterCoreSection.Servers.DeployFolder = value;
			}
		}
        
        /// <summary>
        /// UpdateFile를 구하거나 설정합니다.
        /// </summary>
        public string UpdateFile
        {
        	get
        	{
				return _updaterCoreSection.Servers.UpdateFile;
        	}
        	set
        	{
				_updaterCoreSection.Servers.UpdateFile = value;
        	}
        }
        
        /// <summary>
        /// BufferSize를 구하거나 설정합니다.
        /// </summary>
        public int BufferSize
        {
        	get
        	{
				return _updaterCoreSection.Servers.BufferSize;
        	}
        	set
        	{
				_updaterCoreSection.Servers.BufferSize = value;
        	}
        }
        
        /// <summary>
        /// Default Server의 Alias를 구하거나 설정합니다.
        /// </summary>
        public string DefaultAlias
        {
        	get
        	{
				return _updaterCoreSection.Servers.Default;
        	}
        	set
        	{
				_updaterCoreSection.Servers.Default = value;
        	}
        }

		/// <summary>
		/// Server를 구하거나 설정합니다.<br/>
		/// <remarks>Server를 추가나 삭제 모두 제거는 <seealso cref="ServerAdd"/>, <seealso cref="ServerRemove"/>, <seealso cref="ServerClear"/> 메서드를 사용하십시오.</remarks>
		/// </summary>
		public Dictionary<string, ServerInfo> Servers
		{
			get
			{
				if (_servers == null)
				{
					_servers = new Dictionary<string, ServerInfo>();

					foreach (InternalServerInfo si in _serverInfos)
						_servers.Add(si.Alias, new ServerInfo(si.Alias, si.Uri, si.Port));
				}

				return _servers;
			}
			set
			{
				_servers = value;
			}
		}

		/// <summary>
		/// 기본 서버에 대한 Server 정보를 구합니다.
		/// </summary>
		public ServerInfo Default
		{
			get
			{
				return Servers[DefaultAlias];
			}
		}

		/// <summary>
		/// 입력된 정보로 기본 Url(Uri : Port / DeployFolder)을 구합니다.
		/// </summary>
		public string DefaultUrl
		{
			get
			{
				return string.Format("{0}:{1}/{2}", Default.Uri, Default.Port, DeployFolder);
			}
		}
        #endregion

		/// <summary>
		/// Upate Server를 추가합니다.
		/// </summary>
		/// <param name="alias">추가할 Server alias(ex : Real)</param>
		/// <param name="url">추가할 Server service Url(ex : http://127.0.0.1)</param>
		/// <param name="port">추가할 Server serivce port(Option)</param>
		public void ServerAdd(string alias, string url, int port)
		{
			_serverInfos.Add(new InternalServerInfo(alias, url, port));
		}

		/// <summary>
		/// Upate Server를 제거합니다.
		/// </summary>
		/// <param name="alias">Server alias(ex : Real)</param>
		public void ServerRemove(string alias)
		{
			_serverInfos.Remove(alias);
		}

		/// <summary>
		/// Upate Server 목록을 모두 제거 합니다.
		/// </summary>
		public void ServerClear()
		{
			_serverInfos.Clear();
		}
	}
}
