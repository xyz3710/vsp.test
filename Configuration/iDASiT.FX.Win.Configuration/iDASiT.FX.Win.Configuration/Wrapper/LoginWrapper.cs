﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.LoginWrapper
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 11일 수요일 오후 3:28
/*	Purpose		:	Login의 설정 파일에 관련된 Wrapper 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler;
using System.Text.RegularExpressions;

namespace iDASiT.FX.Win.Configuration
{
	/// <summary>
	/// Login의 설정 파일에 관련된 Wrapper 클래스 입니다.
	/// </summary>
	public class LoginWrapper : WrapperBase
	{
		#region Fields
        private LoginSection _loginSection;
		private FileInfoCollection _updateNeededFileCollection;
		private List<string> _updateNeededFiles;
		#endregion

		#region Use Singleton LoginWrapper
		private static LoginWrapper _newInstance;

		#region Constructor for Single Ton LoginWrapper
		/// <summary>
        /// LoginWrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
        /// </summary>
        /// <returns></returns>
        private LoginWrapper(string exePath)
        {
        	_newInstance = null;
			Config = GetConfig(exePath);

			Sections group = Config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new Sections();

				Config.SectionGroups.Add(Sections.SectionGroupNameString, group);
			}

			_loginSection = group.Login;

			if (_loginSection == null)
			{
				_loginSection = new LoginSection();
				group.Sections.Add(LoginSection.SectionNameString, _loginSection);

				_loginSection.CheckUpdateNeededFile = false;
				_loginSection.LimitFileCountOnBeforeDownload = 30;
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Server.dll"));
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.dll"));
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Configuration.dll"));
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Updater.exe"));
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Updater.exe.config"));
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Resources.dll"));
			}

			_updateNeededFileCollection = _loginSection.UpdateNeededFiles;

			SaveModifed();
        }
        #endregion 
        
        #region GetInstance
		/// <summary>
		/// LoginWrapper class의 Single Ton을 위한 새 인스턴스를 초기화 합니다.
		/// 
		/// </summary>
		/// <returns></returns>
		public static LoginWrapper GetInstance()
		{
			string exePath = String.Empty;

			return GetInstance(exePath);
		}
        
		/// <summary>
        /// LoginWrapper class의 Single Ton을 위한 새 인스턴스를 초기화 합니다.
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.</param>
		/// </summary>
        /// <returns></returns>
		public static LoginWrapper GetInstance(string exePath)
        {
        	if (_newInstance == null)
				_newInstance = new LoginWrapper(exePath);
        
        	return _newInstance;
        }

		/// <summary>
		/// LoginWrapper class의 초기화된 인스턴스를 구합니다.
		/// <remarks>기본 config는 현재 실행 파일이름.config 입니다.</remarks>
		/// </summary>
		public static LoginWrapper Instance
		{
			get
			{
				if (_newInstance == null)
					GetInstance();

				return _newInstance;
			}
		}
		#endregion

        #endregion
                
		#region Properties
		/// <summary>
		/// LimitFileCountOnBeforeDownload를 구하거나 설정합니다.<br/>
		/// Updater Download 전 최대 어셈블리 개수를 구하거나 설정 하여 해당 개수 이상이면 Win 폴더를 제거 하고 다시 받습니다.
		/// </summary>
		public int LimitFileCountOnBeforeDownload
		{
			get
			{
				return _loginSection.LimitFileCountOnBeforeDownload;
			}
			set
			{
				_loginSection.LimitFileCountOnBeforeDownload = value;
			}
		}

		/// <summary>
		/// Update 필요 파일의 검사여부를 구하거나 설정합니다.
		/// <remarks>false이면 <seealso cref="UpdateNeededFiles"/>를 검사하지 않습니다.</remarks>
		/// </summary>
		public bool CheckUpdateNeededFile
		{
			get
			{
				return _loginSection.CheckUpdateNeededFile;
			}
			set
			{
				_loginSection.CheckUpdateNeededFile = value;
			}
		}

		/// <summary>
		/// UpdateNeededFile을 구하거나 설정합니다.
		/// <remarks>UpdateNeededFile의 추가나 삭제 모두 제거는 <seealso cref="UpdateNeededFileAdd"/>, <seealso cref="UpdateNeededFileRemove"/>, <seealso cref="UpdateNeededFileClear"/> 메서드를 사용하십시오.</remarks>
		/// </summary>
		public List<string> UpdateNeededFiles
		{
			get
			{
				if (_updateNeededFiles == null)
				{
					_updateNeededFiles = new List<string>();

					foreach (FileInfo fi in _updateNeededFileCollection)
						_updateNeededFiles.Add(fi.Name);
				}

				return _updateNeededFiles;
			}
			set
			{
				if (value.Count > 0)
				{
					_updateNeededFiles.Clear();

					foreach (string filename in value)
						_updateNeededFileCollection.Add(new FileInfo(filename));
				}

				_updateNeededFiles = value;
			}
		}
        #endregion
		
		/// <summary>
		/// 대상 Filename을 추가합니다.
		/// </summary>
		/// <param name="filename">추가할 filename</param>
		public void UpdateNeededFileAdd(string filename)
		{
			_updateNeededFileCollection.Add(new FileInfo(filename));
		}

		/// <summary>
		/// 대상 Filename을 제거합니다.
		/// </summary>
		/// <param name="filename">제거할 filename</param>
		public void UpdateNeededFileRemove(string filename)
		{
			_updateNeededFileCollection.Remove(filename);
		}

		/// <summary>
		/// 대상 Filename 목록을 모두 제거 합니다.
		/// </summary>
		public void UpdateNeededFileClear()
		{
			_updateNeededFileCollection.Clear();
		}
	}
}
