﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler.ServerChanger;
using iDASiT.FX.Win.Configuration.Handler.AutoServerSwitcher;

namespace iDASiT.FX.Win.Configuration.Handler
{
	class ServerChangerTestDriver
	{
		static void Main(string[] args)
		{
			#region Updater
			Console.WriteLine("\nUpdater Configuration\n");

			Console.WriteLine("Default : {0}", UpdaterWrapper.Instance.DefaultAlias);
			Console.WriteLine("\tAlias : {0}\tUri : {1}\tPort : {2}",
				UpdaterWrapper.Instance.Default.Alias,
				UpdaterWrapper.Instance.Default.Uri,
				UpdaterWrapper.Instance.Default.Port);
			Console.WriteLine("\tUrl   : {0}", UpdaterWrapper.Instance.DefaultUrl);
			Console.WriteLine("DeployFolder : {0}", UpdaterWrapper.Instance.DeployFolder);
			Console.WriteLine("UpdateFile : {0}", UpdaterWrapper.Instance.UpdateFile);
			Console.WriteLine("BufferSize : {0}", UpdaterWrapper.Instance.BufferSize);

			Console.WriteLine("Server List");
			foreach (ServerInfo si in UpdaterWrapper.Instance.Servers.Values)
				Console.WriteLine("\tAlias : {0}\tUri : {1}\tPort : {2}", si.Alias, si.Uri, si.Port);

			Console.WriteLine("ForceDownloadFXWin : {0}", UpdaterWrapper.Instance.ForceDownloadFXWin);
			Console.WriteLine("ForceDownloadFXWinBaseDate : {0}", UpdaterWrapper.Instance.ForceDownloadFXWinBaseDate);

			Console.WriteLine("ForceDownloadFolders List");
			foreach (string fi in UpdaterWrapper.Instance.ForceDownloadFolders)
				Console.WriteLine("\tName : {0}", fi);

			Console.WriteLine("UpdateSkipFolders List");
			foreach (string fi in UpdaterWrapper.Instance.UpdateSkipFolders)
				Console.WriteLine("\tName : {0}", fi);
			#endregion

			#region Login
			Console.WriteLine("\nLogin Configuration\n");

			#region For Test
			/*
			Console.WriteLine("Default : {0}", loginSection.Servers.Default);
			Console.WriteLine("\tAlias : {0}\tUri : {1}\tPort : {2}",
				loginSection.Servers[loginSection.Servers.Default].Alias,
				loginSection.Servers[loginSection.Servers.Default].Uri,
				loginSection.Servers[loginSection.Servers.Default].Port);
			Console.WriteLine("DeployFolder : {0}", loginSection.Servers.DeployFolder);
			Console.WriteLine("UpdateFile : {0}", loginSection.Servers.UpdateFile);
			Console.WriteLine("BufferSize : {0}", loginSection.Servers.BufferSize);

			Console.WriteLine("Server List");
			foreach (ServerInfo si in loginSection.Servers)
				Console.WriteLine("\tAlias : {0}\tUri : {1}\tPort : {2}", si.Alias, si.Uri, si.Port);
			*/
			#endregion

			Console.WriteLine("LimitFileCountOnBeforeDownload : {0}", LoginWrapper.Instance.LimitFileCountOnBeforeDownload);
			Console.WriteLine("CheckUpdateNeededFile : {0}", LoginWrapper.Instance.CheckUpdateNeededFile);

			Console.WriteLine("UpdateSkipFolders List");
			foreach (string fi in LoginWrapper.Instance.UpdateNeededFiles)
				Console.WriteLine("\tName : {0}", fi);
			#endregion

			#region Loader
			Console.WriteLine("\nLoader Configuration\n");

			Console.WriteLine("PlantId : {0}", LoaderWrapper.Instance.PlantId);
			Console.WriteLine("UserId : {0}", LoaderWrapper.Instance.UserId);
			Console.WriteLine("EnableMessageService : {0}", LoaderWrapper.Instance.EnableMessageService);
			Console.WriteLine("DebugMode : {0}", LoaderWrapper.Instance.DebugMode);
			Console.WriteLine("CheckServerInterval : {0}", LoaderWrapper.Instance.CheckServerInterval);
			#endregion
		}
	}
}
