﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler.ServerChanger;
using iDASiT.FX.Win.Configuration.Handler.AutoServerSwitcher;

namespace iDASiT.FX.Win.Configuration.Handler
{
	class ServerChangerTestDriver
	{
		static void Main(string[] args)
		{
			System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			Sections group = config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new Sections();

				config.SectionGroups.Add(Sections.SectionGroupNameString, group);
			}

			#region ServerChanger
			ServerChangerSection serverChangerSection = group.ServerChanger;

			if (serverChangerSection == null)
			{
				serverChangerSection = new ServerChangerSection();
				group.Sections.Add(ServerChangerSection.SectionNameString, serverChangerSection);
			}

			LoginInfo loginInfo = serverChangerSection.LoginInfo;

			if (loginInfo == null)
				// Login 정보
				serverChangerSection.LoginInfo = new LoginInfo("DCCPCS1", "DEV");

			// Server 정보
			ServerCollection serverInfos = serverChangerSection.Servers;

			if (serverInfos == null || serverInfos.Count == 0)
			{
				serverInfos = new ServerCollection();
				serverChangerSection.Servers = serverInfos;

				serverInfos.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				serverInfos.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				serverChangerSection.Servers.Pattern = @"http\:\/\/[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}[\:]{0,1}[\d]{0,6}";
				serverChangerSection.Servers.Default = "TestServer";
			}

			// Target Config File
			FileInfoCollection targetFiles = serverChangerSection.TargetConfigFiles;

			if (targetFiles == null || targetFiles.Count == 0)
			{
				//targetFiles = new GenericCollection<FileInfo>();
				targetFiles = new FileInfoCollection();
				serverChangerSection.TargetConfigFiles = targetFiles;

				targetFiles.Add(new FileInfo("iDASiT.FX.Win.Login.exe.config"));
				targetFiles.Add(new FileInfo("iDASiT.FX.Win.Updater.exe.config"));
				targetFiles.Add(new FileInfo("iDASiT.FX.Win.Loader.exe.config"));
				targetFiles.Add(new FileInfo("Utility.MenuConsole.exe.config"));
			}

			Console.WriteLine("\nServerChanger Configuration\n");
			
			Console.WriteLine(string.Format("PlantId : {0}\tUserId : {1}", loginInfo.PlantId, loginInfo.UserId));
			Console.WriteLine(string.Format("Default : {0}", serverChangerSection.Servers.Default));
			Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}",
				serverChangerSection.Servers[serverChangerSection.Servers.Default].Alias,
				serverChangerSection.Servers[serverChangerSection.Servers.Default].Url,
				serverChangerSection.Servers[serverChangerSection.Servers.Default].Port));
			Console.WriteLine(string.Format("Pattern : {0}", serverChangerSection.Servers.Pattern));

			Console.WriteLine("Server List");
			foreach (ServerInfo si in serverChangerSection.Servers)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", si.Alias, si.Url, si.Port));

			Console.WriteLine("TargetConfigFile List");
			foreach (FileInfo fi in targetFiles)
				Console.WriteLine(string.Format("\tName : {0}", fi.Name));
			#endregion

			#region UpdaterCore
			UpdaterCoreSection updaterCoreSection = group.UpdaterCore;

			if (updaterCoreSection == null)
			{
				updaterCoreSection = new UpdaterCoreSection();
				group.Sections.Add(UpdaterCoreSection.SectionNameString, updaterCoreSection);
			}

			// Server 정보
			UpdateServerCollection serverInfoUpdaterCore = updaterCoreSection.Servers;

			if (serverInfoUpdaterCore == null || serverInfoUpdaterCore.Count == 0)
			{
				serverInfoUpdaterCore = new UpdateServerCollection();
				updaterCoreSection.Servers = serverInfoUpdaterCore;

				serverInfoUpdaterCore.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				serverInfoUpdaterCore.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				updaterCoreSection.Servers.Default = "TestServer";
				updaterCoreSection.Servers.DeployFolder = "iDASiT.FX.Win.Deploy";
				updaterCoreSection.Servers.UpdateFile = "UpdateFile.xml";
				updaterCoreSection.Servers.BufferSize = 10240;
			}

			ConfigurationManager.RefreshSection(UpdaterCoreSection.SectionNameString);

			Console.WriteLine("\nUpdater Core Configuration\n");

			Console.WriteLine(string.Format("Default : {0}", updaterCoreSection.Servers.Default));
			Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}",
				updaterCoreSection.Servers[updaterCoreSection.Servers.Default].Alias,
				updaterCoreSection.Servers[updaterCoreSection.Servers.Default].Url,
				updaterCoreSection.Servers[updaterCoreSection.Servers.Default].Port));
			Console.WriteLine(string.Format("DeployFolder : {0}", updaterCoreSection.Servers.DeployFolder));
			Console.WriteLine(string.Format("UpdateFile : {0}", updaterCoreSection.Servers.UpdateFile));
			Console.WriteLine(string.Format("BufferSize : {0}", updaterCoreSection.Servers.BufferSize));


			Console.WriteLine("Server List");
			foreach (ServerInfo si in updaterCoreSection.Servers)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", si.Alias, si.Url, si.Port));
			#endregion

			#region Updater
			UpdaterSection updaterSection = group.Updater;

			if (updaterSection == null)
			{
				updaterSection = new UpdaterSection();
				group.Sections.Add(UpdaterSection.SectionNameString, updaterSection);

				updaterSection.ForceDownloadFrameworkWin = false;
				updaterSection.ForceDownloadFrameworkWinBaseDate = DateTime.Parse("2008-06-01 00:00:01");
				//updaterSection.ForceDownloadFolders.Add(new FileInfo(""));
				updaterSection.UpdateSkipFolders.Add(new FileInfo("iDoIt"));
				updaterSection.UpdateSkipFolders.Add(new FileInfo("Infragistics"));
				updaterSection.UpdateSkipFolders.Add(new FileInfo("Log"));
			}

			// Server 정보
			UpdateServerCollection serverInfoUpdater = updaterSection.Servers;

			if (serverInfoUpdater == null || serverInfoUpdater.Count == 0)
			{
				serverInfoUpdater = new UpdateServerCollection();
				updaterSection.Servers = serverInfoUpdater;

				serverInfoUpdater.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				serverInfoUpdater.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				updaterSection.Servers.Default = "TestServer";
				updaterSection.Servers.DeployFolder = "iDASiT.FX.Win.Deploy";
				updaterSection.Servers.UpdateFile = "UpdateFile.xml";
				updaterSection.Servers.BufferSize = 512000;
			}

			ConfigurationManager.RefreshSection(UpdaterSection.SectionNameString);

			Console.WriteLine("\nUpdater Configuration\n");

			Console.WriteLine(string.Format("Default : {0}", updaterSection.Servers.Default));
			Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}",
				updaterSection.Servers[updaterSection.Servers.Default].Alias,
				updaterSection.Servers[updaterSection.Servers.Default].Url,
				updaterSection.Servers[updaterSection.Servers.Default].Port));
			Console.WriteLine(string.Format("DeployFolder : {0}", updaterSection.Servers.DeployFolder));
			Console.WriteLine(string.Format("UpdateFile : {0}", updaterSection.Servers.UpdateFile));
			Console.WriteLine(string.Format("BufferSize : {0}", updaterSection.Servers.BufferSize));

			Console.WriteLine("Server List");
			foreach (ServerInfo si in updaterSection.Servers)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", si.Alias, si.Url, si.Port));

			Console.WriteLine(string.Format("ForceDownloadFrameworkWin : {0}", updaterSection.ForceDownloadFrameworkWin));
			Console.WriteLine(string.Format("ForceDownloadFrameworkWinBaseDate : {0}", updaterSection.ForceDownloadFrameworkWinBaseDate));

			Console.WriteLine("ForceDownloadFolders List");
			foreach (FileInfo fi in updaterSection.ForceDownloadFolders)
				Console.WriteLine(string.Format("\tName : {0}", fi.Name));

			Console.WriteLine("UpdateSkipFolders List");
			foreach (FileInfo fi in updaterSection.UpdateSkipFolders)
				Console.WriteLine(string.Format("\tName : {0}", fi.Name));
			#endregion

			#region Login
			LoginSection loginSection = group.Login;

			if (loginSection == null)
			{
				loginSection = new LoginSection();
				group.Sections.Add(LoginSection.SectionNameString, loginSection);

				loginSection.LimitFileCountOnBeforeDownload = 300;
				loginSection.CheckUpdateNeededFile = true;
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Server.dll"));
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.dll"));
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Configuration.dll"));
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Updater.exe"));
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Updater.exe.config"));
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Resources.dll"));
			}

			// Server 정보
			UpdateServerCollection serverInfoLogin = loginSection.Servers;

			if (serverInfoLogin == null || serverInfoLogin.Count == 0)
			{
				serverInfoLogin = new UpdateServerCollection();
				loginSection.Servers = serverInfoLogin;

				serverInfoLogin.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				serverInfoLogin.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				loginSection.Servers.Default = "TestServer";
				loginSection.Servers.DeployFolder = "iDASiT.FX.Win.Deploy";
				loginSection.Servers.UpdateFile = "UpdateFile.xml";
				loginSection.Servers.BufferSize = 512000;
			}

			ConfigurationManager.RefreshSection(LoginSection.SectionNameString);

			Console.WriteLine("\nLogin Configuration\n");

			Console.WriteLine(string.Format("Default : {0}", loginSection.Servers.Default));
			Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}",
				loginSection.Servers[loginSection.Servers.Default].Alias,
				loginSection.Servers[loginSection.Servers.Default].Url,
				loginSection.Servers[loginSection.Servers.Default].Port));
			Console.WriteLine(string.Format("DeployFolder : {0}", loginSection.Servers.DeployFolder));
			Console.WriteLine(string.Format("UpdateFile : {0}", loginSection.Servers.UpdateFile));
			Console.WriteLine(string.Format("BufferSize : {0}", loginSection.Servers.BufferSize));

			Console.WriteLine("Server List");
			foreach (ServerInfo si in loginSection.Servers)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", si.Alias, si.Url, si.Port));

			Console.WriteLine(string.Format("LimitFileCountOnBeforeDownload : {0}", loginSection.LimitFileCountOnBeforeDownload));
			Console.WriteLine(string.Format("CheckUpdateNeededFile : {0}", loginSection.CheckUpdateNeededFile));

			Console.WriteLine("UpdateSkipFolders List");
			foreach (FileInfo fi in loginSection.UpdateNeededFiles)
				Console.WriteLine(string.Format("\tName : {0}", fi.Name));
			#endregion

			#region AutoServerSwitcher
			AutoServerSwitcherSection autoServerSwitcherSection = group.AutoServerSwitcher;

			if (autoServerSwitcherSection == null)
			{
				autoServerSwitcherSection = new AutoServerSwitcherSection();
				group.Sections.Add(AutoServerSwitcherSection.SectionNameString, autoServerSwitcherSection);
			}

			// Server 정보
			ServerInfoCollection servers = autoServerSwitcherSection.Servers;

			if (servers == null || servers.Count == 0)
			{
				servers = new ServerInfoCollection();
				autoServerSwitcherSection.Servers = servers;

				servers.Add(new ServerInfo("MES(Web) Server", "http://192.168.25.246", 8088));
				servers.Add(new ServerInfo("BO Server", "http://192.168.25.28", 8088));

				// Default Server 정보
				autoServerSwitcherSection.Servers.Default = "MES(Web) Server";
			}

			// ServerChanger Target Config File
			ServerChangerFileCollection serverChangerFiles = autoServerSwitcherSection.ServerChangerFiles;

			if (serverChangerFiles == null || serverChangerFiles.Count == 0)
			{
				serverChangerFiles = new ServerChangerFileCollection();
				autoServerSwitcherSection.ServerChangerFiles = serverChangerFiles;

				serverChangerFiles.Pattern = @"http\:\/\/[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}[\:]{0,1}[\d]{0,6}";
				serverChangerFiles.Add(new FileInfo("iDASiT.FX.Win.Login.exe.config"));
				serverChangerFiles.Add(new FileInfo("iDASiT.FX.Win.Updater.exe.config"));
				serverChangerFiles.Add(new FileInfo("iDASiT.FX.Win.Loader.exe.config"));
				serverChangerFiles.Add(new FileInfo("Utility.MenuConsole.exe.config"));
			}

			// Updater needed file
			UpdaterFileCollection updaterFiles = autoServerSwitcherSection.UpdaterFiles;

			if (updaterFiles == null || updaterFiles.Count == 0)
			{
				updaterFiles.CheckUpdateNeededFile = true;
				updaterFiles.DeployFolder = "iDASiT.FX.Win.Deploy";
				updaterFiles.UpdateFile = "UpdateFile.xml";
				updaterFiles.BufferSize = 512000;
				updaterFiles.Add(new FileInfo("iDASiT.FX.Win.Server.dll"));
				updaterFiles.Add(new FileInfo("iDASiT.FX.Win.Login.exe"));
				updaterFiles.Add(new FileInfo("iDASiT.FX.Win.Login.exe.config"));
				updaterFiles.Add(new FileInfo("iDASiT.FX.Win.dll"));
				updaterFiles.Add(new FileInfo("iDASiT.FX.Win.Configuration.dll"));
			}

			Console.WriteLine("\nAutoServerSwitcher Configuration\n");

			Console.WriteLine(string.Format("Default : {0}", autoServerSwitcherSection.Servers.Default));
			Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}",
				autoServerSwitcherSection.Servers[autoServerSwitcherSection.Servers.Default].Alias,
				autoServerSwitcherSection.Servers[autoServerSwitcherSection.Servers.Default].Url,
				autoServerSwitcherSection.Servers[autoServerSwitcherSection.Servers.Default].Port));

			Console.WriteLine("Server List");
			foreach (ServerInfo si in autoServerSwitcherSection.Servers)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", si.Alias, si.Url, si.Port));

			Console.WriteLine("ServerChangerConfigFile List");
			Console.WriteLine("Pattern : {0}", autoServerSwitcherSection.ServerChangerFiles.Pattern);
			foreach (FileInfo fi in serverChangerFiles)
				Console.WriteLine(string.Format("\tName : {0}", fi.Name));

			Console.WriteLine("UpdaterFile List");
			Console.WriteLine(string.Format("CheckUpdateNeededFile : {0}", updaterFiles.CheckUpdateNeededFile));
			Console.WriteLine(string.Format("DeployFolder : {0}", updaterFiles.DeployFolder));
			Console.WriteLine(string.Format("UpdateFile : {0}", updaterFiles.UpdateFile));
			Console.WriteLine(string.Format("BufferSize : {0}", updaterFiles.BufferSize));
			foreach (FileInfo fi in updaterFiles)
				Console.WriteLine(string.Format("\tName : {0}", fi.Name));
			#endregion

			config.Save(ConfigurationSaveMode.Modified);
		}
	}
}
