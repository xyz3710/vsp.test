﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.Framework.Win.Configuration.Handler.ServerChanger;

namespace iDASiT.Framework.Win.Configuration.Handler
{
	class ServerChangerTestDriver
	{
		static void Main(string[] args)
		{
			System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			SectionGroup group = config.SectionGroups[SectionGroup.SectionGroupNameString] as SectionGroup;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new SectionGroup();

				config.SectionGroups.Add(SectionGroup.SectionGroupNameString, group);
			}

			ServerChangerSection serverChangerSection = group.ServerChanger;

			if (serverChangerSection == null)
			{
				serverChangerSection = new ServerChangerSection();
				group.Sections.Add(ServerChangerSection.SectionNameString, serverChangerSection);
			}

			LoginInfo loginInfo = serverChangerSection.LoginInfo;

			if (loginInfo == null)
				// Login 정보
				serverChangerSection.LoginInfo = new LoginInfo("DCCPCS1", "DEV");

			// Server 정보
			ServerCollection serverInfos = serverChangerSection.Servers;

			if (serverInfos == null || serverInfos.Count == 0)
			{
				serverInfos = new ServerCollection();
				serverChangerSection.Servers = serverInfos;

				serverInfos.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				serverInfos.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				serverChangerSection.Servers.Pattern = @"http\:\/\/[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}[\:]{0,1}[\d]{0,6}";
				serverChangerSection.Servers.Default = "TestServer";
			}

			// Target Config File
			FileInfoCollection targetFiles = serverChangerSection.TargetConfigFiles;

			if (targetFiles == null || targetFiles.Count == 0)
			{
				//targetFiles = new GenericCollection<FileInfo>();
				targetFiles = new FileInfoCollection();
				serverChangerSection.TargetConfigFiles = targetFiles;

				targetFiles.Add(new FileInfo("iDASiT.Framework.Win.Login.exe.config"));
				targetFiles.Add(new FileInfo("iDASiT.Framework.Win.Updater.exe.config"));
				targetFiles.Add(new FileInfo("iDASiT.Framework.Win.Loader.exe.config"));
				targetFiles.Add(new FileInfo("Utility.MenuConsole.exe.config"));
			}

			config.Save();


			Console.WriteLine("\nWith Wrapper\n");

			ServerChangerWrapper scw = ServerChangerWrapper.GetInstance();

			scw.PlantId = "동철이";
			scw.ServerAdd("BO", "http://192.168.25.247", 8088);
			scw.ServerRemove("RealServer");
			scw.TargetFileRemove("iDASiT.Framework.Win.Updater.exe.config");
			scw.SaveModifed();

			Console.WriteLine(string.Format("PlantId : {0}\tUserId : {1}", scw.PlantId, scw.UserId));
			Console.WriteLine(string.Format("Default : {0}", scw.Default));
			Console.WriteLine(string.Format("\tUrl : {0}", scw.Servers[scw.Default]));
			Console.WriteLine(string.Format("Pattern : {0}", scw.Pattern));

			Console.WriteLine("Server List");
			foreach (string url in scw.Servers.Keys)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}", url,scw.Servers[url]));

			Console.WriteLine("TargetConfigFile List");
			foreach (string filename in scw.TargetFilenames)
				Console.WriteLine(string.Format("\tName : {0}", filename));


			ConfigurationManager.RefreshSection(ServerChangerSection.SectionNameString);

			Console.WriteLine("\nWith Configuration\n");

			Console.WriteLine(string.Format("PlantId : {0}\tUserId : {1}", loginInfo.PlantId, loginInfo.UserId));
			Console.WriteLine(string.Format("Default : {0}", serverChangerSection.Servers.Default));
			Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", 
				serverChangerSection.Servers[serverChangerSection.Servers.Default].Alias, 
				serverChangerSection.Servers[serverChangerSection.Servers.Default].Url, 
				serverChangerSection.Servers[serverChangerSection.Servers.Default].Port));
			Console.WriteLine(string.Format("Pattern : {0}", serverChangerSection.Servers.Pattern));

			Console.WriteLine("Server List");
			foreach (ServerInfo si in serverChangerSection.Servers)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", si.Alias, si.Url, si.Port));

			Console.WriteLine("TargetConfigFile List");
			foreach (FileInfo fi in targetFiles)
				Console.WriteLine(string.Format("\tName : {0}", fi.Name));
		}
	}
}
