﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace iDASiT.Framework.Win.Configuration.Handler
{
	class UpdaterCoreTestDriver
	{
		static void Main(string[] args)
		{
			System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			SectionGroup group = config.SectionGroups[SectionGroup.SectionGroupNameString] as SectionGroup;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new SectionGroup();

				config.SectionGroups.Add(SectionGroup.SectionGroupNameString, group);
			}

			UpdaterCoreSection updaterCoreSection = group.UpdaterCore;

			if (updaterCoreSection == null)
			{
				updaterCoreSection = new UpdaterCoreSection();
				group.Sections.Add(UpdaterCoreSection.SectionNameString, updaterCoreSection);
			}

			// Server 정보
			UpdateServerCollection serverInfos = updaterCoreSection.Servers;

			if (serverInfos == null || serverInfos.Count == 0)
			{
				serverInfos = new UpdateServerCollection();
				updaterCoreSection.Servers = serverInfos;

				serverInfos.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				serverInfos.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				updaterCoreSection.Servers.Default = "TestServer";
				updaterCoreSection.Servers.DeployFolder = "iDASiT.Framework.Win.Deploy";
				updaterCoreSection.Servers.UpdateFile = "UpdateFile.xml";
				updaterCoreSection.Servers.BufferSize = 10240;
			}

			config.Save();


			Console.WriteLine("\nWith Wrapper\n");

			UpdaterCoreWrapper ucw = UpdaterCoreWrapper.GetInstance();

			ConfigurationManager.RefreshSection(UpdaterCoreSection.SectionNameString);

			ucw.DeployFolder = "iDASiT.Framework.Win.Updater";
			ucw.UpdateFile = "Updater.xml";
			ucw.BufferSize = 512;
			ucw.ServerAdd("BO", "http://192.168.25.247", 8088);
			ucw.ServerRemove("RealServer");
			ucw.Save();

			Console.WriteLine(string.Format("Default : {0}", ucw.Default));
			Console.WriteLine(string.Format("\tUrl : {0}", ucw.Servers[ucw.Default]));
			Console.WriteLine(string.Format("DeployFolder : {0}", ucw.DeployFolder));
			Console.WriteLine(string.Format("UpdateFile : {0}", ucw.UpdateFile));
			Console.WriteLine(string.Format("BufferSize : {0}", ucw.BufferSize));

			Console.WriteLine("Server List");
			foreach (string url in ucw.Servers.Keys)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}", url,ucw.Servers[url]));


			ConfigurationManager.RefreshSection(UpdaterCoreSection.SectionNameString);

			Console.WriteLine("\nWith Configuration\n");

			Console.WriteLine(string.Format("Default : {0}", updaterCoreSection.Servers.Default));
			Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", 
				updaterCoreSection.Servers[updaterCoreSection.Servers.Default].Alias, 
				updaterCoreSection.Servers[updaterCoreSection.Servers.Default].Url, 
				updaterCoreSection.Servers[updaterCoreSection.Servers.Default].Port));
			Console.WriteLine(string.Format("DeployFolder : {0}", updaterCoreSection.Servers.DeployFolder));
			Console.WriteLine(string.Format("UpdateFile : {0}", updaterCoreSection.Servers.UpdateFile));
			Console.WriteLine(string.Format("BufferSize : {0}", updaterCoreSection.Servers.BufferSize));


			Console.WriteLine("Server List");
			foreach (ServerInfo si in updaterCoreSection.Servers)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", si.Alias, si.Url, si.Port));
		}
	}
}
