﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace iDASiT.Framework.Win.Configuration.Handler
{
	class LoginTestDriver
	{
		static void Main(string[] args)
		{
			System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			SectionGroup group = config.SectionGroups[SectionGroup.SectionGroupNameString] as SectionGroup;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new SectionGroup();

				config.SectionGroups.Add(SectionGroup.SectionGroupNameString, group);
			}

			LoginSection loginSection = group.Login;

			if (loginSection == null)
			{
				loginSection = new LoginSection();
				group.Sections.Add(LoginSection.SectionNameString, loginSection);

				loginSection.LimitFileCountOnBeforeDownload = 300;
				loginSection.CheckUpdateNeededFile = true;
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.Framework.Win.Server.dll"));
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.Framework.Win.dll"));
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.Framework.Win.Configuration.dll"));
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.Framework.Win.Updater.exe"));
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.Framework.Win.Updater.exe.config"));
				loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.Framework.Win.Resources.dll"));
			}

			// Server 정보
			UpdateServerCollection serverInfos = loginSection.Servers;

			if (serverInfos == null || serverInfos.Count == 0)
			{
				serverInfos = new UpdateServerCollection();
				loginSection.Servers = serverInfos;

				serverInfos.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				serverInfos.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				loginSection.Servers.Default = "TestServer";
				loginSection.Servers.DeployFolder = "iDASiT.Framework.Win.Deploy";
				loginSection.Servers.UpdateFile = "UpdateFile.xml";
				loginSection.Servers.BufferSize = 512000;
			}

			config.Save();


			Console.WriteLine("\nWith Wrapper\n");

			LoginWrapper uw = LoginWrapper.GetInstance();

			ConfigurationManager.RefreshSection(LoginSection.SectionNameString);

			uw.DeployFolder = "iDASiT.Framework.Win.Updater";
			uw.UpdateFile = "Updater.xml";
			uw.BufferSize = 512;
			uw.ServerAdd("BO", "http://192.168.25.247", 8088);
			uw.ServerRemove("RealServer");
			uw.Save();

			Console.WriteLine(string.Format("Default : {0}", uw.Default));
			Console.WriteLine(string.Format("\tUrl : {0}", uw.Servers[uw.Default]));
			Console.WriteLine(string.Format("DeployFolder : {0}", uw.DeployFolder));
			Console.WriteLine(string.Format("UpdateFile : {0}", uw.UpdateFile));
			Console.WriteLine(string.Format("BufferSize : {0}", uw.BufferSize));

			Console.WriteLine("Server List");
			foreach (string url in uw.Servers.Keys)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}", url,uw.Servers[url]));

			Console.WriteLine(string.Format("LimitFileCountOnBeforeDownload : {0}", uw.LimitFileCountOnBeforeDownload));
			Console.WriteLine(string.Format("CheckUpdateNeededFile : {0}", uw.CheckUpdateNeededFile));

			Console.WriteLine("UpdateSkipFolders List");
			foreach (string fi in uw.UpdateNeededFiles)
				Console.WriteLine(string.Format("\tName : {0}", fi));


			ConfigurationManager.RefreshSection(LoginSection.SectionNameString);

			Console.WriteLine("\nWith Configuration\n");

			Console.WriteLine(string.Format("Default : {0}", loginSection.Servers.Default));
			Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", 
				loginSection.Servers[loginSection.Servers.Default].Alias, 
				loginSection.Servers[loginSection.Servers.Default].Url, 
				loginSection.Servers[loginSection.Servers.Default].Port));
			Console.WriteLine(string.Format("DeployFolder : {0}", loginSection.Servers.DeployFolder));
			Console.WriteLine(string.Format("UpdateFile : {0}", loginSection.Servers.UpdateFile));
			Console.WriteLine(string.Format("BufferSize : {0}", loginSection.Servers.BufferSize));
			
			Console.WriteLine("Server List");
			foreach (ServerInfo si in loginSection.Servers)
				Console.WriteLine(string.Format("\tAlias : {0}\tUrl : {1}\tPort : {2}", si.Alias, si.Url, si.Port));

			Console.WriteLine(string.Format("LimitFileCountOnBeforeDownload : {0}", loginSection.LimitFileCountOnBeforeDownload));
			Console.WriteLine(string.Format("CheckUpdateNeededFile : {0}", loginSection.CheckUpdateNeededFile));

			Console.WriteLine("UpdateSkipFolders List");
			foreach (FileInfo fi in loginSection.UpdateNeededFiles)
				Console.WriteLine(string.Format("\tName : {0}", fi.Name));
		}
	}
}
