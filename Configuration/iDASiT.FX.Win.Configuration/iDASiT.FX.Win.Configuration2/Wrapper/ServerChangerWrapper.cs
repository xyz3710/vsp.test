﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.ServerChangerWrapper
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 11일 수요일 오후 3:28
/*	Purpose		:	ServerChanger의 설정 파일에 관련된 Wrapper 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler;
using System.Text.RegularExpressions;
using iDASiT.FX.Win.Configuration.Handler.ServerChanger;

namespace iDASiT.FX.Win.Configuration
{
	/// <summary>
	/// ServerChanger의 설정 파일에 관련된 Wrapper 클래스 입니다.
	/// </summary>
	public class ServerChangerWrapper
	{
		#region Fields
		private LoginInfo _loginInfo;
		private ServerChangerSection _serverChangerSection;
		private ServerCollection _serverInfos;
		private FileInfoCollection _targetFiles;
		private Dictionary<string, string> _servers;
		private List<string> _targetFilenames;
		#endregion

        #region Use Singleton Pattern
        private static ServerChangerWrapper _newInstance;
		private static System.Configuration.Configuration _config;
        
        #region Constructor for Single Ton Pattern
        /// <summary>
        /// ServerChangerWrapper class의 Single Ton Pattern을 위한 생성자 입니다.
        /// </summary>
        /// <returns></returns>
        private ServerChangerWrapper(string exePath)
        {
        	_newInstance = null;
        	
			if (string.IsNullOrEmpty(exePath) == true)
				_config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			else
				_config = ConfigurationManager.OpenExeConfiguration(exePath);

			Sections group = _config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new Sections();

				_config.SectionGroups.Add(Sections.SectionGroupNameString, group);
			}

			_serverChangerSection = group.ServerChanger;

			if (_serverChangerSection == null)
			{
				_serverChangerSection = new ServerChangerSection();
				group.Sections.Add(ServerChangerSection.SectionNameString, _serverChangerSection);
			}

			_loginInfo = _serverChangerSection.LoginInfo;

			if (_loginInfo == null)
				// Login 정보
				_serverChangerSection.LoginInfo = new LoginInfo("DCCPCS1", "DEV");

			// Server 정보
			_serverInfos = _serverChangerSection.Servers;

			if (_serverInfos == null || _serverInfos.Count == 0)
			{
				_serverInfos = new ServerCollection();
				_serverChangerSection.Servers = _serverInfos;

				_serverInfos.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				_serverInfos.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				_serverChangerSection.Servers.Default = "TestServer";
				_serverChangerSection.Servers.Pattern = @"http\:\/\/[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}[\:]{0,1}[\d]{0,6}";
			}

			// Target Config File
			_targetFiles = _serverChangerSection.TargetConfigFiles;

			if (_targetFiles == null || _targetFiles.Count == 0)
			{
				_targetFiles = new FileInfoCollection();
				_serverChangerSection.TargetConfigFiles = _targetFiles;

				_targetFiles.Add(new FileInfo("iDASiT.FX.Win.Login.exe.config"));
				_targetFiles.Add(new FileInfo("iDASiT.FX.Win.Updater.exe.config"));
				_targetFiles.Add(new FileInfo("iDASiT.FX.Win.Loader.exe.config"));
				_targetFiles.Add(new FileInfo("Utility.MenuConsole.exe.config"));
			}

			_config.Save(ConfigurationSaveMode.Modified);
        }
        #endregion 
        
        #region GetInstance
		/// <summary>
		/// ServerChangerWrapper class의 Single Ton Pattern을 위한 새 인스턴스를 초기화 합니다.
		/// 
		/// </summary>
		/// <returns></returns>
		public static ServerChangerWrapper GetInstance()
		{
			string exePath = String.Empty;

			return GetInstance(exePath);
		}
        
		/// <summary>
        /// ServerChangerWrapper class의 Single Ton Pattern을 위한 새 인스턴스를 초기화 합니다.
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.</param>
		/// </summary>
        /// <returns></returns>
		public static ServerChangerWrapper GetInstance(string exePath)
        {
        	if (_newInstance == null)
				_newInstance = new ServerChangerWrapper(exePath);
        
        	return _newInstance;
        }
        #endregion

        #endregion
                
		#region Properties
		/// <summary>
		/// PlantId를 구하거나 설정합니다.
		/// </summary>
		public string PlantId
		{
			get
			{
				return _loginInfo.PlantId;
			}
			set
			{
				_loginInfo.PlantId = value;
			}
		}
        
        /// <summary>
        /// UserId를 구하거나 설정합니다.
        /// </summary>
        public string UserId
        {
        	get
        	{
				return _loginInfo.UserId;
        	}
        	set
        	{
				_loginInfo.UserId = value;
        	}
        }
        
        /// <summary>
        /// Pattern를 구하거나 설정합니다.
        /// </summary>
        public string Pattern
        {
        	get
        	{
				return _serverChangerSection.Servers.Pattern;
        	}
        	set
        	{
				_serverChangerSection.Servers.Pattern = value;
        	}
        }
        
        /// <summary>
        /// Default Server의 Alias를 구하거나 설정합니다.
        /// </summary>
        public string Default
        {
        	get
        	{
				return _serverChangerSection.Servers.Default;
        	}
        	set
        	{
				_serverChangerSection.Servers.Default = value;
        	}
        }
        
        /// <summary>
        /// Server를 구하거나 설정합니다.<br/>
		/// <remarks>Server를 추가나 삭제 모두 제거는 <seealso cref="ServerAdd"/>, <seealso cref="ServerRemove"/>, <seealso cref="ServerClear"/> 메서드를 사용하십시오.</remarks>
        /// </summary>
        public Dictionary<string, string> Servers
        {
        	get
        	{
				if (_servers == null)
				{
					_servers = new Dictionary<string, string>();

					foreach (ServerInfo si in _serverInfos)
						_servers.Add(si.Alias, string.Format("{0}:{1}", si.Url, si.Port));
				}
                
				return _servers;
        	}
        	set
        	{
				if (value.Count > 0)
				{
					_serverInfos.Clear();

					foreach (string alias in value.Keys)
					{
						string url = alias;						
						int port = 80;

						Regex regex = new Regex(@"(?<Url>http\:\/\/[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})[\:]{0,1}(?<Port>[\d]{0,6})", RegexOptions.Compiled);

						if (regex.Match(alias).Success == true)
						{
							url = regex.Match(alias).Groups["Url"].Value;
							int.TryParse(regex.Match(alias).Groups["Port"].Value, out port);
						}

						_serverInfos.Add(new ServerInfo(alias, url, port));
					}

					_servers = value;
				}				
        	}
        }
        
        /// <summary>
        /// TargetFilename을 구하거나 설정합니다.
		/// <remarks>TargetFilename의 추가나 삭제 모두 제거는 <seealso cref="TargetFileAdd"/>, <seealso cref="TargetFileRemove"/>, <seealso cref="TargetFileClear"/> 메서드를 사용하십시오.</remarks>
		/// </summary>
        public List<string> TargetFilenames
        {
        	get
        	{
				if (_targetFilenames == null)
				{
					_targetFilenames = new List<string>();

					foreach (FileInfo fi in _targetFiles)
						_targetFilenames.Add(fi.Name);
				}
                
				return _targetFilenames;
        	}
        	set
        	{
        		if (value.Count > 0)
				{
					_targetFiles.Clear();

					foreach (string filename in value)
						_targetFiles.Add(new FileInfo(filename));

					_targetFilenames = value;
				}
        	}
        }        
        #endregion

		/// <summary>
		/// Target Server를 추가합니다.
		/// </summary>
		/// <param name="alias">추가할 Server alias(ex : Real)</param>
		/// <param name="url">추가할 Server service Url(ex : http://127.0.0.1)</param>
		/// <param name="port">추가할 Server serivce port(Option)</param>
		public void ServerAdd(string alias, string url, int port)
		{
			_serverInfos.Add(new ServerInfo(alias, url, port));
		}

		/// <summary>
		/// Target Server를 제거합니다.
		/// </summary>
		/// <param name="alias">Server alias(ex : Real)</param>
		public void ServerRemove(string alias)
		{
			_serverInfos.Remove(alias);
		}

		/// <summary>
		/// Target Server 목록을 모두 제거 합니다.
		/// </summary>
		public void ServerClear()
		{
			_serverInfos.Clear();
		}

		/// <summary>
		/// 대상 Filename을 추가합니다.
		/// </summary>
		/// <param name="filename">추가할 filename</param>
		public void TargetFileAdd(string filename)
		{
			_targetFiles.Add(new FileInfo(filename));
		}

		/// <summary>
		/// 대상 Filename을 제거합니다.
		/// </summary>
		/// <param name="filename">제거할 filename</param>
		public void TargetFileRemove(string filename)
		{
			_targetFiles.Remove(filename);
		}

		/// <summary>
		/// 대상 Filename 목록을 모두 제거 합니다.
		/// </summary>
		public void TargetFileClear()
		{
			_targetFiles.Clear();
		}

		/// <summary>
		/// Config 파일에 변경 사항만을 저장합니다.
		/// </summary>		
		public void SaveModifed()
		{
			_config.Save(ConfigurationSaveMode.Modified);
		}

		/// <summary>
		/// Config 파일에 저장 합니다.
		/// </summary>
		public void Save()
		{
			_config.Save();
		}
	}
}
