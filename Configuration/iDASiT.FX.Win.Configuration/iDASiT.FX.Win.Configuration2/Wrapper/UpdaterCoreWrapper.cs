﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.UpdaterCoreWrapper
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 11일 수요일 오후 3:28
/*	Purpose		:	Updater Core의 설정 파일에 관련된 Wrapper 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler;
using System.Text.RegularExpressions;

namespace iDASiT.FX.Win.Configuration
{
	/// <summary>
	/// Updater Core의 설정 파일에 관련된 Wrapper 클래스 입니다.
	/// </summary>
	public class UpdaterCoreWrapper
	{
		#region Fields
		private UpdaterCoreSection _updaterCoreSection;
		private UpdateServerCollection _serverInfos;
		private Dictionary<string, string> _servers;
		#endregion

		#region Use Singleton UpdaterWrapper
		private static UpdaterCoreWrapper _newInstance;
		private static System.Configuration.Configuration _config;

		#region Constructor for Single Ton UpdaterWrapper
		/// <summary>
        /// UpdaterCoreWrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
        /// </summary>
        /// <returns></returns>
        private UpdaterCoreWrapper(string exePath)
        {
        	_newInstance = null;
        	
			if (string.IsNullOrEmpty(exePath) == true)
				_config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			else
				_config = ConfigurationManager.OpenExeConfiguration(exePath);

			Sections group = _config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new Sections();

				_config.SectionGroups.Add(Sections.SectionGroupNameString, group);
			}

			_updaterCoreSection = group.UpdaterCore;

			if (_updaterCoreSection == null)
			{
				_updaterCoreSection = new UpdaterCoreSection();
				group.Sections.Add(UpdaterCoreSection.SectionNameString, _updaterCoreSection);
			}

			// Server 정보
			_serverInfos = _updaterCoreSection.Servers;

			if (_serverInfos == null || _serverInfos.Count == 0)
			{
				_serverInfos = new UpdateServerCollection();
				_updaterCoreSection.Servers = _serverInfos;

				_serverInfos.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				_serverInfos.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				_updaterCoreSection.Servers.Default = "TestServer";
				_updaterCoreSection.Servers.DeployFolder = "iDASiT.FX.Win.Deploy";
				_updaterCoreSection.Servers.UpdateFile = "UpdateFile.xml";
				_updaterCoreSection.Servers.BufferSize = 512000;
			}

			_config.Save(ConfigurationSaveMode.Modified);
        }
        #endregion 
        
        #region GetInstance
		/// <summary>
		/// UpdaterCoreWrapper class의 Single Ton BufferSize을 위한 새 인스턴스를 초기화 합니다.
		/// 
		/// </summary>
		/// <returns></returns>
		public static UpdaterCoreWrapper GetInstance()
		{
			string exePath = String.Empty;

			return GetInstance(exePath);
		}
        
		/// <summary>
        /// UpdaterCoreWrapper class의 Single Ton BufferSize을 위한 새 인스턴스를 초기화 합니다.
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.</param>
		/// </summary>
        /// <returns></returns>
		public static UpdaterCoreWrapper GetInstance(string exePath)
        {
        	if (_newInstance == null)
				_newInstance = new UpdaterCoreWrapper(exePath);
        
        	return _newInstance;
        }
        #endregion

        #endregion
                
		#region Properties
		/// <summary>
		/// DeployFolder를 구하거나 설정합니다.
		/// </summary>
		public string DeployFolder
		{
			get
			{
				return _updaterCoreSection.Servers.DeployFolder;
			}
			set
			{
				_updaterCoreSection.Servers.DeployFolder = value;
			}
		}
        
        /// <summary>
        /// UpdateFile를 구하거나 설정합니다.
        /// </summary>
        public string UpdateFile
        {
        	get
        	{
				return _updaterCoreSection.Servers.UpdateFile;
        	}
        	set
        	{
				_updaterCoreSection.Servers.UpdateFile = value;
        	}
        }
        
        /// <summary>
        /// BufferSize를 구하거나 설정합니다.
        /// </summary>
        public int BufferSize
        {
        	get
        	{
				return _updaterCoreSection.Servers.BufferSize;
        	}
        	set
        	{
				_updaterCoreSection.Servers.BufferSize = value;
        	}
        }
        
        /// <summary>
        /// Default Server의 Alias를 구하거나 설정합니다.
        /// </summary>
        public string Default
        {
        	get
        	{
				return _updaterCoreSection.Servers.Default;
        	}
        	set
        	{
				_updaterCoreSection.Servers.Default = value;
        	}
        }
        
        /// <summary>
        /// Server를 구하거나 설정합니다.<br/>
		/// <remarks>Server를 추가나 삭제 모두 제거는 <seealso cref="ServerAdd"/>, <seealso cref="ServerRemove"/>, <seealso cref="ServerClear"/> 메서드를 사용하십시오.</remarks>
        /// </summary>
        public Dictionary<string, string> Servers
        {
        	get
        	{
				if (_servers == null)
				{
					_servers = new Dictionary<string, string>();

					foreach (ServerInfo si in _serverInfos)
						_servers.Add(si.Alias, string.Format("{0}:{1}", si.Url, si.Port));
				}
                
				return _servers;
        	}
        	set
        	{
				if (value.Count > 0)
				{
					_serverInfos.Clear();

					foreach (string alias in value.Keys)
					{
						string url = alias;						
						int port = 80;

						Regex regex = new Regex(@"(?<Url>http\:\/\/[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})[\:]{0,1}(?<Port>[\d]{0,6})", RegexOptions.Compiled);

						if (regex.Match(alias).Success == true)
						{
							url = regex.Match(alias).Groups["Url"].Value;
							int.TryParse(regex.Match(alias).Groups["Port"].Value, out port);
						}

						_serverInfos.Add(new ServerInfo(alias, url, port));
					}

					_servers = value;
				}				
        	}
        }
        #endregion

		/// <summary>
		/// Upate Server를 추가합니다.
		/// </summary>
		/// <param name="alias">추가할 Server alias(ex : Real)</param>
		/// <param name="url">추가할 Server service Url(ex : http://127.0.0.1)</param>
		/// <param name="port">추가할 Server serivce port(Option)</param>
		public void ServerAdd(string alias, string url, int port)
		{
			_serverInfos.Add(new ServerInfo(alias, url, port));
		}

		/// <summary>
		/// Upate Server를 제거합니다.
		/// </summary>
		/// <param name="alias">Server alias(ex : Real)</param>
		public void ServerRemove(string alias)
		{
			_serverInfos.Remove(alias);
		}

		/// <summary>
		/// Upate Server 목록을 모두 제거 합니다.
		/// </summary>
		public void ServerClear()
		{
			_serverInfos.Clear();
		}

		/// <summary>
		/// Config 파일에 변경 사항만을 저장합니다.
		/// </summary>		
		public void SaveModifed()
		{
			_config.Save(ConfigurationSaveMode.Modified);
		}

		/// <summary>
		/// Config 파일에 저장 합니다.
		/// </summary>
		public void Save()
		{
			_config.Save();
		}
	}
}
