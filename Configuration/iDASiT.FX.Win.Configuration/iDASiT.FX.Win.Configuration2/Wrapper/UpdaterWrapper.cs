﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.UpdaterWrapper
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 11일 수요일 오후 3:28
/*	Purpose		:	Updater의 설정 파일에 관련된 Wrapper 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler;
using System.Text.RegularExpressions;

namespace iDASiT.FX.Win.Configuration
{
	/// <summary>
	/// Updater의 설정 파일에 관련된 Wrapper 클래스 입니다.
	/// </summary>
	public class UpdaterWrapper
	{
		#region Fields
        private UpdaterSection _updaterSection;
		private UpdateServerCollection _serverInfos;
		private Dictionary<string, string> _servers;
		private FileInfoCollection _forceDownloadFolderCollection;
		private FileInfoCollection _updateSkipFolderCollection;
		private List<string> _forceDownloadFolders;
		private List<string> _updateSkipFolders;
		#endregion

		#region Use Singleton UpdaterWrapper
		private static UpdaterWrapper _newInstance;
		private static System.Configuration.Configuration _config;

		#region Constructor for Single Ton UpdaterWrapper
		/// <summary>
        /// UpdaterWrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
        /// </summary>
        /// <returns></returns>
        private UpdaterWrapper(string exePath)
        {
        	_newInstance = null;
        	
			if (string.IsNullOrEmpty(exePath) == true)
				_config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			else
				_config = ConfigurationManager.OpenExeConfiguration(exePath);

			Sections group = _config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new Sections();

				_config.SectionGroups.Add(Sections.SectionGroupNameString, group);
			}

			_updaterSection = group.Updater;

			if (_updaterSection == null)
			{
				_updaterSection = new UpdaterSection();
				group.Sections.Add(UpdaterSection.SectionNameString, _updaterSection);

				_updaterSection.ForceDownloadFrameworkWin = false;
				_updaterSection.ForceDownloadFrameworkWinBaseDate = DateTime.Parse("2008-06-01 00:00:01");
				//updaterSection.ForceDownloadFolders.Add(new FileInfo(""));
				_updaterSection.UpdateSkipFolders.Add(new FileInfo("iDoIt"));
				_updaterSection.UpdateSkipFolders.Add(new FileInfo("Infragistics"));
				_updaterSection.UpdateSkipFolders.Add(new FileInfo("Log"));
			}

			// Server 정보
			_serverInfos = _updaterSection.Servers;

			if (_serverInfos == null || _serverInfos.Count == 0)
			{
				_serverInfos = new UpdateServerCollection();
				_updaterSection.Servers = _serverInfos;

				_serverInfos.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				_serverInfos.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				_updaterSection.Servers.Default = "TestServer";
				_updaterSection.Servers.DeployFolder = "iDASiT.FX.Win.Deploy";
				_updaterSection.Servers.UpdateFile = "UpdateFile.xml";
				_updaterSection.Servers.BufferSize = 512000;
			}

			_forceDownloadFolderCollection = _updaterSection.ForceDownloadFolders;
			_updateSkipFolderCollection = _updaterSection.UpdateSkipFolders;

			_config.Save(ConfigurationSaveMode.Modified);
        }
        #endregion 
        
        #region GetInstance
		/// <summary>
		/// UpdaterWrapper class의 Single Ton BufferSize을 위한 새 인스턴스를 초기화 합니다.
		/// 
		/// </summary>
		/// <returns></returns>
		public static UpdaterWrapper GetInstance()
		{
			string exePath = String.Empty;

			return GetInstance(exePath);
		}
        
		/// <summary>
        /// UpdaterWrapper class의 Single Ton BufferSize을 위한 새 인스턴스를 초기화 합니다.
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.</param>
		/// </summary>
        /// <returns></returns>
		public static UpdaterWrapper GetInstance(string exePath)
        {
        	if (_newInstance == null)
				_newInstance = new UpdaterWrapper(exePath);
        
        	return _newInstance;
        }
        #endregion

        #endregion
                
		#region Properties
		/// <summary>
		/// DeployFolder를 구하거나 설정합니다.
		/// </summary>
		public string DeployFolder
		{
			get
			{
				return _updaterSection.Servers.DeployFolder;
			}
			set
			{
				_updaterSection.Servers.DeployFolder = value;
			}
		}
        
        /// <summary>
        /// UpdateFile를 구하거나 설정합니다.
        /// </summary>
        public string UpdateFile
        {
        	get
        	{
				return _updaterSection.Servers.UpdateFile;
        	}
        	set
        	{
				_updaterSection.Servers.UpdateFile = value;
        	}
        }
        
        /// <summary>
        /// BufferSize를 구하거나 설정합니다.
        /// </summary>
        public int BufferSize
        {
        	get
        	{
				return _updaterSection.Servers.BufferSize;
        	}
        	set
        	{
				_updaterSection.Servers.BufferSize = value;
        	}
        }
        
        /// <summary>
        /// Default Server의 Alias를 구하거나 설정합니다.
        /// </summary>
        public string Default
        {
        	get
        	{
				return _updaterSection.Servers.Default;
        	}
        	set
        	{
				_updaterSection.Servers.Default = value;
        	}
        }
        
        /// <summary>
        /// Server를 구하거나 설정합니다.<br/>
		/// <remarks>Server를 추가나 삭제 모두 제거는 <seealso cref="ServerAdd"/>, <seealso cref="ServerRemove"/>, <seealso cref="ServerClear"/> 메서드를 사용하십시오.</remarks>
        /// </summary>
        public Dictionary<string, string> Servers
        {
        	get
        	{
				if (_servers == null)
				{
					_servers = new Dictionary<string, string>();

					foreach (ServerInfo si in _serverInfos)
						_servers.Add(si.Alias, string.Format("{0}:{1}", si.Url, si.Port));
				}
                
				return _servers;
        	}
        	set
        	{
				if (value.Count > 0)
				{
					_serverInfos.Clear();

					foreach (string alias in value.Keys)
					{
						string url = alias;						
						int port = 80;

						Regex regex = new Regex(@"(?<Url>http\:\/\/[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})[\:]{0,1}(?<Port>[\d]{0,6})", RegexOptions.Compiled);

						if (regex.Match(alias).Success == true)
						{
							url = regex.Match(alias).Groups["Url"].Value;
							int.TryParse(regex.Match(alias).Groups["Port"].Value, out port);
						}

						_serverInfos.Add(new ServerInfo(alias, url, port));
					}

					_servers = value;
				}				
        	}
        }

		/// <summary>
		/// ForceDownloadFolder을 구하거나 설정합니다.
		/// <remarks>ForceDownloadFolder의 추가나 삭제 모두 제거는 <seealso cref="ForceDownloadFolderAdd"/>, <seealso cref="ForceDownloadFolderRemove"/>, <seealso cref="ForceDownloadFolderClear"/> 메서드를 사용하십시오.</remarks>
		/// </summary>
		public List<string> ForceDownloadFolders
		{
			get
			{
				if (_forceDownloadFolders == null)
				{
					_forceDownloadFolders = new List<string>();

					foreach (FileInfo fi in _forceDownloadFolderCollection)
						_forceDownloadFolders.Add(fi.Name);
				}

				return _forceDownloadFolders;
			}
			set
			{
				if (value.Count > 0)
				{
					_forceDownloadFolders.Clear();

					foreach (string filename in value)
						_forceDownloadFolderCollection.Add(new FileInfo(filename));

					_forceDownloadFolders = value;
				}
			}
		}     
   
		/// <summary>
		/// UpdateSkipFolder을 구하거나 설정합니다.
		/// <remarks>UpdateSkipFolder의 추가나 삭제 모두 제거는 <seealso cref="UpdateSkipFolderAdd"/>, <seealso cref="UpdateSkipFolderRemove"/>, <seealso cref="UpdateSkipFolderClear"/> 메서드를 사용하십시오.</remarks>
		/// </summary>
		public List<string> UpdateSkipFolders
		{
			get
			{
				if (_updateSkipFolders == null)
				{
					_updateSkipFolders = new List<string>();

					foreach (FileInfo fi in _updateSkipFolderCollection)
						_updateSkipFolders.Add(fi.Name);
				}

				return _updateSkipFolders;
			}
			set
			{
				if (value.Count > 0)
				{
					_updateSkipFolders.Clear();

					foreach (string filename in value)
						_updateSkipFolderCollection.Add(new FileInfo(filename));

					_updateSkipFolders = value;
				}
			}
		}
        #endregion

		/// <summary>
		/// Upate Server를 추가합니다.
		/// </summary>
		/// <param name="alias">추가할 Server alias(ex : Real)</param>
		/// <param name="url">추가할 Server service Url(ex : http://127.0.0.1)</param>
		/// <param name="port">추가할 Server serivce port(Option)</param>
		public void ServerAdd(string alias, string url, int port)
		{
			_serverInfos.Add(new ServerInfo(alias, url, port));
		}

		/// <summary>
		/// Upate Server를 제거합니다.
		/// </summary>
		/// <param name="alias">Server alias(ex : Real)</param>
		public void ServerRemove(string alias)
		{
			_serverInfos.Remove(alias);
		}

		/// <summary>
		/// Upate Server 목록을 모두 제거 합니다.
		/// </summary>
		public void ServerClear()
		{
			_serverInfos.Clear();
		}

		/// <summary>
		/// 대상 Filename을 추가합니다.
		/// </summary>
		/// <param name="filename">추가할 filename</param>
		public void ForceDownloadFolderAdd(string filename)
		{
			_forceDownloadFolderCollection.Add(new FileInfo(filename));
		}

		/// <summary>
		/// 대상 Filename을 제거합니다.
		/// </summary>
		/// <param name="filename">제거할 filename</param>
		public void ForceDownloadFolderRemove(string filename)
		{
			_forceDownloadFolderCollection.Remove(filename);
		}

		/// <summary>
		/// 대상 Filename 목록을 모두 제거 합니다.
		/// </summary>
		public void ForceDownloadFolderClear()
		{
			_forceDownloadFolderCollection.Clear();
		}

		/// <summary>
		/// 대상 Filename을 추가합니다.
		/// </summary>
		/// <param name="filename">추가할 filename</param>
		public void UpdateSkipFolderAdd(string filename)
		{
			_updateSkipFolderCollection.Add(new FileInfo(filename));
		}

		/// <summary>
		/// 대상 Filename을 제거합니다.
		/// </summary>
		/// <param name="filename">제거할 filename</param>
		public void UpdateSkipFolderRemove(string filename)
		{
			_updateSkipFolderCollection.Remove(filename);
		}

		/// <summary>
		/// 대상 Filename 목록을 모두 제거 합니다.
		/// </summary>
		public void UpdateSkipFolderClear()
		{
			_updateSkipFolderCollection.Clear();
		}

		/// <summary>
		/// Config 파일에 변경 사항만을 저장합니다.
		/// </summary>		
		public void SaveModifed()
		{
			_config.Save(ConfigurationSaveMode.Modified);
		}

		/// <summary>
		/// Config 파일에 저장 합니다.
		/// </summary>
		public void Save()
		{
			_config.Save();
		}
	}
}
