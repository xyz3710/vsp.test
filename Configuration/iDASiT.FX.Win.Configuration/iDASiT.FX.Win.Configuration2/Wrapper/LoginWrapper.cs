﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.LoginWrapper
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 11일 수요일 오후 3:28
/*	Purpose		:	Login의 설정 파일에 관련된 Wrapper 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using iDASiT.FX.Win.Configuration.Handler;
using System.Text.RegularExpressions;

namespace iDASiT.FX.Win.Configuration
{
	/// <summary>
	/// Login의 설정 파일에 관련된 Wrapper 클래스 입니다.
	/// </summary>
	public class LoginWrapper
	{
		#region Fields
        private LoginSection _loginSection;
		private UpdateServerCollection _serverInfos;
		private Dictionary<string, string> _servers;
		private FileInfoCollection _updateNeededFileCollection;
		private List<string> _updateNeededFiles;
		#endregion

		#region Use Singleton LoginWrapper
		private static LoginWrapper _newInstance;
		private static System.Configuration.Configuration _config;

		#region Constructor for Single Ton LoginWrapper
		/// <summary>
        /// LoginWrapper class의 Single Ton BufferSize을 위한 생성자 입니다.
        /// </summary>
        /// <returns></returns>
        private LoginWrapper(string exePath)
        {
        	_newInstance = null;
        	
			if (string.IsNullOrEmpty(exePath) == true)
				_config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			else
				_config = ConfigurationManager.OpenExeConfiguration(exePath);

			Sections group = _config.SectionGroups[Sections.SectionGroupNameString] as Sections;

			// SectionGroup 정보를 생성한다.
			if (group == null)
			{
				group = new Sections();

				_config.SectionGroups.Add(Sections.SectionGroupNameString, group);
			}

			_loginSection = group.Login;

			if (_loginSection == null)
			{
				_loginSection = new LoginSection();
				group.Sections.Add(LoginSection.SectionNameString, _loginSection);

				_loginSection.LimitFileCountOnBeforeDownload = 300;
				_loginSection.CheckUpdateNeededFile = true;
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Server.dll"));
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.dll"));
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Configuration.dll"));
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Updater.exe"));
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Updater.exe.config"));
				_loginSection.UpdateNeededFiles.Add(new FileInfo("iDASiT.FX.Win.Resources.dll"));
			}

			// Server 정보
			_serverInfos = _loginSection.Servers;

			if (_serverInfos == null || _serverInfos.Count == 0)
			{
				_serverInfos = new UpdateServerCollection();
				_loginSection.Servers = _serverInfos;

				_serverInfos.Add(new ServerInfo("RealServer", "http://192.168.25.246", 8088));
				_serverInfos.Add(new ServerInfo("TestServer", "http://192.168.25.28", 8088));

				// Default Server 정보
				_loginSection.Servers.Default = "TestServer";
				_loginSection.Servers.DeployFolder = "iDASiT.FX.Win.Deploy";
				_loginSection.Servers.UpdateFile = "UpdateFile.xml";
				_loginSection.Servers.BufferSize = 512000;
			}

			_updateNeededFileCollection = _loginSection.UpdateNeededFiles;

			_config.Save(ConfigurationSaveMode.Modified);
        }
        #endregion 
        
        #region GetInstance
		/// <summary>
		/// LoginWrapper class의 Single Ton BufferSize을 위한 새 인스턴스를 초기화 합니다.
		/// 
		/// </summary>
		/// <returns></returns>
		public static LoginWrapper GetInstance()
		{
			string exePath = String.Empty;

			return GetInstance(exePath);
		}
        
		/// <summary>
        /// LoginWrapper class의 Single Ton BufferSize을 위한 새 인스턴스를 초기화 합니다.
		/// <param name="exePath">실행 파일과 연결된 구성 파일에 대한 경로입니다.</param>
		/// </summary>
        /// <returns></returns>
		public static LoginWrapper GetInstance(string exePath)
        {
        	if (_newInstance == null)
				_newInstance = new LoginWrapper(exePath);
        
        	return _newInstance;
        }
        #endregion

        #endregion
                
		#region Properties
		/// <summary>
		/// DeployFolder를 구하거나 설정합니다.
		/// </summary>
		public string DeployFolder
		{
			get
			{
				return _loginSection.Servers.DeployFolder;
			}
			set
			{
				_loginSection.Servers.DeployFolder = value;
			}
		}
        
        /// <summary>
        /// UpdateFile를 구하거나 설정합니다.
        /// </summary>
        public string UpdateFile
        {
        	get
        	{
				return _loginSection.Servers.UpdateFile;
        	}
        	set
        	{
				_loginSection.Servers.UpdateFile = value;
        	}
        }
        
        /// <summary>
        /// BufferSize를 구하거나 설정합니다.
        /// </summary>
        public int BufferSize
        {
        	get
        	{
				return _loginSection.Servers.BufferSize;
        	}
        	set
        	{
				_loginSection.Servers.BufferSize = value;
        	}
        }
        
        /// <summary>
        /// Default Server의 Alias를 구하거나 설정합니다.
        /// </summary>
        public string Default
        {
        	get
        	{
				return _loginSection.Servers.Default;
        	}
        	set
        	{
				_loginSection.Servers.Default = value;
        	}
        }

		/// <summary>
		/// LimitFileCountOnBeforeDownload를 구하거나 설정합니다.<br/>
		/// Updater Download 전 최대 어셈블리 개수를 구하거나 설정 하여 해당 개수 이상이면 Win 폴더를 제거 하고 다시 받습니다.
		/// </summary>
		public int LimitFileCountOnBeforeDownload
		{
			get
			{
				return _loginSection.LimitFileCountOnBeforeDownload;
			}
			set
			{
				_loginSection.LimitFileCountOnBeforeDownload = value;
			}
		}

		/// <summary>
		/// Update 필요 파일의 검사여부를 구하거나 설정합니다.
		/// <remarks>false이면 <seealso cref="UpdateNeededFiles"/>를 검사하지 않습니다.</remarks>
		/// </summary>
		public bool CheckUpdateNeededFile
		{
			get
			{
				return _loginSection.CheckUpdateNeededFile;
			}
			set
			{
				_loginSection.CheckUpdateNeededFile = value;
			}
		}

        /// <summary>
        /// Server를 구하거나 설정합니다.<br/>
		/// <remarks>Server를 추가나 삭제 모두 제거는 <seealso cref="ServerAdd"/>, <seealso cref="ServerRemove"/>, <seealso cref="ServerClear"/> 메서드를 사용하십시오.</remarks>
        /// </summary>
        public Dictionary<string, string> Servers
        {
        	get
        	{
				if (_servers == null)
				{
					_servers = new Dictionary<string, string>();

					foreach (ServerInfo si in _serverInfos)
						_servers.Add(si.Alias, string.Format("{0}:{1}", si.Url, si.Port));
				}
                
				return _servers;
        	}
        	set
        	{
				if (value.Count > 0)
				{
					_serverInfos.Clear();

					foreach (string alias in value.Keys)
					{
						string url = alias;						
						int port = 80;

						Regex regex = new Regex(@"(?<Url>http\:\/\/[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})[\:]{0,1}(?<Port>[\d]{0,6})", RegexOptions.Compiled);

						if (regex.Match(alias).Success == true)
						{
							url = regex.Match(alias).Groups["Url"].Value;
							int.TryParse(regex.Match(alias).Groups["Port"].Value, out port);
						}

						_serverInfos.Add(new ServerInfo(alias, url, port));
					}

					_servers = value;
				}				
        	}
        }

		/// <summary>
		/// UpdateNeededFile을 구하거나 설정합니다.
		/// <remarks>UpdateNeededFile의 추가나 삭제 모두 제거는 <seealso cref="UpdateNeededFileAdd"/>, <seealso cref="UpdateNeededFileRemove"/>, <seealso cref="UpdateNeededFileClear"/> 메서드를 사용하십시오.</remarks>
		/// </summary>
		public List<string> UpdateNeededFiles
		{
			get
			{
				if (_updateNeededFiles == null)
				{
					_updateNeededFiles = new List<string>();

					foreach (FileInfo fi in _updateNeededFileCollection)
						_updateNeededFiles.Add(fi.Name);
				}

				return _updateNeededFiles;
			}
			set
			{
				if (value.Count > 0)
				{
					_updateNeededFiles.Clear();

					foreach (string filename in value)
						_updateNeededFileCollection.Add(new FileInfo(filename));
				}

				_updateNeededFiles = value;
			}
		}
        #endregion

		/// <summary>
		/// Upate Server를 추가합니다.
		/// </summary>
		/// <param name="alias">추가할 Server alias(ex : Real)</param>
		/// <param name="url">추가할 Server service Url(ex : http://127.0.0.1)</param>
		/// <param name="port">추가할 Server serivce port(Option)</param>
		public void ServerAdd(string alias, string url, int port)
		{
			_serverInfos.Add(new ServerInfo(alias, url, port));
		}

		/// <summary>
		/// Upate Server를 제거합니다.
		/// </summary>
		/// <param name="alias">Server alias(ex : Real)</param>
		public void ServerRemove(string alias)
		{
			_serverInfos.Remove(alias);
		}

		/// <summary>
		/// Upate Server 목록을 모두 제거 합니다.
		/// </summary>
		public void ServerClear()
		{
			_serverInfos.Clear();
		}

		/// <summary>
		/// 대상 Filename을 추가합니다.
		/// </summary>
		/// <param name="filename">추가할 filename</param>
		public void UpdateNeededFileAdd(string filename)
		{
			_updateNeededFileCollection.Add(new FileInfo(filename));
		}

		/// <summary>
		/// 대상 Filename을 제거합니다.
		/// </summary>
		/// <param name="filename">제거할 filename</param>
		public void UpdateNeededFileRemove(string filename)
		{
			_updateNeededFileCollection.Remove(filename);
		}

		/// <summary>
		/// 대상 Filename 목록을 모두 제거 합니다.
		/// </summary>
		public void UpdateNeededFileClear()
		{
			_updateNeededFileCollection.Clear();
		}

		/// <summary>
		/// Config 파일에 변경 사항만을 저장합니다.
		/// </summary>		
		public void SaveModifed()
		{
			_config.Save(ConfigurationSaveMode.Modified);
		}

		/// <summary>
		/// Config 파일에 저장 합니다.
		/// </summary>
		public void Save()
		{
			_config.Save();
		}
	}
}
