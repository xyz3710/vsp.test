﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.LoginInfo
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:18
/*	Purpose		:	Login 정보에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// Login 정보에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class LoginInfo : ConfigurationElement
	{
		/// <summary>
		/// LoginInfo class의 새 인스턴스를 초기화합니다.
		/// <param name="plantId">PlantId</param>
		/// <param name="userId">사용자 Id</param>
		/// </summary>
		public LoginInfo(string plantId, string userId)
		{
			PlantId = plantId;
			UserId = userId;
		}

		/// <summary>
		/// LoginInfo class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public LoginInfo()
		{
		}

		/// <summary>
        /// PlantId를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("plantId", DefaultValue="DCCPCS1", IsRequired=true, IsKey=true)]
        [Description("PlantId를 구하거나 설정합니다.")]
        public string PlantId
        {
        	get
        	{
        		return (string)this["plantId"];
        	}
        	set
        	{
        		this["plantId"] = value;
        	}
        }
        
        /// <summary>
        /// UserId를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("userId", DefaultValue="DEV", IsRequired=true, IsKey=true)]
        [Description("UserId를 구하거나 설정합니다.")]
        public string UserId
        {
        	get
        	{
        		return (string)this["userId"];
        	}
        	set
        	{
        		this["userId"] = value;
        	}
        }        
	}
}
