﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.FileInfoCollection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:27
/*	Purpose		:	FileInfo 컬렉션을 포함하는 FileInfoCollection 구성 요소를 나타냅니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// FileInfo 컬렉션을 포함하는 FileInfoCollection 구성 요소를 나타냅니다.
	/// </summary>
	[ConfigurationCollection(typeof(FileInfo), CollectionType=ConfigurationElementCollectionType.AddRemoveClearMap, AddItemName="file")]
	internal class FileInfoCollection : GenericCollection<FileInfo>
	{		
	}
}
