﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.AutoServerSwitcher.UpdaterFileCollection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 12일 목요일 오후 7:25
/*	Purpose		:	UpdaterFile 컬렉션과 속성을 포함하는 UpdaterFileCollection 구성 요소를 나타냅니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler.AutoServerSwitcher
{
	/// <summary>
	/// UpdaterFile 컬렉션과 속성을 포함하는 UpdaterFileCollection 구성 요소를 나타냅니다.
	/// </summary>
	internal class UpdaterFileCollection : FileInfoCollection
	{
		private const string CHECKUPDATENEEDEDFILE_KEY = "checkUpdateNeededFile";
		private const string BUFFERSIZE_KEY = "bufferSize";
		private const string UPDATEFILE_KEY = "updateFile";
		private const string DEPLOYFOLDER_KEY = "deployFolder";

		/// <summary>
		/// Update 필요 파일의 검사여부를 구하거나 설정합니다.
		/// <remarks>false이면 <seealso cref="iDASiT.FX.Win.Configuration.Handler.AutoServerSwitcherSection.UpdaterFiles"/>를 검사하지 않습니다.</remarks>
		/// </summary>
		[ConfigurationProperty(CHECKUPDATENEEDEDFILE_KEY, DefaultValue="true", IsRequired=true)]
		[Description("Update 필요 파일의 검사여부를 구하거나 설정합니다.\r\nfalse이면 UpdateNeededFiles를 검사하지 않습니다.")]
		public bool CheckUpdateNeededFile
		{
			get
			{
				return (bool)base.ElementInformation.Properties[CHECKUPDATENEEDEDFILE_KEY].Value;
			}
			set
			{
				base.ElementInformation.Properties[CHECKUPDATENEEDEDFILE_KEY].Value = value;
			}
		}

		/// <summary>
		/// DeployFolder를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty(DEPLOYFOLDER_KEY, DefaultValue="iDASiT.FX.Win.Deploy", IsRequired=true)]
		[Description("DeployFolder를 구하거나 설정합니다.")]
		public string DeployFolder
		{
			get
			{
				return base.ElementInformation.Properties[DEPLOYFOLDER_KEY].Value as string;
			}
			set
			{
				base.ElementInformation.Properties[DEPLOYFOLDER_KEY].Value = value;
			}
		}

		/// <summary>
		/// UpdateFile를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty(UPDATEFILE_KEY, DefaultValue="UpdateFile.xml", IsRequired=true)]
		[Description("UpdateFile를 구하거나 설정합니다.")]
		public string UpdateFile
		{
			get
			{
				return base.ElementInformation.Properties[UPDATEFILE_KEY].Value as string;
			}
			set
			{
				base.ElementInformation.Properties[UPDATEFILE_KEY].Value = value;
			}
		}

		/// <summary>
		/// BufferSize를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty(BUFFERSIZE_KEY, DefaultValue="512000", IsRequired=true)]
		[Description("BufferSize를 구하거나 설정합니다.")]
		public int BufferSize
		{
			get
			{
				return (int)base.ElementInformation.Properties[BUFFERSIZE_KEY].Value;
			}
			set
			{
				base.ElementInformation.Properties[BUFFERSIZE_KEY].Value = value;
			}
		}

	}
}
