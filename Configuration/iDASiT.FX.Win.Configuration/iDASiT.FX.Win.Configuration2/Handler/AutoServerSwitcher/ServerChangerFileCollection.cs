﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.AutoServerSwitcher.TargetFileCollection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 12일 목요일 오후 7:25
/*	Purpose		:	TargetFile 컬렉션과 pattern을 포함하는 TargetFileCollection 구성 요소를 나타냅니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler.AutoServerSwitcher
{
	/// <summary>
	/// TargetFile 컬렉션과 pattern을 포함하는 TargetFileCollection 구성 요소를 나타냅니다.
	/// </summary>
	internal class ServerChangerFileCollection : FileInfoCollection
	{
		private const string DEFAULT_PATTERN = @"http\:\/\/[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}[\:]{0,1}[\d]{0,6}";
		private const string PATTERN_KEY = "pattern";

		/// <summary>
		/// Pattern를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty(PATTERN_KEY, DefaultValue=DEFAULT_PATTERN, IsRequired=false)]
		[Description("Pattern를 구하거나 설정합니다.")]
		public string Pattern
		{
			get
			{
				return base.ElementInformation.Properties[PATTERN_KEY].Value as string;
			}
			set
			{
				base.ElementInformation.Properties[PATTERN_KEY].Value = value;
			}
		}
	}
}
