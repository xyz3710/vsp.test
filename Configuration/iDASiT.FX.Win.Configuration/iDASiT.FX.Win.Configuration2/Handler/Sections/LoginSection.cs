﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.LoginSection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:31
/*	Purpose		:	Login Section에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// Login Section에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class LoginSection : UpdaterCoreSection
	{
		/// <summary>
		/// iDASiT.FX.Win.Configuration.Handler의 Login SectionName string을 구합니다.
		/// </summary>
		public new const string SectionNameString = "Login";

		/// <summary>
		/// LoginSection class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public LoginSection()
		{
		}

		/// <summary>
		/// LimitFileCountOnBeforeDownload를 구하거나 설정합니다.<br/>
		/// Updater Download 전 최대 어셈블리 개수를 구하거나 설정 하여 해당 개수 이상이면 Win 폴더를 제거 하고 다시 받습니다.
		/// </summary>
		[ConfigurationProperty("limitFileCountOnBeforeDownload", DefaultValue="30", IsRequired=false)]
		[Description("LimitFileCountOnBeforeDownload를 구하거나 설정합니다.\r\nUpdater Download 전 최대 어셈블리 개수를 구하거나 설정 하여 해당 개수 이상이면 Win 폴더를 제거 하고 다시 받습니다.")]
		public int LimitFileCountOnBeforeDownload
		{
			get
			{
				return (int)this["limitFileCountOnBeforeDownload"];
			}
			set
			{
				this["limitFileCountOnBeforeDownload"] = value;
			}
		}

		/// <summary>
		/// Update 필요 파일의 검사여부를 구하거나 설정합니다.
		/// <remarks>false이면 <seealso cref="UpdateNeededFiles"/>를 검사하지 않습니다.</remarks>
		/// </summary>
		[ConfigurationProperty("checkUpdateNeededFile", DefaultValue="false", IsRequired=true)]
		[Description("Update 필요 파일의 검사여부를 구하거나 설정합니다.\r\nfalse이면 UpdateNeededFiles를 검사하지 않습니다.")]
		public bool CheckUpdateNeededFile
		{
			get
			{
				return (bool)this["checkUpdateNeededFile"];
			}
			set
			{
				this["checkUpdateNeededFile"] = value;
			}
		}

		/// <summary>
		/// Update시 필요한 File을 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("updateNeededFiles", IsRequired=false)]
		[Description("Update시 필요한 File을 구하거나 설정합니다.")]
		public FileInfoCollection UpdateNeededFiles
		{
			get
			{
				return (FileInfoCollection)this["updateNeededFiles"];
			}
			set
			{
				this["updateNeededFiles"] = value;
			}
		}
	}
}
