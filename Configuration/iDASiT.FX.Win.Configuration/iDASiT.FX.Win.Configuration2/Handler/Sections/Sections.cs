﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.Sections
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 11일 수요일 오후 12:10
/*	Purpose		:	구성 파일에 있는 관련된 섹션의 그룹을 나타냅니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using iDASiT.FX.Win.Configuration.Handler.ServerChanger;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// 구성 파일에 있는 관련된 섹션의 그룹을 나타냅니다.
	/// </summary>
	internal class Sections : ConfigurationSectionGroup
	{
		/// <summary>
		/// iDASiT.FX.Win.Configuration.Handler의 SectionGroup을 구합니다.
		/// </summary>
		public const string SectionGroupNameString = "iDASiT.FX.Win.Configuration.Handler.Sections";

		/// <summary>
		/// SectionGroup class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public Sections()
		{
		}

		/// <summary>
		/// ServerChanger Section을 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("ServerChanger", DefaultValue="ServerChanger", IsRequired=false)]
		[Description("ServerChanger Section을 구하거나 설정합니다.")]
		public ServerChangerSection ServerChanger
		{
			get
			{
				return (ServerChangerSection)base.Sections["ServerChanger"];
			}
			set
			{
				base.Sections.Add("ServerChanger", value);
			}
		}

		/// <summary>
		/// Updater Section을 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("UpdaterCore", DefaultValue="UpdaterCore", IsRequired=false)]
		[Description("Updater Section을 구하거나 설정합니다.")]
		public UpdaterCoreSection UpdaterCore
		{
			get
			{
				return (UpdaterCoreSection)base.Sections["UpdaterCore"];
			}
			set
			{
				base.Sections.Add("UpdaterCore", value);
			}
		}

		/// <summary>
		/// Updater Section을 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("Updater", DefaultValue="Updater", IsRequired=false)]
		[Description("Updater Section을 구하거나 설정합니다.")]
		public UpdaterSection Updater
		{
			get
			{
				return (UpdaterSection)base.Sections["Updater"];
			}
			set
			{
				base.Sections.Add("Updater", value);
			}
		}

		/// <summary>
		/// Login Section을 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("Login", DefaultValue="Login", IsRequired=false)]
		[Description("Login Section을 구하거나 설정합니다.")]
		public LoginSection Login
		{
			get
			{
				return (LoginSection)base.Sections["Login"];
			}
			set
			{
				base.Sections.Add("Login", value);
			}
		}

		/// <summary>
		/// AutoServerSwitcher Section을 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("AutoServerSwitcher", DefaultValue="AutoServerSwitcher", IsRequired=false)]
		[Description("Login Section을 구하거나 설정합니다.")]
		public AutoServerSwitcherSection AutoServerSwitcher
		{
			get
			{
				return (AutoServerSwitcherSection)base.Sections["AutoServerSwitcher"];
			}
			set
			{
				base.Sections.Add("AutoServerSwitcher", value);
			}
		}
	}
}
