﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.ServerChangerSection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:31
/*	Purpose		:	ServerChanger Section에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using iDASiT.FX.Win.Configuration.Handler.ServerChanger;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// ServerChanger Section에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class ServerChangerSection : ConfigurationSection
	{
		/// <summary>
		/// iDASiT.FX.Win.Configuration.Handler의 ServerChanger SectionName string을 구합니다.
		/// </summary>
		public const string SectionNameString = "ServerChanger";

		/// <summary>
		/// ServerChangerSection class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public ServerChangerSection()
		{
		}

		/// <summary>
        /// LoginInfo를 구하거나 설정합니다.
        /// </summary>
		[ConfigurationProperty("loginInfo", IsRequired=true, IsKey=true)]
        [Description("LoginInfo를 구하거나 설정합니다.")]
        public LoginInfo LoginInfo
        {
        	get
        	{
				return (LoginInfo)this["loginInfo"];
        	}
        	set
        	{
				this["loginInfo"] = value;
        	}
        }
        
        /// <summary>
        /// Servers를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("servers", IsRequired=true)]
        [Description("Servers를 구하거나 설정합니다.")]
        public ServerCollection Servers
        {
        	get
        	{
        		return (ServerCollection)this["servers"];
        	}
        	set
        	{
        		this["servers"] = value;
        	}
        }
        
        /// <summary>
        /// TargetConfigFiles를 구하거나 설정합니다.
        /// </summary>
		[ConfigurationProperty("targetConfigFiles", IsRequired=true, IsKey=true)]		
        [Description("TargetConfigFiles를 구하거나 설정합니다.")]
        public FileInfoCollection TargetConfigFiles
        {
        	get
        	{
        		return (FileInfoCollection)this["targetConfigFiles"];
        	}
        	set
        	{
				this["targetConfigFiles"] = value;
        	}
        }
	}
}
