﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.UpdaterSection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:31
/*	Purpose		:	Updater Core Section에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// Updater Core Section에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class UpdaterSection : UpdaterCoreSection
	{
		/// <summary>
		/// iDASiT.FX.Win.Configuration.Handler의 Updater SectionName string을 구합니다.
		/// </summary>
		public new const string SectionNameString = "Updater";

		/// <summary>
		/// UpdaterSection class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public UpdaterSection()
		{
		}

		/// <summary>
        /// <seealso cref="ForceDownloadFrameworkWinBaseDate"/> 설정일 이전의 FX.Win을 강제로 다운로드 하도록 하는 값을 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("forceDownloadFrameworkWin", DefaultValue=false, IsRequired=false)]
        [Description("ForceDownloadFrameworkWinBaseDate 설정일 이전의 FX.Win을 강제로 다운로드 하도록 하는 값을 구하거나 설정합니다.")]
        public bool ForceDownloadFrameworkWin
        {
        	get
        	{
        		return (bool)this["forceDownloadFrameworkWin"];
        	}
        	set
        	{
        		this["forceDownloadFrameworkWin"] = value;
        	}
        }
        
        /// <summary>
        /// <seealso cref="ForceDownloadFrameworkWin"/> 값이 True로 되어 있을 경우 Local에 있는 FX.Win의 날짜가 설정일 이전은 강제 다운 로드 합니다.
        /// </summary>
        [ConfigurationProperty("forceDownloadFrameworkWinBaseDate", DefaultValue="2008-06-01 00:00:01", IsRequired=false)]
        [Description("ForceDownloadFrameworkWin 값이 True로 되어 있을 경우 Local에 있는 FX.Win의 날짜가 설정일 이전은 강제 다운 로드 합니다.")]
        public DateTime ForceDownloadFrameworkWinBaseDate
        {
        	get
        	{
        		return (DateTime)this["forceDownloadFrameworkWinBaseDate"];
        	}
        	set
        	{
        		this["forceDownloadFrameworkWinBaseDate"] = value;
        	}
        }
        
        /// <summary>
        /// ForceDownloadFolders를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("forceDownloadFolders", IsRequired=false)]
        [Description("ForceDownloadFolders를 구하거나 설정합니다.")]
        public FileInfoCollection ForceDownloadFolders
        {
        	get
        	{
        		return (FileInfoCollection)this["forceDownloadFolders"];
        	}
        	set
        	{
        		this["forceDownloadFolders"] = value;
        	}
        }
        
        /// <summary>
        /// Update시 Skip할 Folder를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("updateSkipFolders", IsRequired=false)]
		[Description("Update시 Skip할 Folder를 구하거나 설정합니다.")]
        public FileInfoCollection UpdateSkipFolders
        {
        	get
        	{
        		return (FileInfoCollection)this["updateSkipFolders"];
        	}
        	set
        	{
        		this["updateSkipFolders"] = value;
        	}
        }
	}
}
