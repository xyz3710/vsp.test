﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.AutoServerSwitcherSection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:31
/*	Purpose		:	AutoServerSwitcher Section에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using iDASiT.FX.Win.Configuration.Handler.AutoServerSwitcher;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// AutoServerSwitcher Section에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class AutoServerSwitcherSection : ConfigurationSection
	{
		/// <summary>
		/// iDASiT.FX.Win.Configuration.Handler의 AutoServerSwitcher SectionName string을 구합니다.
		/// </summary>
		public const string SectionNameString = "AutoServerSwitcher";

		/// <summary>
		/// AutoServerSwitcherSection class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public AutoServerSwitcherSection()
		{
		}

		/// <summary>
		/// Ping TimeOut 시간을 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("timeOut", DefaultValue="1000", IsRequired=false)]
		[Description("Ping TimeOut 시간을 구하거나 설정합니다.")]
		public int TimeOut
		{
			get
			{
				return (int)this["timeOut"];
			}
			set
			{
				this["timeOut"] = value;
			}
		}

		/// <summary>
        /// Server를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("servers", IsRequired=true)]
        [Description("Servers를 구하거나 설정합니다.")]
		public ServerInfoCollection Servers
        {
        	get
        	{
				return (ServerInfoCollection)this["servers"];
        	}
        	set
        	{
        		this["servers"] = value;
        	}
        }

		/// <summary>
		/// ServerChanger용 TargetConfigFiles를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("serverChanger", IsRequired=true)]
		[Description("ServerChanger용 TargetConfigFiles를 구하거나 설정합니다.")]
		public ServerChangerFileCollection ServerChangerFiles
		{
			get
			{
				return (ServerChangerFileCollection)this["serverChanger"];
			}
			set
			{
				this["serverChanger"] = value;
			}
		}

		/// <summary>
		/// ServerChanger용 TargetConfigFiles를 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty("updater", IsRequired=true)]
		[Description("ServerChanger용 TargetConfigFiles를 구하거나 설정합니다.")]
		public UpdaterFileCollection UpdaterFiles
		{
			get
			{
				return (UpdaterFileCollection)this["updater"];
			}
			set
			{
				this["updater"] = value;
			}
		}
	}
}
