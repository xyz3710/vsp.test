﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.ServerInfo
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:36
/*	Purpose		:	Server 정보에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// Server 정보에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class ServerInfo : ConfigurationElement
	{
		/// <summary>
		/// ServerInfo class의 새 인스턴스를 초기화합니다.
		/// <param name="alias">Server alias(ex : Real)</param>
		/// <param name="url">Server service Url(ex : http://127.0.0.1)</param>
		/// <param name="port">Server serivce port(Option)</param>
		/// </summary>
		public ServerInfo(string alias, string url, int port)
		{
			Alias = alias;
			Url = url;
			Port = port;
		}

		/// <summary>
		/// ServerInfo class의 새 인스턴스를 초기화합니다.
		/// <param name="alias">Server alias(ex : Real)</param>
		/// <param name="url">Server service Url(ex : http://127.0.0.1)</param>
		/// </summary>
		public ServerInfo(string alias, string url)
		{
			Alias = alias;
			Url = url;
		}

		/// <summary>
		/// ServerInfo class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public ServerInfo()
		{
		}

		/// <summary>
        /// Alias를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("alias", IsRequired=true, IsKey=true)]
        [Description("Alias를 구하거나 설정합니다.")]
        public string Alias
        {
        	get
        	{
        		return (string)this["alias"];
        	}
        	set
        	{
        		this["alias"] = value;
        	}
        }
        
        /// <summary>
		/// Url를 구하거나 설정합니다.(ex : http://127.0.0.1)
        /// </summary>
		[ConfigurationProperty("url", DefaultValue="http://127.0.0.1", IsRequired=true)]
		[Description("Url를 구하거나 설정합니다.(ex : http://127.0.0.1)")]
        public string Url
        {
        	get
        	{
        		return (string)this["url"];
        	}
        	set
        	{
        		this["url"] = value;
        	}
        }
        
        /// <summary>
        /// Port를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("port", DefaultValue="80", IsRequired=false)]
        [Description("Port를 구하거나 설정합니다.")]
        public int Port
        {
        	get
        	{
        		return (int)this["port"];
        	}
        	set
        	{
        		this["port"] = value;
        	}
        }        
	}
}
