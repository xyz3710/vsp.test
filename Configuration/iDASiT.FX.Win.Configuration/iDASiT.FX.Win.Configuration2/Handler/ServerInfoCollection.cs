﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.ServerInfoCollection
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:40
/*	Purpose		:	ServerInfo 컬렉션과 Default element를 포함하는 ServerInfoCollection 구성 요소를 나타냅니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// ServerInfo 컬렉션과 Default element를 포함하는 ServerInfoCollection 구성 요소를 나타냅니다.
	/// </summary>
	[ConfigurationCollection(typeof(ServerInfo), CollectionType=ConfigurationElementCollectionType.AddRemoveClearMap, AddItemName="server")]
	internal class ServerInfoCollection : GenericCollection<ServerInfo>
	{
		private const string DEFAULT_KEY = "default";

		/// <summary>
		/// Default url을 구하거나 설정합니다.
		/// </summary>
		[ConfigurationProperty(DEFAULT_KEY, IsRequired=true)]
		[Description("Default url을 구하거나 설정합니다.")]
		public string Default
		{
			get
			{
				return base.ElementInformation.Properties[DEFAULT_KEY].Value as string;
			}
			set
			{
				base.ElementInformation.Properties[DEFAULT_KEY].Value = value;
			}
		}
	}
}
