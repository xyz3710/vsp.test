﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.FX.Win.Configuration.Handler.FileInfo
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 6월 10일 화요일 오후 5:23
/*	Purpose		:	File 정보에 관련된 설정 파일을 관리하는 클래스 입니다.
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;

namespace iDASiT.FX.Win.Configuration.Handler
{
	/// <summary>
	/// File 정보에 관련된 설정 파일을 관리하는 클래스 입니다.
	/// </summary>
	internal class FileInfo : ConfigurationElement
	{
		/// <summary>
		/// FileInfo class의 새 인스턴스를 초기화합니다.
		/// <param name="filename">관리할 filename</param>
		/// </summary>
		public FileInfo(string filename)
		{
			Name = filename;
		}

		/// <summary>
		/// FileInfo class의 새 인스턴스를 초기화합니다.
		/// </summary>
		public FileInfo()
		{
		}

		/// <summary>
        /// Name를 구하거나 설정합니다.
        /// </summary>
        [ConfigurationProperty("name", DefaultValue="", IsRequired=true, IsKey=true)]
		[StringValidator(InvalidCharacters="!@#$%^&*()[]{}/;'\"|\\")]
        [Description("Name를 구하거나 설정합니다.")]
        public string Name
        {
        	get
        	{
        		return (string)this["name"];
        	}
        	set
        	{
               	this["name"] = value;
        	}
        }
	}
}
