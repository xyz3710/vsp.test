﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ZipTest
{
	class Program
	{
		static void Main(string[] args)
		{
			const string sourceFolder = @"D:\VSP\Test\XML";
			const string targetFile = @"J:\Test.zip";

			if (File.Exists(targetFile) == true)
			{
				File.Delete(targetFile);
			}

			var folders = Directory.GetDirectories(sourceFolder, "*.*", SearchOption.AllDirectories);

						
			//foreach (string folder in folders)
			{
				DoCreateFromDirectory(
					sourceFolder,
					targetFile,
					CompressionLevel.Fastest,
					false,
					null,
					ZipArchiveMode.Update,
					SearchOption.AllDirectories);
			}

			using (var zipArchive = ZipFile.Open(targetFile, ZipArchiveMode.Update))
			{
				var file = @"E:\Gachisoft\Framework\_DeployOutput.cmd";
				var fi = new FileInfo(file);

				if (fi.Exists == true)
				{
					var entry = Path.GetFileName(file);
					zipArchive.CreateEntryFromFile(file, entry);
				}
			}

			//using (var archive = new ZipArchive(zipFileToOpen, ZipArchiveMode.Update))
			//{
			//	{
			//		archive.CreateEntry(string.Format("{0}/", folder));
			//	}
			//}

			//ZipFile.CreateFromDirectory(sourceFolder, targetFile, CompressionLevel.Fastest, false, Encoding.UTF8);

			ZipFile.ExtractToDirectory(targetFile, @"J:\Test");

			var zipArchives = ZipFile.Open(targetFile, ZipArchiveMode.Update);

		}

		//private static string GetEntryName(string filePath)
		//{
		//	string fullPath = Path.GetFullPath(filePath);

		//	return fullPath.Substring(fullName.Length, length);
		//}
		private static void DoCreateFromDirectory(
			string sourceDirectoryName,
			string destinationArchiveFileName,
			CompressionLevel compressionLevel = CompressionLevel.Optimal,
			bool includeBaseDirectory = false,
			Encoding entryNameEncoding = null,
			ZipArchiveMode zipArchiveMode = ZipArchiveMode.Create,
			SearchOption searchOption = SearchOption.TopDirectoryOnly)
		{
			sourceDirectoryName = Path.GetFullPath(sourceDirectoryName);
			destinationArchiveFileName = Path.GetFullPath(destinationArchiveFileName);
			using (ZipArchive zipArchive = ZipFile.Open(destinationArchiveFileName, zipArchiveMode, entryNameEncoding))
			{
				bool flag = true;
				DirectoryInfo directoryInfo = new DirectoryInfo(sourceDirectoryName);
				string fullName = directoryInfo.FullName;

				if (includeBaseDirectory && directoryInfo.Parent != null)
				{
					fullName = directoryInfo.Parent.FullName;
				}

				foreach (FileSystemInfo fileSystemInfo in directoryInfo.EnumerateFileSystemInfos("*", searchOption))
				{
					flag = false;
					int length = fileSystemInfo.FullName.Length - fullName.Length;
					string str = fileSystemInfo.FullName.Substring(fullName.Length, length);
					char[] directorySeparatorChar = new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar };
					str = str.TrimStart(directorySeparatorChar);

					if (!(fileSystemInfo is FileInfo))
					{
						DirectoryInfo directoryInfo1 = fileSystemInfo as DirectoryInfo;

						if (directoryInfo1 == null || IsDirEmpty(directoryInfo1, searchOption) == false)
						{
							continue;
						}

						zipArchive.CreateEntry(string.Concat(str, Path.DirectorySeparatorChar));
					}
					else
					{
						DoCreateEntryFromFile(zipArchive, fileSystemInfo.FullName, str, compressionLevel);
					}
				}

				if (includeBaseDirectory && flag)
				{
					zipArchive.CreateEntry(string.Concat(directoryInfo.Name, Path.DirectorySeparatorChar));
				}
			}
		}

		private static bool IsDirEmpty(DirectoryInfo possiblyEmptyDir, SearchOption searchOption)
		{
			bool flag;
			using (IEnumerator<FileSystemInfo> enumerator = possiblyEmptyDir.EnumerateFileSystemInfos("*", searchOption).GetEnumerator())
			{
				if (enumerator.MoveNext())
				{
					FileSystemInfo current = enumerator.Current;
					flag = false;
				}
				else
				{
					return true;
				}
			}
			return flag;
		}

		internal static ZipArchiveEntry DoCreateEntryFromFile(ZipArchive destination, string sourceFileName, string entryName, CompressionLevel? compressionLevel)
		{
			ZipArchiveEntry zipArchiveEntry;
			if (destination == null)
			{
				throw new ArgumentNullException("destination");
			}
			if (sourceFileName == null)
			{
				throw new ArgumentNullException("sourceFileName");
			}
			if (entryName == null)
			{
				throw new ArgumentNullException("entryName");
			}
			using (Stream stream = File.Open(sourceFileName, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				ZipArchiveEntry zipArchiveEntry1 = (compressionLevel.HasValue ? destination.CreateEntry(entryName, compressionLevel.Value) : destination.CreateEntry(entryName));
				DateTime lastWriteTime = File.GetLastWriteTime(sourceFileName);
				if (lastWriteTime.Year < 1980 || lastWriteTime.Year > 2107)
				{
					lastWriteTime = new DateTime(1980, 1, 1, 0, 0, 0);
				}
				zipArchiveEntry1.LastWriteTime = lastWriteTime;
				using (Stream stream1 = zipArchiveEntry1.Open())
				{
					stream.CopyTo(stream1);
				}
				zipArchiveEntry = zipArchiveEntry1;
			}
			return zipArchiveEntry;
		}
	}
}
