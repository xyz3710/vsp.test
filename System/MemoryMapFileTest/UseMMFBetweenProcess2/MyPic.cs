using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UseMMFBetweenProcess2
{
	[Serializable]
	public class MyPic
	{
		public System.Drawing.Image pic
		{
			get;
			set;
		}

		/// <summary>
		/// RawData를 구하거나 설정합니다.
		/// </summary>
		/// <value>RawData를 반환합니다.</value>
		public byte[] RawData
		{
			get;
			set;
		}
		
		public string picInfo
		{
			get;
			set;
		}
		public MyPic()
		{

		}
	}
}
