﻿namespace UseMMFBetweenProcess2
{
	partial class frmMain
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtImageInfo = new System.Windows.Forms.TextBox();
			this.btnExit = new System.Windows.Forms.Button();
			this.btnLoadPic = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.btnSaveToMMF = new System.Windows.Forms.Button();
			this.btnLoadFromMMF = new System.Windows.Forms.Button();
			this.btnSaveToMMFByte = new System.Windows.Forms.Button();
			this.btnLoadFromMMFByte = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// txtImageInfo
			// 
			this.txtImageInfo.Location = new System.Drawing.Point(12, 12);
			this.txtImageInfo.Name = "txtImageInfo";
			this.txtImageInfo.Size = new System.Drawing.Size(787, 25);
			this.txtImageInfo.TabIndex = 0;
			// 
			// btnExit
			// 
			this.btnExit.Location = new System.Drawing.Point(679, 43);
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new System.Drawing.Size(120, 25);
			this.btnExit.TabIndex = 6;
			this.btnExit.Text = "E&xit";
			this.btnExit.UseVisualStyleBackColor = true;
			this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
			// 
			// btnLoadPic
			// 
			this.btnLoadPic.Location = new System.Drawing.Point(12, 43);
			this.btnLoadPic.Name = "btnLoadPic";
			this.btnLoadPic.Size = new System.Drawing.Size(120, 25);
			this.btnLoadPic.TabIndex = 1;
			this.btnLoadPic.Text = "&Load";
			this.btnLoadPic.UseVisualStyleBackColor = true;
			this.btnLoadPic.Click += new System.EventHandler(this.btnLoadPic_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox1.Location = new System.Drawing.Point(12, 74);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(787, 558);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 4;
			this.pictureBox1.TabStop = false;
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			// 
			// btnSaveToMMF
			// 
			this.btnSaveToMMF.Location = new System.Drawing.Point(138, 43);
			this.btnSaveToMMF.Name = "btnSaveToMMF";
			this.btnSaveToMMF.Size = new System.Drawing.Size(120, 25);
			this.btnSaveToMMF.TabIndex = 2;
			this.btnSaveToMMF.Text = "&Save to MMF";
			this.btnSaveToMMF.UseVisualStyleBackColor = true;
			this.btnSaveToMMF.Click += new System.EventHandler(this.btnSaveToMMF_Click);
			// 
			// btnLoadFromMMF
			// 
			this.btnLoadFromMMF.Location = new System.Drawing.Point(264, 43);
			this.btnLoadFromMMF.Name = "btnLoadFromMMF";
			this.btnLoadFromMMF.Size = new System.Drawing.Size(120, 25);
			this.btnLoadFromMMF.TabIndex = 3;
			this.btnLoadFromMMF.Text = "&Load from MMF";
			this.btnLoadFromMMF.UseVisualStyleBackColor = true;
			this.btnLoadFromMMF.Click += new System.EventHandler(this.btnLoadFromMMF_Click);
			// 
			// btnSaveToMMFByte
			// 
			this.btnSaveToMMFByte.Location = new System.Drawing.Point(397, 43);
			this.btnSaveToMMFByte.Name = "btnSaveToMMFByte";
			this.btnSaveToMMFByte.Size = new System.Drawing.Size(135, 25);
			this.btnSaveToMMFByte.TabIndex = 4;
			this.btnSaveToMMFByte.Text = "Save to MMF Byte";
			this.btnSaveToMMFByte.UseVisualStyleBackColor = true;
			this.btnSaveToMMFByte.Click += new System.EventHandler(this.btnSaveToMMFByte_Click);
			// 
			// btnLoadFromMMFByte
			// 
			this.btnLoadFromMMFByte.Location = new System.Drawing.Point(538, 43);
			this.btnLoadFromMMFByte.Name = "btnLoadFromMMFByte";
			this.btnLoadFromMMFByte.Size = new System.Drawing.Size(135, 25);
			this.btnLoadFromMMFByte.TabIndex = 5;
			this.btnLoadFromMMFByte.Text = "Load to MMF Byte";
			this.btnLoadFromMMFByte.UseVisualStyleBackColor = true;
			this.btnLoadFromMMFByte.Click += new System.EventHandler(this.btnLoadFromMMFByte_Click);
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(811, 644);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.btnLoadPic);
			this.Controls.Add(this.btnLoadFromMMFByte);
			this.Controls.Add(this.btnSaveToMMFByte);
			this.Controls.Add(this.btnLoadFromMMF);
			this.Controls.Add(this.btnSaveToMMF);
			this.Controls.Add(this.btnExit);
			this.Controls.Add(this.txtImageInfo);
			this.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "UseMMFBetweenProcess2";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtImageInfo;
		private System.Windows.Forms.Button btnExit;
		private System.Windows.Forms.Button btnLoadPic;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button btnSaveToMMF;
		private System.Windows.Forms.Button btnLoadFromMMF;
		private System.Windows.Forms.Button btnSaveToMMFByte;
		private System.Windows.Forms.Button btnLoadFromMMFByte;
	}
}

