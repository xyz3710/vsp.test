﻿// ****************************************************************************************************************** //
//	Domain		:	UseMMFBetweenProcess2.frmMain
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2015년 1월 17일 토요일 오전 10:27
//	Purpose		:	MemoryMappedFile test
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	http://www.cprogramdevelop.com/1320558/
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="frmMain.cs" company="(주)가치소프트">
//		Copyright (c) 2015. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Drawing.Imaging;

namespace UseMMFBetweenProcess2
{
	public partial class frmMain : Form
	{
		public frmMain()
		{
			InitializeComponent();
			InitMemoryMappedFile();
		}

		/// <summary>  
		///   
		/// </summary>  
		private Image bmp
		{
			get
			{
				return pictureBox1.Image;
			}
			set
			{
				pictureBox1.Image = value;
			}
		}

		/// <summary>  
		///   
		/// </summary>  
		private string info
		{
			get
			{
				return txtImageInfo.Text;
			}
			set
			{
				txtImageInfo.Text = value;
			}
		}

		private MemoryMappedFile memoryFile = null;
		private MemoryMappedViewStream stream = null;
		private MemoryMappedViewAccessor accessor = null;

		/// <summary>  
		/// :10M  
		/// </summary>  
		private const int FileSize = 1024 * 1024 * 40;

		/// <summary>  
		/// ,  
		/// </summary>  
		private void InitMemoryMappedFile()
		{
			try
			{
				memoryFile = MemoryMappedFile.CreateOrOpen("UseMMFBetweenProcess2", FileSize);
				stream = memoryFile.CreateViewStream();
				accessor = memoryFile.CreateViewAccessor();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				Close();
			}
		}
		/// <summary>  
		///   
		/// </summary>  
		private void DisposeMemoryMappedFile()
		{
			if (stream != null)
				stream.Close();
			if (memoryFile != null)
				memoryFile.Dispose();
		}

		private void btnLoadPic_Click(object sender, EventArgs e)
		{
			ChooseImageFile();
		}

		//  
		private void ChooseImageFile()
		{
			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				bmp = new Bitmap(openFileDialog1.FileName);
			}
		}
		//  
		private MyPic CreateMyPicObj()
		{
			MyPic obj = new MyPic();

			obj.pic = bmp;
			obj.RawData = (bmp as Bitmap).GetRawData();
			obj.picInfo = info;
			return obj;
		}

		/// <summary>  
		/// MyPic  
		/// </summary>  
		private void SaveToMMF()
		{
			try
			{
				MyPic obj = CreateMyPicObj();
				IFormatter formatter = new BinaryFormatter();
				stream.Seek(0, SeekOrigin.Begin);
				formatter.Serialize(stream, obj);
				MessageBox.Show("저장 되었습니다.");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void LoadFromMMF()
		{
			try
			{
				// CreateMyPicObj();  
				IFormatter formatter = new BinaryFormatter();
				stream.Seek(0, SeekOrigin.Begin);
				MyPic obj = formatter.Deserialize(stream) as MyPic;

				if (obj != null)
				{
					bmp = obj.pic;
					info = obj.picInfo;

					bmp.Save(@"j:\b.jpg", bmp.RawFormat);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btnExit_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			DisposeMemoryMappedFile();
		}

		private void btnSaveToMMF_Click(object sender, EventArgs e)
		{
			SaveToMMF();
		}

		private void btnLoadFromMMF_Click(object sender, EventArgs e)
		{
			LoadFromMMF();
		}

		public byte[] GetStreamBytes(Image image)
		{
			byte[] result = null;

			if (image != null)
			{
				using (MemoryStream stream = new MemoryStream())
				{
					image.Save(stream, image.RawFormat);
					result = stream.ToArray();
				}
			}

			return result;
		}

		private Stream GetStream(Image image)
		{
			Stream result = null;
			try
			{
				if (image != null)
				{
					MemoryStream stream = new MemoryStream();

					image.Save(stream, ImageFormat.Jpeg);
					//image.Save(stream, image.RawFormat);
					result = stream;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error converting Bitmap to Stream");
			}

			return result;
		}

		private Bitmap GetBitmap(Stream stream)
		{
			return new Bitmap(stream);
		}

		private void btnSaveToMMFByte_Click(object sender, EventArgs e)
		{
			// with Stream
			//var buffer = GetStreamBytes(bmp);
			//var lengthBytes = BitConverter.GetBytes(buffer.Length);

			//stream.Seek(0L, SeekOrigin.Begin);
			//stream.Write(lengthBytes, 0, lengthBytes.Length);
			//stream.Write(buffer, 0, buffer.Length);

			// with Memory stream
			//var buf = System.Drawing.DrawingExtensions.GetRawData((Bitmap)bmp);
			//var info = new ImageInfo
			//{
			//	Buffer = GetBytes(bmp),
			//	Width = bmp.Width,
			//	PixelFormat = (int)bmp.PixelFormat,
			//};
			//accessor.Write<ImageInfo>(0L, ref info);

			// with Array
			Bitmap bitmap = bmp as Bitmap;
			var buffer = bitmap.GetRawData();
			var intSize = (long)sizeof(int);
			var count = 0L;

			accessor.Write(intSize * count++, bitmap.Width);
			accessor.Write(intSize * count++, (int)bitmap.PixelFormat);
			accessor.Write(intSize * count++, buffer.Length);
			accessor.WriteArray(intSize * count++, buffer, 0, buffer.Length);
		}

		private void btnLoadFromMMFByte_Click(object sender, EventArgs e)
		{
			try
			{
				// with Stream
				//stream.Seek(0L, SeekOrigin.Begin);

				//var lengthBytes = new byte[4];

				//stream.Read(lengthBytes, 0, lengthBytes.Length);

				//var length = BitConverter.ToInt32(lengthBytes, 0);
				//var buffer = new byte[length];
				//var read = stream.Read(buffer, 0, buffer.Length);
				//Bitmap bitmap = null;

				//using (MemoryStream ms = new MemoryStream())
				//{
				//	ms.Write(buffer, 0, buffer.Length);
				//	ms.Seek(0, SeekOrigin.Begin);
				//	bitmap = (Bitmap)Bitmap.FromStream(ms, true, true);
				//}
				//bmp = bitmap;

				//bitmap.Save(@"J:\a.jpg", ImageFormat.Jpeg);


				//var info = new ImageInfo();
				//accessor.Read<ImageInfo>(0L, out info);


				//var bitmap = info.Buffer.ConvertToBitmap(info.Width, (PixelFormat)info.PixelFormat);
				var intSize = (long)sizeof(int);
				var count = 0L;

				var width = accessor.ReadInt32(intSize * count++);
				var pixelFormat = (PixelFormat)accessor.ReadInt32(intSize * count++);
				var bufferLength = accessor.ReadInt32(intSize * count++);
				var buffer = new byte[bufferLength];
				var readLength = accessor.ReadArray(intSize * count++, buffer, 0, bufferLength);
				
				var bitmap = buffer.ConvertToBitmap(width, pixelFormat);

				bmp = bitmap;
				bitmap.Save(@"J:\a.jpg", bitmap.RawFormat);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
	}
}