﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseMMFBetweenProcess2
{
	[Serializable]
	public struct ImageInfo
	{
		/// <summary>
		/// Image를 구하거나 설정합니다.
		/// </summary>
		/// <value>Image를 반환합니다.</value>
		public byte[] Buffer
		{
			get;
			set;
		}

		/// <summary>
		/// Width를 구하거나 설정합니다.
		/// </summary>
		/// <value>Width를 반환합니다.</value>
		public int Width
		{
			get;
			set;
		}

		/// <summary>
		/// PixelFormat를 구하거나 설정합니다.
		/// </summary>
		/// <value>PixelFormat를 반환합니다.</value>
		public int PixelFormat
		{
			get;
			set;
		}
	}
}
