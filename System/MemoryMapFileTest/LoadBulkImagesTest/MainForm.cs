﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoadBulkImagesTest
{
	public partial class MainForm : Form
	{
		#region Constants
		private const string EXT = ".jpg";
		#endregion

		public MainForm()
		{
			InitializeComponent();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			btnLoad.PerformClick();
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			switch (keyData)
			{
				case Keys.Left:
					btnPrev.PerformClick();

					return true;
				case Keys.Right:
					btnNext.PerformClick();

					return true;
				case Keys.Home:
					Position = 0;
					ShiftImages(Position);

					return true;
				case Keys.End:
					Position = Index.Count - 3;
					ShiftImages(Position);

					return true;
				case Keys.PageUp:
					Position = (int)Math.Min(Position, Position - 10);
					Position = (int)Math.Max(Position, 0);
					ShiftImages(Position);

					return true;
				case Keys.PageDown:
					Position = (int)Math.Max(Position, Position + 10);
					Position = (int)Math.Min(Position, Index.Count - 3);
					ShiftImages(Position);

					return true;
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}

		private void PrepareImages()
		{
			var target = tbTarget.Text;
			var items = Directory.GetFiles(target, string.Format("*{0}", EXT), SearchOption.AllDirectories);

			DateTime performanceTestStart120510 = DateTime.Now;
			System.Diagnostics.Debug.WriteLine(string.Format("Start\t{0}", performanceTestStart120510), "MainForm.PrepareImages");

			PrepareDictionary(items.Length);

			foreach (string file in items)
			{
				var mapName = Path.GetFileNameWithoutExtension(file);

				if (Map.ContainsKey(mapName) == true)
				{
					mapName = string.Format(@"{0}_{1}", Path.GetFileNameWithoutExtension(Path.GetDirectoryName(file)), mapName);
				}

				var mmf = MemoryMappedFile.CreateFromFile(file, FileMode.OpenOrCreate, mapName);

				Map.Add(mapName, mmf);
			}

			Index = Map.Keys.Select((key, idx) => new
			{
				Index = idx,
				Key = key,
			})
			.ToDictionary(x => x.Index, x => x.Key);

			System.Diagnostics.Debug.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart120510), "MainForm.PrepareImages");

			Status = string.Format("{0} 에서 {1:#,##0} 건의 파일에 대한 Memory Mapped File을 {2} 초동안 만들었습니다.", target, Index.Count, DateTime.Now - performanceTestStart120510);
		}

		private void PrepareDictionary(int count)
		{
			if (Map != null)
			{
				foreach (string key in Map.Keys)
				{
					Map[key].Dispose();
				}

				Map.Clear();
			}

			if (Index != null)
			{
				Index.Clear();
			}

			Map = new Dictionary<string, MemoryMappedFile>(count);
			Index = new Dictionary<int, string>(count);
		}

		private Dictionary<string, MemoryMappedFile> Map
		{
			get;
			set;
		}

		private Dictionary<int, string> Index
		{
			get;
			set;
		}

		private int Position
		{
			get;
			set;
		}

		private string Status
		{
			get
			{
				return tsStatus.Text;
			}
			set
			{
				tsStatus.Text = value;
			}
		}

		private void btnPrev_Click(object sender, EventArgs e)
		{
			if (Position <= 0)
			{
				Position = 0;
			}
			else
			{
				Position--;
			}

			ShiftImages(Position);
		}

		private void btnNext_Click(object sender, EventArgs e)
		{
			if (Position > Index.Count - 1)
			{
				Position = Index.Count - 1;
			}
			else
			{
				Position++;
			}

			ShiftImages(Position);
		}

		private Graphics _dc1;
		private Graphics _dc2;
		private Graphics _dc3;

		private void btnLoad_Click(object sender, EventArgs e)
		{
			_dc1 = pbPrev.CreateGraphics();
			_dc2 = pbCurrent.CreateGraphics();
			_dc3 = pbNext.CreateGraphics();

			this.SetStyle(ControlStyles.DoubleBuffer, true);
			this.SetStyle(ControlStyles.UserPaint, true);
			this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

			PrepareImages();
			ShiftImages(0);
		}

		private void ShiftImages(int startPosition)
		{
			LoadImage(startPosition + 0, _dc1, pbPrev, tbPrev);
			LoadImage(startPosition + 1, _dc2, pbCurrent, tbCurrent);
			LoadImage(startPosition + 2, _dc3, pbNext, tbNext);
		}

		private void LoadImage(int position, Graphics dc, PictureBox targetControl, TextBox targetTextControl)
		{
			var key = Index[position];
			var mmf = Map[key];

			DateTime performanceTestStart120930 = DateTime.Now;
			System.Diagnostics.Debug.WriteLine(string.Format("Start\t{0}", performanceTestStart120930), "MainForm.LoadImage");

			using (var stream = mmf.CreateViewStream())
			{
				using (BufferedGraphics bufferedgraphic = BufferedGraphicsManager.Current.Allocate(dc, targetControl.ClientRectangle))
				{
					//bufferedgraphic.Graphics.Clear(Color.Silver);
					//bufferedgraphic.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
					//bufferedgraphic.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
					//bufferedgraphic.Graphics.TranslateTransform(this.AutoScrollPosition.X, this.AutoScrollPosition.Y);
					bufferedgraphic.Graphics.DrawImage(Image.FromStream(stream), targetControl.ClientRectangle);
					bufferedgraphic.Render(dc);
				}

				//if (targetControl.Image != null)
				//{
				//	targetControl.Image.Dispose();
				//}

				//targetControl.Image = Image.FromStream(stream);
			}

			TimeSpan estimatedTime = DateTime.Now - performanceTestStart120930;
			System.Diagnostics.Debug.WriteLine(string.Format("Stop \t{0}", estimatedTime), "MainForm.LoadImage");

			targetTextControl.Text = string.Format("{0}: {1} of {2} in {3}", key, position + 1, Index.Count, estimatedTime);
		}
	}
}
