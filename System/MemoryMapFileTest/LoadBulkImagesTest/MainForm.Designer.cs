﻿namespace LoadBulkImagesTest
{
	partial class MainForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbTarget = new System.Windows.Forms.TextBox();
			this.btnPrev = new System.Windows.Forms.Button();
			this.pbPrev = new System.Windows.Forms.PictureBox();
			this.pbNext = new System.Windows.Forms.PictureBox();
			this.btnNext = new System.Windows.Forms.Button();
			this.tbPrev = new System.Windows.Forms.TextBox();
			this.tbNext = new System.Windows.Forms.TextBox();
			this.pbCurrent = new System.Windows.Forms.PictureBox();
			this.tbCurrent = new System.Windows.Forms.TextBox();
			this.btnLoad = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.tsStatus = new System.Windows.Forms.ToolStripStatusLabel();
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			((System.ComponentModel.ISupportInitialize)(this.pbPrev)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbNext)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbCurrent)).BeginInit();
			this.statusStrip1.SuspendLayout();
			this.tlpMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// tbTarget
			// 
			this.tlpMain.SetColumnSpan(this.tbTarget, 3);
			this.tbTarget.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbTarget.Location = new System.Drawing.Point(3, 4);
			this.tbTarget.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tbTarget.Name = "tbTarget";
			this.tbTarget.Size = new System.Drawing.Size(1348, 25);
			this.tbTarget.TabIndex = 0;
			this.tbTarget.Text = "D:\\R-PassImages";
			// 
			// btnPrev
			// 
			this.btnPrev.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnPrev.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
			this.btnPrev.Location = new System.Drawing.Point(3, 34);
			this.btnPrev.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.btnPrev.Name = "btnPrev";
			this.btnPrev.Size = new System.Drawing.Size(445, 28);
			this.btnPrev.TabIndex = 1;
			this.btnPrev.Text = "<- &Prev, PageUp,  Home";
			this.btnPrev.UseVisualStyleBackColor = true;
			this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
			// 
			// pbPrev
			// 
			this.pbPrev.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbPrev.Location = new System.Drawing.Point(3, 70);
			this.pbPrev.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.pbPrev.Name = "pbPrev";
			this.pbPrev.Size = new System.Drawing.Size(445, 465);
			this.pbPrev.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pbPrev.TabIndex = 2;
			this.pbPrev.TabStop = false;
			// 
			// pbNext
			// 
			this.pbNext.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbNext.Location = new System.Drawing.Point(905, 70);
			this.pbNext.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.pbNext.Name = "pbNext";
			this.pbNext.Size = new System.Drawing.Size(446, 465);
			this.pbNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pbNext.TabIndex = 2;
			this.pbNext.TabStop = false;
			// 
			// btnNext
			// 
			this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnNext.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
			this.btnNext.Location = new System.Drawing.Point(905, 34);
			this.btnNext.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new System.Drawing.Size(446, 28);
			this.btnNext.TabIndex = 2;
			this.btnNext.Text = "&Next, PageDown, End ->";
			this.btnNext.UseVisualStyleBackColor = true;
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// tbPrev
			// 
			this.tbPrev.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbPrev.Location = new System.Drawing.Point(3, 543);
			this.tbPrev.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tbPrev.Name = "tbPrev";
			this.tbPrev.Size = new System.Drawing.Size(445, 25);
			this.tbPrev.TabIndex = 3;
			// 
			// tbNext
			// 
			this.tbNext.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbNext.Location = new System.Drawing.Point(905, 543);
			this.tbNext.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tbNext.Name = "tbNext";
			this.tbNext.Size = new System.Drawing.Size(446, 25);
			this.tbNext.TabIndex = 5;
			// 
			// pbCurrent
			// 
			this.pbCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbCurrent.Location = new System.Drawing.Point(454, 70);
			this.pbCurrent.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.pbCurrent.Name = "pbCurrent";
			this.pbCurrent.Size = new System.Drawing.Size(445, 465);
			this.pbCurrent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pbCurrent.TabIndex = 2;
			this.pbCurrent.TabStop = false;
			// 
			// tbCurrent
			// 
			this.tbCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbCurrent.Location = new System.Drawing.Point(454, 543);
			this.tbCurrent.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tbCurrent.Name = "tbCurrent";
			this.tbCurrent.Size = new System.Drawing.Size(445, 25);
			this.tbCurrent.TabIndex = 4;
			// 
			// btnLoad
			// 
			this.btnLoad.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnLoad.Location = new System.Drawing.Point(454, 34);
			this.btnLoad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(445, 28);
			this.btnLoad.TabIndex = 1;
			this.btnLoad.Text = "&Load";
			this.btnLoad.UseVisualStyleBackColor = true;
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStatus});
			this.statusStrip1.Location = new System.Drawing.Point(0, 569);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(1354, 22);
			this.statusStrip1.TabIndex = 6;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// tsStatus
			// 
			this.tsStatus.AutoToolTip = true;
			this.tsStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.tsStatus.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.tsStatus.Name = "tsStatus";
			this.tsStatus.Size = new System.Drawing.Size(1339, 17);
			this.tsStatus.Spring = true;
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 3;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tlpMain.Controls.Add(this.tbPrev, 0, 3);
			this.tlpMain.Controls.Add(this.tbCurrent, 1, 3);
			this.tlpMain.Controls.Add(this.tbTarget, 0, 0);
			this.tlpMain.Controls.Add(this.btnNext, 2, 1);
			this.tlpMain.Controls.Add(this.pbNext, 2, 2);
			this.tlpMain.Controls.Add(this.btnLoad, 1, 1);
			this.tlpMain.Controls.Add(this.tbNext, 2, 3);
			this.tlpMain.Controls.Add(this.btnPrev, 0, 1);
			this.tlpMain.Controls.Add(this.pbCurrent, 1, 2);
			this.tlpMain.Controls.Add(this.pbPrev, 0, 2);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 4;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tlpMain.Size = new System.Drawing.Size(1354, 569);
			this.tlpMain.TabIndex = 7;
			// 
			// MainForm
			// 
			this.AcceptButton = this.btnNext;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1354, 591);
			this.Controls.Add(this.tlpMain);
			this.Controls.Add(this.statusStrip1);
			this.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.KeyPreview = true;
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "MemoryMappedFile Image loader";
			((System.ComponentModel.ISupportInitialize)(this.pbPrev)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbNext)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbCurrent)).EndInit();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.tlpMain.ResumeLayout(false);
			this.tlpMain.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox tbTarget;
		private System.Windows.Forms.Button btnPrev;
		private System.Windows.Forms.PictureBox pbPrev;
		private System.Windows.Forms.PictureBox pbNext;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.TextBox tbPrev;
		private System.Windows.Forms.TextBox tbNext;
		private System.Windows.Forms.PictureBox pbCurrent;
		private System.Windows.Forms.TextBox tbCurrent;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel tsStatus;
		private System.Windows.Forms.TableLayoutPanel tlpMain;
	}
}

