﻿// ****************************************************************************************************************** //
//	Domain		:	UseMMFBetweenProcess.frmMain
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2015년 1월 17일 토요일 오전 10:27
//	Purpose		:	MemoryMappedFile test
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	http://www.cprogramdevelop.com/1320558/
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="frmMain.cs" company="(주)가치소프트">
//		Copyright (c) 2015. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.MemoryMappedFiles;

namespace UseMMFBetweenProcess
{
	public partial class frmMain : Form
	{
		public frmMain()
		{
			InitializeComponent();
			InitMemoryMappedFile();
		}

		/// <summary>  
		///   
		/// </summary>  
		private const int FileSize = 1024 * 1024;
		private MemoryMappedFile file = null;
		private MemoryMappedViewAccessor accessor = null;

		/// <summary>  
		///   
		/// </summary>  
		private void InitMemoryMappedFile()
		{
			file = MemoryMappedFile.CreateOrOpen("UseMMFBetweenProcess", FileSize);
			accessor = file.CreateViewAccessor();
			lblInfo.Text = "";
		}

		/// <summary>  
		///   
		/// </summary>  
		private MyStructure data;

		/// <summary>  
		///   
		/// </summary>  
		private void ShowData()
		{
			textBox1.Text = data.IntValue.ToString();
			textBox2.Text = data.FloatValue.ToString();
		}

		/// <summary>  
		///   
		/// </summary>  
		private void UpdateData()
		{
			data.IntValue = int.Parse(textBox1.Text);
			data.FloatValue = float.Parse(textBox2.Text);
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				UpdateData();
				accessor.Write<MyStructure>(0, ref data);
				lblInfo.Text = "";
			}
			catch (Exception ex)
			{
				lblInfo.Text = ex.Message;
			}
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			accessor.Read<MyStructure>(0, out data);
			ShowData();
			lblInfo.Text = "";
		}

		private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (accessor != null)
				accessor.Dispose();
			if (file != null)
				file.Dispose();
		}
	}
}