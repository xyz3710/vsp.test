﻿// ****************************************************************************************************************** //
//	Domain		:	UseMMFInProcess.frmMain
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2015년 1월 17일 토요일 오전 10:27
//	Purpose		:	MemoryMappedFile test
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	http://www.cprogramdevelop.com/1320558/
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="frmMain.cs" company="(주)가치소프트">
//		Copyright (c) 2015. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.MemoryMappedFiles;

namespace UseMMFInProcess
{
	public partial class frmMain : Form
	{
		public frmMain()
		{
			InitializeComponent();
			CreateMemoryMapFile();
		}
		
		private const int FILE_SIZE = 512;
		/// <summary>  
		///   
		/// </summary>  
		private MemoryMappedFile memoryFile = null;
		/// <summary>  
		///   
		/// </summary>  
		private MemoryMappedViewAccessor accessor1, accessor2, accessor;
		/// <summary>  
		///   
		/// </summary>  
		private void CreateMemoryMapFile()
		{
			try
			{
				memoryFile = MemoryMappedFile.CreateFromFile("MyFile.dat", FileMode.OpenOrCreate, "MyFile", FILE_SIZE);
				//  
				accessor1 = memoryFile.CreateViewAccessor(0, FILE_SIZE / 2);
				//  
				accessor2 = memoryFile.CreateViewAccessor(FILE_SIZE / 2, FILE_SIZE / 2);
				//  
				accessor = memoryFile.CreateViewAccessor();
				//InitFileContent();  
				lblInfo.Text = "";
				ShowFileContents();
			}
			catch (Exception ex)
			{
				lblInfo.Text = ex.Message;
			}
		}
		/// <summary>  
		///   
		/// </summary>  
		private void DisposeMemoryMapFile()
		{
			if (accessor1 != null)
				accessor1.Dispose();
			if (accessor2 != null)
				accessor2.Dispose();
			if (memoryFile != null)
				memoryFile.Dispose();
		}

		private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			DisposeMemoryMapFile();
		}

		private void btnWrite1_Click(object sender, EventArgs e)
		{
			if (textBox1.Text.Length == 0)
			{
				lblInfo.Text = "";
				return;
			}
			char[] chs = textBox1.Text.ToCharArray();
			char ch = chs[0];

			for (int i = 0; i < FILE_SIZE / 2; i += 2)
				accessor1.Write(i, ch);

			lblInfo.Text = "“" + ch + "”";
			ShowFileContents();
		}

		private void btnShow_Click(object sender, EventArgs e)
		{
			ShowFileContents();
		}

		/// <summary>  
		/// “0”  
		/// </summary>  
		private void InitFileContent()
		{
			for (int i = 0; i < FILE_SIZE; i += 2)
				accessor.Write(i, '0');
		}
		/// <summary>  
		///   
		/// </summary>  
		private void ShowFileContents()
		{
			StringBuilder sb = new StringBuilder(FILE_SIZE);
			sb.Append("?\n");

			int j = 0;
			for (int i = 0; i < FILE_SIZE / 2; i += 2)
			{
				sb.Append("\t");
				char ch = accessor.ReadChar(i);
				sb.Append(j);
				sb.Append(":");
				sb.Append(ch);
				j++;
			}
			sb.Append("\n?\n");

			for (int i = FILE_SIZE / 2; i < FILE_SIZE; i += 2)
			{
				sb.Append("\t");
				char ch = accessor.ReadChar(i);
				sb.Append(j);
				sb.Append(":");
				sb.Append(ch);
				j++;
			}
			richTextBox1.Text = sb.ToString();
		}

		private void btnWrite2_Click(object sender, EventArgs e)
		{
			if (textBox2.Text.Length == 0)
			{
				lblInfo.Text = "";
				return;
			}
			char[] chs = textBox2.Text.ToCharArray();
			char ch = chs[0];

			for (int i = 0; i < FILE_SIZE / 2; i += 2)
				accessor2.Write(i, ch);
			lblInfo.Text = "“" + ch + "”";
			ShowFileContents();
		}
	}
}