﻿namespace UseMMFInProcess
{
	partial class frmMain
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblInfo = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.btnWrite1 = new System.Windows.Forms.Button();
			this.btnShow = new System.Windows.Forms.Button();
			this.btnWrite2 = new System.Windows.Forms.Button();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// lblInfo
			// 
			this.lblInfo.AutoSize = true;
			this.lblInfo.Location = new System.Drawing.Point(12, 21);
			this.lblInfo.Name = "lblInfo";
			this.lblInfo.Size = new System.Drawing.Size(47, 19);
			this.lblInfo.TabIndex = 0;
			this.lblInfo.Text = "lblInfo";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(16, 53);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(114, 25);
			this.textBox1.TabIndex = 0;
			// 
			// btnWrite1
			// 
			this.btnWrite1.Location = new System.Drawing.Point(145, 53);
			this.btnWrite1.Name = "btnWrite1";
			this.btnWrite1.Size = new System.Drawing.Size(114, 25);
			this.btnWrite1.TabIndex = 1;
			this.btnWrite1.Text = "Write&1";
			this.btnWrite1.UseVisualStyleBackColor = true;
			this.btnWrite1.Click += new System.EventHandler(this.btnWrite1_Click);
			// 
			// btnShow
			// 
			this.btnShow.Location = new System.Drawing.Point(16, 115);
			this.btnShow.Name = "btnShow";
			this.btnShow.Size = new System.Drawing.Size(243, 25);
			this.btnShow.TabIndex = 4;
			this.btnShow.Text = "&Show";
			this.btnShow.UseVisualStyleBackColor = true;
			this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
			// 
			// btnWrite2
			// 
			this.btnWrite2.Location = new System.Drawing.Point(145, 84);
			this.btnWrite2.Name = "btnWrite2";
			this.btnWrite2.Size = new System.Drawing.Size(114, 25);
			this.btnWrite2.TabIndex = 3;
			this.btnWrite2.Text = "Write&2";
			this.btnWrite2.UseVisualStyleBackColor = true;
			this.btnWrite2.Click += new System.EventHandler(this.btnWrite2_Click);
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(16, 84);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(114, 25);
			this.textBox2.TabIndex = 2;
			// 
			// richTextBox1
			// 
			this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.richTextBox1.Location = new System.Drawing.Point(273, 12);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(498, 755);
			this.richTextBox1.TabIndex = 3;
			this.richTextBox1.Text = "";
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(783, 779);
			this.Controls.Add(this.richTextBox1);
			this.Controls.Add(this.btnShow);
			this.Controls.Add(this.btnWrite2);
			this.Controls.Add(this.btnWrite1);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.lblInfo);
			this.Font = new System.Drawing.Font("맑은 고딕", 10F);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "UseMMFInProcess";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblInfo;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button btnWrite1;
		private System.Windows.Forms.Button btnShow;
		private System.Windows.Forms.Button btnWrite2;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.RichTextBox richTextBox1;
	}
}

