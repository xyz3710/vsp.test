﻿// ****************************************************************************************************************** //
//	Domain		:	NPMemoryMap01.Program
//	Creator		:	X10-MOBILE\xyz37(Kim Ki Won)
//	Create		:	2015년 1월 15일 목요일 오후 8:51
//	Purpose		:	http://msdn.microsoft.com/ko-kr/library/dd997372(v=vs.110).aspx
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="Program.cs" company="(주)가치소프트">
//		Copyright (c) 2015. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NPMemoryMap01
{
	class Program
	{
		static void Main(string[] args)
		{
			using (MemoryMappedFile mmf = MemoryMappedFile.CreateNew("testmap", 10240))
			{
				bool mutexCreated;
				Mutex mutex = new Mutex(true, "testmapmutex", out mutexCreated);

				/*
				string message = "비지속형 메모리 맵 파일 테스트 입니다.";
				var buffer = Encoding.UTF8.GetBytes(message);
				var data = new List<byte>();

				data.AddRange(BitConverter.GetBytes(buffer.Length));
				data.AddRange(buffer);
				*/

				using (MemoryMappedViewStream stream = mmf.CreateViewStream())
				{
					BinaryWriter writer = new BinaryWriter(stream);
					writer.Write(1);
					//writer.Write(data.ToArray());
				}

				mutex.ReleaseMutex();

				Console.WriteLine("Start Process B and press ENTER to continue.");
				Console.ReadLine();

				Console.WriteLine("Start Process C and press ENTER to continue.");
				Console.ReadLine();

				mutex.WaitOne();
				using (MemoryMappedViewStream stream = mmf.CreateViewStream())
				{
					BinaryReader reader = new BinaryReader(stream);
					Console.WriteLine("Process A says: {0}", reader.ReadBoolean());
					Console.WriteLine("Process B says: {0}", reader.ReadBoolean());
					Console.WriteLine("Process C says: {0}", reader.ReadBoolean());
				}
				mutex.ReleaseMutex();
			}
		}
	}
}
