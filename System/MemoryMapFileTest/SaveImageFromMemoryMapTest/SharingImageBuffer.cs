﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace LoadImageToMemoryMapTest
{
	class SharingImageBufferTest
	{
		#region Constants
		private const string MMF_NAME = "testMMF";
		#endregion

		static void Main(string[] args)
		{
			Console.WriteLine("\r\nArguments list: \r\n");
			Console.WriteLine("0: Write with RawData");
			Console.WriteLine("1: Write Serialize with MemoryStream");
			Console.WriteLine("2: Write direct Serialize");
			Console.WriteLine("\r\n");

			var arg = "2";

			if (args.Length > 0)
			{
				arg = args[0];
			}

			var target = @".\Images\DestImage.jpg";
			Action<string, int> action = null;

			switch (arg)
			{
				case "0":
					action = ReadRawData;

					break;
				case "1":
					action = ReadSerialize;

					break;
				case "2":
					action = ReadSerializeDirect;

					break;
			}

			string dirName = Path.GetDirectoryName(target);
			var targetName = Path.Combine(dirName, Path.GetFileNameWithoutExtension(target));

			dirName.CheckDirectory();

			DateTime performanceTestStart172305 = DateTime.Now;

			for (int i = 0; i < 100; i++)
			{
				action(targetName, i);
			}

			Console.WriteLine(string.Format("Total \t{0}", DateTime.Now - performanceTestStart172305), "SharingImageBufferTest.Main");
			Console.ReadLine();
		}

		private static void ReadRawData(string target, int step)
		{
			DateTime performanceTestStart164916 = DateTime.Now;
			Console.WriteLine(string.Format("Start\t{0}", performanceTestStart164916), "Program.Main");

			var sizeOfInt = (long)sizeof(int);
			var count = 0;
			string mmfName = string.Format("{0}{1:000}", MMF_NAME, step);

			using (var mmf = MemoryMappedFile.OpenExisting(mmfName))
			{
				var view = mmf.CreateViewAccessor();
				var length = view.ReadInt32(sizeOfInt * count++);
				var buffer = new byte[length];
				var width = view.ReadInt32(sizeOfInt * count++);
				var pixelFormat = (PixelFormat)view.ReadInt32(sizeOfInt * count++);
				var readLen = view.ReadArray(sizeOfInt * count++, buffer, 0, length);

				Console.WriteLine("length: {0}, width: {1}, pixelFormat: {2}\t from {3}", length, width, pixelFormat, mmfName);

				using (var bitmap = buffer.ConvertToBitmap(width, pixelFormat))
				{
					bitmap.Save(string.Format("{0}{1:000}.jpg", target, step));
				}

				Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart164916), "Program.Main");
			}
		}

		private static void ReadSerialize(string target, int step)
		{
			DateTime performanceTestStart164916 = DateTime.Now;
			Console.WriteLine(string.Format("Start\t{0}", performanceTestStart164916), "Program.Main");

			var sizeOfInt = (long)sizeof(int);
			var count = 0;
			string mmfName = string.Format("{0}{1:000}", MMF_NAME, step);

			using (var mmf = MemoryMappedFile.OpenExisting(mmfName))
			{
				var view = mmf.CreateViewAccessor();
				var length = view.ReadInt32(sizeOfInt * count++);
				var buffer = new byte[length];

				view.ReadArray(sizeOfInt * count++, buffer, 0, length);

				IFormatter formatter = new BinaryFormatter();
				Bitmap returnObject = null;

				using (var ms = new MemoryStream(buffer))
				{
					returnObject = (Bitmap)formatter.Deserialize(ms);
				}

				var rawData = returnObject.GetRawData();

				Console.WriteLine("length: {0}, width: {1}, pixelFormat: {2}\t from {3}", length, returnObject.Width, returnObject.PixelFormat, mmfName);
				//returnObject.Save(string.Format("{0}{1:000}.jpg", target, step));
				returnObject.Dispose();
				Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart164916), "Program.Main");
			}
		}

		static IFormatter formatter = new BinaryFormatter();

		private static void ReadSerializeDirect(string target, int step)
		{
			DateTime performanceTestStart164916 = DateTime.Now;
			Console.WriteLine(string.Format("Start\t{0}", performanceTestStart164916), "Program.Main");

			string mmfName = string.Format("{0}{1:000}", MMF_NAME, step);

			using (var mmf = MemoryMappedFile.OpenExisting(mmfName))
			{
				var view = mmf.CreateViewStream();
				Bitmap returnObject = null;

				returnObject = (Bitmap)formatter.Deserialize(view);

				var rawData = returnObject.GetRawData();

				Console.WriteLine("length: {0}, width: {1}, pixelFormat: {2}\t from {3}", view.Length, returnObject.Width, returnObject.PixelFormat, mmfName);
				returnObject.Save(string.Format("{0}{1:000}.jpg", target, step));
				returnObject.Dispose();
				Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart164916), "Program.Main");
			}
		}
	}
}
