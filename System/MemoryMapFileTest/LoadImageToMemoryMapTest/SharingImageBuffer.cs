﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace LoadImageToMemoryMapTest
{
	class SharingImageBufferTest
	{
		#region Constants
		private const string MMF_NAME = "testMMF";
		#endregion

		private static Dictionary<string, MemoryMappedFile> _dic;

		static void Main(string[] args)
		{
			Console.WriteLine("\r\nArguments list: \r\n");
			Console.WriteLine("0: Write with RawData");
			Console.WriteLine("1: Write Serialize with MemoryStream");
			Console.WriteLine("2: Write direct Serialize");
			Console.WriteLine("\r\n");

			var arg = "2";

			if (args.Length > 0)
			{
				arg = args[0];
			}

			var target = @".\SourceImage.bmp";
			Action<string, int> action = null;

			switch (arg)
			{
				case "0":
					action = WriteRawData;

					break;
				case "1":
					action = WriteSerialize;

					break;
				case "2":
					action = WriteSerializeDirect;

					break;
			}

			_dic = new Dictionary<string, MemoryMappedFile>();
			
			DateTime performanceTestStart172305 = DateTime.Now;

			for (int i = 0; i < 100; i++)
			{
				action(target, i);
			}

			Console.WriteLine(string.Format("Total \t{0}", DateTime.Now - performanceTestStart172305), "SharingImageBufferTest.Main");
			Console.ReadLine();

			foreach (MemoryMappedFile mmf in _dic.Values)
			{
				mmf.Dispose();
			}
		}

		private static void WriteRawData(string target, int step)
		{
			DateTime performanceTestStart164916 = DateTime.Now;
			Console.WriteLine(string.Format("Start\t{0}", performanceTestStart164916), "Program.Main");

			var sizeOfInt = (long)sizeof(int);
			var count = 0;
			string mmfName = string.Format("{0}{1:000}", MMF_NAME, step);

			var mmf = MemoryMappedFile.CreateOrOpen(mmfName, (long)Math.Pow(1024, 2) * 45);
			var view = mmf.CreateViewAccessor();

			_dic.Add(mmfName, mmf);

			var image = ImageFast.FromFile(target);
			byte[] buffer = ((Bitmap)image).GetRawData();

			view.Write(sizeOfInt * count++, buffer.Length);
			view.Write(sizeOfInt * count++, image.Width);
			view.Write(sizeOfInt * count++, (int)image.PixelFormat);
			view.WriteArray(sizeOfInt * count++, buffer, 0, buffer.Length);

			Console.WriteLine("length: {0}, width: {1}, pixelFormat: {2}\t to {3}", buffer.Length, image.Width, image.PixelFormat, mmfName);
			Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart164916), "Program.Main");
		}

		private static void WriteSerialize(string target, int step)
		{
			DateTime performanceTestStart164916 = DateTime.Now;
			Console.WriteLine(string.Format("Start\t{0}", performanceTestStart164916), "Program.Main");

			var sizeOfInt = (long)sizeof(int);
			var count = 0;
			string mmfName = string.Format("{0}{1:000}", MMF_NAME, step);

			var mmf = MemoryMappedFile.CreateOrOpen(mmfName, (long)Math.Pow(1024, 2) * 45);
			var view = mmf.CreateViewAccessor();

			_dic.Add(mmfName, mmf);

			var image = ImageFast.FromFile(target);
			IFormatter formatter = new BinaryFormatter();
			byte[] buffer = null;

			using (var ms = new MemoryStream())
			{
				formatter.Serialize(ms, (Bitmap)image);
				buffer = ms.ToArray();
			}

			view.Write(sizeOfInt * count++, buffer.Length);
			view.WriteArray(sizeOfInt * count++, buffer, 0, buffer.Length);

			Console.WriteLine("length: {0}, width: {1}, pixelFormat: {2}\t to {3}", buffer.Length, image.Width, image.PixelFormat, mmfName);
			Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart164916), "Program.Main");
		}

		static IFormatter formatter = new BinaryFormatter();

		private static void WriteSerializeDirect(string target, int step)
		{
			DateTime performanceTestStart164916 = DateTime.Now;
			Console.WriteLine(string.Format("Start\t{0}", performanceTestStart164916), "Program.Main");

			string mmfName = string.Format("{0}{1:000}", MMF_NAME, step);
			var mmf = MemoryMappedFile.CreateOrOpen(mmfName, (long)Math.Pow(1024, 2) * 45);
			var view = mmf.CreateViewStream();

			_dic.Add(mmfName, mmf);

			var image = ImageFast.FromFile(target);

			formatter.Serialize(view, (Bitmap)image);

			Console.WriteLine("length: {0}, width: {1}, pixelFormat: {2}\t to {3}", view.Length, image.Width, image.PixelFormat, mmfName);
			Console.WriteLine(string.Format("Stop \t{0}", DateTime.Now - performanceTestStart164916), "Program.Main");
		}
	}
}
