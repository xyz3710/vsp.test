﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NPMemoryMap_Proc2
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				using (MemoryMappedFile mmf = MemoryMappedFile.OpenExisting("testmap"))
				{
					Mutex mutex = Mutex.OpenExisting("testmapmutex");
					mutex.WaitOne();

					using (MemoryMappedViewStream stream = mmf.CreateViewStream(1, 0))
					{
						/*
						BinaryReader reader = new BinaryReader(stream);

						var lengthBytes = reader.ReadBytes(4);
						var length = BitConverter.ToInt32(lengthBytes, 0);
						var dataBytes = reader.ReadBytes(length);
						var data = Encoding.UTF8.GetString(dataBytes);
						*/
						BinaryWriter writer = new BinaryWriter(stream);
						writer.Write(1);
					}
					mutex.ReleaseMutex();
				}
			}
			catch (FileNotFoundException)
			{
				Console.WriteLine("Memory-mapped file does not exist. Run Process A first.");
			}
		}
	}
}
