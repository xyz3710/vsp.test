﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Text;

namespace UniqueId
{
	class Program
	{
		static void Main(string[] args)
		{
			string cpuId = WmiHelper.GetProcessorId();
			string mainboardId = WmiHelper.GetMainboardAndSerial("_");
			string macAddress = WmiHelper.GetMacAddresses("_");

			Console.WriteLine("CPU ID  : {0}", cpuId);
			Console.WriteLine("M/B ID  : {0}", mainboardId);
			Console.WriteLine("MAC Addr: {0}", macAddress);

			List<string> macAddresses = WmiHelper.GetMacAddresses();

			//macAddresses.Sort((a, b) => a.CompareTo(b));	// Ascending
			//macAddresses.Sort((a, b) => 1- * a.CompareTo(b));	// Descending

			foreach (string ma in macAddresses)
			{
				Console.WriteLine(ma);
			}

			//WmiTest();
		}

		private static string GetWmiValues(string className, string selector)
		{
			string delimiter = string.Empty;
			string condition = string.Empty;

			return GetWmiValues(className, condition, delimiter, selector);
		}

		private static string GetWmiValues(string className, string condition, string delimiter, params string[] selectors)
		{
			StringBuilder selector = new StringBuilder();
			StringBuilder result = new StringBuilder();

			foreach (string sel in selectors)
			{
				selector.AppendFormat("{0}, ", sel);
			}

			string query = string.Format("SELECT {0} FROM {1}", selector.ToString(0, selector.Length - 2), className);

			if (string.IsNullOrEmpty(condition) == false)
			{
				query = string.Format("{0} Where {1}", query, condition);
			}

			using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(query))
			{
				using (ManagementObjectCollection mgmtCollection = searcher.Get())
				{
					foreach (ManagementObject mgmt in mgmtCollection)
					{
						if (mgmt != null)
						{
							foreach (string sel in selectors)
							{
								result.AppendFormat("{0}{1}", mgmt.Properties[sel].Value, delimiter);
							}
						}
						else
						{
							mgmt.Dispose();
						}
					}
				}
			}

			return result.ToString(0, result.Length - delimiter.Length);
		}
		
		private static string GetCpuId()
		{
			const string key = "ProcessorID";
			string value = string.Empty;

			using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT ProcessorID FROM Win32_processor"))
			{
				ManagementObjectCollection mgmtCollection = searcher.Get();

				foreach (ManagementObject mgmt in mgmtCollection)
				{
					if (mgmt != null)
						value = Convert.ToString(mgmt.Properties[key].Value);

					if (string.IsNullOrEmpty(value) == false)
						break;
				}
			}

			return value;
		}

		private static string GetMainboardId()
		{
			const string productKey = "Product";
			const string serialKey = "SerialNumber";
			string value = string.Empty;

			using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT Product, SerialNumber FROM Win32_BaseBoard"))
			{
				ManagementObjectCollection mgmtCollection = searcher.Get();

				foreach (ManagementObject mgmt in mgmtCollection)
				{
					if (mgmt != null)
					{
						value = string.Format("{0}_{1}", mgmt.Properties[productKey].Value, mgmt.Properties[serialKey].Value);
					}

					if (string.IsNullOrEmpty(value) == false)
						break;
				}
			}

			return value;
		}

		private static string GetMacAddress()
		{
			const string key = "MACAddress";
			string value = string.Empty;

			using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT MACAddress FROM Win32_NetworkAdapter Where AdapterType='Ethernet 802.3' Or AdapterType='이더넷 802.3'"))
			{
				ManagementObjectCollection mgmtCollection = searcher.Get();

				foreach (ManagementObject mgmt in mgmtCollection)
				{
					if (mgmt != null)
						value = Convert.ToString(mgmt.Properties[key].Value);

					if (string.IsNullOrEmpty(value) == false)
						break;
				}
			}

			return value;
		}

		private static void WmiTest()
		{
			string[] classNames = GetClassNames();

			foreach (string className in classNames)
			{
				Console.WriteLine("================= {0} =================", className);

				using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(string.Format("SELECT * FROM {0}", className)))
				{
					ManagementObjectCollection mgmtCollection = searcher.Get();
					foreach (ManagementObject mgmt in mgmtCollection)
					{
						foreach (PropertyData data in mgmt.Properties)
							Console.WriteLine("{0} = {1}", data.Name, data.Value);
					}
				}
			}
		}

		private static string[] GetClassNames()
		{
			return new string[]
			{
				"Win32_Bios",
				"Win32_BaseBoard",
				"Win32_DiskDrive",
				"Win32_OperatingSystem",
				"Win32_Processor",
				"Win32_ComputerSystem",
				"Win32_StartupCommand",
				"Win32_KeyBoard",
				"Win32_Printer",
				"Win32_SystemDevices",
			};
		}
	}
}
