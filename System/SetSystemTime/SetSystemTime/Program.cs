﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace SetSystemTime
{
	class Program
	{
		static void Main(string[] args)
		{
			var now = DateTime.Now;

			SetSystemTime(now.AddHours(3));
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct SystemTime
		{
			public short Year;
			public short Month;
			public short DayOfWeek;
			public short Day;
			public short Hour;
			public short Minute;
			public short Second;
			public short Millisecond;
		}

		[DllImport("kernel32.dll", EntryPoint = "GetSystemTime", SetLastError = true)]
		public extern static void Win32GetSystemTime(ref SystemTime sysTime);

		[DllImport("kernel32.dll", EntryPoint = "SetSystemTime", SetLastError = true)]
		public extern static bool Win32SetSystemTime(ref SystemTime sysTime);

		/// <summary>
		/// Sets the system time.
		/// <remarks>dateTime convert universal time then set.</remarks>
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		public static void SetSystemTime(DateTime dateTime)
		{
			var universalTime = dateTime.ToUniversalTime();

			SystemTime sysTime = new SystemTime()
			{
				Year = (short)universalTime.Year,
				Month = (short)universalTime.Month,
				Day = (short)universalTime.Day,
				Hour = (short)universalTime.Hour,
				Minute = (short)universalTime.Minute,
				Second = (short)universalTime.Second,
				Millisecond = (short)universalTime.Millisecond,
			};

			Win32SetSystemTime(ref sysTime);
		}
	}
}
