﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;

namespace RegistryImport
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			RegistryBackup regBackup = new RegistryBackup();

			regBackup.loadSetting();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			(new RegistryBackup()).saveSetting();
		}
	}

	internal class RegistryBackup
	{
		private const string pathSoftware = "Software";
		private const string pathCompany = "Edudas";

		public void loadSetting()
		{
			// import to registry sub key
			RegistryInfo regInfo = importSubKey();

			// write registry sub key to registry
			writeSubKey(regInfo);
		}

		private RegistryInfo importSubKey()
		{
			OpenFileDialog ofd = new OpenFileDialog();

			ofd.CheckFileExists = true;
			ofd.DefaultExt = "config";
			ofd.FileName = "Blueman Setting";
			ofd.Filter = "config file(*.config)|*.config";
			ofd.FilterIndex = 0;
			ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			ofd.ShowDialog();

			if (ofd.CheckFileExists != true && ofd.CheckPathExists != true)
				return new RegistryInfo();
			
			RegistryInfo regInfo = new RegistryInfo();
			FileStream fs = new FileStream(
				ofd.InitialDirectory + "\\" + ofd.FileName, FileMode.Open);
			StreamReader sr = new StreamReader(fs);

			try
			{
				int count = 0;

				while (sr.EndOfStream != true)
				{
					regInfo[count++] = new RegistryInfoMember(
						sr.ReadLine(),
						sr.ReadLine(),
						(object)sr.ReadLine(),
						(RegistryValueKind)Enum.Parse(typeof(RegistryValueKind), sr.ReadLine())
						);
				}
			}
			finally
			{
				sr.Close();
				fs.Close();
			}

			return regInfo;
		}

		private void writeSubKey(RegistryInfo regInfo)
		{
			// delete root key
			RegistryKey rootKey = Registry.LocalMachine.CreateSubKey(pathSoftware);

			//rootKey.DeleteSubKeyTree(pathCompany);			

			// import registry key
			for (int i = 0; i < regInfo.Count; i++)
			{
				RegistryKey key = rootKey.CreateSubKey(regInfo[i].Key);

				key.SetValue(regInfo[i].KeyName, regInfo[i].Value, regInfo[i].ValueKind);

				key.Close();
			}

			// close root key
			rootKey.Close();
		}

		public void saveSetting()
		{
			RegistryKey rootKey = Registry.LocalMachine.CreateSubKey(pathSoftware + "\\" + pathCompany);
			RegistryInfo regInfo = new RegistryInfo();

			// read sub key registry from registry
			readSubKey(rootKey, ref regInfo);
			rootKey.Close();

			// export from registry sub key
			exportSubKey(regInfo);
		}

		private int readSubKey(RegistryKey subKey, ref RegistryInfo regInfo)
		{
			int count = subKey.SubKeyCount;

			for (int i = 0; i < count; i++)
			{
				string[] keys = subKey.GetSubKeyNames();

				for (int j = 0; j < keys.Length; j++)
				{
					RegistryKey regKey = subKey.CreateSubKey(keys[j]);

					string[] valueNames = regKey.GetValueNames();

					for (int k = 0; k < regKey.ValueCount; k++)
					{
						RegistryValueKind valueKind = regKey.GetValueKind(valueNames[k]);

						object value = regKey.GetValue(valueNames[k]);

						regInfo[regInfo.Count] = new RegistryInfoMember(
							regKey.ToString().Replace(
								Registry.LocalMachine.ToString() + "\\" + pathSoftware + "\\", ""),
							valueNames[k],
							value,
							valueKind
							);
					}

					count = readSubKey(regKey, ref regInfo);

					regKey.Close();
				}
			}

			return count;
		}

		private void exportSubKey(RegistryInfo regInfo)
		{
			FileStream fs = new FileStream(@"D:\Data.bin", FileMode.Create);
			StreamWriter sw = new StreamWriter(fs);

			try
			{
				for (int i = 0; i < regInfo.Count; i++)
				{
					sw.WriteLine(regInfo[i].Key);
					sw.WriteLine(regInfo[i].KeyName);
					sw.WriteLine(regInfo[i].Value);
					sw.WriteLine(regInfo[i].ValueKind);
				}
			}
			finally
			{
				sw.Close();
				fs.Close();
			}
		}

		public void regPrint(RegistryInfo regInfo)
		{
			for (int i = 0; i < regInfo.Count; i++)
			{
				Console.WriteLine(regInfo[i].Key);
				Console.WriteLine(regInfo[i].KeyName + "\t" + regInfo[i].Value);
				Console.WriteLine(regInfo[i].ValueKind.ToString());
			}
		}
	}

	internal class RegistryInfoMember
	{
		private string key;
		private string keyName;
		private object value;
		private RegistryValueKind valueKind;

		public RegistryInfoMember(string key, string keyName, object value, RegistryValueKind valueKind)
		{
			this.key = key;
			this.KeyName = keyName;
			this.value = value;
			this.valueKind = valueKind;
		}

		public string Key
		{
			get
			{
				return key;
			}
			set
			{
				key = value;
			}
		}

		public string KeyName
		{
			get
			{
				return keyName;
			}
			set
			{
				keyName = value;
			}
		}

		public object Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		public RegistryValueKind ValueKind
		{
			get
			{
				return valueKind;
			}
			set
			{
				valueKind = value;
			}
		}
	}

	internal class RegistryInfo
	{
		#region Indexer

		/// <summary>
		/// Indexer ArrayList.
		/// </summary>
		private System.Collections.ArrayList member = new System.Collections.ArrayList();
		/// <summary>
		/// Indexer count.
		/// </summary>
		private int count;

		/// <summary>
		/// Indexer counter.
		/// </summary>
		public int Count
		{
			get
			{
				return count;
			}
		}

		/// <summary>
		/// The index of the item in the collection to get or set.
		/// </summary>
		/// <param name="index">Indexer index.</param>
		/// <returns></returns>
		public RegistryInfoMember this[int index]
		{
			get
			{
				if (index > -1 & index < Count)
				{
					return (RegistryInfoMember)this.member[index];
				}
				else
				{
					return new RegistryInfoMember(string.Empty, string.Empty, null, RegistryValueKind.Unknown);
				}
			}
			set
			{
				if (index > -1 & index < this.member.Count)
				{
					this.member[index] = value;
				}
				else if (index == this.member.Count)
				{
					this.member.Add(value);
					this.count = index + 1;
				}
				else
					throw new IndexOutOfRangeException("Indexer index out of bound.");
			}
		}

		#endregion
	}  

}