﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace TestStructurePointer
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			byte[] buffer = new byte[50];
			byte[] remoteAddr = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
			byte[] SocketAddr = new byte[40];		// socket address
			byte[] BtAddr = new byte[40];			// bluetooth remote address

			// SocketAddr : 40 byte
			// 0~7 byte		: dummy header
			// 8~11 byte	: 4		: address's pointer
			// 12~15 byte	: 4		: address's length 
			// 16~31 byte	: 16	: service offset
			// 31~35 byte	: 4		: port offset
			// 36~			: dummy

			// BtAddr structure : 40 byte
			// 0 byte		: 1		: 32 : length
			// 1~7 byte		: 7		: dummy header
			// 8~14 byte	: 6		: bluetooth remote address
			// 15~			: dummy

			// remote address setting
			BitConverter.GetBytes((int)32).CopyTo(BtAddr, 0);		// length
			byte[] bRemoteAddr = remoteAddr;						// remote address's address
			Buffer.BlockCopy(bRemoteAddr, 0, BtAddr, 8, 6);			// 6 is bluetooth address's size

			// socket address setting
			unsafe		// BtAddr's pointer address
			{
				fixed (byte* pRemoteAddr = BtAddr)
				{
					BitConverter.GetBytes((int)pRemoteAddr).CopyTo(SocketAddr, 8);
				}
			}
			BitConverter.GetBytes((int)40).CopyTo(SocketAddr, 12);	// socket address's length

			// wirte socket address's pointer to buffer index 42
			unsafe
			{
				fixed (byte* pSocketAddr = SocketAddr)
				{
					BitConverter.GetBytes((int)pSocketAddr).CopyTo(buffer, 42);
				}
			}


			// verify remote address
			//pointer to outputbuffer
			int bufptr = BitConverter.ToInt32(buffer, 42);
			//remote socket address
			int sockadptr = Marshal.ReadInt32((IntPtr)bufptr, 8);
			//remote socket len
			int sockadlen = Marshal.ReadInt32((IntPtr)bufptr, 12);

			byte[] btsa1 = new byte[sockadlen];

			for (int sockbyte = 0; sockbyte < sockadlen; sockbyte++)
			{
				btsa1[sockbyte] = Marshal.ReadByte((IntPtr)sockadptr, sockbyte);
			}

			for (int i = 0; i < btsa1.Length; i++)
				txtBox.Text = txtBox.Text + btsa1[i].ToString() + "\r\n";
		}
	}
}