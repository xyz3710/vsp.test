using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ProcessTest
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[MTAThread]
		static void Main()
		{
		}
	}

	public class CheckProcess
	{
		public static bool IsRun(string processName)
		{
			const int TH32CS_SNAPPROCESS = 2;
			const int PROCESSENTRY32_SIZE = 564;
			int processCount = 0;	// To find process count
			bool ret = false;			
			byte[] lppe = new byte[PROCESSENTRY32_SIZE];

			BitConverter.GetBytes(PROCESSENTRY32_SIZE).CopyTo(lppe, 0);

			int hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

			if ((hProcessSnap != 0) && (Process32First(hProcessSnap, lppe) == true))
			{
				do
				{
					string compareName = System.Text.Encoding.Unicode.GetString(lppe, 36, 520);
					int iNull = compareName.IndexOf('\0');

					if (iNull != -1)
						compareName = compareName.Substring(0, iNull);

					if (processName.Equals(compareName))	// ��
						processCount++;

				} while (Process32Next(hProcessSnap, lppe) == true);
			}

			CloseHandle(hProcessSnap);

			if (processCount > 1)
				ret = true;

			return ret;
		}

		[DllImport("toolhelp.dll", EntryPoint="CreateToolhelp32Snapshot", SetLastError=true)]
		private static extern int CreateToolhelp32Snapshot(int dwFlags, int th32ProcessID);

		[DllImport("toolhelp.dll", EntryPoint="Process32First", SetLastError=true)]
		private static extern bool Process32First(int hSnapshot, byte[] lppe);
		
		[DllImport("toolhelp.dll", EntryPoint="Process32Next", SetLastError=true)]
		private static extern bool Process32Next(int hSnapshot, byte[] lppe);
		
		[DllImport("coredll.dll", EntryPoint="CloseHandle", SetLastError=true)]
		private static extern bool CloseHandle(int hObject);	
	}
}