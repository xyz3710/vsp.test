namespace ProcessInfo
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lvProcess = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.lvThread = new System.Windows.Forms.ListView();
			this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
			this.lvModule = new System.Windows.Forms.ListView();
			this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader15 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader16 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader17 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader18 = new System.Windows.Forms.ColumnHeader();
			this.SuspendLayout();
			// 
			// lvProcess
			// 
			this.lvProcess.Columns.Add(this.columnHeader1);
			this.lvProcess.Columns.Add(this.columnHeader2);
			this.lvProcess.Columns.Add(this.columnHeader3);
			this.lvProcess.Columns.Add(this.columnHeader4);
			this.lvProcess.Columns.Add(this.columnHeader5);
			this.lvProcess.Columns.Add(this.columnHeader6);
			this.lvProcess.FullRowSelect = true;
			this.lvProcess.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvProcess.Location = new System.Drawing.Point(0, 0);
			this.lvProcess.Name = "lvProcess";
			this.lvProcess.Size = new System.Drawing.Size(292, 79);
			this.lvProcess.TabIndex = 0;
			this.lvProcess.View = System.Windows.Forms.View.Details;
			this.lvProcess.SelectedIndexChanged += new System.EventHandler(this.lvProcess_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Process";
			this.columnHeader1.Width = 63;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "PID";
			this.columnHeader2.Width = 31;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Base Priority";
			this.columnHeader3.Width = 32;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "ThreadCount";
			this.columnHeader4.Width = 32;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "Base Addr";
			this.columnHeader5.Width = 31;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "Access Key";
			this.columnHeader6.Width = 39;
			// 
			// lvThread
			// 
			this.lvThread.Columns.Add(this.columnHeader7);
			this.lvThread.Columns.Add(this.columnHeader8);
			this.lvThread.Columns.Add(this.columnHeader9);
			this.lvThread.Columns.Add(this.columnHeader10);
			this.lvThread.FullRowSelect = true;
			this.lvThread.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvThread.Location = new System.Drawing.Point(0, 78);
			this.lvThread.Name = "lvThread";
			this.lvThread.Size = new System.Drawing.Size(292, 74);
			this.lvThread.TabIndex = 0;
			this.lvThread.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader7
			// 
			this.columnHeader7.Text = "Thread ID";
			this.columnHeader7.Width = 80;
			// 
			// columnHeader8
			// 
			this.columnHeader8.Text = "Current PID";
			this.columnHeader8.Width = 80;
			// 
			// columnHeader9
			// 
			this.columnHeader9.Text = "Thread Priority";
			this.columnHeader9.Width = 80;
			// 
			// columnHeader10
			// 
			this.columnHeader10.Text = "AccessKey";
			this.columnHeader10.Width = 80;
			// 
			// lvModule
			// 
			this.lvModule.Columns.Add(this.columnHeader11);
			this.lvModule.Columns.Add(this.columnHeader12);
			this.lvModule.Columns.Add(this.columnHeader13);
			this.lvModule.Columns.Add(this.columnHeader14);
			this.lvModule.Columns.Add(this.columnHeader15);
			this.lvModule.Columns.Add(this.columnHeader16);
			this.lvModule.Columns.Add(this.columnHeader17);
			this.lvModule.Columns.Add(this.columnHeader18);
			this.lvModule.FullRowSelect = true;
			this.lvModule.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvModule.Location = new System.Drawing.Point(0, 151);
			this.lvModule.Name = "lvModule";
			this.lvModule.Size = new System.Drawing.Size(292, 68);
			this.lvModule.TabIndex = 0;
			this.lvModule.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader11
			// 
			this.columnHeader11.Text = "Module";
			this.columnHeader11.Width = 80;
			// 
			// columnHeader12
			// 
			this.columnHeader12.Text = "Module ID";
			this.columnHeader12.Width = 80;
			// 
			// columnHeader13
			// 
			this.columnHeader13.Text = "Proc Count";
			this.columnHeader13.Width = 80;
			// 
			// columnHeader14
			// 
			this.columnHeader14.Text = "Global Count";
			this.columnHeader14.Width = 80;
			// 
			// columnHeader15
			// 
			this.columnHeader15.Text = "Base Addr";
			this.columnHeader15.Width = 60;
			// 
			// columnHeader16
			// 
			this.columnHeader16.Text = "Base Size";
			this.columnHeader16.Width = 60;
			// 
			// columnHeader17
			// 
			this.columnHeader17.Text = "hModule";
			this.columnHeader17.Width = 60;
			// 
			// columnHeader18
			// 
			this.columnHeader18.Text = "Full Path";
			this.columnHeader18.Width = 60;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(292, 219);
			this.Controls.Add(this.lvModule);
			this.Controls.Add(this.lvThread);
			this.Controls.Add(this.lvProcess);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView lvProcess;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.ListView lvThread;
		private System.Windows.Forms.ColumnHeader columnHeader7;
		private System.Windows.Forms.ColumnHeader columnHeader8;
		private System.Windows.Forms.ColumnHeader columnHeader9;
		private System.Windows.Forms.ColumnHeader columnHeader10;
		private System.Windows.Forms.ListView lvModule;
		private System.Windows.Forms.ColumnHeader columnHeader11;
		private System.Windows.Forms.ColumnHeader columnHeader12;
		private System.Windows.Forms.ColumnHeader columnHeader13;
		private System.Windows.Forms.ColumnHeader columnHeader14;
		private System.Windows.Forms.ColumnHeader columnHeader15;
		private System.Windows.Forms.ColumnHeader columnHeader16;
		private System.Windows.Forms.ColumnHeader columnHeader17;
		private System.Windows.Forms.ColumnHeader columnHeader18;

	}
}

