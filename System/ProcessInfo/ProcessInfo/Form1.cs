using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections;

namespace ProcessInfo
{
	public partial class Form1 : Form
	{
		private ProcessInfo pi;

		public Form1()
		{
			InitializeComponent();
			this.WindowState = FormWindowState.Maximized;
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			pi = new ProcessInfo();

			getProcess();

			for (int i = 0; i < pi.ProcessEntry.Count; i++)
			{
				Console.WriteLine("= Process =");
				Console.WriteLine("{0}", pi.ProcessEntry[i].ProcessName);

				Console.WriteLine("\t== Thread ==");
				for (int j = 0; j < pi.ProcessEntry[i].ThreadEntry.Count; j++)
				{
					Console.WriteLine("\t\t{0}", pi.ProcessEntry[i].ThreadEntry[j].ThreadID.ToString());
				}

				Console.WriteLine("\t== Module ==");
				for (int k = 0; k < pi.ProcessEntry[i].ModuleEntry.Count; k++)
				{
					Console.WriteLine("\t\t{0}", pi.ProcessEntry[i].ModuleEntry[k].ModuleName);
				}
				Console.WriteLine("");
			}
		}

		private void getProcess()
		{
			for (int i = 0; i < pi.ProcessEntry.Count; i++)
			{
				string process = pi.ProcessEntry[i].ProcessName;
				string processID = pi.ProcessEntry[i].ProcessID.ToString("X");
				string basePriority = pi.ProcessEntry[i].BasePriority.ToString();
				string threadCount = pi.ProcessEntry[i].ThreadCount.ToString();
				string baseAddress = pi.ProcessEntry[i].BaseAddress.ToString("X8");
				string accessKey = pi.ProcessEntry[i].AccessKey.ToString("X8");

				ListViewItem lvItem = new ListViewItem(process);

				lvItem.SubItems.Add(processID);
				lvItem.SubItems.Add(basePriority);
				lvItem.SubItems.Add(threadCount);
				lvItem.SubItems.Add(baseAddress);
				lvItem.SubItems.Add(accessKey);
		
				lvProcess.Items.Add(lvItem);
			}
		}

		private void getThread(int focusedItem)
		{
			for (int i = 0; i < pi.ProcessEntry[focusedItem].ThreadEntry.Count; i++)
			{
				string threadID = pi.ProcessEntry[focusedItem].ThreadEntry[i].ThreadID.ToString("X");
				string currentPID = pi.ProcessEntry[focusedItem].ThreadEntry[i].CurrentProcessID.ToString("X");
				string threadPriority = pi.ProcessEntry[focusedItem].ThreadEntry[i].CurrentProcessID.ToString();
				string accessKey = pi.ProcessEntry[focusedItem].ThreadEntry[i].AccessKey.ToString("X8");

				ListViewItem lvItem = new ListViewItem(threadID);

				lvItem.SubItems.Add(currentPID);
				lvItem.SubItems.Add(threadPriority);
				lvItem.SubItems.Add(accessKey);

				lvThread.Items.Add(lvItem);
			}
		}

		private void getModule(int focusedItem)
		{
			for (int i = 0; i < pi.ProcessEntry[focusedItem].ModuleEntry.Count; i++)
			{
				string module = pi.ProcessEntry[focusedItem].ModuleEntry[i].ModuleName;
				string moduleID = pi.ProcessEntry[focusedItem].ModuleEntry[i].ModuleID.ToString("X");
				string processCount = pi.ProcessEntry[focusedItem].ModuleEntry[i].ProcessCount.ToString();
				string globalCount = pi.ProcessEntry[focusedItem].ModuleEntry[i].GlobalCount.ToString();
				string baseAddr = pi.ProcessEntry[focusedItem].ModuleEntry[i].BaseAddress.ToString();
				string baseSize = pi.ProcessEntry[focusedItem].ModuleEntry[i].BaseSize.ToString();
				string hModule = pi.ProcessEntry[focusedItem].ModuleEntry[i].ModuleHandle.ToString("X");
				string fullPath = pi.ProcessEntry[focusedItem].ModuleEntry[i].FullPath;

				ListViewItem lvItem = new ListViewItem(module);

				lvItem.SubItems.Add(moduleID);
				lvItem.SubItems.Add(processCount);
				lvItem.SubItems.Add(globalCount);
				lvItem.SubItems.Add(baseAddr);
				lvItem.SubItems.Add(baseSize);
				lvItem.SubItems.Add(hModule);
				lvItem.SubItems.Add(fullPath);

				lvModule.Items.Add(lvItem);
			}
		}

		private void lvProcess_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (lvProcess.FocusedItem != null)
			{
				int focusedItem = lvProcess.FocusedItem.Index;

				lvThread.Items.Clear();
				lvModule.Items.Clear();

				getThread(focusedItem);
				getModule(focusedItem);
			}
		}
	}
}