using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ProcessInfo
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[MTAThread]
		static void Main()
		{
			Application.Run(new Form1());

			// 중복 실행 방지 소스

			//const string processName = "ProcessInfo.exe";	// 현재 프로세스 이름
			//const int TH32CS_SNAPPROCESS = 2;	// dwFlags
			//const int PROCESSENTRY32_SIZE = 564;

			//int processCount = 0;	// 찾는 프로세스 계수.
			
			//byte[] lppe = new byte[PROCESSENTRY32_SIZE];

			//BitConverter.GetBytes(PROCESSENTRY32_SIZE).CopyTo(lppe, 0);

			//int hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

			//if ((hProcessSnap != 0) && (Process32First(hProcessSnap, lppe) == true))
			//{
			//    do
			//    {
			//        string compareName = System.Text.Encoding.Unicode.GetString(lppe, 36, 520);
			//        int iNull = compareName.IndexOf('\0');

			//        if (iNull != -1)
			//            compareName = compareName.Substring(0, iNull);

			//        if (processName.Equals(compareName))	// 비교
			//            processCount++;

			//    } while (Process32Next(hProcessSnap, lppe) == true);
			//}

			//CloseHandle(hProcessSnap);

			//// 2번째 실행하려하면 취소 한다.
			//if (processCount < 2)
			//    Application.Run(new Form1());
			//else
			//    MessageBox.Show("Run..");
		}

		[DllImport("toolhelp.dll", EntryPoint="CreateToolhelp32Snapshot", SetLastError=true)]
		public static extern int CreateToolhelp32Snapshot(int dwFlags, int th32ProcessID);

		[DllImport("toolhelp.dll", EntryPoint="Process32First", SetLastError=true)]
		public static extern bool Process32First(int hSnapshot, byte[] lppe);
		
		[DllImport("toolhelp.dll", EntryPoint="Process32Next", SetLastError=true)]
		public static extern bool Process32Next(int hSnapshot, byte[] lppe);
		
		[DllImport("coredll.dll", EntryPoint="CloseHandle", SetLastError=true)]
		public static extern bool CloseHandle(int hObject);
	}
}