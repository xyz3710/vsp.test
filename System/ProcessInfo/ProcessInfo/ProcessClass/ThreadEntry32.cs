using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ProcessInfo
{
	public class ThreadEntry32Member
	{
		/*
				typedef struct tagTHREADENTRY32 { 
				4		  DWORD dwSize; 
				4		  DWORD cntUsage; 
				4		  DWORD th32ThreadID; 
				4		  DWORD th32OwnerProcessID; 
				4		  LONG tpBasePri; 
				4		  LONG tpDeltaPri; 
				4		  DWORD dwFlags;
				4		  DWORD th32AccessKey;
				4		  DWORD th32CurrentProcessID;
				  } ThreadEntry32; 
				  typedef ThreadEntry32* PTHREADENTRY32; 
				typedef ThreadEntry32* LPTHREADENTRY32;
		*/
		private const int SIZE = 36;

		private byte[] m_data;

		#region Constructor
		public ThreadEntry32Member()
			: this(new byte[SIZE])
		{
		}

		public ThreadEntry32Member(byte[] data)
		{
			this.m_data = new byte[SIZE];

			Buffer.BlockCopy(data, 0, m_data, 0, SIZE);

			// set size
			BitConverter.GetBytes(SIZE).CopyTo(m_data, 0);
		}
		#endregion

		public int CurrentUsage
		{
			get
			{
				return BitConverter.ToInt32(m_data, 4);
			}
		}

		public int ThreadID
		{
			get
			{
				return BitConverter.ToInt32(m_data, 8);
			}
		}

		public int OwnerProcessID
		{
			get
			{
				return BitConverter.ToInt32(m_data, 12);
			}
		}

		public int BasePriority
		{
			get
			{
				return BitConverter.ToInt32(m_data, 16);
			}
		}

		public int DeltaPriority
		{
			get
			{
				return BitConverter.ToInt32(m_data, 20);
			}
		}

		public int AccessKey
		{
			get
			{
				return BitConverter.ToInt32(m_data, 28);
			}
		}

		public int CurrentProcessID
		{
			get
			{
				return BitConverter.ToInt32(m_data, 32);
			}
		}

		public byte[] ToByteArray()
		{
			return m_data;
		}

	}

	public class ThreadEntry32
	{
		private ArrayList member = new ArrayList();

		#region Indexer
		public int Count;

		public ThreadEntry32Member this[int index]
		{
			get
			{
				if (index > -1 & index < Count)
					return (ThreadEntry32Member)member[index];
				else
					return new ThreadEntry32Member();
			}
			set
			{
				if (index > -1 & index < member.Count)
					member[index] = value;
				else if (index == member.Count)
				{
					member.Add(value);
					this.Count = index + 1;
				}
				else
					throw new IndexOutOfRangeException("Index out of bound");
			}
		}
		#endregion
	}
}
