﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace ProcessInfo
{
	internal class NativeMethods
	{
		#region P/Invoke API
		[DllImport("toolhelp.dll", EntryPoint="CreateToolhelp32Snapshot", SetLastError=true)]
		internal static extern int CreateToolhelp32Snapshot(TH32CS_FLAGS dwFlags, int th32ProcessID);

		[DllImport("toolhelp.dll", EntryPoint="Process32First", SetLastError=true)]
		internal static extern bool Process32First(int hSnapshot, byte[] lppe);

		[DllImport("toolhelp.dll", EntryPoint="Process32Next", SetLastError=true)]
		internal static extern bool Process32Next(int hSnapshot, byte[] lppe);

		[DllImport("toolhelp.dll", EntryPoint="Module32First", SetLastError=true)]
		internal static extern bool Module32First(int hSnapshot, byte[] lpme);

		[DllImport("toolhelp.dll", EntryPoint="Module32Next", SetLastError=true)]
		internal static extern bool Module32Next(int hSnapshot, byte[] lpme);

		[DllImport("toolhelp.dll", EntryPoint="Thread32First", SetLastError=true)]
		internal static extern bool Thread32First(int hSnapshot, byte[] lpte);

		[DllImport("toolhelp.dll", EntryPoint="Thread32Next", SetLastError=true)]
		internal static extern bool Thread32Next(int hSnapshot, byte[] lpte);

		[DllImport("coredll.dll", EntryPoint="CloseHandle", SetLastError=true)]
		internal static extern bool CloseHandle(int hObject);

		[DllImport("coredll.dll", EntryPoint="GetLastError", SetLastError=true)]
		internal static extern int GetLastError();
		#endregion
	}

	[FlagsAttribute]
	internal enum TH32CS_FLAGS : int
	{
		TH32CS_SNAPHEAPLIST=0x00000001,
		TH32CS_SNAPPROCESS=0x00000002,
		TH32CS_SNAPTHREAD=0x00000004,
		TH32CS_SNAPMODULE=0x00000008,
		TH32CS_SNAPALL=(TH32CS_SNAPHEAPLIST | TH32CS_SNAPPROCESS | TH32CS_SNAPTHREAD | TH32CS_SNAPMODULE),
		//TH32CS_GETALLMODS = 0x80000000,
	}
}
