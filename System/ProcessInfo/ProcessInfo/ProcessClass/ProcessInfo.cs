using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Collections;

namespace ProcessInfo
{
	public class ProcessInfo
	{
		private ProcessEntry32 processEntry;

		#region Constructor
		public ProcessInfo()
		{
			processEntry = new ProcessEntry32();

			processEntry = getProcessEntry();
		}
		#endregion

		public ProcessEntry32 ProcessEntry
		{
			get
			{
				return processEntry;
			}
		}

		public void Refresh()
		{
			processEntry = getProcessEntry();
		}

		private ProcessEntry32 getProcessEntry()
		{
			int hProcessSnap = NativeMethods.CreateToolhelp32Snapshot(TH32CS_FLAGS.TH32CS_SNAPPROCESS, 0);

			if (hProcessSnap != -1)
			{
				byte[] peByte = new ProcessEntry32Member().ToByteArray();
				int count = 0;

				if (NativeMethods.Process32First(hProcessSnap, peByte) == true)
				{
					do
					{
						processEntry[count++] = new ProcessEntry32Member(peByte);
					}
					while (NativeMethods.Process32Next(hProcessSnap, peByte) == true);
				}

				NativeMethods.CloseHandle(hProcessSnap);
			}

			return processEntry;
		}
	}
}
