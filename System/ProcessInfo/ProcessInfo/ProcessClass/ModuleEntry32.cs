using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ProcessInfo
{
	public class ModuleEntry32Member
	{
		/*
			typedef struct tagMODULEENTRY32 { 
			4		  DWORD dwSize; 
			4		  DWORD th32ModuleID; 
			4		  DWORD th32ProcessID; 
			4		  DWORD GlblcntUsage; 
			4		  DWORD ProccntUsage; 
			1		  BYTE *modBaseAddr; 
			4		  DWORD modBaseSize; 
			4		  HMODULE hModule; 
			520(260)  TCHAR szModule[MAX_PATH]; 
			520(260)  TCHAR szExePath[MAX_PATH]; 
			4		  DWORD dwFlags;
			} ModuleEntry32, *PMODULEENTRY32, *LPMODULEENTRY32;
		 */

		private const int SIZE = 1076;

		private byte[] m_data;

		#region Constructor
		public ModuleEntry32Member() 
			: this(new byte[SIZE])
		{
		}

		public ModuleEntry32Member(byte[] data)
		{
			this.m_data = new byte[SIZE];

			Buffer.BlockCopy(data, 0, m_data, 0, SIZE);

			// set size
			BitConverter.GetBytes(SIZE).CopyTo(m_data, 0);
		}
		#endregion

		public string ModuleName
		{
			get
			{
				string name = System.Text.Encoding.Unicode.GetString(m_data, 32, 520);
				int iNull = name.IndexOf('\0');

				if (iNull!=-1)
				{
					name = name.Substring(0, iNull);
				}

				return name;
			}
		}

		public string FullPath
		{
			get
			{
				string name = System.Text.Encoding.Unicode.GetString(m_data, 552, 520);
				int iNull = name.IndexOf('\0');

				if (iNull!=-1)
				{
					name = name.Substring(0, iNull);
				}

				return name;
			}
		}

		public int ProcessID
		{
			get
			{
				return BitConverter.ToInt32(m_data, 8);
			}
		}

		public int ModuleID
		{
			get
			{
				return BitConverter.ToInt32(m_data, 4);
			}
		}

		public int GlobalCount
		{
			get
			{
				return BitConverter.ToInt32(m_data, 12);
			}
		}

		public int ProcessCount
		{
			get
			{
				return BitConverter.ToInt32(m_data, 16);
			}
		}

		public int BaseAddress
		{
			get
			{
				return BitConverter.ToInt32(m_data, 20);
			}
		}

		public int BaseSize
		{
			get
			{
				return BitConverter.ToInt32(m_data, 24);
			}
		}

		public int ModuleHandle
		{
			get
			{
				return BitConverter.ToInt32(m_data, 28);
			}
		}

		public byte[] ToByteArray()
		{
			return m_data;
		}
	}

	public class ModuleEntry32
	{
		private ArrayList member = new ArrayList();

		#region Indexer
		public int Count;

		public ModuleEntry32Member this[int index]
		{
			get
			{
				if (index > -1 & index < Count)
					return (ModuleEntry32Member)member[index];
				else
					return new ModuleEntry32Member();
			}
			set
			{
				if (index > -1 & index < member.Count)
					member[index] = value;
				else if (index == member.Count)
				{
					member.Add(value);
					this.Count = index + 1;
				}
				else
					throw new IndexOutOfRangeException("Index out of bound");
			}
		}
		#endregion
	}
}
