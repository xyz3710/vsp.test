using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ProcessInfo
{
	public class ProcessEntry32Member
	{
		/*
				typedef struct tagPROCESSENTRY32 { 
		4			  DWORD dwSize; 
		4			  DWORD cntUsage; 
		4			  DWORD th32ProcessID; 
		4			  DWORD th32DefaultHeapID; 
		4			  DWORD th32ModuleID; 
		4			  DWORD cntThreads; 
		4			  DWORD th32ParentProcessID; 
		4			  LONG  pcPriClassBase; 
		4			  DWORD dwFlags; 
		520(260)	  TCHAR szExeFile[MAX_PATH]; 
		4			  DWORD th32MemoryBase;
		4			  DWORD th32AccessKey;
				  } ProcessEntry32Member; 
		*/
		private const int SIZE = 564;

		private ModuleEntry32 moduleEntry;
		private ThreadEntry32 threadEntry;
		private byte[] m_data;

		#region Constructor
		public ProcessEntry32Member()
		{
			this.m_data = new byte[SIZE];

			// set size
			BitConverter.GetBytes(SIZE).CopyTo(m_data, 0);
		}

		public ProcessEntry32Member(byte[] data)
		{
			this.moduleEntry = new ModuleEntry32();
			this.threadEntry = new ThreadEntry32();
			this.m_data = new byte[SIZE];

			Buffer.BlockCopy(data, 0, m_data, 0, SIZE);

			// set size
			BitConverter.GetBytes(SIZE).CopyTo(m_data, 0);
		}
		#endregion

		public string ProcessName
		{
			get
			{
				string name = System.Text.Encoding.Unicode.GetString(m_data, 36, 520);
				int iNull = name.IndexOf('\0');

				if (iNull!=-1)
				{
					name = name.Substring(0, iNull);
				}

				return name;
			}
		}

		public int ProcessID
		{
			get
			{
				return BitConverter.ToInt32(m_data, 8);
			}
		}

		public int ModuleID
		{
			get
			{
				return BitConverter.ToInt32(m_data, 16);
			}
		}

		public int BasePriority
		{
			get
			{
				return BitConverter.ToInt32(m_data, 28);
			}
		}

		public int ThreadCount
		{
			get
			{
				return BitConverter.ToInt32(m_data, 20);
			}
		}

		public int BaseAddress
		{
			get
			{
				return BitConverter.ToInt32(m_data, 556);
			}
		}

		public int AccessKey
		{
			get
			{
				return BitConverter.ToInt32(m_data, 560);
			}
		}

		public ModuleEntry32 ModuleEntry
		{
			get
			{
				moduleEntry = getModuleEntry(this.ProcessID);

				return moduleEntry;
			}
		}

		public ThreadEntry32 ThreadEntry
		{
			get
			{
				threadEntry = getThreadEntry(this.ProcessID);

				return threadEntry;
			}
		}

		public byte[] ToByteArray()
		{
			return m_data;
		}

		private ModuleEntry32 getModuleEntry(int processID)
		{
			int hModuleSnap = NativeMethods.CreateToolhelp32Snapshot(TH32CS_FLAGS.TH32CS_SNAPMODULE, processID);

			if (hModuleSnap != -1)
			{
				byte[] meByte = new ModuleEntry32Member().ToByteArray();
				int count = 0;

				if (NativeMethods.Module32First(hModuleSnap, meByte) == true)
				{
					do
					{
						moduleEntry[count++] = new ModuleEntry32Member(meByte);
					}
					while (NativeMethods.Module32Next(hModuleSnap, meByte) == true);
				}

				NativeMethods.CloseHandle(hModuleSnap);
			}

			return moduleEntry;
		}

		private ThreadEntry32 getThreadEntry(int processID)
		{
			int hThreadSnap = NativeMethods.CreateToolhelp32Snapshot(TH32CS_FLAGS.TH32CS_SNAPTHREAD, processID);

			if (hThreadSnap != -1)
			{
				byte[] teByte = new ThreadEntry32Member().ToByteArray();
				int count = 0;

				if (NativeMethods.Thread32First(hThreadSnap, teByte) == true)
				{
					ThreadEntry32Member tem;

					do
					{
						tem = new ThreadEntry32Member(teByte);

						if (tem.OwnerProcessID == processID)
						{
							threadEntry[count++] = tem;
						}
					}
					while (NativeMethods.Thread32Next(hThreadSnap, teByte) == true);
				}

				NativeMethods.CloseHandle(hThreadSnap);
			}

			return threadEntry;
		}
	}

	public class ProcessEntry32
	{
		private ArrayList member = new ArrayList();

		#region Indexer
		public int Count;

		public ProcessEntry32Member this[int index]
		{
			get
			{
				if (index > -1 & index < Count)
					return (ProcessEntry32Member)member[index];
				else
					return new ProcessEntry32Member();
			}
			set
			{
				if (index > -1 & index < member.Count)
					member[index] = value;
				else if (index == member.Count)
				{
					member.Add(value);
					this.Count = index + 1;
				}
				else
					throw new IndexOutOfRangeException("Index out of bound");
			}
		}
		#endregion
	}
}
