﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;

namespace RegistryImportCE
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void regPrint(RegistryInfo regInfo)
		{
			for (int i = 0; i < regInfo.Count; i++)
			{
				txtBox.Text += regInfo[i].Key + "\r\n";
				txtBox.Text += regInfo[i].KeyName + "\t" + regInfo[i].Value.ToString() + "\r\n";
				txtBox.Text += regInfo[i].ValueKind.ToString() + "\r\n";
			}
		}

		private void btnLoad_Click(object sender, EventArgs e)
		{
			RegistryBackup regLoad = new RegistryBackup();

			regLoad.LoadSetting();
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			RegistryBackup regSave = new RegistryBackup();

			regSave.SaveSetting();
		}
	}

	#region Registry Backup and Restore
	internal class RegistryBackup
	{
		private const string PATH_SOFTWARE = "Software";
		private const string PATH_COMPANY = "Edudas";
		private const string FILEEXT = "config";
		private string initDir;
		private string initFilename;

		#region Constructor
		public RegistryBackup()
			: this(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Blueman")
		{
		}

		public RegistryBackup(string initDir, string initFilename)
		{
			this.initDir = initDir;
			this.initFilename = initFilename + "." + FILEEXT;
		}
		#endregion

		public void LoadSetting()
		{
			// import to registry sub key
			RegistryInfo regInfo = importSubKey();

			// write registry sub key to registry
			writeSubKey(regInfo);
		}

		public void SaveSetting()
		{
			RegistryKey rootKey = Registry.LocalMachine.CreateSubKey(PATH_SOFTWARE + "\\" + PATH_COMPANY);
			RegistryInfo regInfo = new RegistryInfo();

			// read sub key registry from registry
			readSubKey(rootKey, ref regInfo);
			rootKey.Close();

			// export from registry sub key
			exportSubKey(regInfo);
		}

		private RegistryInfo importSubKey()
		{
			OpenFileDialog ofd = new OpenFileDialog();

			ofd.FileName = initFilename;
			ofd.Filter = FILEEXT + " file(*." + FILEEXT + ")|*." + FILEEXT;
			ofd.FilterIndex = 0;
			ofd.InitialDirectory = initDir;
			DialogResult ret = ofd.ShowDialog();

			if (ret != DialogResult.OK)
				return new RegistryInfo();

			RegistryInfo regInfo = new RegistryInfo();
			FileStream fs = new FileStream(ofd.FileName, FileMode.Open);
			StreamReader sr = new StreamReader(fs);

			try
			{
				int count = 0;

				while (sr.EndOfStream != true)
				{
					regInfo[count++] = new RegistryInfoMember(
						sr.ReadLine(),
						sr.ReadLine(),
						(object)sr.ReadLine(),
						(RegistryValueKind)Enum.Parse(typeof(RegistryValueKind), sr.ReadLine(), false)
						);
				}
			}
			finally
			{
				sr.Close();
				fs.Close();
			}

			return regInfo;
		}

		private void writeSubKey(RegistryInfo regInfo)
		{
			// delete root key
			RegistryKey rootKey = Registry.LocalMachine.CreateSubKey(PATH_SOFTWARE);

			rootKey.DeleteSubKeyTree(PATH_COMPANY);			

			// import registry key
			for (int i = 0; i < regInfo.Count; i++)
			{
				RegistryKey key = rootKey.CreateSubKey(regInfo[i].Key);

				key.SetValue(regInfo[i].KeyName, regInfo[i].Value, regInfo[i].ValueKind);

				key.Close();
			}

			// close root key
			rootKey.Close();
		}

		private int readSubKey(RegistryKey subKey, ref RegistryInfo regInfo)
		{
			int count = subKey.SubKeyCount;

			for (int i = 0; i < count; i++)
			{
				string[] keys = subKey.GetSubKeyNames();

				for (int j = 0; j < keys.Length; j++)
				{
					RegistryKey regKey = subKey.CreateSubKey(keys[j]);

					string[] valueNames = regKey.GetValueNames();

					for (int k = 0; k < regKey.ValueCount; k++)
					{
						RegistryValueKind valueKind = regKey.GetValueKind(valueNames[k]);

						object value = regKey.GetValue(valueNames[k]);

						regInfo[regInfo.Count] = new RegistryInfoMember(
							regKey.ToString().Replace(
								Registry.LocalMachine.ToString() + "\\" + PATH_SOFTWARE + "\\", ""),
							valueNames[k],
							value,
							valueKind
							);
					}

					count = readSubKey(regKey, ref regInfo);

					regKey.Close();
				}
			}

			return count;
		}

		private bool exportSubKey(RegistryInfo regInfo)
		{
			SaveFileDialog sfd = new SaveFileDialog();

			sfd.FileName = initFilename;
			sfd.Filter = FILEEXT + " file(*." + FILEEXT + ")|*." + FILEEXT;
			sfd.FilterIndex = 0;
			sfd.InitialDirectory = initDir;
			DialogResult ret = sfd.ShowDialog();

			if (ret != DialogResult.OK)
				return false;

			FileStream fs = new FileStream(sfd.FileName + "." + FILEEXT, FileMode.Create);
			StreamWriter sw = new StreamWriter(fs);

			try
			{
				for (int i = 0; i < regInfo.Count; i++)
				{
					sw.WriteLine(regInfo[i].Key);
					sw.WriteLine(regInfo[i].KeyName);
					sw.WriteLine(regInfo[i].Value);
					sw.WriteLine(regInfo[i].ValueKind);
				}
			}
			finally
			{
				sw.Close();
				fs.Close();
			}

			return true;
		}
	}

	internal class RegistryInfoMember
	{
		private string key;
		private string keyName;
		private object value;
		private RegistryValueKind valueKind;

		public RegistryInfoMember(string key, string keyName, object value, RegistryValueKind valueKind)
		{
			this.key = key;
			this.KeyName = keyName;
			this.value = value;
			this.valueKind = valueKind;
		}

		public string Key
		{
			get
			{
				return key;
			}
			set
			{
				key = value;
			}
		}

		public string KeyName
		{
			get
			{
				return keyName;
			}
			set
			{
				keyName = value;
			}
		}

		public object Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		public RegistryValueKind ValueKind
		{
			get
			{
				return valueKind;
			}
			set
			{
				valueKind = value;
			}
		}
	}

	internal class RegistryInfo
	{
		#region Indexer

		/// <summary>
		/// Indexer ArrayList.
		/// </summary>
		private System.Collections.ArrayList member = new System.Collections.ArrayList();
		/// <summary>
		/// Indexer count.
		/// </summary>
		private int count;

		/// <summary>
		/// Indexer counter.
		/// </summary>
		public int Count
		{
			get
			{
				return count;
			}
		}

		/// <summary>
		/// The index of the item in the collection to get or set.
		/// </summary>
		/// <param name="index">Indexer index.</param>
		/// <returns></returns>
		public RegistryInfoMember this[int index]
		{
			get
			{
				if (index > -1 & index < Count)
				{
					return (RegistryInfoMember)this.member[index];
				}
				else
				{
					return new RegistryInfoMember(string.Empty, string.Empty, null, RegistryValueKind.Unknown);
				}
			}
			set
			{
				if (index > -1 & index < this.member.Count)
				{
					this.member[index] = value;
				}
				else if (index == this.member.Count)
				{
					this.member.Add(value);
					this.count = index + 1;
				}
				else
					throw new IndexOutOfRangeException("Indexer index out of bound.");
			}
		}

		#endregion
	}
	#endregion
}