namespace MuteSound
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbWave = new System.Windows.Forms.GroupBox();
			this.tbWave = new System.Windows.Forms.TrackBar();
			this.cbWave = new System.Windows.Forms.CheckBox();
			this.gbAux = new System.Windows.Forms.GroupBox();
			this.cbAux = new System.Windows.Forms.CheckBox();
			this.tbAux = new System.Windows.Forms.TrackBar();
			this.gbMidi = new System.Windows.Forms.GroupBox();
			this.cbMidi = new System.Windows.Forms.CheckBox();
			this.tbMidi = new System.Windows.Forms.TrackBar();
			this.gbWave.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbWave)).BeginInit();
			this.gbAux.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbAux)).BeginInit();
			this.gbMidi.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbMidi)).BeginInit();
			this.SuspendLayout();
			// 
			// gbWave
			// 
			this.gbWave.Controls.Add(this.cbWave);
			this.gbWave.Controls.Add(this.tbWave);
			this.gbWave.Location = new System.Drawing.Point(12, 12);
			this.gbWave.Name = "gbWave";
			this.gbWave.Size = new System.Drawing.Size(72, 249);
			this.gbWave.TabIndex = 3;
			this.gbWave.TabStop = false;
			this.gbWave.Text = "   Wave ";
			// 
			// tbWave
			// 
			this.tbWave.Location = new System.Drawing.Point(15, 24);
			this.tbWave.Name = "tbWave";
			this.tbWave.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.tbWave.Size = new System.Drawing.Size(42, 201);
			this.tbWave.TabIndex = 2;
			this.tbWave.TickStyle = System.Windows.Forms.TickStyle.Both;
			// 
			// cbWave
			// 
			this.cbWave.AutoSize = true;
			this.cbWave.Location = new System.Drawing.Point(10, 227);
			this.cbWave.Name = "cbWave";
			this.cbWave.Size = new System.Drawing.Size(52, 16);
			this.cbWave.TabIndex = 3;
			this.cbWave.Text = "Mute";
			this.cbWave.UseVisualStyleBackColor = true;
			// 
			// gbAux
			// 
			this.gbAux.Controls.Add(this.cbAux);
			this.gbAux.Controls.Add(this.tbAux);
			this.gbAux.Location = new System.Drawing.Point(110, 12);
			this.gbAux.Name = "gbAux";
			this.gbAux.Size = new System.Drawing.Size(72, 249);
			this.gbAux.TabIndex = 3;
			this.gbAux.TabStop = false;
			this.gbAux.Text = "   AUX  ";
			// 
			// cbAux
			// 
			this.cbAux.AutoSize = true;
			this.cbAux.Location = new System.Drawing.Point(10, 227);
			this.cbAux.Name = "cbAux";
			this.cbAux.Size = new System.Drawing.Size(52, 16);
			this.cbAux.TabIndex = 3;
			this.cbAux.Text = "Mute";
			this.cbAux.UseVisualStyleBackColor = true;
			// 
			// tbAux
			// 
			this.tbAux.Location = new System.Drawing.Point(15, 24);
			this.tbAux.Name = "tbAux";
			this.tbAux.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.tbAux.Size = new System.Drawing.Size(42, 201);
			this.tbAux.TabIndex = 2;
			this.tbAux.TickStyle = System.Windows.Forms.TickStyle.Both;
			// 
			// gbMidi
			// 
			this.gbMidi.Controls.Add(this.cbMidi);
			this.gbMidi.Controls.Add(this.tbMidi);
			this.gbMidi.Location = new System.Drawing.Point(208, 12);
			this.gbMidi.Name = "gbMidi";
			this.gbMidi.Size = new System.Drawing.Size(72, 249);
			this.gbMidi.TabIndex = 3;
			this.gbMidi.TabStop = false;
			this.gbMidi.Text = "   Midi ";
			// 
			// cbMidi
			// 
			this.cbMidi.AutoSize = true;
			this.cbMidi.Location = new System.Drawing.Point(10, 227);
			this.cbMidi.Name = "cbMidi";
			this.cbMidi.Size = new System.Drawing.Size(52, 16);
			this.cbMidi.TabIndex = 3;
			this.cbMidi.Text = "Mute";
			this.cbMidi.UseVisualStyleBackColor = true;
			// 
			// tbMidi
			// 
			this.tbMidi.Location = new System.Drawing.Point(15, 24);
			this.tbMidi.Name = "tbMidi";
			this.tbMidi.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.tbMidi.Size = new System.Drawing.Size(42, 201);
			this.tbMidi.TabIndex = 2;
			this.tbMidi.TickStyle = System.Windows.Forms.TickStyle.Both;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.gbMidi);
			this.Controls.Add(this.gbAux);
			this.Controls.Add(this.gbWave);
			this.Name = "Form1";
			this.Text = "Form1";
			this.gbWave.ResumeLayout(false);
			this.gbWave.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbWave)).EndInit();
			this.gbAux.ResumeLayout(false);
			this.gbAux.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbAux)).EndInit();
			this.gbMidi.ResumeLayout(false);
			this.gbMidi.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbMidi)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox gbWave;
		private System.Windows.Forms.CheckBox cbWave;
		private System.Windows.Forms.TrackBar tbWave;
		private System.Windows.Forms.GroupBox gbAux;
		private System.Windows.Forms.CheckBox cbAux;
		private System.Windows.Forms.TrackBar tbAux;
		private System.Windows.Forms.GroupBox gbMidi;
		private System.Windows.Forms.CheckBox cbMidi;
		private System.Windows.Forms.TrackBar tbMidi;

	}
}

