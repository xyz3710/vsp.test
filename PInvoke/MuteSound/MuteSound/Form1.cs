using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MuteSound
{
	public partial class Form1 : Form
	{
		private IntPtr _hwo;
		private DeviceTypes _type;

		public Form1()
		{
			InitializeComponent();
		}

		private int getVolume(ref ushort volLeft, ref ushort volRight)
		{
			uint vol = 0;
			int result = 9999;

			DeviceTypes _type;
			switch (_type)
			{
				case DeviceTypes.Wave:
					result = NativeMethods.waveOutGetVolume(_hwo, out vol);

					break;
				case DeviceTypes.Aux:
					result = NativeMethods.auxOutGetVolume(_deviceID, out vol);
					break;
				case DeviceTypes.Midi:
					break;
				default:
					break;
			}
		}
	}

	public enum DeviceTypes
	{
		Wave = 0,
		Aux,
		Midi
	}
}