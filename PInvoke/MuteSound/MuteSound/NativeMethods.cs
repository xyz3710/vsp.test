using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace MuteSound
{
	class NativeMethods
	{
		[DllImport("winmm.dll", SetLastError=true, EntryPoint="waveOutGetVolume")]
		public static extern int waveOutGetVolume(IntPtr hwo, out uint dwVolume);


		[DllImport("winmm.dll", SetLastError=true, EntryPoint="waveOutSetVolume")]
		public static extern int waveOutSetVolume(IntPtr hwo, out uint dwVolume);
	}
}
