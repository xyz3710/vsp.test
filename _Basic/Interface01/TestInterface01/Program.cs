﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestInterface01
{
	interface IExam
	{
		void Ans();
	}

	class InterfaceClass : IExam
	{
		#region IExam Members

		public void Ans()
		{
			Console.WriteLine("Implementation of Interface method.");
		}

		#endregion

		#region IExam Members

		void IExam.Ans()
		{
			Console.WriteLine("Explicity implementation of Interface method.");
		}

		#endregion
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			InterfaceClass ic = new InterfaceClass();

			ic.Ans();


			IExam ie = new InterfaceClass();

			ie.Ans();


			Console.ReadLine();
		}
	}
}
