using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Reflection;

namespace Enum2List
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(440, 365);
			this.Name = "Form1";
			this.Text = "Form1";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.Form1_Load);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}


		private void Form1_Load(object sender, System.EventArgs e)
		{
			System.Drawing.Color c = new Color();
			Type			colorType = c.GetType();
			PropertyInfo[]	p = colorType.GetProperties();

			for (int i = 0; i < p.Length; i++)
				if(p[i].ToString().Split(' ')[0] == "System.Drawing.Color")
					NewLabel(p[i].ToString().Split(' ')[1].ToString(), i);
		}

		private void NewLabel(string ColorName, int rows)
		{
			rows = rows * 20;

			Label lbl = new Label();

			lbl.Location = new System.Drawing.Point(0, rows);
			lbl.Size = new System.Drawing.Size(150,20);
			lbl.BackColor = Color.FromName(ColorName);
			this.Controls.Add(lbl);

			Label lbl2 = new Label();
			lbl2.Text = ColorName;
			lbl2.Location = new System.Drawing.Point(150, rows);
			lbl2.Size = new System.Drawing.Size(150,20);
			lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.Controls.Add(lbl2);
		}		
	}
}

