﻿/**********************************************************************************************************************/
/*	Domain		:	AnyFactory.FX.Win.Extensions.Extensions
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 11월 26일 목요일 오후 2:26
/*	Purpose		:	Framework에서 사용 가능한 확장 Method를 지원하는 클래스 입니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	Kim Ki Won
/*	Update		:	2010년 5월 25일 화요일 오후 2:15
/*	Changes		:	CompareProperty 추가
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	Kim Ki Won
/*	Update		:	2010년 6월 11일 금요일 오전 11:30
/*	Changes		:	IsDerivedBaseType 추가
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	2010년 6월 11일 금요일 오후 1:57
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;

namespace AnyFactory.FX.Win.Extensions
{
	/// <summary>
	/// Framework에서 사용 가능한 확장 Method를 지원하는 클래스 입니다.
	/// </summary>
	public static class Extensions
	{
		#region In
		/// <summary>
		/// 사용한 인스턴스가 컬렉션에 포함이 되었는지 여부를 구합니다.
		/// </summary>
		/// <param name="item">검색하려는 인스턴스</param>
		/// <param name="collections"><seealso cref="System.Collections.IEnumerable"/>을 구현한 컬렉션</param>
		/// <returns>컬렉션에 항목이 포함되면 true이고, 그렇지 않으면 false 입니다.</returns>
		public static bool In(this object item, IEnumerable collections)
		{
			foreach (object element in collections)
			{
				if (element != null && element.Equals(item) == true)
					return true;
			}

			return false;
		}

		/// <summary>
		/// 사용한 인스턴스가 컬렉션에 포함이 되었는지 여부를 구합니다.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="item">검색하려는 인스턴스</param>
		/// <param name="collections"><seealso cref="System.Collections.Generic.IEnumerable&lt;T&gt;"/>을 구현한 컬렉션</param>
		/// <returns>컬렉션에 항목이 포함되면 true이고, 그렇지 않으면 false 입니다.</returns>
		/// <exception cref="System.ArgumentNullException">collection 또는 predicate가 null인 경우</exception>
		public static bool In<T>(this T item, IEnumerable<T> collections)
		{
			return collections.Contains(item);
		}


		/// <summary>
		/// 사용한 인스턴스가 컬렉션에 포함이 되었는지 여부를 구합니다.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="item">검색하려는 인스턴스</param>
		/// <param name="collections"><seealso cref="System.Collections.Generic.IEnumerable&lt;T&gt;"/>을 구현한 컬렉션</param>
		/// <returns>컬렉션에 항목이 포함되면 true이고, 그렇지 않으면 false 입니다.</returns>
		/// <exception cref="System.ArgumentNullException">collection 또는 predicate가 null인 경우</exception>
		public static T GetValue<T>(this T item)
			where T : IComparable, IConvertible
		{

			return item;
		}

		/// <summary>
		/// 사용한 인스턴스가 컬렉션에 포함이 되었는지 여부를 테스트하는 함수를 통해서 구합니다.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="item">검색하려는 인스턴스</param>
		/// <param name="collections"><seealso cref="System.Collections.Generic.IEnumerable&lt;T&gt;"/>을 구현한 컬렉션</param>
		/// <param name="predicate">각 요소를 조건에 대해 테스트하는 함수</param>
		/// <returns>컬렉션에 항목이 포함되면 true이고, 그렇지 않으면 false 입니다.</returns>
		/// <exception cref="System.ArgumentNullException">collection 또는 predicate가 null인 경우</exception>
		public static bool In<T>(this T item, IEnumerable<T> collections, Func<T, bool> predicate)
		{
			return collections.Where(predicate).Count() > 0;
		}
		#endregion

		#region CompareProperty
		/// <summary>
		/// 인스턴스간 속성을 비교 합니다.
		/// </summary>
		/// <param name="TSource">비교 되는 Type</param>
		/// <param name="TCompare">비교 하려는 Type</param>
		/// <param name="propertyName">비교하려는 속성 이름</param>
		/// <returns>propertyName에 나열된 속성들의 값이 일치 하면 true, 그렇지 않으면 false를 반환 합니다.</returns>
		public static bool CompareProperty<T>(this T TSource, T TCompare, params string[] propertyName)
		{
			bool comparePropertiesResult = true;

			if (propertyName == null || propertyName.Length == 0)
				return comparePropertiesResult;

			Type fromType = TSource.GetType();
			Type toType = TCompare.GetType();

			foreach (string property in propertyName)
			{
				PropertyInfo fromPI = fromType.GetProperty(property);
				PropertyInfo toPI = toType.GetProperty(property);

				if (fromPI == null || toPI == null)
					continue;

				comparePropertiesResult &= fromPI.GetValue(TSource, null).Equals(toPI.GetValue(TCompare, null));

				if (comparePropertiesResult == false)
					return comparePropertiesResult;
			}

			return comparePropertiesResult;
		}
		#endregion

		#region IsDerivedBaseType
		/// <summary>
		/// Type이 지정된 base type에서 상속 받았는지를 <see cref="Type.FullName"/> 일부로 검사합니다.
		/// </summary>
		/// <param name="checkType">검사하려는 type</param>
		/// <param name="partOfBaseTypeFullName">base type <see cref="Type.FullName"/>의 일부 단어</param>
		/// <returns>지정된 Type으로 부터 상속 파생되었으면 true를 반환합니다.</returns>
		/// <seealso cref="IsDerivedBaseType(Type, string, bool)"/>
		public static bool IsDerivedBaseType(this Type checkType, string partOfBaseTypeFullName)
		{
			bool exactlyMatchFullName = false;

			return IsDerivedBaseType(checkType, partOfBaseTypeFullName, exactlyMatchFullName);
		}

		/// <summary>
		/// Type이 지정된 base type에서 상속 받았는지를 검사합니다.
		/// </summary>
		/// <param name="checkType">검사하려는 type</param>
		/// <param name="baseTypeFullName">base type의 <see cref="Type.FullName"/></param>
		/// <param name="exactlyMatchFullName">baseTypeFullName을 정확하게 일치하는 단어로 검사할지 여부</param>
		/// <returns>지정된 Type으로 부터 상속 파생되었으면 true를 반환합니다.</returns>
		/// <seealso cref="IsDerivedBaseType(Type, string)"/>
		public static bool IsDerivedBaseType(this Type checkType, string baseTypeFullName, bool exactlyMatchFullName)
		{
			bool checkBaseTypeResult = false;

			if (checkType == null || baseTypeFullName == string.Empty)
				return checkBaseTypeResult;

			Queue<Type> checkTypeQueue = new Queue<Type>();

			checkTypeQueue.Enqueue(checkType.BaseType);

			while (checkTypeQueue.Count > 0)
			{
				Type baseType = checkTypeQueue.Dequeue();

				if (baseType == null)
					continue;

				if (exactlyMatchFullName == false)
				{
					if (baseType.FullName.Contains(baseTypeFullName) == true)
						return true;
				}
				else
				{
					if (baseType.FullName == baseTypeFullName)
						return true;
				}

				checkTypeQueue.Enqueue(baseType.BaseType);
			}

			return checkBaseTypeResult;
		}
		#endregion
	}
}
