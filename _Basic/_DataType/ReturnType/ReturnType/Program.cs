﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AnyFactory.FX.Win.Extensions;

namespace ReturnType
{
	class Program
	{
		static void Main(string[] args)
		{
			List<MyClass> classes = new List<MyClass>();

			classes.Add(new MyClass(DateTime.Now.ToShortTimeString(), DateTime.Now.Millisecond));
			classes.Add(new MyClass(DateTime.Now.ToShortTimeString(), DateTime.Now.Millisecond));
			classes.Add(new MyClass(DateTime.Now.ToShortTimeString(), DateTime.Now.Millisecond));
			classes.Add(new MyClass(DateTime.Now.ToShortTimeString(), DateTime.Now.Millisecond));
			classes.Add(new MyClass("Test", 1));

			MyClass test = new MyClass("Test", 1);

			Console.WriteLine(test.In(classes));
			Console.WriteLine(classes[4].CompareProperty<object>(test, "Name"));
		}
		
		private static int GetValue()
		{
			return 1;
		}

		private static T GetValue<T>()
		{
			return (T)default(T);
		}
	}
}