using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AnyFactory.FX.Win.Extensions;

namespace ReturnType
{
	public class MyClass
	{
		#region Constructors
		/// <summary>
		/// MyClass class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public MyClass(string name, int age)
		{
			Name = name;
			Age = age;
		}
		#endregion

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Age를 구하거나 설정합니다.
		/// </summary>
		public int Age
		{
			get;
			set;
		}
	}
}
