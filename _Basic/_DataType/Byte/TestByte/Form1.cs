﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace TestByte
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			// byte[] 형 변수의 pointer를 구해서 다시 참조 하는방법
			// http://www.devpia.com/Forum/BoardView.aspx?no=55222&ref=55222&page=1&forumname=csharp_qa&stype=&KeyW=%c6%f7%c0%ce%c5%cd&KeyR=titlecontent 참고함
			byte[] b = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
			byte[] buf = new byte[32];

			unsafe
			{
				fixed (byte* pba = b)	//불안전 코드 이기때문에 fixed 로메모리고정을 시켜주어야함 
				{
					BitConverter.GetBytes((int)pba).CopyTo(buf, 4);
				}
			}

			int ptr = BitConverter.ToInt32(buf, 4);
				//Marshal.ReadInt32(ptr, 4);

			byte[] ret = buf;

			for (int offset = 0; offset < b.Length; offset++)
			{
				ret[offset] = Marshal.ReadByte((IntPtr)ptr, offset);
			}

			txtBox.Text = "";

			for (int i = 0; i < ret.Length; i++)
				txtBox.Text = txtBox.Text + ret[i].ToString() + "\r\n";
		}
	}
}