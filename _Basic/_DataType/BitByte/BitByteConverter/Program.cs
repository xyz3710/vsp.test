﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace BitByteConverter
{
	class Program
	{
		static void Main(string[] args)
		{
			byte[] byteArray = new byte[] { 0x23 };
			byteArray = new byte[] { 0x06, 0x30, 0x31, 0x38, 0x37, 0x53, 0x32, 0x33, 0x33, 0x30, 0x32, 0x30, 0x38, 0x30, 0x38, 0x30, 0x31, 0x31, 0x30, 0x32, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x31, 0x30, 0x36, 0x32, 0x38, 0x30, 0x30, 0x30, 0x30, 0x31, 0x37, 0x42, 0x33, 0x38, 0x33, 0x32, 0x30, 0x32, 0x30, 0x30, 0x31, 0x30, 0x30, 0x30, 0x30, 0x38, 0x33, 0x30, 0x33, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x33, 0x32, 0x0D };

			Console.WriteLine("\t========== BitArrayTestMSDN");
			BitArrayTestMSDN(byteArray);
			
			Console.WriteLine("\t========== BitArrayTest");
			BitArrayTest(byteArray);
			
			Console.WriteLine("\t========== ConvertToBinaryFormat");
			ConvertToBinaryFormat(byteArray[0]);
			
			Console.WriteLine("\t========== BitConvertWithMask");
			BitConvertWithMask(byteArray);

			Console.WriteLine("\t========== BitConvertSimple String");
			BitConvertSimpleString(byteArray);
		}

		private static void BitConvertSimpleString(byte[] byteArray)
		{
			for (int i = 0; i < byteArray.Length; i++)
			{
				byte value = byteArray[i];

				Console.WriteLine(
					"{0}\t{1}(0x{2})", 
					Convert.ToString(value, 2).PadLeft(8, '0').Insert(4, " "), 
					value, 
					value.ToString("x"));
			}
		}

		private static void BitConvertWithMask(byte[] byteArray)
		{
			for (int count = 0; count < byteArray.Length; count++)
			{
				byte value = byteArray[count];
				int innerCount = 1;

				for (int b = 128; b > 0; b = b >> 1)		// b >> 1 == b / 2
				{
					if ((value & b) == 0)
						Console.Write("0 ");
					else
						Console.Write("1 ");

					if ((innerCount++ % 4) == 0)
						Console.Write(" ");
				}

				Console.WriteLine("\t{0}(0x{1})", value, value.ToString("x"));
			}
		}

		private static void ConvertToBinaryFormat(byte n)
		{
			// http://www.byteblocks.com/post/2009/03/21/Manipulating-Bits-and-Bytes-With-C.aspx 참고
			var flags = new BitArray(8);
			var counter = 8;
			var idx = 0;
			while (counter > 0)
			{
				flags.Set(idx++, ((n & 1) == 1));
				n >>= 1;
				counter--;
			}
			PrintBitArray(flags);

			Console.WriteLine();
		}

		private static void PrintBitArray(BitArray ba)
		{
			int innerCount = 1;

			for (int i = ba.Length - 1; i >= 0; i--)
			{
				Console.Write("{0} ", ba[i] ? 1 : 0);
			
				if ((innerCount++ % 4) == 0)
					Console.Write(" ");
			}
		}

		private static void BitArrayTest(byte[] byteArray)
		{
			BitArray bitArray = new BitArray(byteArray);
			int innerCount = 1;

			foreach (var bit in bitArray)
			{
				Console.Write("{0} ", Convert.ToInt32(bit));

				if ((innerCount % 4) == 0)
					Console.Write(" ");

				if ((innerCount++ % 8) == 0)
					Console.WriteLine();
			}

			Console.WriteLine();
		}
		
		private static void BitArrayTestMSDN(byte[] byteArray)
		{
			// Creates and initializes several BitArrays.
			BitArray myBA1 = new BitArray(5);

			BitArray myBA2 = new BitArray(5, false);

			byte[] myBytes = new byte[5] { 1, 2, 3, 4, 5 };
			BitArray myBA3 = new BitArray(myBytes);

			bool[] myBools = new bool[5] { true, false, true, true, false };
			BitArray myBA4 = new BitArray(myBools);

			int[] myInts = new int[5] { 6, 7, 8, 9, 10 };
			BitArray myBA5 = new BitArray(myInts);

			// Displays the properties and values of the BitArrays.
			Console.WriteLine("myBA1");
			Console.WriteLine("   Count:    {0}", myBA1.Count);
			Console.WriteLine("   Length:   {0}", myBA1.Length);
			Console.WriteLine("   Values:");
			PrintValues(myBA1, 8);

			Console.WriteLine("myBA2");
			Console.WriteLine("   Count:    {0}", myBA2.Count);
			Console.WriteLine("   Length:   {0}", myBA2.Length);
			Console.WriteLine("   Values:");
			PrintValues(myBA2, 8);

			Console.WriteLine("myBA3");
			Console.WriteLine("   Count:    {0}", myBA3.Count);
			Console.WriteLine("   Length:   {0}", myBA3.Length);
			Console.WriteLine("   Values:");
			PrintValues(myBA3, 8);

			Console.WriteLine("myBA4");
			Console.WriteLine("   Count:    {0}", myBA4.Count);
			Console.WriteLine("   Length:   {0}", myBA4.Length);
			Console.WriteLine("   Values:");
			PrintValues(myBA4, 8);

			Console.WriteLine("myBA5");
			Console.WriteLine("   Count:    {0}", myBA5.Count);
			Console.WriteLine("   Length:   {0}", myBA5.Length);
			Console.WriteLine("   Values:");
			PrintValues(myBA5, 8);
		}

		private static void PrintValues(IEnumerable myList, int myWidth)
		{
			int i = myWidth;
			foreach (Object obj in myList)
			{
				if (i <= 0)
				{
					i = myWidth;
					Console.WriteLine();
				}
				i--;
				Console.Write("{0,8}", obj);
			}
			Console.WriteLine();
		}
	}
}
