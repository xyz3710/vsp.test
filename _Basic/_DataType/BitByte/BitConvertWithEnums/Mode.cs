using System;
using System.Collections.Generic;

namespace Bitconvert
{
	/// <summary>
	/// Current connection mode.
	/// Updated by Kim Ki Won in 2006.01.12
	/// </summary>
	[FlagsAttribute]
	public enum Mode : byte
	{
		/// <summary>
		/// Active mode.
		/// </summary>
		Active = 0x00,
		/// <summary>
		/// Hold mode.
		/// </summary>
		Hold = 0x08,
		/// <summary>
		/// Sniff mode.
		/// </summary>
		Sniff = 0x10,
		/// <summary>
		/// Park mode.
		/// </summary>
		Park = 0x18
	}
}

