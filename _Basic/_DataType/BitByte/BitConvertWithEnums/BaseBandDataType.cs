using System;
using System.Collections.Generic;

namespace Bitconvert
{
	/// <summary>
	/// Base band data type
	/// bit array : 
	/// 1 : linktype
	/// 2 : encrypt
	/// 4 : authenticate
	/// 8 : hold
	/// 16 : sniff
	/// 24 : park
	/// </summary>
	public enum BaseBandDataType : byte
	{
		/// <summary>
		/// Link type.
		/// </summary>
		LinkType = 0x01,
		/// <summary>
		/// Encrypt.
		/// </summary>
		Encrypt = 0x02,
		/// <summary>
		/// Authenticate.
		/// </summary>
		Authenticate = 0x04,
		/// <summary>
		/// Hold.
		/// </summary>
		Hode = 0x08,
		/// <summary>
		/// Sniff.
		/// </summary>
		Sniff = 0x10,
		/// <summary>
		/// Park.
		/// </summary>
		Park = 0x18,
	}
}

