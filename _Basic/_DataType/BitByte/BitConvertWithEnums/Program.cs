﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Bitconvert
{
	class Program
	{
		static void Main(string[] args)
		{
			LinkType linkType;
			bool encrypt;
			bool authenticate;
			Mode mode;
			int data = 0x09;

			for (int i = 1; i <= 31; i++)
			{
				data = i;

				linkType = (LinkType)(data & (int)LinkType.ACL);
				encrypt = (data & (int)BaseBandDataType.Encrypt) == (int)BaseBandDataType.Encrypt ? true : false;
				authenticate = (data & (int)BaseBandDataType.Authenticate) == (int)BaseBandDataType.Authenticate ? true : false;
				mode = (Mode)(data & 0x38);

				Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", 
					i, linkType.ToString(), authenticate.ToString(), encrypt.ToString(), mode.ToString());
			}
		}
	}
}
