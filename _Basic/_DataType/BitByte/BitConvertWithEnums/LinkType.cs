using System;
using System.Collections.Generic;

namespace Bitconvert
{
	/// <summary>
	/// <see cref="BaseBandConnection"/> Link type
	/// </summary>
	[FlagsAttribute]
	public enum LinkType : byte
	{
		/// <summary>
		/// Synchronous Connection Oriented link
		/// </summary>
		SCO = 0x00,
		/// <summary>
		/// Asynchronous connection Link
		/// </summary>
		ACL = 0x01
	}
}

