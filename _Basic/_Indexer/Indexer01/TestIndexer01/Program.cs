﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace TestIndexer01
{
	class SimpleIndexer
	{
		private ArrayList Iname = new ArrayList();
		public int Count;

		public object this[int index]
		{
			get
			{
				if (index > -1 & index < Iname.Count)
					return Iname[index];
				else
					return null;
			}
			set
			{
				if (index > -1 & index < Iname.Count)
					Iname[index] = value;
				else if (index == Iname.Count)
				{
					Iname.Add(value);
					this.Count = index + 1;
				}
				else
					Console.WriteLine("sid[" + index + "] : 입력 범위 초과");
			}
		}
	}

	internal struct NewPropertyMember
	{
		private string title;
		private string[] memo;

		public NewPropertyMember(string title, string[] memo)
		{
			this.title = title;
			this.memo = memo;
		}

		public string Title
		{
			get
			{
				return title;
			}
			set
			{
				title = value;
			}
		}

		public string[] Memo
		{
			get
			{
				return memo;
			}
			set
			{
				memo = value;
			}
		}
	}

	internal class NewProperties
	{
		private ArrayList member = new ArrayList();

		#region Indexer
		public int Count;

		public NewPropertyMember this[int index]
		{
			get
			{
				if (index > -1 & index < Count)
					return (NewPropertyMember)member[index];
				else
					return new NewPropertyMember();
			}
			set
			{
				if (index > -1 & index < member.Count)
					member[index] = value;
				else if (index == member.Count)
				{
					member.Add(value);
					this.Count = index + 1;
				}
				else
					throw new IndexOutOfRangeException("Index out of bound");
			}
		}
		#endregion
	}

	class Program
	{
		static void Main(string[] args)
		{
			SimpleIndexer sid = new SimpleIndexer();

			sid[0] = "hong";
			sid[1] = "kim";
			sid[2] = "sung";
			sid[10] = "park";

			Console.WriteLine("=== Getting ===");

			for (int i = 0; i < sid.Count; i++)
				Console.WriteLine(sid[i]);

			NewProperties np = new NewProperties();

			for (int i = 0; i < 5; i++)
			{
				np[i] = new NewPropertyMember(
					"Title" + i.ToString(), 
					new string[] { 
						"Memo" + i.ToString(), 
						"MM" + i.ToString() }
					);
			}

			for (int i = 0; i < np.Count; i++)
			{
				Console.WriteLine("{0}, {1}, {2}", np[i].Title, np[i].Memo[0], np[i].Memo[1]);
			}

			Console.WriteLine();



			ProcessEntry32 pe = new ProcessEntry32();

			for (int i = 0; i < 5; i++)
			{
				byte[] b = new byte[564];

				BitConverter.GetBytes(i).CopyTo(b, 0);

				pe[i] = new ProcessEntry32Member(b);
			}

			for (int i = 0; i < pe.Count; i++)
			{
				Console.WriteLine("{0}", pe[i].Data[0].ToString());
			}


			
			ArrayList al = new ArrayList();

			for (int i = 0; i < 5; i++)
			{
				al.Add(new NewPropertyMember("Title" + i.ToString(),
					new string[]{
						"M1" + i.ToString(),
						"M2" + i.ToString()}
						)
					);
			}

			for (int j = 0; j < al.Count; j++)
			{
				Console.WriteLine("{0}", ((NewPropertyMember)al[j]).Title);
			}

			Console.ReadLine();
		}
	}

	public class ProcessEntry32Member
	{
		/*
				typedef struct tagPROCESSENTRY32 { 
		4			  DWORD dwSize; 
		4			  DWORD cntUsage; 
		4			  DWORD th32ProcessID; 
		4			  DWORD th32DefaultHeapID; 
		4			  DWORD th32ModuleID; 
		4			  DWORD cntThreads; 
		4			  DWORD th32ParentProcessID; 
		4			  LONG  pcPriClassBase; 
		4			  DWORD dwFlags; 
		520(260)	  TCHAR szExeFile[MAX_PATH]; 
		4			  DWORD th32MemoryBase;
		4			  DWORD th32AccessKey;
				  } ProcessEntry32Member; 
		*/
		private const int SIZE = 564;
		private byte[] m_data;

		public ProcessEntry32Member(byte[] data)
		{
			this.m_data = data;

			// set size
			BitConverter.GetBytes(SIZE).CopyTo(m_data, 10);
		}

		public byte[] Data
		{
			get
			{
				return m_data;
			}
		}

		//public byte[] ToByteArray()
		//{
		//    return m_data;
		//}
	}

	public class ProcessEntry32
	{
		private ArrayList member = new ArrayList();

		#region Indexer
		public int Count;

		public ProcessEntry32Member this[int index]
		{
			get
			{
				if (index > -1 & index < Count)
					return (ProcessEntry32Member)member[index];
				else
					return new ProcessEntry32Member(new byte[564]);
			}
			set
			{
				if (index > -1 & index < member.Count)
					member[index] = value;
				else if (index == member.Count)
				{
					member.Add(value);
					this.Count = index + 1;
				}
				else
					throw new IndexOutOfRangeException("Index out of bound");
			}
		}
		#endregion
	}
}
