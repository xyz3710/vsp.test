﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace TestIndexer02
{
	class MyString
	{
		private string data1 = "ABCDE";
		private string data2 = "GaNaDaRaMa";
		private int[] nums = new int[5];

		public int this[int index]
		{
			get
			{
				return nums[index];
			}
			set
			{
				this.nums[index] = value;
			}
			
		}

		public string this[string index]
		{
			get
			{
				string ret = string.Empty;

				if (index == "ENGLISH")
					ret = data1;
				else if (index == "KOREAN")
					ret = data2;

				return ret;
			}
			set
			{
				if (index == "ENGLISH")
					this.data1 = value;
				else if (index == "KOREAN")
					this.data2 = value;
				else
					throw new IndexOutOfRangeException();
			}
		}
	}

	public class GenericIndex
	{
		private ArrayList member = new ArrayList();
		private int count;

		public int Count
		{
			get
			{
				return count;
			}
		}

		//public <IndexType> this[int index]
		//{
		//    get
		//    {
		//    }
		//    set
		//    {
		//    }
		//}	
	}


	class Program
	{
		static void Main(string[] args)
		{
			MyString s = new MyString();

			s["ENGLISH"] = "I am American";
			s["KOREAN"] = "I am Korean";

			Console.WriteLine("By English : {0}", s["ENGLISH"]);
			Console.WriteLine("By Korean  : {0}", s["KOREAN"]);

			s[0] = 100;

			Console.WriteLine(s[0]);

			Console.WriteLine();

			Console.ReadLine();
		}
	}
}
