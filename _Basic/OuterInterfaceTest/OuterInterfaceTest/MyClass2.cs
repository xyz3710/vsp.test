﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OuterInterfaceTest
{
	public class MyClass2 : IUpdateModule
	{		
		#region IModule 멤버

		public void Start()
		{
			Console.WriteLine("MyClass2의 IModule 인터페이스를 구현했습니다.");
		}

		#endregion
	}
}
