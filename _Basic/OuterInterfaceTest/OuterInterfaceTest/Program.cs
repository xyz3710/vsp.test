﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Windows.Forms;

namespace OuterInterfaceTest
{
	class Program
	{
		static void Main(string[] args)
		{
			Assembly assembly = Assembly.LoadFile(string.Format("{0}\\{1}", Application.StartupPath, "OuterInterface.dll"));

			if (assembly != null)
			{
				object module = assembly.CreateInstance("OuterInterface.MyClass1");

				if (module != null)
				{
					Type moduleType = module.GetType();

					if (moduleType != null)
					{
						Type interfaceType = moduleType.GetInterface("IUpdateModule");

						if (interfaceType != null)
							interfaceType.InvokeMember("Start", BindingFlags.InvokeMethod, null, module, null);
					}
				}
			}

			IUpdateModule module2 = new MyClass2();

			if (module2 != null)
				module2.Start();
		}
	}
}
