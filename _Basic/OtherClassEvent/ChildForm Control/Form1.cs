using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Delegate2Class
{
	/// <summary>
	/// Form1에 대한 요약 설명입니다.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private TextBox txtBox;

		private Form2 childForm;

		public Form1()
		{
			//
			// Windows Form 디자이너 지원에 필요합니다.
			//
			InitializeComponent();

			//
			// TODO: InitializeComponent를 호출한 다음 생성자 코드를 추가합니다.
			//

			childForm = new Form2();
		}

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
this.button1 = new System.Windows.Forms.Button();
this.label1 = new System.Windows.Forms.Label();
this.txtBox = new System.Windows.Forms.TextBox();
this.SuspendLayout();
// 
// button1
// 
this.button1.Location = new System.Drawing.Point(104, 176);
this.button1.Name = "button1";
this.button1.Size = new System.Drawing.Size(64, 24);
this.button1.TabIndex = 0;
this.button1.Text = "button1";
this.button1.Click += new System.EventHandler(this.button1_Click);
// 
// label1
// 
this.label1.AutoSize = true;
this.label1.Font = new System.Drawing.Font("Gulim", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
this.label1.Location = new System.Drawing.Point(128, 72);
this.label1.Name = "label1";
this.label1.Size = new System.Drawing.Size(24, 24);
this.label1.TabIndex = 1;
this.label1.Text = "0";
// 
// txtBox
// 
this.txtBox.Location = new System.Drawing.Point(12, 240);
this.txtBox.Name = "txtBox";
this.txtBox.Size = new System.Drawing.Size(100, 21);
this.txtBox.TabIndex = 2;
// 
// Form1
// 
this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
this.ClientSize = new System.Drawing.Size(292, 273);
this.Controls.Add(this.txtBox);
this.Controls.Add(this.label1);
this.Controls.Add(this.button1);
this.Name = "Form1";
this.Text = "Form1";
this.Load += new System.EventHandler(this.Form1_Load);
this.ResumeLayout(false);
this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		//이벤트 발생시 차일드 폼의 함수 호출
		private void button1_Click(object sender, System.EventArgs e)
		{
			int temp;
			temp = Int32.Parse(label1.Text) + 1;
			label1.Text = temp.ToString();
			
			childForm.setLabel();

			label1.Text = tmp[2].ToString();

			Test.Inc();
			txtBox.Text = Test.count.ToString();
		}


		private void Form1_Load(object sender, System.EventArgs e)
		{
			for (int i = 0; i < tmp.Length; i++)
			{
				tmp[i] = (i+1)*i;
			}

			childForm.Show();
		}

		private static int[] tmp = new int[3];

		public static void staticTest()
		{
			tmp[2] += 100;
		}
	}

	public class Test
	{
		public static int count;

		public Test()
		{
			count = 10;
		}

		public static void Inc()
		{
			count++;
		}

		public void Dispose()
		{
			count = 0;
		}
	}
}
