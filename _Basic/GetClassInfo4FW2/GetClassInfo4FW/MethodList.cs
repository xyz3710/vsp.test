using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections;

namespace GetClassInfo4FW
{
	public class MethodList
	{
		private string _baseClassName;
		private string _name;
		private Type _type;
		private ParamList _parameterList;

		#region Indexer

		private ArrayList _alIndexer = new ArrayList();
		private int _indexerCnt;

		/// <summary>
		/// MethodList Indexer
		/// </summary>
		/// <param name="index">index</param>
		/// <returns>MethodList</returns>
		public MethodList this[int index]
		{
			get
			{
				if (index > -1 && index < _alIndexer.Count)
					return (MethodList)_alIndexer[index];
				else
					return null;
			}
			set
			{
				if (index > -1 && index < _alIndexer.Count)
					_alIndexer[index] = value;
				else if (index == _alIndexer.Count)
				{
					_alIndexer.Add(value);
					_indexerCnt++;
				}
				else
					throw new IndexOutOfRangeException();
			}
		}

		/// <summary>
		/// Indexer Count 
		/// </summary>
		public int IndexerCount
		{
			get
			{
				return _indexerCnt;
			}
		}

		#endregion

		#region Properties
		/// <summary>
		/// BaseClassName을(를) 구하거나 설정합니다.
		/// </summary>
		public String BaseClassName
		{
			get
			{
				return _baseClassName;
			}
			set
			{
				_baseClassName = value;
			}
		}

		/// <summary>
		/// Name을(를) 구하거나 설정합니다.
		/// </summary>
		public String Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// Type을(를) 구하거나 설정합니다.
		/// </summary>
		public Type Type
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}

		/// <summary>
		/// ParameterList을(를) 구하거나 설정합니다.
		/// </summary>
		public ParamList ParameterList
		{
			get
			{
				return _parameterList;
			}
			set
			{
				_parameterList = value;
			}
		}

		#endregion
        
	}
}
