using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections;
using System.IO;

namespace GetClassInfo4FW
{

	public class CreateSP
	{
		private readonly string _userName = "";

		private Assembly _cfAssembly;

		public CreateSP(string assemblyPath)
		{
			_cfAssembly = Assembly.LoadFrom(assemblyPath);
		}

		/// <summary>
		/// Print List
		/// </summary>
		public void PrintList()
		{
			Type[] cfTypes = _cfAssembly.GetTypes();
			GetClassInfo getClassInfo = new GetClassInfo();

			Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", "Factory", "BaseClass", "MethodName", "Parameters", "Type");

			foreach (Type type in cfTypes)
			{
				if (type.Name.IndexOf("Factory") < 0)
					continue;

				getClassInfo.Type = type;
				MethodList mList = getClassInfo.GetMethodList();

				for (int i = 0; i < mList.IndexerCount; i++)
				{
					Console.WriteLine("{0}\t{1}\t{2}\t\t{3}", type.Name, mList[i].BaseClassName, mList[i].Name.PadRight(8), mList[i].Type.FullName);

					for (int j = 0; j < mList[i].ParameterList.IndexerCount; j++)
					{
						ParamList p = mList[i].ParameterList[j];

						Console.WriteLine("{0}\t{1}\t\t{2}\t{3}", type.Name, mList[i].BaseClassName, p.Name.PadRight(8), p.Type.FullName);
					}
				}
			}
		}

		/// <summary>
		/// Save stored procedure fie
		/// </summary>
		public void Run()
		{
			Type[] cfTypes = _cfAssembly.GetTypes();
			GetClassInfo getClassInfo = new GetClassInfo();

			foreach (Type type in cfTypes)
			{
				if (type.Name.IndexOf("Factory") < 0)
					continue;

				getClassInfo.Type = type;
				MethodList mList = getClassInfo.GetMethodList();

				for (int i = 0; i < mList.IndexerCount; i++)
				{
					string spContext = string.Empty;
					string path = string.Empty;
					ParamList paramList = new ParamList();


					if (checkNoParam(mList[i].BaseClassName) == false)
						 paramList = mList[i].ParameterList;

					if (mList[i].Type.FullName.IndexOf("System.") < 0)
						// Return Cursor Type
						spContext = getCursorTypeReturnString(type.Name, mList[i].BaseClassName, mList[i].Name, paramList);
					else
						// Return System.Type
						spContext = getSystemTypeReturnString(type.Name, mList[i].BaseClassName, mList[i].Name, paramList);

					path = string.Format(@"{0}\{1}", @"H:\\SP", type.Name);

					if (Directory.Exists(path) == false)
						Directory.CreateDirectory(path);

					File.WriteAllText(string.Format(@"{0}\{1}{2}.sql", path, getFactoryPrefix(type.Name), mList[i].Name), spContext, Encoding.Default);
				}
			}
		}

		/// <summary>
		/// Parameter가 없는 stored procedure인지 체크
		/// </summary>
		/// <param name="baseClassName"></param>
		/// <returns></returns>
		private bool checkNoParam(string baseClassName)
		{
			string[] noParams = new string[] { "Block", "Box", "FinishedGoods", "Lot", "LotHst", "ReturnedGoods", "ReworkStatus", "SemiFinishedGoods", "Shelf", "Stocker" };

			for (int i = 0; i < noParams.Length; i++)
				if (noParams[i] == baseClassName)
					return true;

			return false;
		}

		/// <summary>
		/// Get Factory prefix
		/// </summary>
		/// <param name="factory"></param>
		/// <returns></returns>
		private string getFactoryPrefix(string factory)
		{
			string ret = string.Empty;
			
			if (factory == "CodeFactory")
				ret = "CD";
			else
				ret = factory.Remove(2).ToUpper();

			return string.Format("UP_{0}_", ret);
		}

		/// <summary>
		/// getColumn에서만 호출해야 한다.
		/// </summary>
		/// <param name="baseClass"></param>
		/// <returns>Upper case</returns>
		private string getTableName(string baseClass)
		{
			string ret = baseClass;

			if (baseClass.IndexOf("Code") > 0 || baseClass == "LotType" || baseClass == "LotUnit" 
				|| baseClass == "LotGrade" || baseClass == "LotQuantityUnit")
				ret = "Code";
			else if (baseClass == "Wip")
				ret = "Lot";

			return ret.ToUpper();
		}

		/// <summary>
		/// IN parameter로 들어갈 string을 구한다.
		/// </summary>
		/// <param name="tableName">getTableName으로 구하기 전 값을 입력한다.</param>
		/// <param name="param"></param>
		/// <returns></returns>
		private string getColumns(string tableName, string[] param)
		{
			StringBuilder sb = new StringBuilder();
			string ret = string.Empty;
			string convertTable = getTableName(tableName);
			string convertParam = string.Empty;

			for (int i = 0; i < param.Length; i++)
			{
				convertParam = getCamelCase(param[i]);

				if (convertParam == "emailId" || convertParam == "addressId" || convertParam == "faxId" 
					|| convertParam == "homepageId" || convertParam == "ownerId" || convertParam == "telephoneId")
					convertTable = convertParam.Replace("Id", string.Empty).ToUpper();

				// sample (tableName : LotGrade, parameter : lotGradeDesc  => codeDesc)
				if (convertTable == "CODE")
					convertParam = string.Format("{0}{1}", convertTable.ToLower(), convertParam.Replace(getCamelCase(tableName), string.Empty));

				sb.AppendFormat("	{0}{1}	IN 	{2}.{3}%TYPE \n",
					i == 0 ? string.Empty : ",",
					convertParam,
					convertTable,
					convertParam);
			}

			return sb.ToString();
		}

		/// <summary>
		/// Get sample columns 
		/// </summary>
		/// <param name="types"></param>
		/// <returns></returns>
		private string getColumnsSample(Type[] types)
		{
			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < types.Length; i++)
				sb.Append(getTypeSampleValue(types[i]));

			return sb.ToString(); 
		}

		/// <summary>
		/// Get sample column values
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		private string getTypeSampleValue(Type type)
		{
			string ret = String.Empty;

			if (type == typeof(string))
				ret = "'0001', ";
			else if (type == typeof(int))
				ret = "1, ";
			else if (type == typeof(bool))
				ret = "1, ";
			else if (type == typeof(DateTime))
				ret = string.Format("{0}, ", DateTime.Now.ToShortDateString());
			else if (type == typeof(double))
				ret = "100.5, ";

            return ret;
		}

		/// <summary>
		/// Get Camel case
		/// </summary>
		/// <param name="p"></param>
		/// <returns></returns>
		private string getCamelCase(string p)
		{
			return string.Format("{0}{1}", p.Substring(0, 1).ToLower(), p.Substring(1));
		}

		/// <summary>
		/// Get Cursor type return string
		/// </summary>
		/// <param name="factory"></param>
		/// <param name="tableName"></param>
		/// <param name="methodName"></param>
		/// <param name="paramList"></param>
		/// <returns></returns>
		private string getCursorTypeReturnString(string factory, string tableName, string methodName, ParamList paramList)
		{
			StringBuilder sb = new StringBuilder();
			string spName = string.Format("{0}{1}", getFactoryPrefix(factory), methodName);

			sb.AppendFormat("CREATE OR REPLACE PROCEDURE {0}{1} \n", _userName, spName);
			sb.Append("( \n");
			sb.AppendFormat("{0}{1} ", getColumns(tableName, paramList.GetNames()), paramList.IndexerCount == 0 ? string.Empty : "\n");
			sb.AppendFormat("	{0}cur_OUT	OUT	TYPES.DataSet \n", paramList.IndexerCount == 0 ? string.Empty : ",");
			sb.Append(") \n");
			sb.Append("IS \n");
			sb.Append("/****************************************************************************** \n");
			sb.Append("DECLARE \n");
			sb.Append("	cur_OUT 	TYPES.DataSet; \n");
			sb.Append(" 	tableName	varchar(100); \n");
			sb.Append("BEGIN \n");
			sb.AppendFormat("	{0}({1}cur_OUT); \n", spName, getColumnsSample(paramList.GetTypes()));
			sb.Append(" \n");
			sb.Append("	LOOP \n");
			sb.Append("		FETCH cur_OUT INTO tableName; \n");
			sb.Append("		EXIT WHEN cur_OUT%NOTFOUND; \n");
			sb.Append(" \n");
			sb.Append("		DBMS_OUTPUT.PUT_LINE(tableName); \n");
			sb.Append("	END LOOP; \n");
			sb.Append("	CLOSE cur_OUT; \n");
			sb.Append("END; \n");
			sb.Append("******************************************************************************/ \n");
			sb.Append("BEGIN \n");
			sb.Append("	-- TODO: \n");
			sb.Append(" \n");
			sb.Append("		\n");
			sb.Append(" \n");
			sb.Append("	OPEN cur_OUT FOR \n");
			sb.Append("	SELECT \n");
			sb.Append("		table_name \n");
			sb.Append("		FROM USER_TABLES; \n");
			sb.Append(" \n");
			sb.Append("	EXCEPTION \n");
			sb.Append("		WHEN NO_DATA_FOUND THEN \n");
			sb.Append("			NULL; \n");
			sb.Append("		WHEN OTHERS THEN \n");
			sb.Append(" \n");
			sb.Append("		RAISE; \n");
			sb.Append(" \n");
			sb.AppendFormat("END {0}; \n", spName);
			sb.Append("/ ");

			return sb.ToString(); 
		}

		/// <summary>
		/// Get system type(int) return string
		/// </summary>
		/// <param name="factory"></param>
		/// <param name="tableName"></param>
		/// <param name="methodName"></param>
		/// <param name="paramList"></param>
		/// <returns></returns>
		private string getSystemTypeReturnString(string factory, string tableName, string methodName, ParamList paramList)
		{
			StringBuilder sb = new StringBuilder();
			string spName = string.Format("{0}{1}", getFactoryPrefix(factory), methodName);

			sb.AppendFormat("CREATE OR REPLACE PROCEDURE {0}{1} \n", _userName, spName);
			sb.Append("( \n");
			sb.AppendFormat("{0}{1} ", getColumns(tableName, paramList.GetNames()), paramList.IndexerCount == 0 ? string.Empty : "\n");
			sb.AppendFormat("	{0}affectedRow	OUT	number \n", paramList.IndexerCount == 0 ? string.Empty : ",");
			sb.Append(") \n");
			sb.Append("IS \n");
			sb.Append("/****************************************************************************** \n");
			sb.Append("DECLARE \n");
			sb.Append("	affectedRow number; \n");
			sb.Append("BEGIN \n");
			sb.AppendFormat("	{0}({1}affectedRow); \n", spName, getColumnsSample(paramList.GetTypes()));
			sb.Append(" \n");
			sb.Append("	DBMS_OUTPUT.PUT_LINE(affectedRow); \n");
			sb.Append("END; \n");
			sb.Append("******************************************************************************/ \n");
			sb.Append("BEGIN \n");
			sb.Append("	affectedRow := 1; \n");
			sb.Append("	-- TODO: \n");
			sb.Append(" \n");
			sb.Append("		\n");
			sb.Append(" \n");
			sb.Append("	EXCEPTION \n");
			sb.Append("		WHEN NO_DATA_FOUND THEN \n");
			sb.Append("			NULL; \n");
			sb.Append("		WHEN OTHERS THEN \n");
			sb.Append(" \n");
			sb.Append("		RAISE; \n");
			sb.Append(" \n");
			sb.AppendFormat("END {0}; \n", spName);
			sb.Append("/ ");
					
			return sb.ToString();			
		}
	}	
}
