using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections;
using System.IO;
using System.Data;

using iDASiT.TrustCore.Common;

namespace GetClassInfo4FW
{
	public class CreateSP
	{
		private readonly string _outPathRoot = @"H:\SP";
		private readonly string _userName = "";
		private const string PARAM_PREFIX = "p_";

		private Assembly _assembly;
		ArrayList _viewTable;
		private string _assemblyPath;

		#region Constructor
		/// <summary>
		/// CreateSP class의 instance를 생성합니다.
		/// </summary>
		public CreateSP()
		{
			_viewTable = new ArrayList();

			_viewTable.Add("BonusCode");
			_viewTable.Add("CreateCode");
			_viewTable.Add("SlotCode");
			_viewTable.Add("HoldCode");
			_viewTable.Add("TerminateCode");
			_viewTable.Add("LossCode");
			_viewTable.Add("LotGrade");
			_viewTable.Add("ReleaseCode");
			_viewTable.Add("RepairCode");
			_viewTable.Add("ReworkCode");
			_viewTable.Add("LotQuantityUnit");
			_viewTable.Add("LotType");
			_viewTable.Add("LotUnit");
			_viewTable.Add("ReworkStatus");
		}
        
		public CreateSP(string assemblyPath)
			: this()
		{
			_assembly = Assembly.LoadFrom(assemblyPath);
		}

		#endregion

		public string AssemblyPath
		{
			get
			{
				return _assemblyPath;
			}
			set
			{
				_assemblyPath = value;
				_assembly = Assembly.LoadFrom(_assemblyPath);
			}
		}

		/// <summary>
		/// Print List
		/// </summary>
		public void PrintList()
		{
			foreach (SPInfo spi in getSPInfoTotal().ToArray())
			{
				Console.WriteLine("[{0}]\t{1}\t{2})", spi.StoredProcedureName, spi.ClassName, spi.CRUD);

				foreach (ParamInfo pi in spi.ParamInfo.GetValues())
					Console.WriteLine("{0}\t\t{1}", pi.ColumnName, pi.IsPrimaryKey);
			}
		}

		/// <summary>
		/// Save stored procedure fie
		/// </summary>
		public void Run()
		{
			string builder = string.Empty;

			foreach (SPInfo spi in getSPInfoTotal().ToArray())
			{
				if (spi == null)
					continue;

				switch (spi.CRUD)
				{
					case CRUDType.Get:
						builder = getGetString(spi);

						break;
					case CRUDType.Create:
						builder = getCreateString(spi);

						break;
					case CRUDType.Update:
						builder = getUpdateString(spi);

						break;
					case CRUDType.Delete:
						builder = getDeleteString(spi);

						break;
				}

				Console.WriteLine(spi.StoredProcedureName);

				string path = string.Format(@"{0}\{1}", _outPathRoot, spi.StoredProcedureName.Substring(3, 2));

				if (Directory.Exists(path) == false)
					Directory.CreateDirectory(path);

				File.WriteAllText(string.Format(@"{0}\{1}.sql", path, spi.StoredProcedureName), builder, Encoding.Default);
			}

		}
		
		private SPInfo getSPInfoTotal()
		{
			Type[] cfTypes = _assembly.GetTypes();
			SPInfo spInfo = new SPInfo(string.Empty);
			int cnt = 0;

			foreach (Type type in cfTypes)
			{
				// 1. ITrustCoreBase를 구현한 class를 구한다
				if (type.GetInterface("ITrustCoreBase") == null)
					continue;

				ITrustCoreBase tCore = Activator.CreateInstance(type) as ITrustCoreBase;

				if (tCore == null)
				{
					Console.WriteLine("Error >>>>>>>>>> {0}", type.Name);

					continue;
				}

				foreach (MethodInfo mi in type.GetInterface("ITrustCoreBase").GetMethods())
				{
					CRUDType crud = CRUDType.None;

					if (mi.Name == "GetDataSet")
						crud = CRUDType.Get;
					else
						crud = (CRUDType)Enum.Parse(typeof(CRUDType), mi.Name);

					SPInfo spi = getSPInfoUnit(type.Name, tCore, crud);

					spInfo[cnt++] = spi;
				}
			}

			return spInfo; 
		}

		private SPInfo getSPInfoUnit(string className, ITrustCoreBase trustCore, CRUDType crud)
		{
			SPInfo spiRet = new SPInfo(trustCore.GetType().Namespace);

			foreach (PropertyInfo property in trustCore.GetType().GetProperties())
			{
				foreach (Attribute attr in property.GetCustomAttributes(false))
				{
					ColumnInfoAttribute ci = attr as ColumnInfoAttribute;

					if (ci != null)
					{
						spiRet.StoredProcedureName = string.Format("{0}{1}", crud, trustCore.GetType().Name);

						if (_viewTable.IndexOf(className) < 0)
							spiRet.ClassName = className.ToUpper();
						else
							spiRet.ClassName = string.Format("V_{0}", className.ToUpper());

						spiRet.CRUD = crud;

						if (property.PropertyType.IsArray == true)
							if (property.GetValue(trustCore, null) != null)
								spiRet.MaxArrayLength = ((object[])property.GetValue(trustCore, null)).Length;
						
						// PK는 CRUD 여부와 상관없이 포함시킨다(FK로 사용될 수 있다)
						if (ci.IsPrimaryKey == false)
						{
							switch (crud)
							{
								case CRUDType.Get:
									if (ci.Readable == false)
										continue;

									break;
								case CRUDType.Create:
									if (ci.Creatable == false)
										continue;

									break;
								case CRUDType.Update:
									if (ci.Updatable == false)
										continue;

									break;
								case CRUDType.Delete:
									if (ci.Deletable == false)
										continue;

									break;
							}
						}

						if (ci.Inheritable == false)
							continue;

						// Primary Key인 컬럼은 앞으로 이동 시킨다.
						string key = string.Format("{0}{1:00000}", ci.IsPrimaryKey == true ? "A" : "Z", ci.Seq);

						ParamInfo pi = new ParamInfo();

						pi.ColumnName = getCamelCase(ci.Name == string.Empty ? property.Name : ci.Name);
						pi.DbType = TrustCoreBase.GetDbType(property.PropertyType);

						try
						{
							pi.Value = property.GetValue(trustCore, null);
						}
						catch (Exception ex)
						{
							Console.WriteLine(ex.Message);
						}

						pi.IsPrimaryKey = ci.IsPrimaryKey;

						spiRet.ParamInfo[key] = pi;
					}
				}
			}

			if (spiRet.ParamInfo.IndexerCount > 0)
				return spiRet;
			else
				return null;
		}

		private string getCamelCase(string p)
		{
			return string.Format("{0}{1}", p.Substring(0, 1).ToLower(), p.Substring(1));
		}

		private string getDeleteString(SPInfo spi)
		{
			StringBuilder sb = new StringBuilder();

			sb.AppendFormat("CREATE OR REPLACE PROCEDURE {0}{1} \n", _userName, spi.StoredProcedureName);
			sb.Append("( \n");
			sb.Append(getColumns(spi));
			sb.Append(") \n");
			sb.Append("IS \n");
			sb.Append("/****************************************************************************** \n");
			sb.AppendFormat("EXEC {0}({1}); \n", spi.StoredProcedureName, getColumnsSample(spi));
			sb.Append("******************************************************************************/ \n");
			sb.Append("BEGIN \n");
			sb.AppendFormat("	DELETE FROM {0} {1}\n", spi.ClassName, getTableAlias(spi));
			sb.AppendFormat("		WHERE {0} ", getConditions(spi));
			sb.Append(" \n");
			sb.Append("	EXCEPTION \n");
			sb.Append("		WHEN NO_DATA_FOUND THEN \n");
			sb.Append("			NULL; \n");
			sb.Append("		WHEN OTHERS THEN \n");
			sb.Append(" \n");
			sb.Append("		RAISE; \n");
			sb.Append(" \n");
			sb.AppendFormat("END {0}; \n", spi.StoredProcedureName);
			sb.Append("/ \n");

			return sb.ToString();
		}

		private string getUpdateString(SPInfo spi)
		{
			StringBuilder sb = new StringBuilder();

			sb.AppendFormat("CREATE OR REPLACE PROCEDURE {0}{1} \n", _userName, spi.StoredProcedureName);
			sb.Append("( \n");
			sb.Append(getColumns(spi, true));
			sb.Append(") \n");
			sb.Append("IS \n");
			sb.Append("/****************************************************************************** \n");
			sb.AppendFormat("EXEC {0}({1}); \n", spi.StoredProcedureName, getColumnsSample(spi));
			sb.Append("******************************************************************************/ \n");
			sb.Append("BEGIN \n");
			sb.AppendFormat("	UPDATE {0} {1} \n", spi.ClassName, getTableAlias(spi));
			sb.Append("	SET \n");
			sb.Append(getColumnList4Update(spi));
			sb.AppendFormat(" 		WHERE {0} ", getConditions(spi));
			sb.Append(" \n");
			sb.Append("	EXCEPTION \n");
			sb.Append("		WHEN NO_DATA_FOUND THEN \n");
			sb.Append("			NULL; \n");
			sb.Append("		WHEN OTHERS THEN \n");
			sb.Append(" \n");
			sb.Append("		RAISE; \n");
			sb.Append(" \n");
			sb.AppendFormat("END {0}; \n", spi.StoredProcedureName);
			sb.Append("/ \n");

			return sb.ToString();
		}

		private string getCreateString(SPInfo spi)
		{
			StringBuilder sb = new StringBuilder();

			sb.AppendFormat("CREATE OR REPLACE PROCEDURE {0}{1} \n", _userName, spi.StoredProcedureName);
			sb.Append("( \n");
			sb.Append(getColumns(spi, true));
			sb.Append(") \n");
			sb.Append("IS \n");
			sb.Append("/****************************************************************************** \n");
			sb.AppendFormat("EXEC {0}({1}); \n", spi.StoredProcedureName, getColumnsSample(spi));
			sb.Append("******************************************************************************/ \n");
			sb.Append("BEGIN \n");
			sb.AppendFormat("	INSERT INTO {0} \n", spi.ClassName);
			sb.Append("	( \n");
			sb.Append(getColumnList(spi, true, false));
			sb.Append("	) \n");
			sb.Append("	VALUES \n");
			sb.Append("	( \n");
			sb.Append(getColumnList(spi, true, true));
			sb.Append("	); \n");
			sb.Append(" \n");
			sb.Append("	EXCEPTION \n");
			sb.Append("		WHEN NO_DATA_FOUND THEN \n");
			sb.Append("			NULL; \n");
			sb.Append("		WHEN OTHERS THEN \n");
			sb.Append(" \n");
			sb.Append("		RAISE; \n");
			sb.Append(" \n");
			sb.AppendFormat("END {0}; \n", spi.StoredProcedureName);
			sb.Append("/ \n");

			return sb.ToString();
		}

		private string getGetString(SPInfo spi)
		{
			StringBuilder sb = new StringBuilder();
			bool oneParam = spi.ParamInfo.IndexerCount == 0 ? true : false;

			sb.AppendFormat("CREATE OR REPLACE PROCEDURE {0}{1} \n", _userName, spi.StoredProcedureName);
			sb.Append("( \n");
			sb.Append(getColumns(spi));

			if (oneParam == false)
				sb.Append(" \n");

			sb.AppendFormat(" 	{0}cur_OUT	OUT	TYPES.DataSet \n", oneParam == false ? "," : string.Empty);
			sb.Append(") \n");
			sb.Append("IS \n");
			sb.Append("/****************************************************************************** \n");
			sb.Append("DECLARE \n");
			sb.Append("	cur_OUT 	TYPES.DataSet; \n");
			sb.Append(" 	tableName	varchar(100); \n");
			sb.Append("BEGIN \n");
			sb.AppendFormat("	{0}({1}{2}cur_OUT); \n", spi.StoredProcedureName, getColumnsSample(spi), oneParam == false ? ", " : string.Empty);
			sb.Append(" \n");
			sb.Append("	LOOP \n");
			sb.Append("		FETCH cur_OUT INTO tableName; \n");
			sb.Append("		EXIT WHEN cur_OUT%NOTFOUND; \n");
			sb.Append(" \n");
			sb.Append("		DBMS_OUTPUT.PUT_LINE(tableName); \n");
			sb.Append("	END LOOP; \n");
			sb.Append("	CLOSE cur_OUT; \n");
			sb.Append("END; \n");
			sb.Append("******************************************************************************/ \n");
			sb.Append("BEGIN \n");
			sb.Append("	OPEN cur_OUT FOR \n");
			sb.Append("	SELECT \n");
			sb.Append(getColumnList(spi, false, false));
			sb.AppendFormat("		FROM {0} {1} \n", spi.ClassName, getTableAlias(spi));
			sb.AppendFormat("		WHERE {0} \n", getConditions(spi));
			sb.Append(" \n");
			sb.Append("	EXCEPTION \n");
			sb.Append("		WHEN NO_DATA_FOUND THEN \n");
			sb.Append("			NULL; \n");
			sb.Append("		WHEN OTHERS THEN \n");
			sb.Append(" \n");
			sb.Append("		RAISE; \n");
			sb.Append(" \n");
			sb.AppendFormat("END {0}; \n", spi.StoredProcedureName);
			sb.Append("/ \n");

			return sb.ToString();
		}

		private string getTableAlias(SPInfo spi)
		{
			return string.Format("T{0}", spi.ClassName.Substring(0, 2));
		}

		private string getConditions(SPInfo spi)
		{
			StringBuilder sb = new StringBuilder();
			int pkCnt = 0;

			foreach (ParamInfo pi in spi.ParamInfo.GetValues())
			{
				if (pi.IsPrimaryKey == true)
					sb.AppendFormat("{0}{1}.{2} = {3}{2}",
							pkCnt++ > 0 ? "\n			AND " : string.Empty,
							getTableAlias(spi), pi.ColumnName, PARAM_PREFIX);
			}

			sb.Append("; \n");

			return sb.ToString();
		}

		private string getColumnList(SPInfo spi, bool isVariableOnly, bool isParam)
		{
			StringBuilder sb = new StringBuilder();
			int cnt = 0;

			foreach (ParamInfo pi in spi.ParamInfo.GetValues())
			{
				sb.AppendFormat("		{0}{1}{2}{3} \n", 
					cnt++ > 0 ? "," : string.Empty,
					isVariableOnly == false ? getTableAlias(spi) + "." : string.Empty,
					isParam == true ? PARAM_PREFIX : string.Empty,
					pi.ColumnName);
			}

			return sb.ToString();
		}
		
		private string getColumnList4Update(SPInfo spi)
		{
			StringBuilder sb = new StringBuilder();
			int cnt = 0;

			foreach (ParamInfo pi in spi.ParamInfo.GetValues())
			{
				sb.AppendFormat("		{0}{1}.{2} = {3}{2} \n", 
							cnt++ > 0 ? "," : string.Empty,
							getTableAlias(spi), pi.ColumnName, PARAM_PREFIX);
			}

			return sb.ToString();
		}

		private string getColumns(SPInfo spi)
		{
			bool isAllParameter = false;

			return getColumns(spi, isAllParameter);
		}

        private string getColumns(SPInfo spi, bool isAllParameter)
		{
			StringBuilder sb = new StringBuilder();
			int cnt = 0;

			foreach (ParamInfo pi in spi.ParamInfo.GetValues())
			{
				if (isAllParameter == true || pi.IsPrimaryKey == true)
				{
					sb.AppendFormat("	{0}{1}{2}	IN 	{3}.{2}%TYPE \n",
						cnt++ > 0 ? "," : string.Empty,
						PARAM_PREFIX, pi.ColumnName, spi.ClassName);
				}
			}
			
			return sb.ToString();
		}
		
		private string getColumnsSample(SPInfo spi)
		{
			StringBuilder sb = new StringBuilder();
			int cnt = 1;

			foreach (ParamInfo pi in spi.ParamInfo.GetValues())
			{
				sb.AppendFormat("{0}{1}",
					getTypeSampleValue(pi.DbType),
					cnt++ < spi.ParamInfo.IndexerCount ? ", " : string.Empty);
			}

			return sb.ToString();
		}

		private string getTypeSampleValue(DbType dbType)
		{
			string ret = String.Empty;

			switch (dbType)
			{
				case System.Data.DbType.AnsiString:
					
					break;
				case System.Data.DbType.Object:
				case System.Data.DbType.Binary:
					
					break;
				case System.Data.DbType.Currency:
					
					break;
				case System.Data.DbType.Date:
				case System.Data.DbType.DateTime:
				case System.Data.DbType.Time:
					ret = string.Format("{0}", DateTime.Now.ToShortDateString());

					break;
				case System.Data.DbType.Byte:
				case System.Data.DbType.Boolean:
				case System.Data.DbType.UInt16:
				case System.Data.DbType.UInt32:
				case System.Data.DbType.UInt64:
				case System.Data.DbType.VarNumeric:
				case System.Data.DbType.AnsiStringFixedLength:
				case System.Data.DbType.Int16:
				case System.Data.DbType.Int32:
				case System.Data.DbType.Int64:
				case System.Data.DbType.SByte:
				case System.Data.DbType.Single:
					ret = "1";

					break;
				case System.Data.DbType.Decimal:
				case System.Data.DbType.Double:
					ret = "100.5";

					break;
				case System.Data.DbType.Guid:
					
					break;
				case System.Data.DbType.String:
					ret = "'0001'";

					break;
				case System.Data.DbType.StringFixedLength:
					
					break;
				case System.Data.DbType.Xml:
					
					break;
			}

			return ret;
		}
	}
}
