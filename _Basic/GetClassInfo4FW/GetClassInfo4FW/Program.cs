﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections;
using System.IO;

namespace GetClassInfo4FW
{
	class Program
	{
		private static void Main(string[] args)
		{
			CreateSP createSP = new CreateSP();
			string rootPath = @"C:\iDASiT\Bin";
			string[] fileList = Directory.GetFiles(rootPath, "*.dll");

			foreach (string file in fileList)
			{
				createSP.AssemblyPath = file;
				createSP.Run();
			}			

			//createSP.PrintList();
		}
	}
}