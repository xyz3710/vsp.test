using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace GetClassInfo4FW
{
	public class SPInfo : iDASiT.TrustCore.Common.StoredProcInfo
	{
		private string _className;
		private CRUDType _crud;

		/// <summary>
		/// Initializes a new instance of the SPInfo class.
		/// </summary>
		public SPInfo(string nameSpace)
			: base(nameSpace)
		{
		}

		#region Indexer

		protected ArrayList _alIndexer = new ArrayList();
		private int _indexerCnt;

		/// <summary>
		/// SPInfo Indexer
		/// </summary>
		/// <param name="index">index</param>
		/// <returns>SPInfo</returns>
		public SPInfo this[int index]
		{
			get
			{
				if (index > -1 && index < _alIndexer.Count)
					return _alIndexer[index] as SPInfo;
				else
					return null;
			}
			set
			{
				if (index > -1 && index < _alIndexer.Count)
					_alIndexer[index] = value;
				else if (index == _alIndexer.Count)
				{
					_alIndexer.Add(value);
					_indexerCnt++;
				}
				else
					throw new IndexOutOfRangeException();
			}
		}

		public Array ToArray()
		{
			return _alIndexer.ToArray();
		}

		/// <summary>
		/// Indexer Count 
		/// </summary>
		public int IndexerCount
		{
			get
			{
				return _indexerCnt;
			}
		}
				
		#endregion
        
		public string ClassName
		{
			get
			{
				return _className;
			}
			set
			{
				_className = value;
			}
		}

		public CRUDType CRUD
		{
			get
			{
				return _crud;
			}
			set
			{
				_crud = value;
			}
		}
	}


	#region CRUDType
	public enum CRUDType
	{
		None=0,
		/// <summary>
		/// Select(don't change this enum) <br />
		/// used by Stored procedure prefix.
		/// </summary>
		Get,
		/// <summary>
		/// Create
		/// </summary>
		Create,
		/// <summary>
		/// Update
		/// </summary>
		Update,
		/// <summary>
		/// Delete
		/// </summary>
		Delete,
	}
	#endregion

}
