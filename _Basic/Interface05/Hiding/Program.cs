﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiding
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine();

			EditBox edit = new EditBox();

			Console.WriteLine("Calling EditBox.Bind()...");
			//edit.Bind();

			Console.WriteLine();

			IDataBound bound = edit as IDataBound;
			
			if (bound != null)
			{
				Console.WriteLine("Calling (IDataBound)EditBox.Bind()...");
				bound.Bind();
			}
		}
	}
}