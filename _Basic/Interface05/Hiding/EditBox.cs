using System;
using System.Collections.Generic;
using System.Text;

namespace Hiding
{
	public class EditBox : IDataBound
	{

		public EditBox()
		{

		}

		void Bind()
		{
			Console.WriteLine("Binding to data store...(암시적 구현)");
		}

		#region IDataBound 멤버

		void IDataBound.Bind()
		{
			Console.WriteLine("Binding to data store...(명시적 구현)");
		}

		#endregion
	}
}
