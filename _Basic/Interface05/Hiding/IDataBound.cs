using System;
using System.Collections.Generic;
using System.Text;

namespace Hiding
{
	public interface IDataBound
	{
		void Bind();
	}
}
