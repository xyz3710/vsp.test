using System;
using System.Collections.Generic;
using System.Text;

namespace Interface05
{
	public class MyControl : FactoryControl, IValidate
	{
		public MyControl()
		{
			Data = "My Grid data";
		}

		#region IValidate ���

		public bool Validate()
		{
			Console.WriteLine("Validating... {0}", Data);

			return true; 
		}

		#endregion
	}

}
