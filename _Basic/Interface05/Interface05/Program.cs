﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interface05
{
	class Program
	{
		static void Main(string[] args)
		{
			MyControl myControl = new MyControl();
			IValidate val = myControl as IValidate;
			bool success = val.Validate();

			Console.WriteLine("The validation of '{0}' was {1} successful",
				myControl.Data,
				success == true ? string.Empty : "not ");
			
		}
	}
}