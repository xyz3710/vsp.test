using System;
using System.Collections.Generic;
using System.Text;

namespace Interface05
{

	public class FactoryControl
	{
		private string _data;

		public FactoryControl()
		{

		}

		#region Properties
		/// <summary>
		/// Data을(를) 구하거나 설정합니다.
		/// </summary>
		public String Data
		{
			get
			{
				return _data;
			}
			set
			{
				_data = value;
			}
		}

		#endregion
        
	}

}
