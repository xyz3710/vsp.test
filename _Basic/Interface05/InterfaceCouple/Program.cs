﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceCouple
{
	class Program
	{
		static void Main(string[] args)
		{
			MyTreeView tree = new MyTreeView();

			tree.Drag();
			tree.Drop();
			tree.Serialize();
		}
	}
}