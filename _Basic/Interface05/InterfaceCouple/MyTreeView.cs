using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceCouple
{

	public class MyTreeView : Control, ICombo
	{

		public MyTreeView()
		{

		}

		#region IDragDrop 멤버

		public void Drag()
		{
			Console.WriteLine("MyTreeView.Drag called");
		}

		public void Drop()
		{
			Console.WriteLine("MyTreeView.Drop called");
		}

		#endregion

		#region ISerializable 멤버

		public void Serialize()
		{
			Console.WriteLine("MyTreeView.Serialize called");
		}

		#endregion
	}

}
