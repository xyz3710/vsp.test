﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestInterface03
{
	interface IKorea
	{
		void Speaker();
	}

	interface IEnglish
	{
		void Speaker();
	}

	interface IJapanese
	{
		void Speaker();
	}

	public class Speaker : IKorea, IEnglish, IJapanese
	{
		#region IKorea Members

		void IKorea.Speaker()
		{
			Console.WriteLine("안녕하세요");
		}

		#endregion

		#region IEnglish Members

		void IEnglish.Speaker()
		{
			Console.WriteLine("Hi~~~!!");
		}

		#endregion

		#region IJapanese Members

		void IJapanese.Speaker()
		{
			Console.WriteLine("오하이요");
		}

		#endregion
	}

	class Stream
	{
		public void Open()
		{
			Console.WriteLine("Open");
		}
	}

	class FileStream : Stream
	{
		public void Delete()
		{
			Console.WriteLine("Delete");
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Speaker man = new Speaker();
			
			// man has not method

			IKorea korean = (IKorea)man;
			korean.Speaker();

			IEnglish english = (IEnglish)man;
			english.Speaker();

			IJapanese japanese = new Speaker();
			japanese.Speaker();


			FileStream fs1 = new FileStream();		// fs는 자기 자신 객체와 부모 객체에 접근 가능

			Console.WriteLine("[FileStream1]");
			fs1.Open();
			fs1.Delete();


			Stream s = new FileStream();			// s는 자기 자신 객체에만 접근 가능
			
			Console.WriteLine("[Stream]");
			s.Open();

	
			Console.ReadLine();
		}
	}
}
