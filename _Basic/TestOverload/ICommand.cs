﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestOverload
{
	interface ICommand
	{
		void TurnOnCommand();
		void TurnOffCommand();
	}
}
