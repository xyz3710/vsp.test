﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestOverload
{
	public class Radio : PowerModule, ICommand
	{
		public override void TurnOn()
		{
			Count += 5;
			Console.WriteLine("Radio turn on");
		}

		public override void TurnOff()
		{
			Count -= 5;
			Console.WriteLine("Radio turn off");
		}

		#region ICommand 멤버

		public void TurnOnCommand()
		{
			Console.WriteLine("Radio turn on command");
		}

		public void TurnOffCommand()
		{
			Console.WriteLine("Radio turn off command");
		}

		#endregion
	}
}
