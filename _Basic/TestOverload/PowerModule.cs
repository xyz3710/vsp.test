﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestOverload
{
	public class PowerModule
	{
		/// <summary>
		/// Count를 구하거나 설정합니다.
		/// </summary>
		public int Count
		{
			get;
			set;
		}

		public virtual void TurnOn()
		{
			Console.WriteLine("PowerModule turn on");
		}

		public virtual void TurnOff()
		{
			Console.WriteLine("PowerModule turn off");
		}
	}
}
