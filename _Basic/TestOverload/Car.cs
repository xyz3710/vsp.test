﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestOverload
{
	public class Car : PowerModule, ICommand
	{
		public override void TurnOn()
		{
			Count += 10;
			base.TurnOn();
			Console.WriteLine("Car turn on");
		}

		public override void TurnOff()
		{
			Count -= 10;
			Console.WriteLine("Car turn off");
		}

		#region ICommand 멤버

		public void TurnOnCommand()
		{
			Console.WriteLine("Car turn on command");
		}

		public void TurnOffCommand()
		{
			Console.WriteLine("Car turn off command");
		}

		#endregion
	}
}
