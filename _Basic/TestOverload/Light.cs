﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestOverload
{
	public class Light : PowerModule, ICommand
	{
		public override void TurnOn()
		{
			Count += 3;
			Console.WriteLine("Light turn on");
		}

		public override void TurnOff()
		{
			Count -=3;
			Console.WriteLine("Light turn off");
		}

		#region ICommand 멤버

		public void TurnOnCommand()
		{
			Console.WriteLine("Light turn on command");
		}

		public void TurnOffCommand()
		{
			Console.WriteLine("Light turn off command");
		}

		#endregion
	}
}
