﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestInterface04
{
	interface INothing
	{
		void INothing();
	}

	interface IKorea
	{
		void Speak();
	}

	class Speaker : IKorea
	{
		public void Nothing()
		{
		}

		public void Speak()
		{
			Console.WriteLine("안녕하세요");
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Speaker man = new Speaker();

			if (man is INothing)	// 객체가 인터페이스와 호환이 가능하지 여부 체크
				Console.WriteLine("INothing 지원");
			else
				Console.WriteLine("INothing 지원 안함");


			IKorea kman1 = man as IKorea; // 인터페이스와 호환 가능 여부 체크 후 성공하면 변환값 반환

			if (kman1 != null)
				kman1.Speak();
			else
				Console.WriteLine("IKorea 지원 안함");


			IKorea kman2 = man as INothing;		// error

			if (kman2 != null)
				Console.WriteLine("INothing 지원함");
			else
				Console.WriteLine("INothing 지원 안함");


			Console.ReadLine();
		}
	}
}
