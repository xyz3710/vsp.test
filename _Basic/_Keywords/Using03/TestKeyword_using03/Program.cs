﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestKeyword_using03
{
	public class C : IDisposable
	{
		public void UseLimitedResource()
		{
			Console.WriteLine("using limited resource...");
		}
		
		#region IDisposable Members

		void IDisposable.Dispose()
		{
			Console.WriteLine("Disposing limited resource.");
		}

		#endregion
	}
  
	class Program
	{
		static void Main(string[] args)
		{
			using (C c = new C())
			{
				c.UseLimitedResource();
			}

			Console.WriteLine("Now outside using statments.");
			Console.ReadLine();

		}
		extern alias
	}
}
