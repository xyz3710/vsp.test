﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestKeyword_implicit01
{
	class MyType
	{
		private int m_int = int.MinValue;
		private string m_string = string.Empty;

		public MyType()
		{
		}	

		public MyType(object type)
		{
			if (type is int)
			{
				m_int = (int)type;
			}
			else if (type is string)
			{
				m_string = type as string;
			}
			else
				throw new InvalidCastException();
		}
	
		public static implicit operator int(MyType i)
		{
			// code to convert from MyType to int
			return i.m_int;
		}

		public static implicit operator string(MyType s)
		{
			return s.m_string;
		}
	}
  
	class Program
	{
		static void Main(string[] args)
		{
			int i = 0;
			int j = 0;
			string s = string.Empty;

			try
			{
				j = new MyType(0x10);
				s = new MyType("string");
				i = new MyType(0.2);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			finally
			{
				Console.WriteLine(i);
				Console.WriteLine(j);
				Console.WriteLine(s);
			}

			Console.ReadLine();
		}
	}
}
