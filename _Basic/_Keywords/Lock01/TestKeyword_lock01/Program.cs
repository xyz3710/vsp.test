﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TestKeyword_lock01
{
	class ThreadTest
	{
		public void RunMe()
		{
			Console.WriteLine("RunMe called.");
		}

		static void Main(string[] args)
		{
			ThreadTest b = new ThreadTest();
			Thread t = new Thread(new ThreadStart(b.RunMe));

			lock (b)
			{
				t.Start();
			}			
		}
	}
}
