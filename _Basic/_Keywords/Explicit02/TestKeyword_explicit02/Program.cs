﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestKeyword_explicit02
{
	struct Digit
	{
		private byte value;

		public Digit(byte value)
		{
			if (value > 9)
			{
				throw new ArgumentException();
			}

			this.value = value;
		}
	
		// Define explicit byte-to-Digit conversion operator:
		public static explicit operator Digit(byte b)
		{
			Digit d = new Digit(b);

			Console.WriteLine("byte {0} to Digit Conversion occurred.", b);

			return d;
		}
	} 

	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				byte b = 3;
				Digit d = (Digit)b;		// explicit conversion		
			}
			catch (Exception e)
			{
				Console.WriteLine("{0} Exception caught.", e);
				// throw new Exception();
			}

			Console.ReadLine();
		}
	}
}
