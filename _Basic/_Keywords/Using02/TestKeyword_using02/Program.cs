﻿// directive using statement
using System;
using System.Collections.Generic;
using System.Text;
using AliasToMyClass=NameSpace1.MyClass;

namespace NameSpace1
{
	public class MyClass
	{
		public override string ToString()
		{
			return "You are in NameSpace1.MyClass";
		}
	}  
}

namespace NameSpace2
{
	public class MyClass
	{

	}
  
}

namespace NameSpace3
{
	using NameSpace1;
	using NameSpace2;

	class Program
	{
		static void Main(string[] args)
		{
			AliasToMyClass someVar = new AliasToMyClass();

			Console.WriteLine(someVar);

			Console.ReadLine();

		}
	}
}
