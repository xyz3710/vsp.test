﻿// The as operator.
using System;
using System.Collections.Generic;
using System.Text;

namespace TestKeyword_as01
{
	class Class1
	{

	}

	class Class2
	{

	}
  
	class Program
	{
		static void Main(string[] args)
		{
			object[] objArr = new object[6]{
				new Class1(),
				new Class2(),
				"hello",
				123,
				123.4,
				null
			};

			for (int i = 0; i < objArr.Length; i++)
			{
				string s = objArr[i] as string;

				Console.WriteLine("{0} : ", i);

				if (s != null)
				{
					Console.WriteLine("'" + s + "'");
				}
				else
				{
					Console.WriteLine("not a string");				
				}				
			}

			Console.ReadLine();

		}
	}
}
