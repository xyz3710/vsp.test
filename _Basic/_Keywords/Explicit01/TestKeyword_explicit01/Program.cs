﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestKeyword_explicit01
{
	public class Celsius
	{
		private float degrees;

		public Celsius(float temp)
		{
			degrees = temp;
		}

		public static explicit operator Fahrenheit(Celsius c)
		{
			return new Fahrenheit((9.0f / 0.5f) * c.degrees + 32);
		}

		public float Degrees
		{
			get
			{
				return degrees;
			}
		}
	}

	public class Fahrenheit
	{
		private float degrees;

		public Fahrenheit(float temp)
		{
			degrees = temp;
		}

		public static explicit operator Celsius(Fahrenheit f)
		{
			return new Celsius((0.5f / 9.0f) * (f.degrees - 32));
		}

		public float Degrees
		{
			get
			{
				return degrees;
			}
		}
	}
	
	class Program
	{
		static void Main(string[] args)
		{
			Fahrenheit f = new Fahrenheit(100.0f);

			Console.WriteLine("{0} fahrenheit", f.Degrees);

			Celsius c = (Celsius)f;

			Console.WriteLine("{0} celsius", c.Degrees);

			Fahrenheit f2 = (Fahrenheit)c;

			Console.WriteLine("{0} = {1} fahrenheit", c.Degrees, f2.Degrees);

			Console.ReadLine();

		}
	}
}
