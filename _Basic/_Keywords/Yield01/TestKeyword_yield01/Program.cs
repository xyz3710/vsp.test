﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace TestKeyword_yield01
{
	public class List
	{
		public static IEnumerable Power(int number, int exponent)
		{
			int counter = 0;
			int result = 1;

			while (counter++ < exponent)
			{
				result *= number;

				yield return result;
			}
	
		}
	}
  
	class Program
	{
		static void Main(string[] args)
		{
			// display powers of 2 up to the exponent 8:
			foreach (int i in List.Power(2, 8))
			{
				Console.Write("{0} ", i);
			}

			Console.ReadLine();
		}
	}
}
