﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestKeyword_parrams01
{
	class Program
	{
		// params를 빼게 되면 아래 UseParams(1, 2, 3)의 3개의 인자값이
		// 전달이 안된다 
		// params 사용시 추가적인 인자값을 입력할 수 없다
		public static void UseParams(params int[] list)		
		{
			for (int i = 0; i < list.Length; i++)
			{
				Console.WriteLine(list[i]);
			}

			Console.WriteLine("");
		}

		public static void UseParams2(params object[] list)
		{
			for (int i = 0; i < list.Length; i++)
			{
				Console.WriteLine(list[i]);
			}

			Console.WriteLine("");
		}

		static void Main(string[] args)
		{
			UseParams(1, 2, 3);
			UseParams2(1, 'a', "Test");

			// An array of objects can also be passed, as long as
			// the array type matches the method being called.
			int[] myArray = new int[3] { 10, 11, 12 };

			UseParams(new int[] { 1, 2, 3 });

			Console.ReadLine();
		}
	}
}
