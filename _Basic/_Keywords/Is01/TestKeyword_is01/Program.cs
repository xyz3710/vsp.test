﻿// The is operator.
using System;
using System.Collections.Generic;
using System.Text;

namespace TestKeyword_is01
{
	class Class1
	{
		public void Show()
		{
			Console.WriteLine("Class1 show");
				
		}
	}

	class Class2
	{
		public void Show()
		{
			Console.WriteLine("Class2 show");

		}
	}
  
	class Program
	{
		public static void Test(object o)
		{
			Class1 c1;
			Class2 c2;

			if (o is Class1)
			{
				Console.WriteLine("o is Class1");

				c1 = (Class1)o;				
				
				// do something with "c1"
				c1.Show();
			}
			else if (o is Class2)
			{
				Console.WriteLine("o is Class2");

				c2 = (Class2)o;
				
				// do something with "c2"
				c2.Show();
			}
			else
			{
				Console.WriteLine("o is neither Class1 nor Class2");				
			}
		}

		static void Main(string[] args)
		{
			Class1 c1 = new Class1();
			Class2 c2 = new Class2();

			Test(c1);
			Test(c2);
			Test("a string");

			Console.ReadLine();
		}
	}
}
