﻿// The operator stackalloc
// compile with: /unsafe
using System;
using System.Collections.Generic;
using System.Text;

namespace TestKeyword_stackalloc01
{
	class Program
	{
		static unsafe void Main(string[] args)
		{
			int* fib = stackalloc int[100];
			int* p = fib;

			*p++ = *p++ = 1;

			for (int i = 2; i < 100; ++i, ++p)
			{
				*p = p[-1] + p[-2];
			}

			for (int i = 0; i < 10; i++)
			{
				Console.WriteLine(fib[i]);
			}

			Console.ReadLine();
		}
	}
}
