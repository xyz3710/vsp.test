﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace TestTryCatch01
{
	class Program
	{
		static void OpenNWrite(string fileName)
		{
			FileStream fs = null;
			StreamWriter sw = null;
			
			try
			{
				fs = new FileStream(fileName, FileMode.OpenOrCreate | FileMode.Append);
				sw = new StreamWriter(fs);

				sw.WriteLine("{0}\t{1}", "01", "Test");
			}
			catch (IOException e)
			{
				Console.WriteLine("Error opening file {0}", fileName);
				Console.WriteLine(e);
			}
			finally
			{
				if (sw != null)
					sw.Close();
				
				if (fs != null)
					fs.Close();
			}
		}

		static void OpenNWrite2(string fileName)
		{
			using (FileStream fs = new FileStream(fileName, FileMode.Create))
			{
				try
				{
					StreamWriter sw = new StreamWriter(fs);

					sw.WriteLine("{0}\t{1}", "01", "Test");

					sw.Close();
					fs.Close();
				}
				catch (IOException e)
				{
					Console.WriteLine("Error opening file {0}.", fileName);
					Console.WriteLine(e);
				}
			}
		}

		static void Main(string[] args)
		{
			OpenNWrite2(@"D:\Temp\Test.txt");
			OpenNWrite(@"D:\Temp\Test.txt");

			Console.ReadLine();
		}
	}
}
