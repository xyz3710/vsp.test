﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TestKeyword_lock02
{
	class Account
	{
		private object thisLock = new object();
		private int balance;

		Random r = new Random();

		public Account(int initial)
		{
			balance = initial;
		}

		public void DoTransactons()
		{
			for (int i = 0; i < 100; i++)
			{
				withDraw(r.Next(1, 100));
			}			
		}

		private int withDraw(int amount)
		{
			// this condition will never be true unless the lock statement
			// is commented out : 
			if (balance < 0)
			{
				throw new Exception("Negative Balance");
			}

			// Comment out the next line to see the effec of leaving out
			// the lock keywork:
			lock (thisLock)
			{
				if (balance >= amount)
				{
					Console.WriteLine("Balance before withDraw1\t: " + balance);
					Console.WriteLine("Amount to withDraw\t\t: -" + amount );
					balance -= amount;
					Console.WriteLine("Balance after withDraw1\t\t: " + balance);
					Console.WriteLine("");

					return amount;
				}
				else
				{
					return 0;		// transaction rejected
				}
			}
		}
	}
	
	class Test
	{
		static int Main(string[] args)
		{
			Thread[] threads = new Thread[10];
			Account acc = new Account(1000);

			for (int i = 0; i < 10; i++)
			{
				threads[i] = new Thread(new ThreadStart(acc.DoTransactons));
			}

			for (int i = 0; i < 10; i++)
			{
				threads[i].Start();
			}

			Console.ReadLine();
						
			return 0;
		}

	}
}
