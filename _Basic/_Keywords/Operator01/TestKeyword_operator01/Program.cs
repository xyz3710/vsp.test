﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestKeyword_operator01
{
	internal class Fraction
	{
		private int num;
		private int den;

		public Fraction(int num, int den)
		{
			this.num = num;
			this.den = den;
		}
	
		// overload operator +
		public static Fraction operator+(Fraction a, Fraction b)
		{
			return new Fraction(a.num * b.den + b.num * a.den, a.den * b.den);
		}

		// overload operator *
		public static Fraction operator*(Fraction a, Fraction b)
		{
			return new Fraction(
					a.num * b.num, a.den * b.den
				);
		}

		// define operator double
		public static implicit operator double (Fraction f)
		{
			return (double)(f.num / f.den);
		}	
	}
   
	class Program
	{
		static void Main(string[] args)
		{
			Fraction a = new Fraction(10, 2);
			Fraction b = new Fraction(3, 7);
			Fraction c = new Fraction(2, 3);

			Console.WriteLine("{0}", (double)(a * b + c));
			Console.ReadLine();
		}
	}
}
