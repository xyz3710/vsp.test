﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestKeyword_checked01
{
	public class OverFlowTest
	{
		private static short x = 32767;		// max short value
		private static short y = 32767;		// 최대값

		// using a checked expression
		public static int CheckedMethod()
		{
			int z = 0;

			try
			{
				checked
				{
					z = (short)(x + y);
				}

				// or z = checked((short)(x + y));
			}
			catch (System.OverflowException e)
			{
				Console.WriteLine(e.ToString());
			}

			return z;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Checked output value is : {0}", OverFlowTest.CheckedMethod());

			const int x = int.MaxValue;
			const int y = 2;

			int z;
			
			unchecked
			{
				z = x * y;
			}
			Console.WriteLine("Unchecked output value : {0}", z);

			Console.ReadLine();
		}
	}
}
