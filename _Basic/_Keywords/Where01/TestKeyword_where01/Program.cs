﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace TestKeyword_where01
{
	public class MyClass<T, U> 
		where T : class
		where U : struct
	{

	}

	public class MyGenericClass <T> 
		where T : IComparable, new()
	{
		// The follwing line is not possible without new() constraint
		T itm = new T();
	}
   

	interface IWhere
	{

	}

	class Dictionary <TKey, TVal>
		where TKey : IComparable, IEnumerable
		where TVal : IWhere
	{
		public void Add(TKey key, TVal val)
		{
		}
	}

	class Program
	{
		public bool MyMethod<T>(T t)
			where T : IWhere
		{
			return true;
		}

		delegate T MyDelegate<T>() where T : new();

		static void Main(string[] args)
		{
		}
	}
}
