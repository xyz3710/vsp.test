﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestOperatorOverload01
{
	struct Vector
	{
		float x;
		float y;

		public Vector(float x, float y)
		{
			this.x = x;
			this.y = y;
		}

		public float Length
		{
			get
			{
				return ((float) Math.Sqrt(x*x + y*y));
			}
		}

		public static Vector operator*(Vector vector, float multiplier)
		{
			return (new Vector(vector.x*multiplier, vector.y*multiplier));
		}

		public static Vector operator+(Vector vector1, Vector vector2)
		{
			return (new Vector(vector1.x+vector2.x, vector2.y+vector2.y));
		}
	}


	class Program
	{
		static void Main(string[] args)
		{
		}
	}
}
