﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseInterface
{
	public partial class NonIdEntity : DbmlBase
	{
		/// <summary>
		/// EntityID를 구하거나 설정합니다.
		/// </summary>
		public int EntityID
		{
			get;
			set;
		}
	}
}
