﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseInterface
{
	partial class NonIdEntity
	{
		/// <summary>
		/// ID를 구하거나 설정합니다.
		/// </summary>
		public int ID
		{
			get
			{
				return EntityID;
			}
			set
			{
				EntityID = value;
			}
		}
	}
}
