﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseInterface
{
	public partial class Entity
	{
		/// <summary>
		/// ID를 구하거나 설정합니다.
		/// </summary>
		public int ID
		{
			get;
			set;
		}

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}
		
		
	}
}
