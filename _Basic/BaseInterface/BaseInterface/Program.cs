﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseInterface
{
	class Program
	{
		static void Main(string[] args)
		{
			Entity entity1 = new Entity
			{
				ID = 1,
				Name = "이름1",
			};
			Entity entity2 = new Entity
			{
				ID = 2,
				Name = "이름2",
			};

			NonIdEntity nonIdEntity1 = new NonIdEntity
			{
				EntityID = 11,
			};
			NonIdEntity nonIdEntity2 = new NonIdEntity
			{
				EntityID = 22,
			};

			NonIdEntity2 nonIdEntity21 = new NonIdEntity2
			{
				EntityID = 21,
			};
			NonIdEntity2 nonIdEntity22 = new NonIdEntity2
			{
				EntityID = 22,
			};

			IDbmlBase base1 = entity1 as DbmlBase;
			Console.WriteLine("Entity.ID: {0}", base1.ID);
			Console.WriteLine("NonIdEntity.ID: {0}", nonIdEntity1.ID);

			IDbmlBase iDbmlBase = nonIdEntity21 as IDbmlBase;

			Console.WriteLine("NonIdEntity2.ID: {0}", iDbmlBase.ID);
		}
	}
}
