﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace TestClassMember01
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			//TestMember tm = new TestMember();

			//tm.Service[0].Service = new Guid(0x00001115, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB);
			//tm.Service[0].Channel = 1;

			//textBox1.Text += tm.Service[0].Service.ToString() + ", " + tm.Service[0].Channel.ToString() + "\r\n";

			
			Services svc = new Services();

			svc[0] = new ServicesMember(
				new Guid(0x00001115, 0x0000, 0x1000, 0x80, 0x00, 0x00, 0x80, 0x5F, 0x9B, 0x34, 0xFB),
				1);

			textBox1.Text += svc[0].Service.ToString() + ", " + svc[0].Channel.ToString() + "\r\n";

			//ListView lv = new ListView();

			//lv.Items.Add
			//lv.Items[0].Text = "11";

			//textBox1.Text += lv.Items[0].Text;
		}
	}

	public class ServicesMember
	{
		private Guid service;
		private int channel;

		public ServicesMember(Guid service, int channel)
		{
			this.service = service;
			this.channel = channel;
		}

		public Guid Service
		{
			get
			{
				return service;
			}
			set
			{
				service = value;
			}
		}

		public int Channel
		{
			get
			{
				return channel;
			}
			set
			{
				channel = value;
			}
		}
	}

	public class Services
	{
		public Services()
		{
			member = new ArrayList();
		}

		#region Indexer
		private ArrayList member;

		public int Count;

		public ServicesMember this[int index]
		{
			get
			{
				if (index > -1 && index < Count)
					return (ServicesMember)member[index];
				else
					return new ServicesMember(new Guid(), 0);
			}
			set
			{
				if (index > -1 && index < member.Count)
					member[index] = value;
				else if (index == member.Count)
				{
					member.Add(value);
					this.Count = index + 1;
				}
				else
					throw new IndexOutOfRangeException("Index out of bound.");
			}
		}
		#endregion
	}

	//public class TestMember
	//{
	//    private Services service;

	//    public TestMember()
	//    {
	//        service = new Services();
	//    }

	//    public int Count;

	//    public Services Service
	//    {
	//        get
	//        {
	//            return service;
	//        }
	//        set
	//        {
	//            service = value;
	//        }
	//    }
	//}
}