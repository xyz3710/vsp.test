﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using NSCodeFactory;
using System.Collections;

namespace GetClassInfo
{
	class Program
	{
		private static void Main(string[] args)
		{
			//Assembly cfAssembly = Assembly.LoadFrom(@"D:\VSP\Test\_Class\GetClassInfo\CodeFactory\bin\Debug\CodeFactory.dll");
			Assembly cfAssembly = Assembly.LoadFrom(@"C:\iDASiT\iCube\ApplicationFoundation\iDASiT.ApplicationFoundation.Tracking\bin\Debug\iDASiT.ApplicationFoundation.Tracking.dll");
			Type[] cfTypes = cfAssembly.GetTypes();
			GetClassInfo getClassInfo = new GetClassInfo();
            
			foreach (Type type in cfTypes)
			{
				if (type.Name != "CodeFactory")
					continue;
				
				Console.WriteLine("{0}", type.Name);

				getClassInfo.Type = type;
				MethodList mList = getClassInfo.GetMethodList();

				for (int i = 0; i < mList.IndexerCount; i++)
				{
					Console.WriteLine("\t{0}\t\t{1}", mList[i].Name.PadRight(8), mList[i].Type.FullName);
					
					for (int j = 0; j < mList[i].ParameterList.IndexerCount; j++)
					{
						ParamList p = mList[i].ParameterList[j];

						Console.WriteLine("\t\t{0}\t\t{1}", p.Name.PadRight(8), p.Type.FullName);
					}
				}				
			}
		}
	}
}