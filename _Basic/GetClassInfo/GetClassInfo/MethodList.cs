using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using NSCodeFactory;
using System.Collections;

namespace GetClassInfo
{
	public class MethodList
	{
		private string _name;
		private Type _type;
		private ParamList _parameterList;

		#region Indexer
		private ArrayList _alIndexer = new ArrayList();
		private int _indexerCnt;

		/// <summary>
		/// MethodList Indexer
		/// </summary>
		/// <param name="index">index</param>
		/// <returns>MethodList</returns>
		public MethodList this[int index]
		{
			get
			{
				if (index > -1 && index < _alIndexer.Count)
					return (MethodList)_alIndexer[index];
				else
					return null;
			}
			set
			{
				if (index > -1 && index < _alIndexer.Count)
					_alIndexer[index] = value;
				else if (index == _alIndexer.Count)
				{
					_alIndexer.Add(value);
					_indexerCnt++;
				}
				else
					throw new IndexOutOfRangeException();
			}
		}

		/// <summary>
		/// Indexer Count 
		/// </summary>
		public int IndexerCount
		{
			get
			{
				return _indexerCnt;
			}
		}
		#endregion

		#region Properties
		/// <summary>
		/// Name??瑜? 援ы븯嫄곕굹 ?ㅼ젙?⑸땲??
		/// </summary>
		public String Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// Type??瑜? 援ы븯嫄곕굹 ?ㅼ젙?⑸땲??
		/// </summary>
		public Type Type
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}

		/// <summary>
		/// ParameterList??瑜? 援ы븯嫄곕굹 ?ㅼ젙?⑸땲??
		/// </summary>
		public ParamList ParameterList
		{
			get
			{
				return _parameterList;
			}
			set
			{
				_parameterList = value;
			}
		}

		#endregion

	}
}
