using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using NSCodeFactory;
using System.Collections;

namespace GetClassInfo
{
	public class ParamList
	{
		private Type _type;
		private string _name;

		#region Constructor
		/// <summary>
		/// ParamInfo class의 instance를 생성합니다.
		/// </summary>
		public ParamList()
		{
			_name = string.Empty;
		}
		#endregion
        
		#region Indexer
		private ArrayList _alIndexer = new ArrayList();
		private int _indexerCnt;

		/// <summary>
		/// ParamInfo Indexer
		/// </summary>
		/// <param name="index">index</param>
		/// <returns>ParamInfo</returns>
		public ParamList this[int index]
		{
			get
			{
				if (index > -1 && index < _alIndexer.Count)
					return (ParamList)_alIndexer[index];
				else
					return null;
			}
			set
			{
				if (index > -1 && index < _alIndexer.Count)
					_alIndexer[index] = value;
				else if (index == _alIndexer.Count)
				{
					_alIndexer.Add(value);
					_indexerCnt++;
				}
				else
					throw new IndexOutOfRangeException();
			}
		}

		/// <summary>
		/// Indexer Count 
		/// </summary>
		public int IndexerCount
		{
			get
			{
				return _indexerCnt;
			}
		}
		#endregion

		#region Properties
		/// <summary>
		/// TypeName
		/// </summary>
		public Type Type
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}

		/// <summary>
		/// Name
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		#endregion
	}
}
