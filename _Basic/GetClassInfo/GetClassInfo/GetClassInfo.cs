using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using NSCodeFactory;
using System.Collections;

namespace GetClassInfo
{
	public class GetClassInfo
	{
		private Type _type;

		#region Constructor
		/// <summary>
		/// GetClassInfo class의 instance를 생성합니다.
		/// </summary>
		public GetClassInfo()
		{
			
		}
		#endregion

		#region Properties
		/// <summary>
		/// Type을(를) 구하거나 설정합니다.
		/// </summary>
		public Type Type
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}

		#endregion
        
		public MethodList GetMethodList()
		{
			MethodInfo[] mi = _type.GetMethods();
			MethodList mList = new MethodList();
			string[] exceptMethods = getExceptMethodNames();
			int mCnt = 0;

			// Get/Set이 있는 property는 get_<Fieldname>, set_<Fieldname>을 생성하기 때문에 제거해줘야 한다.
			foreach (MethodInfo m in mi)
			{
				if (isExists<string>(exceptMethods, m.Name) == true)
					continue;

				if (m.Name.IndexOf("get_") < 0 && m.Name.IndexOf("set_") < 0)
				{
					ParamList pList = pList = getParamList(m);
					MethodList ml = new MethodList();

					ml.Name = m.Name;
					ml.Type = m.ReturnType;
					ml.ParameterList = pList;

					mList[mCnt++] = ml;
				}
			}

			return mList;
		}

		private bool isExists<T>(T[] array, T match)
		{
			bool ret = false;

			for (int i = 0; i < array.Length; i++)
			{
				if (array[i].Equals(match) == true)
				{
					ret = true;

					break;
				}
			}

			return ret;
		}

		private ParamList getParamList(MethodInfo m)
		{
			ParameterInfo[] piArray = m.GetParameters();
			ParamList pList = new ParamList();
			int pCnt = 0;

			foreach (ParameterInfo pi in piArray)
			{
				// User defined class
				if (pi.ParameterType.IsClass == true && pi.ParameterType.Namespace != "System")
				{
					ParamList paramInfo = getClassProperties2Param(pi.ParameterType);

					for (int i = 0; i < paramInfo.IndexerCount; i++)
						pList[pCnt++] = paramInfo[i];

					continue;
				}
				else
				{
					ParamList p = new ParamList();

					p.Type = pi.ParameterType;
					p.Name = pi.Name;

					pList[pCnt] = p;
				}

				pCnt++;
			}

			return pList;
		}

		private ParamList getClassProperties2Param(Type type)
		{
			ParamList retParamInfo = new ParamList();
			int pCnt = 0;

			foreach (PropertyInfo pInfo in type.GetProperties())
			{
				if (pInfo.CanRead == true && pInfo.CanWrite == true)
				{
					ParamList p = new ParamList();

					if (pInfo.PropertyType.IsArray == true)
						// Array type은 single로 변환한다
						p.Type = Type.GetType(pInfo.PropertyType.FullName.Replace("[]", string.Empty));
					else
						p.Type = pInfo.PropertyType;

					p.Name = getCamelType(pInfo.Name);

					retParamInfo[pCnt++] = p;
				}
			}

			return retParamInfo;
		}

		private string getCamelType(string p)
		{
			return string.Format("{0}{1}", p.Substring(0, 1).ToLower(), p.Substring(1));
		}

		private string[] getExceptMethodNames()
		{
			object o = new object();
			ArrayList alMethods = new ArrayList();

			foreach (MethodInfo m in o.GetType().GetMethods())
				alMethods.Add(m.Name);

			return alMethods.ToArray(typeof(string)) as string[];
		}
	}
}
