using System;
using System.Collections.Generic;
using System.Text;

namespace NSCodeFactory
{
	public class Address
	{
		private string _name;
		private string _addr;
		private int _subAddress;
		private string[] _email;
		#region Constructor
		/// <summary>
		/// Address class??instance�??�성?�니??
		/// </summary>
		public Address(string name, string addr, int subAddress)
		{
			_name = name;
			_addr = addr;
			_subAddress = subAddress;
			_email = new string[5];
		}

		public Address()
		{
			
		}
		#endregion

		#region Properties
		/// <summary>
		/// Name??�? 구하거나 ?�정?�니??
		/// </summary>
		public String Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// Address??�? 구하거나 ?�정?�니??
		/// </summary>
		public string Addr
		{
			get
			{
				return _addr;
			}
			set
			{
				_addr = value;
			}
		}

		/// <summary>
		/// SubAddress??�? 구하거나 ?�정?�니??
		/// </summary>
		public int SubAddress
		{
			get
			{
				return _subAddress;
			}
			set
			{
				_subAddress = value;
			}
		}

		public string[] Email
		{
			get
			{
				return _email;
			}
			set
			{
				_email = value;
			}
		}
		#endregion
	}
}
