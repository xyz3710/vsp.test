﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestInterface02
{
	interface ISound
	{
		/// <summary>
		/// Volume Control method.
		/// </summary>
		void VolumeControl();
		/// <summary>
		/// Speaker Out method.
		/// </summary>
		void SpeakerOut();
	}

	interface IMouse
	{
		/// <summary>
		/// Move method.
		/// </summary>
		/// <param name="x">x axis</param>
		/// <param name="y">y axis</param>
		void Move(int x, int y);
		/// <summary>
		/// Click event.
		/// </summary>
		void Click();
	}

	public abstract class Computer : ISound, IMouse
	{
		// If interface member from abstract class then can selected implementation where sub class.

		abstract public void VolumeControl();
		abstract public void SpeakerOut();
		abstract public void Move(int x, int y);
		abstract public void Click();
	}

	public class Desktop : Computer
	{
		/// <summary>
		/// volumn control
		/// </summary>
		public override void VolumeControl()
		{
			Console.WriteLine("Volume Control");
		}

		/// <summary>
		/// speaker out
		/// </summary>
		public override void SpeakerOut()
		{
			Console.WriteLine("Speaker Out");
		}

		/// <summary>
		/// move
		/// </summary>
		/// <param name="x">x</param>
		/// <param name="y">y</param>
		public override void Move(int x, int y)
		{
			Console.WriteLine("Move x : {0}, y : {1}", x, y);
		}

		/// <summary>
		/// click
		/// </summary>
		public override void Click()
		{
			Console.WriteLine("Click");
		}
	}

	public class SuperComputer : ISound, IMouse
	{
		private string className = typeof(SuperComputer).Name;

		#region ISound Members

		public virtual void VolumeControl()
		{
			Console.WriteLine(className + " Volume Control");
		}

		public virtual void SpeakerOut()
		{
			Console.WriteLine(className + " Speaker Out");
		}

		#endregion

		#region IMouse Members

		public virtual void Move(int x, int y)
		{
			Console.WriteLine(className + " Move x : {0}, y : {1}", x, y);			
		}

		public virtual void Click()
		{
			Console.WriteLine(className + " Click");
		}

		#endregion
	}

	public class Com1002Computer : SuperComputer
	{
		private string className = typeof(Com1002Computer).Name;

		public override void VolumeControl()
		{
			base.VolumeControl();
			Console.WriteLine(className + " Volume Control");
		}

		public override void SpeakerOut()
		{
			base.SpeakerOut();
			Console.WriteLine(className + " Speaker Out");
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Desktop myCom = new Desktop();

			Console.WriteLine("[Desktop]");
			myCom.VolumeControl();
			myCom.SpeakerOut();
			myCom.Move(1, 10);
			myCom.Click();

			Console.WriteLine("\n[Com10002 Computer]");
			Com1002Computer com = new Com1002Computer();

			com.VolumeControl();
			com.SpeakerOut();
			com.Move(100, 1000);
			com.Click();
		}
	}
}
