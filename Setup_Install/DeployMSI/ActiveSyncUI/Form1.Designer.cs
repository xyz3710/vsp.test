namespace ActiveSyncUI
{
	/// <summary>
	/// 
	/// </summary>
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			bas.DeActivateDevice();

			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnScan = new System.Windows.Forms.Button();
			this.cbxChannel = new System.Windows.Forms.ComboBox();
			this.txtMtu = new System.Windows.Forms.TextBox();
			this.lblAddr = new System.Windows.Forms.Label();
			this.lblChannel = new System.Windows.Forms.Label();
			this.lblMtu = new System.Windows.Forms.Label();
			this.cbtAuth = new System.Windows.Forms.CheckBox();
			this.cbtEncrypt = new System.Windows.Forms.CheckBox();
			this.cbxAddr = new System.Windows.Forms.ComboBox();
			this.btnActivate = new System.Windows.Forms.Button();
			this.btnDeActivate = new System.Windows.Forms.Button();
			this.btnTrust = new System.Windows.Forms.Button();
			this.cbxTrust = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// btnScan
			// 
			this.btnScan.Location = new System.Drawing.Point(3, 119);
			this.btnScan.Name = "btnScan";
			this.btnScan.Size = new System.Drawing.Size(97, 23);
			this.btnScan.TabIndex = 1;
			this.btnScan.Text = "DeviceScan";
			this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
			// 
			// cbxChannel
			// 
			this.cbxChannel.Enabled = false;
			this.cbxChannel.Items.Add("1");
			this.cbxChannel.Items.Add("2");
			this.cbxChannel.Items.Add("3");
			this.cbxChannel.Items.Add("4");
			this.cbxChannel.Items.Add("5");
			this.cbxChannel.Items.Add("6");
			this.cbxChannel.Items.Add("7");
			this.cbxChannel.Items.Add("8");
			this.cbxChannel.Items.Add("9");
			this.cbxChannel.Items.Add("10");
			this.cbxChannel.Items.Add("11");
			this.cbxChannel.Items.Add("12");
			this.cbxChannel.Items.Add("13");
			this.cbxChannel.Items.Add("14");
			this.cbxChannel.Items.Add("15");
			this.cbxChannel.Items.Add("16");
			this.cbxChannel.Items.Add("17");
			this.cbxChannel.Items.Add("18");
			this.cbxChannel.Items.Add("19");
			this.cbxChannel.Items.Add("20");
			this.cbxChannel.Items.Add("21");
			this.cbxChannel.Items.Add("22");
			this.cbxChannel.Items.Add("23");
			this.cbxChannel.Items.Add("24");
			this.cbxChannel.Items.Add("25");
			this.cbxChannel.Items.Add("26");
			this.cbxChannel.Items.Add("27");
			this.cbxChannel.Items.Add("28");
			this.cbxChannel.Items.Add("29");
			this.cbxChannel.Items.Add("30");
			this.cbxChannel.Location = new System.Drawing.Point(65, 35);
			this.cbxChannel.Name = "cbxChannel";
			this.cbxChannel.Size = new System.Drawing.Size(67, 23);
			this.cbxChannel.TabIndex = 3;
			// 
			// txtMtu
			// 
			this.txtMtu.Location = new System.Drawing.Point(65, 64);
			this.txtMtu.Name = "txtMtu";
			this.txtMtu.Size = new System.Drawing.Size(67, 23);
			this.txtMtu.TabIndex = 2;
			this.txtMtu.Text = "2048";
			// 
			// lblAddr
			// 
			this.lblAddr.Location = new System.Drawing.Point(5, 6);
			this.lblAddr.Name = "lblAddr";
			this.lblAddr.Size = new System.Drawing.Size(58, 23);
			this.lblAddr.Text = "Address";
			// 
			// lblChannel
			// 
			this.lblChannel.Location = new System.Drawing.Point(5, 35);
			this.lblChannel.Name = "lblChannel";
			this.lblChannel.Size = new System.Drawing.Size(58, 23);
			this.lblChannel.Text = "Channel";
			// 
			// lblMtu
			// 
			this.lblMtu.Location = new System.Drawing.Point(5, 64);
			this.lblMtu.Name = "lblMtu";
			this.lblMtu.Size = new System.Drawing.Size(58, 23);
			this.lblMtu.Text = "Mtu";
			// 
			// cbtAuth
			// 
			this.cbtAuth.Checked = true;
			this.cbtAuth.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbtAuth.Location = new System.Drawing.Point(5, 93);
			this.cbtAuth.Name = "cbtAuth";
			this.cbtAuth.Size = new System.Drawing.Size(100, 20);
			this.cbtAuth.TabIndex = 8;
			this.cbtAuth.Text = "Authenticate";
			// 
			// cbtEncrypt
			// 
			this.cbtEncrypt.Checked = true;
			this.cbtEncrypt.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbtEncrypt.Location = new System.Drawing.Point(108, 93);
			this.cbtEncrypt.Name = "cbtEncrypt";
			this.cbtEncrypt.Size = new System.Drawing.Size(100, 20);
			this.cbtEncrypt.TabIndex = 8;
			this.cbtEncrypt.Text = "Encrypt";
			// 
			// cbxAddr
			// 
			this.cbxAddr.Items.Add("00:0D:18:01:25:A1");
			this.cbxAddr.Location = new System.Drawing.Point(65, 6);
			this.cbxAddr.Name = "cbxAddr";
			this.cbxAddr.Size = new System.Drawing.Size(143, 23);
			this.cbxAddr.TabIndex = 3;
			this.cbxAddr.SelectedIndexChanged += new System.EventHandler(this.cbxAddr_SelectedIndexChanged);
			// 
			// btnActivate
			// 
			this.btnActivate.Enabled = false;
			this.btnActivate.Location = new System.Drawing.Point(3, 148);
			this.btnActivate.Name = "btnActivate";
			this.btnActivate.Size = new System.Drawing.Size(97, 23);
			this.btnActivate.TabIndex = 1;
			this.btnActivate.Text = "Activate";
			this.btnActivate.Click += new System.EventHandler(this.btnActivate_Click);
			// 
			// btnDeActivate
			// 
			this.btnDeActivate.Enabled = false;
			this.btnDeActivate.Location = new System.Drawing.Point(111, 148);
			this.btnDeActivate.Name = "btnDeActivate";
			this.btnDeActivate.Size = new System.Drawing.Size(97, 23);
			this.btnDeActivate.TabIndex = 1;
			this.btnDeActivate.Text = "DeActivate";
			this.btnDeActivate.Click += new System.EventHandler(this.btnDeActivate_Click);
			// 
			// btnTrust
			// 
			this.btnTrust.Location = new System.Drawing.Point(111, 119);
			this.btnTrust.Name = "btnTrust";
			this.btnTrust.Size = new System.Drawing.Size(97, 23);
			this.btnTrust.TabIndex = 1;
			this.btnTrust.Text = "Trust";
			this.btnTrust.Click += new System.EventHandler(this.btnTrust_Click);
			// 
			// cbxTrust
			// 
			this.cbxTrust.Enabled = false;
			this.cbxTrust.Location = new System.Drawing.Point(138, 67);
			this.cbxTrust.Name = "cbxTrust";
			this.cbxTrust.Size = new System.Drawing.Size(70, 20);
			this.cbxTrust.TabIndex = 8;
			this.cbxTrust.Text = "Trust";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(219, 188);
			this.Controls.Add(this.cbxTrust);
			this.Controls.Add(this.cbtEncrypt);
			this.Controls.Add(this.cbtAuth);
			this.Controls.Add(this.lblMtu);
			this.Controls.Add(this.lblChannel);
			this.Controls.Add(this.lblAddr);
			this.Controls.Add(this.cbxAddr);
			this.Controls.Add(this.cbxChannel);
			this.Controls.Add(this.txtMtu);
			this.Controls.Add(this.btnDeActivate);
			this.Controls.Add(this.btnActivate);
			this.Controls.Add(this.btnTrust);
			this.Controls.Add(this.btnScan);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.Text = "ActiveSyncUI";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnScan;
		private System.Windows.Forms.ComboBox cbxChannel;
		private System.Windows.Forms.TextBox txtMtu;
		private System.Windows.Forms.Label lblAddr;
		private System.Windows.Forms.Label lblChannel;
		private System.Windows.Forms.Label lblMtu;
		private System.Windows.Forms.CheckBox cbtAuth;
		private System.Windows.Forms.CheckBox cbtEncrypt;
		private System.Windows.Forms.ComboBox cbxAddr;
		private System.Windows.Forms.Button btnActivate;
		private System.Windows.Forms.Button btnDeActivate;
		private System.Windows.Forms.Button btnTrust;
		private System.Windows.Forms.CheckBox cbxTrust;
	}
}

