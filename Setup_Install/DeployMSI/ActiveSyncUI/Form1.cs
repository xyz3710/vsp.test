using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BluetoothLibrary;
using BluetoothLibrary.Net;
using BluetoothLibrary.Net.Bluetooth;
using BluetoothLibrary.Net.Bluetooth.Internal;
using BluetoothLibrary.Net.Sockets;

namespace ActiveSyncUI
{
	public partial class Form1 : Form
	{
		private BluetoothLibrary.Net.Bluetooth.BluetoothActiveSync bas;
		private int[] channelArr;

		/// <summary>
		/// 
		/// </summary>
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			bas = new BluetoothActiveSync();
			cbxChannel.SelectedIndex = 1;
			cbxAddr.SelectedIndex = 0;
			btnActivate.Enabled = true;			
		}

		private void btnScan_Click(object sender, EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;

			cbxAddr.Items.Clear();

			BluetoothClient bc = new BluetoothClient();
			bc.QueryLength = 4;
			
			BluetoothDeviceInfo []bdi = bc.DiscoverDevices(30);

			channelArr = new int[bdi.Length];

			for (int i = 0; i < bdi.Length; i++)
			{
				Guid guid = BluetoothService.SerialPort;
				int channel = 0;
				BluetoothAddress ba = bdi[i].DeviceID;

				bool result = NativeMethods.ServiceCheckWithGuid(ba.ToByteArray(), guid, out channel);

				channelArr[i] = channel;
				cbxAddr.Items.Add(ba.ToString());
			}

			Cursor.Current = Cursors.Default;
		}

		private void cbxAddr_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cbxAddr != null && channelArr != null)
			{
				int channelIndex = channelArr[cbxAddr.SelectedIndex] - 1;

				cbxChannel.SelectedIndex = channelIndex;

				if (channelIndex > -1)
					btnActivate.Enabled = true;
				else
					btnActivate.Enabled = false;
			}
		}

		private void btnTrust_Click(object sender, EventArgs e)
		{
			//string selectItem = cbxAddr.SelectedItem.ToString();

			//BluetoothAddress ba = BluetoothAddress.Parse(selectItem);

			//if (cbxTrust.Checked == false)
			//{
			//    bool ret = BluetoothSecurity.PairRequest(ba);

			//    if (ret)
			//    {
			//        cbxTrust.Checked = true;
			//        btnActivate.Enabled = true;
			//        btnDeActivate.Enabled = false;

			//        bas.regHLMTrusted(selectItem);
			//    }
			//}
			//else
			//{
			//    bool ret = BluetoothSecurity.RevokePin(ba);
			//    NativeMethods.BthRevokeLinkKey(ba.ToByteArray());

			//    cbxTrust.Checked = false;
			//    btnActivate.Enabled = false;
			//    btnDeActivate.Enabled = false;

			//    bas.regHLMTrustedDelete(selectItem);
			//}
		}

		private void btnActivate_Click(object sender, EventArgs e)
		{
			bas.Address = BluetoothAddress.Parse(cbxAddr.SelectedItem.ToString());
			bas.Authentication = cbtAuth.Checked;
			bas.Encryption = cbtEncrypt.Checked;
			bas.Mtu = int.Parse(txtMtu.Text);
			bas.Channel = Convert.ToInt32(cbxChannel.SelectedItem);

			if (bas.ActivateDevice() == true)
			{
				btnActivate.Enabled = false;
				btnDeActivate.Enabled = true;
			}
		}

		private void btnDeActivate_Click(object sender, EventArgs e)
		{
			if (bas.DeActivateDevice() == true)
			{
				btnActivate.Enabled = true;
				btnDeActivate.Enabled = false;
			}
		}
	}
}