using System;
using System.Collections.Generic;
using System.Text;
using OpenNETCF.Net;
using OpenNETCF.Net.Bluetooth;
using OpenNETCF.Net.Bluetooth.Internal;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.ComponentModel;

namespace ActiveSyncUI
{
	class BluetoothActiveSync : IDisposable
	{
		private string m_portPrefix;
		private int m_portIndex;
		private PORTEMUPortParams pep;
		private IntPtr	m_handle;
		private int handleService;
		private int handleCOMport;
		private int channel;

		internal BluetoothActiveSync(string portPrefix, int portIndex)
		{
			pep = new PORTEMUPortParams();

			m_portPrefix = portPrefix;
			m_portIndex = portIndex;
		}

		private void register()
		{
			m_handle = RegisterDevice(m_portPrefix, m_portIndex, "btd.dll", pep.ToByteArray());

			if(m_handle == IntPtr.Zero)
			{
				int error = Marshal.GetLastWin32Error();

				throw new Win32Exception(error, "Error creating virtual com port");
			}
			
			Handle = m_handle;
		}

		public static BluetoothActiveSync CreateServer(int portIndex)
		{
			return BluetoothActiveSync.createServer("COM", portIndex, BluetoothService.SerialPort);
		}

		private static BluetoothActiveSync createServer(string portPrefix, int portIndex, Guid service)
		{
			BluetoothActiveSync bsp = new BluetoothActiveSync(portPrefix, portIndex);

			try
			{
				// TODO: upgrade get channel
				// channel scan from 1 to 31 
				bsp.pep.Channel = 0;
				for (int i = 1; i <= 31; i++)
				{
					try
					{
						bsp.pep.Channel = i;
						bsp.pep.Flags = 0;
						bsp.pep.Mtu = 2048;

						bsp.register();

						if (bsp.Handle != IntPtr.Zero)
						{
							bsp.channel = i;

							break;
						}
					}
					catch (Exception)
					{
					}
				}

				if (bsp.Handle != IntPtr.Zero)
				{
					string preFix = string.Format("COM{0}:", portIndex);

					bsp.handleCOMport = NativeMethods.CreateFile(preFix,
						FileAccessEx.GENERIC_READ | FileAccessEx.GENERIC_WRITE,
						0, 0, CreateDisposition.OPEN_EXISTING, 0, 0);

					if (bsp.handleCOMport != 0)
					{
						bsp.handleService = BluetoothService.ServiceRegister(BluetoothService.SerialPort, bsp.channel);

						if (bsp.handleService == 0)
						{
							bsp.Close();

							throw new Exception("Could not register serial port profile");
						}
					}
					else
					{
						bsp.Close();

						throw new Exception("Could not create file to COM port");
					}
				}
				else
				{
					throw new Exception("Could not create virtual COM port");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex + "\r\n" + "System restart manually.", "Error");

				Application.Exit();
			}

			return bsp;
		}

		public static BluetoothActiveSync CreateClient(int portIndex, BluetoothAddress remoteAddress)
		{
			return BluetoothActiveSync.createClient("COM", portIndex,
				new BluetoothEndPoint(remoteAddress, BluetoothService.SerialPort));
		}

		private static BluetoothActiveSync createClient(string portPrefix, int portIndex, BluetoothEndPoint endPoint)
		{
			BluetoothActiveSync bsp = new BluetoothActiveSync(portPrefix, portIndex);

			bsp.pep.Local = false;
			bsp.pep.Address = endPoint.Address;
			bsp.pep.Service = endPoint.Service;

			bsp.register();

			return bsp;
		}

		public int HandleService
		{
			get
			{
				return handleService;
			}
			set
			{
				handleService = value;
			}
		}

		public int HandleCOMport
		{
			get
			{
				return handleCOMport;
			}
			set
			{
				handleCOMport = value;
			}
		}

		public IntPtr Handle
		{
			get
			{
				return m_handle;
			}
			set
			{
				m_handle = value;
			}
		}

		public int Channel
		{
			get
			{
				return channel;
			}
			set
			{
				channel = value;
			}
		}

		public string PortName
		{
			get
			{
				return m_portPrefix + m_portIndex.ToString() + ":";
			}
		}

		public BluetoothAddress Address
		{
			get
			{
				return pep.Address;
			}
		}

		public Guid Service
		{
			get
			{
				return pep.Service;
			}
		}

		public int PortIndex
		{
			get
			{
				return m_portIndex;
			}
		}

		public bool Local
		{
			get
			{
				return pep.Local;
			}
		}

		public void Close()
		{
			if (handleService != 0)
			{
				BluetoothService.ServiceDeregister(handleService);
				HandleService = 0;
			}

			if (handleCOMport != 0)
			{
				NativeMethods.CloseHandle(handleCOMport);
				HandleCOMport = 0;
			}

			if (m_handle != IntPtr.Zero)
			{
				bool success = DeregisterDevice(m_handle);

				if(success)
				{
					m_handle = IntPtr.Zero;
				}
				else
				{
					throw new SystemException("Error deregistering virtual COM port " + Marshal.GetLastWin32Error().ToString("X"));
				}
			}
		}

		#region Imported API DLLs.
		[DllImport("coredll.dll", SetLastError=true)]
		private static extern IntPtr RegisterDevice(
			string lpszType,
			int dwIndex,
			string lpszLib,
			byte[] dwInfo);

		[DllImport("coredll.dll", SetLastError=true)]
		private static extern bool DeregisterDevice(
			IntPtr handle);
		#endregion

		#region IDisposable Members

		protected void Dispose(bool disposing)
		{
			Close();

			if(disposing)
			{
				m_portPrefix = null;
				pep = null;
			}
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		~BluetoothActiveSync()
		{
			Dispose(false);
		}

		#endregion
	}
}