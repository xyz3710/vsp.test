﻿namespace DumpTest
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnDivideByZero = new System.Windows.Forms.Button();
			this.btnMeroryLeak = new System.Windows.Forms.Button();
			this.tbMemory = new System.Windows.Forms.TextBox();
			this.lblSize = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnDivideByZero
			// 
			this.btnDivideByZero.Location = new System.Drawing.Point(33, 24);
			this.btnDivideByZero.Name = "btnDivideByZero";
			this.btnDivideByZero.Size = new System.Drawing.Size(195, 23);
			this.btnDivideByZero.TabIndex = 0;
			this.btnDivideByZero.Text = "Divide By Zero";
			this.btnDivideByZero.UseVisualStyleBackColor = true;
			this.btnDivideByZero.Click += new System.EventHandler(this.btnDivideByZero_Click);
			// 
			// btnMeroryLeak
			// 
			this.btnMeroryLeak.Location = new System.Drawing.Point(33, 68);
			this.btnMeroryLeak.Name = "btnMeroryLeak";
			this.btnMeroryLeak.Size = new System.Drawing.Size(195, 23);
			this.btnMeroryLeak.TabIndex = 0;
			this.btnMeroryLeak.Text = "Merory Leak Test";
			this.btnMeroryLeak.UseVisualStyleBackColor = true;
			this.btnMeroryLeak.Click += new System.EventHandler(this.btnMeroryLeak_Click);
			// 
			// tbMemory
			// 
			this.tbMemory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tbMemory.Location = new System.Drawing.Point(12, 284);
			this.tbMemory.Multiline = true;
			this.tbMemory.Name = "tbMemory";
			this.tbMemory.Size = new System.Drawing.Size(855, 173);
			this.tbMemory.TabIndex = 1;
			// 
			// lblSize
			// 
			this.lblSize.AutoSize = true;
			this.lblSize.Location = new System.Drawing.Point(254, 73);
			this.lblSize.Name = "lblSize";
			this.lblSize.Size = new System.Drawing.Size(35, 12);
			this.lblSize.TabIndex = 2;
			this.lblSize.Text = "0byte";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(879, 469);
			this.Controls.Add(this.lblSize);
			this.Controls.Add(this.tbMemory);
			this.Controls.Add(this.btnMeroryLeak);
			this.Controls.Add(this.btnDivideByZero);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Dump Test";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnDivideByZero;
		private System.Windows.Forms.Button btnMeroryLeak;
		private System.Windows.Forms.TextBox tbMemory;
		private System.Windows.Forms.Label lblSize;
	}
}

