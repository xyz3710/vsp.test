﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace TestAttribute01
{
	public class Program
	{
		private int privateField = 1000;

		public int PublicField = 1000;

		public Program() : this("Test")
		{
		}

		public Program(string test)
		{

		}

		[Conditional("DEBUG")]
		public static void DebugPrint()
		{
			Console.WriteLine("Debuging...");
		}

		public static void Reflections()
		{
			Assembly a = Assembly.LoadFrom(@"C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\System.dll");
			//Assembly a = Assembly.Load("System");

			Type[] type = a.GetTypes();

			Console.WriteLine("Count of Type : {0}", type.Length);

			//foreach (Type t in type)
			//{
			//    Console.WriteLine(t.FullName);
			//}

			string s = "Jabook";
			Type t1 = s.GetType();

			Console.WriteLine("Type of string t1 : {0}", t1);


			Type t2 = Type.GetType("System.String");

			Console.WriteLine("Type of string t2 : {0}", t2);


			Type t3 = typeof(System.String);

			Console.WriteLine("Type of string t3 : {0}", t3);
		}

		private void PrivateMethod()
		{
		}
	}

	public class MainClas
	{
		static void Main(string[] args)
		{
			Program.DebugPrint();

			Program.Reflections();

			try
			{
				Type t = typeof(Program);

				Console.WriteLine("[Base Class Type]");
				Console.WriteLine(t.BaseType);


				Console.WriteLine("[Constructor list]");
				ConstructorInfo[] ctor = t.GetConstructors();

				for (int i = 0; i < ctor.Length; i++)
				{
					Console.WriteLine(ctor[i]);
				}


				Console.WriteLine("[Field list]");
				FieldInfo[] f = t.GetFields();
				for (int i = 0; i < f.Length; i++)
				{
					Console.WriteLine(f[i]);
				}


				Console.WriteLine("[Member list]");
				MemberInfo[] m = t.GetMembers();
				for (int i = 0; i < m.Length; i++)
				{
					Console.WriteLine(m[i]);
				}


				Console.WriteLine("[Method list]");
				MethodInfo[] mi = t.GetMethods();
				for (int i = 0; i < mi.Length; i++)
				{
					Console.WriteLine(mi[i]);
				}

				Console.WriteLine("[Static Method list]");
				MethodInfo[] mi2 = t.GetMethods(BindingFlags.Static);
				for (int i = 0; i < mi2.Length; i++)
				{
					Console.WriteLine(mi[i]);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
		}
	}
}
