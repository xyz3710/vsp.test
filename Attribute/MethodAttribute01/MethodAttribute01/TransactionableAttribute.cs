using System;
using System.Collections.Generic;
using System.Text;

namespace MethodAttribute01
{
	[AttributeUsage(AttributeTargets.Method)]
	public class TransactionableAttribute : Attribute
	{
		public TransactionableAttribute()
		{

		}
	}

}
