using System;
using System.Collections.Generic;
using System.Text;

namespace MethodAttribute01
{
	public class TestClass
	{
		[Transactionable]
		public void Foo()
		{
			
		}

		public void Bar()
		{
			
		}

		[Transactionable]
		public void Baz()
		{
			
		}
	}

}
