﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace MethodAttribute01
{
	class Program
	{
		static void Main(string[] args)
		{
			Type type = typeof(TestClass);

			foreach (MethodInfo mi in type.GetMethods())				
			{
				foreach (Attribute attr in mi.GetCustomAttributes(false))
				{
					if (attr is TransactionableAttribute)
						Console.WriteLine("{0} is transactionable.", mi.Name);
				}
			}
		}
	}	
}
