using System;
using System.Collections.Generic;
using System.Text;

namespace CustomAttribute
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple=true)]
	public class MyAttribute : Attribute
	{
		private string _firstName;
		private string _lastName;

		/// <summary>
		/// Initializes a new instance of the MyAttribute class.
		/// </summary>
		/// <param name="firstName"></param>
		/// <param name="lastName"></param>
		public MyAttribute(string firstName, string lastName)
		{
			_firstName = firstName;
			_lastName = lastName;
		}
	}

	public class My2Attribute : MyAttribute
	{
		private string _middleName;

		/// <summary>
		/// Initializes a new instance of the My2Attribute class.
		/// </summary>
		/// <param name="middleName"></param>
		public My2Attribute()
			: base("defaultFirstName", "defaultLastName")
		{
			
		}

		public string MiddleName
		{
			get
			{
				return _middleName;
			}
			set
			{
				_middleName = value;
			}
		}
		
	}
}
