﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;

namespace System
{
	/// <summary>
	/// Enum type converter 
	/// </summary>
	public class EnumTypeConverter : EnumConverter
	{
		private System.Type _enumType;

		/// <summary>
		/// Initializes a new instance of the <see cref="EnumTypeConverter"/> class.
		/// </summary>
		/// <param name="type">A <see cref="T:System.Type" /> that represents the type of enumeration to associate with this enumeration converter.</param>
		public EnumTypeConverter(System.Type type) : base(type)
		{
			_enumType = type;
		}

		/// <summary>
		/// Determines whether this instance [can convert to] the specified context.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="destType">Type of the dest.</param>
		/// <returns><c>true</c> if this instance [can convert to] the specified context; otherwise, <c>false</c>.</returns>
		public override bool CanConvertTo(ITypeDescriptorContext context, System.Type destType)
		{
			return destType == typeof(string);
		}

		/// <summary>
		/// Converts to.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="value">The value.</param>
		/// <param name="destType">Type of the dest.</param>
		/// <returns>System.Object.</returns>
		public override object ConvertTo(
			ITypeDescriptorContext context,
			CultureInfo culture,
			object value,
			System.Type destType)
		{
			FieldInfo fi = _enumType.GetField(Enum.GetName(_enumType, value));
			DescriptionAttribute dna = (DescriptionAttribute)Attribute.GetCustomAttribute(fi, typeof(DescriptionAttribute));

			if (dna != null)
			{
				return dna.Description;
			}
			else
			{
				return value.ToString();
			}
		}

		/// <summary>
		/// Gets a value indicating whether this converter can convert an object in the given source type to an enumeration object using the specified context.
		/// </summary>
		/// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
		/// <param name="sourceType">A <see cref="T:System.Type" /> that represents the type you wish to convert from.</param>
		/// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
		public override bool CanConvertFrom(ITypeDescriptorContext context, System.Type sourceType)
		{
			return sourceType == typeof(string);
		}

		/// <summary>
		/// Converts the specified value object to an enumeration object.
		/// </summary>
		/// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
		/// <param name="culture">An optional <see cref="T:System.Globalization.CultureInfo" />. If not supplied, the current culture is assumed.</param>
		/// <param name="value">The <see cref="T:System.Object" /> to convert.</param>
		/// <returns>An <see cref="T:System.Object" /> that represents the converted <paramref name="value" />.</returns>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			foreach (FieldInfo fi in _enumType.GetFields())
			{
				DescriptionAttribute dna = (DescriptionAttribute)Attribute.GetCustomAttribute(fi, typeof(DescriptionAttribute));

				if ((dna != null) && ((string)value == dna.Description))
				{
					return Enum.Parse(_enumType, fi.Name);
				}
			}

			return Enum.Parse(_enumType, (string)value);
		}
	}
}
