﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumDescription
{
	[TypeConverterAttribute(typeof(EnumTypeConverter))]
	public enum TestType
	{
		[Description("test1 하나 둘 셋 ★")]
		Test1,
		[Description("test2 하나 둘 셋 ※")]
		Test2,
		[Description("test3 하나 둘 셋 ☆")]
		Test3,
		[Description("test4 하나 둘 셋 ◎")]
		Test4,
		Test5,
	}
}
