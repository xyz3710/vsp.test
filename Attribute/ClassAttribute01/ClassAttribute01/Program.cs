﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassAttribute01
{
	class Program
	{
		static void Main(string[] args)
		{
			Type type = typeof(MyRemotableClass);

			foreach (Attribute attr in type.GetCustomAttributes(false))
			{
				RemoteObjectAttribute remoteAttr = attr as RemoteObjectAttribute;

				if (remoteAttr != null)
				{
					Console.WriteLine("Create this object on {0}.", remoteAttr.Server);
				}
			}
		}
	}
}