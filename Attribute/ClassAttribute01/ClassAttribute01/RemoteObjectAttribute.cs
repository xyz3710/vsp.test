using System;
using System.Collections.Generic;
using System.Text;

namespace ClassAttribute01
{
	[AttributeUsage(AttributeTargets.Class)]
	public class RemoteObjectAttribute : Attribute
	{
		protected RemoteServers _server;

		/// <summary>
		/// Initializes a new instance of the RemoteObjectAttribute class.
		/// </summary>
		/// <param name="server"></param>
		public RemoteObjectAttribute(RemoteServers server)
		{
			_server = server;
		}

		public string Server
		{
			get
			{
				return _server.ToString();
			}		
		}        
	}

}
