using System;
using System.Collections.Generic;
using System.Text;

namespace FieldAttribute01
{
	public class TestClass
	{
		[RegistryKey(RegistryHives.HKEY_CURRENT_USER, "2개의 지정위치 파라미터")]
		public int Foo;

		[RegistryKey("지정위치 파라미터", Hive = RegistryHives.HKEY_LOCAL_MACHINE)]
		public int Bar;
	}

}
