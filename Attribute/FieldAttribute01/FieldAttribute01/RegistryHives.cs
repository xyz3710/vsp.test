using System;
using System.Collections.Generic;
using System.Text;

namespace FieldAttribute01
{
	public enum RegistryHives
	{
		None=0,
		HKEY_CLASSES_ROOT,
		HKEY_CURRENT_USER,
		HKEY_LOCAL_MACHINE,
		HKEY_USERS,
		HKEY_CURRENT_CONFIG,
	}
}
