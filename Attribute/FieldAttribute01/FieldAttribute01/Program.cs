﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace FieldAttribute01
{
	class Program
	{
		static void Main(string[] args)
		{
			Type type = typeof(TestClass);

			foreach (FieldInfo field in type.GetFields())
			{
				foreach (Attribute attr in field.GetCustomAttributes(false))
				{
					RegistryKeyAttribute registryKeyAttr = attr as RegistryKeyAttribute;

					if (registryKeyAttr != null)
					{
						Console.WriteLine("{0} will be saved in {1}\\\\{2}",
							field.Name,
							registryKeyAttr.Hive,
							registryKeyAttr.ValueName);
					}
				}
			}
		}
	}
}