using System;
using System.Collections.Generic;
using System.Text;

namespace FieldAttribute01
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public class RegistryKeyAttribute : Attribute
	{
		private RegistryHives _hive;
		private string _valueName;

		/// <summary>
		/// Initializes a new instance of the RegistryKeyAttribute class.
		/// </summary>
		/// <param name="hive"></param>
		/// <param name="valueName"></param>
		public RegistryKeyAttribute(RegistryHives hive, string valueName)
		{
			_hive = hive;
			_valueName = valueName;
		}
		
		/// <summary>
        /// Initializes a new instance of the RegistryKeyAttribute class.
        /// </summary>
        /// <param name="valueName"></param>
        public RegistryKeyAttribute(string valueName)
        {
			_valueName = valueName;
        }

		#region Properties
		/// <summary>
		/// Hive을(를) 구하거나 설정합니다.
		/// </summary>
		public RegistryHives Hive
		{
			get
			{
				return _hive;
			}
			set
			{
				_hive = value;
			}
		}

		/// <summary>
		/// ValueName을(를) 구하거나 설정합니다.
		/// </summary>
		public String ValueName
		{
			get
			{
				return _valueName;
			}
			set
			{
				_valueName = value;
			}
		}

		#endregion
        
	}

}
