﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TelerikDataAccess
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		protected override void OnShown(EventArgs e)
		{
			using (var db = new TelerikDataAccessContext())
			{
				System.Diagnostics.Debug.WriteLine(db.Products.Count(), "Product count: ");
			}

			base.OnShown(e);
		}
	}
}
