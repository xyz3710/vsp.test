﻿// ****************************************************************************************************************** //
//	Domain		:	CE_35_Net35_Test.Form1
//	Creator		:	GS_DEV\xyz37(Kim Ki Won)
//	Create		:	2016년 3월 17일 목요일 오전 12:57
//	Purpose		:	Sql CE 3.5 SP2 설치하지 않고 배포 하는 방법
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	http://www.codeproject.com/Articles/33661/Creating-a-Private-Installation-for-SQL-Compact
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="Form1.cs" company="(주)가치소프트">
//		Copyright (c) 2016. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Threading;

namespace CE_35_Net35_Test
{
	public partial class Form1 : Form
	{
		private string _connectionString = @"Data Source=DZone.Local.sdf;Max Database Size=1024;Password=theProgrammer100";

		private Thread _testThread;
		private bool _running;

		public Form1()
		{
			InitializeComponent();

			_connectionString = string.Format(@"Data Source={0}\Data\DZone.Local.sdf;Max Database Size=1024;Password=theProgrammer100", Application.StartupPath);
			SqlCeHelper.SetConnectionString(_connectionString);

			_testThread = new Thread(() =>
			{
				while (_running == true)
				{
					Invoke(new MethodInvoker(() =>
					{
						tbCustomer.AppendText(SqlCeHelper.Database.TableExist("Customers") == true ? "있음\r\n" : "없음\r\n");
						tbCustomer.ScrollToCaret();

						if (tbCustomer.Lines.Length > 8)
						{
							tbCustomer.ResetText();
						}
					}));
					Thread.Sleep(100);
				}
			});
		}

		protected override void OnShown(EventArgs e)
		{
			_running = true;
			_testThread.Start();

			base.OnShown(e);
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			_running = false;
			base.OnClosing(e);
		}

		private void btnCreate_Click(object sender, EventArgs e)
		{
			if (SqlCeHelper.CreateDatabaseFile(_connectionString, true) == true)
			{
				SqlCeHelper.SetConnectionString(_connectionString);

				lbStatus.Text = "데이타베이스가 생성되었습니다.";
			}
			else
			{
				lbStatus.Text = "데이타베이스 생성에 실패하였습니다.";
			}

			btnGetAllTables.PerformClick();
		}

		private void btnGetAllTables_Click(object sender, EventArgs e)
		{
			var list = SqlCeHelper.Database.GetAllTableList();

			lbTables.Items.Clear();
			lbTables.Items.AddRange(list.ToArray());
		}

		private void btnCreateTable_Click(object sender, EventArgs e)
		{
			var tableScript = GetTableScript("Customers", "AddressPresets", "LocalDbTxIds");

			if (SqlCeHelper.Database.CreateTable(tableScript) == true)
			{
				lbStatus.Text = "테이블이 생성되었습니다.";
			}
			else
			{
				lbStatus.Text = "테이블 생성에 실패 했습니다.";
			}

			btnGetAllTables.PerformClick();
		}

		private string GetTableScript(params string[] tableNames)
		{
			var list = SqlCeHelper.Database.GetAllTableList();
			var result = new StringBuilder();

			foreach (string tableName in tableNames)
			{
				if (list.Contains(tableName) == true)
				{
					result.AppendFormat("DROP TABLE [{0}];\r\nGO\r\n", tableName);
				}

				switch (tableName)
				{
					case "Customers":
						result.AppendLine(GetCreateCustomerTableScript());

						break;
					case "AddressPresets":
						result.AppendLine(GetCreateAddressPresetTableScript());

						break;
					case "LocalDbTxIds":
						result.AppendLine(GetCreateLocalDbTxIdTableScript());

						break;
				}
			}

			return result.ToString();
		}

		private string GetCreateCustomerTableScript()
		{
			// char => nchar
			var createScript = @"
CREATE TABLE [Customers](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Phone] [nvarchar](100) NULL,
	[Address] [nvarchar](200) NULL,
	[FriendlyAddress] [nvarchar](200) NULL,
	[Region] [nvarchar](40) NULL,
	[City] [nvarchar](40) NULL,
	[StdDong] [nvarchar](100) NULL,
	[HnoMajor] [int] NULL,
	[HnoMinor] [int] NULL,
	[RoadCode] [nchar](12) NULL,
	[RoadName] [nvarchar](80) NULL,
	[BuildingMajor] [int] NULL,
	[BuildingMinor] [int] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Memo] [nvarchar](4000) NULL,
	[Phone2] [nvarchar](100) NULL,
	[Phone3] [nvarchar](100) NULL,
	[Taste] [nvarchar](400) NULL,
	[LocalId] [int] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL
);
GO
CREATE INDEX [CustomersIdxId] ON [Customers] ([Id] ASC);
GO
CREATE INDEX [CustomersIdxPhone] ON [Customers] ([Phone] ASC);
GO
CREATE INDEX [CustomersIdxFriendlyAddress] ON [Customers] ([FriendlyAddress] ASC);
GO
";

			return createScript;
		}

		private string GetCreateAddressPresetTableScript()
		{
			// char => nchar
			var createScript = @"
CREATE TABLE [AddressPresets](
	[CallCenterId] [int] NOT NULL,
	[LocalId] [int] NOT NULL,
	[Id] [int] NOT NULL,
	[InLocal] [bit] NOT NULL,
	[ParentId] [int] NOT NULL,
	[OrderNo] [int] NOT NULL,
	[Tag] [nvarchar](100) NULL,
	[Name] [nvarchar](400) NOT NULL,
	[Region] [nvarchar](40) NULL,
	[City] [nvarchar](40) NULL,
	[StdDong] [nvarchar](40) NULL,
	[HnoMajor] [int] NULL,
	[HnoMinor] [int] NULL,
	[RoadCode] [nchar](12) NULL,
	[RoadName] [nvarchar](80) NULL,
	[BuildingName] [nvarchar](200) NULL,
	[BuildingDetail] [nvarchar](100) NULL,
	[BuildingMajor] [int] NULL,
	[BuildingMinor] [int] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[UserInput] [bit] NULL,
	[Deleted] [bit] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	PRIMARY KEY(
		[CallCenterId] ,
		[LocalId] ,
		[Id]
	)
)
GO
";

			return createScript;
		}

		private string GetCreateLocalDbTxIdTableScript()
		{
			var createScript = @"
CREATE TABLE [LocalDbTxIds](
	[TableName] [nvarchar](100) NOT NULL PRIMARY KEY ,
	[TxId] [bigint] NOT NULL
)
GO
";

			return createScript;
		}
	}
}
