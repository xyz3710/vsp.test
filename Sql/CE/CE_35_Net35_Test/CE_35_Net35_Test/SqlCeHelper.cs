﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace System.Data.SqlServerCe
{
	/// <summary>
	/// SqlCe 명령을 간단히 처리하는 Helper 클래스 입니다.
	/// </summary>
	/// <remarks>Thead Safty 합니다.</remarks>
	public class SqlCeHelper
	{
		private static string _connectionString;

		#region Use Singleton Pattern
		private static SqlCeHelper _newInstance;
		private object _lockObject;

		/// <summary>
		/// SqlCeHelper 클래스의 Single Ton Pattern을 위한 생성자 입니다.
		/// </summary>
		/// <param name="connectionString">The connection string.</param>
		private SqlCeHelper(string connectionString)
		{
			_lockObject = new object();
			_newInstance = null;
			_connectionString = connectionString;
		}

		/// <summary>
		/// SqlCeHelper 클래스의 Single Ton Pattern을 위한 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <returns>유일한 SqlCeHelper instance</returns>
		public static SqlCeHelper SetConnectionString(string connectionString)
		{
			if (_newInstance == null)
			{
				_newInstance = new SqlCeHelper(connectionString);
			}

			return _newInstance;
		}

		/// <summary>
		/// SqlCeHelper 클래스의 Single Ton Pattern을 위한 새 인스턴스를 구합니다.
		/// </summary>
		/// <returns>유일한 SqlCeHelper instance</returns>
		public static SqlCeHelper Database
		{
			get
			{
				if (_newInstance == null)
				{
					throw new InvalidOperationException("You must executed SetConnectionString method.");
				}

				return _newInstance;
			}
		}
		#endregion

		/// <summary>
		/// Creates the data base.
		/// </summary>
		/// <param name="connectionString">The connection string.</param>
		/// <param name="deleteIfExist">if set to <c>true</c> [delete if exist].</param>
		/// <returns></returns>
		/// <exception cref="ArgumentException">connectionString에서 Data Source를 구할 수 없습니다.</exception>
		public static bool CreateDatabaseFile(string connectionString, bool deleteIfExist = true)
		{
			var filePath = GetDataFilePath(connectionString);

			if (filePath == string.Empty)
			{
				throw new ArgumentException("connectionString에서 Data Source를 구할 수 없습니다.");
			}

			var path = Path.GetDirectoryName(filePath);

			if (Directory.Exists(path) == false)
			{
				Directory.CreateDirectory(path);
			}

			if (deleteIfExist == true)
			{
				if (File.Exists(filePath) == true)
				{
					try
					{
						File.Delete(filePath);
					}
					catch
					{
					}
				}
			}

			using (SqlCeEngine engine = new SqlCeEngine(connectionString))
			{
				engine.CreateDatabase();
			}

			return File.Exists(filePath);
		}

		private static string GetDataFilePath(string connectionString)
		{
			var regex = new Regex(@"Data Source=(?<DataFile>.*?);", RegexOptions.IgnoreCase);

			if (connectionString.Last() != ';')
			{
				connectionString = string.Format("{0};", connectionString);
			}

			return regex.Match(connectionString).Groups["DataFile"].Value;
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="sqlCommand">The SQL command.</param>
		/// <param name="options">The options.</param>
		/// <param name="resultSet">The result set.</param>
		/// <param name="sqlCeConnection">The SQL ce connection.</param>
		/// <returns></returns>
		public SqlCeResultSet ExecuteResultSet(
			SqlCeCommand sqlCommand,
			ResultSetOptions options = ResultSetOptions.Scrollable,
			SqlCeResultSet resultSet = null,
			SqlCeConnection sqlCeConnection = null)
		{
			if (sqlCommand == null)
			{
				throw new ArgumentNullException("sqlCommand");
			}

			SqlCeResultSet result = null;

			lock (_lockObject)
			{
				if (sqlCeConnection == null)
				{
					using (var connection = new SqlCeConnection(_connectionString))
					{
						connection.Open();
						sqlCommand.Connection = connection;

						if (resultSet == null)
						{
							result = sqlCommand.ExecuteResultSet(options);
						}
						else
						{
							result = sqlCommand.ExecuteResultSet(options, resultSet);
						}
					}
				}
				else
				{
					sqlCommand.Connection = sqlCeConnection;

					if (sqlCeConnection.State == ConnectionState.Closed)
					{
						sqlCeConnection.Open();
					}

					if (resultSet == null)
					{
						result = sqlCommand.ExecuteResultSet(options);
					}
					else
					{
						result = sqlCommand.ExecuteResultSet(options, resultSet);
					}
				}
			}

			return result;
		}

		/// <summary>
		/// Gets the data table.
		/// </summary>
		/// <param name="sqlCommand">The SQL command.</param>
		/// <param name="tableName">Name of the table.</param>
		/// <param name="sqlCeConnection">The SQL ce connection.</param>
		/// <returns></returns>
		/// <exception cref="ArgumentNullException">sqlCommand</exception>
		public DataTable GetDataTable(
			SqlCeCommand sqlCommand,
			string tableName = "",
			SqlCeConnection sqlCeConnection = null)
		{
			if (sqlCommand == null)
			{
				throw new ArgumentNullException("sqlCommand");
			}

			var result = new DataTable(tableName);
			var options = ResultSetOptions.Scrollable;

			lock (_lockObject)
			{
				if (sqlCeConnection == null)
				{
					using (var connection = new SqlCeConnection(_connectionString))
					{
						connection.Open();
						sqlCommand.Connection = connection;

						var rs = sqlCommand.ExecuteResultSet(options);

						result = ToDataTable(rs, tableName);
					}
				}
				else
				{
					sqlCommand.Connection = sqlCeConnection;

					if (sqlCeConnection.State == ConnectionState.Closed)
					{
						sqlCeConnection.Open();
					}

					var rs = sqlCommand.ExecuteResultSet(options);

					result = ToDataTable(rs, tableName);
				}
			}

			return result;
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="sqlCommand">The SQL command.</param>
		/// <param name="sqlCeConnection">The SQL ce connection.</param>
		/// <returns></returns>
		public SqlCeDataReader ExecuteReader(SqlCeCommand sqlCommand, SqlCeConnection sqlCeConnection = null)
		{
			if (sqlCommand == null)
			{
				throw new ArgumentNullException("sqlCommand");
			}

			SqlCeDataReader result = null;

			lock (_lockObject)
			{
				if (sqlCeConnection == null)
				{
					using (var connection = new SqlCeConnection(_connectionString))
					{
						sqlCommand.Connection = connection;
						connection.Open();

						result = sqlCommand.ExecuteReader();
					}
				}
				else
				{
					sqlCommand.Connection = sqlCeConnection;

					if (sqlCeConnection.State == ConnectionState.Closed)
					{
						sqlCeConnection.Open();
					}

					result = sqlCommand.ExecuteReader();
				}
			}

			return result;
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="sqlCommand">The SQL command.</param>
		/// <param name="sqlCeConnection">The SQL ce connection.</param>
		/// <returns></returns>
		public object ExecuteScalar(SqlCeCommand sqlCommand, SqlCeConnection sqlCeConnection = null)
		{
			if (sqlCommand == null)
			{
				throw new ArgumentNullException("sqlCommand");
			}

			object result = null;

			lock (_lockObject)
			{
				if (sqlCeConnection == null)
				{
					using (var connection = new SqlCeConnection(_connectionString))
					{
						sqlCommand.Connection = connection;
						connection.Open();

						result = sqlCommand.ExecuteScalar();
					}
				}
				else
				{
					sqlCommand.Connection = sqlCeConnection;

					if (sqlCeConnection.State == ConnectionState.Closed)
					{
						sqlCeConnection.Open();
					}

					result = sqlCommand.ExecuteScalar();
				}
			}

			return result;
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="sqlCommand">The SQL command.</param>
		/// <returns></returns>
		public int ExecuteNonQuery(SqlCeCommand sqlCommand, SqlCeConnection sqlCeConnection = null)
		{
			if (sqlCommand == null)
			{
				throw new ArgumentNullException("sqlCommand");
			}

			int result = 0;

			lock (_lockObject)
			{
				if (sqlCeConnection == null)
				{
					using (var connection = new SqlCeConnection(_connectionString))
					{
						sqlCommand.Connection = connection;
						connection.Open();

						result = sqlCommand.ExecuteNonQuery();
					}
				}
				else
				{
					sqlCommand.Connection = sqlCeConnection;

					if (sqlCeConnection.State == ConnectionState.Closed)
					{
						sqlCeConnection.Open();
					}

					result = sqlCommand.ExecuteNonQuery();
				}
			}

			return result;
		}

		/// <summary>
		/// Executes the result set.
		/// </summary>
		/// <param name="commandText">The command text.</param>
		/// <param name="options">The options.</param>
		/// <param name="resultSet">The result set.</param>
		/// <returns></returns>
		public SqlCeResultSet GetResultSet(
			string commandText,
			ResultSetOptions options = ResultSetOptions.Scrollable,
			SqlCeResultSet resultSet = null)
		{
			using (var cmd = new SqlCeCommand(commandText))
			{
				return ExecuteResultSet(cmd, options, resultSet);
			}
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public SqlCeDataReader GetReader(string commandText)
		{
			using (var cmd = new SqlCeCommand(commandText))
			{
				return ExecuteReader(cmd);
			}
		}

		/// <summary>
		/// Gets the scalar.
		/// </summary>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public object GetScalar(string commandText)
		{
			using (var cmd = new SqlCeCommand(commandText))
			{
				return ExecuteScalar(cmd);
			}
		}

		/// <summary>
		/// Check Tables the exist.
		/// </summary>
		/// <param name="tableName">Name of the table.</param>
		/// <param name="sqlCeConnection">The SQL ce connection.</param>
		/// <returns></returns>
		public bool TableExist(string tableName, SqlCeConnection sqlCeConnection = null)
		{
			var commandText = string.Format("SELECT COUNT(TABLE_NAME) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{0}'", tableName);
			object result = false;

			using (var cmd = new SqlCeCommand(commandText))
			{
				result = ExecuteScalar(cmd, sqlCeConnection);
			}

			return Convert.ToBoolean(result);
		}

		/// <summary>
		/// Gets all table list.
		/// </summary>
		/// <param name="sqlCeConnection">The SQL ce connection.</param>
		/// <returns></returns>
		public List<string> GetAllTableList(SqlCeConnection sqlCeConnection = null)
		{
			var commandText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES";
			var result = new List<string>();

			using (var cmd = new SqlCeCommand(commandText))
			{
				var rs = GetDataTable(cmd, "TableNames", sqlCeConnection);

				result = rs.Rows.Cast<DataRow>().Select(x => Convert.ToString(x[0])).ToList();
			}

			return result;
		}

		/// <summary>
		/// To the data table.
		/// </summary>
		/// <param name="resultSet">The result set.</param>
		/// <param name="tableName">Name of the table.</param>
		/// <returns></returns>
		public DataTable ToDataTable(SqlCeResultSet resultSet, string tableName = "")
		{
			var dataTable = new DataTable(tableName);

			for (int col = 0; col < resultSet.FieldCount; col++)
			{
				dataTable.Columns.Add(resultSet.GetName(col), resultSet.GetFieldType(col));
			}

			while (resultSet.Read() == true)
			{
				var row = dataTable.NewRow();

				for (int col = 0; col < resultSet.FieldCount; col++)
				{
					int ordinal = resultSet.GetOrdinal(resultSet.GetName(col));
					row[col] = resultSet.GetValue(ordinal);
				}

				dataTable.Rows.Add(row);
			}

			return dataTable;
		}

		/// <summary>
		/// Creates the table.
		/// </summary>
		/// <remarks>dropTable을 true로 지정하는 경우는 createScript에서 1개의 테이블만 생성할 때 사용한다.</remarks>
		/// <param name="createScript">The create script.</param>
		/// <param name="dropTable">생성하려는 테이블이 여러개 일 경우에는 false로 지정 후, createScript에 DROP Table 문을 삽입한다.</param>
		/// <returns></returns>
		/// <exception cref="ArgumentException">createScript에서 tableName을 찾을 수 없습니다.</exception>
		public bool CreateTable(string createScript, bool dropTable = false)
		{
			var tableName = GetTableName(createScript);

			if (tableName == string.Empty)
			{
				throw new ArgumentException("createScript에서 tableName을 찾을 수 없습니다.");
			}

			int goCount = -1;
			int result = 0;

			lock (_lockObject)
			{
				using (var connection = new SqlCeConnection(_connectionString))
				{
					connection.Open();

					if (dropTable == true)
					{
						var exist = TableExist(tableName, connection);

						if (exist == true)
						{
							createScript = string.Format("DROP TABLE [{0}];\r\nGO\r\n{1}", tableName, createScript);
						}
					}

					goCount = GetGoCountFromScript(createScript);
					result = ExecuteBatchNonQuery(connection, createScript);
				}
			}

			return goCount == Math.Abs(result);
		}

		private string GetTableName(string createScript)
		{
			var regex = new Regex(@"CREATE TABLE \[?(?<TableName>\w*)\]?", RegexOptions.Compiled | RegexOptions.IgnoreCase);

			return regex.Match(createScript).Groups["TableName"].Value;
		}

		private int GetGoCountFromScript(string script)
		{
			var regex = new Regex(@"^GO", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);

			return regex.Matches(script).Count;
		}

		/// <summary>
		/// 여러개의 Sql NoneQuery를 실행 한다.
		/// </summary>
		/// <remarks>실행 전 sqlCeConnection을 Open해야 하고, 종료 후 Close 시켜야 한다.</remarks>
		/// <param name="sqlCeConnection"></param>
		/// <param name="sqlCommands">각 SQL 구분은 GO로 구별, 마지막 구분에도 GO를 추가해야 마지막 구문이 실행 된다.</param>
		public int ExecuteBatchNonQuery(SqlCeConnection sqlCeConnection, string sqlCommands)
		{
			var affectedRow = 0;
			var sqlBatch = string.Empty;
			var cmd = new SqlCeCommand(string.Empty, sqlCeConnection);

			try
			{
				//sqlCommands += "\nGO";   // make sure last batch is executed.

				foreach (string line in sqlCommands.Split(new string[2] { "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries))
				{
					if (line.ToUpperInvariant().Trim() == "GO")
					{
						cmd.CommandText = sqlBatch;
						affectedRow += cmd.ExecuteNonQuery();
						sqlBatch = string.Empty;
					}
					else
					{
						sqlBatch += line + "\n";
					}
				}
			}
			finally
			{
				cmd.Dispose();
			}

			return affectedRow;
		}

		/// <summary>
		/// Copies the data table to table.
		/// </summary>
		/// <param name="dataTable">The data table.</param>
		/// <param name="tableName">Name of the table.</param>
		/// <param name="deleteTable">if set to <c>true</c> [delete table].</param>
		/// <returns></returns>
		/// <exception cref="InvalidOperationException">dataTable의 TableName이나 tableName 파라미터를 지정해야 합니다.</exception>
		public bool CopyDataTableToTable(
			DataTable dataTable,
			string tableName = "",
			bool deleteTable = false)
		{
			var result = false;
			var sqlCeConnection = new SqlCeConnection(_connectionString);

			if (sqlCeConnection.State == ConnectionState.Closed)
			{
				sqlCeConnection.Open();
			}

			if (tableName == string.Empty)
			{
				tableName = dataTable.TableName;
			}

			if (tableName == string.Empty)
			{
				throw new InvalidOperationException("dataTable의 TableName이나 tableName 파라미터를 지정해야 합니다.");
			}

			SqlCeTransaction transaction = sqlCeConnection.BeginTransaction();
			SqlCeCommand sqlCeCommand = sqlCeConnection.CreateCommand();
			SqlCeResultSet rs = null;

			try
			{
				if (deleteTable == true)
				{
					sqlCeCommand.Transaction = transaction;
					sqlCeCommand.CommandText = "DELETE " + tableName;
					sqlCeCommand.ExecuteNonQuery();
				}

				sqlCeCommand.CommandType = System.Data.CommandType.TableDirect;
				sqlCeCommand.CommandText = tableName;

				rs = sqlCeCommand.ExecuteResultSet(ResultSetOptions.Updatable);

				for (int rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
				{
					SqlCeUpdatableRecord rec = rs.CreateRecord();
					DataRow row = dataTable.Rows[rowIndex];

					for (int column = 0; column < dataTable.Columns.Count - 1; column++)
					{
						rec.SetValue(column + 1, row[column]);
					}

					rs.Insert(rec);
				}

				transaction.Commit();
			}
			catch
			{
				transaction.Rollback();
				throw;
			}
			finally
			{
				rs.Close();

				if (sqlCeConnection.State == ConnectionState.Open)
				{
					sqlCeConnection.Close();
				}

				result = true;
			}

			return result;
		}
	}
}
