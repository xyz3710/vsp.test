﻿namespace CE_35_Net35_Test
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCreateTable = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.status = new System.Windows.Forms.ToolStripStatusLabel();
			this.lbStatus = new System.Windows.Forms.Label();
			this.btnGetAllTables = new System.Windows.Forms.Button();
			this.lbTables = new System.Windows.Forms.ListBox();
			this.btnCreate = new System.Windows.Forms.Button();
			this.tbCustomer = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnCreateTable
			// 
			this.btnCreateTable.Location = new System.Drawing.Point(160, 22);
			this.btnCreateTable.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnCreateTable.Name = "btnCreateTable";
			this.btnCreateTable.Size = new System.Drawing.Size(128, 38);
			this.btnCreateTable.TabIndex = 0;
			this.btnCreateTable.Text = "&Create Tables";
			this.btnCreateTable.UseVisualStyleBackColor = true;
			this.btnCreateTable.Click += new System.EventHandler(this.btnCreateTable_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.status});
			this.statusStrip1.Location = new System.Drawing.Point(0, 591);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 18, 0);
			this.statusStrip1.Size = new System.Drawing.Size(1305, 22);
			this.statusStrip1.TabIndex = 1;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// status
			// 
			this.status.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.status.Name = "status";
			this.status.Size = new System.Drawing.Size(1286, 17);
			this.status.Spring = true;
			// 
			// lbStatus
			// 
			this.lbStatus.AutoSize = true;
			this.lbStatus.Location = new System.Drawing.Point(12, 74);
			this.lbStatus.Name = "lbStatus";
			this.lbStatus.Size = new System.Drawing.Size(89, 20);
			this.lbStatus.TabIndex = 2;
			this.lbStatus.Text = "상태 메세지";
			// 
			// btnGetAllTables
			// 
			this.btnGetAllTables.Location = new System.Drawing.Point(296, 22);
			this.btnGetAllTables.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnGetAllTables.Name = "btnGetAllTables";
			this.btnGetAllTables.Size = new System.Drawing.Size(142, 38);
			this.btnGetAllTables.TabIndex = 0;
			this.btnGetAllTables.Text = "&Get All Tables";
			this.btnGetAllTables.UseVisualStyleBackColor = true;
			this.btnGetAllTables.Click += new System.EventHandler(this.btnGetAllTables_Click);
			// 
			// lbTables
			// 
			this.lbTables.FormattingEnabled = true;
			this.lbTables.ItemHeight = 20;
			this.lbTables.Location = new System.Drawing.Point(264, 135);
			this.lbTables.Name = "lbTables";
			this.lbTables.Size = new System.Drawing.Size(186, 184);
			this.lbTables.TabIndex = 3;
			// 
			// btnCreate
			// 
			this.btnCreate.Location = new System.Drawing.Point(13, 22);
			this.btnCreate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnCreate.Name = "btnCreate";
			this.btnCreate.Size = new System.Drawing.Size(139, 38);
			this.btnCreate.TabIndex = 0;
			this.btnCreate.Text = "Create &Database";
			this.btnCreate.UseVisualStyleBackColor = true;
			this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
			// 
			// tbCustomer
			// 
			this.tbCustomer.Location = new System.Drawing.Point(16, 135);
			this.tbCustomer.Multiline = true;
			this.tbCustomer.Name = "tbCustomer";
			this.tbCustomer.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbCustomer.Size = new System.Drawing.Size(218, 184);
			this.tbCustomer.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(21, 112);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(159, 20);
			this.label1.TabIndex = 2;
			this.label1.Text = "Customer 테이블 여부";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1305, 613);
			this.Controls.Add(this.tbCustomer);
			this.Controls.Add(this.lbTables);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lbStatus);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.btnGetAllTables);
			this.Controls.Add(this.btnCreate);
			this.Controls.Add(this.btnCreateTable);
			this.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnCreateTable;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel status;
		private System.Windows.Forms.Label lbStatus;
		private System.Windows.Forms.Button btnGetAllTables;
		private System.Windows.Forms.ListBox lbTables;
		private System.Windows.Forms.Button btnCreate;
		private System.Windows.Forms.TextBox tbCustomer;
		private System.Windows.Forms.Label label1;
	}
}

