﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CE_35_Net35_Test2
{
	public partial class Form1 : Form
	{
		private SqlCeConnection _conn;

		public Form1()
		{
			InitializeComponent();

			_conn = new SqlCeConnection(@"Data Source = |DataDirectory|\Northwind.sdf");
			this.dataGridView1.AutoGenerateColumns = true;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			try
			{
				SqlCeCommand cmd = new SqlCeCommand();
				cmd.Connection = _conn;
				cmd.CommandText = "SELECT [Employee ID], [Last Name], [First Name], Photo FROM Employees";
				//SqlCeEngine sqlCeEngine = new SqlCeEngine(_conn.ConnectionString);

				//sqlCeEngine.Upgrade();
				_conn.Open();
				SqlCeResultSet resultSet = cmd.ExecuteResultSet(ResultSetOptions.Scrollable | ResultSetOptions.Updatable);
				this.bindingSource1.DataSource = resultSet;
			}
			catch (Exception ex)
			{
				MessageBox.Show(string.Format("{0}\r\n{1}", ex.Message, ex.StackTrace.ToString()));
			}
		}
	}
}
