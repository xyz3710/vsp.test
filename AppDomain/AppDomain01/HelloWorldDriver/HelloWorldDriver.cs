﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace AppDomain01
{
	class HelloWorldDriver
	{
		static void Main(string[] args)
		{
			string baseDir = @"D:\VSP\_Test\AppDomain01\";
			string helloWorldPath = string.Format(@"{0}{1}", baseDir, "HelloWorld.exe");

			Console.WriteLine("Before Execute Assembly");

			AppDomainSetup info = new AppDomainSetup();

			info.ApplicationBase = baseDir;
			info.ConfigurationFile = string.Format("{0}{1}", baseDir, "HelloWorld.exe.config");
			info.ShadowCopyFiles = "true";
			info.ShadowCopyDirectories = string.Format("{0}Temp", baseDir);
			info.CachePath = string.Format("{0}Cache", baseDir);

			AppDomain domain = AppDomain.CreateDomain("HelloWorld", null, info);

			domain.ExecuteAssembly(helloWorldPath);

			Console.WriteLine("After Execute Assembly");
		} 
	}
}
