﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDomain01
{
	class Program
	{
		static void Main(string[] args)
		{
			AppDomain domain = AppDomain.CurrentDomain;

			Console.WriteLine("Domain Id \t\t: {0}", domain.Id);
			Console.WriteLine("Domain Name\t\t: {0}", domain.FriendlyName);
			Console.WriteLine("Is Default AppDomain\t: {0}", domain.IsDefaultAppDomain());
			Console.WriteLine("LoaderOptimization\t: {0}", domain.SetupInformation.LoaderOptimization);
			Console.WriteLine("ConfigfileFile\t\t: {0}", domain.SetupInformation.ConfigurationFile);
			Console.WriteLine("CachePath\t\t: {0}", domain.SetupInformation.CachePath);
			Console.WriteLine("ShadowCopyFiles\t\t: {0}", domain.ShadowCopyFiles);
			Console.WriteLine("ShadowCopyDirectores\t: {0}", domain.SetupInformation.ShadowCopyDirectories);
			Console.WriteLine("Private Bin path\t: {0}", domain.SetupInformation.PrivateBinPath);
		}
	}
}
