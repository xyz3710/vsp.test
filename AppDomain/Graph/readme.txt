SuperGraph readme

This is the current version of supergraph. A few notes:

0) You'll want to open the SuperGraph.sln solution file in the SuperGraph directory

1) The location that the program looks for extra functions is hardcoded in the constructor for Form1. 

2) The ExtraFunctions project is there to add things dynamically. Edit class1.cs, use the .bat file to compile it, and then drag it to a running instance of the program. Or, you can just copy it to the location you set in #1. 