using System;
using System.IO;

namespace ExtraFunctions
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class ExtraFuncs
	{
		private ExtraFuncs()
		{
		}

		public static double Square(double v)
		{
			return v * v;
		}

		public static double Cube(double v)
		{
			return v * v * v;
		}

		public static double Identify2(double v)
		{
			return v;
		}

		public static double BadFunction(double v)
		{
			File.CreateText(@"C:\junk.log");
			return v;
		}
	}
}
