using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.Serialization;
using System.Reflection;
using System.IO;
using System.Security.Policy;
using SuperGraphInterface;

namespace SuperGraph
{
	public class ReloadEventArgs: EventArgs
	{
		int reloadCount;

		public ReloadEventArgs(int reloadCount)
		{
			this.reloadCount = reloadCount;
		}

		public int ReloadCount
		{
			get
			{
				return reloadCount;
			}
		}
	}
	/// <summary>
	/// Summary description for Graph.
	/// </summary>
	/// 
	[Serializable]
	public class Graph: ISerializable
	{
		const int ColorCount = 6;
		static readonly Color[] colors = new Color[] {Color.Red, Color.Green, Color.Blue, Color.Yellow, Color.Magenta, Color.Cyan};

		public delegate void ReloadCountHandler(object sender, ReloadEventArgs args);

		public event ReloadCountHandler ReloadCountChanged;
		public event UnhandledExceptionEventHandler UnhandledException;

		FunctionList availableFunctions;

		ArrayList graphLines = new ArrayList();
		Function tFunction;
		double t;
		double min;
		double max;
		double increment = 1.0f;
		double scaleX;
		double scaleY;
		double offsetX;
		double offsetY;
		int reloadCount = 0;
		Loader loader;
		string functionAssemblyDirectory;
		FileSystemWatcher watcher;

		public Graph()
		{


#if fred
			GraphLine line = new GraphLine();
			line.XFunction = tFunction;
			line.YFunction = triangleWave;
			AddGraphLine(line);

			line = new GraphLine();
			line.XFunction = cos;
			line.YFunction = sin;
			AddGraphLine(line);
#endif
		}

		public string FunctionAssemblyDirectory
		{
			get
			{
				return functionAssemblyDirectory;
			}
			set
			{
				functionAssemblyDirectory = value;
				loader = new Loader(functionAssemblyDirectory);
				LoadUserAssemblies();
				watcher = new FileSystemWatcher(functionAssemblyDirectory, "*.dll");
				watcher.EnableRaisingEvents = true;
				watcher.Changed += new FileSystemEventHandler(FunctionFileChanged);
				watcher.Created += new FileSystemEventHandler(FunctionFileChanged);
				watcher.Deleted += new FileSystemEventHandler(FunctionFileChanged);
			}
		}

		public void FunctionFileChanged(object sender, FileSystemEventArgs args)
		{
			Console.WriteLine(args.ChangeType + " " + args.FullPath);
			Reload();
		}

		public void CopyFiles(string[] filenames)
		{
			foreach (string filename in filenames)
			{
				FileInfo fileInfo = new FileInfo(filename);
				string destination = functionAssemblyDirectory + "\\" + fileInfo.Name;
				File.Copy(filename, destination, true);
			}
		}

		// Deserialization constructor.
		// We do this ourselves so that we use the same delegate instances
		// as are in the available list...
		public Graph(SerializationInfo info, StreamingContext context): this()
		{
			int count = info.GetInt32("Count");
			for (int line = 0; line < count; line++)
			{
				GraphLine graphLine = new GraphLine( 
						info.GetString("xFunc" + line.ToString()),
						info.GetString("yFunc" + line.ToString()));

				graphLines.Add(graphLine);
			}
		}

		public void HookupFunctions()
		{
			foreach (GraphLine graphLine in graphLines)
			{
				graphLine.XFunction = FindFunction(graphLine.XName);
				graphLine.YFunction = FindFunction(graphLine.YName);
			}
		}

		/// <summary>
		/// Return the current function that has the right name...
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public Function FindFunction(string name)
		{
			foreach (Function function in availableFunctions)
			{
				if (function.Name == name)
					return function;
			}
			return tFunction;
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			int lineNumber = graphLines.Count;
			info.AddValue("Count", lineNumber);
			int currentLine = 0;
			foreach (GraphLine graphLine in graphLines)
			{
				info.AddValue("xFunc" + currentLine.ToString(),
					graphLine.XFunction.ToString());
				info.AddValue("yFunc" + currentLine.ToString(),
					graphLine.YFunction.ToString());
				currentLine++;
			}
		}

		public double Min
		{
			get
			{
				return min;
			}
			set
			{
				min = value;
			}
		}

		public double Max
		{
			get
			{
				return max;
			}
			set
			{
				max = value;
			}
		}

		public double Inc
		{
			get
			{
				return increment;
			}
			set
			{
				increment = value;
			}
		}

		public void AddGraphLine(GraphLine line)
		{
			graphLines.Add(line);
		}

		public void SetScale(Size size)
		{
			double minX = float.MaxValue;
			double maxX = float.MinValue;
			double minY = float.MaxValue;
			double maxY = float.MinValue;
			
			t = min;
			while (t <= max)
			{
				double tNew = t + increment;

				foreach (GraphLine line in graphLines)
				{
					double x1 = line.GetX(t);
					double y1 = line.GetY(t);
					double x2 = line.GetX(tNew);
					double y2 = line.GetY(tNew);

					if (x1 > maxX)
						maxX = x1;

					if (x1 < minX)
						minX = x1;

					if (x2 > maxX)
						maxX = x2;

					if (x2 < minX)
						minX = x2;

					if (y1 > maxY)
						maxY = y1;

					if (y1 < minY)
						minY = y1;

					if (y2 > maxY)
						maxY = y2;

					if (y2 < minY)
						minY = y2;
				}
				t = tNew;
			}

			offsetX = minX;
			scaleX = size.Width / (maxX - minX); 
			offsetY = minY;
			scaleY = size.Height / (maxY - minY);
			t = min;
		}

		public void DrawLines(Graphics g, Size size)
		{
			lock (this)
			{
				double tNew = t + increment;

				int lineNumber = 0;
				foreach (GraphLine line in graphLines)
				{
					double x1 = line.GetX(t);
					double y1 = line.GetY(t);
					double x2 = line.GetX(tNew);
					double y2 = line.GetY(tNew);

					float xPlot1 = (float)
						((x1 - offsetX) * scaleX);
					float xPlot2 = (float)
						((x2 - offsetX) * scaleX);
					float yPlot1 = (float)
						(size.Height - (y1 - offsetY) * scaleY);
					float yPlot2 = (float)
						(size.Height - (y2 - offsetY) * scaleY);

					try
					{
						Pen p = new Pen(colors[lineNumber % colors.Length], 2.0f);
						g.DrawLine(p,
							xPlot1, yPlot1, xPlot2, yPlot2);
						p.Dispose();
					}
					catch (OverflowException)
					{

					}
					lineNumber++;
				}

				t = tNew;
				g.Dispose();
				if (t > max)
				{
					t = min;
				}
			}
		}
	
		public double T(params double[] values)
		{
			return values[0];
		}

		public double func(params double[] values)
		{
			return values[0] % 25;
		}

		public double func4(params double[] values)
		{
			return 25 - (values[0] % 25);
		}

		public double func2(params double[] values)
		{
			return (100.0 * Math.Sin(values[0] / 30.0f));
		}

		public double func3(params double[] values)
		{
			return (75.0 * Math.Cos(values[0] / 30.0f));
		}

		public void UpdateListbox(ListBox listBox)
		{
			listBox.Items.Clear();

			foreach (GraphLine line in graphLines)
			{
				listBox.Items.Add(line);
			}
		}

		public void NewGraphLine()
		{
			GraphLine graphLine = new GraphLine();
			graphLine.XFunction = tFunction;
			graphLine.YFunction = tFunction;

			GraphLineDialog dlg = 
				new GraphLineDialog(graphLine, availableFunctions);

			if (dlg.ShowDialog() == DialogResult.OK)
			{
				graphLines.Add(graphLine);
			}
		}

		public void EditGraphLine(ListBox listBox)
		{
			GraphLine graphLine = (GraphLine) listBox.SelectedItem;
			GraphLineDialog dlg = 
				new GraphLineDialog(graphLine, availableFunctions);

			dlg.ShowDialog();
		}

		public void DeleteGraphLine(ListBox listBox)
		{
			graphLines.Remove(listBox.SelectedItem);
		}

		public void Reload()
		{
			lock(this)
			{
				loader.Unload();
				loader = new Loader(functionAssemblyDirectory);
				LoadUserAssemblies();
				HookupFunctions();
				reloadCount++;

				if (this.ReloadCountChanged != null)
					ReloadCountChanged(this, new ReloadEventArgs(reloadCount));
			}
		}

		void LoadBuiltInFunctions()
		{
			availableFunctions.AddAllFromType(typeof(Math));
			availableFunctions.AddAllFromType(typeof(Random));
			Function triangleWave = new Function("TriangleWave", new Function.FuncArrParam(func)); 
			availableFunctions.Add(triangleWave);
			Function sin = new Function("Sin", new Function.FuncArrParam(func2));
			availableFunctions.Add(sin);
			Function cos = new Function("Cos", new Function.FuncArrParam(func3));
			availableFunctions.Add(cos);
			Function triWave2 = new Function("TriangleWave2", new Function.FuncArrParam(func4));
			availableFunctions.Add(triWave2);
			tFunction = new Function("T", new Function.FuncArrParam(T));
			availableFunctions.Add(tFunction);
		}

		void LoadUserAssemblies()
		{
			availableFunctions = new FunctionList();
			LoadBuiltInFunctions();

			DirectoryInfo d = new DirectoryInfo(functionAssemblyDirectory);
			foreach (FileInfo file in d.GetFiles("*.dll"))
			{	
				FunctionList functionList;
				try
				{
					functionList = loader.LoadAssembly(file.FullName);
				}
				catch (PolicyException e)
				{
					throw new PolicyException(
						String.Format("Cannot load {0} - code requires privilege to execute", file.Name),
						e);
				}

				availableFunctions.Merge(functionList);
			}
		}
	}
}

