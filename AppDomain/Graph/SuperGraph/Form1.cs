using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using System.Security.Policy;

namespace SuperGraph
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;

		Graph graph = new Graph();

		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.TextBox min;
		private System.Windows.Forms.TextBox max;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ListBox graphLineListbox;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox inc;
		private System.Windows.Forms.Button save;
		private System.Windows.Forms.Button Unload;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox ReloadCount;
		private System.Windows.Forms.Panel graphPanel;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			LoadGraph();
			try
			{
				graph.FunctionAssemblyDirectory = AppDomain.CurrentDomain.BaseDirectory + "\\addins";
				graph.HookupFunctions();
				graph.Min = Convert.ToDouble(this.min.Text);
				graph.Max = Convert.ToDouble(this.max.Text);

				graph.SetScale(graphPanel.Size);
				graph.UpdateListbox(this.graphLineListbox);

				graph.ReloadCountChanged += new Graph.ReloadCountHandler(ReloadChanged);
			}
			catch (PolicyException e)
			{
				MessageBox.Show(e.Message);
			}

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.min = new System.Windows.Forms.TextBox();
			this.max = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.graphPanel = new System.Windows.Forms.Panel();
			this.graphLineListbox = new System.Windows.Forms.ListBox();
			this.button1 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.inc = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.save = new System.Windows.Forms.Button();
			this.Unload = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.ReloadCount = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 20;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// min
			// 
			this.min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.min.Location = new System.Drawing.Point(670, 52);
			this.min.Name = "min";
			this.min.Size = new System.Drawing.Size(120, 21);
			this.min.TabIndex = 1;
			this.min.Text = "0";
			this.min.Leave += new System.EventHandler(this.min_Leave);
			// 
			// max
			// 
			this.max.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.max.Location = new System.Drawing.Point(670, 86);
			this.max.Name = "max";
			this.max.Size = new System.Drawing.Size(120, 21);
			this.max.TabIndex = 2;
			this.max.Text = "100";
			this.max.Leave += new System.EventHandler(this.max_Leave);
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.Location = new System.Drawing.Point(603, 52);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(39, 24);
			this.label2.TabIndex = 3;
			this.label2.Text = "Min:";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.Location = new System.Drawing.Point(603, 86);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(48, 25);
			this.label3.TabIndex = 4;
			this.label3.Text = "Max:";
			// 
			// graphPanel
			// 
			this.graphPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.graphPanel.BackColor = System.Drawing.Color.Black;
			this.graphPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.graphPanel.Location = new System.Drawing.Point(29, 34);
			this.graphPanel.Name = "graphPanel";
			this.graphPanel.Size = new System.Drawing.Size(488, 462);
			this.graphPanel.TabIndex = 5;
			// 
			// graphLineListbox
			// 
			this.graphLineListbox.ItemHeight = 12;
			this.graphLineListbox.Location = new System.Drawing.Point(595, 198);
			this.graphLineListbox.Name = "graphLineListbox";
			this.graphLineListbox.Size = new System.Drawing.Size(192, 184);
			this.graphLineListbox.TabIndex = 6;
			this.graphLineListbox.DoubleClick += new System.EventHandler(this.graphLineListbox_DoubleClick);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(605, 396);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(90, 25);
			this.button1.TabIndex = 7;
			this.button1.Text = "Add New";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(595, 172);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(87, 25);
			this.label4.TabIndex = 8;
			this.label4.Text = "Graph Lines";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.inc);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(586, 34);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(211, 121);
			this.groupBox1.TabIndex = 9;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "T";
			// 
			// inc
			// 
			this.inc.Location = new System.Drawing.Point(77, 86);
			this.inc.Name = "inc";
			this.inc.Size = new System.Drawing.Size(120, 21);
			this.inc.TabIndex = 1;
			this.inc.Text = "1";
			this.inc.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			this.inc.Leave += new System.EventHandler(this.textBox1_Leave);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(10, 86);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(48, 25);
			this.label1.TabIndex = 0;
			this.label1.Text = "Inc:";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(710, 396);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(90, 25);
			this.button2.TabIndex = 10;
			this.button2.Text = "Delete";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// save
			// 
			this.save.Location = new System.Drawing.Point(643, 439);
			this.save.Name = "save";
			this.save.Size = new System.Drawing.Size(90, 25);
			this.save.TabIndex = 11;
			this.save.Text = "Save";
			this.save.Click += new System.EventHandler(this.save_Click);
			// 
			// Unload
			// 
			this.Unload.Location = new System.Drawing.Point(643, 482);
			this.Unload.Name = "Unload";
			this.Unload.Size = new System.Drawing.Size(90, 25);
			this.Unload.TabIndex = 12;
			this.Unload.Text = "Reload";
			this.Unload.Click += new System.EventHandler(this.Unload_Click);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(29, 482);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(96, 25);
			this.label5.TabIndex = 13;
			this.label5.Text = "Reload Count:";
			// 
			// ReloadCount
			// 
			this.ReloadCount.Location = new System.Drawing.Point(144, 482);
			this.ReloadCount.Name = "ReloadCount";
			this.ReloadCount.Size = new System.Drawing.Size(38, 21);
			this.ReloadCount.TabIndex = 14;
			this.ReloadCount.Text = "0";
			this.ReloadCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// Form1
			// 
			this.AllowDrop = true;
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(824, 546);
			this.Controls.Add(this.ReloadCount);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.Unload);
			this.Controls.Add(this.save);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.graphLineListbox);
			this.Controls.Add(this.graphPanel);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.max);
			this.Controls.Add(this.min);
			this.Controls.Add(this.groupBox1);
			this.Name = "Form1";
			this.Text = "SuperGraph";
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
			this.Resize += new System.EventHandler(this.Form1_Resize);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		void UpdateGraphLineListbox()
		{
			graph.UpdateListbox(graphLineListbox);
		}

		private void DrawFunction()
		{
			Graphics g = graphPanel.CreateGraphics();
			g.SmoothingMode = SmoothingMode.HighQuality;
			graph.DrawLines(g, graphPanel.Size);
		}


		private void timer1_Tick(object sender, System.EventArgs e)
		{
			DrawFunction();
		}

		private void Form1_Resize(object sender, System.EventArgs e)
		{
			Invalidate();
			graph.SetScale(graphPanel.Size);
		}

		private void max_Leave(object sender, System.EventArgs e)
		{
			graph.Max = Convert.ToDouble(max.Text);
			graph.SetScale(graphPanel.Size);
			graphPanel.Invalidate();
	
		}

		private void min_Leave(object sender, System.EventArgs e)
		{
			graph.Min = Convert.ToDouble(min.Text);
			graph.SetScale(graphPanel.Size);
			graphPanel.Invalidate();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			timer1.Enabled = false;
			graph.NewGraphLine();
			UpdateGraphLineListbox();
			graphPanel.Invalidate();
			graph.SetScale(graphPanel.Size);
			timer1.Enabled = true;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			timer1.Enabled = false;
			graph.DeleteGraphLine(graphLineListbox);
			UpdateGraphLineListbox();
			graphPanel.Invalidate();
			graph.SetScale(graphPanel.Size);
			timer1.Enabled = true;
		}

		private void graphLineListbox_DoubleClick(object sender, System.EventArgs e)
		{
			timer1.Enabled = false;
			graph.EditGraphLine(graphLineListbox);
			UpdateGraphLineListbox();
			graphPanel.Invalidate();
			graph.SetScale(graphPanel.Size);
			timer1.Enabled = true;
		}

		private void textBox1_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void textBox1_Leave(object sender, System.EventArgs e)
		{
			graph.Inc = Convert.ToDouble(inc.Text);
			graph.SetScale(graphPanel.Size);
			graphPanel.Invalidate();
		}

		private void save_Click(object sender, System.EventArgs e)
		{
			using (Stream s = File.Create("supergraph.xml"))
			{
				SoapFormatter formatter = new SoapFormatter();
				formatter.Serialize(s, graph);
			}
		}

		private void LoadGraph()
		{
			Stream s = null;
			Graph loadedGraph = null;
			try
			{
				s = File.OpenRead("supergraph.xml");
				SoapFormatter formatter = new SoapFormatter();
				loadedGraph = (Graph)
					formatter.Deserialize(s);
			}
			catch (Exception e)
			{
				return;
			}
			finally
			{
				if (s != null)
					s.Close();
			}
			graph = loadedGraph;
		}

		private void Unload_Click(object sender, System.EventArgs e)
		{
			this.timer1.Enabled = false;
			graph.Reload();
			this.timer1.Enabled = true;
		}

		void ReloadChanged(object sender, ReloadEventArgs args)
		{
			//this.ReloadCount.Text = args.ReloadCount.ToString();
			graphPanel.Invalidate();
		}

		private void Form1_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			string[] filenames = (string[]) e.Data.GetData(DataFormats.FileDrop);
			graph.CopyFiles(filenames);
		}

		private void Form1_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			object o = e.Data.GetData(DataFormats.FileDrop);
			if (o != null)
			{
				e.Effect = DragDropEffects.Copy;
			}
			string[] formats = e.Data.GetFormats();
			string s = e.Data.GetType().ToString();
		}
	}
}
