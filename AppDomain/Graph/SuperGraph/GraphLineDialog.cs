using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace SuperGraph
{
	/// <summary>
	/// Summary description for GraphLineDialog.
	/// </summary>
	public class GraphLineDialog : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		GraphLine graphLine;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button ok;
		private System.Windows.Forms.ComboBox xAxis;
		private System.Windows.Forms.ComboBox yAxis;
		FunctionList availableFunctions;

		public GraphLineDialog(GraphLine graphLine, FunctionList availableFunctions)
		{
			this.graphLine = graphLine;
			this.availableFunctions = availableFunctions;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			foreach (Function func in availableFunctions)
			{
				xAxis.Items.Add(func);
				yAxis.Items.Add(func);
			}

			xAxis.SelectedItem = graphLine.XFunction;
			yAxis.SelectedItem = graphLine.YFunction;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.ok = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.xAxis = new System.Windows.Forms.ComboBox();
			this.yAxis = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(24, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(48, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "X Axis:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(24, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(40, 23);
			this.label2.TabIndex = 1;
			this.label2.Text = "YAxis";
			// 
			// ok
			// 
			this.ok.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.ok.Location = new System.Drawing.Point(96, 96);
			this.ok.Name = "ok";
			this.ok.TabIndex = 2;
			this.ok.Text = "OK";
			this.ok.Click += new System.EventHandler(this.ok_Click);
			// 
			// button2
			// 
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.Location = new System.Drawing.Point(216, 96);
			this.button2.Name = "button2";
			this.button2.TabIndex = 3;
			this.button2.Text = "Cancel";
			// 
			// xAxis
			// 
			this.xAxis.Location = new System.Drawing.Point(72, 24);
			this.xAxis.Name = "xAxis";
			this.xAxis.Size = new System.Drawing.Size(328, 21);
			this.xAxis.TabIndex = 4;
			this.xAxis.Text = "comboBox1";
			// 
			// yAxis
			// 
			this.yAxis.Location = new System.Drawing.Point(72, 56);
			this.yAxis.Name = "yAxis";
			this.yAxis.Size = new System.Drawing.Size(328, 21);
			this.yAxis.TabIndex = 5;
			this.yAxis.Text = "comboBox2";
			// 
			// GraphLineDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(408, 142);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.yAxis,
																		  this.xAxis,
																		  this.button2,
																		  this.ok,
																		  this.label2,
																		  this.label1});
			this.Name = "GraphLineDialog";
			this.Text = "Define Graph Line";
			this.ResumeLayout(false);

		}
		#endregion

		private void ok_Click(object sender, System.EventArgs e)
		{
			graphLine.XFunction =
				(Function) xAxis.SelectedItem;
			graphLine.YFunction =
				(Function) yAxis.SelectedItem;
		}
	}
}
