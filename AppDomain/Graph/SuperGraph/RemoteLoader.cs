using System;
using System.IO;
using System.Reflection;
using System.Security;
using System.Security.Policy;
using SuperGraph;


namespace SuperGraphInterface
{
	/// <summary>
	/// Summary description for RemoteLoader.
	/// </summary>
	public class RemoteLoader: MarshalByRefObject
	{
		public RemoteLoader()
		{
			//Console.WriteLine(AppDomain.CurrentDomain);
		}

		public FunctionList LoadAssembly(string fullname)
		{
			string path = Path.GetDirectoryName(fullname);
			string filename = Path.GetFileNameWithoutExtension(fullname);

			FunctionList functionList = new FunctionList();
			Assembly assembly = Assembly.Load(filename);

			foreach (Type t in assembly.GetTypes())
			{
				functionList.AddAllFromType(t);
			}

			return functionList;
		}
	}
}
