using System;
using System.Runtime.Serialization;

namespace SuperGraph
{
	/// <summary>
	/// Contains enough information to draw a single
	/// line on a graph...
	/// </summary>
	[Serializable]
	public class GraphLine
	{
		Function	xFunction;
		Function	yFunction;
		string		xName;
		string		yName;

		public GraphLine()
		{
		}

		public GraphLine(string xName, string yName)
		{
			this.xName = xName;
			this.yName = yName;
		}

		public override string ToString()
		{
			return String.Format("X={0}, Y={1}", 
				XFunction, yFunction);
		}

		public string XName
		{
			get
			{
				return xName;
			}
		}

		public string YName
		{
			get
			{
				return yName;
			}
		}

		public Function XFunction
		{
			get
			{
				return xFunction;
			}
			set
			{
				xFunction = value;
				xName = xFunction.Name;
			}
		}

		public Function YFunction
		{
			get
			{
				return yFunction;
			}
			set
			{
				yFunction = value;
				yName = yFunction.Name;
			}
		}

		public double GetX(double t)
		{
			if (xFunction != null)
			{
				return xFunction.Invoke(t);
			}
			else
			{
				return double.NaN;
			}
		}

		public double GetY(double t)
		{
			if (yFunction != null)
			{
				return yFunction.Invoke(t);
			}
			else
			{
				return double.NaN;
			}		
		}
	}
}
