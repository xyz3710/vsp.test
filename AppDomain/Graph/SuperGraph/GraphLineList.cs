using System;
using System.Collections;

namespace SuperGraph
{
	/// <summary>
	/// Summary description for GraphLineList.
	/// </summary>
	public class GraphLineList
	{
		ArrayList lines = new ArrayList();

		public GraphLineList()
		{
		}

		public void Add(GraphLine line)
		{
			lines.Add(line);
		}
	}
}
