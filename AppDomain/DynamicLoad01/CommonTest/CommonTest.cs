﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace iDASiT.Win.Common
{
	[Serializable]
	public class CommonTest 
	{
		private string _fieldName;

		#region Properties
		/// <summary>
		/// FieldName를 구하거나 설정합니다.
		/// </summary>
		public string FieldName
		{
			get
			{				
				if (string.IsNullOrEmpty(_fieldName) == true)
					return "ver 2.0";
					
				return _fieldName;
			}
			set
			{
				_fieldName = value;
			}
		}

		#endregion
        
		public const string TestValue0 = "Test Value";

		public bool TestMethod0()
		{
			return true;
		}

		public static string TestValue = "Test Value";

		public static bool TestMethod()
		{
        	return true;
        }
	}
}
