﻿namespace iDASiT.Win.Mes
{
	partial class F0990
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
			Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(F0990));
			this.xtxtLog = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.xtxtLotId = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.pnlContainerBody.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xtxtLog)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.xtxtLotId)).BeginInit();
			this.SuspendLayout();
			// 
			// pnlContainerBody
			// 
			this.pnlContainerBody.BackColor = System.Drawing.Color.White;
			this.pnlContainerBody.Controls.Add(this.tableLayoutPanel1);
			// 
			// xtxtLog
			// 
			appearance1.BackColor = System.Drawing.Color.White;
			appearance1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(139)))), ((int)(((byte)(141)))));
			appearance1.ForeColor = System.Drawing.Color.Black;
			appearance1.TextHAlignAsString = "Left";
			appearance1.TextVAlignAsString = "Middle";
			this.xtxtLog.Appearance = appearance1;
			this.xtxtLog.AutoSize = false;
			this.xtxtLog.BackColor = System.Drawing.Color.White;
			this.xtxtLog.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xtxtLog.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xtxtLog.Location = new System.Drawing.Point(3, 31);
			this.xtxtLog.Multiline = true;
			this.xtxtLog.Name = "xtxtLog";
			this.xtxtLog.Scrollbars = System.Windows.Forms.ScrollBars.Both;
			this.xtxtLog.Size = new System.Drawing.Size(969, 546);
			this.xtxtLog.TabIndex = 0;
			this.xtxtLog.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.xtxtLog, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.xtxtLotId, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(975, 580);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// xtxtLotId
			// 
			appearance2.BackColor = System.Drawing.Color.White;
			appearance2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(139)))), ((int)(((byte)(141)))));
			appearance2.ForeColor = System.Drawing.Color.Black;
			appearance2.TextHAlignAsString = "Left";
			appearance2.TextVAlignAsString = "Middle";
			this.xtxtLotId.Appearance = appearance2;
			this.xtxtLotId.AutoSize = false;
			this.xtxtLotId.BackColor = System.Drawing.Color.White;
			this.xtxtLotId.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded1;
			this.xtxtLotId.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xtxtLotId.Location = new System.Drawing.Point(3, 3);
			this.xtxtLotId.Name = "xtxtLotId";
			this.xtxtLotId.Size = new System.Drawing.Size(969, 22);
			this.xtxtLotId.TabIndex = 1;
			this.xtxtLotId.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
			// 
			// F0990
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
			this.ClientSize = new System.Drawing.Size(983, 588);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "F0990";
			this.ThemeStyle = iDASiT.Framework.Win.ThemeStyle.Theme1;
			this.Shown += new System.EventHandler(this.F0990_Shown);
			this.pnlContainerBody.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xtxtLog)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.xtxtLotId)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Infragistics.Win.UltraWinEditors.UltraTextEditor xtxtLog;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private Infragistics.Win.UltraWinEditors.UltraTextEditor xtxtLotId;
	}
}