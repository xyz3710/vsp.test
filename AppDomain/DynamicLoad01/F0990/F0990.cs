﻿/**********************************************************************************************************************/
/*	Domain		:	iDASiT.Win.Mes.F0990
/*	Creator		:	iDASiT (KIMKIWON)
/*	Create		:	2008-08-12 오후 03:57:57
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using iDASiT.AppFoundation.Process;
using iDASiT.Engine.Process;
using iDASiT.Framework.Win;
using iDASiT.TrustCore.Process;

namespace iDASiT.Win.Mes
{
	/// <summary>
	/// F0990 클래스를 선언합니다.
	/// </summary>
	public partial class F0990 : BaseForm
	{
		#region Fields
		private bool _after;
		private Dictionary<int, string> _log = new Dictionary<int, string>();
		#endregion

		#region Constructor
		/// <summary>
		/// F0990의 인스턴스를 생성합니다.
		/// </summary>
		public F0990()
			: this(bool.FalseString)
		{
			
		}

		/// <summary>
		/// F0990의 인스턴스를 생성합니다.
		/// </summary>
		public F0990(string after)
			: base()
		{
			InitializeComponent();

			InitializeForm();

			bool.TryParse(after, out _after);
		}
		#endregion

		#region InitializeForm
		private void InitializeForm()
		{
			// Form 초기화 method를 삽입합니다.


			base.ThemeStyle = ThemeStyle.Theme1;
		}
		#endregion

		#region Toolbar Event Handler
		/// <summary>
		/// 표준 툴바의 [신규] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnNewClick()
		{
			base.OnNewClick();



			xtxtLotId.Text = "";
		}

		/// <summary>
		/// 표준 툴바의 [저장] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnSaveClick()
		{
			base.OnSaveClick();


		}

		/// <summary>
		/// 표준 툴바의 [조회] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnSearchClick()
		{
			base.OnSearchClick();


		}

		/// <summary>
		/// 표준 툴바의 [삭제] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnDeleteClick()
		{
			base.OnDeleteClick();


		}

		/// <summary>
		/// 표준 툴바의 [실행] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnRunClick()
		{
			base.OnRunClick();

			RunTest();
		}

		/// <summary>
		/// 표준 툴바의 [취소] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnCancelClick()
		{
			base.OnCancelClick();


		}

		/// <summary>
		/// 표준 툴바의 [행 추가] 버튼이 눌렸을 때<br/>
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnAddRowClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnAddRowClick();
		}

		/// <summary>
		/// 표준 툴바의 [행 삭제] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnDelRowClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnDelRowClick();
		}

		/// <summary>
		/// 표준 툴바의 [엑셀] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnExcelClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnExcelClick();
		}

		/// <summary>
		/// 표준 툴바의 [미리보기] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnPreviewClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnPreviewClick();
		}

		/// <summary>
		/// 표준 툴바의 [인쇄] 버튼이 눌렸을 때
		/// <seealso cref="iDASiT.Framework.Win.BaseForm.ToolbarTargetXGrid"/>를 먼저 설정해야 합니다.
		/// </summary>
		protected override void OnPrintClick()
		{
			//base.ToolbarTargetXGrid = ;
			base.OnPrintClick();
		}

		/// <summary>
		/// 표준 툴바의 [창닫기] 버튼이 눌렸을 때
		/// </summary>
		protected override void OnExitClick()
		{
			base.OnExitClick();
		}
		#endregion

		#region Properties

		#endregion

		#region Event Methods
		private void F0990_Shown(object sender, EventArgs e)
		{			
			Text = string.Format("{0} : {1}", Assembly.GetExecutingAssembly().GetName().Version.ToString(), _after.ToString());
			xtxtLotId.Text = "00898401S";
			Text = Assembly.GetExecutingAssembly().FullName;
		}
		#endregion

		#region User defined Methods
		private void RunTest()
		{
			#region For Test
			/*
			// MoveOutStandard
			List<Lot> moveOutLots = new List<Lot>();
			LotProxy lotProxy = new LotProxy();

			Lot mLot = lotProxy.GetLotInProcessing(PlantId, "00898401S001");
			LinkedLotQty linkedLotQtyMoveOut = new LinkedLotQty(mLot.LotQtyValue, mLot.LotQtyCount, mLot.LotQtyWeight);
			LotMoveOutStandardEngine.NewLot(mLot, mLot.PlantId, mLot.LotId, mLot.LotHstSeq, mLot.OperId, linkedLotQtyMoveOut);

			moveOutLots.Add(mLot);
			int retryCount = 3;

			for (int i = 0; i < retryCount; i++)
			{
				try
				{
					List<Lot> moLots = lotProxy.LotsMoveOutStandard(moveOutLots);

					break;
				}
				catch (Exception ex)
				{
					string patternString = "ORA-00001";
					Regex regex = new Regex(patternString);

					if (regex.IsMatch(ex.Message) == true)
						continue;
					else
						throw new Exception(ex.Message, ex);
				}
			}

			return;
			*/
			#endregion

			xtxtLog.Clear();

			if (_after == false)
			{
				for (int i = 1; i <= 500; i++)
					RunLotDistribute(i, "DCC-N-S500", "DCC-N-S500.1");
			}
			else
			{
				for (int i = 501; i < 601; i++)
					RunLotDistribute(i, "DCC-N-S500", "DCC-N-S500.1");

				for (int i = 601; i <= 800; i++)
					RunLotDistribute(i, "DCC-N-S700", "DCC-N-S700.1");

				for (int i = 801; i <= 840; i++)
					RunLotDistribute(i, "DCC-N-S630", "DCC-N-S630.1");

				for (int i = 841; i <= 850; i++)
					RunLotDistribute(i, "DCC-N-S900", "DCC-N-S900.1");

				for (int i = 851; i <= 900; i++)
					RunLotDistribute(i, "DCC-N-S970", "DCC-N-S970.1");

				for (int i = 951; i <= 999; i++)
					RunLotDistribute(i, "DCC-N-S600", "DCC-N-S600.1");
			}

			foreach (int key in _log.Keys)
				xtxtLog.Text += string.Format("{0:000}\t{1}{2}", key, _log[key], Environment.NewLine);

			MessageBox.Show("완료되었습니다.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private delegate void WriteLogDelegate(string log);

		private void WriteLog(string log)
		{
			if (xtxtLog.InvokeRequired == true)
			{
				WriteLogDelegate writeLogDelegate = new WriteLogDelegate(WriteLog);

				Invoke(writeLogDelegate, log);                
			}
			else
			{
				xtxtLog.BeginUpdate();
				xtxtLog.Text += string.Format("{0}{1}", log, Environment.NewLine);
				xtxtLog.Update();
				xtxtLog.EndUpdate();
			}
		}

		private void RunLotDistribute(int minorSeq, string productId, string specId)
		{
			Thread.Sleep(1);

			try
			{
                string lotId = xtxtLotId.Text.Trim();
				LotProxy lotProxy = new LotProxy();
				Lot fromLot = lotProxy.GetLot(lotId);
				Lot toLot = new Lot();
				
				fromLot.Clone(out toLot);

				toLot.LotId = string.Format("{0}{1:000}", lotId, minorSeq);
				toLot.LotIdMajor = lotId;
				toLot.LotIdMinor = string.Format("{0:000}", minorSeq);
				toLot.LotIdMinorSeq = minorSeq;
				toLot.LotIdDesc = toLot.LotId;

				List<Lot> toLots = new List<Lot>();

				toLots.Add(toLot);

				LinkedLotQty linkedLotQty = new LinkedLotQty();

				foreach (Lot lot in toLots)
				{
					ILotQty lotQty = new LotQty(5.02d, 0, 0);
					LotDistributeEngine.NewToLot(lot, fromLot.LotId, lot.LotIdMajor, lot.LotIdMinor, lotQty, productId, specId);

					UnitTransaction.GetDistributedLots(lot, productId, lot.OperId, PlantId, UserId);
					linkedLotQty.Add(lotQty);
				}

				LotDistributeEngine.NewLot(fromLot, fromLot.PlantId, fromLot.LotId, fromLot.LotHstSeq, linkedLotQty);

				fromLot.UserId = "ADMIN";

				int retryCount = 3;

				// Distribute
				List<Lot> distributeLots = new List<Lot>();

				for (int i = 0; i < retryCount; i++)
				{
					try
					{
						distributeLots = lotProxy.LotDistribute(fromLot, toLots);
                        	
						break;
					}
					catch (Exception ex)
					{
						string patternString = "ORA-00001";
						Regex regex = new Regex(patternString);

						if (regex.IsMatch(ex.Message) == true)
							continue;
						else
							throw new Exception(ex.Message, ex);                        	
					}	
				}

				// MoveOutStandard
				List<Lot> moveOutLots = new List<Lot>();

				Lot mLot = lotProxy.GetLotInProcessing(PlantId, toLots[0].LotId);
				LinkedLotQty linkedLotQtyMoveOut = new LinkedLotQty(mLot.LotQtyValue, mLot.LotQtyCount, mLot.LotQtyWeight);
				LotMoveOutStandardEngine.NewLot(mLot, mLot.PlantId, mLot.LotId, mLot.LotHstSeq, mLot.OperId, linkedLotQtyMoveOut);

				moveOutLots.Add(mLot);

				for (int i = 0; i < retryCount; i++)
				{
					try
					{
						List<Lot> moLots = lotProxy.LotsMoveOutStandard(moveOutLots);

						break;
					}
					catch (Exception ex)
					{
						string patternString = "ORA-00001";
						Regex regex = new Regex(patternString);

						if (regex.IsMatch(ex.Message) == true)
							continue;
						else
							throw new Exception(ex.Message, ex);
					}
				}

				// Loss
				LinkedLotQty totalLinkedLotQty = new LinkedLotQty(0, 0, 0);
				// Lot을 할당한다.
				List<Lot> lossedLots = new List<Lot>();
				Lot lossLot = lotProxy.GetLot(moveOutLots[0].LotId);

				ILinkedLotQty linkedLotQtyLoss = new LinkedLotQty(0.02d, lossLot.LotQtyCount, lossLot.LotQtyWeight);
				totalLinkedLotQty.Add(linkedLotQtyLoss);

				Lot testLot = LotLossEngine.NewLot(PlantId, lossLot.LotId, lossLot.LotHstSeq, linkedLotQtyLoss);
				testLot.UserId = "DEV";

				lossedLots.Add(testLot);

				// 코드를 할당한다.
				List<LotCodeHistory> lotCodeHistories = new List<LotCodeHistory>();

				LotCodeHistory lotCodeHistory = LotLossEngine.NewLotCodeHistory(lossLot.LotId, "NONE", totalLinkedLotQty);
				lotCodeHistories.Add(lotCodeHistory);

				for (int i = 0; i < retryCount; i++)
				{
					try
					{
						lossedLots = lotProxy.LotsLoss(lossedLots, lotCodeHistories);

						break;
					}
					catch (Exception ex)
					{
						string patternString = "ORA-00001";
						Regex regex = new Regex(patternString);

						if (regex.IsMatch(ex.Message) == true)
							continue;
						else
							throw new Exception(ex.Message, ex);
					}
				}
			}
			catch (Exception ex)
			{
				WriteLog(string.Format("{0:000}\t{1}", minorSeq, ex.Message));				
			}
		}
		#endregion
	}
}