﻿namespace DynamicLoad01
{
	partial class TestDriverForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnTest = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.txtLog = new System.Windows.Forms.TextBox();
			this.btnCallCommon = new System.Windows.Forms.Button();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnTest
			// 
			this.btnTest.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnTest.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnTest.Location = new System.Drawing.Point(3, 3);
			this.btnTest.Name = "btnTest";
			this.btnTest.Size = new System.Drawing.Size(253, 37);
			this.btnTest.TabIndex = 0;
			this.btnTest.Text = "Form Test";
			this.btnTest.UseVisualStyleBackColor = true;
			this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
			// 
			// btnClose
			// 
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.btnClose.Location = new System.Drawing.Point(0, 345);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(519, 28);
			this.btnClose.TabIndex = 3;
			this.btnClose.Text = "&Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// txtLog
			// 
			this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtLog.Location = new System.Drawing.Point(0, 43);
			this.txtLog.Multiline = true;
			this.txtLog.Name = "txtLog";
			this.txtLog.Size = new System.Drawing.Size(519, 302);
			this.txtLog.TabIndex = 1;
			// 
			// btnCallCommon
			// 
			this.btnCallCommon.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCallCommon.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnCallCommon.Location = new System.Drawing.Point(262, 3);
			this.btnCallCommon.Name = "btnCallCommon";
			this.btnCallCommon.Size = new System.Drawing.Size(254, 37);
			this.btnCallCommon.TabIndex = 4;
			this.btnCallCommon.Text = "Common Test";
			this.btnCallCommon.UseVisualStyleBackColor = true;
			this.btnCallCommon.Click += new System.EventHandler(this.btnCallCommon_Click);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.btnTest, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnCallCommon, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(519, 43);
			this.tableLayoutPanel1.TabIndex = 5;
			// 
			// TestDriverForm
			// 
			this.AcceptButton = this.btnTest;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(519, 373);
			this.Controls.Add(this.txtLog);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "TestDriverForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "TestDriverForm";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnTest;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.TextBox txtLog;
		private System.Windows.Forms.Button btnCallCommon;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
	}
}