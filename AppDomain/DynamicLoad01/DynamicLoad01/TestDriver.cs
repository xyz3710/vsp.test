using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Threading;
using System.Security.Policy;
using iDASiT.Framework.Win;
using iDASiT.Win.Common;

namespace DynamicLoad01
{
	[Serializable]
	public class TestDriver
	{
		public void Run(ExceptionOccured exceptionOccured, bool callForm)
		{
			// 로드할 어셈블리의 경로는 menus.config 파일 경로 + Win 으로 지정한다.
			//string applicationBase = @"C:\Program Files\iDASiT\Win\M";
			string applicationBase = AppDomain.CurrentDomain.BaseDirectory;
			string assemblyName = "F0990.dll";
			string assemblyPath = string.Format("{0}\\{1}", applicationBase, assemblyName);

			#region For Test
			/*
			if (File.Exists(assemblyPath) == false)
			{
				Console.WriteLine("파일이 없습니다.");
				return;
			}
			*/
			#endregion

//			Assembly assembly = Assembly.LoadFrom(assemblyPath);
//			string typeFullName = string.Empty;
//
//			foreach (Type type in assembly.GetTypes())
//			{
//				if (type.Name == assembly.GetName().Name)
//				{
//					typeFullName = type.FullName;
//
//					break;
//				}
//			}
//
//			if (typeFullName == string.Empty)
//			{
//				MessageBox.Show("메뉴 어셈블리 파일의 네임스페이스가 틀리거나 정상적으로 생성된 파일이 아닙니다", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
//
//				return;
//			}

			try
			{
				AppDomainSetup newDomainInfo = new AppDomainSetup();
				string shadowFolderName = @"Win\M";
				string remoteBase = "http://192.168.25.28:8088/iDASiT.Framework.Win.Deploy";

				newDomainInfo.ApplicationName = (callForm == true ? "Form " : "Common ") + "AppDomainTest";
				// NOTICE: applicationBase로 변경하면 Local에서 Load한다.
				// by KIMKIWON\xyz37 in 2008년 9월 4일 목요일 오전 9:37
				newDomainInfo.ApplicationBase = applicationBase;
				newDomainInfo.PrivateBinPath = string.Format(@"{0};Framework.Win", shadowFolderName);								// 폴더명만
				newDomainInfo.CachePath = string.Format(@"{0}\{1}\Cache\", applicationBase, shadowFolderName);		// \을 포함한 full path
				newDomainInfo.ShadowCopyFiles = "true";
				newDomainInfo.ShadowCopyDirectories = string.Format(@"{0}{1}\", applicationBase, shadowFolderName);	// \을 포함한 full path;

				Evidence evidence = AppDomain.CurrentDomain.Evidence;
								
				_newDomain = AppDomain.CreateDomain(assemblyName, null, newDomainInfo);
				// NOTICE: Remoting으로 연결하므로 ApplicationBase가 기본 도메인과 틀릴 경우 Event에 대해서 Exception이 발생한다.
				// by KIMKIWON\xyz37 in 2008년 8월 14일 목요일 오전 11:16
				//_newDomain.DomainUnload += new EventHandler(newDomain_DomainUnload);

				//AssemblyName assemblyName1 = new AssemblyName("F0990");
				//_newDomain.AssemblyLoad += new AssemblyLoadEventHandler(_newDomain_AssemblyLoad);
								
				DateTime now = DateTime.Now;

				//Assembly loadedAssembly = _newDomain.Load("F0990");
				//BaseForm menuForm = loadedAssembly.CreateInstance(loadedAssembly.GetTypes()[0].FullName) as BaseForm;		// iDASiT.Win.Mes.F0990

				BindingFlags bindingFlags = BindingFlags.CreateInstance | BindingFlags.Instance | BindingFlags.Public;
				
				if (callForm == false)
				{
					object common = _newDomain.CreateInstanceAndUnwrap("CommonTest",
											"iDASiT.Win.Common.CommonTest",
											true,
											bindingFlags,
											null,
											null,
											null,
											null,
											null);
					string result = string.Empty;

					exceptionOccured((string)common.GetType().InvokeMember("TestMethod0", BindingFlags.InvokeMethod, null, common, null));

					//common.GetType().InvokeMember("FieldName", BindingFlags.SetProperty, null, common, new object[] { "test value into Property by InvokeMember"});
					//result = (string)common.GetType().InvokeMember("FieldName", BindingFlags.GetProperty, null, common, null);
					//exceptionOccured(result);

					common.GetType().GetProperty("FieldName").SetValue(common, "test value into Property", null);
					result = (string)common.GetType().GetProperty("FieldName").GetValue(common, null);
					exceptionOccured(result);

					MessageBox.Show(string.Format("Method Invoke time : {0}", DateTime.Now - now));

					now = DateTime.Now;
				}
				else
				{
					object menuFormInstance = Activator.CreateInstance(_newDomain,
												  "F0990",
												  "iDASiT.Win.Mes.F0990",
												  true,
												  bindingFlags,
												  null,
												  new object[] { bool.TrueString },
												  null,
												  null,
												  null);

					if (menuFormInstance != null)
					{
						;
					}
					#region For Test
					/*
					BaseForm menuForm = _newDomain.CreateInstanceAndUnwrap("F0990",
											"iDASiT.Win.Mes.F0990",
											true,
											bindingFlags,
											null,
											new object[] { bool.TrueString },
											null,
											null,
											null) as BaseForm;
					
					if (menuForm != null)
					{
						menuForm.Text = string.Empty;
						//loadedAssembly.GetName().Version.ToString();
						// NOTICE: Remoting으로 연결하므로 ApplicationBase가 기본 도메인과 틀릴 경우 Event에 대해서 Exception이 발생한다.
						// by KIMKIWON\xyz37 in 2008년 8월 14일 목요일 오전 11:16
						//menuForm.FormClosing += new FormClosingEventHandler(menuForm_FormClosing);

						MessageBox.Show(string.Format("총 소요 시간 : {0}", DateTime.Now - now));

						menuForm.ShowDialog();

						AppDomain.Unload(_newDomain);
					}
					*/
					#endregion
				}

				#region Test
				/*
				object menuFormInstance = Activator.CreateInstance(typeToLoad);

				if (menuFormInstance != null)
				{
					BaseForm menuForm = menuFormInstance as BaseForm;

					// NOTICE: POP-UP창도 고려해서 작성해야 한다(팝업인지 여부)
					// by KIMKIWON\xyz37 in 2007년 9월 20일 목요일 오전 10:53
					if (menuForm != null)
					{
						//menuFormInstance.GetType().InvokeMember("MdiParent", BindingFlags.SetProperty, null, menuFormInstance, new object[] { this });
						//menuFormInstance.GetType().InvokeMember("MenuFullPath", BindingFlags.SetProperty, null, menuFormInstance, new object[] { targetNode.FullPath.Replace("\\", " > ") });
						//menuFormInstance.GetType().InvokeMember("Text", BindingFlags.SetProperty, null, menuFormInstance, new object[] { menuView.MenuName });

						//// Standard Toolbar에 권한을 설정한다.
						//menuFormInstance.GetType().InvokeMember("EnableNewButton", BindingFlags.InvokeMethod, null, menuFormInstance, new object[] { false });

						//// BindingFlags.InvokeMethod를 해야 참조 어셈블리도 참조된다.
						//menuFormInstance.GetType().InvokeMember("Show", BindingFlags.InvokeMethod, null, menuFormInstance, new object[] { });
						menuForm.Text = "M01F0010";

						// 해당 폼을 일반 폼으로 Load 한다.
						menuForm.Show();

					}
				}
				*/
				#endregion
			}
			catch (Exception ex)
			{
				exceptionOccured(string.Format("메뉴 폼을 초기화 할 수 없습니다.\r\n{0}", ex.Message));
			}
		}

		void _newDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
		{
			
		}

		void menuForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			//MessageBox.Show(string.Format("{0}\r\nUnload 되었습니다.", AppDomain.CurrentDomain.FriendlyName), "", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private AppDomain _newDomain;

		void newDomain_DomainUnload(object sender, EventArgs e)
		{
			
		}
	}
}
