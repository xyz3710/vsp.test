﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DynamicLoad01
{
	public delegate void ExceptionOccured(string message);

	public partial class TestDriverForm : Form
	{
		public TestDriverForm()
		{
			InitializeComponent();
		}

		private void btnTest_Click(object sender, EventArgs e)
		{
			TestDriver testDriver = new TestDriver();

			testDriver.Run(WriteLog, true);
		}

		private void WriteLog(string message)
		{
			txtLog.Text += string.Format("{0}{1}{1}", message, Environment.NewLine);
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btnCallCommon_Click(object sender, EventArgs e)
		{
			TestDriver testDriver = new TestDriver();

			testDriver.Run(WriteLog, false);
		}
	}
}