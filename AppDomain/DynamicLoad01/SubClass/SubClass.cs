﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Test
{
	public class SubClass : SuperClass
	{
		/// <summary>
		/// Class1 class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public SubClass()
		{
			Console.WriteLine("Sub class was created. {0}", Assembly.GetExecutingAssembly().GetName().Version.ToString());
			Console.WriteLine("FriendlyName : {0}", AppDomain.CurrentDomain.FriendlyName);
		}

		public SubClass(string param)
			: this()
		{
			Console.WriteLine("Param : {0}", param);
		}
	}
}
