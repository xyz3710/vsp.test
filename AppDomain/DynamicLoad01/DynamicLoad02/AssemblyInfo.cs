﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynamicLoad02
{
	/// <summary>
	/// Summary description for AssemblyInfo.
	/// </summary>
	[Serializable]
	public class AssemblyInfo
	{
		public string Name = "";
		public string FullName = "";
		public string Location = "";
		public string CodeBase = "";
		public string Version = "";
	}

}
