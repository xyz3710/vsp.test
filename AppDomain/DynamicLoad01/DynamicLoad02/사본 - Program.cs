﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Policy;
using System.Reflection;
using System.Runtime.Remoting;

namespace DynamicLoad02
{
	class Program
	{	
		static void Main(string[] args)
		{
			string assemblyName = "Test";
			string assemblyFileName = string.Format(@"{0}\{1}\", AppDomain.CurrentDomain.BaseDirectory, assemblyName);

			Evidence evidence = new Evidence(AppDomain.CurrentDomain.Evidence);
			AppDomainSetup appDomainSetup = new AppDomainSetup();

			appDomainSetup.ApplicationName = assemblyName;
			appDomainSetup.ApplicationBase = AppDomain.CurrentDomain.BaseDirectory;
			//appDomainSetup.PrivateBinPath = ".";
			appDomainSetup.CachePath = string.Format(@"{0}\Cache\", AppDomain.CurrentDomain.BaseDirectory);
			appDomainSetup.ShadowCopyDirectories = string.Format(@"{0}\", AppDomain.CurrentDomain.BaseDirectory);
			appDomainSetup.ShadowCopyFiles = "True";

			AppDomain testDomain = AppDomain.CreateDomain(assemblyName, evidence, appDomainSetup);

			ObjectHandle objectHandle = Activator.CreateInstance(testDomain, assemblyName, "Test.Class1");
			//object obj = TestDomain.CreateInstanceAndUnwrap(assemblyName, "Test.Class1");
			//AppDomain testDomain = AppDomain.CreateDomain("TestDomain", evidence, AppDomain.CurrentDomain.BaseDirectory, System.IO.Path.GetFullPath(assemblyFileName), true);
			Assembly loadedAssembly = testDomain.Load(assemblyName);
						
			testDomain.AssemblyLoad += new AssemblyLoadEventHandler(TestDomain_AssemblyLoad);
			testDomain.DomainUnload += new EventHandler(TestDomain_DomainUnload);

			Console.WriteLine(loadedAssembly.FullName);

			loadedAssembly.CreateInstance("Test.Class1");

			AppDomain.Unload(testDomain);
		}

		static void TestDomain_DomainUnload(object sender, EventArgs e)
		{
			Console.WriteLine("Unload domain : {0}", AppDomain.CurrentDomain.FriendlyName);
		}

		static void TestDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
		{
			Console.WriteLine("Load : {0}", args.LoadedAssembly.FullName);
		}
	}
}
