﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Policy;
using System.Reflection;
using System.Runtime.Remoting;

namespace DynamicLoad02
{
	class Program
	{
		static void Main(string[] args)
		{
			#region For Test
			/*
			AppDomainManager manager = new AppDomainManager(
				AppDomain.CurrentDomain.BaseDirectory, 
				string.Format(@"{0}\ShadowCache\", AppDomain.CurrentDomain.BaseDirectory));

			manager.AddAssembly(string.Format(@"{0}\ShadowCache\{1}", AppDomain.CurrentDomain.BaseDirectory, "SubClass.dll"));

			object obj = manager.GetObjectByName("SubClass", "Test.SubClass");
			manager.Unload();
			*/
			#endregion

			AppDomainSetup setup1 = new AppDomainSetup();
			string appDomainName = "test";

			setup1.ApplicationName = appDomainName;
			setup1.ShadowCopyDirectories = string.Format(@"{0}\ShadowCache\", AppDomain.CurrentDomain.BaseDirectory);
			//setup1.ApplicationBase = setup1.ShadowCopyDirectories;
			setup1.ShadowCopyFiles = "true";
			setup1.CachePath = "ShadowCache";
			setup1.PrivateBinPath = "ShadowCache";
            AppDomain newDomain = AppDomain.CreateDomain(appDomainName, null, setup1);

			newDomain.AssemblyLoad += new AssemblyLoadEventHandler(newDomain_AssemblyLoad);

			Assembly asm1 = newDomain.Load("SubClass");
			object obj1 =  asm1.CreateInstance("Test.SubClass");

			Console.ReadLine();

			AppDomain.Unload(newDomain);


			AppDomainSetup setup2 = new AppDomainSetup();

			setup2.ApplicationName = appDomainName;
			setup2.ShadowCopyDirectories = string.Format(@"{0}\ShadowCache\", AppDomain.CurrentDomain.BaseDirectory);
			setup2.ApplicationBase = setup2.ShadowCopyDirectories;
			setup2.ShadowCopyFiles = "true";
			setup2.CachePath = "ShadowCache";
			setup2.PrivateBinPath = "ShadowCache";

			AppDomain newDomain2 = AppDomain.CreateDomain(appDomainName, null, setup1);

			Assembly asm2 = newDomain2.Load("SubClass");
			object obj2 =  asm2.CreateInstance("Test.SubClass");

			Console.ReadLine();
		}

		static void newDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
		{
			Console.WriteLine("Loaded : {0}", args.LoadedAssembly.FullName);
			Console.WriteLine("CodeBase : {0}", args.LoadedAssembly.CodeBase);
		}
	}
}
