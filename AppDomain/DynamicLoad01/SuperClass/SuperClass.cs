﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Test
{
	public class SuperClass : MarshalByRefObject
	{
		/// <summary>
		/// Class1 class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public SuperClass()
		{
			Console.WriteLine("Super class was created. {0}", Assembly.GetExecutingAssembly().GetName().Version.ToString());
			Console.WriteLine("FriendlyName : {0}", AppDomain.CurrentDomain.FriendlyName);
		}     
   
		public void Show()
		{
			Console.WriteLine("Show method called.");
		}
	}
}
