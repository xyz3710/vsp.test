﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Policy;
using System.Reflection;
using System.Runtime.Remoting;

namespace DynamicLoad02
{
	class Program2
	{
		static void Main2(string[] args)
		{
			#region For Test
			/*
			AppDomainManager manager = new AppDomainManager(
				AppDomain.CurrentDomain.BaseDirectory, 
				string.Format(@"{0}\ShadowCache\", AppDomain.CurrentDomain.BaseDirectory));

			manager.AddAssembly(string.Format(@"{0}\ShadowCache\{1}", AppDomain.CurrentDomain.BaseDirectory, "SubClass.dll"));

			object obj = manager.GetObjectByName("SubClass", "Test.SubClass");
			manager.Unload();
			*/
			#endregion

			AppDomainSetup setup1 = new AppDomainSetup();
			string appDomainName = "Test Domain";
			string cachFolder = "ShadowCache";

			setup1.ApplicationName = appDomainName;
			setup1.ApplicationBase = AppDomain.CurrentDomain.BaseDirectory;
			//setup1.PrivateBinPath = cachFolder;
			//setup1.CachePath = string.Format(@"{0}{1}\", AppDomain.CurrentDomain.BaseDirectory, cachFolder);
			setup1.ShadowCopyFiles = "true";
			setup1.ShadowCopyDirectories = string.Format(@"{0}{1}\", AppDomain.CurrentDomain.BaseDirectory, cachFolder);
			AppDomain newDomain = AppDomain.CreateDomain(appDomainName, null, setup1);

			newDomain.AssemblyLoad += new AssemblyLoadEventHandler(newDomain_AssemblyLoad);
			newDomain.DomainUnload += new EventHandler(newDomain_DomainUnload);
			ObjectHandle oHandle = newDomain.CreateInstance("SubClass", "Test.SubClass");

			//object obj = Activator.Cr("SubClass", "Test.SubClass");
			//Assembly asm1 = newDomain.Load("SubClass");
			//object obj1 =  asm1.CreateInstance("Test.SubClass");

			AppDomain.Unload(newDomain);

			Console.ReadLine();

			AppDomainSetup setup2 = new AppDomainSetup();

			setup2.ApplicationName = appDomainName;
			setup2.ShadowCopyDirectories = string.Format(@"{0}\ShadowCache\", AppDomain.CurrentDomain.BaseDirectory);
			setup2.ApplicationBase = setup2.ShadowCopyDirectories;
			setup2.ShadowCopyFiles = "true";
			setup2.CachePath = "ShadowCache";
			setup2.PrivateBinPath = "ShadowCache";

			AppDomain newDomain2 = AppDomain.CreateDomain(appDomainName + "@", null, setup1);

			newDomain2.AssemblyLoad += new AssemblyLoadEventHandler(newDomain_AssemblyLoad);
			newDomain2.DomainUnload += new EventHandler(newDomain_DomainUnload);

			Assembly asm2 = newDomain2.Load("SubClass");
			object obj2 =  asm2.CreateInstance("Test.SubClass");

			AppDomain.Unload(newDomain2);
			Console.ReadLine();
		}

		static void newDomain_DomainUnload(object sender, EventArgs e)
		{
			Console.WriteLine("{0} domain unload.", AppDomain.CurrentDomain.FriendlyName);
		}

		static void newDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
		{
			Console.WriteLine("Loaded : {0}", args.LoadedAssembly.FullName);
			Console.WriteLine("CodeBase : {0}", args.LoadedAssembly.CodeBase);
		}
	}
}
