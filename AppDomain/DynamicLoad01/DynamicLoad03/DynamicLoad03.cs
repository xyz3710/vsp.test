﻿/**********************************************************************************************************************/
/*	Domain		:	AppDomainTest.DynamicLoad03
/*	Creator		:	KIMKIWON\xyz37
/*	Create		:	2008년 9월 5일 금요일 오전 9:10
/*	Purpose		:	
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*	Comment		:	MarshalByRefObject
 *						SuperClass				<= [Serializable] 사용하지 않는다.
 *							SubClass			<= [Serializable] 사용하지 않는다.
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Policy;
using System.Reflection;
using System.Runtime.Remoting;
using Test;
using System.Diagnostics;

namespace AppDomainTest
{
	class DynamicLoad03
	{
		static void Main(string[] args)
		{
			//FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(@"D:\VSP\_Test\_AppDomain\DynamicLoad01\_Result\Shadow\SubClass.dll");
			AppDomain.CurrentDomain.AssemblyLoad += new AssemblyLoadEventHandler(CurrentDomain_AssemblyLoad);
			
			AppDomainSetup setup1 = new AppDomainSetup();
			string appDomainName = "Test Domain";
			string shadowFolder = "Shadow";

			setup1.ApplicationName = appDomainName;
			setup1.ApplicationBase = AppDomain.CurrentDomain.BaseDirectory;
			setup1.PrivateBinPath = shadowFolder;
			setup1.CachePath = string.Format(@"{0}{1}\{2}\", AppDomain.CurrentDomain.BaseDirectory, shadowFolder, "Cache");
			setup1.ShadowCopyFiles = "true";
			//setup1.ShadowCopyDirectories = string.Format(@"{0}{1}\", AppDomain.CurrentDomain.BaseDirectory, shadowFolder);

			// *************************************************************************************************************
			// domain 0
			// *************************************************************************************************************
			AppDomain newDomain0 = AppDomain.CreateDomain(appDomainName + "0", null, setup1);
			object loaderObject = newDomain0.CreateInstanceAndUnwrap("DynamicLoader", 
                                      "AppDomainTest.DynamicLoader", 
                                      false, 
                                      BindingFlags.Default,
                                      null, 
                                      new object[] {@"D:\VSP\_Test\_AppDomain\DynamicLoad01\_Result\Shadow\SubClass.dll"}, 
                                      null, 
                                      null, 
                                      null);

			if (loaderObject != null && loaderObject.ToString() == "AppDomainTest.DynamicLoader")
			{
				Console.WriteLine("어셈블리 정보를 구했습니다. Unload 후 계속하시려면 Enter를 누르세요.");
				Console.ReadLine();

				AppDomain.Unload(newDomain0);
			}

			// *************************************************************************************************************
			// domain 1
			// *************************************************************************************************************
			AppDomain newDomain1 = AppDomain.CreateDomain(appDomainName + "1", null, setup1);

			newDomain1.AssemblyLoad += new AssemblyLoadEventHandler(newDomain_AssemblyLoad);
			newDomain1.DomainUnload += new EventHandler(newDomain_DomainUnload);

			object oHandle = newDomain1.CreateInstanceAndUnwrap("SubClass", 
                                 "Test.SubClass", 
                                 false,
								 BindingFlags.Default,
                                 null, 
                                 new object[]{"test param1"}, 
                                 null, 
                                 null, 
                                 null);

			//Assembly asm1 = newDomain.Load("SubClass");		
			//object obj1 =  asm1.CreateInstance("Test.SubClass");

			Console.WriteLine("Unload하려면 Enter를 누르세요.");
			Console.ReadLine();
			
			AppDomain.Unload(newDomain1);

			Console.WriteLine("다른 버전을 로드하려면 Enter를 누르세요.");
			Console.ReadLine();

			// *************************************************************************************************************
			// domain 2
			// *************************************************************************************************************
			AppDomain newDomain2 = AppDomain.CreateDomain(appDomainName + "2", null, setup1);

			newDomain2.AssemblyLoad += new AssemblyLoadEventHandler(newDomain_AssemblyLoad);
			newDomain2.DomainUnload += new EventHandler(newDomain_DomainUnload);
						
			//Assembly asm2 = newDomain2.Load("SubClass");
			//object obj2 =  asm2.CreateInstance("Test.SubClass");

			BindingFlags bindingFlags = BindingFlags.CreateInstance | BindingFlags.Instance | BindingFlags.Public;

			SuperClass superClass = newDomain2.CreateInstanceAndUnwrap("SubClass", 
                                        "Test.SubClass",
										true,
										bindingFlags, 
                                        null, 
                                        new object[] { "test param2" }, 
                                        null, 
                                        null,
										null) as SuperClass;

			if (superClass != null)
				superClass.Show();

			AppDomain.Unload(newDomain2);
			Console.ReadLine();
		}

		static void CurrentDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
		{
			Console.WriteLine("***********************************************************");
			Console.WriteLine(args.LoadedAssembly.FullName);
			Console.WriteLine("***********************************************************");
		}

		static void newDomain_DomainUnload(object sender, EventArgs e)
		{
			AppDomain appDomain = sender as AppDomain;

			if (appDomain != null)
            	Console.WriteLine("{0} domain unload.", appDomain.FriendlyName);
		}

		static void newDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
		{
			Console.WriteLine("Loaded : {0}", args.LoadedAssembly.FullName);
			Console.WriteLine("CodeBase : {0}", args.LoadedAssembly.CodeBase);
		}
	}
}
