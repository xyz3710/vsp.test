﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace AppDomainTest
{
	class ShadowCopyEmulate
	{
		static void Main(string[] args)
		{
			#region For Test
			/*
			AppDomainSetup newDomainInfo = new AppDomainSetup();
			string shadowFolderName = string.Format(@"Win\{0}", menuView.FormPath);
			string remoteBase = "http://192.168.25.28:8088/iDASiT.Framework.Win.Deploy";
			string applicationBase = FrameworkSetup.GetInstance().InstallFolder;
			string fullName = GetAssemblyFullName(menuView);

			newDomainInfo.ApplicationName = menuView.FormId;
			newDomainInfo.ApplicationBase = LoaderSetting.IsDeveloper == false ? remoteBase : applicationBase;
			// NOTICE: WorkingFolder이 \\을 포함하면 GetDirectoryName으로 해야 한다.
			// by KIMKIWON\xyz37 in 2008년 9월 9일 화요일 오전 11:53
			newDomainInfo.PrivateBinPath = string.Format(@"{0};{1}",
											   shadowFolderName,
											   Path.GetFileName(FrameworkSetup.GetInstance().WorkingFolder));			// 폴더명만
			newDomainInfo.CachePath = string.Format(@"{0}\iDASiT Cache\{1}\",
										  Path.GetTempPath(),
										  shadowFolderName);															// \을 포함한 full path
			newDomainInfo.ShadowCopyFiles = "true";
			newDomainInfo.ShadowCopyDirectories = string.Format(@"{0}\{1}\", applicationBase, shadowFolderName);		// \을 포함한 full path;

			newDomain = AppDomain.CreateDomain(menuView.FormId, AppDomain.CurrentDomain.Evidence, newDomainInfo);
			*/
			#endregion

			string assemblyPath = "F0990.dll";
			string assemblyName = Path.GetFileNameWithoutExtension(assemblyPath);
			FileInfo fileInfo = new FileInfo(assemblyPath);
			string cachFolder = string.Format(@"{0}FX.Win Cache\{1}\{2:X}", Path.GetTempPath(), assemblyName, fileInfo.LastWriteTime.ToFileTimeUtc());
			string targetFile = string.Format(@"{0}\{1}", cachFolder, Path.GetFileName(assemblyPath));

			if (Directory.Exists(cachFolder) == false)
				Directory.CreateDirectory(cachFolder);

			if (File.Exists(targetFile) == false)
				File.Copy(assemblyPath, targetFile);

			Assembly assembly = Assembly.LoadFile(targetFile);
			string fullName = string.Empty;

			try
			{
				foreach (Type type in assembly.GetTypes())
				{
					if (type.Name == assembly.GetName().Name)
					{
						fullName = type.FullName;

						break;
					}
				}
			}
			catch (ReflectionTypeLoadException ex)
			{
				string exMsg = string.Format("요청한 형식 중 하나 이상을 로드할 수 없습니다.{0}{0}", Environment.NewLine);

				return;
			}

			if (fullName == string.Empty)
			{
				const string msg = "메뉴 어셈블리 파일의 네임스페이스가 틀리거나 정상적으로 생성된 파일이 아닙니다";

				return;
			}

			Type typeToLoad = assembly.GetType(fullName);

			if (typeToLoad == null)
			{
				return;
			}

			object menuFormInstance = null;

			menuFormInstance = Activator.CreateInstance(typeToLoad);

			if (menuFormInstance != null)
			{
				;
			}
			#region For Test
			/*
			AppDomain newDomain = AppDomain.CreateDomain(Path.GetFileNameWithoutExtension(assemblyPath), 
                                      AppDomain.CurrentDomain.Evidence, 
                                      @"C:\Program Files\iDASiT",
									  "Framework.Win", 
                                      true);

			Assembly loadedAssembly = newDomain.Load(assemblyName);

			object baseForm = loadedAssembly.CreateInstance(loadedAssembly.GetTypes()[0].FullName, false, BindingFlags.Default, null, null, null, null);

			AppDomain.Unload(newDomain);
			*/
			#endregion
		}
	}
}
