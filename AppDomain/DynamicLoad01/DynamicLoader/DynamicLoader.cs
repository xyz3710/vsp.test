﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace AppDomainTest
{
	public class DynamicLoader : MarshalByRefObject
	{
		/// <summary>
		/// DynamicLoader class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="assemblyFile"></param>
		public DynamicLoader(string assemblyFile)
		{
			GetAssemblyInfo(assemblyFile);
		}

		public void GetAssemblyInfo(string assemblyName)
		{
			Assembly assembly = Assembly.LoadFrom(assemblyName, null);

			#region For Test
			/*
			AppDomainSetup setup1 = new AppDomainSetup();
			string appDomainName = "DynamicLoader Domain";
			string shadowFolder = "Shadow";

			setup1.ApplicationName = appDomainName;
			setup1.ApplicationBase = AppDomain.CurrentDomain.BaseDirectory;
			setup1.PrivateBinPath = shadowFolder;
			setup1.CachePath = string.Format(@"{0}{1}\{2}\", AppDomain.CurrentDomain.BaseDirectory, shadowFolder, "Cache");
			setup1.ShadowCopyFiles = "true";
			setup1.ShadowCopyDirectories = string.Format(@"{0}{1}\", AppDomain.CurrentDomain.BaseDirectory, shadowFolder);

			// *************************************************************************************************************
			// domain 0
			// *************************************************************************************************************
			AppDomain newDomain0 = AppDomain.CreateDomain(appDomainName + "0", null, setup1);
			object dynamicLoader = newDomain0.CreateInstanceAndUnwrap(assemblyName, typeName);

			Assembly assembly = dynamicLoader.GetType().Assembly;
			*/
			#endregion

			if (assembly != null)
			{
				Console.WriteLine("Friendly : {0}", AppDomain.CurrentDomain.FriendlyName);
				Console.WriteLine("FullName : {0}", assembly.FullName);
				Console.WriteLine("Version  : {0}", assembly.GetName().Version);
			}

			//AppDomain.Unload(newDomain0);
		}
	}
}
