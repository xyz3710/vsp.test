﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace AppDomainSnippets
{
	class ADDynamicBase
	{
		// SetDynamicBase.exe
		static void Main(string[] args)
		{
			// Create a new AppDomain.
			AppDomainSetup setup = new AppDomainSetup();
			// Need to set the application name before setting the dynamic base.
			setup.ApplicationName = "MyApplication";
			AppDomain domain = AppDomain.CreateDomain("MyDomain", null, setup);

			// Tell the domain to search for assemblies in subdirectories
			// of DynamicAssemblyDir.
			domain.SetDynamicBase("C:\\DynamicAssemblyDir");

			// Note that the actual dynamic directory has the form
			// <DynamicBase>\<number>\<ApplicationName>, rather than
			// simply <DynamicBase>.
			String dynamicDir = domain.DynamicDirectory;

			// The AssemblyBuilder won't create this directory automatically.
			if (!System.IO.Directory.Exists(dynamicDir))
			{
				System.IO.Directory.CreateDirectory(dynamicDir);
			}

			// Define the dynamic assembly.
			AssemblyName asmName = new AssemblyName();

			asmName.Name = "DynamicHelloWorld";

			AssemblyBuilder asm = AppDomain.CurrentDomain.DefineDynamicAssembly(asmName, AssemblyBuilderAccess.Save, dynamicDir);

			// Define a dynamic module in the assembly.
			ModuleBuilder mod = asm.DefineDynamicModule("DynamicHelloWorld", "DynamicHelloWorld.dll");

			// Define the "HelloWorld" type in the module.
			TypeBuilder typ = mod.DefineType("HelloWorld", TypeAttributes.Public);

			// Define the "SayHello" method.
			MethodBuilder meth = typ.DefineMethod("SayHello", MethodAttributes.Public, null, null);
			ILGenerator il = meth.GetILGenerator();

			il.EmitWriteLine("Hello World!");
			il.Emit(OpCodes.Ret);

			// Complete the HelloWorld type.
			typ.CreateType();

			// Save the assembly to the dynamic assembly directory.
			asm.Save("DynamicHelloWorld.dll");

			// Launch MyExecutable.exe, which will load DynamicHelloWorld.dll.
			domain.ExecuteAssembly("MyExecutable.exe");
		}
	}
}