﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace AppDomainTest
{
	[Serializable]
	public class NewDomain
	{
		public void Load()
		{
			AppDomainSetup setup1 = new AppDomainSetup();
			string appDomainName = "Test Domain";
			string shadowFolder = "Shadow";
			//string baseDirectory = string.Format(@"{0}{1}\", AppDomain.CurrentDomain.BaseDirectory, shadowFolder);
			string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

			setup1.ApplicationName = appDomainName;
			setup1.ApplicationBase = baseDirectory;
			setup1.PrivateBinPath = shadowFolder;
			setup1.CachePath = string.Format(@"{0}{1}\{2}\", baseDirectory, shadowFolder, "Cache");
			setup1.ShadowCopyFiles = "true";
			setup1.ShadowCopyDirectories = string.Format(@"{0}{1}\", baseDirectory, shadowFolder);

			// *************************************************************************************************************
			// domain 1
			// *************************************************************************************************************
			AppDomain newDomain = AppDomain.CreateDomain(appDomainName, null, setup1);

			//newDomain.AssemblyLoad += new AssemblyLoadEventHandler(newDomain_AssemblyLoad);
			//newDomain.DomainUnload += new EventHandler(newDomain_DomainUnload);

			Assembly asm1 = newDomain.Load("SubClass");

			Console.WriteLine("Unload하려면 아무키나 누르세요");
			Console.ReadLine();

			AppDomain.Unload(newDomain);
		}


		void newDomain_DomainUnload(object sender, EventArgs e)
		{
			AppDomain appDomain = sender as AppDomain;

			if (appDomain != null)
				Console.WriteLine("{0} domain unload.", appDomain.FriendlyName);
		}

		void newDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
		{
			Console.WriteLine("Loaded : {0}", args.LoadedAssembly.FullName);
			Console.WriteLine("CodeBase : {0}", args.LoadedAssembly.CodeBase);
		}

	}
}
