using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace DynamicLoad02
{
	public class TestDriver
	{
		public void Run()
		{
			// 로드할 어셈블리의 경로는 menus.config 파일 경로 + Win 으로 지정한다.
			string applicationBase = Application.StartupPath;
			string assemblyName = "M01F9999.dll";
			string assemblyPath = string.Format("{0}\\{1}", applicationBase, assemblyName);

			if (File.Exists(assemblyPath) == false)
			{
				Console.WriteLine("파일이 없습니다.");

				return;
			}

			Assembly assembly = Assembly.LoadFrom(assemblyPath);
			string typeFullName = string.Empty;

			foreach (Type type in assembly.GetTypes())
			{
				if (type.Name == assembly.GetName().Name)
				{
					typeFullName = type.FullName;

					break;
				}
			}

			if (typeFullName == string.Empty)
			{
				Console.WriteLine("메뉴 어셈블리 파일의 네임스페이스가 틀리거나 정상적으로 생성된 파일이 아닙니다", "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);

				return;
			}

			try
			{
				AppDomainSetup newDomainInfo = new AppDomainSetup();

				newDomainInfo.ApplicationBase = applicationBase;
				newDomainInfo.PrivateBinPath = applicationBase;
				newDomainInfo.ShadowCopyFiles = "true";
				newDomainInfo.ShadowCopyDirectories = string.Format("{0}\\Temp", applicationBase);
				newDomainInfo.CachePath = string.Format("{0}\\Cache", applicationBase);

				AppDomain newDomain = AppDomain.CreateDomain(assemblyName, null, newDomainInfo);

				newDomain.CreateInstance(assembly.FullName, typeFullName);
			}
			catch (Exception ex)
			{
				Console.WriteLine("메뉴 폼을 초기화 할 수 없습니다.\r\n{0}", ex.Message);
			}
		}
	}
}
