﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
	class Program
	{
		static void Main(string[] args)
		{
			//Component com = new Component();
			//var catalog = new AggregateCatalog();

			Program program = new Program();

			//program.RunHelloWorld();
			program.Run();
		}

		public void Run()
		{
			var catalog = new AggregateCatalog();

			catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));

			var container = new CompositionContainer(catalog);
			var batch = new CompositionBatch();

			batch.AddPart(this);
			container.Compose(batch);
		}

		public void RunHelloWorld()
		{
			var container = new CompositionContainer();
			var batch = new CompositionBatch();

			batch.AddPart(this);
			container.Compose(batch);

			HelloWorldComponent.Say();
		}

		[Import]
		public IHelloWorld HelloWorldComponent
		{
			get;
			set;
		}
	}

	public class MessageSender
	{
		[Export("MessageSender")]
		public void Say(string message)
		{
			Console.WriteLine(message);
		}
	}

	[Export]
	public class MessageProcess
	{
		[Import("MessageSender")]
		public Action<string> MessageSender
		{
			get;
			set;
		}

		public void Send()
		{
			MessageSender("Call send process in MessageProcess");
		}
	}

	public interface IHelloWorld
	{
		void Say();
	}

	[Export(typeof(IHelloWorld))]
	public class HelloWorld : IHelloWorld
	{
		public void Say()
		{
			Console.WriteLine("Hello MEF");
		}
	}
}
