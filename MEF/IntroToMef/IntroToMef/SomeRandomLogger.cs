﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace IntroToMef
{
	[Export(typeof(ILogger))]
	public class SomeRandomLogger : ILogger
	{
		#region ILogger 멤버

		public void Write(string message)
		{
			Console.WriteLine("this is some random logger.");
		}

		#endregion
	}
}
