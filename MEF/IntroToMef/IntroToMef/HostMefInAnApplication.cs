﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;

namespace IntroToMef
{
	class HostMefInAnApplication
	{
		[Import]
		protected ILogger Logger
		{
			get;
			set;
		}

		internal void Run()
		{
			Compose();
			Logger.Write("My Message");
		}

		private void Compose()
		{
			var container = new CompositionContainer();

			//container.ComposeParts(this, new ConsoleLogger());
			container.ComposeParts(this, new SomeRandomLogger());
		}
	}
}
