﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroToMef_Part2
{
	class Program
	{
		static void Main(string[] args)
		{
			UsingAnAssemblyCatalog();

			Console.Read();
		}

		private static void UsingAnAssemblyCatalog()
		{
			var ex = new UsingAnAssemblyCatalog();

			ex.Run();
		}
	}
}
