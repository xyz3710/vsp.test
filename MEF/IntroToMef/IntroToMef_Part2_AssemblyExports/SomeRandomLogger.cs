﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroToMef_Part2
{
	[Export(typeof(ILogger))]
	class SomeRandomLogger : ILogger
	{
		#region ILogger 멤버

		public void Log(string message)
		{
			Console.WriteLine("From some random logger: {0}", message);
		}

		#endregion
	}
}
