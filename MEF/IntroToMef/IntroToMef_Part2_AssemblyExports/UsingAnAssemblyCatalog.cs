﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading;
namespace IntroToMef_Part2
{
	class UsingAnAssemblyCatalog
	{
		[ImportMany]
		public IEnumerable<ILogger> Loggers
		{
			get;
			set;
		}

		public void Run()
		{
			Compose();

			foreach (ILogger logger in Loggers)
			{
				logger.Log(string.Format("Hi: {0}", DateTime.Now));

				Thread.Sleep(10);
			}
		}

		private void Compose()
		{
			var assemblyCatalog = new AssemblyCatalog(GetType().Assembly);
			var container = new CompositionContainer(assemblyCatalog);

			container.ComposeParts(this);
		}
	}
}
