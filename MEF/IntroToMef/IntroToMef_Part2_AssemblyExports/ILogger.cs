﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IntroToMef_Part2
{
	public interface ILogger
	{
		void Log(string message);
	}
}
