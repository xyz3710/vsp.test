﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroToMef_Part3
{
	public class PropertyExports
	{
		/// <summary>
		/// Timeout를 구하거나 설정합니다.
		/// </summary>
		/// <value>Timeout를 반환합니다.</value>
		[Import("Timeout")]
		public int Timeout
		{
			get;
			set;
		}
			
		public void Run()
		{
			Compose();
			Console.WriteLine("The imported Timeout value is: {0}", Timeout);
		}

		private void Compose()
		{
			var container = new CompositionContainer(new AssemblyCatalog(GetType().Assembly));

			container.ComposeParts(this);
		}
	}
}
