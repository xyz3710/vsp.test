﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroToMef_Part3
{
	class Program
	{
		static void Main(string[] args)
		{
			PropertyExports propertyExports = new PropertyExports();

			propertyExports.Run();
		}
	}
}
