﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace IntroToMef_Part3
{
	class ConfigurationSettings
	{
		/// <summary>
		/// Timeout를 구하거나 설정합니다.
		/// </summary>
		/// <value>Timeout를 반환합니다.</value>
		[Export("Timeout")]
		public int Timeout
		{
			get
			{
				return int.Parse(ConfigurationManager.AppSettings["Timeout"]);
			}
		}
		
	}
}
