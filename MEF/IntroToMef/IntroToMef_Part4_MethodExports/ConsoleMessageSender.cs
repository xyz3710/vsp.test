﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroToMef_MethodExports
{
	public class ConsoleMessageSender
	{
		//[Export(typeof(Action<string>))]		// 아래와 동일하다
		[Export]
		public void Send(string message)
		{
			Console.WriteLine("ConsoleMessageSender: {0}", message);
		}

	}
}
