﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroToMef_MethodExports
{
	class Program
	{
		static void Main(string[] args)
		{
			MethodExports methodExports = new MethodExports();

			methodExports.Run();
		}
	}
}
