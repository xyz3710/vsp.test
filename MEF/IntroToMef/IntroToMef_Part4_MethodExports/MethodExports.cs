﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroToMef_MethodExports
{
	class MethodExports
	{
		//[Import(typeof(Action<string>))]		// 아래와 동일하다
		[Import]
		public Action<string> MessageSender
		{
			get;
			set;
		}

		public void Run()
		{
			Compose();

			MessageSender("Message being sent from MethodExports class.");
		}

		private void Compose()
		{
			var container = new CompositionContainer();

			container.ComposeParts(this, new ConsoleMessageSender());
		}
	}
}
