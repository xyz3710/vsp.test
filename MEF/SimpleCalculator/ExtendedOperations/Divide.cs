﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtendedOperations
{
	[Export(typeof(SimpleCalculator.IOperation))]
	[ExportMetadata("Symbol", '/')]
	public class Divide : SimpleCalculator.IOperation
	{
		#region IOperation 멤버

		public int Operate(int left, int right)
		{
			if (right == 0)
			{
				right = 1;
			}

			return left / right;
		}

		#endregion
	}
}
