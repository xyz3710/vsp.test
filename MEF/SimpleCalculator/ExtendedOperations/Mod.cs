﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtendedOperations
{
	[Export(typeof(SimpleCalculator.IOperation))]
	[ExportMetadata("Symbol", '%')]
	public class Mod : SimpleCalculator.IOperation
	{

		#region IOperation 멤버

		public int Operate(int left, int right)
		{
			return left % right;
		}

		#endregion
	}
}
