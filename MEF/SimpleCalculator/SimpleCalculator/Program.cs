﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCalculator
{
	class Program
	{
		private CompositionContainer _container;

		[Import(typeof(ICalculator))]
		public ICalculator Calculator
		{
			get;
			set;
		}

		static void Main(string[] args)
		{
			Program p = new Program();
			string cmd = string.Empty;

			Console.WriteLine("Enter command: ");

			while (true)
			{
				cmd = Console.ReadLine();

				Console.WriteLine(p.Calculator.Calculate(cmd));
			}
		}

		private Program()
		{
			var catalog = new AggregateCatalog();

			catalog.Catalogs.Add(new AssemblyCatalog(typeof(Program).Assembly));
			catalog.Catalogs.Add(new DirectoryCatalog(@".\Extensions"));

			_container = new CompositionContainer(catalog);

			try
			{
				_container.ComposeParts(this);
			}
			catch (CompositionException ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}
	}
}
