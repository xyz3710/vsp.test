﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCalculator
{
	[Export(typeof(ICalculator))]
	public class MySimpleCalculator : ICalculator
	{
		[ImportMany]
		private IEnumerable<Lazy<IOperation, IOperationData>> Operations
		{
			get;
			set;
		}

		#region ICalculator 멤버

		public string Calculate(string input)
		{
			int left = 0;
			int right = 0;
			char operation = char.MinValue;

			int fn = FindFirstNonDigit(input);

			if (fn < 0)
			{
				return "Could not parse command.";
			}

			try
			{
				left = int.Parse(input.Substring(0, fn));
				right = int.Parse(input.Substring(fn + 1));
			}
			catch (Exception ex)
			{
				return string.Format("Could not parse command.\r\n{0}", ex.Message);
			}

			operation = input[fn];

			foreach (Lazy<IOperation, IOperationData> item in Operations)
			{
				if (item.Metadata.Symbol.Equals(operation))
				{
					return item.Value.Operate(left, right).ToString();
				}
			}

			return "Operation not found!";
		}

		private int FindFirstNonDigit(string input)
		{
			for (int i = 0; i < input.Length; i++)
			{
				if (char.IsDigit(input[i]) == false)
				{
					return i;
				}
			}

			return -1;
		}

		#endregion
	}
}
