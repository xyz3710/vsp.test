﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MSMQCommon
{
	[Serializable]
	public class Payment
	{
		public string Payor { get; set; }
		public string Name { get; set; }
		public int Amount { get; set; }

		public DateTime TimeStamp { get; set; }

		public bool Transaction { get; set; }

		#region ToString
		/// <summary>
		/// Payment를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			var prefix = Transaction == true ? "Tx" : "__";

			return $"{TimeStamp} [{prefix}] Payor:{Payor}, Name:{Name}, Amount:{Amount}";
		}
		#endregion
	}
}
