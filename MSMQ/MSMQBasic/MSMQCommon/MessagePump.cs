﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MSMQCommon
{
	/// <summary>
	/// Provides the capability to asynchronously check the Peek in the <seealso cref="System.Messaging.MessageQueue"/> and remove it from the <seealso cref="System.Messaging.MessageQueue"/> when finished.
	/// </summary>
	public class MessagePump : IDisposable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="MessagePump" /> class.
		/// </summary>
		/// <param name="messageQueue">The message queue.</param>
		/// <param name="peekAction">The peek action.</param>
		/// <param name="exceptionAction">The exception action.</param>
		/// <exception cref="ArgumentException">
		/// MessagePump: messageQueue of type MessageQueue must not be null.
		/// or
		/// MessagePump: peekAction of type Func must not be null.
		/// </exception>
		public MessagePump(MessageQueue messageQueue,
			Func<MessageQueue, Message, bool> peekAction,
			Action<Exception> exceptionAction = null)
		{
			MessageQueue = messageQueue ?? throw new ArgumentException("MessagePump: messageQueue of type MessageQueue must not be null.");
			PeekAction = peekAction ?? throw new ArgumentException("MessagePump: peekAction of type Func must not be null.");

			messageQueue.PeekCompleted += (sender, e) =>
			{
				var message = e.Message;
				var peekActionResult = false;

				try
				{
					peekActionResult = peekAction(messageQueue, message);

					if (peekActionResult == true)
					{
						messageQueue.EndPeek(e.AsyncResult);
						messageQueue.Receive();
					}
				}
				catch (Exception ex)
				{
					exceptionAction?.Invoke(ex);
				}
				finally
				{
					if (peekActionResult == true)
					{
						messageQueue.BeginPeek();
					}
				}
			};
		}

		/// <summary>
		/// Gets the message queue.
		/// </summary>
		public MessageQueue MessageQueue { get; }

		/// <summary>
		/// Gets the peek action.
		/// </summary>
		public Func<MessageQueue, Message, bool> PeekAction { get; }

		/// <summary>
		/// Gets or sets the exception action.
		/// </summary>
		public Action<Exception> ExceptionAction { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance is started.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is started; otherwise, <c>false</c>.
		/// </value>
		public bool Started { get; private set; }

		/// <summary>
		/// 관리되지 않는 리소스의 확보, 해제 또는 다시 설정과 관련된 응용 프로그램 정의 작업을 수행합니다.
		/// </summary>
		public void Dispose()
		{
			MessageQueue.Close();
			MessageQueue.Dispose();
		}

		/// <summary>
		/// Starts this instance that has no time-out.
		/// </summary>
		public void Start()
		{
			if (Started == false)
			{
				MessageQueue.BeginPeek();
				Started = true;
			}
		}

		/// <summary>
		/// Starts the specified timeout.
		/// </summary>
		/// <param name="timeout">The timeout.</param>
		public void Start(TimeSpan timeout)
		{
			if (Started == false)
			{
				MessageQueue.BeginPeek(timeout);
				Started = true;
			}
		}

		/// <summary>
		/// Stops this instance.
		/// </summary>
		public void Stop()
		{
			if (Started == true)
			{
				MessageQueue.Close();
				Started = false;
			}
		}
	}
}
