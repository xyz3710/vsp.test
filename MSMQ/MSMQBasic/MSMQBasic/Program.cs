﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using MSMQCommon;

namespace MSMQBasic
{
	class Program
	{
		const string QUEUE_NAME = @".\Private$\Test";
		static MessageQueue queue;

		static void Main(string[] args)
		{
			//StartQueueBasic();
			StartWithMessagePump();
		}
		private static void StartWithMessagePump()
		{
			var queue1 = new MessageQueue(QUEUE_NAME + "1");
			var queue2 = new MessageQueue(QUEUE_NAME + "2");

			CheckQueue(queue1, QUEUE_NAME + "1");
			CheckQueue(queue2, QUEUE_NAME + "2");

			queue1.Formatter = new BinaryMessageFormatter();
			queue2.Formatter = new BinaryMessageFormatter();

			Func<MessageQueue, Message, bool> peekAction = (mq, message) =>
			{
				if (message.Body is Payment payment)
				{
					Console.WriteLine($"Receiving: {mq.QueueName}, ID:{message.Id}, Label:{message.Label}, Body:{payment}");

					return true;
				}
				else
				{
					Console.WriteLine("Invalid message.");

					return false;
				}
			};
			Action<Exception> exceptionAction = ex =>
			{
				Console.WriteLine($"Error: {ex.Message}");
			};
			var pump1 = new MessagePump(queue1, peekAction, exceptionAction);
			var pump2 = new MessagePump(queue2, peekAction, exceptionAction);

			pump1.Start();
			pump2.Start();

			Console.WriteLine("Press 'q' to quit.");
			Console.WriteLine("Listening...");
			Console.WriteLine("'1' to start1 peek.");
			Console.WriteLine("'2' to stop 1 peek.");
			Console.WriteLine("'3' to start2 peek.");
			Console.WriteLine("'4' to stop 2 peek.");

			while (true)
			{
				var readKey = Console.ReadKey();

				switch (readKey.Key)
				{
					case ConsoleKey.Q:

						return;
					case ConsoleKey.D1:
						pump1.Start();
						Console.WriteLine("\tQueue1 start peek.");

						break;
					case ConsoleKey.D2:
						pump1.Stop();
						Console.WriteLine("\tQueue1 closed.");

						break;
					case ConsoleKey.D3:
						pump2.Start();
						Console.WriteLine("\tQueue2 start peek.");

						break;
					case ConsoleKey.D4:
						pump2.Stop();
						Console.WriteLine("\tQueue2 closed.");

						break;
				}
			}
		}

		private static void CheckQueue(MessageQueue queue, string queueName)
		{
			if (MessageQueue.Exists(queueName) == false)
			{
				MessageQueue.Create(queueName, true);
			}
		}

		private static void StartQueueBasic()
		{
			Console.WriteLine("Press 'q' to quit.");
			Console.WriteLine("Listening...");
			Console.WriteLine("'c' to stop peek.");
			Console.WriteLine("'s' to start peek.");

			queue = new MessageQueue(QUEUE_NAME)
			{
				Formatter = new BinaryMessageFormatter()
			};
			queue.PeekCompleted += Queue_PeekCompleted;
			queue.BeginPeek();

			var key = new ConsoleKeyInfo('q', ConsoleKey.Q, false, false, false);
			var isPeeked = true;

			while (true)
			{
				var readKey = Console.ReadKey();

				if (readKey == key)
				{
					return;
				}

				if (readKey.Key == ConsoleKey.C)
				{
					queue.Close();
					Console.WriteLine("\tQueue closed.");
					isPeeked = false;
				}
				else if (readKey.Key == ConsoleKey.S && isPeeked == false)
				{
					queue.BeginPeek();
					Console.WriteLine("\tQueue started peek.");
					isPeeked = true;
				}
			}
		}

		private static void Queue_PeekCompleted(object sender, PeekCompletedEventArgs e)
		{
			var message = e.Message;

			try
			{
				if (message.Body is Payment payment)
				{
					Console.WriteLine($"Receiving: ID:{message.Id}, Label:{message.Label}, Body:{payment}");
				}
				else
				{
					Console.WriteLine("Invalid message.");
				}

				queue.EndPeek(e.AsyncResult);
				queue.Receive();
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Error: {ex.Message}");
			}
			finally
			{
				queue.BeginPeek();
			}
		}
	}
}
