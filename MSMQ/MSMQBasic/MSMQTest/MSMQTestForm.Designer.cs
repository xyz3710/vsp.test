﻿namespace MSMQTest
{
	partial class MSMQTestForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.tbPayTo = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbName = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.nudAmount = new System.Windows.Forms.NumericUpDown();
			this.btnSendQueue = new System.Windows.Forms.Button();
			this.btnReceive = new System.Windows.Forms.Button();
			this.cbAutoReceive = new System.Windows.Forms.CheckBox();
			this.tbQueueName = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label4 = new System.Windows.Forms.Label();
			this.cbAutoSend = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.rbBinary = new System.Windows.Forms.RadioButton();
			this.rbXmlFormatter = new System.Windows.Forms.RadioButton();
			this.cbTransaction = new System.Windows.Forms.CheckBox();
			this.btnCreateQueue = new System.Windows.Forms.Button();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.tbSendLog = new System.Windows.Forms.TextBox();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.tbReceiveLog = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.nudAmount)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(17, 40);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(91, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "Queue Path";
			// 
			// tbPayTo
			// 
			this.tbPayTo.Location = new System.Drawing.Point(155, 33);
			this.tbPayTo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.tbPayTo.Name = "tbPayTo";
			this.tbPayTo.Size = new System.Drawing.Size(232, 27);
			this.tbPayTo.TabIndex = 0;
			this.tbPayTo.Text = "xyz3710@gmail.com";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(17, 70);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(83, 20);
			this.label2.TabIndex = 0;
			this.label2.Text = "Your name";
			// 
			// tbName
			// 
			this.tbName.Location = new System.Drawing.Point(155, 65);
			this.tbName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.tbName.Name = "tbName";
			this.tbName.Size = new System.Drawing.Size(232, 27);
			this.tbName.TabIndex = 1;
			this.tbName.Text = "Kim Ki Won";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(17, 96);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 20);
			this.label3.TabIndex = 0;
			this.label3.Text = "Amount";
			// 
			// nudAmount
			// 
			this.nudAmount.Location = new System.Drawing.Point(155, 96);
			this.nudAmount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.nudAmount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.nudAmount.Name = "nudAmount";
			this.nudAmount.Size = new System.Drawing.Size(233, 27);
			this.nudAmount.TabIndex = 2;
			this.nudAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.nudAmount.ThousandsSeparator = true;
			this.nudAmount.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			// 
			// btnSendQueue
			// 
			this.btnSendQueue.Location = new System.Drawing.Point(12, 172);
			this.btnSendQueue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnSendQueue.Name = "btnSendQueue";
			this.btnSendQueue.Size = new System.Drawing.Size(164, 38);
			this.btnSendQueue.TabIndex = 4;
			this.btnSendQueue.Text = "Send Queue";
			this.btnSendQueue.UseVisualStyleBackColor = true;
			// 
			// btnReceive
			// 
			this.btnReceive.Location = new System.Drawing.Point(212, 172);
			this.btnReceive.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnReceive.Name = "btnReceive";
			this.btnReceive.Size = new System.Drawing.Size(175, 38);
			this.btnReceive.TabIndex = 6;
			this.btnReceive.Text = "Receive Queue";
			this.btnReceive.UseVisualStyleBackColor = true;
			// 
			// cbAutoReceive
			// 
			this.cbAutoReceive.AutoSize = true;
			this.cbAutoReceive.Location = new System.Drawing.Point(212, 138);
			this.cbAutoReceive.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.cbAutoReceive.Name = "cbAutoReceive";
			this.cbAutoReceive.Size = new System.Drawing.Size(161, 24);
			this.cbAutoReceive.TabIndex = 5;
			this.cbAutoReceive.Text = "Auto Receive Async";
			this.cbAutoReceive.UseVisualStyleBackColor = true;
			// 
			// tbQueueName
			// 
			this.tbQueueName.Location = new System.Drawing.Point(166, 37);
			this.tbQueueName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.tbQueueName.Name = "tbQueueName";
			this.tbQueueName.Size = new System.Drawing.Size(221, 27);
			this.tbQueueName.TabIndex = 0;
			this.tbQueueName.Text = ".\\Private$\\Test";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.tbPayTo);
			this.groupBox1.Controls.Add(this.cbAutoSend);
			this.groupBox1.Controls.Add(this.cbAutoReceive);
			this.groupBox1.Controls.Add(this.btnReceive);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.btnSendQueue);
			this.groupBox1.Controls.Add(this.tbName);
			this.groupBox1.Controls.Add(this.nudAmount);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Location = new System.Drawing.Point(413, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(394, 220);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "MSMQ Consume";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(17, 36);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(55, 20);
			this.label4.TabIndex = 0;
			this.label4.Text = "Pay To";
			// 
			// cbAutoSend
			// 
			this.cbAutoSend.AutoSize = true;
			this.cbAutoSend.Location = new System.Drawing.Point(12, 138);
			this.cbAutoSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.cbAutoSend.Name = "cbAutoSend";
			this.cbAutoSend.Size = new System.Drawing.Size(175, 24);
			this.cbAutoSend.TabIndex = 3;
			this.cbAutoSend.Text = "Auto Send per 500ms";
			this.cbAutoSend.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Controls.Add(this.groupBox3);
			this.groupBox2.Controls.Add(this.cbTransaction);
			this.groupBox2.Controls.Add(this.tbQueueName);
			this.groupBox2.Controls.Add(this.btnCreateQueue);
			this.groupBox2.Location = new System.Drawing.Point(13, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(394, 220);
			this.groupBox2.TabIndex = 6;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "About MSMQ configuration";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.rbBinary);
			this.groupBox3.Controls.Add(this.rbXmlFormatter);
			this.groupBox3.Location = new System.Drawing.Point(9, 157);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(375, 51);
			this.groupBox3.TabIndex = 4;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Message Formatter";
			// 
			// rbBinary
			// 
			this.rbBinary.AutoSize = true;
			this.rbBinary.Location = new System.Drawing.Point(9, 21);
			this.rbBinary.Name = "rbBinary";
			this.rbBinary.Size = new System.Drawing.Size(139, 24);
			this.rbBinary.TabIndex = 0;
			this.rbBinary.TabStop = true;
			this.rbBinary.Text = "Binary Formatter";
			this.rbBinary.UseVisualStyleBackColor = true;
			// 
			// rbXmlFormatter
			// 
			this.rbXmlFormatter.AutoSize = true;
			this.rbXmlFormatter.Location = new System.Drawing.Point(214, 21);
			this.rbXmlFormatter.Name = "rbXmlFormatter";
			this.rbXmlFormatter.Size = new System.Drawing.Size(118, 24);
			this.rbXmlFormatter.TabIndex = 1;
			this.rbXmlFormatter.TabStop = true;
			this.rbXmlFormatter.Text = "XmlFormatter";
			this.rbXmlFormatter.UseVisualStyleBackColor = true;
			// 
			// cbTransaction
			// 
			this.cbTransaction.AutoSize = true;
			this.cbTransaction.Checked = true;
			this.cbTransaction.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbTransaction.Location = new System.Drawing.Point(9, 75);
			this.cbTransaction.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.cbTransaction.Name = "cbTransaction";
			this.cbTransaction.Size = new System.Drawing.Size(169, 24);
			this.cbTransaction.TabIndex = 1;
			this.cbTransaction.Text = "Transactional Queue";
			this.cbTransaction.UseVisualStyleBackColor = true;
			// 
			// btnCreateQueue
			// 
			this.btnCreateQueue.Location = new System.Drawing.Point(9, 107);
			this.btnCreateQueue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btnCreateQueue.Name = "btnCreateQueue";
			this.btnCreateQueue.Size = new System.Drawing.Size(378, 38);
			this.btnCreateQueue.TabIndex = 2;
			this.btnCreateQueue.Text = "Check Queue name and &Create";
			this.btnCreateQueue.UseVisualStyleBackColor = true;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.tbSendLog);
			this.groupBox4.Location = new System.Drawing.Point(13, 234);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(794, 300);
			this.groupBox4.TabIndex = 7;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Send Log";
			// 
			// tbSendLog
			// 
			this.tbSendLog.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbSendLog.Location = new System.Drawing.Point(3, 23);
			this.tbSendLog.Multiline = true;
			this.tbSendLog.Name = "tbSendLog";
			this.tbSendLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbSendLog.Size = new System.Drawing.Size(788, 274);
			this.tbSendLog.TabIndex = 0;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.tbReceiveLog);
			this.groupBox5.Location = new System.Drawing.Point(13, 532);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(794, 300);
			this.groupBox5.TabIndex = 7;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Receive Log";
			// 
			// tbReceiveLog
			// 
			this.tbReceiveLog.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tbReceiveLog.Location = new System.Drawing.Point(3, 23);
			this.tbReceiveLog.Multiline = true;
			this.tbReceiveLog.Name = "tbReceiveLog";
			this.tbReceiveLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbReceiveLog.Size = new System.Drawing.Size(788, 274);
			this.tbReceiveLog.TabIndex = 0;
			// 
			// MSMQTestForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(826, 841);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Font = new System.Drawing.Font("맑은 고딕", 11F);
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "MSMQTestForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "MSMQTestForm";
			((System.ComponentModel.ISupportInitialize)(this.nudAmount)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbPayTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown nudAmount;
		private System.Windows.Forms.Button btnSendQueue;
		private System.Windows.Forms.Button btnReceive;
		private System.Windows.Forms.CheckBox cbAutoReceive;
		private System.Windows.Forms.TextBox tbQueueName;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button btnCreateQueue;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.RadioButton rbBinary;
		private System.Windows.Forms.RadioButton rbXmlFormatter;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.TextBox tbSendLog;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.TextBox tbReceiveLog;
		private System.Windows.Forms.CheckBox cbTransaction;
		private System.Windows.Forms.CheckBox cbAutoSend;
	}
}