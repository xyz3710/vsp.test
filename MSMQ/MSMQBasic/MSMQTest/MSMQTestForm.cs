﻿// ****************************************************************************************************************** //
//	Domain		:	
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2017년 11월 3일 금요일 14:59
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	MSMQ 설치 방법
//					dism.exe /online /enable-feature /all /featurename:"MSMQ-Server"
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="MSMQTestForm.cs" company="(주)가치소프트">
//		Copyright (c) 2017. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using MSMQCommon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MSMQTest
{
	public partial class MSMQTestForm : Form
	{
		private System.Timers.Timer _autoSendingTimer;

		public MSMQTestForm()
		{
			InitializeComponent();

			BinaryFormatter = new BinaryMessageFormatter();
			XmlFormatter = new XmlMessageFormatter();
		}

		private string QueueName
		{
			get
			{
				return tbQueueName.Text;
			}
		}

		private bool AutoReceiveAsync
		{
			get
			{
				return cbAutoReceive.Checked;
			}
		}

		private bool Transactinoal
		{
			get
			{
				return cbTransaction.Checked;
			}
		}

		private MessageQueue MSMQ { get; set; }
		private MessagePump MessagePump { get; set; }

		private BinaryMessageFormatter BinaryFormatter { get; set; }
		private XmlMessageFormatter XmlFormatter { get; set; }

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);

			MSMQ = new MessageQueue(QueueName)
			{
				Formatter = GetMessageFormatter()
			};

			var queueNameOnly = Path.GetFileName(QueueName);
			var queues = MessageQueue.GetPrivateQueuesByMachine(Environment.MachineName);
			var currentQueue = queues.FirstOrDefault(x => string.Compare(Path.GetFileName(x.QueueName), queueNameOnly, true) == 0);

			if (currentQueue != null)
			{
				cbTransaction.Checked = currentQueue.Transactional;
			}

			cbTransaction.CheckedChanged += CbTransaction_CheckedChanged;
			rbBinary.CheckedChanged += OnMessageFomatter_CheckedChanged;
			rbXmlFormatter.CheckedChanged += OnMessageFomatter_CheckedChanged;
			btnReceive.Click += BtnReceive_Click;
			cbAutoSend.CheckedChanged += CbAutoSend_CheckedChanged;
			btnSendQueue.Click += BtnSendQueue_Click;
			cbAutoReceive.CheckedChanged += CbAutoReceive_CheckedChanged;
			btnCreateQueue.Click += BtnCreateQueue_Click;

			btnCreateQueue.PerformClick();
			rbBinary.PerformClick();
		}

		protected override void OnFormClosed(FormClosedEventArgs e)
		{
			if (MessagePump != null)
			{
				MessagePump.Dispose();
			}

			base.OnFormClosed(e);
		}

		private void CbTransaction_CheckedChanged(object sender, EventArgs e)
		{
			if (MessageBox.Show("현재 큐를 제거 하고 재 생성합니다.\r\n계속 진행하시겠습니까?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
				== DialogResult.No)
			{
				return;
			}

			if (MessageQueue.Exists(QueueName) == true)
			{
				MessageQueue.Delete(QueueName);
			}

			btnCreateQueue.PerformClick();
		}

		private void BtnCreateQueue_Click(object sender, EventArgs e)
		{
			if (MessageQueue.Exists(QueueName) == false)
			{
				MessageQueue.Create(QueueName, Transactinoal);
			}
			else
			{
				MSMQ.QueueName = QueueName.Substring(2);        // .\ 제거
			}
		}

		private void CbAutoSend_CheckedChanged(object sender, EventArgs e)
		{
			if (_autoSendingTimer == null)
			{
				_autoSendingTimer = new System.Timers.Timer(500d);
				_autoSendingTimer.Elapsed += (s, ea) =>
				{
					btnSendQueue.Invoke(new MethodInvoker(() =>
					{
						btnSendQueue.PerformClick();
					}));
				};
			}

			if (cbAutoSend.Checked == true)
			{
				_autoSendingTimer.Start();
			}
			else
			{
				_autoSendingTimer.Stop();
			}
		}

		private void CbAutoReceive_CheckedChanged(object sender, EventArgs e)
		{
			if (cbAutoReceive.Checked == true)
			{
				MessagePump = new MessagePump(MSMQ, (mq, message) =>
				{
					using (var trans = new MessageQueueTransaction())
					{
						trans.Begin();

						try
						{
							WriteReceiveLog((message.Body as Payment)?.ToString());
							//throw new Exception("test");
							trans.Commit();

							return true;
						}
						catch (Exception ex)
						{
							WriteReceiveLog(ex.Message);
							trans.Abort();

							return false;
						}
					}
				});
				MessagePump.Start();
			}
			else
			{
				MessagePump.Stop();
			}
			//var receivedMessage = await Task.Factory.FromAsync<System.Messaging.Message>(MSMQ.BeginReceive(), MSMQ.EndReceive);

			//MessageBox.Show((receivedMessage.Body as Payment)?.ToString());
		}

		private void OnMessageFomatter_CheckedChanged(object sender, EventArgs e)
		{
			if (MSMQ != null)
			{
				MSMQ.Formatter = GetMessageFormatter();
			}
		}

		private IMessageFormatter GetMessageFormatter()
		{
			if (rbBinary.Checked == true)
			{
				return BinaryFormatter;
			}
			else
			{
				return XmlFormatter;
			}
		}

		private void BtnSendQueue_Click(object sender, EventArgs e)
		{
			var payment = new Payment
			{
				Payor = tbPayTo.Text,
				Name = tbName.Text,
				Amount = (int)nudAmount.Value,
				TimeStamp = DateTime.Now,
				Transaction = Transactinoal,
			};
			var message = new System.Messaging.Message(payment, GetMessageFormatter());

			if (Transactinoal == true)
			{
				using (var trans = new MessageQueueTransaction())
				{
					trans.Begin();

					try
					{
						MSMQ.Send(message, $"Payment: {payment.Name}", trans);
						trans.Commit();
						WriteSendLog(payment.ToString());
					}
					catch (Exception ex)
					{
						WriteSendLog(ex.Message);
						trans.Abort();
					}
				}
			}
			else
			{
				try
				{
					MSMQ.Send(message, $"Payment: {payment.Name}");
					WriteSendLog(payment.ToString());
				}
				catch (Exception ex)
				{
					WriteSendLog(ex.Message);
				}
			}
		}

		private void BtnReceive_Click(object sender, EventArgs e)
		{
			Task.Run(() =>
			{
				var body = string.Empty;

				if (Transactinoal == true)
				{
					using (var trans = new MessageQueueTransaction())
					{
						trans.Begin();

						try
						{
							body = (MSMQ.Receive(trans).Body as Payment)?.ToString();
							trans.Commit();
						}
						catch (Exception ex)
						{
							WriteReceiveLog(ex.Message);
							trans.Abort();
						}
					}
				}
				else
				{
					body = (MSMQ.Receive().Body as Payment)?.ToString();
				}

				WriteReceiveLog(body ?? String.Empty);
			});
		}

		private void WriteSendLog(string message)
		{
			tbSendLog.Invoke(new MethodInvoker(() =>
			{
				tbSendLog.AppendText($"{message}\r\n");
			}));
		}

		private void WriteReceiveLog(string message)
		{
			tbSendLog.Invoke(new MethodInvoker(() =>
			{
				tbReceiveLog.AppendText($"{message}\r\n");
			}));
		}
	}
}
