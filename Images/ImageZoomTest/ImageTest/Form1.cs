﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageProcessor;
using ImageProcessor.Imaging.Formats;

namespace ImageTest
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);
			btnZoom1.PerformClick();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			var file = @"D:\R-PassImages\20150305\91503-0519-1031.jpg";

			if (pbImage.Image != null)
			{
				pbImage.Image.Dispose();
				pbImage.Image = null;
			}

			pbImage.Image = new Bitmap(file);
		}

		private void btnZoom1_Click(object sender, EventArgs e)
		{
			var file = @"D:\R-PassImages\20150305\91503-0519-1031.jpg";
			var image = new Bitmap(file);
			var pixelFormat = System.Drawing.Imaging.PixelFormat.Format24bppRgb;

			if (image.PixelFormat == PixelFormat.Indexed || image.PixelFormat == PixelFormat.Format8bppIndexed)
			{
				image = ConvertTo24bpp(image);
			}

			double zoomFactor = (double)50 / (double)100;
			int newWidth = Convert.ToInt32(image.Width * zoomFactor);
			int newHeight = Convert.ToInt32(image.Height * zoomFactor);
			var originalRec = new Rectangle(0, 0, newWidth, newHeight);

			Bitmap resizeImage = new Bitmap(newWidth, newHeight, pixelFormat);
			using (Graphics g = Graphics.FromImage(resizeImage))
			{
				//var zoomArea = new Rectangle(500, 1584, 3900, 3120);
				//var zoomArea = new Rectangle(newWidth, newHeight, image.Width, image.Height);
				//var zoomArea = new Rectangle(2192, 1644, image.Width, image.Height);
				var zoomArea = new Rectangle(1096, 822, 2192 * 3, 1644 * 3);
				g.DrawImage(image, originalRec, zoomArea, GraphicsUnit.Pixel);
			}
						
			image.Dispose();
			image = null;

			if (pbImage.Image != null)
			{
				pbImage.Image.Dispose();
				pbImage.Image = null;
			}

			pbImage.Image = resizeImage;
			Text = string.Format("Width: {0}, Height: {1}", pbImage.Image.Width, pbImage.Image.Height);
		}

		public Bitmap ConvertTo24bpp(Image image)
		{
			var bitmap = new Bitmap(image.Width, image.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

			using (var g = Graphics.FromImage(bitmap))
			{
				g.DrawImage(image, new Rectangle(Point.Empty, image.Size));
			}

			return bitmap;
		}

		private Image ConvertPixelFormat(Image image, PixelFormat pixelFormat = System.Drawing.Imaging.PixelFormat.Format24bppRgb)
		{
			if (image == null)
			{
				throw new ArgumentNullException("image");
			}

			Bitmap converted = new Bitmap(image.Width, image.Height, pixelFormat);
			using (Graphics g = Graphics.FromImage(converted))
			{
				g.DrawImage(image, new Rectangle(Point.Empty, image.Size));
			}

			image = new Bitmap(converted);
			converted.Dispose();
			converted = null;

			return image;
		}
		
	}
}
