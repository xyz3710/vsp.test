﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using iDASiT.TrustCore.Security;

namespace DataSet_List
{
	class Program
	{
		const int REPEAT = 10000;

		static void Main(string[] args)
		{
			Console.WriteLine("DataTable과 List collection이 초기화 및 검색 속도 비교 입니다.");
			Console.WriteLine("아무키나 누르면 시작합니다.");
			Console.ReadKey();

			DataTable dataTable = InitDataTable();
			List<Person> persons = InitList();
			List<MenuView> menuViews = InitList2();

			DateTime start = DateTime.Now;
			string name = "name" + (REPEAT - 1);
			int age = REPEAT - 1;

            SelectDataTable(dataTable, name, age);
			SelectList(persons, name, age);
			SelectList2(menuViews, name, age);
		}

		private static void SelectDataTable(DataTable dataTable, string name, int age)
		{
			DateTime start = DateTime.Now;
			
			DataRow[] rows = dataTable.Select(string.Format("name = '{0}' AND age = '{1}'", name, age));

			Console.WriteLine("\r\nDataTable");
			Console.WriteLine("Name : {0}", Convert.ToString(rows[0]["Name"]));
			Console.WriteLine("Age  : {0}", Convert.ToString(rows[0]["Age"]));
			Console.WriteLine("Sex  : {0}", Convert.ToString(rows[0]["Sex"]));
			Console.WriteLine("Addr : {0}", Convert.ToString(rows[0]["Addr"]));
			Console.WriteLine("SelectDataTable\t: {0}", DateTime.Now - start);
		}

		private static void SelectList(List<Person> persons, string name, int age)
		{
			DateTime start = DateTime.Now;

			Person person = persons.Find(delegate(Person p)
			{
				if (p.Name == name && p.Age == age)
					return true;

				return false;
			});

			Console.WriteLine("\r\nList");
			Console.WriteLine("Name : {0}", person.Name);
			Console.WriteLine("Age  : {0}", person.Age);
			Console.WriteLine("Sex  : {0}", person.Sex);
			Console.WriteLine("Addr : {0}", person.Addr);
			Console.WriteLine("SelectList\t: {0}", DateTime.Now - start);
		}

		private static void SelectList2(List<MenuView> menuViews, string name, int seq)
		{
			DateTime start = DateTime.Now;

			List<MenuView> mViews = menuViews.FindAll(delegate(MenuView p)
			{
				if (p.MenuName == name && p.Seq == seq)
					return true;

				return false;
			});

			Console.WriteLine("\r\nList2");

			foreach (MenuView menuView in mViews)
			{
				Console.WriteLine("Name : {0}", menuView.MenuName);
				Console.WriteLine("Age  : {0}", menuView.Seq);
				Console.WriteLine("Sex  : {0}", menuView.IsContainer);
				Console.WriteLine("Addr : {0}", menuView.MenuId);
			}

			Console.WriteLine("SelectList\t: {0}", DateTime.Now - start);
		}
		private static DataTable InitDataTable()
		{
			DateTime start = DateTime.Now;
			DataTable dataTable = new DataTable("Test");

			dataTable.Columns.Add("Name", typeof(string));
			dataTable.Columns.Add("Age", typeof(int));
			dataTable.Columns.Add("Sex", typeof(bool));
			dataTable.Columns.Add("Addr", typeof(string));

			for (int i = 1; i <= REPEAT; i++)
				dataTable.Rows.Add("name" + i.ToString(), i, i%2 == 0 ? true : false, DateTime.Now.ToLongTimeString());

			Console.WriteLine("InitDataTable\t: {0}", DateTime.Now - start);

			return dataTable;
		}

		private static List<Person> InitList()
		{
			DateTime start = DateTime.Now;
			List<Person> persons = new List<Person>();

			for (int i = 1; i <= REPEAT; i++)
				persons.Add(new Person("name" + i.ToString(), i, i%2 == 0 ? true : false, DateTime.Now.ToLongTimeString()));

			Console.WriteLine("InitList\t: {0}", DateTime.Now - start);

			return persons;
		}

		private static List<MenuView> InitList2()
		{
			DateTime start = DateTime.Now;
			List<MenuView> menuViews = new List<MenuView>();

			for (int i = 1; i <= REPEAT; i++)
			{
				MenuView menuView = new MenuView();

				menuView.MenuName = "name" + i.ToString();
				menuView.Seq = i;
				menuView.IsContainer = i%2 == 0 ? true : false;
				menuView.MenuId = DateTime.Now.ToLongTimeString();
				
				menuViews.Add(menuView);
			}

			Console.WriteLine("InitList2\t: {0}", DateTime.Now - start);

			return menuViews;
		}
	}

	class Person
	{
		private string _name;
		private int _age;
		private bool _sex;
		private string _addr;

		/// <summary>
		/// Person class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Person()
		{
		}

		/// <summary>
		/// Person class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="age"></param>
		/// <param name="sex"></param>
		/// <param name="addr"></param>
		public Person(string name, int age, bool sex, string addr)
		{
			_name = name;
			_age = age;
			_sex = sex;
			_addr = addr;
		}

		#region Properties
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}
        
        /// <summary>
        /// Age를 구하거나 설정합니다.
        /// </summary>
        public int Age
        {
        	get
        	{
        		return _age;
        	}
        	set
        	{
        		_age = value;
        	}
        }
        
        /// <summary>
        /// Sex를 구하거나 설정합니다.
        /// </summary>
        public bool Sex
        {
        	get
        	{
        		return _sex;
        	}
        	set
        	{
        		_sex = value;
        	}
        }
        
        /// <summary>
        /// Addr를 구하거나 설정합니다.
        /// </summary>
		public string Addr
        {
        	get
        	{
        		return _addr;
        	}
        	set
        	{
        		_addr = value;
        	}
        }
        
        #endregion
        
	}
}
