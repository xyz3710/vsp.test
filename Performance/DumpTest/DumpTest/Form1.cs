﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DumpTest
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();

			_buffer = new List<byte[]>();

			var timer = Utilities.ShowMemories(tbMemory);

			timer.Start();
		}

		private void btnDivideByZero_Click(object sender, EventArgs e)
		{
			DivideByZeroAction();
		}

		public static void DivideByZeroAction()
		{
			var zero = 0;
			var test = 10 / zero;
		}

		private static List<byte[]> _buffer;

		private void btnMeroryLeak_Click(object sender, EventArgs e)
		{
			byte[] newBuf = new byte[1024 * 1024];

			_buffer.Add(newBuf);
			lblSize.Text = string.Format("{0:#,##0} KB", newBuf.Length * _buffer.Count);
		}
	}
}
