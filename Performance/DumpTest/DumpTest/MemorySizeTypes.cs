﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Diagnostics
{
	public enum MemorySizeTypes
	{
		/// <summary>
		/// Byte
		/// </summary>
		Byte = 0,
		/// <summary>
		/// Kilo Byte
		/// </summary>
		KiloByte,
		/// <summary>
		/// Mega Byte
		/// </summary>
		MegaByte,
		/// <summary>
		/// Giga Byte
		/// </summary>
		GigaByte,
		/// <summary>
		/// Tera Byte
		/// </summary>
		TeraByte,
		/// <summary>
		/// B
		/// </summary>
		ByteShort = 11,
		/// <summary>
		/// KB
		/// </summary>
		KiloByteShort,
		/// <summary>
		/// MB
		/// </summary>
		MegaByteShort,
		/// <summary>
		/// GB
		/// </summary>
		GigaByteShort,
		/// <summary>
		/// TB
		/// </summary>
		TeraByteShort,
	}
}
