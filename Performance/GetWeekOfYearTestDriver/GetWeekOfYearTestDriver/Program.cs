﻿#define PERFORMANCE_TEST
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace GetWeekOfYearTestDriver
{
	class Program
	{
		static void Main(string[] args)
		{
			const int TEST_COUNT = 100;
			#region PERFORMANCE TEST START 90523
#if PERFORMANCE_TEST
			DateTime performanceTestStart90523 = DateTime.Now;
			Console.WriteLine("GetWeekOfYear");
			Console.WriteLine("Start\t{0}", performanceTestStart90523);
#endif
			#endregion
			for (int j = 0; j < TEST_COUNT; j++)
			{
				DateTime start = new DateTime(2000 + j, 1, 1);

				for (int i = 0; i <= 365; i++)
				{
					DateTime date = start.AddDays(i);
					int week = date.GetWeekOfYear();
					//Console.WriteLine("{0} : {1}", today.ToShortDateString(), week);
				}
			}
			#region PERFORMANCE TEST STOP 90523
#if PERFORMANCE_TEST
			Console.WriteLine("Stop \t{0}", DateTime.Now - performanceTestStart90523);
#endif
			#endregion

			Console.WriteLine();

			#region PERFORMANCE TEST START 90633
#if PERFORMANCE_TEST
			DateTime performanceTestStart90633 = DateTime.Now;
			Console.WriteLine("GetWeekOfYear2");
			Console.WriteLine("Start\t{0}", performanceTestStart90633);
#endif
			#endregion
			for (int j = 0; j < TEST_COUNT; j++)
			{
				DateTime start = new DateTime(2000 + j, 1, 1);

				for (int i = 0; i <= 365; i++)
				{
					DateTime date = start.AddDays(i);
					int week = date.GetWeekOfYear2();
					//Console.WriteLine("{0} : {1}", today.ToShortDateString(), week);
				}
			}
			#region PERFORMANCE TEST STOP 90633
#if PERFORMANCE_TEST
			Console.WriteLine("Stop \t{0}", DateTime.Now - performanceTestStart90633);
#endif
			#endregion
		}
	}
}

namespace System
{
	/// <summary>
	/// 날짜형 적용되는 확장 기능을 지원합니다.
	/// </summary>
	public static class DateTimeExtensions
	{
		/// <summary>
		/// 해당 월의 1일의 날짜 개체를 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static DateTime GetFirstDayOfMonth(this DateTime value)
		{
			return new DateTime(value.Year, value.Month, 1);
		}

		/// <summary>
		/// 해당 년도의 1월 1일의 날짜 개체를 구합니다.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static DateTime GetFirstDayOfYear(this DateTime value)
		{
			return new DateTime(value.Year, 1, 1);
		}

		/// <summary>
		/// 해당일자가 1월 1일로 부터 몇번째 주인지를 구합니다.
		/// </summary>
		/// <param name="value">몇째주인지를 구하고자 하는 일자입니다.</param>
		/// <returns></returns>
		public static int GetWeekOfYear(this DateTime value)
		{
			// 1. 1월 1일의 요일값
			// 2. 현재까지의 일수 / 7
			// 3. 2번의 몫
			// 4. 2번의 나머지
			// 5. revision : 
			// 5-1. +0 : 1번과 4번의 값이 0인 경우
			// 5-2. +1 : 현재 요일의 값이 1번 보다 크거나 같은 경우, 4번의 나머지 값이 0인 경우
			// 5-3. +2 : 현재 요일의 값이 1번 보다 작은 경우
			// result = share + revision
			int firstDayOfYearWeek = (int)value.GetFirstDayOfYear().DayOfWeek;
			int todayWeek = (int)value.DayOfWeek;
			int share = value.DayOfYear / 7;
			int rest = value.DayOfYear % 7;
			int revision = 0;

			if (rest == 0 && firstDayOfYearWeek == 0)
				revision = 0;
			else if (todayWeek >= firstDayOfYearWeek || rest == 0)
				revision = 1;
			else
				revision = 2;

			return share + revision;
		}

		/// <summary>
		/// 해당일자가 1월 1일로 부터 몇번째 주인지를 구합니다.
		/// </summary>
		/// <param name="value">몇째주인지를 구하고자 하는 일자입니다.</param>
		/// <returns></returns>
		public static int GetWeekOfYear2(this DateTime value)
		{
			GregorianCalendar gregorianCalendar = new GregorianCalendar();

			return gregorianCalendar.GetWeekOfYear(value, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
		}

	}
}
