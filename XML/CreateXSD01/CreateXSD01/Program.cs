﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateXSD01
{
	/// <summary>
	/// XSD 유틸리티를 사용하면 Schema 파일을 생성 할 수 있다
	/// C:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin\Xsd.exe
	/// </summary>
	public class Yield
	{
		public string ProductId;
		public DateTime ProdDate;
		public string OperationId;
		public double OutResult;
		public double Loss;

		public Yield()
		{
		}
	}
}
