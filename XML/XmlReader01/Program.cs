﻿#define PERFORMANCE_TEST

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace XmlReader01
{
	class Program
	{
		private class DockSettingNode
		{
			#region Constructors
			/// <summary>
			/// ConfigNode class의 새 인스턴스를 초기화 합니다.
			/// </summary>
			public DockSettingNode(string text)
			{
				Text = text;
			}

			/// <summary>
			/// ConfigNode class의 새 인스턴스를 초기화 합니다.
			/// </summary>
			public DockSettingNode()
			{
				
			}
			#endregion

			#region Properties
			/// <summary>
			/// Settings를 구하거나 설정합니다.
			/// </summary>
			public string Settings
			{
				get;
				set;
			}

			/// <summary>
			/// Text를 구하거나 설정합니다.
			/// </summary>
			public string Text
			{
				get;
				set;
			}

			/// <summary>
			/// ToolTipCaption를 구하거나 설정합니다.
			/// </summary>
			public string ToolTipCaption
			{
				get;
				set;
			}
						
			/// <summary>
			/// Key를 구하거나 설정합니다.
			/// </summary>
			public string Key
			{
				get;
				set;
			}

			/// <summary>
			/// ControlName를 구하거나 설정합니다.
			/// </summary>
			public string ControlName
			{
				get;
				set;
			}

			/// <summary>
			/// Pinned를 구하거나 설정합니다.
			/// </summary>
			public string Pinned
			{
				get;
				set;
			}

			/// <summary>
			/// ControlPaneIndex를 구하거나 설정합니다.
			/// </summary>
			public string ControlPaneIndex
			{
				get;
				set;
			}

			#endregion            
		}

		static void Main(string[] args)
		{
			string filePath = @"DockManager.configU";

			//XmlTextReader xmlTextReader = new XmlTextReader(filePath);

			XmlDocument doc = new XmlDocument();

			try
			{
				doc.Load(filePath);
			}
			catch
			{
			}

			XmlNodeList nodes = doc.GetElementsByTagName("a1:DockableControlPane");
			List<DockSettingNode> items = new List<DockSettingNode>();

			foreach (XmlNode node in nodes)
			{
				DockSettingNode dockSettingNode = new DockSettingNode()
				{
					Settings = node["Settings"].Attributes["href"].Value,
					Text = node["Text"].InnerText,
					ToolTipCaption = node["ToolTipCaption"].InnerText,
					Key = node["Key"].InnerText,
					ControlName = node["ControlName"].InnerText,
					ControlPaneIndex = node["ControlPaneIndex"].InnerText,
				};

				try
				{
					dockSettingNode.Pinned = node["Pinned"].InnerText;
				}
				catch 
				{
					dockSettingNode.Pinned = bool.FalseString;
				}

				items.Add(dockSettingNode);
			}

			var qItems = nodes.Cast<XmlNode>().AsQueryable().Select(node => new DockSettingNode()
			{
				Settings = node["Settings"].Attributes["href"].Value,
				Text = node["Text"].InnerText,
				ToolTipCaption = node["ToolTipCaption"].InnerText,
				Key = node["Key"].InnerText,
				ControlName = node["ControlName"].InnerText,				
               	Pinned = node["Pinned"].InnerText == string.Empty ? bool.FalseString : node["Pinned"].InnerText,
				ControlPaneIndex = node["ControlPaneIndex"].InnerText,
			});

			var qItems2 = nodes.Cast<XmlNode>().AsQueryable().Select(delegate(XmlNode node)
			{
				return node["Key"].InnerText;
			});
			List<string> keys = new List<string>(qItems2.AsEnumerable());
			
			//qItems.Contains<ConfigNode>()
			//{
			//	return;
			//};
			
			foreach (XmlNode xmlNode in nodes)
				System.Diagnostics.Debug.WriteLine(xmlNode["Key"].InnerText);

			string[] fruits = { "apple", "banana", "mango", "orange", "passionfruit", "grape" };
			DockSettingNode mango = new DockSettingNode("mango");
			List<DockSettingNode> list = new List<DockSettingNode>();

			foreach (string fruit in fruits)
				list.Add(new DockSettingNode(fruit));

			var qqItems = list.AsQueryable().Select(item => new DockSettingNode("mango"));

			foreach (DockSettingNode node in qqItems)
				Console.WriteLine(node.Text);
			
			Console.WriteLine("\tafter");
			foreach(DockSettingNode configNode in list.FindAll(delegate(DockSettingNode configNodeItem)
                        {
            				return configNodeItem.Text == "mango";
                        }))
            {
				Console.WriteLine(configNode.Text);
            }
		}
	}
}
