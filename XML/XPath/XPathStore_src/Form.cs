using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml;

namespace XPathXmlStore
{
	/// <summary>
	/// User interface for testing XPathStore
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox XPath;
		private System.Windows.Forms.TextBox data;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnRead;
		private System.Windows.Forms.Button btnWrite;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox edtName;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox edtApp;
		private string sPrevAppText;
		private string sPrevNameText;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();

			sPrevAppText = edtApp.Text;
			sPrevNameText = edtName.Text;

			try
			{
				XPathStore storage = new XPathStore();
				XmlNode node = storage.QueryCreatNode("settings/@SampleInfo");
				node.Value = "Read and write XML data using XPath\r\r\nYou can\r\n*)\tDelete the xml file\r\n*)\tChose XPath";
				storage.SaveSettings();
			}
			catch
			{
				MessageBox.Show("Is XML correct installed ?");
			}

			comboBox1.SelectedIndex = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnRead = new System.Windows.Forms.Button();
			this.btnWrite = new System.Windows.Forms.Button();
			this.XPath = new System.Windows.Forms.TextBox();
			this.data = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.edtName = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.edtApp = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnRead
			// 
			this.btnRead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnRead.Location = new System.Drawing.Point(230, 147);
			this.btnRead.Name = "btnRead";
			this.btnRead.Size = new System.Drawing.Size(90, 25);
			this.btnRead.TabIndex = 4;
			this.btnRead.Text = "Read XML";
			this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
			// 
			// btnWrite
			// 
			this.btnWrite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnWrite.Location = new System.Drawing.Point(336, 147);
			this.btnWrite.Name = "btnWrite";
			this.btnWrite.Size = new System.Drawing.Size(90, 25);
			this.btnWrite.TabIndex = 5;
			this.btnWrite.Text = "Write XML";
			this.btnWrite.Click += new System.EventHandler(this.btnWrite_Click);
			// 
			// XPath
			// 
			this.XPath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.XPath.Location = new System.Drawing.Point(86, 69);
			this.XPath.Multiline = true;
			this.XPath.Name = "XPath";
			this.XPath.Size = new System.Drawing.Size(617, 61);
			this.XPath.TabIndex = 3;
			this.XPath.TextChanged += new System.EventHandler(this.btnRead_Click);
			// 
			// data
			// 
			this.data.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.data.Location = new System.Drawing.Point(10, 208);
			this.data.Multiline = true;
			this.data.Name = "data";
			this.data.Size = new System.Drawing.Size(693, 103);
			this.data.TabIndex = 6;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(10, 60);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 18);
			this.label1.TabIndex = 6;
			this.label1.Text = "XPath";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.Location = new System.Drawing.Point(10, 182);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(120, 17);
			this.label2.TabIndex = 7;
			this.label2.Text = "Data";
			// 
			// comboBox1
			// 
			this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBox1.Items.AddRange(new object[] {
            "Select here a XPath sample Query",
            "settings app 1",
            "settings app 2",
            "settings app/page 1",
            "settings app/page 2",
            "settings app/page/control 1",
            "settings app/page/control 2",
            "settings app/page/control 3",
            "settings app/page/control 4"});
			this.comboBox1.Location = new System.Drawing.Point(403, 9);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(300, 20);
			this.comboBox1.TabIndex = 0;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboSelectionChanged);
			// 
			// edtName
			// 
			this.edtName.Location = new System.Drawing.Point(86, 34);
			this.edtName.Name = "edtName";
			this.edtName.Size = new System.Drawing.Size(120, 21);
			this.edtName.TabIndex = 2;
			this.edtName.Text = "MyName";
			this.edtName.TextChanged += new System.EventHandler(this.edtName_TextChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(10, 34);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(76, 25);
			this.label3.TabIndex = 10;
			this.label3.Text = "Account";
			// 
			// edtApp
			// 
			this.edtApp.Location = new System.Drawing.Point(86, 9);
			this.edtApp.Name = "edtApp";
			this.edtApp.Size = new System.Drawing.Size(120, 21);
			this.edtApp.TabIndex = 1;
			this.edtApp.Text = "MyApp";
			this.edtApp.TextChanged += new System.EventHandler(this.edtApp_TextChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(10, 9);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(76, 24);
			this.label4.TabIndex = 12;
			this.label4.Text = "Application";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(712, 325);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.edtApp);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.edtName);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.data);
			this.Controls.Add(this.XPath);
			this.Controls.Add(this.btnWrite);
			this.Controls.Add(this.btnRead);
			this.Name = "Form1";
			this.Text = "Writes data with XPath that doens\'t exists yet";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void ComboSelectionChanged(object sender, System.EventArgs e)
		{
			// Only one top level element is allowed in an XML document. 
			switch( comboBox1.SelectedIndex )
			{
				case 0: XPath.Text = "settings/@SampleInfo";	break;
				case 1: XPath.Text = "settings/" + edtApp.Text + "/User[@Name=\"" + edtName.Text + "\"]/@AutoHide";	break;
				case 2: XPath.Text = "settings/" + edtApp.Text + "/User[@Name=\"" + edtName.Text + "\"]/@Email";		break;
				case 3: XPath.Text = "settings/" + edtApp.Text + "/User[@Name=\"" + edtName.Text + "\"]/ReportFilter/Report[@Name=\"Afvoermeldingen per Categorie\"]/@ReloadThisSettings";	break;
				case 4: XPath.Text = "settings/" + edtApp.Text + "/User[@Name=\"" + edtName.Text + "\"]/ReportFilter/Report[@Name=\"Afvoermeldingen per Categorie\"]/@Title";	break;
				case 5: XPath.Text = "settings/" + edtApp.Text + "/User[@Name=\"" + edtName.Text + "\"]/ReportFilter/Report[@Name=\"Afvoermeldingen per Categorie\"]/Filter[@Name=\"Categorie\"]/@Selection";	break;
				case 6: XPath.Text = "settings/" + edtApp.Text + "/User[@Name=\"" + edtName.Text + "\"]/ReportFilter/Report[@Name=\"Afvoermeldingen per Categorie\"]/Filter[@Name=\"Filter2\"]/@Selection";	break;
				case 7: XPath.Text = "settings/" + edtApp.Text + "/User[@Name=\"" + edtName.Text + "\"]/ReportFilter/Report[@Name=\"Report2\"]/Filter[@Name=\"Categorie\"]/@Selection";	break;
				case 8: XPath.Text = "settings/" + edtApp.Text + "/User[@Name=\"" + edtName.Text + "\"]/ReportFilter/Report[@Name=\"Report2\"]/Filter[@Name=\"Filter3\"]/@Selection";	break;
			}	
		}

		private void edtApp_TextChanged(object sender, System.EventArgs e)
		{
			int nLoc = XPath.Text.IndexOf(sPrevAppText);
			if( nLoc!=-1 )
			{
				XPath.Text = XPath.Text.Substring(0, nLoc) + edtApp.Text + XPath.Text.Substring( nLoc + sPrevAppText.Length );
				sPrevAppText = edtApp.Text;
				XPath.Invalidate();
			}
		}

		private void edtName_TextChanged(object sender, System.EventArgs e)
		{
			int nLoc = XPath.Text.IndexOf(sPrevNameText);
			if( nLoc!=-1 )
			{
				XPath.Text = XPath.Text.Substring(0, nLoc) + edtName.Text + XPath.Text.Substring( nLoc + sPrevNameText.Length );
				sPrevNameText = edtName.Text;
				XPath.Invalidate();
			}			
		}

		// Read 
		private void btnRead_Click(object sender, System.EventArgs e)
		{
			XPathStore storage = new XPathStore();
			XmlNode node = storage.QueryCreatNode(XPath.Text);
			data.Text = (node==null ? "\r\n#### XPATH RETURNED NULL ####":node.Value);
		}

		private void btnWrite_Click(object sender, System.EventArgs e)
		{
			XPathStore storage = new XPathStore();
			XmlNode node = storage.QueryCreatNode(XPath.Text);
			if(node!=null)
			{
				node.Value = data.Text;
				storage.SaveSettings();
			}
		}
	}
}
