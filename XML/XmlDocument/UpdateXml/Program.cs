﻿/**********************************************************************************************************************/
/*	Domain		:	UpdateXml.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 11월 16일 월요일 오후 3:55
/*	Purpose		:	MPMF0090.csproj에서 내용을 읽은 뒤 SpecificVersion XmlElement를 추가한 뒤 저장 한다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace UpdateXml
{
	class Program
	{
		static void Main(string[] args)
		{
			string fileName = "MPMF0090.csproj";
			XmlDocument doc = new XmlDocument();
			string rootNamespace = string.Empty;

			try
			{
				doc.Load(fileName);
				rootNamespace = doc.GetElementsByTagName("Project")[0].Attributes["xmlns"].Value;
			}
			catch
			{
			}
						
			
			IEnumerable<XmlElement> references = doc.GetElementsByTagName("Reference")
				.Cast<XmlElement>()
				.AsQueryable()
				.Where(delegate(XmlElement node)
				{
					return node.Attributes["Include"].Value.StartsWith("AnyFactory") == true		// Include Attribute에 AnyFactory로 시작하고
							   && node.InnerXml.Contains("SpecificVersion") == false;					// SpecificVersion 값이 없는 Node
				});


			// SpecificVersion을 추가
			foreach (XmlElement node in references)
			{
				XmlElement newElement = doc.CreateElement("SpecificVersion", rootNamespace);		// rootNamespace를 동일하게 해야 xmlns="" 속성이 추가되지 않는다.
				XmlText specificVersionXmlText = doc.CreateTextNode(bool.FalseString);

				newElement.AppendChild(specificVersionXmlText);
				node.AppendChild(newElement);
			}

			doc.Save(fileName);
		}
	}
}
