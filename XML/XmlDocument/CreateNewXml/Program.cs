﻿/**********************************************************************************************************************/
/*	Domain		:	CreateNewXml.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 11월 4일 수요일 오전 8:46
/*	Purpose		:	Create new Xml with XmlDocument
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
<?xml version="1.0" encoding="utf-8"?>
<CategoryList>
  <Category ID="01">
    <MainCategory>XML</MainCategory>
    <Description>This is a list my XML articles.</Description>
    <Active>true</Active>
  </Category>
</CategoryList>
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CreateNewXml
{
	class Program
	{
		static void Main(string[] args)
		{
			XmlDocument xmlDoc = new XmlDocument();

			// Write down the XML declaration
			XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);

			// Create the root element
			XmlElement rootNode  = xmlDoc.CreateElement("CategoryList");
			xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
			xmlDoc.AppendChild(rootNode);

			// Create a new <Category> element and add it to the root node
			XmlElement parentNode  = xmlDoc.CreateElement("Category");

			// Set attribute name and value!
			parentNode.SetAttribute("ID", "01");

			xmlDoc.DocumentElement.AppendChild(parentNode);

			// Create the required nodes
			XmlElement mainNode  = xmlDoc.CreateElement("MainCategory");
			XmlElement descNode  = xmlDoc.CreateElement("Description");
			XmlElement activeNode  = xmlDoc.CreateElement("Active");

			// retrieve the text 
			XmlText categoryText= xmlDoc.CreateTextNode("XML");
			XmlText descText  = xmlDoc.CreateTextNode("This is a list my XML articles.");
			XmlText activeText  = xmlDoc.CreateTextNode("true");

			// append the nodes to the parentNode without the value
			parentNode.AppendChild(mainNode);
			parentNode.AppendChild(descNode);
			parentNode.AppendChild(activeNode);

			// save the value of the fields into the nodes
			mainNode.AppendChild(categoryText);
			descNode.AppendChild(descText);
			activeNode.AppendChild(activeText);

			// Create a new <Category> element and add it to the root node
			XmlElement parentNode2  = xmlDoc.CreateElement("Category");

			// Set attribute name and value!
			parentNode2.SetAttribute("ID", "02");

			xmlDoc.DocumentElement.AppendChild(parentNode2);

			// Create the required nodes
			XmlElement mainNode2  = xmlDoc.CreateElement("MainCategory");
			XmlElement descNode2  = xmlDoc.CreateElement("Description");
			XmlElement activeNode2  = xmlDoc.CreateElement("Active");

			// retrieve the text 
			XmlText categoryText2= xmlDoc.CreateTextNode("XML2");
			XmlText descText2  = xmlDoc.CreateTextNode("This is a list my XML articles2.");
			XmlText activeText2  = xmlDoc.CreateTextNode("false");

			// append the nodes to the parentNode without the value
			parentNode2.AppendChild(mainNode2);
			parentNode2.AppendChild(descNode2);
			parentNode2.AppendChild(activeNode2);

			// save the value of the fields into the nodes
			mainNode2.AppendChild(categoryText2);
			descNode2.AppendChild(descText2);
			activeNode2.AppendChild(activeText2);

			// Save to the XML file
			xmlDoc.Save("categories.xml");

			Console.WriteLine("XML file created");
		}
	}
}
