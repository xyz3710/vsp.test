﻿namespace WindowsApplication1
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnGetColor = new System.Windows.Forms.Button();
			this.txtColor = new System.Windows.Forms.TextBox();
			this.pnlColor = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// btnGetColor
			// 
			this.btnGetColor.Location = new System.Drawing.Point(253, 12);
			this.btnGetColor.Name = "btnGetColor";
			this.btnGetColor.Size = new System.Drawing.Size(98, 23);
			this.btnGetColor.TabIndex = 0;
			this.btnGetColor.Text = "색상 구하기";
			this.btnGetColor.UseVisualStyleBackColor = true;
			this.btnGetColor.Click += new System.EventHandler(this.btnGetColor_Click);
			// 
			// txtColor
			// 
			this.txtColor.Location = new System.Drawing.Point(120, 12);
			this.txtColor.Name = "txtColor";
			this.txtColor.Size = new System.Drawing.Size(118, 21);
			this.txtColor.TabIndex = 1;
			this.txtColor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtColor_KeyDown);
			// 
			// pnlColor
			// 
			this.pnlColor.Location = new System.Drawing.Point(25, 82);
			this.pnlColor.Name = "pnlColor";
			this.pnlColor.Size = new System.Drawing.Size(421, 193);
			this.pnlColor.TabIndex = 2;
			// 
			// Form1
			// 
			this.AcceptButton = this.btnGetColor;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(471, 339);
			this.Controls.Add(this.pnlColor);
			this.Controls.Add(this.txtColor);
			this.Controls.Add(this.btnGetColor);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnGetColor;
		private System.Windows.Forms.TextBox txtColor;
		private System.Windows.Forms.Panel pnlColor;
	}
}

