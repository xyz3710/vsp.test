﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsApplication1
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnGetColor_Click(object sender, EventArgs e)
		{
			GetColor();
		}

		private void GetColor()
		{
			TypeConverter converter = TypeDescriptor.GetConverter(typeof(Color));
			Color color = Color.Empty;

			try
			{
				if (converter.CanConvertTo(typeof(string)) == true)
					color = (Color)converter.ConvertFrom(txtColor.Text);
			}
			catch
			{
				MessageBox.Show("인식할 수 없는 색상 이름 입니다.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);                
			}

			pnlColor.BackColor = color;
		}

		private void txtColor_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
            	GetColor();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			txtColor.Select();
		}
	}
}