﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Drawing;

namespace TypeConverterTester
{
	class Program
	{
		static void Main(string[] args)
		{
			TypeConverter typeConverter = new TypeConverter();
			Type type = Type.GetType("System.Drawing.Color");
			object data = "185, 187, 204";

			//Console.WriteLine("CanConverter : {0}", typeConverter.CanConvertFrom(type));
			Color color = (Color)typeConverter.ConvertTo(data, typeof(Color));

			Console.WriteLine(color);
		}
	}
}
