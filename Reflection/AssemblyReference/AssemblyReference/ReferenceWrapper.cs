﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssemblyReference
{
	class ReferenceWrapper
	{
		/// <summary>
		/// AssemblyName를 구하거나 설정합니다.
		/// </summary>
		public string AssemblyName
		{
			get;
			set;
		}

		/// <summary>
		/// Option 이름을 구하거나 설정합니다.
		/// </summary>
		public List<string> Options
		{
			get;
			set;
		}
	}
}
