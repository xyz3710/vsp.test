﻿/**********************************************************************************************************************/
/*	Domain		:	AssemblyReference.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 11월 16일 월요일 오후 12:17
/*	Purpose		:	Reflection으로 해당 assembly에 참조되는 reference를 구해옵니다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Xml.Linq;
using System.Collections;
using System.IO;
using System.Xml;

namespace AssemblyReference
{
	class Program
	{
		static void Main(string[] args)
		{
			ReferenceNameTest();
		}

		private static void ReferenceNameTest()
		{
			string path = @"C:\Program Files\iSet-DA\Win\M\MPMF0090.dll";
			Assembly assembly = Assembly.LoadFile(path);

			foreach (AssemblyName assemblyName in GetAssemblyNameStartWithNamespace(assembly, "AnyFactory"))
				Console.WriteLine(assemblyName.Name);
		}

		private static IEnumerable<AssemblyName> GetAssemblyNameStartWithNamespace(Assembly assembly, string nameSpace)
		{
			return assembly.GetReferencedAssemblies()
				.Where(asm => asm.Name.StartsWith(nameSpace))
				.OrderByDescending(asm => asm.Name);
		}
	}
}
