﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GetLocalMethodsVariable
{
	public class TestClass
	{
		#region Constructor
		/// <summary>
		/// TestClass 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public TestClass(string name)
		{
			Name = name;
		}
		#endregion
        
		private void TestMethod()
		{
			int a = 10;
			string b = "test";
			Person person = new Person
			{
				Name = Name,
				Age = a + b.Length,
				Male = true,
			};

			Person = person;
			Console.WriteLine("{0}", person);
		}

		private string _name;
		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;

				TestMethod();
			}
		}

		/// <summary>
		/// Person를 구하거나 설정합니다.
		/// </summary>
		public Person Person
		{
			private get;
			set;
		}
	}
}
