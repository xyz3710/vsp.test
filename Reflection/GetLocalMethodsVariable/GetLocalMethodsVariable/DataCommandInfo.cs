﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;

namespace AnyFactory.Win.Mes
{
	[DebuggerDisplay("{MethodName}, {CommandType}, {CommandText}", Name="DataCommandInfo")]
	internal class DataCommandInfo
	{
		#region Fields
		private string _methodName;
		private string _commandType;
		private string _commandText;
		private List<KeyValuePair<string, object>> _inParameters;
		private List<KeyValuePair<string, object>> _outParameters;
		#endregion
		
		#region Constructor
		/// <summary>
		/// DataCommandInfo 클래스의 새 인스턴스를 초기화 합니다.
		/// </summary>
		/// <param name="methodName"></param>
		/// <param name="localVariableInfos"></param>
		public DataCommandInfo(string methodName, IEnumerable<LocalVariableInfo> localVariableInfos)
		{
			System.Diagnostics.Debug.WriteLine(methodName, "DataCommandInfo.DataCommandInfo");
			MethodName = methodName;
			LocalVariableInfos = localVariableInfos;
		}
		#endregion

		#region Properties
		public IEnumerable<LocalVariableInfo> LocalVariableInfos
		{
			get;
			set;
		}

		/// <summary>
		/// MethodName를 구합니다.
		/// </summary>
		public string MethodName
		{
			get
			{
				return _methodName;
			}
			private set
			{
				_methodName = value;
			}
		}

		/// <summary>
		/// CommandType를 구합니다.
		/// </summary>
		public string CommandType
		{
			get
			{
				return _commandType;
			}
			set
			{
				_commandType = value;
			}
		}

		/// <summary>
		/// CommandText를 구합니다.
		/// </summary>
		public string CommandText
		{
			get
			{
				return _commandText;
			}
			set
			{
				_commandText = value;
			}
		}

		/// <summary>
		/// InParameter, Value 쌍를 구합니다.
		/// </summary>
		public List<KeyValuePair<string, object>> InParameters
		{
			get
			{
				return _inParameters;
			}
			set
			{
				_inParameters = value;
			}
		}

		/// <summary>
		/// OutParameter, Value 쌍를 구합니다.
		/// </summary>
		public List<KeyValuePair<string, object>> OutParameters
		{
			get
			{
				return _outParameters;
			}
			set
			{
				_outParameters = value;
			}
		}
		#endregion
		
		#region ToString
		/// <summary>
		/// DataCommandInfo를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return string.Format("MethodName:{0}, CommandType:{1}, CommandText:{2}",
					MethodName, CommandType, CommandText);
		}
		#endregion
	}
}
