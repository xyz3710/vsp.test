using System;
using System.Collections.Generic;
using System.Reflection;

namespace AnyFactory.Win.Mes
{
	public class MethodInfoComparer : IEqualityComparer<MethodInfo>
	{
		#region IEqualityComparer<MethodInfo> 멤버

		public bool Equals(MethodInfo x, MethodInfo y)
		{
			return x.Name == y.Name;
		}

		public int GetHashCode(MethodInfo obj)
		{
			return obj.GetHashCode();
		}

		#endregion
	}
}
