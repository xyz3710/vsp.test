﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace GetLocalMethodsVariable
{
	[DebuggerDisplay("Name:{Name}, Age:{Age}, Male:{Male}", Name="Person")]
	public class Person
	{
		#region Constructors
		/// <summary>
		/// Person class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Person()
			: this(String.Empty)
		{
		}

        /// <summary>
		/// Person class의 새 인스턴스를 초기화 합니다.
		/// </summary>
		public Person(string name)
		{
			Name = name;
		}
		#endregion

		/// <summary>
		/// Name를 구하거나 설정합니다.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Age를 구하거나 설정합니다.
		/// </summary>
		public int Age
		{
			get;
			set;
		}
		
		/// <summary>
		/// Male를 구하거나 설정합니다.
		/// </summary>
		public bool Male
		{
			get;
			set;
		}		
	}
}
