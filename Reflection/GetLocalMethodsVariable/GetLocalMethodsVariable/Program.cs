﻿/**********************************************************************************************************************/
/*	Domain		:	GetLocalMethodsVariable.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2009년 12월 3일 목요일 오후 3:51
/*	Purpose		:	private method의 Local 값 구하는것 테스트 중인데 실패한다.
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using AnyFactory.Win.Mes;
using System.Windows.Forms;
using System.IO;

namespace GetLocalMethodsVariable
{
	class Program
	{
		static void Main(string[] args)
		{
			Type type = GetType(Assembly.GetExecutingAssembly(), "TestClass");

			if (type == null)
				return;

			TestClass testClass = Activator.CreateInstance(type, "Hwang Tae Young") as TestClass;
			MethodInfo mi = type.GetMethod("TestMethod", BindingFlags.Instance | BindingFlags.NonPublic);
			IList<System.Reflection.LocalVariableInfo> localVariables = mi.GetMethodBody().LocalVariables;

			Person person = Activator.CreateInstance(localVariables[2].LocalType, "Kim Ki Won") as Person;

			//object value = localVariables[2].LocalType.InvokeMember(
			//				   "Name",
			//				   BindingFlags.InvokeMethod | BindingFlags.GetField | BindingFlags.GetProperty,
			//				   null,
			//				   testClass.GetType(),
			//				   null);
			
			Type immType = GetType(Assembly.LoadFile(Path.Combine(Application.StartupPath, "IMMF0200.dll")), "IMMF0200");
			GetLocalVariablesValue(immType);
		}

		private static Type GetType(Assembly assembly, string typeName)
		{
			Type[] types = assembly.GetTypes();
			List<Type> typesWhereToList = types.Where(type => type.Name == typeName).ToList();

			return typesWhereToList.Count > 0 ? typesWhereToList[0] : null;
		}

		private static void GetLocalVariablesValue(Type type)
		{			
			List<DataCommandInfo> dataCommandInfos = GetDataCommandInfos(type, "AnyFactory.Common.DataCommandBase");
			List<LocalVariableInfo> locals = dataCommandInfos[0].LocalVariableInfos.ToList();

			foreach (LocalVariableInfo localVariableInfo in locals)
			{
				Type localType = localVariableInfo.LocalType;

				foreach (PropertyInfo pi in localType.GetProperties())
				{
					//string commandText = pi.GetValue();
				}
			}
		}

		private static List<DataCommandInfo> GetDataCommandInfos(Type type, string fullName)
		{
			List<DataCommandInfo> dataCommandInfos = new List<DataCommandInfo>();

			foreach (MethodInfo mi in GetDerivedMethodsOnly(type))
			{
				MethodBody methodBody = mi.GetMethodBody();

				if (methodBody != null)
				{
					IEnumerable<LocalVariableInfo> methodBodyLocalVariables 
						= methodBody.LocalVariables.Where(local => local.LocalType.FullName == fullName);

					if (methodBodyLocalVariables != null && methodBodyLocalVariables.Count() > 0)
						dataCommandInfos.Add(new DataCommandInfo(mi.Name, methodBodyLocalVariables));
				}
			}

			return dataCommandInfos;
		}

		private static IEnumerable<MethodInfo> GetDerivedMethodsOnly(Type type)
		{
			MethodInfo[] derivedMethods = type.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.CreateInstance | BindingFlags.Instance);

			return derivedMethods
				.Where(dmi => (dmi.Name.StartsWith("On") && dmi.Name.EndsWith("Click")) == false
								&& dmi.Name != "Dispose"
								&& dmi.Name.Contains("Initialize") == false);
		}
	}
}
