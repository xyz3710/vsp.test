﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Speech;
using Microsoft.Speech.Synthesis;

namespace TTSTest
{
	class Program
	{
		static void Main(string[] args)
		{
			// System.Speech는 기본 영문만 제공한다.
			/*
			// C# TTS 사용 순서

				1. Microsoft Speech Platform - Runtime 설치 (http://www.microsoft.com/en-us/download/details.aspx?id=27225)
				2. Microsoft Speech Platform - Software Development Kit (SDK) (http://www.microsoft.com/en-us/download/details.aspx?id=27226)
				3. Microsoft Speech Platform - Runtime Languages (http://www.microsoft.com/en-us/download/details.aspx?id=27224)
				   다운로드 후 KR을 검색해서 2개의 언어 파일을 받는다.
			*/
			// 2 SDK 설치 후 C:\Program Files\Microsoft SDKs\Speech\v11.0\Assembly\Microsoft.Speech.dll 를 참조
			// System.Speech.dll을 참조하면 한국어를 인식 못함.

			SpeechSynthesizer ts = new SpeechSynthesizer();

			// 보이스를 선택하지 않아도 처리됨
			//ts.SelectVoice("Microsoft Server Speech Text to Speech Voice (ko-KR, Heami)");
			ts.SetOutputToDefaultAudioDevice();

			string speechString = "안녕하세요 TTS 세계에 오신걸 환영합니다.";

			Console.WriteLine(speechString);

			ts.Speak(speechString);
		}
	}
}
