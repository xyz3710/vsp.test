﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Microsoft.Speech.Synthesis;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;

namespace TTSVolumeUpTest
{
	public partial class Form1 : Form
	{
		private SpeechSynthesizer _speechSynthesizer;

		[DllImport("winmm.dll")]
		public static extern int waveOutGetVolume(IntPtr hwo, out uint dwVolume);

		[DllImport("winmm.dll")]
		public static extern int waveOutSetVolume(IntPtr hwo, uint dwVolume);

		public Form1()
		{
			InitializeComponent();

			_speechSynthesizer = new SpeechSynthesizer();
			_speechSynthesizer.SetOutputToDefaultAudioDevice();
		}

		private void btnTest_Click(object sender, EventArgs e)
		{
			var message = tbText.Text;

			_speechSynthesizer.Speak(message);
		}

		private void btnPromptBuilder_Click(object sender, EventArgs e)
		{
			PromptBuilder pb1 = new PromptBuilder();
			PromptBuilder pb2 = new PromptBuilder();
			PromptBuilder pb3 = new PromptBuilder();
			PromptBuilder pb4 = new PromptBuilder();

			var names = Enum.GetNames(typeof(PromptVolume)).Where(x => x != PromptVolume.NotSet.ToString());

			foreach (string name in names)
			{
				pb1.AppendText(name, (PromptVolume)Enum.Parse(typeof(PromptVolume), name));
				pb1.AppendBreak();
			}

			pb1.AppendText("ExtraLoud", Microsoft.Speech.Synthesis.PromptVolume.ExtraLoud);

			pb2.AppendTextWithAlias("SAPI", "Speech Application Programming Interface");

			pb3.AppendTextWithHint("2/19/2009", SayAs.MonthDayYear);
			pb3.AppendText("February");

			pb3.AppendTextWithHint("21 내동", SayAs.NumberCardinal);
			pb3.AppendTextWithHint("22 관저동", SayAs.NumberCardinal);

			_speechSynthesizer.SpeakAsync(pb1);
			_speechSynthesizer.SpeakAsync(pb2);
			_speechSynthesizer.SpeakAsync(pb3);
			_speechSynthesizer.SpeakAsync(pb4);
		}

		private void btnSource_Click(object sender, EventArgs e)
		{
			//_speechSynthesizer.SetOutputToAudioStream()

			using (SpeechSynthesizer synth = new SpeechSynthesizer())
			{
				MemoryStream streamAudio = new MemoryStream();      // using문으로 streamAudio를 하면 안된다.
																	//System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer();

				// Configure the synthesizer to output to an audio stream.
				synth.SetOutputToWaveStream(streamAudio);

				synth.Speak(tbText.Text);

				streamAudio.Position = 0;
				//soundPlayer.Stream = streamAudio;

				//// https://social.msdn.microsoft.com/Forums/vstudio/en-US/42b46e40-4d4a-48f8-8681-9b0167cfe781/attenuating-soundplayer-volume?forum=csharpgeneral
				//var sg = new SignalGenerator(44100, 1);
				//sg.Frequency = 500;
				//sg.Type = SignalGeneratorType.Sin;
				//var fadeIn = new FadeInOutSampleProvider(sg, true);
				//fadeIn.BeginFadeIn(20000); // fade in over 20 seconds

				WaveOut waveOut = new WaveOut();

				var mix = new MixingSampleProvider(new WaveFormat());
				var volume = new VolumeSampleProvider(mix);
				waveOut.Init(new WaveFileReader(streamAudio));
				waveOut.Volume = 1f;        // 1 : 최대값

				waveOut.Play();

				//waveOut.Dispose();
				//streamAudio.Dispose();		// 리소스가 해제 되면 output이 없어져서 소리가 멈춘다.

				//soundPlayer.Play();				
				synth.SetOutputToNull();        // Set the synthesizer output to null to release the stream. 
			}
		}
	}
}
