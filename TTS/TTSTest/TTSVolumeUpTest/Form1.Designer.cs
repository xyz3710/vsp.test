﻿namespace TTSVolumeUpTest
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbText = new System.Windows.Forms.TextBox();
			this.btnTest = new System.Windows.Forms.Button();
			this.btnSource = new System.Windows.Forms.Button();
			this.btnPromptBuilder = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// tbText
			// 
			this.tbText.Location = new System.Drawing.Point(14, 16);
			this.tbText.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tbText.Name = "tbText";
			this.tbText.Size = new System.Drawing.Size(576, 27);
			this.tbText.TabIndex = 0;
			this.tbText.Text = "안녕하세요 TTS 세계에 오신걸 환영합니다. 123";
			// 
			// btnTest
			// 
			this.btnTest.Location = new System.Drawing.Point(506, 69);
			this.btnTest.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.btnTest.Name = "btnTest";
			this.btnTest.Size = new System.Drawing.Size(84, 31);
			this.btnTest.TabIndex = 1;
			this.btnTest.Text = "테스트(&T)";
			this.btnTest.UseVisualStyleBackColor = true;
			this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
			// 
			// btnSource
			// 
			this.btnSource.Location = new System.Drawing.Point(405, 147);
			this.btnSource.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.btnSource.Name = "btnSource";
			this.btnSource.Size = new System.Drawing.Size(185, 31);
			this.btnSource.TabIndex = 1;
			this.btnSource.Text = "오디오 소스 테스트(&A)";
			this.btnSource.UseVisualStyleBackColor = true;
			this.btnSource.Click += new System.EventHandler(this.btnSource_Click);
			// 
			// btnPromptBuilder
			// 
			this.btnPromptBuilder.Location = new System.Drawing.Point(405, 108);
			this.btnPromptBuilder.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.btnPromptBuilder.Name = "btnPromptBuilder";
			this.btnPromptBuilder.Size = new System.Drawing.Size(185, 31);
			this.btnPromptBuilder.TabIndex = 1;
			this.btnPromptBuilder.Text = "&PrormptBuilder 테스트";
			this.btnPromptBuilder.UseVisualStyleBackColor = true;
			this.btnPromptBuilder.Click += new System.EventHandler(this.btnPromptBuilder_Click);
			// 
			// Form1
			// 
			this.AcceptButton = this.btnTest;
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(602, 439);
			this.Controls.Add(this.btnSource);
			this.Controls.Add(this.btnPromptBuilder);
			this.Controls.Add(this.btnTest);
			this.Controls.Add(this.tbText);
			this.Font = new System.Drawing.Font("맑은 고딕", 11.25F);
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox tbText;
		private System.Windows.Forms.Button btnTest;
		private System.Windows.Forms.Button btnSource;
		private System.Windows.Forms.Button btnPromptBuilder;
	}
}

