﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using CalculatePays;

namespace WinApps
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnSearch_Click(object sender, EventArgs e)
		{
			CalcPays calcPays = new CalcPays();

			calcPays.Name = txtName.Text;
			lblResult.Text = calcPays.Calculate();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}