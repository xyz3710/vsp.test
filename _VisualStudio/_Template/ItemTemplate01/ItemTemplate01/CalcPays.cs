﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculatePays
{
	public class CalcPays
	{
		private string _name;
		private int _pay;

		#region Constructor
		/// <summary>
		/// CalcPays class의 instance를 생성합니다.
		/// </summary>		
		public CalcPays(string name)
		{
			_name = name;
		}

		/// <summary>
		/// CalcPays class의 instance를 생성합니다.
		/// </summary>
		public CalcPays()
		{
			
		}
        
		#endregion
        
		#region Properties
		/// <summary>
		/// Name을(를) 구하거나 설정합니다.
		/// </summary>
		public String Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}
        
        /// <summary>
        /// Pay을(를) 구하거나 설정합니다.
        /// </summary>
		public int Pay
		{
			get
			{
				try
				{
					Random random = new Random();

					_pay = random.Next(1200000, 2000000);
				}
				finally
				{
					
				}

				return _pay;
			}			
		}
        
        #endregion

		public string Calculate()
		{
			string ret = String.Empty;

			ret = string.Format("{0}님의 급여는 {1:#,##0}원 입니다.", _name, Pay);

			return ret;			
		}
	}
}
