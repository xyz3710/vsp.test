﻿namespace ExceptionTestWinForm
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnRaiseException = new System.Windows.Forms.Button();
			this.btnRaiseDivideByZero = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnRaiseException
			// 
			this.btnRaiseException.Location = new System.Drawing.Point(76, 91);
			this.btnRaiseException.Name = "btnRaiseException";
			this.btnRaiseException.Size = new System.Drawing.Size(133, 23);
			this.btnRaiseException.TabIndex = 0;
			this.btnRaiseException.Text = "&Exception 발생";
			this.btnRaiseException.UseVisualStyleBackColor = true;
			this.btnRaiseException.Click += new System.EventHandler(this.btnRaiseException_Click);
			// 
			// btnRaiseDivideByZero
			// 
			this.btnRaiseDivideByZero.Location = new System.Drawing.Point(44, 150);
			this.btnRaiseDivideByZero.Name = "btnRaiseDivideByZero";
			this.btnRaiseDivideByZero.Size = new System.Drawing.Size(196, 23);
			this.btnRaiseDivideByZero.TabIndex = 0;
			this.btnRaiseDivideByZero.Text = "&Divied by 0 Exception 발생";
			this.btnRaiseDivideByZero.UseVisualStyleBackColor = true;
			this.btnRaiseDivideByZero.Click += new System.EventHandler(this.btnRaiseDivideByZero_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 264);
			this.Controls.Add(this.btnRaiseDivideByZero);
			this.Controls.Add(this.btnRaiseException);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnRaiseException;
		private System.Windows.Forms.Button btnRaiseDivideByZero;
	}
}

