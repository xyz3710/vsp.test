﻿/**********************************************************************************************************************/
/*	Domain		:	ExceptionTestWinForm.Form1
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2010년 7월 27일 화요일 오후 5:11
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace ExceptionTestWinForm
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void btnRaiseException_Click(object sender, EventArgs e)
		{			
			throw new ApplicationException("응용프로그램 오류 입니다.");
		}

		private void btnRaiseDivideByZero_Click(object sender, EventArgs e)
		{
			Thread thread = new Thread(delegate()
			{
				int i = 1;
				int j = 0;
				int c = i / j;
			});

			thread.Start();
		}
	}
}
