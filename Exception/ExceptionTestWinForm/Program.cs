﻿/**********************************************************************************************************************/
/*	Domain		:	ExceptionTestWinForm.Program
/*	Creator		:	KIMKIWON\xyz37(김기원)
/*	Create		:	2010년 7월 27일 화요일 오후 5:34
/*	Purpose		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Modifier	:	
/*	Update		:	
/*	Changes		:	
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Comment		:	
 * WindowsFomr 어플리케이션에서 처리되지 않은 예외중에서 
 * Primary thread에서 발생한 예외는 Application.ThreadException 처리기로 보내지고
 * 만약, Secondary thread에서 예외가 발생하면 AppDomain.UnhandledException 으로 보내진다.
 * 
 * Application.SetUnhandledExceptionMode가 설정되면 
 * 예외를 Application.ThreadException 처리기로 보내지 않고 
 * 다시 throw 시켜서 결국 AppDomain의 UnhandledException 처리기가 받게 된다.
 * http://dalbong2.net/entry/CLR의-Global-예외처리 참고
/*--------------------------------------------------------------------------------------------------------------------*/
/*	Reviewer	:	Kim Ki Won
/*	Rev. Date	:	
/**********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.SqlServer.MessageBox;
using System.Threading;
using System.Diagnostics;

namespace ExceptionTestWinForm
{
	static class Program
	{
		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		static void Main()
		{
			// NOTICE: Application.SetUnhandledExceptionMode가 설정되면 
			// 예외를 Application.ThreadException 처리기로 보내지 않고 
			// 다시 throw 시켜서 결국 AppDomain의 UnhandledException 처리기가 받게 된다.
			// by KIMKIWON\xyz37(김기원) in 2010년 7월 27일 화요일 오후 5:31
			//Application.SetUnhandledExceptionMode(UnhandledExceptionMode.Automatic);
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
			Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new Form1());
		}

		static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			ExceptionMessageBox exceptionMessageBox 
					= new ExceptionMessageBox(
					  "처리되지 않는 오류가 발생했습니다.\r\nCurrentDomain_UnhandledException",
					  "Global exception(WinForm)",
					  ExceptionMessageBoxButtons.Custom,
					  ExceptionMessageBoxSymbol.Exclamation,
					  ExceptionMessageBoxDefaultButton.Button1);

			exceptionMessageBox.SetButtonText("응용 프로그램 종료(&A)");
			exceptionMessageBox.InnerException = e.ExceptionObject as Exception;
			exceptionMessageBox.Show(null);

			if (exceptionMessageBox.CustomDialogResult == ExceptionMessageBoxDialogResult.Button1)
			{
				// Application.ThreadException은 해당 Exception을 처리하여 다음으로 진행하는데
				// CurrentDomain.UnhandledException은 종료 후에도 해당 Exception이 지속되어서 문제 발생
				// 다음으로 진행하는 버튼은 제거한다.
				Process.GetCurrentProcess().Kill();
			}
		}

		static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
		{
			ExceptionMessageBox exceptionMessageBox 
				= new ExceptionMessageBox(
					  "처리되지 않는 오류가 발생했습니다.\r\nApplication_ThreadException", 
                      "Global exception(WinForm)", 
                      ExceptionMessageBoxButtons.Custom, 
                      ExceptionMessageBoxSymbol.Exclamation, 
                      ExceptionMessageBoxDefaultButton.Button2);

			exceptionMessageBox.SetButtonText("응용 프로그램 종료(&A)", "계속(&C)");
			exceptionMessageBox.InnerException = e.Exception;
			exceptionMessageBox.Show(null);

			if (exceptionMessageBox.CustomDialogResult == ExceptionMessageBoxDialogResult.Button1)
				Application.Exit();
		}
	}
}
