﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.MessageBox;
using System.Windows.Forms;

namespace ExceptionTestConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

			throw new ApplicationException("응용프로그램 오류 입니다.");
		}

		static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			ExceptionMessageBox exceptionMessageBox 
				= new ExceptionMessageBox(
					  "처리되지 않는 오류가 발생했습니다.",
					  "Global exception(Console Application)",
					  ExceptionMessageBoxButtons.AbortRetryIgnore,
					  ExceptionMessageBoxSymbol.Exclamation,
					  ExceptionMessageBoxDefaultButton.Button1);

			exceptionMessageBox.InnerException = e.ExceptionObject as Exception;

			DialogResult result = exceptionMessageBox.Show(null);
		}
	}
}
