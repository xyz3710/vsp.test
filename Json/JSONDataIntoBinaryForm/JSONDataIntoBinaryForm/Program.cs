﻿// ****************************************************************************************************************** //
//	Domain		:	JSONDataIntoBinaryForm.Program
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2016년 11월 22일 화요일 오후 11:56
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	http://www.dotnetcurry.com/csharp/1279/serialize-json-data-binary
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="Program.cs" company="(주)가치소프트">
//		Copyright (c) 2016. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace JSONDataIntoBinaryForm
{
	class Program
	{
		static void Main(string[] args)
		{
			// 1. 
			string studentData = @"{
'StudentId':'1',
'StudentName':'MS',
'AcadmicYear':'First',
'Courses':[
		{'CourseId':'101', 'CourseName':'Compiler Design'},			
		{'CourseId':'101', 'CourseName':'Compiler Design'},
	]
}";
			// 2.
			var studentObject = JsonConvert.DeserializeObject(studentData);
			// 3.
			var jsonSerializer = new JsonSerializer();
			// 4.
			var objBsonMS = new MemoryStream();
			// 5.
			var bsonWriterObject = new BsonWriter(objBsonMS);
			// 6.
			jsonSerializer.Serialize(bsonWriterObject, studentObject);

			// 7.
			string encodingASCIIGetString = Encoding.ASCII.GetString(objBsonMS.ToArray());
			Console.WriteLine(encodingASCIIGetString);
			Console.WriteLine($"length: {encodingASCIIGetString.Length}");
			Console.WriteLine();

			string convertToBase64String = Convert.ToBase64String(objBsonMS.ToArray());
			Console.WriteLine(convertToBase64String);
			Console.WriteLine($"length: {convertToBase64String.Length}");
		}
	}
}
