:: ARS_Octopus의 packages 폴더를 link로 연결한다.
SET cwd=%~dp0
CD /d %cwd%

IF /i EXIST "packages" RD packages /s /q
mklink /d packages ..\packages
PAUSE
