﻿// ****************************************************************************************************************** //
//	Domain		:	QueryingJSONusingLINQ.Program
//	Creator		:	KIMKIWON\xyz37(Kim Ki Won)
//	Create		:	2016년 11월 22일 화요일 오후 11:56
//	Purpose		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Modifier	:	
//	Update		:	
//	Changes		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	Reviewer	:	
//	Rev. Date	:	
//	Comment		:	
// ------------------------------------------------------------------------------------------------------------------ //
//	<copyright file="Program.cs" company="(주)가치소프트">
//		Copyright (c) 2016. (주)가치소프트. All rights reserved.
//	</copyright>
// <summary></summary>
// ****************************************************************************************************************** //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace QueryingJSONusingLINQ
{
	class Program
	{
		static void Main(string[] args)
		{
			//1. Get the data in JSON Serialized Form
			var studentData = JsonConvert.SerializeObject(new StudentsDatabase(), Formatting.Indented);
			Console.WriteLine(studentData);

			//2. Convert the JSON string into an Array
			JArray studentArray = JArray.Parse(studentData);

			//3. Read all Student Names
			var resStudents = (from s in studentArray
							   select s["StudentName"]).ToArray();
			Console.WriteLine("Only Student Names");

			foreach (var item in resStudents)
			{
				Console.WriteLine(item.Value<string>());
			}

			//4. Get only Course Details
			Console.WriteLine();
			var result = (from s in studentArray.Children()["Courses"]
						  select s).ToList();

			Console.WriteLine("Course Details");
			foreach (var item in result.Children())
			{
				var course = item.ToObject<Course>();

				Console.WriteLine($"{course.CourseId}\t{course.CourseName}");
			}

			//5. Get the Data in JSON Serialized Form
			var employeeData = JsonConvert.SerializeObject(new EmployeesDatabase(), Formatting.Indented);

			//6. Convert the JSON string into an Array
			var employeeArray = JArray.Parse(employeeData);

			//7.
			var empGroupByDeptName = from e in employeeArray
									 group e by e["DeptName"] into deptGroup
									 select new
									 {
										 DeptName = deptGroup.Key,
										 EmpCount = deptGroup.Count(),
									 };

			Console.WriteLine("Department \t\t Total Employees Employee");
			foreach (var deptGrp in empGroupByDeptName)
			{
				Console.WriteLine($"{deptGrp.DeptName}\t\t{deptGrp.EmpCount}");
			}
		}
	}
}
