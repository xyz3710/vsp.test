﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestGabageCollector02
{
	class csDispose : IDisposable
	{
		private bool isDispose = false;

		public csDispose()
		{
			Console.WriteLine("Create Human class");
		}

		~csDispose()
		{
			// Dispose method와 resource 해제 하는 코드가 중복됨
			if (isDispose == false)
			{
				Dispose();
				// Dispose method가 호출 된적이 있는지 살펴본다.
				// 호출된적이 있다면 GC.SuppressFinalize가 호출되었을 것이므로 
				// finalize method를 실행시키지 않는다.
			}
			else
			{
				Console.WriteLine("Destroy Human class");
			}
		}

		public void Dispose()
		{
			// resource를 해제함
			Console.WriteLine("Running Dispose");
			GC.SuppressFinalize(this);		// ~csDispose() method를 실행하지 못하도록 설정함
			this.isDispose = true;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			csDispose cd = new csDispose();

			cd.Dispose();

			Console.WriteLine("Program terminated.");

			Console.Read();
		}
	}
}
