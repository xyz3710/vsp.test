﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections;

namespace TestGarbageCollector02_Agent
{
	public class Agent
	{
		private Stack objects = new Stack();

		public void Push(object o)
		{
			objects.Push(o);
		}

		public void InvokeAll()
		{
			foreach (object o in objects)
			{
				Type type = o.GetType();

				try
				{
					type.InvokeMember("Dispose",
						BindingFlags.InvokeMethod, null, o, null);
				}
				catch (Exception e)
				{
					Console.WriteLine("{0} could not Disposed.\r\n\t{1}", type.Name, e.Message);
				}
			}

			objects.Clear();
		}
	}

	public class X
	{
		private int n;

		public X(int n)
		{
			Console.WriteLine("\tX() {0}", n);

			this.n = n;
		}

		~X()
		{
			Dispose();
		}

		public void Dispose()
		{
			Console.WriteLine("Dispose() {0}", n);

			GC.SuppressFinalize(this);
		}
	}

	public class Y
	{
		private int n;

		public Y(int n)
		{
			Console.WriteLine("\tY() {0}", n);

			this.n = n;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Agent agent = new Agent();

			for (int i = 0; i < 3; i++)
			{
				agent.Push(new X(i));
				agent.Push(new Y(i+5));
			}

			agent.InvokeAll();

			Console.ReadLine();
		}
	}
}
