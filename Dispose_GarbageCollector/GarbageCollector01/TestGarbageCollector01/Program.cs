﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestGarbageCollector01
{
	class X
	{
		public int value;

		public X(int value)
		{
			this.value = value;
		}

		public static X operator+(X left, X right)
		{
			return new X(left.value + right.value);
		}
	}

	public class Y
	{
		private int n;

		public Y(int n)
		{
			this.n = n;
		}

		~Y()
		{
			Console.WriteLine("~X() {0}", n);
		}

		public void Dispose()
		{
			//Finalize();
			GC.SuppressFinalize(this);
		}
	}

	public class Programs
	{
		static void Main(string[] args)
		{
			X a, b, c;

			c = new X(0);
			a = b = new X(10);

			c = a + b;

			Console.WriteLine(c.value);

			
			Console.WriteLine();


			finalize();
			GC.Collect();
			GC.WaitForPendingFinalizers();

			Console.ReadLine();
		}

		private static void finalize()
		{
			Y y1 = new Y(10);
			Y y2 = new Y(20);

			y1.Dispose();
		}
	}
}
