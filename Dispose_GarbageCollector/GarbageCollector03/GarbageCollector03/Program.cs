﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestGabageCollector01
{
	class Human
	{
		public static int num = 0;

		public Human()
		{
			Console.WriteLine("Create Human class");
			Console.WriteLine("Human count : {0}", ++num);
		}

		~Human()
		{
			// 소멸자는 접근 권한이 항상 public 이어야 하기때문에 별도의 접근권한을 지정하지 못하도록 되어있다.
			// 생성자와 다른점은 parameter를 받을 수 없다.

			Console.WriteLine("Destroy Human class");
			Console.WriteLine("Human count : {0}", --num);
		}
	}

	class Program
	{
		static void callHuman()
		{
			Human man1 = new Human();
			Human man2 = new Human();
			Human man3 = new Human();
			Human man4 = new Human();
			Human man5 = new Human();

			Console.WriteLine(Human.num);
		}

		static void callHumans(int count)
		{
			for (int i = 0; i < count; i++)
			{
				new Human();
			}

			Console.WriteLine(Human.num);
		}

		static void Main(string[] args)
		{
			callHuman();

			Console.WriteLine();


			callHumans(3);

			Console.ReadLine();
		}
	}
}
