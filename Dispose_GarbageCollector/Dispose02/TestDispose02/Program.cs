﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDispose02
{
	class X
	{
		public X()
		{
			Console.WriteLine("X()");
		}

		~X()
		{
			Console.WriteLine("~X()");
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			new X();

			GC.Collect();
			GC.WaitForPendingFinalizers();
		}
	}
}
