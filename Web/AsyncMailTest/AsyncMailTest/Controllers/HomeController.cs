﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using Luxury.SmartAudio.Common.Utils;

namespace MailTest.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.Message = "ASP.NET MVC 시작";

			return View();
		}

		public ActionResult About()
		{
			Exception ex1 = new Exception("단계1");
			Exception ex2 = new Exception("단계2", ex1);
			Exception ex3 = new Exception("단계3", ex2);
			Exception ex4 = new Exception("단계4", ex3);

			Stack stack = new Stack();

			stack.Push(ex4);

			while (ex4.InnerException != null)
			{
				Exception exBase = stack.Pop() as Exception;

				stack.Push(exBase.InnerException);
			}
			return View();
		}

		public ActionResult SendMail()
		{
			MailSender.SendAsync("xyz37@naver.com", "비동기 테스트 메일", "내용 없음");

			return View("Index");
		}
	}
}
