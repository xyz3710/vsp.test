﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;

namespace CalcServiceTest
{
	/// <summary>
	/// CalcServiceTest의 요약 설명
	/// </summary>
	[TestClass]
	public class CalcServiceTest
	{
		public CalcServiceTest()
		{
			//
			// TODO: 여기에 생성자 논리를 추가합니다.
			//
		}

		private TestContext testContextInstance;

		/// <summary>
		///현재 테스트 실행에 대한 정보 및 기능을
		///제공하는 테스트 컨텍스트를 가져오거나 설정합니다.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region 추가 테스트 특성
		//
		// 테스트를 작성할 때 다음 추가 특성을 사용할 수 있습니다.
		//
		// ClassInitialize를 사용하여 클래스의 첫 번째 테스트를 실행하기 전에 코드를 실행합니다.
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// ClassCleanup을 사용하여 클래스의 테스트를 모두 실행한 후에 코드를 실행합니다.
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// 테스트를 작성할 때 다음 추가 특성을 사용할 수 있습니다. 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// TestInitialize를 사용하여 각 테스트를 실행하기 전에 코드를 실행합니다.
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		private WS.Proxy.CalcService _calcService;

		[TestMethod]
		public void CallAdd()
		{
			const int VALUE1 = 100;
			const int VALUE2 = 200;
			_calcService = new WS.Proxy.CalcService();
			AsyncCallback asyncCallback = new AsyncCallback(AddInvokeEnd);
			IAsyncResult result = _calcService.BeginAdd(VALUE1, VALUE2, asyncCallback, null);
			string resultValue = string.Empty;

			AutoResetEvent autoResetEvent = new AutoResetEvent(true);

			autoResetEvent.WaitOne();

			if (result.IsCompleted == true)
				resultValue = string.Format("{0} + {1} add Result : {2}", VALUE1, VALUE2, _calcService.EndAdd(result));

			Assert.AreEqual(300, resultValue);
		}

		private void AddInvokeEnd(IAsyncResult asyncResult)
		{			
			Assert.AreEqual(300, _calcService.EndAdd(asyncResult));
		}
	}
}
