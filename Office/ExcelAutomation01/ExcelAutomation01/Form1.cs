﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ExcelIo=Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel; 

namespace ExcelAutomation01
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			ExcelIo.Application oXL;
			ExcelIo._Workbook oWB;
			ExcelIo._Worksheet oSheet;
			ExcelIo.Range oRng;

			try
			{
				//Start Excel and get Application object.
				oXL = new ExcelIo.Application();
				oXL.Visible = true;

				//Get a new workbook.
				oWB = (ExcelIo._Workbook)(oXL.Workbooks.Add(Missing.Value));
				oSheet = (ExcelIo._Worksheet)oWB.ActiveSheet;

				//Add table headers going cell by cell.
				oSheet.Cells[1, 1] = "First Name";
				oSheet.Cells[1, 2] = "Last Name";
				oSheet.Cells[1, 3] = "Full Name";
				oSheet.Cells[1, 4] = "Salary";

				//Format A1:D1 as bold, vertical alignment = center.
				oSheet.get_Range("A1", "D1").Font.Bold = true;
				oSheet.get_Range("A1", "D1").VerticalAlignment = 
			ExcelIo.XlVAlign.xlVAlignCenter;

				// Create an array to multiple values at once.
				string[,] saNames = new string[5, 2];

				saNames[0, 0] = "John";
				saNames[0, 1] = "Smith";
				saNames[1, 0] = "Tom";
				saNames[1, 1] = "Brown";
				saNames[2, 0] = "Sue";
				saNames[2, 1] = "Thomas";
				saNames[3, 0] = "Jane";
				saNames[3, 1] = "Jones";
				saNames[4, 0] = "Adam";
				saNames[4, 1] = "Johnson";

				//Fill A2:B6 with an array of values (First and Last Names).
				oSheet.get_Range("A2", "B6").Value2 = saNames;

				//Fill C2:C6 with a relative formula (=A2 & " " & B2).
				oRng = oSheet.get_Range("C2", "C6");
				oRng.Formula = "=A2 & \" \" & B2";

				//Fill D2:D6 with a formula(=RAND()*100000) and apply format.
				oRng = oSheet.get_Range("D2", "D6");
				oRng.Formula = "=RAND()*100000";
				oRng.NumberFormat = "$0.00";

				//AutoFit columns A:D.
				oRng = oSheet.get_Range("A1", "D1");
				oRng.EntireColumn.AutoFit();

				//Manipulate a variable number of columns for Quarterly Sales Data.
				DisplayQuarterlySales(oSheet);

				//Make sure Excel is visible and give the user control
				//of Microsoft Excel's lifetime.
				oXL.Visible = true;
				oXL.UserControl = true;
			}
			catch (Exception theException)
			{
				String errorMessage;
				errorMessage = "Error: ";
				errorMessage = String.Concat(errorMessage, theException.Message);
				errorMessage = String.Concat(errorMessage, " Line: ");
				errorMessage = String.Concat(errorMessage, theException.Source);

				MessageBox.Show(errorMessage, "Error");
			}
		}

		private void DisplayQuarterlySales(Microsoft.Office.Interop.Excel._Worksheet oWS)
		{
			ExcelIo._Workbook oWB;
			ExcelIo.Series oSeries;
			ExcelIo.Range oResizeRange;
			ExcelIo._Chart oChart;
			String sMsg;
			int iNumQtrs;

			//Determine how many quarters to display data for.
			for (iNumQtrs = 4; iNumQtrs >= 2; iNumQtrs--)
			{
				sMsg = "Enter sales data for ";
				sMsg = String.Concat(sMsg, iNumQtrs);
				sMsg = String.Concat(sMsg, " quarter(s)?");

				DialogResult iRet = MessageBox.Show(sMsg, "Quarterly Sales?",
					MessageBoxButtons.YesNo);
				if (iRet == DialogResult.Yes)
					break;
			}

			sMsg = "Displaying data for ";
			sMsg = String.Concat(sMsg, iNumQtrs);
			sMsg = String.Concat(sMsg, " quarter(s).");

			MessageBox.Show(sMsg, "Quarterly Sales");

			//Starting at E1, fill headers for the number of columns selected.
			oResizeRange = oWS.get_Range("E1", "E1").get_Resize(Missing.Value, iNumQtrs);
			oResizeRange.Formula = "=\"Q\" & COLUMN()-4 & CHAR(10) & \"Sales\"";

			//Change the Orientation and WrapText properties for the headers.
			oResizeRange.Orientation = 38;
			oResizeRange.WrapText = true;

			//Fill the interior color of the headers.
			oResizeRange.Interior.ColorIndex = 36;

			//Fill the columns with a formula and apply a number format.
			oResizeRange = oWS.get_Range("E2", "E6").get_Resize(Missing.Value, iNumQtrs);
			oResizeRange.Formula = "=RAND()*100";
			oResizeRange.NumberFormat = "$0.00";

			//Apply borders to the Sales data and headers.
			oResizeRange = oWS.get_Range("E1", "E6").get_Resize(Missing.Value, iNumQtrs);
			oResizeRange.Borders.Weight = ExcelIo.XlBorderWeight.xlThin;

			//Add a Totals formula for the sales data and apply a border.
			oResizeRange = oWS.get_Range("E8", "E8").get_Resize(Missing.Value, iNumQtrs);
			oResizeRange.Formula = "=SUM(E2:E6)";
			oResizeRange.Borders.get_Item(ExcelIo.XlBordersIndex.xlEdgeBottom).LineStyle 
		= ExcelIo.XlLineStyle.xlDouble;
			oResizeRange.Borders.get_Item(ExcelIo.XlBordersIndex.xlEdgeBottom).Weight 
		= ExcelIo.XlBorderWeight.xlThick;

			//Add a Chart for the selected data.
			oWB = (ExcelIo._Workbook)oWS.Parent;
			oChart = (ExcelIo._Chart)oWB.Charts.Add(Missing.Value, Missing.Value,
				Missing.Value, Missing.Value);

			//Use the ChartWizard to create a new chart from the selected data.
			oResizeRange = oWS.get_Range("E2:E6", Missing.Value).get_Resize(
				Missing.Value, iNumQtrs);
			oChart.ChartWizard(oResizeRange, ExcelIo.XlChartType.xl3DColumn, Missing.Value,
				ExcelIo.XlRowCol.xlColumns, Missing.Value, Missing.Value, Missing.Value,
				Missing.Value, Missing.Value, Missing.Value, Missing.Value);
			oSeries = (ExcelIo.Series)oChart.SeriesCollection(1);
			oSeries.XValues = oWS.get_Range("A2", "A6");
			for (int iRet = 1; iRet <= iNumQtrs; iRet++)
			{
				oSeries = (ExcelIo.Series)oChart.SeriesCollection(iRet);
				String seriesName;
				seriesName = "=\"Q";
				seriesName = String.Concat(seriesName, iRet);
				seriesName = String.Concat(seriesName, "\"");
				oSeries.Name = seriesName;
			}

			oChart.Location(ExcelIo.XlChartLocation.xlLocationAsObject, oWS.Name);

			//Move the chart so as not to cover your data.
			oResizeRange = (ExcelIo.Range)oWS.Rows.get_Item(10, Missing.Value);
			oWS.Shapes.Item("Chart 1").Top = (float)(double)oResizeRange.Top;
			oResizeRange = (ExcelIo.Range)oWS.Columns.get_Item(2, Missing.Value);
			oWS.Shapes.Item("Chart 1").Left = (float)(double)oResizeRange.Left;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			string fileName = string.Format(@"{0}\{1}", System.Windows.Forms.Application.StartupPath, "1110_B.xls");
			ExcelIo.Application oXl = new ExcelIo.Application();
			Series oSeries;
			_Chart oChart;
			Range chartResizeRange;
			_Workbook workBook = null;
			_Worksheet workSheet = null;

			try
			{
				workBook = oXl.Workbooks.Open(fileName, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
				workSheet = (ExcelIo._Worksheet)workBook.ActiveSheet;

				oChart = (ExcelIo._Chart)workBook.Charts.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
				chartResizeRange = workSheet.get_Range("B26:N26", Missing.Value).get_Resize(Missing.Value, Missing.Value);

				oChart.ChartType = ExcelIo.XlChartType.xlLineMarkers;
				oChart.SetSourceData(chartResizeRange, ExcelIo.XlRowCol.xlRows);
				
				ExcelIo.Range seriesResizeRange = workSheet.get_Range("C24:N24", Missing.Value).get_Resize(Missing.Value, Missing.Value);
				
				oSeries = (Series)oChart.SeriesCollection(1);
				oSeries.XValues = seriesResizeRange;

				Chart activeChart = oChart.Location(ExcelIo.XlChartLocation.xlLocationAsObject, "연간 B Trend");

				activeChart.HasTitle = true;
				activeChart.ChartTitle.Text = "B AVG";
				
				ExcelIo.Axis axisCategory = (ExcelIo.Axis)activeChart.Axes(ExcelIo.XlAxisType.xlCategory, ExcelIo.XlAxisGroup.xlPrimary);
				ExcelIo.Axis axisPrimary = (ExcelIo.Axis)activeChart.Axes(ExcelIo.XlAxisType.xlValue, ExcelIo.XlAxisGroup.xlPrimary);

				axisCategory.HasTitle = false;
				axisPrimary.HasTitle = true;
				axisPrimary.AxisTitle.Text = "ppba";

				activeChart.HasLegend = true;
				activeChart.Legend.Position = XlLegendPosition.xlLegendPositionBottom;

				// Chart 위치 이동
				_Worksheet activeSheet = (_Worksheet)workBook.ActiveSheet;

				Shape chartShape = activeSheet.Shapes._Default(1);

				chartShape.Height = 12.75f * 20f;
				chartShape.Width = 855f;
				chartShape.Top = 34.6f;
				chartShape.Left = 0f;
				//chartShape.IncrementLeft(-310.25f);
				//chartShape.IncrementTop(-140.25f);
				//chartShape.ScaleWidth(1.38f, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoScaleFrom.msoScaleFromTopLeft);
				//chartShape.ScaleHeight(0.78f, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoScaleFrom.msoScaleFromTopLeft);
				#region For Test
				/*
				chartShape.IncrementLeft(-231f);
				chartShape.IncrementTop(-150.9f);
				chartShape.ScaleWidth(1.835f, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoScaleFrom.msoScaleFromTopLeft);
				chartShape.ScaleHeight(0.71f, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoScaleFrom.msoScaleFromTopLeft);
				chartShape.IncrementTop(12.75f);
				*/
				#endregion

				oXl.ActiveWorkbook.Save();
				MessageBox.Show("저장되었습니다.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);                
			}
			catch (COMException ex)
			{
				Debug.Write(ex.ErrorCode);	
			}
			finally
			{
				if (oXl != null)
					oXl.Workbooks.Close();                
			}			
		}

		private void button3_Click(object sender, EventArgs e)
		{
			string fileName = string.Format(@"{0}\{1}", System.Windows.Forms.Application.StartupPath, @"2120.xls");
			ExcelIo.Application oXl = new ExcelIo.Application();
			Series oSeries = null;
			_Chart oChart = null;
			Range chartResizeRange = null;
			_Workbook workBook = null;
			_Worksheet workSheet = null;
			
			try
			{
				workBook = oXl.Workbooks.Open(fileName, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
				workSheet = (ExcelIo._Worksheet)workBook.ActiveSheet;

				oChart = (ExcelIo._Chart)workBook.Charts.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
				oChart.ChartType = ExcelIo.XlChartType.xlLineMarkers;

				string valueRagnes = "B26:N26,B33:N33,B40:N40";
				string titleRanges = "A25:A31,A32:A38,A39:A45";
				string valueRangeString = string.Empty;
				string[] rangeValues = valueRagnes.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
				string[] rangeTitles = titleRanges.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
				List<string> seriseTitles = new List<string>();

				try
				{
					for (int i = 0; i < rangeValues.Length; i++)
					{
						string cellValue = ((object[,])workSheet.get_Range(rangeTitles[i], Missing.Value).Value2)[1, 1] as string;

						if (string.IsNullOrEmpty(cellValue) == true)
                        	continue;

						seriseTitles.Add(cellValue);
						valueRangeString += string.Format("{0},", rangeValues[i]);
					}					
				}
				catch
				{
				}

				oChart.SetSourceData(workSheet.get_Range(valueRangeString.Substring(0, valueRangeString.Length - 1), Missing.Value), ExcelIo.XlRowCol.xlRows);

				ExcelIo.Range seriesResizeRange = workSheet.get_Range("C24:N24", Missing.Value).get_Resize(Missing.Value, Missing.Value);

				try
				{
					for (int i = 0; i < seriseTitles.Count; i++)
					{
						oSeries = (Series)oChart.SeriesCollection(i + 1);
						oSeries.XValues = seriesResizeRange;
						oSeries.Name = seriseTitles[i];
					}					
				}
				catch 
				{					
				}

				Chart activeChart = oChart.Location(ExcelIo.XlChartLocation.xlLocationAsObject, "월간 Trend");

				activeChart.HasTitle = true;
				activeChart.ChartTitle.Text = "B AVG";

				ExcelIo.Axis axisCategory = (ExcelIo.Axis)activeChart.Axes(ExcelIo.XlAxisType.xlCategory, ExcelIo.XlAxisGroup.xlPrimary);
				ExcelIo.Axis axisPrimary = (ExcelIo.Axis)activeChart.Axes(ExcelIo.XlAxisType.xlValue, ExcelIo.XlAxisGroup.xlPrimary);

				axisCategory.HasTitle = false;
				axisPrimary.HasTitle = true;
				axisPrimary.AxisTitle.Text = "ppba";

				activeChart.HasLegend = true;
				activeChart.Legend.Position = XlLegendPosition.xlLegendPositionBottom;

				// Chart 위치 이동
				_Worksheet activeSheet = (_Worksheet)workBook.ActiveSheet;

				Shape chartShape = activeSheet.Shapes._Default(1);

				chartShape.Height = 12.75f * 20f;
				chartShape.Width = 855f;
				chartShape.Top = 34.6f;
				chartShape.Left = 0f;
				//chartShape.IncrementLeft(-310.25f);
				//chartShape.IncrementTop(-140.25f);
				//chartShape.ScaleWidth(1.38f, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoScaleFrom.msoScaleFromTopLeft);
				//chartShape.ScaleHeight(0.78f, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoScaleFrom.msoScaleFromTopLeft);
				#region For Test
				/*
				chartShape.IncrementLeft(-231f);
				chartShape.IncrementTop(-150.9f);
				chartShape.ScaleWidth(1.835f, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoScaleFrom.msoScaleFromTopLeft);
				chartShape.ScaleHeight(0.71f, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoScaleFrom.msoScaleFromTopLeft);
				chartShape.IncrementTop(12.75f);
				*/
				#endregion

				oXl.ActiveWorkbook.Save();
				MessageBox.Show("저장되었습니다.", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (COMException ex)
			{
				Debug.Write(ex.ErrorCode);
			}
			finally
			{
				if (oXl != null)
					oXl.Workbooks.Close();
			}	
		}
	}
}